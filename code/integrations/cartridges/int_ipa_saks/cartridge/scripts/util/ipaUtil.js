'use strict';

var Transaction = require('dw/system/Transaction');
var collections = require('*/cartridge/scripts/util/collections');
var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
var Logger = require('dw/system/Logger');
var ipaLogger = Logger.getLogger('IPA-Service-Log', 'IPA-Service');
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');
var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
var Result = require('dw/svc/Result');
var enableAuthLoggers = ipaConstants.enableAuthLoggers;
var allowOrderIFServiceDown = ipaConstants.allowOrderIfAuthServiceDown;
var preferences = require('*/cartridge/config/preferences');

/**
 *
 * Update the response to order payment instrument
 * @params {Object} Order  json argument object
 */

function updateAuthResponseToOrderPI(Order, resultObject) {
  var ipaPayments = resultObject.payments;
  var paymentInstruments = Order.getPaymentInstruments();
  collections.forEach(paymentInstruments, function (pi) {
    for (var i = 0; i < ipaPayments.length; i++) {
      var ipaPayment = ipaPayments[i];
      var card = ipaPayment.card ? ipaPayment.card : ipaPayment.Card;
      var token = card ? card.token_number : null;
      var number = card ? card.number : null;
      if (
        (pi.paymentMethod === ipaConstants.CREDIT_CARD_PAYMENT_METHOD && pi.creditCardToken.substring(0, 16) === token) ||
        ((pi.paymentMethod === ipaConstants.GIFT_CARD || pi.paymentMethod === 'GIFT_CARD') && pi.custom.giftCardNumber === number) ||
        pi.paymentMethod === ipaConstants.PAYPAL ||
        pi.paymentMethod === ipaConstants.REWARD_POINTS
      ) {
        Transaction.wrap(function () {
          if (ipaPayment.authorization.authorization_code) {
            pi.custom.authorization_code = ipaPayment.authorization.authorization_code;
          }
          if (ipaPayment.authorization.authorization_reference) {
            pi.custom.authorization_reference = ipaPayment.authorization.authorization_reference;
          }
          if (ipaPayment.authorization.cvv_response_code) {
            pi.custom.cvv_response_code = ipaPayment.authorization.cvv_response_code;
          }
          if (ipaPayment.authorization.avs_response_code) {
            pi.custom.avs_response_code = ipaPayment.authorization.avs_response_code;
          }
          if (ipaPayment.authorization.authorization_date) {
            pi.custom.authorization_date = ipaPayment.authorization.authorization_date;
          }
          if (ipaPayment.authorization.authorization_expiration_date) {
            pi.custom.authorization_expiration_date = ipaPayment.authorization.authorization_expiration_date;
          }
          if (ipaPayment.authorization.max_charge_limit) {
            pi.custom.max_charge_limit = ipaPayment.authorization.max_charge_limit;
          }
          if (ipaPayment.authorization.authorization_terminal_number) {
            pi.custom.authorization_terminal_number = ipaPayment.authorization.authorization_terminal_number;
          }
          if (ipaPayment.authorization.authorization_transaction_number) {
            pi.custom.authorization_transaction_number = ipaPayment.authorization.authorization_transaction_number;
          }
          if (ipaPayment.authorization.merchant_reference_number) {
            pi.custom.merchant_reference_number = ipaPayment.authorization.merchant_reference_number;
          }
          if (ipaPayment.authorization.authorization_tracer) {
            pi.custom.authorization_tracer = ipaPayment.authorization.authorization_tracer;
          }
          if (ipaPayment.authorization.authorization_reference_data_code) {
            pi.custom.authorization_reference_data_code = ipaPayment.authorization.authorization_reference_data_code;
          }
          if (ipaPayment.authorization.authorization_pos_data_code) {
            pi.custom.authorization_pos_data_code = ipaPayment.authorization.authorization_pos_data_code;
          }
          if (ipaPayment.result_code) {
            if (ipaPayment.result_code.indexOf(':') != -1) {
              var responseCodeMessage = ipaPayment.result_code.split(':');
              pi.custom.response_code = responseCodeMessage[0];
              pi.custom.response_message = responseCodeMessage[1];
            } else if (ipaPayment.result_code) {
              pi.custom.response_code = ipaPayment.result_code;
              pi.custom.response_message = resultObject.response_message || '';
            }
          }
          if (tsysHelper.isTsysMode()) {
            if (pi.creditCardType === 'SAKS' || pi.creditCardType === 'SAKSMC') {
              if (ipaPayment.authorization.card_acceptor_id) {
                pi.custom.card_acceptor_id = ipaPayment.authorization.card_acceptor_id;
              }
              if (ipaPayment.authorization.authorization_terminal_capability) {
                pi.custom.authorization_terminal_capability = ipaPayment.authorization.authorization_terminal_capability;
              }
            }
          }

          if (pi.paymentMethod === ipaConstants.PAYPAL) {
            if (ipaPayment.Paypal && ipaPayment.Paypal.authorization_id) {
              pi.custom.ipa_authorization_id = ipaPayment.Paypal.authorization_id;
            }
            if (ipaPayment.Paypal && ipaPayment.Paypal.transactionID) {
              pi.custom.ipa_transactionID = ipaPayment.Paypal.transactionID;
            }
            if (ipaPayment.Paypal && ipaPayment.Paypal.EASToken) {
              pi.custom.ipa_EASToken = ipaPayment.Paypal.EASToken;
            }
            if (ipaPayment.Paypal && ipaPayment.Paypal.status) {
              pi.custom.ipa_status = ipaPayment.Paypal.status;
            }
            if (ipaPayment.Paypal && ipaPayment.Paypal.RequestID) {
              pi.custom.ipa_requestID = ipaPayment.Paypal.RequestID;
            }
            if (ipaPayment.Paypal && ipaPayment.Paypal.token) {
              pi.custom.ipa_token = ipaPayment.Paypal.token;
            }
            if (ipaPayment.Paypal && ipaPayment.Paypal.PayerID) {
              pi.custom.ipa_PayerID = ipaPayment.Paypal.PayerID;
            }
          }
        });
      }
    }
  });
}

/**
 *
 * calling from Cart Page
 * @params {Object} args  json argument object
 * @return {Object} result- response of the IPA authorization
 */
function authorizeCreditCard(args) {
  var iapAuthRequest = require('*/cartridge/models/ipaAuthRequest');
  var ipaAuthService = new (require('*/cartridge/scripts/init/ipaAuthorizationService'))();
  var ipaRequest = iapAuthRequest.getRequestJSON(args);
  var result = ipaAuthService.Authorize(ipaRequest);
  var order = args.Order;
  var response = {};
  response.error = false;
  if (enableAuthLoggers) {
    ipaLogger.info(
      '*********************************************************ORder {0} IPA Request*****************************************',
      args.Order.orderNo
    );
    var requestJsonString = JSON.stringify(ipaRequest);
    ipaLogger.info(requestJsonString);
  }
  // Check for service availability.
  if (result.status === Result.SERVICE_UNAVAILABLE) {
    Logger.getRootLogger().fatal('[ipaUtil.js Authorize] Error : SERVICE_UNAVAILABLE response');
  }

  var resultjson = JSON.stringify(result.object); // TODO remove later
  if (enableAuthLoggers) {
    ipaLogger.info(JSON.stringify(result.object));
  }

  if (result.status !== 'OK' || !result.object || result.object.error) {
    if (!allowOrderIFServiceDown) {
      response.error = true;
      response.errorMessage = result.errorMessage;
    }
    // update the fraud hold status on the order
    Transaction.wrap(function () {
      order.custom.orderStatus = ['AUTH_HOLD'];
    });
    Logger.getRootLogger().fatal('[ipaUtil.js Authorize] : SERVICE_ERROR response' + result.errorMessage);
  } else if (result.object) {
    if (result.object.response_code == 1 || result.object.response_code == 2) {
      updateAuthResponseToOrderPI(args.Order, result.object);
      if (result.object.response_code == 1) {
        COHelpers.updateCustomerPI(args.customer, args.Order);
      }
      if (result.object.response_code == 2) {
        // update the fraud hold status on the order
        Transaction.wrap(function () {
          order.custom.orderStatus = ['HBC_FRAUD_REVIEW'];
        });
      }
    } else if (
      result.object.response_code == 97 ||
      result.object.response_code == 99
    ) {
      var payments = result.object.payments;
      var isAuthApproved = true;
      for (var i = 0; i < result.object.payments.length; i++) {
        var payment = payments[i];
        if (payment.result_code.toLowerCase().indexOf('1') === -1) {
          isAuthApproved = false;
          break;
        }
      }
      if (isAuthApproved) {
        updateAuthResponseToOrderPI(args.Order, result.object);
        // update the fraud hold status on the order
        Transaction.wrap(function () {
          order.custom.orderStatus = ['FRAUD_HOLD'];
        });
      } else {
        // for off5th

        response.error = !allowOrderIFServiceDown;

        // if the service down and order is not supposed to go through
        if (allowOrderIFServiceDown) {
          Transaction.wrap(function () {
            order.custom.orderStatus = ['AUTH_HOLD'];
          });
        }
      }
    } else if (result.object.response_code == 0) { // IPA returns 0 string value. 3 equals will not evaluate to true. changing it 2 equals..
      // C2NFTS-194 Forter | forter approved that order which are Auth or IPA declined.
      updateAuthResponseToOrderPI(args.Order, result.object);
      var result_codes_array = ["0", "95", "96"];
      var payments = result.object.payments;
      var isAuthApproved = true;
      for (var i = 0; i < result.object.payments.length; i++) {
        var payment = payments[i];
        if (result_codes_array.indexOf(payment.result_code) !== -1) {
          isAuthApproved = false;
          break;
        }
      }
      if (!isAuthApproved) {
        // Not sure what to do. Row 9
        response.error = true;
        response.errorMessage = result.object.response_message;
        ipaLogger.info('Authorization failure: {0}', result.object.response_message);

        // If rewards points were used in the order - call payment auth reversal for loyalty payments
        var hasRewardsPaymentData = payments.some(function (paymentType) {
          return paymentType.Loyalty;
        });
        if (hasRewardsPaymentData) {
          // Call Reverse Auth
          loyaltyAuthorizationReversal(args, payments);
        }
      } else {
        Transaction.wrap(function () {
          order.custom.orderStatus = ['FRAUD_HOLD'];
        });
      }
    } else {
      response.error = true;
      response.errorMessage = result.object.response_message;
      ipaLogger.info('Authorization failure: {0}', result.object.response_message);
    }
  }
  return response;
}

/**
 *
 * calling from Cart Page
 * @params {Object} args  json argument object
 * @return {Object} result- response of the IPA authorization
 */
function paypalFraud(args) {
  var iapAuthRequest = require('*/cartridge/models/ipaAuthRequest');
  var ipaAuthService = new (require('*/cartridge/scripts/init/ipaAuthorizationService'))();
  var ipaRequest = iapAuthRequest.getRequestJSON(args);
  var result = ipaAuthService.Authorize(ipaRequest);
  var response = {};
  var order = args.Order;
  response.error = false;
  if (enableAuthLoggers) {
    var requestJsonString = JSON.stringify(ipaRequest);
    ipaLogger.info(requestJsonString);
  }
  // Check for service availability.
  if (result.status === Result.SERVICE_UNAVAILABLE) {
    Logger.getRootLogger().fatal('[ipaUtil.js] Error : SERVICE_UNAVAILABLE response');
  }
  if (enableAuthLoggers) {
    ipaLogger.info(JSON.stringify(result.object));
  }
  if (result.status != 'OK' || !result.object) {
    response.error = true;
    response.errorMessage = result.errorMessage;
    ipaLogger.info('Authorization failure: {0}', result.errorMessage);
    Logger.getRootLogger().fatal('[ipaUtil.js PaypalFraud] : SERVICE_ERROR - PayPal Fraud Error Response');
  } else if (result.object && (result.object.response_code == 1 || result.object.response_code == 2)) {
    updateAuthResponseToOrderPI(args.Order, result.object);
    if (result.object.response_code == 2) {
      // update the fraud hold status on the order
      Transaction.wrap(function () {
        order.custom.orderStatus = ['HBC_FRAUD_REVIEW'];
      });
    }
  } else if (result.object && (result.object.response_code == 97 || result.object.response_code == 99)) {
    Transaction.wrap(function () {
      order.custom.orderStatus = ['FRAUD_HOLD'];
    });
  } else {
    response.error = true;
    response.errorMessage = result.object.response_message;
    ipaLogger.info('Authorization failure: {0}', result.object.response_message);
  }
  return response;
}

function authorizationReversal(args) {
  var iapAuthRequest = require('*/cartridge/models/ipaAuthRequest');
  var ipaAuthService = new (require('*/cartridge/scripts/init/ipaAuthorizationService'))();
  var ipaRequest = iapAuthRequest.getRequestJSON(args);
  var result = ipaAuthService.Authorize(ipaRequest);
  var response = {};
  response.error = false;
  if (enableAuthLoggers) {
    var requestJsonString = JSON.stringify(ipaRequest);
    ipaLogger.info(requestJsonString);
  }
  // Check for service availability.
  if (result.status === Result.SERVICE_UNAVAILABLE) {
    Logger.getRootLogger().fatal('[ipaUtil.js authorizationReversal] Error : SERVICE_UNAVAILABLE response');
  }
  if (enableAuthLoggers) {
    ipaLogger.info(JSON.stringify(result.object));
  }
  if (result.status != 'OK' || !result.object) {
    response.error = true;
    response.errorMessage = result.errorMessage;
    ipaLogger.info('Authorization failure: {0}', result.errorMessage);
  } else if (result.object && (result.object.response_code == 1 || result.object.response_code == 2)) {
    // do nothing
  } else {
    response.error = true;
    response.errorMessage = result.object.response_message;
    ipaLogger.info('Authorization failure: {0}', result.object.response_message);
  }
  return result;
}

function loyaltyAuthorizationReversal(args, payments) {
  var ipaAuthRequest = require('*/cartridge/models/ipaAuthRequest');
  var ipaAuthService = new (require('*/cartridge/scripts/init/ipaAuthorizationService'))();
  var ipaRequest = ipaAuthRequest.getReversalRequestJSON(args, payments);
  var result = ipaAuthService.Authorize(ipaRequest);
  var response = {};
  response.error = false;
  if (enableAuthLoggers) {
    var requestJsonString = JSON.stringify(ipaRequest);
    ipaLogger.info(requestJsonString);
  }
  // Check for service availability.
  if (result.status === Result.SERVICE_UNAVAILABLE) {
    Logger.getRootLogger().fatal('[ipaUtil.js authorizationReversal] Error : SERVICE_UNAVAILABLE response');
  }
  if (enableAuthLoggers) {
    ipaLogger.info(JSON.stringify(result.object));
  }
  if (result.status != 'OK' || !result.object) {
    response.error = true;
    response.errorMessage = result.errorMessage;
    ipaLogger.info('Loyalty Authorization Reversal failure: {0}', result.errorMessage);
  } else if (result.object && (result.object.response_code == 1 || result.object.response_code == 2)) {
    // do nothing
  } else {
    response.error = true;
    response.errorMessage = result.object.response_message;
    ipaLogger.info('Loyalty Authorization Reversal failure: {0}', result.object.response_message);
  }
  return result;
}

function gcBalanceCheck(args) {
  var ipaGiftCardRequest = require('*/cartridge/models/ipaGiftCardRequest');
  var ipaAuthService = new (require('*/cartridge/scripts/init/ipaAuthorizationService'))();
  var ipaRequest = ipaGiftCardRequest.getRequestJSON(args);
  var result = ipaAuthService.giftCardBalanceCheck(ipaRequest);
  var response = {};
  response.error = false;
  if (enableAuthLoggers) {
    var requestJsonString = JSON.stringify(ipaRequest);
    ipaLogger.info(requestJsonString);
  }
  // Check for service availability.
  if (result.status === Result.SERVICE_UNAVAILABLE) {
    Logger.getRootLogger().fatal('[ipaUtil.js gcBalanceCheck] Error : SERVICE_UNAVAILABLE response');
  }
  if (enableAuthLoggers) {
    ipaLogger.info(JSON.stringify(result.object));
  }
  if (result.status != 'OK' || !result.object) {
    response.error = true;
    response.errorMessage = result.errorMessage;
  } else if (result.object && (result.object.response_code == 1 || result.object.response_code == 2)) {
    // do nothing
  } else {
    response.error = true;
    response.errorMessage = result.object.response_message;
    ipaLogger.info('Authorization failure: {0}', result.object.response_message);
  }
  return response;
}

module.exports = {
  authorizeCreditCard: authorizeCreditCard,
  authorizationReversal: authorizationReversal,
  gcBalanceCheck: gcBalanceCheck,
  paypalFraud: paypalFraud
};
