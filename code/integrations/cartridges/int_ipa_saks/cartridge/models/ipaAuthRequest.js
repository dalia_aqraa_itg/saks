'use strict';

var server = require('server');

var collections = require('*/cartridge/scripts/util/collections');

var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
var UUIDUtils = require('dw/util/UUIDUtils');
var Logger = require('dw/system/Logger');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var Transaction = require('dw/system/Transaction');
var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');
var isEmployee = 'N';

/**
 * This function identifies the countryCode to be sent to Auth.
 * CountryCode has to be GB for UK/GB.
 * @param {dw.order.OrderAddress} address
 */
function getCountryCode(address) {
  if (address && address.countryCode && address.countryCode.value) {
    if (address.countryCode.value.toLowerCase() == 'uk' || address.countryCode.value.toLowerCase() == 'gb') {
      return 'GB'; // country code for the Auth request
    } else {
      return address.countryCode.value;
    }
  }
  return 'US'; // if it reaches this point, then the address was empty or does not have a country code. Return US as default value
}

function getCustomer(profile, customerFromOrder) {
  return {
    registered_id: profile ? profile.customerNo : '',
    registration_date: profile ? StringUtils.formatCalendar(new Calendar(profile.creationDate), 'yyyyMMddHHmmss') : '',
    first_name: profile ? profile.firstName : customerFromOrder.first_name,
    last_name: profile ? profile.lastName : customerFromOrder.last_name,
    email_address: profile ? profile.email : customerFromOrder.email_address,
    employee_ind: isEmployee === 'Y' ? 'Y' : 'N'
  };
}

function getAssociateDetails(Order, session) {
  return {
    operator_id: Order.getCreatedBy(),
    operator_name: session && session.raw ? session.raw.userName : ''
  };
}

function getPaymentType(type) {
  var cardType = type;
  try {
    type = type.replace(' ', '').toLowerCase(); // eslint-disable-line
    var ccType = JSON.parse(ipaConstants.PAYMENT_CARD_TYPE);
    cardType = ccType[type];
  } catch (e) {
    cardType = type.toUpperCase();
  }
  return cardType;
}

function getCardType(type) {
  var cardType = type;
  try {
    type = type.replace(' ', '').toLowerCase(); // eslint-disable-line
    var ccType = JSON.parse(ipaConstants.CARD_TYPE_MAP);
    cardType = ccType[type];
  } catch (e) {
    cardType = type.toUpperCase();
  }
  return cardType;
}

function getPromotionDetails(Order) {
  var promotions = [];
  var lineItems = Order.getAllLineItems().iterator();

  while (lineItems.hasNext()) {
    let item = lineItems.next();
    let itemClassName = item.constructor.name;

    if (itemClassName == 'dw.order.PriceAdjustment') {
      var promo = {};

      if ('couponLineItem' in item && item.couponLineItem != null) {
        promo.code = item.couponLineItem.couponCode + ': ' + item.promotionID;
      } else {
        if ('isCSRAdjustment' in item.custom && item.custom.isCSRAdjustment) {
          promo.code = 'CSR Adj: ' + item.lineItemText;
        } else {
          promo.code = 'Auto Applied: ' + item.promotionID;
        }
      }
      promo.amount = new String(item.getPriceValue());
      promotions.push(promo);
    }
  }
  return promotions;
}

function getShipToDetails(shipment, email, line3, addressBook, item, Order) {
  var shippingAddress = shipment.shippingAddress;
  var currentDate = new Date();
  var one_day = 1000 * 60 * 60 * 24;
  var savedAddress = false;
  var addrCreationDate = new Date().getTime();
  if (addressBook !== null) {
    collections.forEach(addressBook.addresses, function (address) {
      if (isSameAddr(address, shippingAddress)) {
        savedAddress = true;
        addrCreationDate = address.creationDate.getTime();
      }
    });
  }
  return {
    first_name: shippingAddress.firstName ? shippingAddress.firstName : '',
    last_name: shippingAddress.lastName ? shippingAddress.lastName : '',
    email_address: email,
    address_line_1: shippingAddress.address1,
    address_line_2: shippingAddress.address2 ? shippingAddress.address2 : '',
    address_line_3: line3,
    city: shippingAddress.city,
    state_province: shippingAddress.stateCode,
    postal_code: shippingAddress.postalCode,
    phone_number: shippingAddress.phone ? shippingAddress.phone : '',
    shipping_method: item.custom.shippingMethod,
    country_code: getCountryCode(shippingAddress),
    shipping_age: savedAddress === true ? Math.round((currentDate.getTime() - addrCreationDate) / one_day).toString() : '0',
    last_modified_date: StringUtils.formatCalendar(new Calendar(shippingAddress.getLastModified()), 'yyyy-MM-dd'),
    seller_protection: 'sellerProtection' in shipment.custom && shipment.custom.sellerProtection ? 'Y' : 'N'
  };
}

function isSameAddr(addr1, addr2) {
  return (
    addr1.address1 === addr2.address1 &&
    addr1.address2 === addr2.address2 &&
    addr1.stateCode === addr2.stateCode &&
    addr1.city === addr2.city &&
    addr1.postalCode === addr2.postalCode
  );
}

function getBillingAddress(Order) {
  var billingAddress = Order.billingAddress;
  return {
    first_name: billingAddress.firstName ? billingAddress.firstName : '',
    last_name: billingAddress.lastName ? billingAddress.lastName : '',
    address_line_1: billingAddress.address1,
    address_line_2: billingAddress.address2 ? billingAddress.address2 : '',
    address_line_3: '',
    city: billingAddress.city,
    state_province: billingAddress.stateCode ? billingAddress.stateCode : '',
    postal_code: billingAddress.postalCode,
    country: getCountryCode(billingAddress),
    phone_number: billingAddress.phone ? billingAddress.phone : '',
    email_address: Order.getCustomerEmail() ? Order.getCustomerEmail() : ''
  };
}

/* Applicable for SAKS banner only.
 * Description:
 * Identify if the order is complete preOrder and use the result to update the Authorization amount to 0
 *
 * */
function preOrder(Order) {
  var allLineItems = Order.getProductLineItems();
  var obj = {};
  obj.hasOnePreOrderItem = false;
  obj.completePreOrder = true;
  obj.preOrderTotal = 0;
  obj.allItemsTotal = 0;
  collections.forEach(allLineItems, function (item) {
    obj.allItemsTotal += item.getAdjustedNetPrice().value;
    if ('inventoryStatus' in item.custom && item.custom.inventoryStatus == 'PREORDER') {
      obj.hasOnePreOrderItem = true;
      obj.preOrderTotal += item.getAdjustedNetPrice().value;
    } else {
      obj.completePreOrder = false;
    }
  });
  return obj;
}

function getPayments(Order, args) {
  var payments = [];
  var gcPayments = [];
  var ccPayments = [];
  var gcPI = [];
  var ccPI = [];
  var paymentInstruments = Order.getPaymentInstruments();
  var paymentForm = server.forms.getForm('billing');
  var preOrderObj = args.preOrderObj;
  Logger.debug('SFSX-3637 paymentInstruments.length {0}', paymentInstruments.length);

  collections.forEach(paymentInstruments, function (pi) {
    var merchantReferenceNumber = '';
    if('merchant_reference_number' in pi.custom && pi.custom.merchant_reference_number) {
      merchantReferenceNumber = pi.custom.merchant_reference_number;
    }
    var authAmount = pi.paymentTransaction.amount.value;
    if ('authAmountExcludingPreOrder' in pi.custom) {
      authAmount = Number(pi.custom.authAmountExcludingPreOrder);
    }
    Logger.debug('SFSX-3637 paymentMethod: {0}', pi.paymentMethod);
    if (pi.paymentMethod === ipaConstants.CREDIT_CARD_PAYMENT_METHOD) {
      ccPI.push(pi);
      var cardType = pi.creditCardType
        ? pi.creditCardType
        : paymentForm && paymentForm.creditCardFields.cardType
        ? paymentForm.creditCardFields.cardType.htmlValue
        : '';
      var cvn = '';
      if (paymentForm && paymentForm.creditCardFields.securityCode && paymentForm.creditCardFields.securityCode.htmlValue) {
        cvn = paymentForm.creditCardFields.securityCode.htmlValue;
      } else if (args.cvn) {
        cvn = args.cvn;
      }
      var tokenNumber = pi.creditCardToken;
      if (tokenNumber.length === 29) {
        tokenNumber = tokenNumber.substring(0, 16);
      }
      ccPayments.push({
        Card: {
          name_on_card: pi.creditCardHolder,
          token_number: tokenNumber,
          token_type: 'TKNX',
          type: getCardType(cardType),
          cvv: cvn,
          amount: authAmount.toFixed(2),
          currency_code: pi.paymentTransaction.amount.currencyCode,
          expiry_year: !empty(pi.getCreditCardExpirationYear()) ? pi.getCreditCardExpirationYear().toString().substring(2, 4) : '',
          expiry_month: !empty(pi.getCreditCardExpirationMonth()) ? StringUtils.formatNumber(pi.getCreditCardExpirationMonth().toString(), '00') : ''
        },
        entry_mode: 'entry_mode' in pi.custom && pi.custom.entry_mode ? pi.custom.entry_mode : ipaConstants.ENTRY_MODE,
        type: getPaymentType(cardType),
        bill_to: getBillingAddress(Order),
        authorization: {
          merchant_reference_number : merchantReferenceNumber
        }
      });
    } else if (pi.paymentMethod === ipaConstants.GIFT_CARD || pi.paymentMethod === 'GIFT_CARD') {
      gcPI.push(pi);
      gcPayments.push({
        Card: {
          number: pi.custom.giftCardNumber,
          pin: pi.custom.giftCardPin,
          type: 'giftCardType' in pi.custom ? pi.custom.giftCardType : ipaConstants.GIFTCARD_TYPE,
          entry_mode: ipaConstants.ENTRY_MODE,
          currency_code: pi.paymentTransaction.amount.currencyCode,
          amount: authAmount.toFixed(2)
        },
        entry_mode: ipaConstants.ENTRY_MODE,
        type: ipaConstants.GIFT_CARD,
        bill_to: getBillingAddress(Order),
        authorization: {
          merchant_reference_number : merchantReferenceNumber
        }
      });
    } else if (pi.paymentMethod === ipaConstants.PAYPAL) {
      ccPI.push(pi);
      ccPayments.push({
        Paypal: {
          amount: authAmount.toFixed(2),
          currency: pi.paymentTransaction.amount.currencyCode,
          address_status: 'paypalAddressStatus' in pi.custom ? pi.custom.paypalAddressStatus : '',
          status: 'paypalPaymentStatus' in pi.custom ? pi.custom.paypalPaymentStatus : 'Y',
          token: 'paypalToken' in pi.custom ? pi.custom.paypalToken : '',
          PayerID: 'paypalPayerID' in pi.custom ? pi.custom.paypalPayerID : '',
          email: 'paypalEmail' in pi.custom ? pi.custom.paypalEmail : '',
          transactionID: pi.paymentTransaction.getTransactionID(),
          MsgSubID: 'paypalMsgSubID' in pi.custom ? pi.custom.paypalMsgSubID : UUIDUtils.createUUID()
        },
        entry_mode: ipaConstants.ENTRY_MODE,
        type: 'PAYPAL',
        bill_to: getBillingAddress(Order)
      });
    }
  });

  //GC should go first in the Auth request.
  gcPayments.forEach(function (gc) {
    payments.push(gc);
  });
  ccPayments.forEach(function (cc) {
    payments.push(cc);
  });
  //set chargeSequence for CreateOrder Service
  Transaction.wrap(function () {
    var ctr = 1;
    Logger.debug('SFSX-3637 gcPI: {0}, ccPI: {1}', gcPI.length, ccPI.length);
    gcPI.forEach(function (gc) {
      gc.custom.chargeSequence = ctr.toFixed();
      ctr++;
    });
    ccPI.forEach(function (cc) {
      cc.custom.chargeSequence = ctr.toFixed();
      ctr++;
    });
  });

  return payments;
}

function getItems(Order) {
  var lineItems = [];
  var allLineItems = Order.getProductLineItems();
  collections.forEach(allLineItems, function (item) {
    if (item.custom.productPromotionAssociateDiscount != null && item.custom.productPromotionAssociateDiscount != 0) {
      isEmployee = 'Y';
    }
    var shipNode = 'shipNode' in item.product.custom ? item.product.custom.shipNode : '';
    var storeId = 'fromStoreId' in item.shipment.custom ? item.shipment.custom.fromStoreId : '';
    var addr_line3 = item.shipment.shippingMethodID === 'PICK' ? shipNode : storeId;
    var unitPrice = item.getAdjustedNetPrice().value / item.quantity.value;
    unitPrice = unitPrice.toFixed(2);
    var masterProduct = item.product && item.product.variationModel ? item.product.variationModel.master : '';
    if (item.product) {
      var lineItem = {
        product_svs: item.product && item.product.variant && item.product.masterProduct ? item.product.masterProduct.ID : item.productID,
        product_sku: item.productID,
        product_upc: item.product.UPC ? item.product.UPC : '',
        prime_line_no: item.position.toString(),
        product_class: 'productClass' in item.product.custom ? item.product.custom.productClass : '',
        product_color: 'color' in item.product.custom ? item.product.custom.color : '',
        product_size: 'size' in item.product.custom ? item.product.custom.size : '',
        type: 'productLine' in item.product.custom ? item.product.custom.productLine : '',
        product_category: masterProduct && 'departmentId' in masterProduct.custom ? masterProduct.custom.departmentId : '',
        delivery_method: item.custom.shippingMethod,
        ship_node: shipNode,
        signature_required: 'signatureRequired' in item.getShipment().custom && item.getShipment().custom.signatureRequired == true ? 'Y' : 'N',
        gift_wrap:
          item.shipment.custom.hasOwnProperty('giftWrapType') &&
          item.shipment.custom.giftWrapType === 'giftpack' &&
          item.product.custom.hasOwnProperty('giftWrapEligible') &&
          item.product.custom.giftWrapEligible === 'true'
            ? 'Y'
            : 'N', //  this would have to fetched from the checkout based on user input
        unit_price: item.product.getPriceModel().price.available
          ? item.product.getPriceModel().price.value.toFixed(2)
          : item.product.getPriceModel().minPrice.value.toFixed(2),
        unit_cost: 'costAmountDol' in item.product.custom ? item.product.custom.costAmountDol.toString() : '',
        current_status: 'inventoryStatus' in item.custom && item.custom.inventoryStatus == 'PREORDER' ? 'PO' : 'IN',
        product_name: item.productName || '',
        product_code: item.product.getBrand() ? item.product.getBrand() : '',
        list_price: item.adjustedNetPrice.value.toFixed(2),
        extended_price: item.getProratedPrice().value.toFixed(2), // Fix me
        quantity: item.quantity.value.toFixed(0),
        group_number: masterProduct && 'dmmNo' in masterProduct.custom ? masterProduct.custom.dmmNo : '',
        ship_to: getShipToDetails(item.getShipment(), Order.getCustomerEmail(), addr_line3, Order.customer.addressBook, item, Order)
      };

      if (tsysHelper.isTsysMode()) {
        lineItem.department = 'departmentId' in item.product.custom ? item.product.custom.departmentId : '';
      }

      lineItems.push(lineItem);
    }
  });
  return lineItems;
}

function highValueShipment(Order) {
  var shipments = Order.getShipments();
  var amount;
  var highShipment;
  collections.forEach(shipments, function (shipment) {
    if (!amount || shipment.getAdjustedShippingTotalPrice() > amount) {
      amount = shipment.getAdjustedShippingTotalPrice();
      highShipment = shipment;
    }
  });
  if (highShipment) {
    Transaction.wrap(function () {
      highShipment.custom.sellerProtection = true;
    });
  }
}

/**
 * @constructor
 * @classdesc CartModel class that represents the current basket
 *
 * @param {json} args - arguments
 */
function getRequestJSON(args) {
  var Order = args.Order;
  var shipment = Order.defaultShipment;
  var shipToHomeShipment = cartHelper.getShipToHomeShipment(Order);
  var shippingAddress = shipToHomeShipment ? shipToHomeShipment.getShippingAddress() : shipment.getShippingAddress();
  var isShipToHomeAvailable = shipToHomeShipment ? true : !('shipmentType' in Order.defaultShipment.custom);
  var customer = args.customer;
  var preOrderObj = preOrder(Order);
  args.preOrderObj = preOrderObj;
  highValueShipment(Order);
  this.banner = ipaConstants.BANNER;
  this.channel = session.isUserAuthenticated() ? 'Call Center' : ipaConstants.CHANNEL;
  this.sub_channel = ipaConstants.SUB_CHANNEL;
  this.store_number = ipaConstants.STORE_NUMBER;
  this.transaction_mode = ipaConstants.transactionMode[args.authType];
  this.transaction_type = ipaConstants.transactionType[args.authType];
  this.country_code = getCountryCode(Order.billingAddress);
  this.language_code = Order.customerLocaleID === 'fr_CA' ? 'FR' : 'EN';
  this.ip_address = args.remoteAddress;
  this.device_fingerprint = args.deviceFingerPrintID ? args.deviceFingerPrintID : '';
  this.fail_attempts = args.failAttempts.toString();
  this.loyalty = {
    number: customer && customer.profile && 'hudsonRewards' in customer.profile.custom ? customer.profile.custom.hudsonRewards : '' // Fix me
  };
  var customerOnOrder = {
    first_name: Order.billingAddress.firstName ? Order.billingAddress.firstName : '',
    last_name: Order.billingAddress.lastName ? Order.billingAddress.lastName : '',
    email_address: Order.getCustomerEmail() ? Order.getCustomerEmail() : ''
  };

  // update the payment instrument with preorder excluded amount
  COHelpers.getAuthAmountExcludingPreOrderTotal(Order);

  this.order = {
    type: args.type, // Always regular
    number: Order.orderNo,
    suffix: preOrderObj.hasOnePreOrderItem ? '000' : Order.orderNo.substring(0, 3),
    IsIntialFraudCheck: 'Y', // Always true
    create_date: StringUtils.formatCalendar(new Calendar(Order.getCreationDate()), 'yyyyMMddHHmmss'),
    total_amount: Order.getTotalGrossPrice().value.toFixed(2),
    total_shipping_amount: Order.getAdjustedShippingTotalNetPrice().value.toFixed(2),
    total_tax_amount: Order.getTotalTax().value.toFixed(2),
    modified_date: StringUtils.formatCalendar(new Calendar(Order.getLastModified()), 'yyyyMMddHHmmss'),
    ship_to: {
      first_name: isShipToHomeAvailable ? shippingAddress.firstName : customerOnOrder.first_name,
      last_name: isShipToHomeAvailable ? shippingAddress.lastName : customerOnOrder.last_name,
      email_address: Order.getCustomerEmail() ? Order.getCustomerEmail() : ''
    },
    items: getItems(Order),
    promotions: getPromotionDetails(Order) // order level promotions
  };

  this.payments = getPayments(Order, args);
  this.customer = getCustomer(customer.profile, customerOnOrder);
  this.associate = getAssociateDetails(Order, args.session);

  if (tsysHelper.isTsysMode()) {
    this.login_status = customer && customer.profile && customer.profile.customerNo ? 'Y' : 'N';
  }
  return this;
}

function getReversalRequestJSON(args, payments) {
  var Order = args.Order;

  this.order = {
    number: Order.orderNo,
    total_amount: Order.getTotalGrossPrice().value.toFixed(2),
    total_shipping_amount: Order.getAdjustedShippingTotalNetPrice().value.toFixed(2),
    total_tax_amount: Order.getTotalTax().value.toFixed(2),
    items: getItems(Order)
  };

  return this;
}

module.exports = {
  getRequestJSON: getRequestJSON,
  getReversalRequestJSON: getReversalRequestJSON,
  getCardType: getCardType,
  getCountryCode: getCountryCode,
  getPaymentType: getPaymentType,
  getBillingAddress: getBillingAddress
};
