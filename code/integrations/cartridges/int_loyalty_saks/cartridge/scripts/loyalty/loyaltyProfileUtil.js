'use strict';
var LoyaltyProfileService = new (require('*/cartridge/scripts/loyalty/loyaltyProfileService'))();
var preferences = require('*/cartridge/config/preferences');

/**
 * function to build the request json
 *
 * @param {dw.customer.Profile} profile - customer profile
 * @param {string} banner - current banner string
 * @returns {Object} requestObject - request object
 */
function buildRequestJSON(profile, banner) {
  var rewardsRequest = {
    LoyaltyProfile: {
      bannerCode: banner,
      CustomerInfo: {
        firstName: profile.firstName,
        lastName: profile.lastName,
        Contact: {
          Phone: {
            phoneNumber: profile.phoneHome ? profile.phoneHome : ''
          },
          Email: {
            emailAddress: profile.email
          }
        }
      }
    }
  };
  if ('moreCustomer' in profile.custom && !!profile.custom.moreCustomer) {
    rewardsRequest.LoyaltyProfile.CustomerInfo.moreNumber = profile.custom.moreCustomer;
  } else {
    rewardsRequest.LoyaltyProfile.CustomerInfo.Contact.Address = {
      zipCode: 'zipCode' in profile.custom ? profile.custom.zipCode : ''
    };
  }
  return rewardsRequest;
}

/**
 * Helper method for calling loyalty profile service
 * @param {dw.customer.Profile} profile - customer profile
 * @param {boolean} create - boolean to identify if its a create/update call
 * @returns {Object} - result of the service
 */
function callLoyaltyProfile(profile, create) {
  var reqPayLoad = {};
  reqPayLoad.apiKey = preferences.hbcAPIKey;
  reqPayLoad.apigwKey = preferences.hbcAPIID;
  var banner = preferences.hbcBanner;
  var requestJSON = buildRequestJSON(profile, banner);
  reqPayLoad.requestJSON = requestJSON;
  if (!create) {
    reqPayLoad.serviceName = 'loyalty.https.profile.update';
  } else {
    reqPayLoad.serviceName = 'loyalty.https.profile.create';
  }
  var result = LoyaltyProfileService.callLoyaltyProfile(reqPayLoad);
  return result;
}
exports.callLoyaltyProfile = callLoyaltyProfile;
