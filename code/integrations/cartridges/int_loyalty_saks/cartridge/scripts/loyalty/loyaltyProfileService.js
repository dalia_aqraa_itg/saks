'use strict';

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var Logger = require('dw/system/Logger');
var ServiceHelper = require('*/cartridge/scripts/services/helpers/ServiceHelper');

var LoyaltySaksService = function () {
  /**
   ** Helper method for calling loyalty profile service
   *
   * @param {Object} data The JSON data to send in the body of the request
   * @returns {Object} response
   */
  this.callLoyaltyProfile = function (data) {
    var callLoyaltyProfileService = LocalServiceRegistry.createService(data.serviceName, {
      // eslint-disable-next-line no-unused-vars
      createRequest: function (service, params) {
        service.setRequestMethod('POST');
        // eslint-disable-next-line no-param-reassign
        service = ServiceHelper.addServiceHeaders(service);
        return JSON.stringify(data.requestJSON);
      },
      parseResponse: function (service, httpClient) {
        return JSON.parse(httpClient.text);
      },
      filterLogMessage: function (res) {
        if (res.indexOf('ResponseCode') > -1) {
          let message = JSON.parse(res);
          if (message.LoyaltyProfile.CustomerInfo.moreNumber) {
            message.LoyaltyProfile.CustomerInfo.moreNumber = '**********';
          }
          if (message.LoyaltyProfile.CustomerInfo.Contact && message.LoyaltyProfile.CustomerInfo.Contact.Email) {
            message.LoyaltyProfile.CustomerInfo.Contact.Email.emailAddress = '***********';
          }
          if (message.LoyaltyProfile.CustomerInfo.Contact && message.LoyaltyProfile.CustomerInfo.Contact.Address) {
            message.LoyaltyProfile.CustomerInfo.Contact.Address.zipCode = '********';
          }
          if (message.LoyaltyProfile.CustomerInfo.Contact && message.LoyaltyProfile.CustomerInfo.Contact.Phone) {
            message.LoyaltyProfile.CustomerInfo.Contact.Phone.phoneNumber = '***********';
          }
          return JSON.stringify(message);
        } else if (res.indexOf('LoyaltyProfile') > -1) {
          let message = JSON.parse(res);
          message.LoyaltyProfile.CustomerInfo.Contact.Phone.phoneNumber = '***********';
          message.LoyaltyProfile.CustomerInfo.Contact.Email.emailAddress = '***********';
          if (message.LoyaltyProfile.CustomerInfo.moreNumber) {
            message.LoyaltyProfile.CustomerInfo.moreNumber = '**********';
          }
          if (message.LoyaltyProfile.CustomerInfo.Contact.Address) {
            message.LoyaltyProfile.CustomerInfo.Contact.Address.zipCode = '********';
          }
          return JSON.stringify(message);
        }
        return res;
      },
      mockCall: function (service, params) {
        let mockRes;
        if (service.getConfiguration().getID().indexOf('create') > -1) {
          mockRes =
            '{"LoyaltyProfile": {"bannerCode": "OFF5","CustomerInfo": {"moreNumber": "1234342343"}},"ResponseCode": "0","ResposneMessage": "Successful"} ';
        } else {
          mockRes =
            '{"LoyaltyProfile": {"bannerCode": "OFF5","CustomerInfo": {"firstName": "Mock","lastName": "Call","Email": {"emailAddress": "mock@call.com"},"Phone": {"phoneNumber": "4232331234"}}},"ResponseCode": "0","ResposneMessage": "Successful"}';
        }
        return {
          statusCode: 200,
          statusMessage: 'Success',
          text: mockRes
        };
      }
    });

    var result = callLoyaltyProfileService.call(JSON.stringify(data));
    if (!result.isOk() || !result.object) {
      var Result = require('dw/svc/Result');
      if (result.status === Result.SERVICE_UNAVAILABLE) {
        Logger.getRootLogger().fatal('[Loyalty Profile Info failure (loyaltyProfileService.js)] {0}', result.errorMessage);
      }
      return {
        error: true,
        errorMessage: result.errorMessage
      };
    }
    return result.object;
  };
};
module.exports = LoyaltySaksService;
