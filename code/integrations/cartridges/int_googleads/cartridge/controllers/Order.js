'use strict';
var page = module.superModule;
var server = require('server');
server.extend(page);
var Logger = require('dw/system/Logger');

server.append('Confirm', function (req, res, next) {
  // With host only redirects, controller-action is not coming thru
  // set the action explicitly, w/o proper action downstream requests such as
  // adobe tag manager etc. which depend on proper action will not work
  try {
    var Site = require('dw/system/Site');
    if ('googleAdsEnabled' in Site.current.preferences.custom && Site.current.preferences.custom.googleAdsEnabled) {
      var viewData = res.getViewData();
      var CustomObjectMgr = require('dw/object/CustomObjectMgr');
      var customObjectsItr = CustomObjectMgr.queryCustomObjects('GoogleAds', 'custom.controllerPath = {0}', null, viewData.action);
      if (customObjectsItr.count > 0) {
        var i = 0;
        var arrObjects = [];
        while (customObjectsItr.hasNext()) {
          var co = customObjectsItr.next();

          arrObjects[i] = {
            adId: co.custom.adId,
            adUnitPath: co.custom.adUnitPath,
            action: co.custom.action,
            adSize: co.custom.adSize,
            show: true
          };

          i++;
        }
        viewData.googleAds = arrObjects;
        res.setViewData(viewData);
      }
    }
  } catch (e) {
    Logger.error('Error with googleads controller' + e.message);
  }
  next();
});

module.exports = server.exports();
