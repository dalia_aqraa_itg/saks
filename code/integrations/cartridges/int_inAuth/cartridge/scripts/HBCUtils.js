'use strict';

var Site = require('dw/system/Site');

/**
 *@Description - Generating Secure Random number
 *@return {Object} UUID- returning Unique ID
 */

function generateUUID() {
  var UUIDUtils = require('dw/util/UUIDUtils');
  var UUID = UUIDUtils.createUUID();
  return UUID;
}

/**
 *Setting the Transaction UUID in session when we click on PlaceOrder in Checkout to be used in IPA request call
 *@params {Object} req - Setting the Transaction UUID
 */

function setTransactionUUID(req, transaction_UUID) {
  if (inAuthEbabled()) {
    req.session.privacyCache.set('transactionUUID', transaction_UUID);
  }
}

/**
 *Get all Inauth Details
 *@params {Object} req - getting the Transaction UUID
 *@return {Object} inAuthObject - details of Inauth Object stored in that
 */

function getInAuthDetails(req) {
  var inAuthObject;
  // Fetch All details like URL, SID, CF
  var inAuthURL = 'inAuthURL' in Site.current.preferences.custom ? Site.current.preferences.custom.inAuthURL : '';
  var inAuthSID = 'inAuthSID' in Site.current.preferences.custom ? Site.current.preferences.custom.inAuthSID : '';
  var inAuthCF = 'inAuthCF' in Site.current.preferences.custom ? Site.current.preferences.custom.inAuthCF : '';
  var inAuthUUID = generateUUID();
  // Check if inAuth is enabled
  if (inAuthEbabled()) {
    // Create a object, which store all the details
    inAuthObject = {
      Url: inAuthURL,
      Sid: inAuthSID,
      CollectionFlag: inAuthCF,
      TransactionUUID: inAuthUUID
    };
  }
  return inAuthObject;
}

/**
 *Checking for Inauth Enable
 */

function inAuthEbabled() {
  //return if inAuth is enabled or disbales
  var isInauthEnabled = 'isInauthEnabled' in Site.current.preferences.custom && Site.current.preferences.custom.isInauthEnabled ? true : false;
  return isInauthEnabled;
}

module.exports = {
  setTransactionUUID: setTransactionUUID,
  getInAuthDetails: getInAuthDetails
};
