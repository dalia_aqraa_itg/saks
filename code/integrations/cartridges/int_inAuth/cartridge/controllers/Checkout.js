'use strict';

var server = require('server');
var base = module.superModule;
server.extend(base);

server.append('Begin', function (req, res, next) {
  return next();
});

module.exports = server.exports();
