'use strict';
var File = require('dw/io/File');
var Status = require('dw/system/Status');
/**
 * Triggers the pipeline execution
 *
 * @param {dw.util.HashMap} args
 * @param {dw.job.JobStepExecution} jobStepExecution
 * @returns {dw.system.Status}
 */
function execute(args, jobStepExecution) {
  var ImpexLocation = args.IMPEX_Location;
  var filePattern = args.filePattern;
  var removeIffenOnStart = args.removeIffenOnStart;
  var prefixOnIffen = args.prefixOnIffen;
  var archive = args.archive;
  if (isValidInputs(ImpexLocation, filePattern)) {
    var allFiles = downloadFiles(ImpexLocation, filePattern);
    if (allFiles) {
      zipAndrenameFiles(allFiles, removeIffenOnStart, prefixOnIffen, archive);
    }
    return new Status(Status.OK);
  }
  return new Status(Status.ERROR, 'Invalid inputs');
}

function downloadFiles(ImpexLocation, filePattern) {
  var ArrayList = require('dw/util/ArrayList');
  var regExp = new RegExp(filePattern);
  var fileDir = new File(File.IMPEX + File.SEPARATOR + ImpexLocation);
  if (fileDir.exists) {
    var fileList = new ArrayList();
    fileList.addAll(
      fileDir.listFiles(function (file) {
        if (filePattern) {
          return regExp.test(file.name);
        }
        return true;
      })
    );
    return fileList;
  }
  return null;
}

function zipAndrenameFiles(allFiles, removeIffenOnStart, prefixOnIffen, archive) {
  if (allFiles && allFiles.size() > 0) {
    var iter = allFiles.iterator();
    while (iter.hasNext()) {
      var aFile = iter.next();
      var fileName = aFile.fullPath;
      if (removeIffenOnStart && removeIffenOnStart === 'Yes') {
        fileName = getNonIffenFileName(fileName, prefixOnIffen);
        if (!fileName) {
          return false;
        }
      }
      var rootDirectory = new File(fileName + '.gz');
      aFile.gzip(rootDirectory);

      var backupFile = new File(File.IMPEX + File.SEPARATOR + archive + File.SEPARATOR + aFile.getName());
      if (!backupFile.exists) {
        backupFile.mkdir();
      }
      aFile.renameTo(backupFile);
    }
    return true;
  }
}

function isValidInputs(ImpexLocation, filePattern) {
  return ImpexLocation && filePattern;
}

function getNonIffenFileName(path, prefixOnIffen) {
  if (path && prefixOnIffen && path.indexOf(prefixOnIffen) !== -1) {
    prefixOnIffen = prefixOnIffen + '-';
    var fileName = path.split(prefixOnIffen);
    return fileName[0].concat(prefixOnIffen.slice(0, -1)).concat(fileName[1]);
  }
  return path;
}

exports.execute = execute;
