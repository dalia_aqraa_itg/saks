'use strict';
/**
 *
 * Remove the files from target folder under /Impex/src/ which are certain number of days old
 * @param {Object} args : Job Object
 * @returns {dw.system.Status} Status : dw.system.Status
 */

var Status = require('dw/system/Status');
var File = require('dw/io/File');
var Calendar = require('dw/util/Calendar');

const SUB_FOLDER: '/src/';
const DAYS_OLD: 30;

function run(args) {
  var targetFolder = args.targetFolder || '';
  var daysToBeKept = args.daysToBeKept || DAYS_OLD;
  var targetDir = new File(File.IMPEX + SUB_FOLDER + targetFolder);

  if (!targetDir.exists()) {
    throw new Error('Folder ' + targetDir.fullPath + ' does not exist.');
  }

  var oldCalendar = new Calendar();
  oldCalendar.add(Calendar.DAY_OF_YEAR, -1 * daysToBeKept);

  var listFiles = targetDir.listFiles();
  var collections = require('*/cartridge/scripts/util/collections');

  collections.forEach(listFiles, function (targetFile) {
    var fileCalendar = new Calendar(new Date(targetFile.lastModified()));
    if (fileCalendar.before(oldCalendar)) {
      targetFile.remove();
    }
  });

  return new Status(Status.OK);
}

exports.Run = run;
