'use strict';
/**
 *  This is a one time Job step. It is designed to set the hbcProductType product custom attribute to standard.
 * productType
 * Used to indicate which template should be used for the PDP
 * Proper Values: home, bridal, gwp, giftcard, chanel, null (standard)"
 * It is to be updated only at the master product level or at the product type level ('standard'). Variant and other product types to be ignored
 */

var Status = require('dw/system/Status');
var Resource = require('dw/web/Resource');
var ProductMgr = require('dw/catalog/ProductMgr');
var Logger = require('dw/system/Logger');
var Transaction = require('dw/system/Transaction');

function run() {
  try {
    let products;
    products = ProductMgr.queryAllSiteProducts();
    while (products.hasNext()) {
      try {
        var product = products.next();
        // Check if the product is master or standard product. Variant is not to be updated
        if (product.master || product.isProduct()) {
          Transaction.wrap(function () {
            if (
              !('hbcProductType' in product.custom) ||
              ('hbcProductType' in product.custom && !product.custom.hbcProductType) ||
              ('hbcProductType' in product.custom && product.custom.hbcProductType === '')
            ) {
              product.custom.hbcProductType = 'standard';
            }
          });
        }
      } catch (e) {
        var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
        Logger.debug(errorMsg);
      }
    }
    return new Status(Status.OK, 'OK', 'successful');
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    Logger.debug(errorMsg);
    return new Status(Status.ERROR, 'ERROR', 'Error occured while executing job.');
  }
}

exports.Run = run;
