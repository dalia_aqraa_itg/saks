'use strict';

var Status = require('dw/system/Status');
var ProductMgr = require('dw/catalog/ProductMgr');
var Logger = require('dw/system/Logger');
var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var XMLStreamWriter = require('dw/io/XMLStreamWriter');
var PREORDER_F = 'F';
var PREORDER_T = 'T';
var StringUtils = require('dw/util/StringUtils');

function run(args) {
  if (empty(args.JobThreadIndex) || empty(args.ChunkSize)) {
    return new Status(Status.ERROR, 'ERROR', 'JobThreadIndex/ChunkSize/CatalogID param empty.');
  }
  var chunkSize = args.ChunkSize || 100000;
  var startIndex = args.JobThreadIndex * chunkSize;
  var jobIndex = args.JobThreadIndex;
  var catalogID = args.CatalogID || 'master-s5a';
  var enableLogging = args.EnableLogging || false;
  var forceUpdate = args.ForceUpdate || false;

  var productIterator: SeekableIterator = ProductMgr.queryAllSiteProducts();
  var productCount = productIterator.getCount();

  productIterator.forward(startIndex, chunkSize);

  var it = productIterator,
    product,
    counter = startIndex;
  var countPreorderMastersTrue = 0;
  var countPreorderMastersFalse = 0;
  var countPreorderVariationsTrue = 0;
  var countPreorderVariationsFalse = 0;
  var countNoInventory = 0;
  var countBackorderable = 0;
  var countNoInStockDate = 0;
  var searchFalse = 0;
  var searchTrue = 0;
  var noPublishSearchFalse = 0;
  var Calendar = require('dw/util/Calendar');
  var currentTime = new Calendar();
  var todayMinus180 = new Calendar();
  todayMinus180.add(Calendar.DAY_OF_YEAR, -180);

  var rootDirectory: String = File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'tmp';

  var xmlfileName: String = 'catalog_master_set_product_flags_' + jobIndex + '.xml';
  var xmlfile: File = new File(rootDirectory + File.SEPARATOR + xmlfileName);

  var xmlfileWriter: FileWriter;
  var xmlWriter: XMLStreamWriter;

  try {
    var dir: File = new File(rootDirectory);
    var dirExists = dir.exists();

    if (dirExists) {
      if (dir.isDirectory()) {
      }
    } else {
      dirExists = dir.mkdirs();
    }

    xmlfile.createNewFile();
    xmlfileWriter = new FileWriter(xmlfile);
    xmlWriter = new XMLStreamWriter(xmlfileWriter);

    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement('catalog');
    xmlWriter.writeAttribute('xmlns', 'http://www.demandware.com/xml/impex/catalog/2006-10-31');
    xmlWriter.writeAttribute('catalog-id', catalogID);

    while (it.hasNext()) {
      counter++;
      product = it.next();

      if (product.master) {
        var mSearchableFlag = false;
        var mSfccPreorder = null;
        if ('sfccPreorder' in product.custom) {
          mSfccPreorder = product.custom.sfccPreorder;
        }
        if (enableLogging) {
          Logger.info('master id: ' + product.ID + ' preOrder: ' + product.custom.preOrder);
        }

        var variants = product.variants ? product.variants : [];
        var variantsModel = product.variationModel ? product.variationModel.variants : [];

        if (enableLogging) {
          Logger.info(' variants: ' + variants.length + ' variantsModel: ' + variantsModel.length);
        }

        if (variants.length > 0) {
          var allVariantPreOrderable = true;
          var countVariantNoInventory = 0;
          var countSoldOut = 0;

          for (var i = 0; i < variants.length; i++) {
            var variant = variants[i];
            var inventoryRecord = variant.availabilityModel.inventoryRecord;
            var logMsg = '';

            var vSfccPreorder = null;
            if (variant && 'sfccPreorderVariant' in variant.custom) {
              vSfccPreorder = variant.custom.sfccPreorderVariant;
            }

            var vSearchableIfFlag = null;
            if (variant && 'waitlist' in variant.custom && variant.custom.waitlist == 'true') {
              if (variant.custom.waitlist) {
                vSearchableIfFlag = true;
              } else {
                vSearchableIfFlag = false;
              }
            }

            if (variant && 'preOrder' in variant.custom && variant.custom.preOrder === PREORDER_T) {
              /**
               * Preorder Flagging Rules
               * 1. Master has PIM Driven Preorder set to T
               * 2. Variant is backorderable with future in-stock date, the variant SFCC Preorder is set to T
               * 3. If all elliglble variants are preorderable, then master has SFCC Preorder set to T
               *
               * Variant inventory of on-hand allocation > 0 or backorderpreorderallocation = 0
               * on the variant
               *
               * If one variant has all preorder stock sold:
               * - master should still have SFCC Preorder set to T, so that customers see this is a preorder product
               * - variant should still have SFCC Preorder set to T, so that any new incoming inventory will have Preorder PDP
               *
               */
              if (inventoryRecord) {
                if (inventoryRecord.backorderable) {
                  countBackorderable++;
                  if (inventoryRecord.inStockDate) {
                    var inStockTime = new Calendar(inventoryRecord.inStockDate);
                    if (inStockTime.after(currentTime)) {
                      logMsg =
                        'backorderable: ' +
                        inventoryRecord.backorderable +
                        ' bo allocation: ' +
                        inventoryRecord.preorderBackorderAllocation.value +
                        ' oh allocation: ' +
                        inventoryRecord.stockLevel.value +
                        ' future inStockDate: ' +
                        inventoryRecord.inStockDate;
                      if (inventoryRecord.stockLevel.value > 0) {
                        if (forceUpdate || vSfccPreorder != PREORDER_F) {
                          writeProduct(xmlWriter, variant.ID, PREORDER_F, PREORDER_F);
                        }
                        allVariantPreOrderable = false;
                      } else {
                        if (forceUpdate || vSfccPreorder != PREORDER_T) {
                          writeProduct(xmlWriter, variant.ID, PREORDER_T, PREORDER_T);
                        }
                      }
                      countPreorderVariationsTrue++;
                    } else {
                      logMsg =
                        'backorderable: ' +
                        inventoryRecord.backorderable +
                        ' bo allocation: ' +
                        inventoryRecord.preorderBackorderAllocation.value +
                        ' oh allocation: ' +
                        inventoryRecord.stockLevel.value +
                        ' past inStockDate: ' +
                        inventoryRecord.inStockDate;
                      if (forceUpdate || vSfccPreorder != PREORDER_F) {
                        writeProduct(xmlWriter, variant.ID, PREORDER_F, PREORDER_F);
                      }
                      allVariantPreOrderable = false;
                      countPreorderVariationsFalse++;
                    }
                  } else {
                    logMsg = 'has no inStockDate set';
                    if (forceUpdate || vSfccPreorder != PREORDER_F) {
                      writeProduct(xmlWriter, variant.ID, PREORDER_F, PREORDER_F);
                    }
                    allVariantPreOrderable = false;
                    countPreorderVariationsFalse++;
                    countNoInStockDate++;
                  }
                } else {
                  if (inventoryRecord.stockLevel.value > 0) {
                    /**
                     * Variant has inventory and is not backorderable, so we are going to assume
                     * it is now for regular sale
                     */
                    logMsg = 'is not set as a backorder, and has on-hand: ' + inventoryRecord.stockLevel.value;
                    if (forceUpdate || vSfccPreorder != PREORDER_F) {
                      writeProduct(xmlWriter, variant.ID, PREORDER_F, PREORDER_F);
                    }
                    allVariantPreOrderable = false;
                    countPreorderVariationsFalse++;
                  } else {
                    /**
                     * Variant has no inventory and is not backorderable, so we are going to assume
                     * it is a sold out backorder item and should not count against master's preorder display
                     */
                    logMsg = 'is not set as a backorder, and has no on-hand inventory';
                    if (forceUpdate || vSfccPreorder != PREORDER_T) {
                      writeProduct(xmlWriter, variant.ID, PREORDER_T, PREORDER_T);
                    }
                    countPreorderVariationsTrue++;
                  }
                }
              } else {
                logMsg = 'has no inventory record';
                /**
                 * If the variant has no inventory record, HBC buyers never ordered this variant
                 * - product has no backorder record, so SFCC Preorder is F
                 */
                if (forceUpdate || vSfccPreorder != PREORDER_F) {
                  writeProduct(xmlWriter, variant.ID, PREORDER_F, PREORDER_F);
                }
                /**
                 * Variations missing inventory records should not be included in evaluating
                 * if this master should be set to SFCC Preorder T.
                 * - allVariantPreOrderable = false not needed for this scenario
                 * - countVariantNoInventory instead tracks number of variants that have no inventory
                 */
                countVariantNoInventory++; // used later to determine if master should not be SFCC Preorder T

                countPreorderVariationsFalse++;
                countNoInventory++;
              }
              if (enableLogging) {
                Logger.info('variant id: ' + variant.ID + ' ' + logMsg);
              }
            } else {
              /**
               * Master is PIM Preorder F
               */
              allVariantPreOrderable = false;
              countPreorderVariationsFalse++;

              if (inventoryRecord) {
                if (inventoryRecord.backorderable) {
                  /**
                   *  Preorder F variant is backordered but has no on hand, count it as sold out
                   */
                  if (inventoryRecord.stockLevel.value <= 0) {
                    countSoldOut++;
                  }

                  if (inventoryRecord.inStockDate) {
                    var inStockTime = new Calendar(inventoryRecord.inStockDate);
                    if (inStockTime.after(currentTime)) {
                      logMsg =
                        'backorderable: ' +
                        inventoryRecord.backorderable +
                        ' allocation: ' +
                        inventoryRecord.preorderBackorderAllocation.value +
                        ' future inStockDate: ' +
                        inventoryRecord.inStockDate;
                    } else {
                      logMsg =
                        'backorderable: ' +
                        inventoryRecord.backorderable +
                        ' allocation: ' +
                        inventoryRecord.preorderBackorderAllocation.value +
                        ' past inStockDate: ' +
                        inventoryRecord.inStockDate;
                    }
                  } else {
                    logMsg = 'has no inStockDate set';
                  }
                } else {
                  logMsg = 'is a regular for sale product with on hand allocation: ' + inventoryRecord.stockLevel.value;
                  if (inventoryRecord.stockLevel.value <= 0) {
                    countSoldOut++;
                  }
                }
              } else {
                /**
                 *  Preorder F variant has no inventory record, count it as sold out
                 */
                logMsg = 'has no inventory record record';
                countSoldOut++;
              }

              if (enableLogging) {
                Logger.info('variant id: ' + variant.ID + ' ' + logMsg);
              }

              if (forceUpdate || vSfccPreorder != PREORDER_F) {
                writeProduct(xmlWriter, variant.ID, PREORDER_F, PREORDER_F);
              }
            }
          }

          /**
           *  MASTER SEARCHABILITY
           */

          if ('publishedDate' in product.custom && !empty(product.custom['publishedDate'])) {
            var hbcProductType = product.custom['hbcProductType'] || 'standard';
            var publishedDate = new Calendar(product.custom['publishedDate']);
            /**
             * GIVEN a master product has publishedDate older than 180 days
             * AND master has inventory of 0 OR preorder (backorder) inventory of 0
             * AND hbcProductType is not "CSRonly" OR "CSRstores"
             * THEN master is marked as Searchable = False
             *
             * SAKS logic:
             * https://hbcdigital.atlassian.net/wiki/spaces/Sfsx/pages/1223163970/Indexing+Criteria+SAKS
             */
            if (publishedDate.before(todayMinus180) && !product.availabilityModel.orderable && hbcProductType != 'CSRonly' && hbcProductType != 'CSRstores') {
              if (enableLogging) {
                Logger.info('master id ' + product.ID + '  does NOT meet the indexing criteria and should be set searchable false');
              }
              mSearchableFlag = false;
              searchFalse++;
            } else {
              /**
               * GIVEN master product has publishedDate up to 180 days ago
               * OR Product has inventory greater than 0 OR preorder (backorder) inventory greater than 0
               * OR hbcProductType = "CSRonly" or "CSRstores"
               * THEN Product is marked as Searchable = True
               * AND Indexed in order to appear in arrays and search
               */
              mSearchableFlag = true;
              searchTrue++;
              if (enableLogging) {
                Logger.info('master id ' + product.ID + ' meets the indexing criteria and should be set searchable true');
              }
            }
          } else {
            /**
             * No published date, the master should be searchable false
             */
            mSearchableFlag = false;
            noPublishSearchFalse++;
            if (enableLogging) {
              Logger.info('master id ' + product.ID + ' has no published date and should not be searchable, mSearchableFlag: ' + mSearchableFlag);
            }
          }

          /**
           * If all variants of a preorder F master have backorder records, but
           * no on-hand inventory (or are missing inventory record), master shouldn't appear online.
           */
          if (variants.length == countSoldOut && hbcProductType != 'CSRonly' && hbcProductType != 'CSRstores') {
            mSearchableFlag = false;
            if (enableLogging) {
              Logger.info(
                'All Preorder F variants are backorderable with no on-hand inventory, or are missing inventory records, or are actually sold out. Setting master id ' +
                  product.ID +
                  ' to mSearchableFlag: ' +
                  mSearchableFlag
              );
            }
          }

          /**
           * If all variants of a master have no inventory record
           * then the master should NOT be set to SFCC Preorder T
           */
          if (variants.length == countVariantNoInventory) {
            allVariantPreOrderable = false;
            if (enableLogging) {
              Logger.info('All variants are missing inventory records. Setting allVariantPreOrderable to: ' + allVariantPreOrderable);
            }
          }

          /**
           *  WRITING MASTER RECORDS
           */

          if (allVariantPreOrderable) {
            /**
             * If allVariantPreOrderable is true,
             * then master SFCC Preorder flag is set to T for true
             */
            if (enableLogging) {
              Logger.info(
                'All variants preorderable. Setting master id ' + product.ID + ' sfccPreorder to: ' + PREORDER_T + ' and searchable: ' + mSearchableFlag
              );
            }
            if (forceUpdate || mSfccPreorder != PREORDER_T || product.searchable != mSearchableFlag) {
              writeProduct(xmlWriter, product.ID, PREORDER_T, null, mSearchableFlag);
            }
            countPreorderMastersTrue++;
          } else {
            /**
             * If one or more variants are NOT preorderable,
             * then master SFCC Preorder flag must be F for false
             */
            if (enableLogging) {
              Logger.info(
                'One or more variant, not preorderable. Setting master id ' +
                  product.ID +
                  ' sfccPreorder to: ' +
                  PREORDER_F +
                  ' and searchable: ' +
                  mSearchableFlag
              );
            }
            if (forceUpdate || mSfccPreorder != PREORDER_F || product.searchable != mSearchableFlag) {
              writeProduct(xmlWriter, product.ID, PREORDER_F, null, mSearchableFlag);
            }
            countPreorderMastersFalse++;
          }
        } else {
          /**
           *  No variants, then master SFCC Preorder flag must be F for false
           */
          if (enableLogging) {
            Logger.info('Master has no variants. Setting master id ' + product.ID + ' sfccPreorder to: ' + PREORDER_F + ' and searchable: ' + mSearchableFlag);
          }
          if (forceUpdate || mSfccPreorder != PREORDER_F || product.searchable != mSearchableFlag) {
            writeProduct(xmlWriter, product.ID, PREORDER_F, null, mSearchableFlag);
          }
          countPreorderMastersFalse++;
        }
      }
    }
    xmlWriter.writeEndElement(); // catalog end
    xmlWriter.writeEndDocument();
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    Logger.debug(errorMsg);
    Logger.info(errorMsg);
    if (productIterator) productIterator.close();
    return new Status(Status.ERROR, 'ERROR', 'Error occured while executing job.');
  } finally {
    if (xmlWriter) xmlWriter.close();
    if (xmlfileWriter) xmlfileWriter.close();

    if (productIterator) productIterator.close();
  }

  Logger.info(' ---------------------- JOB STEP TOTALS ----------------------');
  Logger.info(' start: ' + startIndex + ' end: ' + counter + ' of productCount: ' + productCount);
  Logger.info(' ----------------------     MASTERS     ----------------------');
  Logger.info(' searchable: ' + searchTrue);
  Logger.info(' not searchable: ' + searchFalse);
  Logger.info(' with no publish date: ' + noPublishSearchFalse);
  Logger.info(' preorderable: ' + countPreorderMastersTrue);
  Logger.info(' not preorderable: ' + countPreorderMastersFalse);
  Logger.info(' ----------------------    VARIATIONS   ----------------------');
  Logger.info(' preorderable: ' + countPreorderVariationsTrue);
  Logger.info(' not preorderable: ' + countPreorderVariationsFalse);
  Logger.info(' with no inventory: ' + countNoInventory);
  Logger.info(' backorderable: ' + countBackorderable);
  Logger.info(' no in-stock date: ' + countNoInStockDate);
  Logger.info(' --------------------------------------------------------------');

  return new Status(Status.OK, 'OK', 'successful.');
}

function writeProduct(xmlWriter, ID, sfccPreorder, sfccPreorderVariant, searchable, searchableIf, online) {
  xmlWriter.writeStartElement('product');
  xmlWriter.writeAttribute('product-id', ID);
  if (online === false || online === true) {
    xmlWriter.writeStartElement('online-flag');
    xmlWriter.writeCharacters(online);
    xmlWriter.writeEndElement(); // online-flag end
  }
  if (searchable === false || searchable === true) {
    xmlWriter.writeStartElement('searchable-flag');
    xmlWriter.writeCharacters(searchable);
    xmlWriter.writeEndElement(); // searchable-flag end
  }
  if (searchableIf === false || searchableIf === true) {
    xmlWriter.writeStartElement('searchable-if-unavailable-flag');
    xmlWriter.writeCharacters(searchableIf);
    xmlWriter.writeEndElement(); // searchable-if-unavailable-flag end
  }
  xmlWriter.writeStartElement('custom-attributes');
  xmlWriter.writeStartElement('custom-attribute');
  xmlWriter.writeAttribute('attribute-id', 'sfccPreorder');
  xmlWriter.writeCharacters(sfccPreorder);
  xmlWriter.writeEndElement(); // sfccPreorder end
  if (sfccPreorderVariant != null) {
    xmlWriter.writeStartElement('custom-attribute');
    xmlWriter.writeAttribute('attribute-id', 'sfccPreorderVariant');
    xmlWriter.writeCharacters(sfccPreorderVariant);
    xmlWriter.writeEndElement(); // sfccPreorderVariant end
  }
  xmlWriter.writeEndElement(); // custom-attributes end
  xmlWriter.writeEndElement(); // product end
}

exports.Run = run;
