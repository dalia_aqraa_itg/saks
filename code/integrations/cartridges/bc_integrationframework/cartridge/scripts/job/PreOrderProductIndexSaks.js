'use strict';

/**
 *
 *  ==========  DEPRECATED ==========
 *  This job step script was the first incarnation of evaluating
 *  product preorderability and setting SFCC Preorder
 *
 *  EvaluateProductFlags has replaced this file as it combines both:
 *  - Searchability logic found inside CleanseProductIndexSaks.js
 *  - Preorder treatment found in EvaluateSfccPreorderFlag.js (below)
 *
 */

/**
 *  This is to update the SFCC Preorder for all the products on the catalog based on
 * 	1. PIM attribute preorder should be set to T  or Y
 *  2. Inventory record should be preoorderable or backorderable (For Saks Backorder is also consider as preorder per business usecase)
 *    -------------INVENTORY RULES -------------------------------
 *			1. On Hand Inventory could be 0 or more than 0.
 *			2. Future hand Inventory is greater than 0.
 *			3. Future on Hand - InStock date is in Future date of current time.
 */

var Status = require('dw/system/Status');
var ProductMgr = require('dw/catalog/ProductMgr');
var Logger = require('dw/system/Logger');
var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var XMLStreamWriter = require('dw/io/XMLStreamWriter');
var PREORDER_F = 'F';
var PREORDER_T = 'T';

function run(args) {
  if (empty(args.JobThreadIndex) || empty(args.ChunkSize)) {
    return new Status(Status.ERROR, 'ERROR', 'JobThreadIndex/ChunkSize/CatalogID param empty.');
  }
  var chunkSize = args.ChunkSize || 1000000;
  var startIndex = args.JobThreadIndex * chunkSize;
  var transactional = args.Transactional || false;
  var jobIndex = args.JobThreadIndex;
  var catalogID = args.CatalogID || 'master-bay';

  var productIterator = ProductMgr.queryAllSiteProducts();
  var productCount = productIterator.getCount();

  productIterator.forward(startIndex, chunkSize);

  var it = productIterator,
    product,
    counter = startIndex;
  var searchfalse = 0,
    searchfalsepids = '';
  var searchtrue = 0,
    searchtruepids = '';
  var searchfalse1 = 0,
    searchfalse1pids = '';
  var Calendar = require('dw/util/Calendar');

  var rootDirectory = File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'preorder';

  var xmlfileName = 'catalog_master_set_sfccpreorder' + jobIndex + '.xml';
  var xmlfile = new File(rootDirectory + File.SEPARATOR + xmlfileName);

  var xmlfileWriter;
  var xmlWriter;

  try {
    var dir = new File(rootDirectory);
    var dirExists = dir.exists();

    if (dirExists) {
      if (dir.isDirectory()) {
      }
    } else {
      dirExists = dir.mkdirs();
    }

    xmlfile.createNewFile();

    xmlfileWriter = new FileWriter(xmlfile);
    xmlWriter = new XMLStreamWriter(xmlfileWriter);

    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement('catalog');
    xmlWriter.writeAttribute('xmlns', 'http://www.demandware.com/xml/impex/catalog/2006-10-31');
    xmlWriter.writeAttribute('catalog-id', catalogID);

    while (it.hasNext()) {
      counter++;
      product = it.next();
      if (product && 'preOrder' in product.custom && (product.custom.preOrder === 'T' || product.custom.preOrder === 'Y')) {
        if (!product.variant) {
          var variants = product.variationModel ? product.variationModel.variants : [];
          if (variants.length > 0) {
            var allVariantPreOrderable = true;
            for (var i = 0; i < variants.length; i++) {
              var variant = variants[i];
              var availabilityModel = variant.availabilityModel;
              if (availabilityModel && !empty(availabilityModel)) {
                var inventoryRecord = availabilityModel.inventoryRecord;
                /** Rules
                 * 1. On Hand Inventory could be 0 or more than 0.
                 * 2. Future hand Inventory is greater than 0.
                 * 3. Future on Hand - InStock date is in Future date of current time.
                 *  */
                if (
                  !empty(inventoryRecord) &&
                  (inventoryRecord.preorderable || inventoryRecord.backorderable) &&
                  inventoryRecord.preorderBackorderAllocation.value > 0 &&
                  !empty(inventoryRecord.inStockDate)
                ) {
                  var inStockTime = new Calendar(inventoryRecord.inStockDate);
                  if (inStockTime.compareTo(currentTime) >= 0) {
                    // do not update line here as it is being updated for variant differently
                  } else {
                    allVariantPreOrderable = false;
                  }
                } else {
                  allVariantPreOrderable = false;
                }
              } else {
                allVariantPreOrderable = false;
              }
            }
            // If all variation are not Pre Orderable, master will SFCC Preorder msut be false;
            if (allVariantPreOrderable) {
              writeProduct(xmlWriter, product.ID, PREORDER_T);
            } else {
              writeProduct(xmlWriter, product.ID, PREORDER_F);
            }
          } else {
            writeProduct(xmlWriter, product.ID, PREORDER_F);
          }
        } else {
          // for Other types of Product, If PIM Pre Order is Y, update the SFCC Pre Order
          var availabilityModel = product.availabilityModel;
          if (availabilityModel && !empty(availabilityModel)) {
            var inventoryRecord = availabilityModel.inventoryRecord;
            /** Rules
             * 1. On Hand Inventory could be 0 or more than 0.
             * 2. Future hand Inventory is greater than 0.
             * 3. Future on Hand - InStock date is in Future date of current time.
             *  */

            if (
              !empty(inventoryRecord) &&
              (inventoryRecord.preorderable || inventoryRecord.backorderable) &&
              inventoryRecord.preorderBackorderAllocation.value > 0 &&
              !empty(inventoryRecord.inStockDate)
            ) {
              var inStockTime = new Calendar(inventoryRecord.inStockDate);
              if (inStockTime.compareTo(currentTime) >= 0) {
                writeProduct(xmlWriter, product.ID, PREORDER_T);
              } else {
                writeProduct(xmlWriter, product.ID, PREORDER_F);
              }
            } else {
              writeProduct(xmlWriter, product.ID, PREORDER_F);
            }
          } else {
            writeProduct(xmlWriter, product.ID, PREORDER_F);
          }
        }
      } else {
        // If Product is not applcable for Pre Orde, make sure SFCC Pre Order is set to false.
        writeProduct(xmlWriter, product.ID, PREORDER_F);
      }
    }
    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    Logger.debug(errorMsg);
    if (productIterator) productIterator.close();
    return new Status(Status.ERROR, 'ERROR', 'Error occured while executing job.');
  } finally {
    if (xmlWriter) xmlWriter.close();
    if (xmlfileWriter) xmlfileWriter.close();

    if (productIterator) productIterator.close();
  }

  return new Status(
    Status.OK,
    'OK',
    'successful. start=' +
      startIndex +
      ' end=' +
      counter +
      ' productCount=' +
      productCount +
      ' searchtrue=' +
      searchtrue +
      ', searchfalse=' +
      searchfalse +
      ', searchfalse1=' +
      searchfalse1
  );
}

function writeProduct(xmlWriter, ID, preorderable) {
  xmlWriter.writeStartElement('product');
  xmlWriter.writeAttribute('product-id', ID);
  xmlWriter.writeStartElement('sfccPreorder');
  xmlWriter.writeCharacters(preorderable);
  xmlWriter.writeEndElement();
  xmlWriter.writeEndElement();
}

exports.Run = run;
