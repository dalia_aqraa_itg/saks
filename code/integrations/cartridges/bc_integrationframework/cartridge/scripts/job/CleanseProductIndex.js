'use strict';
/**
 *  This is a one time Job step. It is designed to set the hbcProductType product custom attribute to standard.
 * productType
 * Used to indicate which template should be used for the PDP
 * Proper Values: home, bridal, gwp, giftcard, chanel, null (standard)"
 * It is to be updated only at the master product level or at the product type level ('standard'). Variant and other product types to be ignored
 */

var Status = require('dw/system/Status');
var Resource = require('dw/web/Resource');
var ProductMgr = require('dw/catalog/ProductMgr');
var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
var Logger = require('dw/system/Logger');
var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var CSVStreamWriter = require('dw/io/CSVStreamWriter');
var XMLStreamWriter = require('dw/io/XMLStreamWriter');
var Transaction = require('dw/system/Transaction');
var collections = require('bc_library/cartridge/scripts/util/Collections.ds').Collections;

function run(args) {
  if (empty(args.JobThreadIndex) || empty(args.ChunkSize)) {
    return new Status(Status.ERROR, 'ERROR', 'JobThreadIndex/ChunkSize/CatalogID param empty.');
  }
  var chunkSize = args.ChunkSize || 1000000;
  var startIndex = args.JobThreadIndex * chunkSize;
  var transactional = args.Transactional || false;
  var jobIndex = args.JobThreadIndex;
  var catalogID = args.CatalogID || 'master-bay';

  //return new Status(Status.ERROR, 'ERROR', 'job param empty.');
  var productIterator: SeekableIterator = ProductMgr.queryAllSiteProducts();
  //var productIterator : SeekableIterator = ProductMgr.queryProductsInCatalogSorted(Catalog);
  var productCount = productIterator.getCount();

  productIterator.forward(startIndex, chunkSize);

  var it = productIterator,
    product,
    counter = startIndex;
  var searchfalse = 0,
    searchfalsepids = '';
  var searchtrue = 0,
    searchtruepids = '';
  var searchfalse1 = 0,
    searchfalse1pids = '';
  var Calendar = require('dw/util/Calendar');

  var rootDirectory: String = File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'tmp';
  /*var fileName : String = "ProductsDroppedFromIndex"+jobIndex+".csv";
	var file : File = new File(rootDirectory + File.SEPARATOR + fileName);
	var fileName1 : String = "ProductsAddedToIndex"+jobIndex+".csv";
	var file1 : File = new File(rootDirectory + File.SEPARATOR + fileName1);
	var fileName2 : String = "ProductsNoPubDateDroppedFromIndex"+jobIndex+".csv";
	var file2 : File = new File(rootDirectory + File.SEPARATOR + fileName2);
	
	var fileWriter : FileWriter;	
	var csvWriter : CSVStreamWriter;
	var fileWriter1 : FileWriter;	
	var csvWriter1 : CSVStreamWriter;
	var fileWriter2 : FileWriter;	
	var csvWriter2 : CSVStreamWriter;*/

  var xmlfileName: String = 'catalog_master_set_searchable' + jobIndex + '.xml';
  var xmlfile: File = new File(rootDirectory + File.SEPARATOR + xmlfileName);

  var xmlfileWriter: FileWriter;
  var xmlWriter: XMLStreamWriter;

  try {
    var dir: File = new File(rootDirectory);
    var dirExists = dir.exists();

    if (dirExists) {
      if (dir.isDirectory()) {
      }
    } else {
      dirExists = dir.mkdirs();
    }

    /*file.createNewFile();
    	file1.createNewFile();
    	file2.createNewFile();*/

    xmlfile.createNewFile();

    /*if (!file.exists() && !file.createNewFile()) {
			logger.error("Unable to read inventory file " + fileName);
			return "";
		}*/

    xmlfileWriter = new FileWriter(xmlfile);
    xmlWriter = new XMLStreamWriter(xmlfileWriter);

    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement('catalog');
    xmlWriter.writeAttribute('xmlns', 'http://www.demandware.com/xml/impex/catalog/2006-10-31');
    xmlWriter.writeAttribute('catalog-id', catalogID);

    /*fileWriter = new FileWriter(file);
		csvWriter = new CSVStreamWriter(fileWriter);
		fileWriter1 = new FileWriter(file1);
		csvWriter1 = new CSVStreamWriter(fileWriter1);
		fileWriter2 = new FileWriter(file2);
		csvWriter2 = new CSVStreamWriter(fileWriter2);*/

    while (it.hasNext()) {
      counter++;
      product = it.next();
      var line = [];
      line.push(product.ID);

      if (product.master || !product.variant) {
        if ('publishedDate' in product.custom && !empty(product.custom['publishedDate'])) {
          var hbcProductType = product.custom['hbcProductType'] || 'standard';
          var publishedDate = new Calendar(product.custom['publishedDate']);
          var todayMinus180 = new Calendar();
          todayMinus180.add(Calendar.DAY_OF_YEAR, -180);

          /*
					GIVEN Product has publishedDate older than 180 days
					AND Product has inventory of 0
					AND hbcProductType is not “home” OR “bridal”
					THEN Product is marked as Searchable = False
    				*/

          if (publishedDate.before(todayMinus180) && !product.availabilityModel.orderable && hbcProductType != 'home' && hbcProductType != 'bridal') {
            if (product.searchable == true) {
              if (transactional) {
                /*Transaction.wrap(function() {
    								product.setSearchableFlag(false);
    							});*/
                writeProduct(xmlWriter, product.ID, false);
              }
            }
            searchfalse++;
            //csvWriter.writeNext(line);
          } else if (product.searchable == false) {
          /*
    				GIVEN Product has publishedDate up to 180 days ago
    				OR Product has inventory greater than 0
    				OR hbcProductType = “home” or “bridal”
    				THEN Product is marked as Searchable = True
    				AND Indexed in order to appear in arrays and search
    				*/
            if (transactional) {
              /*Transaction.wrap(function() {
								product.setSearchableFlag(true);
							});*/
              writeProduct(xmlWriter, product.ID, true);
            }
            searchtrue++;
            //csvWriter1.writeNext(line);
          }
        } else if (product.searchable == true) {
          if (transactional) {
            /*Transaction.wrap(function() {
							product.setSearchableFlag(false);
						});*/
            writeProduct(xmlWriter, product.ID, false);
          }
          searchfalse1++;
          //csvWriter2.writeNext(line);
        }
      }
    }
    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    Logger.debug(errorMsg);
    if (productIterator) productIterator.close();
    return new Status(Status.ERROR, 'ERROR', 'Error occured while executing job.');
  } finally {
    /*if(csvWriter) csvWriter.close();
		if(fileWriter) fileWriter.close();
		if(csvWriter1) csvWriter1.close();
		if(fileWriter1) fileWriter1.close();
		if(csvWriter2) csvWriter2.close();
		if(fileWriter2) fileWriter2.close();*/

    if (xmlWriter) xmlWriter.close();
    if (xmlfileWriter) xmlfileWriter.close();

    if (productIterator) productIterator.close();
  }

  return new Status(
    Status.OK,
    'OK',
    'successful. start=' +
      startIndex +
      ' end=' +
      counter +
      ' productCount=' +
      productCount +
      ' searchtrue=' +
      searchtrue +
      ', searchfalse=' +
      searchfalse +
      ', searchfalse1=' +
      searchfalse1
  );
}

function writeProduct(xmlWriter, ID, searchable) {
  xmlWriter.writeStartElement('product');
  xmlWriter.writeAttribute('product-id', ID);
  xmlWriter.writeStartElement('searchable-flag');
  xmlWriter.writeCharacters(searchable);
  xmlWriter.writeEndElement();
  xmlWriter.writeEndElement();
}

exports.Run = run;
