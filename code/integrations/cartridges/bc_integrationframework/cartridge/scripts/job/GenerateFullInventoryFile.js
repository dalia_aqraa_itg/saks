'use strict';
/**
 *  This is a one time Job step. It is designed to set the hbcProductType product custom attribute to standard.
 * productType
 * Used to indicate which template should be used for the PDP
 * Proper Values: home, bridal, gwp, giftcard, chanel, null (standard)"
 * It is to be updated only at the master product level or at the product type level ('standard'). Variant and other product types to be ignored
 */

var Status = require('dw/system/Status');
var Resource = require('dw/web/Resource');
var ProductMgr = require('dw/catalog/ProductMgr');
var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
var Logger = require('dw/system/Logger');
var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var XMLStreamWriter = require('dw/io/XMLStreamWriter');
var Transaction = require('dw/system/Transaction');

function run(args) {
  if (empty(args.allocation)) {
    //Logger.debug(errorMsg);
    return new Status(Status.ERROR, 'ERROR', 'allocation job param empty.');
  }
  var allocation = args.allocation;
  let products;
  var rootDirectory: String = File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'tmp';
  var fileName: String = 'FullInventory.xml';
  var file: File = new File(rootDirectory + File.SEPARATOR + fileName);

  var fileWriter: FileWriter;
  var xmlWriter: XMLStreamWriter;

  try {
    var dir: File = new File(rootDirectory);

    var dirExists = dir.exists();

    if (dirExists) {
      if (dir.isDirectory()) {
      }
    } else {
      dirExists = dir.mkdirs();
    }

    file.createNewFile();
    /*if (!file.exists() && !file.createNewFile()) {
			logger.error("Unable to read inventory file " + fileName);
			return "";
		}*/

    fileWriter = new FileWriter(file);
    xmlWriter = new XMLStreamWriter(fileWriter);

    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement('inventory');
    xmlWriter.setDefaultNamespace('http://www.demandware.com/xml/impex/inventory/2007-05-31');
    xmlWriter.setPrefix('ns', 'http://www.demandware.com/xml/impex/inventory/2007-05-31');
    xmlWriter.writeAttribute('xmlns', 'http://www.demandware.com/xml/impex/inventory/2007-05-31');

    xmlWriter.writeStartElement('inventory-list');

    // Header information
    var headerListId = ProductInventoryMgr.getInventoryList().getID(); //Returns the inventory list ID assigned to the current site
    var headerDefaultInStock = 'false';
    var headerDescription = ' Inventory';
    var headerUseBundle = 'false';

    xmlWriter.writeStartElement('header');
    xmlWriter.writeAttribute('list-id', headerListId);
    //if (header.mode != null) xmlWriter.writeAttribute("mode", header.mode); // null
    xmlWriter.writeStartElement('default-instock');
    xmlWriter.writeCharacters(headerDefaultInStock); // false
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement('description');
    xmlWriter.writeCharacters(headerDescription); //  Inventory
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement('use-bundle-inventory-only');
    xmlWriter.writeCharacters(headerUseBundle); //  Inventory
    xmlWriter.writeEndElement();

    xmlWriter.writeEndElement(); // header

    xmlWriter.writeStartElement('records');

    products = ProductMgr.queryAllSiteProducts();
    while (products.hasNext()) {
      try {
        var product = products.next();
        // Check if the product is master or standard product. Variant is not to be updated
        if (!product.master && !product.productSet) {
          xmlWriter.writeStartElement('record');
          xmlWriter.writeAttribute('product-id', product.ID);
          xmlWriter.writeStartElement('allocation');
          xmlWriter.writeCharacters(allocation);
          xmlWriter.writeEndElement(); // allocation
          /*xmlWriter.writeStartElement("preorder-backorder-handling");
        			xmlWriter.writeCharacters("backorder");
        			xmlWriter.writeEndElement();
        			xmlWriter.writeStartElement("preorder-backorder-allocation");
        			xmlWriter.writeCharacters(99999999);
        			xmlWriter.writeEndElement();
        			*/
          xmlWriter.writeEndElement(); // record
        }
      } catch (e) {
        var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
        Logger.debug(errorMsg);
      }
    }

    xmlWriter.writeEndElement(); // records
    xmlWriter.writeEndElement(); // inventory-list
    xmlWriter.writeEndElement(); // inventory
    xmlWriter.writeEndDocument();
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    Logger.debug(errorMsg);
    return new Status(Status.ERROR, 'ERROR', 'Error occured while executing job.');
  } finally {
    if (xmlWriter) xmlWriter.close();
    if (fileWriter) fileWriter.close();
    if (products) products.close();
  }
  return new Status(Status.OK, 'OK', 'successful');
}

exports.Run = run;
