'use strict';

/**
 *
 *  ==========  DEPRECATED ==========
 *
 *  EvaluateProductFlags has replaced this file as it combines both:
 *  - Searchability logic found inside CleanseProductIndexSaks.js
 *  - Preorder treatment found in EvaluateSfccPreorderFlag.js (below)
 *
 *
 */

var Status = require('dw/system/Status');
var ProductMgr = require('dw/catalog/ProductMgr');
var Logger = require('dw/system/Logger');
var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var XMLStreamWriter = require('dw/io/XMLStreamWriter');
var PREORDER_F = 'F';
var PREORDER_T = 'T';
var StringUtils = require('dw/util/StringUtils');

function run(args) {
  if (empty(args.JobThreadIndex) || empty(args.ChunkSize)) {
    return new Status(Status.ERROR, 'ERROR', 'JobThreadIndex/ChunkSize/CatalogID param empty.');
  }
  var chunkSize = args.ChunkSize || 100000;
  var startIndex = args.JobThreadIndex * chunkSize;
  var jobIndex = args.JobThreadIndex;
  var catalogID = args.CatalogID || 'master-s5a';
  var enableLogging = args.EnableLogging || false;

  //return new Status(Status.ERROR, 'ERROR', 'job param empty.');
  var productIterator: SeekableIterator = ProductMgr.queryAllSiteProducts();
  //var productIterator : SeekableIterator = ProductMgr.queryProductsInCatalogSorted(Catalog);
  var productCount = productIterator.getCount();

  productIterator.forward(startIndex, chunkSize);

  var it = productIterator,
    product,
    counter = startIndex;
  var countPreorderMastersTrue = 0;
  var countPreorderMastersFalse = 0;
  var countPreorderVariationsTrue = 0;
  var countPreorderVariationsFalse = 0;
  var countIssuePOFWBI = 0; // Issue counter, Preorder False with Backorder Inventory
  var countNoInventory = 0;
  var countBackorderable = 0;
  var countNoInStockDate = 0;
  var Calendar = require('dw/util/Calendar');
  var currentTime = new Calendar();

  var rootDirectory: String = File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'tmp';

  var xmlfileName: String = 'catalog_master_set_sfcc_preorder' + jobIndex + '.xml';
  var xmlfile: File = new File(rootDirectory + File.SEPARATOR + xmlfileName);

  var xmlfileWriter: FileWriter;
  var xmlWriter: XMLStreamWriter;

  try {
    var dir: File = new File(rootDirectory);
    var dirExists = dir.exists();

    if (dirExists) {
      if (dir.isDirectory()) {
      }
    } else {
      dirExists = dir.mkdirs();
    }

    xmlfile.createNewFile();
    xmlfileWriter = new FileWriter(xmlfile);
    xmlWriter = new XMLStreamWriter(xmlfileWriter);

    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement('catalog');
    xmlWriter.writeAttribute('xmlns', 'http://www.demandware.com/xml/impex/catalog/2006-10-31');
    xmlWriter.writeAttribute('catalog-id', catalogID);

    while (it.hasNext()) {
      counter++;
      product = it.next();

      if (product.master) {
        if ('sfccPreorder' in product.custom) {
          var mSfccPreorder = product.custom.sfccPreorder;
        }
        if (enableLogging) {
          Logger.info('master id: ' + product.ID + ' preOrder: ' + product.custom.preOrder);
        }

        var variants = product.variationModel ? product.variationModel.variants : [];
        if (variants.length > 0) {
          var allVariantPreOrderable = true;
          var countVariantNoInventory = 0;
          for (var i = 0; i < variants.length; i++) {
            var variant = variants[i];
            var vOnline = variant.online;
            var inventoryRecord = variant.availabilityModel.inventoryRecord;
            var logMsg = '';

            if (variant && 'sfccPreorder' in variant.custom) {
              var vSfccPreorder = variant.custom.sfccPreorder;
            }

            if (variant && 'preOrder' in variant.custom && variant.custom.preOrder === PREORDER_T) {
              /** Preorder Flagging Rules
               * 1. Master has PIM Driven Preorder set to T
               * 2. Variant is backorderable with future in-stock date, the variant SFCC Preorder is set to T
               * 3. If all elliglble variants are preorderable, then master has SFCC Preorder set to T
               *
               * Variant inventory of on-hand allocation > 0 or backorderpreorderallocation = 0
               * on the variant
               *
               * If one variant has all preorder stock sold:
               * - master should still have SFCC Preorder set to T, so that customers see this is a preorder product
               * - variant should still have SFCC Preorder set to T, so that any new incoming inventory will have Preorder PDP
               *
               *
               */
              if (inventoryRecord) {
                if (inventoryRecord.backorderable) {
                  countBackorderable++;
                  if (inventoryRecord.inStockDate) {
                    var inStockTime = new Calendar(inventoryRecord.inStockDate);
                    if (inStockTime.after(currentTime)) {
                      logMsg =
                        'backorderable: ' +
                        inventoryRecord.backorderable +
                        ' bo allocation: ' +
                        inventoryRecord.preorderBackorderAllocation.value +
                        ' oh allocation: ' +
                        inventoryRecord.stockLevel.value +
                        ' future inStockDate: ' +
                        inventoryRecord.inStockDate;
                      if (vSfccPreorder != PREORDER_T) {
                        writePreorderLine(xmlWriter, variant.ID, PREORDER_T);
                      }
                      if (inventoryRecord.stockLevel.value > 0) {
                        allVariantPreOrderable = false;
                      }
                      countPreorderVariationsTrue++;
                    } else {
                      logMsg =
                        'backorderable: ' +
                        inventoryRecord.backorderable +
                        ' allocation: ' +
                        inventoryRecord.preorderBackorderAllocation.value +
                        ' oh allocation: ' +
                        inventoryRecord.stockLevel.value +
                        ' past inStockDate: ' +
                        inventoryRecord.inStockDate;
                      if (vSfccPreorder != PREORDER_F) {
                        writePreorderLine(xmlWriter, variant.ID, PREORDER_F);
                      }
                      allVariantPreOrderable = false;
                      countPreorderVariationsFalse++;
                    }
                  } else {
                    logMsg = 'has no inStockDate set';
                    if (vSfccPreorder != PREORDER_F) {
                      writePreorderLine(xmlWriter, variant.ID, PREORDER_F);
                    }
                    allVariantPreOrderable = false;
                    countPreorderVariationsFalse++;
                    countNoInStockDate++;
                  }
                } else {
                  if (inventoryRecord.stockLevel.value > 0) {
                    /*
                     * Variant has inventory and is not backorderable, so we are going to assume
                     * it is now for regular sale
                     */
                    logMsg = 'is not set as a backorder, and has on-hand: ' + inventoryRecord.stockLevel.value;
                    if (vSfccPreorder != PREORDER_F) {
                      writePreorderLine(xmlWriter, variant.ID, PREORDER_F);
                    }
                    allVariantPreOrderable = false;
                    countPreorderVariationsFalse++;
                  } else {
                    /*
                     * Variant has no inventory and is not backorderable, so we are going to assume
                     * it is a sold out backorder item and should not count against master's preorder display
                     */
                    logMsg = 'is not set as a backorder, and has no on-hand inventory';
                    if (vSfccPreorder != PREORDER_T) {
                      writePreorderLine(xmlWriter, variant.ID, PREORDER_T);
                    }
                    countPreorderVariationsTrue++;
                  }
                }
              } else {
                /* If the variant has no inventory record, HBC buyers never ordered this variant
                 * - sku is not meant for sale, Onlines status set to False
                 * - product has no backorder records, so SFCC Preorder is F
                 *
                 * if (vSfccPreorder != PREORDER_F || vOnline != false) {
                 *	writePreorderOnlineFlag(xmlWriter, variant.ID, PREORDER_F, false);
                 * }
                 */
                logMsg = 'has no inventory record';
                /*
                 * Holding the above change, just set variant normally without taking offline.
                 */
                if (vSfccPreorder != PREORDER_F) {
                  writePreorderLine(xmlWriter, variant.ID, PREORDER_F);
                }
                /* Variations missing inventory records should not be included in evaluating
                 * if this master should be set to SFCC Preorder T.
                 * no allVariantPreOrderable = false; needed. CountVariantNoInventory used instead
                 */
                countPreorderVariationsFalse++;
                countVariantNoInventory++; // counter to track if all variants of this master have no inventory
                countNoInventory++;
              }
              if (enableLogging) {
                Logger.info('variant id: ' + variant.ID + ' ' + logMsg);
              }
            } else {
              if (vSfccPreorder != PREORDER_F) {
                writePreorderLine(xmlWriter, variant.ID, PREORDER_F);
              }
              allVariantPreOrderable = false;
              countPreorderVariationsFalse++;

              if (inventoryRecord && inventoryRecord.backorderable) {
                countIssuePOFWBI++;
                if (inventoryRecord.inStockDate) {
                  var inStockTime = new Calendar(inventoryRecord.inStockDate);
                  if (inStockTime.after(currentTime)) {
                    logMsg =
                      'backorderable: ' +
                      inventoryRecord.backorderable +
                      ' allocation: ' +
                      inventoryRecord.preorderBackorderAllocation.value +
                      ' future inStockDate: ' +
                      inventoryRecord.inStockDate;
                  } else {
                    logMsg =
                      'backorderable: ' +
                      inventoryRecord.backorderable +
                      ' allocation: ' +
                      inventoryRecord.preorderBackorderAllocation.value +
                      ' past inStockDate: ' +
                      inventoryRecord.inStockDate;
                  }
                } else {
                  logMsg = 'has no inStockDate set';
                }
                if (enableLogging) {
                  Logger.info('variant id: ' + variant.ID + ' ' + logMsg);
                }
              }
            }
          }
          /* If all variants of a master have no inventory record
           * then the master should NOT be set to SFCC Preorder T
           */
          if (variants.length == countVariantNoInventory) {
            allVariantPreOrderable = false;
          }

          /* If one or more variants are NOT preorderable,
           * then master SFCC Preorder flag must be F for false
           */
          if (!allVariantPreOrderable) {
            if (enableLogging) {
              Logger.info('Setting master id ' + product.ID + ' sfccPreorder to: ' + PREORDER_F);
            }
            if (mSfccPreorder != PREORDER_F) {
              writePreorderLine(xmlWriter, product.ID, PREORDER_F);
            }
            countPreorderMastersFalse++;
          } else {
            if (enableLogging) {
              Logger.info('Setting master id ' + product.ID + ' sfccPreorder to: ' + PREORDER_T);
            }
            if (mSfccPreorder != PREORDER_T) {
              writePreorderLine(xmlWriter, product.ID, PREORDER_T);
            }
            countPreorderMastersTrue++;
          }
        } else {
          if (mSfccPreorder != PREORDER_F) {
            writePreorderLine(xmlWriter, product.ID, PREORDER_F);
          }
          countPreorderMastersFalse++;
        }
      }
    }
    xmlWriter.writeEndElement(); // catalog end
    xmlWriter.writeEndDocument();
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    Logger.debug(errorMsg);
    Logger.info(errorMsg);
    if (productIterator) productIterator.close();
    return new Status(Status.ERROR, 'ERROR', 'Error occured while executing job.');
  } finally {
    if (xmlWriter) xmlWriter.close();
    if (xmlfileWriter) xmlfileWriter.close();

    if (productIterator) productIterator.close();
  }

  Logger.info(' start: ' + startIndex + ' end: ' + counter + ' of productCount: ' + productCount);
  Logger.info(' countPreorderMastersTrue: ' + countPreorderMastersTrue);
  Logger.info(' countPreorderMastersFalse: ' + countPreorderMastersFalse);
  Logger.info(' countPreorderVariationsTrue: ' + countPreorderVariationsTrue);
  Logger.info(' countPreorderVariationsFalse: ' + countPreorderVariationsFalse);
  Logger.info(' countNoInventory: ' + countNoInventory);
  Logger.info(' countBackorderable: ' + countBackorderable);
  Logger.info(' countNoInStockDate: ' + countNoInStockDate);
  Logger.info(' issue: ' + countIssuePOFWBI + ' Preorder False with Backorder Inventory');

  return new Status(Status.OK, 'OK', 'successful.');
}

function writePreorderLine(xmlWriter, ID, preorderable) {
  xmlWriter.writeStartElement('product');
  xmlWriter.writeAttribute('product-id', ID);
  xmlWriter.writeStartElement('custom-attributes');
  xmlWriter.writeStartElement('custom-attribute');
  xmlWriter.writeAttribute('attribute-id', 'sfccPreorder');
  xmlWriter.writeCharacters(preorderable);
  xmlWriter.writeEndElement(); // custom-attribute end
  xmlWriter.writeEndElement(); // custom-attributes end
  xmlWriter.writeEndElement(); // product end
}

function writePreorderOnlineFlag(xmlWriter, ID, preorderable, onlineFlag) {
  xmlWriter.writeStartElement('product');
  xmlWriter.writeAttribute('product-id', ID);
  xmlWriter.writeStartElement('online-flag');
  xmlWriter.writeCharacters(onlineFlag);
  xmlWriter.writeEndElement(); // online-flag end
  xmlWriter.writeStartElement('custom-attributes');
  xmlWriter.writeStartElement('custom-attribute');
  xmlWriter.writeAttribute('attribute-id', 'sfccPreorder');
  xmlWriter.writeCharacters(preorderable);
  xmlWriter.writeEndElement(); // custom-attribute end
  xmlWriter.writeEndElement(); // custom-attributes end
  xmlWriter.writeEndElement(); // product end
}

exports.Run = run;
