'use strict';

/**
 *
 *  ==========  DEPRECATED ==========
 *
 *  EvaluateProductFlags has replaced this file as it combines both:
 *  - Searchability logic found inside CleanseProductIndexSaks.js (below)
 *  - Preorder treatment found in EvaluateSfccPreorderFlag.js
 *
 *
 */

/**
 *  This is a one time Job step. It is designed to set the hbcProductType product custom attribute to standard.
 * productType
 * Used to indicate which template should be used for the PDP
 * Proper Values: home, bridal, gwp, giftcard, chanel, null (standard)"
 * It is to be updated only at the master product level or at the product type level ('standard'). Variant and other product types to be ignored
 */

var Status = require('dw/system/Status');
var Resource = require('dw/web/Resource');
var ProductMgr = require('dw/catalog/ProductMgr');
var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
var Logger = require('dw/system/Logger');
var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var XMLStreamWriter = require('dw/io/XMLStreamWriter');

function run(args) {
  if (empty(args.JobThreadIndex) || empty(args.ChunkSize)) {
    return new Status(Status.ERROR, 'ERROR', 'JobThreadIndex/ChunkSize/CatalogID param empty.');
  }
  var chunkSize = args.ChunkSize || 100000;
  var startIndex = args.JobThreadIndex * chunkSize;
  var transactional = args.Transactional || false;
  var jobIndex = args.JobThreadIndex;
  var catalogID = args.CatalogID || 'master-s5a';

  //return new Status(Status.ERROR, 'ERROR', 'job param empty.');
  var productIterator: SeekableIterator = ProductMgr.queryAllSiteProducts();
  //var productIterator : SeekableIterator = ProductMgr.queryProductsInCatalogSorted(Catalog);
  var productCount = productIterator.getCount();

  productIterator.forward(startIndex, chunkSize);

  var it = productIterator,
    product,
    counter = startIndex;
  var searchFalse = 0;
  var searchTrue = 0;
  var noPublishSearchFalse = 0;
  var Calendar = require('dw/util/Calendar');

  var rootDirectory: String = File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'tmp';

  var xmlfileName: String = 'catalog_master_set_searchable' + jobIndex + '.xml';
  var xmlfile: File = new File(rootDirectory + File.SEPARATOR + xmlfileName);

  var xmlfileWriter: FileWriter;
  var xmlWriter: XMLStreamWriter;

  try {
    var dir: File = new File(rootDirectory);
    var dirExists = dir.exists();

    if (dirExists) {
      if (dir.isDirectory()) {
      }
    } else {
      dirExists = dir.mkdirs();
    }

    xmlfile.createNewFile();

    xmlfileWriter = new FileWriter(xmlfile);
    xmlWriter = new XMLStreamWriter(xmlfileWriter);

    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement('catalog');
    xmlWriter.writeAttribute('xmlns', 'http://www.demandware.com/xml/impex/catalog/2006-10-31');
    xmlWriter.writeAttribute('catalog-id', catalogID);

    while (it.hasNext()) {
      counter++;
      product = it.next();
      var line = [];
      line.push(product.ID);

      if (product.master || !product.variant) {
        if ('publishedDate' in product.custom && !empty(product.custom['publishedDate'])) {
          var hbcProductType = product.custom['hbcProductType'] || 'standard';
          var publishedDate = new Calendar(product.custom['publishedDate']);
          var todayMinus180 = new Calendar();
          todayMinus180.add(Calendar.DAY_OF_YEAR, -180);

          /*
					GIVEN a master product has publishedDate older than 180 days
					AND master has inventory of 0 OR preorder (backorder) inventory of 0
					AND hbcProductType is not "CSRonly" OR "CSRstores"
					THEN master is marked as Searchable = False

					SAKS logic: https://hbcdigital.atlassian.net/wiki/spaces/Sfsx/pages/1223163970/Indexing+Criteria+SAKS
    				*/

          if (publishedDate.before(todayMinus180) && !product.availabilityModel.orderable && hbcProductType != 'CSRonly' && hbcProductType != 'CSRstores') {
            if (product.searchable == true) {
              if (transactional) {
                writeProduct(xmlWriter, product.ID, false);
              }
            }
            searchFalse++;
          } else if (product.searchable == false) {
          /*
    				GIVEN master product has publishedDate up to 180 days ago
    				OR Product has inventory greater than 0 OR preorder (backorder) inventory greater than 0
    				OR hbcProductType = "CSRonly" or "CSRstores"
    				THEN Product is marked as Searchable = True
    				AND Indexed in order to appear in arrays and search
    				*/
            if (transactional) {
              writeProduct(xmlWriter, product.ID, true);
            }
            searchTrue++;
          }
        } else if (product.searchable == true) {
          if (transactional) {
            writeProduct(xmlWriter, product.ID, false);
          }
          noPublishSearchFalse++;
        }
      }
    }
    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    Logger.debug(errorMsg);
    Logger.info(errorMsg);
    if (productIterator) productIterator.close();
    return new Status(Status.ERROR, 'ERROR', 'Error occured while executing job.');
  } finally {
    if (xmlWriter) xmlWriter.close();
    if (xmlfileWriter) xmlfileWriter.close();

    if (productIterator) productIterator.close();
  }
  Logger.info(' start: ' + startIndex + ' end: ' + counter + ' products: ' + productCount);
  Logger.info(' searchable true: ' + searchTrue);
  Logger.info(' searchable false: ' + searchFalse);
  Logger.info(' no publish date: ' + noPublishSearchFalse);

  return new Status(Status.OK, 'OK', 'successful.');
}

function writeProduct(xmlWriter, ID, searchable) {
  xmlWriter.writeStartElement('product');
  xmlWriter.writeAttribute('product-id', ID);
  xmlWriter.writeStartElement('searchable-flag');
  xmlWriter.writeCharacters(searchable);
  xmlWriter.writeEndElement();
  xmlWriter.writeEndElement();
}

exports.Run = run;
