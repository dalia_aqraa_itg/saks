'use strict';
/**
 *  This is a one time Job step. It is designed to set the hbcProductType product custom attribute to standard.
 * productType
 * Used to indicate which template should be used for the PDP
 * Proper Values: home, bridal, gwp, giftcard, chanel, null (standard)"
 * It is to be updated only at the master product level or at the product type level ('standard'). Variant and other product types to be ignored
 */

var Status = require('dw/system/Status');
var Resource = require('dw/web/Resource');
var ProductMgr = require('dw/catalog/ProductMgr');
var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
var Logger = require('dw/system/Logger');
var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var CSVStreamWriter = require('dw/io/CSVStreamWriter');
var Transaction = require('dw/system/Transaction');
var collections = require('bc_library/cartridge/scripts/util/Collections.ds').Collections;

function run(args) {
  if (empty(args.attributes)) {
    return new Status(Status.ERROR, 'ERROR', 'job param empty.');
  }
  var attributes = JSON.parse(args.attributes);
  let products;
  var rootDirectory: String = File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'tmp';
  var fileName: String = 'whatsavailable.csv';
  var file: File = new File(rootDirectory + File.SEPARATOR + fileName);
  var fileName1: String = 'masters.csv';
  var file1: File = new File(rootDirectory + File.SEPARATOR + fileName1);
  var fileName2: String = 'skus.csv';
  var file2: File = new File(rootDirectory + File.SEPARATOR + fileName2);

  var fileNameMunassigned: String = 'masters-unassigned.csv';
  var fileMunassigned: File = new File(rootDirectory + File.SEPARATOR + fileNameMunassigned);

  var fileNameSunassigned: String = 'skus-unassigned.csv';
  var fileSunassigned: File = new File(rootDirectory + File.SEPARATOR + fileNameSunassigned);

  var fileNameNullPrices: String = 'null-prices.csv';
  var fileNullPrices: File = new File(rootDirectory + File.SEPARATOR + fileNameNullPrices);

  var fileWriter: FileWriter;
  var csvWriter: CSVStreamWriter;
  var fileWriter1: FileWriter;
  var csvWriter1: CSVStreamWriter;
  var fileWriter2: FileWriter;
  var csvWriter2: CSVStreamWriter;

  var fileWriterMunassigned: FileWriter;
  var csvWriterMunassigned: CSVStreamWriter;

  var fileWriterSunassigned: FileWriter;
  var csvWriterSunassigned: CSVStreamWriter;

  var fileWriterNullPrices: FileWriter;
  var csvWriterNullPrices: CSVStreamWriter;

  try {
    var dir: File = new File(rootDirectory);

    var dirExists = dir.exists();

    if (dirExists) {
      if (dir.isDirectory()) {
      }
    } else {
      dirExists = dir.mkdirs();
    }

    file.createNewFile();
    file1.createNewFile();
    file2.createNewFile();
    fileMunassigned.createNewFile();
    fileSunassigned.createNewFile();
    fileNullPrices.createNewFile();
    /*if (!file.exists() && !file.createNewFile()) {
			logger.error("Unable to read inventory file " + fileName);
			return "";
		}*/

    fileWriter = new FileWriter(file);
    csvWriter = new CSVStreamWriter(fileWriter);
    fileWriter1 = new FileWriter(file1);
    csvWriter1 = new CSVStreamWriter(fileWriter1);
    fileWriter2 = new FileWriter(file2);
    csvWriter2 = new CSVStreamWriter(fileWriter2);

    //fileWriter2 = new FileWriter(file2);
    //csvWriter2 = new CSVStreamWriter(fileWriter2);

    fileWriterMunassigned = new FileWriter(fileMunassigned);
    csvWriterMunassigned = new CSVStreamWriter(fileWriterMunassigned);

    fileWriterSunassigned = new FileWriter(fileSunassigned);
    csvWriterSunassigned = new CSVStreamWriter(fileWriterSunassigned);

    fileWriterNullPrices = new FileWriter(fileNullPrices);
    csvWriterNullPrices = new CSVStreamWriter(fileWriterNullPrices);

    //csvWriter.writeNext(attributes);

    //var Catalog = require('dw/catalog/CatalogMgr').getCatalog("master-bay");

    //var site : Site = Site.getCurrent();
    var productIterator = null;
    /*var productIterator : SeekableIterator = ProductMgr.queryAllSiteProductsSorted();
		//var productIterator : SeekableIterator = ProductMgr.queryProductsInCatalogSorted(Catalog);
		var productCount = productIterator.getCount();*/
    /*csvWriter.writeNext([productCount]);
		csvWriter1.writeNext([productCount]);
		csvWriter2.writeNext([productCount]);*/

    //productIterator.close();

    var ProductSearchModel = require('dw/catalog/ProductSearchModel');
    var psm = new ProductSearchModel();
    //psm.setOrderableProductsOnly(true);
    psm.setCategoryID('root');
    psm.setRecursiveCategorySearch(true);
    psm.setSearchPhrase('*');
    //psm.setSearchPhrase("89749518");
    //psm.addRefinementValues('hbcProductType', preferences.nonChanelProductTypes);
    psm.search();
    var it = psm.getProductSearchHits();
    //var it = productIterator;
    var hit;

    var results = '';
    var total = 0,
      siz,
      varModel,
      varAttrs,
      counter = 0;

    while (it.hasNext()) {
      counter++;
      hit = it.next();
      var line = [];
      line.push(hit.productID);

      if (hit.hitType == dw.catalog.ProductSearchHit.HIT_TYPE_PRODUCT_MASTER) {
        //if (hit.assignedToSiteCatalog) {
        // assigned
        csvWriter1.writeNext(line);
        /*} 
	    		else {
	    			//unassigned
	    			csvWriterMunassigned.writeNext(line);
	    		}*/
      }
      /*else {
	    		// sku
	    		if (hit.assignedToSiteCatalog) {
	    			// assigned
	    			csvWriter2.writeNext(line);
	    		} 
	    		else {
	    			//unassigned
	    			csvWriterSunassigned.writeNext(line);
	    		}
	    	}*/

      continue;

      varModel = hit.product.variationModel;
      //varModel = hit.variationModel;

      if (varModel) {
        //var line = [];
        //line.push(hit.ID);
        var addLine = false;
        collections.each(varModel.productVariationAttributes, function (varAttr) {
          //var varvals = varModel.getAllValues(varAttr);
          //var varvalsstr = varvals.toArray().join();
          //var varsize = varvals.size();

          if (varModel.getAllValues(varAttr).size() < 1) {
            //if (hit.getRepresentedVariationValues(varAttr).size() < 1){
            //line.push(varAttr.attributeID+':'+hit.getRepresentedVariationValues(varAttr).join());
            line.push(varAttr.attributeID);
            addLine = true;
          }
        });
        addLine ? csvWriter.writeNext(line) : '';
      }

      //if (hit.getPriceModel().price.valueOrNull == null) {
      if (hit.minPrice.valueOrNull == null) {
        //null price items
        csvWriterNullPrices.writeNext(line);
      }
      /*if (hit.minPrice.valueOrNull != null) {
   		
	    		siz = hit.representedProductIDs.size();
	    		
	    		collections.each(hit.representedProducts, function (product) {
	    			var masterP = product.variant ? product.masterProduct : product;
	    			var line = [];
	    			collections.each(new dw.util.ArrayList(attributes), function(attr) {
	    				var attrVal = attr in product ? product[attr] : (attr in product.custom ? product.custom[attr] : '');
	    				// if still no value try master product
	    				if (empty(attrVal)) {
	    					attrVal = attr in masterP ? masterP[attr] : (attr in masterP.custom ? masterP.custom[attr] : '');
	    				}
	    				line.push(attrVal);
	    			});

	    			csvWriter.writeNext(line);
    			
  		});
	    		
	    		total += siz;
	    	}*/
    }
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    Logger.debug(errorMsg);
    if (productIterator) productIterator.close();
    return new Status(Status.ERROR, 'ERROR', 'Error occured while executing job.');
  } finally {
    if (csvWriter) csvWriter.close();
    if (fileWriter) fileWriter.close();
    if (csvWriter1) csvWriter1.close();
    if (fileWriter1) fileWriter1.close();
    if (csvWriter2) csvWriter2.close();
    if (fileWriter2) fileWriter2.close();

    if (csvWriterMunassigned) csvWriterMunassigned.close();
    if (fileWriterMunassigned) fileWriterMunassigned.close();
    if (csvWriterSunassigned) csvWriterSunassigned.close();
    if (fileWriterSunassigned) fileWriterSunassigned.close();

    if (csvWriterNullPrices) csvWriterNullPrices.close();
    if (fileWriterNullPrices) fileWriterNullPrices.close();

    if (products) products.close();
    if (productIterator) productIterator.close();
  }
  return new Status(Status.OK, 'OK', 'successful counter=' + counter);
}

exports.Run = run;
