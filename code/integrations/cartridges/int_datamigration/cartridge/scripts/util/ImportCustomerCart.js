'use strict';
var Logger = require('dw/system/Logger');
var ProductListMgr = require('dw/customer/ProductListMgr');
var File = require('dw/io/File');
var FileReader = require('dw/io/FileReader');
var FileWriter = require('dw/io/FileWriter');
var XMLStreamReader = require('dw/io/XMLStreamReader');
var XMLStreamConstants = require('dw/io/XMLStreamConstants');
var ProductMgr = require('dw/catalog/ProductMgr');
var Logger = require('dw/system/Logger');
var Status = require('dw/system/Status');
var Calendar = require('dw/util/Calendar');
var Site = require('dw/system/Site');
const IMPEX_PROFILE_SYNC_PATH: 'LegacyData/Cart/Incoming';
var DeltaCartSyncLogs = Logger.getLogger("DeltaCartSync", "DeltaCartSync");

function importCustomerCart(args, jobStepExecution){
	try {
		var cartSyncFolder = args.cartSyncFolder ? args.cartSyncFolder : IMPEX_PROFILE_SYNC_PATH;
		var cartSyncFeedFilePattern = args.cartSyncFeedFilePattern;
		if ( empty(cartSyncFolder) || empty(cartSyncFeedFilePattern)) {
			return new Status(Status.ERROR, 'ERROR', 'One or more mandatory parameters are missing.  profileSyncFolder = (' + cartSyncFolder + ') profileSyncFeedFilePattern = (' + cartSyncFeedFilePattern + ')');
		}
		var importfile : File = new File(File.IMPEX + /src/ + cartSyncFolder);
		if (!importfile.exists()) {
			DeltaCartSyncLogs.info('Import FAILED: File not found. File Name: ' + importfile.fullPath );
			return Status.ERROR;
		}
		importfile = importfile.listFiles();
		// If no files are found, go to the next locale
		if(!importfile) {
			DeltaCartSyncLogs.info("NO FILES FOUND");
		}
		// filter out files which match the file pattern
		for each(var file in importfile) {
			if(file.getName().match(cartSyncFeedFilePattern) == null || file.isDirectory()) {
				continue;
			} else {
			var fileReader : FileReader = new FileReader(file, "UTF-8");
			var xmlStreamReader : XMLStreamReader = new XMLStreamReader(fileReader);
			var exportfile : File = new File(File.IMPEX + /src/ + cartSyncFolder +  "TEMP"+ File.SEPARATOR + "TEMP-" +file.name);
			var destDir = new File(File.IMPEX + /src/ + cartSyncFolder+"TEMP"+ '/');
			if (!destDir.exists()) {
				destDir.mkdirs();
			}
			var fileWriter ;
			var headerXML : XML  = null ;
			while (xmlStreamReader.hasNext()) {
				var element = xmlStreamReader.next();
				if (element == XMLStreamConstants.START_ELEMENT) {
					var localElementName : String = xmlStreamReader.getLocalName();
					if (localElementName == "product-lists") {
						var xmlChildren = xmlStreamReader.readXMLObject().children();
						for each(var child : Object in xmlChildren) {
							try {
								var localName = child.localName();
								var productlistFromSFCC ;
								var lastModifiedinSFCC ;
								var lastModifiedinSFCCCalendar;
								var lastModifiedinBlueMartini ;
								var lastModifiedinBlueMartiniCalendar ;
								if (child.localName() != null && localName == 'product-list' && child.attribute('list-id') && child.attribute('list-id').toLowerCase() != 'none') {
									productlistFromSFCC = null;
									lastModifiedinSFCC = null;
									lastModifiedinSFCCCalendar= null;
									lastModifiedinBlueMartini = null;
									lastModifiedinBlueMartiniCalendar = null;
										for each(var child2 : Object in child.children()) {
											var localName2 = child2.localName();
											if (child2.localName() != null && localName2 == 'owner') {
												DeltaCartSyncLogs.info("Import Customer Number =>" + child2.attribute('customer-no') + " list-id =>" + child.attribute('list-id'));
												productlistFromSFCC = ProductListMgr.getProductList(child.attribute('list-id'));
												if (productlistFromSFCC != null)
												{
													lastModifiedinSFCC = productlistFromSFCC.getLastModified();
													lastModifiedinSFCCCalendar = new Calendar(lastModifiedinSFCC);
												}
											}
											if (child2.localName() != null && child2.localName() == 'custom-attributes'){
												for each(var child3 : Object in child2.children()) {
													if (child3.localName() != null && child3.localName() == 'custom-attribute' 
														&& child3.attribute('attribute-id') == 'lastModifiedCustom')
													{
														lastModifiedinBlueMartini = new Date(child3.valueOf());
														lastModifiedinBlueMartiniCalendar = new Calendar(lastModifiedinBlueMartini);
														DeltaCartSyncLogs.info("Import login lastModifiedinBlueMartini--->" + lastModifiedinBlueMartini);
														DeltaCartSyncLogs.info("Import login lastModifiedinSFCC--->" + lastModifiedinSFCC);
														var lastImportedDate = new Date().toISOString(); 
														var lastImportedDateXML : XML = <custom-attribute attribute-id="lastImportedDate">{lastImportedDate}</custom-attribute> ;
														child2.appendChild(lastImportedDateXML);
													}
												}
											}
										}
										if( lastModifiedinSFCCCalendar == null || (lastModifiedinBlueMartiniCalendar!= null &&
												lastModifiedinSFCCCalendar != null &&
												lastModifiedinBlueMartiniCalendar.after(lastModifiedinSFCCCalendar))){
													DeltaCartSyncLogs.info("login compare--->" + "TRUE");
											if(headerXML == null){
												fileWriter = new FileWriter(exportfile, "UTF-8", true);
												var schemaURL = "http://www.demandware.com/xml/impex/productlist/2009-10-28";
												headerXML  = <product-lists xmlns={schemaURL}></product-lists> ;
											}
											headerXML.appendChild(child);
										}
								}
							} catch (error) {
								DeltaCartSyncLogs.error("Import Customer Number =>" + child.attribute('list-id'), error);
							}
						}
					}
				}
			}
			if (headerXML != null)
			{
				fileWriter.write(headerXML.toXMLString());
			}
			if (fileWriter != null) {
				fileWriter.close();
			}

			var archiveFolder = jobStepExecution.getParameterValue('archiveFolder');
			var archiveFileDir = new File(File.IMPEX + '/src/LegacyData/'+ archiveFolder);
			if (!archiveFileDir.exists()) {
				archiveFileDir.mkdirs();
			}
			var archiveFile = new File(archiveFileDir.fullPath + File.SEPARATOR + file.name);
			file.renameTo(archiveFile);

			}
			}
	} finally {
		if (xmlStreamReader != null) {
			xmlStreamReader.close();
		}
		if (fileReader != null) {
			fileReader.close();
		}
		if (fileWriter != null) {
			fileWriter.close();
		}
	}
	return new Status(Status.OK);
}
exports.importCustomerCart = importCustomerCart;