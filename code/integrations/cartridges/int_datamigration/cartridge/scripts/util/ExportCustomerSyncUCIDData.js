'use strict';
var CustomerMgr = require('dw/customer/CustomerMgr');
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');
var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var CSVStreamWriter = require('dw/io/CSVStreamWriter');
var Logger = require('dw/system/Logger').getLogger('DeltaCustomerSync', 'DeltaCustomerSync');
var Status = require('dw/system/Status');
var Site = require('dw/system/Site');

var count = 0;
var lineCounter = 0;
var profileItr;
var fileWriter;
var writer;

function createCSVHeader() {
  var headerArr = [];
  headerArr[0] = 'customer-no';
  headerArr[1] = 'customer-id';
  headerArr[2] = 'first-name';
  headerArr[3] = 'last-name';
  headerArr[4] = 'emailId';
  return headerArr;
}

function generateCustomerData(profile) {
  var customerData = [];
  customerData[0] = profile.customerNo ? profile.customerNo : '';
  customerData[1] = profile.customer ? profile.customer.ID : '';
  customerData[2] = profile.firstName ? profile.firstName : '';
  customerData[3] = profile.lastName ? profile.lastName : '';
  customerData[4] = profile.email ? profile.email : '';
  return customerData;
}

function createFile(fileName) {
  count = count + 1;
  var targetDirectory = new File(
    File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'LegacyData' + File.SEPARATOR + 'UCIDSync' + File.SEPARATOR + 'Outgoing'
  );
  var result = targetDirectory.mkdirs();
  var timeStamp = StringUtils.formatCalendar(new Calendar(), 'yyyy_MM_dd_hh_mm');
  var fileName = Site.getCurrent().getID() + '_' + fileName + '_' + count + '_' + timeStamp + '.txt';
  var file = new File(
    File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'LegacyData' + File.SEPARATOR + 'UCIDSync' + File.SEPARATOR + 'Outgoing' + File.SEPARATOR + fileName
  );
  result = file.createNewFile();
  return file;
}

function exportCustomerData(args, jobStepExecution) {
  try {
    var numberOfLinesPerFile = args.numberOfLinesPerFile ? parseInt(args.numberOfLinesPerFile) : 10;
    profileItr = CustomerMgr.queryProfiles('', null);
    if (profileItr.count > 0) {
      while (profileItr.hasNext()) {
        var profile = profileItr.next();
        // Create a new File when counter is 0;
        if (lineCounter === 0) {
          if (writer != null) {
            writer.close();
          }
          if (fileWriter != null) {
            fileWriter.close();
          }
          // Create a new Text File.
          fileWriter = new FileWriter(createFile('UICDSyncData'), 'UTF-8');
          writer = new CSVStreamWriter(fileWriter, '|');
          writer.writeNext(createCSVHeader());
        }

        // generate the Customer Data to Write.
        writer.writeNext(generateCustomerData(profile));
        // Increment the counter
        lineCounter = lineCounter + 1;

        // Reset the Counter if File is full.
        var c = lineCounter;
        if (lineCounter === numberOfLinesPerFile) {
          lineCounter = 0;
        }
      }
    }
  } catch (e) {
    Logger.error('There was an error while generating the UCID Sync Customer Data from SFCC ' + e.message);
    return new Status(Status.ERROR, 'ERROR');
  } finally {
    if (profileItr != null) {
      profileItr.close();
    }
    if (writer != null) {
      writer.close();
    }
    if (fileWriter != null) {
      fileWriter.close();
    }
  }
  return new Status(Status.OK);
}

exports.exportCustomerData = exportCustomerData;
