'use strict';

var Status = require('dw/system/Status');

var File = require('dw/io/File');
var CustomerMgr = require('dw/customer/CustomerMgr');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var Logger = require('dw/system/Logger');
var Site = require('dw/system/Site');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var FileWriter = require('dw/io/FileWriter');
var Transaction = require('dw/system/Transaction');
var DeltaPaymentSyncLogs = Logger.getLogger('DeltaPaymentSync', 'DeltaPaymentSync');

var profilesIter;
var exportFileName;
var destDir;
var profileSyncFolder;
var count = 0;
var fileWriter;
var customObj;

/**
 * Initialize readers and writers for job processing
 *
 * @param {Object}
 *            parameters job parameters
 * @param {JobStepExecution}
 *            stepExecution job step execution
 * @returns {Status} if error returns status
 */
exports.beforeStep = function (parameters, stepExecution) {
  var profileSyncFileName = parameters.profileSyncFeedFileName ? parameters.profileSyncFeedFileName : 'payment_export_feed';
  profileSyncFolder = parameters.profileSyncFolder ? parameters.profileSyncFolder : 'LegacyData/Payment/Outgoing';
  destDir = new File(File.IMPEX + /src/ + profileSyncFolder);

  if (!destDir.exists()) {
    destDir.mkdirs();
  }
  var calendar = new Calendar();
  calendar.timeZone = 'GMT';
  var gmtDateString = StringUtils.formatCalendar(calendar, 'yyyy-MM-dd_HH-mm-ss');
  exportFileName = profileSyncFileName + '_' + gmtDateString;
  var lastPaymentExportJobRunDate = new Date();
  customObj = CustomObjectMgr.getCustomObject('LastCustomerExport', '100');
  if (customObj != null && customObj.custom.lastPaymentExportJobRunDate != null && customObj.custom.lastPaymentExportJobRunDate != '') {
    lastPaymentExportJobRunDate = customObj.custom.lastPaymentExportJobRunDate;
  } else {
    lastPaymentExportJobRunDate = new Date();
  }

  profilesIter = CustomerMgr.queryProfiles('lastModified > {0}', null, lastPaymentExportJobRunDate);
  return undefined;
};

exports.getTotalCount = function () {
  return profilesIter.count;
};

exports.read = function () {
  if (profilesIter.hasNext()) {
    return profilesIter.next();
  }
  return undefined;
};

exports.process = function (customer) {
  var lastImportedBlueMartiniCalendar;
  var lastModifiedinSFCCCalendar;
  var lastImportedBlueMartini = customer.custom.lastImportedDate;
  var lastModifiedSFCC = customer.custom.lastPaymentModified;
  var jobDate;
  var jobDateCal;

  var lastModifiedSystemSFCC = new Calendar(customer.getLastModified());

  if (lastModifiedSFCC !== null && lastModifiedSFCC !== '') {
    lastModifiedinSFCCCalendar = new Calendar(new Date(lastModifiedSFCC));
    lastModifiedinSFCCCalendar.add(Calendar.SECOND, 3);
  }

  var custObj = CustomObjectMgr.getCustomObject('LastCustomerExport', '100');

  if (customObj != null && customObj.custom.lastPaymentExportJobRunDate != null && customObj.custom.lastPaymentExportJobRunDate != '') {
    jobDate = customObj.custom.lastPaymentExportJobRunDate;
    jobDateCal = new Calendar(jobDate);
  }

  DeltaPaymentSyncLogs.info('Export customerNo -> ' + customer.customerNo);

  if (!empty(lastModifiedinSFCCCalendar)) {
    if (
      lastModifiedinSFCCCalendar.after(lastModifiedSystemSFCC) ||
      lastModifiedinSFCCCalendar.equals(lastModifiedSystemSFCC) ||
      lastModifiedinSFCCCalendar.after(jobDateCal)
    ) {
      DeltaPaymentSyncLogs.info(
        'Export System date greater then Custom ->' +
          lastModifiedinSFCCCalendar.after(lastModifiedSystemSFCC) +
          ' System date equals Custom -> ' +
          lastModifiedinSFCCCalendar.equals(lastModifiedSystemSFCC)
      );

      if (lastImportedBlueMartini != null && lastImportedBlueMartini != '') {
        lastImportedBlueMartiniCalendar = new Calendar(new Date(lastImportedBlueMartini));
      } else {
        return customer;
      }

      DeltaPaymentSyncLogs.info('lastImportedBlueMartini -> ' + new Date(lastImportedBlueMartini) + ' lastModifiedinSFCC -> ' + new Date(lastModifiedSFCC));
      DeltaPaymentSyncLogs.info('Export SFCC date greater then BlueMartini ->' + lastModifiedinSFCCCalendar.after(lastImportedBlueMartiniCalendar));

      if (lastModifiedinSFCCCalendar.after(lastImportedBlueMartiniCalendar)) {
        return customer;
      } else {
        return undefined;
      }
    } else {
      return undefined;
    }
  } else {
    return undefined;
  }
};

exports.write = function (lines, parameters, stepExecution) {
  count = count + 1;
  var filename = profileSyncFolder + Site.getCurrent().getID() + '-' + exportFileName + '-' + count + '.xml';
  var exportfile: File = new File(File.IMPEX + /src/ + filename);
  var headerXML: XML = null;
  var customerIter = lines.iterator();
  var child: XML = null;

  while (customerIter.hasNext()) {
    var customerFromSFCC = customerIter.next();
    customerNo = customerFromSFCC.customerNo;
    child = <customer customer-no={customerNo}></customer>;
    var walletCust = customerFromSFCC.getWallet();
    var walletCustCollection = walletCust.getPaymentInstruments();
    var walletCustIter = walletCustCollection.iterator();
    var paymentInstrXML = <payment-instruments></payment-instruments>;
    var hasCC = false;
    while (walletCustIter.hasNext()) {
      hasCC = true;
      var custPaymentInstrument = walletCustIter.next();
      var payment: XML = (
        <payment-instrument>
          <credit-card>
            <card-type>{getCreditCardType(custPaymentInstrument.creditCardType)}</card-type>
            <card-number>{custPaymentInstrument.maskedCreditCardNumber}</card-number>
            <card-token>{custPaymentInstrument.creditCardToken}</card-token>
            <card-holder>{custPaymentInstrument.creditCardHolder}</card-holder>
            <expiration-month>{custPaymentInstrument.creditCardExpirationMonth}</expiration-month>
            <expiration-year>{custPaymentInstrument.creditCardExpirationYear}</expiration-year>
            <custom-attributes>
              <custom-attribute attribute-id="lastModifiedCustom">{customerFromSFCC.custom.lastPaymentModified}</custom-attribute>
            </custom-attributes>
          </credit-card>
          <ek-id>-1</ek-id>
        </payment-instrument>
      );

      paymentInstrXML.appendChild(payment);
    }
    /*var lastModifiedCustom : XML = <custom-attributes>
            <custom-attribute attribute-id="lastModifiedCustom">{customerFromSFCC.custom.lastPaymentModified}</custom-attribute>
            </custom-attributes> ;
			paymentInstrXML.appendChild(lastModifiedCustom);*/

    if (hasCC) {
      if (headerXML == null) {
        fileWriter = new FileWriter(exportfile, 'UTF-8', true);
        fileWriter.writeLine('<?xml version="1.0" encoding="UTF-8"?>');
        var schemaURL = 'http://www.demandware.com/xml/impex/customerpaymentinstrument/2014-03-31';
        headerXML = <customers xmlns={schemaURL}></customers>;
      }

      child.appendChild(paymentInstrXML);
      headerXML.appendChild(child);
    }
  }

  if (headerXML != null) {
    fileWriter.write(headerXML.toXMLString());
  }
};

exports.afterChunk = function () {
  if (fileWriter != null) {
    fileWriter.close();
  }
};

exports.afterStep = function () {
  if (customObj != null) {
    Transaction.wrap(function () {
      customObj.custom.lastPaymentExportJobRunDate = new Date();
    });
  }
};

function getCreditCardType(type) {
  var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
  var cardType = type;
  try {
    var ccType = JSON.parse(ipaConstants.BM_CARD_TYPE_MAP);
    cardType = ccType['CCFormat']['BMFormat'][type];
  } catch (e) {
    cardType = type;
  }

  DeltaPaymentSyncLogs.info('Export payment instrument cardType' + cardType);
  return cardType;
}
