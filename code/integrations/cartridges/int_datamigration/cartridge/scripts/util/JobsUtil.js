var Status = require('dw/system/Status');
var io = require('dw/io');
var File = require('dw/io/File');
var Calendar = require('dw/util/Calendar');
var Transaction = require('dw/system/Transaction');
var Logger = require('dw/system/Logger');
var customer;
var profilesIter;
var exportFileName;
var destDir;
var folder;
var count = 0;
const IMPEX_PATH: 'LegacyData/Customer/';
const SRC_FOLDER: '/src/';
const DELIMITER: ',';
const EXTENSION: '.xml';
const FILE_NAME: 'profile_export_feed';
const ARCHIVE_FOLDER: 'archive';
const ARCHIVE_DAYS: 30;
/**
 *
 * Step 2: send generated file to sftp server and archive the file
 * @param {Object} args : Job Object
 * @param {dw.job.JobStepExecution} jobStepExecution : dw.job.JobStepExecution
 * @returns {dw.system.Status} Status : dw.system.Status
 */
exports.sftpUpload = function (args, jobStepExecution) {
  var folder = jobStepExecution.getParameterValue('folder') || IMPEX_PATH;
  var sourceDir = new File(File.IMPEX + SRC_FOLDER + folder);
  var feedName = jobStepExecution.getParameterValue('filename') || FILE_NAME;
  var sftpFolder = jobStepExecution.getParameterValue('SFTPLocation');
  var archiveFolder = jobStepExecution.getParameterValue('archiveFolder') || ARCHIVE_FOLDER;
  var collections = require('*/cartridge/scripts/util/collections');
  collections.forEach(sourceDir.listFiles(), function (file) {
    if (file.name.indexOf(feedName) !== -1) {
      // Upload to SFTP location
      var svc = profileSyncSFTPService.get();
      svc.setThrowOnError();
      svc.setOperation('putBinary', [sftpFolder + file.name, file]);
      svc.call();
      // archive the file after upload
      var archiveFileDir = new File(sourceDir.fullPath + File.SEPARATOR + archiveFolder);
      if (!archiveFileDir.exists()) {
        archiveFileDir.mkdirs();
      }
      var archiveFile = new File(archiveFileDir.fullPath + File.SEPARATOR + file.name);
      file.renameTo(archiveFile);
    }
  });
  return new Status(Status.OK);
};
/**
 *
 * Step 3: cleanup the files in archive folder which are certain number of days old
 * @param {Object} args : Job Object
 * @param {dw.job.JobStepExecution} jobStepExecution : dw.job.JobStepExecution
 * @returns {dw.system.Status} Status : dw.system.Status
 */
exports.cleanupArchive = function (args, jobStepExecution) {
  var folder = jobStepExecution.getParameterValue('folder') || IMPEX_PATH;
  var archiveFolder = jobStepExecution.getParameterValue('archiveFolder') || ARCHIVE_FOLDER;
  var daysToBeArchived = jobStepExecution.getParameterValue('daysToBeArchived') || ARCHIVE_DAYS;
  var archiveDir = new File(File.IMPEX + SRC_FOLDER + folder + File.SEPARATOR + archiveFolder);
  if (!archiveDir.exists()) {
    throw new Error('Folder Archive ' + archiveDir.fullPath + ' does not exist.');
  }
  var calendar = new Calendar();
  var System = require('dw/system/System');
  var oldCalendar = new Calendar();
  oldCalendar.add(Calendar.DAY_OF_YEAR, -1 * daysToBeArchived);
  var listFiles = archiveDir.listFiles();
  var collections = require('*/cartridge/scripts/util/collections');
  collections.forEach(listFiles, function (archiveFile) {
    var fileCalendar = new Calendar(new Date(archiveFile.lastModified()));
    if (fileCalendar.before(oldCalendar)) {
      archiveFile.remove();
    }
  });
  return new Status(Status.OK);
};
