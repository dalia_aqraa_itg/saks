'use strict';
/**
 * cart Sync Feed Export
 *
 *
 * Processes and exports a XML in chunk format. Progress can be seen in job run
 * status
 */
var Status = require('dw/system/Status');
var io = require('dw/io');
var File = require('dw/io/File');
var ProductListMgr = require('dw/customer/ProductListMgr');
var ProductList = require('dw/customer/ProductList');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var Logger = require('dw/system/Logger');
var Site = require('dw/system/Site');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var cartsIter;
var exportFileName;
var destDir;
var cartSyncFolder;
var count = 0;
var customObj;
const IMPEX_CART_SYNC_PATH: 'LegacyData/Cart/Outgoing';
const CART_SYNC_FILE_NAME: 'cart_export_feed';
var DeltaCartSyncLogs = Logger.getLogger('DeltaCartSync', 'DeltaCartSync');

/**
 * Initialize readers and writers for job processing
 *
 * @param {Object}
 *            parameters job parameters
 * @param {JobStepExecution}
 *            stepExecution job step execution
 * @returns {Status} if error returns status
 */
exports.beforeStep = function (parameters, stepExecution) {
  var cartSyncFileName = parameters.cartSyncFeedFileName ? parameters.cartSyncFeedFileName : CART_SYNC_FILE_NAME;
  cartSyncFolder = parameters.cartSyncFolder ? parameters.cartSyncFolder : IMPEX_CART_SYNC_PATH;
  destDir = new File(File.IMPEX + /src/ + cartSyncFolder);
  if (!destDir.exists()) {
    destDir.mkdirs();
  }
  var calendar = new Calendar();
  calendar.timeZone = 'GMT';
  var gmtDateString = StringUtils.formatCalendar(calendar, 'yyyy-MM-dd_HH-mm-ss');
  exportFileName = cartSyncFileName + '_' + gmtDateString;
  customObj = CustomObjectMgr.getCustomObject('LastCustomerExport', '100');
  var lastCartExportJobRunDate;
  if (customObj != null && customObj.custom.lastCartExportJobRunDate != null && customObj.custom.lastCartExportJobRunDate != '') {
    lastCartExportJobRunDate = customObj.custom.lastCartExportJobRunDate;
  } else {
    lastProfileExportJobRunDate = new Date();
  }
  cartsIter = ProductListMgr.queryProductLists(
    'lastModified > {0} and type = {1} and anonymous = false',
    null,
    lastCartExportJobRunDate,
    ProductList.TYPE_CUSTOM_1
  );
  DeltaCartSyncLogs.info('beforeStep cartsIter --->' + cartsIter.count);
  DeltaCartSyncLogs.info('beforeStep exportFileName --->' + exportFileName);
  return undefined;
};
exports.getTotalCount = function () {
  return cartsIter.count;
};
exports.read = function () {
  if (cartsIter.hasNext()) {
    return cartsIter.next();
  }
  return undefined;
};
/**
 * fetch  processed order
 * Process function in chunk oriented script module
 * @param {dw.customer.ProductList} cart
 * @returns {Array} returns all processed order
 */
exports.process = function (cart) {
  var lastImportedBlueMartini = cart.custom.lastImportedDate;
  var lastModifiedinSFCCCalendar = new Calendar(cart.getLastModified());
  var lastImportedBlueMartiniCalendar;
  if (lastImportedBlueMartini != null && lastImportedBlueMartini != '') {
    lastImportedBlueMartiniCalendar = new Calendar(new Date(lastImportedBlueMartini));
  } else {
    Transaction.wrap(function () {
      cart.custom.lastModifiedCustom = cart.getLastModified().toISOString(); // eslint-disable-line
    });
    return cart;
  }
  DeltaCartSyncLogs.info(
    'Export cart.getLastModified() > cart.custom.lastImportedDate   -->' + lastModifiedinSFCCCalendar.after(lastImportedBlueMartiniCalendar)
  );
  DeltaCartSyncLogs.info('Export lastImportedBlueMartiniCalendar  -->' + lastImportedBlueMartiniCalendar);
  DeltaCartSyncLogs.info('Export lastModifiedinSFCCCalendar  -->' + lastModifiedinSFCCCalendar);
  if (lastModifiedinSFCCCalendar.after(lastImportedBlueMartiniCalendar)) {
    Transaction.wrap(function () {
      cart.custom.lastModifiedCustom = cart.getLastModified().toISOString(); // eslint-disable-line
    });
    return cart;
  } else {
    return undefined;
  }
};
exports.write = function (lines, parameters, stepExecution) {
  count = count + 1;
  var filename = cartSyncFolder + Site.getCurrent().getID() + '-' + exportFileName + '-' + count + '.xml';
  var cartsList = new dw.system.Pipelet('ExportProductLists').execute({
    ExportFile: filename,
    OverwriteExportFile: true,
    ProductLists: lines.iterator()
  });
};

exports.afterChunk = function () {};

exports.afterStep = function () {
  if (customObj != null) {
    Transaction.wrap(function () {
      customObj.custom.lastCartExportJobRunDate = new Date();
    });
  }
};
