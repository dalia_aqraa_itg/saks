var Status = require('dw/system/Status');
var File = require('dw/io/File');

/**
 *
 * Step 2: Archive files
 * @param {Object} args : Job Object
 * @param {dw.job.JobStepExecution} jobStepExecution : dw.job.JobStepExecution
 * @returns {dw.system.Status} Status : dw.system.Status
 */
exports.archiveDeltaFiles = function (args, jobStepExecution) {
  var folder = jobStepExecution.getParameterValue('SourceFolder');
  var sourceDir = new File(File.IMPEX + '/src/' + folder);
  var feedName = jobStepExecution.getParameterValue('fileName');
  var archiveFolder = jobStepExecution.getParameterValue('archiveFolder');
  var collections = require('*/cartridge/scripts/util/collections');
  collections.forEach(sourceDir.listFiles(), function (file) {
    if (file.name.indexOf(feedName) !== -1) {
      // archive the files
      var archiveFileDir = new File(File.IMPEX + '/src/LegacyData/' + archiveFolder);
      if (!archiveFileDir.exists()) {
        archiveFileDir.mkdirs();
      }
      var archiveFile = new File(archiveFileDir.fullPath + File.SEPARATOR + file.name);
      file.renameTo(archiveFile);
    }
  });
  return new Status(Status.OK);
};
