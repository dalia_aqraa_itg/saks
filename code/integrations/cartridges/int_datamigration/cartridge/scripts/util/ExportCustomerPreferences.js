'use strict';

var Status = require('dw/system/Status');

var File = require('dw/io/File');
var CustomerMgr = require('dw/customer/CustomerMgr');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var Logger = require('dw/system/Logger');
var CustomerSyncLogs = Logger.getLogger('DeltaCustomerSync', 'DeltaCustomerSync');
var Transaction = require('dw/system/Transaction');
var FileWriter = require('dw/io/FileWriter');
var profilesIter;
var exportFileName;
var destDir;
var profileSyncFolder;
var exportXML = '';
var fileWriter;
var xmlWriter;

exports.beforeStep = function (parameters, stepExecution) {
  profilesIter = CustomerMgr.searchProfiles('custom.preferencesRecentlyUpdated = true', null);
  exportXML = <customers />;
  if (profilesIter.count == 0) {
    //Create Empty file
    writeFile(parameters);
  }
  return undefined;
};

exports.getTotalCount = function () {
  return profilesIter.count;
};

exports.read = function () {
  if (profilesIter.hasNext()) {
    return profilesIter.next();
  }
  return undefined;
};

exports.process = function (customer) {
  var customerId = customer.customerNo;
  var customerEmail = customer.email;

  CustomerSyncLogs.info(customerId);
  var categoryPreferences = customer.custom.CategoryPreferences;
  var designerPreferences = customer.custom.DesignerPreferences;
  var sizePreferences = customer.custom.SizePreferences;

  if (!designerPreferences) {
    designerPreferences = '""';
  }
  if (!categoryPreferences) {
    categoryPreferences = '""';
  }
  if (!sizePreferences) {
    sizePreferences = '""';
  }

  var customerXML = <customer />;

  //add customer ID to XML
  var customerNoXML = <customerno />;
  customerNoXML.appendChild(customerId);
  customerXML.appendChild(customerNoXML);

  //add customer email to XML
  var customerEmailXML = <email />;
  customerEmailXML.appendChild(customerEmail);
  customerXML.appendChild(customerEmailXML);

  //add designer preferences to XML
  var designerPreferencesXML = <DesignerPreferences />;
  designerPreferencesXML.appendChild(designerPreferences);
  customerXML.appendChild(designerPreferencesXML);

  //add category preferences to XML
  var categoryPreferencesXML = <CategoryPreferences />;
  categoryPreferencesXML.appendChild(categoryPreferences);
  customerXML.appendChild(categoryPreferencesXML);

  //add size preferences to XML
  var sizePreferencesXML = <sizePreferences />;
  sizePreferencesXML.appendChild(sizePreferences);
  customerXML.appendChild(sizePreferencesXML);

  //add current customer to the export xml file
  exportXML.appendChild(customerXML);

  //update the recently updated attribute
  Transaction.wrap(function () {
    customer.custom.preferencesRecentlyUpdated = false;
  });
};

exports.write = function (lines, parameters, stepExecution) {
  writeFile(parameters);
};
function writeFile(parameters) {
  var profileSyncFileName = parameters.profileSyncFeedFileName ? parameters.profileSyncFeedFileName : 'LegacyData/Customer/Outgoing';
  profileSyncFolder = parameters.profileSyncFolder ? parameters.profileSyncFolder : 'profile_export_feed';
  destDir = new File(File.IMPEX + /src/ + profileSyncFolder);
  if (!destDir.exists()) {
    destDir.mkdirs();
  }
  var calendar = new Calendar();
  calendar.timeZone = 'GMT';
  var gmtDateString = StringUtils.formatCalendar(calendar, 'yyyyMMddhhmmss');
  exportFileName = profileSyncFileName + '_' + gmtDateString;
  var filename = profileSyncFolder + exportFileName + '.xml';
  var exportfile: File = new File(File.IMPEX + /src/ + filename);
  fileWriter = new FileWriter(exportfile, 'UTF-8', true);
  if (exportXML == '') {
    exportXML = "<?xml version='1.0' encoding='UTF-8'?><customers></customers>";
  }
  fileWriter.write(exportXML);
  fileWriter.close();
}

exports.afterChunk = function () {};

exports.afterStep = function () {
  exportXML = '';
};
