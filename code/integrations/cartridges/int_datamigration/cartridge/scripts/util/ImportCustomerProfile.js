'use strict';
var Logger = require('dw/system/Logger');
var CustomerMgr = require('dw/customer/CustomerMgr');
var File = require('dw/io/File');
var FileReader = require('dw/io/FileReader');
var FileWriter = require('dw/io/FileWriter');
var XMLStreamReader = require('dw/io/XMLStreamReader');
var XMLStreamConstants = require('dw/io/XMLStreamConstants');
var ProductMgr = require('dw/catalog/ProductMgr');
var Logger = require('dw/system/Logger');
var Status = require('dw/system/Status');
var Calendar = require('dw/util/Calendar');
var DeltaCustomerSyncLogs = Logger.getLogger("DeltaCustomerSync", "DeltaCustomerSync");
var Site = require('dw/system/Site');
const IMPEX_PROFILE_SYNC_PATH: 'LegacyData/Customer/Incoming';
function importCustomerProfile(args, jobStepExecution){
	try {
		var profileSyncFolder = args.profileSyncFolder ? args.profileSyncFolder : IMPEX_PROFILE_SYNC_PATH;
		var profileSyncFeedFilePattern = args.profileSyncFeedFilePattern;
		if ( empty(profileSyncFolder) || empty(profileSyncFeedFilePattern)) {
			return new Status(Status.ERROR, 'ERROR', 'One or more mandatory parameters are missing.  profileSyncFolder = (' + profileSyncFolder + ') profileSyncFeedFilePattern = (' + profileSyncFeedFilePattern + ')');
		}
		var importfile : File = new File(File.IMPEX + /src/ + profileSyncFolder);
		if (!importfile.exists()) {
			DeltaCustomerSyncLogs.error('FAILED: File not found. File Name: ' + importfile.fullPath);
			return Status.ERROR;
		}
		importfile = importfile.listFiles();
		// If no files are found, go to the next locale
		if(!importfile) {
			DeltaCustomerSyncLogs.info("NO FILES FOUND");
		}
		for each(var file in importfile) {
			if(file.getName().match(profileSyncFeedFilePattern) == null || file.isDirectory() ) {
				continue;
			} else {
			var fileReader : FileReader = new FileReader(file, "UTF-8");
			var xmlStreamReader : XMLStreamReader = new XMLStreamReader(fileReader);
			var exportfile : File = new File(File.IMPEX + /src/ + profileSyncFolder +  "TEMP"+ File.SEPARATOR + "TEMP-" +file.name);
			var destDir = new File(File.IMPEX + /src/ + profileSyncFolder+"TEMP"+ '/');
			if (!destDir.exists()) {
				destDir.mkdirs();
			}
			var fileWriter ;
			var headerXML : XML  = null ;
			while (xmlStreamReader.hasNext()) {
				var element = xmlStreamReader.next();	
				if (element == XMLStreamConstants.START_ELEMENT) {
					var localElementName : String = xmlStreamReader.getLocalName();		
					if (localElementName == "customers") {
						var xmlChildren = xmlStreamReader.readXMLObject().children();
						for each(var child : Object in xmlChildren) {						
							var localName = child.localName();
							if (child.localName() != null && localName == 'customer') {
								DeltaCustomerSyncLogs.info("Import Customer-no -->" + child.attribute('customer-no'));
								var customerFromSFCC = CustomerMgr.getProfile(child.attribute('customer-no'));
								var lastModifiedinSFCC ;
								var lastModifiedinSFCCCalendar;
								if(customerFromSFCC != null) 
								{   
									lastModifiedinSFCC = customerFromSFCC.getLastModified();
									lastModifiedinSFCCCalendar = new Calendar(lastModifiedinSFCC);
								} 
									var lastModifiedinBlueMartini ;
									for each(var child2 : Object in child.children()) {
										var localName2 = child2.localName();
										if (child2.localName() != null && localName2 == 'profile') {
											for each(var child3 : Object in child2.children()) { 		
												if (child3.localName() != null && child3.localName() == 'custom-attributes'){		
													for each(var child4 : Object in child3.children()) { 	
														if (child4.localName() != null && child4.localName() == 'custom-attribute' 
															&& child4.attribute('attribute-id') == 'accountModifiedDate'){														
															lastModifiedinBlueMartini = new Date(child4.valueOf());
															var lastModifiedinBlueMartiniCalendar = new Calendar(lastModifiedinBlueMartini);
															DeltaCustomerSyncLogs.info("Import lastModifiedinBlueMartini--->" + lastModifiedinBlueMartini);
															DeltaCustomerSyncLogs.info("Import lastModifiedinSFCC--->" + lastModifiedinSFCC);
															//Logger.debug("login compare--->" + lastModifiedinBlueMartini > lastModifiedinSFCC );
															if(customerFromSFCC == null || lastModifiedinBlueMartiniCalendar.after(lastModifiedinSFCCCalendar)){
																DeltaCustomerSyncLogs.info("Import lastModifiedinBlueMartiniCalendar After SFCC");
																var lastImportedDate = new Date().toISOString(); 
																var lastImportedDateXML : XML = <custom-attribute attribute-id="lastImportedDate">{lastImportedDate}</custom-attribute> ;
																child3.appendChild(lastImportedDateXML);				
																if(headerXML == null){
																	 fileWriter = new FileWriter(exportfile, "UTF-8", true);
																	var schemaURL = "http://www.demandware.com/xml/impex/customer/2006-10-31";			
																	headerXML  = <customers xmlns={schemaURL}></customers> ;
																}
																headerXML.appendChild(child);
															}
														}
													}
												}
											}
										} 
									}
								
							} 
						}
					}
				}
			} 
			if (headerXML != null)			
			{
				fileWriter.write(headerXML.toXMLString());
			}
			if (fileWriter != null) {
				fileWriter.close();
			}

			var archiveFolder = jobStepExecution.getParameterValue('archiveFolder');
			var archiveFileDir = new File(File.IMPEX + '/src/LegacyData/'+ archiveFolder);
			if (!archiveFileDir.exists()) {
				archiveFileDir.mkdirs();
			}
			var archiveFile = new File(archiveFileDir.fullPath + File.SEPARATOR + file.name);
			file.renameTo(archiveFile);
			}
			}
	} finally {
		if (xmlStreamReader != null) {
			xmlStreamReader.close();
		}
		if (fileReader != null) {
			fileReader.close();
		}
		if (fileWriter != null) {
			fileWriter.close();
		}
	}
	return new Status(Status.OK);
}
exports.importCustomerProfile = importCustomerProfile;