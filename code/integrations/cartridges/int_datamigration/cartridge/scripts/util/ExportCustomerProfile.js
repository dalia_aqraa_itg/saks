'use strict';

var Status = require('dw/system/Status');

var File = require('dw/io/File');
var CustomerMgr = require('dw/customer/CustomerMgr');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var Logger = require('dw/system/Logger');
var Site = require('dw/system/Site');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var DeltaCustomerSyncLogs = Logger.getLogger('DeltaCustomerSync', 'DeltaCustomerSync');

var profilesIter;
var exportFileName;
var destDir;
var profileSyncFolder;
var count = 0;
var customObj;

exports.beforeStep = function (parameters, stepExecution) {
  var profileSyncFileName = parameters.profileSyncFeedFileName ? parameters.profileSyncFeedFileName : 'LegacyData/Customer/Outgoing';
  profileSyncFolder = parameters.profileSyncFolder ? parameters.profileSyncFolder : 'profile_export_feed';
  destDir = new File(File.IMPEX + /src/ + profileSyncFolder);
  if (!destDir.exists()) {
    destDir.mkdirs();
  }
  var calendar = new Calendar();
  calendar.timeZone = 'GMT';
  var gmtDateString = StringUtils.formatCalendar(calendar, 'yyyy-MM-dd_HH-mm-ss');
  exportFileName = profileSyncFileName + '_' + gmtDateString;
  var lastProfileExportJobRunDate;
  customObj = CustomObjectMgr.getCustomObject('LastCustomerExport', '100');

  if (customObj != null && customObj.custom.lastProfileExportJobRunDate != null && customObj.custom.lastProfileExportJobRunDate != '') {
    lastProfileExportJobRunDate = customObj.custom.lastProfileExportJobRunDate;
  } else {
    lastProfileExportJobRunDate = new Date();
  }
  profilesIter = CustomerMgr.queryProfiles('lastModified > {0}', null, lastProfileExportJobRunDate);

  return undefined;
};

exports.getTotalCount = function () {
  return profilesIter.count;
};

exports.read = function () {
  if (profilesIter.hasNext()) {
    return profilesIter.next();
  }
  return undefined;
};

exports.process = function (customer) {
  var lastImportedBlueMartiniCalendar;
  var lastModifiedinSFCCCalendar;
  var lastImportedBlueMartini = customer.custom.lastImportedDate;
  var lastModifiedSFCC = customer.custom.accountModifiedDate;
  var jobDate;
  var jobDateCal;

  var lastModifiedSystemSFCC = new Calendar(customer.getLastModified());

  if (lastModifiedSFCC !== null && lastModifiedSFCC !== '') {
    lastModifiedinSFCCCalendar = new Calendar(new Date(lastModifiedSFCC));
    lastModifiedinSFCCCalendar.add(Calendar.SECOND, 3);
  }

  var custObj = CustomObjectMgr.getCustomObject('LastCustomerExport', '100');

  if (customObj != null && customObj.custom.lastProfileExportJobRunDate != null && customObj.custom.lastProfileExportJobRunDate != '') {
    jobDate = customObj.custom.lastProfileExportJobRunDate;
    jobDateCal = new Calendar(jobDate);
  }

  DeltaCustomerSyncLogs.info('Export customerNo -> ' + customer.customerNo);
  if (!empty(lastModifiedinSFCCCalendar)) {
    if (
      lastModifiedinSFCCCalendar.after(lastModifiedSystemSFCC) ||
      lastModifiedinSFCCCalendar.equals(lastModifiedSystemSFCC) ||
      lastModifiedinSFCCCalendar.after(jobDateCal)
    ) {
      DeltaCustomerSyncLogs.info(
        'Export System date greater then Custom ->' +
          lastModifiedinSFCCCalendar.after(lastModifiedSystemSFCC) +
          ' System date equals Custom -> ' +
          lastModifiedinSFCCCalendar.equals(lastModifiedSystemSFCC)
      );

      if (lastImportedBlueMartini != null && lastImportedBlueMartini != '') {
        lastImportedBlueMartiniCalendar = new Calendar(new Date(lastImportedBlueMartini));
      } else {
        return customer;
      }

      DeltaCustomerSyncLogs.info(
        'Export lastImportedBlueMartini -> ' + new Date(lastImportedBlueMartini) + ' lastModifiedinSFCC -> ' + new Date(lastModifiedSFCC)
      );
      DeltaCustomerSyncLogs.info('Export SFCC date greater then BlueMartini ->' + lastModifiedinSFCCCalendar.after(lastImportedBlueMartiniCalendar));

      if (lastModifiedinSFCCCalendar.after(lastImportedBlueMartiniCalendar)) {
        return customer;
      } else {
        return undefined;
      }
    } else {
      return undefined;
    }
  } else {
    return undefined;
  }
};

exports.write = function (lines, parameters, stepExecution) {
  count = count + 1;
  var filename = profileSyncFolder + Site.getCurrent().getID() + '-' + exportFileName + '-' + count + '.xml';
  var profilesList = new dw.system.Pipelet('ExportCustomers').execute({
    ExportFile: filename,
    OverwriteExportFile: true,
    Customers: lines.iterator()
  });
};

exports.afterChunk = function () {};

exports.afterStep = function () {
  if (customObj != null) {
    Transaction.wrap(function () {
      customObj.custom.lastProfileExportJobRunDate = new Date();
    });
  }
};
