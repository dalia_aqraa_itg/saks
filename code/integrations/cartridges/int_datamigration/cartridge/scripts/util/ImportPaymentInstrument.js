'use strict';


var Logger = require('dw/system/Logger');
var CustomerMgr = require('dw/customer/CustomerMgr');
var Profile = require('dw/customer/Profile');
var File = require('dw/io/File');
var FileReader = require('dw/io/FileReader');
var XMLStreamReader = require('dw/io/XMLStreamReader');
var XMLStreamConstants = require('dw/io/XMLStreamConstants');
var ProductMgr = require('dw/catalog/ProductMgr');
var Logger = require('dw/system/Logger');
var Status = require('dw/system/Status');
var Calendar = require('dw/util/Calendar');
var Site = require('dw/system/Site');
var DeltaPaymentSyncLogs = Logger.getLogger("DeltaPaymentSync", "DeltaPaymentSync");

const IMPEX_PROFILE_SYNC_PATH: 'LegacyData/Payment/Incoming';

function importPaymentInstrument(args, jobStepExecution){

	try {

		var profileSyncFolder = args.profileSyncFolder ? args.profileSyncFolder : IMPEX_PROFILE_SYNC_PATH;

		var profileSyncFeedFilePattern = args.profileSyncFeedFilePattern;
		
		if ( empty(profileSyncFolder) || empty(profileSyncFeedFilePattern)) {
			return new Status(Status.ERROR, 'ERROR', 'One or more mandatory parameters are missing.  profileSyncFolder = (' + profileSyncFolder + ') profileSyncFeedFilePattern = (' + profileSyncFeedFilePattern + ')');
		}

		
		var importfile : File = new File(File.IMPEX + File.SEPARATOR + "src" + File.SEPARATOR 
				+ profileSyncFolder);
		if (!importfile.exists()) {
			DeltaPaymentSyncLogs.error('FAILED: File not found. File Name: ' + importfile.fullPath );
			return Status.ERROR;
		}
		importfile = importfile.listFiles();
		
		// If no files are found, go to the next locale
		if(!importfile) {
			DeltaPaymentSyncLogs.info("NO FILES FOUND");
		}
		

		for each(var file in importfile) {
			if(file.getName().match(profileSyncFeedFilePattern) == null || file.isDirectory()) {
				continue;
			} else {
			DeltaPaymentSyncLogs.info("Import File Name -->" + file.getName());
			var fileReader : FileReader = new FileReader(file, "UTF-8");
			var xmlStreamReader : XMLStreamReader = new XMLStreamReader(fileReader);
			var headerXML : XML  = null ;


			while (xmlStreamReader.hasNext()) {
				var element = xmlStreamReader.next();	
				if (element == XMLStreamConstants.START_ELEMENT) {
		
		
					var localElementName : String = xmlStreamReader.getLocalName();		
					if (localElementName == "customers") {
						var xmlChildren = xmlStreamReader.readXMLObject().children();
	
						for each(var child : Object in xmlChildren) {						
							var localName = child.localName();
			
							if (child.localName() != null && localName == 'customer') {
							
								var customerFromSFCC = CustomerMgr.getProfile(child.attribute('customer-no'));
								
								if(customerFromSFCC != null) 
								{
									
									DeltaPaymentSyncLogs.info("Import Customer Number ==>" + child.attribute('customer-no'));
									var walletCust = customerFromSFCC.getWallet();	
									var walletCustCollection = walletCust.getPaymentInstruments() ;
									var walletCustIter = walletCustCollection.iterator() ;
									var txn = require('dw/system/Transaction');
									while (walletCustIter.hasNext()) {
										var custPaymentInstrument = walletCustIter.next();
										txn.wrap(function(){
											 walletCust.removePaymentInstrument(custPaymentInstrument) ;
												 });
									}
									 
									for each(var child2 : Object in child.children()) {
										var localName2 = child2.localName();
				
										if (child2.localName() != null && localName2 == 'payment-instruments') {
											//Logger.debug("payment-instruments");
											for each(var child3 : Object in child2.children()) { 		
												if (child3.localName() != null && child3.localName() == 'payment-instrument'){		
													DeltaPaymentSyncLogs.info("importing payment-instrument");
													var cctype ="";
													var ccnumber = "";
													var cctoken = "";
													var ccholder ="";
													var ccexpmonth="";
													var ccexpyear="";
													var isDefaultCard='false';
													
													for each(var child4 : Object in child3.children()) {
														if (child4.localName() != null && child4.localName() == 'custom-attributes'){	
															for each(var child6 : Object in child4.children()) {
																if (child6.localName() != null && child6.localName() == 'custom-attribute' 
																&& child6.attribute('attribute-id') == 'defaultCreditCard'){			
																	isDefaultCard = child6.valueOf().toString();
																}
															}
														}

														if(child4.localName() == "credit-card")
														{
															for each(var child5 : Object in child4.children()) {
																
																 if (child5.localName() != null && child5.localName() == 'card-type')
																	{ 
																	 cctype = child5.valueOf();
																	 cctype = getCreditCardType(cctype);
																	
																	}
																 if (child5.localName() != null && child5.localName() == 'card-number')
																	{
																	 ccnumber = child5.valueOf();
																	}
																 if (child5.localName() != null && child5.localName() == 'card-token')
																	{
																	 cctoken = child5.valueOf();
																	}
																 if (child5.localName() != null && child5.localName() == 'card-holder')
																	{
																	 ccholder = child5.valueOf();
																	}
																 if (child5.localName() != null && child5.localName() == 'expiration-month')
																	{
																	 ccexpmonth = parseInt(child5.valueOf());
																	}
																 if (child5.localName() != null && child5.localName() == 'expiration-year')
																	{
																	 ccexpyear = parseInt(child5.valueOf());
																	}
															}
															txn.wrap(function(){
																 var PaymentInstr = walletCust.createPaymentInstrument("CREDIT_CARD") ;
																 		PaymentInstr.setCreditCardHolder(ccholder);
																 		PaymentInstr.setCreditCardNumber(ccnumber);
																	    PaymentInstr.setCreditCardType(cctype);
																	    PaymentInstr.setCreditCardExpirationMonth(ccexpmonth);
																	    PaymentInstr.setCreditCardExpirationYear(ccexpyear);
																		PaymentInstr.setCreditCardToken(cctoken);
																		if (isDefaultCard === 'true') {
																			PaymentInstr.custom.defaultCreditCard = true;
																		} else {
																			PaymentInstr.custom.defaultCreditCard = false;
																		}
																		DeltaPaymentSyncLogs.info("isDefaultCard ==>" + PaymentInstr.custom.defaultCreditCard);
																});
														}
													}
												}
												
											}
										} 
									}
									
									//Logger.debug("customerFromSFCC.profile.getLastModified()"  + customerFromSFCC.getLastModified());
								} else {
									DeltaPaymentSyncLogs.info("Import Customer not found ->" + child.attribute('customer-no'));
								}
							} 
						}
					}
				}
			} 
			
			}

			var archiveFolder = jobStepExecution.getParameterValue('archiveFolder');
			var archiveFileDir = new File(File.IMPEX + '/src/LegacyData/'+ archiveFolder);
			if (!archiveFileDir.exists()) {
				archiveFileDir.mkdirs();
			}
			var archiveFile = new File(archiveFileDir.fullPath + File.SEPARATOR + file.name);
			file.renameTo(archiveFile);

			}
	} finally {
		if (xmlStreamReader != null) {
			xmlStreamReader.close();
		}

		if (fileReader != null) {
			fileReader.close();
		}
	}


	return new Status(Status.OK);
}	


function getCreditCardType(type){
	var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');

	   var cardType = type;
	    try {
	        var ccType = JSON.parse(ipaConstants.BM_CARD_TYPE_MAP);
	        cardType = ccType["CCFormat"]["SFCCFormat"][type];
	   } catch (e) {
	        cardType = type;
	   }
	   DeltaPaymentSyncLogs.info("Import Card Type ==>" + cardType);
	    return cardType;
	
}


exports.importPaymentInstrument = importPaymentInstrument;