'use strict';
/**
 * wishlist Sync Feed Export
 *
 *
 * Processes and exports a XML in chunk format. Progress can be seen in job run
 * status
 */
var Status = require('dw/system/Status');
var io = require('dw/io');
var File = require('dw/io/File');
var ProductListMgr = require('dw/customer/ProductListMgr');
var ProductList = require('dw/customer/ProductList');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var Logger = require('dw/system/Logger');
var Site = require('dw/system/Site');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var wishlistsIter;
var exportFileName;
var destDir;
var wishlistSyncFolder;
var count = 0;
var customObj;
const IMPEX_WISHLIST_SYNC_PATH: 'LegacyData/Wishlist/Outgoing';
const WISHLIST_SYNC_FILE_NAME: 'wishlist_export_feed';
var DeltaWishlistSyncLogs = Logger.getLogger('DeltaWishlistSync', 'DeltaWishlistSync');

/**
 * Initialize readers and writers for job processing
 *
 * @param {Object}
 *            parameters job parameters
 * @param {JobStepExecution}
 *            stepExecution job step execution
 * @returns {Status} if error returns status
 */
exports.beforeStep = function (parameters, stepExecution) {
  var wishlistSyncFileName = parameters.wishlistSyncFeedFileName ? parameters.wishlistSyncFeedFileName : WISHLIST_SYNC_FILE_NAME;
  wishlistSyncFolder = parameters.wishlistSyncFolder ? parameters.wishlistSyncFolder : IMPEX_WISHLIST_SYNC_PATH;

  destDir = new File(File.IMPEX + /src/ + wishlistSyncFolder);
  if (!destDir.exists()) {
    destDir.mkdirs();
  }
  var calendar = new Calendar();
  calendar.timeZone = 'GMT';
  var gmtDateString = StringUtils.formatCalendar(calendar, 'yyyy-MM-dd_HH-mm-ss');
  exportFileName = wishlistSyncFileName + '_' + gmtDateString;
  customObj = CustomObjectMgr.getCustomObject('LastCustomerExport', '100');
  var lastWishlistExportJobRunDate;
  if (customObj != null && customObj.custom.lastWishlistExportJobRunDate != null && customObj.custom.lastWishlistExportJobRunDate != '') {
    lastWishlistExportJobRunDate = customObj.custom.lastWishlistExportJobRunDate;
  } else {
    lastProfileExportJobRunDate = new Date();
  }
  wishlistsIter = ProductListMgr.queryProductLists(
    'lastModified > {0} and type = {1} and anonymous = false ',
    null,
    lastWishlistExportJobRunDate,
    ProductList.TYPE_WISH_LIST
  );
  return undefined;
};
exports.getTotalCount = function () {
  return wishlistsIter.count;
};
exports.read = function () {
  if (wishlistsIter.hasNext()) {
    return wishlistsIter.next();
  }
  return undefined;
};
/**
 * fetch  processed order
 * Process function in chunk oriented script module
 * @param {dw.customer.ProductList} wishlist
 * @returns {Array} returns all processed order
 */
exports.process = function (wishlist) {
  var lastImportedBlueMartini = wishlist.custom.lastImportedDate;
  var lastModifiedinSFCCCalendar = new Calendar(wishlist.getLastModified());
  var lastImportedBlueMartiniCalendar;
  if (lastImportedBlueMartini != null && lastImportedBlueMartini != '') {
    lastImportedBlueMartiniCalendar = new Calendar(new Date(lastImportedBlueMartini));
  } else {
    Transaction.wrap(function () {
      wishlist.custom.lastModifiedCustom = wishlist.getLastModified().toISOString(); // eslint-disable-line
    });
    if (wishlist.items) {
      if (wishlist.items.length > 0) {
        return wishlist;
      } else {
        return undefined;
      }
    }
  }
  DeltaWishlistSyncLogs.info(
    'Export wishlist.getLastModified() > wishlist.custom.lastImportedDate   -->' + lastModifiedinSFCCCalendar.after(lastImportedBlueMartiniCalendar)
  );

  if (lastModifiedinSFCCCalendar.after(lastImportedBlueMartiniCalendar)) {
    Transaction.wrap(function () {
      wishlist.custom.lastModifiedCustom = wishlist.getLastModified().toISOString(); // eslint-disable-line
    });

    if (wishlist.items) {
      if (wishlist.items.length > 0) {
        return wishlist;
      } else {
        return undefined;
      }
    }
  } else {
    return undefined;
  }
};
exports.write = function (lines, parameters, stepExecution) {
  count = count + 1;
  var filename = wishlistSyncFolder + Site.getCurrent().getID() + '-' + exportFileName + '-' + count + '.xml';
  DeltaWishlistSyncLogs.info('write exportFileName  -->' + filename);
  var wishlistsList = new dw.system.Pipelet('ExportProductLists').execute({
    ExportFile: filename,
    OverwriteExportFile: true,
    ProductLists: lines.iterator()
  });
};

exports.afterChunk = function () {};

exports.afterStep = function () {
  if (customObj != null) {
    Transaction.wrap(function () {
      customObj.custom.lastWishlistExportJobRunDate = new Date();
    });
  }
};
