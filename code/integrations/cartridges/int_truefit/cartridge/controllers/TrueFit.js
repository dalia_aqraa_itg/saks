/**
 * Description of the Controller and the logic it provides
 *
 * @module  controllers/TrueFit
 */
'use strict';

// HINT: do not put all require statements at the top of the file
// unless you really need them for all functions
var server = require('server');
var ProductMgr = require('dw/catalog/ProductMgr');

/**
 * TfcLibraries - renders/includes the true fit libraries.
 */
server.get('TfcLibraries', function (req, res, next) {
  res.render('truefit/tfcLibraries');
  next();
});

server.get('TfcCachedPage', function (req, res, next) {
  res.render('truefit/tfcCachedPage');
  next();
});

/**
 * TfcFitrecProduct - this function is used to get the truefit integration on PDP
 * The required parameters would be retrieved from URL
 * @pid and @selectedColor
 */
server.get('TfcFitrecProduct', function (req, res, next) {
  var Product = ProductMgr.getProduct(req.querystring.pid);
  res.render('truefit/tfcFitrecProduct', {
    Product: Product,
    customer: req.currentCustomer.raw,
    locale: req.locale
  });
  next();
});

/**
 * TfcFitrecQuickView - this function is used to get the truefit integration on QuickView
 * The required parameters would be retrieved from URL
 * @pid and @selectedColor
 */
server.get('TfcFitrecQuickView', function (req, res, next) {
  var Product = ProductMgr.getProduct(req.querystring.pid);
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

  var tfqvdata = renderTemplateHelper.getRenderedHtml(
    {
      Product: Product,
      customer: req.currentCustomer.raw,
      locale: req.locale
    },
    'truefit/tfcFitrecQuickView'
  );

  res.json({
    renderedTemplate: tfqvdata
  });
  next();
});

module.exports = server.exports();
