/**
 * True Fit Utility functions
 *
 */
var ProductMgr = require('dw/catalog/ProductMgr');

var TrueFitUtils = {
  /**
   * Returns the Product Line Items in an order
   */
  getProductLineItems: function (pdict) {
    var productLineItems = [];
    if (pdict.Order != null) {
      productLineItems = pdict.Order.allProductLineItems;
    }
    return productLineItems;
  },

  /**
   * Returns the Demandware product ID associated with a line item product
   */
  getLineItemProductId: function (product) {
    var productId = '';
    if (product != null) {
      productId = product.product.masterProduct.ID;
    }
    return productId;
  },

  /**
   * Returns the quantity of a line item product
   */
  getLineItemQuantity: function (product) {
    var productQuantity = '';
    if (product != null) {
      productQuantity = product.quantityValue;
    }
    return productQuantity;
  },

  /**
   * Returns the SKU of a line item product
   */
  getLineItemSku: function (product) {
    var productSKU = '';
    if (product != null) {
      productSKU = product.product.ID;
    }
    return productSKU;
  },

  /**
   * Returns the price of a line item product
   */
  getLineItemPrice: function (product) {
    var productPrice = '';
    if (product != null) {
      productPrice = product.getBasePrice().value;
    }
    return productPrice;
  },

  /**
   * Returns the size of a line item product
   */
  getLineItemSize: function (product) {
    var productSize = '';
    if (product.product.variant) {
      var pvm = product.product.getVariationModel();
      var vaList = pvm.getProductVariationAttributes();
      var vaIter = vaList.iterator();
      while (vaIter.hasNext()) {
        var va = vaIter.next();
        if (va.getID().toLowerCase() == 'size') {
          var size = pvm.getSelectedValue(va);
          if (size != null) {
            productSize = size.getDisplayValue();
          }
        }
      }
    }
    return productSize;
  },

  /**
   * Returns the color of the current request
   */
  getLineItemColor: function (product) {
    var productColor = '';
    if (product.product.variant) {
      var pvm = product.product.getVariationModel();
      var vaList = pvm.getProductVariationAttributes();
      var vaIter = vaList.iterator();
      while (vaIter.hasNext()) {
        var va = vaIter.next();
        if (va.getID().toLowerCase() == 'color') {
          var color = pvm.getSelectedValue(va);
          if (color != null) {
            productColor = color.getDisplayValue();
          }
        }
      }
    }
    return productColor;
  },

  /**
   * Returns the Demandware product ID associated with the current request.
   */
  getProductId: function (pdict) {
    var productId = '';
    /*if (pdict.product.id != null) {
			if (pdict.product.productType == 'variant') {
				var masterProd = ProductMgr.getProduct(pdict.product.id).getVariationModel().getMaster();
				productId = masterProd.getID();
			} else {
				productId = pdict.product.id;
			}
		}*/

    if (pdict.Product != null) {
      if (pdict.Product.variant) {
        productId = pdict.Product.masterProduct.ID;
      } else {
        productId = pdict.Product.ID;
      }
    }
    return productId;
  },

  /**
   * Returns the currency of the current request
   */
  getCurrency: function (pdict) {
    var productCurrency = '';
    if (pdict.Order != null) {
      productCurrency = pdict.Order.currentOrder.currencyCode;
    }
    return productCurrency;
  },

  /**
   * Returns the locale of the current request
   */
  getDataLocaleAttribute: function (pdict) {
    var dataLocaleAttribute = '';
    var trueFitDataLocale = dw.system.Site.current.preferences.custom.TrueFitDataLocale;
    // if we didn't get a locale from the site preferences, then use the locale of the current request
    if (trueFitDataLocale == null || trueFitDataLocale == '') {
      trueFitDataLocale = pdict.locale.id;
    }
    // if we got a data locale from any source, then set up the attribute for the True Fit widget tag
    if (trueFitDataLocale != null && trueFitDataLocale != '' && trueFitDataLocale != 'default') {
      dataLocaleAttribute = "data-locale='" + trueFitDataLocale + "'";
    }
    return dataLocaleAttribute;
  },

  /**
   * Returns the a boolean (true|false) indicating whether or not the current user is registered
   */
  getCustomerRegistered: function (pdict) {
    var registered = false;
    if (pdict.CurrentCustomer != null) {
      registered = pdict.CurrentCustomer.registered;
    }
    return registered;
  },

  /**
   * Returns the Demandware orderNo associated with the current request.
   * Returns the empty string if no orderNo is available
   */
  getOrderNo: function (pdict) {
    var orderNo = '';
    if (pdict.Order != null) {
      orderNo = pdict.Order.orderNo;
    }
    return orderNo;
  },

  /**
   * Returns the Demandware customerNo associated with the current request.
   * Returns the empty string if no customerNo is available
   */
  getCustomerNo: function (pdict) {
    var customerNo = '';
    if (pdict.order != null) {
      customerNo = pdict.order.customerNo;
    } else if (pdict.customer != null && pdict.customer.authenticated && pdict.customer.profile != null) {
      customerNo = pdict.customer.profile.customerNo;
    }
    return customerNo;
  }
};

// Helper method to export the helper

module.exports = TrueFitUtils;
