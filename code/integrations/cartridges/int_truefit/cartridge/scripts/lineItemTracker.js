'use strict';

/**
 *   @output tfProducts : Array
 *
 */

importScript('int_truefit:/lib/trueFitUtils.ds');

function execute(pdict) {
  pdict.products = getProducts();
  return PIPELET_NEXT;
}

function getProducts(pdict: PipelineDictionary) {
  var products = getTrueFitUtils().getProductLineItems(pdict);
  var tfProducts = new Array();
  for (var i = 0; i < products.length; i++) {
    var tfProductInfo = {
      productId: getTrueFitUtils().getLineItemProductId(products[i]),
      colorId: getTrueFitUtils().getLineItemColor(products[i]),
      quantity: getTrueFitUtils().getLineItemQuantity(products[i]),
      price: getTrueFitUtils().getLineItemPrice(products[i]),
      currency: getTrueFitUtils().getCurrency(pdict),
      size: getTrueFitUtils().getLineItemSize(products[i]),
      sku: getTrueFitUtils().getLineItemSku(products[i])
    };
    tfProducts.push(tfProductInfo);
  }
  return tfProducts;
}

module.exports = {
  getProducts: getProducts
};
