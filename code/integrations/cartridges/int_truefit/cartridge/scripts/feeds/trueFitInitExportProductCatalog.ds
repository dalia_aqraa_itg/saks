/**
* trueFitInitExportProductCatalog.ds
*
* Chunks the product catalog entries into groups of 100 and creates a temp file for processing. 
*
* @output TempCatalogFile : String
* @output ProductGroups : dw.util.Collection
* @output Message : String
*/
importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.catalog );
importPackage( dw.customer );
importPackage( dw.io );
importPackage( dw.net );
importPackage( dw.object );
importPackage( dw.campaign );
importPackage( dw.web );

importScript( "int_truefit:/lib/trueFitExportPCUtils.ds" );

// Initializes the temp file and creates the product groups for export 
function execute( pdict : PipelineDictionary ) : Number
{
    var debugMode = getExportPCUtils().debugMode();
 	if (debugMode) {
		Logger.debug("START");
 	}
	var site : Site = Site.getCurrent();
	var productIterator : SeekableIterator = ProductMgr.queryAllSiteProducts();
	var productCount = productIterator.getCount();
	productIterator.close();
	if (debugMode) {
		Logger.debug("FOUND " + productCount + " PRODUCTS");
	}
	var productGroups: ArrayList = new ArrayList();
	var startIndex = 0;
	while(startIndex < productCount) {
		productGroups.add1(startIndex);
		startIndex = startIndex + 500;
	}
    pdict.ProductGroups = productGroups;

    var date : Date = new Date();
    var cal : Calendar = new Calendar(date)
	var filename : String = "Test-TrueFit-Catalog-" + site.getName() + "-" + StringUtils.formatCalendar(cal, 'yyyyMMdd') + ".txt";
    var localFilePath : String = File.TEMP + File.SEPARATOR + filename;
    var file : File = new File(localFilePath);
    var fileWriter : FileWriter = new FileWriter(file, false);
	var csvWriter : CSVStreamWriter = new CSVStreamWriter(fileWriter, ExportPCConstants.DELIMITER);
	
	writeHeader(csvWriter);

	csvWriter.close();
    fileWriter.close();
    
    pdict.TempCatalogFile = localFilePath;

	return PIPELET_NEXT;
}

// write the header row to the provided csv stream
function writeHeader(writer : CSVStreamWriter) {
	var headers = [];
	headers[ExportPCConstants.PRODUCT_ID_FIELD] = 'product_id'; 
	headers[ExportPCConstants.TITLE_FIELD] = 'title';
	headers[ExportPCConstants.SHORT_DESC_FIELD] = 'short_description';
	headers[ExportPCConstants.LONG_DESC_FIELD] = 'long_description';
	headers[ExportPCConstants.MANUFACTURER_FIELD] = 'brand';
	headers[ExportPCConstants.MANUFACTURER_PRODUCT_FIELD] = 'brand_product_id';
	headers[ExportPCConstants.PRODUCT_CATEGORY_FIELD] = 'hierarchy_id';
	headers[ExportPCConstants.ACTIVE_DATE_FIELD] = 'active_date';
	headers[ExportPCConstants.ACTIVE_STATUS_FIELD] = 'active_status';
	headers[ExportPCConstants.AVAILABILITY_FIELD] = 'availability_flag';
	headers[ExportPCConstants.VARIANT_AVAILABILITY_FIELD] = 'variant_availability_flag';
	headers[ExportPCConstants.SIZE_FIELD] = 'size';
	headers[ExportPCConstants.COLOR_FIELD] = 'color';
	headers[ExportPCConstants.UPC_FIELD] = 'upc';
	headers[ExportPCConstants.IMAGE_URL_FIELD] = 'image_url';
	headers[ExportPCConstants.PRODUCT_URL_FIELD] = 'product_url';
	writer.writeNext(headers);
}