'use strict';

var GiftCertificateMgr = require('dw/order/GiftCertificateMgr');
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');

var server = require('server');

var addProductToCart = require('*/cartridge/scripts/cart/cartHelpers').addProductToCart;

/**
 * server.replace('AddCoupon')
 *
 * Forking Cart-AddCoupon so that we can call this functionality from Shoprunner-PayrunnerAPI
 */
function addCoupon(req, res, next, couponCode) {
  var BasketMgr = require('dw/order/BasketMgr');
  var CouponStatusCodes = require('dw/campaign/CouponStatusCodes');
  var Resource = require('dw/web/Resource');
  var Transaction = require('dw/system/Transaction');
  var URLUtils = require('dw/web/URLUtils');
  var CartModel = require('*/cartridge/models/cart');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

  var currentBasket = BasketMgr.getCurrentBasket();

  if (!currentBasket) {
    return CouponStatusCodes.COUPON_CODE_UNKNOWN;
  }

  try {
    Transaction.wrap(function () {
      return currentBasket.createCouponLineItem(couponCode, true);
    });
  } catch (e) {
    return e.errorCode || CouponStatusCodes.COUPON_CODE_UNKNOWN;
  }

  Transaction.wrap(function () {
    basketCalculationHelpers.calculateTotals(currentBasket);
  });

  return CouponStatusCodes.APPLIED;
}

/**
 * Used to add the current PDP product to the cart.
 */
function addItem() {
  var BasketMgr = require('dw/order/BasketMgr');
  var ProductMgr = require('dw/catalog/ProductMgr');
  var currentBasket = BasketMgr.getCurrentBasket();

  // JJTODO: Is request still an object we can access without initialization or assignment?
  var pid = request.httpParameterMap.pid.stringValue;
  var product = ProductMgr.getProduct(pid);
  if (empty(product)) {
    return;
  }

  // Quantity? Child products? Options?
  addProductToCart(currentBasket, pid, 1, '', {});
}

/**
 * Sets up billing-related details in order to place an order through
 * the ShopRunner Express popup.
 */
function billing(BillingObj, CreditCardPayment, jsonPRCart) {
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();

  Transaction.wrap(function () {
    currentBasket.custom.shoprunnerExpressOrder = true;
  });

  var billingAddress = currentBasket.getBillingAddress();
  if (empty(billingAddress)) {
    Transaction.wrap(function () {
      billingAddress = currentBasket.createBillingAddress();
    });
  }

  Transaction.wrap(function () {
    require('int_shoprunner/cartridge/scripts/payrunner/placeorder/AddBillingAddress').addAddress(billingAddress, BillingObj);
  });

  var UserEmail = customer.authenticated ? customer.profile.email : BillingObj.billingEmail.toString();
  if (!empty(UserEmail)) {
    Transaction.wrap(function () {
      require('int_shoprunner/cartridge/scripts/payrunner/placeorder/AddBillingEmail').addEmail(currentBasket, UserEmail, jsonPRCart);
    });
  }

  var billingForm = server.forms.getForm('billing');
  billingForm.paymentMethod.value = 'CREDIT_CARD';

  var creditCardFields = billingForm.creditCardFields;

  creditCardFields.cardNumber.value = CreditCardPayment.CCNumber;
  creditCardFields.expirationMonth.value = Number(CreditCardPayment.CCMonth);
  creditCardFields.expirationYear.value = Number(CreditCardPayment.CCYear);
  creditCardFields.cardType.value = CreditCardPayment.creditCard;
  creditCardFields.cardOwner.value = CreditCardPayment.CCHolderName;

  billingForm.contactInfoFields.email.value = BillingObj.billingEmail;
  billingForm.contactInfoFields.phone.value = BillingObj.billingPhone;

  var billingResults = submitPayment(billingForm);

  return billingResults;
}

/**
 * Used to clear the current basket during a PDP Express Checkout
 * (contents have already been saved to a new ProductList
 * to be used to reinstate this basket downstream.
 */
function clearCart() {
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();

  var plis = currentBasket.getAllProductLineItems();
  Transaction.wrap(function () {
    for (var i = 0; i < plis.length; i++) {
      var pli = plis[i];
      // JJTODO: Shouldn't removeProductLineItem have a parameter here?
      currentBasket.removeProductLineItem();
    }
  });

  currentBasket.calculate();
}

/**
 * Sets up shipping-related details in order to place an
 * order through the ShopRunner Express popup.
 */
function defaultShipping(ShippingObj) {
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();

  // JJTODO: Is cart in the httpParameterMap?
  var BasketSR = request.httpParameterMap.cart.stringValue;
  var Context = 'Default Shipping';
  var ProductLineItems = currentBasket.getAllProductLineItems();
  var Shipment = currentBasket.getDefaultShipment();
  var params = {
    BasketSR: BasketSR,
    Context: Context,
    ProductLineItems: ProductLineItems,
    Shipment: Shipment
  };
  Transaction.wrap(function () {
    require('int_shoprunner/cartridge/scripts/payrunner/placeorder/AddShippingAddress').addAddress(Shipment, ShippingObj);
    require('int_shoprunner/cartridge/scripts/payrunner/placeorder/AddShippingMethod').addMethod(params);
    currentBasket.calculate();
  });
  validationResult = currentBasket.validateForCheckout();
  return validationResult;
}

/**
 * Used during an applyPRGiftCard method call to apply the Gift Card/Cert payment,
 * update the Demandware Basket, and update the ShopRunner Basket.
 */
// PixelMEDIA2018: Left empty because SFRA hasn't implemented gift cards yet
function handleApplyPRGiftCard() {}

/**
 * Used during an updatePRCart method call to grab any updates
 * from the ShopRunner Express Checkout popup (shipping method,
 * quantities, Promo Code), update the Demandware Basket, and
 * update the ShopRunner Basket.
 */
function handleUpdatePRCart() {
  var AppliedPromoStatus, cart, CartJSON, cartForm, couponCode, couponStatus;
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();
  cart = currentBasket;

  CartJSON = JSON.parse(request.httpParameterMap.cart.stringValue);
  cartForm = server.forms.getForm('cart');
  if (!empty(CartJSON.promotions)) {
    couponCode = CartJSON.promotions[0].code;
    cartForm.couponCode.htmlValue = couponCode;
    couponStatus = cart.addCoupon(couponCode);
    Transaction.wrap(function () {
      cart.calculate();
    });
    AppliedPromoStatus = couponStatus.CouponStatus;
  }
  var pliToRemove = require('int_shoprunner/cartridge/scripts/payrunner/UpdateQuantities').update(cart, CartJSON);
  if (!empty(pliToRemove)) {
    Transaction.wrap(function () {
      cart.removeProductLineItem(pliToRemove);
    });
  }
  Transaction.wrap(function () {
    cart.calculate();
  });
  return AppliedPromoStatus;
}

/**
 * Initialize shipments for PayRunner modal.
 * If shipping method selected shipments will be initialized
 */
function initializeShipments() {
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();
  var cart = currentBasket;

  if (!cart) {
    return;
  }

  var SRMixedOption = Site.getCurrent().getCustomPreferenceValue('sr_mixedorder');
  var Status = require('int_shoprunner/cartridge/scripts/checkout/CheckCartEligibility').checkEligibility(cart);

  var params = {
    BasketSR: request.httpParameterMap.cart.stringValue,
    Context: 'InitializeShipments',
    ProductLineItems: cart.allProductLineItems,
    Shipment: cart.defaultShipment
  };

  Transaction.wrap(function () {
    require('int_shoprunner/cartridge/scripts/payrunner/placeorder/AddShippingMethod').addMethod(params);
  });
  if (Status == 'MIXED' && SRMixedOption == 'splitShip') {
    var shippingObj = require('int_shoprunner/cartridge/scripts/payrunner/GetShippingObj').shippingObj(cart);
    Transaction.wrap(function () {
      require('int_shoprunner/cartridge/scripts/payrunner/placeorder/SetupShipments').setShipment(cart, request.httpParameterMap.cart.stringValue, shippingObj);
    });
  }

  return;
}

/**
 * This is the main Place Order functionality pipeline.
 */
function placeOrderPrivate() {
  return;
}

/**
 * Setup redirect and order number variables so we end up on the correct confirmation page.
 */
function prepareCartPlaceOrderRedirect(order, jsonPRCart) {
  var FirstOrder, FirstOrderNo, HttpParameterMap, Order, OrderNo;
  HttpParameterMap = request.httpParameterMap;
  Order = order;
  if (!empty(Order)) {
    OrderNo = Order.orderNo;
    session.custom.orderNo = !empty(OrderNo) ? OrderNo : Order.orderNo;
    session.custom.firstOrderNo = !empty(FirstOrder) ? FirstOrder.orderNo : '';
  }
  var params = {
    FirstOrderNo: FirstOrderNo,
    HttpParameterMap: HttpParameterMap,
    Order: Order,
    OrderNo: OrderNo,
    PaymentInstrument: order.getPaymentInstruments()[0]
  };
  var RedirectScript = require('int_shoprunner/cartridge/scripts/payrunner/placeorder/SetupConfirmationRedirect').setupRedirect(params);
  jsonPRCart = RedirectScript;
  return jsonPRCart;
}

function preparePDPPlaceOrderRedirect() {
  return;
}

/**
 * Attempts to redeem a gift certificate entered through the PayRunner popup.
 */
// PixelMEDIA2018: Left empty because SFRA hasn't implemented gift cards yet
function redeemGiftCertificate(EnteredGC) {}

/**
 * Parses and validates the received Order JSON. Calls all of the
 * other pipelets that are involved inside placing order process.
 * Ends in json response.
 * @param {*} shippingObj
 * @param {*} billingObj
 * @param {*} creditCardPayment
 * @param {*} jsonPRCart
 */
function setupOrder(shippingObj, billingObj, creditCardPayment, jsonPRCart) {
  var cart;
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();
  cart = currentBasket;

  require('int_shoprunner/cartridge/scripts/checkout/CheckCartEligibility').checkEligibility(cart);
  shipping(shippingObj);
  billing(billingObj, creditCardPayment);
  //updateGiftCertificates(jsonPRCart);
  return;
}

/**
 * Used during a PDP Express Checkout to save the existing basket as a product list,
 * clear the basket, then add the current PDP product to the basket so the
 * PayRunner popup uses the PDP basket instead of the old existing basket.
 */
function setupPDPCheckout() {
  var cart;
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();
  cart = currentBasket;

  if (!cart) {
    return;
  } else {
    var ProductList = require('int_shoprunner/cartridge/scripts/TransferCartToProductList').CreateProductList();
    session.custom.productListID = ProductList.ID;
  }

  clearCart();
  addItem();
}

/**
 * Handles splitship option
 * @param {*} ShippingObj
 */
function shipping(ShippingObj) {
  var cart, SRMixedOption;
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();
  cart = currentBasket;

  if (!request.httpParameterMap.method.value == 'reviewOrder') {
    SRMixedOption = Site.getCurrent().getCustomPreferenceValue('sr_mixedorder').value;
    if (Status == 'MIXED' && SRMixedOption === 'splitShip') {
      Transaction.wrap(function () {
        cart.calculate();
      });
      return;
    } else {
      if (Status == 'MIXED' && SRMixedOption == 'splitorder') {
        Transaction.wrap(function () {
          require('int_shoprunner/cartridge/scripts/checkout/PullShoprunnerItems').pullItems(cart);
        });
        defaultShipping(ShippingObj);
        return;
      } else {
        if (Status == 'MIXED' && SRMixedOption == 'downgrade') {
          defaultShipping(ShippingObj);
          return;
        } else {
          if (Status == 'MIXED' && SRMixedOption == 'block') {
            defaultShipping(ShippingObj);
            return;
          }
        }
      }
    }
  }
  return;
}

/**
 *
 *
 */
function submitPayment(paymentForm) {
  var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
  var billingFormErrors = {};
  var creditCardErrors = {};
  var viewData = {};

  // verify billing form data
  billingFormErrors = COHelpers.validateBillingForm(paymentForm.addressFields);

  // verify credit card form data
  creditCardErrors = COHelpers.validateCreditCard(paymentForm);

  if (Object.keys(creditCardErrors).length || Object.keys(billingFormErrors).length) {
    // respond with form data and errors
    return {
      form: paymentForm,
      fieldErrors: [billingFormErrors, creditCardErrors],
      serverErrors: [],
      error: true
    };
  } else {
    viewData.paymentMethod = {
      value: paymentForm.paymentMethod.value,
      htmlName: paymentForm.paymentMethod.value
    };

    viewData.paymentInformation = {
      cardType: {
        value: paymentForm.creditCardFields.cardType.value,
        htmlName: paymentForm.creditCardFields.cardType.htmlName
      },
      cardNumber: {
        value: paymentForm.creditCardFields.cardNumber.value,
        htmlName: paymentForm.creditCardFields.cardNumber.htmlName
      },
      securityCode: {
        value: paymentForm.creditCardFields.securityCode.value,
        htmlName: paymentForm.creditCardFields.securityCode.htmlName
      },
      expirationMonth: {
        value: paymentForm.creditCardFields.expirationMonth.value,
        htmlName: paymentForm.creditCardFields.expirationMonth.htmlName
      },
      expirationYear: {
        value: paymentForm.creditCardFields.expirationYear.value,
        htmlName: paymentForm.creditCardFields.expirationYear.htmlName
      }
    };

    // route.on('Complete')
    var BasketMgr = require('dw/order/BasketMgr');
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var HookMgr = require('dw/system/HookMgr');
    var Resource = require('dw/web/Resource');
    var PaymentMgr = require('dw/order/PaymentMgr');
    var Transaction = require('dw/system/Transaction');
    var AccountModel = require('*/cartridge/models/account');
    var OrderModel = require('*/cartridge/models/order');
    var URLUtils = require('dw/web/URLUtils');
    var array = require('*/cartridge/scripts/util/array');
    var Locale = require('dw/util/Locale');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var currentBasket = BasketMgr.getCurrentBasket();
    var billingData = viewData;

    if (!currentBasket) {
      delete billingData.paymentInformation;

      return {
        error: true,
        cartError: true,
        fieldErrors: [],
        serverErrors: [],
        redirectUrl: URLUtils.url('Cart-Show').toString()
      };
    }

    var billingAddress = currentBasket.billingAddress;
    var billingForm = paymentForm;
    var paymentMethodID = billingData.paymentMethod.value;
    var result;

    billingForm.creditCardFields.cardNumber.htmlValue = '';
    billingForm.creditCardFields.securityCode.htmlValue = '';

    // if there is no selected payment option and balance is greater than zero
    if (!paymentMethodID && currentBasket.totalGrossPrice.value > 0) {
      var noPaymentMethod = {};

      noPaymentMethod[billingData.paymentMethod.htmlName] = Resource.msg('error.no.selected.payment.method', 'creditCard', null);

      delete billingData.paymentInformation;

      return {
        form: billingForm,
        fieldErrors: [noPaymentMethod],
        serverErrors: [],
        error: true
      };
    }

    // check to make sure there is a payment processor
    if (!PaymentMgr.getPaymentMethod(paymentMethodID).paymentProcessor) {
      throw new Error(Resource.msg('error.payment.processor.missing', 'checkout', null));
    }

    var processor = PaymentMgr.getPaymentMethod(paymentMethodID).getPaymentProcessor();

    if (HookMgr.hasHook('app.payment.processor.shr_express')) {
      result = HookMgr.callHook('app.payment.processor.shr_express', 'Handle', currentBasket, billingData.paymentInformation);
    } else {
      result = HookMgr.callHook('app.payment.processor.default', 'Handle');
    }

    // need to invalidate credit card fields
    if (result.error) {
      delete billingData.paymentInformation;

      return {
        form: billingForm,
        fieldErrors: result.fieldErrors,
        serverErrors: result.serverErrors,
        error: true
      };
    }

    // Calculate the basket
    Transaction.wrap(function () {
      basketCalculationHelpers.calculateTotals(currentBasket);
    });

    // Re-calculate the payments.
    var calculatedPaymentTransaction = COHelpers.calculatePaymentTransaction(currentBasket);

    if (calculatedPaymentTransaction.error) {
      return {
        form: paymentForm,
        fieldErrors: [],
        serverErrors: [Resource.msg('error.technical', 'checkout', null)],
        error: true
      };
    }

    return {
      error: false
    };
  }
}

/**
 * Redeem the Gift Certificates after updating, reviewing or purchasing the cart
 */
// PixelMEDIA2018: Left empty because SFRA hasn't implemented gift cards yet
function updateGiftCertificates(jsonPRCart) {}

function updatePaymentInstruments() {
  var cart;
  var BasketMgr = require('dw/order/BasketMgr');
  var calculate = require('*/cartridge/scripts/hooks/cart/calculate.js').calculate;
  var currentBasket = BasketMgr.getCurrentBasket();
  cart = currentBasket;

  var basketPaymentInstruments = cart.getPaymentInstruments();
  Transaction.wrap(function () {
    for (var i = 0; i < basketPaymentInstruments.length; i++) {
      var basketPaymentInstrument = basketPaymentInstruments[i];
      var basketPaymentTransaction = basketPaymentInstrument.getPaymentTransaction();
      var basketTransactionAmountVal = basketPaymentTransaction.getAmount().value;
      if (basketTransactionAmountVal <= 0) {
        cart.removePaymentInstrument(basketPaymentInstrument);
      }
    }
    calculate(cart);
  });
  return;
}

module.exports = {
  AddCoupon: addCoupon,
  AddItem: addItem,
  Billing: billing,
  ClearCart: clearCart,
  DefaultShipping: defaultShipping,
  HandleUpdatePRCart: handleUpdatePRCart,
  InitializeShipments: initializeShipments,
  PlaceOrderPrivate: placeOrderPrivate,
  PrepareCartPlaceOrderRedirect: prepareCartPlaceOrderRedirect,
  PreparePDPPlaceOrderRedirect: preparePDPPlaceOrderRedirect,
  SetupOrder: setupOrder,
  SetupPDPCheckout: setupPDPCheckout,
  Shipping: shipping,
  UpdatePaymentInstruments: updatePaymentInstruments
};
