'use strict';

/* API Includes */
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var Logger = require('dw/system/Logger');

var ShoprunnerSameDayDelivery = function () {
  /**
   * Check Whether or not the checkout is eligible for same-day delivery
   *
   * @param {String} resource URL
   * @param {Object} data The JSON data to send in the body of the request
   *
   */
  this.checkEligibility = function (data) {
    var serviceDefinition = LocalServiceRegistry.createService('shoprunner.sameday.session.checkout', {
      createRequest: function (service, params) {
        service.setRequestMethod('POST');
        service.addHeader('Content-Type', 'application/json');
        return params;
      },
      parseResponse: function (service, httpClient) {
        return JSON.parse(httpClient.text);
      },
      // eslint-disable-next-line no-unused-vars
      mockCall: function (svc, client) {
        var mockedReponse =
          '{"retailerCode":"RETAILER1","shippingAddress":{"firstName":"John","lastName":"Doe","address1":"350 N Orleans St","address2":"Suite 300N","city":"Chicago","state":"IL","postalCode":"60654","phone":"(555) 555-5555"},"products":[{"sku":"SOMESKU","quantity":2},{"sku":"SOMEOTHERSKU","quantity":1}],"subtotal":123.45,"smsOptIn":true,"smsPhone":"(555) 555-5555","email":"janedoe@shoprunner.com","deliveryInstructions":"Ring the buzzer at the gate","eligible":true,"token":"SameDayEligibilityToken","pickupLocation":{"id":6,"retailerCode":"RETAILER1","retailerLocationId":"externalId6","address":{"name":"Fine Arts Building","address1":"410 S Michigan Ave","address2":null,"city":"Chicago","postalCode":"60605","state":"IL"},"timezone":"America/Chicago","pickupInstructions":"Curbside pickup","contact":{"firstName":"Jane","lastName":"Doe","email":"","phone":"(555) 555-5555"}}}';
        return {
          statusCode: 200,
          statusMessage: 'Success',
          text: mockedReponse
        };
      },
      filterLogMessage: function (logMsg) {
        try {
          if (logMsg.indexOf('https') === -1) {
            var obj = JSON.parse(logMsg);
            obj.token = '***********';
            obj.email = '***********';
            return JSON.stringify(obj);
          }
        } catch (e) {
          Logger.error('Line #42, File sddShoprunnerService.js | Error stack trace' + e.message);
        }
        return logMsg;
      }
    });

    var result = serviceDefinition.call(JSON.stringify(data));

    return result;
  };
};

module.exports = ShoprunnerSameDayDelivery;
