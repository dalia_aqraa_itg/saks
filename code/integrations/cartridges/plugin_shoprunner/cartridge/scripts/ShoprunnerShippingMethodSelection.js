'use strict';

/**
 * If the user has just logged into ShopRunner, select ShopRunner's shipping
 * method
 *
 * If the user has just logged out, select the default shipping method if
 * ShopRunner was previously selected
 */
function shoprunnerShippingMethodSelection(isPlaceOrder) {
  var BasketMgr = require('dw/order/BasketMgr');
  var ShippingMgr = require('dw/order/ShippingMgr');
  var ShippingHelper = require('*/cartridge/scripts/checkout/shippingHelpers');
  var SRHelper = require('~/cartridge/scripts/SRHelper');
  var Transaction = require('dw/system/Transaction');
  var collections = require('*/cartridge/scripts/util/collections');

  var currentBasket = BasketMgr.getCurrentBasket();
  var srEligibleCart = SRHelper.CheckCartEligibility(currentBasket);

  if (!empty(session.custom.freshSRLogin)) {
    if (session.custom.freshSRLogin && !empty(session.custom.srtoken)) {
      // If cart is SR-eligible, select ShopRunner as the shipping method
      var shoprunnerShippingMethod;

      if (srEligibleCart === 'ALL_SR') {
        Transaction.wrap(function () {
          ShippingHelper.selectShippingMethod(currentBasket.defaultShipment, 'shoprunner');
        });
      }
    } else if (empty(session.custom.srtoken)) {
      // If you've just logged out, set the shipping method back to the default
      var defaultShippingMethod = ShippingMgr.getDefaultShippingMethod();
      if (currentBasket.defaultShipment.shippingMethodID === 'shoprunner') {
        Transaction.wrap(function () {
          ShippingHelper.selectShippingMethod(currentBasket.defaultShipment, defaultShippingMethod.ID);
        });
      }
    }

    delete session.custom.freshSRLogin;
  }

  if (empty(session.custom.srtoken) || (srEligibleCart !== 'ALL_SR' && currentBasket.defaultShipment.shippingMethodID === 'shoprunner')) {
    // If the cart is no longer ShopRunner eligible, set the shipping method back to the default
    var defaultShippingMethod = ShippingMgr.getDefaultShippingMethod();

    // SFDEV-11155 - don't switch away from SR shipping after Place Order is clicked
    if (!isPlaceOrder && currentBasket.defaultShipment.shippingMethodID === 'shoprunner') {
      Transaction.wrap(function () {
        ShippingHelper.selectShippingMethod(currentBasket.defaultShipment, defaultShippingMethod.ID);
      });
    }
  }
}

module.exports = {
  ShoprunnerShippingMethodSelection: shoprunnerShippingMethodSelection
};
