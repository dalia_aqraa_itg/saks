'use strick';

/**
 * @constructor
 *
 * @param {json} data - arguments
 */
function getRequestJSON(shippingAddress, saksAvenueOptIn, items) {
  var shippingAddress = shippingAddress;
  var requestObj = {};
  requestObj.phone = shippingAddress.phone;
  requestObj.smsOptIn = saksAvenueOptIn;
  if (saksAvenueOptIn) {
    requestObj.smsPhone = requestObj.phone;
  }
  requestObj.shippingAddress = {};
  requestObj.shippingAddress.firstName = shippingAddress.firstName;
  requestObj.shippingAddress.lastName = shippingAddress.lastName;
  requestObj.shippingAddress.address1 = shippingAddress.address1;
  requestObj.shippingAddress.address2 = shippingAddress.address2 ? shippingAddress.address2 : '';
  requestObj.shippingAddress.city = shippingAddress.city;
  requestObj.shippingAddress.state = shippingAddress.stateCode;
  requestObj.shippingAddress.postalCode = shippingAddress.postalCode;
  requestObj.shippingAddress.phone = requestObj.phone;
  var subtotal = 0;
  var storeid = session.custom.sddstoreid ? session.custom.sddstoreid : '';
  requestObj.products = [];
  items.forEach(function (item) {
    var prod = {};
    prod.sku = item.id;
    prod.quantity = item.quantity;
    requestObj.products.push(prod);
    subtotal += item.priceTotal.unFormattedprice.value;
  });

  requestObj.subtotal = subtotal.toFixed(2);
  if (shippingAddress.deliveryInstructions) {
    requestObj.deliveryInstructions = shippingAddress.deliveryInstructions.replace(/(!+)|(\?+)/g, '.');
  }
  requestObj.email = shippingAddress.email;
  requestObj.pickupLocations = storeid.split();

  return requestObj;
}

/**
 * Helper method for checking Same day delivery via Shoprunner
 * For SDD, ShopRunner is purely a fulfillment partner and
 * facilitates the collection and delivery of the purchase.
 * @param {string} data - request data
 * @returns {Object) response - response from shoprunner
 */
function checkEligibility(shippingAddress, customer, items) {
  var sddShoprunnerService = new (require('*/cartridge/scripts/services/init/sddShoprunnerService'))();
  var saksAvenueOptIn = customer.profile && 'saksAvenueOptIn' in customer.profile.custom && customer.profile.custom.saksAvenueOptIn;
  var requestJson = getRequestJSON(shippingAddress, !!saksAvenueOptIn, items);
  var result = sddShoprunnerService.checkEligibility(requestJson);
  var Result = require('dw/svc/Result');
  var Logger = require('dw/system/Logger');
  // Check for service availability.
  if (result.status === Result.SERVICE_UNAVAILABLE) {
    Logger.getRootLogger().fatal('checkout/sameday/checkDeliveryEligibility Error : SERVICE_UNAVAILABLE response');
  }
  var response = {};
  response.error = true;
  response.result = {};
  if (result.status !== 'OK' || !result.object) {
    Logger.getRootLogger().fatal('checkout/sameday/checkDeliveryEligibility : SERVICE_ERROR response' + result.errorMessage);
    response.result.errorMessage = result.errorMessage;
    response.result.status = result.status;
  } else if (result.object) {
    response.result = result.object;
    if (result.object.eligible && result.object.pickupLocation && result.object.token) {
      response.error = false;
      response.token = result.object.token;
    }
    if (result.object.ineligibleReason) {
      response.ineligibleReason = result.object.ineligibleReason.message;
    }
  } else {
    response.result.errorMessage = 'Unexpected Error';
  }

  return response;
}
module.exports = {
  checkEligibility: checkEligibility
};
