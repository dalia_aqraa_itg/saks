'use strict';

/**
 * Processor to handle CREDIT_CARD processing without CVV validation
 * Override this file with your specific PSP provider code (bypassing CVV)
 *
 * Include the custom payment transaction attribute to denote this was a
 * ShopRunner Express checkout order
 */

var Cart = require('*/cartridge/models/cart');
var collections = require('*/cartridge/scripts/util/collections');
var PaymentInstrument = require('dw/order/PaymentInstrument');
var PaymentMgr = require('dw/order/PaymentMgr');
var PaymentStatusCodes = require('dw/order/PaymentStatusCodes');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');

var server = require('server');

/**
 * Creates a token. This should be replaced by utilizing a tokenization provider
 * @returns {string} a token
 */
function createMockToken() {
  return Math.random().toString(36).substr(2);
}

/**
 * Verifies a credit card against a valid card number and expiration date and possibly invalidates invalid form fields
 * If the verification was successful, a credit card payment instrument is created
 */
function Handle(basket, paymentInformation) {
  var currentBasket = basket;
  var cardErrors = {};
  var cardNumber = paymentInformation.cardNumber.value;
  var cardSecurityCode = paymentInformation.securityCode.value;
  var expirationMonth = paymentInformation.expirationMonth.value;
  var expirationYear = paymentInformation.expirationYear.value;
  var serverErrors = [];
  var creditCardStatus;

  var cardType = paymentInformation.cardType.value;
  if (cardType == 'MasterCard') {
    cardType = 'Master Card';
  }

  var cardType = cardType;
  var paymentCard = PaymentMgr.getPaymentCard(cardType);

  if (!paymentInformation.creditCardToken) {
    if (paymentCard) {
      creditCardStatus = paymentCard.verify(expirationMonth, expirationYear, cardNumber);
    } else {
      cardErrors[paymentInformation.cardNumber.htmlName] = Resource.msg('error.invalid.card.number', 'creditCard', null);

      return { fieldErrors: [cardErrors], serverErrors: serverErrors, error: true };
    }

    if (creditCardStatus.error) {
      collections.forEach(creditCardStatus.items, function (item) {
        switch (item.code) {
          case PaymentStatusCodes.CREDITCARD_INVALID_CARD_NUMBER:
            cardErrors[paymentInformation.cardNumber.htmlName] = Resource.msg('error.invalid.card.number', 'creditCard', null);
            break;

          case PaymentStatusCodes.CREDITCARD_INVALID_EXPIRATION_DATE:
            cardErrors[paymentInformation.expirationMonth.htmlName] = Resource.msg('error.expired.credit.card', 'creditCard', null);
            cardErrors[paymentInformation.expirationYear.htmlName] = Resource.msg('error.expired.credit.card', 'creditCard', null);
            break;

          case PaymentStatusCodes.CREDITCARD_INVALID_SECURITY_CODE:
            cardErrors[paymentInformation.securityCode.htmlName] = Resource.msg('error.invalid.security.code', 'creditCard', null);
            break;
          default:
            serverErrors.push(Resource.msg('error.card.information.error', 'creditCard', null));
        }
      });

      return { fieldErrors: [cardErrors], serverErrors: serverErrors, error: true };
    }
  }
  // Remove all the payment instruments in the basket as Shop runner PI can't be combined with other PI.
  Transaction.wrap(function () {
    var paymentInstruments = currentBasket.getPaymentInstruments();

    collections.forEach(paymentInstruments, function (item) {
      currentBasket.removePaymentInstrument(item);
    });

    var paymentInstrument = currentBasket.createPaymentInstrument(PaymentInstrument.METHOD_CREDIT_CARD, currentBasket.totalGrossPrice);

    paymentInstrument.setCreditCardHolder(currentBasket.billingAddress.fullName);
    paymentInstrument.setCreditCardNumber(cardNumber);
    paymentInstrument.setCreditCardType(cardType);
    paymentInstrument.setCreditCardExpirationMonth(expirationMonth);
    paymentInstrument.setCreditCardExpirationYear(expirationYear);
    // As ShopRunner will return TokenEX token for CC Number, We will just set to the CC.
    paymentInstrument.setCreditCardToken(cardNumber);
  });

  return { fieldErrors: cardErrors, serverErrors: serverErrors, error: false };
}

/**
 * Authorizes a payment using a credit card. The payment is authorized by using the BASIC_CREDIT processor
 * only and setting the order no as the transaction ID. Customizations may use other processors and custom
 * logic to authorize credit card payment.
 */
function Authorize(orderNumber, paymentInstrument, paymentProcessor, params) {
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  return hooksHelper(
    'app.payment.processor.ipa_credit',
    'Authorize',
    [orderNumber, paymentInstrument, paymentProcessor, params],
    require('*/cartridge/scripts/hooks/payment/processor/ipa_credit').Authorize
  );
}

exports.Handle = Handle;
exports.Authorize = Authorize;
