'use strict';

/**
 * Helper controller used to execute int_shoprunner scripts from storefront cartridge controllers.
 * This is done to make cartridge integration simpler as well as make future adjustments to scripts easier.
 *
 * @module controllers/SRHelper
 */

/* API includes */
var Transaction = require('dw/system/Transaction');

var BasketMgr = require('dw/order/BasketMgr');

function getPREligibilityCart() {
  var result;
  var cart = BasketMgr.getCurrentBasket();
  if (!cart) {
    return;
  }
  var srToken = session.custom.srtoken;
  Transaction.wrap(function () {
    result = require('int_shoprunner/cartridge/scripts/payrunner/getPREligibilityCart').getEligibility(cart, srToken);
  });
  return result;
}

// JJTODO: can probably get rid of this method
function getApplicableShippingMethods(params) {
  var applicableMethods;
  Transaction.wrap(function () {
    applicableMethods = require('int_shoprunner/cartridge/scripts/checkout/GetApplicableShippingMethods').getMethods(params);
  });
  return applicableMethods;
}

function deleteShopRunnerCookie() {
  require('int_shoprunner/cartridge/scripts/DeleteShopRunnerCookie').deleteCookie();
  return;
}

function filterMultiShipMethods(shippingMethods) {
  var result = require('int_shoprunner/cartridge/scripts/checkout/FilterMultiShipMethods').filterMethods(shippingMethods);
  return result;
}

function checkCartEligibility(cart) {
  var result = require('int_shoprunner/cartridge/scripts/checkout/CheckCartEligibility').checkEligibility(cart);
  return result;
}

function preCalculateDefaultShipment() {
  Transaction.wrap(function () {
    //checkout/PreCalculateDefaultShipment.ds
  });
}

function createShipmentShippingAddress() {
  Transaction.wrap(function () {
    //checkout/CreateShipmentShippingAddress.ds
  });
}

function checkGroundFreePromo() {
  var cart = BasketMgr.getCurrentBasket();
  var result = require('int_shoprunner/cartridge/scripts/checkout/CheckShippingGroundFree').checkFreeGround(cart);
  return result;
}

/*
 * Private methods
 */
exports.GetPREligibilityCart = getPREligibilityCart;
exports.GetApplicableShippingMethods = getApplicableShippingMethods;
exports.DeleteShopRunnerCookie = deleteShopRunnerCookie;
exports.FilterMultiShipMethods = filterMultiShipMethods;
exports.CheckCartEligibility = checkCartEligibility;
exports.PreCalculateDefaultShipment = preCalculateDefaultShipment;
exports.CreateShipmentShippingAddress = createShipmentShippingAddress;
exports.CheckGroundFreePromo = checkGroundFreePromo;
