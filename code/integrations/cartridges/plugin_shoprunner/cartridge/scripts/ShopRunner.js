'use strict';

/* API includes */
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');
var RootLogger = require('dw/system/Logger').getRootLogger();

var server = require('server');

var BasketMgr = require('dw/order/BasketMgr');
var CartModel = require('*/cartridge/models/cart');
var currentBasket = BasketMgr.getCurrentBasket();

/**
 * Hooks from COCustomer use this controller to select either the regular
 * shipping controller or the multiship controller
 */
function checkoutMethod() {
  var cart, Status;
  cart = new CartModel(currentBasket);
  if (empty(session.custom.srtoken)) {
    return Status;
  } else {
    var Status = require('int_shoprunner/cartridge/scripts/checkout/CheckCartEligibility').checkEligibility(cart);
    return Status;
  }
}

/**
 * Custom CheckAddress controller specific to ShopRunner
 */
function checkAddress() {
  //checkout/CheckPOBox.ds
  //checkout/CheckAPOFPO.ds
  return;
}

/**
 * Custom ClearCheckoutForms controller specific to ShopRunner
 */
function clearCheckoutForms() {
  var SavePayment;
  server.forms.getForm('singleshipping').clear();
  server.forms.getForm('multishipping').clear();
  if (SavePayment !== 'save') {
    server.forms.getForm('billing').clear();
  }
  return;
}

/**
 * If a user is logged in to ShopRunner, this checks if their basket
 * qualifies and checks against the mixed order preference
 */
function eligibleBasket() {
  var srtoken = require('int_shoprunner/cartridge/scripts/checkout/GetShopRunnerToken').getToken();
  var cart, Status, srtoken;
  if (empty(session.custom.srtoken || empty(srtoken))) {
    return;
  } else {
    cart = new CartModel(currentBasket);
    var Status = require('int_shoprunner/cartridge/scripts/checkout/CheckCartEligibility').checkEligibility(cart);
    return Status;
  }
}

/**
 * server.prepend('PlaceOrder')
 *
 * Defined here so that we can effectively call this 'controller' via ShopRunner-PayrunnerAPI
 */
function placeOrderPrepend(req, res, next) {
  var Site = require('dw/system/Site');
  var Status = require('dw/system/Status');

  // Check if we're ShopRunner and it's been selected
  if (Site.getCurrent().getCustomPreferenceValue('sr_enabled')) {
    var BasketMgr = require('dw/order/BasketMgr');
    var currentBasket = BasketMgr.getCurrentBasket();
    var Status = require('int_shoprunner/cartridge/scripts/checkout/CheckCartEligibility').checkEligibility(currentBasket);
    if (!Status) {
      return;
    }
    if (Site.getCurrent().getCustomPreferenceValue('sr_mixedorder').value !== 'splitorder' || Status !== 'MIXED') {
    } else {
      if (Site.getCurrent().getCustomPreferenceValue('sr_mixedorder').value === 'splitShip' && Status === 'MIXED') {
        Transaction.wrap(function () {
          require('~/cartridge/scripts/checkout/PullShoprunnerItems').pullItems(currentBasket);
        });
      }
      Transaction.wrap(function () {
        var SrOrderItems = require('int_shoprunner/cartridge/scripts/checkout/PullShoprunnerShipment').pullShipment(currentBasket);
      });

      res.setViewData({
        srOrderItems: SrOrderItems
      });
    }
  }
}

/**
 * server.replace('PlaceOrder')
 *
 * Defined here so that we can effectively call this 'controller' via ShopRunner-PayrunnerAPI
 */
function placeOrder(req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
  var HookMgr = require('dw/system/HookMgr');
  var OrderMgr = require('dw/order/OrderMgr');
  var Resource = require('dw/web/Resource');
  var Transaction = require('dw/system/Transaction');
  var URLUtils = require('dw/web/URLUtils');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');

  var currentBasket = BasketMgr.getCurrentBasket();

  if (!currentBasket) {
    res.json({
      error: true,
      cartError: true,
      fieldErrors: [],
      serverErrors: [],
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }

  if (req.session.privacyCache.get('fraudDetectionStatus')) {
    res.json({
      error: true,
      cartError: true,
      redirectUrl: URLUtils.url('Error-ErrorCode', 'err', '01').toString(),
      errorMessage: Resource.msg('error.technical', 'checkout', null)
    });

    return next();
  }
  var validationOrderStatus = hooksHelper(
    'app.validate.order',
    'validateOrder',
    currentBasket,
    require('*/cartridge/scripts/hooks/validateOrder').validateOrder
  );
  if (validationOrderStatus.error) {
    res.json({
      error: true,
      errorMessage: validationOrderStatus.message
    });
    return next();
  }
  /*    var validationOrderStatus = HookMgr.callHook(
        'app.validate.order',
        'validateOrder',
        currentBasket
    );
    if (validationOrderStatus.error) {
        res.json({
            error: true,
            errorMessage: validationOrderStatus.message
        });
        return next();
    }*/

  // Check to make sure there is a shipping address
  if (currentBasket.defaultShipment.shippingAddress === null) {
    res.json({
      error: true,
      errorStage: {
        stage: 'shipping',
        step: 'address'
      },
      errorMessage: Resource.msg('error.no.shipping.address', 'checkout', null)
    });
    return next();
  }

  // Check to make sure billing address exists
  if (!currentBasket.billingAddress) {
    res.json({
      error: true,
      errorStage: {
        stage: 'payment',
        step: 'billingAddress'
      },
      errorMessage: Resource.msg('error.no.billing.address', 'checkout', null)
    });
    return next();
  }

  // Calculate the basket
  Transaction.wrap(function () {
    basketCalculationHelpers.calculateTotals(currentBasket);
  });

  // Calculate FDD, Associate and discount values for the product line items
  COHelpers.prorateLineItemDiscounts(currentBasket, req.currentCustomer.raw);

  // Re-validates existing payment instruments
  var validPayment = COHelpers.validatePayment(req, currentBasket);
  if (validPayment.error) {
    res.json({
      error: true,
      errorStage: {
        stage: 'payment',
        step: 'paymentInstrument'
      },
      errorMessage: Resource.msg('error.payment.not.valid', 'checkout', null)
    });
    return next();
  }

  // Re-calculate the payments.
  var calculatedPaymentTransactionTotal = COHelpers.calculatePaymentTransaction(currentBasket);
  if (calculatedPaymentTransactionTotal.error) {
    res.json({
      error: true,
      errorMessage: Resource.msg('error.technical', 'checkout', null)
    });
    return next();
  }

  //update the carrier service code
  COHelpers.updateCarrierCodes(currentBasket);

  // Creates a new order.
  var order = COHelpers.createOrder(currentBasket);
  if (!order) {
    res.json({
      error: true,
      errorMessage: Resource.msg('error.technical', 'checkout', null)
    });
    return next();
  }

  // Handles payment authorization
  var handlePaymentResult = COHelpers.handlePayments(order, order.orderNo, req);
  if (handlePaymentResult.error) {
    res.json({
      error: true,
      errorMessage: Resource.msg('error.technical', 'checkout', null)
    });
    return next();
  }

  //var fraudDetectionStatus = HookMgr.callHook('app.fraud.detection', 'fraudDetection', currentBasket);
  var fraudDetectionStatus = hooksHelper(
    'app.validate.order',
    'fraudDetection',
    currentBasket,
    require('*/cartridge/scripts/hooks/fraudDetection').fraudDetection
  );
  if (fraudDetectionStatus.status === 'fail') {
    Transaction.wrap(function () {
      OrderMgr.failOrder(order);
    });

    // fraud detection failed
    req.session.privacyCache.set('fraudDetectionStatus', true);

    res.json({
      error: true,
      cartError: true,
      redirectUrl: URLUtils.url('Error-ErrorCode', 'err', fraudDetectionStatus.errorCode).toString(),
      errorMessage: Resource.msg('error.technical', 'checkout', null)
    });

    return next();
  }

  // Places the order
  var placeOrderResult = COHelpers.placeOrder(order, fraudDetectionStatus);
  if (placeOrderResult.error) {
    res.json({
      error: true,
      errorMessage: Resource.msg('error.technical', 'checkout', null)
    });
    return next();
  }

  //Removed order confirmation email from Shoprunner as it would be sent via OMS.
  //COHelpers.sendConfirmationEmail(order, req.locale.id);

  // Reset usingMultiShip after successful Order placement
  req.session.privacyCache.set('usingMultiShipping', false);

  // TODO: Exposing a direct route to an Order, without at least encoding the orderID
  //  is a serious PII violation.  It enables looking up every customers orders, one at a
  //  time.
  res.json({
    error: false,
    orderID: order.orderNo,
    orderToken: order.orderToken,
    continueUrl: URLUtils.url('Order-Confirm').toString(),
    placeOrderResult: placeOrderResult
  });
}

/**
 * server.append('PlaceOrder')
 *
 * Defined here so that we can effectively call this 'controller' via ShopRunner-PayrunnerAPI
 */
function placeOrderAppend(req, res, next) {
  var viewData = res.getViewData();
  var Site = require('dw/system/Site');

  if (!viewData.error && Site.getCurrent().getCustomPreferenceValue('sr_enabled')) {
    var OrderMgr = require('dw/order/OrderMgr');

    var order = OrderMgr.getOrder(viewData.orderID);

    if (Site.getCurrent().getCustomPreferenceValue('sr_mixedorder').value.value !== 'splitorder' || Status !== 'MIXED') {
      // PixelMEDIA2018: Due to the typo in the line above, the if statement will always return true; Multiorder doesn't
      //                 work in Javascript controllers, and hasn't been fixed coming to SFRA
      saveOrderToken(order);
      //require('int_shoprunner/cartridge/controllers/SRFeeds').SendOrderToSR(order);
      if (!empty(req.session.custom) && !empty(req.session.custom.productListID)) {
        require('~/cartridge/scripts/TransferCartToProductList').ProductListToBasket();
        req.session.custom.productListID == null;
      }
    } else {
      // place MultiOrder
      var BasketMgr = require('dw/order/BasketMgr');
      var currentBasket = BasketMgr.getCurrentBasket();
      var MainOrder = order;

      var viewData = res.getViewData();

      if (empty(currentBasket.getBillingAddress())) {
        currentBasket.createBillingAddress();
      }

      require('int_shoprunner/cartridge/scripts/checkout/BuildShoprunnerBasket').buildBasket(currentBasket, viewData.SrOrderItems, MainOrder);
      // JJTODO: Copy payment instrument from mainOrder to currentBasket
      //app.getController('COBilling').HandlePaymentSelection();

      // Call PlaceOrder a second time?
      //app.getController('COPlaceOrder').Start();

      saveOrderToken();
      //require('int_shoprunner/cartridge/controllers/SRFeeds').SendOrderToSR(order);
    }
  }
}

/**
 * Determines whether or not multi ship is required.
 */
function requireMultiShip() {
  if (Site.getCurrent().getCustomPreferenceValue('sr_mixedorder').value !== 'splitorder') {
    return true;
  }
  return false;
}

/**
 * Simple controller for save the ShopRunner Order Token
 */
function saveOrderToken(order) {
  Transaction.wrap(function () {
    require('int_shoprunner/cartridge/scripts/checkout/SaveShopRunnerOrderToken').saveToken(session, order);
  });
  return;
}

/**
 * If splitorders are active and we have a mixed cart, pull all
 * shoprunner items into a seperate/hidden shipment.
 */
function storeShopRunnerItems(cart) {
  var Status;
  if (empty(session.custom.srtoken)) {
    return;
  } else {
    Status = require('int_shoprunner/cartridge/scripts/checkout/CheckCartEligibility').checkEligibility(cart);
    if (Site.getCurrent().getCustomPreferenceValue('sr_mixedorder').value === 'splitShip' && Status === 'MIXED') {
      Transaction.wrap(function () {
        require('int_shoprunner/cartridge/scripts/checkout/PullShoprunnerItems').pullItems(cart);
      });
    } else {
      return;
    }
  }
}

/**
 * Custom Payment Validation for ShopRunner
 */
function validatePayment() {
  require('int_shoprunner/cartridge/scripts/checkout/GetNonGiftCertificatePaymentAmount').getAmount(cart);
  Transaction.wrap(function () {
    require('int_shoprunner/cartridge/scripts/checkout/ValidatePaymentInstruments').validate(cart);
  });
  //checkout/CalculatePaymentTransactionTotals.ds
  return;
}

/**
 * Sending Order XML file to OMS
 */
function createOrderInOMSForShopRunnerOrder(req, res, next) {
  var viewData = res.getViewData();
  if (!viewData.error && Site.getCurrent().getCustomPreferenceValue('sr_enabled')) {
    var OrderMgr = require('dw/order/OrderMgr');

    var order = OrderMgr.getOrder(viewData.orderID);
    var Logger = require('dw/system/Logger');
    var OrderAPIUtils = require('*/cartridge/scripts/util/OrderAPIUtil');
    var Order = require('dw/order/Order');

    try {
      // save the device finger print and fail attempts to the order object
      Transaction.wrap(function () {
        if (req.session.privacyCache.get('inAuthTransactionUUID')) {
          order.custom.deviceFingerPrintID = req.session.privacyCache.get('inAuthTransactionUUID');
        }
      });

      var responseOrderXML = OrderAPIUtils.createOrderInOMS(order);
      // eslint-disable-next-line no-undef
      var resXML = new XML(responseOrderXML);
      Logger.debug('resXML.child(ResponseMessage) -->' + resXML.child('ResponseMessage'));

      if (resXML.child('ResponseMessage').toString() === 'Success') {
        Transaction.wrap(function () {
          order.exportStatus = Order.EXPORT_STATUS_EXPORTED;
          order.trackOrderChange('Order Export Successful');
          order.trackOrderChange(responseOrderXML);
          order.custom.reversalForCancelOrder = false;
        });
      } else {
        Transaction.wrap(function () {
          order.exportStatus = Order.EXPORT_STATUS_FAILED;
          order.trackOrderChange('Order Exported Failed');
          order.trackOrderChange(responseOrderXML);
          order.custom.reversalForCancelOrder = false;
        });
      }
    } catch (err) {
      RootLogger.fatal('Error while calling the Order Create Service for Order ' + order.orderNo + ' error message ' + err.message);
    }
  }
}

module.exports = {
  CheckoutMethod: checkoutMethod,
  CheckAddress: checkAddress,
  ClearCheckoutForms: clearCheckoutForms,
  EligibleBasket: eligibleBasket,
  RequireMultiShip: requireMultiShip,
  PlaceOrderPrepend: placeOrderPrepend,
  PlaceOrder: placeOrder,
  PlaceOrderAppend: placeOrderAppend,
  SaveOrderToken: saveOrderToken,
  StoreShopRunnerItems: storeShopRunnerItems,
  ValidatePayment: validatePayment,
  CreateOrderInOMSForShopRunnerOrder: createOrderInOMSForShopRunnerOrder
};
