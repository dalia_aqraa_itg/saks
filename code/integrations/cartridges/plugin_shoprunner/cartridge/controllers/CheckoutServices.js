'use strict';

var server = require('server');

server.extend(module.superModule);

server.prepend('PlaceOrder', function (req, res, next) {
  var shoprunnerShippingMethodSelection = require('~/cartridge/scripts/ShoprunnerShippingMethodSelection').ShoprunnerShippingMethodSelection;
  shoprunnerShippingMethodSelection();

  var ShopRunner = require('~/cartridge/scripts/ShopRunner');
  ShopRunner.PlaceOrderPrepend(req, res, next);

  return next();
});

server.append('PlaceOrder', function (req, res, next) {
  var ShopRunner = require('~/cartridge/scripts/ShopRunner');
  ShopRunner.PlaceOrderAppend(req, res, next);

  return next();
});

module.exports = server.exports();
