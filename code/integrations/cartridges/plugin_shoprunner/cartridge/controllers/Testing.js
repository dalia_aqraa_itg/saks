'use strict';

/**
 * This controller exists to allow more easily testing pages that require
 * some effort to load; example: order confirmation page
 *
 * @module controllers/Testing
 */

var server = require('server');

var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');

server.get('OrderConfirmation', consentTracking.consent, server.middleware.https, csrfProtection.generateToken, function (req, res, next) {
  var reportingUrlsHelper = require('*/cartridge/scripts/reportingUrls');
  var OrderMgr = require('dw/order/OrderMgr');
  var OrderModel = require('*/cartridge/models/order');
  var Locale = require('dw/util/Locale');

  var order = OrderMgr.getOrder(req.querystring.ID);

  var config = {
    numberOfLineItems: '*'
  };

  var currentLocale = Locale.getLocale(req.locale.id);

  var orderModel = new OrderModel(order, { config: config, countryCode: currentLocale.country, containerView: 'order' });
  var passwordForm;

  var reportingURLs = reportingUrlsHelper.getOrderReportingURLs(order);

  if (!req.currentCustomer.profile) {
    passwordForm = server.forms.getForm('newPasswords');
    passwordForm.clear();
    res.render('checkout/confirmation/confirmation', {
      order: orderModel,
      returningCustomer: false,
      passwordForm: passwordForm,
      reportingURLs: reportingURLs
    });
  } else {
    res.render('checkout/confirmation/confirmation', {
      order: orderModel,
      returningCustomer: true,
      reportingURLs: reportingURLs
    });
  }

  return next();
});

module.exports = server.exports();
