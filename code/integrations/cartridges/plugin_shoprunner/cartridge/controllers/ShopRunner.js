'use strict';

var server = require('server');

server.post('EligibleBasketForm', function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var CartModel = require('*/cartridge/models/cart');
  var URLUtils = require('dw/web/URLUtils');
  var Transaction = require('dw/system/Transaction');

  var currentBasket = BasketMgr.getCurrentBasket();
  var cart = currentBasket;

  var ineligibleCartForm = server.forms.getForm('ineligiblecart');

  if (ineligibleForm.keepitems.triggered === true) {
    require('int_shoprunner/cartridge/scripts/DeleteShopRunnerCookie').deleteCookie();
    req.session.custom.keepItems = true;

    res.json({
      error: false,
      continueUrl: URLUtils.https('Checkout-Begin')
    });
  } else if (ineligibleForm.removeitems.triggered === true) {
    require('int_shoprunner/cartridge/scripts/checkout/RemoveNonShopRunner').removeItems(cart);
    req.session.custom.removeItems = true;

    res.json({
      error: false,
      continueUrl: URLUtils.https('Checkout-Begin')
    });
  }

  return next();
});

/**
 * This is the main processing controller when using PayRunner. All incoming PayRunner
 * requests invoke this controller and it is determined within this controller what
 * functionality to execute based on the request's method parameter.
 *
 */
server.use('PayrunnerAPI', server.middleware.https, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var PayRunner = require('~/cartridge/scripts/PayRunner');
  var URLUtils = require('dw/web/URLUtils');
  var Transaction = require('dw/system/Transaction');
  var ShopRunner = require('~/cartridge/scripts/ShopRunner');
  var Site = require('dw/system/Site');
  var customPreferences = Site.current.preferences.custom;

  var jsonPRCart;
  var AppliedPromoStatus;
  var couponStatus;
  var couponCode;
  var ShopRunnerFrom;
  var currentBasket = BasketMgr.getCurrentBasket();

  if (!currentBasket) {
    res.redirect(URLUtils.url('Cart-Show'));
    return next();
  }
  
  delete req.session.raw.custom.associateTier; 
  delete req.session.raw.custom.specialVendorDiscountTier; 
  req.session.raw.custom.isHBCTenderType = false;

  var method = req.form.method;
  var firstName = req.form.firstName;
  var lastName = req.form.lastName;
  var email = req.form.email;
  var phone = req.form.phone;
  var cart = req.form.cart;
  var srCheckoutId = req.form.SRCheckoutId;
  var payRunnerResult;
  var gcResult;

  if (method === 'getPRCart' && !empty(req.form.pid) && !empty(req.form.quantity)) {
    PayRunner.SetupPDPCheckout();
  }

  PayRunner.InitializeShipments();

  Transaction.wrap(function () {
    var HookMgr = require('dw/system/HookMgr');
    HookMgr.callHook('dw.order.calculate', 'calculate', currentBasket);
  });

  if (method === 'applyPRGiftCard') {
    gcResult = PayRunner.HandleUpdatePRCart();
  }

  if (method === 'updatePRCart') {
    var CouponStatusCodes = require('dw/campaign/CouponStatusCodes');
    CartJSON = JSON.parse(request.httpParameterMap.cart.stringValue);
    var couponCode;
    if (!empty(CartJSON.promotions)) {
      couponCode = CartJSON.promotions[0].code;
      AppliedPromoStatus = PayRunner.AddCoupon(req, res, next, couponCode);
    } else {
      AppliedPromoStatus = CouponStatusCodes.COUPON_CODE_UNKNOWN;
    }
  }

  if (method === 'startPRCheckout' || method === 'updatePRCart' || method === 'applyPRGiftCard') {
    //PayRunner.UpdateGiftCertificates();
  }

  if (empty(AppliedPromoStatus)) {
    AppliedPromoStatus = '';
  }

  var payRunnerArguments = {
    APIMethod: method,
    Basket: currentBasket,
    Firstname: firstName,
    Lastname: lastName,
    Email: email,
    Phone: phone,
    Cart: cart,
    SRCheckoutId: srCheckoutId,
    SRPromoStatus: AppliedPromoStatus,
    CouponStatus: couponStatus,
    CouponCode: couponCode
  };

  if (!empty(gcResult)) {
    (payRunnerArguments['NewGCPaymentInstrument'] = gcResult.NewGCPaymentInstrument),
      (payRunnerArguments['GiftCertificateStatus'] = gcResult.GiftCertificateStatus),
      (payRunnerArguments['EnteredGC'] = gcResult.EnteredGC);
  }

  Transaction.wrap(function () {
    payRunnerResult = require('int_shoprunner/cartridge/scripts/payrunner/payrunnerAPI').getMethods(payRunnerArguments);
  });

  var ShippingObj = payRunnerResult.ShippingObj;
  var BillingObj = payRunnerResult.BillingObj;
  var CreditCardPayment = payRunnerResult.CreditCardPayment;
  var jsonPRCart = payRunnerResult.JSONPRCart;

  Transaction.wrap(function () {
    var HookMgr = require('dw/system/HookMgr');
    HookMgr.callHook('dw.order.calculate', 'calculate', currentBasket);
  });

  if (method === 'reviewOrder') {
    PayRunner.SetupOrder(ShippingObj, BillingObj, CreditCardPayment, jsonPRCart);
  }

  if (method === 'processPROrder') {
    PayRunner.SetupOrder(ShippingObj, BillingObj, CreditCardPayment, jsonPRCart);
    PayRunner.UpdatePaymentInstruments();
    var url = require('int_shoprunner/cartridge/scripts/util/srFriendlyURL').getURL();
    if (ShopRunnerFrom === 'Product-Show') {
      // PixelMEDIA2018: ShopRunnerFrom never appears to be set, so this is never run
      PayRunner.PreparePDPPlaceOrderRedirect();
    } else {
      // Copy in CheckoutServices-PlaceOrder actions, with Shoprunner customizations

      ShopRunner.PlaceOrderPrepend(req, res, next);
      ShopRunner.PlaceOrder(req, res, next);
      ShopRunner.PlaceOrderAppend(req, res, next);

      if (customPreferences.orderCreateBatchEnabled !== true) {
        ShopRunner.CreateOrderInOMSForShopRunnerOrder(req, res, next);
      }

      var viewData = res.getViewData();

      if (!viewData.error) {
        var OrderMgr = require('dw/order/OrderMgr');
        var order = OrderMgr.getOrder(viewData.orderID);

        Transaction.wrap(function () {
          //SFDEV-11204 | Generate OMS Create Order XML to be used in ExportOrders batch job
          var OrderAPIUtils = require('*/cartridge/scripts/util/OrderAPIUtil');
          order.custom.omsCreateOrderXML = OrderAPIUtils.buildRequestXML(order);
        });

        jsonPRCart = PayRunner.PrepareCartPlaceOrderRedirect(order);
      } else {
        jsonPRCart = PayRunner.PrepareCartPlaceOrderRedirect(null);
      }
    }
  }

  res.render('payrunner/getPRCart', {
    jsonPRCart: jsonPRCart
  });

  return next();
});

server.get('SetupConfirmation', function (req, res, next) {
  var OrderMgr = require('dw/order/OrderMgr');
  var URLUtils = require('dw/web/URLUtils');

  var order;
  var orderNo;
  var firstOrderNo;

  orderNo = session.custom.orderNo;
  order = OrderMgr.getOrder(orderNo);

  if (!order) {
    res.redirect(URLUtils.https('Account-Show'));
  } else {
    if (typeof session.custom.firstOrderNo !== 'undefined' && empty(session.custom.firstOrderNo) === false) {
      firstOrderNo = session.custom.firstOrderNo;
      order = Order.get(firstOrderNo);

      if (!order) {
        res.redirect(URLUtils.https('Account-Show'));
      }
    }
  }

  res.redirect(URLUtils.url('Order-Confirm', 'ID', order.orderNo, 'token', order.orderToken));

  return next();
});

/**
 * ShopRunner partner landing page. Loads up an iFrame.
 */
server.use('Show', function (req, res, next) {
  res.render('shoprunner/landing');
  return next();
});

/**
 * Called by ShopRunner Javascript after a successful login.
 * Write the token (or blank token) to the session and cookie
 * after re-verifying with ShopRunner.
 */
server.get('ValidateToken', function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();
  var cart = currentBasket;
  var token;
  var validToken;

  token = req.querystring.srtoken;

  var result = require('int_shoprunner/cartridge/scripts/ShopRunnerAuth').validate(token, cart);

  session.custom.freshSRLogin = result.signin;

  res.render('shoprunner/cookiesession', {
    ValidToken: result.validToken
  });

  return next();
});

module.exports = server.exports();
