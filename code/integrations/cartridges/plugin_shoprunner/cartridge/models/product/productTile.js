'use strict';

var shoprunner = require('*/cartridge/models/product/decorators/shoprunner');

var base = require('app_storefront_base/cartridge/models/product/productTile');

function productTile(product, apiProduct, options) {
  base.call(this, product, apiProduct, options);
  shoprunner(product, apiProduct);

  return product;
}

module.exports = productTile;
