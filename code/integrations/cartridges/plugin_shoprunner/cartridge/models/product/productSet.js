'use strict';

var shoprunner = require('*/cartridge/models/product/decorators/shoprunner');

var base = require('app_storefront_base/cartridge/models/product/productSet');

function productSet(product, apiProduct, options, factory) {
  base.call(this, product, apiProduct, options, factory);
  shoprunner(product, apiProduct);

  return product;
}

module.exports = productSet;
