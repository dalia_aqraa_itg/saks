'use strict';

module.exports = function (object, apiProduct) {
  var shopRunnerEligible = false;

  if (apiProduct.isProduct() || apiProduct.isMaster()) {
    shopRunnerEligible = Boolean(apiProduct.custom.shopRunnerEligible);
  } else if (apiProduct.isVariant()) {
    shopRunnerEligible = Boolean(apiProduct.masterProduct.custom.shopRunnerEligible);
  }

  if (typeof object.custom === 'undefined') {
    Object.defineProperty(object, 'custom', {
      value: {
        shopRunnerEligible: shopRunnerEligible
      }
    });
  } else {
    Object.defineProperty(object.custom, 'shopRunnerEligible', {
      value: shopRunnerEligible
    });
  }
};
