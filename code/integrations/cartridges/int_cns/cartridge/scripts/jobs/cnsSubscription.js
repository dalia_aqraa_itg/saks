'use strict';
var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var CSVStreamWriter = require('dw/io/CSVStreamWriter');
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');
var system = require('dw/system');
var CustomObject = require('app_hbc_core/cartridge/models/customobject');
var customObjectModel = new CustomObject('Subscriptions');

/**
 * Create a csv file
 * @param {Object} fileName csv file name to be created
 * @return {Object} Returns a empty csv file
 */
function createFile(fileName) {
  var targetDirectory = new File('Impex/src/Subscription');
  targetDirectory.mkdirs();
  var file = new File('Impex/src/Subscription/' + fileName);
  file.createNewFile();
  return file;
}

/**
 * Create a header for csv file
 * @param {Object} writer object where to write
 */
function createHeader(writer) {
  if (writer) {
    var headerArr = [];
    headerArr[0] = 'SOURCE_ID';
    headerArr[1] = 'EMAIL_ADDRESS';
    headerArr[2] = 'FIRST_NAME';
    headerArr[3] = 'MIDDLE_NAME';
    headerArr[4] = 'LAST_NAME';
    headerArr[5] = 'ADDRESS';
    headerArr[6] = 'ADDRESS_TWO';
    headerArr[7] = 'CITY';
    headerArr[8] = 'STATE';
    headerArr[9] = 'ZIP_FULL';
    headerArr[10] = 'COUNTRY';
    headerArr[11] = 'PHONE';
    headerArr[12] = 'OFF5TH_OPT_STATUS';
    headerArr[13] = 'SAKS_OPT_STATUS';
    headerArr[14] = 'SAKS_CANADA_OPT_STATUS';
    headerArr[15] = 'OFF5TH_CANADA_OPT_STATUS';
    headerArr[16] = 'THE_BAY_OPT_STATUS';
    headerArr[17] = 'SUBSCRIBED/UNSUBSCRIBED';
    headerArr[18] = 'LANGUAGE';
    headerArr[19] = 'BANNER';
    headerArr[20] = 'CANADA_FLAG';
    headerArr[21] = 'SAKS_FAMILY_OPT_STATUS';
    headerArr[22] = 'MORE_NUMBER';
    headerArr[23] = 'HBC_REWARDS_NUMBER';
    headerArr[24] = 'BIRTHDAY';
    headerArr[25] = 'GENDER';
    // Write Header
    writer.writeNext(headerArr);
  }
}

/**
 * Create a body for csv file
 * @param {Object} writer object where to write
 * @param {Object} co custom object to write in csv
 */
function createBody(writer, co) {
  var data = [];
  data[0] = co.custom.sourceId.displayValue;
  data[1] = co.custom.emailAddress;
  data[2] = co.custom.firstName;
  data[3] = co.custom.middleName;
  data[4] = co.custom.lastName;
  data[5] = co.custom.address;
  data[6] = co.custom.addressTwo;
  data[7] = co.custom.city;
  data[8] = co.custom.state;
  data[9] = co.custom.zipFull;
  data[10] = co.custom.country;
  data[11] = co.custom.phone;
  data[12] = co.custom.offFiveThOptStatus;
  data[13] = co.custom.saksOptStatus;
  data[14] = co.custom.saksCanadaOptStatus;
  data[15] = co.custom.offFiveThCanadaOptStatus;
  data[16] = co.custom.theBayOptStatus;
  data[17] = formatDateField(co.custom['subscribedOrUnsubscribed']);
  data[18] = co.custom.language;
  data[19] = co.custom.banner;
  data[20] = co.custom.canadaFlag;
  data[21] = co.custom.saksFamilyOptStatus;
  data[22] = co.custom.moreNumber;
  data[23] = co.custom.hbcRewardsNumber;
  data[24] = formatDateField(co.custom.birthday);
  data[25] = co.custom.gender;
  writer.writeNext(data);
}

/**
 * Formats a date field
 * @param {Object} dateField
 * @returns {string}
 */
function formatDateField(dateField) {
  if (!dateField) {
    return dateField;
  }
  var Calendar = require('dw/util/Calendar');
  var StringUtils = require('dw/util/StringUtils');
  var cal = new Calendar(dateField);
  return StringUtils.formatCalendar(cal, 'yyyy-MM-dd').concat('Z');
}

/**
 * Update custom object
 * @param {Object} map
 * @param {Object} co
 * @returns {boolean} status of update
 */
function updateSubscription(map, co) {
  return customObjectModel.updateCustom(map, co);
}

/**
 * @param {Object} map
 * @param {Object} co
 * @returns {boolean} status of update
 */
function fetchAllSubscription() {
  var records = customObjectModel.queryCustom('custom.exported = NULL OR custom.exported = false', null);
  return records;
}

/**
 * starting point for job
 * @param {Object} args All job parameters
 * @return {Object} Returns status of job
 */
function execute(args) {
  var HashMap = require('dw/util/HashMap');
  var hashMap = new HashMap();
  var records = fetchAllSubscription();
  if (records && records.count > 0) {
    var timeStamp = StringUtils.formatCalendar(new Calendar(), 'yyyy-MM-dd_HH-mm-ss');
    var writer = new CSVStreamWriter(new FileWriter(createFile('emailSubscriptionFeed' + timeStamp + '.csv')), ',');
    try {
      createHeader(writer);
      var collections = require('*/cartridge/scripts/util/collections');
      collections.forEach(records.asList(), function (co) {
        createBody(writer, co);
        hashMap.clear();
        hashMap.put('exported', true);
        updateSubscription(hashMap, co);
      });
    } catch (e) {
      return new system.Status(system.Status.ERROR, null, e);
    } finally {
      writer.close();
    }
  }
  return new system.Status(system.Status.OK);
}

module.exports = {
  execute: execute
};
