'use strict';

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var Logger = require('dw/system/Logger');
var ServiceHelper = require('*/cartridge/scripts/services/helpers/ServiceHelper');

var CNSService = function () {
  /**
   * Helper method for sending email requests
   *
   * @param {Object} data The JSON data to send in the body of the request
   * @returns {Object} response
   */
  this.sendEmail = function (data) {
    var sendEmailService = LocalServiceRegistry.createService('cns.https.send.email', {
      // eslint-disable-next-line no-unused-vars
      createRequest: function (service, params) {
        service.setRequestMethod('POST');
        service = ServiceHelper.addServiceHeaders(service); //eslint-disable-line
        return JSON.stringify(data.requestJSON);
      },
      parseResponse: function (service, httpClient) {
        return JSON.parse(httpClient.text);
      },
      filterLogMessage: function (res) {
        if (res.indexOf('email_addresses') > -1) {
          let message = JSON.parse(res);
          message.email_addresses.to = '*********@***.***';
          if (message.parameters.current_email) {
            message.parameters.current_email = '*******@***.***';
          }
          if (message.parameters.old_email) {
            message.parameters.old_email = '*******@***.***';
          }
          if (message.parameters.reset_password_link) {
            message.parameters.reset_password_link = '********************';
          }
          return JSON.stringify(message);
        }
        return res;
      },
      mockCall: function (service, httpClient) {
        return {
          statusCode: 200,
          statusMessage: 'Success',
          text: '{"status": "accepted"}'
        };
      }
    });

    var result = sendEmailService.call(JSON.stringify(data));
    if (!result.isOk() || !result.object) {
      var Result = require('dw/svc/Result');
      if (result.status === Result.SERVICE_UNAVAILABLE) {
        Logger.getRootLogger().fatal('[CNS Send Email failure] (cnsService.js) {0}', result.errorMessage);
      }
      return {
        error: true,
        errorMessage: result.errorMessage
      };
    }
    return result.object;
  };
};
module.exports = CNSService;
