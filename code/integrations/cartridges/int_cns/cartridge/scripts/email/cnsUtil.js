'use strict';
var cnsService = new (require('*/cartridge/scripts/email/cnsService'))();
var preferences = require('*/cartridge/config/preferences');

/**
 * Request JSON build function
 *
 * @param {dw/customer/Customer} customer - Current customer
 * @param {Object} parameters - email parameters
 * @param {string} subtype - email subtype
 * @param {string} banner - email banner
 * @param {string} language - current locale language
 * @returns {Object} Email request
 */
function buildRequestJSON(customer, parameters, subtype, banner, language) {
  var emailRequest = {
    email_addresses: {
      to: [customer.email],
      cc: [],
      bcc: []
    },
    first_name: customer.firstName,
    last_name: customer.lastName,
    banner: banner,
    language: language,
    event_subtype: subtype,
    event: preferences.cnsEventType
  };
  emailRequest.parameters = parameters;
  return emailRequest;
}

/**
 * Helper method for sending emails
 * @param {dw/customer/Customer} customer - Current customer
 * @param {Object} parameters - email parameters
 * @param {string} subtype - email subtype
 * @param {string} language - current locale language
 * @returns {Object} result
 */
function sendEmail(customer, parameters, subtype, language) {
  var reqPayLoad = {};
  reqPayLoad.apiKey = preferences.hbcAPIKey;
  reqPayLoad.apiId = preferences.hbcAPIID;
  var banner = preferences.cnsBanner;
  var requestJSON = buildRequestJSON(customer, parameters, subtype, banner, language.toLocaleUpperCase());
  reqPayLoad.requestJSON = requestJSON;
  var result = cnsService.sendEmail(reqPayLoad);
  return result;
}

exports.sendEmail = sendEmail;
