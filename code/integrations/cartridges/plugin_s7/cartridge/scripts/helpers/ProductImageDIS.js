/**
 * This script provides a wrapper for Product Image API so transformation rules can be retrieved
 * from preferences without having any effect on the actual code. That helps to easily adjust
 * transformation settings and introduce new view types.
 *
 * ---------------
 *
 * How to use the image wrapper?
 *
 * var ProductImageDIS = require('/cartridge/scripts/helpers/ProductImageDIS');  //create wrapper object with product and defined viewtype
 *
 * // create the image API facades
 * var images = ProductImageDIS.getImages(product, type);   // get all images of a given product and specified viewType
 * var image11 = new ProductImageDIS(product, type, 10);    // get the 11th image of the given product and specified viewType
 * var image = new ProductImageDIS(product, type, 0);       // get the first image of the given product and specified viewType (index == 0 || undefined) get the first image
 *
 * // using the image API facades is SFCC API neutral,
 * // i.e. as soon as you have the initialized object,
 * // you can simply use it as it is!!!
 * url: image.getURL();         // get dynamic imaging service url as string
 * url: image.getHttpsURL();    // get dynamic imaging service https url as string
 * title: image.getTitle();     // get image title
 * image.getAlt();              // get image alt text
 *
 * ---------------
 *
 * How to configure the Dynamic Imaging Service Wrapper?
 *
 * The configuration should be in cartridge/preferences/image_config_s7.json
 * Configure viewTypes, viewTypeMapping and missing images.
 *
 * ViewType example:
 * {
 *   "viewTypeMapping": {   // maps the to be scaled view types to the scalable view type
 *       "medium": "large", // when requested, transform viewType "medium" based on viewType "large"
 *       "small": "large"   // when requested, transform viewType "small" based on viewType "large"
 *   },
 *   "medium": {            // defines the transformation parameters for the scaled view type 'medium'
 *       "scaleWidth": 400,
 *       "scaleHeight": 400,
 *       "scaleMode": "fit"
 *   },
 *   "small": {              // defines the transformation parameters for the scaled view type 'medium'
 *       "scaleWidth": 100,
 *       "scaleHeight": 100
 *   },
 *   "missingImages": {      // defines fallback images for the respective viewTypes
 *       "large": "large_missing.jpg",
 *       "medium": "medium_missing.jpg",
 *       "small": "small_missing.jpg"
 *   }
 * }
 *
 * See https://documentation.demandware.com/DOC2/topic/com.demandware.dochelp/ImageManagement/CreatingImageTransformationURLs.html for a complete list of transformation parameters
 */

var URLUtils = require('dw/web/URLUtils');
var URL = require('dw/web/URL');
var ArrayList = require('dw/util/ArrayList');
var Site = require('dw/system/Site');
var preferences = require('*/cartridge/config/preferences');

// var System = require('dw/system/System');
var ProductVariationAttributeValue = require('dw/catalog/ProductVariationAttributeValue');
var ProductVariationModel = require('dw/catalog/ProductVariationModel');
// configuration 'singleton' for the duration of a request
var cvS7Configuration = require('*/cartridge/preferences/image_config_S7');

/**
 * Initializes the ProductImage wrapper object
 *
 * @param {String} viewType type of view (resolution) that should be generated (required)
 * @param {Object} imageObject Product or ProductVariationAttributeValue (required)
 * @param {number} index Number position of the image in the list of images for the view type. Defaults to 0 if not provided
 *
 * @returns {Object} Initialized Image object that is MedialFile API neutral
 */
function ProductImage(imageObject, viewType, index) {
  // init basic object attributes
  // --> keeps the image reference
  this.image = null;
  // --> defines if image needs to be scaled
  this.scaleImage = false;
  // --> view type that should be rendered
  this.viewType = viewType;
  // --> the image object --> escape empty object
  this.imageObject = imageObject;

  if (this.imageObject === null) {
    return;
  }

  // Check what type of image object the wrapper got
  if (this.imageObject instanceof ProductVariationAttributeValue) {
    this.referenceType = 'ProductVariationAttributeValue';
  } else if (this.imageObject instanceof ProductVariationModel) {
    this.referenceType = 'Product';
    this.imageObject = imageObject.selectedVariants.length > 0 ? imageObject.selectedVariants[0] : imageObject.master;
  } else {
    this.referenceType = 'Product';
  }

  // --> the view type that can be scaled - typically high resolution
  this.scalableViewType = null;
  this.highResViewType = null;
  this.highResModelViewType = null;
  // --> index of the image
  this.index = !index ? 0 : index;
  // --> defines if the image needs to be scaled. That's not necessary if a product has an image for the given view type configured
  this.scaleImage = false;

  this.transformationObj = cvS7Configuration.hasOwnProperty(viewType) ? cvS7Configuration[viewType] : {};

  var transobj = this.transformationObj;

  // determine the scaleableImageType that correspoonds with the viewType
  // set the default viewtype if no specific configuration was found
  this.scalableViewType = this.viewType;
  this.highResViewType = this.viewType;
  this.highResModelViewType = this.viewType;

  // use the JSON configuration in 'disConfiguration' to determine scaleableImageType
  if (Object.prototype.hasOwnProperty.call(cvS7Configuration, 'viewTypeMapping') && cvS7Configuration.viewTypeMapping[this.viewType]) {
    this.scalableViewType = cvS7Configuration.viewTypeMapping[this.viewType];
  }

  if (Object.prototype.hasOwnProperty.call(cvS7Configuration, 'highResMapping') && cvS7Configuration.highResMapping[this.viewType]) {
    this.highResViewType = cvS7Configuration.highResMapping[this.viewType];
  }
  if (Object.prototype.hasOwnProperty.call(cvS7Configuration, 'highResModelMapping') && cvS7Configuration.highResModelMapping[this.viewType]) {
    this.highResModelViewType = cvS7Configuration.highResModelMapping[this.viewType];
  }

  this.highResTransformationObj = cvS7Configuration.hasOwnProperty(this.highResViewType) ? cvS7Configuration[this.highResViewType] : {};

  this.highResModelTransformationObj = cvS7Configuration.hasOwnProperty(this.highResModelViewType) ? cvS7Configuration[this.highResModelViewType] : {};

  this.scaleableImage = this.imageObject.getImage(this.scalableViewType, this.index);
  this.scalableModelImage = this.imageObject.getImage('hi-res-model', this.index);

  var scaleViewtyp = this.scalableViewType;
  var scalableimg = this.scaleableImage;
  // Get the image for the image object if not only test images should be used

  this.image = this.imageObject.getImage(this.viewType, this.index);
  var normalImg = this.image;
  if (!this.image) {
    // there hasn't been a image configured and we fall back to the highres one which needs to be scaled
    this.image = this.scaleableImage;
    if ((this.viewType !== 'swatch' || this.viewType !== 'video') && this.viewType.indexOf('hiresmodel') > -1) {
      this.image = this.scalableModelImage;
    }
    this.scaleImage = true;
  }
  this.alt = this.getAlt();
  this.title = this.getTitle();
}

/** ***********************************************************************************************
 ******************************** API Methods *****************************************************
 ************************************************************************************************ */

ProductImage.prototype.getURL = function () {
  if (this.imageObject === null) {
    return null;
  }
  return this.getImageURL();
};

ProductImage.prototype.getHttpURL = function () {
  if (this.imageObject === null) {
    return null;
  }
  return this.getImageURL('Http');
};

ProductImage.prototype.getHttpsURL = function () {
  if (this.imageObject === null) {
    return null;
  }
  return this.getImageURL('Https');
};

ProductImage.prototype.getAbsURL = function () {
  if (this.imageObject === null) {
    return null;
  }
  return this.getImageURL('Abs');
};

ProductImage.prototype.getHighResURL = function () {
  if (this.imageObject === null) {
    return null;
  }
  return this.getHighResImageURL();
};

ProductImage.prototype.getHighResModelURL = function () {
  if (this.imageObject === null) {
    return null;
  }
  return this.getHighResModelImageURL();
};

/**
 * Gets the actual image URL for different API calls.
 *
 * @param {String} imageFunctionID
 */
ProductImage.prototype.getImageURL = function (imageFunctionID) {
  if (this.imageObject === null) {
    return null;
  }

  var imageURL = null;
  var finalStaticFunctionID = imageFunctionID ? imageFunctionID.toLowerCase() + 'Static' : 'staticURL';
  if (!this.image) {
    // check if test images should be used --> makes sense in cases where the product images haven't yet been configured
    let testImage = null;
    if (cvS7Configuration.missingImages) {
      if (cvS7Configuration.missingImages[Site.current.ID][this.viewType]) {
        testImage = cvS7Configuration.missingImages[Site.current.ID][this.viewType];
        this.scaleImage = false;
      }
      if (!testImage && this.scalableViewType !== this.viewType && cvS7Configuration.missingImages[this.scalableViewType]) {
        testImage = cvS7Configuration.missingImages[Site.current.ID][this.scalableViewType];
        this.scaleImage = true;
      }
    }
    if (testImage) {
      // if (this.scaleImage) {
      //     imageURL = URLUtils[imageFunctionID ? (imageFunctionID.toLowerCase() + 'Static') : 'imageURL'](URLUtils.CONTEXT_SITE, Site.current.ID, testImage, this.transformationObj);
      // } else {
      //     imageURL = URLUtils[finalStaticFunctionID](URLUtils.CONTEXT_SITE, Site.current.ID, testImage);
      // }

      return this.getFinalUrlAsString(testImage, cvS7Configuration[this.viewType]);
    }

    return URLUtils[finalStaticFunctionID]('/images/noimage' + this.viewType + '.png');
  }
  if (this.scaleImage) {
    return this.getFinalUrlAsString(
      this.image[imageFunctionID ? 'get' + imageFunctionID + 'ImageURL' : 'getImageURL'](this.transformationObj),
      this.transformationObj
    );
  }

  return this.getFinalUrlAsString(this.image[imageFunctionID ? 'get' + imageFunctionID + 'URL' : 'getURL'](), this.transformationObj);
};

/**
 * Gets the actual image URL for different API calls.
 *
 * @param {String} imageFunctionID
 */
ProductImage.prototype.getHighResImageURL = function (imageFunctionID) {
  if (this.imageObject === null) {
    return null;
  }

  var imageURL = null;
  var finalStaticFunctionID = imageFunctionID ? imageFunctionID.toLowerCase() + 'Static' : 'staticURL';
  if (!this.image) {
    // check if test images should be used --> makes sense in cases where the product images haven't yet been configured
    let testImage = null;
    if (cvS7Configuration.missingImages) {
      if (cvS7Configuration.missingImages[Site.current.ID][this.viewType]) {
        testImage = cvS7Configuration.missingImages[Site.current.ID][this.viewType];
        this.scaleImage = false;
      }
      if (!testImage && this.scalableViewType !== this.viewType && cvS7Configuration.missingImages[this.scalableViewType]) {
        testImage = cvS7Configuration.missingImages[Site.current.ID][this.scalableViewType];
        this.scaleImage = true;
      }
    }
    if (testImage) {
      // if (this.scaleImage) {
      //     imageURL = URLUtils[imageFunctionID ? (imageFunctionID.toLowerCase() + 'Static') : 'imageURL'](URLUtils.CONTEXT_SITE, Site.current.ID, testImage, this.transformationObj);
      // } else {
      //     imageURL = URLUtils[finalStaticFunctionID](URLUtils.CONTEXT_SITE, Site.current.ID, testImage);
      // }

      return this.getFinalUrlAsString(testImage, cvS7Configuration[this.highResTransformationObj]);
    }

    return URLUtils[finalStaticFunctionID]('/images/noimage' + this.viewType + '.png');
  }
  if (this.scaleImage) {
    return this.getFinalUrlAsString(
      this.image[imageFunctionID ? 'get' + imageFunctionID + 'ImageURL' : 'getImageURL'](this.highResTransformationObj),
      this.highResTransformationObj
    );
  }

  return this.getFinalUrlAsString(this.image[imageFunctionID ? 'get' + imageFunctionID + 'URL' : 'getURL'](), this.highResTransformationObj);
};

/**
 * Gets the actual image URL for different API calls.
 *
 * @param {String} imageFunctionID
 */
ProductImage.prototype.getHighResModelImageURL = function (imageFunctionID) {
  if (this.imageObject === null) {
    return null;
  }
  if (this.scalableModelImage) {
    return this.getFinalUrlAsString(
      this.scalableModelImage[imageFunctionID ? 'get' + imageFunctionID + 'ImageURL' : 'getImageURL'](this.highResModelTransformationObj),
      this.highResModelTransformationObj
    );
  }

  return null;
};

/**
 * If a URL replacement is used it would return the final URL, otherwise the given URL object
 */

ProductImage.prototype.getFinalUrlAsString = function (imageURL, transformationObject) {
  var LargeConfigObj = cvS7Configuration.hasOwnProperty('large') ? cvS7Configuration.large : {};
  var mediumConfigObj = cvS7Configuration.hasOwnProperty('medium') ? cvS7Configuration.medium : {};
  var smallConfigObj = cvS7Configuration.hasOwnProperty('small') ? cvS7Configuration.small : {};
  var LargeConfigObjhighres = cvS7Configuration.hasOwnProperty('large-highres') ? cvS7Configuration.largehighres : {};
  var mediumConfigObjhighres = cvS7Configuration.hasOwnProperty('medium-highres') ? cvS7Configuration.mediumhighres : {};
  var smallConfigObjhighres = cvS7Configuration.hasOwnProperty('small-highres') ? cvS7Configuration.smallhighres : {};
  var LargeConfigObjhighresmodel = cvS7Configuration.hasOwnProperty('largehighresmodel') ? cvS7Configuration.largehighresmodel : {};
  var mediumConfigObjhighresmodel = cvS7Configuration.hasOwnProperty('mediumhighresmodel') ? cvS7Configuration.mediumhighresmodel : {};
  var smallConfigObjhighresmodel = cvS7Configuration.hasOwnProperty('smallhighresmodel') ? cvS7Configuration.smallhighresmodel : {};

  let swatchConfigObj = Object.prototype.hasOwnProperty.call(cvS7Configuration, 'swatch') ? cvS7Configuration.swatch : {};
  var imgRelativePath = preferences.imageRelativePath;
  var videoRelativePath = preferences.videoRelativePath;
  var imageSharpeningParams = preferences.imageSharpeningParams ? JSON.parse(preferences.imageSharpeningParams) : {};

  // In case the site preference is set, update the instance used as image source
  let currentImgUrl = imageURL.toString();
  if ((!empty(currentImgUrl) && currentImgUrl.indexOf(imgRelativePath) >= 0 && imageURL instanceof URL) || empty(currentImgUrl)) {
    // this is an image URL
    var preset;
    if (Object.keys(imageSharpeningParams).length > 0) {
      if (LargeConfigObj.scaleWidth === transformationObject.scaleWidth && imageSharpeningParams.large) {
        preset = '?wid=' + transformationObject.scaleWidth + '&hei=' + transformationObject.scaleHeight + imageSharpeningParams.large;
      } else if (mediumConfigObj.scaleWidth === transformationObject.scaleWidth && imageSharpeningParams.medium) {
        preset = '?wid=' + transformationObject.scaleWidth + '&hei=' + transformationObject.scaleHeight + imageSharpeningParams.medium;
      } else if (smallConfigObj.scaleWidth === transformationObject.scaleWidth && imageSharpeningParams.small) {
        preset = '?wid=' + transformationObject.scaleWidth + '&hei=' + transformationObject.scaleHeight + imageSharpeningParams.small;
      } else if (swatchConfigObj.scaleWidth === transformationObject.scaleWidth && imageSharpeningParams.swatch) {
        preset = '?wid=' + transformationObject.scaleWidth + '&hei=' + transformationObject.scaleHeight + imageSharpeningParams.swatch;
      } else if (LargeConfigObjhighres.scaleWidth === transformationObject.scaleWidth && imageSharpeningParams.largehighres) {
        preset = '?wid=' + transformationObject.scaleWidth + '&hei=' + transformationObject.scaleHeight + imageSharpeningParams.largehighres;
      } else if (LargeConfigObjhighresmodel.scaleWidth === transformationObject.scaleWidth && imageSharpeningParams.largehighresmodel) {
        preset = '?wid=' + transformationObject.scaleWidth + '&hei=' + transformationObject.scaleHeight + imageSharpeningParams.largehighresmodel;
      } else if (mediumConfigObjhighres.scaleWidth === transformationObject.scaleWidth && imageSharpeningParams.mediumhighres) {
        preset = '?wid=' + transformationObject.scaleWidth + '&hei=' + transformationObject.scaleHeight + imageSharpeningParams.mediumhighres;
      } else if (mediumConfigObjhighresmodel.scaleWidth === transformationObject.scaleWidth && imageSharpeningParams.mediumhighresmodel) {
        preset = '?wid=' + transformationObject.scaleWidth + '&hei=' + transformationObject.scaleHeight + imageSharpeningParams.mediumhighresmodel;
      } else if (smallConfigObjhighres.scaleWidth === transformationObject.scaleWidth && imageSharpeningParams.smallhighres) {
        preset = '?wid=' + transformationObject.scaleWidth + '&hei=' + transformationObject.scaleHeight + imageSharpeningParams.smallhighres;
      } else if (smallConfigObjhighresmodel.scaleWidth === transformationObject.scaleWidth && imageSharpeningParams.smallhighresmodel) {
        preset = '?wid=' + transformationObject.scaleWidth + '&hei=' + transformationObject.scaleHeight + imageSharpeningParams.smallhighresmodel;
      } else {
        preset = '?wid=' + transformationObject.scaleWidth + '&hei=' + transformationObject.scaleHeight + '&qlt=90&resMode=sharp2&op_usm=0.9,1.0,8,0';
      }
    } else {
      preset = '?wid=' + transformationObject.scaleWidth + '&hei=' + transformationObject.scaleHeight + '&qlt=90&resMode=sharp2&op_usm=0.9,1.0,8,0';
    }
    let finalURL = currentImgUrl.concat(preset);
    return finalURL;
  } else if (!empty(currentImgUrl) && currentImgUrl.indexOf(videoRelativePath) >= 0) {
    return currentImgUrl;
  }
  // if there is a hex color in image path, e.g. in swatches
  if (!empty(currentImgUrl) && currentImgUrl.lastIndexOf('/') !== -1) {
    return '#' + currentImgUrl.substr(currentImgUrl.lastIndexOf('/') + 1);
  }

  let s7ImageHostURL = preferences.s7ImageHostURL;
  if (transformationObject != null) {
    var imgURL;
    if (Object.keys(imageSharpeningParams).length > 0) {
      if (LargeConfigObj.scaleWidth === transformationObject.scaleWidth && imageSharpeningParams.large) {
        imgURL =
          s7ImageHostURL +
          imgRelativePath +
          '/' +
          imageURL +
          '?wid=' +
          transformationObject.scaleWidth +
          '&hei=' +
          transformationObject.scaleHeight +
          imageSharpeningParams.large;
      } else if (mediumConfigObj.scaleWidth === transformationObject.scaleWidth && imageSharpeningParams.medium) {
        imgURL =
          s7ImageHostURL +
          imgRelativePath +
          '/' +
          imageURL +
          '?wid=' +
          transformationObject.scaleWidth +
          '&hei=' +
          transformationObject.scaleHeight +
          imageSharpeningParams.medium;
      } else if (smallConfigObj.scaleWidth === transformationObject.scaleWidth && imageSharpeningParams.small) {
        imgURL =
          s7ImageHostURL +
          imgRelativePath +
          '/' +
          imageURL +
          '?wid=' +
          transformationObject.scaleWidth +
          '&hei=' +
          transformationObject.scaleHeight +
          imageSharpeningParams.small;
      } else if (swatchConfigObj.scaleWidth === transformationObject.scaleWidth && imageSharpeningParams.swatch) {
        imgURL =
          s7ImageHostURL +
          imgRelativePath +
          '/' +
          imageURL +
          '?wid=' +
          transformationObject.scaleWidth +
          '&hei=' +
          transformationObject.scaleHeight +
          imageSharpeningParams.swatch;
      } else if (LargeConfigObjhighres.scaleWidth === transformationObject.scaleWidth && imageSharpeningParams.largehighres) {
        imgURL =
          s7ImageHostURL +
          imgRelativePath +
          '/' +
          imageURL +
          '?wid=' +
          transformationObject.scaleWidth +
          '&hei=' +
          transformationObject.scaleHeight +
          imageSharpeningParams.largehighres;
      } else if (mediumConfigObjhighres.scaleWidth === transformationObject.scaleWidth && imageSharpeningParams.mediumhighres) {
        imgURL =
          s7ImageHostURL +
          imgRelativePath +
          '/' +
          imageURL +
          '?wid=' +
          transformationObject.scaleWidth +
          '&hei=' +
          transformationObject.scaleHeight +
          imageSharpeningParams.mediumhighres;
      } else if (smallConfigObjhighres.scaleWidth === transformationObject.scaleWidth && imageSharpeningParams.smallhighres) {
        imgURL =
          s7ImageHostURL +
          imgRelativePath +
          '/' +
          imageURL +
          '?wid=' +
          transformationObject.scaleWidth +
          '&hei=' +
          transformationObject.scaleHeight +
          imageSharpeningParams.smallhighres;
      } else {
        imgURL =
          s7ImageHostURL +
          imgRelativePath +
          '/' +
          imageURL +
          '?wid=' +
          transformationObject.scaleWidth +
          '&hei=' +
          transformationObject.scaleHeight +
          '&qlt=90&resMode=sharp2&op_usm=0.9,1.0,8,0';
      }
    } else {
      imgURL =
        s7ImageHostURL +
        imgRelativePath +
        '/' +
        imageURL +
        '?wid=' +
        transformationObject.scaleWidth +
        '&hei=' +
        transformationObject.scaleHeight +
        '&qlt=90&resMode=sharp2&op_usm=0.9,1.0,8,0';
    }
    return imgURL;
  }
  return s7ImageHostURL + imgRelativePath + imageURL;
};

/**
 * Gets the tile for images.
 */
ProductImage.prototype.getTitle = function () {
  if (this.imageObject === null) {
    return null;
  }
  if (this.referenceType === 'ProductVariationAttributeValue' && this.viewType === 'swatch') {
    return this.imageObject.displayValue;
  }
  if (!this.image || !this.image.title) {
    if (cvS7Configuration.imageMissingText) {
      return cvS7Configuration.imageMissingText;
    } else if (this.referenceType === 'Product') {
      return this.imageObject.name;
    }
    return this.imageObject.displayValue;
  }
  return this.image.title;
};

/**
 * Gets the alternative text for images.
 */
ProductImage.prototype.getAlt = function () {
  if (this.imageObject === null) {
    return null;
  }
  if (this.referenceType === 'ProductVariationAttributeValue' && this.viewType === 'swatch') {
    return this.imageObject.displayValue;
  }
  if (!this.image || !this.image.alt) {
    // same as above
    if (cvS7Configuration.imageMissingText) {
      return cvS7Configuration.imageMissingText;
    } else if (this.referenceType === 'Product') {
      var ref = this.referenceType;
      var img = this.imageObject;
      return this.imageObject.name;
    }
    return this.imageObject.displayValue;
  }
  return this.image.alt;
};

/**
 * Gets all images for the given view type and image object
 */
ProductImage.prototype.getImages = function () {
  return this.getAllImages();
};

/**
 * Returns a Collection of ProductImage Instances of the productimages assigned for this viewtype.
 */
ProductImage.prototype.getAllImages = function () {
  var result = new ArrayList();
  if (this.imageObject !== null) {
    var images = this.viewType !== 'swatch' || this.viewType !== 'video' ? this.imageObject.getImages(this.scalableViewType) : null;
    var highResModel = this.imageObject.getImage('hi-res-model', 0);
    if (images) {
      for (let i = 0; i < images.length; i++) {
        // i=0
        // 1stimage
        if (i === this.index) {
          result.add(this);
        } else {
          result.add(ProductImage.getImage(this.imageObject, this.viewType, i));
        }
        if ((images.length == 1 || i == 0) && highResModel && this.viewType !== 'swatch' && this.viewType !== 'video') {
          result.add(ProductImage.getImage(this.imageObject, this.viewType + 'highresmodel', 0));
          highResModel = null; //invalidate variable
        }
      }
    }
  }
  return result;
};

/**
 * Gets a new Product Image Wrapper object if it hasn't been initialized during the request,
 * otherwise the already initialzed version will be returned.
 *
 * @param {String} viewType Type of view (resolution) that should be generated (required)
 * @param {Object} imageObject Product or ProductVariationAttributeValue(required)
 * @param {Number} index Position of the image in the list of images for the view type. Defaults to 0 if not provided
 *
 * @returns {ProductImage} A wrapped image that does not need to be initialized if already created in context of the current request.
 */
ProductImage.getImage = function (imageObject, viewType, index) {
  if (!imageObject || !viewType) {
    return null;
  }
  return new ProductImage(imageObject, viewType, index);
};

/**
 * Gets a all images for a given image object
 *
 * @param {String} viewType Type of view (resolution) that should be generated (required)
 * @param {Object} imageObject Product or ProductVariationAttributeValue(required)
 *
 * @returns {Collection} Collection of images assiciated with the image object and the view type
 */
ProductImage.getImages = function (imageObject, viewType) {
  if (!imageObject || !viewType) {
    return null;
  }
  return ProductImage.getImage(imageObject, viewType, 0).getImages();
};

module.exports = ProductImage;
