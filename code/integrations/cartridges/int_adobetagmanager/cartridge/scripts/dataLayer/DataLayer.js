/* API Includes */
var CatalogMgr = require('dw/catalog/CatalogMgr');
var ProductMgr = require('dw/catalog/ProductMgr');
var StringUtils = require('dw/util/StringUtils');
var ArrayList = require('dw/util/ArrayList');
var system = require('dw/system/System');
var Resource = require('dw/web/Resource');
var OrderMgr = require('dw/order/OrderMgr');
var Cookie = require('dw/web/Cookie');

var collections = require('*/cartridge/scripts/util/collections');

/**
 * Returns the rendering template name for particular category
 * @param {dw.catalog.Category} category - Current category
 * @returns {string} rendering template name
 */
function getRenderingTemplate(category) {
  var renderingTemplatePath = category ? category.getTemplate() : null;
  var renderTemplate = renderingTemplatePath ? renderingTemplatePath.split('/')[renderingTemplatePath.split('/').length - 1] : null;

  return renderTemplate;
}

/**
 * Fetches Email Opt in
 * @param {dw.customer.Profile} profile
 * @returns {string} status of email subscription
 */
function provideEmailOpt(profile) {
  var emailOptIn = 'emailOptIn' in profile.custom ? profile.custom.emailOptIn : false;
  var saksAvenueOptIn = 'saksAvenueOptIn' in profile.custom ? profile.custom.saksAvenueOptIn : false;
  var saksAvenueOptInCA = 'saksAvenueOptInCA' in profile.custom ? profile.custom.saksAvenueOptInCA : false;
  var saksOptIn = 'saksOptIn' in profile.custom ? profile.custom.saksOptIn : false;
  var saksOptInCA = 'saksOptInCA' in profile.custom ? profile.custom.saksOptInCA : false;
  return emailOptIn || saksAvenueOptIn || saksAvenueOptInCA || saksOptIn || saksOptInCA;
}

/**
 * Set the recurring data of the page to the globalData object variable
 * @param {Object} globalData - Global Object
 */
function setRecurringData(globalData) {
  var server = require('server');
  var QueryString = server.querystring;

  var MessageDigest = require('dw/crypto/MessageDigest');
  var WeakMessageDigest = require('dw/crypto/WeakMessageDigest');
  var messageDigestMD5 = new WeakMessageDigest(WeakMessageDigest.DIGEST_MD5);
  var messageDigetstSHA_256 = new MessageDigest(MessageDigest.DIGEST_SHA_256);

  globalData.site = {};
  globalData.visitor = {};

  var isProdInstance = system.getInstanceType() == system.PRODUCTION_SYSTEM;
  var customerObject = customer;
  var customerNo = customerObject && customerObject.profile ? customerObject.profile.customerNo : '';
  var customerEmail = customerObject && customerObject.profile ? customerObject.profile.getEmail() : null;
  var customerEmailSHA256 = customerEmail ? dw.crypto.Encoding.toHex(messageDigetstSHA_256.digestBytes(new dw.util.Bytes(customerEmail))) : '';
  var customerEmailMD5 = customerEmail ? dw.crypto.Encoding.toHex(messageDigestMD5.digestBytes(new dw.util.Bytes(customerEmail))) : '';
  var currentBasket = dw.order.BasketMgr.getCurrentBasket();
  var queryStringObject = request.httpQueryString ? new QueryString(request.httpQueryString) : '';
  var newAccount = queryStringObject.querystring ? queryStringObject.querystring.indexOf('submitted') > -1 : false;
  globalData.site.is_production = isProdInstance.toString();

  switch (dw.system.Site.getCurrent().ID) {
    case 'TheBay':
      globalData.site.name = 'thebay.com';
      break;
    case 'SaksOff5th':
      globalData.site.name = 'off5th.com';
      break;
    case 'SaksFifthAvenue':
      globalData.site.name = 'saks.com';
      break;
    default:
      break;
  }
  globalData.visitor.account_id = customerNo;
  globalData.visitor.bag_id = currentBasket ? currentBasket.getUUID() : '';
  globalData.visitor.logged_in_status = customerObject.authenticated ? 'registered' : 'anonymous';

  var loyaltyID = '';
  if (customerObject.profile && 'hudsonReward' in customerObject.profile.custom) {
    loyaltyID = customerObject.profile.custom.hudsonReward;
  }
  if (customerObject.profile && 'moreCustomer' in customerObject.profile.custom) {
    loyaltyID = customerObject.profile.custom.moreCustomer;
  }
  globalData.visitor.loyalty_id = loyaltyID;

  globalData.visitor.MD5_hash_email_address = customerEmailMD5;
  if ('action' in queryStringObject && queryStringObject.action === 'Account-Show') {
    globalData.visitor.new_account = newAccount.toString();
    if (loyaltyID) {
      if (customerObject.profile.custom.isLinkedAccount) {
        globalData.visitor.linked_loyalty = 'true';
      } else if (customerObject.profile.custom.isLinkedAccount === false) {
        // explicit boolean check since isLinkedAccount could be null or undefined
        globalData.visitor.new_loyalty = 'true';
      }
    }
  } else if (
    ('action' in queryStringObject && queryStringObject.action === 'Account-Profile') ||
    queryStringObject.action === 'Loyalty-EnrollForm' ||
    queryStringObject.action === 'Loyalty-LinkAccountForm'
  ) {
    globalData.visitor.email_opt_in = (queryStringObject.querystring ? queryStringObject.querystring.indexOf('save') > -1 : false).toString();
    globalData.visitor.receives_emails = customerObject.profile ? provideEmailOpt(customerObject.profile).toString() : 'false';
    // handle opt-out scenario
    if (globalData.visitor.receives_emails === 'false') {
      globalData.visitor.email_opt_in = 'false';
    }
  } else if ('action' in queryStringObject && queryStringObject.action === 'PaymentInstruments-List') {
    if (
      getParamsFromQueryString(queryStringObject.querystring, 'new-payment') &&
      getParamsFromQueryString(queryStringObject.querystring, 'new-payment') === 'true'
    ) {
      globalData.visitor.new_payment = 'true';
    }
  }

  globalData.visitor.personalization_score = '';
  globalData.visitor.SHA256_hash_email_address = customerEmailSHA256;

  // set additional visitor data
  if ('action' in queryStringObject && queryStringObject.action === 'Order-Confirm') {
    if (queryStringObject.querystring && getParamsFromQueryString(queryStringObject.querystring, 'ID')) {
      var orderID = getParamsFromQueryString(queryStringObject.querystring, 'ID');
      var order = OrderMgr.getOrder(orderID);
      globalData.visitor.email_address = order.getCustomerEmail() ? order.getCustomerEmail() : '';
      globalData.visitor.first_name = order.getBillingAddress().getFirstName() ? order.getBillingAddress().getFirstName() : '';
      globalData.visitor.last_name = order.getBillingAddress().getLastName() ? order.getBillingAddress().getLastName() : '';
    }
  }

  return;
}

/**
 * Set the category hierarchy to the globalData object variable
 * @param {Object} globalData - Global Object
 * @param {dw.catalog.Category} category - Current category
 */
function setCatHierarchy(globalData, category) {
  // handle Hidden Category for content
  var parentContentCat = category;
  while (!parentContentCat.topLevel && parentContentCat.ID !== 'root') {
    parentContentCat = parentContentCat.getParent();
    if (parentContentCat.ID === 'content' || parentContentCat.ID === 'editorial') {
      var pageName = category.displayName !== null ? category.displayName : category.ID;
      var pageType = parentContentCat.displayName !== null ? parentContentCat.displayName : parentContentCat.ID;
      globalData.page.name = pageName;
      globalData.page.type = pageType;
      return;
    }
  }

  // handle normal categories
  var cat = [];
  globalData.page.categories = [];
  var categoryName = category.custom.defaultName ? category.custom.defaultName : category.getDisplayName();
  cat.push(categoryName);
  var parentCat = category;
  while (!parentCat.topLevel && parentCat.ID !== 'root') {
    parentCat = parentCat.getParent();
    var parentCatName = parentCat.custom.defaultName ? parentCat.custom.defaultName : parentCat.getDisplayName();
    cat.push(parentCatName);
  }
  globalData.page.categories = cat.reverse();
  globalData.page.type = 'section';
  return;
}

/**
 * Get top level category from the current category
 * @param {dw.catalog.Category} category - Current category
 */
function getTopLevelCategory(category) {
  var parentCat = category;
  while (!parentCat.topLevel) {
    parentCat = parentCat.getParent();
  }
  return parentCat;
}

/**
 * Get refinement values from the refinement definition
 * @param {dw.catalog.ProductSearchModel} productSearch
 * @param {dw.catalog.ProductSearchRefinements} refinements - Search refinements
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinitions
 * @returns {Object} refinement values
 */

function getRefinements(productSearch, refinements, refinementDefinitions) {
  return collections.map(refinementDefinitions, function (definition) {
    var refinementValues = refinements.getAllRefinementValues(definition);
    var searchRefinementsFactory = require('*/cartridge/scripts/factories/searchRefinements');
    var values = searchRefinementsFactory.get(productSearch, definition, refinementValues);

    return {
      displayName: definition.displayName,
      isCategoryRefinement: definition.categoryRefinement,
      isAttributeRefinement: definition.attributeRefinement,
      isPriceRefinement: definition.priceRefinement,
      values: values
    };
  });
}

/**
 * Prepare refinements array object for data layer from the refinements
 * @param {dw.catalog.ProductSearchRefinements} refinements - Search refinements
 * @returns {Object} refinement values
 */
function getRefinementsArray(refinements) {
  var refinementsArray = [];

  Object.keys(refinements).forEach(function (key) {
    var refineObject = {};
    refineObject.name = refinements[key].displayName;
    refineObject.values = [];
    var refinementValues = refinements[key].values;
    Object.keys(refinementValues).forEach(function (key) {
      if (refinementValues[key].selected) {
        var valueObject = {};
        valueObject.name = refinementValues[key].displayValue;
        valueObject.type = 'selected';
        refineObject.values.push(valueObject);
      }
      if (refinementValues[key].type === 'category' && refinementValues[key].subCategories != null && refinementValues[key].subCategories.length > 0) {
        refinementValues[key].subCategories.forEach(function (categoryRefinement) {
          if (categoryRefinement.selected) {
            var valueObject = {};
            valueObject.name = categoryRefinement.displayValue;
            valueObject.type = 'selected';
            refineObject.values.push(valueObject);
          }
        });
      }
    });
    refinementsArray.push(refineObject);
  });

  return refinementsArray;
}

/**
 * Prepare products array for data layer from product object
 */
function getProductIDArray(productSearch) {
  var productsArray = [];
  var productIds = productSearch.productIds;

  if (productIds) {
    Object.keys(productIds).forEach(function (key) {
      if (productIds[key].productID) {
        var productCode = {};
        productCode.code = productIds[key].productID;
        productsArray.push(productCode);
      }
    });
  }
  return productsArray;
}

/**
 * All stores
 * @param product
 * @returns
 */
function getStores(product) {
  var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');
  var instorePUstoreHelpers = require('*/cartridge/scripts/helpers/instorePickupStoreHelpers');
  var defaultSearchRadius = 15;
  var stores = storeHelpers.getStores(defaultSearchRadius, null, null, null, request.geolocation);
  var tStores = [];
  if (stores && stores.stores) {
    stores.stores.forEach(function (store) {
      var tStore = {};
      tStore.available = instorePUstoreHelpers.getStoreInventory(store.ID, product.id).toString();
      tStore.id = store.ID.toString();
      tStores.push(tStore);
    });
  }
  return tStores;
}

/**
 *
 * @param {string} querystring url string
 * @param {string} params fetch the params
 * @returns {string} returns the value
 */
function getParamsFromQueryString(querystring, params) {
  var result = '';
  if (querystring) {
    querystring.split('&').forEach(function (item) {
      var key = item && item.split('=').length === 2 ? item.split('=')[0] : '';
      var value = item && item.split('=').length === 2 ? item.split('=')[1] : '';
      if (key === params) {
        result = value;
      }
    });
  }
  return result;
}

/**
 *
 * @param {object} req current request
 * @returns {boolean}
 */
function provideDesignerName(req) {
  var isBrand = req.querystring ? getParamsFromQueryString(req.querystring.querystring, 'prefn1') === 'brand' : false;
  return isBrand && req.querystring ? getParamsFromQueryString(req.querystring.querystring, 'prefv1') : '';
}

/**
 * Prepare GlobalData object to push to DataLayer
 */
function getGlobalData(req) {
  var globalData = {};
  globalData.page = {};
  switch (req.querystring.action) {
    case 'Home-CA':
      globalData.page.type = 'off 5th canada';
      setRecurringData(globalData);
      break;
    case 'Home-Show':
    case 'Default-Start':
      globalData.page.type = 'home page';
      setRecurringData(globalData);
      break;
    case 'EmailSubscribe-EmailSignUp':
    case 'EmailSubscribe-EmailSignUpAction':
      globalData.page.type = 'email signup';
      setRecurringData(globalData);
      break;
    case 'Search-Show':
      var cgid = 'cgid' in req.querystring ? req.querystring.cgid : null;
      var category = cgid ? dw.catalog.CatalogMgr.getCategory(cgid) : null;
      var isDesigner = 'designer' in req.querystring ? req.querystring.designer : false;
      var preferences = require('*/cartridge/config/preferences');

      var topCategory = null;
      var pageDesign = null;
      var pageType = null;
      var renderTemplate = getRenderingTemplate(category);

      if (!cgid) {
        globalData.page.type = 'search';
        setRecurringData(globalData);
        break;
      }

      // Designer Index page
      if (cgid && preferences.designerCategoryID && preferences.designerCategoryID === cgid) {
        globalData.page.type = 'designer index';
        setRecurringData(globalData);
        break;
      }

      // Designer Product Landing Page
      if (cgid && cgid === 'brand') {
        globalData.page.type = 'product array';
        globalData.page.designer = provideDesignerName(req);
        globalData.page.categories = ['brand', globalData.page.designer];
        setRecurringData(globalData);
        break;
      }
      // If no rendering template, we are considering as a PLP
      if (!renderTemplate) {
        if (category) setCatHierarchy(globalData, category);
        setCatHierarchy(globalData, category);
        globalData.page.designer = provideDesignerName(req);
        globalData.page.type = 'product array';
        setRecurringData(globalData);
        break;
      }

      if (renderTemplate) {
        switch (renderTemplate.toLowerCase()) {
          case 'catlanding':
            // CLP page
            setCatHierarchy(globalData, category);
            break;
          case 'designerbay':
          case 'categorylandingchanel':
            // Channel CLP
            globalData.page.designer = 'CHANEL';
            globalData.page.type = 'designer';
            break;
          case 'producthitschanel':
            // Channel PLP
            globalData.page.designer = 'CHANEL';
            globalData.page.type = 'designer';
            break;
          case 'producthitsgiftcards':
            // PLP
            setCatHierarchy(globalData, category);
            globalData.page.type = 'product array';
            globalData.page.designer = provideDesignerName(req);
            break;
          default:
            // PLP template
            setCatHierarchy(globalData, category);
            globalData.page.type = 'product array';
            globalData.page.designer = provideDesignerName(req);
            break;
        }
        setRecurringData(globalData);
      }
      break;
    case 'Product-Show':
    case 'Product-ShowInCategory':
      globalData.page.type = 'product detail';
      setRecurringData(globalData);
      break;
    case 'Cart-Show':
      globalData.page.type = 'shopping bag';
      setRecurringData(globalData);
      break;
    case 'Checkout-Begin':
      var stage = 'stage' in req.querystring ? req.querystring.stage : '';
      switch (stage) {
        case 'shipping':
          stage = 'shipping address';
          break;
        case 'placeOrder':
          stage = 'review and submit';
          break;
        case 'pickupperson':
          stage = 'pickup options';
          break;
        default:
          break;
      }
      if (stage) {
        globalData.page.checkoutStep = stage;
        globalData.page.type = 'checkout';
        setRecurringData(globalData);
      }
      break;
    case 'Login-Show':
      globalData.page.name = 'sign in or create account';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    case 'Account-Show':
      globalData.page.name = 'landing';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    case 'Account-Profile':
      globalData.page.name = 'profile';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    case 'Wishlist-Show':
      globalData.page.name = 'favorites';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    case 'Loyalty-AccountRewardsTab':
      globalData.page.name = 'Loyalty AccountRewardsTab';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    case 'Loyalty-LinkAccountForm':
      globalData.page.name = 'Loyalty LinkAccountForm';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    case 'Loyalty-EnrollForm':
      globalData.page.name = 'Loyalty EnrollForm';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    case 'Loyalty-RequestPinCreate':
      globalData.page.name = 'Loyalty Create Pin';
      setRecurringData(globalData);
      break;
    case 'Loyalty-ForgotLoyaltyPin':
      globalData.page.name = 'Loyalty Forgot Pin';
      setRecurringData(globalData);
      break;
    case 'Loyalty-PinReset':
      globalData.page.name = 'Loyalty Reset Pin';
      setRecurringData(globalData);
      break;
    case 'PaymentInstruments-List':
      globalData.page.name = 'payment';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    case 'Order-Confirm':
      globalData.page.type = 'checkout confirmation';
      setRecurringData(globalData);
      break;
    case 'Order-History':
    case 'Order-Status':
      globalData.page.name = 'order status';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    case 'Order-Details':
      globalData.page.name = 'order detail';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    case 'Account-EditProfile':
      globalData.page.name = 'edit profile';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    case 'Account-EditPassword':
      globalData.page.name = 'edit password';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    case 'Account-EmailPreferences':
      globalData.page.name = 'email preferences';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    case 'PaymentInstruments-AddPayment':
      globalData.page.name = 'edit payment';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    case 'Address-List':
      globalData.page.name = 'address';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    case 'Address-AddAddress':
      globalData.page.name = 'address';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    case 'Address-EditAddress':
      globalData.page.name = 'edit address';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    case 'GiftRegistry-Start':
      globalData.page.type = 'gift card balance';
      setRecurringData(globalData);
      break;
    case 'Default-Offline':
      globalData.page.type = 'maintenance';
      setRecurringData(globalData);
      break;
    case 'Home-ErrorNotFound':
      globalData.page.name = '404';
      globalData.page.type = 'error';
      setRecurringData(globalData);
      break;
    case 'Page-Show':
      var cid = 'cid' in req.querystring ? req.querystring.cid : '';
      globalData.page.name = cid.toString();
      globalData.page.type = 'editorial';
      if (req.querystring.cid === 'bay-flyer') {
        globalData.page.name = 'flyer';
        globalData.page.type = 'editorial';
      }
      setRecurringData(globalData);
      break;
    case 'PreferenceCenter-Show':
      globalData.page.name = 'preference center';
      globalData.page.type = 'account';
      setRecurringData(globalData);
      break;
    default:
    //DO Nothing
  }
  var currencyCode = req.session.currency.currencyCode;
  var cookie = new Cookie('E4X_CURRENCY', currencyCode);
  cookie.setMaxAge(2592000);
  cookie.setPath('/');
  cookie.setSecure(true);
  cookie.setHttpOnly(false); // Adobe requires this to be false
  response.addHttpCookie(cookie); // eslint-disable-line

  return globalData;
}

/**
 * Get Payment method
 * @param order
 * @returns
 */
function getOrderPaymentMethod(order) {
  var paymentMethods = [];
  var PaymentInstrument = require('dw/order/PaymentInstrument');
  if (order.billing.payment.selectedPaymentInstruments) {
    order.billing.payment.selectedPaymentInstruments.forEach(function (paymentInstrument) {
      switch (paymentInstrument.paymentMethod) {
        case PaymentInstrument.METHOD_CREDIT_CARD:
          switch (paymentInstrument.type.toLowerCase()) {
            case 'discover':
              paymentMethods.push('disc');
              break;
            case 'master card':
              paymentMethods.push('mc');
              break;
            case 'amex':
              paymentMethods.push('amex');
              break;
            default:
              paymentMethods.push(paymentInstrument.type.toLowerCase());
              break;
          }
          break;
        case 'GiftCard':
          paymentMethods.push('gc');
          break;
        case 'PayPal':
          paymentMethods.push('paypal');
          break;
        default:
          break;
      }
    });
  }
  return paymentMethods;
}

/**
 * Fetch all payment service
 * @returns
 */
function getOrderPaymentService(order) {
  var paymentServices = '';
  var PaymentInstrument = require('dw/order/PaymentInstrument');
  var OrderMgr = require('dw/order/OrderMgr');
  var orderData = OrderMgr.getOrder(order.orderNumber);
  if (order.billing.payment.selectedPaymentInstruments) {
    order.billing.payment.selectedPaymentInstruments.forEach(function (paymentInstrument) {
      switch (paymentInstrument.paymentMethod) {
        case 'masterpass':
          paymentServices = 'masterpass';
          break;
        case 'PayPal':
          paymentServices = 'paypal';
          break;
        default:
          break;
      }
    });
  }
  if (orderData && 'masterpassWalletId' in orderData.custom && orderData.custom.masterpassWalletId !== null && orderData.custom.masterpassWalletId !== '') {
    paymentServices = 'masterpass';
  }
  if (orderData && 'sr_token' in orderData.custom && orderData.custom.sr_token != null && orderData.custom.sr_token !== '') {
    paymentServices = 'shoprunner_express';
  }
  return paymentServices;
}

/**
 * get product array type
 * @param pdict
 * @returns
 */
function getProductArrayType(pdict) {
  var productArrayType = pdict.src && pdict.src === 'brand' ? 'brand' : 'search';
  var category = 'category' in pdict && pdict.category ? pdict.category : null;
  var renderTemplate = category ? getRenderingTemplate(category) : null;
  if (category) {
    switch (category.ID) {
      case 'sale':
        productArrayType = 'sale';
        break;
      // Kleinfeld
      case '2534374302032692':
        productArrayType = 'brand';
        break;
      case 'root':
        productArrayType = 'product array';
        break;
      default:
        productArrayType = 'product';
        break;
    }
    if (category.ID === '1531927839724' || (category.parent && category.parent.ID === '1531927839724')) {
      productArrayType = 'custom';
    }
    if (renderTemplate && renderTemplate.toLowerCase() === 'categorylandingchanel') {
      productArrayType = 'brand';
    }
  }
  return productArrayType;
}

function provideListPrice() {}
/**
 * get product price
 * @param product
 * @returns
 */
function getProductPrice(product) {
  var productObject = {};
  if (product.price) {
    if (product.price.type && product.price.type === 'range') {
      var min = product.price.min.list ? product.price.min.list.value : product.price.min.sales.value;
      var max = product.price.max.list ? product.price.max.list.value : product.price.max.sales.value;
      productObject.originalPrice = min + '-' + max;
      productObject.price = product.price.min.sales.value + '-' + product.price.max.sales.value;
    } else {
      productObject.originalPrice = product.price.list ? product.price.list.value : product.price.sales.value;
      productObject.price = product.price.sales.value;
    }
  }
  return productObject;
}

/**
 * Prepare Page data object to push to DataLayer
 */
function getPageData(pdict) {
  var pageData = {};
  switch (pdict.action) {
    case 'Search-Show':
      var category = 'category' in pdict && pdict.category ? pdict.category : null;
      if (category && (category.parent.ID === 'content' || category.parent.ID === 'editorial')) {
        return;
      }
      var renderTemplate = category ? getRenderingTemplate(category) : null;
      var isDesignerIndex = !pdict.brandValue && category && category.ID === 'root';
      if (
        isDesignerIndex ||
        (renderTemplate &&
          (renderTemplate.toLowerCase() === 'designerbay.isml' ||
            renderTemplate.toLowerCase() === 'catlanding.isml' ||
            renderTemplate.toLowerCase() === 'categorylandingchanel.isml'))
      ) {
        break;
      }
      var productSearch = pdict.productSearch.productSearch;
      var refinements = getRefinements(productSearch, productSearch.refinements, productSearch.refinements.refinementDefinitions);
      var refinementsArray = [];
      var effectiveSortingRule =
        productSearch && 'effectiveSortingRule' in productSearch && productSearch.effectiveSortingRule ? productSearch.effectiveSortingRule.ID : '';
      if (refinements) refinementsArray = getRefinementsArray(refinements);
      var ProductIDArray = getProductIDArray(pdict.productSearch);

      pageData.product_array = {};
      pageData.product_array.array_page_number = pdict.productSearch.paginationUrls.currentPage
        ? pdict.productSearch.paginationUrls.currentPage.toString()
        : '1';
      pageData.product_array.array_type = getProductArrayType(pdict);
      pageData.product_array.refinements = refinementsArray;
      pageData.product_array.results_per_page = pdict.productSearch.pageSize ? pdict.productSearch.pageSize.toString() : '1';
      pageData.product_array.sort = {};
      pageData.product_array.sort.name = effectiveSortingRule;
      pageData.product_array.sort.type = 'default'; //Page load will be default
      pageData.product_array.total_results = productSearch.count ? productSearch.count.toString() : '0';
      pageData.product_array.results_across = '3'; //Products in row, Fixed 3, check if we are getting this in pdict
      if (renderTemplate && renderTemplate.toLowerCase() === 'producthitsgiftcards.isml') {
        pageData.product_array.results_across = '4';
      }
      pageData.products = ProductIDArray;

      if (category && category.ID === 'root') {
        break;
      }

      if (!category) {
        pageData.search = {};
        var str = pdict.searchWord.toLowerCase();
        if (str.indexOf('</script>') !== -1) {
          pageData.search.term = pdict.searchWord ? encodeURIComponent(pdict.searchWord.toLowerCase()) : '';
        } else {
          pageData.search.term = pdict.searchWord ? pdict.searchWord.toLowerCase() : '';
        }
        pageData.search.type =
          pdict.productSearch.suggestedTerms && pdict.productSearch.suggestedTerms.length
            ? pdict.productSearch.suggestedTerms[0] === 'Suggestions'
              ? 'suggested'
              : pdict.productSearch.suggestedTerms[0]
            : '';
      }

      if (!category && pdict.previousWord) {
        pageData.search = {};
        pageData.search.term = pdict.previousWord ? pdict.previousWord.toLowerCase() : '';
        pageData.search.type = 'corrected';
        pageData.search.corrected_term = pdict.searchWord ? pdict.searchWord.toLowerCase() : '';
      }

      if (category && pdict.searchTerm && pdict.searchType) {
        pageData.search = {};
        pageData.search.term = pdict.searchTerm.toLowerCase();
        pageData.search.type = pdict.searchType === 'Featured Items' ? 'featured' : pdict.searchType === 'Suggestions' ? 'suggested' : pdict.searchType;
      }
      break;
    case 'Product-Show':
      pageData.products = [];

      var productID = pdict.product.id;
      var apiproduct = ProductMgr.getProduct(productID);
      var masterProduct = apiproduct.isVariant() ? apiproduct.getMasterProduct() : apiproduct; //not handled for setProduct.
      var productVariants = masterProduct.getVariants();
      var skus = [];
      Object.keys(productVariants).forEach(function (key) {
        var variant = {};
        variant.available_dc =
          productVariants[key].availabilityModel && productVariants[key].availabilityModel.inventoryRecord
            ? productVariants[key].availabilityModel.inventoryRecord.ATS.value.toString()
            : '';
        variant.sku = productVariants[key].ID;
        //variant.stores = getStores(productVariants[key]);
        skus.push(variant);
      });
      var productObject = {};
      var inventoryLevel =
        pdict.product.availability && 'isAboveThresholdLevel' in pdict.product.availability ? pdict.product.availability.isAboveThresholdLevel : false;
      var priceType = '';
      var productBadge = pdict.product && pdict.product.badge ? pdict.product.badge : null;
      if (productBadge && productBadge.isClearance) {
        priceType = 'clearance';
      } else if (productBadge && productBadge.isFinalSale) {
        priceType = 'final sale';
      }
      var returnable = (!pdict.product.isNotReturnable.value).toString();

      productObject.average_rating = pdict.product.starRating ? pdict.product.starRating.toString() : '0';
      productObject.brand = pdict.product.brand.name ? pdict.product.brand.name : '';
      productObject.code = masterProduct.ID;
      productObject.name = pdict.product.productName ? pdict.product.productName : '';
      var productPrice = getProductPrice(pdict.product);
      productObject.original_price = productPrice.originalPrice ? productPrice.originalPrice.toString() : '';
      productObject.price = productPrice.price ? productPrice.price.toString() : '';
      productObject.skus = skus;
      productObject.tags = {};
      productObject.tags.feature_type = 'featuredType' in pdict.product && pdict.product.featuredType.value ? pdict.product.featuredType.value : '';
      productObject.tags.inventory_label =
        pdict.product.availability.messages && pdict.product.availability.messages.length ? pdict.product.availability.messages[0] : '';
      productObject.tags.pip_text = pdict.product.promotions && pdict.product.promotions.length ? pdict.product.promotions[0].calloutMsg : '';
      productObject.tags.price_type = priceType;
      productObject.tags.publish_date = 'new';
      productObject.tags.returnable = returnable.toString();
      productObject.total_reviews = pdict.product.turntoReviewCount ? pdict.product.turntoReviewCount.toString() : '0';
      pageData.products.push(productObject);
      if (pdict.searchTerm && pdict.searchType) {
        pageData.search = {};
        pageData.search.term = pdict.searchTerm;
        pageData.search.type = pdict.searchType === 'Featured Items' ? 'featured' : pdict.searchType === 'Suggestions' ? 'suggested' : pdict.searchType;
      }
      break;

    case 'Cart-Show':
    case 'Checkout-Begin':
    case 'Order-Confirm':
      pageData.products = [];
      var items = pdict.items;
      if (!items) {
        items = 'order' in pdict && 'items' in pdict.order && 'items' in pdict.order.items ? pdict.order.items.items : null;
      }
      var lineItems = [];
      var productLevelDiscountAmount = 0;
      Object.keys(items).forEach(function (key) {
        var itemObject = {};
        itemObject.brand = items[key].brand && items[key].brand.name ? items[key].brand.name : '';
        itemObject.bopus_store_id = items[key].fromStoreId ? items[key].fromStoreId : '';
        itemObject.code = items[key].masterProductID ? items[key].masterProductID : items[key].id;

        itemObject.name = items[key].productName;
        itemObject.original_price = items[key].price.list ? items[key].price.list.value.toString() : items[key].price.sales.value.toString();
        itemObject.price = items[key].price.sales.value.toString();
        itemObject.quantity = items[key].quantity.toString();
        itemObject.selected_sku = items[key].id;
        itemObject.ship_from_store_id = '';
        if (pdict.action === 'Order-Confirm') {
          var excludingDiscount =
            items[key].priceTotal.unFormattedNonAdjustedPrice && items[key].priceTotal.unFormattedNonAdjustedPrice.available
              ? items[key].priceTotal.unFormattedNonAdjustedPrice.value
              : items[key].priceTotal.unFormattedprice && items[key].priceTotal.unFormattedprice.available
              ? items[key].priceTotal.unFormattedprice.value
              : 0;
          var includingDiscount =
            items[key].priceTotal.unFormattedprice && items[key].priceTotal.unFormattedprice.available ? items[key].priceTotal.unFormattedprice.value : 0;
          productLevelDiscountAmount = StringUtils.formatNumber(excludingDiscount - includingDiscount, '#,##0.00', 'en_US');
          itemObject.discount_amount = productLevelDiscountAmount ? productLevelDiscountAmount : '0';
          itemObject.gift_wrap_amount = '0';
          itemObject.gift_wrap_type = '';
        }

        lineItems.push(itemObject);
      });

      if (pdict.action === 'Order-Confirm' && pdict.order) {
        pageData.order = {};
        pageData.order.billing_country = pdict.order.billing.billingAddress.address.countryCode.value;
        pageData.order.billing_state = pdict.order.billing.billingAddress.address.stateCode;
        pageData.order.billing_zip = pdict.order.billing.billingAddress.address.postalCode;
        pageData.order.discount_total = pdict.order.totals.orderLevelDiscountTotal ? pdict.order.totals.orderLevelDiscountTotal.value.toString() : '';
        pageData.order.id = pdict.order.orderNumber;
        if (pdict.order.rewards && pdict.order.rewards.hasPointsApplied) {
          pageData.order.loyalty_redeem_amount = pdict.order.rewards.pwpData[0].pointValue.substring(1); // currency value without the symbol
          pageData.order.loyalty_redeem_points = String(pdict.order.rewards.pwpData[0].pointAmount); // number of points
        }
        pageData.order.payment_method = getOrderPaymentMethod(pdict.order);
        pageData.order.payment_service = getOrderPaymentService(pdict.order);
        pageData.order.promo_code =
          pdict.order.totalAppliedCouponCodes && pdict.order.totalAppliedCouponCodes.length ? pdict.order.totalAppliedCouponCodes : '';
        pageData.order.shipping_country = pdict.order.shipping[0].shippingAddress.countryCode.value;
        pageData.order.shipping_method = pdict.order.shipping[0].selectedShippingMethod.ID;
        pageData.order.shipping_state = pdict.order.shipping[0].shippingAddress.stateCode;
        pageData.order.shipping_total =
          pdict.order.shipping[0].selectedShippingMethod.unFormatFinalShippingCost &&
          pdict.order.shipping[0].selectedShippingMethod.unFormatFinalShippingCost
            .toString()
            .equalsIgnoreCase(Resource.msg('info.cart.free.shipping', 'cart', null))
            ? '0'
            : pdict.order.shipping[0].selectedShippingMethod.unFormatFinalShippingCost.toString();
        pageData.order.shipping_zip = pdict.order.shipping[0].shippingAddress.postalCode;
        pageData.order.subtotal = pdict.order.totals.subTotalUnformatted ? pdict.order.totals.subTotalUnformatted.value.toString() : '';
        pageData.order.tax_total = pdict.order.totals.totalTaxUnformatted.available ? pdict.order.totals.totalTaxUnformatted.value.toString() : '';
      }

      pageData.products = lineItems;
      break;
    default:
    // Do Nothing
  }

  return pageData;
}

/**
 * Prepare BF Confirmation Tracking Data
 * @param req
 * @returns
 */
function prepareBFConfirmationData(req) {
  var CartModel = require('*/cartridge/models/cart');
  var BasketMgr = require('dw/order/BasketMgr');
  var System = require('dw/system/System');
  var RootLogger = require('dw/system/Logger').getRootLogger();

  try {
    var currentBasket = BasketMgr.getCurrentBasket();
    var basket = new CartModel(currentBasket);
    var trackingData = {};
    // Prepare Order Data
    var order = {};
    order.billing_country = '';
    order.billing_state = '';
    order.billing_zip = '';
    order.discount_total = basket.totals && basket.totals.orderLevelDiscountTotal ? basket.totals.orderLevelDiscountTotal.value.toString() : '';
    order.id = req.session.privacyCache.get('bfxOrderNumber');
    order.loyalty_redeem_amount = '0';
    order.loyalty_redeem_points = '0';
    order.payment_method = [];
    order.payment_service = '';
    order.promo_code = basket.totalAppliedCouponCodes && basket.totalAppliedCouponCodes.length ? basket.totalAppliedCouponCodes : '';
    order.shipping_country = '';
    order.shipping_method = '';
    order.shipping_state = '';
    order.shipping_total = '0';
    order.shipping_zip = '';
    order.subtotal = basket.totals && basket.totals.subTotalUnformatted ? basket.totals.subTotalUnformatted.value.toString() : '';
    order.tax_total = '';

    // Add Page Data
    var page = {};
    page.type = 'Borderfree Checkout Confirmation';
    trackingData.page = page;
    var isProdInstance = System.getInstanceType() == System.PRODUCTION_SYSTEM;

    // Add Banner Data
    var site = {};
    site.is_production = isProdInstance.toString();
    switch (dw.system.Site.getCurrent().ID) {
      case 'TheBay':
        site.name = 'thebay.com';
        break;
      case 'SaksOff5th':
        site.name = 'off5th.com';
        break;
      case 'SaksFifthAvenue':
        site.name = 'saks.com';
        break;
      default:
        break;
    }
    trackingData.site = site;

    // Add Line Items to the tracking data
    trackingData.products = [];

    var items = basket && 'items' in basket ? basket.items : null;

    if (items) {
      var lineItems = [];
      var productLevelDiscountAmount = 0;
      Object.keys(items).forEach(function (key) {
        var itemObject = {};
        itemObject.brand = items[key].brand && items[key].brand.name ? items[key].brand.name : '';
        itemObject.bopus_store_id = items[key].fromStoreId ? items[key].fromStoreId : '';
        itemObject.code = items[key].masterProductID ? items[key].masterProductID : items[key].id;

        itemObject.name = items[key].productName;
        itemObject.original_price = items[key].price.list ? items[key].price.list.value.toString() : items[key].price.sales.value.toString();
        itemObject.price = items[key].price.sales.value.toString();
        itemObject.quantity = items[key].quantity.toString();
        itemObject.selected_sku = items[key].id;
        itemObject.ship_from_store_id = '';
        var excludingDiscount =
          items[key].priceTotal.unFormattedNonAdjustedPrice && items[key].priceTotal.unFormattedNonAdjustedPrice.available
            ? items[key].priceTotal.unFormattedNonAdjustedPrice.value
            : items[key].priceTotal.unFormattedprice && items[key].priceTotal.unFormattedprice.available
            ? items[key].priceTotal.unFormattedprice.value
            : 0;
        var includingDiscount =
          items[key].priceTotal.unFormattedprice && items[key].priceTotal.unFormattedprice.available ? items[key].priceTotal.unFormattedprice.value : 0;
        productLevelDiscountAmount = StringUtils.formatNumber(excludingDiscount - includingDiscount, '#,##0.00', 'en_US');
        itemObject.discount_amount = productLevelDiscountAmount ? productLevelDiscountAmount : '0';
        itemObject.gift_wrap_amount = '0';
        itemObject.gift_wrap_type = '';
        lineItems.push(itemObject);
      });
      trackingData.products = lineItems;
    }
    trackingData.order = order;

    return {
      success: true,
      trackingData: trackingData
    };
  } catch (err) {
    RootLogger.fatal('Error while creating BF tracking data for ' + req.session.privacyCache.get('bfxOrderNumber') + ' error message ' + err.message);
    return {
      success: false
    };
  }
}

module.exports = {
  getGlobalData: getGlobalData,
  getPageData: getPageData,
  prepareBFConfirmationData: prepareBFConfirmationData
};
