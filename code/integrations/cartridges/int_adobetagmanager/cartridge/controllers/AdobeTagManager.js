'use strict';

/**
 * Controller for Adobe Tag Manager
 *
 * @module controllers/AdobeTagManager
 */

var server = require('server');

/**
 * Prepare and render Adobe Tag Manager global data.
 * Response should not be cached since the date is customer specific.
 */
server.get('GlobalData', function (req, res, next) {
  var globalData = require('*/cartridge/scripts/dataLayer/DataLayer.js').getGlobalData(req);
  res.render('adobeTagManager/atmGlobalData', {
    GlobalData: globalData
  });
  next();
});

module.exports = server.exports();
