'use strict';

var OrderMgr = require('dw/order/OrderMgr');
var Order = require('dw/order/Order');

var cnfrmOrder = require('~/cartridge/scripts/jobsteps/ConfirmOrder');

var run = function () {
  var orders = OrderMgr.searchOrders(
    'custom.bfxOrderId!=NULL AND status={0} AND (custom.bfxConfirmStatus={1} OR custom.bfxConfirmStatus={2})',
    'creationDate desc',
    Order.ORDER_STATUS_OPEN,
    'Timeout',
    'Unconfirmed'
  );
  var ordList = orders.asList();
  var errorOrders = new dw.util.ArrayList();
  for (var i = 0; i < ordList.size(); i++) {
    var ord = ordList.get(i);
    errorOrders = cnfrmOrder.confirmOrder(ord, errorOrders);
  }
  if (errorOrders.size() > 0) {
    var cc =
      dw.system.Site.getCurrent().getCustomPreferenceValue('bfxErrorEmail').length > 1
        ? dw.system.Site.getCurrent()
            .getCustomPreferenceValue('bfxErrorEmail')
            .slice(1, dw.system.Site.getCurrent().getCustomPreferenceValue('bfxErrorEmail').length)
            .join(',')
        : null;

    var Mail = require('dw/net/Mail');
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
    var context = {
      errorOrders: errorOrders
    };

    var email = new Mail();
    email.addTo(dw.system.Site.getCurrent().getCustomPreferenceValue('bfxErrorEmail')[0]);
    email.setSubject(dw.web.Resource.msg('resource.errorconfirmorder.subject', 'borderfree', null));
    email.setFrom(dw.system.Site.getCurrent().getCustomPreferenceValue('customerServiceEmail'));
    email.setContent(renderTemplateHelper.getRenderedHtml(context, 'mail/borderfree/orderconfirmationerror'), 'text/html', 'UTF-8');
    if (cc != null) {
      email.setCc(cc);
    }
    email.send();
  }
};

exports.run = run;
