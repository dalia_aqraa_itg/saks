'use strict';

var OrderMgr = require('dw/order/OrderMgr');
var Order = require('dw/order/Order');

var shipmentNotification = require('~/cartridge/scripts/jobsteps/NotifyShipmentFromCustomObject');

var run = function () {
  var num = dw.system.Site.getCurrent().getCustomPreferenceValue('bfxMaxFailedShipmentNotificationAttempts');
  var shipmentNotifications = dw.object.CustomObjectMgr.queryCustomObjects(
    'BorderfreePendingShipmentNotifications',
    '(custom.numFailedAttempts = NULL OR custom.numFailedAttempts < {0}) AND (custom.exported = {1} OR custom.exported = NULL)',
    null,
    num,
    false
  );
  dw.system.Logger.getLogger('ShipmentNotificationJob', 'test').debug(shipmentNotifications.count);
  if (shipmentNotifications.count > 0) {
    while (shipmentNotifications.hasNext()) {
      var shipment = shipmentNotifications.next();
      shipmentNotification.notifyShipment(shipment);
    }
  }
};

exports.run = run;
