'use strict';

/**
 * Pipelet for fetching unacknowledged orders from border free.
 *
 * @input poOrder : Object mandatory, PO Order to be processed.
 * @input Basket : dw.order.Basket
 */

const Logger = require('dw/system/Logger');
const Util = require('~/cartridge/scripts/utils/Util');

/**
 *
 * @param args
 * @returns
 */
function execute(args) {
  try {
    // NOTE args.Basket works on controllers -- but not on pipelines
    var poOrder = JSON.parse(args.poOrder);
    buildBasket(args.Basket, poOrder);

    if ('object' in args.Basket) {
      normalizeBasket(args.Basket.object);
    } else {
      normalizeBasket(args.Basket);
    }
  } catch (e) {
    var errormsg = e;
    Logger.error(e);
    return PIPELET_ERROR;
  }

  return PIPELET_NEXT;
}

function buildBasket(basket, poOrder) {
  var shipment = basket.getDefaultShipment();
  for (var i = 0; i < poOrder.basketDetails.basketItems.length; i++) {
    var productToAdd = dw.catalog.ProductMgr.getProduct(poOrder.basketDetails.basketItems[i].merchantSKU);
    if (!empty(productToAdd)) {
      var lineItem = basket.createProductLineItem(poOrder.basketDetails.basketItems[i].merchantSKU, shipment);
      lineItem.setPriceValue(poOrder.basketDetails.basketItems[i].productListPrice * poOrder.basketDetails.basketItems[i].productQuantity);
      lineItem.setQuantityValue(poOrder.basketDetails.basketItems[i].productQuantity);
    }
  }
}

function normalizeBasket(basket) {
  var lineItems = basket.allProductLineItems;
  for (var i = 0; i < lineItems.length; i++) {
    if (!lineItems[i].priceValue) {
      lineItems[i].setPriceValue(0);
    }
  }
}

module.exports = {
  execute: execute
};
