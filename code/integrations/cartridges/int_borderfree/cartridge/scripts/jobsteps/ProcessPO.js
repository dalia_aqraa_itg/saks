'use strict';

/**
 * Pipelet for fetching unacknowledged orders from border free.
 *
 * @input mismatchOrderList : dw.util.ArrayList mandatory , list of all mismatched unacknowledged orders
 * @input ackOrderList : dw.util.ArrayList mandatory, list of all orders to be acknowledged
 * @input siteOrder : dw.order.Order mandatory, site Order
 * @input poOrder : Object mandatory, PO Order to be processed.
 * @input errorMailContent : dw.util.ArrayList mandatory, list of errors on PO
 */

const OrderMgr = require('dw/order/OrderMgr');
const Logger = require('dw/system/Logger');
const Resource = require('dw/web/Resource');
const Status = require('dw/system/Status');
const Order = require('dw/order/Order');
const Transaction = require('dw/system/Transaction');
const Util = require('*/cartridge/scripts/utils/Util');
var Money = require('dw/value/Money');
const Site = require('dw/system/Site');
var customPreferences = Site.current.preferences.custom;
var RootLogger = require('dw/system/Logger').getRootLogger();
var globalHelper = require('*/cartridge/scripts/helpers/globalHelper');

/**
 *
 * @param args
 * @return
 */
function execute(args) {
  processPOOrder(args.siteOrder, args.poOrder, args.ackOrderList, args.mismatchOrderList, args.errorMailContent);
}

/**
 * This function is used to process a SiteOrder basing on PO
 * @param siteOrder
 * @param poOrder
 * @param ackOrderList
 * @param mismatchOrderList
 * @return
 */
function processPOOrder(siteOrder, poOrder, ackOrderList) {
  let fraudState,
    orderStatus,
    bfxOrderId,
    authStatus,
    bfxEnableAuthorization = false;

  bfxEnableAuthorization = dw.system.Site.current.getCustomPreferenceValue('bfxEnableAuthorization') === true;

  fraudState = poOrder.fraudState;
  if (Util.GREENSTATE === fraudState) {
    orderStatus = siteOrder.status.value;
    if (Order.ORDER_STATUS_FAILED === orderStatus) {
      Logger.info('Order is already marked as failed, so ignore further processing for order number: ' + siteOrder.orderNo);
    } else if (Order.ORDER_STATUS_CREATED === orderStatus) {
      try {
        Transaction.begin();

        updateBFOrder(poOrder, siteOrder);
        // We commented it out because it causes following issues:
        // 1. Product price  is taken from storefront price book instead of using BF sales price
        // 2. Tax is calculated using SFCC tax table but should be ZERO
        // 3. Shipping price is override from SFCC too.
        // dw.system.HookMgr.callHook('dw.ocapi.shop.basket.calculate', 'calculate', siteOrder);

        Transaction.commit();
      } catch (e) {
        Transaction.rollback();

        Logger.error('Error while updating the Order due to mismatch ' + siteOrder.orderNo + ' error: ' + e.message);
      }

      if (bfxEnableAuthorization) {
        authStatus = authorizePayment(siteOrder, poOrder);
      }

      if ((bfxEnableAuthorization && !empty(authStatus) && authStatus.OK) || !bfxEnableAuthorization) {
        if (placeOrder(siteOrder, ackOrderList)) {
          bfxOrderId = siteOrder.custom.bfxOrderId;
          if (null === bfxOrderId) {
            Logger.error('bfxOrderId is not associated with order: ' + siteOrder.orderNo);
          } else {
            Logger.info('Order placed, PO to ack: ' + bfxOrderId);
          }
        }
      } else {
        //TODO: cancel order here if you'd prefere that workflow. This leaves order "created"
        Logger.error(
          'Authorization attempted and failed, order not placed. order: ' +
            siteOrder.orderNo +
            '. Error: ' +
            authStatus.code +
            ' ' +
            authStatus.message.toString()
        );
      }
    } else if (Order.ORDER_STATUS_NEW === orderStatus || Order.ORDER_STATUS_OPEN === orderStatus) {
      bfxOrderId = siteOrder.custom.bfxOrderId;
      if (null !== bfxOrderId) {
        Logger.info('Order already placed, PO to ack: ' + bfxOrderId);
        ackOrderList.add(bfxOrderId);
      } else {
        Logger.error('bfxOrderId is not associated with order: ' + siteOrder.orderNo);
      }
    }
  } else if (Util.REDSTATE === fraudState) {
    cancelOrder(siteOrder);
  }
}

/**
 * Place an order using OrderMgr. If order is placed successfully,
 * its status will be set as confirmed.
 * @param {dw.order.Order} order
 */
function placeOrder(order, ackOrderList) {
  let placeOrderStatus;
  try {
    Transaction.begin();
    placeOrderStatus = OrderMgr.placeOrder(order);
    if (placeOrderStatus === Status.ERROR) {
      Logger.error('Error while placing order for orderId: ' + order.orderNo + 'error: ' + e.message);
    } else {
      order.custom.orderStatus = ['INTL_HOLD'];
      order.setConfirmationStatus(Order.CONFIRMATION_STATUS_CONFIRMED);
      order.setExportStatus(Order.EXPORT_STATUS_READY);
    }
    Transaction.commit();

    if (placeOrderStatus !== Status.ERROR) {
      ackOrderList.add(order.custom.bfxOrderId);
      var OrderAPIUtils = require('*/cartridge/scripts/util/OrderAPIUtil');
      Transaction.wrap(function () {
        //SFDEV-11204 | Generate OMS Create Order XML to be used in ExportOrders batch job
        order.custom.omsCreateOrderXML = OrderAPIUtils.buildRequestXML(order);
      });
      if (customPreferences.orderCreateBatchEnabled !== true) {
        try {
          var OrderAPIUtils = require('*/cartridge/scripts/util/OrderAPIUtil');
          var responseOrderXML = OrderAPIUtils.createOrderInOMS(order);
          // eslint-disable-next-line no-undef
          var resXML = new XML(responseOrderXML);
          Logger.debug('resXML.child(ResponseMessage) -->' + resXML.child('ResponseMessage'));

          if (resXML.child('ResponseMessage').toString() === 'Success') {
            Transaction.wrap(function () {
              order.exportStatus = Order.EXPORT_STATUS_EXPORTED;
              order.trackOrderChange('Order Export Successful');
              order.trackOrderChange(responseOrderXML);
              order.custom.reversalForCancelOrder = false;
            });
          } else {
            Transaction.wrap(function () {
              order.exportStatus = Order.EXPORT_STATUS_FAILED;
              order.trackOrderChange('Order Exported Failed');
              order.trackOrderChange(responseOrderXML);
              order.custom.reversalForCancelOrder = false;
            });
          }
        } catch (err) {
          RootLogger.fatal('Error while calling the Order Create Service for Order ' + order.orderNo + ' error message ' + err.message);
        }
      }
      return true;
    } else {
      // log error and send email if unable to place order
      Logger.error('Error while placing order for orderId: ' + order.orderNo);
      return false;
    }
  } catch (e) {
    Logger.error('Error while placing order for orderId: ' + order.orderNo + 'error' + e);
    Transaction.rollback();
    return false;
  }
}

/**
 * Save the Borderfree credit card info on an order. Note, this kills all METHOD_CREDIT_CARD payment methods on order
 * @param {dw.order.Order} sfccOrder SFCC Order object that matches borderfree order
 * @param {Object} bfxOrder Borderfree order JSON from po request
 */
function addBfxCreditCard(sfccOrder, bfxOrder) {
  //SFDEV-10869 - remove call to this function from processPOOrder() to prevent ProcessPO job from adding payment info to orders
  try {
    Transaction.wrap(function () {
      sfccOrder.removeAllPaymentInstruments();
      var paymentInstrument = sfccOrder.createPaymentInstrument(dw.order.PaymentInstrument.METHOD_CREDIT_CARD, sfccOrder.getTotalGrossPrice());
      paymentInstrument.creditCardHolder = bfxOrder.creditCard.nameOnCard;
      paymentInstrument.creditCardNumber = bfxOrder.creditCard.number;
      paymentInstrument.creditCardType = updateCardType(bfxOrder.creditCard.type);
      paymentInstrument.creditCardExpirationMonth = bfxOrder.creditCard.expiry.month;
      paymentInstrument.creditCardExpirationYear = bfxOrder.creditCard.expiry.year;

      var PaymentMgr = require('dw/order/PaymentMgr');
      paymentInstrument.paymentTransaction.paymentProcessor = PaymentMgr.getPaymentMethod(dw.order.PaymentInstrument.METHOD_CREDIT_CARD).getPaymentProcessor();
    });
  } catch (e) {
    Logger.error(
      'Error while saving credit card info for SFCC orrder ' + sfccOrder.orderNo + ' / bfx order ' + bfxOrder.orderId.e4XOrderId + ' error: ' + e.message
    );
  }
}

/**
 * Cancel order using OrderMgr.
 * @param {dw.order.Order} order
 * @return {Boolean}
 */
function cancelOrder(order) {
  try {
    Transaction.begin();
    OrderMgr.failOrder(order);
    Transaction.commit();
    return true;
  } catch (e) {
    Logger.error('Error while cancelling order for orderId: ' + order.orderNo + ' error: ' + e.message);
    Transaction.rollback();
    return false;
  }
}

/**
 * This function iterates through order items and updates the existing SFCC order
 * @param {Object} bfxorder
 * @param {dw.order.Order} order
 * @return {dw.order.Order}
 */
function updateBFOrder(bfxOrder, order) {
  let basketItems = bfxOrder.basketDetails.basketItems;
  let productIter = order.getAllProductLineItems().iterator();
  let priceAdjustments = order.getPriceAdjustments();
  let coupons = order.getCouponLineItems();
  let bfxTotalOrderLevelDiscounts = Number(bfxOrder.basketDetails.orderDetails.totalOrderLevelDiscounts);

  //Looping through all of the price adjustments, and totaling them
  for (let i = 0, j = priceAdjustments.length; i < j; i++) {
    order.removePriceAdjustment(priceAdjustments[i]);
  }

  if (bfxTotalOrderLevelDiscounts > 0) {
    var bfPriceAdjustment = order.createPriceAdjustment('BorderfreeOrderLevelAdjustments', new dw.campaign.AmountDiscount(bfxTotalOrderLevelDiscounts));
    bfPriceAdjustment.setPriceValue(-1 * bfxTotalOrderLevelDiscounts);
    bfPriceAdjustment.updateTax(0);
    bfPriceAdjustment.setLineItemText('Adjustment from Borderfree PO');
  }

  let pli, qty, productID, errorMessage;
  while (productIter.hasNext()) {
    pli = productIter.next();
    order.removeProductLineItem(pli);
  }

  // create bfxorder item mapping
  let merchantSKU, productQuantity;
  for (var i = 0; i < basketItems.length; i += 1) {
    merchantSKU = basketItems[i].merchantSKU;
    productQuantity = basketItems[i].productQuantity;
    var lineItem = order.createProductLineItem(merchantSKU, order.defaultShipment);
    lineItem.setQuantityValue(productQuantity);

    //set price based on PO order
    lineItem.setPriceValue(basketItems[i].productSalePrice);

    //zero out taxes, taxes handled on BF side
    // Manually calculate and update the tax basis
    var lineItemTotalSalePrice = basketItems[i].productSalePrice * productQuantity;
    var proratedDiscount = (lineItemTotalSalePrice / bfxOrder.basketDetails.orderDetails.totalProductSaleValue) * bfxTotalOrderLevelDiscounts;
    var taxBasis = new Money(lineItemTotalSalePrice - proratedDiscount, order.getCurrencyCode());
    lineItem.updateTax(0, taxBasis);
  }

  //set shipping price
  var shippingLineItems = order.defaultShipment.shippingLineItems;

  for (var s = 0; s < shippingLineItems.length; s++) {
    shippingLineItems[s].setPriceValue(bfxOrder.copShippingMethod.shippingPrice);
    //zero out taxes, taxes handled on BF side
    shippingLineItems[s].updateTax(0);
  }
  var emailSuffix = dw.system.Site.current.getCustomPreferenceValue('bfxE4XEmail');

  var bfxEmail = emailSuffix && bfxOrder.orderId && bfxOrder.orderId.e4XOrderId ? bfxOrder.orderId.e4XOrderId.toString().toLocaleLowerCase() + emailSuffix : '';
  if (bfxEmail) {
    order.setCustomerEmail(bfxEmail);
  }

  updateShippingAddress(order, bfxOrder);

  // Update order totals
  order.updateTotals();

  return order;
}

/**
 * @description Saves actual custom shipping address from BFX PO
 * @param {dw.order.Order} order
 * @param {Object} bfxOrder
 */
function updateShippingAddress(order, bfxOrder) {
  var defaultShipment = !empty(order) && !empty(order.getDefaultShipment()) ? order.getDefaultShipment() : null;
  var shippingAddress = !empty(defaultShipment) ? defaultShipment.getShippingAddress() : null;

  if (!empty(shippingAddress)) {
    // address 2 must be E4X order number
    var bfxOrderNo = !empty(bfxOrder) && !empty(bfxOrder.orderId) && !empty(bfxOrder.orderId.e4XOrderId) ? bfxOrder.orderId.e4XOrderId : '';
    /*if (!empty(bfxOrderNo)) {
            shippingAddress.setAddress2(bfxOrderNo);
        }*/

    var domesticProfile = !empty(bfxOrder) && !empty(bfxOrder.domesticProfile) ? bfxOrder.domesticProfile : null;
    var bfxShipping = !empty(domesticProfile) && !empty(domesticProfile.Shipping) ? domesticProfile.Shipping : null;
    if (!empty(bfxShipping)) {
      if (!empty(bfxShipping.firstName)) {
        shippingAddress.setFirstName(globalHelper.replaceSpecialCharsAndEmojis(bfxShipping.firstName));
      }
      if (!empty(bfxShipping.lastName)) {
        shippingAddress.setLastName(globalHelper.replaceSpecialCharsAndEmojis(bfxShipping.lastName));
      }
      if (!empty(bfxShipping.addressLine1)) {
        shippingAddress.setAddress1(bfxShipping.addressLine1);
      }
      // We already set the E4X number in Billing Address 2
      if (!empty(bfxShipping.addressLine2)) {
        shippingAddress.setAddress2(bfxShipping.addressLine2);
      }
      if (!empty(bfxShipping.city)) {
        shippingAddress.setCity(bfxShipping.city);
      }
      if (!empty(bfxShipping.postalCode)) {
        shippingAddress.setPostalCode(bfxShipping.postalCode);
      }
      if (!empty(bfxShipping.region)) {
        shippingAddress.setStateCode(bfxShipping.region);
      }
      if (!empty(bfxShipping.country)) {
        shippingAddress.setCountryCode(bfxShipping.country);
      }
      if (!empty(bfxShipping.primaryPhone)) {
        shippingAddress.setPhone(bfxShipping.primaryPhone);
      }
    }
  }
}

/**
 * @description Implement authorization of the BFX credit card with your specific payment gateway here
 * @param {dw.order.Order} order SFCC Order
 * @param {Object} bfxOrder BFX Order from PO api
 * @return {dw.system.Status}
 */
function authorizePayment(order, bfxOrder) {
  if (order.getPaymentInstruments().length === 0) {
    return new Status(Status.ERROR, 'bfx.authorizePayment.noPaymentInstrument', 'No payment instrument on order');
  }

  if (empty(bfxOrder) || empty(bfxOrder.creditCard)) {
    return new Status(Status.ERROR, 'bfx.authorizePayment.noBfxCreditCard', 'No credit card details from Borderfree');
  }

  //return new Status(Status.ERROR, 'bfx.authorizePayment.notImplemented', 'Custom authorization logic not implemented');
  return new Status(Status.OK);
}

/**
 * @description Convert from BFX credit card type name to SFCC standard
 * @param {String} bfxCardType
 * @return {String} 'Visa' or 'MasterCard' etc.
 */
function updateCardType(bfxCardType) {
  if (!empty(bfxCardType)) {
    switch (bfxCardType.toLowerCase()) {
      case 'visa':
        bfxCardType = 'Visa';
        break;
      case 'mastercard':
      case 'master':
        bfxCardType = 'MasterCard';
        break;
      case 'amex':
        bfxCardType = 'Amex';
        break;
      case 'discover':
        bfxCardType = 'Discover';
        break;
      case 'maestro':
        bfxCardType = 'Maestro';
        break;
    }
  }
  return bfxCardType;
}

exports.execute = execute;
