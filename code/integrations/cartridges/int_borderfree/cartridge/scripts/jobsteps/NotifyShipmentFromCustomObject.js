/* eslint-disable quotes */
'use strict';

/**
 * Pipelet for notifying shipment
 *
 * @input CustomObject : dw.object.CustomObject
 *
 */

/**
 * Require API dependencies
 */
var Logger = require('dw/system/Logger');
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var svc;
var Transaction = require('dw/system/Transaction');

function initService() {
  svc = LocalServiceRegistry.createService('borderfree.soap.shipmentNotification', {
    initServiceClient: function (svc) {
      this.webReference = webreferences.PartnerAPI;
      var cred = this.getConfiguration().getCredential();
      var stub = this.webReference.getService('PartnerAPI', 'PartnerAPIv2SOAP1.1');
      var usernameToken = new XML(
        '<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">\n      <wsse:UsernameToken>\n        <wsse:Username>' +
          cred.user +
          '</wsse:Username>\n        <wsse:Password>' +
          cred.password +
          '</wsse:Password>\n      </wsse:UsernameToken>\n    </wsse:Security>'
      );
      dw.rpc.SOAPUtil.setHeader(stub, usernameToken, true);
      return stub;
    },

    createRequest: function (svc, parcelData, bfxOrderId) {
      try {
        var webRef = this.webReference;
        var parcelShipmentNotification = new webRef.ParcelShipmentNotification();
        var parcelDetails = new webRef.ParcelDetails();
        var carrierName, carrierService, parcelId, parcelReference, shippingDate, trackingURL, shippedDate, i, j;

        // parsing shipment data for the service
        var firstIterationDone = false;
        var item;
        var items = [];

        if (!firstIterationDone) {
          parcelId = parcelData.parcelId;
          carrierName = parcelData.carrierName;
          carrierService = parcelData.carrierService;
          trackingURL = parcelData.trackingURL;
          parcelReference = parcelData.parcelReference;
          shippingDate = parcelData.shippingDate;
          firstIterationDone = true;
        }

        Object.keys(parcelData.items).forEach(function (pd) {
          // loop through the items
          if (!empty(pd)) {
            item = new webRef.Item();
            item.sku = parcelData.items[pd].itemId;
            item.quantity = parcelData.items[pd].quantity.toString();
            items.push(item);
          }
        });

        // Example <shippingDate>2012-12-01</shippingDate>
        if (shippingDate != null) {
          var dateObj = new Date(shippingDate);
          shippedDate = new dw.util.Calendar(dateObj);
        } else {
          shippedDate = new dw.util.Calendar();
        }

        shippedDate = dw.util.StringUtils.formatCalendar(shippedDate, 'yyyy-MM-dd');

        parcelDetails.shippingDate = shippedDate;

        if (!empty(carrierName)) {
          parcelDetails.carrierName = carrierName;
        }

        if (!empty(carrierService)) {
          parcelDetails.carrierService = carrierService;
        }

        parcelDetails.items = items;
        parcelDetails.orderId = bfxOrderId;
        parcelDetails.parcelId = !empty(parcelId) ? parcelId : '';

        if (!empty(parcelReference)) {
          parcelDetails.parcelReference = parcelReference;
        } else {
          parcelDetails.parcelReference = '';
        }

        if (!empty(trackingURL)) {
          parcelDetails.trackingURL = trackingURL;
        } else {
          parcelDetails.trackingURL = '';
        }

        parcelDetails.tpl = 'false';

        parcelShipmentNotification.parcel = parcelDetails;
        parcelShipmentNotification.requestPackingSlip = false;
        return parcelShipmentNotification;
      } catch (e) {
        var err = e;
        Logger.error('borderfree.soap.shipmentNotification - Error message: ' + e.message);
      }
    },
    execute: function (svc, requestObject) {
      //request object is null here TODO
      return svc.serviceClient.parcelShipmentNotification(requestObject);
    },
    parseResponse: function (svc, responseObject) {
      var result = responseObject;
      return result;
    }
  });
}

/**
 *
 * @param {dw.order.Order} order
 * @param {Object} customObject
 * @param {String} response
 */
function bfxShipmentNotificationStatusToConfirmed(order, customObject, response) {
  customObject.custom.exported = true;
  customObject.custom.responseLog = (empty(customObject.custom.responseLog) ? '' : customObject.custom.responseLog + '\n') + JSON.stringify(response);

  if (order) {
    var arrayOfValues = [];
    if ('bfxShipmentNotificationParcelIDs' in order.custom && !empty(order.custom.bfxShipmentNotificationParcelIDs)) {
      for (var i = 0; i < order.custom.bfxShipmentNotificationParcelIDs.length; i++) {
        arrayOfValues.push(order.custom.bfxShipmentNotificationParcelIDs[i]);
      }
    }
    arrayOfValues.push(customObject.custom.parcelID);
    order.custom.bfxShipmentNotificationParcelIDs = arrayOfValues;
  }
}

/**
 *
 * @param {Object} customObject
 * @param {String} response
 */
function bfxShipmentNotificationStatusToError(customObject, response) {
  Transaction.wrap(function () {
    customObject.custom.responseLog = (empty(customObject.custom.responseLog) ? '' : customObject.custom.responseLog + '\n') + JSON.stringify(response);
    customObject.custom.numFailedAttempts += 1;
  });
}

/**
 * @param {dw.object.CustomObject} customObject
 * function to invoke notify shipment
 */
function notifyShipment(customObject) {
  try {
    initService();
    var result, parcelData, bfxOrderId;
    if (!empty(customObject)) {
      parcelData = JSON.parse(customObject.custom.bfxShipmentData);
      var order = dw.order.OrderMgr.getOrder(customObject.custom.orderID);
      if (empty(order)) {
        throw new Error('SFCC order ' + customObject.custom.orderID + ' was not found');
      }
      bfxOrderId = 'bfxOrderId' in order.custom && order.custom.bfxOrderId ? order.custom.bfxOrderId : null;

      result = svc.call(parcelData, bfxOrderId);

      var serviceStatus = result.status;

      if (serviceStatus === dw.svc.Result.OK) {
        bfxShipmentNotificationStatusToConfirmed(order, customObject, result.msg);
      } else {
        bfxShipmentNotificationStatusToError(customObject, result.msg);
      }
    }
  } catch (e) {
    var errorMsg = 'Error while executing Notifyshipment script ' + e;
    if (!empty(svc)) {
      errorMsg = 'Error while calling Notifyshipment Service ' + svc.URL + e;
    }
    Logger.error(errorMsg);

    bfxShipmentNotificationStatusToError(customObject, errorMsg);
  }
}

/**
 *
 * @param {Object} args
 * @return {Number}
 */
function execute(args) {
  notifyShipment(args.CustomObject);

  return PIPELET_NEXT;
}

exports.notifyShipment = notifyShipment;
