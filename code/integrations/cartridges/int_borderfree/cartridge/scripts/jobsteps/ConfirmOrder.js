'use strict';

/**
 * Pipelet for confirm order
 *
 * @input Order : dw.order.Order, created order
 * @input errorOrders : dw.util.ArrayList
 *
 * @output errorArray : dw.util.ArrayList
 */

/**
 * Require API dependencies
 */
const Service = require('dw/svc/Service');
const Logger = require('dw/system/Logger');
const Transaction = require('dw/system/Transaction');
var svc = require('dw/svc');
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var svc;

/**
 * Initialize the service components
 */
function initservice() {
  //Create a service object
  svc = LocalServiceRegistry.createService('borderfree.soap.orderConfirmation', {
    initServiceClient: function (svc) {
      this.webReference = webreferences.PartnerAPI;
      var cred = this.getConfiguration().getCredential();
      var stub = this.webReference.getService('PartnerAPI', 'PartnerAPIv2SOAP1.1');
      var usernameToken = new XML(
        '<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">\n      <wsse:UsernameToken>\n        <wsse:Username>' +
          cred.user +
          '</wsse:Username>\n        <wsse:Password>' +
          cred.password +
          '</wsse:Password>\n      </wsse:UsernameToken>\n    </wsse:Security>'
      );
      dw.rpc.SOAPUtil.setHeader(stub, usernameToken, true);
      return stub;
    },

    createRequest: function (svc, merchantOrderId, bfxOrderId) {
      try {
        var webRef = this.webReference;
        var orderConfirmation = new webRef.OrderConfirmation();
        var orderDetails = new webRef.OrderDetails();
        orderDetails.merchantOrderId = merchantOrderId;
        orderDetails.orderId = bfxOrderId;
        orderConfirmation.order = orderDetails;
        return orderConfirmation;
      } catch (e) {
        var err = e;
        Logger.error('borderfree.soap.orderConfirmation - Error message: ' + e.message);
      }
    },
    execute: function (svc, requestObject) {
      return svc.serviceClient.orderConfirmation(requestObject);
    },
    parseResponse: function (svc, responseObject) {
      var result = responseObject;
      return result;
    }
  });
}

function execute(args) {
  var errorOrdersArray = confirmOrder(args.Order, args.errorOrders);

  if (!empty(errorOrdersArray)) {
    args.errorArray = errorOrdersArray;
  }

  return PIPELET_NEXT;
}

/**
 * function to invoke confirm order
 */
function confirmOrder(order, errorOrders) {
  initservice();
  let service, result, jsonResponse, currentConfirmStatus;
  var errorMessage = '';
  currentConfirmStatus = order.custom.bfxConfirmStatus;
  try {
    result = svc.call(order.currentOrderNo, order.custom.bfxOrderId);

    var serviceStatus = result.status;

    if (serviceStatus === dw.svc.Result.OK && result.object !== null && result.object.confirmed === true) {
      updateBfxConfirmStatusToConfirmed(order);
    } else if (result.errorMessage.toString().indexOf('already confirmed') > -1) {
      updateBfxConfirmStatusToConfirmed(order);
    } else if (serviceStatus === dw.svc.Result.ERROR) {
      updateBfxConfirmStatusToError(order);
      errorMessage = 'Order : ' + order.orderNo + ' bfx order: ' + order.custom.bfxOrderId + ' error ' + result.errorMessage;
    } else if (serviceStatus === dw.svc.Result.SERVICE_UNAVAILABLE) {
      if (currentConfirmStatus.value === 'Unconfirmed') {
        updateBfxConfirmStatusToTimeout(order);
      } else if (currentConfirmStatus.value === 'Timeout') {
        updateBfxConfirmStatusToError(order);
      }
    }
  } catch (e) {
    Logger.error('Error while calling confirm order Service ' + svc.URL + e);
    errorMessage = 'Order : ' + order.orderNo + ' bfx order: ' + order.custom.bfxOrderId + ' error ' + e;
    updateBfxConfirmStatusToError(order);
  }

  //add error message if needed (i.e. has error message)\
  if (!empty(errorMessage)) {
    errorOrders.add(errorMessage);
  }

  return errorOrders;
}

function updateBfxConfirmStatusToConfirmed(order) {
  Transaction.wrap(function () {
    order.custom.bfxConfirmStatus = 'Confirmed';
  });
}

function updateBfxConfirmStatusToTimeout(order) {
  Transaction.wrap(function () {
    order.custom.bfxConfirmStatus = 'Timeout';
  });
}

function updateBfxConfirmStatusToError(order) {
  Transaction.wrap(function () {
    order.custom.bfxConfirmStatus = 'Error';
  });
}

function updateBfxConfirmStatusToUnConfirmed(order) {
  Transaction.wrap(function () {
    order.custom.bfxConfirmStatus = 'Unconfirmed';
  });
}

exports.confirmOrder = confirmOrder;
