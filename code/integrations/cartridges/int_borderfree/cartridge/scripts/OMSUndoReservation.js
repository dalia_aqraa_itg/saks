'use strict';
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Status = require('dw/system/Status');
var Site = require('dw/system/Site');
var Logger = require('dw/system/Logger');
var Transaction = require('dw/system/Transaction');
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

var prepareCancelcancelReservationRequest = function (reservationID) {
  var promise = {};
  var reqData = {};
  var reservationParameters = {};

  promise.Action = 'Cancel';
  promise.EnterpriseCode = Site.current.getCustomPreferenceValue('enterpriseCode');
  reservationParameters.ReservationID = reservationID;
  promise.ReservationParameters = reservationParameters;
  reqData.Promise = promise;
  return JSON.stringify(reqData);
};

function initService(serviceName) {
  var preferences = {
    API_KEY: 'hbcAPIKey' in Site.current.preferences.custom && Site.current.preferences.custom.hbcAPIKey ? Site.current.preferences.custom.hbcAPIKey : '',
    API_KEY_GW:
      'hbcGWAPIKey' in Site.current.preferences.custom && Site.current.preferences.custom.hbcGWAPIKey ? Site.current.preferences.custom.hbcGWAPIKey : ''
  };
  return LocalServiceRegistry.createService(serviceName, {
    createRequest: function (service, args) {
      service.setRequestMethod('POST'); // eslint-disable-next-line no-param-reassign
      service.addHeader('x-api-key', preferences.API_KEY);
      service.addHeader('x-apigw-api-id', preferences.API_KEY_GW);
      service.addHeader('Content-Type', 'application/json');
      return args;
    },

    parseResponse: function (service, response) {
      return response;
    },

    mockCall: function (service, client) {
      return {
        statusCode: 200,
        statusMessage: 'Success',
        text: ''
      };
    }
  });
}

exports.UnreserveInventory = function unreserveInventory() {
  try {
    // Get All Custom Object and check if Order is placed with that order Number.
    // IF not placed, un-reserve the Inventory with the inventory ID and Delete the custom object
    var BFXOrderContiner = CustomObjectMgr.getAllCustomObjects('BFXOrderContainer');
    while (BFXOrderContiner.hasNext()) {
      var bfxObject = BFXOrderContiner.next();
      // We will not check the order if created or not, as we are already deleting it which order placement.
      var reservationInventoryID = bfxObject.custom.bfxInventoryReservationID;
      if (reservationInventoryID) {
        Logger.debug('Unreserved the Reservation ID: ' + reservationInventoryID);
        var cancelReservationRequest = prepareCancelcancelReservationRequest(reservationInventoryID);
        var svc = initService('GetInventoryReservationService');
        var result = svc.call(cancelReservationRequest);
        if (result.status === 'OK') {
          // Remove the order Number stored in Session and Remove BFX Custom Object
          Transaction.wrap(function () {
            CustomObjectMgr.remove(bfxObject);
          });
        } else {
          Logger.error('Error while executing the script cancel Reservation Error:- ' + result.errorMessage ? result.errorMessage : '');
        }
      }
    }
    BFXOrderContiner.close();
    return new Status(Status.OK, 'OK', '');
  } catch (e) {
    Logger.error('Error occured while unreservation call to OMS for failed BFX order: ', e.message);
    return new Status(Status.ERROR, 'ERROR', e);
  }
};
