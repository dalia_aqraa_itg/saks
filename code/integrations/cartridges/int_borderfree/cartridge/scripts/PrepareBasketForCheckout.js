'use strict';

/**
 *
 * Single shipping allows only one shipment, shipping address, and shipping method per order.
 *
 * @input Basket: dw.order.Basket Basket object
 */

/* API Includes */
var ShippingMgr = require('dw/order/ShippingMgr');
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');
var Resource = require('dw/web/Resource');
var globalHelper = require('*/cartridge/scripts/helpers/globalHelper');

exports.updateBasket = function (cart, bfxOrder) {
  if (!empty(cart)) {
    setBorderFreeShippingMethod(cart);
    createShippingAddress(cart, bfxOrder);
    createBillingAddress(cart, bfxOrder);
    updateCartEmailAddress(cart, bfxOrder);
  }
};

function setBorderFreeShippingMethod(cart) {
  // Determine the list of applicable shipping methods.
  var borderFreeShippingId = dw.system.Site.current.getCustomPreferenceValue('bfxShippingMethodID') || 'Borderfree';
  var shipment = cart.getDefaultShipment();
  var shippingMethods = ShippingMgr.getAllShippingMethods();
  var shippingMethodsIter = shippingMethods.iterator();
  while (shippingMethodsIter.hasNext()) {
    var method = shippingMethodsIter.next();
    if (!method.ID.equals(borderFreeShippingId)) continue;
    // set this shipping method
    shipment.setShippingMethod(method);
  }
  ShippingMgr.applyShippingCost(cart);
}

function createShippingAddress(cart, bfxOrderJSON) {
  var bfxOrder = bfxOrderJSON;
  try {
    bfxOrder = JSON.parse(bfxOrderJSON.value);
  } catch (e) {}

  var defaultShipment = cart.getDefaultShipment();
  var shippingAddress = defaultShipment.shippingAddress;
  // if the shipment has no shipping address yet, create one
  if (shippingAddress == null) {
    shippingAddress = defaultShipment.createShippingAddress();
  }

  var domesticProfileExists = false;
  var domesticProfile = null;
  if (!empty(bfxOrder) && !empty(bfxOrder.domesticProfile) && bfxOrder.domesticProfile != null) {
    domesticProfile = bfxOrder.domesticProfile;
    domesticProfileExists = true;
  }
  Transaction.wrap(function () {
    if (domesticProfileExists) {
      var bfxShipping = !empty(domesticProfile.Shipping) ? domesticProfile.Shipping : null;
      if (!empty(bfxShipping) && !empty(bfxShipping.firstName)) {
        shippingAddress.setFirstName(globalHelper.replaceSpecialCharsAndEmojis(bfxShipping.firstName));
      }
      if (!empty(bfxShipping) && !empty(bfxShipping.lastName)) {
        shippingAddress.setLastName(globalHelper.replaceSpecialCharsAndEmojis(bfxShipping.lastName));
      }
      if (!empty(bfxShipping) && !empty(bfxShipping.addressLine1)) {
        shippingAddress.setAddress1(bfxShipping.addressLine1);
      }
      if (!empty(bfxShipping) && !empty(bfxShipping.addressLine2)) {
        if (bfxOrder && bfxOrder.orderId && bfxOrder.orderId.e4XOrderId) {
          shippingAddress.setAddress2(bfxOrder.orderId.e4XOrderId);
        }
      }
      if (!empty(bfxShipping) && !empty(bfxShipping.city)) {
        shippingAddress.setCity(bfxShipping.city);
      }
      if (!empty(bfxShipping) && !empty(bfxShipping.postalCode)) {
        shippingAddress.setPostalCode(bfxShipping.postalCode);
      }
      if (!empty(bfxShipping) && !empty(bfxShipping.region)) {
        shippingAddress.setStateCode(bfxShipping.region);
      }
      if (!empty(bfxShipping) && !empty(bfxShipping.country)) {
        shippingAddress.setCountryCode(bfxShipping.country);
      }
      if (!empty(bfxShipping) && !empty(bfxShipping.primaryPhone)) {
        shippingAddress.setPhone(bfxShipping.primaryPhone);
      }
    } else {
      shippingAddress.setFirstName(globalHelper.replaceSpecialCharsAndEmojis(dw.system.Site.current.getCustomPreferenceValue('bfxShippingFirstName')));

      shippingAddress.setLastName(globalHelper.replaceSpecialCharsAndEmojis(dw.system.Site.current.getCustomPreferenceValue('bfxShippingLastName')));

      shippingAddress.setAddress1(dw.system.Site.current.getCustomPreferenceValue('bfxShippingAddress1'));

      shippingAddress.setAddress2(dw.system.Site.current.getCustomPreferenceValue('bfxShippingAddress2'));

      shippingAddress.setCity(dw.system.Site.current.getCustomPreferenceValue('bfxShippingCity'));

      shippingAddress.setPostalCode(dw.system.Site.current.getCustomPreferenceValue('bfxShippingPostalCode'));

      shippingAddress.setStateCode(dw.system.Site.current.getCustomPreferenceValue('bfxShippingStateCode'));

      shippingAddress.setCountryCode(dw.system.Site.current.getCustomPreferenceValue('bfxShippingCountryCode'));

      shippingAddress.setPhone(dw.system.Site.current.getCustomPreferenceValue('bfxShippingPhone'));
    }
  });
}

/**
 * this function creates a billing address if not present and updates billing info accordingly.
 * @param cart
 * @returns
 */

function createBillingAddress(cart, bfxOrderJSON) {
  var bfxOrder;
  if (bfxOrderJSON && !empty(bfxOrderJSON)) {
    bfxOrder = JSON.parse(bfxOrderJSON);
  }
  var billingAddress = cart.getBillingAddress();
  Transaction.wrap(function () {
    if (!billingAddress) {
      billingAddress = cart.createBillingAddress();
    }
    billingAddress.setFirstName(globalHelper.replaceSpecialCharsAndEmojis(dw.system.Site.current.getCustomPreferenceValue('bfxBillingFirstName')));
    billingAddress.setLastName(globalHelper.replaceSpecialCharsAndEmojis(dw.system.Site.current.getCustomPreferenceValue('bfxBillingLastName')));
    billingAddress.setAddress1(dw.system.Site.current.getCustomPreferenceValue('bfxBillingAddress1'));
    billingAddress.setAddress2(dw.system.Site.current.getCustomPreferenceValue('bfxBillingAddress2'));
    billingAddress.setCity(dw.system.Site.current.getCustomPreferenceValue('bfxBillingCity'));
    billingAddress.setPostalCode(dw.system.Site.current.getCustomPreferenceValue('bfxBillingPostalCode'));
    billingAddress.setStateCode(dw.system.Site.current.getCustomPreferenceValue('bfxBillingStateCode'));
    billingAddress.setCountryCode(dw.system.Site.current.getCustomPreferenceValue('bfxBillingCountryCode'));
    billingAddress.setPhone(dw.system.Site.current.getCustomPreferenceValue('bfxBillingPhone'));
  });
}

/**
 * this function to update the email address to basket.
 * @param cart
 * @returns
 */

function updateCartEmailAddress(cart, bfxOrderJSON) {
  var emailSuffix = dw.system.Site.current.getCustomPreferenceValue('bfxE4XEmail');
  var bfxOrder;
  if (bfxOrderJSON && !empty(bfxOrderJSON)) {
    bfxOrder = JSON.parse(bfxOrderJSON);
  }
  var billingAddress = cart.getBillingAddress();
  Transaction.wrap(function () {
    if (bfxOrder && bfxOrder.orderId && bfxOrder.orderId.e4XOrderId && emailSuffix) {
      // Update Order email with E4X number.
      cart.setCustomerEmail(bfxOrder.orderId.e4XOrderId.toString().toLocaleLowerCase() + emailSuffix);
    }
  });
}
