'use strict';

/**
 * Gift card hook for form processor
 * @return {Object} an object that contains error information
 */
function processForm(req, paymentForm, viewFormData) {
  var viewData = viewFormData;

  viewData.paymentMethod = {
    value: paymentForm.paymentMethod.value,
    htmlName: paymentForm.paymentMethod.value
  };
  return {
    error: false,
    viewData: viewData
  };
}

/**
 *  Gift card hook since no save information is supported in GC
 */
function savePaymentInformation() {
  return;
}

exports.processForm = processForm;
exports.savePaymentInformation = savePaymentInformation;
