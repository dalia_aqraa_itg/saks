'use strict';

/**
 * Pay with Points hook form processor
 * @return {Object} an object that contains error information
 */
function processForm(req, paymentForm, viewFormData) {
  var viewData = viewFormData;

  viewData.paymentMethod = {
    value: paymentForm.paymentMethod.value,
    htmlName: paymentForm.paymentMethod.value
  };
  return {
    error: false,
    viewData: viewData
  };
}

/**
 *  No save information is supported for Pay With Points
 */
function savePaymentInformation() {
  return;
}

exports.processForm = processForm;
exports.savePaymentInformation = savePaymentInformation;
