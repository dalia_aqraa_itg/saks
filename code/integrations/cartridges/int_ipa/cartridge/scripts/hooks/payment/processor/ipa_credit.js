'use strict';

var collections = require('*/cartridge/scripts/util/collections');

var PaymentInstrument = require('dw/order/PaymentInstrument');
var PaymentMgr = require('dw/order/PaymentMgr');
var PaymentStatusCodes = require('dw/order/PaymentStatusCodes');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var Site = require('dw/system/Site');
var TokenExFacade = require('*/cartridge/scripts/services/TokenExFacade');
var Logger = require('dw/system/Logger');
var ipaLogger = Logger.getLogger('IPA-Service-Log', 'IPA-Service');
var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
var allowOrderIFServiceDown = ipaConstants.allowOrderIfAuthServiceDown;
var TCCHelper = require('*/cartridge/scripts/checkout/TCCHelpers');
var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');
var sitePrefs = require('dw/system/Site').getCurrent().getPreferences().getCustom();

/**
 * Creates a token. This should be replaced by utilizing a tokenization provider
 * @returns {string} a token
 */
function createToken(encryptedCard) {
  var token;
  if ('enableTokenEx' in Site.current.preferences.custom && Site.current.preferences.custom.enableTokenEx === true) {
    token = TokenExFacade.tokenizeEncryptedCard(encryptedCard);
  } else {
    if (!empty(Site.current.preferences.custom.enableTokenExRetry) && Site.current.preferences.custom.enableTokenExRetry === true) {
      token = {
        error: false,
        success: true,
        token: '',
        tokenRetry: true,
        tokenEx: true
      };
    } else {
      token = {
        error: true,
        success: false,
        tokenRetry: false
      };
    }
  }
  return token;
}

/**
 * Verifies that entered credit card information is a valid card. If the information is valid a
 * credit card payment instrument is created
 * @param {dw.order.Basket} basket Current users's basket
 * @param {Object} paymentInformation - the payment information
 * @return {Object} returns an error object
 */
function Handle(basket, paymentInformation) {
  var currentBasket = basket;
  var cardErrors = {};
  var cardNumber = paymentInformation.cardNumber.value;
  var cardSecurityCode = paymentInformation.securityCode.value;
  var expirationMonth = paymentInformation.expirationMonth.value;
  var expirationYear = paymentInformation.expirationYear.value;
  var creditCardToken = paymentInformation.creditCardToken;
  var cardOwnerName = paymentInformation.cardOwner.value;
  var paymentErrors = [];
  var serverErrors = [];
  var creditCardStatus;
  var tokenObj = {};

  var cardType = paymentInformation.cardType.value;
  var paymentCard = PaymentMgr.getPaymentCard(cardType);
  // TCC is already a Token. So we not required to call TokenEX
  if (cardType && cardType === 'TCC') {
    var entryMode = paymentInformation.entryMode;
    if (entryMode && entryMode === ipaConstants.STORED_MODE) {
      cardNumber = paymentInformation.creditCardToken;
    }
    if (TCCHelper.isTCCDateExpired(cardNumber)) {
      paymentErrors.push(Resource.msg('tcc.not.valid', 'creditCard', null));

      return {
        fieldErrors: [cardErrors],
        serverErrors: serverErrors,
        paymentErrors: paymentErrors,
        error: true
      };
    }
    // Return logic if any TCC parameter is invalid.
  } else if (!paymentInformation.creditCardToken) {
    // Generate the Card Token Based on Encrypted data and use that token for LUHN validations.
    tokenObj = createToken(cardNumber);
    if (paymentCard && tokenObj.success === true) {
      if (!tokenObj.tokenRetry && tokenObj.token) {
        creditCardStatus = {}; //  fix me
        creditCardStatus.error = false; // fix me
      } else if (tokenObj.tokenRetry) {
        creditCardStatus = {};
        creditCardStatus.error = false;
      } else if (tokenObj.success === true && !tokenObj.token) {
        cardErrors[paymentInformation.cardNumber.htmlName] = Resource.msg('error.invalid.card.number', 'creditCard', null);

        return {
          fieldErrors: [cardErrors],
          serverErrors: serverErrors,
          error: true
        };
      } else {
        // Stop Customer if Retry not enabled.
        serverErrors.push(Resource.msg('error.technical', 'checkout', null));
        return {
          fieldErrors: [cardErrors],
          serverErrors: serverErrors,
          error: true
        };
      }
    } else if (tokenObj && tokenObj.success === false) {
      // Stop Customer if Retry not enabled.
      serverErrors.push(Resource.msg('error.technical', 'checkout', null));
      return {
        fieldErrors: [cardErrors],
        serverErrors: serverErrors,
        error: true
      };
    } else {
      cardErrors[paymentInformation.cardNumber.htmlName] = Resource.msg('error.invalid.card.number', 'creditCard', null);

      return {
        fieldErrors: [cardErrors],
        serverErrors: serverErrors,
        error: true
      };
    }

    if (creditCardStatus.error) {
      collections.forEach(creditCardStatus.items, function (item) {
        switch (item.code) {
          case PaymentStatusCodes.CREDITCARD_INVALID_CARD_NUMBER:
            cardErrors[paymentInformation.cardNumber.htmlName] = Resource.msg('error.invalid.card.number', 'creditCard', null);
            break;

          case PaymentStatusCodes.CREDITCARD_INVALID_EXPIRATION_DATE:
            cardErrors[paymentInformation.expirationMonth.htmlName] = Resource.msg('error.expired.credit.card', 'creditCard', null);
            cardErrors[paymentInformation.expirationYear.htmlName] = Resource.msg('error.expired.credit.card', 'creditCard', null);
            break;

          case PaymentStatusCodes.CREDITCARD_INVALID_SECURITY_CODE:
            cardErrors[paymentInformation.securityCode.htmlName] = Resource.msg('error.invalid.security.code', 'creditCard', null);
            break;
          default:
            serverErrors.push(Resource.msg('error.card.information.error', 'creditCard', null));
        }
      });

      return {
        fieldErrors: [cardErrors],
        serverErrors: serverErrors,
        error: true
      };
    }
  }

  Transaction.wrap(function () {
    var paymentInstruments = currentBasket.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);

    var paypalPI = currentBasket.getPaymentInstruments('PayPal');

    if (paypalPI) {
      collections.forEach(paypalPI, function (item) {
        currentBasket.removePaymentInstrument(item);
      });
    }

    collections.forEach(paymentInstruments, function (item) {
      currentBasket.removePaymentInstrument(item);
    });

    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var total = COHelpers.getNonGiftCardAmount(currentBasket, null);

    var paymentInstrument = currentBasket.createPaymentInstrument(PaymentInstrument.METHOD_CREDIT_CARD, total.remainingAmount);

    paymentInstrument.setCreditCardHolder(cardOwnerName);
    paymentInstrument.setCreditCardType(cardType);
    paymentInstrument.custom.entry_mode = paymentInformation.entryMode;
    Logger.debug(
      'ipa_credit creditCardExpirationMonth :{0} creditCardExpirationYear:{1} authorization_tracer : {2}',
      paymentInstrument.creditCardExpirationMonth,
      paymentInstrument.creditCardExpirationYear,
      paymentInstrument.custom.authorization_tracer
    );

    if (cardType && cardType === 'TCC') {
      paymentInstrument.setCreditCardNumber(cardNumber);
      // Set the card type to SAKS for down stream processing
      paymentInstrument.setCreditCardType('SAKS');
      // Extract expiration year and month to be set below
      expirationYear = parseInt(cardNumber.substr(16, 2)) + 2000;
      expirationMonth = parseInt(cardNumber.substr(18, 2));

      var date = TCCHelper.getExpirationDate(cardNumber);
      paymentInstrument.custom.tccExpirationDate = '' + date;
    } else if (!tokenObj.tokenEx) {
      paymentInstrument.setCreditCardNumber(cardNumber);
    } else if (paymentInformation.creditCardToken) {
      paymentInstrument.setCreditCardNumber(paymentInformation.creditCardToken);
    } else if (tokenObj.token) {
      paymentInstrument.setCreditCardNumber(tokenObj.token);
    }

    if (expirationMonth) {
      paymentInstrument.setCreditCardExpirationMonth(expirationMonth);
    }
    if (expirationYear) {
      paymentInstrument.setCreditCardExpirationYear(expirationYear);
    }

    if (cardType && cardType === 'TCC') {
      // TCC Card itself is an Token
      if (cardNumber && cardNumber.length == 29) {
        var token = cardNumber;
        paymentInstrument.setCreditCardToken(token);
        creditCardToken = token;
      }
    } else if (paymentInformation.creditCardToken) {
      paymentInstrument.setCreditCardToken(paymentInformation.creditCardToken);
    } else if (tokenObj && tokenObj.token) {
      paymentInstrument.setCreditCardToken(tokenObj.token);
    }

    if (cardType === 'TCC') {
      // Don't set Order for Retry as TCC is already tokenized
    } else if (tokenObj && tokenObj.tokenRetry && !paymentInstrument.creditCardToken) {
      currentBasket.custom.tokenExRetry = true;
      currentBasket.custom.tokenExData = cardNumber; // This is encrypted data.
    }
  });

  if (cardType && cardType === 'SAKS') {
    cardNumber = cardNumber ? cardNumber : paymentInformation.creditCardToken;
    if (
      (cardNumber && (cardNumber.length === 8 || cardNumber.length === 10)) ||
      (tokenObj.token && (tokenObj.token.length === 8 || tokenObj.token.length === 10))
    ) {
      paymentErrors.push(Resource.msg('plcc.no.longer.valid', 'creditCard', null));

      return {
        fieldErrors: [cardErrors],
        serverErrors: serverErrors,
        paymentErrors: paymentErrors,
        error: true
      };
    }
  }

  return {
    fieldErrors: cardErrors,
    serverErrors: serverErrors,
    error: false,
    token: creditCardToken ? creditCardToken : tokenObj.token // eslint-disable-line
  };
}

/**
 * Authorizes a payment using a credit card. Customizations may use other processors and custom
 *      logic to authorize credit card payment.
 * @param {number} orderNumber - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current
 *      payment method
 * @param {Object} req -  request object
 * @return {Object} returns an error object
 */
function Authorize(orderNumber, paymentInstrument, paymentProcessor, params) {
  var serverErrors = [];
  var fieldErrors = {};
  var error = false;

  try {
    Transaction.wrap(function () {
      paymentInstrument.paymentTransaction.setTransactionID(orderNumber);
      paymentInstrument.paymentTransaction.setPaymentProcessor(paymentProcessor);
    });
    var OrderMgr = require('dw/order/OrderMgr');
    var order = OrderMgr.getOrder(orderNumber);
    var ipaUtil = require('*/cartridge/scripts/util/ipaUtil');

    if ('tokenExRetry' in order.custom && order.custom.tokenExRetry) {
      Transaction.wrap(function () {
        order.custom.orderStatus = ['TOKEN_HOLD'];
      });
      if (!allowOrderIFServiceDown) {
        error = true;
      }
      Transaction.wrap(function () {
        var ctr = 1;
        var paymentInstruments = order.getPaymentInstruments();
        Logger.debug('SFSX-3637 ipa_credit authorize paymentInstruments.length {0} ctr: {1}', paymentInstruments.length, ctr);
        collections.forEach(paymentInstruments, function (paymentInst) {
          paymentInst.custom.chargeSequence = ctr.toFixed();
          ctr++;
        });
      });
    } else {
      var args = {};
      args.Order = order;
      args.remoteAddress = params.remoteAddress;
      args.type = 'REGULAR';
      args.session = params.session;
      args.authType = 'Auth';
      args.failAttempts = params.failAttempts;
      args.customer = params.customer;
      args.deviceFingerPrintID = params.deviceFingerPrintID;
      if (params.cvn) {
        args.cvn = params.cvn;
      }
      var result = ipaUtil.authorizeCreditCard(args);

      error = result.error;
      if (result.errorcode) {
        serverErrors.push(Resource.msg(result.errorcode, 'ipa', null));
      }
    }

    // C2NFTS: Validation API
    if (sitePrefs.forterEnabled && order) {
      // these lines must be uncommented in case if you want to activate the pre-authorization flow or being included in a top-level cartridge
      var orderNumber = order.getCurrentOrderNo();
      var argOrderValidate = {
        orderNumber               : orderNumber,
        orderValidateAttemptInput : 1,
        authorizationStep         : 'POST_AUTHORIZATION'
      };
      var forterCall       = require('*/cartridge/scripts/pipelets/forter/forterValidate');
      var forterDecision   = forterCall.validateOrder(argOrderValidate);

      // in case if no response from Forter, try to call one more time
      if (forterDecision.result === false && forterDecision.orderValidateAttemptInput == 2) {
        argOrderValidate = {
          orderNumber               : orderNumber,
          orderValidateAttemptInput : 2,
          authorizationStep         : 'POST_AUTHORIZATION'
        };
        forterCall       = require('*/cartridge/scripts/pipelets/forter/forterValidate');
        forterDecision   = forterCall.validateOrder(argOrderValidate);
      }

      if (forterDecision.JsonResponseOutput.processorAction == 'void') {
        ReverseAuth(orderNumber, params);

        Transaction.wrap(function () {
          OrderMgr.failOrder(order, true);
        });

        error = true;

        if (!empty(forterDecision.PlaceOrderError)) {
          serverErrors.push(
            forterDecision.PlaceOrderError.code
          );
        } else {
          serverErrors.push(
            Resource.msg('error.technical', 'checkout', null)
          );
        }
      }
    }
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    ipaLogger.info('Order Number: ' + orderNumber + 'Stack Trace: ' + errorMsg);
    error = true;
    serverErrors.push(Resource.msg('error.technical', 'checkout', null));
  }

  return {
    fieldErrors: fieldErrors,
    serverErrors: serverErrors,
    error: error
  };
}

/**
 * Reverses a payment. Customizations may use other processors and custom
 *      logic to authorize credit card payment.
 * @param {number} orderNumber - The current order's number
 * @param {Object} req -  request object
 * @return {Object} returns an error object
 */
function ReverseAuth(orderNumber, params) {
  var serverErrors = [];
  var fieldErrors = {};
  var error = false;

  try {
    var OrderMgr = require('dw/order/OrderMgr');
    var order = OrderMgr.getOrder(orderNumber);
    var ipaUtil = require('*/cartridge/scripts/util/ipaUtil');

    var args = {};
    args.Order = order;
    args.remoteAddress = order.remoteHost;
    args.type = 'REGULAR';
    args.session = params.session;
    args.authType = 'Reversal';

    args.failAttempts = params.failAttempts; // Fail attempts is 1. Fix me
    args.customer = params.customer;
    args.deviceFingerPrintID = params.deviceFingerPrintID;
    var result = ipaUtil.authorizationReversal(args);
    if (!result.error) {
      Transaction.wrap(function () {
        order.custom.reversalForCancelOrder = true;
      });
    }

    error = result.error;
    if (result.errorcode) {
      serverErrors.push(Resource.msg(result.errorcode, 'ipa', null));
    }
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    ipaLogger.info('Order Number: ' + orderNumber + 'Stack Trace: ' + errorMsg);
    error = true;
    serverErrors.push(Resource.msg('error.technical', 'checkout', null));
  }

  return {
    fieldErrors: fieldErrors,
    serverErrors: serverErrors,
    error: error
  };
}
/**
 * Creates a token
 * @param {Object} rawCard - creating encrypted card
 * @returns {string} a token
 */
function createTokenUnencrypted (rawCard) {
  var token;
  if ('enableTokenEx' in Site.current.preferences.custom && Site.current.preferences.custom.enableTokenEx === true) {
      token = TokenExFacade.tokenizeUnEncryptedCard(rawCard);
  } else {
      token = {
          error: false,
          success: true,
          token: Math.random().toString(36).substr(2),
          tokenRetry: false,
          tokenEx: false
      };
  }
  return token;
};
/**
 * Authorize Klarna VCN
 * @param {string} orderNumber - order number
 * @param {Object} _ - Payment Instument
 * @param {Object} __ - Payment Processor
 * @return {Object} contains error or success
 */
function VCNAuthorize(orderNumber, _, __, ipaContext) {
  var genericKlarnaError = {
      fieldErrors: {},
      serverErrors: ['KLARNAERROR'],
      error: true
  };
  var serverErrors = [];
  var error = false;

  var Cipher = require('dw/crypto/Cipher');
  var Encoding = require('dw/crypto/Encoding');
  var OrderMgr = require('dw/order/OrderMgr');
  var order = OrderMgr.getOrder(orderNumber);

  var VCNPrivateKey = Site.getCurrent().getCustomPreferenceValue('vcnPrivateKey');
  var cipher = new Cipher();
  var keyEncryptedBase64 = order.custom.kpVCNAESKey;
  var keyEncryptedBytes = Encoding.fromBase64(keyEncryptedBase64);
  var keyDecrypted = cipher.decryptBytes(keyEncryptedBytes, VCNPrivateKey, 'RSA/ECB/PKCS1PADDING', null, 0);
  var keyDecryptedBase64 = Encoding.toBase64(keyDecrypted);
  var cardDataEncryptedBase64 = order.custom.kpVCNPCIData;
  var cardDataEncryptedBytes = Encoding.fromBase64(cardDataEncryptedBase64);
  var cardDecrypted = cipher.decryptBytes(cardDataEncryptedBytes, keyDecryptedBase64, 'AES/CTR/NoPadding', order.custom.kpVCNIV, 0);
  var cardDecryptedUtf8 = decodeURIComponent(cardDecrypted);
  var cardObj = JSON.parse(cardDecryptedUtf8);
  var expiryDateArr = cardObj.expiry_date.split('/');

  // Retrieve ecnrypted card details
  var cardNumber = cardObj.pan;
  var cardSecurityCode = cardObj.cvv;
  var expirationMonth = Number(expiryDateArr[0]);
  var expirationYear = Number(expiryDateArr[1]) + 2000; // hackey but this works for now -PG

  var cardOwnerName = order.custom.kpVCNHolder;
  var cardType = order.custom.kpVCNBrand;
  var paymentCard = PaymentMgr.getPaymentCard('Visa' || cardType);

  var tokenObj = createTokenUnencrypted(cardNumber); // tokenex functionality

  if (!paymentCard || tokenObj.success !== true ||
      (!tokenObj.token && !tokenObj.tokenRetry)
  ) {
      return genericKlarnaError;
  }

  var creditCardStatus = { error: false };

  if (tokenObj.token && !tokenObj.tokenRetry) {
      creditCardStatus = paymentCard.verify(
          expirationMonth,
          expirationYear,
          tokenObj.tokenEx ? tokenObj.token : cardNumber,
          cardSecurityCode
      );
  }
  if (creditCardStatus.error) {
      return genericKlarnaError;
  }

  var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
  Transaction.wrap(function () {
      var klarnaPI = order.getPaymentInstruments('KLARNA_PAYMENTS');
      collections.forEach(klarnaPI, function (item) {
          order.removePaymentInstrument(item);
      });

      var orderTotal;
      if('miraklMarketplaceEnabled' in Site.current.preferences.custom && Site.current.preferences.custom.miraklMarketplaceEnabled){
          var Money = require("dw/value/Money");
          var orderTotals = COHelpers.getOrderTotals(order);
          if(!empty(orderTotals) && !empty(orderTotals.orderTotal)){
              orderTotal = new Money(orderTotals.orderTotal, session.currency.currencyCode);
          }
      }else{
          orderTotal = order.totalGrossPrice;
      }

      var PI = order.createPaymentInstrument(
          PaymentInstrument.METHOD_CREDIT_CARD, orderTotal
      );

      PI.setCreditCardHolder(cardOwnerName);
      PI.setCreditCardType(cardType);
      PI.setCreditCardExpirationMonth(expirationMonth);
      PI.setCreditCardExpirationYear(expirationYear);
      PI.setCreditCardNumber(
          tokenObj.tokenEx ? tokenObj.token : cardNumber
      );
      if (tokenObj.token) {
          PI.setCreditCardToken(tokenObj.token);
      }
      if (tokenObj.tokenRetry) {
          order.custom.tokenExRetry = true;
          order.custom.tokenExData = cardNumber;
      }

      PI.paymentTransaction.setTransactionID(orderNumber);
      PI.paymentTransaction.setPaymentProcessor(
          PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_CREDIT_CARD).paymentProcessor
      );
      PI.custom.entry_mode = ipaConstants.ENTRY_MODE;
      PI.custom.isKlarnaPI = true;
  });
  var ipaUtil = require('*/cartridge/scripts/util/ipaUtil');

  if ('tokenExRetry' in order.custom && order.custom.tokenExRetry) {
      Transaction.wrap(function () {
          order.custom.orderStatus = ['TOKEN_HOLD'];
      });

      var allowOrderIFServiceDown = ipaConstants.allowOrderIfAuthServiceDown;
      if (!allowOrderIFServiceDown) {
          error = true;
      }
      Transaction.wrap(function () {
          var ctr = 1;
          collections.forEach(order.getPaymentInstruments(), function (pInst) {
              pInst.custom.chargeSequence = ctr.toFixed();
              ctr++;
          });
      });
  } else {
      var ipaBody = {
          Order: order,
          remoteAddress: ipaContext.remoteAddress,
          type: 'REGULAR',
          authType: 'Auth',
          session: ipaContext.session,
          failAttempts: ipaContext.failAttempts,
          customer: ipaContext.customer,
          deviceFingerPrintID: ipaContext.deviceFingerPrintID,
          cvn: cardSecurityCode
      };
      var ipaResult = ipaUtil.authorizeCreditCard(ipaBody);
      error = ipaResult.error;
      if (ipaResult.errorcode) {
          serverErrors.push(
              Resource.msg(ipaResult.errorcode, 'ipa', null)
          );
      }
  }

  // C2NFTS: Validation API
  if (sitePrefs.forterEnabled && order) {
    // these lines must be uncommented in case if you want to activate the pre-authorization flow or being included in a top-level cartridge
    var orderNumber = order.getCurrentOrderNo();
    var argOrderValidate = {
      orderNumber               : orderNumber,
      orderValidateAttemptInput : 1,
      authorizationStep         : 'POST_AUTHORIZATION'
    };
    var forterCall       = require('*/cartridge/scripts/pipelets/forter/forterValidate');
    var forterDecision   = forterCall.validateOrder(argOrderValidate);

    // no need to handle the decline flow as the forter will always return not-review status for the paypal and klarna payment methods.
  }

  return {
      fieldErrors: {},
      serverErrors: serverErrors,
      error: error
  };
};
exports.Reverse = ReverseAuth;
exports.Handle = Handle;
exports.Authorize = Authorize;
exports.createToken = createToken;
exports.VCNAuthorize = VCNAuthorize;
