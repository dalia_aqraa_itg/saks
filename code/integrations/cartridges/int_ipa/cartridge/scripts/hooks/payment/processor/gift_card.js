'use strict';

/**
 * Verifies that entered gift card information is a valid card. If the information is valid a
 * gift card payment instrument is created
 * @param {dw.order.Basket} basket Current users's basket
 * @param {number} balance - GC Card balance
 * @param {Object} cardNumber - the gift card number entered by user
 * @param {number} pin - GC pin
 * @param {boolean} handlePass - boolean to pass handle or not
 * @return {Object} returns an error object
 */
function Handle(basket, balance, handlePass, cardNumber, pin) {
  var result = {};
  result.error = false;
  if (!handlePass) {
    var currentBasket = basket;
    var arrayHelper = require('*/cartridge/scripts/util/array');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var Transaction = require('dw/system/Transaction');
    var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
    result.error = true;

    var paypalPaymentInstrument = basket.getPaymentInstruments(ipaConstants.PAYPAL);
    arrayHelper.find(paypalPaymentInstrument, function (payPalInstr) {
      Transaction.wrap(function () {
        currentBasket.removePaymentInstrument(payPalInstr);
      });
    });

    let ccPaymentInstruments = currentBasket.getPaymentInstruments(ipaConstants.CREDIT_CARD_PAYMENT_METHOD);
    arrayHelper.find(ccPaymentInstruments, function (ccPaymentIntr) {
      Transaction.wrap(function () {
        currentBasket.removePaymentInstrument(ccPaymentIntr);
      });
    });
    var paymentInstruments = currentBasket.getPaymentInstruments();
    var existingPaymentInstrument = arrayHelper.find(paymentInstruments, function (paymentInstrument) {
      return paymentInstrument.custom.giftCardNumber === cardNumber;
    });

    if (!existingPaymentInstrument) {
      var total = COHelpers.getNonGiftCardAmount(currentBasket, balance);
      if (total.giftCardAmountToApply > 0) {
        Transaction.wrap(function () {
          var paymentInstrument = currentBasket.createPaymentInstrument(ipaConstants.GIFT_CARD, total.giftCardAmountToApply);
          paymentInstrument.custom.giftCardNumber = cardNumber;
          paymentInstrument.custom.giftCardPin = pin;
          var type = COHelpers.getGiftCardType(cardNumber);
          paymentInstrument.custom.giftCardType = type;
          if (total.amountLeft) {
            paymentInstrument.custom.giftBalanceLeft = true;
          } else {
            paymentInstrument.custom.giftBalanceLeft = false;
          }
          result.error = false;
          result.type = 'APPLIED';
          result.amountApplied = paymentInstrument.getPaymentTransaction().getAmount();
        });
      } else {
        result.type = 'NO_CARD_REQUIRED';
      }
    } else {
      result.type = 'CARD_ALREADY_IN_CART';
    }
  }
  return result;
}

/**
 * Authorizes a payment using a gift card. Customizations may use other processors and custom
 *      logic to authorize GC & card payment.
 * @param {number} orderNumber - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current
 *      payment method
 * @param {Object} params -  request object
 * @return {Object} returns an error object
 */
function Authorize(orderNumber, paymentInstrument, paymentProcessor, params) {
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  return hooksHelper(
    'app.payment.processor.ipa_credit',
    'Authorize',
    [orderNumber, paymentInstrument, paymentProcessor, params],
    require('*/cartridge/scripts/hooks/payment/processor/ipa_credit').Authorize
  );
}
exports.Handle = Handle;
exports.Authorize = Authorize;
