'use strict';

/**
 * If the information is valid a
 *  payment instrument is created
 * @param {dw.order.Basket} basket Current users's basket
 * @param {number} points - amount of points to apply to cart
 * @return {Object} returns an error object
 */
function Handle(basket, points) {
  var collections = require('*/cartridge/scripts/util/collections');
  var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
  var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
  var Transaction = require('dw/system/Transaction');

  var result = {};
  result.error = false;
  var currentBasket = basket;

  // Required check when processor is called via CheckoutServices-SubmitPayment
  if (points === 'undefined') {
    var pwpPI = currentBasket.getPaymentInstruments(ipaConstants.REWARD_POINTS);
    var amount;
    collections.forEach(pwpPI, function (pwpPaymentIntr) {
      amount = pwpPaymentIntr.custom.pointsApplied;
    });

    points = amount;
  }

  // Removes PayPal Payment instrument -
  // As this payment method will not be used with pay with points currently
  var paypalPaymentInstrument = currentBasket.getPaymentInstruments(ipaConstants.PAYPAL);
  collections.forEach(paypalPaymentInstrument, function (payPalInstr) {
    Transaction.wrap(function () {
      currentBasket.removePaymentInstrument(payPalInstr);
    });
  });

  // Removes Credit Card Payment Instrument - if previously entered to recalculate multiple tender types
  var ccPaymentInstruments = currentBasket.getPaymentInstruments(ipaConstants.CREDIT_CARD_PAYMENT_METHOD);
  collections.forEach(ccPaymentInstruments, function (ccPaymentIntr) {
    Transaction.wrap(function () {
      currentBasket.removePaymentInstrument(ccPaymentIntr);
    });
  });

  // Removes Gift Card Instruments - if previously entered to recalculate multiple tender types
  var gcPaymentInstruments = currentBasket.getPaymentInstruments(ipaConstants.GIFT_CARD);
  collections.forEach(gcPaymentInstruments, function (gcPaymentIntr) {
    Transaction.wrap(function () {
      currentBasket.removePaymentInstrument(gcPaymentIntr);
    });
  });

  // Removes any pay with points payment instrument from the basket and recreates it later
  var pwpPaymentInstruments = currentBasket.getPaymentInstruments(ipaConstants.REWARD_POINTS);
  collections.forEach(pwpPaymentInstruments, function (pwpPaymentIntr) {
    Transaction.wrap(function () {
      currentBasket.removePaymentInstrument(pwpPaymentIntr);
    });
  });

  // Get the dollar value to apply - calculates the amount to charge for the payment instrument.
  var total = COHelpers.getRewardsAmount(currentBasket, points);

  if (!total.error && total.dollarAmountToApply.value > 0) {
    Transaction.wrap(function () {
      // Create PWP payment instrument
      var paymentInstrument = currentBasket.createPaymentInstrument(ipaConstants.REWARD_POINTS, total.dollarAmountToApply);

      // Add a value to custom property "pointsApplied" in the OrderPaymentInstrument system object - the number of points applied
      paymentInstrument.custom.pointsApplied = total.points;

      result.amountApplied = paymentInstrument.getPaymentTransaction().getAmount();
      result.amountLeft = total.amountLeft;
      result.error = false;
      result.type = 'APPLIED';
    });
  } else {
    result.error = true;
  }

  return result;
}

/**
 * Authorizes a payment using a gift card. Customizations may use other processors and custom
 *      logic to authorize GC & card payment.
 * @param {number} orderNumber - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current
 *      payment method
 * @param {Object} params -  request object
 * @return {Object} returns an error object
 */
function Authorize(orderNumber, paymentInstrument, paymentProcessor, params) {
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  return hooksHelper(
    'app.payment.processor.ipa_credit',
    'Authorize',
    [orderNumber, paymentInstrument, paymentProcessor, params],
    require('*/cartridge/scripts/hooks/payment/processor/ipa_credit').Authorize
  );
}
exports.Handle = Handle;
exports.Authorize = Authorize;
