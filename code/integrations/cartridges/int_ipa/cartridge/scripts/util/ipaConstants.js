var Site = require('dw/system/Site');
var CurrentSite = Site.current.preferences;
var preferences = CurrentSite ? CurrentSite.custom : {};

var Constants = {
  API_KEY: preferences.hbcAPIKey || 'IRtnSplqOV6GXx0IVpeNbaHnW0nkslQ91Yekdt7L',

  STORE_NUMBER: preferences.storeNumber || '8689',

  CREDIT_CARD_PAYMENT_METHOD: 'CREDIT_CARD', // Payment method on the SFCC paymentMethods

  CHANNEL: preferences.channel || 'Online',

  SUB_CHANNEL: preferences.subChannel || 'Web',

  BANNER: preferences.banner || 'OFF5',

  GIFT_CARD: preferences.giftCardMethod || 'GiftCard',

  SAKS_GIFT_CARD: preferences.saksGiftCardMethod || 'SAKSGiftCard',

  REWARD_POINTS: 'PAY_WITH_POINTS',

  ENTRY_MODE: preferences.entryMode || 'KEYED',

  STORED_MODE: preferences.storedMode || 'STORED',

  GIFTCARD_TYPE: preferences.gcTypeOnRequest || 'SAKS_EGC',

  CREDIT_CARD: preferences.ccTypeOnRequest || 'CreditCard',

  CARD_TYPE_MAP: preferences.cardTypeMapping,
  OMS_CARD_TYPE_MAP: preferences.OMSCardTypeMapping,
  BM_CARD_TYPE_MAP: preferences.BlueMartiniCardTypeMapping,

  PAYPAL: 'PayPal',

  transactionServiceEmail: preferences.serviceEmailDL,

  allowOrderIfAuthServiceDown: preferences.allowOrderIfAuthServiceDown,

  PAYMENT_CARD_TYPE: preferences.paymentCardTypeMapping,

  PAYMENT_CARD_TYPE_OMS: 'paymentCardTypeMappingOMS' in preferences ? preferences.paymentCardTypeMappingOMS : null,

  transactionMode: {
    Auth: preferences.authMode || 'AUTH_FRAUD',
    Reversal: preferences.authReversalMode || 'AUTH_FRAUD',
    Fraud: 'FRAUD_ONLY'
  },

  transactionType: {
    Auth: preferences.authType || 'Authorization',
    Reversal: preferences.reversalType || 'Reversal',
    Fraud: preferences.authType || 'Authorization'
  },
  transactionService: 'ipa.authorization.' + Site.getCurrent().ID.toLowerCase(),
  giftCardBalanceCheckService: 'ipa.gcbalancecheck.' + Site.getCurrent().ID.toLowerCase(),
  enableAuthLoggers: false //preferences.enableAuthLoggers || true
};

module.exports = Constants;
