'use strict';

var ipaService = new (require('*/cartridge/scripts/init/ipaGCBalanceCheckService'))();
var preferences = require('*/cartridge/config/preferences');

/**
 * Build GC balance check request
 *
 * @param {string} gcnumber - GC number
 * @param {string} gcpin - GC pin
 * @returns {Object} request
 */
function buildRequestJSON(gcnumber, gcpin) {
  var gcBalanceCheckRequest = {
    banner: preferences.hbcBanner,
    store_number: preferences.hbcStoreNumber,
    channel: preferences.hbcChannel,
    sub_channel: preferences.hbcSubChannel,
    card: {
      number: gcnumber,
      pin: gcpin,
      type: preferences.hbcGcTypeOnRequest
    }
  };
  return gcBalanceCheckRequest;
}

/**
 * Helper to call GC balance check service
 *
 * @param {string} gcnumber - GC number
 * @param {string} gcpin - GC pin
 * @returns {Object} service result
 */
function gcBalanceCheck(gcnumber, gcpin) {
  var reqPayLoad = {};
  reqPayLoad.apiKey = preferences.hbcAPIKey;
  reqPayLoad.apiId = preferences.hbcAPIID;
  var requestJSON = buildRequestJSON(gcnumber, gcpin);
  reqPayLoad.requestJSON = requestJSON;
  var result = ipaService.giftCardBalanceCheck(reqPayLoad);
  return result;
}

exports.gcBalanceCheck = gcBalanceCheck;
