'use strict';

function reverseAuth() {
  var Site = require('dw/system/Site');
  var Logger = require('dw/system/Logger');
  var OrderMgr = require('dw/order/OrderMgr');
  var Order = require('dw/order/Order');
  var Transaction = require('dw/system/Transaction');
  var ipaUtil = require('*/cartridge/scripts/util/ipaUtil');
  var Status = require('dw/system/Status');

  try {
    Logger.debug('Calling ReverseAuthorization for Cancelled Orders');

    var orderIterator = OrderMgr.queryOrders('status={0} AND custom.reversalForCancelOrder={1}', 'orderNo asc', Order.ORDER_STATUS_CANCELLED, false);
    if (!empty(orderIterator)) {
      while (orderIterator.hasNext()) {
        var order = orderIterator.next();
        var args = {};
        args.Order = order;
        args.remoteAddress = order.remoteHost;
        args.type = 'REGULAR';
        args.session = {}; // session is not available on the job run
        args.authType = 'Reversal';
        args.failAttempts = 1; // Fail attempts is 1 in case of job for the given execution
        args.customer = order.getCustomer();
        args.deviceFingerPrintID = ''; // We will not be able to fetch session in the job context. So the device fingerprint value would be empty
        var result = ipaUtil.authorizationReversal(args);
        if (!result.error) {
          Transaction.wrap(function () {
            order.custom.reversalForCancelOrder = true;
          });
        }
        Logger.debug('order ID - ' + order.orderNo);
        Logger.debug('Reverse Authorization Status : ' + result.error);
      }
    }
    return new Status(Status.OK, 'OK', 'successful');
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    Logger.debug('Reverse Authorization Error Stack Trace : ' + errorMsg);
  }
}

exports.ReverseAuth = reverseAuth;
