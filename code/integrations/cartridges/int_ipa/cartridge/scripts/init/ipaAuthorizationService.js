'use strict';

/* API Includes */
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
var ServiceHelper = require('*/cartridge/scripts/services/helpers/ServiceHelper');
var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');

var Logger = require('dw/system/Logger');

var IPAAuthorizationUtil = function () {
  /**
   * Helper method for sending Authorization requests
   *
   * @param {String} resource URL
   * @param {Object} data The JSON data to send in the body of the request
   *
   */
  this.Authorize = function (data) {
    var authService = LocalServiceRegistry.createService(ipaConstants.transactionService, {
      createRequest: function (service, params) {
        service.setRequestMethod('POST');
        service = ServiceHelper.addServiceHeaders(service);
        return params;
      },
      parseResponse: function (service, httpClient) {
        return JSON.parse(httpClient.text);
      },
      // eslint-disable-next-line no-unused-vars
      mockCall: function (svc, client) {
        var mockedReponse =
          '{"banner":"BAY","store_number":"1963","order_number":"00001073","transaction_type":"Authorization","payments":[{"card":{"type":"VISA","amount":"553.35","currency_code":"CAD","token_number":"4520054812674420","token_type":"TKNX"},"authorization":{"authorization_code":"393983","merchant_reference_number":"10069209","cvv_response_code":"TD:M:CVV Match ","avs_response_code":"TD:C:Street address and Postal/ZIP not verified. ","authorization_date":"2019-12-03 06:16:53.434707","authorization_expiration_date":"2019-12-09","response_code":"TD:1:Approved","authorization_terminal_number":"219.65.97.168","max_charge_limit":"","authorization_transaction_number":"0000107300000000"},"result_code":"1:Approved"}],"response_code":"1","response_message":"Approved"}';
        return {
          statusCode: 200,
          statusMessage: 'Success',
          text: mockedReponse
        };
      },
      filterLogMessage: function (logMsg) {
        try {
          if (logMsg.indexOf('https') === -1) {
            var obj = JSON.parse(logMsg);
            obj.payments.forEach(function (payment) {
              if (payment.Card && payment.Card.token_number) {
                payment.Card.token_number = '*************';
              }
              if (payment.Card && payment.Card.number) {
                payment.Card.number = '*************';
              }
              if (payment.Card && payment.Card.card_type) {
                payment.Card.card_type = '*************';
              }
              if (payment.Card && payment.Card.cvv) {
                payment.Card.cvv = '***';
              }
              if (payment.Card && payment.Card.pin) {
                payment.Card.pin = '***';
              }
              if (payment.Paypal && payment.Paypal.token) {
                payment.Paypal.token = '*************';
              }
              if (payment.bill_to && payment.bill_to.first_name) {
                payment.bill_to.first_name = '*************';
              }
              if (payment.bill_to && payment.bill_to.last_name) {
                payment.bill_to.last_name = '*************';
              }
              if (payment.bill_to && payment.bill_to.address_line_1) {
                payment.bill_to.address_line_1 = '*************';
              }
              if (payment.bill_to && payment.bill_to.city) {
                payment.bill_to.city = '*************';
              }
              if (payment.bill_to && payment.bill_to.state_province) {
                payment.bill_to.state_province = '*************';
              }
              if (payment.bill_to && payment.bill_to.postal_code) {
                payment.bill_to.postal_code = '*************';
              }
              if (payment.bill_to && payment.bill_to.phone_number) {
                payment.bill_to.phone_number = '*************';
              }
              if (payment.bill_to && payment.bill_to.email_address) {
                payment.bill_to.email_address = '*************';
              }
            });
            return JSON.stringify(obj);
          }
        } catch (e) {
          Logger.error('Error message' + e.message);
        }
        return logMsg;
      }
    });

    var result = authService.call(JSON.stringify(data));

    return result;
  };
  /**
   * Helper method for sending GC balance check requests
   *
   * @param {String} resource URL
   * @param {Object} data The JSON data to send in the body of the request
   *
   */
  this.giftCardBalanceCheck = function (data) {
    var authService = LocalServiceRegistry.createService(ipaConstants.giftCardBalanceCheckService, {
      createRequest: function (service, params) {
        service.setRequestMethod('POST');
        service = ServiceHelper.addServiceHeaders(service);
        return params;
      },
      parseResponse: function (service, httpClient) {
        return JSON.parse(httpClient.text);
      }
    });

    var result = authService.call(JSON.stringify(data));

    return result;
  };
};

module.exports = IPAAuthorizationUtil;
