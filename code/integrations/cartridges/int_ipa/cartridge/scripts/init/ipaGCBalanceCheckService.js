'use strict';

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var Logger = require('dw/system/Logger');
var preferences = require('*/cartridge/config/preferences');
var ServiceHelper = require('*/cartridge/scripts/services/helpers/ServiceHelper');

var GiftCardBalanceCheckService = function () {
  /**
   * Helper method for sending email requests
   *
   * @param {Object} data The JSON data to send in the body of the request
   *
   */
  this.giftCardBalanceCheck = function (data) {
    Logger.debug('SERVICE NAME - ' + preferences.giftCardBalanceCheckService);
    var giftCardBalanceCheckService = LocalServiceRegistry.createService(preferences.giftCardBalanceCheckService, {
      createRequest: function (service, params) {
        //eslint-disable-line
        service = ServiceHelper.addServiceHeaders(service); //eslint-disable-line
        service.setRequestMethod('POST');
        return JSON.stringify(data.requestJSON);
      },
      parseResponse: function (service, httpClient) {
        return JSON.parse(httpClient.text);
      },
      filterMessage: function (logMsg) {
        if (logMsg.indexOf('https') === -1) {
          let message = '';
          message = JSON.parse(logMsg);
          if (message.card.number) {
            message.card.number = '**********';
          }
          if (message.card.funds_available) {
            message.card.funds_available = '***********';
          }
          if (message.card.pin) {
            message.card.pin = '************';
          }
          return JSON.stringify(message);
        }
        return logMsg;
      },
      mockCall: function (service, httpClient) {
        return {
          statusCode: 200,
          statusMessage: 'Success',
          text:
            '{"store_number":"1963","transaction":[{"data":[{"response_code":"0","response_message":"RecoverableException:Code-3701 Description-RecoverableException : Error Making SOAP JNI Call: Axis2R"}]}]}'
        };
      }
    });

    var result = giftCardBalanceCheckService.call(JSON.stringify(data));
    if (!result.isOk() || !result.object) {
      var Result = require('dw/svc/Result');
      if (result.status === Result.SERVICE_UNAVAILABLE) {
        Logger.error('Gift card balance check failure: {0}', result.errorMessage);
      }
      return result;
    }
    return result.object;
  };
};
module.exports = GiftCardBalanceCheckService;
