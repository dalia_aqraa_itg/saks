'use strict';

var server = require('server');

var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');

/**
 * @constructor
 * @classdesc GiftCard Request class that represents the gift card request to IPA
 *
 * @param {json} args - arguments
 */
function get(args) {
  this.banner = ipaConstants.BANNER;
  this.channel = ipaConstants.CHANNEL;
  this.order_number = ''; // No order number is available for the giftcard check balance
  this.giftcard_type = ipaConstants.GIFTCARD_TYPE;
  this.giftcard_number = args.giftCardNumber;
  this.giftcard_pin = args.giftCardPin;
}

module.exports = {
  getRequestJSON: getRequestJSON
};
