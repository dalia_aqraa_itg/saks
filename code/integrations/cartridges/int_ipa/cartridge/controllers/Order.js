'use strict';

var server = require('server');
var Site = require('dw/system/Site');
var OrderMgr = require('dw/order/OrderMgr');
var Transaction = require('dw/system/Transaction');

server.extend(module.superModule);

server.get('Cancel', server.middleware.https, function (req, res, next) {
  var Resource = require('dw/web/Resource');
  var ipaUtil = require('*/cartridge/scripts/util/ipaUtil');

  var orderno = req.querystring.orderNo;
  var order = OrderMgr.getOrder(orderno);
  if (!empty(order)) {
    Transaction.wrap(function () {
      OrderMgr.failOrder(order);
    });
    var args = {};
    args.Order = order;
    args.remoteAddress = order.remoteHost;
    args.type = 'REGULAR';
    args.session = req.session;
    args.authType = 'Reversal';

    //Capture the no of failure attempts executed by the customer in the same session
    var attempts = req.session.privacyCache.get('failAttempts') || 0;
    attempts = attempts + 1;
    req.session.privacyCache.set('failAttempts', attempts);

    args.failAttempts = attempts;
    args.customer = req.currentCustomer.raw;
    args.deviceFingerPrintID = req.session.privacyCache.get('transactionUUID');
    var result = ipaUtil.authorizationReversal(args);
  }
  res.json({ success: true }); //Fix me to send any additional response paramters or render a page. This is unknown for now
  next();
});

module.exports = server.exports();
