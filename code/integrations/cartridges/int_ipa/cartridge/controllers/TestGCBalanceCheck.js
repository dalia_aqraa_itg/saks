/**
 * Description of the Controller and the logic it provides
 *
 * @module  controllers/TestGCBalanceCheck
 */

'use strict';

var server = require('server');
var ipaUtil = require('*/cartridge/scripts/util/ipaGCUtils');
var Logger = require('dw/system/Logger');
var URLUtils = require('dw/web/URLUtils');

// HINT: do not put all require statements at the top of the file
// unless you really need them for all functions

/**
 * Description of the function
 *
 * @return {String} The string 'myFunction'
 */

server.get('MyFunction', function (req, res, next) {
  myFunction();
  res.render('/home/homePage');
  next();
});

var myFunction = function () {
  var gcnumber = '6018990000164367';
  var gcpin = '4522';
  var ordernumber = '';
  var currency = 'USD';
  var resposneGC = ipaUtil.gcBalanceCheck(gcnumber, gcpin, ordernumber);
  Logger.info('IN CONTROLLER:\n{0}', JSON.stringify(resposneGC));
};

/* Exports of the controller */
///**
// * @see {@link module:controllers/TestGCBalanceCheck~myFunction} */
module.exports = server.exports();
