'use strict';

var OMSConstants = require('*/cartridge/scripts/config/OMSServiceConfig');
var Money = require('dw/value/Money');
var Resource = require('dw/web/Resource');
var Logger = require('dw/system/Logger');

/**
 * Return not null value only
 *
 * @param {string} attr - any attribute
 * @returns {string} attribute value
 */
function returnNotNull(attr) {
  return !!attr && attr !== 'null' ? attr : '';
}

/**
 * Function to return web friendly status
 *
 * @param {string} status - OMS status
 * @returns {string} - Web friendly status
 */
function getWebStatus(status) {
  var orderStatusPrefix = 'ORDER_STATUS_';
  let statusMapping = OMSConstants.OMS_ORDER_DETAILS_STATUS;
  var status = dw.web.Resource.msg(statusMapping[status] ? orderStatusPrefix + statusMapping[status] : orderStatusPrefix + 'IN_PROGRESS', 'order', status);
  return status;
}
/**
 * Get Locale Information
 *
 * @param {Object} orderLine - Order line object from OMS
 * @returns {Object} locale object
 */
function getItemLocaleInfo(orderLine) {
  var locales = [];
  if (orderLine.ItemDetails && orderLine.ItemDetails.ItemLocaleList && orderLine.ItemDetails.ItemLocaleList.ItemLocale) {
    orderLine.ItemDetails.ItemLocaleList.ItemLocale.forEach(function (itemLocale) {
      var locale = {};
      locale.language = itemLocale.Language;
      locale.Country = itemLocale.Country;
      locale.shortDescription =
        itemLocale.PrimaryInformation && itemLocale.PrimaryInformation.ShortDescription ? itemLocale.PrimaryInformation.ShortDescription : '';
      locale.description = itemLocale.PrimaryInformation && itemLocale.PrimaryInformation.Description ? itemLocale.PrimaryInformation.Description : '';
      locales.push(locale);
    });
  }
  return locales;
}

/**
 * constructs shipping address
 *
 * @param {Object} personBillTo - Billing information OMS object
 * @returns {Object} billing address object model for storefront
 */
function getBillingAddress(personBillTo) {
  var billingAddress = {};
  billingAddress.firstName = returnNotNull(personBillTo.FirstName);
  billingAddress.lastName = returnNotNull(personBillTo.LastName);
  billingAddress.addressLine1 = returnNotNull(personBillTo.AddressLine1);
  billingAddress.addressLine2 = returnNotNull(personBillTo.AddressLine2);
  billingAddress.city = returnNotNull(personBillTo.City);
  billingAddress.state = returnNotNull(personBillTo.State);
  billingAddress.zipCode = returnNotNull(personBillTo.ZipCode);
  billingAddress.country = returnNotNull(personBillTo.Country);
  billingAddress.phoneNo = returnNotNull(personBillTo.DayPhone);
  billingAddress.email = returnNotNull(personBillTo.EMailID);
  return billingAddress;
}

/**
 * Get payment details of order
 *
 * @param {Object} order - OMS Order response object
 * @param {string} currency - Order currency
 * @returns {Object} - payment model object
 */
function getPaymentDetails(order, currency) {
  var paymentDetails = {};
  var paymentMethods = [];
  if (order.PaymentMethods && order.PaymentMethods.PaymentMethod) {
    order.PaymentMethods.PaymentMethod.forEach(function (method) {
      var paymentMethod = {};
      paymentMethod.paymentType = returnNotNull(method.PaymentType);
      if (method.DisplaySvcNo) {
        paymentMethod.giftCardNumber = returnNotNull(method.DisplaySvcNo);
      }
      if (method.CreditCardType) {
        paymentMethod.creditCardType = Object.prototype.hasOwnProperty.call(OMSConstants.OMS_CREDIT_CARD_TYPE_MAPPING, method.CreditCardType)
          ? OMSConstants.OMS_CREDIT_CARD_TYPE_MAPPING[method.CreditCardType]
          : method.CreditCardType;
      }
      if (method.PaymentReference2 === 'KLARNA') {
          paymentMethod.paymentType = 'KLARNA';
      }
      if (method.DisplayCreditCardNo && method.CreditCardType != 'GIFT_CARD') {
        paymentMethod.creditCardNo = returnNotNull(method.DisplayCreditCardNo);
      }
      if (method.CreditCardName) {
        paymentMethod.creditCardName = returnNotNull(method.CreditCardName);
      }
      let totalAuthorized = method.TotalAuthorized || '0';
      let totalCharged = method.TotalCharged || '0';
      let maxChargeLimit = method.MaxChargeLimit || '0';
      paymentMethod.totalAuthorized = new Money(Number(totalAuthorized), currency).toFormattedString();
      paymentMethod.totalCharged = new Money(Number(totalCharged), currency).toFormattedString();
      paymentMethod.totalPayment = new Money(Number(maxChargeLimit), currency).toFormattedString();
      Logger.debug('SFSX-3637 chargeSequence: {0}', method.ChargeSequence);
      paymentMethod.chargeSequence = method.ChargeSequence;
      paymentMethod.billingAddress = method.PersonInfoBillTo ? getBillingAddress(method.PersonInfoBillTo) : getBillingAddress(order.PersonInfoBillTo);
      paymentMethods.push(paymentMethod);
    });
  }
  paymentDetails.paymentMethods = paymentMethods;
  return paymentDetails;
}

/**
 * constructs shipping address
 *
 * @param {Object} personShipTo - OMS shipping address object
 * @returns {Object} Shipping address object for storefront
 */
function getShippingAddress(personShipTo) {
  var shippingAddress = {};
  if (personShipTo) {
    shippingAddress.firstName = returnNotNull(personShipTo.FirstName);
    shippingAddress.lastName = returnNotNull(personShipTo.LastName);
    shippingAddress.addressLine1 = returnNotNull(personShipTo.AddressLine1);
    shippingAddress.addressLine2 = returnNotNull(personShipTo.AddressLine2);
    shippingAddress.company = returnNotNull(personShipTo.Company);
    shippingAddress.city = returnNotNull(personShipTo.City);
    shippingAddress.state = returnNotNull(personShipTo.State);
    shippingAddress.zipCode = returnNotNull(personShipTo.ZipCode);
    shippingAddress.country = returnNotNull(personShipTo.Country);
    shippingAddress.phoneNo = returnNotNull(personShipTo.DayPhone);
    shippingAddress.email = returnNotNull(personShipTo.EMailID);
  }
  return shippingAddress;
}

/**
 *
 * Get the shipment data based on the shipped items in the order
 *
 * @param {Object} order - OMS service order object
 * @param {string} customerId - customer ID
 * @returns {Object} - shipment order object for storefront
 */
function getOrderShipments(order, customerId) {
  var shipments = [];
  if (order.Shipments && order.Shipments.Shipment) {
    order.Shipments.Shipment.forEach(function (orderShipment) {
      var shipment = {};
      shipment.shipmentNo = orderShipment.ShipmentNo;
      if (orderShipment.ShipNode && orderShipment.ShipNode.ShipNodePersonInfo) {
        shipment.shippingAddress = getShippingAddress(orderShipment.ShipNode.ShipNodePersonInfo);
      }
      if (!shipment.shippingAddress) {
        shipment.shippingAddress = getShippingAddress(order.PersonInfoShipTo);
      }
      var shipmentLineItems = [];
      if (orderShipment.Containers && orderShipment.Containers.Container) {
        orderShipment.Containers.Container.forEach(function (container) {
          if (container.TrackingNo) {
            var shipmentLineItem = {};
            shipmentLineItem.itemID =
              container.ContainerDetails && Number(container.ContainerDetails.TotalNumberOfRecords) > 0 && container.ContainerDetails.ContainerDetail[0]
                ? container.ContainerDetails.ContainerDetail[0].ItemID
                : '';
            var itemId = escape(shipmentLineItem.itemID);
            shipmentLineItem.carrierServiceCode = returnNotNull(container.CarrierServiceCode);
            shipmentLineItem.SCAC = returnNotNull(container.SCAC);
            shipmentLineItem.trackingNumber = container.TrackingNo;
            shipmentLineItem.trackingURL =
              OMSConstants.ORDER_TRACKING_URL +
              container.SCAC +
              '?' +
              'tracking_numbers=' +
              container.TrackingNo +
              '&product_id=' +
              itemId +
              '&ozip=&dzip=' +
              shipment.shippingAddress.zipCode +
              '&customer_id=' +
              customerId;
            shipmentLineItems.push(shipmentLineItem);
          }
        });
      }
      shipment.lineItems = shipmentLineItems;
      shipments.push(shipment);
    });
  }
  return shipments;
}

/**
 * Get EDD (estimated delivery date) of line item
 *
 * @param {Object} orderLine - OMS object for line item
 * @returns {string} delivery date
 */
function getEstimatedDeliveryDate(orderLine) {
  var deliveryDate;
  if (orderLine.OrderDates && orderLine.OrderDates.OrderDate) {
    orderLine.OrderDates.OrderDate.forEach(function (orderDate) {
      if (orderDate.DateTypeId && orderDate.DateTypeId === OMSConstants.OMS_SHIPMENT_ESTIMATED_DELIVERY_DATE_TYPE && orderDate.ActualDate) {
        let orderDt = orderDate.ActualDate.split('T')[0];
        let DateArray = orderDt.split('-');
        orderDt = new Date(DateArray[0] + '/' + DateArray[1] + '/' + DateArray[2]);
        deliveryDate = getDeliveryDate(orderDt);
      }
    });
  }

  if (deliveryDate) {
    return Resource.msg('label.estimated.delivery', 'checkout', null) + ' ' + deliveryDate;
  }
  return '';
}

function getEstimatedDeliveryDateUnFormatted(orderLine) {
  var deliveryDate;
  if (orderLine.OrderDates && orderLine.OrderDates.OrderDate) {
    orderLine.OrderDates.OrderDate.forEach(function (orderDate) {
      if (orderDate.DateTypeId && orderDate.DateTypeId === OMSConstants.OMS_SHIPMENT_ESTIMATED_DELIVERY_DATE_TYPE && orderDate.ActualDate) {
        let orderDt = orderDate.ActualDate.split('T')[0];
        let DateArray = orderDt.split('-');
        deliveryDate = new Date(DateArray[0] + '/' + DateArray[1] + '/' + DateArray[2]);
      }
    });
  }
  return deliveryDate;
}

function getDeliveryDate(orderDate) {
  var dateStr = orderDate.toLocaleDateString();
  var month = dateStr.split(' ')[0];
  var monthNew = dw.web.Resource.msg(month, 'order', month);

  var Locale = require('dw/util/Locale');
  var currentLocale = Locale.getLocale(request.locale);

  if (currentLocale.ID === 'en_CA') {
    return dateStr.replace(month, monthNew);
  } else if (currentLocale.ID === 'fr_CA') {
    var monthDay = dateStr.split(',')[0];
    var year = dateStr.split(',')[1];
    var day = monthDay.substring(month.length + 1);
    var finalDateString = day + ' ' + monthNew + ' ' + year;
    return finalDateString;
  }
  return dateStr.replace(month, monthNew);
}

/**
 * Lookup shipping method configured in BM and return the shipping method name
 * @param {string} carrierServiceCode - CSC for order
 * @param {string} scac - SCAC for order
 * @returns {string} Shipping method name
 */
function lookupShippingMethodName(carrierServiceCode, scac, srtoken, orderLineShippingMethodName) {
  let CSCMapping = OMSConstants.OMS_CARRIER_CODE_MAPPING;
  var shippingMethodName = returnNotNull(carrierServiceCode);
  let CSC = returnNotNull(carrierServiceCode);
  if (CSC && !empty(CSC)) {
    var cscCode = CSC.replace(/\s/g, '').toLowerCase();
    if (cscCode && CSCMapping[cscCode] && !empty(CSCMapping[cscCode])) {
      CSC = CSCMapping[cscCode];
    }
  }
  let SCAC = returnNotNull(scac);
  if (!!CSC & !!SCAC) {
    var ShippingMgr = require('dw/order/ShippingMgr');
    var methods = ShippingMgr.getAllShippingMethods();
    for (var i = 0; i < methods.length; i++) {
      if (
        'omsCarrierServiceCode' in methods[i].custom &&
        methods[i].custom.omsCarrierServiceCode &&
        'omsSCAC' in methods[i].custom &&
        methods[i].custom.omsSCAC &&
        CSC === methods[i].custom.omsCarrierServiceCode &&
        SCAC === methods[i].custom.omsSCAC &&
        methods[i].displayName
      ) {
        if (methods[i].ID.indexOf('shoprunner') > -1 && srtoken.isToken && !empty(srtoken.shippingMethod) && CSC == 'TwoDay') {
          shippingMethodName = srtoken.shippingMethod;
          break;
        } else if (methods[i].ID.indexOf('shoprunner') === -1) {
          shippingMethodName = methods[i].displayName;
          // SN-15 Next Day | Order History & Order Status Ship Method
          if (typeof orderLineShippingMethodName !== 'undefined' && orderLineShippingMethodName !== shippingMethodName &&
             'omsShippingMehtodUpgradeName' in methods[i].custom && !empty(methods[i].custom.omsShippingMehtodUpgradeName)) {
            shippingMethodName = methods[i].custom.omsShippingMehtodUpgradeName;
          }
          break;
        }
      }
    }
  }
  return shippingMethodName;
}

/**
 * get shipping method name for an order line item
 * @param {Object} orderLine - Line item Object from OMS
 * @param {Object} shipments - Shipments line item object
 * @returns {string} Shipping method name
 */
function getLineItemShippingMethod(orderLine, shipments, srtoken) {
  var shippingMethodName = orderLine.CarrierServiceCode ? lookupShippingMethodName(orderLine.CarrierServiceCode, orderLine.SCAC, srtoken) : '';
  if (shipments) {
    var itemId = orderLine.Item && orderLine.Item.ItemID ? orderLine.Item.ItemID : '';
    shipments.forEach(function (shipment) {
      if (shipment.lineItems) {
        shipment.lineItems.forEach(function (item) {
          if (itemId === item.itemID) {
            shippingMethodName = lookupShippingMethodName(item.carrierServiceCode, item.SCAC, srtoken, shippingMethodName);
          }
        });
      }
    });
  }
  return shippingMethodName;
}

/**
 * get shipping method name for an order line item
 * @param {Object} ipaShippingMethod - IPA Shipping method
 * @returns {string} Shipping method name
 */
function getLineItemAPIShippingMethod(ipaShippingMethod) {
  var shippingMethodName;
  session.custom.srtoken = ' ';
  var ShippingMgr = require('dw/order/ShippingMgr');
  var methods = ShippingMgr.getAllShippingMethods();
  for (var i = 0; i < methods.length; i++) {
    if (ipaShippingMethod && methods[i].custom.ipaShippingMethod && methods[i].custom.ipaShippingMethod == ipaShippingMethod && methods[i].displayName) {
      shippingMethodName = methods[i].displayName;
    }
  }
  delete session.custom.srtoken;
  return shippingMethodName;
}

/**
 * Calculate the total of fees and surcharges
 *
 * @param {Object} order - Order object
 * @param {string} currency - Order currency
 * @returns {Object} - fees and taxes object
 */
function getFeesAndTaxes(order, currency) {
  var feesAndCharges = {};
  var discountTotal = 0;
  var ecoFeeTaxTotal = 0;
  var ecoFeeChargeTotal = 0;
  var gstHstTotal = 0;
  var qstTotal = 0;
  var generalSalesAndUseTaxTotal = 0;
  var pstTotal = 0;
  var rstTotal = 0;

  if (order.OrderLines && order.OrderLines.OrderLine) {
    var orderLines = order.OrderLines.OrderLine;
    orderLines.forEach(function (orderLine) {
      if (orderLine.Status === OMSConstants.OMS_LINE_ITEM_STATUS_CANCELLED) {
        if (orderLine.Extn && orderLine.Extn.HbcLinetaxBreakupList && orderLine.Extn.HbcLinetaxBreakupList.HbcLinetaxBreakup) {
          var hbcLinetaxBreakup = orderLine.Extn.HbcLinetaxBreakupList.HbcLinetaxBreakup;
          hbcLinetaxBreakup.forEach(function (lineTax) {
            var lineTaxFee = lineTax.Tax ? Number(lineTax.Tax) : 0;
            if (lineTax.TaxName && lineTax.TaxName === OMSConstants.OMS_ORDERLINE_TAX_NAME_GST_HST) {
              gstHstTotal += lineTaxFee;
            } else if (
              lineTax.TaxName &&
              (lineTax.TaxName === OMSConstants.OMS_ORDERLINE_TAX_NAME_QST || lineTax.TaxName === OMSConstants.OMS_ORDERLINE_TAX_NAME_QST_VAT)
            ) {
              qstTotal += lineTaxFee;
            } else if (lineTax.TaxName && lineTax.TaxName === OMSConstants.OMS_ORDERLINE_TAX_NAME_GENERAL_SALES_AND_USE_TAX) {
              generalSalesAndUseTaxTotal += lineTaxFee;
            } else if (lineTax.TaxName && lineTax.TaxName === OMSConstants.OMS_ORDERLINE_TAX_NAME_PST) {
              pstTotal += lineTaxFee;
            } else if (lineTax.TaxName && lineTax.TaxName === OMSConstants.OMS_ORDERLINE_TAX_NAME_RST) {
              rstTotal += lineTaxFee;
            }
          });
        }

        if (orderLine.Extn && orderLine.Extn.HbcOrderLineChargesList && orderLine.Extn.HbcOrderLineChargesList.HbcOrderLineCharges) {
          var hbcLineCharges = orderLine.Extn.HbcOrderLineChargesList.HbcOrderLineCharges;
          hbcLineCharges.forEach(function (lineCharge) {
            var lineChargeAmount = lineCharge.ChargeAmount ? Number(lineCharge.ChargeAmount) : 0;
            if (lineCharge.ChargeCategory && lineCharge.ChargeCategory === OMSConstants.OMS_ORDERLINE_TAX_CATEGORY_ECO_FEE) {
              ecoFeeChargeTotal += lineChargeAmount;
            }
            if (lineCharge.ChargeCategory && lineCharge.ChargeCategory === OMSConstants.OMS_ORDERLINE_CHARGE_CATEGORY_DISCOUNT) {
              discountTotal += lineChargeAmount;
            }
          });
        }
      } else {
        if (orderLine.LineTaxes.LineTax) {
          orderLine.LineTaxes.LineTax.forEach(function (lineTax) {
            var lineTaxFee = lineTax.Tax ? Number(lineTax.Tax) : 0;
            if (lineTax.TaxName && lineTax.TaxName === OMSConstants.OMS_ORDERLINE_TAX_NAME_GST_HST) {
              gstHstTotal += lineTaxFee;
            } else if (
              lineTax.TaxName &&
              (lineTax.TaxName === OMSConstants.OMS_ORDERLINE_TAX_NAME_QST || lineTax.TaxName === OMSConstants.OMS_ORDERLINE_TAX_NAME_QST_VAT)
            ) {
              qstTotal += lineTaxFee;
            } else if (lineTax.TaxName && lineTax.TaxName === OMSConstants.OMS_ORDERLINE_TAX_NAME_GENERAL_SALES_AND_USE_TAX) {
              generalSalesAndUseTaxTotal += lineTaxFee;
            } else if (lineTax.TaxName && lineTax.TaxName === OMSConstants.OMS_ORDERLINE_TAX_NAME_PST) {
              pstTotal += lineTaxFee;
            } else if (lineTax.TaxName && lineTax.TaxName === OMSConstants.OMS_ORDERLINE_TAX_NAME_RST) {
              rstTotal += lineTaxFee;
            }
          });
        }

        if (orderLine.LineCharges.LineCharge) {
          orderLine.LineCharges.LineCharge.forEach(function (lineCharge) {
            var lineChargeAmount = lineCharge.ChargeAmount ? Number(lineCharge.ChargeAmount) : 0;
            if (lineCharge.ChargeCategory && lineCharge.ChargeCategory === OMSConstants.OMS_ORDERLINE_TAX_CATEGORY_ECO_FEE) {
              ecoFeeChargeTotal += lineChargeAmount;
            }
            if (lineCharge.ChargeCategory && lineCharge.ChargeCategory === OMSConstants.OMS_ORDERLINE_CHARGE_CATEGORY_DISCOUNT) {
              discountTotal += lineChargeAmount;
            }
          });
        }
      }
    });
  }

  feesAndCharges.discountTotal = discountTotal > 0 ? new Money(discountTotal, currency).toFormattedString() : '';
  feesAndCharges.ecoFeeTaxTotal = ecoFeeTaxTotal > 0 ? new Money(ecoFeeTaxTotal, currency).toFormattedString() : '';
  feesAndCharges.ecoFeeChargeTotal = ecoFeeChargeTotal > 0 ? new Money(ecoFeeChargeTotal, currency).toFormattedString() : '';
  feesAndCharges.GSTHST = new Money(gstHstTotal, currency).toFormattedString();
  feesAndCharges.QST = new Money(qstTotal, currency).toFormattedString();
  feesAndCharges.QSTPSTRST = new Money(qstTotal + pstTotal + rstTotal, currency).toFormattedString();
  feesAndCharges.generalSalesAndUseTax = new Money(generalSalesAndUseTaxTotal, currency).toFormattedString();
  feesAndCharges.PST = new Money(pstTotal, currency).toFormattedString();
  feesAndCharges.RST = new Money(rstTotal, currency).toFormattedString();

  return feesAndCharges;
}

/**
 * get charges and taxes at line item level
 *
 * @param {Object} orderLine - Order line OMS object
 * @returns {Object} Line item taxes and charges
 */
function getLineItemChargesAndTaxes(orderLine) {
  var lineItemCharges = [];
  var lineItemTaxes = [];
  if (orderLine.Status === OMSConstants.OMS_LINE_ITEM_STATUS_CANCELLED) {
    if (orderLine.Extn && orderLine.Extn.HbcOrderLineChargesList && orderLine.Extn.HbcOrderLineChargesList.HbcOrderLineCharges) {
      var hbcOrderLineCharges = orderLine.Extn.HbcOrderLineChargesList.HbcOrderLineCharges;
      hbcOrderLineCharges.forEach(function (lineCharge) {
        var lineItemCharge = {};
        lineItemCharge.chargeName = lineCharge.ChargeName ? lineCharge.ChargeName : '';
        lineItemCharge.category = lineCharge.ChargeCategory ? lineCharge.ChargeCategory : '';
        lineItemCharge.taxAmount = lineCharge.ChargeAmount ? Number(lineCharge.ChargeAmount) : 0;
        lineItemCharges.push(lineItemCharge);
      });
    }
    if (orderLine.Extn && orderLine.Extn.HbcLinetaxBreakupList && orderLine.Extn.HbcLinetaxBreakupList.HbcLinetaxBreakup) {
      var hbcLinetaxBreakup = orderLine.Extn.HbcLinetaxBreakupList.HbcLinetaxBreakup;
      hbcLinetaxBreakup.forEach(function (lineTax) {
        var lineItemTax = {};
        lineItemTax.taxName = lineTax.TaxName ? lineTax.TaxName : '';
        lineItemTax.category = lineTax.ChargeCategory ? lineTax.ChargeCategory : '';
        lineItemTax.taxAmount = lineTax.Tax ? Number(lineTax.Tax) : 0;
        lineItemTaxes.push(lineItemTax);
      });
    }
  } else {
    if (orderLine.LineCharges.LineCharge) {
      orderLine.LineCharges.LineCharge.forEach(function (lineCharge) {
        var lineItemCharge = {};
        lineItemCharge.chargeName = lineCharge.ChargeName ? lineCharge.ChargeName : '';
        lineItemCharge.category = lineCharge.ChargeCategory ? lineCharge.ChargeCategory : '';
        lineItemCharge.taxAmount = lineCharge.ChargeAmount ? Number(lineCharge.ChargeAmount) : 0;
        lineItemCharges.push(lineItemCharge);
      });
    }
    if (orderLine.LineTaxes.LineTax) {
      orderLine.LineTaxes.LineTax.forEach(function (lineTax) {
        var lineItemTax = {};
        lineItemTax.taxName = lineTax.TaxName ? lineTax.TaxName : '';
        lineItemTax.category = lineTax.ChargeCategory ? lineTax.ChargeCategory : '';
        lineItemTax.taxAmount = lineTax.Tax ? Number(lineTax.Tax) : 0;
        lineItemTaxes.push(lineItemTax);
      });
    }
  }
  return {
    lineItemCharges: lineItemCharges,
    lineItemTaxes: lineItemTaxes
  };
}

/**
 * Calculate total shipping charges
 *
 * @param {Object} order - Order object
 * @param {string} currency - Order currency
 * @returns {dw.value.Money} - shipping total of order
 */
function getShippingTotal(order, currency) {
  var shippingTotal = order.OverallTotals && order.OverallTotals.GrandShippingTotal ? Number(order.OverallTotals.GrandShippingTotal) : 0;
  if (order.OrderLines && order.OrderLines.OrderLine) {
    var orderLines = order.OrderLines.OrderLine;
    orderLines.forEach(function (orderLine) {
      if (orderLine.Status === OMSConstants.OMS_LINE_ITEM_STATUS_CANCELLED) {
        if (orderLine.Extn && orderLine.Extn.HbcOrderLineChargesList && orderLine.Extn.HbcOrderLineChargesList.HbcOrderLineCharges) {
          var hbcOrderLineCharges = orderLine.Extn.HbcOrderLineChargesList.HbcOrderLineCharges;
          hbcOrderLineCharges.forEach(function (charge) {
            if (charge.ChargeCategory === OMSConstants.OMS_ORDERLINE_CHARGE_CATEGORY_SHIPPING_CHARGE) {
              shippingTotal += Number(charge.ChargeAmount);
            }
          });
        }
      }
    });
  }
  return shippingTotal > 0 ? new Money(shippingTotal, currency).toFormattedString() : Resource.msg('info.cart.free.shipping', 'cart', null);
}

/**
 * Contains logic to calculate original items total
 *
 * @param {Object} order - OMS Service order object
 * @returns  {Object} line items total object
 */
function getOriginalItemsTotal(order) {
  var lineItemSubTotal = 0;

  var subTotalWithoutTaxes = order.OverallTotals && order.OverallTotals.SubtotalWithoutTaxes ? Number(order.OverallTotals.SubtotalWithoutTaxes) : 0;
  var grandShippingTotal = order.OverallTotals && order.OverallTotals.GrandShippingTotal ? Number(order.OverallTotals.GrandShippingTotal) : 0;
  var cancelledItems = false;
  var originalTotalAmount = order.OriginalTotalAmount ? Number(order.OriginalTotalAmount) : 0;
  var grandTax = order.OverallTotals.GrandTax ? Number(order.OverallTotals.GrandTax) : 0;
  var hbcLineTotalTax = 0;
  var orderLineTotalShippingCharges = 0;

  if (order.OrderLines && order.OrderLines.OrderLine) {
    order.OrderLines.OrderLine.forEach(function (orderLine) {
      var finalQuantity = Number(orderLine.OriginalOrderedQty) - Number(orderLine.SplitQty);
      lineItemSubTotal += (orderLine.LinePriceInfo && orderLine.LinePriceInfo.UnitPrice ? Number(orderLine.LinePriceInfo.UnitPrice) : 0) * finalQuantity;

      if (orderLine.Status === OMSConstants.OMS_LINE_ITEM_STATUS_CANCELLED) {
        cancelledItems = true;
        if (orderLine.Extn && orderLine.Extn.HbcLinetaxBreakupList && orderLine.Extn.HbcLinetaxBreakupList.HbcLinetaxBreakup) {
          orderLine.Extn.HbcLinetaxBreakupList.HbcLinetaxBreakup.forEach(function (hbcLineTaxBreakup) {
            hbcLineTotalTax += Number(hbcLineTaxBreakup.Tax);
          });
        }
        if (orderLine.LineCharges && orderLine.LineCharges.LineCharge) {
          orderLine.LineCharges.LineCharge.forEach(function (linecharge) {
            if (linecharge.ChargeCategory === OMSConstants.OMS_ORDERLINE_CHARGE_CATEGORY_SHIPPING_CHARGE) {
              orderLineTotalShippingCharges += Number(linecharge.ChargeAmount);
            }
          });
        }
      }
    });
  }
  if (!cancelledItems) {
    return {
      lineItemSubTotal: lineItemSubTotal,
      originalOrderItemsTotal: subTotalWithoutTaxes - grandShippingTotal,
      originalOrderTaxTotal: grandTax
    };
  }
  return {
    lineItemSubTotal: lineItemSubTotal,
    originalOrderItemsTotal: originalTotalAmount - grandShippingTotal - grandTax - hbcLineTotalTax - orderLineTotalShippingCharges,
    originalOrderTaxTotal: grandTax + hbcLineTotalTax
  };
}
/**
 * Get Order line items ( as per user selection)
 *
 * @param {Object} order - OMS Order object
 * @param {string} currency - OMS Order currency
 * @param {Object} trackingInformation - Tracking Information Object
 * @param {Object} shipments - Shipment object
 * @returns {Object} - product line item model
 */
function getOrderLines(order, currency, trackingInformation, shipments) {
  var OrderMgr = require('dw/order/OrderMgr');
  var OrderObjct;
  let lineItemsObject = {};
  var orderLineItems = [];
  let returnable = false;
  var orderStatus = order.Status ? order.Status.trim().toLowerCase().replace(/\s/g, '') : '';
  var srtoken = {};
  if (!empty(order.OrderNo)) {
    try {
      OrderObjct = OrderMgr.getOrder(order.OrderNo);
      if (OrderObjct) {
        /*Added validation while fixing SFSX-2574 - https://hbcdigital.atlassian.net/browse/SFSX-2574*/
        srtoken.isToken = 'sr_token' in OrderObjct.custom && OrderObjct.custom.sr_token ? true : false;
        srtoken.shippingMethod =
          !empty(OrderObjct.defaultShipment) && !empty(OrderObjct.defaultShipment.shippingMethod) ? OrderObjct.defaultShipment.shippingMethod.displayName : '';
      }
    } catch (e) {
      var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Statck:' + e.stack;
    }
  }
  if (order.OrderLines && order.OrderLines.OrderLine) {
    var ProductMgr = require('dw/catalog/ProductMgr');
    var URLUtils = require('dw/web/URLUtils');
    var StringUtils = require('dw/util/StringUtils');
    var Calendar = require('dw/util/Calendar');
    order.OrderLines.OrderLine.forEach(function (orderLine) {
      var lineItem = {};
      let prod;
      let ipaShippingmethod;
      lineItem.itemId = orderLine.Item && orderLine.Item.ItemID ? orderLine.Item.ItemID : '';
      if (lineItem.itemId) {
        prod = ProductMgr.getProduct(lineItem.itemId);
        if (trackingInformation.get(lineItem.itemId)) {
          lineItem.trackingURL = trackingInformation.get(lineItem.itemId);
        }
        if (!empty(prod)) {
          lineItem.pdpURL = URLUtils.https('Product-Show', 'pid', lineItem.itemId).toString();
        }
      }
      if (lineItem.itemId && OrderObjct != null && !empty(OrderObjct)) {
        var apiLineItems = OrderObjct.getProductLineItems(lineItem.itemId);
        if (apiLineItems && !empty(apiLineItems) && apiLineItems.length > 0) {
          for (var i = 0; i < apiLineItems.length; i++) {
            var apiLineItem = apiLineItems[i];
            if ('shippingMethod' in apiLineItem.custom && apiLineItem.custom.shippingMethod && !empty(apiLineItem.custom.shippingMethod)) {
              ipaShippingmethod = apiLineItem.custom.shippingMethod;
            }
          }
        }
      }

      lineItem.name = orderLine.Item && orderLine.Item.CustomerItemDesc ? orderLine.Item.CustomerItemDesc : '';
      lineItem.shortDescription = orderLine.Item && orderLine.Item.ItemShortDesc ? orderLine.Item.ItemShortDesc : '';
      lineItem.description = orderLine.Item && orderLine.Item.ItemDesc ? orderLine.Item.ItemDesc : '';
      lineItem.localeInfo = getItemLocaleInfo(orderLine);
      if (!isEnglishLocale() && prod && prod.variant) {
        var variantAttributes = getVariantAttributes(prod);
        if (variantAttributes.color || variantAttributes.size) {
          lineItem.color = variantAttributes.color ? variantAttributes.color : '';
          lineItem.size = variantAttributes.size ? variantAttributes.size : '';
        } else {
          lineItem.color = orderLine.Extn && orderLine.Extn.ExtnColor ? orderLine.Extn.ExtnColor : '';
          lineItem.size = orderLine.Extn && orderLine.Extn.ExtnSize ? orderLine.Extn.ExtnSize : '';
        }
      } else {
        lineItem.color = orderLine.Extn && orderLine.Extn.ExtnColor ? orderLine.Extn.ExtnColor : '';
        lineItem.size = orderLine.Extn && orderLine.Extn.ExtnSize ? orderLine.Extn.ExtnSize : '';
      }
      lineItem.returnable = orderLine.Extn && orderLine.Extn.ExtnIsReturnable && orderLine.Extn.ExtnIsReturnable === 'Y';
      lineItem.signatureRequired = orderLine.Extn && orderLine.Extn.ExtnSignatureRequired && orderLine.Extn.ExtnSignatureRequired === 'Y';
      if (orderLine.Extn && orderLine.Extn.ExtnEstimatedShipDate && !empty(orderLine.Extn && orderLine.Extn.ExtnEstimatedShipDate)) {
        /*Changed for SFSX-2574 - https://hbcdigital.atlassian.net/browse/SFSX-2574
                Invalid variable name : ExtnFirstEstimatedShipDate used instead of : ExtnEstimatedShipDate*/
        var preOrderItemEstimatedate = orderLine.Extn.ExtnEstimatedShipDate;

        let DateArray = preOrderItemEstimatedate.split('-');
        orderDt = new Date(DateArray[0] + '/' + DateArray[1] + '/' + DateArray[2]);
        var preOrderDate = StringUtils.formatCalendar(new Calendar(orderDt), 'MM/dd/yyyy');
        lineItem.PreOrderItemEstimatedate = preOrderDate;
      }

      lineItem.quantity = returnNotNull(getOrderLineQuantity(orderLine));
      if (orderLine.ItemDetails.PrimaryInformation.ImageLocation.indexOf('http://') == 0) {
        orderLine.ItemDetails.PrimaryInformation.ImageLocation = orderLine.ItemDetails.PrimaryInformation.ImageLocation.replace('http://', 'https://');
      }
      if (
        orderLine.ItemDetails &&
        orderLine.ItemDetails.PrimaryInformation &&
        orderLine.ItemDetails.PrimaryInformation.ImageLocation &&
        orderLine.ItemDetails.PrimaryInformation.ImageID
      ) {
        lineItem.imageURL = orderLine.ItemDetails.PrimaryInformation.ImageLocation + orderLine.ItemDetails.PrimaryInformation.ImageID;
      }
      lineItem.brandName =
        orderLine.ItemDetails && orderLine.ItemDetails.PrimaryInformation && orderLine.ItemDetails.PrimaryInformation.ManufacturerName
          ? orderLine.ItemDetails.PrimaryInformation.ManufacturerName
          : '';
      if (prod != null) {
        lineItem.name = prod.name || lineItem.name;
        lineItem.reviewable = 'reviewable' in prod.custom && prod.custom.reviewable;
      }
      lineItem.masterProductId = prod && prod.variant && prod.masterProduct ? prod.masterProduct.ID : lineItem.itemId;
      lineItem.unitPrice = new Money(
        orderLine.LinePriceInfo && orderLine.LinePriceInfo.UnitPrice ? Number(orderLine.LinePriceInfo.UnitPrice) : 0,
        currency
      ).toFormattedString();
      lineItem.giftFlag = orderLine.GiftFlag ? orderLine.GiftFlag : 'N';
      lineItem.giftWrapEligible = orderLine.GiftWrap ? orderLine.GiftWrap : 'N';
      var giftWrap;
      if (orderLine.Instructions && orderLine.Instructions.Instruction) {
        var giftMessages = [];
        var giftRec = [];
        orderLine.Instructions.Instruction.forEach(function (instruction) {
          var instructionType = instruction.InstructionType;
          if (instructionType.indexOf('GIFT_MSG') > -1) {
            giftMessages.push(instruction.InstructionText ? instruction.InstructionText : '');
          }
          if (instructionType.indexOf('GIFT_RECP') > -1) {
            giftRec.push(instruction.InstructionText ? instruction.InstructionText : '');
          }
          if (instructionType.indexOf('WRAP_TYPE') > -1) {
            giftWrap = instruction.InstructionText ? instruction.InstructionText : '';
          }
        });
        lineItem.giftMessages = giftMessages;
        lineItem.giftRec = giftRec;
      }
      if (giftWrap) {
        lineItem.giftWrap = giftWrap;
      }
      lineItem.status = getWebStatus(orderLine.Status);
      returnable =
        !returnable &&
        (lineItem.status.toString().equalsIgnoreCase(Resource.msg('order.lineitem.status.shipped', 'order', null)) ||
          lineItem.status.toString().equalsIgnoreCase(Resource.msg('ORDER_STATUS_PICKED_UP', 'order', null)))
          ? lineItem.returnable
          : returnable;
      lineItem.pickup = orderLine.Status;
      lineItem.dropShip = false;
      if (orderLine.LineType === OMSConstants.OMS_ORDERLINE_DROP_SHIP) {
        lineItem.dropShip = true;
      }
      if (orderLine.LineType === OMSConstants.OMS_ORDERLINE_PRE_ORDER) {
        lineItem.preorder = true;
      }
      lineItem.deliveryMethod = orderLine.DeliveryMethod ? orderLine.DeliveryMethod : '';
      lineItem.estimatedDelivery = getEstimatedDeliveryDate(orderLine);
      // get unformmated date
      var unformattedDeliverydate = getEstimatedDeliveryDateUnFormatted(orderLine);
      if (unformattedDeliverydate) {
        var saksDeliverydate = StringUtils.formatCalendar(new Calendar(unformattedDeliverydate), 'MM/dd/yy');
        lineItem.saksDeliveryDate = Resource.msg('label.estimated.saks.delivery', 'checkout', null) + ' ' + saksDeliverydate;
      }
      lineItem.carrierServiceCode = orderLine.CarrierServiceCode ? orderLine.CarrierServiceCode : '';
      lineItem.SCAC = orderLine.SCAC ? orderLine.SCAC : '';

      // SFSX-2180 - When Order was placed with SR and Shipped
      if (ipaShippingmethod && !empty(ipaShippingmethod)) {
        lineItem.shippingMethodName = getLineItemAPIShippingMethod(ipaShippingmethod);
      }

      if (empty(lineItem.shippingMethodName)) {
        lineItem.shippingMethodName = getLineItemShippingMethod(orderLine, shipments, srtoken);
      }

      lineItem.storeLocation = orderLine.Extn && orderLine.Extn.ExtnPickUpLocation ? orderLine.Extn.ExtnPickUpLocation : '';
      if (lineItem.deliveryMethod === Resource.msg('order.details.pick.status', 'order', null)) {
        lineItem.storeName = orderLine.Shipnode && orderLine.Shipnode.Description ? orderLine.Shipnode.Description.substring(5) : '';
        lineItem.shipNode = orderLine.Shipnode;
      }
      // The following determine if it is a dropship/ship from vendor (i.e NodeType is Vendor and ShipnodeType is DROP_SHIP)
      lineItem.nodeType = orderLine.Shipnode && orderLine.Shipnode.NodeType ? orderLine.Shipnode.NodeType : '';
      lineItem.shipNodeType = orderLine.Shipnode && orderLine.Shipnode.ShipnodeType ? orderLine.Shipnode.ShipnodeType : '';
      lineItem.shippingAddress = getShippingAddress(orderLine.PersonInfoShipTo);
      if (orderLine.Status === OMSConstants.OMS_LINE_ITEM_STATUS_CANCELLED) {
        if (orderLine.Extn && orderLine.Extn.HbcOrderLineChargesList && orderLine.Extn.HbcOrderLineChargesList.HbcOrderLineCharges) {
          var hbcOrderLineCharges = [];
          orderLine.Extn.HbcOrderLineChargesList.HbcOrderLineCharges.forEach(function (charge) {
            var hbcOrderLineCharge = {};
            hbcOrderLineCharge.chargeCategory = charge.ChargeCategory;
            hbcOrderLineCharge.ChargeAmount = charge.ChargeAmount;
            hbcOrderLineCharges.push(hbcOrderLineCharge);
          });
          lineItem.hbcOrderLineCharges = hbcOrderLineCharges;
        }
      }

      var lineItemChargesTaxes = getLineItemChargesAndTaxes(orderLine);
      lineItem.charges = lineItemChargesTaxes.lineItemCharges;
      let shippingCharge = 0;
      var giftWrapCharge = 0;
      lineItemChargesTaxes.lineItemCharges.forEach(function (lineItemCharge) {
        if (lineItemCharge.chargeName === 'SHIPPINGCHARGE') {
          shippingCharge += lineItemCharge.taxAmount;
        }
        if (lineItemCharge.chargeName === 'GIFTWRAPCHARGE') {
          giftWrapCharge += lineItemCharge.taxAmount;
        }
      });
      lineItem.shippingCharges =
        shippingCharge > 0 ? new Money(shippingCharge, currency).toFormattedString() : Resource.msg('info.order.free.shipping', 'order', 'FREE');
      lineItem.giftWrapCharge = giftWrapCharge > 0 ? new Money(giftWrapCharge, currency) : null;
      lineItem.taxes = lineItemChargesTaxes.lineItemTaxes;
      let lineItemDiscount = 0;
      lineItemChargesTaxes.lineItemCharges.forEach(function (lineItemCharge) {
        if (lineItemCharge.category === OMSConstants.OMS_ORDERLINE_CHARGE_CATEGORY_DISCOUNT) {
          lineItemDiscount += lineItemCharge.taxAmount;
        }
      });
      lineItem.discountApplied = lineItemDiscount > 0;
      lineItem.discount = new Money(lineItemDiscount, currency).toFormattedString();
      var itemQty = 0;
      itemQty = getOrderLineQuantity(orderLine);
      lineItem.total = new Money(
        orderLine.LinePriceInfo && orderLine.LinePriceInfo.UnitPrice && itemQty
          ? Number(orderLine.LinePriceInfo.UnitPrice) * Number(itemQty) - lineItemDiscount
          : 0,
        currency
      ).toFormattedString();
      if (orderLine.LineType) {
        lineItem.lineType = orderLine.LineType;
      }
      orderLineItems.push(lineItem);
    });
  }
  lineItemsObject.orderLineItems = orderLineItems;
  lineItemsObject.returnable = returnable;
  return lineItemsObject;
}

function getOrderLineQuantity(orderLine) {
  var itemQty = 0;
  if (orderLine && orderLine.OrderedQty && Number(orderLine.OrderedQty) > 0) {
    itemQty = Number(orderLine.OrderedQty);
  } else {
    if (orderLine && orderLine.OriginalOrderedQty && orderLine.SplitQty && Number(orderLine.SplitQty) > 0) {
      itemQty = Number(orderLine.OriginalOrderedQty) - Number(orderLine.SplitQty);
    } else {
      itemQty = Number(orderLine.OriginalOrderedQty);
    }
  }
  return itemQty;
}
/**
 * Function to fetch order level shipping details
 *
 * @param {Object} productLineItems - SFCC order line object
 * @returns {Object} - order level shipping details
 */
function getOrderShippingDetails(productLineItems) {
  let shippingDetails = {};
  let pickupOrder = true;
  for (var i = 0; i < productLineItems.length; i++) {
    //if (!productLineItems[i].dropShip) {
    shippingDetails.name = productLineItems[i].shippingMethodName;
    shippingDetails.charges = productLineItems[i].shippingCharges;
    shippingDetails.estimatedDelivery = productLineItems[i].estimatedDelivery;
    if (!empty(productLineItems[i].shippingMethodName)) {
      break;
    }
    // }
  }
  productLineItems.forEach(function (lineItem) {
    if (!(lineItem.deliveryMethod === Resource.msg('order.details.pick.status', 'order', null))) {
      pickupOrder = false;
    }
  });
  if (pickupOrder) {
    shippingDetails.name = Resource.msg('order.details.shipping.name.storepickup', 'order', null);
  }
  return shippingDetails;
}

/**
 * Function to get the gift message from first line item
 *
 * @param {Object} productLineItems - SFCC line item object
 * @returns {string} - gift message
 */
function getOrderLineGiftMessage(productLineItems) {
  let giftMessage = null;
  productLineItems.forEach(function (lineItem) {
    if (!!lineItem.giftMessages && !giftMessage) {
      giftMessage = lineItem.giftMessages.toString();
    }
  });
  return giftMessage;
}

/**
 * Function to get the gift message from first line item
 *
 * @param {Object} productLineItems - SFCC line item object
 * @returns {string} - gift message
 */
function getOrderLineGiftRecipient(productLineItems) {
  let giftRec = null;
  productLineItems.forEach(function (lineItem) {
    if (!!lineItem.giftRec && !giftRec) {
      giftRec = lineItem.giftRec.toString();
    }
  });
  return giftRec;
}

/**
 * Function to get if the order is eligible to cancel
 *
 * @param {Object} order - order returned from OMS
 * @returns {string} isCancellable - is cancellable
 */
function isOrderCancellable(order) {
  var OrderMgr = require('dw/order/OrderMgr');
  var ApiOrder = require('dw/order/Order');
  let orderHoldTypes = order.OrderHoldTypes;
  let extn = order.Extn;
  var isCancellable = 'false';

  var OrderObjct = OrderMgr.getOrder(order.OrderNo);

  if (orderHoldTypes && orderHoldTypes.OrderHoldType && orderHoldTypes.OrderHoldType.length > 0) {
    for (let i = 0; i < orderHoldTypes.OrderHoldType.length; i++) {
      var orderType = orderHoldTypes.OrderHoldType[i];
      if (orderType && orderType.Status === '1100' && orderType.HoldType === 'BUYER_REMORSE') {
        isCancellable = 'true';
        break;
      }
    }
  }

  if (extn && extn.ExtnIsFiftyOne && extn.ExtnIsFiftyOne === 'Y') {
    isCancellable = 'false';
  }
  // SFSX-3216
  if (OrderObjct != null && !empty(OrderObjct) && OrderObjct.exportStatus.value == ApiOrder.EXPORT_STATUS_READY) {
    isCancellable = 'true';
  }
  return isCancellable;
}

/**
 * Function to make customer full name with billing first and last name
 *
 * @param {Object} billingAddress - OMS line item object
 * @returns {string} - customer full name
 */
function makeCustomerName(billingAddress) {
  let customerFullName = '';
  if (billingAddress) {
    customerFullName = billingAddress.firstName + ' ' + billingAddress.lastName;
  }
  return customerFullName;
}

/**
 * Constructs a hashmap with itemID and tracking url
 *
 * @param {Object} order - OMS Order object
 * @param {string} customerId - customer ID
 * @returns {dw/util/HashMap} - trackingurl with Itemid information
 */
function getTrackingInformation(order, customerId) {
  var HashMap = require('dw/util/HashMap');
  let shipmentTrackingMap = new HashMap();
  if (order.Shipments && order.Shipments.Shipment) {
    order.Shipments.Shipment.forEach(function (orderShipment) {
      let shippingAddress;
      if (orderShipment.ShipNode && orderShipment.ShipNode.ShipNodePersonInfo) {
        shippingAddress = getShippingAddress(orderShipment.ShipNode.ShipNodePersonInfo);
      }
      if (!shippingAddress) {
        shippingAddress = getShippingAddress(order.PersonInfoShipTo);
      }
      if (orderShipment.Containers && orderShipment.Containers.Container) {
        orderShipment.Containers.Container.forEach(function (container) {
          if (container.TrackingNo && container.ContainerDetails && Number(container.ContainerDetails.TotalNumberOfRecords) > 0) {
            container.ContainerDetails.ContainerDetail.forEach(function (containerDetail) {
              let itemID = containerDetail.ItemID;
              let trackingURL =
                OMSConstants.ORDER_TRACKING_URL +
                container.SCAC +
                '?' +
                'tracking_numbers=' +
                container.TrackingNo +
                '&product_id=' +
                itemID +
                '&ozip=&dzip=' +
                shippingAddress.zipCode +
                '&customer_id=' +
                customerId;
              shipmentTrackingMap.put(itemID, trackingURL);
            });
          }
        });
      }
    });
  }
  return shipmentTrackingMap;
}

/**
 * retrieve the product variant value selected by the user
 *
 * @param {Object} apiProduct - dw.catalog.Product
 * @returns {Object} - values for color and size
 */
function getVariantAttributes(apiProduct) {
  var colorSize = {};
  var variationModel = apiProduct.variationModel;
  var allAttributes = variationModel.productVariationAttributes;
  var collections = require('*/cartridge/scripts/util/collections');
  collections.forEach(allAttributes, function (attr) {
    var selectedValue = variationModel.getSelectedValue(attr);
    if (attr.attributeID === 'color' && selectedValue) {
      colorSize.color = selectedValue.displayValue;
    } else if (attr.attributeID === 'size' && selectedValue) {
      colorSize.size = selectedValue.displayValue;
    }
  });
  return colorSize;
}

/**
 * Check if the current user is in English locale
 * @returns
 */
function isEnglishLocale() {
  var locale = request.locale;
  if (locale === 'en_US' || locale === 'en_CA' || locale === 'en' || locale === 'en_GB') {
    return true;
  }
  return false;
}

/**
 * Calculate the total quantity of items in an order
 * @param orderInfo
 * @returns
 */
function getOrderLineItemsTotalQuantity(orderInfo) {
  var totalQty = 0;
  var lineItems = orderInfo.productLineItems;
  lineItems.forEach(function (lineItem) {
    if (lineItem.quantity && lineItem.quantity !== '') {
      totalQty += Number(lineItem.quantity);
    }
  });
  return totalQty;
}

function giftWrapPrice(lineItem, currency) {
  var giftPrice = 0;
  if (lineItem.length > 0) {
    for (var i = 0; i < lineItem.length; i++) {
      var item = lineItem[i];
      if (item.giftWrapCharge && item.giftWrapCharge != null) {
        giftPrice += item.giftWrapCharge;
      }
    }
  }
  return giftPrice > 0 ? new Money(giftPrice, currency).toFormattedString() : null;
}

function isOnlyPickupOrder(lineItems) {
  try {
    var isOnlyPickup = true;
    if (lineItems.length > 0) {
      for (var i = 0; i < lineItems.length; i++) {
        var item = lineItems[i];
        if (item.deliveryMethod && item.deliveryMethod != null && item.deliveryMethod != 'PICK') {
          isOnlyPickup = false;
          break;
        }
      }
    }
  } catch (e) {
    isOnlyPickup = false;
  }
  return isOnlyPickup;
}

function hasBOPISShipment(lineItems) {
  try {
    var hasBopisShipment = false;
    if (lineItems.length > 0) {
      for (var i = 0; i < lineItems.length; i++) {
        var item = lineItems[i];
        if (item.deliveryMethod && item.deliveryMethod != null && item.deliveryMethod == 'PICK') {
          hasBopisShipment = true;
          break;
        }
      }
    }
  } catch (e) {
    hasBopisShipment = false;
  }
  return hasBopisShipment;
}

function hasSDDShipment(lineItems) {
  try {
    var hasSDDShipment = false;
    if (lineItems.length > 0) {
      for (var i = 0; i < lineItems.length; i++) {
        var item = lineItems[i];
        if (item.carrierServiceCode && item.carrierServiceCode != null && item.carrierServiceCode.toLowerCase() == 'sameday') {
          hasSDDShipment = true;
          break;
        }
      }
    }
  } catch (e) {
    hasSDDShipment = false;
  }
  return hasSDDShipment;
}

/**
 * Order Model construction
 *
 * @param {Object} order - order object
 * @param {string} customerId - order customer ID
 */
function OrderModel(order, customerId) {
  var Site = require('dw/system/Site');
  var orderInfo = {};
  orderInfo.sfccOrder = false;
  orderInfo.status = order.Status ? getWebStatus(order.Status) : '';
  var orderNo = order.OrderNo || '';
  if (!empty(orderNo) && OMSConstants.OMS_RemovedZeroPrefix) {
    orderNo = orderNo.replace(/^0+/, '');
  }
  orderInfo.orderNo = orderNo;
  let orderDate = order.OrderDate ? order.OrderDate.split('T')[0] : '';
  let DateArray = orderDate ? orderDate.split('-') : '';
  orderDate = DateArray.length > 0 ? new Date(DateArray[0] + '/' + DateArray[1] + '/' + DateArray[2]) : '';
  var StringUtils = require('dw/util/StringUtils');
  var Calendar = require('dw/util/Calendar');
  orderInfo.orderDate = StringUtils.formatCalendar(new Calendar(orderDate), request.locale, 1);
  orderInfo.orderDateFormatted = StringUtils.formatCalendar(new Calendar(orderDate), 'MMMM dd, yyyy');
  orderInfo.documnentType = order.DocumentType;
  orderInfo.banner = order.EnterpriseCode;
  orderInfo.currency = order.PriceInfo ? order.PriceInfo.Currency : Site.getCurrent().defaultCurrency;
  orderInfo.originalOrderTotalAmount = new Money(order.OriginalTotalAmount ? Number(order.OriginalTotalAmount) : 0, orderInfo.currency).toFormattedString();
  orderInfo.originalOrderShippingTotal = getShippingTotal(order, orderInfo.currency);
  var orginalOrderTotals = getOriginalItemsTotal(order);
  orderInfo.originalOrderItemsTotal = new Money(
    orginalOrderTotals.originalOrderItemsTotal ? Number(orginalOrderTotals.originalOrderItemsTotal) : 0,
    orderInfo.currency
  ).toFormattedString();
  orderInfo.lineItemSubTotal = new Money(
    orginalOrderTotals.lineItemSubTotal ? Number(orginalOrderTotals.lineItemSubTotal) : 0,
    orderInfo.currency
  ).toFormattedString();
  orderInfo.feesAndTaxes = getFeesAndTaxes(order, orderInfo.currency);
  orderInfo.discountSubTotal = orderInfo.feesAndTaxes.discountTotal;
  orderInfo.originalOrderTaxTotal = new Money(
    orginalOrderTotals.originalOrderTaxTotal ? Number(orginalOrderTotals.originalOrderTaxTotal) : 0,
    orderInfo.currency
  ).toFormattedString();
  orderInfo.promotions = order.Promotions ? order.Promotions : '';
  orderInfo.billingAddress = getBillingAddress(order.PersonInfoBillTo);
  orderInfo.shippingAddress = getShippingAddress(order.PersonInfoShipTo);
  orderInfo.shipments = getOrderShipments(order, customerId);
  orderInfo.paymentInfo = getPaymentDetails(order, orderInfo.currency);
  orderInfo.trackingInformation = getTrackingInformation(order, customerId);
  let lineItemsObject = getOrderLines(order, orderInfo.currency, orderInfo.trackingInformation, orderInfo.shipments);
  orderInfo.productLineItems = lineItemsObject.orderLineItems;
  orderInfo.lineItemsTotalQuantity = getOrderLineItemsTotalQuantity(orderInfo);
  orderInfo.returnable = lineItemsObject.returnable;
  orderInfo.productLineItemsCount = orderInfo.productLineItems.length;
  orderInfo.shippingDetails = getOrderShippingDetails(orderInfo.productLineItems);
  orderInfo.onlyPickupOrder = isOnlyPickupOrder(orderInfo.productLineItems);
  orderInfo.hasBOPISShipment = hasBOPISShipment(orderInfo.productLineItems);
  orderInfo.hasSDDShipment = hasSDDShipment(orderInfo.productLineItems);
  orderInfo.giftMessages = getOrderLineGiftMessage(orderInfo.productLineItems);
  orderInfo.giftRecipient = getOrderLineGiftRecipient(orderInfo.productLineItems);
  orderInfo.cancellable = isOrderCancellable(order);
  orderInfo.datePlaced = order.OrderDate;
  orderInfo.giftWrapPrice = giftWrapPrice(orderInfo.productLineItems, orderInfo.currency);
  this.order = orderInfo;
}
module.exports = OrderModel;
