'use strict';
var Money = require('dw/value/Money');
var OMSConstants = require('*/cartridge/scripts/config/OMSServiceConfig');

/**
 * constructs shipping address
 *
 * @param personBillTo
 * @returns
 */
function getBillingAddress(personBillTo) {
  var billingAddress = new Object();
  billingAddress.firstName = personBillTo.firstName;
  billingAddress.lastName = personBillTo.lastName;
  billingAddress.addressLine1 = personBillTo.address1;
  billingAddress.addressLine2 = personBillTo.address2;
  billingAddress.city = personBillTo.city;
  billingAddress.state = personBillTo.stateCode;
  billingAddress.zipCode = personBillTo.postalCode;
  billingAddress.country = personBillTo.countryCode.value;
  billingAddress.phoneNo = personBillTo.phone;
  return billingAddress;
}

/**
 * Fetch SFCC payment details
 *
 * @param {Object} order - SFCC order model
 * @param {string} currency - currency code
 * @returns {Object} - payment details model
 */
function getPaymentDetails(order, currency) {
  var paymentDetails = {};
  var paymentMethods = [];
  if (order.billing && order.billing.payment && order.billing.payment.selectedPaymentInstruments) {
    order.billing.payment.selectedPaymentInstruments.forEach(function (method) {
      var paymentMethod = {};
      paymentMethod.paymentType = method.paymentMethod;
      if (method.gcNumber) {
        paymentMethod.giftCardNumber = method.shortMaskedNumber.replace('*', '', 'g');
      } else if (method.shortMaskedNumber) {
        paymentMethod.creditCardNo = method.shortMaskedNumber;
      }
      if (method.type) {
        paymentMethod.creditCardType = Object.prototype.hasOwnProperty.call(OMSConstants.OMS_CREDIT_CARD_TYPE_MAPPING, method.type)
          ? OMSConstants.OMS_CREDIT_CARD_TYPE_MAPPING[method.type]
          : method.name;
      }
      if (method.owner) {
        paymentMethod.creditCardName = method.owner;
      }
      
      if (paymentMethod.creditCardName === 'Klarna Bank AB') {
        paymentMethod.paymentType = 'KLARNA';
      }
      let totalAuthorized = method.amount || '0';
      paymentMethod.totalPayment = new Money(Number(totalAuthorized), currency).toFormattedString();
      paymentMethods.push(paymentMethod);
    });
  }
  paymentDetails.paymentMethods = paymentMethods;
  return paymentDetails;
}

/**
 * constructs shipping address
 *
 * @param personShipTo
 * @returns
 */
function getShippingAddress(personShipTo) {
  var shippingAddress = {};
  if (personShipTo) {
    shippingAddress.firstName = personShipTo.firstName;
    shippingAddress.lastName = personShipTo.lastName;
    shippingAddress.addressLine1 = personShipTo.address1;
    shippingAddress.addressLine2 = personShipTo.address2 || '';
    shippingAddress.city = personShipTo.city;
    shippingAddress.state = personShipTo.stateCode;
    shippingAddress.zipCode = personShipTo.postalCode;
    shippingAddress.country = personShipTo.countryCode.value;
    shippingAddress.phoneNo = personShipTo.phone;
    shippingAddress.email = personShipTo.email;
  }

  return shippingAddress;
}

/**
 * Get the shipment data based on the shipped items in the order
 *
 * @param {Object} order - SFCC order model
 * @returns {Object} - shipment order model
 */
function getOrderShipments(order) {
  var shipments = [];
  if (order.shipping) {
    order.shipping.forEach(function (shipping) {
      var shipment = {};
      if (shipping.shippingAddress) {
        shipment.shippingAddress = getShippingAddress(shipping.shippingAddress);
      }
      if (shipping.selectedShippingMethod) {
        shipment.selectedShippingMethod = shipping.selectedShippingMethod.displayName;
      }
      if (shipping.isGift) {
        shipment.isGift = shipping.isGift;
      }
      if (shipping.giftMessage) {
        shipment.giftMessage = shipping.giftMessage;
      }
      shipments.push(shipment);
    });
  }
  return shipments;
}
/**
 * get Order tax value
 * @param order
 * @returns
 */
function getOrderTaxTotal(order) {
  var totalTax = order.totals && order.totals.totalTax ? order.totals.totalTax : '0';
  return totalTax;
}

/**
 * Fetch order shipping cost
 *
 * @param {Object} order - SFCC order model
 * @returns {string} - shipping cost
 */
function getOrderShippingTotal(order) {
  var totalShipping =
    order.totals && order.totals.totalShippingAdjusted && order.totals.totalShippingAdjusted.value > 0
      ? dw.util.StringUtils.formatMoney(order.totals.totalShippingAdjusted)
      : 'FREE';
  return totalShipping;
}

/**
 * Fetch shipping method details at order line level
 *
 * @param {Object} order - SFCC order model
 * @param {Object} lineItem - SFCC line item model
 * @returns {Object} - Shipment model for line item
 */
function getShippingMethodDetails(order, lineItem) {
  let shipmentDetails = {};
  order.shipping.forEach(function (shippingItem) {
    if (shippingItem.UUID === lineItem.shipmentUUID) {
      shipmentDetails.shippingMethodName = shippingItem.selectedShippingMethod.displayName;
      shipmentDetails.shippingCharges =
        shippingItem.selectedShippingMethod.unFormatFinalShippingCost > 0 ? shippingItem.selectedShippingMethod.finalShippingCost : 'FREE';
      shipmentDetails.estimatedDelivery = shippingItem.selectedShippingMethod.estimatedArrivalTime;
    }
  });
  return shipmentDetails;
}
/**
 * Fetch order line items
 *
 * @param {Object} order - SFCC order model
 * @returns {Object} - product line items model
 */
function getOrderLines(order) {
  var orderLineItems = [];
  var ProductMgr = require('dw/catalog/ProductMgr');
  var URLUtils = require('dw/web/URLUtils');
  var preferences = require('*/cartridge/config/preferences');
  var isFinalSaleToogle = preferences && preferences.product ? preferences.product.IS_FINAL_SALE_TOOGLE : '';

  if (order.items && order.items.items) {
    order.items.items.forEach(function (item) {
      var lineItem = {};
      lineItem.name = item.productName ? item.productName : '';
      lineItem.itemId = item.id ? item.id : '';
      let apiProduct;
      if (lineItem.itemId) {
        apiProduct = ProductMgr.getProduct(lineItem.itemId);
        if (apiProduct) {
          lineItem.reviewable = 'reviewable' in apiProduct.custom && apiProduct.custom.reviewable;
          lineItem.brandName = apiProduct.brand || '';
          var returnable = true;
          if (
            (isFinalSaleToogle === 'true' && 'finalSale' in apiProduct.custom && apiProduct.custom.finalSale) ||
            ('returnable' in apiProduct.custom && apiProduct.custom.returnable === 'false')
          ) {
            returnable = false;
          }
          lineItem.returnable = returnable;
          lineItem.dropShip = 'dropShipInd' in apiProduct.custom && apiProduct.custom.dropShipInd === 'true';
          lineItem.pdpURL = URLUtils.https('Product-Show', 'pid', lineItem.itemId).toString();
          lineItem.masterProductId = apiProduct && apiProduct.variant && apiProduct.masterProduct ? apiProduct.masterProduct.ID : lineItem.itemId;
        }
      }
      if (item.variationAttributes) {
        item.variationAttributes.forEach(function (attribute) {
          if (attribute.id === 'size') {
            lineItem.size = attribute.displayValue;
          } else if (attribute.id === 'color') {
            lineItem.color = attribute.displayValue;
          }
        });
      }
      lineItem.unitPrice = item.price.sales.formatted;
      lineItem.quantity = item.quantity;
      lineItem.imageURL = item.images && item.images.small && item.images.small[0] && item.images.small[0].url ? item.images.small[0].url.toString() : '';
      lineItem.giftFlag = item.isGift ? item.isGift : 'N';
      lineItem.status = order.orderStatus.displayValue ? getWebStatus(order.orderStatus.displayValue) : '';
      let shipmentDetails = getShippingMethodDetails(order, item);
      lineItem.shippingMethodName = shipmentDetails.shippingMethodName;
      lineItem.shippingCharges = shipmentDetails.shippingCharges;
      lineItem.estimatedDelivery = shipmentDetails.estimatedDelivery;
      lineItem.deliveryMethod = item.fromStoreId ? 'PICK' : '';
      orderLineItems.push(lineItem);
    });
  }

  return orderLineItems;
}

/**
 * Function to return web friendly status
 *
 * @param {string} status - OMS status
 * @returns {string} - Web friendly status
 */
function getWebStatus(status) {
  var orderStatusPrefix = 'ORDER_STATUS_';
  let statusMapping = OMSConstants.OMS_ORDER_DETAILS_STATUS;
  var status = dw.web.Resource.msg(statusMapping[status] ? orderStatusPrefix + statusMapping[status] : orderStatusPrefix + 'IN_PROGRESS', 'order', status);
  return status;
}

/**
 * Fetch the promo codes applied on the order
 *
 * @param {Array} totalCoupons - Array of all the coupon codes
 * @returns {Object} - object of all the promotion coupond codes
 */
function getAppliedPromotionCoupons(totalCoupons) {
  let promotions = {};
  if (totalCoupons.length) {
    let promos = [];
    totalCoupons.forEach(function (coupon) {
      promos.push({ PromotionId: coupon });
    });
    promotions.Promotion = promos;
  }
  return promotions;
}

/**
 * Function to fetch order level shipping details
 *
 * @param {Object} productLineItems - SFCC order line object
 * @returns {Object} - order level shipping details
 */
function getOrderShippingDetails(productLineItems) {
  let shippingDetails = {};
  for (var i = 0; i < productLineItems.length; i++) {
    if (!productLineItems[i].dropShip) {
      shippingDetails.name = productLineItems[i].shippingMethodName;
      shippingDetails.charges = productLineItems[i].shippingCharges;
      shippingDetails.estimatedDelivery = productLineItems[i].estimatedDelivery;
      if (!empty(productLineItems[i].shippingMethodName)) {
        break;
      }
    }
  }
  return shippingDetails;
}

/**
 * Fetch gift message from shipments to be shown at order level
 *
 * @param {Object} shipments - Array of shipment objects
 * @returns {string} - gift message
 */
function getGiftMessage(shipments) {
  let giftMessage = '';
  if (shipments.length) {
    shipments.forEach(function (shipment) {
      if (shipment.isGift && shipment.giftMessage && !giftMessage) {
        giftMessage = shipment.giftMessage;
      }
    });
  }
  return giftMessage;
}

/**
 * Calculate the total quantity of items in an order
 * @param orderInfo
 * @returns
 */
function getOrderLineItemsTotalQuantity(orderInfo) {
  var totalQty = 0;
  var lineItems = orderInfo.productLineItems;
  lineItems.forEach(function (lineItem) {
    if (lineItem.quantity && lineItem.quantity != '') {
      totalQty = totalQty + Number(lineItem.quantity);
    }
  });
  return totalQty;
}

/**
 * Order Model construction
 *
 * @param {Object} order - SFCC order model
 */
function OrderModel(order) {
  var Site = require('dw/system/Site');
  var orderInfo = {};
  orderInfo.sfccOrder = true;
  orderInfo.status = order.orderStatus ? getWebStatus(order.orderStatus.displayValue) : '';
  orderInfo.orderNo = order.orderNumber ? order.orderNumber : '';
  orderInfo.orderDate = order.creationDate ? order.creationDate : '';
  orderInfo.documnentType = OMSConstants.SERVICE_DOCUMENT_TYPE;
  orderInfo.banner = OMSConstants.BANNER_CODE;
  orderInfo.currency = order.totals && order.totals.currencyCode ? order.totals.currencyCode : Site.getCurrent().defaultCurrency;
  orderInfo.originalOrderTotalAmount = new Money(
    order.totals && order.totals.grandTotalValue ? Number(order.totals.grandTotalValue) : 0,
    orderInfo.currency
  ).toFormattedString();
  orderInfo.originalOrderTaxTotal = getOrderTaxTotal(order);
  orderInfo.originalOrderShippingTotal = getOrderShippingTotal(order);
  let orderLevelDiscount =
    order.totals && order.totals.orderLevelDiscountTotal && order.totals.orderLevelDiscountTotal.value ? order.totals.orderLevelDiscountTotal.value : 0;
  orderInfo.discountSubTotal = orderLevelDiscount > 0 ? new Money(orderLevelDiscount, orderInfo.currency).toFormattedString() : '';
  orderInfo.shippingLevelDiscountTotal =
    order.totals && order.totals.shippingLevelDiscountTotal && order.totals.shippingLevelDiscountTotal.formatted
      ? order.totals.shippingLevelDiscountTotal.formatted
      : 0;
  orderInfo.originalOrderItemsTotal = new Money(
    order.totals && order.totals.subTotalValue ? order.totals.subTotalValue : 0,
    orderInfo.currency
  ).toFormattedString();
  orderInfo.lineItemSubTotal = order.totals && order.totals.subTotal ? order.totals.subTotal : 0;
  var feesAndTaxes = {};
  if (orderInfo.banner === 'BAY') {
    var canadaTaxation = {};
    if (order.totals.canadaTaxation && Object.keys(order.totals.canadaTaxation).length) {
      canadaTaxation = order.totals.canadaTaxation;
    }
    feesAndTaxes.GSTHST = canadaTaxation.GSTHST || new Money(0, orderInfo.currency).toFormattedString();
    feesAndTaxes.QSTPSTRST = canadaTaxation.QSTPSTRST || new Money(0, orderInfo.currency).toFormattedString();
    feesAndTaxes.ecoFeeChargeTotal = canadaTaxation.ecoFee > new Money(0, orderInfo.currency) ? feesAndTaxes.ecoFee.toFormattedString() : '';
  } else {
    feesAndTaxes.generalSalesAndUseTax = order.totals.totalTax || '';
  }
  feesAndTaxes.discountTotal = orderInfo.discountSubTotal;
  orderInfo.feesAndTaxes = feesAndTaxes;
  if (order.billing && order.billing.billingAddress && order.billing.billingAddress.address) {
    orderInfo.billingAddress = getBillingAddress(order.billing.billingAddress.address);
  }
  if (!!Object.keys(order.shipping).length && !!Object.keys(order.shipping[0].shippingAddress)) {
    orderInfo.shippingAddress = getShippingAddress(order.shipping[0].shippingAddress);
  }
  orderInfo.paymentInfo = getPaymentDetails(order, orderInfo.currency);
  orderInfo.shipments = getOrderShipments(order);
  orderInfo.productLineItems = getOrderLines(order);
  orderInfo.lineItemsTotalQuantity = getOrderLineItemsTotalQuantity(orderInfo);
  orderInfo.productLineItemsCount = orderInfo.productLineItems.length;
  orderInfo.promotions = getAppliedPromotionCoupons(order.totalAppliedCouponCodes);
  orderInfo.shippingDetails = getOrderShippingDetails(orderInfo.productLineItems);
  orderInfo.giftMessages = getGiftMessage(orderInfo.shipments);
  this.order = orderInfo;
}

module.exports = OrderModel;
