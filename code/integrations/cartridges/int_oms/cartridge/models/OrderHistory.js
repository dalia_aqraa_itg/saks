'use strict';

var OMSConstants = require('*/cartridge/scripts/config/OMSServiceConfig');

/**
 * Fetch the web status based on locale
 *
 * @param {string} status - order status
 * @returns {string} locale status
 */
function getWebStatus(status) {
  var Resource = require('dw/web/Resource');
  var orderStatusPrefix = 'ORDER_STATUS_';
  let statusMapping = OMSConstants.OMS_ORDER_DETAILS_STATUS;
  let localeStatus = Resource.msg(statusMapping[status] ? orderStatusPrefix + statusMapping[status] : orderStatusPrefix + 'IN_PROGRESS', 'order', status);
  return localeStatus;
}

/**
 * Fetch order date based on the locale
 *
 * @param {Date} orderDate - order Date from oms
 * @returns {string} - locale date
 */
function getOrderDate(orderDate) {
  var StringUtils = require('dw/util/StringUtils');
  var Calendar = require('dw/util/Calendar');
  return StringUtils.formatCalendar(new Calendar(orderDate), request.locale, 1);
}

/**
 * Order history model for storefront
 *
 * @param {Object} orders - OMS order model
 */
function OrderHistoryModel(orders) {
  var orderHistory = [];
  orders.forEach(function (order) {
    var orderObject = {};
    orderObject.orderNumber = order.OrderNo;
    orderObject.status = getWebStatus(order.Status);
    let orderDate = order.OrderDate.split('T')[0];
    let DateArray = orderDate.split('-');
    orderDate = new Date(DateArray[0] + '/' + DateArray[1] + '/' + DateArray[2]);
    orderObject.creationDate = getOrderDate(orderDate);
    orderObject.orderTotal = order.PriceInfo.TotalAmount;
    orderObject.orderTotalCurrency = order.PriceInfo.Currency;
    orderObject.orderStatus = order.orderStatus;
    orderHistory.push(orderObject);
  });

  this.orders = orderHistory;
}

module.exports = OrderHistoryModel;
