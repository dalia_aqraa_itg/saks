'use strict';


var Logger = require('dw/system/Logger');
importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.customer );
importPackage( dw.io );
importPackage(dw.value);
var RootLogger = require('dw/system/Logger').getRootLogger();


function offlineCreateOrder (args, jobStepExecution){

	var OrderAPIUtils = require('*/cartridge/scripts/util/OrderAPIUtil');

	var OrderMgr = require('dw/order/OrderMgr');
	var Logger = require('dw/system/Logger');
	var Order = require('dw/order/Order');
	var Transaction = require('dw/system/Transaction');
  	var Calendar = require('dw/util/Calendar');
	var Site = require('dw/system/Site');
	var params = arguments[0];
	var prevDays = params.ExportFromPastTheseDays ? - Number(params.ExportFromPastTheseDays) : -1;
	var customPreferences = Site.current.preferences.custom;

	var cal = new Calendar();
    cal.add(Calendar.DAY_OF_YEAR, prevDays);

	var orderIterartor = OrderMgr.queryOrders(
			'(exportStatus={0} OR exportStatus={1}) AND (status={2} OR status={3}) AND creationDate > {4}',
			'creationDate desc',
			Order.EXPORT_STATUS_FAILED,
			Order.EXPORT_STATUS_READY,
			Order.ORDER_STATUS_NEW,
			Order.ORDER_STATUS_OPEN,
			cal.time
	);

	Logger.info("Offline Orders Count -->" + orderIterartor.getCount());
	try {
		for each (var order : Order in orderIterartor) {
				try {
					Logger.info("Create Offline Order No -->" + order.getOrderNo());

					Transaction.wrap(function () {
						//SFDEV-11204 | Generate OMS Create Order XML to be used in ExportOrders batch job
						order.custom.omsCreateOrderXML = OrderAPIUtils.buildRequestXML(order);
					});

					if (customPreferences.orderCreateBatchEnabled !== true) {
						var responseOrderXML = OrderAPIUtils.createOrderInOMS(order);
						Logger.info("Offline responseOrderXML -->" + responseOrderXML);
						var resXML = new XML(responseOrderXML);
						Logger.debug("resXML.child(ResponseMessage) -->" + resXML.child("ResponseMessage"));

						if(resXML.child("ResponseMessage") == "Success"){
							Transaction.wrap(function () {
								order.exportStatus = Order.EXPORT_STATUS_EXPORTED ;
								order.trackOrderChange("Order Export Successful") ;
								order.trackOrderChange(responseOrderXML) ;

							});
						} else {
							Transaction.wrap(function () {
								order.trackOrderChange("Order Exported Failed") ;
								order.trackOrderChange(responseOrderXML) ;

							});
						}
					} else {
						Transaction.wrap(function () {
							order.exportStatus = Order.EXPORT_STATUS_READY ;
						});
					}

				} catch (e) {
					// this try catch block allows the next correct order to be processed.
					var errorMsg = e.fileName + "| line#:"+ e.lineNumber + "| Message:" + e.message+ "| Stack:" + e.stack;
					Logger.info("Export Failed Order Number -->" + order.getOrderNo());
					Logger.error("Export Failed Order Number -->" + order.getOrderNo() + " Error stack trace : " + errorMsg);
				}
		}
	} catch (err) {
        RootLogger.fatal('Error while calling the Order Create Service for Order ' + order.orderNo + ' error message ' + err.message);
    }
	return new Status(Status.OK);

}

exports.offlineCreateOrder = offlineCreateOrder;
