'use strict';
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');
var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var XMLStreamWriter = require('dw/io/XMLStreamWriter');
var Logger = require('dw/system/Logger');
var Status = require('dw/system/Status');
var Transaction = require('dw/system/Transaction');

var count = 0;
var totalOrderCounter = 0; // Running Count of Orders.
var ordersInFile = 0; // Total Orders in a Single file ("TotalNoOfOrders" attribute).
var fileOrderCounter = 0; // Running count of Orders in a single file.
var totalFilesCount = 0; // Running count of xml files created.
var ordersItr;
var fileWriter;
var xmlWriter;
var orderList;
var ordersFile;

function createOrdersExportFile(fileName) {
  count = count + 1;
  var targetDirectory = new File(File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'order_export');
  if (!targetDirectory.exists()) {
    targetDirectory.mkdirs();
  }
  var timeStamp = StringUtils.formatCalendar(new Calendar(), 'yyyy_MM_dd_HH_mm');
  var fileName = fileName + '_' + count + '_' + timeStamp + '.xml';
  var file = new File(targetDirectory, fileName);
  return file;
}

exports.exportOrderData = function (args, jobStepExecution) {
  try {
    var numberOfOrdersPerFile = args.NumberOfOrdersPerFile ? parseInt(args.NumberOfOrdersPerFile) : 1000;
    var OrderAPIUtils = require('*/cartridge/scripts/util/OrderAPIUtil');
    var Order = require('dw/order/Order');
    var OrderMgr = require('dw/order/OrderMgr');
    var ArrayList = require('dw/util/ArrayList');
    orderList = new ArrayList();

    ordersItr = OrderMgr.searchOrders(
      '(exportStatus!={0} AND exportStatus!={1}) AND (status={2} OR status={3})',
      'creationDate desc',
      Order.EXPORT_STATUS_EXPORTED,
      Order.EXPORT_STATUS_FAILED,
      Order.ORDER_STATUS_NEW,
      Order.ORDER_STATUS_OPEN
    );

    Logger.info('====Order Count Initial===' + ordersItr.count);
    if (ordersItr.count > 0) {
      var OrderListTagAdded = false;
      var totalOrders = ordersItr.count;
      var totalFiles = Math.ceil(totalOrders / numberOfOrdersPerFile); // Total number of Files, which will be created.

      ordersInFile = totalOrders >= numberOfOrdersPerFile ? numberOfOrdersPerFile : totalOrders; // Total Orders in a Single file ("TotalNoOfOrders" attribute)

      while (ordersItr.hasNext()) {
        var order = ordersItr.next();
        var orderXMLString = '';
        try {
          orderXMLString = order.custom.omsCreateOrderXML;
          Logger.info('====Order XML for === ' + order.orderNo + '\n' + orderXMLString);
        } catch (error) {
          Logger.error('There was an error while generating the Order.xml for Order Number ' + order.orderNo + ' error message ' + error.message);
          Transaction.wrap(function () {
            order.exportStatus = Order.EXPORT_STATUS_FAILED;
            order.trackOrderChange('Order Export Failed while generating the Order.xml');
          });
          totalOrders = totalOrders - 1; // Adjusting the Total Orders count.
          ordersInFile = totalOrders >= numberOfOrdersPerFile ? numberOfOrdersPerFile : totalOrders;
          continue;
        }

        // Create a new file when fileOrderCounter is 0;
        if (fileOrderCounter === 0) {
          if (xmlWriter != null) {
            xmlWriter.close();
          }
          if (fileWriter != null) {
            fileWriter.close();
          }
          totalFilesCount = totalFilesCount + 1;

          if (totalFilesCount === totalFiles) {
            //  If we are on the last file
            if (totalOrders > numberOfOrdersPerFile) {
              ordersInFile = totalOrders % numberOfOrdersPerFile; // Get the reminder orders count
            }
          }
          ordersFile = createOrdersExportFile('OrdersExportFile');
          fileWriter = new FileWriter(ordersFile, 'UTF-8');
          xmlWriter = new XMLStreamWriter(fileWriter);

          if (!empty(orderXMLString)) {
            xmlWriter.writeStartDocument('UTF-8', '1.0');
            var OrderListStr = '<OrderList TotalNoOfOrders="' + ordersInFile + '">';
            orderXMLString = OrderListStr + '\n' + orderXMLString;
            orderList.add(order);
            xmlWriter.writeRaw('\n' + orderXMLString);
          }
        } else {
          if (!empty(orderXMLString)) {
            orderList.add(order);
            xmlWriter.writeRaw('\n' + orderXMLString);
          }
        }
        // Increment the counter
        fileOrderCounter = fileOrderCounter + 1;
        totalOrderCounter = totalOrderCounter + 1;

        if (totalOrders < numberOfOrdersPerFile) {
          // if only 1 file is being created
          if (fileOrderCounter === totalOrders) {
            // if this is the last order in that file
            fileOrderCounter = 0;
            xmlWriter.writeRaw('\n' + '</OrderList>');
            xmlWriter.writeEndDocument();
            OrderListTagAdded = true;
          }
        } else {
          // If multiple files are being created
          if (fileOrderCounter === numberOfOrdersPerFile) {
            // If this is the last order in one of the files except the last file.
            fileOrderCounter = 0;
            xmlWriter.writeRaw('\n' + '</OrderList>');
            xmlWriter.writeEndDocument();
            OrderListTagAdded = true;
          } else if (totalOrderCounter === totalOrders) {
            // If this is the last order in the last file.
            fileOrderCounter = 0;
            xmlWriter.writeRaw('\n' + '</OrderList>');
            xmlWriter.writeEndDocument();
            OrderListTagAdded = true;
          }
        }
      }
      //Adding a Sanity check for issue Missing </OrderList> closing tag - SFSX-3598
      if (!OrderListTagAdded) {
        fileOrderCounter = 0;
        xmlWriter.writeRaw('\n' + '</OrderList>');
        xmlWriter.writeEndDocument();
      }
    }
  } catch (e) {
    Logger.getRootLogger().fatal('There was an error while creating the Order Export batch file' + e.message);
    return new Status(Status.ERROR, 'ERROR');
  } finally {
    var collections = require('*/cartridge/scripts/util/collections');
    Logger.info('====Total Orders Exported ===' + orderList.size());
    collections.forEach(orderList, function (order) {
      Transaction.wrap(function () {
        order.exportStatus = Order.EXPORT_STATUS_EXPORTED;
        order.trackOrderChange('Order Export Successful');
      });
    });
    if (ordersItr != null) {
      ordersItr.close();
    }
    if (xmlWriter != null) {
      xmlWriter.close();
    }
    if (fileWriter != null) {
      fileWriter.close();
    }

    orderList.removeAll(orderList);
  }
  return new Status(Status.OK);
};

exports.compressFiles = function (args, jobStepExecution) {
  var orderExportFolder = jobStepExecution.getParameterValue('OrderExportFolder');
  var sourceDir = new File(File.IMPEX + '/src/' + orderExportFolder);
  var collections = require('*/cartridge/scripts/util/collections');
  collections.forEach(sourceDir.listFiles(), function (exportFile) {
    var fileName = exportFile.fullPath;
    if (fileName.indexOf('.xml') !== -1) {
      var zippedFile = new File(fileName + '.gz');
      exportFile.gzip(zippedFile);
      exportFile.remove();
    }
  });
  return new Status(Status.OK);
};

exports.archiveOrderFiles = function (args, jobStepExecution) {
  var collections = require('*/cartridge/scripts/util/collections');
  var folder = jobStepExecution.getParameterValue('SourceFolder');
  var sourceDir = new File(File.IMPEX + '/src/' + folder);
  var archiveFolder = jobStepExecution.getParameterValue('ArchiveFolder');
  var daysToBeArchived = jobStepExecution.getParameterValue('DaysToBeArchived');

  var archiveFileDir = new File(File.IMPEX + '/src/' + archiveFolder);
  if (!archiveFileDir.exists()) {
    archiveFileDir.mkdirs();
  }

  collections.forEach(sourceDir.listFiles(), function (file) {
    if (file.name.indexOf('.gz') !== -1) {
      var archiveFile = new File(archiveFileDir.fullPath + File.SEPARATOR + file.name);
      file.renameTo(archiveFile);
    }
  });
  // Logic to remove all the Older Archived files
  var oldCalendar = new Calendar();
  oldCalendar.add(Calendar.DAY_OF_YEAR, -1 * daysToBeArchived);
  var listFiles = archiveFileDir.listFiles();
  collections.forEach(listFiles, function (archiveFile) {
    var fileCalendar = new Calendar(new Date(archiveFile.lastModified()));
    if (fileCalendar.before(oldCalendar)) {
      archiveFile.remove();
    }
  });

  return new Status(Status.OK);
};
