'use strict';

/**
 * Unique OMS service identifiers
 */
const OMSServices = {
  OMS_ORDER_LIST: 'GetOrderHistoryService',
  OMS_ORDER_DETAILS: 'GetOrderDetailsService',
  OMS_CANCEL_ORDER: 'CancelOrderService'
};

module.exports = OMSServices;
