var Site = require('dw/system/Site');
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var OMSServices = require('*/cartridge/scripts/services/OMSServices');
var OMSServiceConfig = require('*/cartridge/scripts/config/OMSServiceConfig');
var OMSLogger = dw.system.Logger.getLogger('OMS', 'OMSService');

/**
 * Service initialization
 */
module.exports = {
  getInstance: function () {
    return LocalServiceRegistry.createService(OMSServices.OMS_ORDER_LIST, {
      createRequest: function (svc, args) {
        svc = svc.addHeader('Content-Type', 'application/json');
        svc = svc.setRequestMethod('POST');
        svc = svc.addHeader('x-api-key', OMSServiceConfig.API_KEY);
        svc = svc.addHeader('x-apigw-api-id', OMSServiceConfig.API_KEY_GW);
        return JSON.stringify(args);
      },

      parseResponse: function (svc, client) {
        return client.text;
      },
      filterLogMessage: function (msg) {
        return msg.replace('headers', 'OFFWITHTHEHEADERS');
      },
      getRequestLogMessage: function (reqObj) {
        return reqObj;
      },
      getResponseLogMessage: function (respObj) {
        return respObj.text;
      },

      mockFull: function (service) {
        var mockRes = require('*/cartridge/scripts/mocks/mock.oms.api.order.list');
        return JSON.stringify(mockRes);
      },

      mockCall: function (svc, client) {
        var mockedReponse = {
          Output: {
            OrderList: {
              TotalOrderList: 0
            }
          },
          ResponseCode: '0',
          ResposneMessage: 'Success'
        };
        return {
          statusCode: 200,
          statusMessage: 'Success',
          text: JSON.stringify(mockedReponse)
        };
      }
    });
  }
};
