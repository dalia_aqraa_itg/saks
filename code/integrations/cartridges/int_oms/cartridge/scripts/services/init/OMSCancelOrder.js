var Site = require('dw/system/Site');
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var OMSServices = require('*/cartridge/scripts/services/OMSServices');
var OMSServiceConfig = require('*/cartridge/scripts/config/OMSServiceConfig');
var OMSLogger = dw.system.Logger.getLogger('OMS', 'OMSService');

/**
 * Service initialization
 */
module.exports = {
  getInstance: function () {
    return LocalServiceRegistry.createService(OMSServices.OMS_CANCEL_ORDER, {
      createRequest: function (svc, args) {
        svc = svc.addHeader('Content-Type', 'application/json');
        svc = svc.setRequestMethod('POST');
        svc = svc.addHeader('x-api-key', OMSServiceConfig.API_KEY);
        svc = svc.addHeader('x-apigw-api-id', OMSServiceConfig.API_KEY_GW);
        return JSON.stringify(args);
      },

      parseResponse: function (svc, client) {
        return client.text;
      },
      filterLogMessage: function (msg) {
        return msg.replace('headers', 'OFFWITHTHEHEADERS');
      },
      getRequestLogMessage: function (reqObj) {
        return reqObj;
      },
      getResponseLogMessage: function (respObj) {
        return respObj.text;
      },

      mockCall: function (svc, client) {
        var mockedReponse = {
          Notes: {
            NumberOfNotes: '1',
            Note: [
              {
                ContactUser: 'H895462',
                NoteText: 'Customer Buyer Remorse'
              }
            ]
          },
          OrderNo: '182001805',
          EnterpriseCode: 'OFF5',
          OrderLines: {
            OrderLine: [
              {
                Notes: {
                  NumberOfNotes: '0'
                },
                PrimeLineNo: '1',
                MaxLineStatusDesc: 'Cancelled',
                Item: {
                  ItemID: '92507025',
                  UnitOfMeasure: 'EACH'
                },
                OrderedQty: '0'
              }
            ]
          },
          MaxOrderStatusDesc: 'Cancelled',
          MaxOrderStatus: '9000',
          DocumentType: '0001'
        };
        return {
          statusCode: 200,
          statusMessage: 'Success',
          text: JSON.stringify(mockedReponse)
        };
      }
    });
  }
};
