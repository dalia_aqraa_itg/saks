var Site = require('dw/system/Site');
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var OMSServices = require('*/cartridge/scripts/services/OMSServices');
var OMSServiceConfig = require('*/cartridge/scripts/config/OMSServiceConfig');

/**
 * Service initialization
 */
module.exports = {
  getInstance: function () {
    return LocalServiceRegistry.createService(OMSServices.OMS_ORDER_DETAILS, {
      createRequest: function (svc, args) {
        svc = svc.addHeader('Content-Type', 'application/json');
        svc = svc.setRequestMethod('POST');
        svc = svc.addHeader('x-api-key', OMSServiceConfig.API_KEY);
        svc = svc.addHeader('x-apigw-api-id', OMSServiceConfig.API_KEY_GW);
        return JSON.stringify(args);
      },

      parseResponse: function (svc, client) {
        return client.text;
      },
      filterLogMessage: function (msg) {
        return msg.replace('headers', 'OFFWITHTHEHEADERS');
      },
      getRequestLogMessage: function (reqObj) {
        return reqObj;
      },

      getResponseLogMessage: function (respObj) {
        return respObj.text;
      },

      mockCall: function (svc, client) {
        var mockedReponse = {
          OrderDate: '2019-12-18T16:34:42-05:00',
          Promotions: {},
          PaymentMethods: {
            PaymentMethod: [
              {
                UnlimitedCharges: 'N',
                TotalRefundedAmount: '0.00',
                RequestedChargeAmount: '0.00',
                DisplaySvcNo: '8453',
                RequestedAuthAmount: '0.00',
                TotalAuthorized: '0.00',
                MaxChargeLimit: '74.01',
                PaymentType: 'GIFT_CARD',
                TotalCharged: '74.01',
                SvcNo: '6018010000018453',
                CreditCardName: '',
                CreditCardType: '',
                PersonInfoBillTo: {
                  EMailID: 'roman.kalyuzhny@hhc.com',
                  MiddleName: '',
                  State: 'NY',
                  FirstName: 'Ro',
                  LastName: 'Man',
                  DayPhone: '9172225555',
                  Country: 'US',
                  ZipCode: '10281',
                  AddressLine1: '225 Liberty St, 26th Floor',
                  City: 'new york',
                  AddressLine2: ''
                },
                ChargeSequence: '0',
                DisplayCreditCardNo: ''
              }
            ]
          },
          OrderDates: {},
          Extn: {
            HbcOrderHeaderChargesList: {},
            ExtnIsFiftyOne: 'N',
            HbcHeadertaxBreakupList: {},
            ExtnReference1: 'UOP'
          },
          PersonInfoShipTo: {
            EMailID: 'roman.kalyuzhny@hhc.com',
            MiddleName: '',
            State: 'NY',
            FirstName: 'Ro',
            MobilePhone: '',
            LastName: 'Man',
            Country: 'US',
            ZipCode: '10281',
            AddressLine1: '225 Liberty St, 26th Floor',
            City: 'new york',
            AddressLine2: ''
          },
          AdditionalAddresses: {},
          HeaderTaxes: {},
          Status: 'Shipped',
          OriginalTax: '0.00',
          OrderNo: '182000442',
          EnterpriseCode: 'OFF5',
          OverallTotals: {
            SubtotalWithoutTaxes: '67.98',
            GrandTax: '6.03',
            LineSubTotal: '59.99',
            GrandShippingTotal: '7.99',
            GrandTotal: '74.01'
          },
          OrderLines: {
            OrderLine: [
              {
                SCAC: 'FEDX',
                LinePriceInfo: {
                  UnitPrice: '59.99'
                },
                LineCharges: {
                  LineCharge: [
                    {
                      ChargeAmount: '7.99',
                      ChargeName: 'SHIPPINGCHARGE',
                      ChargeCategory: 'SHIPPINGCHARGE'
                    }
                  ]
                },
                OriginalOrderedQty: '1',
                ItemDetails: {
                  ItemID: '92507026',
                  Createprogid: 'HBCItemLoadIntServer',
                  IsShippingCntr: 'N',
                  Createuserid: 'HBCItemLoadIntServer',
                  Modifyuserid: 'HBCItemLoadIntServer',
                  Modifyprogid: 'HBCItemLoadIntServer',
                  ItemKey: '20180713104056355430903',
                  ItemLocaleList: {
                    ItemLocale: [
                      {
                        Language: 'en',
                        Country: 'US',
                        PrimaryInformation: {
                          ShortDescription: 'Drawstring Jogger Pants',
                          Description: 'CLSC LGO JGR SWTPNT'
                        }
                      }
                    ]
                  },
                  OrganizationCode: 'OFF5',
                  ItemGroupCode: 'PROD',
                  Createts: '2018-07-13T10:40:56-04:00',
                  Modifyts: '2019-11-27T22:17:09-05:00',
                  ItemAliasList: {
                    ItemAlias: [
                      {
                        AliasName: 'ACTIVE_UPC',
                        AliasValue: '889347561715'
                      }
                    ]
                  },
                  AdditionalAttributeList: {
                    AdditionalAttribute: [
                      {
                        Name: 'Color',
                        AttributeDomainID: 'ItemAttribute',
                        Value: 'BLACK',
                        AttributeGroupID: 'Style Variation'
                      },
                      {
                        Name: 'Size',
                        AttributeDomainID: 'ItemAttribute',
                        Value: 'M',
                        AttributeGroupID: 'Style Variation'
                      }
                    ]
                  },
                  InheritAttributesFromClassification: 'Y',
                  UnitOfMeasure: 'EACH',
                  DisplayItemId: '92507026',
                  MaxModifyTS: '2019-11-27T22:17:09-05:00',
                  CanUseAsServiceTool: 'N',
                  Lockid: '4',
                  GlobalItemID: '',
                  PrimaryInformation: {
                    ManufacturerName: 'True Religion',
                    ImageID: '0400092507015_BLACK_247x329.jpg',
                    ImageLocation: 'https://image.s5a.com/is/image/saksoff5th/'
                  }
                },
                OrderDates: {
                  OrderDate: [
                    {
                      ActualDate: '2019-12-23T11:34:45-05:00',
                      OrderHeaderKey: '20191218113445484070949',
                      OrderReleaseKey: '',
                      OrderLineKey: '20191218113445484070952',
                      DateTypeId: 'HBC_ORDER_LINE_CANCEL_DATE'
                    },
                    {
                      ActualDate: '2019-12-30T07:24:05-05:00',
                      OrderHeaderKey: '20191218113445484070949',
                      OrderReleaseKey: '',
                      OrderLineKey: '20191218113445484070952',
                      DateTypeId: 'ORD_LINE_EXP_DEL_DATE'
                    },
                    {
                      ActualDate: '2019-12-18T11:46:38-05:00',
                      OrderHeaderKey: '20191218113445484070949',
                      ExpectedDate: '2019-12-18T11:46:38-05:00',
                      CommittedDate: '2019-12-18T11:46:38-05:00',
                      OrderReleaseKey: '',
                      OrderLineKey: '20191218113445484070952',
                      RequestedDate: '2019-12-18T11:46:38-05:00',
                      DateTypeId: 'YCD_COMPLETELY_SHIPPED_OR_CANCELLED'
                    }
                  ]
                },
                Extn: {
                  HbcOrderLineChargesList: {},
                  ExtnColor: 'BLACK',
                  ExtnSize: 'M',
                  HbcLinetaxBreakupList: {}
                },
                OrderedQty: '1',
                ShipNode: '7776',
                GiftFlag: 'N',
                PersonInfoShipTo: {
                  EMailID: 'roman.kalyuzhny@hhc.com',
                  MiddleName: '',
                  State: 'NY',
                  FirstName: 'Ro',
                  MobilePhone: '',
                  LastName: 'Man',
                  Country: 'US',
                  ZipCode: '10281',
                  AddressLine1: '225 Liberty St, 26th Floor',
                  City: 'new york',
                  AddressLine2: ''
                },
                LineType: 'REGULAR',
                LineTaxes: {
                  LineTax: [
                    {
                      ChargeName: 'PRICE',
                      ChargeCategory: 'PRICE',
                      TaxPercentage: '0.08875',
                      Tax: '5.32',
                      TaxName: 'General Sales and Use Tax'
                    },
                    {
                      ChargeName: 'SHIPPINGCHARGE',
                      ChargeCategory: 'SHIPPINGCHARGE',
                      TaxPercentage: '0.08875',
                      Tax: '0.71',
                      TaxName: 'General Sales and Use Tax'
                    }
                  ]
                },
                CarrierServiceCode: 'Standard (AK&HI)',
                Status: 'Shipped',
                Item: {
                  ItemID: '92507026',
                  ItemDesc: 'CLSC LGO JGR SWTPNT',
                  CustomerItemDesc: 'Drawstring Jogger Pants',
                  ItemShortDesc: 'Drawstring Jogger Pants'
                },
                Instructions: {
                  NumberOfInstructions: '0'
                },
                DeliveryMethod: 'SHP',
                Shipnode: {
                  Localecode: 'en_US_EST',
                  NodeType: 'Store',
                  ShipnodeType: ''
                }
              }
            ]
          },
          DocumentType: '0001',
          PersonInfoBillTo: {
            EMailID: 'roman.kalyuzhny@hhc.com',
            MiddleName: '',
            State: 'NY',
            FirstName: 'Ro',
            MobilePhone: '',
            LastName: 'Man',
            Country: 'US',
            ZipCode: '10281',
            AddressLine1: '225 Liberty St, 26th Floor',
            City: 'new york',
            AddressLine2: ''
          },
          Shipments: {
            Shipment: [
              {
                CarrierServiceCode: '2 Day',
                Containers: {
                  Container: [
                    {
                      CarrierServiceCode: '2 Day',
                      SCAC: 'FEDX',
                      ContainerNo: '100018793',
                      ContainerDetails: {
                        TotalNumberOfRecords: '1',
                        ContainerDetail: [
                          {
                            ItemID: '92507026',
                            ShipmentLine: {
                              ItemID: '92507026',
                              Quantity: '1',
                              PrimeLineNo: '1',
                              SubLineNo: '1'
                            },
                            Quantity: '1',
                            UnitOfMeasure: 'EACH'
                          }
                        ]
                      },
                      URL: 'https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber=794675452278',
                      TrackingNo: '794675452278'
                    }
                  ]
                },
                SCAC: 'FEDX',
                ExpectedShipmentDate: '2019-12-18T00:00:00-05:00',
                ExpectedDeliveryDate: '2019-12-18T11:39:23-05:00',
                DeliveryMethod: 'SHP'
              }
            ]
          },
          HeaderCharges: {},
          OriginalTotalAmount: '74.01',
          ResponseCode: '0',
          ResposneMessage: 'Sucess'
        };
        return {
          statusCode: 200,
          statusMessage: 'Success',
          text: JSON.stringify(mockedReponse)
        };
      }
    });
  }
};
