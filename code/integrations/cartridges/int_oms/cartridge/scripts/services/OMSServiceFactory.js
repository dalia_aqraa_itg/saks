'use strict';

const OMSServices = require('*/cartridge/scripts/services/OMSServices');
const OMS_OrderDetails = require('*/cartridge/scripts/services/init/OMSOrderDetails');
const OMS_OrderList = require('*/cartridge/scripts/services/init/OMSOrderList');
const OMS_CancelOrder = require('*/cartridge/scripts/services/init/OMSCancelOrder');

/**
 * Using the serviceName return an instance of the Service.
 *
 * @param serviceName
 * @returns Instance of Service
 */
function _getServiceBuilder(serviceName) {
  switch (serviceName) {
    case OMSServices.OMS_ORDER_LIST:
      return OMS_OrderList;
    case OMSServices.OMS_ORDER_DETAILS:
      return OMS_OrderDetails;
    case OMSServices.OMS_CANCEL_ORDER:
      return OMS_CancelOrder;
    default:
      throw new Error('Error initializing the service: ' + serviceName);
  }
}

exports.getInstance = function (serviceName) {
  const builder = _getServiceBuilder(serviceName);
  return builder.getInstance();
};
