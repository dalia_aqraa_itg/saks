'use strict';

var Site = require('dw/system/Site');
var OMSLogger = dw.system.Logger.getLogger('OMS', 'OMSService');
var OMSServices = require('*/cartridge/scripts/services/OMSServices');
var OMSServiceFactory = require('*/cartridge/scripts/services/OMSServiceFactory');

/**
 * Calls OrderList service in OMS
 * @param orderDetailsRequest
 * @returns
 */
function callGetOrderDetails(orderDetailsRequest) {
  var service = OMSServiceFactory.getInstance(OMSServices.OMS_ORDER_DETAILS);
  var response = service.call(orderDetailsRequest);
  if (response && response.error === 0 && response.status === 'OK') {
    return {
      success: true,
      result: response.object
    };
  } else {
    OMSLogger.error('ERROR: There was an error with the OrderDetails service call:' + response.errorMessage);
  }

  return {
    success: false
  };
}

/**
 * Calls OrderHistory service in OMS
 * @param orderListRequest
 * @returns
 */
function callGetOrderHistory(orderListRequest) {
  var service = OMSServiceFactory.getInstance(OMSServices.OMS_ORDER_LIST);
  var response = service.call(orderListRequest);

  if (response && response.error === 0 && response.status === 'OK') {
    var resultObj = JSON.parse(response.object);
    if (!empty(resultObj) && !empty(resultObj.ResponseCode)) {
      if (resultObj.ResponseCode === '0') {
        return {
          success: true,
          result: response.object
        };
      } else {
        return {
          success: false
        };
      }
    } else {
      return {
        success: false
      };
    }
  } else {
    OMSLogger.error('ERROR: There was an error with the OrderList service call:' + response.errorMessage);
    return {
      success: false
    };
  }
}

/**
 * Calls CancelOrder service in OMS
 * @param cancelOrderRequest
 * @returns
 */
function callCancelOrder(cancelOrderRequest) {
  var service = OMSServiceFactory.getInstance(OMSServices.OMS_CANCEL_ORDER);
  var response = service.call(cancelOrderRequest);

  if (response && response.error === 0 && response.status === 'OK') {
    var resultObj = JSON.parse(response.object);
    if (!empty(resultObj)) {
      return {
        success: true,
        result: response.object
      };
    } else {
      return {
        success: false
      };
    }
  } else {
    OMSLogger.error('ERROR: There was an error with the CancelOrder service call:' + response.errorMessage);
    return {
      success: false
    };
  }
}

module.exports = {
  callGetOrderDetails: callGetOrderDetails,
  callGetOrderHistory: callGetOrderHistory,
  callCancelOrder: callCancelOrder
};
