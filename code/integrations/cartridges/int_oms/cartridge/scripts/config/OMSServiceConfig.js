var Site = require('dw/system/Site');
var CurrentSite = Site.current.preferences;
var preferences = CurrentSite ? CurrentSite.custom : {};
var omsPreferences = 'omsOrderPreferences' in preferences && preferences.omsOrderPreferences ? JSON.parse(preferences.omsOrderPreferences) : '';
/**
 * Constants
 */

const constants = {
  OMS_ENABLED: ('OMSEnabled' in preferences && preferences.OMSEnabled) || false,
  OMS_RemovedZeroPrefix: ('OMSRemovedZeroPrefix' in preferences && preferences.OMSRemovedZeroPrefix) || false,
  API_KEY: preferences.hbcAPIKey,
  API_KEY_GW: preferences.hbcGWAPIKey,
  BANNER_CODE: 'banner' in preferences && preferences.banner ? preferences.banner : 'OFF5',
  ORDER_TRACKING_URL: preferences.orderTrackingURL,
  SERVICE_PAGINATION_STRATEGY: 'GENERIC',
  SERVICE_APPLY_QUERY_TIMEOUT: 'Y',
  SERVICE_LATEST_FIRST: 'Y',
  SERVICE_DOCUMENT_TYPE: '0001',
  SERVICE_DRAFT_ORDER_FLAG: 'omsDraftOrderFlag' in preferences ? preferences.omsDraftOrderFlag : 'N',
  SERVICE_INVOKED_FROM_ONLINE: 'Online',
  SERVICE_IGNORE_ORDERING: 'N',
  SERVICE_ORDER_DATE_QRY_TYPE: 'DATERANGE',
  SERVICE_QRY_TIMEOUT: '300',
  SERVICE_READ_FROM_HISTORY_DEFAULT: 'OMSReadFromHistory' in preferences ? preferences.OMSReadFromHistory : 'B',
  SERVICE_ORDERBY_ATTRIBUTE_DESC: 'Y',
  SERVICE_ORDERBY_ATTRIBUTE_NAME: 'OrderDate',
  SERVICE_ORDER_HISTORY_PAGESIZE: 'pageSize' in preferences && preferences.pageSize ? preferences.pageSize : 10,
  OMS_ORDER_DETAILS_INVOKEDFROM: omsPreferences != null && omsPreferences.InvokedFrom ? omsPreferences.InvokedFrom : 'Online',
  OMS_ORDER_DETAILS_DROPSHIP: omsPreferences != null && omsPreferences.DROP_SHIP ? omsPreferences.DROP_SHIP : [],
  OMS_ORDER_DETAILS_STATUS: 'omsOrderLineStatus' in preferences && preferences.omsOrderLineStatus ? JSON.parse(preferences.omsOrderLineStatus) : {},
  SFCC_PAYMENT_TYPE_GIFT_CARD: 'GIFT_CARD',
  OMS_CREDIT_CARD_TYPE_MAPPING: 'OMSCreditCardType' in preferences ? JSON.parse(preferences.OMSCreditCardType) : {},
  OMS_LINE_ITEM_STATUS_CANCELLED: 'Cancelled',
  OMS_ORDERLINE_DROP_SHIP: 'DROPSHIP',
  OMS_ORDERLINE_PRE_ORDER: 'PREORDER',
  OMS_ORDERLINE_TAX_CATEGORY_ECO_FEE: 'ECOFEE',
  OMS_ORDERLINE_TAX_CATEGORY_SHIPPING_SURCHARGE: 'SHIPPINGSURCHARGE',
  OMS_ORDERLINE_TAX_CATEGORY_PRICE: 'PRICE',
  OMS_ORDERLINE_TAX_CATEGORY_SHIPPING_CHARGE: 'SHIPPINGCHARGE',
  OMS_ORDERLINE_TAX_CATEGORY_Price: 'Price',
  OMS_ORDERLINE_CHARGE_CATEGORY_SHIPPING_CHARGE: 'SHIPPINGCHARGE',
  OMS_ORDERLINE_CHARGE_CATEGORY_DISCOUNT: 'DISCOUNT',

  OMS_ORDERLINE_TAX_NAME_GST_HST: 'GST/HST',
  OMS_ORDERLINE_TAX_NAME_QST: 'Quebec Sales Tax (QST)',
  OMS_ORDERLINE_TAX_NAME_QST_VAT: 'Quebec Sales Tax (VAT)',
  OMS_ORDERLINE_TAX_NAME_GENERAL_SALES_AND_USE_TAX: 'General Sales and Use Tax',
  OMS_ORDERLINE_TAX_NAME_PST: 'Provincial Sales Tax (PST)',
  OMS_ORDERLINE_TAX_NAME_RST: 'Retail Sales Tax (RST)',

  OMS_SHIPMENT_DELIVERY_METHOD_STORE_PICKUP: 'PICK',
  OMS_SHIPMENT_DELIVERY_METHOD_SHITP_TO_HOME: 'SHP',
  OMS_SHIPMENT_ESTIMATED_DELIVERY_DATE_TYPE: 'ORD_LINE_EXP_DEL_DATE',
  SFCC_PAYMENT_TYPE_CREDIT_CARD: 'Credit Card',
  SFCC_ORDER_HISTORY_PERIOD: -1,

  OMS_CANCEL_ORDER_INVOKEDFROM: 'Online',
  OMS_CANCEL_ORDER_ACTION: 'CANCEL',
  OMS_CANCEL_MODIFICATION_REASON_CODE:
    'modificationReasonCode' in preferences && preferences.modificationReasonCode ? preferences.modificationReasonCode : 'HBC_MC_CC_009',
  OMS_CANCEL_MODIFICATION_REASON_TEXT:
    'modificationReasonText' in preferences && preferences.modificationReasonText ? preferences.modificationReasonText : 'Customer Buyer Remorse',
  OMS_CANCEL_NOTES_NOTE_TEXT: 'noteText' in preferences && preferences.noteText ? preferences.noteText : 'Customer Buyer Remorse',
  OMS_CANCEL_NOTES_REASON_CODE: 'noteReasonCode' in preferences && preferences.noteReasonCode ? preferences.noteReasonCode : 'HBC_MC_CC_009',
  OMS_CANCEL_CANTACT_USER_GUEST: 'Guest User',
  OMS_CARRIER_CODE_MAPPING:
    'carrierCodeShippingMethodNameMapping' in preferences && preferences.carrierCodeShippingMethodNameMapping
      ? JSON.parse(preferences.carrierCodeShippingMethodNameMapping)
      : {}
};
module.exports = constants;
