'use strict';
var Logger = require('dw/system/Logger');
var Calendar = require('dw/util/Calendar');
var OMSServiceConfig = require('*/cartridge/scripts/config/OMSServiceConfig');
var OrderHistoryServiceUtil = require('*/cartridge/scripts/services/utils/OMSServiceUtils');
var OrderHistoryModel = require('*/cartridge/models/OrderHistory');
var OMSLogger = Logger.getLogger('OMS', 'OMSService');

/**
 * Build request for order history
 *
 * @param {string} customerId - Current customer ID
 * @param {Object} orderHistoryRequest - Order history request body object
 * @returns {Object} - order history list request object
 */
function getOrderListRequest(customerId, orderHistoryRequest) {
  var request = {};
  var orderInput = {};
  orderInput.BillToID = customerId;
  orderInput.DocumentType = orderHistoryRequest.OrderType ? orderHistoryRequest.OrderType : OMSServiceConfig.SERVICE_DOCUMENT_TYPE;
  orderInput.DraftOrderFlag = OMSServiceConfig.SERVICE_DRAFT_ORDER_FLAG;
  orderInput.LatestFirst = orderHistoryRequest.LatestFirst ? orderHistoryRequest.LatestFirst : OMSServiceConfig.SERVICE_LATEST_FIRST;
  // adding pagination for storefront
  if (orderHistoryRequest.pageNumber) {
    orderInput.PageInput = {
      PageNumber: orderHistoryRequest.pageNumber
    };
  }
  if (orderHistoryRequest.storefront) {
    orderInput.PageInput = {
      PageSize: OMSServiceConfig.SERVICE_ORDER_HISTORY_PAGESIZE.toString()
    };
  }
  if (orderHistoryRequest.FromOrderDate) {
    orderInput.FromOrderDate = orderHistoryRequest.FromOrderDate;
  }
  if (orderHistoryRequest.ToOrderDate) {
    orderInput.ToOrderDate = orderHistoryRequest.ToOrderDate;
  }
  orderInput.ReadFromHistory =
    orderHistoryRequest && orderHistoryRequest.ReadFromHistory ? orderHistoryRequest.ReadFromHistory : OMSServiceConfig.SERVICE_READ_FROM_HISTORY_DEFAULT;
  orderInput.EnterpriseCode = orderHistoryRequest.BusinessUnit ? orderHistoryRequest.BusinessUnit : OMSServiceConfig.BANNER_CODE;
  request.Order = orderInput;
  return request;
}

/**
 * Build paging request for order history
 *
 * @param {string} customerId - Current customer ID
 * @param {Object} orderHistoryRequest - Order history request body object
 * @returns {Object} - order history list request object
 */
function getOrderListPageRequest(customerId, orderHistoryRequest) {
  var pagingRequest = orderHistoryRequest.PageInput ? orderHistoryRequest.PageInput : '';

  var request = {};
  var pageInput = {};
  pageInput.PageNumber = pagingRequest.PageNumber;
  pageInput.PageSize = pagingRequest.PageSize || OMSServiceConfig.SERVICE_ORDER_HISTORY_PAGESIZE.toString();
  pageInput.PaginationStrategy = OMSServiceConfig.SERVICE_PAGINATION_STRATEGY;

  var previousPageRequest = {};
  previousPageRequest.PageNumber = pagingRequest.PageNumber > 0 ? pagingRequest.PageNumber - 1 : '';
  var prevOrder = {};
  prevOrder.OrderHeaderKey = pagingRequest.OrderHeaderKey ? pagingRequest.OrderHeaderKey : '';
  previousPageRequest.Order = prevOrder;
  pageInput.PreviousPage = previousPageRequest;

  var orderInputPage = {};
  orderInputPage.ApplyQueryTimeout = OMSServiceConfig.SERVICE_APPLY_QUERY_TIMEOUT;
  orderInputPage.BillToID = customerId;
  orderInputPage.DocumentType = orderHistoryRequest.OrderType ? orderHistoryRequest.OrderType : OMSServiceConfig.SERVICE_DOCUMENT_TYPE;
  orderInputPage.DraftOrderFlag = OMSServiceConfig.SERVICE_DRAFT_ORDER_FLAG;
  orderInputPage.IgnoreOrdering = OMSServiceConfig.SERVICE_IGNORE_ORDERING;
  orderInputPage.OrderDateQryType = OMSServiceConfig.SERVICE_ORDER_DATE_QRY_TYPE;
  orderInputPage.QueryTimeout = OMSServiceConfig.SERVICE_QRY_TIMEOUT;
  orderInputPage.ReadFromHistory = orderHistoryRequest.ReadFromHistory
    ? orderHistoryRequest.ReadFromHistory
    : OMSServiceConfig.SERVICE_READ_FROM_HISTORY_DEFAULT;
  orderInputPage.EnterpriseCode = orderHistoryRequest.BusinessUnit ? orderHistoryRequest.BusinessUnit : OMSServiceConfig.BANNER_CODE;
  orderInputPage.LatestFirst = orderHistoryRequest.LatestFirst ? orderHistoryRequest.LatestFirst : OMSServiceConfig.SERVICE_LATEST_FIRST;
  if (orderHistoryRequest.FromOrderDate) {
    orderInputPage.FromOrderDate = orderHistoryRequest.FromOrderDate;
  }
  if (orderHistoryRequest.ToOrderDate) {
    orderInputPage.ToOrderDate = orderHistoryRequest.ToOrderDate;
  }

  var orderByObject = {};
  var attributeObject = {};
  attributeObject.Desc = OMSServiceConfig.SERVICE_ORDERBY_ATTRIBUTE_DESC;
  attributeObject.Name = OMSServiceConfig.SERVICE_ORDERBY_ATTRIBUTE_NAME;
  orderByObject.Attribute = attributeObject;
  orderInputPage.OrderBy = orderByObject;
  pageInput.Order = orderInputPage;
  request.Page = pageInput;
  return request;
}

/**
 * Get orders from SFCC which are not exported to OMS.
 *
 * @param {string} customerNo - customer number
 * @returns {Object} - SFCC customer order list Object
 */
function getOrderAtSFCC(customerNo) {
  var orderList = [];
  var OrderMgr = require('dw/order/OrderMgr');
  var Order = require('dw/order/Order');
  var StringUtils = require('dw/util/StringUtils');
  var Calendar = require('dw/util/Calendar');
  // Query orders that have not been exported to OMS from SFCC (look for the
  // past 1 day only)
  var oldCalendar = new Calendar();
  oldCalendar.add(Calendar.DAY_OF_YEAR, OMSServiceConfig.SFCC_ORDER_HISTORY_PERIOD);
  var orders = OrderMgr.searchOrders(
    'customerNo={0} AND (NOT exportStatus={1}) AND (NOT status ={2}) AND (NOT status ={3}) AND creationDate > {4}',
    'creationDate desc',
    customerNo,
    Order.EXPORT_STATUS_EXPORTED,
    Order.ORDER_STATUS_FAILED,
    Order.ORDER_STATUS_CREATED,
    oldCalendar.time
  );

  while (orders.hasNext()) {
    var order = orders.next();
    var orderObject = {};
    if (empty(order.custom.bfxOrderId)) {
      orderObject.sfccOrder = true;
      orderObject.orderNumber = order.orderNo;
      orderObject.creationDate = order.creationDate; // eslint-disable-line
      orderObject.orderTotal = order.totalGrossPrice.value;
      orderObject.orderTotalCurrency = order.currencyCode;
      orderObject.status = order.status ? getWebStatus(order.status) : '';
      orderList.push(orderObject);
    }
  }
  return orderList;
}

/**
 * Get customer Id (BillToId) to be sent to OMS
 *
 * @param customer
 * @param banner
 * @returns
 */
function getCustomerId(customer, banner) {
  var billToId = '';

  switch (banner) {
    case 'BAY':
      if (customer.profile && customer.profile.customerNo) {
        var profile = dw.customer.CustomerMgr.getProfile(customer.profile.customerNo);
        if (profile && profile.credentials) {
          billToId = profile.credentials.login;
        }
      }
      if (!billToId && customer.profile) {
        billToId = customer.profile.email;
      }
      break;
    default:
      if (customer.profile && customer.profile.customerNo) {
        billToId = customer.profile.customerNo;
      }
  }
  return billToId;
}

function validateRequest(request) {
  try {
    var orderRequest = JSON.parse(request);
    if (orderRequest.OrderHistoryRequest && orderRequest.OrderHistoryRequest.ReadFromHistory) {
      return {
        valid: true,
        orderHistoryRequest: orderRequest.OrderHistoryRequest
      };
    }
    return {
      valid: false,
      message: 'Check the format of order history request, the right format is { "OrderHistoryRequest" : {"ReadFromHistory" : "< B/N>"}}'
    };
  } catch (ex) {
    OMSLogger.error('[OMS] Error : {0} {1}', ex, ex.stack);
    return {
      valid: false,
      message:
        'Error in parsing order details request, check the format of order history request, the right format is { "OrderHistoryRequest" : {"ReadFromHistory" : "< B/N>"}}'
    };
  }
}

/**
 * fetch order history from OMS and build the data object for client (Storefront
 * or API)
 *
 * @param customer
 * @param requestBody
 * @param locale
 * @returns
 */
function getOrderHistory(customer, orderHistoryRequest, locale) {
  var orderHistoryResponse = {};
  // get orders from SFCC
  var orderHistory = getOrderAtSFCC(customer.profile.customerNo);
  var orderListRequest = null;
  var businessUnit = orderHistoryRequest.BusinessUnit ? orderHistoryRequest.BusinessUnit : OMSServiceConfig.BANNER_CODE;
  var customerId = getCustomerId(customer, businessUnit);
  if (orderHistoryRequest.PageInput) {
    orderListRequest = getOrderListPageRequest(customerId, orderHistoryRequest, locale);
  } else {
    orderListRequest = getOrderListRequest(customerId, orderHistoryRequest, locale);
  }

  try {
    // get orders from OMS
    var serviceReponse = OrderHistoryServiceUtil.callGetOrderHistory(orderListRequest);
    if (serviceReponse.success) {
      var orderList = null;
      var resultObj = JSON.parse(serviceReponse.result);
      if (resultObj.Order) {
        orderList = resultObj.Order;
      } else {
        orderList = resultObj.Output && resultObj.Output.OrderList && resultObj.Output.OrderList.Order ? resultObj.Output.OrderList.Order : [];
      }
      if (orderList.length > 0) {
        var ordersCount = orderList.length;
        var omsOrderHeaderKey = orderList[ordersCount - 1].OrderHeaderKey;
        var ordersFromOMS = new OrderHistoryModel(orderList).orders;
        // combine orders from OMS and SFCC
        orderHistory = orderHistory.concat(ordersFromOMS);
        /*if (resultObj.LastOrderHeaderKey) {
                    orderHistoryResponse.lastOrderHeaderKey = resultObj.LastOrderHeaderKey;
                } else if (resultObj.Output && resultObj.Output.OrderList && resultObj.Output.OrderList.LastOrderHeaderKey) {
                    orderHistoryResponse.lastOrderHeaderKey = resultObj.Output.OrderList.LastOrderHeaderKey;
                }*/
        orderHistoryResponse.lastOrderHeaderKey = omsOrderHeaderKey;
        orderHistoryResponse.IsLastPage = resultObj.IsLastPage === 'Y';
        orderHistoryResponse.orders = orderHistory;
        orderHistoryResponse.historyStatus = 'MULTIPLE_ORDERS';
      } else if (orderHistory.length > 0) {
        orderHistoryResponse.orders = orderHistory;
        orderHistoryResponse.historyStatus = 'MULTIPLE_ORDERS';
      } else {
        orderHistoryResponse.historyStatus = 'NO_ORDERS';
      }
    } else if (orderHistory.length > 0) {
      orderHistoryResponse.orders = orderHistory;
      orderHistoryResponse.historyStatus = 'SFCC_ORDERS';
    } else {
      orderHistoryResponse.historyStatus = 'SERVICE_DOWN';
    }
  } catch (ex) {
    OMSLogger.error('[OMS] Error : {0} {1}', ex, ex.stack);
    if (orderHistory.length > 0) {
      orderHistoryResponse.orders = orderHistory;
      orderHistoryResponse.historyStatus = 'SFCC_ORDERS';
    } else {
      orderHistoryResponse.historyStatus = 'SERVICE_DOWN';
    }
  }

  return orderHistoryResponse;
}

/**
 * Function to return web friendly status
 *
 * @param {string} status - OMS status
 * @returns {string} - Web friendly status
 */
function getWebStatus(status) {
  var orderStatusPrefix = 'ORDER_STATUS_';
  let statusMapping = OMSServiceConfig.OMS_ORDER_DETAILS_STATUS;
  var status = dw.web.Resource.msg(statusMapping[status] ? orderStatusPrefix + statusMapping[status] : orderStatusPrefix + 'IN_PROGRESS', 'order', status);
  return status;
}

module.exports = {
  getOrderHistory: getOrderHistory,
  validateRequest: validateRequest
};
