'use strict';
var Logger = require('dw/system/Logger');
var OMSConstants = require('*/cartridge/scripts/config/OMSServiceConfig');
var OrderDetailsServiceUtil = require('*/cartridge/scripts/services/utils/OMSServiceUtils');
var OrderModel = require('*/cartridge/models/OrderDetails');
var OMSLogger = Logger.getLogger('OMS', 'OMSService');

/**
 * Create Request Object for OrderList service
 * @param {Object} orderDetailsRequest - Order request from controller
 * @returns {Object} request
 */
function getOrderDetailsRequest(orderDetailsRequest) {
  var request = {};
  var orderInput = {};
  orderInput.OrderNo = orderDetailsRequest.OrderNumber;
  orderInput.DocumentType = orderDetailsRequest.OrderType ? orderDetailsRequest.OrderType : OMSConstants.SERVICE_DOCUMENT_TYPE;

  orderInput.EnterpriseCode = orderDetailsRequest.BusinessUnit ? orderDetailsRequest.BusinessUnit : OMSConstants.BANNER_CODE;
  if (orderDetailsRequest.IsTrackingURLRequired) {
    orderInput.IsTrackURLReq = orderDetailsRequest.IsTrackingURLRequired;
  }
  if (orderDetailsRequest.BillToID) {
    orderInput.BillToID = orderDetailsRequest.BillToID;
  }
  if (orderDetailsRequest.ZipCode) {
    var personBillTo = {};
    personBillTo.ZipCode = orderDetailsRequest.ZipCode;
    orderInput.PersonInfoBillTo = personBillTo;
  }
  if (orderDetailsRequest.InvokedFrom) {
    orderInput.InvokedFrom = orderDetailsRequest.InvokedFrom;
  }
  request.Order = orderInput;
  return request;
}

/**
 * Create Request Object for OrderCancel service
 * @param {Object} orderCancelRequest - Order request from controller
 * @returns {Object} request
 */
function getOrderCancelRequest(orderCancelRequest) {
  var request = {};
  var orderInput = {},
    notes = {},
    note = {};
  orderInput.OrderNo = orderCancelRequest.OrderNumber;
  orderInput.InvokedFrom = OMSConstants.OMS_CANCEL_ORDER_INVOKEDFROM;
  orderInput.Action = OMSConstants.OMS_CANCEL_ORDER_ACTION;
  orderInput.EnterpriseCode = OMSConstants.BANNER_CODE;
  orderInput.DocumentType = OMSConstants.SERVICE_DOCUMENT_TYPE;
  orderInput.ModificationReasonCode = OMSConstants.OMS_CANCEL_MODIFICATION_REASON_CODE;
  orderInput.ModificationReasonText = OMSConstants.OMS_CANCEL_NOTES_NOTE_TEXT;
  note.NoteText = OMSConstants.OMS_CANCEL_NOTES_NOTE_TEXT;
  note.ContactUser = session.userAuthenticated ? session.userName : orderCancelRequest.userFullName;
  note.ReasonCode = OMSConstants.OMS_CANCEL_NOTES_REASON_CODE;
  notes.Note = note;
  orderInput.Notes = notes;
  request.Order = orderInput;
  return request;
}

/**
 * OrderDetails Helper function
 *
 * @param {Object} orderDetailsRequest - Order request from controller
 * @returns {Object} Order object from service and SFCC
 */
function getOrderDetails(orderDetailsRequest) {
  var orderInfo = null;
  try {
    var result = OrderDetailsServiceUtil.callGetOrderDetails(getOrderDetailsRequest(orderDetailsRequest));
    if (result.success) {
      var order = JSON.parse(result.result);
      var customerId = orderDetailsRequest.customerId ? orderDetailsRequest.customerId : '';
      if (order && order.OrderNo) {
        orderInfo = new OrderModel(order, customerId).order;
      }
    }
    if (!orderInfo) {
      var OrderMgr = require('dw/order/OrderMgr');
      var Order = require('dw/order/Order');
      var orderObj = OrderMgr.getOrder(orderDetailsRequest.OrderNumber);
      if (
        orderObj &&
        ((orderDetailsRequest.ZipCode &&
          orderObj.getBillingAddress() != null &&
          orderObj.getBillingAddress().postalCode &&
          orderObj.getBillingAddress().postalCode.equalsIgnoreCase(orderDetailsRequest.ZipCode)) ||
          orderDetailsRequest.BillToID)
      ) {
        if (orderObj.exportStatus.value !== Order.EXPORT_STATUS_EXPORTED && orderObj.exportStatus.value !== Order.ORDER_STATUS_FAILED) {
          var SFCCOrderModel = require('*/cartridge/models/order');
          var sfccOrderInfo = new SFCCOrderModel(orderObj, { containerView: 'order' });
          var OrderSFCCDetails = require('*/cartridge/models/OrderSFCCDetails');
          orderInfo = new OrderSFCCDetails(sfccOrderInfo).order;
          delete session.custom.orderDetailsStatus;
        } else {
          session.custom.orderDetailsStatus = 'SERVICE_DOWN';
        }
      } else {
        session.custom.orderDetailsStatus = 'NO_ORDER';
      }
      OMSLogger.debug('SFCC Order JSON : {0}', JSON.stringify(orderInfo));
    }
  } catch (ex) {
    OMSLogger.error('[OMS] Error : {0} {1}', ex, ex.stack);
  }

  return orderInfo;
}

/**
 * OrderDetails Helper function
 *
 * @param {Object} orderCancelRequest - Order request from controller
 * @returns {Object} Order object from service and SFCC
 */
function getOrderCancelStatus(orderCancelRequest) {
  var orderCancelled = false;
  var dwError = false,
    omsError = false;
  try {
    var result = OrderDetailsServiceUtil.callCancelOrder(getOrderCancelRequest(orderCancelRequest));
    if (result.success) {
      var orderCancelResponse = JSON.parse(result.result);
      var OrderMgr = require('dw/order/OrderMgr');
      var Transaction = require('dw/system/Transaction');
      var orderObj = OrderMgr.getOrder(orderCancelRequest.OrderNumber);
      if (orderCancelResponse && orderCancelResponse.ResponseCode === '0') {
        if (
          orderCancelResponse.MaxOrderStatus &&
          orderCancelResponse.MaxOrderStatusDesc &&
          orderCancelResponse.MaxOrderStatus === '9000' &&
          orderCancelResponse.MaxOrderStatusDesc === 'Cancelled'
        ) {
          Transaction.wrap(function () {
            // cancel SFCC order
            var cancelStatus = OrderMgr.cancelOrder(orderObj);
            if (cancelStatus && cancelStatus.code === 'OK') {
              let obj = {};
              obj.MaxOrderStatusDesc = orderCancelResponse.MaxOrderStatusDesc;
              obj.MaxOrderStatus = orderCancelResponse.MaxOrderStatus;
              orderObj.trackOrderChange('OMS Response : ' + JSON.stringify(obj));
              orderCancelled = true;
            } else {
              dwError = true;
            }
          });
        } else {
          omsError = true;
        }
      } else {
        Transaction.wrap(function () {
          orderObj.trackOrderChange('Order Cancel : Failed');
          orderObj.trackOrderChange('OMS Response : ' + JSON.stringify(orderCancelResponse));
        });
        omsError = true;
      }
    } else {
      omsError = true;
    }
  } catch (ex) {
    OMSLogger.error('[OMS] Error : {0} {1}', ex, ex.stack);
    dwError = true;
    return {
      OrderCancelled: orderCancelled,
      OmsError: omsError,
      DwError: dwError
    };
  }

  return {
    OrderCancelled: orderCancelled,
    OmsError: omsError,
    DwError: dwError
  };
}

/**
 * validate the incoming request from storefront
 *
 * @param {Object} orderReq - incoming request object from storefront
 * @returns {Object} result object based on validation
 */
function validateRequest(orderReq) {
  try {
    var orderRequest = JSON.parse(orderReq);
    if (
      orderRequest.OrderDetailsRequest &&
      orderRequest.OrderDetailsRequest.OrderNumber &&
      (orderRequest.OrderDetailsRequest.BillToID || orderRequest.OrderDetailsRequest.ZipCode) &&
      orderRequest.OrderDetailsRequest.InvokedFrom
    ) {
      return {
        valid: true,
        orderDetailsRequest: orderRequest.OrderDetailsRequest
      };
    }
    return {
      valid: false,
      message:
        'Check the format of order details request, the right format is { "OrderDetailsRequest" : {"OrderNumber" : "<order number","BillToID" : < customer number/username>, "InvokedFrom" : <online>,"zipCode" : "<zip code>"}. Either BillToID/zipCode is mandatory.'
    };
  } catch (ex) {
    OMSLogger.error('[OMS] Error : {0} {1}', ex, ex.stack);
    return {
      valid: false,
      message:
        'Error in parsing order details request, check the format of order details request, the right format is { "OrderDetailsRequest" : {"OrderNumber" : "<order number","BillToID" : < customer number/username>, "InvokedFrom" : <online>,"zipCode" : "<zip code>"}.Either BillToID/zipCode is mandatory.'
    };
  }
}

module.exports = {
  getOrderDetails: getOrderDetails,
  validateRequest: validateRequest,
  getOrderCancelStatus: getOrderCancelStatus
};
