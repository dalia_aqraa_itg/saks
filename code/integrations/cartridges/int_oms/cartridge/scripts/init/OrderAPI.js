'use strict';

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var Logger = require('dw/system/Logger');
var preferences = require('*/cartridge/config/preferences');

var CreateOrderService = function () {
  /**
   * Helper method for sending email requests
   *
   * @param {Object} data The JSON data to send in the body of the request
   *
   */
  this.createOrderService = function (data) {
    Logger.debug('SERVICE NAME - ' + preferences.createOrderService);
    var createOrderOMSService = LocalServiceRegistry.createService(preferences.createOrderService, {
      createRequest: function (service, params) {
        service.addHeader('Content-Type', 'application/xml');
        service.addHeader('x-api-key', data.apiKey);
        service.addHeader('x-apigw-api-id', data.apiId);
        service.setRequestMethod('POST');
        return data.requestXML;
      },
      parseResponse: function (service, httpClient) {
        Logger.info('Response:\n{0}', httpClient.text);
        return httpClient.text;
      },
      filterLogMessage: function (msg) {
        msg = msg.replace(/BillToID=\".*?\"/g, 'BillToID=********');
        msg = msg.replace(/CustomerEMailID=\".*?\"/g, 'CustomerEMailID=********');
        msg = msg.replace(/EMailID=\".*?\"/g, 'EMailID=********');
        msg = msg.replace(/FirstName=\".*?\"/g, 'FirstName=********');
        msg = msg.replace(/LastName=\".*?\"/g, 'LastName=********');
        msg = msg.replace(/AddressLine1=\".*?\"/g, 'AddressLine1=********');
        msg = msg.replace(/State=\".*?\"/g, 'State=********');
        msg = msg.replace(/ZipCode=\".*?\"/g, 'ZipCode=********');
        msg = msg.replace(/AddressLine2=\".*?\"/g, 'AddressLine2=********');
        msg = msg.replace(/DayPhone=\".*?\"/g, 'DayPhone=********');
        msg = msg.replace(/PaymentReference1=\".*?\"/g, 'PaymentReference1=********');
        msg = msg.replace(/PaymentReference3=\".*?\"/g, 'PaymentReference3=********');
        msg = msg.replace(/PaymentReference4=\".*?\"/g, 'PaymentReference4=********');
        msg = msg.replace(/AuthorizationID=\".*?\"/g, 'AuthorizationID=********');
        msg = msg.replace(/CustomerFirstName=\".*?\"/g, 'CustomerFirstName=********');
        msg = msg.replace(/CustomerLastName=\".*?\"/g, 'CustomerLastName=********');
        msg = msg.replace(/TranReturnMessage=\".*?\"/g, 'TranReturnMessage=********');
        msg = msg.replace(/AuthAvs=\".*?\"/g, 'AuthAvs=********');
        msg = msg.replace(/SvcNo=\".*?\"/g, 'SvcNo=********');
        msg = msg.replace(/DisplayCardNo=\".*?\"/g, 'DisplayCardNo=********');
        msg = msg.replace(/DisplaySvcNo=\".*?\"/g, 'DisplaySvcNo=********');
        msg = msg.replace(/RequestId=\".*?\"/g, 'RequestId=********');
        msg = msg.replace(/CreditCardNo=\".*?\"/g, 'CreditCardNo=********');

        return msg.replace('headers', 'OFFWITHTHEHEADERS');
      },
      mockCall: function (svc, client) {
        var mockedReponse = '<Data><ResponseCode>0</ResponseCode><ResponseMessage>Success</ResponseMessage></Data>';
        Logger.debug('mock call create order');
        return {
          statusCode: 200,
          statusMessage: 'Success',
          text: mockedReponse
        };
      }
    });

    var result = createOrderOMSService.call(data);
    Logger.debug('result.object --->' + result.object);
    if (result.status != 'OK' || !result.object) {
      Logger.error('create order service failed : ', result.errorMessage);
      return '<Data><ResponseCode>1</ResponseCode><ResponseMessage>Failure</ResponseMessage></Data>';
    }
    return result.object;
  };
};
module.exports = CreateOrderService;
