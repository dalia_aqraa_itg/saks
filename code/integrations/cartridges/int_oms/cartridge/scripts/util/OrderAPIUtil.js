'use strict';
var orderService = new (require('*/cartridge/scripts/init/OrderAPI'))();
var preferences = require('*/cartridge/config/preferences');
var Logger = require('dw/system/Logger');
var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
var Money = require('dw/value/Money');
var Quantity = require('dw/value/Quantity');
var collections = require('*/cartridge/scripts/util/collections');
var Calendar = require('dw/util/Calendar') ;
var Decimal = require('dw/util/Decimal');
var HashMap = require('dw/util/HashMap');
var PropertyComparator = require('dw/util/PropertyComparator');
var ArrayList = require('dw/util/ArrayList');

var StringUtils = require('dw/util/StringUtils');
var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');
var globalHelper = require('*/cartridge/scripts/helpers/globalHelper');

var orderActionModify = false;
var Site = require('dw/system/Site');
var customPreferences = Site.current.preferences.custom;

var pliCount = new HashMap();
var shippingCharge = new HashMap();
var shippingTax = new HashMap();
var isEmployee = 'N';

var currencyCode;
var firstTaxableLineNo;
var firstTaxableLineShipTax;
var firstTaxableLineShipTaxIndex;
var firstLineNo;
var firstLineShipTax;
var firstLineShipTaxIndex;
var firstRegularTaxableLineNo;
var firstRegularTaxableLineShipTax;
var firstRegularTaxableLineShipTaxIndex;
var firstRegularPennyUpdate = false;
var proratedShippingTaxTotal = 0;
var instockOrderTotal = 0;
var orderInstockOrderTotal = 0;
var preOrderObj;
var computedOrderTotal = 0;
var sfccOrderTotal;
var noOfProducts = 0;

var orderAPILogger = Logger.getLogger('OrderAPIUtils', 'OrderAPIUtils');

/**
 * This function identifies the countryCode to be sent to Auth.
 * CountryCode has to be GB for UK/GB.
 * @param {dw.order.OrderAddress} address
 */
function getCountryCode(address) {
  if (address && address.countryCode && address.countryCode.value) {
    if (address.countryCode.value.toLowerCase() == 'uk' || address.countryCode.value.toLowerCase() == 'gb') {
      return 'GB'; // country code for the Auth request
    } else {
      return address.countryCode.value;
    }
  }
  return ''; // if it reaches this point, then the address was empty or does not have a country code
}

function countDecimalDigits(number) {
  var char_array = number.toString().split(""); // split every single char
  var not_decimal = char_array.lastIndexOf(".");
  return (not_decimal<0) ? 0 : char_array.length - not_decimal -1;
}

/* Description:
 * Identify if the order is complete preOrder and use the result to update the Authorization amount to 0
 * */
function preOrder(Order) {
  var allLineItems = Order.getProductLineItems();
  var obj = {};
  obj.hasOnePreOrderItem = false;
  obj.completePreOrder = true;
  collections.forEach(allLineItems, function (item) {
    if ('inventoryStatus' in item.custom && item.custom.inventoryStatus == 'PREORDER') {
      obj.hasOnePreOrderItem = true;
    } else {
      obj.completePreOrder = false;
    }
  });
  return obj;
}

function hasSpecialVendorDiscountAppliedInBasket(basket) {
  var hasSpecialDiscount = false;
  if (basket) {
    var plis = basket.getAllProductLineItems().iterator();
    while (plis.hasNext()) {
      var pli = plis.next();
      if (pli && 'productPromotionSpecialVendorDiscount' in pli.custom &&  pli.custom.productPromotionSpecialVendorDiscount) {
        hasSpecialDiscount = true;
        break;
      }
    }
  }
  return hasSpecialDiscount;
}

/**
 * Helper method for Create Order API
 *
 * @param order
 * @returns
 */
function createOrderInOMS(order) {
	var reqPayLoad = {};
	reqPayLoad.apiKey = preferences.hbcAPIKey;
	reqPayLoad.apiId = preferences.hbcAPIID;
	var requestXML = buildRequestXML(order);
	if (requestXML == null) {
		return dw.system.Status.ERROR;
	}
	reqPayLoad.requestXML = requestXML;
  return orderService.createOrderService(reqPayLoad);
}

function buildRequestXML( order) {
	if (order == null) {
		return null;
	}
	
	var createOrderInOMSRequest;
	var orderXML ;
	currencyCode = order.getCurrencyCode();
	var defaultShipment = order.defaultShipment;
	var giftOptionsPA = order.getPriceAdjustmentByPromotionID('GiftOptions');
	var wrapTypeText = 'N';
	// calculate the prorated charge if giftwrap is applied to shipment
	if (defaultShipment && defaultShipment.custom.hasOwnProperty('giftWrapType')) {
		if (defaultShipment.custom.giftWrapType === 'giftpack') {
			wrapTypeText = 'A';
		}
	}
	if ('instockOrderTotal' in order.custom && order.custom.instockOrderTotal) {
		orderInstockOrderTotal = order.custom.instockOrderTotal;
	}

	sfccOrderTotal = order.totalGrossPrice.value;

	orderXML = <Order />;
	if ('orderActionModify' in order.custom && order.custom.orderActionModify) {
		orderActionModify = true;
		orderXML.@Action = "MODIFY";
	} else {
		orderActionModify = false;
		orderXML.@Action = "CREATE";
	}

	if (defaultShipment && 'sddShoprunnerToken' in defaultShipment.custom) {
		orderXML.@NotificationType = "SAMEDAY";
		orderXML.@NotificationReference = defaultShipment.custom.sddShoprunnerToken;
	}
	orderXML.@AllocationRuleID = preferences.omsAllocationRuleID;
	orderXML.@AuthorizedClient = preferences.omsEnterpriseCode;
  if (order.getCustomerNo() != null) {
    orderXML.@BillToID = order.getCustomerNo();
    orderXML.@CustomerContactID = order.getCustomerNo();
  }

	if (order.getCustomerEmail() != null) {
		orderXML.@CustomerEMailID = order.getCustomerEmail();
	}

	if ((!empty (order.channelType) && order.channelType.value === 11) || order.getCreatedBy() !== 'Customer') {
		orderXML.@EnteredBy = order.getCreatedBy();
		orderXML.@EntryType = preferences.omsCSREntryType;
		orderXML.@Createuserid = order.getCreatedBy();
	} else {
		orderXML.@EnteredBy = preferences.omsEnteredBy;
		orderXML.@EntryType = preferences.omsEntryType;
	}

	orderXML.@DocumentType = preferences.omsDocumentType ;
	orderXML.@EnterpriseCode = preferences.omsEnterpriseCode;
	orderXML.@OrderNo = order.getOrderNo();

	var orderCalender = new Calendar(order.getCreationDate());
	orderCalender = StringUtils.formatCalendar(orderCalender, "yyyy-MM-dd'T'HH:mm:ss'Z'");
	orderXML.@OrderDate = orderCalender;

	preOrderObj = preOrder(order);
	if (preOrderObj.hasOnePreOrderItem) {
	  orderXML.@EXTNWEBORDERSUFFIX = '000';
	}

	//set create user id at order
	if ('createUserid' in order.custom && order.custom.createUserid) {
	  orderXML.@Createuserid = order.custom.createUserid;
	}
	orderXML.@SellerOrganizationCode = preferences.omsEnterpriseCode;
	if (order.getCustomer() != null && order.getCustomer().getProfile() != null ) {
		var firstName = emptyStringIfNull(order.getCustomer().getProfile().getFirstName());
		firstName = globalHelper.replaceSpecialCharsAndEmojis(firstName);
		orderXML.@CustomerFirstName = firstName;

		var lastName = emptyStringIfNull(order.getCustomer().getProfile().getLastName());
		lastName = globalHelper.replaceSpecialCharsAndEmojis(lastName);
		orderXML.@CustomerLastName = lastName;
	}
	orderXML.@DraftOrderFlag = preferences.omsDraftOrderFlag;
	orderXML = appendOrderLevelBillingInfo(orderXML, order);
	orderXML = appendOrderLevelShippingInfo(orderXML, order);

	var couponCount = 0;
	for each (coupon in order.getCouponLineItems()) {
		orderXML.Promotions.Promotion += <Promotion/>;
		orderXML.Promotions.Promotion[couponCount].@Action = "CREATE" ;
		orderXML.Promotions.Promotion[couponCount].@PromotionId = coupon.getCouponCode() ;
		couponCount = couponCount + 1;
	}

	var lineItems = order.productLineItems;
	for each (item in lineItems) {
		if ('isCSRAdjustment' in item.custom && item.custom.isCSRAdjustment) {
			orderXML.Promotions.Promotion += <Promotion/>;
			orderXML.Promotions.Promotion[couponCount].@Action = "CREATE" ;
			orderXML.Promotions.Promotion[couponCount].@PromotionId = 'CSR Adj: ' + item.lineItemText;
			couponCount = couponCount + 1;
		}
	}
	// SAKS PRIME MEMBERSHIP
  var saksplusHelper = require('*/cartridge/scripts/helpers/saksplusHelper');
  var hasSaksPlus = saksplusHelper.hasSaksPlusInBasket(order);
  if (hasSaksPlus) {
    orderXML.Promotions.Promotion += <Promotion/>;
    orderXML.Promotions.Promotion[couponCount].@Action = "CREATE";
    orderXML.Promotions.Promotion[couponCount].@PromotionId = 'SAKSPRIME';
    couponCount = couponCount + 1;
  }
	if (hasSpecialVendorDiscountAppliedInBasket(order)) {
		orderXML.Promotions.Promotion += <Promotion/>;
		orderXML.Promotions.Promotion[couponCount].@Action = "CREATE" ;
		orderXML.Promotions.Promotion[couponCount].@PromotionId = "CPNSVEND";
		couponCount = couponCount + 1;
	}

	var lineItems = order.productLineItems;
	var itemNumber = 0;
	for each (var pli in lineItems) {
    orderXML.OrderLines.OrderLine += <OrderLine />;
    //set create user id at order line
    if ('createUserid' in order.custom && order.custom.createUserid) {
      orderXML.OrderLines.OrderLine[itemNumber].@Createuserid = order.custom.createUserid;
    }
    var shipment = pli.shipment;
    var shipmentID = pli.getShipment().ID;

    if (pliCount.get(shipmentID) == null) {
      pliCount.put(shipmentID, 1);
    } else {
      pliCount.put(shipmentID, pliCount.get(shipmentID) + 1);
    }
    noOfProducts += 1;

    if (shipment != null) {
      var CSC = pli && pli.custom.CarrierServiceCode ? pli.custom.CarrierServiceCode : shipment.getShippingMethod().custom.omsCarrierServiceCode;
      var scac = pli && pli.custom.scac ? pli.custom.scac : shipment.getShippingMethod().custom.omsSCAC;
      orderXML.OrderLines.OrderLine[itemNumber].@CarrierServiceCode = emptyStringIfNull(CSC);
      orderXML.OrderLines.OrderLine[itemNumber].@SCAC = emptyStringIfNull(scac);

      if (shipment.custom.shipmentType == "instore" && pli.custom.fromStoreId != null && pli.custom.fromStoreId != "") {
        orderXML.OrderLines.OrderLine[itemNumber].@DeliveryMethod = "PICK";
      } else {
        orderXML.OrderLines.OrderLine[itemNumber].@DeliveryMethod = "SHP";
      }
    }
	
    if ("fulfillmentType" in pli.custom && pli.custom.fulfillmentType != null) {
      orderXML.OrderLines.OrderLine[itemNumber].@FulfillmentType = pli.custom.fulfillmentType;
    } else {
      if ('inventoryStatus' in pli.custom && pli.custom.inventoryStatus == 'PREORDER') {
        orderXML.OrderLines.OrderLine[itemNumber].@FulfillmentType = 'PREORDER';
      } else if (shipment.custom.shipmentType == "instore" && pli.custom.fromStoreId != null && pli.custom.fromStoreId != "") {
        orderXML.OrderLines.OrderLine[itemNumber].@FulfillmentType ="PICK";
      } else {
        orderXML.OrderLines.OrderLine[itemNumber].@FulfillmentType ="SHIP";
      }
    }

    if (pli.product && pli.product.custom && pli.product.custom.hasOwnProperty('giftWrapEligible') && pli.product.custom.giftWrapEligible === 'true' && pli.shipment.isGift()) {
      if (pli.shipment.custom.giftWrapType && pli.shipment.custom.giftWrapType === 'giftpack') {
        orderXML.OrderLines.OrderLine[itemNumber].@GiftWrap = "Y";
      } else {
        orderXML.OrderLines.OrderLine[itemNumber].@GiftWrap = "N";
      }
      orderXML.OrderLines.OrderLine[itemNumber].@GiftFlag = "Y";
    } else {
      orderXML.OrderLines.OrderLine[itemNumber].@GiftWrap = "N";
      orderXML.OrderLines.OrderLine[itemNumber].@GiftFlag = "N";
    }

    var lineType;
		if ('inventoryStatus' in pli.custom && pli.custom.inventoryStatus == 'PREORDER') {
			lineType ="PREORDER";
		} else if (pli.product != null && pli.product.custom.dropShipInd != null && pli.product.custom.dropShipInd === 'true') {
			lineType ="DROPSHIP";
		} else if (pli.product != null && pli.product.custom.hbcProductType != null && pli.product.custom.hbcProductType.toLowerCase() === 'standard') {
			lineType ="REGULAR";
		} else if (pli.product != null && pli.product.custom.hbcProductType != null && pli.product.custom.hbcProductType.toLowerCase() === 'giftcard') {
			lineType ="PHYSICALGIFT";
		} else if (pli.product != null && pli.product.custom.hbcProductType != null && pli.product.custom.hbcProductType.toLowerCase() === 'gwp') {
			lineType ="GWP";
		} else  {
			lineType ="REGULAR";
		}

		orderXML.OrderLines.OrderLine[itemNumber].@LineType = lineType ;
		orderXML.OrderLines.OrderLine[itemNumber].@OrderedQty = pli.quantity.value ;
		orderXML.OrderLines.OrderLine[itemNumber].@PrimeLineNo = pli.position ;
		orderXML.OrderLines.OrderLine[itemNumber].@SubLineNo = "1" ;
		if ("shipNode" in pli.custom && pli.custom.shipNode != null) {
			orderXML.OrderLines.OrderLine[itemNumber].@ShipNode =  pli.custom.shipNode ;
		} else {
			if ("fromStoreId" in pli.custom && pli.custom.fromStoreId != null && pli.custom.fromStoreId != "") {
				orderXML.OrderLines.OrderLine[itemNumber].@ShipNode = pli.custom.fromStoreId;
			} else {
				orderXML.OrderLines.OrderLine[itemNumber].@ShipNode = "";
			}
		}
		orderXML.OrderLines.OrderLine[itemNumber].Item.@ItemID = pli.productID ;
		//replaces all non alphanumeric characters contained in CustomerItemDesc attribute with emptystring
		var customerItemDesc = emptyStringIfNull(pli.productName);
		customerItemDesc = customerItemDesc.replace(/[^a-zA-Z0-9\s]/g,'');
		orderXML.OrderLines.OrderLine[itemNumber].Item.@CustomerItemDesc = customerItemDesc;

		orderXML.OrderLines.OrderLine[itemNumber].Item.@UnitOfMeasure = preferences.omsUnitOfMeasure ;
		if (pli.product != null && pli.product.custom.productLine != null) {
			orderXML.OrderLines.OrderLine[itemNumber].Item.@ProductLine = pli.product.custom.productLine ;
		} else {
			orderXML.OrderLines.OrderLine[itemNumber].Item.@ProductLine = "1" ;
		}
		orderXML = appendLineLevelShippingInfo(orderXML, order, pli, itemNumber);
		if ("reservationID" in pli.custom && pli.custom.reservationID != null) {
			orderXML.OrderLines.OrderLine[itemNumber].OrderLineReservations += <OrderLineReservations/>
			orderXML.OrderLines.OrderLine[itemNumber].OrderLineReservations.OrderLineReservation.@ItemID = pli.getProductID() ;
			orderXML.OrderLines.OrderLine[itemNumber].OrderLineReservations.OrderLineReservation.@UnitOfMeasure = "EACH";
			orderXML.OrderLines.OrderLine[itemNumber].OrderLineReservations.OrderLineReservation.@ReservationID = pli.custom.reservationID ;
			if ("shipNode" in pli.custom) {
				orderXML.OrderLines.OrderLine[itemNumber].OrderLineReservations.OrderLineReservation.@Node = pli.custom.shipNode;
			}
			if ("availableQty" in pli.custom) {
				orderXML.OrderLines.OrderLine[itemNumber].OrderLineReservations.OrderLineReservation.@Quantity = pli.custom.availableQty;
			}
		}

		orderXML.OrderLines.OrderLine[itemNumber].LinePriceInfo.@ListPrice = getTwoDecimalMoneyFormat(getProratedPriceCustom(pli,order)/pli.quantity.value);

		var prodprice = new Money(getProratedPriceCustom(pli,order)/pli.quantity.value, order.getCurrencyCode());
		prodprice = prodprice.value * pli.quantity.value;
		if ('inventoryStatus' in pli.custom && pli.custom.inventoryStatus == 'REGULAR') {
		  // this step is to avoid rounding off issues. OMS does this based on the list price which is a unit price
			instockOrderTotal += prodprice;
		}
		computedOrderTotal += prodprice;

		var retailprice = 0;
		if ('retailprice' in pli.custom) {
			retailprice = pli.custom.retailprice/pli.quantity.value;
		} else {
			retailprice = pli.getNetPrice() / pli.quantity.value;
		}
		orderXML.OrderLines.OrderLine[itemNumber].LinePriceInfo.@RetailPrice = getTwoDecimalMoneyFormat(retailprice) ;
		
		var unitPrice;
		if ('isPromotionPriceBook' in pli.custom && pli.custom.isPromotionPriceBook) {
			//unitPrice = retailprice;
			var onlySimplePromoDiscount = 0;
			var productPromotionDiscount = 0;
			var productPromotionDiscountWOPricebook = 0;

			if (!empty(pli.custom.productPromotionDiscount)) {
				productPromotionDiscount = pli.custom.productPromotionDiscount;
			}
			if (!empty(pli.custom.productPromotionDiscountWOPricebook)) {
				productPromotionDiscountWOPricebook = pli.custom.productPromotionDiscountWOPricebook;
			}

			if (productPromotionDiscount !== productPromotionDiscountWOPricebook) {
				onlySimplePromoDiscount = productPromotionDiscount - productPromotionDiscountWOPricebook;
			} else {
				onlySimplePromoDiscount = productPromotionDiscount;
			}

			onlySimplePromoDiscount = onlySimplePromoDiscount / pli.quantity.value;
			unitPrice = retailprice - onlySimplePromoDiscount;
		} else {
			unitPrice = pli.getNetPrice()/pli.quantity.value;
		}
		orderXML.OrderLines.OrderLine[itemNumber].LinePriceInfo.@UnitPrice = getTwoDecimalMoneyFormat(unitPrice);
		orderXML.OrderLines.OrderLine[itemNumber].LinePriceInfo.@IsLinePriceForInformationOnly = "N";
		if (!orderActionModify) {
			orderXML.OrderLines.OrderLine[itemNumber].LinePriceInfo.@PricingQuantityStrategy = "FIX";
		}
		orderXML = appendLineLevelChargeInfo(orderXML, order, pli, itemNumber, giftOptionsPA);
    	orderXML = appendLineLevelTaxInfo(orderXML, order, pli, itemNumber, giftOptionsPA);
		
		
		var instructionCounter = 0;
		var wrapType = 'N';
		var giftRecipient = null;
    if (pli.product && pli.product.custom && pli.product.custom.hasOwnProperty('giftWrapEligible') && pli.product.custom.giftWrapEligible === 'true') {
      if (wrapTypeText === 'A') {
        wrapType = 'A-GIFT BOX WITH RIBBON (COMPLIMENTARY)';
      }
      if (pli.shipment && pli.shipment.custom.giftRecipientName) {
        giftRecipient = pli.shipment.custom.giftRecipientName;
      }
    }
		//set site-refer value as instruction
    orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction += <Instruction/>;
    orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionType = 'PS_RET_FLG';
    orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionText = 'Y';
    instructionCounter++;
    orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction += <Instruction/>;
    orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionType = 'PS_COLUMN';
    orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionText = 'N';
    instructionCounter++;
    if (pli.product && pli.product.custom && pli.product.custom.hasOwnProperty('giftWrapEligible') &&
        pli.product.custom.giftWrapEligible === 'true' && pli.shipment.isGift() && (pli.shipment.giftMessage != null || pli.shipment.giftMessage != "")) {
      orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction += <Instruction/>;
      orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionType = 'WRAP_TYPE';
      orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionText = wrapType;
      instructionCounter++;
    } else {
      orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction += <Instruction/>;
      orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionType = 'WRAP_TYPE';
      orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionText = 'N-';
      instructionCounter++;
    }
    if (giftRecipient) {
      orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction += <Instruction/>;
      orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionType = 'GIFT_RECP';
      orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionText = giftRecipient;
      instructionCounter++;
    }
    if (pli && pli.custom.sddLineItem && defaultShipment && 'sddShoprunnerToken' in defaultShipment.custom && 'deliveryInstructions' in defaultShipment.custom) {
      orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction += <Instruction/>;
      orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionType = 'SAMEDAY';
      orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionText = defaultShipment.custom.deliveryInstructions;
      instructionCounter++;
    }
		if ('site_refer' in order.custom && order.custom.site_refer && order.custom.site_refer !== '') {
			var sendInstruction = false;
			var sfValues = preferences.site_refer_Salesfloor_Values;
			if (sfValues && sfValues.length > 0) {
				for (var i = 0; i < sfValues.length; i++) {
					var regStr = '^('+sfValues[i].toLocaleLowerCase()+')';
					sendInstruction = new RegExp(regStr).test(order.custom.site_refer.toLocaleLowerCase());
					if (sendInstruction) {
						break;
					}
				}
			}
			if (order.custom.site_refer.toLowerCase() == 'comx' && 'comxNote' in order.custom && !empty(order.custom.comxNote)) {
				orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction += <Instruction/>;
        orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionType = preferences.instructionTypeName_SITE_REFER;
        orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionUsage = order.custom.site_refer;
        if ('comxNote' in order.custom && !empty(order.custom.comxNote)) {
          orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionText = order.custom.comxNote;
        } else {
          orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionText = "";
        }
        instructionCounter++;
			} else {
				if (sendInstruction && preferences.instructionUsage_Salesfloor_OMS) {
				 	orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction += <Instruction/>;
				 	orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionType = preferences.instructionTypeName_SITE_REFER;
				 	orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionText = order.custom.site_refer;
				 	orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionUsage = preferences.instructionUsage_Salesfloor_OMS;
				 	instructionCounter++;
				}
			}
		}

		var giftmessage =  pli.shipment.giftMessage;
		if (giftmessage != null) {
			var splitMessage = splitGiftMessage(giftmessage);
			var startIndex = 1;
			if (pli.product && pli.product.custom && pli.product.custom.hasOwnProperty('giftWrapEligible') && pli.product.custom.giftWrapEligible === 'true' && pli.shipment.isGift()) {
				for (i = 0; i < splitMessage.length; i++) {
					var trimMsg = splitMessage[i].trim();
					if (trimMsg != "") {
						orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction += <Instruction/>;
						orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionText = trimMsg;
						orderXML.OrderLines.OrderLine[itemNumber].Instructions.Instruction[instructionCounter].@InstructionType = "GIFT_MSG" + startIndex;
						instructionCounter++;
						startIndex++;
					}
				}
			}
		}

		var eddInfo = pli.getShipment().custom.selectedEstimatedDateforOMS;
		if (eddInfo != null) {
			eddInfo = eddInfo.toString();
      		orderXML.OrderLines.OrderLine[itemNumber].OrderDates.OrderDate.@ActualDate = eddInfo + "T00:00:00";
			orderXML.OrderLines.OrderLine[itemNumber].OrderDates.OrderDate.@DateTypeId = "ORD_LINE_EXP_DEL_DATE";
		}
		orderXML.OrderLines.OrderLine[itemNumber].Extn.@ExtnIsPromoEvaluated = "Y" ;
		orderXML.OrderLines.OrderLine[itemNumber].Extn.@ExtnWebLineNumber = pli.position ;

		var availabilityModel = pli.product.availabilityModel;
		if (availabilityModel && !empty(availabilityModel)) {
			var inventoryRecord = availabilityModel.inventoryRecord;
			if (pli && pli.product != null && 'inventoryStatus' in pli.custom && pli.custom.inventoryStatus == 'PREORDER' && !empty(inventoryRecord.inStockDate)) {
				orderXML.OrderLines.OrderLine[itemNumber].Extn.@ExtnEstimatedShipDate = inventoryRecord.inStockDate.toISOString();
				orderXML.OrderLines.OrderLine[itemNumber].Extn.@ExtnFirstEstimatedShipDate = inventoryRecord.inStockDate.toISOString();
				var suffixForPreOrder = StringUtils.formatNumber(pli.position, '000');
				orderXML.OrderLines.OrderLine[itemNumber].Extn.@ExtnWebOrderSuffix = suffixForPreOrder;
			} else if (pli && pli.product != null && 'inventoryStatus' in pli.custom && pli.custom.inventoryStatus == 'PREORDER') {
				var suffixForPreOrder = StringUtils.formatNumber(pli.position, '000');
				orderXML.OrderLines.OrderLine[itemNumber].Extn.@ExtnWebOrderSuffix = suffixForPreOrder;
			} else {
				orderXML.OrderLines.OrderLine[itemNumber].Extn.@ExtnWebOrderSuffix = '000';
			}
		} else if ("shipDate" in pli.custom) {
			orderXML.OrderLines.OrderLine[itemNumber].Extn.@ExtnEstimatedShipDate = pli.custom.shipDate;
		} else {
			orderXML.OrderLines.OrderLine[itemNumber].Extn.@ExtnWebOrderSuffix = '000';
		}
		var isReturnable = "Y";

		// to be based on final sale Toggle on the site preference
			if (pli.product != null && (('finalSale' in pli.product.custom && pli.product.custom.finalSale &&
			 (preferences && preferences.product && preferences.product.IS_FINAL_SALE_TOOGLE == 'true')) ||
			 ('returnable' in pli.product.custom && pli.product.custom.returnable === 'false'))) {
				isReturnable = "N";
			}
			
		orderXML.OrderLines.OrderLine[itemNumber].Extn.@ExtnIsReturnable = isReturnable;
		if (customPreferences.signatureRequired === true) {
		var masterProduct = pli.product && pli.product.variationModel ? pli.product.variationModel.master : '';
			if (masterProduct && !empty(masterProduct.custom.signatureRequiredType) && masterProduct.custom.signatureRequiredType === 'true') {
				orderXML.OrderLines.OrderLine[itemNumber].Extn.@ExtnSignatureRequired = "Y";
			} else if (pli.getShipment() && 'signatureRequired' in pli.getShipment().custom && pli.getShipment().custom.signatureRequired == true) {
				orderXML.OrderLines.OrderLine[itemNumber].Extn.@ExtnSignatureRequired = "Y";
			}
			//orderXML.OrderLines.OrderLine[itemNumber].Notes = ""; // pending For CSR Orders
			itemNumber = itemNumber + 1;
		}

	}

	orderXML = splitGCLines(orderXML, order);

	if ("youSavedTotal" in order.custom && order.custom.youSavedTotal != "" && order.custom.youSavedTotal != "0") {
		orderXML.HeaderCharges.HeaderCharge.@ChargeAmount = getTwoDecimalMoneyFormat(order.custom.youSavedTotal);
		orderXML.HeaderCharges.HeaderCharge.@ChargeCategory = "YOUSAVED";
		orderXML.HeaderCharges.HeaderCharge.@ChargeName = "YOUSAVED";
	}
	orderXML = appendPaymentInfo(orderXML, order);

	if ('orderStatus' in order.custom && order.custom.orderStatus != null) {
		var holdTypes = order.custom.orderStatus;
		for (let i = 0; i < holdTypes.length; i++) {
			orderXML.OrderHoldTypes.OrderHoldType += <OrderHoldType/>;
			orderXML.OrderHoldTypes.OrderHoldType[i].@HoldType = order.custom.orderStatus[i].value;
			orderXML.OrderHoldTypes.OrderHoldType[i].@ReasonText = order.custom.orderStatus[i].value;
		}
	}

	orderXML.Extn.@ExtnCurrencyCode = currencyCode;

	if (order.getCustomer() == null || order.getCustomer().getProfile() == null || order.getCustomer().getProfile().custom.preferredLanguage.value == null) {
    orderXML.Extn.@ExtnCommLang = order.getCustomerLocaleID();
  }

  orderXML.Extn.@ExtnReference1 = "UOP";
	if ('bfxMerchantOrderRef' in order.custom && order.custom.bfxMerchantOrderRef != null && order.custom.bfxMerchantOrderRef != "") {
		orderXML.Extn.@ExtnIsFiftyOne ="Y";
		orderXML.Extn.@ExtnFiftyOneOrderNo = 'bfxOrderId' in order.custom && !empty(order.custom.bfxOrderId) ? order.custom.bfxOrderId : order.getOrderNo();
	} else {
		orderXML.Extn.@ExtnIsFiftyOne ="N";
	}

	if ('sr_token' in order.custom && order.custom.sr_token) {
		orderXML.Extn.@ExtnShopRnrAuthTok = order.custom.sr_token;
	}

	// overwrite loyalty tierName with rewards cardholder tierName
	if (order.custom.rewardsCardTierName) {
		orderXML.Extn.@ExtnLoyaltyTier = order.custom.rewardsCardTierName;
	}
	if (order.custom.hudsonRewardNumber != "") {
		orderXML.Extn.@ExtnLoyaltyNo = order.custom.hudsonRewardNumber;
	}

	orderXML.Extn.@ExtnAssociateName = order.getCreatedBy();

	if (order.getCustomerNo() != null) {
		orderXML.Extn.@ExtnClientID = order.getCustomerNo();
	}
	if ('deviceFingerPrintID' in order.custom) {
		var device_fingerprint = emptyStringIfNull(order.custom.deviceFingerPrintID);
		var in_auth_session_id = emptyStringIfNull(order.custom.deviceFingerPrintID);
	}
	if ('failAttempts' in order.custom) {
		var fail_attempts = emptyStringIfNull(parseInt(order.custom.failAttempts));
	}

	var employee_ind = emptyStringIfNull(isEmployee);

	//get the ip address
	var ipAddress = order.remoteHost;
	//default value for customer creation date is an empty string
	var customerCreationDate = '';
	//if customer has created a profile populate the creation date
	if (order.getCustomer() != null && order.getCustomer().getProfile() != null ) {
		//got this from API documentation
    customerCreationDate = dw.util.StringUtils.formatCalendar(new Calendar(order.getCustomer().getProfile().getCreationDate()), 'yyyyMMddHHmmss');
	}

	//add IP and CustomerCreationDate to FraudReference1 node
	var fraudReference1 : XML = <YFSEnvironment device_fingerprint={device_fingerprint}
	in_auth_session_id={in_auth_session_id} fail_attempts={fail_attempts} employee_ind={employee_ind} IP={ipAddress} CustomerCreationDate={customerCreationDate}/>;

	if (tsysHelper.isTsysMode()) {
		var entryMode = getCardEntryMode(order);
		fraudReference1 = <YFSEnvironment device_fingerprint={device_fingerprint}
		in_auth_session_id={in_auth_session_id}
		fail_attempts={fail_attempts}
		employee_ind={employee_ind}
		IP={ipAddress}
		CustomerCreationDate={customerCreationDate}
		EM={entryMode}/>;
	}

	orderXML.Extn.HbcOrderHeaderAttrList.HbcOrderHeaderAttr.@FraudReference1 = fraudReference1.toXMLString(); //pending with team to set value in the order
	createOrderInOMSRequest = orderXML;
	orderActionModify = false;

	
	return createOrderInOMSRequest;
}

function getCardEntryMode(order) {
	var PaymentInstrument = require('dw/order/PaymentInstrument');
	var paymentInstruments = order.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);

	if (paymentInstruments && paymentInstruments.length > 0) {
		var entryMode = paymentInstruments[0].custom.entry_mode;
	}
	return entryMode;
}

function splitGiftMessage(giftMessage) {
	var MAX_LINE_LENGTH = 40;
	var NUM_OF_LINES = 4;
	// If there is no gift message just return empty object.
	if (!giftMessage) {
		return {};
	}
	var strippedGiftMessage = giftMessage;
	// Replace one or more occurance of '!' and '?' with a single '.' due to incorrect formatting of special chars

	var giftSplitByLine = strippedGiftMessage.split('\n');
	var giftMessageUnwrapped = giftMessage;
	var result;
	var removeNewLine = false;
	var lines = [];
	// if the entered lines match our API requirements, use them straight up.
	if (giftSplitByLine.length <= NUM_OF_LINES && removeNewLine == false) {
		removeNewLine = false;
		lines = giftSplitByLine;

		for (let i = 0 ; i < giftSplitByLine.length; i++) {
			if (giftSplitByLine[i].length <= MAX_LINE_LENGTH) {
				removeNewLine = false;
			} else {
				removeNewLine = true;
				lines = [];
				break;
			}
		}
	}

	if (giftSplitByLine.length > NUM_OF_LINES || removeNewLine == true)  {
		var regex = new RegExp('.{0,42}(?:\\s|$)', 'g');
		while ((result = regex.exec(giftMessageUnwrapped)) && lines.length < NUM_OF_LINES) {
			lines.push(result[0]);
		}
	}

	var giftMessageFormData = [];
	for (let i = 0; i < NUM_OF_LINES; i++) {
		if (lines.length > 0 && lines[i]) {
      giftMessageFormData[i] = i < lines.length ? lines[i] : '';
    }
	}
	return giftMessageFormData;
}

function getSellerProtectionShipment(order) {
  var mostExpensiveShipment;
  var pymtInstruments = order.getPaymentInstruments();
  if (pymtInstruments != null && pymtInstruments.length > 0) {
    for each (var paymentInstrument in pymtInstruments) {
      if (paymentInstrument.paymentMethod == 'PayPal') {
        var allShipments = order.getShipments();
        var arrayListShipment = new ArrayList(allShipments);

        if (!empty(arrayListShipment)) {
          arrayListShipment.sort(new PropertyComparator('adjustedMerchandizeTotalGrossPrice', false));
          mostExpensiveShipment = arrayListShipment.get(0);
          for each (var shipment in arrayListShipment) {
            if (shipment.custom.shipmentType != "instore") {
              return shipment;
            }
          }
          return mostExpensiveShipment;
        }
      }
    }
  }

  return;
}

function appendLineLevelChargeInfo(orderXML, order, pli, itemNumber, giftOptionsPA) {
	var i = 0;
	var discountIndex = -1;
	if (pli.custom.productPromotionDiscountWOPricebook !== null && pli.custom.productPromotionDiscountWOPricebook !== 0) {
		if (discountIndex > -1) {
			var chargeLine = orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[discountIndex].@ChargePerLine;
			var chargeLineMoney = getMoneyFormat(chargeLine);
			orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[discountIndex].@ChargePerLine = chargeLineMoney.add(getMoneyFormat(pli.custom.productPromotionDiscountWOPricebook)).toNumberString();
		} else {
			orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge += <LineCharge/>;
			orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargeCategory = "DISCOUNT";
			orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargeName = "DISCOUNT";
			orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargePerLine = getTwoDecimalMoneyFormat(pli.custom.productPromotionDiscountWOPricebook);
			discountIndex = i;
			i++;
		}
	}
	if (pli.custom.productPromotionAssociateDiscount != null && pli.custom.productPromotionAssociateDiscount != 0) {
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge += <LineCharge/>;
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargeCategory = "DISCOUNT";
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargeName = "ASSOCIATE";
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargePerLine =  getTwoDecimalMoneyFormat(pli.custom.productPromotionAssociateDiscount);
		i++;
		isEmployee = 'Y';
	}
	if (pli.custom.productPromotionSpecialVendorDiscount != null && pli.custom.productPromotionSpecialVendorDiscount != 0) {
		isEmployee = 'Y';
		if (discountIndex > -1) {
			var chargeLine = orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[discountIndex].@ChargePerLine;
			var chargeLineMoney = getMoneyFormat(chargeLine);
			orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[discountIndex].@ChargePerLine = chargeLineMoney.add(getMoneyFormat(pli.custom.productPromotionSpecialVendorDiscount)).toNumberString();
		} else {
			orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge += <LineCharge/>;
			orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargeCategory = "DISCOUNT";
			orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargeName = "DISCOUNT";
			orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargePerLine =  getTwoDecimalMoneyFormat(pli.custom.productPromotionSpecialVendorDiscount);
			discountIndex = i;
			i++;
		}
	}
	if (pli.custom.productPromotionFirstDayDiscount != null && pli.custom.productPromotionFirstDayDiscount != 0) {
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge += <LineCharge/>;
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargeCategory = "DISCOUNT";
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargeName = "FIRSTDAY";
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargePerLine =  getTwoDecimalMoneyFormat(pli.custom.productPromotionFirstDayDiscount);
		i++;
	}
	var surcharge = 0;
	if (pli.shippingLineItem != null && pli.shippingLineItem.surcharge) {
		surcharge = pli.shippingLineItem.adjustedPrice;
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge += <LineCharge/>;
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargeCategory = "SHIPPINGSURCHARGE";
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargeName = "SHIPPINGSURCHARGE";
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargePerLine = getTwoDecimalMoneyFormat(surcharge);
		if ('inventoryStatus' in pli.custom && pli.custom.inventoryStatus == 'REGULAR') {
			instockOrderTotal += surcharge.value;
		}
		computedOrderTotal += surcharge.value;
		i++;
	}
	var shippingPrice = 0;
	if (pli.getShipment() != null ) {
		var shipmentID = pli.getShipment().ID;

		if (shippingCharge.get(shipmentID) == null) {
			shippingCharge.put(shipmentID, 0 );
		}

		if (pli.getShipment().productLineItems.length == pliCount.get(shipmentID)) {
			shippingPrice = Money (pli.getShipment().adjustedShippingTotalPrice - shippingCharge.get(shipmentID), order.getCurrencyCode());
		} else {
			var shipmntPrice = new Number(pli.getShipment().getAdjustedMerchandizeTotalPrice(true));
			if (shipmntPrice.valueOf() !== 0) {
				var adjustedShippingPriceTotal = pli.getShipment().adjustedShippingTotalPrice;
				var customProratedPrice = getProratedPriceCustom(pli, order);
				var adjustedMerchandizeTotalPrice = pli.getShipment().getAdjustedMerchandizeTotalPrice(true);
				if (adjustedShippingPriceTotal.value > 0 && customProratedPrice.value > 0 && adjustedMerchandizeTotalPrice.value > 0) {
					shippingPrice = new Decimal( adjustedShippingPriceTotal).abs().multiply(new Number(customProratedPrice)).divide(new Number(adjustedMerchandizeTotalPrice));
				}
			}
			shippingPrice = Money (shippingPrice, order.getCurrencyCode());
			shippingCharge.put(shipmentID, shippingCharge.get(shipmentID) + shippingPrice);
		}

		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge += <LineCharge/>;
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargeCategory = "SHIPPINGCHARGE";
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargeName = "SHIPPINGCHARGE";
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargePerLine = getTwoDecimalMoneyFormat(shippingPrice - surcharge);
		var shipOnly = shippingPrice - surcharge;
		shipOnly = new Money(shipOnly, order.getCurrencyCode());
		if ('inventoryStatus' in pli.custom && pli.custom.inventoryStatus == 'REGULAR') {
			instockOrderTotal += shipOnly.value;
		}
		computedOrderTotal += shipOnly.value;
		i++;
	}

	if ("ecoFee" in pli.custom && pli.custom.ecoFee != null) {
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge += <LineCharge/>;
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargeCategory = "ECOFEE";
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargeName = "ECOFEE";
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargePerLine = getTwoDecimalMoneyFormat(pli.custom.ecoFee);
		i++;

	}

	if (giftOptionsPA && pli.shipment.custom.hasOwnProperty('giftWrapType') && pli.shipment.custom.giftWrapType === 'giftpack' &&
      pli.product && pli.product.custom && pli.product.custom.hasOwnProperty('giftWrapEligible') && pli.product.custom.giftWrapEligible === 'true') {
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge += <LineCharge/>;
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargeCategory = "GIFTWRAPCHARGE";
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargeName = "GIFTWRAPCHARGE";
		orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge[i].@ChargePerLine = getTwoDecimalMoneyFormat(pli.custom.proratedGiftCharge);
		var GiftWrapProratedCharge = getTwoDecimalMoneyFormat(pli.custom.proratedGiftCharge);
		if ('inventoryStatus' in pli.custom && pli.custom.inventoryStatus == 'REGULAR') {
			instockOrderTotal += Number(GiftWrapProratedCharge);
		}
		computedOrderTotal += Number(GiftWrapProratedCharge);
		i++;
	}

	return orderXML;
}

function appendPaymentInfo(orderXML, order) {
	var paymentInstruments = order.getPaymentInstruments();
	var sortedPaymentInstruments = new ArrayList(paymentInstruments);
	sortedPaymentInstruments.sort(new PropertyComparator('custom.chargeSequence', true));

	var paymentSequence = -1;
	if (sortedPaymentInstruments != null && sortedPaymentInstruments.length > 0) {
		for each (var paymentInstrument in sortedPaymentInstruments) {
			orderXML.PaymentMethods.PaymentMethod += <PaymentMethod/>;
			if (paymentInstrument == null) {
				continue;
			} else if (paymentInstrument.getPaymentMethod().equals( paymentInstrument.METHOD_CREDIT_CARD)) {
				paymentSequence++;
				orderXML = appendCreditCardInfo(orderXML, order, paymentInstrument, paymentSequence);
			} else if (paymentInstrument.paymentMethod == 'PayPal') {
				paymentSequence++;
				orderXML = appendPaypalInfo(orderXML, order, paymentInstrument, paymentSequence);
			} else if (paymentInstrument.paymentMethod == 'GiftCard' || paymentInstrument.paymentMethod === 'GIFT_CARD') {
				paymentSequence++;
				orderXML = appendGiftCardInfo(orderXML, order, paymentInstrument, paymentSequence);
			} else if (paymentInstrument.paymentMethod == 'KLARNA_PAYMENTS') {
				paymentSequence++;
				orderXML = appendCreditCardInfo(orderXML, order, paymentInstrument, paymentSequence);
			}
		}
	}
	return orderXML;
}

function splitGCLines(orderXML, order) {
	var orderAPILogger = Logger.getLogger('OrderAPIUtils', 'OrderAPIUtils');
	var totalOrderLines = orderXML.OrderLines.children().length();
	for each (var orderLine in orderXML.OrderLines.OrderLine) {
		var qty = orderLine.@OrderedQty;
		if (orderLine && orderLine.@LineType) {
			//split only for GC
			if (orderLine.@LineType == 'PHYSICALGIFT') {
        var origLineCharges = orderLine.LineCharges;
        var origLineTaxes = orderLine.LineTaxes;
        var orderLineCopy = orderLine.copy();

        for (let i = 1; i < qty; i++) {
          //Setting Quantity to 1 for original line and the copy
          orderLine.@OrderedQty = "1";
          if (orderLine.OrderLineReservations && orderLine.OrderLineReservations.OrderLineReservation) {
            orderLine.OrderLineReservations.OrderLineReservation.@Quantity = '1';
            orderLineCopy.OrderLineReservations.OrderLineReservation.@Quantity = '1';
          }
          orderLineCopy.@OrderedQty = "1";
          orderLineCopy.@PrimeLineNo = totalOrderLines + i;
          //Split LineCharges
          for (let origLineCharge = 0; origLineCharge < origLineCharges.children().length(); origLineCharge++) {
            //Original Charge for comparison
            var origChargePerLine = getTwoDecimalMoneyFormatForProration(origLineCharges.LineCharge[origLineCharge].@ChargePerLine);
            for (let lineCharge = 0; lineCharge < orderLineCopy.LineCharges.children().length(); lineCharge++) {
              //compare ChargeCategory and ChargeName, update matching Line Charge in copy
              if ((origLineCharges.LineCharge[origLineCharge].@ChargeCategory == orderLineCopy.LineCharges.LineCharge[lineCharge].@ChargeCategory) &&
                 (origLineCharges.LineCharge[origLineCharge].@ChargeName == orderLineCopy.LineCharges.LineCharge[lineCharge].@ChargeName)) {
                var newCharge = getTwoDecimalMoneyFormatForProration(origChargePerLine / qty);
                if (i == qty - 1) {
                  //setting for the first line item.
                  origLineCharges.LineCharge[origLineCharge].@ChargePerLine = newCharge;
                  var remainingamt = origChargePerLine.subtract(newCharge.multiply(new Quantity(qty - 1, 1)));
                  orderLineCopy.LineCharges.LineCharge[lineCharge].@ChargePerLine = remainingamt;
                } else {
                  orderLineCopy.LineCharges.LineCharge[lineCharge].@ChargePerLine = newCharge;
                }
              }
            }
          }

          for (let origLineTax = 0; origLineTax < origLineTaxes.children().length(); origLineTax++) {
            var originalLineTax = getTwoDecimalMoneyFormatForProration(origLineTaxes.LineTax[origLineTax].@Tax);
			orderAPILogger.warn("Order {0} originalLineTax: {1} getTwoDecimalMoneyFormatForProration: {2}", order.getOrderNo(), origLineTaxes.LineTax[origLineTax].@Tax, getTwoDecimalMoneyFormatForProration(origLineTaxes.LineTax[origLineTax].@Tax));
            if (originalLineTax > 0) {
              for (let lineTax = 0; lineTax < orderLineCopy.LineTaxes.children().length(); lineTax++) {
                if ((origLineTaxes.LineTax[origLineTax].@ChargeName == orderLineCopy.LineTaxes.LineTax[lineTax].@ChargeName) &&
                    (origLineTaxes.LineTax[origLineTax].@TaxName == orderLineCopy.LineTaxes.LineTax[lineTax].@TaxName)) {
                  var newTax = getTwoDecimalMoneyFormatForProration(originalLineTax / qty);
				  orderAPILogger.warn("Order {0} newTax: {1} ", order.getOrderNo(), newTax);
                  if (i == qty - 1) {
                    //setting for the first line item.
                    origLineTaxes.LineTax[origLineTax].@Tax = newTax;
                    orderLineCopy.LineTaxes.LineTax[lineTax].@Tax = originalLineTax.subtract(newTax.multiply(new Quantity(qty - 1, 1)));
					orderAPILogger.warn("Order {0} LineTaxes.LineTax[lineTax].@Tax : {1} ", order.getOrderNo(), orderLineCopy.LineTaxes.LineTax[lineTax].@Tax);
                  } else {
                    orderLineCopy.LineTaxes.LineTax[lineTax].@Tax = newTax;
                  }
                }
              }
            }
          }

          orderXML.OrderLines.appendChild(orderLineCopy);
        }
			}
		}
	}

	return orderXML;
}

function appendGiftCardInfo(orderXML, order, paymentInstrument, paymentSequence) {
	var orderHoldType = '';

	if ('orderStatus' in order.custom && order.custom.orderStatus != null) {
		var holdTypes = order.custom.orderStatus;
		for (let ij = 0; ij < holdTypes.length; ij++) {
			orderHoldType = order.custom.orderStatus[ij].value;
		}
	}
	Logger.debug('SFSX-3637 ChargeSequence: {0}', paymentInstrument.custom.chargeSequence);
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@ChargeSequence = emptyStringIfNull(paymentInstrument.custom.chargeSequence);
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@SvcNo = emptyStringIfNull(paymentInstrument.custom.giftCardNumber);

	var authAmount = paymentInstrument.paymentTransaction.amount.value;
	if ('authAmountExcludingPreOrder' in paymentInstrument.custom) {
		authAmount = Number(paymentInstrument.custom.authAmountExcludingPreOrder);
	}
	if (!orderActionModify && orderHoldType.indexOf('AUTH_HOLD') == -1 && orderHoldType.indexOf('TOKEN_HOLD') == -1) {
	  orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@AuthorizationExpirationDate = "2500-01-01T00:00:00";
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@AuthorizationID = emptyStringIfNull(paymentInstrument.custom.authorization_code);
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@ChargeType = emptyStringIfNull(preferences.paymentChargeType);
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@Reference1 = "";
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@ProcessedAmount = getTwoDecimalMoneyFormat(authAmount);
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@RequestAmount = getTwoDecimalMoneyFormat(authAmount);
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@TranReturnMessage = emptyStringIfNull(paymentInstrument.custom.merchant_reference_number);
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@AuthAvs = emptyStringIfNull(paymentInstrument.custom.avs_response_code);
		if (preOrderObj && preOrderObj.completePreOrder) {
			orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@RequestProcessed = 'Y';
		} else if (authAmount == 0) {
			orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@RequestProcessed = 'Y';
		}

		var cardNumLen = new String(paymentInstrument.custom.giftCardNumber).length;

		orderXML.PaymentMethods.PaymentMethod[paymentSequence].@DisplayCardNo = emptyStringIfNull(new String(paymentInstrument.custom.giftCardNumber).substring(cardNumLen-4));
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].@DisplaySvcNo = emptyStringIfNull(new String(paymentInstrument.custom.giftCardNumber).substring(cardNumLen-4));

		if ("response_code" in paymentInstrument.custom && paymentInstrument.custom.response_code !="") {
			orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@AuthReturnCode = paymentInstrument.custom.response_code;
		} else {
			orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@AuthReturnCode = "1";
		}
	} // validate the order token

	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@MaxChargeLimit = getTwoDecimalMoneyFormat(authAmount);
	var gcCardType = 'giftCardType' in paymentInstrument.custom && paymentInstrument.custom.giftCardType ?
                   getPaymentType(paymentInstrument.custom.giftCardType.toLowerCase()) :
                   getPaymentType(paymentInstrument.paymentMethod);
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentType = emptyStringIfNull(gcCardType);
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentService = "REGULAR";
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@UnlimitedCharges = "N";

	var authDate = getSterlingDateOnlyFormat(emptyStringIfNull(paymentInstrument.custom.authorization_date));

  orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentReference4 = emptyStringIfNull(authDate) + ","
    + emptyStringIfNull(paymentInstrument.custom.authorization_terminal_number) + ","
    + emptyStringIfNull(paymentInstrument.custom.authorization_tracer) + ","
    + emptyStringIfNull(ipaConstants.STORE_NUMBER);

	// add the Tender Tender Total
	if (preOrderObj.hasOnePreOrderItem) {
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].Extn.@ExtnTenderMaxLimit = getTwoDecimalMoneyFormat(paymentInstrument.paymentTransaction.amount);
	}

	orderXML = appendBillingInfoInPayment(orderXML, order, paymentSequence);
	return orderXML;
}

function appendPaypalInfo(orderXML, order, paymentInstrument, paymentSequence) {
	var authAmount = paymentInstrument.paymentTransaction.amount.value;
	if ('authAmountExcludingPreOrder' in paymentInstrument.custom) {
		authAmount = Number(paymentInstrument.custom.authAmountExcludingPreOrder);
	}
	Logger.debug('SFSX-3637 ChargeSequence: {0}', paymentInstrument.custom.chargeSequence);

	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@ChargeSequence = emptyStringIfNull(paymentInstrument.custom.chargeSequence);
  // Conflict Resolution.  Removed emptyStringIfNull() from next two lines and left parameters that LiveArea team ussed.
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@MaxChargeLimit = getTwoDecimalMoneyFormat(authAmount);
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentType = getPaymentType(paymentInstrument.paymentMethod);
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@CheckReference = paymentInstrument.custom.paypalMsgSubID;

	var orderHoldType = '';
	if ('orderStatus' in order.custom && order.custom.orderStatus != null) {
		var holdTypes = order.custom.orderStatus;
		for (let ij = 0; ij < holdTypes.length; ij++) {
			orderHoldType = order.custom.orderStatus[ij].value;
		}
	}

	if (!orderActionModify && orderHoldType.indexOf('AUTH_HOLD') == -1 && orderHoldType.indexOf('TOKEN_HOLD') == -1) {
	  if (paymentInstrument.custom.authorization_expiration_date != null) {
      var ccAuthExp = emptyStringIfNull(paymentInstrument.custom.authorization_expiration_date) + "T00:00:00";
      orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@AuthorizationExpirationDate = ccAuthExp;
		} else {
      var paypalAuthExp = new Calendar(order.getCreationDate());
      paypalAuthExp.add(Calendar.DAY_OF_YEAR, 30);
      paypalAuthExp = paypalAuthExp.getTime();

      orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@AuthorizationExpirationDate = getSterlingDateFormat(emptyStringIfNull(paypalAuthExp.toISOString()));
		}

    orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@AuthorizationID = emptyStringIfNull(paymentInstrument.custom.ipa_authorization_id);
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@ChargeType ="AUTHORIZATION";
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@ProcessedAmount = getTwoDecimalMoneyFormat(emptyStringIfNull(authAmount));
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@Reference1 = "";
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@RequestAmount = getTwoDecimalMoneyFormat(emptyStringIfNull(authAmount));
		if (preOrderObj && preOrderObj.completePreOrder) {
			orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@RequestProcessed = 'Y';
		} else if (authAmount == 0) {
			orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@RequestProcessed = 'Y';
		}
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@TranReturnMessage = emptyStringIfNull(paymentInstrument.custom.ipa_transactionID);
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@RequestId = emptyStringIfNull(paymentInstrument.custom.ipa_EASToken);

		if ("response_code" in paymentInstrument.custom && paymentInstrument.custom.response_code != "") {
			orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@AuthReturnCode = emptyStringIfNull(paymentInstrument.custom.response_code);
		} else {
			orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@AuthReturnCode = "1";
		}
	}// validate the order hold value

  Logger.warn('SFSX-3584 order #: {0} & ipa_token: {1} & paypal_token: {2} & payment method: {3}',
              order.getCurrentOrderNo(), paymentInstrument.custom.ipa_token, paymentInstrument.custom.paypalToken, paymentInstrument.paymentMethod);
  if (paymentInstrument.custom.ipa_token) {
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentReference1 = emptyStringIfNull(paymentInstrument.custom.ipa_token);
  } else if (paymentInstrument.custom.paypalToken) {
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentReference1 = emptyStringIfNull(paymentInstrument.custom.paypalToken);
  } else {
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentReference1 = '';
  }
  if (paymentInstrument.custom.ipa_PayerID) {
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentReference3 = emptyStringIfNull(paymentInstrument.custom.ipa_PayerID);
  } else if (paymentInstrument.custom.paypalPayerID) {
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentReference3 = emptyStringIfNull(paymentInstrument.custom.paypalPayerID);
  } else {
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentReference3 = '';
  }
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentService = "REGULAR";
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@UnlimitedCharges = "N";

	if ('authorization_date' in paymentInstrument.custom && paymentInstrument.custom.authorization_date) {
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentReference4 =
      emptyStringIfNull(getSterlingDateOnlyFormat(paymentInstrument.custom.authorization_date)) + ","
			+ emptyStringIfNull(paymentInstrument.custom.authorization_terminal_number) + ","
			+ emptyStringIfNull(paymentInstrument.custom.authorization_tracer) + ","
			+ emptyStringIfNull(ipaConstants.STORE_NUMBER);
	} else {
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentReference4 = "";
  }

	// add the Tender Tender Total
	if (preOrderObj.hasOnePreOrderItem) {
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].Extn.@ExtnTenderMaxLimit = getTwoDecimalMoneyFormat(paymentInstrument.paymentTransaction.amount);
	}
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentReference9 = "Y|Y";
	orderXML = appendBillingInfoInPayment(orderXML, order, paymentSequence);
	return orderXML;
}

function emptyStringIfNull(str) {
	if (str != null) {
		return str;
  } else {
		return "";
  }
}

function appendCreditCardInfo(orderXML, order, paymentInstrument, paymentSequence) {
	Logger.debug('SFSX-3637 ChargeSequence: {0}', paymentInstrument.custom.chargeSequence);
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@ChargeSequence = emptyStringIfNull(paymentInstrument.custom.chargeSequence);

	var ccAuthExp = emptyStringIfNull(paymentInstrument.custom.authorization_expiration_date) + "T00:00:00";
	var orderHoldType = '';
	if ('orderStatus' in order.custom && order.custom.orderStatus != null) {
		var holdTypes = order.custom.orderStatus;
		for (let ij = 0; ij < holdTypes.length; ij++) {
			orderHoldType = order.custom.orderStatus[ij].value;
		}
	}

	// For the BF Order with INTL_HOLD, set the default authexp
	if (orderHoldType.indexOf('INTL_HOLD') > 0) {
		ccAuthExp = "2500-01-01T00:00:00";
	}

	var cardTokenLength = paymentInstrument.getCreditCardToken() ? paymentInstrument.getCreditCardToken().length : null;
	var lastFourDigitsBasedOnToken = cardTokenLength ? paymentInstrument.getCreditCardToken().substring(cardTokenLength - 4) : '';
	var authAmount = paymentInstrument.paymentTransaction.amount.value;
  if ('authAmountExcludingPreOrder' in paymentInstrument.custom) {
    authAmount = Number(paymentInstrument.custom.authAmountExcludingPreOrder);
  }

  if (!orderActionModify && orderHoldType.indexOf('AUTH_HOLD') == -1 && orderHoldType.indexOf('TOKEN_HOLD') == -1) {
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@AuthorizationExpirationDate = ccAuthExp;
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@AuthorizationID = emptyStringIfNull(paymentInstrument.custom.authorization_code);
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@ChargeType = "AUTHORIZATION";
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@ProcessedAmount = getTwoDecimalMoneyFormat(emptyStringIfNull(authAmount));
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@Reference1 = "";
    if (paymentInstrument && paymentInstrument.creditCardType && getCreditCardType(paymentInstrument.creditCardType) == 'AMEX') {
      var reqId = emptyStringIfNull(paymentInstrument.custom.authorization_reference_data_code) + "|" +
                  emptyStringIfNull(paymentInstrument.custom.authorization_pos_data_code);
      orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@RequestId = reqId;
    }
    if (tsysHelper.isTsysMode()) {
      if (paymentInstrument && paymentInstrument.creditCardType &&
          (getCreditCardType(paymentInstrument.creditCardType) == 'SAKS' || getCreditCardType(paymentInstrument.creditCardType) == 'SAKSMC')) {
        orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@RequestId = emptyStringIfNull(paymentInstrument.custom.authorization_reference_data_code);
      }
    }

    orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@Reference2 = emptyStringIfNull(paymentInstrument.custom.cvv_response_code);
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@RequestAmount = getTwoDecimalMoneyFormat(emptyStringIfNull(authAmount));
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@TranReturnMessage = emptyStringIfNull(paymentInstrument.custom.merchant_reference_number);
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@AuthAvs = emptyStringIfNull(paymentInstrument.custom.avs_response_code);
    if ("response_code" in paymentInstrument.custom && paymentInstrument.custom.response_code !="") {
      orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@AuthReturnCode = emptyStringIfNull(paymentInstrument.custom.response_code);
    } else {
      orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@AuthReturnCode = "1";
    }
    if (preOrderObj && preOrderObj.completePreOrder) {
      orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@RequestProcessed = 'Y';
    } else if (authAmount == 0) {
      orderXML.PaymentMethods.PaymentMethod[paymentSequence].PaymentDetails.@RequestProcessed = 'Y';
    }
	} // VALIDATE AUTH HOLD CONDITION

	Logger.debug("OrderAPIUtil orderID:{0} creditCardExpirationMonth :{1} creditCardExpirationYear:{2} authorization_tracer : {3}",
               order.currentOrderNo, paymentInstrument.creditCardExpirationMonth,
               paymentInstrument.creditCardExpirationYear, paymentInstrument.custom.authorization_tracer);

	if (!empty(paymentInstrument.creditCardExpirationMonth) && !empty(paymentInstrument.creditCardExpirationYear)) {
	  orderXML.PaymentMethods.PaymentMethod[paymentSequence].@CreditCardExpDate =
      StringUtils.formatNumber(paymentInstrument.creditCardExpirationMonth.toString(), '00') + "/" + paymentInstrument.creditCardExpirationYear;
	} else {
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].@CreditCardExpDate = '';
	}

	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@CreditCardName =
    globalHelper.replaceSpecialCharsAndEmojis(emptyStringIfNull(paymentInstrument.creditCardHolder));

  var creditCardNo = paymentInstrument.getCreditCardToken() ? paymentInstrument.getCreditCardToken() : '';
  creditCardNo = creditCardNo && creditCardNo.length === 29 ? creditCardNo.substr(0,16) : creditCardNo;
  orderXML.PaymentMethods.PaymentMethod[paymentSequence].@CreditCardNo = creditCardNo;

	var creditCardType = getCreditCardType(paymentInstrument.creditCardType);
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@CreditCardType = emptyStringIfNull(creditCardType);

	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@DisplayCreditCardNo =
    paymentInstrument.creditCardNumberLastDigits ? paymentInstrument.creditCardNumberLastDigits : lastFourDigitsBasedOnToken;

	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@DisplayCardNo =
    paymentInstrument.creditCardNumberLastDigits ? paymentInstrument.creditCardNumberLastDigits : lastFourDigitsBasedOnToken;
	if ('kpIsVCN' in order.custom && order.custom.kpIsVCN == true) { // Klarna-specific custom value.
		var klarnaPaymentPlan = 'klarnaPaymentPlan' in order.custom && order.custom.klarnaPaymentPlan ? JSON.parse(order.custom.klarnaPaymentPlan) : '';
		var strKlarnaPaymentPlan = Object.keys(klarnaPaymentPlan).map(function(key){return klarnaPaymentPlan[key]}).toString();
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentReference2 = 'KLARNA';
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].@CheckReference = strKlarnaPaymentPlan;
	}
  
	var name = new String(paymentInstrument.creditCardHolder).split(" ");
	if (name.length >= 2) {
		var firstName = emptyStringIfNull(name[0]);
		firstName = globalHelper.replaceSpecialCharsAndEmojis(firstName);
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].@FirstName = firstName;

		var lastName = emptyStringIfNull(name[1]);
		lastName = globalHelper.replaceSpecialCharsAndEmojis(lastName);
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].@LastName = lastName;
	} else {
		var firstName = emptyStringIfNull(paymentInstrument.creditCardHolder);
		firstName = globalHelper.replaceSpecialCharsAndEmojis(firstName);
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].@FirstName = firstName;
	}
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@MaxChargeLimit = getTwoDecimalMoneyFormat(emptyStringIfNull(authAmount));
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentType = getPaymentType(emptyStringIfNull(paymentInstrument.creditCardType));

	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentReference1 =
    paymentInstrument.creditCardNumberLastDigits ? paymentInstrument.creditCardNumberLastDigits : lastFourDigitsBasedOnToken;

	Logger.warn('SFSX-3584 order #: {0} & PaymentReference1 {1} & payment method: {2} & card_token: {3}',
              order.getCurrentOrderNo(), paymentInstrument.creditCardNumberLastDigits, paymentInstrument.paymentMethod, paymentInstrument.getCreditCardToken());

	if ('authorization_date' in paymentInstrument.custom && paymentInstrument.custom.authorization_date) {
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentReference4 =
      emptyStringIfNull(getSterlingDateOnlyFormat(paymentInstrument.custom.authorization_date)) + ","
			+ emptyStringIfNull(paymentInstrument.custom.authorization_terminal_number) + ","
			+ emptyStringIfNull(paymentInstrument.custom.authorization_tracer) + ","
			+ emptyStringIfNull(ipaConstants.STORE_NUMBER);

    if (tsysHelper.isTsysMode()) {
      if ('authorization_terminal_capability' in paymentInstrument.custom) {
        orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentReference4 +=
          "," + emptyStringIfNull(paymentInstrument.custom.authorization_terminal_capability)
      }
      if ('card_acceptor_id' in paymentInstrument.custom) {
        orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentReference4 +=
          "," + emptyStringIfNull(paymentInstrument.custom.card_acceptor_id);
      }
    }
	} else {
    orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentReference4 = "";
  }

	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentReference6 = emptyStringIfNull(preferences.omsTokenizationCode);
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@PaymentService = "REGULAR";
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].@UnlimitedCharges = "N";

	// add the Tender Tender Total
	if (preOrderObj.hasOnePreOrderItem) {
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].Extn.@ExtnTenderMaxLimit = getTwoDecimalMoneyFormat(paymentInstrument.paymentTransaction.amount);
	}

	orderXML = appendBillingInfoInPayment(orderXML, order, paymentSequence);
	return orderXML;
}

function getTwoDecimalMoneyFormat(value) {
	var numVal = new Number(value);
	if (numVal < 0) {
		return new Money(new Number(0), currencyCode).toNumberString();
	} else {
		return new Money(numVal, currencyCode).toNumberString();
	}
}

function getMoneyFormat(value) {
	var numVal = new Number(value);
	if (numVal < 0) {
		return new Money(new Number(0), currencyCode);
	} else {
		return new Money(numVal, currencyCode);
	}
}

function getTwoDecimalMoneyFormatForProration(value) {
	return new Money(new Number(value), currencyCode);
}

function getSterlingDateFormat(date) {
	return new String(date).substr(0,19);
}

function getSterlingDateOnlyFormat(date) {
	return new String(date).substr(0, 10);
}

function getPaymentType(type) {
  var cardType = type;
  try {
    type = type.replace(' ','').toLowerCase(); // eslint-disable-line
    if (type === 'gift_card') {
      type = 'giftcard';
    }
    var ccType = JSON.parse(ipaConstants.PAYMENT_CARD_TYPE_OMS);
    cardType = ccType[type];
  } catch (e) {
    cardType = type.toUpperCase();
  }
  return cardType;
}

function getCreditCardType(type) {
  var cardType = type;
  try {
    type = type.replace(' ','').toLowerCase(); // eslint-disable-line
    var ccType = JSON.parse(ipaConstants.OMS_CARD_TYPE_MAP);
    cardType = ccType[type];
  } catch (e) {
    cardType = type.toUpperCase();
  }
  return cardType;
}

function getProratedPriceCustom(lineItem, order) {
	var proratedPriceDiscount = new Money(0, order.getCurrencyCode());
	var discount;
  if (lineItem.custom.productPromotionDiscountWOPricebook != null ) {
    discount = new Money(lineItem.custom.productPromotionDiscountWOPricebook, order.getCurrencyCode());
    proratedPriceDiscount = proratedPriceDiscount.add(discount);
  }
  if (lineItem.custom.productPromotionAssociateDiscount != null) {
    discount = new Money(lineItem.custom.productPromotionAssociateDiscount, order.getCurrencyCode());
    proratedPriceDiscount = proratedPriceDiscount.add(discount);
  }
  if (lineItem.custom.productPromotionSpecialVendorDiscount != null) {
    discount = new Money(lineItem.custom.productPromotionSpecialVendorDiscount, order.getCurrencyCode());
    proratedPriceDiscount = proratedPriceDiscount.add(discount);
  }
  if (lineItem.custom.productPromotionFirstDayDiscount != null) {
    discount = new Money(lineItem.custom.productPromotionFirstDayDiscount, order.getCurrencyCode());
    proratedPriceDiscount = proratedPriceDiscount.add(discount);
  }
  return lineItem.getNetPrice().subtract(proratedPriceDiscount);
}

function appendLineLevelTaxInfo(orderXML, order , pli , itemNumber, giftOptionsPA) {
  orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax += <LineTax/>;
  orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax[0].@ChargeCategory = "PRICE";
  orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax[0].@ChargeName = "PRICE";
  orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax[0].@Tax = getTwoDecimalMoneyFormat(pli.getTax());
  orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax[0].@TaxName = "General Sales and Use Tax";
  
  var plitaxRate = countDecimalDigits(new Decimal(pli.getTaxRate())) > 5 ? new Decimal(pli.getTaxRate()).round(5) : new Decimal(pli.getTaxRate()); 
  orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax[0].@TaxPercentage = plitaxRate;
  if ('inventoryStatus' in pli.custom && pli.custom.inventoryStatus == 'REGULAR') {
    instockOrderTotal += new Money(pli.getTax().value, order.getCurrencyCode()).value;
  }
  computedOrderTotal += new Money(pli.getTax().value, order.getCurrencyCode()).value;

  var shippingPriceTax = 0;
  var shipmentID = pli.getShipment().ID;
  if (shippingTax.get(shipmentID) == null) {
    shippingTax.put(shipmentID, 0);
  }

  if (pli.getShipment().productLineItems.length == pliCount.get(shipmentID)) {
    if (('isNonTaxableProduct' in pli.custom && pli.custom.isNonTaxableProduct) || pli.adjustedPrice.value == 0) {
      shippingPriceTax = 0;
    } else {
      shippingPriceTax = Money (pli.getShipment().adjustedShippingTotalTax - shippingTax.get(shipmentID), order.getCurrencyCode());
    }
    var shippingTaxNumber = shippingPriceTax.class && shippingPriceTax instanceof dw.value.Money ? shippingPriceTax.value : shippingPriceTax;
    var shippingTaxMoney = new Money(shippingTaxNumber, order.getCurrencyCode());
    if ('inventoryStatus' in pli.custom && pli.custom.inventoryStatus == 'REGULAR') {
      instockOrderTotal += shippingTaxMoney.value;
    }
    computedOrderTotal += shippingTaxMoney.value;
    if (giftOptionsPA && pli.shipment.custom.hasOwnProperty('giftWrapType') && pli.shipment.custom.giftWrapType === 'giftpack' &&
        pli.product && pli.product.custom && pli.product.custom.hasOwnProperty('giftWrapEligible') && pli.product.custom.giftWrapEligible === 'true') {
      var giftwrapTax = getTwoDecimalMoneyFormat(pli.custom.proratedGiftTax);
      if ('inventoryStatus' in pli.custom && pli.custom.inventoryStatus == 'REGULAR') {
        instockOrderTotal += Number(giftwrapTax);
      }
      computedOrderTotal += Number(giftwrapTax);
    }
    if (!firstRegularTaxableLineShipTax && !firstRegularTaxableLineNo) {
      // store the first regular item shipping tax
      if ('inventoryStatus' in pli.custom && pli.custom.inventoryStatus == 'REGULAR') {
        firstRegularTaxableLineShipTax = Money (shippingTaxNumber, order.getCurrencyCode());
        firstRegularTaxableLineNo = itemNumber;
        firstRegularTaxableLineShipTaxIndex = 1;
      }
    }

    if (preOrderObj.hasOnePreOrderItem) {
      /**
       *  Developer notes
       *  orderInstockOrderTotal - is the auth amount prorated for the regular items
       *  instockOrderTotal -  is the same amount recalculated on the order create xml
       *  if there is a penny difference
       * 		a.  positive penny difference
       * 			we add the additional penny to the shipping tax of the first regular item
       * 		b.  negative penny difference
       * 			instock auth amount is lesser than regular item total
       * 			we remove the additional penny on the shipping tax
       * 			and add the same penny to the first preorder lineitem -  this is done to not loose money
       * 				in this case if shipping tax is zero
       * 					a. we do not remove any penny ?
       * 					b. we add the difference penny to the preorder lineitem ?
       */
      instockOrderTotal = new Money(instockOrderTotal, order.getCurrencyCode()).value;
      orderInstockOrderTotal = Number(orderInstockOrderTotal);
      if (orderInstockOrderTotal != instockOrderTotal) {
        // positive difference use case
        // regular auth amount is a penny greater than the order lines in the order create xml
        // add the additional penny to the shipping tax
        var diff = new Money(orderInstockOrderTotal - instockOrderTotal, order.getCurrencyCode());
        firstRegularTaxableLineShipTax = firstRegularTaxableLineShipTax.add(diff);
        // Update the Request
        firstRegularPennyUpdate = true;
      }
    }
    // If last item was non taxable and we need to adjust the penny difference
    if (shippingPriceTax === 0 && firstTaxableLineShipTax) {
      var pennyDifferenceTax = Money (pli.getShipment().adjustedShippingTotalTax - shippingTax.get(shipmentID), order.getCurrencyCode());
      firstTaxableLineShipTax = firstTaxableLineShipTax.add(pennyDifferenceTax);
      // Update the Request
      orderXML.OrderLines.OrderLine[firstTaxableLineNo].LineTaxes.LineTax[firstTaxableLineShipTaxIndex].@Tax =
        getTwoDecimalMoneyFormat(firstTaxableLineShipTax);
    }
  } else {
    if (('isNonTaxableProduct' in pli.custom && pli.custom.isNonTaxableProduct) || pli.adjustedPrice.value == 0) {
      shippingPriceTax = 0;
    } else {
      var shipmntPrice = new Number(pli.getShipment().getAdjustedMerchandizeTotalPrice(true));
      if (shipmntPrice.valueOf() !== 0) {
        var adjustedShippingTotalTax = pli.getShipment().adjustedShippingTotalTax;
        var proratedPriceCustom = getProratedPriceCustom(pli,order);
        var adjustedMerchandizeTotalPrice = pli.getShipment().getAdjustedMerchandizeTotalPrice(true);
        if (adjustedShippingTotalTax.value > 0 && proratedPriceCustom.value > 0 && adjustedMerchandizeTotalPrice.value > 0) {
          shippingPriceTax = new Decimal(adjustedShippingTotalTax).abs()
            .multiply(new Number(proratedPriceCustom)).divide(new Number(adjustedMerchandizeTotalPrice));
        }
      }
      // Store the First Taxable item shipping tax which we can use for penny difference adjustment.
      if (!firstTaxableLineShipTax && !firstTaxableLineNo) {
        firstTaxableLineShipTax = Money (shippingPriceTax, order.getCurrencyCode());
        firstTaxableLineNo = itemNumber;
        firstTaxableLineShipTaxIndex = 1;
      }
    }
    if (!firstRegularTaxableLineShipTax && !firstRegularTaxableLineNo) {
      // store the first regular item shipping tax
      if ('inventoryStatus' in pli.custom && pli.custom.inventoryStatus == 'REGULAR') {
        firstRegularTaxableLineShipTax = Money (shippingPriceTax, order.getCurrencyCode());
        firstRegularTaxableLineNo = itemNumber;
        firstRegularTaxableLineShipTaxIndex = 1;
      }
    }

    shippingPriceTax = Money (shippingPriceTax, order.getCurrencyCode());
    shippingTax.put(shipmentID ,shippingTax.get(shipmentID) + shippingPriceTax);
    var shippingTaxNumber = shippingPriceTax.class && shippingPriceTax instanceof dw.value.Money ? shippingPriceTax.value : shippingPriceTax;
    var shippingTaxMoney = new Money(shippingTaxNumber, order.getCurrencyCode());
    if ('inventoryStatus' in pli.custom && pli.custom.inventoryStatus == 'REGULAR') {
      instockOrderTotal += shippingTaxMoney.value;
    }
    computedOrderTotal += shippingTaxMoney.value;

    if (giftOptionsPA && pli.shipment.custom.hasOwnProperty('giftWrapType') && pli.shipment.custom.giftWrapType === 'giftpack' &&
        pli.product && pli.product.custom && pli.product.custom.hasOwnProperty('giftWrapEligible') && pli.product.custom.giftWrapEligible === 'true') {
	  var giftwrapTax = getTwoDecimalMoneyFormat(pli.custom.proratedGiftTax);
      if ('inventoryStatus' in pli.custom && pli.custom.inventoryStatus == 'REGULAR') {
        instockOrderTotal += Number(giftwrapTax);
      }
      computedOrderTotal += Number(giftwrapTax);
    }
  }
  // If there are many lineitems, we may end up having rounding off issues, which may cause the prorated tax to be negative
  // in order to fix the negative tax, sum up all the prorated tax amounts and equate it to shippingTotalTax, if it is equal or greater - then consider the
  // shipping tax to be zero as the prorated value is bound to be negative amount.
  // if we round up just the negative tax it will cause differences in the total tax amount
  if (proratedShippingTaxTotal >= pli.getShipment().adjustedShippingTotalTax) {
    shippingPriceTax = Money (0, order.getCurrencyCode());
  }

  // Final check for zero shipping charge
  for each (lineCharge in orderXML.OrderLines.OrderLine[itemNumber].LineCharges.LineCharge) {
    if (lineCharge.@ChargeName == "SHIPPINGCHARGE") {
      // if the shipping charge is <= 0 then make sure tax is 0 as well
      if (lineCharge.@ChargePerLine <= 0) {
        shippingPriceTax = Money (0, order.getCurrencyCode());
      }
      break;
    }
  }

  var shippingTaxNumber = shippingPriceTax.class && shippingPriceTax instanceof dw.value.Money ? shippingPriceTax.value : shippingPriceTax;
  var shippingTaxMoney = new Money(shippingTaxNumber, order.getCurrencyCode());
  proratedShippingTaxTotal = proratedShippingTaxTotal + shippingTaxMoney.value;

  if (!firstLineShipTax && !firstLineNo) {
    firstLineShipTax = Money (shippingTaxMoney.value, order.getCurrencyCode());
    firstLineNo = itemNumber;
    firstLineShipTaxIndex = 1;
  }

  orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax += <LineTax/>;
  orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax[1].@ChargeCategory = "SHIPPINGCHARGE";
  orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax[1].@ChargeName = "SHIPPINGCHARGE";
  orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax[1].@Tax = getTwoDecimalMoneyFormat(shippingPriceTax);
  orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax[1].@TaxName = "General Sales and Use Tax";

  var shiptaxRate = countDecimalDigits(new Decimal(pli.getShipment().standardShippingLineItem.getTaxRate())) > 5 ?
                    new Decimal(pli.getShipment().standardShippingLineItem.getTaxRate()).round(5) :
                    new Decimal(pli.getShipment().standardShippingLineItem.getTaxRate());
 
  orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax[1].@TaxPercentage = shiptaxRate;
  if (giftOptionsPA && pli.shipment.custom.hasOwnProperty('giftWrapType') && pli.shipment.custom.giftWrapType === 'giftpack' &&
      pli.product && pli.product.custom && pli.product.custom.hasOwnProperty('giftWrapEligible') && pli.product.custom.giftWrapEligible === 'true') {
    var giftwrapTax = getTwoDecimalMoneyFormat(pli.custom.proratedGiftTax);
    orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax += <LineTax/>;
    orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax[2].@ChargeCategory = "GIFTWRAPCHARGE";
    orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax[2].@ChargeName = "GIFTWRAPCHARGE";
    orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax[2].@Tax = giftwrapTax;
    orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax[2].@TaxName = "General Sales and Use Tax";

    var gifttaxRate = countDecimalDigits(new Decimal(giftOptionsPA.getTaxRate())) > 5 ?
                      new Decimal(giftOptionsPA.getTaxRate()).round(5) :
                      new Decimal(giftOptionsPA.getTaxRate());
	
    orderXML.OrderLines.OrderLine[itemNumber].LineTaxes.LineTax[2].@TaxPercentage = gifttaxRate;
  }
  if (firstRegularPennyUpdate) {
	orderAPILogger.warn("Order Number: {0} \t Original Tax amount: {1} \t New Tax Amount: {2} ", 
		order.getOrderNo(), shippingPriceTax, getTwoDecimalMoneyFormat(firstRegularTaxableLineShipTax));

    orderXML.OrderLines.OrderLine[firstRegularTaxableLineNo].LineTaxes.LineTax[firstRegularTaxableLineShipTaxIndex].@Tax =
      getTwoDecimalMoneyFormat(firstRegularTaxableLineShipTax);

    firstRegularPennyUpdate = false;
  }
  if (noOfProducts == order.productLineItems.length) {
    computedOrderTotal = new Money(computedOrderTotal, order.getCurrencyCode()).value;
    if (computedOrderTotal != sfccOrderTotal && firstLineShipTax) {
      var diff = new Money(computedOrderTotal - sfccOrderTotal, order.getCurrencyCode());
      firstLineShipTax = firstLineShipTax.add(diff);
	  // Update the Request
      orderXML.OrderLines.OrderLine[firstLineNo].LineTaxes.LineTax[firstLineShipTaxIndex].@Tax = getTwoDecimalMoneyFormat(firstLineShipTax);
      orderAPILogger.warn("Order Number: {0}, Original Tax Amout: {1}, Computed Order total: {2}, diff: {3}, firstLineShipTax: {4}", 
	  	order.getOrderNo(), shippingPriceTax, computedOrderTotal, diff, firstLineShipTax); 
    }
  }

  

    var suspect = createLogMsg(orderXML, false);
	if (suspect) {
		createLogMsg(orderXML, true);
	}
  
	return orderXML;
}

function createLogMsg(orderXML, printInLog) {
	try {
		if (orderXML) {
			var suspect = false;
			var orderNo = orderXML.@OrderNo;
			var logMsg = "";
			if (printInLog) {
			  logMsg = "OrderNo: " +orderNo + "\t"; 
			}
			var map = new HashMap();
			for (var i=0; i< orderXML.OrderLines.children().length(); i++) {
				var orderLine = orderXML.OrderLines.children()[i];
				if (orderLine) {
					var itemID = orderLine.Item.@ItemID;
					var listPrice = orderLine.LinePriceInfo.@ListPrice;
					var orderedQty = orderLine.@OrderedQty;
					var lineCharges = orderLine.LineCharges;
					if (printInLog) {
						
						logMsg += "itemID: " +itemID + "\t"; 
						logMsg += "listPrice: " +listPrice + "\t"; 
					}
					if (lineCharges) {
						for (var j=0;j<lineCharges.children().length(); j++) {
							var lineCharge = lineCharges.children()[j];
							var chargeName = lineCharge.@ChargeName;
							var chargePerLine = lineCharge.@ChargePerLine;
							var chargeCategory = lineCharge.@ChargeCategory;
							if (printInLog) {
								logMsg += " \t chargeName: " + chargeName + "\t";
								logMsg += " \t chargePerLine: " + chargePerLine + "\t";
								logMsg += " \t chargeCategory: " + chargeCategory + "\t";
							}
							map.put(itemID.toString() + "_" + chargeCategory.toString(), chargePerLine);
						}
					}
					var lineTaxes = orderLine.LineTaxes;
					if (lineTaxes) {
						for (var j=0;j<lineTaxes.children().length(); j++) {
							var lineTax = lineTaxes.children()[j];
							var chargeCategory = lineTax.@ChargeCategory;
							var tax = lineTax.@Tax;
							var taxPercentage = lineTax.@TaxPercentage;
							
							if (printInLog){
								logMsg += "TaxCategory: " + chargeCategory + "\t";
								logMsg += "tax: " + tax + "\t";
								logMsg += "taxPercentage: " + taxPercentage + "\t \n\n";
							}
							var chargePerLine = map.get(itemID.toString() + "_" + chargeCategory.toString());
							if (tax > 0 && chargePerLine == 0) {
								suspect = true;
							}
							else if (tax >= 0 && chargePerLine > 0) {
								if (tax / (chargePerLine * 100) >= 10) {
									suspect = true;
								}
							}
						}
					}
					if (printInLog) {
						var orderAPILogger = Logger.getLogger('OrderAPIUtils', 'OrderAPIUtils');
						orderAPILogger.warn(logMsg);
					}
				}
			}
			return suspect;
		}
	} catch (error) {
		orderAPILogger.warn(error);
	}
  }
function appendLineLevelShippingInfo(orderXML, order, pli, itemNumber) {
	var firstName = emptyStringIfNull(pli.shipment.shippingAddress.firstName);
	var lastName = emptyStringIfNull(pli.shipment.shippingAddress.lastName);
	// if line item is BOPIS
	if (pli.custom && pli.custom.fromStoreId && order.custom && order.custom.personInfoMarkFor) {
		var pickUpData = JSON.parse(order.custom.personInfoMarkFor);
		var pickUpFullName = pickUpData.fullName || "";
		var pickUpFullNameArray = pickUpFullName.split(" ");
		firstName = pickUpFullNameArray.shift();
		lastName = pickUpFullNameArray.join("");
	}

	orderXML.OrderLines.OrderLine[itemNumber].PersonInfoShipTo.@FirstName = globalHelper.replaceSpecialCharsAndEmojis(firstName);
	orderXML.OrderLines.OrderLine[itemNumber].PersonInfoShipTo.@LastName = globalHelper.replaceSpecialCharsAndEmojis(lastName);
	orderXML.OrderLines.OrderLine[itemNumber].PersonInfoShipTo.@AddressLine1 = globalHelper.replaceSpecialCharsAndEmojis(pli.shipment.shippingAddress.address1);

	if (pli.shipment.shippingAddress.address2 == null || pli.shipment.shippingAddress.address2 == "NULL") {
		orderXML.OrderLines.OrderLine[itemNumber].PersonInfoShipTo.@AddressLine2 = "";
	} else {
		orderXML.OrderLines.OrderLine[itemNumber].PersonInfoShipTo.@AddressLine2 = globalHelper.replaceSpecialCharsAndEmojis(pli.shipment.shippingAddress.address2);
	}
	orderXML.OrderLines.OrderLine[itemNumber].PersonInfoShipTo.@City = globalHelper.replaceSpecialCharsAndEmojis(pli.shipment.shippingAddress.city);
	orderXML.OrderLines.OrderLine[itemNumber].PersonInfoShipTo.@ZipCode = pli.shipment.shippingAddress.postalCode;
	if (pli.shipment.shippingAddress.stateCode == null) {
		orderXML.OrderLines.OrderLine[itemNumber].PersonInfoShipTo.@State = "";
	} else {
		orderXML.OrderLines.OrderLine[itemNumber].PersonInfoShipTo.@State = globalHelper.replaceSpecialCharsAndEmojis(pli.shipment.shippingAddress.stateCode);
	}

	orderXML.OrderLines.OrderLine[itemNumber].PersonInfoShipTo.@Country = getCountryCode(pli.shipment.shippingAddress);
	if (pli.shipment.shippingAddress.phone != null) {
		var phone = pli.shipment.shippingAddress.phone;
    phone = phone.replace(/\D/g, '');
		orderXML.OrderLines.OrderLine[itemNumber].PersonInfoShipTo.@DayPhone = phone;
	} else {
		orderXML.OrderLines.OrderLine[itemNumber].PersonInfoShipTo.@DayPhone = preferences.defaultPhoneNumber;
	}
	var selerProtectionShipment = getSellerProtectionShipment(order);
	if (selerProtectionShipment) {
		if (selerProtectionShipment.getID() == pli.shipment.getID()) {
		  orderXML.OrderLines.OrderLine[itemNumber].PersonInfoShipTo.@AddressLine4 = "Y";
		} else {
		  orderXML.OrderLines.OrderLine[itemNumber].PersonInfoShipTo.@AddressLine4 = "N";
		}
	}

	orderXML.OrderLines.OrderLine[itemNumber].PersonInfoShipTo.@EMailID = order.customerEmail;
	return orderXML;
}

function appendOrderLevelShippingInfo(orderXML, order) {
	var isBopisOnly = false;
	if (order.custom && order.custom.personInfoMarkFor) {
		isBopisOnly = true;
		for (var i = 0; i < order.shipments.length; i++) {
			var shipment = order.shipments[i];
			if (shipment.shippingMethodID != "instore") {
				isBopisOnly = false;
				break;
			}
		}
	}
	var firstName = emptyStringIfNull(order.defaultShipment.shippingAddress.firstName);
	var lastName = emptyStringIfNull(order.defaultShipment.shippingAddress.lastName);

	if (isBopisOnly) {
		var pickUpData = JSON.parse(order.custom.personInfoMarkFor);
		var pickUpFullName = pickUpData.fullName || "";
		var pickUpFullNameArray = pickUpFullName.split(" ");
		firstName = pickUpFullNameArray.shift();
		lastName = pickUpFullNameArray.join("");
	}

	orderXML.PersonInfoShipTo.@FirstName = globalHelper.replaceSpecialCharsAndEmojis(firstName);
	orderXML.PersonInfoShipTo.@LastName = globalHelper.replaceSpecialCharsAndEmojis(lastName);
	orderXML.PersonInfoShipTo.@EMailID = order.customerEmail;

	orderXML.PersonInfoShipTo.@AddressLine1 = globalHelper.replaceSpecialCharsAndEmojis(order.defaultShipment.shippingAddress.address1);
	if (order.defaultShipment.shippingAddress.address2 == null || order.defaultShipment.shippingAddress.address2 == "NULL") {
		orderXML.PersonInfoShipTo.@AddressLine2 = "";
	} else {
		orderXML.PersonInfoShipTo.@AddressLine2 = globalHelper.replaceSpecialCharsAndEmojis(order.defaultShipment.shippingAddress.address2);
	}
	orderXML.PersonInfoShipTo.@City = globalHelper.replaceSpecialCharsAndEmojis(order.defaultShipment.shippingAddress.city);
	if (order.defaultShipment.shippingAddress.stateCode == null) {
		orderXML.PersonInfoShipTo.@State = "";
	} else {
		orderXML.PersonInfoShipTo.@State = globalHelper.replaceSpecialCharsAndEmojis(order.defaultShipment.shippingAddress.stateCode);
	}
	orderXML.PersonInfoShipTo.@ZipCode = order.defaultShipment.shippingAddress.postalCode;
	orderXML.PersonInfoShipTo.@Country = getCountryCode(order.defaultShipment.shippingAddress);
	if (order.defaultShipment.shippingAddress.phone != null) {
		var phone = order.defaultShipment.shippingAddress.phone;
    phone = phone.replace(/\D/g, '');
		orderXML.PersonInfoShipTo.@DayPhone = phone;
	} else {
		orderXML.PersonInfoShipTo.@DayPhone = preferences.defaultPhoneNumber;
	}

	return orderXML;
}

function appendBillingInfoInPayment(orderXML, order, paymentSequence) {
	var firstName = emptyStringIfNull(order.billingAddress.firstName);
	firstName = globalHelper.replaceSpecialCharsAndEmojis(firstName);
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].PersonInfoBillTo.@FirstName = firstName;

	var lastName = emptyStringIfNull(order.billingAddress.lastName);
	lastName = globalHelper.replaceSpecialCharsAndEmojis(lastName);
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].PersonInfoBillTo.@LastName = lastName;

	orderXML.PaymentMethods.PaymentMethod[paymentSequence].PersonInfoBillTo.@AddressLine1 =
    globalHelper.replaceSpecialCharsAndEmojis(order.billingAddress.address1);

	if (order.billingAddress.address2 == null || order.billingAddress.address2 == "NULL") {
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PersonInfoBillTo.@AddressLine2 = "";
	} else {
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PersonInfoBillTo.@AddressLine2 =
      globalHelper.replaceSpecialCharsAndEmojis(order.billingAddress.address2);
	}
	if (!(order.billingAddress.city == null || order.billingAddress.city == "NULL" || order.billingAddress.city == "")) {
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PersonInfoBillTo.@City = globalHelper.replaceSpecialCharsAndEmojis(order.billingAddress.city);
	}
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].PersonInfoBillTo.@ZipCode = order.billingAddress.postalCode;
	if (!(order.billingAddress.stateCode == null || order.billingAddress.stateCode == "NULL" || order.billingAddress.stateCode == "")) {
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PersonInfoBillTo.@State = globalHelper.replaceSpecialCharsAndEmojis(order.billingAddress.stateCode);
	}
	if (!(order.billingAddress.countryCode == null || order.billingAddress.countryCode == "NULL" || order.billingAddress.countryCode == "")) {
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PersonInfoBillTo.@Country = getCountryCode(order.billingAddress);
	}
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].PersonInfoBillTo.@Country = getCountryCode(order.billingAddress);
	orderXML.PaymentMethods.PaymentMethod[paymentSequence].PersonInfoBillTo.@EMailID = order.customerEmail;
	if (order.billingAddress.phone != null) {
		var phone = order.billingAddress.phone;
    	phone = phone.replace(/\D/g, '');
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PersonInfoBillTo.@DayPhone = phone;
	} else {
		orderXML.PaymentMethods.PaymentMethod[paymentSequence].PersonInfoBillTo.@DayPhone = preferences.defaultPhoneNumber;
	}
	return orderXML;
}

function appendOrderLevelBillingInfo(orderXML, order) {
	var firstName = emptyStringIfNull(order.billingAddress.firstName);
	firstName = globalHelper.replaceSpecialCharsAndEmojis(firstName);
	orderXML.PersonInfoBillTo.@FirstName = firstName;

	var lastName = emptyStringIfNull(order.billingAddress.lastName);
	lastName = globalHelper.replaceSpecialCharsAndEmojis(lastName);
	orderXML.PersonInfoBillTo.@LastName = lastName;

	orderXML.PersonInfoBillTo.@AddressLine1 = globalHelper.replaceSpecialCharsAndEmojis(order.billingAddress.address1);
  if (!(order.billingAddress.address2 == null || order.billingAddress.address2 == "NULL")) {
    orderXML.PersonInfoBillTo.@AddressLine2 = globalHelper.replaceSpecialCharsAndEmojis(order.billingAddress.address2);
  }
  if (!(order.billingAddress.city == null || order.billingAddress.city == "NULL" || order.billingAddress.city == "")) {
		orderXML.PersonInfoBillTo.@City = globalHelper.replaceSpecialCharsAndEmojis(order.billingAddress.city);
	}
	if (!(order.billingAddress.stateCode == null || order.billingAddress.stateCode == "NULL" || order.billingAddress.stateCode == "")) {
		orderXML.PersonInfoBillTo.@State = globalHelper.replaceSpecialCharsAndEmojis(order.billingAddress.stateCode);
	}
	if (!(order.billingAddress.countryCode == null || order.billingAddress.countryCode == "NULL" || order.billingAddress.countryCode == "")) {
		orderXML.PersonInfoBillTo.@Country = getCountryCode(order.billingAddress);
	}
	orderXML.PersonInfoBillTo.@ZipCode = order.billingAddress.postalCode;
	orderXML.PersonInfoBillTo.@Country = getCountryCode(order.billingAddress);
	if (order.billingAddress.phone != null) {
		var phone = order.billingAddress.phone;
    phone = phone.replace(/\D/g, '');
		orderXML.PersonInfoBillTo.@DayPhone = phone;
	} else {
		orderXML.PersonInfoBillTo.@DayPhone = preferences.defaultPhoneNumber;
	}
	orderXML.PersonInfoBillTo.@EMailID = order.customerEmail;
	return orderXML;
}

exports.createOrderInOMS = createOrderInOMS;
exports.buildRequestXML = buildRequestXML;
exports.splitGiftMessage = splitGiftMessage;
