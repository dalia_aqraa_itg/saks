/**
 * Description of the Controller and the logic it provides
 *
 * @module  controllers/TestGCBalanceCheck
 */

'use strict';

var server = require('server');
var OrderAPIUtils = require('*/cartridge/scripts/util/OrderAPIUtil');
var Logger = require('dw/system/Logger');
var URLUtils = require('dw/web/URLUtils');
var RootLogger = require('dw/system/Logger').getRootLogger();

// HINT: do not put all require statements at the top of the file
// unless you really need them for all functions

/**
 * Description of the function
 *
 * @return {String} The string 'myFunction'
 */

server.get('MyFunction', function (req, res, next) {
  myFunction(req.querystring.ordernumber);
  //myFunction("123");

  res.render('/home/homePage');
  next();
});

var myFunction = function (ordernumber) {
  var OrderMgr = require('dw/order/OrderMgr');
  var Transaction = require('dw/system/Transaction');
  var Order = require('dw/order/Order');

  var order = OrderMgr.getOrder(ordernumber);

  var responseOrderXML = OrderAPIUtils.createOrderInOMS(order);

  Logger.debug('responseOrderXML -->' + responseOrderXML);
  var resXML = new XML(responseOrderXML);
  Logger.debug('resXML.child(ResponseMessage) -->' + resXML.child('ResponseMessage'));

  if (resXML.child('ResponseMessage') == 'Success') {
    Logger.debug('resXML.child(ResponseMessage) success-->');
    Transaction.wrap(function () {
      order.exportStatus = Order.EXPORT_STATUS_EXPORTED;
      order.trackOrderChange('Order Export Successful');
      order.trackOrderChange(responseOrderXML);
    });
  } else {
    Logger.debug('resXML.child(ResponseMessage) failure-->');

    Transaction.wrap(function () {
      order.trackOrderChange('Order Exported Failed');
      order.trackOrderChange(responseOrderXML);
    });
  }

  Logger.info('OrderAPIUtils IN CONTROLLER responseOrderXML:\n{0}', responseOrderXML);

  return responseOrderXML;
};

/* Exports of the controller */
///**
// * @see {@link module:controllers/TestGCBalanceCheck~myFunction} */
module.exports = server.exports();
