'use strict';

/**
 * determines if this is an OCAPI request.
 *
 * @return {boolean} true/false if current request in OCAPI
 */
exports.isOCAPI = function isOCAPI() {
  return request && request.ocapiVersion;
};
