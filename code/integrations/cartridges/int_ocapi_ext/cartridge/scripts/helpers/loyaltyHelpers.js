'use strict';

var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var collections = require('*/cartridge/scripts/util/collections');

var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var ProductMgr = require('dw/catalog/ProductMgr');
var PaymentInstrument = require('dw/order/PaymentInstrument');
var Money = require('dw/value/Money');

var customerLookupModule = 'app_hbc_loyalty/cartridge/scripts/services/customerLookup';
var normalizeFormattedNumberModule = 'app_hbc_loyalty/cartridge/shared/normalizeFormattedNumber';
var verifyLoyaltyPinModule = 'app_hbc_loyalty/cartridge/scripts/services/verifyLoyaltyPin';

/**
 * This function verifies if a given module is present before using it.
 * It throws a silent exception to assure compatibility with other sites that may not
 * have the module but still make use of the OCAPI (i.e.: SO5 doesn't use the loyalty
 * modules so this method will assure that no exceptions are thrown on SO5 when loyalty
 * modules are called)
 * @param {string} modulePath - given module/class
 * @returns {Object} module - module or null
 */
function getModule(modulePath) {
  var module = null;
  try {
    module = require(modulePath);
  } catch (e) {
    // module not found exception
  }
  return module;
}

/**
 * Verifies if a given customer is a member
 * @param {dw.customer.Customer} customer - given customer
 * @returns {boolean} member - 'true' if customer is a member; 'false' otherwise
 */
function isCustomerMember(customer) {
  var profile = customer.profile;
  var member = profile && 'hudsonReward' in profile.custom && !empty(profile.custom.hudsonReward);

  return member;
}

/**
 * Returns the minimum points in number format
 * @param {string} minPointsString - minimum points (should be a string on the format: $XXX.XX)
 * @returns {number} minPointsNumber - number portion of the given minimum points
 */
function getMinimumPointsNumber(minPointsString) {
  var minPoints = minPointsString.split('$');
  var minPointsNumber;
  try {
    if (minPoints.length > 1) {
      minPointsNumber = Number(minPoints[1]);
    }
  } catch (e) {
    minPointsNumber = null;
  }
  return minPointsNumber;
}

/**
 * Validate points applied to basket
 * @param {number} amount - points to be applied
 * @param {Object} currentUser - customer loyalty data
 * @returns {Object} result - validation result
 */
function validatePointsTransferAmount(amount, currentUser, basket) {
  var result = {
    error: false
  };
  // Check that an amount is not greater than the current user's balance
  if (amount > currentUser.loyaltyLookupDigitalResponse.points_available) {
    result.error = true;
    result.errorCode = 'pwp_insufficient_points_in_account';
  }

  // convert points to dollar amount
  var dollarAmount = COHelpers.getRewardsAmount(basket, amount);

  // Check that amount to redeem is no more than order total
  if (dollarAmount.dollarAmountToApply.value > basket.totalGrossPrice.value) {
    result.error = true;
    result.errorCode = 'pwp_redeemed_points_greater_than_total';
  }

  return result;
}

/**
 * Verifies if customer meets minimum points required to apply PWP
 * @param {Object} customerData - customer data
 * @returns {Object} result
 */
function customerHasEnoughPoints(customerData) {
  var result = {
    error: false
  };

  if (customerData.pointsBalance && customerData.minimumPointsCurrency) {
    // convert points balance into dollar amount
    var pointsDollarAmount = customerData.pointsBalance / 200;
    if (pointsDollarAmount < customerData.minimumPointsCurrency) {
      result.error = true;
      result.errorCode = 'notEnoughPoints';
    }
  }

  return result;
}

/**
 * Verifies if loyalty pin is correct
 * @param {string} pin - loyalty pin
 * @param {string} loyaltyId - loyalty id
 * @returns {Object} result
 */
function verifyLoyaltyPin(pin, loyaltyId) {
  var verifyPinService = require(verifyLoyaltyPinModule);
  var result = verifyPinService.verifyLoyaltyPin(pin, loyaltyId);
  return result;
}

/**
 * Verify if customer membership is active
 * @param {dw.customer.Customer} customer - given customer
 * @returns {boolean} membershipActive - 'true' if customer membership is active; 'false' otherwise
 */
function isMembershipActive(customer) {
  var membershipActive = false;
  var profile = customer.profile;

  if (!profile) {
    return membershipActive;
  }

  var customerLookupApiResponse;

  if (isCustomerMember(customer)) {
    var customerLookupService = getModule(customerLookupModule);
    if (customerLookupService) {
      customerLookupApiResponse = customerLookupService.callApi(profile).loyaltyLookupDigitalResponse;
    }
  }
  membershipActive = customerLookupApiResponse && customerLookupApiResponse.pinStatus;

  return membershipActive;
}

/**
 * Return estimated points earned for container (basket, order or product line item)
 * @param {dw.order.LineItemCtnr} lineItemCtnr - container (basket, order or product line item)
 * @param {String} loyaltyId - the current customer's loyalty reward id
 * @returns {number} hudsonPoints - estimated points earned
 */
function getEstimatedPointsEarned(lineItemCtnr, loyaltyId) {
  // calculate estimated points earned
  var hudsonPoints = productHelper.getHudsonPoints(lineItemCtnr, true, null, true, loyaltyId);
  return hudsonPoints;
}

/**
 * Return customer loyalty data
 * @param {dw.customer.Customer} customer
 * @returns {Object} data - customer loyalty data
 */
function getCustomerLoyaltyData(customer) {
  var profile = customer.profile;
  var data = {
    membershipActive: false
  };

  if (!profile) {
    return membershipActive;
  }

  var customerLookupApiResponse;

  if (isCustomerMember(customer)) {
    var customerLookupService = getModule(customerLookupModule);
    if (customerLookupService) {
      customerLookupApiResponse = customerLookupService.callApi(profile).loyaltyLookupDigitalResponse;
      if (customerLookupApiResponse) {
        data.pointsBalance = customerLookupApiResponse.points_available;
        data.membershipActive = customerLookupApiResponse.pinStatus;
        data.minimumPointsCurrency = getMinimumPointsNumber(customerLookupApiResponse.minimum_points);
        data.loyaltyId = customerLookupApiResponse.loyalty_id;
        var hasEnoughPointsResult = customerHasEnoughPoints(data);
        if (hasEnoughPointsResult.error) {
          data.pwpStatus = hasEnoughPointsResult.errorCode;
        }
      }
    }
  }
  return data;
}

/**
 * Validate requirements for applying reward points to the basket
 * @param {dw.order.Basket} basket - given basket
 * @param {Object} paymentInstrumentRequest
 * @returns {Object} result - validation result
 */
function validateLoyaltyPaymentMethod(basket, paymentInstrumentRequest) {
  var result = {
    error: false,
    errorMessage: ''
  };

  // get all required modules
  var normalizeFormattedNumber = getModule(normalizeFormattedNumberModule);
  var customerLookupService = getModule(customerLookupModule);
  var verifyPinService = require(verifyLoyaltyPinModule);

  if (!(normalizeFormattedNumber && customerLookupService && verifyPinService)) {
    // return, since the site doesn't use loyalty library and required modules are missing
    return result;
  }

  var amount = paymentInstrumentRequest.c_loyaltyPoints;
  var pin = paymentInstrumentRequest.c_loyaltyPin;
  var customer = basket.customer;
  var profile = customer.profile;

  if (!isCustomerMember(basket.customer)) {
    // customer not logged in or does not have membership
    result.error = true;
    result.errorCode = 'customerNotLoyaltyMember';
    return result;
  }

  var currentUser;
  try {
    // Get the current user to check data in real time (fraud detection, points balance, etc)
    currentUser = customerLookupService.callApi({ email: customer.profile.email });
    if (!currentUser) {
      result.error = true;
      result.errorCode = 'loyaltyServiceError';
      return result;
    }
  } catch (e) {
    result.error = true;
    result.errorCode = 'loyaltyServiceError';
    return result;
  }

  var verifyPin;
  try {
    // Verify pin
    verifyPin = verifyLoyaltyPin(pin, profile.custom.hudsonReward);
    // If pin is wrong, verify if pin is locked
    if (!verifyPin.success) {
      result.error = true;
      if (currentUser.loyaltyLookupDigitalResponse.pin_locked) {
        result.errorCode = 'pwpInvalidPinLockOut';
      } else {
        result.errorCode = 'pwpPinInvalid';
      }
      return result;
    }
  } catch (e) {
    result.error = true;
    result.errorCode = 'loyaltyServiceError';
    return result;
  }

  if (currentUser.loyaltyLookupDigitalResponse.fraud_hold_indicator !== '0') {
    // flag transaction as fraud
    result.error = true;
    result.errorCode = 'pwpFraud';
    return result;
  }

  // Ammount of Points the user has input
  var amount_1 = normalizeFormattedNumber(amount);

  // Check if entered points is valid - using pointTranfser function here
  var isValidAmount = validatePointsTransferAmount(amount_1, currentUser, basket);
  // validate amount
  if (isValidAmount.error) {
    // invalid amount
    result.error = true;
    result.errorCode = isValidAmount.errorCode;
    return result;
  }

  return result;
}

/**
 * Update payment transaction with dollar amount equivalent to points applied
 * @param {dw.order.Basket} basket - given basket
 * @param {number} points - points to be applied
 * @returns {Object} result - result of transaction
 */
function updateDollarAmountLoyalty(basket, paymentInstrumentRequest) {
  var result = {
    error: false
  };
  if (!empty(paymentInstrumentRequest)) {
    var points = paymentInstrumentRequest.c_loyaltyPoints;
    // Get the dollar value to apply - calculates the amount to charge for the payment instrument.
    var total = COHelpers.getRewardsAmount(basket, points);
    if (!total.error && total.dollarAmountToApply.value > 0) {
      // Set transaction amount
      paymentInstrumentRequest.amount = total.dollarAmountToApply.value;
    }
  } else {
    result = {
      error: true
    };
  }
  return result;
}

/**
 * Remove PWP payment instrument if gift card is present in bag
 * @param {dw.order.Basket} basket - given basket
 * @param {array} items - items added to bag
 */
function validateBasketForPWP(basket, items) {
  // verify if PWP was applied to basket
  var pwpPayments = basket.getPaymentInstruments('PAY_WITH_POINTS');
  var basketHasGiftCards = true;

  if (!empty(pwpPayments) && !empty(items)) {
    collections.forEach(new dw.util.ArrayList(items), function (item) {
      if (item && !empty(item.productId)) {
        var product = ProductMgr.getProduct(item.productId);
        if (product && 'hbcProductType' in product.custom && product.custom.hbcProductType == 'giftcard') {
          basketHasGiftCards = true;
        }
      }
    });
    if (basketHasGiftCards) {
      Transaction.wrap(function () {
        collections.every(pwpPayments, function (pwpPayment) {
          basket.removePaymentInstrument(pwpPayment);
        });
      });
    }
  }
}

/**
 * Verify if basket has giftcards present
 * @param {dw.order.Basket} basket
 * @returns {boolean} result
 */
function basketHasGiftcards(basket) {
  var result = false;

  collections.forEach(basket.allProductLineItems, function (pli) {
    if (pli && pli.product && 'hbcProductType' in pli.product.custom && pli.product.custom.hbcProductType == 'giftcard') {
      result = true;
    }
  });

  return result;
}

function updateTransactionAmount(basket, paymentInstrumentRequest, paymentInstrument) {
  var loyaltyPaymentMethod = 'PAY_WITH_POINTS';
  var gcType = 'GiftCard';
  // total payment methods
  var totalAmountTransaction = 0;
  // balance of the order after applying payment
  var totalRemainingOrder = basket.totalGrossPrice.value;
  var paymentInstruments = basket.getPaymentInstruments(loyaltyPaymentMethod);

  // check for required module
  if (!getModule('*/cartridge/scripts/util/ipaGCUtils')) {
    return;
  }

  // No payment instrument was added/removed
  if (!paymentInstrumentRequest && !paymentInstrument) {
    return;
  }

  var paymentMethod;
  if (paymentInstrumentRequest) {
    paymentMethod = paymentInstrumentRequest.paymentMethodId;
    totalAmountTransaction = paymentInstrumentRequest.amount;
  } else {
    paymentMethod = paymentInstrument.paymentMethod;
    // Verify if PWP payment instrument was added to basket; retrieve amount applied
    collections.forEach(paymentInstruments, function (item) {
      totalAmountTransaction += item.paymentTransaction.amount.value;
    });
  }

  // If any payment method different than PWP was added/removed and there's no PWP, amount doesn't have to be updated
  if (paymentMethod != loyaltyPaymentMethod && empty(paymentInstruments)) {
    return;
  }

  // Subtract amount applied from basket remaining balance
  totalRemainingOrder -= totalAmountTransaction;

  // For each gift card try to use the max amount for the remaining balance
  paymentInstruments = basket.getPaymentInstruments(gcType);
  collections.forEach(paymentInstruments, function (gcPaymentInstrument) {
    Transaction.wrap(function () {
      if (totalRemainingOrder > 0) {
        // check gift card balance; if error or balance was already covered, remove gift card from basket
        var result = hooksHelper(
          'app.payment.giftcard.balance.check',
          'gcBalanceCheck',
          [gcPaymentInstrument.custom.giftCardNumber, gcPaymentInstrument.custom.giftCardPin, ''],
          require('*/cartridge/scripts/util/ipaGCUtils').gcBalanceCheck
        );

        if (result != null && result.ok != null && !result.ok) {
          return new Status(Status.ERROR, 'ERROR', Resource.msg('error.message.giftcard.service', 'giftcard', null));
        } else if (result != null && !!result.response_code && Number(result.response_code) !== 1) {
          return new Status(Status.ERROR, 'ERROR', Resource.msg('error.message.giftcard.request', 'giftcard', null));
        } else {
          var cardBalance = Number(result.card.funds_available);
          if (cardBalance > 0) {
            var amountToRedeem;
            if (cardBalance > totalRemainingOrder) {
              amountToRedeem = totalRemainingOrder;
            } else {
              amountToRedeem = cardBalance;
            }
            if (amountToRedeem > 0) {
              // apply giftcard amount
              Transaction.wrap(function () {
                gcPaymentInstrument.getPaymentTransaction().setAmount(new Money(amountToRedeem, basket.currencyCode));
                totalRemainingOrder -= amountToRedeem;
              });
            } else {
              // Set gift card transaction amount to zero
              Transaction.wrap(function () {
                gcPaymentInstrument.getPaymentTransaction().setAmount(new Money(0, basket.currencyCode));
              });
            }
          } else {
            // Set gift card transaction amount to zero
            Transaction.wrap(function () {
              gcPaymentInstrument.getPaymentTransaction().setAmount(new Money(0, basket.currencyCode));
            });
          }
        }
      } else {
        // Set transaction amount of gift card to zero
        Transaction.wrap(function () {
          gcPaymentInstrument.getPaymentTransaction().setAmount(new Money(0, basket.currencyCode));
        });
      }
    });
  });

  // TODO: make sure remaining balance >= 0
  if (totalRemainingOrder >= 0) {
    // Apply remaining balance to CC
    paymentInstruments = basket.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);
    collections.forEach(paymentInstruments, function (item) {
      Transaction.wrap(function () {
        item.getPaymentTransaction().setAmount(new Money(totalRemainingOrder, basket.currencyCode));
      });
    });
  }

  dw.system.HookMgr.callHook('dw.order.calculate', 'calculate', basket);
}

module.exports = {
  isMembershipActive: isMembershipActive,
  getEstimatedPointsEarned: getEstimatedPointsEarned,
  getCustomerLoyaltyData: getCustomerLoyaltyData,
  validateLoyaltyPaymentMethod: validateLoyaltyPaymentMethod,
  updateDollarAmountLoyalty: updateDollarAmountLoyalty,
  validateBasketForPWP: validateBasketForPWP,
  basketHasGiftcards: basketHasGiftcards,
  updateTransactionAmount: updateTransactionAmount
};
