'use strict'; 
/**
 * * OCAPI Extensions Product Search Hooks. *
 * 
 * @module scripts/hooks/product/ProductSearchHooks
 */ 

/**
 * Modifies the productSearchResult response by adding brand etc for each hit.
 * 
 * @param productSearchResult
 * @returns {Status}
 */

function modifyGETResponse(productSearchResult)  {
	var log = require("dw/system/Logger").getLogger("OCAPI");
	var Status = require("dw/system/Status");
	var ProductMgr = require('dw/catalog/ProductMgr');
	var PromotionMgr = require('dw/campaign/PromotionMgr');
	var priceFactory = require('*/cartridge/scripts/factories/price');
	var promotions;

	log.debug("produc search modifyGETResponse: ");
	
	var hits = productSearchResult.hits;
	
	for each (hit in hits) {
		var product = ProductMgr.getProduct(hit.product_id);
		if (product.brand) {
		    hit.c_brand = product.brand;
		}
		
		promotions = PromotionMgr.activeCustomerPromotions.getProductPromotions(product);
		var priceObj = priceFactory.getPrice(product, null, true, promotions, null);
		hit.c_priceRange = priceObj;
		
		if (!empty(promotions)) {
			var promo = promotions[0];
			if (!empty(promo.calloutMsg) && !empty(promo.calloutMsg.markup)) {
				hit.c_promoMsg = promo.calloutMsg.markup;
			}
		} else {
			var searchHitProdId = hit.representedProduct.ID;
			var variantProd = ProductMgr.getProduct(searchHitProdId);
			var priceModel = variantProd.priceModel;
			if (priceModel && priceModel.priceInfo && priceModel.priceInfo.priceBook) {
				var priceBook = priceModel.priceInfo.priceBook;
				var isPromotionPriceBook = 'isPromotionPriceBook' in priceBook.custom && priceBook.custom.isPromotionPriceBook;
				var priceInfo = priceModel.priceInfo.priceInfo || '';
				if (isPromotionPriceBook && priceInfo !== '') {
					if(priceInfo.indexOf('|') > -1) {
						if (request.locale != 'fr_CA'){
							hit.c_promoMsg= priceInfo.split('|')[0];
						} else {
							hit.c_promoMsg= priceInfo.split('|')[1];
						}
					} else {
						hit.c_promoMsg= priceInfo;
					}
				}
			}
		}
	}
	
	log.debug("product search modifyGETResponse: after ");
	return new Status(Status.OK);
}

exports.modifyGETResponse = modifyGETResponse