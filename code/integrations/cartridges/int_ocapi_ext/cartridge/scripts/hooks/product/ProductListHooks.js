'use strict';
var Logger = require('dw/system/Logger');
var Status = require('dw/system/Status');

/**
 * OCAPI Extensions ProductList Hooks.
 *
 * @module scripts/customer/ProductListHooks
 */

// dw.ocapi.shop.customer.product_lists.modifyGETResponse_v2
exports.modifyGETResponse_v2 = function (customer, customerProductListResultResponse) {
  if (request.httpParameters != null && request.httpParameters.type != null) {
    var type = request.httpParameters.get('type')[0];

    var data = customerProductListResultResponse.data.toArray().filter(function (productList, ind, productLists) {
      if (productList.type == type) {
        return true;
      } else {
        return false;
      }
    });
    customerProductListResultResponse.data = data;
    customerProductListResultResponse.count = data.length;
  }

  return new dw.system.Status(dw.system.Status.OK);
};

//dw.ocapi.shop.customer.product_list.beforePATCH
exports.beforePATCH = function (customer, plist) {
  Logger.debug('entered in beforePATCH');
  if (request.httpParameters != null && request.httpParameters.pid != null) {
    var pid = request.httpParameters.get('pid')[0];

    var list = require('dw/customer/ProductListMgr').getProductList(plist.id);

    if (!empty(list)) {
      var listItems = list.items.toArray();
      var found = false;

      listItems.forEach(function (item) {
        if (item.productID === pid) {
          found = item;
        }
      });

      if (found) {
        list.removeItem(found);
      }
    }
  }

  Logger.debug('returning from beforePATCH');
  return new dw.system.Status(dw.system.Status.OK);
};
