'use strict';
/**
 * * OCAPI Extensions Product Hooks. *
 *
 * @module scripts/hooks/product/ProductHooks
 */

/**
 * Modifies the productdetails response by adding the price range info
 *
 * @param product
 * @param doc
 * @returns {Status}
 */

function modifyGETResponse(scriptProduct, doc) {
  var log = require('dw/system/Logger').getLogger('OCAPI');
  var Status = require('dw/system/Status');
  var PromotionMgr = require('dw/campaign/PromotionMgr');
  var URLUtils = require('dw/web/URLUtils');
  var priceFactory = require('*/cartridge/scripts/factories/price');
  var promotions;

  if (scriptProduct) {
    promotions = PromotionMgr.activeCustomerPromotions.getProductPromotions(scriptProduct);

    /*if (!empty(promotions)) {
			var promo = promotions[0];
			if (!empty(promo.calloutMsg) && !empty(promo.calloutMsg.markup)) {
				doc.c_promoMsg = promo.calloutMsg.markup;
			}
		} else {
			var apiProductForPromo = getDefaultVariant(scriptProduct);
			var priceModel = apiProductForPromo.priceModel;
			if (priceModel && priceModel.priceInfo && priceModel.priceInfo.priceBook) {
				var priceBook = priceModel.priceInfo.priceBook;
				var isPromotionPriceBook = 'isPromotionPriceBook' in priceBook.custom && priceBook.custom.isPromotionPriceBook;
				var priceInfo = priceModel.priceInfo.priceInfo || '';
				if (isPromotionPriceBook && priceInfo !== '') {
					if(priceInfo.indexOf('|') > -1) {
						if (request.locale != 'fr_CA'){
							doc.c_promoMsg= priceInfo.split('|')[0];
						} else {
							doc.c_promoMsg= priceInfo.split('|')[1];
						}
					} else {
						doc.c_promoMsg= priceInfo;
					}
				}
			} 
		}*/
  }

  var priceObj = priceFactory.getPrice(scriptProduct, null, true, promotions, null);
  doc.c_priceRange = priceObj;

  if (doc && doc.id) {
    doc.c_seourl = URLUtils.https('Product-Show', 'pid', doc.id).toString();
  }

  return new Status(Status.OK);
}

function getDefaultVariant(product) {
  var defaultVariant;
  if (product.master) {
    if (product.variationModel.defaultVariant) {
      defaultVariant = product.variationModel.defaultVariant;
    } else if (product.variationModel && product.variationModel.variants.length > 0) {
      defaultVariant = product.variationModel.variants[0];
    } else {
      defaultVariant = product;
    }
  } else {
    defaultVariant = product;
  }
  return defaultVariant;
}

exports.modifyGETResponse = modifyGETResponse;
