'use strict';
/**
 * * OCAPI Extensions Category Hooks. *
 *
 * @module scripts/hooks/product/CategoryHooks
 */

/**
 * Modifies the categories response by adding alternateURL info
 *
 * @param scriptCategory
 * @param doc
 * @returns {Status}
 */

var CatalogMgr = require('dw/catalog/CatalogMgr');

function modifyGETResponse(scriptCategory: Category, doc: Category) {
  var Status = require('dw/system/Status');
  updateCategory(doc);
  return new Status(Status.OK);
}

function updateCategory(doc: Category) {
  if (doc) {
    var cat = CatalogMgr.getCategory(doc.id);
    if (cat && cat.custom.alternativeUrl) {
      doc.c_refinementParameters = cat.custom.alternativeUrl.source;
    }
    if (doc.categories && doc.categories.length > 0) {
      for (var i = 0; i < doc.categories.length; i++) {
        updateCategory(doc.categories[i]);
      }
    }
  }
}

exports.modifyGETResponse = modifyGETResponse;
