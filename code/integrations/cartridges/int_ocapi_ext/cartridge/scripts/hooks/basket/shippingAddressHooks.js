'use strict';

/* API Includes */
var Logger = require('dw/system/Logger');
var Status = require('dw/system/Status');

//recalculate promotions based on payment instrument
exports.beforePUT = function (basket, shipment, shippingAddress) {
  Logger.debug('dw.ocapi.shop.basket.shipment.shipping_address.beforePUT');

  var PaymentInstrument = require('dw/order/PaymentInstrument');
  var collections = require('*/cartridge/scripts/util/collections');

  var paymentInstruments = basket.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);

  // Logic to apply a Tender Type promotion to the Cart.
  collections.forEach(paymentInstruments, function (paymentInstr) {
    var cardType = paymentInstr.creditCardType;
    if (cardType === 'SAKS' || cardType === 'SAKSMC' || cardType === 'HBC' || cardType === 'HBCMC' || cardType === 'MPA') {
      session.custom.isHBCTenderType = true;
    } else {
      session.custom.isHBCTenderType = false;
    }
  });

  dw.system.HookMgr.callHook('dw.order.calculate', 'calculate', basket);

  return new Status(Status.OK);
};
