'use strict';
/** * OCAPI Extensions Customer Hooks.
 * * @module scripts/customer/BasketPaymentInstrumentHooks */

var Logger = require('dw/system/Logger');
var Transaction = require('dw/system/Transaction');
var Status = require('dw/system/Status');

var LoyaltyHelper = require('*/cartridge/scripts/helpers/loyaltyHelpers');

exports.afterPOST = function (basket, paymentInstrument) {
  Logger.debug('dw.ocapi.shop.basket.payment_instrument.afterPOST');

  var PaymentInstrument = require('dw/order/PaymentInstrument');
  var collections = require('*/cartridge/scripts/util/collections');

  var paymentInstruments = basket.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);

  // Logic to apply a Tender Type promotion to the Cart.
  collections.forEach(paymentInstruments, function (paymentInstr) {
    var cardType = paymentInstr.creditCardType;
    if (cardType === 'SAKS' || cardType === 'SAKSMC' || cardType === 'HBC' || cardType === 'HBCMC' || cardType === 'MPA') {
      session.custom.isHBCTenderType = true;
    } else {
      session.custom.isHBCTenderType = false;
    }
  });

  dw.system.HookMgr.callHook('dw.order.calculate', 'calculate', basket);

  return new dw.system.Status(dw.system.Status.OK);
};

exports.afterPATCH = function (basket, paymentInstrument) {
  Logger.debug('dw.ocapi.shop.basket.payment_instrument.afterPATCH');

  var PaymentInstrument = require('dw/order/PaymentInstrument');
  var collections = require('*/cartridge/scripts/util/collections');

  var paymentInstruments = basket.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);

  // Logic to apply a Tender Type promotion to the Cart.
  collections.forEach(paymentInstruments, function (paymentInstr) {
    var cardType = paymentInstr.creditCardType;
    if (cardType === 'SAKS' || cardType === 'SAKSMC' || cardType === 'HBC' || cardType === 'HBCMC' || cardType === 'MPA') {
      session.custom.isHBCTenderType = true;
    } else {
      session.custom.isHBCTenderType = false;
    }
  });

  dw.system.HookMgr.callHook('dw.order.calculate', 'calculate', basket);

  return new dw.system.Status(dw.system.Status.OK);
};

exports.afterDELETE = function (basket, paymentInstrument) {
  Logger.debug('dw.ocapi.shop.basket.payment_instrument.afterDELETE');

  var PaymentInstrument = require('dw/order/PaymentInstrument');
  var collections = require('*/cartridge/scripts/util/collections');

  var paymentInstruments = basket.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);

  // Logic to apply a Tender Type promotion to the Cart.
  collections.forEach(paymentInstruments, function (paymentInstr) {
    var cardType = paymentInstr.creditCardType;
    if (cardType === 'SAKS' || cardType === 'SAKSMC' || cardType === 'HBC' || cardType === 'HBCMC' || cardType === 'MPA') {
      session.custom.isHBCTenderType = true;
    } else {
      session.custom.isHBCTenderType = false;
    }
  });

  dw.system.HookMgr.callHook('dw.order.calculate', 'calculate', basket);

  return new dw.system.Status(dw.system.Status.OK);
};

exports.beforeDELETE = function (basket, paymentInstrument) {
  Logger.debug('dw.ocapi.shop.basket.payment_instrument.beforeDELETE');

  // Update transaction amounts for each payment method after deleting PWP payment instrument
  LoyaltyHelper.updateTransactionAmount(basket, null, paymentInstrument);

  dw.system.HookMgr.callHook('dw.order.calculate', 'calculate', basket);

  return new dw.system.Status(dw.system.Status.OK);
};
