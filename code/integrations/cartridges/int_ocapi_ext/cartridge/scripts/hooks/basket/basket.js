'use strict';

/* API Includes */
var Logger = require('dw/system/Logger').getLogger('ocapi-hooks', 'basket-payment-instrument');
var Status = require('dw/system/Status');
var gcType = 'GiftCard';
var paypal = 'PayPal';
var loyaltyPaymentMethod = 'PAY_WITH_POINTS';
var Transaction = require('dw/system/Transaction');

var LoyaltyHelper = require('*/cartridge/scripts/helpers/loyaltyHelpers');

/**
 * Creates a token. This should be replaced by utilizing a tokenization provider
 * @param {Object} encryptedCard - creating encryptedCard
 * @returns {string} token - a token
 */
function createToken(encryptedCard) {
  var TokenExFacade = require('*/cartridge/scripts/services/TokenExFacade');
  var token = TokenExFacade.tokenizeEncryptedCard(encryptedCard);
  return token;
}
var token = null;
exports.beforePOST = function (basket, paymentInstrumentRequest) {
  Logger.debug('dw.ocapi.shop.basket.payment_instrument.beforePOST');
  var PaymentInstrument = require('dw/order/PaymentInstrument');
  var Resource = require('dw/web/Resource');
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  var preferences = require('*/cartridge/config/preferences');
  var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
  var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
  var Transaction = require('dw/system/Transaction');
  var collections = require('*/cartridge/scripts/util/collections');

  var paymentMethod = paymentInstrumentRequest.payment_method_id;

  try {
    Transaction.wrap(function () {
      if (
        basket.customer &&
        basket.customer.profile &&
        'hudsonReward' in basket.customer.profile.custom &&
        !empty(basket.customer.profile.custom.hudsonReward)
      ) {
        basket.custom.hudsonRewardNumber = basket.customer.profile.custom.hudsonReward;
      } else {
        basket.custom.hudsonRewardNumber = null;
      }
    });
  } catch (ex) {
    Logger.error('error setting hudson reward to basket: ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
  }

  if (paymentMethod && paymentMethod.equals(PaymentInstrument.METHOD_CREDIT_CARD)) {
    //Remove all Existing PAyment instruments
    Transaction.wrap(function () {
      var paymentInstruments = basket.getPaymentInstruments(paymentMethod);
      collections.forEach(paymentInstruments, function (item) {
        basket.removePaymentInstrument(item);
      });
    });

    if (paymentInstrumentRequest.customer_payment_instrument_id) {
      //Saved Credit Card, get Token from Saved Card
      if (basket && basket.getCustomer() && basket.getCustomer().getProfile() && basket.getCustomer().getProfile().getWallet()) {
        var pi = basket.getCustomer().getProfile().getWallet().getPaymentInstruments(paymentMethod);
        collections.forEach(pi, function (item) {
          if (item.getUUID() === paymentInstrumentRequest.customer_payment_instrument_id) {
            token = item.getCreditCardToken();
          }
        });
      }
    } else {
      var creditCardNumber = paymentInstrumentRequest.paymentCard.number;
      var tokenObj = null;
      if (creditCardNumber) {
        // check the balance for the gift certificate
        tokenObj = createToken(creditCardNumber);
      }
      if (!empty(tokenObj)) {
        paymentInstrumentRequest.paymentCard.number = tokenObj.token;
        paymentInstrumentRequest.c_ocapi_token = tokenObj.token;
        //store token
        token = tokenObj.token;
      }
    }
  } else if (paymentMethod && paymentMethod.equals(gcType)) {
    // Gift Card Check Balance Call before applying the Gift card to the basket
    if (basket && basket.getPaymentInstruments(gcType) && basket.getPaymentInstruments(gcType).length === preferences.maxGCLimit) {
      return new Status(Status.ERROR, 'ERROR', 'Maximum number of Gift Cards applied');
    }

    if (paymentInstrumentRequest.c_giftCardNumber === null) {
      return new Status(Status.ERROR, 'ERROR', 'Please provide Gift Card number');
    }

    if (paymentInstrumentRequest.c_giftCardPin === null) {
      return new Status(Status.ERROR, 'ERROR', 'Please provide Gift Card pin');
    }
    var result = hooksHelper(
      'app.payment.giftcard.balance.check',
      'gcBalanceCheck',
      [paymentInstrumentRequest.c_giftCardNumber, paymentInstrumentRequest.c_giftCardPin, ''],
      require('*/cartridge/scripts/util/ipaGCUtils').gcBalanceCheck
    );

    if (result != null && result.ok != null && !result.ok) {
      return new Status(Status.ERROR, 'ERROR', Resource.msg('error.message.giftcard.service', 'giftcard', null));
    } else if (result != null && !!result.response_code && Number(result.response_code) !== 1) {
      return new Status(Status.ERROR, 'ERROR', Resource.msg('error.message.giftcard.request', 'giftcard', null));
    } else {
      var cardBalance = Number(result.card.funds_available);
      if (cardBalance > 0) {
        var total = COHelpers.getNonGiftCardAmount(basket, cardBalance);
        if (total.giftCardAmountToApply.available && total.giftCardAmountToApply.value > 0) {
          paymentInstrumentRequest.amount = total.giftCardAmountToApply.value;
        } else {
          return new Status(Status.ERROR, 'ERROR', 'Order Amount should be greater than zero to apply a GiftCard');
        }
      } else {
        return new Status(Status.ERROR, 'ERROR', 'Gift Card do not have a positive balance');
      }
    }
  } else if (paymentMethod && paymentMethod.equals(paypal)) {
    Transaction.wrap(function () {
      var paymentInstruments = basket.getPaymentInstruments();
      collections.forEach(paymentInstruments, function (item) {
        basket.removePaymentInstrument(item);
      });
    });
    if (paymentInstrumentRequest.c_paypalBillingAgreement === null) {
      return new Status(Status.ERROR, 'ERROR', 'PayPal Billing Agreement not provided');
    }

    if (paymentInstrumentRequest.c_paypalEmail === null) {
      return new Status(Status.ERROR, 'ERROR', 'Missing PayPal Email');
    }
    if (paymentInstrumentRequest.c_paypalPayerID === null) {
      return new Status(Status.ERROR, 'ERROR', 'Missing PayPal Payer ID');
    }

    if (paymentInstrumentRequest.c_paypalToken === null) {
      return new Status(Status.ERROR, 'ERROR', 'Missing PayPal Token');
    }
  } else if (paymentMethod && paymentMethod.equals(loyaltyPaymentMethod)) {
    Transaction.wrap(function () {
      var paymentInstruments = basket.getPaymentInstruments(loyaltyPaymentMethod);
      collections.forEach(paymentInstruments, function (item) {
        basket.removePaymentInstrument(item);
      });
    });
    // validate loyalty: implement method to validate loyaltyPaymentMethod
    var validateLoyaltyResult = LoyaltyHelper.validateLoyaltyPaymentMethod(basket, paymentInstrumentRequest);
    if (validateLoyaltyResult.error) {
      return new Status(Status.ERROR, 'ERROR', validateLoyaltyResult.errorCode, validateLoyaltyResult.errorMessage);
    }
    // update payment amount
    var updateAmountLoyaltyResult = LoyaltyHelper.updateDollarAmountLoyalty(basket, paymentInstrumentRequest);
    if (updateAmountLoyaltyResult.error) {
      return new Status(Status.ERROR, 'ERROR', validateLoyaltyResult.errorCode, updateAmountLoyaltyResult.errorMessage);
    }
  }
  return new Status(Status.OK);
};

// Update basket for ocapi requests
exports.updateBasketforOCAPI = function (basket) {
  var isOCAPI = require('*/cartridge/scripts/helpers/ocapiHelpers').isOCAPI();
  var productHelper = require('*/cartridge/scripts/helpers/productHelpers');

  if (isOCAPI) {
    // Calculate points earned for this basket
    if (basket && basket.totalGrossPrice && basket.totalGrossPrice.available) {
      basket.custom.points = productHelper.getHudsonPoints(basket, true, null);
    } else {
      basket.custom.points = 0;
    }
  }

  return new Status(Status.OK);
};

exports.afterPUT = function (basket, shipment, shippingMethodDoc) {
  // Call EDD to get the Estimated date.
  var EDDFacade = require('*/cartridge/scripts/services/EDDFacade');
  EDDFacade.getEDDDate(shipment);
};

//recalculate promotions based on payment instrument
exports.beforePATCH = function (basket, basketInput) {
  var PaymentInstrument = require('dw/order/PaymentInstrument');
  var collections = require('*/cartridge/scripts/util/collections');

  var paymentInstruments = basket.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);

  // Logic to apply a Tender Type promotion to the Cart.
  collections.forEach(paymentInstruments, function (paymentInstr) {
    var cardType = paymentInstr.creditCardType;
    if (cardType === 'SAKS' || cardType === 'SAKSMC' || cardType === 'HBC' || cardType === 'HBCMC' || cardType === 'MPA') {
      session.custom.isHBCTenderType = true;
    } else {
      session.custom.isHBCTenderType = false;
    }
  });

  dw.system.HookMgr.callHook('dw.order.calculate', 'calculate', basket);

  return new Status(Status.OK);
};

exports.afterPATCH = function (basket, basketInput) {
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  Logger.debug('dw.ocapi.shop.basket.beforePATCH');

  var loyaltyResponse;

  if (!empty(basket.custom.hudsonRewardNumber)) {
    loyaltyResponse = hooksHelper(
      'app.customer.loyalty.rewards.info',
      'checkLoyaltyRewards',
      basket.custom.hudsonRewardNumber,
      require('*/cartridge/scripts/loyalty/loyaltyRewardsInfoUtil').checkLoyaltyRewards
    );
    if (loyaltyResponse !== null && loyaltyResponse.errorMessage) {
      return new Status(Status.ERROR, 'ERROR', 'Invalid Rewards Number');
    } else if (loyaltyResponse !== null && !!loyaltyResponse.ResponseCode && Number(loyaltyResponse.ResponseCode) !== 0) {
      return new Status(Status.ERROR, 'ERROR', 'Invalid Rewards Number');
    } else {
      return new Status(Status.OK);
    }
  }
  return new Status(Status.OK);
};

exports.modifyGETResponse = function (basket, basketResponse) {
  var customer = basket.customer;
  var pwpEligible = false;

  if (customer.authenticated) {
    var customerLoyaltyData = LoyaltyHelper.getCustomerLoyaltyData(customer);
    if (customerLoyaltyData && customerLoyaltyData.membershipActive) {
      pwpEligible = customerLoyaltyData.membershipActive;
      basketResponse.c_hudsonPoints = LoyaltyHelper.getEstimatedPointsEarned(basket, '' + customerLoyaltyData.loyaltyId);
      basketResponse.c_pointsBalance = customerLoyaltyData.pointsBalance;
      if (!empty(customerLoyaltyData.minimumPointsCurrency)) {
        basketResponse.c_minimumPointsCurrency = customerLoyaltyData.minimumPointsCurrency;
      }
      if (customerLoyaltyData.pwpStatus) {
        basketResponse.c_pwpStatus = customerLoyaltyData.pwpStatus;
      }
      if (LoyaltyHelper.basketHasGiftcards(basket)) {
        basketResponse.c_pwpStatus = 'gcInBag';
      }
    }
  }
  basketResponse.c_pwpEligible = pwpEligible;

  return new Status(Status.OK);
};
