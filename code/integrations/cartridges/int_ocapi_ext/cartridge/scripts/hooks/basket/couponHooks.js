'use strict';

/* API Includes */
var Logger = require('dw/system/Logger').getLogger('ocapi-hooks', 'basket-payment-instrument');
var Status = require('dw/system/Status');
var Transaction = require('dw/system/Transaction');

exports.beforePOST = function (basket, couponItem) {
  var PaymentInstrument = require('dw/order/PaymentInstrument');
  var collections = require('*/cartridge/scripts/util/collections');

  var paymentInstruments = basket.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);

  // Logic to apply a Tender Type promotion to the Cart.
  collections.forEach(paymentInstruments, function (paymentInstr) {
    var cardType = paymentInstr.creditCardType;
    if (cardType === 'SAKS' || cardType === 'SAKSMC' || cardType === 'HBC' || cardType === 'HBCMC' || cardType === 'MPA') {
      session.custom.isHBCTenderType = true;
    } else {
      session.custom.isHBCTenderType = false;
    }
  });

  dw.system.HookMgr.callHook('dw.order.calculate', 'calculate', basket);

  return new dw.system.Status(dw.system.Status.OK);
};
