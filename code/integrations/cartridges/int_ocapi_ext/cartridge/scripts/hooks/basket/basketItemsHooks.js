'use strict';

/* API Includes */
var Logger = require('dw/system/Logger');
var Status = require('dw/system/Status');

var LoyaltyHelper = require('*/cartridge/scripts/helpers/loyaltyHelpers');

//recalculate promotions based on payment instrument
exports.beforePOST = function (basket, items) {
  Logger.debug('dw.ocapi.shop.basket.items.beforePOST');
  var PaymentInstrument = require('dw/order/PaymentInstrument');
  var collections = require('*/cartridge/scripts/util/collections');

  var paymentInstruments = basket.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);

  // Logic to apply a Tender Type promotion to the Cart.
  collections.forEach(paymentInstruments, function (paymentInstr) {
    var cardType = paymentInstr.creditCardType;
    if (cardType === 'SAKS' || cardType === 'SAKSMC' || cardType === 'HBC' || cardType === 'HBCMC' || cardType === 'MPA') {
      session.custom.isHBCTenderType = true;
    } else {
      session.custom.isHBCTenderType = false;
    }
  });

  // If there are gift cards present in the cart, remove PWP payment instrument
  LoyaltyHelper.validateBasketForPWP(basket, items);

  dw.system.HookMgr.callHook('dw.order.calculate', 'calculate', basket);

  return new Status(Status.OK);
};

//recalculate promotions based on payment instrument
exports.beforePATCH = function (basket, items) {
  Logger.debug('dw.ocapi.shop.basket.items.beforePATCH');

  var PaymentInstrument = require('dw/order/PaymentInstrument');
  var collections = require('*/cartridge/scripts/util/collections');

  var paymentInstruments = basket.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);

  // Logic to apply a Tender Type promotion to the Cart.
  collections.forEach(paymentInstruments, function (paymentInstr) {
    var cardType = paymentInstr.creditCardType;
    if (cardType === 'SAKS' || cardType === 'SAKSMC' || cardType === 'HBC' || cardType === 'HBCMC' || cardType === 'MPA') {
      session.custom.isHBCTenderType = true;
    } else {
      session.custom.isHBCTenderType = false;
    }
  });

  dw.system.HookMgr.callHook('dw.order.calculate', 'calculate', basket);

  return new Status(Status.OK);
};

//recalculate promotions based on payment instrument
exports.beforeDELETE = function (basket, items) {
  Logger.debug('dw.ocapi.shop.basket.items.beforeDELETE');

  var PaymentInstrument = require('dw/order/PaymentInstrument');
  var collections = require('*/cartridge/scripts/util/collections');

  var paymentInstruments = basket.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);

  // Logic to apply a Tender Type promotion to the Cart.
  collections.forEach(paymentInstruments, function (paymentInstr) {
    var cardType = paymentInstr.creditCardType;
    if (cardType === 'SAKS' || cardType === 'SAKSMC' || cardType === 'HBC' || cardType === 'HBCMC' || cardType === 'MPA') {
      session.custom.isHBCTenderType = true;
    } else {
      session.custom.isHBCTenderType = false;
    }
  });

  dw.system.HookMgr.callHook('dw.order.calculate', 'calculate', basket);

  return new Status(Status.OK);
};
