'use strict';
/** * OCAPI Extensions Customer Hooks.
 * * @module scripts/customer/CustomerPaymentInstrumentHooks */

var Logger = require('dw/system/Logger');
var Transaction = require('dw/system/Transaction');
var Status = require('dw/system/Status');

exports.beforePOST = function (customer, paymentInstrument) {
  // call TokenEx service to tokenize the credit card (similar to storefront)
  var token = '';
  var tokenObj;

  if (paymentInstrument.c_ocapi_token) {
    //set the token, When Token is present in the request. Used when card is deleted and recreated to update default flag. This is a workaround as there is no patch available for customer payment instrument.
    //paymentInstrument.c_ocapi_token = paymentInstrument.payment_card.credit_card_token;
    //Remove default flag for all saved customer payment instruments to maintain only one default always.
    var paymentInstruments = customer.profile.wallet.paymentInstruments;
    /* eslint-disable */
    if (paymentInstruments) {
      for (var i = 0; i < paymentInstruments.length; i++) {
        var pi = paymentInstruments[i];
        Transaction.wrap(function () {
          pi.custom.defaultCreditCard = false;
        });
      }
    }
    return new dw.system.Status(dw.system.Status.OK);
  } else {
    if (paymentInstrument.payment_card.number) {
      tokenObj = createToken(paymentInstrument.payment_card.number);
    }

    if (tokenObj != null && tokenObj.token != null && tokenObj.token != '') {
      paymentInstrument.c_ocapi_token = tokenObj.token;
      return new dw.system.Status(dw.system.Status.OK);
    } else {
      return new dw.system.Status(dw.system.Status.ERROR);
    }
  }
  /** Return Status.OK: if success. Return Status.ERROR: If failed. **/
};

exports.afterPOST = function (customer, paymentInstrumentDoc) {
  if (customer.profile.wallet && customer.profile.wallet.paymentInstruments) {
    var paymentInstruments = customer.profile.wallet.paymentInstruments;
    for (var i = 0; i < paymentInstruments.length; i++) {
      var token = paymentInstruments[i].custom.ocapi_token;
      if (paymentInstruments[i].custom.ocapi_token === paymentInstrumentDoc.c_ocapi_token) {
        if (paymentInstruments[i].creditCardNumber !== paymentInstruments[i].custom.ocapi_token) {
          Transaction.wrap(function () {
            paymentInstruments[i].setCreditCardNumber(paymentInstruments[i].custom.ocapi_token);
          });
        }
      }
    }
  }
};

/**
 * Creates a token. This should be replaced by utilizing a tokenization provider
 * @param {Object} encryptedCard - creating encryptedCard
 * @returns {string} token - a token
 */
function createToken(encryptedCard) {
  var TokenExFacade = require('*/cartridge/scripts/services/TokenExFacade');
  var token = TokenExFacade.tokenizeEncryptedCard(encryptedCard);
  return token;
}
