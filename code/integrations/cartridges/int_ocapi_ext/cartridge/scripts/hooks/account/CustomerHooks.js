'use strict';
/**
 * * OCAPI Extensions Customer Hooks. *
 *
 * @module scripts/customer/CustomerHooks
 */
/**
 * * Send new customer registration to UCID.
 */
var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
var Logger = require('dw/system/Logger');
var Status = require('dw/system/Status');

function afterPOST(customer: Customer, registration: CustomerRegistration) {
  hooksHelper('ucid.middleware.service', 'createUCIDCustomer', [customer.profile, customer.profile.customerNo, null]);

  if ('ucidNo' in customer.profile.custom && customer.profile.custom.ucidNo != null && customer.profile.custom.ucidNo != '') {
    return new dw.system.Status(dw.system.Status.OK);
  } else {
    // Need to discuss special handling in case UCID service failure.
    //Should profile get created if ucid service fails?
    //For now, returning OK because services are unstable
    return new dw.system.Status(dw.system.Status.OK);
  }
}

function afterPATCH(customer: Customer, customerInput: Customer) {
  hooksHelper('ucid.middleware.service', 'updateUCIDCustomer', [customer.profile, customer.profile.customerNo, null, null, null, null]);

  return new dw.system.Status(dw.system.Status.OK);
}

exports.afterPATCH = afterPATCH;
exports.afterPOST = afterPOST;
