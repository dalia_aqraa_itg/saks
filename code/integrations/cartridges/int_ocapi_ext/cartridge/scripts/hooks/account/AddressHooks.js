'use strict';
/**
 * * OCAPI Extensions Customer Hooks. *
 *
 * @module scripts/customer/AddressHooks
 */

var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
var Logger = require('dw/system/Logger');
var Status = require('dw/system/Status');

function addressUpdate(
  customer: Customer,
  addressName: String,
  customerAddress: CustomerAddress,
  addressType: String,
  isDefaultAddress: boolean,
  isAddAddress: boolean
) {
  // call UCID service to send address updates (similar to storefront)

  hooksHelper('ucid.middleware.service', 'updateUCIDCustomer', [
    customer.profile,
    customer.profile.customerNo,
    customerAddress,
    addressType,
    isDefaultAddress,
    isAddAddress
  ]);

  return new dw.system.Status(dw.system.Status.OK);
}

exports.afterPATCH = addressUpdate;
exports.afterPOST = addressUpdate;
