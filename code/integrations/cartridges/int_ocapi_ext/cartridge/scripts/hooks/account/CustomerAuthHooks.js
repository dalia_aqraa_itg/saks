'use strict';

/**
 * * OCAPI Extensions Customer Auth Hooks. *
 *
 * @module scripts/customer/CustomerAuthHooks
 */
var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
var Logger = require('dw/system/Logger');
var Transaction = require('dw/system/Transaction');
var Status = require('dw/system/Status');
var StringUtils = require('dw/util/StringUtils');

function afterPOST(customer: Customer, authRequestType: EnumValue) {
  //	 call Loyalty service to get points balance (similar to storefront)
  if (
    !empty(customer.profile) &&
    'hudsonReward' in customer.profile.custom &&
    customer.profile.custom.hudsonReward !== null &&
    customer.profile.custom.hudsonReward !== ''
  ) {
    var loyaltyResponse = hooksHelper(
      'app.customer.loyalty.rewards.info',
      'checkLoyaltyRewards',
      customer.profile.custom.hudsonReward,
      require('*/cartridge/scripts/loyalty/loyaltyRewardsInfoUtil').checkLoyaltyRewards
    );

    if (!(loyaltyResponse !== null && Number(loyaltyResponse.ResponseCode) === 0)) {
    } else {
      Transaction.wrap(function () {
        customer.profile.custom.hudsonRewardPointBalance = loyaltyResponse.RewardProfile.RewardsInfo.pointBalance;
      });
    }
  }

  return new dw.system.Status(dw.system.Status.OK);
}

function beforePOST(authorizationHeader: String, authRequestType: EnumValue) {
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');

  var result = {
    loginId: null,
    customerNo: null,
    firstName: null,
    lastName: null
  };

  if (authRequestType.displayValue == 'credentials') {
    try {
      var credentials = authorizationHeader.substr(6);

      var base64decoded: String = StringUtils.decodeBase64(credentials, 'UTF-8');
      var username: String = base64decoded.split(':')[0];
      var password: String = base64decoded.split(':')[1];

      var tempCustomer = CustomerMgr.getCustomerByLogin(username);

      // First verify if the Customer is available on the SFCC side
      if (!empty(tempCustomer)) {
        if (tempCustomer.profile.credentials.locked) {
          // Return error, if the customer account is locked
          result.loginId = tempCustomer.profile.credentials.login;
          result.customerNo = tempCustomer.profile.customerNo;
          result.firstName = tempCustomer.profile.firstName;
          result.lastName = tempCustomer.profile.lastName;
          status = new Status(Status.ERROR, '102', 'Account Locked');
          status.addDetail('ACCOUNT_LOCKED', result);
          return status;
        }
        // Check if this is a legacy customer or not.
        if (!empty(tempCustomer.profile.custom.legacyPasswordHash)) {
          var isLegacyHashMatch = legacyCustomerLogin.isLegacyPassworMatched(tempCustomer.profile.custom.legacyPasswordHash, password);
          // if the legacy user password matches
          if (isLegacyHashMatch) {
            legacyCustomerLogin.setLegacyPwd(tempCustomer, password);

            var authStatus = CustomerMgr.authenticateCustomer(username, password);
            var authenticatedCustomerLegacy = CustomerMgr.loginCustomer(authStatus, false);
            if (!empty(authenticatedCustomerLegacy)) {
              delete tempCustomer.profile.custom.legacyPasswordHash;
              return new Status(Status.OK);
            } else {
              result.loginId = tempCustomer.profile.credentials.login;
              result.customerNo = tempCustomer.profile.customerNo;
              result.firstName = tempCustomer.profile.firstName;
              result.lastName = tempCustomer.profile.lastName;
              status = new Status(Status.ERROR, '101', 'Wrong Password');
              status.addDetail('WRONG_PASSWORD', result);
              return status;
            }
          } else {
            result.loginId = tempCustomer.profile.credentials.login;
            result.customerNo = tempCustomer.profile.customerNo;
            result.firstName = tempCustomer.profile.firstName;
            result.lastName = tempCustomer.profile.lastName;
            status = new Status(Status.ERROR, '101', 'Wrong Password');
            status.addDetail('WRONG_PASSWORD', result);
            return status;
          }
        }
        return new Status(Status.OK);
      } else {
        status = new Status(Status.ERROR, '101', 'Wrong Password');
        status.addDetail('WRONG_PASSWORD', result);
        return status;
      }
    } catch (ex) {
      return new Status(Status.ERROR, '103', ex.message);
    }
  }
}

exports.beforePOST = beforePOST;
exports.afterPOST = afterPOST;
