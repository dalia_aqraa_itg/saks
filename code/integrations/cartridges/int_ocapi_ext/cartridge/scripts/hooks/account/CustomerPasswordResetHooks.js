'use strict';

/**
 * OCAPI Extensions Customer Password Reset Hooks.
 *
 * @module scripts/customer/CustomerPasswordResetHooks
 */

function afterPOST(customer: Customer, resetToken: String) {
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
  var Resource = require('dw/web/Resource');
  var Logger = require('dw/system/Logger');
  var Status = require('dw/system/Status');
  var Locale = require('dw/util/Locale');

  try {
    var currentLanguage = Locale.getLocale(request.locale).getLanguage();
    var email = customer && customer.profile ? customer.profile.email : null;
    if (!customer || !email || !resetToken) {
      Logger.error('dw.ocapi.shop.customers.password_reset.afterPOST - required params were missing!');
      return new Status(Status.ERROR);
    }

    var status = accountHelpers.sendPasswordResetEmailForOCAPI(customer, resetToken, currentLanguage);
    if (status != null) {
      return new dw.system.Status(dw.system.Status.OK);
    } else {
      return new dw.system.Status(dw.system.Status.ERROR);
    }
  } catch (e) {
    Logger.debug('CustomerPasswordResetHooks Exception -->' + e.message);
    return new dw.system.Status(dw.system.Status.ERROR);
  }
}

//dw.ocapi.shop.customers.password_reset.afterPOST
exports.afterPOST = afterPOST;
