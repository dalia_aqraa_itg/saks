'use strict';

/* API Includes */
var Logger = require('dw/system/Logger').getLogger('ocapi-hooks', 'order');
var Status = require('dw/system/Status');
var RootLogger = require('dw/system/Logger').getRootLogger();
var Transaction = require('dw/system/Transaction');
var Resource = require('dw/web/Resource');
var Order = require('dw/order/Order');
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var Site = require('dw/system/Site');
var customPreferences = Site.current.preferences.custom;

/**
 * After Post for the order
 *
 * @return {Status} Status - Status response
 */
exports.afterPOST = function (order) {
  Logger.debug('dw.ocapi.shop.order.afterPOST');
  var Transaction = require('dw/system/Transaction');
  var OrderAPIUtils = require('*/cartridge/scripts/util/OrderAPIUtil');
  var Order = require('dw/order/Order');
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  var reserveInventoryResult = hooksHelper('app.inventory.reservation', 'reserveInventory', [order]);
  // Fail the order and throw user to previous checkout step, only if service is up and any of the items doesn't have sufficient inventory in OMS.
  if (!reserveInventoryResult.hasInventory) {
    Transaction.wrap(function () {
      order.trackOrderChange('Failed to reserve inenventory');
      dw.order.OrderMgr.failOrder(order);
    });
    // get the productIds
    var productIds = '';
    reserveInventoryResult.omsInventory.forEach(function (inventoryData) {
      productIds = productIds + ',' + inventoryData.itemID;
    });

    return new Status(Status.ERROR, 'OMSException', 'One of the items does not have sufficient inventory in OMS. ProductIds:[' + productIds + ']');
  }
  return new Status(Status.OK);
};

/**
 * Authorize Credit Card
 *
 * @return {Status} Status - Status response'
 */
exports.authorizeCreditCard = function (order, paymentInstrument, cvn) {
  Logger.debug('dw.ocapi.shop.order.authorizeCreditCard');
  try {
    var PaymentInstrument = require('dw/order/PaymentInstrument');

    if (!order || !paymentInstrument) {
      Logger.error('authorizeCreditCard - required parameters (order and paymentInstrument) are missing!');
      return new Status(Status.ERROR, 'ERROR', 'required parameters (order and paymentInstrument) are missing');
    }

    var col = order.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);
    col = col.toArray();
    var pi = col[0];

    paymentInstrument = pi;

    if (empty(paymentInstrument.creditCardNumber)) {
      paymentInstrument.creditCardNumber = pi.custom.ocapi_token;
    }
    if (empty(paymentInstrument.creditCardToken)) {
      paymentInstrument.creditCardToken = pi.custom.ocapi_token;
    }

    var HookMgr = require('dw/system/HookMgr');
    var authorizationResult;
    //var paymentProcessor = paymentInstrument.paymentTransaction.paymentProcessor;
    var paymentProcessor = dw.order.PaymentMgr.getPaymentMethod(paymentInstrument.paymentMethod).paymentProcessor;
    var args = {};
    args.session = {};
    args.Order = order;
    // no change in the remoteHost for OCAPI
    args.remoteAddress = order.remoteHost;
    args.customer = order.getCustomer();
    args.deviceFingerPrintID = ''; //  no device fingerprint on the OCAPI calls
    if (cvn) {
      args.cvn = cvn;
    }
    //Capture the no of failure attempts executed by the customer in the same session
    var attempts = session.custom.failAttempts || 0;
    attempts += 1;
    session.custom.failAttempts = attempts;
    args.failAttempts = attempts;
    if (HookMgr.hasHook('app.payment.processor.' + paymentProcessor.ID.toLowerCase())) {
      authorizationResult = HookMgr.callHook(
        'app.payment.processor.' + paymentProcessor.ID.toLowerCase(),
        'Authorize',
        order.orderNo,
        paymentInstrument,
        paymentProcessor,
        args
      );
    } else {
      authorizationResult = HookMgr.callHook('app.payment.processor.default', 'Authorize');
    }
    if (authorizationResult.error) {
      return new Status(Status.ERROR);
    }
    return new Status(Status.OK);
  } catch (ex) {
    Logger.error('authorizeCreditCard error: ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
    return new Status(Status.ERROR);
  }
};

/**
 * Authorize Gift Card or PayPal
 */
exports.authorize = function (order, paymentInstrument) {
  var errorMessage;

  try {
    Logger.debug('dw.order.payment.authorize');

    if (!order || !paymentInstrument) {
      Logger.error('authorize - required parameters are missing!');
      return new Status(Status.ERROR);
    }
    var HookMgr = require('dw/system/HookMgr');
    var authorizationResult;
    var paymentProcessor = paymentInstrument.paymentTransaction.paymentProcessor;
    var args = {};
    args.session = {};
    args.Order = order;
    args.remoteAddress = order.remoteHost;
    args.customer = order.getCustomer();
    args.deviceFingerPrintID = ''; //  no device fingerprint on the OCAPI calls
    //Capture the no of failure attempts executed by the customer in the same session
    var attempts = session.custom.failAttempts || 0;
    attempts += 1;
    session.custom.failAttempts = attempts;

    args.failAttempts = attempts;
    if (HookMgr.hasHook('app.payment.processor.' + paymentProcessor.ID.toLowerCase())) {
      authorizationResult = HookMgr.callHook(
        'app.payment.processor.' + paymentProcessor.ID.toLowerCase(),
        'Authorize',
        order.orderNo,
        paymentInstrument,
        paymentProcessor,
        args
      );
    } else {
      authorizationResult = HookMgr.callHook('app.payment.processor.default', 'Authorize');
    }
    if (authorizationResult.error) {
      return new Status(Status.ERROR);
    }
    return new Status(Status.OK);
  } catch (ex) {
    errorMessage = 'authorize error: ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber;
    Logger.error(errorMessage);
    return new Status(Status.ERROR, 'FAULT', errorMessage);
  }

  // error if we made it this far
  return new Status(Status.ERROR);
};

/**
 * Exports order to OMS
 * @param order dw.order.Order
 *
 */
function exportOrderToOMS(order) {
  var OrderAPIUtils = require('*/cartridge/scripts/util/OrderAPIUtil');
  try {
    var responseOrderXML = OrderAPIUtils.createOrderInOMS(order);
    var resXML = new XML(responseOrderXML);
    Logger.debug('resXML.child(ResponseMessage) -->' + resXML.child('ResponseMessage'));

    if (resXML.child('ResponseMessage').toString() === 'Success') {
      Transaction.wrap(function () {
        order.exportStatus = Order.EXPORT_STATUS_EXPORTED;
        order.trackOrderChange('Order Export Successful');
        order.trackOrderChange(responseOrderXML);
        order.custom.reversalForCancelOrder = false;
      });
    } else {
      Transaction.wrap(function () {
        order.exportStatus = Order.EXPORT_STATUS_FAILED;
        order.trackOrderChange('Order Exported Failed');
        order.trackOrderChange(responseOrderXML);
        order.custom.reversalForCancelOrder = false;
      });
    }
  } catch (err) {
    RootLogger.fatal('Error while calling the Order Create Service for Order ' + order.orderNo + ' error message ' + err.message);
  }
}
/**
 * Places order if order is still in created status and
 * auth amount is equal to or greater than order total
 * @param {dw.order.Order} order
 * @return {Status} Status - Status response
 */
function placeOrder(order) {
  var status = new Status(Status.OK);
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  var OrderModel = require('*/cartridge/models/order');
  var orderInfo = new OrderModel(order, { containerView: 'order' });
  var orderTotal = orderInfo.totals && orderInfo.totals.grandTotalValue ? Number(orderInfo.totals.grandTotalValue) : 0;
  if (orderTotal > 0) {
    var totalAuthorizedAmount = 0;
    if (orderInfo.billing && orderInfo.billing.payment && orderInfo.billing.payment.selectedPaymentInstruments) {
      orderInfo.billing.payment.selectedPaymentInstruments.forEach(function (paymentMethod) {
        totalAuthorizedAmount += Number(paymentMethod.amount);
      });
    }
    if (totalAuthorizedAmount >= orderTotal) {
      try {
        Transaction.begin();
        var placeOrderStatus = dw.order.OrderMgr.placeOrder(order);
        if (placeOrderStatus === Status.ERROR) {
          throw new Error();
        }
        order.setExportStatus(Order.EXPORT_STATUS_READY);
        Transaction.commit();
      } catch (e) {
        RootLogger.fatal('Error while placing order ' + order.orderNo + ' error message ' + e.message);
        Transaction.wrap(function () {
          dw.order.OrderMgr.failOrder(order);
        });
        // Call cancel reservation.
        hooksHelper('app.inventory.reservation', 'cancelInventoryReservation', [order.custom.reservationID]);
        var args = {};
        args.session = {};
        args.Order = order;
        args.remoteAddress = order.remoteHost;
        args.customer = order.getCustomer();
        args.deviceFingerPrintID = ''; //  no device fingerprint on the OCAPI calls
        //Capture the no of failure attempts executed by the customer in the same session
        var attempts = session.custom.failAttempts || 0;
        attempts += 1;
        session.custom.failAttempts = attempts;
        args.failAttempts = attempts;
        // call Reversal for any technical error
        hooksHelper('app.payment.processor.ipa_credit', 'Reverse', [order.orderNo, args]);
        var errorMessage = Resource.msg('error.technical', 'checkout', null);
        status = new Status(Status.ERROR, 'FAULT', errorMessage);
      }
    } else {
      var errorMessage = Resource.msg('error.message.invalid.authorization', 'error', null);
      status = new Status(Status.ERROR, 'FAULT', errorMessage);
    }
  } else {
    var errorMessage = Resource.msg('error.message.invalid.order.total', 'error', null);
    status = new Status(Status.ERROR, 'FAULT', errorMessage);
  }
  return status;
}

function afterPlaceOrder(order) {
  // Update EDD Date
  var shippingHelpers = require('*/cartridge/scripts/checkout/shippingHelpers');
  // Delete the EDD Save Zip code
  delete session.privacy.EDDZipCode;
  var orderShipments = order.shipments;
  Transaction.wrap(function () {
    for (var i = 0; i < orderShipments.length; i++) {
      var thisShipment = orderShipments[i];
      var EDDMap;
      if ('EDDResponse' in thisShipment.custom && !empty(thisShipment.custom.EDDResponse)) {
        EDDMap = shippingHelpers.getEDDDateMap(JSON.parse(thisShipment.custom.EDDResponse));
      }
      var orderEstimateddate = shippingHelpers.getEDDdate(thisShipment.shippingMethod, EDDMap);
      if (orderEstimateddate.EstimatedDate) {
        thisShipment.custom.selectedEstimatedDate = orderEstimateddate.EstimatedDate;
      }
      if (orderEstimateddate.OrdercreationEstimatedDate) {
        thisShipment.custom.selectedEstimatedDateforOMS = orderEstimateddate.OrdercreationEstimatedDate;
      }

      delete thisShipment.custom.EDDResponse;
    }
  });

  Transaction.wrap(function () {
    //SFDEV-11204 | Generate OMS Create Order XML to be used in ExportOrders batch job
    var OrderAPIUtils = require('*/cartridge/scripts/util/OrderAPIUtil');
    order.custom.omsCreateOrderXML = OrderAPIUtils.buildRequestXML(order);
  });

  if (customPreferences.orderCreateBatchEnabled !== true) {
    exportOrderToOMS(order);
  }
}

if (customPreferences.orderCreateBatchEnabled !== true) {
  exportOrderToOMS(order);
}

/**
 * After payment instrument is patched on the order
 *
 */
exports.afterPATCH = function (order, paymentInstrument, newPaymentInstrument, successfullyAuthorized) {
  var status = new Status(Status.OK);
  if (successfullyAuthorized) {
    if (order.status && order.status.value === dw.order.Order.ORDER_STATUS_CREATED) {
      status = placeOrder(order);
    }
    if (!status.error) {
      afterPlaceOrder(order);
    } else {
      return status;
    }
  } else {
    // Call cancel reservation.
    var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
    hooksHelper('app.inventory.reservation', 'cancelInventoryReservation', [order.custom.reservationID]);
  }
};

exports.beforePOST = function (basket) {
  var PaymentInstrument = require('dw/order/PaymentInstrument');
  var collections = require('*/cartridge/scripts/util/collections');

  var paymentInstruments = basket.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);

  // Logic to apply a Tender Type promotion to the Cart.
  collections.forEach(paymentInstruments, function (paymentInstr) {
    var cardType = paymentInstr.creditCardType;
    if (cardType === 'SAKS' || cardType === 'SAKSMC' || cardType === 'HBC' || cardType === 'HBCMC' || cardType === 'MPA') {
      session.custom.isHBCTenderType = true;
    } else {
      session.custom.isHBCTenderType = false;
    }
  });

  dw.system.HookMgr.callHook('dw.order.calculate', 'calculate', basket);

  COHelpers.prorateLineItemDiscounts(basket);

  return new dw.system.Status(dw.system.Status.OK);
};
