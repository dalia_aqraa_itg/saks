'use strict';

/**
 * OCAPI Extensions GiftCardApi Controller.
 *
 * @module controllers/GiftCardApi
 */

var server = require('server'); //the server module is used by all controllers
var ipaUtil = require('*/cartridge/scripts/util/ipaGCUtils');
var Logger = require('dw/system/Logger');
var URLUtils = require('dw/web/URLUtils');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
/**
 * Fetches order history from SFCC, OMS and merges them and renders response as JSON.
 *
 */
server.post('BalanceCheck', server.middleware.https, function (req, res, next) {
  // Input Params GiftCardNo, PIN
  //	var gcnumber = req.querystring.gcnumber;
  //	var gcpin = req.querystring.gcpin;
  //	var ordernumber = req.querystring.ordernumber;

  var gcnumber = '';
  var gcpin = '';
  var ordernumber = '';

  //Logger.debug(' getRequestBodyAsString ' + req.body);

  var requestBody = req.body;
  //Logger.debug(' requestBody ' + requestBody);

  var requestParams = JSON.parse(requestBody);
  //Logger.debug(' requestParams ' + requestParams);

  gcnumber = requestParams.gcnumber;
  //Logger.debug(' gcnumber ' + gcnumber);

  gcpin = requestParams.gcpin;
  //Logger.debug(' gcpin ' + gcpin);

  ordernumber = requestParams.ordernumber;
  //Logger.debug(' ordernumber ' + ordernumber);

  var resposneGC = ipaUtil.gcBalanceCheck(gcnumber, gcpin, ordernumber);

  //Logger.debug(' IN CONTROLLER:\n{0}', JSON.stringify(resposneGC));
  var giftcardJson = resposneGC;
  res.json(giftcardJson);
  next();
});

module.exports = server.exports();
