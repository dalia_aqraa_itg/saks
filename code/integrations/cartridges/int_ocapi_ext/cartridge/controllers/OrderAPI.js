'use strict';

var server = require('server');
var Logger = require('dw/system/Logger');
var OMSLogger = Logger.getLogger('OMS', 'OMSService');

/**
 * OCAPI Extensions OrderApi Controller.
 *
 * @module controllers/OrderApi
 */

var server = require('server');

/**
 * Fetches order history from SFCC, OMS and merges them and renders response as
 * JSON.
 *
 */
server.post('History', server.middleware.https, function (req, res, next) {
  var customer = req.currentCustomer;
  var orderHistoryResponse = '';
  if (!customer || !customer.profile) {
    res.json({
      error: 'customer needs to authenticate to access order history'
    });
  } else {
    var OrderHistoryHelper = require('*/cartridge/scripts/helpers/OrderHistoryHelper');
    var result = OrderHistoryHelper.validateRequest(request.httpParameterMap.requestBodyAsString);
    if (result.valid) {
      try {
        orderHistoryResponse = OrderHistoryHelper.getOrderHistory(customer, result.orderHistoryRequest, req.locale.id);
      } catch (ex) {
        OMSLogger.error('[OMS] Error : {0} {1}', ex, ex.stack);
        res.json({
          error: 'Error in processing response from Order Management'
        });
      }
      res.json({
        orderHistory: orderHistoryResponse
      });
    } else {
      res.json({
        error: result.message
      });
    }
  }
  next();
});

/**
 * Renders order detail response as JSON.
 *
 */
server.post('Detail', server.middleware.https, function (req, res, next) {
  //Input format
  //{ "OrderDetailsRequest" : {
  //   "OrderNumber" : "3000030171",
  //   "OrderType" : "0001",
  //  "BusinessUnit" : "BAY",
  //  "IsTrackingURLRequired" : "Y",
  //   "BillToID" : "",
  //   "ZipCode" : "11558-1218",
  //    "InvokedFrom" : "Online"
  //    }
  //}
  var OrderDetailsHelper = require('*/cartridge/scripts/helpers/OrderDetailsHelper');
  var result = OrderDetailsHelper.validateRequest(request.httpParameterMap.requestBodyAsString);
  if (result.valid) {
    var orderDetails = '';
    try {
      var orderDetailsRequest = result.orderDetailsRequest;
      orderDetailsRequest.customerId = req.currentCustomer && req.currentCustomer.profile ? req.currentCustomer.profile.customerNo : '';
      orderDetails = OrderDetailsHelper.getOrderDetails(orderDetailsRequest);
      if (orderDetails) {
        res.json({
          order: orderDetails
        });
      } else {
        res.json({
          error: 'Did not get order data from OMS'
        });
      }
    } catch (ex) {
      OMSLogger.error('[OMS] Error : {0} {1}', ex, ex.stack);
      res.json({
        error: 'Error in processing response from Order Management'
      });
    }
  } else {
    res.json({
      error: result.message
    });
  }
  next();
});

module.exports = server.exports();
