'use strict';

/**
 * OCAPI Extensions WaitlistApi Controller.
 *
 * @module controllers/WaitlistApi
 */

var server = require('server'); //the server module is used by all controllers
var Helper = require('*/cartridge/scripts/helper/Helper');

/**
 * Adds a waitlist entry into the waitlist custom object.
 *
 */
server.post('Add', server.middleware.https, function (req, res, next) {
  // **** Developer Note:  leverage storefront controller implementation as much as possible ****

  // Input Params
  // emailId (required), productId (required), phoneNumber (optional)
  var Logger = require('dw/system/Logger');

  var requestBody = req.body;
  Logger.debug(' requestBody ' + requestBody);

  var requestParams = JSON.parse(requestBody);
  Logger.debug(' requestParams ' + requestParams);

  var email = requestParams.waitlistEmail;
  var productID = requestParams.productID;
  var mobile = requestParams.waitlistMobile;

  var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
  var CustomObjectMgr = require('dw/object/CustomObjectMgr');
  var Transaction = require('dw/system/Transaction');
  var Resource = require('dw/web/Resource');
  var Logger = require('dw/system/Logger');

  var StringUtils = require('dw/util/StringUtils');
  var Calendar = require('dw/util/Calendar');

  if (email && productID) {
    try {
      Transaction.wrap(function () {
        Helper.createWaitlistDetailsObject(email, productID, mobile);
      });
    } catch (e) {
      res.json({
        success: false,
        msg: Resource.msg('msg.waitlist.not.submitted', 'product', null)
      });

      return next();
    }
    res.json({
      success: true,
      msg: Resource.msg('msg.waitlist.submitted', 'product', null)
    });
  } else {
    res.json({
      success: false,
      msg: Resource.msg('msg.waitlist.not.submitted', 'product', null)
    });
  }

  next();
});

module.exports = server.exports();
