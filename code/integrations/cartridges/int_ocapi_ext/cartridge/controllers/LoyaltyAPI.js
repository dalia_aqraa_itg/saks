'use strict';

/**
 * OCAPI Extensions LoyaltyApi Controller.
 *
 * @module controllers/LoyaltyApi
 */

var server = require('server'); //the server module is used by all controllers
var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
var Logger = require('dw/system/Logger');
/**
 * Verifies a given loyalty id (rewards number) and returns points balance.
 *
 */
server.post('GetInfo', server.middleware.https, function (req, res, next) {
  // **** Developer Note:  leverage storefront controller implementation as much as possible ****

  // Input Params
  // rewardsId (required)

  // Call Loyalty Service (for TheBay) as mentioned in HBC_ISD_Loyalty_Bay section 5.1.3.3
  var requestBody = req.body;
  //Logger.debug(' requestBody ' + requestBody);

  var requestParams = JSON.parse(requestBody);
  //Logger.debug(' requestParams ' + requestParams);

  rewardsId = requestParams.rewardsId;
  //Logger.debug(' rewardsId ' + rewardsId);

  if (rewardsId !== null && rewardsId != '') {
    var loyaltyResponse = hooksHelper(
      'app.customer.loyalty.rewards.info',
      'checkLoyaltyRewards',
      rewardsId,
      require('*/cartridge/scripts/loyalty/loyaltyRewardsInfoUtil').checkLoyaltyRewards
    );

    // Logger.debug("BAY loyaltyResponse -->>>>" + JSON.stringify( loyaltyResponse));

    if (!(loyaltyResponse !== null && Number(loyaltyResponse.ResponseCode) === 0)) {
      res.json({
        action: 'LoyaltyAPI-GetInfo',
        queryString: '',
        locale: 'en_CA',
        ResponseCode: '99',
        ResposneMessage: 'ERROR - Record not found'
      });
    } else {
      res.json(loyaltyResponse);
    }
  } else {
    res.json({
      action: 'LoyaltyAPI-GetInfo',
      queryString: '',
      locale: 'en_CA',
      ResponseCode: '99',
      ResposneMessage: 'ERROR - Request Input Missing'
    });
  }

  next();
});

module.exports = server.exports();
