'use strict';

/**
 * Controller that handles feeds for ShopRunner
 *
 * @module controllers/SRFeeds
 */

/* API includes */

/* Script Modules */

/**
 * This will call the ShopRunner WebService to send them data about the order.
 * This will fire when the order is placed.
 */
function sendOrderToSR(order) {
  var result = require('int_shoprunner/cartridge/scripts/feeds/placeOrder').placeOrder(order);
  return result;
}

/*
 * Private methods
 */
exports.SendOrderToSR = sendOrderToSR;
