'use strict';

/**
 * Controller that handles various ShopRunner functions in the storefront
 *
 * @module controllers/ShopRunner
 */

/* API includes */
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');

var Cart = app.getModel('Cart');
var Order = app.getModel('Order');

var PayRunner = require('*/cartridge/controllers/PayRunner');

/**
 * Hooks from COCustomer use this controller to select either the regular
 * shipping controller or the multiship controller
 */
function checkoutMethod() {
  var cart, Status;
  cart = Cart.get();
  if (empty(session.custom.srtoken)) {
    return Status;
  } else {
    cart = Cart.get();
    var Status = require('~/cartridge/scripts/checkout/CheckCartEligibility').checkEligibility(cart);
    return Status;
  }
}

/**
 * Custom CheckAddress controller specific to ShopRunner
 */
function checkAddress() {
  //checkout/CheckPOBox.ds
  //checkout/CheckAPOFPO.ds
  return;
}

/**
 * Custom ClearCheckoutForms controller specific to ShopRunner
 */
function clearCheckoutForms() {
  var SavePayment;
  app.getForm('singleshipping').clear();
  app.getForm('multishipping').clear();
  if (SavePayment !== 'save') {
    app.getForm('billing').clear();
  }
  return;
}

/**
 * If a user is logged in to ShopRunner, this checks if their basket
 * qualifies and checks against the mixed order preference
 */
function eligibleBasket() {
  var srtoken = require('~/cartridge/scripts/checkout/GetShopRunnerToken').getToken();
  var cart, Status, srtoken;
  if (empty(session.custom.srtoken || empty(srtoken))) {
    return;
  } else {
    cart = Cart.get();
    var Status = require('~/cartridge/scripts/checkout/CheckCartEligibility').checkEligibility(cart);
    return Status;
  }
}

function handleForm() {
  var cart = Cart.get().object;
  var ineligibleCartForm = app.getForm('ineligiblecart');
  ineligibleCartForm.handleAction({
    keepitems: function () {
      require('~/cartridge/scripts/DeleteShopRunnerCookie').deleteCookie();
      session.custom.keepItems = true;
      response.redirect(URLUtils.https('COCustomer-Start'));
    },
    removeitems: function () {
      Transaction.wrap(function () {
        require('~/cartridge/scripts/checkout/RemoveNonShopRunner').removeItems(cart);
        session.custom.removeItems = true;
        response.redirect(URLUtils.https('COCustomer-Start'));
      });
    }
  });
}

/**
 * Determines whether or not multi ship is required.
 */
function requireMultiShip() {
  if (Site.getCurrent().getCustomPreferenceValue('sr_mixedorder').value !== 'splitorder') {
    return true;
  }
  return false;
}

/**
 * This is ShopRunner place order entry point for ShopRunner only
 * (when PayRunner is disabled). This should be public.
 */
function placeOrder() {
  var placeOrderResult = placeOrderPrivate();
  if (placeOrderResult.placeOrderStatus.status === dw.system.Status.OK) {
    app.getController('COSummary').ShowConfirmation(placeOrderResult.order);
  } else {
    response.redirect(URLUtils.https('COSummary-Start'));
  }
}

/**
 * If we have a mixed order of SR-eligible and non-SR-eligible items,
 * we need to split the order up and place two separate orders.
 */
function placeMultiOrder() {
  var cart, FirstOrder, Order, PayRunnerSecondOrder, SavePayment;
  cart = Cart.get().object;
  this.storeShopRunnerItems(cart);
  Transaction.wrap(function () {
    var SrOrderItems = require('~/cartridge/scripts/checkout/PullShoprunnerShipment').pullShipment(cart);
  });
  SavePayment = 'save';
  var placeOrderResult = app.getController('COPlaceOrder').Start();
  MainOrder = placeOrderResult.Order;
  if (empty(cart.getBillingAddress())) {
    cart.createBillingAddress();
  }
  require('~/cartridge/scripts/checkout/BuildShoprunnerBasket').buildBasket(cart, MainOrder, SrOrderItems);
  app.getController('COBilling').HandlePaymentSelection();
  PayRunnerSecondOrder = true;
  app.getController('COPlaceOrder').Start();
  saveOrderToken();
  //require('int_shoprunner/cartridge/controllers/SRFeeds').SendOrderToSR(placeOrderResult.Order);
}

/**
 * This is the main Place Order functionality controller.
 */
function placeOrderPrivate() {
  var cart, PlaceOrderError, Status;
  cart = Cart.get().object;
  if (!cart) {
    return;
  }
  var Status = require('~/cartridge/scripts/checkout/CheckCartEligibility').checkEligibility(cart);
  if (!Status) {
    return;
  }
  PlaceOrderError = null;
  if (Site.getCurrent().getCustomPreferenceValue('sr_mixedorder').value.value !== 'splitorder' || Status !== 'MIXED') {
    var placeOrderResult = app.getController('COPlaceOrder').Start();
    if (placeOrderResult.order_created) {
      saveOrderToken(placeOrderResult.Order);
      //require('int_shoprunner/cartridge/controllers/SRFeeds').SendOrderToSR(placeOrderResult.Order);
      if (!empty(session.custom.productListID)) {
        require('int_shoprunner/cartridge/controllers/TransferCartToProductList').ProductListToBasket();
        session.custom.productListID == null;
      }
      return {
        placeOrderStatus: new dw.system.Status(dw.system.Status.OK),
        order: placeOrderResult.Order
      };
    } else {
      return new dw.system.Status(dw.system.Status.ERROR);
    }
  } else {
    placeMultiOrder();
  }
}

/**
 * Simple controller for save the ShopRunner Order Token
 */
function saveOrderToken(order) {
  Transaction.wrap(function () {
    require('~/cartridge/scripts/checkout/SaveShopRunnerOrderToken').saveToken(session, order);
  });
  return;
}

/**
 * During a split shipment, this controller is used to set the confirmation
 * page redirect so that it goes to the most recently placed order.
 */
function setupConfirmation() {
  var order, orderNo, firstOrderNo;
  orderNo = session.custom.orderNo;
  order = Order.get(orderNo);
  if (!order) {
    response.redirect(URLUtils.https('Account-Show'));
  } else {
    if (typeof session.custom.firstOrderNo !== 'undefined' && empty(session.custom.firstOrderNo) == false) {
      firstOrderNo = session.custom.firstOrderNo;
      order = Order.get(firstOrderNo);
      if (!order) {
        response.redirect(URLUtils.https('Account-Show'));
      }
    }
  }
  app.getController('COSummary').ShowConfirmation(order.object);
}

/**
 * ShopRunner partner landing page. Loads up an iFrame.
 */
function show() {
  app.getView().render('shoprunner/landing');
}

/**
 * If splitorders are active and we have a mixed cart, pull all
 * shoprunner items into a seperate/hidden shipment.
 */
function storeShopRunnerItems(cart) {
  var Status;
  if (empty(session.custom.srtoken)) {
    return;
  } else {
    Status = require('~/cartridge/scripts/checkout/CheckCartEligibility').checkEligibility(cart);
    if (Site.getCurrent().getCustomPreferenceValue('sr_mixedorder').value === 'splitShip' && Status === 'MIXED') {
      Transaction.wrap(function () {
        require('~/cartridge/scripts/checkout/PullShoprunnerItems').pullItems(cart);
      });
    } else {
      return;
    }
  }
}

/**
 * Custom Payment Validation for ShopRunner
 */
function validatePayment() {
  require('~/cartridge/scripts/checkout/GetNonGiftCertificatePaymentAmount').getAmount(cart);
  Transaction.wrap(function () {
    require('~/cartridge/scripts/checkout/ValidatePaymentInstruments').validate(cart);
  });
  //checkout/CalculatePaymentTransactionTotals.ds
  return;
}

/**
 * Called by ShopRunner Javascript after a successful login.
 * Write the token (or blank token) to the session and cookie
 * after re-verifying with ShopRunner.
 */
function validateToken() {
  var cart, token, validToken;
  cart = !empty(Cart.get()) ? Cart.get().object : null;
  token = request.httpParameterMap.srtoken.value;
  var result = require('~/cartridge/scripts/ShopRunnerAuth').validate(token, cart);
  app
    .getView({
      ValidToken: result.validToken
    })
    .render('shoprunner/cookiesession');
}

/*
 * This is the main processing controller when using PayRunner. All incoming PayRunner
 * requests invoke this controller and it is determined within this controller what
 * functionality to execute based on the request's method parameter.
 *
 */
function payrunnerAPI() {
  var jsonPRCart, AppliedPromoStatus, couponStatus, couponCode, ShopRunnerFrom;
  var basket = Cart.get();
  if (!basket) {
    return;
  }
  var method = request.httpParameterMap.method.value;
  var firstName = request.httpParameterMap.firstName.value;
  var lastName = request.httpParameterMap.lastName.value;
  var email = request.httpParameterMap.email.value;
  var phone = request.httpParameterMap.phone.value;
  var cart = request.httpParameterMap.cart.value;
  var srCheckoutId = request.httpParameterMap.SRCheckoutId.value;
  var payRunnerResult;

  if (method == 'getPRCart' && !empty(request.httpParameterMap.pid.value) && !empty(request.httpParameterMap.Quantity.value)) {
    PayRunner.SetupPDPCheckout();
  }

  PayRunner.InitializeShipments();

  Transaction.wrap(function () {
    basket.calculate();
  });

  if (method === 'applyPRGiftCard') {
    var gcResult = PayRunner.HandleApplyPRGiftCard();
  }

  if (method === 'updatePRCart') {
    AppliedPromoStatus = PayRunner.HandleUpdatePRCart();
  }

  if (method === 'startPRCheckout' || method === 'updatePRCart' || method === 'applyPRGiftCard') {
    PayRunner.UpdateGiftCertificates();
  }

  if (empty(AppliedPromoStatus)) {
    AppliedPromoStatus = '';
  }

  var payRunnerArguments = {
    APIMethod: method,
    Basket: basket.object,
    Firstname: firstName,
    Lastname: lastName,
    Email: email,
    Phone: phone,
    Cart: cart,
    SRCheckoutId: srCheckoutId,
    SRPromoStatus: AppliedPromoStatus,
    CouponStatus: couponStatus,
    CouponCode: couponCode
  };

  if (!empty(gcResult)) {
    (payRunnerArguments['NewGCPaymentInstrument'] = gcResult.NewGCPaymentInstrument),
      (payRunnerArguments['GiftCertificateStatus'] = gcResult.GiftCertificateStatus),
      (payRunnerArguments['EnteredGC'] = gcResult.EnteredGC);
  }

  Transaction.wrap(function () {
    payRunnerResult = require('~/cartridge/scripts/payrunner/payrunnerAPI').getMethods(payRunnerArguments);
  });

  var ShippingObj = payRunnerResult.ShippingObj;
  var BillingObj = payRunnerResult.BillingObj;
  var CreditCardPayment = payRunnerResult.CreditCardPayment;
  var jsonPRCart = payRunnerResult.JSONPRCart;

  Transaction.wrap(function () {
    basket.calculate();
  });

  if (method === 'reviewOrder') {
    PayRunner.SetupOrder(ShippingObj, BillingObj, CreditCardPayment, jsonPRCart);
  }

  if (method === 'processPROrder') {
    PayRunner.SetupOrder(ShippingObj, BillingObj, CreditCardPayment, jsonPRCart);
    PayRunner.UpdatePaymentInstruments();
    var url = require('~/cartridge/scripts/util/srFriendlyURL').getURL();
    if (ShopRunnerFrom === 'Product-Show') {
      PayRunner.PreparePDPPlaceOrderRedirect();
    } else {
      var placeOrderResult = placeOrderPrivate();
      if (!placeOrderResult.error) {
        jsonPRCart = PayRunner.PrepareCartPlaceOrderRedirect(placeOrderResult.order);
      } else {
        jsonPRCart = PayRunner.PrepareCartPlaceOrderRedirect(null);
      }
    }
  }

  app
    .getView({
      jsonPRCart: jsonPRCart
    })
    .render('payrunner/getPRCart');
}

/*
 * Module exports
 */

/*
 * Web-exposed methods
 */
/** @see module:controllers/ShopRunner~show */
exports.Show = guard.ensure(['https'], show);
/** @see module:controllers/ShopRunner~validateToken */
exports.ValidateToken = guard.ensure(['get'], validateToken);
/** @see module:controllers/ShopRunner~payrunnerAPI */
exports.PayrunnerAPI = guard.ensure(['https'], payrunnerAPI);
/** @see module:controllers/ShopRunner~setupConfirmation */
exports.SetupConfirmation = guard.ensure(['get', 'https'], setupConfirmation);
/** @see module:controllers/ShopRunner~handleForm */
exports.EligibleBasketForm = guard.ensure(['https', 'post'], handleForm);

/*
 * Private methods
 */
exports.CheckoutMethod = checkoutMethod;
exports.EligibleBasket = eligibleBasket;
exports.StoreShopRunnerItems = storeShopRunnerItems;
exports.RequireMultiShip = requireMultiShip;
exports.ClearCheckoutForms = clearCheckoutForms;
exports.CheckAddress = checkAddress;
exports.SaveOrderToken = saveOrderToken;
exports.PlaceOrder = placeOrder;
exports.PlaceOrderPrivate = placeOrderPrivate;
exports.PlaceMultiOrder = placeMultiOrder;
exports.ValidatePayment = validatePayment;
