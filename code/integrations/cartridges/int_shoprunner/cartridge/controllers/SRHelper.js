'use strict';

/**
 * Helper controller used to execute int_shoprunner scripts from storefront cartridge controllers.
 * This is done to make cartridge integration simpler as well as make future adjustments to scripts easier.
 *
 * @module controllers/SRHelper
 */

/* API includes */
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');

var Cart = app.getModel('Cart');

function getPREligibilityCart() {
  var result;
  var cart = Cart.get();
  if (!cart) {
    return;
  }
  var srToken = session.custom.srtoken;
  Transaction.wrap(function () {
    result = require('~/cartridge/scripts/payrunner/getPREligibilityCart').getEligibility(cart.object, srToken);
  });
  return result;
}

function getApplicableShippingMethods(params) {
  var applicableMethods;
  Transaction.wrap(function () {
    applicableMethods = require('~/cartridge/scripts/checkout/GetApplicableShippingMethods').getMethods(params);
  });
  return applicableMethods;
}

function deleteShopRunnerCookie() {
  require('~/cartridge/scripts/DeleteShopRunnerCookie').deleteCookie();
  return;
}

function filterMultiShipMethods(shippingMethods) {
  var result = require('~/cartridge/scripts/checkout/FilterMultiShipMethods').filterMethods(shippingMethods);
  return result;
}

function checkCartEligibility(cart) {
  var result = require('~/cartridge/scripts/checkout/CheckCartEligibility').checkEligibility(cart);
  return result;
}

function preCalculateDefaultShipment() {
  Transaction.wrap(function () {
    //checkout/PreCalculateDefaultShipment.ds
  });
}

function createShipmentShippingAddress() {
  Transaction.wrap(function () {
    //checkout/CreateShipmentShippingAddress.ds
  });
}

function checkGroundFreePromo() {
  var cart = Cart.get();
  var result = require('~/cartridge/scripts/checkout/CheckShippingGroundFree').checkFreeGround(cart.object);
  return result;
}

/*
 * Private methods
 */
exports.GetPREligibilityCart = getPREligibilityCart;
exports.GetApplicableShippingMethods = getApplicableShippingMethods;
exports.DeleteShopRunnerCookie = deleteShopRunnerCookie;
exports.FilterMultiShipMethods = filterMultiShipMethods;
exports.CheckCartEligibility = checkCartEligibility;
exports.PreCalculateDefaultShipment = preCalculateDefaultShipment;
exports.CreateShipmentShippingAddress = createShipmentShippingAddress;
exports.CheckGroundFreePromo = checkGroundFreePromo;
