'use strict';

/**
 * Controller that handles various PayRunner functions in the storefront
 *
 * ShopRunner Express Checkout
 *
 * @module controllers/PayRunner
 */

/* API includes */
var GiftCertificateMgr = require('dw/order/GiftCertificateMgr');
var ProductMgr = require('dw/catalog/ProductMgr');
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');

var Cart = app.getModel('Cart');

/**
 * Used to add the current PDP product to the cart.
 */
function addItem() {
  var cart;
  var pid = request.httpParameterMap.pid.stringValue;
  var product = ProductMgr.getProduct(pid);
  if (empty(product)) {
    return;
  }
  cart = Cart.get().object;
  cart.addProductToCart();
}

/**
 * Sets up billing-related details in order to place an order through
 * the ShopRunner Express popup.
 */
function billing(BillingObj, CreditCardPayment, jsonPRCart) {
  var cart = Cart.get();
  Transaction.wrap(function () {
    cart.object.custom.shoprunnerExpressOrder = true;
  });
  var billingAddress = cart.getBillingAddress();
  if (empty(billingAddress)) {
    Transaction.wrap(function () {
      billingAddress = cart.createBillingAddress();
    });
  }
  Transaction.wrap(function () {
    require('~/cartridge/scripts/payrunner/placeorder/AddBillingAddress').addAddress(billingAddress, BillingObj);
  });
  var UserEmail = customer.authenticated ? customer.profile.email : BillingObj.billingEmail.toString();
  Transaction.wrap(function () {
    require('~/cartridge/scripts/payrunner/placeorder/AddBillingEmail').addEmail(cart, UserEmail, jsonPRCart);
  });
  //payrunner/placeorder/getCVNLength.ds
  var billingForm = app.getForm('billing').object;
  var paymentMethods = billingForm.paymentMethods;
  paymentMethods.selectedPaymentMethodID.value = 'CREDIT_CARD';
  paymentMethods.creditCard.number.value = CreditCardPayment.CCNumber;
  // need to force an override for CVV checking since ShopRunner does NOT return the CVV
  // with their payment information
  //paymentMethods.creditCard.cvn.value = CreditCardPayment.creditCard == 'Amex' ? '0000' : '000';
  paymentMethods.creditCard.expiration.month.value = Number(CreditCardPayment.CCMonth);
  paymentMethods.creditCard.expiration.year.value = Number(CreditCardPayment.CCYear);
  paymentMethods.creditCard.type.value = CreditCardPayment.creditCard;
  paymentMethods.creditCard.owner.value = CreditCardPayment.CCHolderName;
  var ccStatus = app.getController('COBilling').HandlePaymentSelection(cart);
  /* if (ccStatus.error) {
        require('~/cartridge/scripts/payrunner/placeorder/HandleCompletePurchaseStatus').handlePurchase(ccStatus, jsonPRCart);
        return;
    } */
  billingForm.fulfilled.value = true;
  return;
}

/**
 * Used to clear the current basket during a PDP Express Checkout
 * (contents have already been saved to a new ProductList
 * to be used to reinstate this basket downstream.
 */
function clearCart() {
  var cart;
  cart = Cart.get().object;
  var plis = cart.getAllProductLineItems();
  Transaction.wrap(function () {
    for (var i = 0; i < plis.length; i++) {
      var pli = plis[i];
      cart.removeProductLineItem();
    }
  });

  cart.calculate();
}

/**
 * Sets up shipping-related details in order to place an
 * order through the ShopRunner Express popup.
 */
function defaultShipping(ShippingObj) {
  var cart, validationResult;
  cart = Cart.get();
  var BasketSR = request.httpParameterMap.cart.stringValue;
  var Context = 'Default Shipping';
  var ProductLineItems = cart.object.getAllProductLineItems();
  var Shipment = cart.object.getDefaultShipment();
  var params = {
    BasketSR: BasketSR,
    Context: Context,
    ProductLineItems: ProductLineItems,
    Shipment: Shipment
  };
  Transaction.wrap(function () {
    require('~/cartridge/scripts/payrunner/placeorder/AddShippingAddress').addAddress(Shipment, ShippingObj);
    require('~/cartridge/scripts/payrunner/placeorder/AddShippingMethod').addMethod(params);
    cart.calculate();
  });
  validationResult = cart.validateForCheckout();
  return validationResult;
}

/**
 * Used during an applyPRGiftCard method call to apply the Gift Card/Cert payment,
 * update the Demandware Basket, and update the ShopRunner Basket.
 */
function handleApplyPRGiftCard() {
  var GiftCardJSON = JSON.parse(request.httpParameterMap.gc.stringValue);
  var EnteredGC = GiftCardJSON.giftCards[0].number;
  var cart = Cart.get();
  app.getController('COShipping').PrepareShipments(cart.object);
  Transaction.wrap(function () {
    cart.calculate();
  });
  var result = redeemGiftCertificate(EnteredGC);
  return result;
}

/**
 * Used during an updatePRCart method call to grab any updates
 * from the ShopRunner Express Checkout popup (shipping method,
 * quantities, Promo Code), update the Demandware Basket, and
 * update the ShopRunner Basket.
 */
function handleUpdatePRCart() {
  var AppliedPromoStatus, cart, CartJSON, cartForm, couponCode, couponStatus;
  cart = Cart.get();
  CartJSON = JSON.parse(request.httpParameterMap.cart.stringValue);
  cartForm = app.getForm('cart');
  if (!empty(CartJSON.promotions)) {
    couponCode = CartJSON.promotions[0].code;
    cartForm.object.couponCode.htmlValue = couponCode;
    couponStatus = cart.addCoupon(couponCode);
    Transaction.wrap(function () {
      cart.calculate();
    });
    AppliedPromoStatus = couponStatus.CouponStatus;
  }
  var pliToRemove = require('~/cartridge/scripts/payrunner/UpdateQuantities').update(cart.object, CartJSON);
  if (!empty(pliToRemove)) {
    Transaction.wrap(function () {
      cart.removeProductLineItem(pliToRemove);
    });
  }
  Transaction.wrap(function () {
    cart.calculate();
  });
  return AppliedPromoStatus;
}

/**
 * Initialize shipments for PayRunner modal.
 * If shipping method selected shioments will be initialized
 */
function initializeShipments() {
  var cart = Cart.get();
  if (!cart) {
    return;
  } else {
    cart = cart.object;
  }
  var SRMixedOption = Site.getCurrent().getCustomPreferenceValue('sr_mixedorder');
  var Status = require('~/cartridge/scripts/checkout/CheckCartEligibility').checkEligibility(cart);

  var params = {
    BasketSR: request.httpParameterMap.cart.stringValue,
    Context: 'InitializeShipments',
    ProductLineItems: cart.allProductLineItems,
    Shipment: cart.defaultShipment
  };

  Transaction.wrap(function () {
    require('~/cartridge/scripts/payrunner/placeorder/AddShippingMethod').addMethod(params);
  });
  if (Status == 'MIXED' && SRMixedOption == 'splitShip') {
    var shippingObj = require('~/cartridge/scripts/payrunner/GetShippingObj').shippingObj(cart);
    Transaction.wrap(function () {
      require('~/cartridge/scripts/payrunner/placeorder/SetupShipments').setShipment(cart, request.httpParameterMap.cart.stringValue, shippingObj);
    });
  }

  return;
}

/**
 * This is the main Place Order functionality pipeline.
 */
function placeOrderPrivate() {
  return;
}

/**
 * Setup redirect and order number variables so we end up on the correct confirmation page.
 */
function prepareCartPlaceOrderRedirect(order, jsonPRCart) {
  var FirstOrder, FirstOrderNo, HttpParameterMap, Order, OrderNo;
  HttpParameterMap = request.httpParameterMap;
  Order = order;
  if (!empty(Order)) {
    OrderNo = Order.orderNo;
    session.custom.orderNo = !empty(OrderNo) ? OrderNo : Order.orderNo;
    session.custom.firstOrderNo = !empty(FirstOrder) ? FirstOrder.orderNo : '';
  }
  var params = {
    FirstOrderNo: FirstOrderNo,
    HttpParameterMap: HttpParameterMap,
    Order: Order,
    OrderNo: OrderNo,
    PaymentInstrument: order.getPaymentInstruments()[0]
  };
  var RedirectScript = require('~/cartridge/scripts/payrunner/placeorder/SetupConfirmationRedirect').setupRedirect(params);
  jsonPRCart = RedirectScript;
  return jsonPRCart;
}

function preparePDPPlaceOrderRedirect() {
  return;
}

/**
 * Attempts to redeem a gift certificate entered through the PayRunner popup.
 */
function redeemGiftCertificate(EnteredGC) {
  var giftCertError, paymentInstrument;
  var cart = Cart.get();
  var gc = GiftCertificateMgr.getGiftCertificateByCode(EnteredGC);
  Transaction.wrap(function () {
    paymentInstrument = cart.createGiftCertificatePaymentInstrument(gc);
  });
  if (empty(paymentInstrument)) {
    giftCertError = 'invalid_giftcert';
  } else {
    giftCertError = '';
    Transaction.wrap(function () {
      cart.calculate();
    });
  }
  return {
    NewGCPaymentInstrument: paymentInstrument,
    GiftCertificateStatus: giftCertError,
    EnteredGC: EnteredGC
  };
}

/**
 * Handles splitship option
 * @param {*} ShippingObj
 */
function shipping(ShippingObj) {
  var cart, SRMixedOption;
  cart = Cart.get();
  if (!request.httpParameterMap.method.value == 'reviewOrder') {
    SRMixedOption = Site.getCurrent().getCustomPreferenceValue('sr_mixedorder').value;
    if (Status == 'MIXED' && SRMixedOption === 'splitShip') {
      Transaction.wrap(function () {
        cart.calculate();
      });
      return;
    } else {
      if (Status == 'MIXED' && SRMixedOption == 'splitorder') {
        Transaction.wrap(function () {
          require('~/cartridge/scripts/checkout/PullShoprunnerItems').pullItems(cart);
        });
        defaultShipping(ShippingObj);
        return;
      } else {
        if (Status == 'MIXED' && SRMixedOption == 'downgrade') {
          defaultShipping(ShippingObj);
          return;
        } else {
          if (Status == 'MIXED' && SRMixedOption == 'block') {
            defaultShipping(ShippingObj);
            return;
          }
        }
      }
    }
  }
  return;
}

/**
 * Used during a PDP Express Checkout to save the existing basket as a product list,
 * clear the basket, then add the current PDP product to the basket so the
 * PayRunner popup uses the PDP basket instead of the old existing basket.
 */
function setupPDPCheckout() {
  var cart;
  cart = Cart.get().object;
  if (!cart) {
    return;
  } else {
    var ProductList = require('int_shoprunner/cartridge/controllers/TransferCartToProductList').CreateProductList();
    session.custom.productListID = ProductList.ID;
  }

  clearCart();
  addItem();
}

/**
 * Parses and validates the received Order JSON. Calls all of the
 * other pipelets that are involved inside placing order process.
 * Ends in json response.
 * @param {*} shippingObj
 * @param {*} billingObj
 * @param {*} creditCardPayment
 * @param {*} jsonPRCart
 */
function setupOrder(shippingObj, billingObj, creditCardPayment, jsonPRCart) {
  var cart;
  cart = Cart.get();
  require('~/cartridge/scripts/checkout/CheckCartEligibility').checkEligibility(cart.object);
  shipping(shippingObj);
  billing(billingObj, creditCardPayment);
  updateGiftCertificates(jsonPRCart);
  return;
}

/**
 * Redeem the Gift Certificates after updating, reviewing or purchasing the cart
 */
function updateGiftCertificates(jsonPRCart) {
  var cart, EnteredGC, giftCertError;
  cart = Cart.get();
  var gcs = require('~/cartridge/scripts/payrunner/getGiftCertificateLineItems').getGCLineItems(cart.object);
  Transaction.wrap(function () {
    for (var i = 0; i < gcs.length; i++) {
      var GiftCertificateEG = dw.order.GiftCertificateMgr.getGiftCertificateByCode(gcs[i].number);
      var EnteredGC = GiftCertificateEG.giftCertificateCode;
      var gcPI = cart.createGiftCertificatePaymentInstrument(GiftCertificateEG);
      if (empty(gcPI)) {
        giftCertError = 'invalid_giftcert';
      } else {
        giftCertError = null;
      }
    }
    cart.calculate();
  });
  return jsonPRCart;
}

function updatePaymentInstruments() {
  var cart;
  cart = Cart.get();
  var basketPaymentInstruments = cart.getPaymentInstruments();
  Transaction.wrap(function () {
    for (var i = 0; i < basketPaymentInstruments.length; i++) {
      var basketPaymentInstrument = basketPaymentInstruments[i];
      var basketPaymentTransaction = basketPaymentInstrument.getPaymentTransaction();
      var basketTransactionAmountVal = basketPaymentTransaction.getAmount().value;
      if (basketTransactionAmountVal <= 0) {
        cart.removePaymentInstrument(basketPaymentInstrument);
      }
    }
    cart.calculate();
  });
  return;
}

exports.AddItem = addItem;
exports.Billing = billing;
exports.ClearCart = clearCart;
exports.DefaultShipping = defaultShipping;
exports.HandleApplyPRGiftCard = handleApplyPRGiftCard;
exports.HandleUpdatePRCart = handleUpdatePRCart;
exports.InitializeShipments = initializeShipments;
exports.PlaceOrderPrivate = placeOrderPrivate;
exports.PrepareCartPlaceOrderRedirect = prepareCartPlaceOrderRedirect;
exports.PreparePDPPlaceOrderRedirect = preparePDPPlaceOrderRedirect;
exports.RedeemGiftCertificate = redeemGiftCertificate;
exports.SetupOrder = setupOrder;
exports.SetupPDPCheckout = setupPDPCheckout;
exports.Shipping = shipping;
exports.UpdateGiftCertificates = updateGiftCertificates;
exports.UpdatePaymentInstruments = updatePaymentInstruments;
