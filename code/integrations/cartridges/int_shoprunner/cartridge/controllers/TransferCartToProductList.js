'use strict';

/**
 * Controller that handles various PayRunner functions in the storefront
 *
 * @module controllers/TransferCartToProductList
 */

/* API includes */

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');

var Cart = app.getModel('Cart');
var ProductList = app.getModel('ProductList');

function createProductList() {
  var cart = Cart.get();
  var plis = cart.getProductLineItems();
  if (!empty(plis) && plis.size() > 0) {
    var ProductList = require('~/cartridge/scripts/payrunner/placeorder/createProductList');
    for (var i = 0; i < plis.length; i++) {
      var pli = plis[i];
      var product = ProductMgr.getProduct(pli.productID);
      var qty = pli.getQuantitValue();
      var options = pli.getOptionModel();
      ProductList.addProduct(product, qty, options);
    }
  }
  return ProductList;
}

function productListToBasket() {
  var cart = Cart.get();
  var prodList = ProductList.get(session.custom.productListID);
  var productItems = prodList.getProductItems().iterator();
  while (productItems.hasNext()) {
    var productItem = productItems.next();
    Cart.addProductListItem(productItem, productItem.quantityValue);
  }
}

exports.CreateProductList = createProductList;
exports.ProductListToBasket = productListToBasket;
