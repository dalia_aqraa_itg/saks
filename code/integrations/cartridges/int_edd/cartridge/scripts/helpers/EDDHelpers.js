'use strict';
var Logger = require('dw/system/Logger');
var HashMap = require('dw/util/HashMap');

var EDDLogger = Logger.getLogger('EDD', 'EDDService');

function parseEDDResponse(response) {
  try {
    var parseRes = JSON.parse(response);
    var ShippingMethodMap = new HashMap();
    Object.keys(parseRes).forEach(function (key) {
      var value = parseRes[key];
      if (value) {
        ShippingMethodMap.put(key, value);
      }
    });
    return ShippingMethodMap;
  } catch (e) {
    EDDLogger.error('ERROR while parseing the EDD response', e);
  }
}

module.exports = {
  parseEDDResponse: parseEDDResponse
};
