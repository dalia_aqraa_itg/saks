var Site = require('dw/system/Site');
var CurrentSite = Site.current.preferences;
var preferences = CurrentSite ? CurrentSite.custom : {};

const constants = {
  EDD_ENABLED: preferences['EDDEnabled'] || true,
  API_KEY: preferences['hbcAPIKey'] || '',
  API_KEY_GW: preferences['hbcGWAPIKey'] || '',
  EDD_BANNER: preferences['EDDBanner'] || 'bay'
};

module.exports = constants;
