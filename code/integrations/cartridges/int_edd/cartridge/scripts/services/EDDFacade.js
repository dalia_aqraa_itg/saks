'use strict';

var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');
var EDDLogger = require('dw/system/Logger').getLogger('EDD', 'EDDService');
var RootLogger = require('dw/system/Logger').getRootLogger();
var EDDServices = require('*/cartridge/scripts/services/EDDServices');
var EDDServiceFactory = require('*/cartridge/scripts/services/EDDServiceFactory');
var EDDHelpers = require('*/cartridge/scripts/helpers/EDDHelpers');
var eddConstants = require('*/cartridge/scripts/eddConstants.js');
var Result = require('dw/svc/Result');

function getEDDDate(shipment) {
  if (eddConstants.EDD_ENABLED && shipment.shippingAddress) {
    var destZipCode = shipment.shippingAddress.postalCode;
    if (destZipCode) {
      var savedEDDZipCode = session.privacy.EDDZipCode;
      if (!savedEDDZipCode || savedEDDZipCode !== destZipCode) {
        var service = EDDServiceFactory.getInstance(EDDServices.EDD);
        var response = service.call({ zipCode: destZipCode });
        if (response && response.error === 0 && response.status === 'OK') {
          Transaction.wrap(function () {
            shipment.custom.EDDResponse = JSON.stringify(response.object);
          });
          // Save Zip Code in session, so that we can avoid multiple call to EDD.
          session.privacy.EDDZipCode = destZipCode;
        } else {
          Transaction.wrap(function () {
            delete shipment.custom.EDDResponse;
          });
          if (response.status === Result.SERVICE_UNAVAILABLE && response.unavailableReason !== Result.UNAVAILABLE_DISABLED) {
            RootLogger.fatal('FATAL: There was an error with the EDD service call.', response.errorMessage);
          }
          EDDLogger.info('ERROR: There was an error with the EDD service call.', response.errorMessage);
        }
      }
    }
  }
}

module.exports = {
  getEDDDate: getEDDDate
};
