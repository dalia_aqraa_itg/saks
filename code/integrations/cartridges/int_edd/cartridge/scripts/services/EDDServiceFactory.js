'use strict';

const EDDServices = require('*/cartridge/scripts/services/EDDServices');

/**
 * EDD Services
 */
const EDDInit = require('*/cartridge/scripts/services/init/EDDInit');

/**
 * Using the serviceName return an instance of the Service.
 *
 * @param serviceName
 * @returns Instance of Service
 */
function _getServiceBuilder(serviceName) {
  if (serviceName === EDDServices.EDD) {
    return EDDInit;
  }
  throw new Error('EDD Service ID not recognized: ' + serviceName);
}

exports.getInstance = function (serviceName) {
  const builder = _getServiceBuilder(serviceName);
  return builder.getInstance();
};
