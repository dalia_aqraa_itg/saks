'use strict';

/**
 * Unique EDD service identifiers
 */
const EDDServices = {
  EDD: 'edd.http.oms'
};

module.exports = EDDServices;
