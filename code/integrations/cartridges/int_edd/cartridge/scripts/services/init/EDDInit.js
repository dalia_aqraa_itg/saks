var Site = require('dw/system/Site');
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var EDDServices = require('*/cartridge/scripts/services/EDDServices');
var eddConstants = require('*/cartridge/scripts/eddConstants.js');
var ServiceHelper = require('*/cartridge/scripts/services/helpers/ServiceHelper');

module.exports = {
  getInstance: function () {
    return LocalServiceRegistry.createService(EDDServices.EDD, {
      createRequest: function (svc, args) {
        svc = svc.setRequestMethod('GET');
        svc = ServiceHelper.addServiceHeaders(svc);
        var serviceConfig = svc.getConfiguration();
        var serviceCredentials = serviceConfig.getCredential();
        var url = serviceCredentials.URL + '?dest_zip_code=' + encodeURIComponent(args.zipCode) + '&banner=' + eddConstants.EDD_BANNER;
        svc = svc.setURL(url);
        return JSON.stringify({ dest_zip_code: args.zipCode });
      },

      parseResponse: function (svc, client) {
        return client.text;
      },

      mockCall: function (svc, client) {
        var mockedReponse =
          '{ "FEDEX_GROUND": "2018-09-06", "FEDEX_2_DAY_FREIGHT": "2018-09-07", "FEDEX_3_DAY_FREIGHT": "2018-09-10", "SATURDAY_DELIVERY": "2018-09-08"}';
        return {
          statusCode: 200,
          statusMessage: 'Success',
          text: mockedReponse
        };
      },

      filterLogMessage: function (res) {
        return res.replace('headers', 'OFFWITHTHEHEADERS');
      }
    });
  }
};
