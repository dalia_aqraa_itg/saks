'use strict';

/*
 * Curalate module. Implements Curalate specific logic.
 */

/* API Includes */
var Site = require('dw/system/Site');

/*
 * Returns current locale in appropriate format
 */
function prepareLocale(currentLocale) {
  var locale = currentLocale === 'default' ? 'en_US' : currentLocale;

  return locale.replace(/_/, '-');
}

/* Module exports */
exports.prepareLocale = prepareLocale;
