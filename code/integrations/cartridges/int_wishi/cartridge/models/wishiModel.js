'use strict';

var Logger = require('dw/system/Logger');
var wishiLogger = Logger.getLogger('Wishi', 'Wishi');
var wishiConstants = require('*/cartridge/scripts/util/wishiConstants');
var Site = require('dw/system/Site');
var customPreferences = Site.current.preferences.custom;

function callAPIService(customerNo) {
    var wishiService = new (require('int_wishi/cartridge/scripts/services/wishiServices.js'))();
    try {
        var sessionStatus = wishiService.sessionStatus({customerNo: customerNo});
        if (sessionStatus) {
            return sessionStatus.object;
        }
    } catch (error) {
        wishiLogger.error('Error trying to retrieve wishi status for customer {0} error {1}', customerNo, error.message);
        return null;
    }
}

/**
 * Return the session status based on customer number.
 * 
 * @param {CustomerNo} customerNo 
 */
function getUserStatus(customerNo) {
    var json = callAPIService(customerNo);
    var status = {'status': wishiConstants.WISHI_UnknownUser, 'unreadMessageCount': 0};
    try {
        if (json == null) {
            status.status = wishiConstants.WISHI_UnknownUser;
        } else if (json.session_status == "quiz") {
            var wishiFirstQuizStep = 'wishiFirstQuizStep' in customPreferences ? customPreferences.wishiFirstQuizStep : null;
            if (json.quiz_step == wishiFirstQuizStep) {
                status.status = wishiConstants.WISHI_QuizNotStarted;
            } else {
                status.status = wishiConstants.WISHI_QuizInProgress;
            }
        } else if (json.session_status == "match") {
            status.status = wishiConstants.WISHI_MatchedWithSytlist;
        } else if (json.session_status == "styling") {
            status.status = wishiConstants.WISHI_ChatMode;
            status.unreadMessageCount = json.unread_messages_count;
        }
       
    } catch (error) {
        var err = error;
        wishiLogger.error('Error trying to retrieve wishi status for customer {0} error {1}', customerNo, error.message);
        return null;
    }
    return status; 
}

module.exports = {
    getUserStatus: getUserStatus
};