'use strict';

/* API Includes */
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var Logger = require('dw/system/Logger');
var Site = require('dw/system/Site');
var customPreferences = Site.current.preferences.custom;

var WishiService = function () {
    /**
     * Get Wishi Session Status
     */
    this.sessionStatus = function(data) {
        var serviceDefinition = LocalServiceRegistry.createService('https.get.wishi.sessionStatus', {
            createRequest: function (service, customerNo) {
              service.setRequestMethod('GET');
              service.addHeader('Content-Type', 'application/json');
              var serviceURL = service.getURL().replace(":partner_key", "wishiPartnerId" in customPreferences ? customPreferences.wishiPartnerId : null);
              serviceURL = serviceURL.replace(":user_id", customerNo);
              service.setURL(serviceURL);
              return customerNo;
            },
            parseResponse: function (service, httpClient) {
              return JSON.parse(httpClient.text);
            },
            // eslint-disable-next-line no-unused-vars
            mockCall: function (svc, client) {
              if ("wishiMockResponseType" in customPreferences) {
                return {
                  statusCode: 200,
                  statusMessage: 'Success',
                  text: customPreferences.wishiMockResponseType.displayValue
                };
              }
            }
        });

        var result = serviceDefinition.call(data.customerNo);
        return result;
    }
}

module.exports = WishiService;