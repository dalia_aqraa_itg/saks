'use strict';

var Constants = {
    WISHI_UnknownUser: "Unknown",
    WISHI_QuizNotStarted: "Quiz Not Started",
    WISHI_QuizInProgress: "Quiz In Progress",
    WISHI_MatchedWithSytlist: "Quiz Match Stylist",
    WISHI_ChatMode: "Chat Mode"
};

module.exports = Constants;
