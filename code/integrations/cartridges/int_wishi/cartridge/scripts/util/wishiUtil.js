'use strict';
var Site = require('dw/system/Site');
var customPreferences = Site.current.preferences.custom;

/**
 * Function to check if Custom preference for wishi is turned on / off.
 */
function isWishiEnabled() {
    var isWishiEnabled = 'isWishiEnabled' in customPreferences ? customPreferences.isWishiEnabled : null;
    return isWishiEnabled
}

module.exports.isWishiEnabled = isWishiEnabled;