'use strict';

var server = require('server');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var wishiModel = require('*/cartridge/models/wishiModel');
var wishiConstants = require('*/cartridge/scripts/util/wishiConstants');
var Site = require('dw/system/Site');
var customPreferences = Site.current.preferences.custom;
var Logger = require('dw/system/Logger');
var wishiLogger = Logger.getLogger('Wishi', 'Wishi');

server.get('MyStylist', server.middleware.https, csrfProtection.generateToken, userLoggedIn.validateLoggedIn, function (req, res, next) {
    var customerNo = req.currentCustomer.profile.customerNo;
    
    // If this preference is set, use it for test purposes instead of getting from logged user. 
    var wishiTestClientId = 'wishiTestClientId' in customPreferences ? customPreferences.wishiTestClientId : null;
    if (wishiTestClientId) {
        wishiLogger.debug('WishTest client ID used {0}', wishiTestClientId.value);
        customerNo = wishiTestClientId.value;
    }
    var sessionStatus = wishiModel.getUserStatus(customerNo);
    if (sessionStatus) {
        var Resource = require('dw/web/Resource');
        if (sessionStatus.status == wishiConstants.WISHI_QuizNotStarted) {
            res.render('account/quizNotStarted', {
                wishiMode: sessionStatus.status,
                wishiMessage: Resource.msg('quiz.notstarted', 'wishi', null),
                pageType: "myStylist",
                unreadMessagesCount: sessionStatus.unreadMessageCount
            });
        } else if (sessionStatus.status == wishiConstants.WISHI_QuizInProgress) {
            res.render('account/quizInProgress', {
                wishiMode: sessionStatus.status,
                wishiMessage: Resource.msg('quiz.notstarted', 'wishi', null),
                pageType: "myStylist",
                unreadMessagesCount: sessionStatus.unreadMessageCount
            });
        } else if (sessionStatus.status == wishiConstants.WISHI_MatchedWithSytlist) {
            res.render('account/matchStylist', {
                wishiMode: sessionStatus.status,
                wishiMessage: Resource.msg('quiz.matchstylist', 'wishi', null),
                pageType: "myStylist",
                unreadMessagesCount: sessionStatus.unreadMessageCount
            });
        } else if (sessionStatus.status == wishiConstants.WISHI_ChatMode) {
            res.render('account/quizInChatMode', {
                wishiMode: sessionStatus.status,
                wishiMessage: Resource.msg('quiz.chatstylist', 'wishi', null),
                pageType: "myStylist",
                unreadMessagesCount: sessionStatus.unreadMessageCount
            });
        }
    }
    next();
});

module.exports = server.exports();