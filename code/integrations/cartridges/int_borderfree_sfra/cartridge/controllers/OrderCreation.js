'use strict';
var server = require('server');

/**
 * Require API dependencies
 */
var URLUtils = require('dw/web/URLUtils');
var ServiceRegistry = require('dw/svc/ServiceRegistry');
var Transaction = require('dw/system/Transaction');
var Site = require('dw/system/Site');
var OrderMgr = require('dw/order/OrderMgr');
var Logger = require('dw/system/Logger');
var Resource = require('dw/web/Resource');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var InventoryService = require('*/cartridge/scripts/services/InventoryService');
var collections = require('*/cartridge/scripts/util/collections');
var RootLogger = require('dw/system/Logger').getRootLogger();
var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
var customPreferences = Site.current.preferences.custom;

/**
 * Attempts to create an BFX order from the current basket
 * @param {dw.order.Basket} currentBasket - The current basket
 * @returns {dw.order.Order} The order object created from the current basket
 */
function createBFXOrder(currentBasket, orderNumber) {
  var order;

  try {
    order = Transaction.wrap(function () {
      return OrderMgr.createOrder(currentBasket, orderNumber);
    });
  } catch (error) {
    return null;
  }
  return order;
}

function placeOrder(req, E4XNum) {
  var BasketMgr = require('dw/order/BasketMgr');
  var ShippingMgr = require('dw/order/ShippingMgr');
  var Transaction = require('dw/system/Transaction');
  var OrderMgr = require('dw/order/OrderMgr');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
  var Status = require('dw/system/Status');
  var Order = require('dw/order/Order');
  var PaymentMgr = require('dw/order/PaymentMgr');

  var currentBasket = BasketMgr.getCurrentBasket();

  // Read the E4X Number.
  var E4XNumber = E4XNum;

  var shipment = currentBasket.getDefaultShipment();
  var shippingMethods = ShippingMgr.getAllShippingMethods();
  var shippingMethodsIter = shippingMethods.iterator();
  while (shippingMethodsIter.hasNext()) {
    var method = shippingMethodsIter.next();
    if (method.ID === 'Borderfree') {
      Transaction.wrap(function () {
        shipment.setShippingMethod(method);
        ShippingMgr.applyShippingCost(currentBasket);
      });
    }
  }
  var shippingAddress = shipment.getShippingAddress();
  Transaction.wrap(function () {
    if (shippingAddress == null) {
      shippingAddress = shipment.createShippingAddress();
    }
    shippingAddress.setFirstName(dw.system.Site.current.getCustomPreferenceValue('bfxShippingFirstName'));
    shippingAddress.setLastName(dw.system.Site.current.getCustomPreferenceValue('bfxShippingLastName'));
    shippingAddress.setAddress1(dw.system.Site.current.getCustomPreferenceValue('bfxShippingAddress1'));
    if (E4XNumber && E4XNumber.length > 0) {
      shippingAddress.setAddress2(E4XNumber);
    } else {
      shippingAddress.setAddress2(dw.system.Site.current.getCustomPreferenceValue('bfxShippingAddress2'));
    }
    shippingAddress.setCity(dw.system.Site.current.getCustomPreferenceValue('bfxShippingCity'));
    shippingAddress.setPostalCode(dw.system.Site.current.getCustomPreferenceValue('bfxShippingPostalCode'));
    shippingAddress.setStateCode(dw.system.Site.current.getCustomPreferenceValue('bfxShippingStateCode'));
    shippingAddress.setCountryCode(dw.system.Site.current.getCustomPreferenceValue('bfxShippingCountryCode'));
    shippingAddress.setPhone(dw.system.Site.current.getCustomPreferenceValue('bfxShippingPhone'));
  });

  var billingAddress = currentBasket.getBillingAddress();
  Transaction.wrap(function () {
    if (billingAddress == null) {
      billingAddress = currentBasket.createBillingAddress();
    }
    billingAddress.setFirstName(dw.system.Site.current.getCustomPreferenceValue('bfxBillingFirstName'));
    billingAddress.setLastName(dw.system.Site.current.getCustomPreferenceValue('bfxBillingLastName'));
    billingAddress.setAddress1(dw.system.Site.current.getCustomPreferenceValue('bfxBillingAddress1'));
    billingAddress.setAddress2(dw.system.Site.current.getCustomPreferenceValue('bfxBillingAddress2'));
    billingAddress.setCity(dw.system.Site.current.getCustomPreferenceValue('bfxBillingCity'));
    billingAddress.setPostalCode(dw.system.Site.current.getCustomPreferenceValue('bfxBillingPostalCode'));
    billingAddress.setStateCode(dw.system.Site.current.getCustomPreferenceValue('bfxBillingStateCode'));
    billingAddress.setCountryCode(dw.system.Site.current.getCustomPreferenceValue('bfxBillingCountryCode'));
    billingAddress.setPhone(dw.system.Site.current.getCustomPreferenceValue('bfxBillingPhone'));
  });

  // Set Customer Email Address // Set For registered.
  /*if (req.currentCustomer.profile && req.currentCustomer.profile.email) {
        Transaction.wrap(function () {
            currentBasket.setCustomerEmail(req.currentCustomer.profile.email);
        });
    }*/

  // Email Address should be E4X number
  var emailSuffix = dw.system.Site.current.getCustomPreferenceValue('bfxE4XEmail');
  if (E4XNumber && E4XNumber.length > 0) {
    Transaction.wrap(function () {
      currentBasket.setCustomerEmail(E4XNumber.toString().toLocaleLowerCase() + emailSuffix);
    });
  } else {
    if (req.currentCustomer.profile && req.currentCustomer.profile.email) {
      Transaction.wrap(function () {
        currentBasket.setCustomerEmail(req.currentCustomer.profile.email);
      });
    }
  }

  // Update Tax for all Line Item. It will be ZERO for BFX Orders
  var lineItems = currentBasket.getAllLineItems();
  // Calculate the basket
  Transaction.wrap(function () {
    collections.forEach(lineItems, function (lineItem) {
      lineItem.updateTax(0);
    });
    currentBasket.updateTotals();
  });

  // SFSX-3637 Remove Previos Saved Payment Instruments that saved on the currentBasket before Proceed to checkout wiht Borderfree
  var paymentInstruments = currentBasket.getPaymentInstruments();
  Transaction.wrap(function () {
    collections.forEach(paymentInstruments, function (item) {
      currentBasket.removePaymentInstrument(item);
    });
  });

  // Add Dummy Payment Instrument
  try {
    Transaction.wrap(function () {
      paymentInstrument = currentBasket.createPaymentInstrument('BFX_PAYMENT', currentBasket.getTotalGrossPrice());
      paymentInstrument.paymentTransaction.paymentProcessor = PaymentMgr.getPaymentMethod('BFX_PAYMENT').getPaymentProcessor();
    });
  } catch (e) {
    Logger.error('Error while saving dummy Payment Instrumnet info for SFCC orrder ' + e.message);
  }

  // SFSX-3637 In case dummy payment added to currentBasket, create chargeSequence for it
  Transaction.wrap(function () {
    paymentInstruments = currentBasket.getPaymentInstruments();
    var counter = 1;
    collections.forEach(paymentInstruments, function (paymentInst) {
      paymentInst.custom.chargeSequence = counter.toFixed();
      counter++;
    });
  });

  // Calculate FDD, Associate and discount values for the product line items
  COHelpers.prorateLineItemDiscounts(currentBasket, req.currentCustomer.raw);

  // Creates a new order.
  var orderNumber = req.session.privacyCache.get('bfxOrderNumber');
  var order = createBFXOrder(currentBasket, orderNumber);

  if (!order) {
    Logger.error('Error while creating BFX Order for orrder Number ' + orderNumber + ' :' + e.message);
    return next();
  }

  // Try to do dummy Auth. We can remove this
  try {
    Transaction.wrap(function () {
      paymentInstrument.paymentTransaction.setTransactionID(order.orderNo);
      paymentInstrument.paymentTransaction.paymentProcessor = PaymentMgr.getPaymentMethod('BFX_PAYMENT').getPaymentProcessor();
    });
  } catch (e) {
    Logger.error('Error while saving dummy Payment Instrumnet TransactionID info for SFCC orrder Number ' + order.orderNo + ' :' + e.message);
  }

  // Place the BFX Order
  try {
    Transaction.begin();
    var placeOrderStatus = OrderMgr.placeOrder(order);
    if (placeOrderStatus === Status.ERROR) {
      Logger.error('Error while placing order for orderId: ' + order.orderNo + 'error: ' + e.message);
    } else {
      order.custom.orderStatus = ['INTL_HOLD'];
      order.setConfirmationStatus(Order.CONFIRMATION_STATUS_CONFIRMED);
      order.setExportStatus(Order.EXPORT_STATUS_READY);
    }
    Transaction.commit();

    if (order) {
      Transaction.wrap(function () {
        order.custom.bfxMerchantOrderRef = session.custom.orderRefVal;
        if (E4XNumber && E4XNumber.length > 0) {
          order.custom.bfxOrderId = E4XNumber;
        }
        order.custom.bfxConfirmStatus = 'Unconfirmed';
      });
    }

    Transaction.wrap(function () {
      //SFDEV-11204 | Generate OMS Create Order XML to be used in ExportOrders batch job
      var OrderAPIUtils = require('*/cartridge/scripts/util/OrderAPIUtil');
      order.custom.omsCreateOrderXML = OrderAPIUtils.buildRequestXML(order);
    });

    if (customPreferences.orderCreateBatchEnabled !== true) {
      if (placeOrderStatus !== Status.ERROR) {
        try {
          var OrderAPIUtils = require('*/cartridge/scripts/util/OrderAPIUtil');

          var responseOrderXML = OrderAPIUtils.createOrderInOMS(order);
          // eslint-disable-next-line no-undef
          var resXML = new XML(responseOrderXML);
          Logger.debug('resXML.child(ResponseMessage) -->' + resXML.child('ResponseMessage'));

          if (resXML.child('ResponseMessage').toString() === 'Success') {
            Transaction.wrap(function () {
              order.exportStatus = Order.EXPORT_STATUS_EXPORTED;
              order.trackOrderChange('Order Export Successful');
              order.trackOrderChange(responseOrderXML);
              order.custom.reversalForCancelOrder = false;
            });
          } else {
            Transaction.wrap(function () {
              order.exportStatus = Order.EXPORT_STATUS_FAILED;
              order.trackOrderChange('Order Exported Failed');
              order.trackOrderChange(responseOrderXML);
              order.custom.reversalForCancelOrder = false;
            });
          }
        } catch (err) {
          RootLogger.fatal('Error while calling the Order Create Service for Order ' + order.orderNo + ' error message ' + err.message);
        }
      }
    }
  } catch (e) {
    Logger.error('Error while placing order for orderId: ' + order.orderNo + 'error' + e);
    // We will fail the order if couldn't get placed successfully.
    Transaction.wrap(function () {
      OrderMgr.failOrder(order);
    });
  }

  // Remove the order Number stored in Session and Remove BFX Custom Object
  Transaction.wrap(function () {
    var BFXOrderContainer = CustomObjectMgr.getCustomObject('BFXOrderContainer', orderNumber);
    if (BFXOrderContainer) {
      CustomObjectMgr.remove(BFXOrderContainer);
    }
  });

  req.session.privacyCache.set('bfxOrderNumber', null);

  //delete session
  session.custom.orderRefVal = '';
}

/**
 * updates the shipping address details to item object
 * @param {Object} productLineItems - productLinteItems modal object
 * @param {dw.util.HashMap} shipmentMap - Shipping Map
 */
function updateShippingAddressToItem(productLineItems) {
  productLineItems.items.forEach(function (item) {
    item.shippingCountryCode = dw.system.Site.current.getCustomPreferenceValue('bfxShippingCountryCode');
    item.shippingStateCode = dw.system.Site.current.getCustomPreferenceValue('bfxShippingStateCode');
    item.shippingPostalCode = dw.system.Site.current.getCustomPreferenceValue('bfxShippingPostalCode').substr(0, 5);
  });
}

server.post('BFXPreCheckout', server.middleware.https, function (req, res, next) {
  var OrderMgr = require('dw/order/OrderMgr');
  var Transaction = require('dw/system/Transaction');
  var BasketMgr = require('dw/order/BasketMgr');
  var URLUtils = require('dw/web/URLUtils');
  var ProductLineItemsModel = require('*/cartridge/models/productLineItems');
  var validationHelpers = require('*/cartridge/scripts/helpers/basketValidationHelpers');
  var InventoryServiceUtils = require('*/cartridge/scripts/services/init/InventoryServiceUtils');
  var ServiceHelper = require('*/cartridge/scripts/services/helpers/ServiceHelper');
  var preferences = require('*/cartridge/config/preferences');
  var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');

  var currentBasket = BasketMgr.getCurrentBasket();

  var validatedProducts = validationHelpers.validateCheckoutProducts(currentBasket);
  // If Basket is empty
  if (!currentBasket || validatedProducts.error || validatedProducts.itemRemoved) {
    res.redirect(URLUtils.url('Cart-Show'));
    return next();
  }

  var bfxCountryCode = req.form.bfxCountryCode;
  req.session.privacyCache.set('bfxCountryCode', bfxCountryCode);
  var bfxOrderNumber = req.session.privacyCache.get('bfxOrderNumber');
  var bfxOrder;
  var BFXOrderContainer;
  // Do we need to create a new order every time customer hand over the basket to BFX.
  if (!bfxOrderNumber) {
    res.json({
      error: true,
      errorMessage: Resource.msg('error.cart.or.checkout.error', 'cart', null)
    });
    return next();
  }

  // Save the Order Number
  Transaction.wrap(function () {
    BFXOrderContainer = CustomObjectMgr.getCustomObject('BFXOrderContainer', bfxOrderNumber);
    if (!BFXOrderContainer) {
      BFXOrderContainer = CustomObjectMgr.createCustomObject('BFXOrderContainer', bfxOrderNumber);
    }
  });

  // If Already have reservation ID saved, which means customer came back again
  if (BFXOrderContainer && 'bfxInventoryReservationID' in BFXOrderContainer.custom && !empty(BFXOrderContainer.custom.bfxInventoryReservationID)) {
    var cancelReservation = InventoryService.cancelReservation(BFXOrderContainer.custom.bfxInventoryReservationID);
  }

  if (COHelpers.hasDropShipItems(currentBasket)) {
    res.json({
      success: true
    });
    return next();
    // skip everything after so customer can first remove drop-ship items from Bag
  }

  // Call the Inventory Lookup Service to check if product is available in OMS
  // var inventoryLookupItem = InventoryService.getAvailability(productLineItems, 'lookup');
  if (!validatedProducts.error) {
    var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
    validatedProducts = hooksHelper('app.validate.validate', 'validateInventory', [currentBasket]);
    if (validatedProducts && !validatedProducts.errorInService && !validatedProducts.hasInventory) {
      if (validatedProducts.omsInventory && validatedProducts.omsInventory.length > 0) {
        var omsInventory = JSON.stringify(validatedProducts.omsInventory);
        session.custom.omsInventory = omsInventory;
        var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
        cartHelper.adjustBasketQuantities(currentBasket, validatedProducts.omsInventory);
      }
      res.redirect(URLUtils.url('Cart-Show'));
      return next();
    }
  }
  if (!currentBasket || validatedProducts.error) {
    res.redirect(URLUtils.url('Cart-Show'));
    return next();
  }

  // Check if the Product is available at Inventory Side.
  var productLineItems = new ProductLineItemsModel(currentBasket.productLineItems, 'basket');

  // We are here which means inventory is available
  updateShippingAddressToItem(productLineItems);

  var reserveInventoryRequest = ServiceHelper.prepareRequest(productLineItems, 'reserve', bfxOrderNumber);
  // call the service.
  try {
    // call the inventory reservation service.
    var objResponse = InventoryServiceUtils.callGetInventoryReservation(reserveInventoryRequest);
    var unAvailabilityMap = null;
    if (objResponse && objResponse.status === 'OK' && objResponse.result) {
      // If All the Line item has inventory available ,then proceed with BF Envoy checkout, else stop the customer.
      var parsedResponse = ServiceHelper.parseReserveInventoryResponse(objResponse.result);
      if (parsedResponse.ResponseCode == '99' || parsedResponse.ResponseCode == '94' || parsedResponse.ResponseCode == '1') {
        res.json({
          error: true,
          errorMessage: Resource.msg('error.technical', 'checkout', null)
        });
        return next();
      } else if (parsedResponse.unAvailabilityMap && parsedResponse.unAvailabilityMap.size() > 0) {
        // It means any of the productlineItem couldn't reserve at inventory
        unAvailabilityMap = parsedResponse.unAvailabilityMap;
        var itemsQuantity = [];
        var productIds = unAvailabilityMap.keySet();
        var collections = require('*/cartridge/scripts/util/collections');
        collections.forEach(productIds, function (productId) {
          var inventoryData = {};
          inventoryData.itemID = productId;
          inventoryData.quantity = unAvailabilityMap.get(productId).availableQty;
          itemsQuantity.push(inventoryData);
        });
        var omsInventory = JSON.stringify(itemsQuantity);
        session.custom.omsInventory = omsInventory;
        var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
        cartHelper.adjustBasketQuantities(currentBasket, itemsQuantity);
        res.redirect(URLUtils.url('Cart-Show'));
        return next();
      } else if (parsedResponse.availabilityMap && parsedResponse.availabilityMap.size() > 0) {
        // Reserve the SFCC Inventory as well.
        var reservationStatus = currentBasket.reserveInventory(preferences.bfxInventoryReservationTime);
        ServiceHelper.updateDetailsToPlis(parsedResponse.availabilityMap, currentBasket);
        if (reservationStatus.code !== 'OK') {
          res.json({
            error: true,
            errorMessage: Resource.msg('error.technical', 'checkout', null)
          });
          return next();
        }
        // Inventory reserved successfully. Save Reservation ID.
        Transaction.wrap(function () {
          BFXOrderContainer.custom.bfxInventoryReservationID = parsedResponse.reservationIDs[0];
          BFXOrderContainer.custom.bfxMerchantNumber = req.form.bfxMerchatNumber ? decodeURIComponent(req.form.bfxMerchatNumber) : '';
        });
        res.json({
          success: true
        });
        return next();
      }
      // Here we are means something was not right!!
      res.json({
        error: true,
        errorMessage: Resource.msg('error.technical', 'checkout', null)
      });
      return next();
    } else if (objResponse && objResponse.serviceUnavailable) {
      res.json({
        success: true
      });
      return next();
    }
  } catch (e) {
    // log the exception
    Logger.error('Error InventoryService.reserveInventory() : ' + e);
    res.json({
      error: true,
      errorMessage: Resource.msg('error.cart.or.checkout.error', 'cart', null)
    });
    return next();
  }

  res.json({
    error: true,
    errorMessage: Resource.msg('error.cart.or.checkout.error', 'cart', null)
  });
  return next();
});

server.get('Success', server.middleware.https, function (req, res, next) {
  // placeOrder(req);
  // Invoked by Border Free we now bypass this route in order to guarantee that the correct E4X number is being populated from the confirmation page.
});

server.get('PostSuccess', server.middleware.https, function (req, res, next) {
  var e4xNum = request.httpParameterMap.E4XNUM.stringValue;
  if (e4xNum !== null && e4xNum !== '') {
    placeOrder(req, e4xNum);
  } else {
    // do nothing order will be picked up by the BFProcessPO job
  }
});

server.get('Pending', server.middleware.https, function (req, res, next) {
  // placeOrder(req);
  // Invoked by Border Free we now bypass this route in order to guarantee that the correct E4X number is being populated from the confirmation page.
});

server.get('Failure', server.middleware.https, function (req, res, next) {
  // IF Checkout failed, delete the E4X cookies.
  // cookiesHelper.deleteCookie('bfx-envoy-order-created');
  // cookiesHelper.deleteCookie('bfx.envoy.orderId');
});

server.post('SetBFXCountryCode', server.middleware.https, function (req, res, next) {
  var bfxCountryCode = req.form.bfxCountryCode;
  req.session.privacyCache.set('bfxCountryCode', bfxCountryCode);
  res.json({
    success: true
  });
  next();
});

server.get('BFXOrderTrackingData', server.middleware.https, function (req, res, next) {
  var CartModel = require('*/cartridge/models/cart');
  var BasketMgr = require('dw/order/BasketMgr');
  var confirmationData = require('*/cartridge/scripts/dataLayer/DataLayer.js').prepareBFConfirmationData(req);
  res.json(confirmationData);
  next();
});

module.exports = server.exports();
