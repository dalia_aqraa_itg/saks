'use strict';
var globalHelper = require('*/cartridge/scripts/helpers/globalHelper');

/**
 * ForterOrder class is the DTO object for request.
 *
 * To include this script use:
 * var ForterOrder = require('~/cartridge/scripts/lib/forter/dto/forterOrder');
 *
 * @param {Object} currentOrder - current order
 * @param {Object} request - current page request
 */

function ForterCustomer(order) {
    if (order.customer.profile != null) {
        var OrderMgr = require('dw/order/OrderMgr');
        this.firstName = globalHelper.replaceSpecialCharsAndEmojis(order.customer.profile.firstName || '');
        this.lastName = globalHelper.replaceSpecialCharsAndEmojis(order.customer.profile.lastName || '');
        this.email = order.customer.profile.email;
        this.accountId = order.customerNo || '';
        this.created = Number((order.customer.profile.getCreationDate().getTime() / 1000).toFixed());

        var query = 'customerNo = {0} AND paymentStatus = {1}';
        var allOrders = OrderMgr.searchOrders(query, 'creationDate desc', order.customer.profile.customerNo, 2);

        this.pastOrdersCount = Number(allOrders.count);
    } else {
        this.firstName = globalHelper.replaceSpecialCharsAndEmojis(order.billingAddress.firstName || '');
        this.lastName = globalHelper.replaceSpecialCharsAndEmojis(order.billingAddress.lastName || '');
        this.email = order.customerEmail;
    }
}

function ForterPhone(phone) {
    this.phone = phone;
}

function ForterCreditCard(auth, cc) {
    var creditCardExpMonth;

    // format the expiration month. from 1 to 01, etc.
    if (cc.creditCardExpirationMonth.toString().length === 1) {
        creditCardExpMonth = '0' + cc.creditCardExpirationMonth.toString();
    } else {
        creditCardExpMonth = cc.creditCardExpirationMonth.toString();
    }

    this.nameOnCard = globalHelper.replaceSpecialCharsAndEmojis(cc.creditCardHolder || '');
    this.cardBrand = cc.creditCardType || '';
    this.bin = !empty(cc.creditCardToken) ? cc.creditCardToken.substring(0, 6) : '';

    this.lastFourDigits = cc.creditCardNumberLastDigits || '';
    this.expirationMonth = creditCardExpMonth || '';
    this.expirationYear = !empty(cc.creditCardExpirationYear) ? cc.creditCardExpirationYear.toString() : '';

    this.verificationResults = {};
    this.verificationResults.avsFullResult = auth && !empty(auth.avs_response_code) ? auth.avs_response_code.toString() : '';     // must be adjusted according to the payment gateway used
    this.verificationResults.cvvResult = auth && !empty(auth.cvv_response_code) ? auth.cvv_response_code.toString() : '';         // must be adjusted according to the payment gateway used
    this.verificationResults.authorizationCode = auth && !empty(auth.authorization_code) ? auth.authorization_code.toString() : '';      // must be adjusted according to the payment gateway used

    this.paymentGatewayData = {};
    this.paymentGatewayData.gatewayName = 'IPA';                  // must be adjusted according to the payment gateway used
    this.paymentGatewayData.gatewayTransactionId = auth && !empty(auth.merchant_reference_number) ? auth.merchant_reference_number.toString() : ''; // must be adjusted according to the payment gateway used
    this.verificationResults.processorResponseCode = auth.response_code;
    this.verificationResults.processorResponseText = auth.response_message;
}

function ForterLoyaltyPoints(auth, loyalty) {
    var amexAmount = loyalty.custom.amexAmountApplied;
    try {
        var conversionRate = JSON.parse(loyalty.custom.paywithPointsResponse).rewards.conversion_rate;
        this.loyaltyPointsCount = Math.round(amexAmount/conversionRate);
    } catch (e) {
        this.loyaltyPointsCount = 0;
    }

    this.loyaltyPointsSource = 'CARD_SCHEME_POINTS';
}

function ForterGiftCard(auth, giftCard) {
    var MessageDigest = require('dw/crypto/MessageDigest');
    var messageDigestSHA_256 = new MessageDigest(MessageDigest.DIGEST_SHA_256);

    this.value = {};
    if(!empty(giftCard.paymentTransaction) && !empty(giftCard.paymentTransaction.amount)) {
        this.value.amountUSD = giftCard.paymentTransaction.amount.value.toString();
        this.value.amountLocalCurrency = giftCard.paymentTransaction.amount.value.toString();
        this.value.currency = giftCard.paymentTransaction.amount.currencyCode;
        this.creditCurrency = giftCard.paymentTransaction.amount.currencyCode;
    }
        this.merchantPaymentId = dw.crypto.Encoding.toHex(messageDigestSHA_256.digestBytes(new dw.util.Bytes(giftCard.custom.giftCardNumber)));
}

function ForterPaypal(order, payPal) {
    this.payerId = payPal.custom.ipa_PayerID || '';
    this.payerEmail = payPal.custom.paypalEmail || order.customerEmail  || '';
    this.paymentMethod = 'PayPal Express';
    this.paymentStatus = payPal.custom && payPal.custom.response_code === '1' ? 'Y' : 'N';
}

function ForterInstallmentService(Klarna, order) {
    this.serviceName = "KLARNA";
    this.paymentId = Klarna.custom.merchant_reference_number || '';
    this.firstName = order.billingAddress.firstName;
    this.lastName = order.billingAddress.lastName;
}

function ForterPayment(order, authResponse, payment, log, isAmexPWP) {
    var billingAddress = order.billingAddress;
    this.billingDetails = {};
    this.billingDetails.personalDetails = {};
    this.billingDetails.personalDetails.firstName = globalHelper.replaceSpecialCharsAndEmojis(billingAddress.firstName || '');
    this.billingDetails.personalDetails.lastName = globalHelper.replaceSpecialCharsAndEmojis(billingAddress.lastName || '');

    this.billingDetails.address          = {};
    this.billingDetails.address.address1 = globalHelper.replaceSpecialCharsAndEmojis(billingAddress.address1 || '');
    this.billingDetails.address.address2 = !empty(billingAddress.address2) ? globalHelper.replaceSpecialCharsAndEmojis(billingAddress.address2 || '') : "";
    this.billingDetails.address.zip      = billingAddress.postalCode;
    this.billingDetails.address.city     = globalHelper.replaceSpecialCharsAndEmojis(billingAddress.city || '');
    this.billingDetails.address.region   = billingAddress.stateCode;
    this.billingDetails.address.country  = billingAddress.countryCode.value.toUpperCase();

    if (billingAddress.phone) {
        this.billingDetails.phone = [];
        this.billingDetails.phone.push(new ForterPhone(billingAddress.phone));
    }

    this.amount = {
        amountLocalCurrency: (typeof isAmexPWP !== 'undefined' && isAmexPWP === true) ? '0' : payment.paymentTransaction.amount.value.toString(),
        currency:  payment.paymentTransaction.amount.currencyCode
    };

    if (payment) {
        if ('isKlarnaPI' in payment.custom && payment.custom.isKlarnaPI) {
            this.installmentService = new ForterInstallmentService(payment, order);
        } else if (payment.paymentMethod === 'CREDIT_CARD' && (typeof isAmexPWP !== 'undefined' && isAmexPWP === true)) {  // adjust to the existing payment instruments
            this.loyaltyPoints = new ForterLoyaltyPoints(authResponse, payment);
        } else if (payment.paymentMethod === 'CREDIT_CARD') {
            this.creditCard = new ForterCreditCard(authResponse, payment);
        } else if (payment.paymentMethod === 'GiftCard') {
            this.giftCard = new ForterGiftCard(authResponse, payment);
        } else if (payment.paymentMethod === 'PayPal') {
            this.paypal = new ForterPaypal(order, payment);
        }
    } else {
        log.error('No payment method information for order: ' + order.originalOrderNo);
    }
}

function ForterBeneficiaryDetailsFromGiftCard(item) {
    this.personalDetails = {};
    this.comments = {};

    this.personalDetails.fullName = globalHelper.replaceSpecialCharsAndEmojis(item.recipientName || '');
    this.personalDetails.email = item.recipientEmail;
    this.comments.messageToBeneficiary = item.message ? item.message : '';
}

function ForterBasicSellerData() {
    this.sellerDetails = {};
    this.sellerDetails.sellerAccountCreationDate = Number((new Date().getTime() / 1000).toFixed());
    this.sellerDetails.sellerPastSalesCount = 0;
    this.sellerDetails.sellerPastSalesSum = { amountUSD: '0.00' };
    this.sellerDetails.availableFundsForWithdrawal = { amountUSD: '0.00' };
}

function ForterCartItem(item, itemType) {
    this.basicItemData = {};
    //this.seller = new ForterBasicSellerData(); // request should NOT have additional properties: seller
    if (itemType === 'product') {
        this.basicItemData.productId = item.productID;     // Optional
        this.basicItemData.name = item.productName || '';        // Required
        this.basicItemData.quantity = item.quantityValue;  // Required

        this.deliveryDetails = {};
        this.deliveryDetails.deliveryType = 'PHYSICAL';
        this.deliveryDetails.deliveryMethod = (item.shipment.getShippingMethod() && item.shipment.getShippingMethod().getDisplayName()) ? item.shipment.getShippingMethod().getDisplayName() : '';
        this.deliveryDetails.deliveryPrice = {};
        this.deliveryDetails.deliveryPrice.amountUSD = item && !empty(item.shipment) ? item.shipment.shippingTotalNetPrice.value.toString() : '';

        var product = item.getProduct();
        if (product.getCategories().isEmpty() && product.getVariationModel()) {
            product = product.getVariationModel().getMaster();
        }

        var categoryDisplayName = '';
        if (product.getCategories()[0]) {
            categoryDisplayName = product.getCategories()[0].getDisplayName();
        }

        this.basicItemData.category = categoryDisplayName;
        this.basicItemData.type = 'TANGIBLE'; // Add if type is available. Change according to the actual item type

        this.basicItemData.price = {};
        this.basicItemData.price.amountUSD = item && !empty(item.adjustedNetPrice) ? item.adjustedNetPrice.value.toString() : '';
    }

    if (itemType === 'gift') {
        this.basicItemData.name = item.lineItemText;        // Required
        this.basicItemData.quantity = 1;                    // Required (set 1 by default for a gift cert?)
        this.basicItemData.type = 'NON_TANGIBLE';   // Add if type is available. Change according to the actual item type

        this.deliveryDetails = {};
        this.deliveryDetails.deliveryType = 'DIGITAL';
        this.deliveryDetails.deliveryMethod = 'email';

        this.basicItemData.price = {};

        this.beneficiaries = [];
        this.beneficiaries.push(new ForterBeneficiaryDetailsFromGiftCard(item));
    }
}

function ForterOrder(currentOrder, request, authorizationStep) {
    var ForterLogger = require('*/cartridge/scripts/lib/forter/forterLogger');
    var log = new ForterLogger('ForterOrder.js');
    var order = currentOrder;
    var paymentInstruments = order.getPaymentInstruments();
    var payment = null;
    var authResponse = null;
    var shipment = null;
    var i;

    // Payments
    this.payment = []; // Required

    for (i = 0; i < paymentInstruments.length; i++) {
        var paymentInstrument = paymentInstruments[i];
        payment = paymentInstrument;

        authResponse = paymentInstrument.custom;
        if (payment.paymentTransaction.amount.value !== 0) {
            this.payment.push(new ForterPayment(order, authResponse, payment, log));

            if (payment.paymentMethod === 'CREDIT_CARD' && payment.creditCardType ==='Amex' && payment.custom.amexAmountApplied) {
              this.payment.push(new ForterPayment(order, authResponse, payment, log, true));
            }
        }
    }

    function ForterConnectionInformation(request) { // eslint-disable-line
      if(order.getCreatedBy() === 'Customer'){
        this.customerIP = request.httpRemoteAddress;                    // Required
        this.userAgent = request.httpUserAgent;
        this.forterTokenCookie = '';

        for (i = 0; i < request.httpCookies.cookieCount; i++) {
            if (request.httpCookies[i].name === 'forterToken') {
                this.forterTokenCookie = request.httpCookies[i].value;   // Required
            }
        }
      } else { // blank out for call center orders (orderType PHONE)
        this.customerIP = '';
        this.userAgent = '';
        this.forterTokenCookie = '';
      }
    }

    // General parameters
    this.orderId = order.originalOrderNo;                                          // Required
    this.orderType = order.getCreatedBy() === 'Customer' ? 'WEB' : 'PHONE';        // Required
    this.timeSentToForter = (new Date()).getTime();                                // Required
    this.checkoutTime = Number((order.creationDate.getTime() / 1000).toFixed());   // Required //must be seconds, not milliseconds
    this.connectionInformation = new ForterConnectionInformation(request);         // Required
    this.authorizationStep = authorizationStep;                                    // Required

    if(request.httpUserAgent && ~request.httpUserAgent.indexOf('FABUILD')) {       // Saks mobile app provider injects FABUILD/#.#.# in their UserAgent string.
      this.additionalIdentifiers = {};
      this.additionalIdentifiers.orderSegment = 'MOBILE';
    }

    // Calculate totals
    this.totalAmount = { // Required
        amountLocalCurrency: order.totalGrossPrice.value.toFixed(2),
        currency: order.adjustedMerchandizeTotalPrice.currencyCode
    };

    // Discounts
    var discountPrice = 0;
    var couponName = '';


    if (!order.getCouponLineItems().isEmpty()) {
        var coupons = order.getCouponLineItems();
        var couponNames = [];

        for (i = 0; i < coupons.length; i++) { // UNIT
            var coup = coupons[i];
            couponNames.push(coup.getCouponCode());

            if (!coup.getPriceAdjustments().isEmpty()) {
                var coupAdjustments = coup.getPriceAdjustments();

                for (var j = 0; j < coupAdjustments.length; j++) { // UNIT
                    var coupAdj = coupAdjustments[j];
                    discountPrice += coupAdj.priceValue;
                }
            }
        }

        couponName = couponNames.join(',');
        discountPrice *= -1;

        if (discountPrice > 0) {
            this.totalDiscount = {};                         // Optional
            this.totalDiscount.couponCodeUsed = couponName.substring(0, 20); // Required
            this.totalDiscount.discountType = 'COUPON';                   // Required
        }
    }

    // Customer's details
    this.accountOwner = new ForterCustomer(order);

    // Cart items (regular product)
    this.cartItems = []; // Required

    for (i = 0; i < order.productLineItems.length; i++) { // UNIT
        var pli = order.productLineItems[i];
        this.cartItems.push(new ForterCartItem(pli, 'product'));
    }

    // Cart items (gift certificate)
    for (i = 0; i < order.giftCertificateLineItems.length; i++) { // UNIT
        var gcli = order.giftCertificateLineItems[i];
        this.cartItems.push(new ForterCartItem(gcli, 'gift'));
    }

    // Delivery and Recipient (shipping information)
    if (order.shipments.length > 0) {
        var shipments = order.shipments;
        // default to the SHIPPING recipient details over the pickup details
        for (var i = 0; i < shipments.length; i++) {
          if (!('shipmentType' in shipments[i].custom && shipments[i].custom.shipmentType === 'instore' && shipments[i].shippingAddress !== null)) {
            shipment = shipments[i]; // ship to home
            break;
          } else {
            shipment = shipments[i]; // pick in store
          }
        }

        this.primaryDeliveryDetails = {};
        this.primaryDeliveryDetails.deliveryMethod = (shipment.getShippingMethod() && shipment.getShippingMethod().getDisplayName()) ? shipment.getShippingMethod().getDisplayName() : ''; // 'BY AIR';

        var deliveryType = 'PHYSICAL'; // default value
        if (order.getProductLineItems().size() > 0 && order.getGiftCertificateLineItems().size() === 0) {
            deliveryType = 'PHYSICAL'; // if real products only
        } else if (order.getProductLineItems().size() === 0 && order.getGiftCertificateLineItems().size() > 0) {
            deliveryType = 'DIGITAL';  // if gift certificates only
            this.primaryDeliveryDetails.deliveryMethod = 'email';
        } else if (order.getProductLineItems().size() > 0 && order.getGiftCertificateLineItems().size() > 0) {
            deliveryType = 'HYBRID';  // if gift certificates and real products
        }
        this.primaryDeliveryDetails.deliveryType = deliveryType;

        if (shipment.productLineItems.size() > 0) {
            this.primaryRecipient = {};                 // Optional
            this.primaryRecipient.personalDetails = {};

            if (!('shipmentType' in shipment.custom && shipment.custom.shipmentType === 'instore' && shipment.shippingAddress !== null)) {
              this.primaryRecipient.personalDetails.firstName = globalHelper.replaceSpecialCharsAndEmojis(shipment.shippingAddress.firstName || ''); // from the shipping address
              this.primaryRecipient.personalDetails.lastName = globalHelper.replaceSpecialCharsAndEmojis(shipment.shippingAddress.lastName || '');  // from the shipping address
            }

            this.primaryRecipient.address = {};
            this.primaryRecipient.address.address1 = globalHelper.replaceSpecialCharsAndEmojis(shipment.shippingAddress.address1 || '');
            this.primaryRecipient.address.address2 = shipment.shippingAddress.address2 ? globalHelper.replaceSpecialCharsAndEmojis(shipment.shippingAddress.address2 || '') : '';
            this.primaryRecipient.address.zip = shipment.shippingAddress.postalCode;
            this.primaryRecipient.address.city = globalHelper.replaceSpecialCharsAndEmojis(shipment.shippingAddress.city || '');
            this.primaryRecipient.address.region = shipment.shippingAddress.stateCode;
            this.primaryRecipient.address.country = shipment.shippingAddress.countryCode.value.toUpperCase();

            this.primaryRecipient.phone = [];
            if ('shipmentType' in shipment.custom && shipment.custom.shipmentType === 'instore' && shipment.shippingAddress !== null) {
                if ('personInfoMarkFor' in order.custom && !empty(order.custom.personInfoMarkFor)) {
                    try {
                        var personInfoMarkFor = JSON.parse(order.custom.personInfoMarkFor);
                        this.primaryRecipient.personalDetails.fullName = globalHelper.replaceSpecialCharsAndEmojis(personInfoMarkFor.fullName || '');
                        if(!empty(personInfoMarkFor.phone)) {
                            this.primaryRecipient.phone.push(new ForterPhone(personInfoMarkFor.phone)); // pickup in store phone
                        }
                    } catch(e) {
                        //do nothing
                    }
                }
            } else if (shipment.shippingAddress.phone && shipment.shippingAddress.phone.length > 0) {
                this.primaryRecipient.phone.push(new ForterPhone(shipment.shippingAddress.phone)); // ship to home phone
            }
        } else if (shipment.giftCertificateLineItems.size() > 0) {
            this.primaryRecipient = {};     // Optional
            this.primaryRecipient.personalDetails = {};

            this.primaryRecipient.personalDetails.fullName = globalHelper.replaceSpecialCharsAndEmojis(shipment.giftCertificateLineItems[0].recipientName || '');  // from the gift form
            this.primaryRecipient.personalDetails.email = shipment.giftCertificateLineItems[0].recipientEmail; // from the gift form
        }

        if (shipment.gift === true) {
            this.primaryRecipient.comments = {};
            this.primaryRecipient.comments.messageToBeneficiary = shipment.giftMessage ? shipment.giftMessage : '';
        }
    }
}

module.exports = ForterOrder;
