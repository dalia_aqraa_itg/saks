'use strict';

var Site = require('dw/system/Site');
var UCIDLogger = require('dw/system/Logger').getLogger('UCID', 'UCIDService');
var RootLogger = require('dw/system/Logger').getRootLogger();
var Transaction = require('dw/system/Transaction');
var UCIDServices = require('*/cartridge/scripts/services/UCIDServices');
var UCIDServiceFactory = require('*/cartridge/scripts/services/UCIDServiceFactory');
var UCIDHelpers = require('*/cartridge/scripts/helpers/UCIDHelpers');
var ucidConstants = require('*/cartridge/scripts/ucidConstants.js');
var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');

function getAssociateDiscount(token) {
  var request = UCIDHelpers.getAdsRequest(token);

  var service = UCIDServiceFactory.getInstance(UCIDServices.UCID_ADS);
  var response = service.call(request);
  if (response && response.error === 0 && response.status === 'OK') {
    var codes = UCIDHelpers.parseAdsResponse(response.object);
    if (!empty(codes) && codes.length > 0) {
      return {
        success: true,
        codes: codes
      };
    } else {
      return {
        success: false
      };
    }
  } else {
    RootLogger.fatal('FATAL: There was an error with the ADS service call: {0}: ', response.errorMessage);
    UCIDLogger.error('ERROR: There was an error with the ADS service call:' + response.errorMessage);
    return {
      success: false
    };
  }
}

function associateDiscountCode(req, token) {
  if (ucidConstants.UCID_ADS_ENABLED) {
    var result = getAssociateDiscount(token);
    if (result.success && result.codes && result.codes.length > 0) {
      delete req.session.raw.custom.associateTier;
      delete req.session.raw.custom.specialVendorDiscountTier;
      result.codes.forEach(function (code) {
        if (code.toLowerCase().indexOf('emp') > -1) {
          req.session.raw.custom.associateTier = code;
        } else if (code.toLowerCase().indexOf('cpn') > -1) {
          req.session.raw.custom.specialVendorDiscountTier = code;
        } else if (code.toLowerCase().indexOf('csr') > -1 && empty(req.session.raw.custom.associateTier)) {
          req.session.raw.custom.associateTier = code;
        }
      });
    } else {
      delete req.session.raw.custom.associateTier;
      delete req.session.raw.custom.specialVendorDiscountTier;
    }
  }
}

function applyFDDDiscount(req, billingData) {
  var today = new Date();
  var cartType = billingData.paymentInformation.cardType.value;
  if (tsysHelper.isTsysMode()) {
    if (cartType && cartType === 'TCC') {
      // If (Pass expiry date is > current date) and
      // (difference between Pass expiry date and current date is >= 12 then apply FDD
      var cartExpirationDate = new Date(billingData.paymentInformation.tccExpirationDate);
      var diff = cartExpirationDate - today;
      var diffDays = Math.floor(diff / (1000 * 60 * 60 * 24));
      var isFDDDate = UCIDHelpers.isFDDDate();
      if (diffDays >= 12 && isFDDDate) {
        req.session.raw.custom.associateTier = ucidConstants.FDD_ASSOCIATE_CODE;
      }
    }
  }
}

function cleanFDD(req) {
  if (req.session.raw.custom.associateTier) {
    if (req.session.raw.custom.associateTier.toLowerCase().indexOf('csr') > -1) {
      delete req.session.raw.custom.associateTier;
    }
  }
}

function getUCIDNumber(profile, customerNumber, address) {
  var request = UCIDHelpers.getCreateRequest(profile, customerNumber, address);

  var service = UCIDServiceFactory.getInstance(UCIDServices.UCID_CREATE);
  var response = service.call(request);
  if (response && response.error === 0 && response.status === 'OK') {
    var UCIDNumber = UCIDHelpers.parseCreateResponse(response.object);
    return {
      success: true,
      UCIDNumber: UCIDNumber
    };
  } else {
    // If Sync call failed, store request for Offline retry.
    UCIDHelpers.saveSyncRequestData(customerNumber, request, 'CREATE');
    RootLogger.fatal('FATAL: There was an error with the UCID Create service call for Customer Number {0}: {1}', customerNumber, response.errorMessage);
    UCIDLogger.error('ERROR: There was an error with the UCID Create service call for Customer Number {0}: {1}', customerNumber, response.errorMessage);
  }
  return {
    success: false
  };
}

function createUCIDCustomer(profile, customerNumber, address) {
  if (ucidConstants.UCID_ENABLED) {
    var result = getUCIDNumber(profile, customerNumber, address);
    if (result.success && result.UCIDNumber) {
      // Save it to the Profile Level UCID Attribute
      Transaction.wrap(function () {
        profile.custom.ucidNo = result.UCIDNumber;
      });
      // If We have Updated the Profile to UCID, delete the pending CO.
      UCIDHelpers.deletePendingSyncRequest(customerNumber);
    }
  }
}

function updateUCIDNumber(profile, customerNumber, address, addressType, isDefaultAddress, isAddAddress) {
  var request = UCIDHelpers.getUpdateRequest(profile, customerNumber, address, addressType, isDefaultAddress, isAddAddress);

  var service = UCIDServiceFactory.getInstance(UCIDServices.UCID_UPDATE);
  var response = service.call(request);
  if (response && response.error === 0 && response.status === 'OK') {
    var UCIDNumber = UCIDHelpers.parseCreateResponse(response.object);
    return {
      success: true,
      UCIDNumber: UCIDNumber
    };
  } else {
    // If Sync call failed, store request for Offline retry.
    UCIDHelpers.saveSyncRequestData(customerNumber, request, 'UPDATE');
    RootLogger.fatal('FATAL: There was an error with the UCID Update service call for Customer Number {0}: {1}', customerNumber, response.errorMessage);
    UCIDLogger.error('ERROR: There was an error with the UCID Update service call for Customer Number {0}: {1}', customerNumber, response.errorMessage);
  }
  return {
    success: false
  };
}

function updateUCIDCustomer(profile, customerNumber, address, addressType, isDefaultAddress, isAddAddress) {
  if (ucidConstants.UCID_ENABLED) {
    var result = updateUCIDNumber(profile, customerNumber, address, addressType, isDefaultAddress, isAddAddress);
    if (result.success && result.UCIDNumber) {
      // Save it to the Profile Level UCID Attribute
      Transaction.wrap(function () {
        profile.custom.ucidNo = result.UCIDNumber;
      });
      // If We have Updated the Profile to UCID, delete the pending CO.
      UCIDHelpers.deletePendingSyncRequest(customerNumber);
    }
  }
}

function offlineUpdateUCIDCustomer(profile, customerNumber, requestData) {
  if (ucidConstants.UCID_ENABLED) {
    var service = UCIDServiceFactory.getInstance(UCIDServices.UCID_UPDATE);
    var response = service.call(JSON.parse(requestData));
    if (response && response.error === 0 && response.status === 'OK') {
      var UCIDNumber = UCIDHelpers.parseCreateResponse(response.object);
      if (UCIDNumber) {
        Transaction.wrap(function () {
          profile.custom.ucidNo = UCIDNumber;
        });
        // If We have Updated the Profile to UCID, delete the pending CO.
        UCIDHelpers.deletePendingSyncRequest(customerNumber);
      }
    } else {
      RootLogger.fatal('FATAL: There was an error with the UCID Update service call for Customer Number {0}: {1}', customerNumber, response.errorMessage);
      UCIDLogger.error('ERROR: There was an error with the UCID Update service call for Customer Number {0}: {1}', customerNumber, response.errorMessage);
    }
  }
}

function fddNotifyService(token) {
  if (ucidConstants.UCID_ADS_ENABLED) {
    var request = UCIDHelpers.getAdsRequest(token);
    var requestSecondBanner = UCIDHelpers.getAdsRequest(token, ucidConstants.SECOND_BANNER_CODE);

    var service = UCIDServiceFactory.getInstance(UCIDServices.UCID_FDD_NOTIFY);
    var response = service.call(request);
    var responseSecondBanner = service.call(requestSecondBanner);
    if (response && response.status === 'OK' && responseSecondBanner && responseSecondBanner.status === 'OK') {
      return {
        success: true
      };
    } else {
      RootLogger.fatal('FATAL: There was an error with the FDD Notify service call:' + response.errorMessage);
      UCIDLogger.error('ERROR: There was an error with the FDD Notify service call:' + response.errorMessage);
      return {
        success: false
      };
    }
  }
}

module.exports = {
  associateDiscountCode: associateDiscountCode,
  createUCIDCustomer: createUCIDCustomer,
  updateUCIDCustomer: updateUCIDCustomer,
  offlineUpdateUCIDCustomer: offlineUpdateUCIDCustomer,
  fddNotifyService: fddNotifyService,
  applyFDDDiscount: applyFDDDiscount,
  cleanFDD: cleanFDD
};
