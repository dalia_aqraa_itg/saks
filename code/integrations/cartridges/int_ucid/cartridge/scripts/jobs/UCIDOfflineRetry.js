var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var CustomerMgr = require('dw/customer/CustomerMgr');
var UCIDFacade = require('*/cartridge/scripts/hooks/UCIDFacade');
var Logger = dw.system.Logger.getLogger('UCID', 'UCIDService');

exports.OfflineProcess = function offlineProcess() {
  try {
    var customObjectItr = CustomObjectMgr.queryCustomObjects('CustomerProfileSyncData', 'custom.recordSynced={0}', null, false);
    while (customObjectItr.hasNext()) {
      var SyncObject = customObjectItr.next();
      // If Request type is CREATE, We will make a UCID call. We will also check if we have any address associated with it. We will send that
      if (SyncObject) {
        if (SyncObject.custom.customerNumber) {
          var profile = CustomerMgr.getProfile(SyncObject.custom.customerNumber);
          if (profile) {
            if (SyncObject.custom.requestType === 'CREATE') {
              // Send the first address as well if we have
              var address = profile.addressBook.addresses.length > 0 ? profile.addressBook.addresses[0] : null;
              UCIDFacade.createUCIDCustomer(profile, profile.customerNo, address);
            } else if (SyncObject.custom.requestType === 'UPDATE') {
              var requestData = SyncObject.custom.requestMessage;
              UCIDFacade.offlineUpdateUCIDCustomer(profile, profile.customerNo, requestData);
            }
          }
        }
      }
    }
  } catch (e) {
    Logger.error('[ASD] : Error while Running the Offline Sync Job', e);
  }
};
