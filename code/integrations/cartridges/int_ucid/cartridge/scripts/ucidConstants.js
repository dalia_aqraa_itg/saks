var Site = require('dw/system/Site');
var CurrentSite = Site.current.preferences;
var preferences = CurrentSite ? CurrentSite.custom : {};

const constants = {
  UCID_ADS_ENABLED: preferences['UCIDAdsEnabled'] || false,
  API_KEY: preferences['hbcAPIKey'],
  API_KEY_GW: preferences['hbcGWAPIKey'],
  BANNER_CODE: preferences['UCIDBannerCode'],
  SECOND_BANNER_CODE: 'UCIDSecondBannerCode' in preferences && preferences['UCIDSecondBannerCode'],
  TOKEN_TYPE: preferences['UCIDTokneType'],
  UCID_ENABLED: preferences['UCIDEnabled'] || false,
  UCID_EXTERNAL_KEY: preferences['UCIDExternalKey'] || 'SFCC',
  UCID_SOURCE_ID: preferences['UCIDSourceID'] || 'SFCC',
  UCID_SYNC_BANNER_CODE: preferences['UCIDSyncBannerCode'] || 100,
  UCID_LANGUAGE_PREF_CODE: preferences['UCIDLanguagePrefCode'] || '1',
  UCID_EXTERNAL_GUID_KEY: 'SFCC_GUID',
  FDD_ASSOCIATE_CODE: 'CSRFDD'
};

module.exports = constants;
