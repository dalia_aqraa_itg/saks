'use strict';
var Logger = require('dw/system/Logger');
var ucidConstants = require('*/cartridge/scripts/ucidConstants.js');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var Calendar = require('dw/util/Calendar');

var UCIDLogger = Logger.getLogger('UCID', 'UCIDService');

/**
 * Create Request Object for Ads Service
 * @param {string} token
 * @param {string} bannerCode
 * @returns {Object} request
 */
function getAdsRequest(token, bannerCode) {
  var request = Object.create(null);
  request.bannerCode = bannerCode || ucidConstants.BANNER_CODE;
  request.tokenType = ucidConstants.TOKEN_TYPE;
  request.tokenValue = token;
  if (token.length === 29) {
    request.tokenValue = token.substr(0, 16);
  }
  return request;
}

/**
 * Parse the ADS response
 * @param {string} response
 * @returns {Object} triggerCodes
 */
function parseAdsResponse(response) {
  var triggerCodes = [];
  try {
    var parsedResponse = JSON.parse(response);
    if (parsedResponse.customerDiscountRecord && parsedResponse.customerDiscountRecord.length > 0) {
      //triggerCode = parsedResponse.customerDiscountRecord[0].triggerCode;
      parsedResponse.customerDiscountRecord.forEach(function (code) {
        if (code.triggerCode) {
          triggerCodes.push(code.triggerCode);
        }
      });
    }
  } catch (e) {
    UCIDLogger.error('[ASD] : Error while parsing the Trigger Code {0} \n {1}', e.message, e.stack);
  }
  return triggerCodes;
}

function prepareUCIDRequest(profile, customerNumber, address, action, addressType, isDefaultAddress, isAddAddress) {
  var request = Object.create(null);
  if (profile.firstName) {
    request.firstName = profile.firstName;
  }
  if (profile.lastName) {
    request.lastName = profile.lastName;
  }
  request.languagePrefCode = ucidConstants.UCID_LANGUAGE_PREF_CODE;

  if (address && action !== 'CREATE') {
    var contactDetails = {};
    contactDetails.firstName = address.firstName;
    contactDetails.lastName = address.lastName;

    if ('ID' in address && !empty(address.ID)) {
      contactDetails.addressNickname = address.ID;
    } else {
      contactDetails.addressNickname = address.addressID;
    }

    contactDetails.address1 = address.address1;
    if (address.address2) {
      contactDetails.address2 = address.address2;
    }
    contactDetails.city = address.city;
    contactDetails.stateCode = address.stateCode;
    contactDetails.postCode = address.postalCode;
    contactDetails.countryCode = address.countryCode.value;
    contactDetails.addressType = addressType ? addressType : 'U';
    if (isDefaultAddress) {
      contactDetails.isAddressDefault = 'Y';
    } else {
      contactDetails.isAddressDefault = 'N';
    }

    if (address.phone) {
      var phoneDetails = {};
      phoneDetails.phoneNumber = address.phone;
      phoneDetails.countryCode = 1;
      phoneDetails.phoneNumberType = 'U';
      contactDetails.address_phoneDetails = phoneDetails;
    }
    request.contactDetails = contactDetails;
  }

  var accountDetails = {};
  var customerID = '';

  if (profile.email) {
    accountDetails.emailId = profile.email;
  }
  if (profile.customer) {
    customerID = profile.customer.ID;
  }
  accountDetails.externalId = [];
  var SFCCCustomerNumDetails = {};
  SFCCCustomerNumDetails.key = ucidConstants.UCID_EXTERNAL_KEY;
  SFCCCustomerNumDetails.value = customerNumber;
  accountDetails.externalId.push(SFCCCustomerNumDetails);

  var SFCCCustomerIDDetails = {};
  SFCCCustomerIDDetails.key = ucidConstants.UCID_EXTERNAL_GUID_KEY;
  SFCCCustomerIDDetails.value = customerID;
  accountDetails.externalId.push(SFCCCustomerIDDetails);

  // If Customer has Reward number in his profile, send it as well as an external ID

  if ('hudsonReward' in profile.custom && !empty(profile.custom.hudsonReward)) {
    var RewardExternalDetails = {};
    RewardExternalDetails.key = 'LOYMEM';
    RewardExternalDetails.value = profile.custom.hudsonReward;
    accountDetails.externalId.push(RewardExternalDetails);
  } else if ('moreCustomer' in profile.custom && !empty(profile.custom.moreCustomer)) {
    var RewardExternalDetails = {};
    RewardExternalDetails.key = 'LOYMEM';
    RewardExternalDetails.value = profile.custom.moreCustomer;
    accountDetails.externalId.push(RewardExternalDetails);
  }

  if (action === 'CREATE') {
    if ('zipCode' in profile.custom && profile.custom.zipCode) {
      var contactDetails = {};
      contactDetails.postCode = profile.custom.zipCode;
      accountDetails.contactDetails = contactDetails;
    }

    if (!empty(profile.phoneHome)) {
      request.phoneActiveIND = 'A';
      var phoneDetails = {};
      phoneDetails.phoneNumber = profile.phoneHome;
      phoneDetails.countryCode = 1;
      phoneDetails.phoneNumberType = 'U';
      accountDetails.phoneDetails = phoneDetails;
    } else {
      request.phoneActiveIND = 'U';
    }
  }

  request.accountDetails = accountDetails;
  if ('ucidNo' in profile.custom && profile.custom.ucidNo) {
    request.UCIDNumber = profile.custom.ucidNo;
  }
  request.sourceId = ucidConstants.UCID_SOURCE_ID;
  request.bannerCode = ucidConstants.UCID_SYNC_BANNER_CODE;

  if (profile.custom.saksOptIn || profile.custom.emailOptIn) {
    request.emailActiveIND = 'A';
  } else {
    request.emailActiveIND = 'I';
  }

  if (profile.custom.isCanadianCustomer) {
    request.canadianCustomer = 'Y';
  } else {
    request.canadianCustomer = 'N';
  }

  if (address) {
    if (isAddAddress) {
      request.addressActiveIND = 'A';
    } else {
      request.addressActiveIND = 'B';
    }
  }

  return request;
}

function getCreateRequest(profile, customerNumber, address) {
  return prepareUCIDRequest(profile, customerNumber, address, 'CREATE', null, null, null);
}

function getUpdateRequest(profile, customerNumber, address, addressType, isDefaultAddress, isAddAddress) {
  return prepareUCIDRequest(profile, customerNumber, address, 'UPDATE', addressType, isDefaultAddress, isAddAddress);
}

function parseCreateResponse(response) {
  var ucidNumber;
  try {
    var parsedResponse = JSON.parse(response);
    if (parsedResponse && !empty(parsedResponse.customerRecord) && !empty(parsedResponse.customerRecord.UCIDNumber)) {
      ucidNumber = parsedResponse.customerRecord.UCIDNumber;
    }
  } catch (e) {
    UCIDLogger.error('[ASD] : Error while parsing the UCID Create Response {0} \n {1}', e.message, e.stack);
  }
  return ucidNumber;
}

function saveSyncRequestData(customerNumber, requestMessage, requestType) {
  try {
    Transaction.wrap(function () {
      // If Request Type is CREATE, save the Data directly.
      if (requestType === 'CREATE') {
        var customerProfileSyncDataObj = CustomObjectMgr.createCustomObject('CustomerProfileSyncData', customerNumber);
        customerProfileSyncDataObj.custom.requestType = requestType;
        customerProfileSyncDataObj.custom.requestMessage = JSON.stringify(requestMessage);
        customerProfileSyncDataObj.custom.recordSynced = false;
      } else if (requestType === 'UPDATE') {
        // Query Customer Object and save UPDATE request only if we have UPDATE type saved in CO.
        // If CREATE service also failed, DON't Update it.
        var customerProfileSyncDataObj = CustomObjectMgr.getCustomObject('CustomerProfileSyncData', customerNumber);
        if (customerProfileSyncDataObj && customerProfileSyncDataObj.custom.requestType === 'UPDATE') {
          customerProfileSyncDataObj.custom.requestMessage = JSON.stringify(requestMessage);
        } else if (!customerProfileSyncDataObj) {
          var customerProfileSyncDataObj = CustomObjectMgr.createCustomObject('CustomerProfileSyncData', customerNumber);
          customerProfileSyncDataObj.custom.requestType = requestType;
          customerProfileSyncDataObj.custom.requestMessage = JSON.stringify(requestMessage);
          customerProfileSyncDataObj.custom.recordSynced = false;
        }
      }
    });
  } catch (e) {
    UCIDLogger.error('[ASD] : When Saving Sync Request in the Custom Object {0} \n {1}', e.message, e.stack);
  }
}

function deletePendingSyncRequest(customerNumber) {
  Transaction.wrap(function () {
    var customerProfileSyncDataObj = CustomObjectMgr.getCustomObject('CustomerProfileSyncData', customerNumber);
    if (customerProfileSyncDataObj) {
      CustomObjectMgr.remove(customerProfileSyncDataObj);
    }
  });
}

var addServiceHeaders = function (service) {
  service.addHeader('x-api-key', ucidConstants.API_KEY);
  service.addHeader('x-apigw-api-id', ucidConstants.API_KEY_GW);
  service.addHeader('Content-Type', 'application/json');
  // additional headers can be added here
  return service;
};

var isBordersAllow = function () {
  var profile = customer.profile;
  if (profile && profile.custom.saksFirstApplyDate) {
    var currentDate = new Date();
    var saksFirstApplyDate = profile.custom.saksFirstApplyDate;
    var diff = currentDate - saksFirstApplyDate; // diff in milliseconds
    var diffDays = diff / (1000 * 60 * 60 * 24);

    var currentDateCalendar = new Calendar(currentDate);
    var applyDatePlusOne = new Calendar(saksFirstApplyDate);
    applyDatePlusOne.add(Calendar.DATE, 1);
    var isSameDay = currentDateCalendar.isSameDay(applyDatePlusOne);

    return diffDays <= 1 || isSameDay;
  }

  return false;
};

var isFDDDate = function () {
  var profile = customer.profile;
  if (profile) {
    if (profile.custom.fddUsedDate) {
      var currentDate = new Date();
      var fddUsedDate = profile.custom.fddUsedDate;
      return currentDate.getDate() === fddUsedDate.getDate();
    } else {
      // if fdd date empty customer can use it
      return true;
    }
  }
  return true;
};

module.exports = {
  getAdsRequest: getAdsRequest,
  parseAdsResponse: parseAdsResponse,
  getCreateRequest: getCreateRequest,
  getUpdateRequest: getUpdateRequest,
  parseCreateResponse: parseCreateResponse,
  saveSyncRequestData: saveSyncRequestData,
  deletePendingSyncRequest: deletePendingSyncRequest,
  addServiceHeaders: addServiceHeaders,
  isBordersAllow: isBordersAllow,
  isFDDDate: isFDDDate
};
