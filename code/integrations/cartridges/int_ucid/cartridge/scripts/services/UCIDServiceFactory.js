'use strict';

const UCIDServices = require('*/cartridge/scripts/services/UCIDServices');

/**
 * UCID Services
 */
const UCID_ADSInit = require('*/cartridge/scripts/services/init/UCID_ADSInit');
const UCID_CreateInit = require('*/cartridge/scripts/services/init/UCID_Create');
const UCID_UpdateInit = require('*/cartridge/scripts/services/init/UCID_Update');
const UCID_FDDNotify = require('*/cartridge/scripts/services/init/UCID_FDDNotify');

/**
 * Using the serviceName return an instance of the Service.
 *
 * @param serviceName
 * @returns Instance of Service
 */
function _getServiceBuilder(serviceName) {
  switch (serviceName) {
    case UCIDServices.UCID_ADS:
      return UCID_ADSInit;
    case UCIDServices.UCID_CREATE:
      return UCID_CreateInit;
    case UCIDServices.UCID_UPDATE:
      return UCID_UpdateInit;
    case UCIDServices.UCID_FDD_NOTIFY:
      return UCID_FDDNotify;
    default:
      throw new Error('UCID Service ID not recognized: ' + serviceName);
  }
}

exports.getInstance = function (serviceName) {
  const builder = _getServiceBuilder(serviceName);
  return builder.getInstance();
};
