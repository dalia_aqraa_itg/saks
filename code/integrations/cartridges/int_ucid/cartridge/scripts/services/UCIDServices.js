'use strict';

/**
 * Unique UCID service identifiers
 */
const UCIDServices = {
  UCID_ADS: 'ucid.http.ads.',
  UCID_FDD_NOTIFY: 'ucid.http.fdd.notify.',
  UCID_CREATE: 'ucid.http.create.',
  UCID_UPDATE: 'ucid.http.update.'
};

module.exports = UCIDServices;
