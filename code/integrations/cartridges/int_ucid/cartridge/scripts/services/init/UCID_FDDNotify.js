var Site = require('dw/system/Site');
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var UCIDServices = require('*/cartridge/scripts/services/UCIDServices');
var ucidConstants = require('*/cartridge/scripts/ucidConstants.js');
var UCIDHelpers = require('*/cartridge/scripts/helpers/UCIDHelpers');

module.exports = {
  getInstance: function () {
    return LocalServiceRegistry.createService(UCIDServices.UCID_FDD_NOTIFY + Site.current.ID, {
      createRequest: function (svc, args) {
        svc = svc.setRequestMethod('PUT');
        svc = UCIDHelpers.addServiceHeaders(svc);
        return JSON.stringify(args);
      },

      parseResponse: function (svc, client) {
        return client.text;
      },

      mockCall: function (svc, client) {
        var mockedReponse = '{"responseMsg":"RECORD UPDATED"}';
        return {
          statusCode: 200,
          statusMessage: 'Success',
          text: mockedReponse
        };
      }
    });
  }
};
