# Salesforce Commerce Cloud BloomReach Cartridge

BloomReach provides a LINK cartridge to integrate with Salesforce Commerce Cloud (SFCC). This cartridge supports SFRA version 4.4.x and SiteGenesis JS-Controllers version 104.0.0. Also, this cartridge was built and tested against SFCC B2C Commerce API version 19.10

# The latest version

The latest version of this cartridge is 19.1.0

# Integration Component Overview

Available Bloomreach Components

- BloomReach Core Integration
  - Javascript Tracking Pixel
  - Product feed
- BloomReach Organic
  - BloomReach Widgets
  - BloomReach Thematic Pages

# Requirements

- You must have a Bloomreach Client/AccountID and an Account Key/AuthID to complete this integration

# Cartridges Overview

_Description of available cartridges in_ **link_bloomreach/cartridges**

| Cartridge Name             | Description                                                                                          |
| -------------------------- | ---------------------------------------------------------------------------------------------------- |
| app_storefront_controllers | SiteGenesis storefront cartridge, provides example usage in controllers to run BloomReach components |
| app_storefront_core        | SiteGenesis storefront cartridge, provides example code for a SiteGenesis integration                |
| app_storefront_bloomreach  | SFRA overlay cartridge, provides example usage for SFRA integration                                  |
| int_bloomreach             | SiteGenesis integration cartridge                                                                    |
| int_bloomreach_sfra        | SFRA integration cartridge                                                                           |

- Note: Only 'int_bloomreach' or 'int_bloomreach_sfra' cartridges are meant to be installed into instances, the other cartridges are merely to provide examples and use cases for BloomReach components

# Installation, Usage and Configuration

Installation, Usage and Configuration is explained in detail in the **documentation/Bloom_Reach_LINK_Integration.docx** word document.

### SFRA Dependancy Installation

The int_bloomreach_sfra cartridge requires [Node.js](https://nodejs.org/) v10.6.3+ (a.k.a Dubnium) to run.

Install the dependencies and devDependencies.

```sh
$ cd link_bloomreach
$ npm install

```

## License

MIT
