'use strict';
/**
 * Gets BloomReach Widget HTMl data to render within templates
 *
 * @module controllers/BloomReachWidget
 */

var server = require('server');
var Site = require('dw/system/Site');
var cache = require('*/cartridge/scripts/middleware/cache');

/**
 * Parses Fetch to return the HTML for the specific widget requested.
 */
server.get('GetWidget', cache.applyDefaultCache, function (req, res, next) {
  // Check Widget is enabled
  if (Site.current.preferences.custom.BR_Enable_Widget === true) {
    var bloomReachWidget = require('*/cartridge/scripts/getBloomReachWidget.js');

    var brwidget = {};
    brwidget.BRWidgetType = !empty(request.httpParameterMap.type) ? request.httpParameterMap.type.stringValue : '';
    brwidget.BRWidgetAccountID = !empty(request.httpParameterMap.accountID) ? request.httpParameterMap.accountid.stringValue : '';
    brwidget.BRWidgetAccountKey = !empty(request.httpParameterMap.accountKey) ? request.httpParameterMap.accountkey.stringValue : '';
    brwidget.BRWidgetURL = !empty(request.httpParameterMap.url) ? request.httpParameterMap.url.stringValue : '';
    brwidget.BRWidgetUserAgent = request.httpUserAgent;
    brwidget.BRWidgetReferrer = request.httpReferer;
    brwidget.BRWidgetPageType = !empty(request.httpParameterMap.pageType) ? request.httpParameterMap.pagetype.stringValue : '';
    brwidget.BRWidgetProductID = !empty(request.httpParameterMap.productID) ? request.httpParameterMap.productid.stringValue : '';
    brwidget.BRWidgetProductName = !empty(request.httpParameterMap.productName) ? request.httpParameterMap.productname.stringValue : '';
    brwidget.BRWidgetPageStatus = !empty(request.httpParameterMap.pageStatus) ? request.httpParameterMap.pagestatus.stringValue : '';

    var widget = bloomReachWidget.getBloomReachWidget(
      brwidget.BRWidgetType,
      brwidget.BRWidgetURL,
      brwidget.BRWidgetUserAgent,
      brwidget.BRWidgetReferrer,
      brwidget.BRWidgetPageType,
      brwidget.BRWidgetProductID,
      brwidget.BRWidgetProductName,
      brwidget.BRWidgetPageStatus
    );
    var hasData = widget != null && !empty(widget);
    if (hasData) brwidget = widget;

    res.render('getWidget', {
      brwidget: brwidget
    });
    next();
  } else {
    res.render('BRError_blank', {
      Message: 'Widget Site Pref Disabled'
    });
  }
});

/**
 * Fetches full content from BloomReach server and caches it for one day.
 */
server.get('Fetch', cache.applyDefaultCache, function (req, res, next) {
  if (request.httpParameterMap.isParameterSubmitted('usestatic')) {
    res.render('staticFetch', {});
    next();
  } else {
    var bloomReachWidget = require('*/cartridge/scripts/getBloomReachWidget.js');

    var widget = bloomReachWidget.getBloomReachFullWidgetHTML(
      !empty(request.httpParameterMap.url) ? request.httpParameterMap.url.stringValue : '',
      request.httpUserAgent,
      request.httpReferer,
      !empty(request.httpParameterMap.pageType) ? request.httpParameterMap.pagetype.stringValue : '',
      !empty(request.httpParameterMap.productID) ? request.httpParameterMap.productid.stringValue : '',
      !empty(request.httpParameterMap.productName) ? request.httpParameterMap.productname.stringValue : '',
      !empty(request.httpParameterMap.pageStatus) ? request.httpParameterMap.pagestatus.stringValue : ''
    );

    res.render('getFullWidgetHTML', {
      widget: widget
    });
    next();
  }
});

module.exports = server.exports();
