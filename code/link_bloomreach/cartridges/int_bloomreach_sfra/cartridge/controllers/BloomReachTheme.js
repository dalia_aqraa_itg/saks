'use strict';
/**
 *
 * Controller that renders a sample Thematic template used for testing purposes.
 *
 * @module controllers/BloomReachTheme
 */

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');

/**
 * Fetches raw Thematic data from BloomReach and caches it on SFCC instance for one day.
 */
server.get('Fetch', cache.applyDefaultCache, function (req, res, next) {
  var getBloomReachTheme = require('~/cartridge/scripts/getBloomReachTheme');
  var currentRequest = request;
  var brThemeName = request.httpParameterMap.theme.value;
  var referUrl = request.httpParameterMap.refer.value;

  var args = getBloomReachTheme.execute(currentRequest, brThemeName, referUrl);

  if (args.error) {
    res.json(args.errorMsg);
    next();
  }

  res.render('staticTheme', {
    args: args
  });
  next();
});

/**
 * Parses out the header portion of the Thematic data cached by Fetch pipeline, and renders it as HTML.
 */
server.get('Header', function (req, res, next) {
  var args = {};
  args.BRThemePart = 'header';
  args.BRThemeName = request.httpParameterMap.theme.value;

  res.render('getTheme', {
    args: args
  });
  next();
});

/**
 * Parses out the body portion of the Thematic data cached by Fetch pipeline, and renders it as HTML.
 */
server.get('Body', function (req, res, next) {
  var args = {};
  args.BRThemePart = 'body';
  args.BRThemeName = request.httpParameterMap.theme.value;

  res.render('getTheme', {
    args: args
  });
  next();
});

/**
 * Renders out the content as fetched from BloomReach, without custom styling for testing
 */
server.get('Show', function (req, res, next) {
  var bloomReachTheme = require('*/cartridge/scripts/getBloomReachTheme.js');

  var currentRequest = request;
  var brThemeName = request.httpParameterMap.theme.value;
  var themeHeaderResponse = bloomReachTheme.getBloomReachTheme('header', brThemeName, currentRequest);
  var themeBodyResponse = bloomReachTheme.getBloomReachTheme('body', brThemeName, currentRequest);

  res.render('bloomReachTheme', {
    themeHeaderResponse: themeHeaderResponse,
    themeBodyResponse: themeBodyResponse
  });
  next();
});

module.exports = server.exports();
