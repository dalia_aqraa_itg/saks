'use strict';
/**
 *
 * Creates an analytics script on the page that provides browsing data to BloomReach.
 */

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');
var Site = require('dw/system/Site');

/**
 * Renders blank error isml and error message
 *
 * @param {Object} res response object
 * @param {string} error error string to be passed
 *
 */
function renderBlankError(res, error) {
  res.render('BRError_blank', {
    Message: 'Error Occurred' + error
  });
}

server.get('Show', cache.applyDefaultCache, function (req, res, next) {
  // If custom site pref is enabled render pixel
  if (Site.current.preferences.custom.BR_Enable_Pixel === true) {
    var args = {};
    args.pageType = request.httpParameterMap.pagetype;
    args.category = request.httpParameterMap.category;
    args.categoryID = request.httpParameterMap.categoryid;
    args.productID = request.httpParameterMap.productid;
    args.productName = request.httpParameterMap.productname;
    args.sku = request.httpParameterMap.sku;
    args.searchTerm = request.httpParameterMap.searchterm;
    args.isConversion = request.httpParameterMap.isconversion;
    args.basketValue = request.httpParameterMap.basketValue;
    args.orderID = request.httpParameterMap.orderid;

    var getBloomReachPixel = require('*/cartridge/scripts/getBloomReachPixelModel');
    args = getBloomReachPixel.execute(args);

    if (args.ErrorFlag === true) {
      renderBlankError(res, 'error');
    } else {
      res.render('bloomReachPixel', {
        pixelModel: args.pixelModel
      });
      next();
    }
  } else {
    renderBlankError(res, 'Pixel Site Pref Disabled');
  }
});

module.exports = server.exports();
