'use strict';
/**
 * Controller that returns Thematic Content.
 *
 * @module controllers/Theme
 */

var server = require('server');
var Site = require('dw/system/Site');
var cache = require('*/cartridge/scripts/middleware/cache');
var preferences = require('*/cartridge/config/preferences');
var refineSearch = require('*/cartridge/models/bopis/refineSearch');

var PAGE_SIZE_ITEMS = preferences.defaultPageSize ? preferences.defaultPageSize : 24;
var TOTAL_PAGE_SIZE = preferences.totalPageSize ? preferences.totalPageSize : 96;

server.get('Show', function (req, res, next) {
  var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');
  var CatalogMgr = require('dw/catalog/CatalogMgr');
  var ProductSearchModel = require('dw/catalog/ProductSearchModel');
  var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
  var ProductSearch = require('*/cartridge/models/search/productSearch');

  var redirectToHP = true;

  if (Site.current.getCustomPreferenceValue('BR_Enable_ThematicAPI') === true) {
    var bloomReachTheme = require('~/cartridge/scripts/getBloomReachTheme.js');
    var getPaginationUrls = require('~/cartridge/scripts/getPaginationUrls.js');
    var currentRequest = request;
    var brThemeName = request.httpParameterMap.theme.value;
    session.custom.brThemeName = brThemeName;

    var pageNumber = req.querystring.pageNumber ? parseInt(req.querystring.pageNumber) : 1;
    var start = req.querystring.start ? parseInt(req.querystring.start) : 0;
    var page = req.querystring.page ? parseInt(req.querystring.page) : 1;

    var themeResponse = bloomReachTheme.getBloomReachTheme(brThemeName, currentRequest, start);

    var apiProductSearch = new ProductSearchModel();
    apiProductSearch = searchHelper.setupSearch(apiProductSearch, req.querystring);

    var storeRefineResult = refineSearch.search(apiProductSearch, req.querystring);
    apiProductsearch = storeRefineResult.apiProductsearch; // eslint-disable-line

    var productSearch = new ProductSearch(
      apiProductSearch,
      req.querystring,
      req.querystring.srule,
      CatalogMgr.getSortingOptions(),
      CatalogMgr.getSiteCatalog().getRoot()
    );
    if (themeResponse != null && themeResponse.ThematicResponse != null && !empty(themeResponse.ThematicResponse)) {
      var thematicJSONResponse = JSON.parse(themeResponse.ThematicResponse);
      if (thematicJSONResponse.response != null && !empty(thematicJSONResponse.response) && thematicJSONResponse.response.numFound > 0) {
        redirectToHP = false;

        var thematicLeftNav =
          thematicJSONResponse != null && thematicJSONResponse.page_header != null && thematicJSONResponse.page_header.left_nav
            ? JSON.parse(thematicJSONResponse.page_header.left_nav)
            : '';
        var MetaTag = {};
        MetaTag.pageTitle = thematicJSONResponse != null && thematicJSONResponse.page_header != null ? thematicJSONResponse.page_header.title : ''; // eslint-disable-line
        MetaTag.pageDescription =
          thematicJSONResponse != null && thematicJSONResponse.page_header != null ? thematicJSONResponse.page_header.meta_description : '';
        MetaTag.keywords = thematicJSONResponse != null && thematicJSONResponse.page_header != null ? thematicJSONResponse.page_header.meta_keywords : '';
        pageMetaHelper.setPageMetaData(req.pageMetaData, MetaTag);
        pageMetaHelper.setPageMetaTags(req.pageMetaData, MetaTag);

        var lazyLoadingUrls = getPaginationUrls.getLazyLoadingUrls(thematicJSONResponse.response.numFound, pageNumber, brThemeName, start, page);
        res.render('thematic/theme', {
          productSort: productSearch,
          brThematic: thematicJSONResponse,
          thematicLeftNav: thematicLeftNav,
          lazyLoadingUrls: lazyLoadingUrls,
          paginationUrls: lazyLoadingUrls.themePagingUrls
        });
        next();
      }
    }
  }
  if (redirectToHP) {
    res.render('thematic/redirect', {});
    next();
  }
});

server.get('UpdateGrid', function (req, res, next) {
  var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');
  var CatalogMgr = require('dw/catalog/CatalogMgr');
  var ProductSearchModel = require('dw/catalog/ProductSearchModel');
  var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
  var ProductSearch = require('*/cartridge/models/search/productSearch');

  var redirectToEmptyGrid = true;

  if (Site.current.getCustomPreferenceValue('BR_Enable_ThematicAPI') === true) {
    var bloomReachTheme = require('~/cartridge/scripts/getBloomReachTheme.js');
    var getPaginationUrls = require('~/cartridge/scripts/getPaginationUrls.js');
    var currentRequest = request;
    var brThemeName = request.httpParameterMap.theme.value;

    session.custom.brThemeName = brThemeName;

    var pageNumber = req.querystring.pageNumber ? parseInt(req.querystring.pageNumber) : 1;
    var start = req.querystring.start ? parseInt(req.querystring.start) : 0;
    var page = req.querystring.page ? parseInt(req.querystring.page) : 1;

    var themeResponse = bloomReachTheme.getBloomReachTheme(brThemeName, currentRequest, start);
    var apiProductSearch = new ProductSearchModel();
    apiProductSearch = searchHelper.setupSearch(apiProductSearch, req.querystring);

    var storeRefineResult = refineSearch.search(apiProductSearch, req.querystring);
    apiProductsearch = storeRefineResult.apiProductsearch; // eslint-disable-line

    var productSearch = new ProductSearch(
      apiProductSearch,
      req.querystring,
      req.querystring.srule,
      CatalogMgr.getSortingOptions(),
      CatalogMgr.getSiteCatalog().getRoot()
    );

    if (themeResponse != null && themeResponse.ThematicResponse != null && !empty(themeResponse.ThematicResponse)) {
      var thematicJSONResponse = JSON.parse(themeResponse.ThematicResponse);
      if (thematicJSONResponse.response != null && !empty(thematicJSONResponse.response) && thematicJSONResponse.response.numFound > 0) {
        redirectToEmptyGrid = false;

        var thematicLeftNav =
          thematicJSONResponse != null && thematicJSONResponse.page_header != null && thematicJSONResponse.page_header.left_nav
            ? JSON.parse(thematicJSONResponse.page_header.left_nav)
            : '';
        var MetaTag = {};
        MetaTag.pageTitle = thematicJSONResponse != null && thematicJSONResponse.page_header != null ? thematicJSONResponse.page_header.title : ''; // eslint-disable-line
        MetaTag.pageDescription =
          thematicJSONResponse != null && thematicJSONResponse.page_header != null ? thematicJSONResponse.page_header.meta_description : '';
        MetaTag.keywords = thematicJSONResponse != null && thematicJSONResponse.page_header != null ? thematicJSONResponse.page_header.meta_keywords : '';
        pageMetaHelper.setPageMetaData(req.pageMetaData, MetaTag);
        pageMetaHelper.setPageMetaTags(req.pageMetaData, MetaTag);
        var lazyLoadingUrls = getPaginationUrls.getLazyLoadingUrls(thematicJSONResponse.response.numFound, pageNumber, brThemeName, start, page);
        //
        res.render('thematic/components/productGrid', {
          productSort: productSearch,
          brThematic: thematicJSONResponse,
          thematicLeftNav: thematicLeftNav,
          lazyLoadingUrls: lazyLoadingUrls,
          paginationUrls: lazyLoadingUrls.themePagingUrls
        });
        next();
      }
    }
  }
  if (redirectToEmptyGrid) {
    res.render('thematic/components/productGrid', {});
    next();
  }
});

module.exports = server.exports();
