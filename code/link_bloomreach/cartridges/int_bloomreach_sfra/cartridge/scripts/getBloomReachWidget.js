'use strict';
/**
 *
 * This script returns the widget requested from the
 * cached widget text of our fetch pipeline.
 * It parses the correct portion, based on the type parameter,
 * then returns the requested text.
 *
 *   @input CurrentRequest : dw.system.Request
 *   @input pageType : String
 *	@input productId : String
 *   @input productName : String
 *	@input pageStatus : String
 *
 *	@output bloomReachMoreResults : String
 *	@output bloomReachRelatedSearch : String
 *	@output bloomReachHtmlHeader : String
 */

var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');
var URLAction = require('dw/web/URLAction');

var Encoding = require('dw/crypto/Encoding');
var logBloomReachError = require('*/cartridge/scripts/logBloomReachError');

/**
 * Gets the URL for the page on which the widget is included.
 * @param {Object} request request object
 * @returns {string} currentURL
 */
function getRequestedUrl(request, pageType) {
  // Logic to obtain current pipeline name
  var currentPipeline = 'Home-Show';
  // List
  // eslint-disable-next-line no-undef
  var clicks = session.clickStream.clicks;
  // String
  var path = request.httpPath;
  for (var i = clicks.length - 1; i >= 0; i--) {
    // ClickStreamEntry
    var click = clicks[i];
    if (click.path === path) {
      currentPipeline = click.pipelineName;
      break;
    }
  }

  // Rebuilding URL from current pipeline name, and current GET parameters (using query string to obtain the GET parameters)
  // URLAction
  var action = new URLAction(currentPipeline);
  // URL
  var currentURL = URLUtils.abs(action);
  // Array
  var vars = (request.httpQueryString || '').split('&');
  for (var j = 0; j < vars.length; j++) {
    var pair = vars[j].split('=');
    currentURL.append(decodeURIComponent(pair[0]), decodeURIComponent(pair[1]));
  }
  var strCurrentURL = currentURL.toString();

  if (pageType === 'category') {
    var catPath = strCurrentURL;
    var categoryPath = catPath.substring(catPath.indexOf('/c/') + 3, catPath.length);
    var replacer = new RegExp('/', 'g');
    strCurrentURL = catPath.substring(0, catPath.indexOf('/c/') + 3) + categoryPath.replace(replacer, '_'); 
  }

  return strCurrentURL;
}

/**
 * Provides simple validation that all the parameters necessary for the successful fetch
 * @param {string} url url
 * @param {string} userAgent userAgent
 * @param {string} referrer referrer
 * @param {string} pageType pageType
 * @returns {boolean} isWidgetFetchParamsValid
 */
function validateParametersForWidgetFetch(url, userAgent, referrer, pageType) {
  // Boolean
  var isWidgetFetchParamsValid = true;

  if (empty(url)) {
    logBloomReachError.logFailure('url is a required parameter');
    isWidgetFetchParamsValid = false;
  }
  if (empty(userAgent)) {
    logBloomReachError.logFailure('userAgent is a required parameter');
    isWidgetFetchParamsValid = false;
  }
  if (empty(pageType)) {
    logBloomReachError.logFailure('pageType is a required parameter');
    isWidgetFetchParamsValid = false;
  }
  return isWidgetFetchParamsValid;
}

/**
 * Fetches the compiled content from BloomReach. This content is cached for 24 hours so that subsequent requests
 * against the content do not invoke another round trip to BloomReach Servers, and makes the experience more performant
 * @param {string} url url
 * @returns {Object<dw.svc.Result>} Result object
 */
function fetchWidgetText(url) {
  var bloomReachServices = require('*/cartridge/scripts/services/bloomReachServices');
  var widgetService = bloomReachServices.getBloomReachWidgetService();

  // String
  var text;
  widgetService.setCachingTTL(86400000);

  // Call the service created above
  // Result
  var result = widgetService.call(url);
  var response = result.object;
  if (result.status === 'OK') {
    if (response.statusCode === 200) {
      text = response.text;
    } else {
      text = '';
    }
  } else {
    // Error handling
    logBloomReachError.logFailure('BloomReachWidget : An error occured from web service with status ' + result.errorMessage);
  }
  return text;
}

/**
 * This function is responsible for calling BloomReach's servers and returning the full HTML for widgets
 * This function is expected to be called from the BloomReachWidget-GetWidget controller.
 * @param {string} url url
 * @param {string} userAgent userAgent
 * @param {string} referrer referrer
 * @param {string} pageType pagetype
 * @param {string} productID productID
 * @param {string} productName productName
 * @param {string} pageStatus pageStatus
 * @returns {string} html for widgets
 */
function getBloomReachFullWidgetHTML(url, userAgent, referrer, pageType, productID, productName, pageStatus) {
  if (empty(Site.current.getCustomPreferenceValue('BR_ClientID'))) {
    logBloomReachError.logFailure('BR_ClientID missing from site preferences.');
    return '<!-- Configuration issue, see custom-BloomReach log for details -->';
  }
  var accountID = Site.current.getCustomPreferenceValue('BR_ClientID');
  var output_format = Site.current.getCustomPreferenceValue('BR_OutputFormat') || '';

  if (empty(Site.current.getCustomPreferenceValue('BR_AuthID'))) {
    logBloomReachError.logFailure('BR_AuthID missing from site preferences.');
    return '<!-- Configuration issue, see custom-BloomReach log for details -->';
  }
  var accountKey = Site.current.getCustomPreferenceValue('BR_AuthID');

  var isValid = validateParametersForWidgetFetch(url, userAgent, referrer, pageType);
  if (!isValid) {
    logBloomReachError.logFailure('getBloomReachWidgetHTML called with invalid parameters, see previous messages');
    return '<!-- Some parameters are missing from Widget call. See your logs for details -->';
  }

  if (Site.getCurrent().preferences.custom.BR_Widget_URL == null) {
    logBloomReachError.logFailure('BR_Widget_URL missing from site preferences.');
    return '<!-- Some parameters are missing from Widget call. See your logs for details -->';
  }

  var brURL =
    Site.current.getCustomPreferenceValue('BR_Widget_URL') +
    '?url=' +
    url +
    '&acct_id=' +
    accountID +
    '&acct_auth=' +
    accountKey +
    '&ptype=' +
    pageType +
    '&user_agent=' +
    userAgent +
    '&prod_id=' +
    productID +
    '&prod_name=' +
    productName +
    '&pstatus=' +
    pageStatus +
    '&output_format=' +
    output_format;

  return fetchWidgetText(brURL);
}

/**
 * Creates the pattern matches for the individual components of the BloomReach widgets
 * @param {string} type type of widget pattern
 * @return {RegExp} patt
 */
function regExForType(type) {
  var patt = /a^/; // match nothing

  if (type === 'related') {
    patt = /<!-- BEGIN RELATED -->([\s\S]+)<!-- END RELATED -->/;
  }

  if (type === 'moreresults') {
    patt = /<!-- BEGIN MORE-RESULTS -->([\s\S]+)<!-- END MORE-RESULTS -->/;
  }

  if (type === 'htmlheader') {
    patt = /<!-- BEGIN HTML-HEADER -->([\s\S]+)<!-- END HTML-HEADER -->/;
  }

  return patt;
}

/**
 * Default execution pulls in call widget content for Bloomreach, parses it into the individual widget components present,
 * and puts the HTML of each into its output parameters
 * @param {Object} args = PipelineDictionary
 * @returns {string} - widget html output
 */
function execute(args) {
  var returnObj = args;
  if (!empty(args.CurrentRequest)) {
    // dw.svc.Request
    var request = args.CurrentRequest;
    var url = getRequestedUrl(request, args.pageType);
    var userAgent = !empty(request.httpUserAgent) ? Encoding.toURI(request.httpUserAgent) : '';
    var referrer = !empty(request.httpReferer) ? Encoding.toURI(request.httpReferer) : '';
    var productName = !empty(args.productName) ? Encoding.toURI(args.productName) : '';
    var BR_output_format = Site.current.getCustomPreferenceValue('BR_OutputFormat') || '';

    var widgetText = getBloomReachFullWidgetHTML(url, userAgent, referrer, args.pageType, args.productId, productName, args.pageStatus);

    if (!empty(BR_output_format)) {
      returnObj.bloomReachWidgetJSONRes = JSON.parse(widgetText);
    } else {
      var patt = regExForType('related');
      var result = patt.exec(widgetText);
      returnObj.bloomReachRelatedSearch = result != null && result.length > 1 ? result[1] : '';

      patt = regExForType('moreresults');
      result = patt.exec(widgetText);
      returnObj.bloomReachMoreResults = result != null && result.length > 1 ? result[1] : '';

      patt = regExForType('htmlheader');
      result = patt.exec(widgetText);
      returnObj.bloomReachHtmlHeader = result != null && result.length > 1 ? result[1] : '';
    }
  } else {
    logBloomReachError.logFailure('CurrentRequest not set');
  }
  return returnObj;
}

/**
 * This funciton is responsible for calling a cached pipeline and returning only the widget html requested in the type parameter.
 * This function is expected to be called from the BloomReachWidget-GetWidget pipeline.
 * @param {string} type type
 * @param {string} url url
 * @param {string} userAgent userAgent
 * @param {string} referrer referrer
 * @param {string} pageType pageType
 * @param {string} productID productID
 * @param {string} productName productName
 * @param {string} pageStatus pageStatus
 * @returns {string} error string or html widget
 */
function getBloomReachWidget(type, url, userAgent, referrer, pageType, productID, productName, pageStatus) {
  if (empty(Site.current.getCustomPreferenceValue('BR_ClientID'))) {
    logBloomReachError.logFailure('BR_ClientID missing from site preferences.');
    return '<!-- Configuration issue, see custom-BloomReach log for details -->';
  }
  var accountID = Site.current.getCustomPreferenceValue('BR_ClientID');

  if (empty(Site.current.getCustomPreferenceValue('BR_AuthID'))) {
    logBloomReachError.logFailure('BR_AuthID missing from site preferences.');
    return '<!-- Configuration issue, see custom-BloomReach log for details -->';
  }
  var accountKey = Site.current.getCustomPreferenceValue('BR_AuthID');

  // verify required arguments
  // Boolean
  var isValid = validateParametersForWidgetFetch(url, userAgent, referrer, pageType);
  if (!isValid) {
    logBloomReachError.logFailure('getBloomReachWidget called with invalid parameters, see previous messages');
    return '<!-- Some parameters are missing from Widget call. See your logs for details -->';
  }

  // convert null arguments to empty strings
  var localReferrer = referrer == null ? '' : referrer;
  var localProductID = productID == null ? '' : productID;
  var localProductName = productName == null ? '' : productName;
  var localPageStatus = pageStatus == null ? '' : pageStatus;

  var localUrl = escape(url);
  var localUserAgent = escape(userAgent);
  localReferrer = escape(referrer);
  var localPageType = escape(pageType);
  localProductID = escape(productID);
  localProductName = escape(productName);
  localPageStatus = escape(pageStatus);

  // build our URL
  var brURL = URLUtils.abs(
    'BloomReachWidget-Fetch',
    'accountid',
    accountID,
    'accountkey',
    accountKey,
    'url',
    localUrl,
    'useragent',
    localUserAgent,
    'referrer',
    localReferrer,
    'pagetype',
    localPageType,
    'productid',
    localProductID,
    'productname',
    localProductName,
    'pagestatus',
    localPageStatus
  );

  if (Site.getCurrent().preferences.custom.BR_Widget_API_Timeout == null) {
    logBloomReachError.logFailure('Bloomreach widget settings not defined.');
    return '<!-- Some parameters are missing from Widget call. See your logs for details -->';
  }

  // make the call
  var text = fetchWidgetText(brURL);

  // parse the URL
  var patt = regExForType(type);
  var result = patt.exec(text);

  // Return result
  return result != null && result.length > 1 ? result[1] : '';
}

/**
 * Validates parameters used to make BloomReach HTML fetch
 * @param {string} accountID accountID
 * @param {string} accountKey accountKey
 * @param {string} url url
 * @param {string} userAgent userAgent
 * @param {string} referrer referrer
 * @param {string} pageType pageType
 * @returns {boolean} isValid
 */
function validateParametersForBloomReachHTMLFetch(accountID, accountKey, url, userAgent, referrer, pageType) {
  // eslint-disable-line
  var isValid = true;

  if (empty(accountID)) {
    logBloomReachError.logFailure('accountID is a required parameter');
    isValid = false;
  }
  if (empty(accountKey)) {
    logBloomReachError.logFailure('accountKey is a required parameter');
    isValid = false;
  }
  if (empty(url)) {
    logBloomReachError.logFailure('url is a required parameter');
    isValid = false;
  }
  if (empty(userAgent)) {
    logBloomReachError.logFailure('userAgent is a required parameter');
    isValid = false;
  }
  if (empty(pageType)) {
    logBloomReachError.logFailure('pageType is a required parameter');
    isValid = false;
  }

  return isValid;
}

module.exports = {
  execute: execute,
  getBloomReachWidget: getBloomReachWidget,
  getBloomReachFullWidgetHTML: getBloomReachFullWidgetHTML
};
