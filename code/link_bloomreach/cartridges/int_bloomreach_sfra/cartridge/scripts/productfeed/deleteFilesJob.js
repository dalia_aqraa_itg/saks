'use strict';
/**
 * Deletes files in the /bloomreach directory in IMPEX
 * older than a number of days specified in site preferences. (default: 7 days)
 */

/* API Includes */
var File = require('dw/io/File');
var Site = require('dw/system/Site');
var Logger = require('dw/system/Logger');

var logBloomReachError = require('*/cartridge/scripts/logBloomReachError');
var collections = require('*/cartridge/scripts/util/collections');

/**
 * Upon default execution, deletes files in the impex folder that are older than a configured number of days
 * @param {Object <dw.system.PipelineDictionary>} args args
 * @returns {number} number
 */
function execute(args) {
  // Get the directory the feed files are in
  // <dw.io.File>
  var dir = new File(File.getRootDirectory('IMPEX'), '/bloomreach');
  var isDir = dir.isDirectory();
  var MyDate = new Date();

  if (Site.getCurrent().preferences.custom.BR_FeedFile_Expiration == null) {
    logBloomReachError.logFailure('Bloomreach job settings not defined.');
    return args;
  }

  // Custom Pref
  var feedFileExpiration = !empty(Site.current.getCustomPreferenceValue('BR_FeedFile_Expiration'))
    ? Site.current.getCustomPreferenceValue('BR_FeedFile_Expiration')
    : '7';

  MyDate.setDate(MyDate.getDate() - feedFileExpiration);

  // string
  var dateStr;

  dateStr = MyDate.getFullYear() + '' + ('0' + (MyDate.getMonth() + 1)).slice(-2) + '' + ('0' + MyDate.getDate()).slice(-2);
  // Number
  var olderThan = parseInt(dateStr, 10);

  // make sure its a directory
  if (isDir) {
    // forEach File in the directory
    // tempFile is a <dw.io.File>
    collections.forEach(dir.listFiles(), function (tempFile) {
      // If the filename is long enough to contain the date
      if (tempFile.name.length >= 12) {
        // If file is in correct format (feedXXXXXXX.zip) this should get the date as a number
        var dateNum = parseInt(tempFile.name.substr(4, 8), 10);
        // compare
        if (dateNum < olderThan) {
          // remove returns boolean of attempt
          if (!tempFile.remove()) {
            Logger.error('Failed to delete Feed File : {0}', tempFile.name);
          }
        }
      }
    });
  } else {
    return args;
  }
  return args;
}

module.exports = {
  execute: execute
};
