'use strict';
/**
 *   Send file via FTP to BloomReach.
 *   @input source : String
 *   @input destination : String
 */

/* API Includes */
var File = require('dw/io/File');
var Site = require('dw/system/Site');

var bloomReachServices = require('*/cartridge/scripts/services/bloomReachServices');
var logBloomReachError = require('*/cartridge/scripts/logBloomReachError');

/**
 * Calls FTP LocalService
 * and returns Result Object from service call
 * @returns {Object} Result
 */
function callFTPService() {
  var bloomReachFtpService = bloomReachServices.getBloomreachFTPService();
  // Result Object
  var result = bloomReachFtpService.call();

  if (result.status === 'OK') {
    // The result.object is the object returned by the 'after' callback.
    result = result.object;
  } else {
    // Handle the error. See result.error for more information.
    result = result.error;
  }
  return result;
}

/**
 * Sends file to BLoomreach server via FTP
 * @param {Object <dw.system.PipelineDictionary>} args args
 * @returns {Object} args
 */
function execute(args) {
  var ftp = callFTPService();
  // Object <dw.io.File>
  var file = new File('/IMPEX/' + args.source);

  if (
    Site.current.getCustomPreferenceValue('BR_FTPURL') == null ||
    Site.current.getCustomPreferenceValue('BR_FTPUsername') == null ||
    Site.current.getCustomPreferenceValue('BR_FTPPassword') == null
  ) {
    logBloomReachError.logFailure('Bloomreach ftp settings not defined.');
    return args;
  }

  // Get the BloomReach FTP host and credentials from the BloomReach preferences.
  var host = Site.current.getCustomPreferenceValue('BR_FTPURL');
  var username = Site.current.getCustomPreferenceValue('BR_FTPUsername');
  var password = Site.current.getCustomPreferenceValue('BR_FTPPassword');

  var connected = false;
  var uploaded = false;

  // Connect to the BloomReach FTP server.
  connected = ftp.connect(host, username, password);

  if (!connected) logBloomReachError.logFailure('Unable to connect to host: ' + host);

  // Upload the product feed to the BloomReach FTP server.
  if (connected) {
    if (file.isFile()) {
      uploaded = ftp.putBinary(args.destination, file);

      if (!uploaded) {
        logBloomReachError.logFailure('Failed to upload file ' + file.name + ' to BloomReach FTP host.');
      }
    } else {
      logBloomReachError.logFailure('File "' + file.name + '" does not exist!');
    }

    ftp.disconnect();
  }

  return args;
}
module.exports = {
  execute: execute
};
