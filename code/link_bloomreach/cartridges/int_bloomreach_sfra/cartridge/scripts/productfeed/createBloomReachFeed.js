/* eslint-disable indent */
'use strict';
/**
 *
 * This script iterates over all site products,
 * serializes it to XML in a format consumable by BloomReach,
 * and writes them all to file in the Import/Export directory.
 *
 *  @output FeedFileName : String
 *
 */

/* API Includes */
var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var XMLStreamWriter = require('dw/io/XMLStreamWriter');
var ProductMgr = require('dw/catalog/ProductMgr');

var ProductAvailabilityModel = require('dw/catalog/ProductAvailabilityModel');

var StringUtils = require('dw/util/StringUtils');

var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');
var Calendar = require('dw/util/Calendar');

var collections = require('*/cartridge/scripts/util/collections');
var logBloomReachError = require('~/cartridge/scripts/logBloomReachError');

/**
 * Iterates over the categories for a specific product,
 * gets the categories that it is a part of,
 * and traverses the category tree to create all possible breadcrumbs for the product
 * @param {Object <dw.io.XMLStreamWriter>} xmlWriter xmlWriter
 * @param {Object <dw.catalog.Product>} product Product object
 */
function getProductCategories(xmlWriter, product) {
  var categoryString = '';
  var categoryIdString = '';
  var masterProduct = product; // categories are on master products

  if (product.isVariant()) {
    masterProduct = product.getVariationModel().getMaster();
  }

  collections.forEach(masterProduct.getCategories(), function (category) {
    categoryString = empty(categoryString) ? '' : (categoryString += ';');
    categoryIdString = empty(categoryIdString) ? '' : (categoryIdString += ';');

    while (category.parent != null) {
      if (category.online) {
        if (categoryString !== '') {
          categoryString += '>';
        }
        categoryString += category.displayName;

        if (categoryIdString !== '') {
          categoryIdString += '>';
        }
        categoryIdString += category.ID;
      }

      // eslint-disable-next-line no-param-reassign
      category = category.parent;
    }
  });

  xmlWriter.writeStartElement('crumbs');
  xmlWriter.writeCData(categoryString);
  xmlWriter.writeEndElement();

  xmlWriter.writeStartElement('crumbs_id');
  xmlWriter.writeCData(categoryIdString);
  xmlWriter.writeEndElement();
}

/**
 * Grabs product attributes for a single product and writes them to the entry
 * @param {Object <dw.io.XMLStreamWriter>} xmlWriter xmlWriter
 * @param {Object <dw.catalog.Product>} product Product object
 */
function writeProductAttributes(xmlWriter, product) {
  /* There is no need to send the parent product in the case of variations.  All info is on the children. */
  if (!(product.isMaster() && product.variationModel.variants.size() > 0) && !product.isProductSet()) {
    xmlWriter.writeStartElement('product');
    if (!empty(product.getName())) {
      xmlWriter.writeStartElement('title');
      xmlWriter.writeCData(product.getName());
      xmlWriter.writeEndElement();
    }
    xmlWriter.writeStartElement('url');
    xmlWriter.writeAttribute('href', URLUtils.http('Product-Show', 'pid', product.ID));
    xmlWriter.writeEndElement();
    if (!empty(product.getLongDescription())) {
      xmlWriter.writeStartElement('description');
      xmlWriter.writeCData(product.getLongDescription());
      xmlWriter.writeEndElement();
    }
    xmlWriter.writeStartElement('pid');
    xmlWriter.writeCharacters(product.getID());
    xmlWriter.writeEndElement();
    // eslint-disable-next-line no-undef
    if (product.priceModel != null && product.getPriceModel().getPrice() !== dw.value.Money.NOT_AVAILABLE) {
      xmlWriter.writeStartElement('price');
      xmlWriter.writeCharacters(product.getPriceModel().getPrice());
      xmlWriter.writeEndElement();
    }
    xmlWriter.writeStartElement('availability');
    switch (product.getAvailabilityModel().getAvailabilityStatus()) {
      case ProductAvailabilityModel.AVAILABILITY_STATUS_IN_STOCK:
        xmlWriter.writeCharacters('in stock');
        break;
      case ProductAvailabilityModel.AVAILABILITY_STATUS_PREORDER:
        xmlWriter.writeCharacters('preorder');
        break;
      case ProductAvailabilityModel.AVAILABILITY_STATUS_BACKORDER:
        xmlWriter.writeCharacters('available for order');
        break;
      default:
        xmlWriter.writeCharacters('out of stock');
    }
    xmlWriter.writeEndElement();
    getProductCategories(xmlWriter, product);
    if (!empty(product.getBrand())) {
      xmlWriter.writeStartElement('brand');
      xmlWriter.writeCData(product.getBrand());
      xmlWriter.writeEndElement();
    }
    /*
        xmlWriter.writeStartElement('color');
        xmlWriter.writeEndElement();
        xmlWriter.writeStartElement('size');
        xmlWriter.writeEndElement();
        */
    if (product.isVariant() && !product.isMaster()) {
      xmlWriter.writeStartElement('item_group_id');
      xmlWriter.writeCharacters(product.getVariationModel().getMaster().getID());
      xmlWriter.writeEndElement();
    }

    /* If not using DW imaging, and URL contains '&', make sure to change the below to writeCData(URL) */
    if (product.getImage('large') != null) {
      xmlWriter.writeStartElement('large_image');
      xmlWriter.writeCharacters(product.getImage('large').absURL);
      xmlWriter.writeEndElement();
    }

    /* If not using DW imaging, and URL contains '&', make sure to change the below to writeCData(URL) */
    if (product.getImage('small') != null) {
      xmlWriter.writeStartElement('small_image');
      xmlWriter.writeCharacters(product.getImage('small').absURL);
      xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
  }
}

/**
 * Returns a date obj used to compare with a product's last modified date attribute using days job parameter
 * @param {string} days days
 * @return {Object <Date>} newDate
 */
function dateArithmetic(days) {
  // Todays date
  var currentDate = new Date();
  // Local Days parameter to milliseconds
  var localDays = 1000 * 60 * 60 * 24 * days;
  var newDate = new Date(currentDate - localDays);
  // return the difference as a new date obj
  return newDate;
}

/**
 * Iterates over every site product
 * fetching attributes that BloomReach will use to build SEO rich nvigation,
 * and serializes this data to XML
 * @param {Object <dw.io.XMLStreamWriter>} xmlWriter xmlWriter
 * @param {string} days days
 */
function writeAllCatalogProducts(xmlWriter, days) {
  // <dw.util.SeekableIterator>
  var products = ProductMgr.queryAllSiteProductsSorted();
  try {
    while (products.hasNext()) {
      // Object <dw.catalog.Product>
      var product = products.next();
      // Only get products last modified in the last XX days
      if (!empty(days) && days !== '0') {
        var compareDate = dateArithmetic(days);
        // Add only newly modified products to the xml
        // Newer than than compare date
        if (product.lastModified > compareDate) {
          writeProductAttributes(xmlWriter, product);
        }
      } else {
        // Write all products as days parameter is not defined
        writeProductAttributes(xmlWriter, product);
      }
    }
  } catch (e) {
    logBloomReachError.logFailure(e.toString());
  }
  products.close();
}

/**
 * Upon the default execution of the script,
 * all work to create the product file, in XML format
 * @param {string} days days
 * @returns {string} FeedFileName
 */
function execute(days, filepattern) {
  // <dw.io.File>
  var brDir = new File(File.IMPEX + '/bloomreach/');
  if (!brDir.exists()) {
    brDir.mkdirs();
  }

  // <dw.io.File>
  if (!empty(filepattern)) {
    var file = new File(File.IMPEX + '/bloomreach/' + filepattern + StringUtils.formatCalendar(new Calendar(), 'yyyyMMdd') + '.xml');
  } else {
    var file = new File(File.IMPEX + '/bloomreach/feed.xml');
  }

  if (!file.exists()) {
    if (!file.createNewFile()) {
      logBloomReachError.logFailure('File ' + file.name + ' could not be created!');
      return false;
    }
  }

  // <dw.io.FileWriter>
  var fileWriter = new FileWriter(file, 'UTF-8');
  // <dw.io.XMLStreamWriter>
  var xsw = new XMLStreamWriter(fileWriter);

  xsw.writeStartDocument();
  xsw.writeStartElement('feed'); // <feed>
  xsw.writeStartElement('title'); // <title>
  xsw.writeCharacters('Bloomreach data feed');
  xsw.writeEndElement(); // </title>
  xsw.writeStartElement('link'); // <link>
  xsw.writeAttribute('href', 'http://' + Site.getCurrent().getHttpHostName());
  xsw.writeAttribute('rel', 'self');
  xsw.writeEndElement(); // </link>
  xsw.writeStartElement('updated'); //	<updated>
  xsw.writeCharacters(Date());
  xsw.writeEndElement(); // </updated>
  writeAllCatalogProducts(xsw, days); /* Get all the products and create <entry/> elements */
  xsw.writeEndElement(); // </feed>
  xsw.writeEndDocument();
  xsw.close();
  fileWriter.close();

  // <dw.io.File>
  if (!empty(filepattern)) {
    var zipfile = new File(File.IMPEX + '/bloomreach/' + filepattern + StringUtils.formatCalendar(new Calendar(), 'yyyyMMdd') + '.zip');
  } else {
    var zipfile = new File(File.IMPEX + '/bloomreach/feed' + StringUtils.formatCalendar(new Calendar(), 'yyyyMMdd') + '.zip');
  }

  if (!file.exists()) {
    if (!file.createNewFile()) {
      logBloomReachError.logFailure('File ' + file.name + ' could not be created!');
      return false;
    }
  }

  file.zip(zipfile);
  file.remove();

  var FeedFileName = zipfile.getName();

  return FeedFileName;
}

/*

- Should be the canonical URL.
- Each product should have a unique URL.
    If multiple products share the same URL, please let us know

7.3 Optional Fields

Field
Brand or Vendor
Color - e.g. red, blue
Gender - Female, Male, Unisex
Size - e.g. S/M/L, size 7, 8, etc.
Discounted Price - In addition to regular price
Shipping Info - Free shipping, ships in 2-3 days
Ratings or reviews
    e.g. Overall average rating, etc. 4 stars and/or
    Top 20 review 4+ stars
Availability/ Inventory
Description
*/

module.exports = {
  execute: execute
};
