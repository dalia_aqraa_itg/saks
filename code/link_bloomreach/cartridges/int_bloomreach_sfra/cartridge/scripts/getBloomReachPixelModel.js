/* eslint-disable no-param-reassign */
'use strict';
/**
 * This script file accepts the parameters for the BloomReach analytics "pixel" and creates a model to be passed to the view (template).
 * @output pixelModel : Object
 * @output ErrorFlag : Boolean
 */

var OrderMgr = require('dw/order/OrderMgr');
var CatalogMgr = require('dw/catalog/CatalogMgr');
var Site = require('dw/system/Site');

var collections = require('*/cartridge/scripts/util/collections');
var logBloomReachError = require('*/cartridge/scripts/logBloomReachError');
var pixelModel = require('*/cartridge/scripts/pixelModel');

/**
 * @constructor
 * @classdesc Item Bloomreach item object
 * @param {dw.util.Collection<dw.order.ProductLineItem>} pli - a product line item
 */
function Item(pli) {
  if (!empty(pli.manufacturerSKU)) {
    this.sku = pli.manufacturerSKU;
  }
  this.name = pli.product.name;
  this.price = pli.getAdjustedPrice() / pli.quantityValue.toString();
  this.prod_id = pli.productID;
  this.quantity = pli.quantityValue.toString();
}

/**
 * Returns the model of the Bloomreach pixel
 * @param {PipelineDictionary} args request object parameters
 * @returns {Object} args
 */
function execute(args) {
  if (Site.getCurrent().preferences.custom.BR_ClientID === null) {
    logBloomReachError.logFailure('Bloomreach pixel settings not defined.');
    args.ErrorFlag = true;
    return args;
  }

  args.pixelModel = pixelModel.getPixelModel(args);

  // In the case of a conversion page, capture data specific about the order. No PII is captured.
  if (args.isConversion.value === '1') {
    args.pixelModel.isConversion = '1';
    if (!empty(args.orderID)) {
      // brOrder is a <dw.order.Order>
      var brOrder = OrderMgr.getOrder(args.orderID);

      var basket = {
        items: []
      };

      // shipments is a <dw.order.LineItemCtnr>
      collections.forEach(brOrder.shipments, function (shipments) {
        // pli is a <dw.order.ProductLineItem>
        collections.forEach(shipments.productLineItems, function (pli) {
          var lineItem = new Item(pli);
          basket.items.push(lineItem);
        });
      });

      args.pixelModel.basket = JSON.stringify(basket);
      args.pixelModel.basketValue = brOrder.totalGrossPrice.value;
    }
  }

  /* Create the category by traversing to root element by parent categories */
  if (args.categoryID.value !== null) {
    var categoryId = args.categoryID.value;
    // Category Object
    var cat = CatalogMgr.getCategory(categoryId);
    if (cat != null) {
      var categoryString = cat.displayName;
      while (cat.parent != null && !cat.parent.isRoot()) {
        if (cat.online) {
          categoryString = cat.parent.displayName + '|' + categoryString;
        }
        cat = cat.parent;
      }
      args.pixelModel.category = categoryString;
    }
  }

  args.ErrorFlag = false;
  return args;
}

module.exports = {
  execute: execute
};
