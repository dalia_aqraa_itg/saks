'use strict';

var Logger = require('dw/system/Logger');

/**
 * This function attempts to log to a custom log "BloomReach".  If it fails, it logs to a generic logger.
 * @param {string} msg message
 */
function logFailure(msg) {
  // <dw.system.Log>
  var log = null;
  try {
    /* Try to get a logger object with a the file name prefix of "BloomReach".  This fails on a daily limit of custom log file names */
    log = Logger.getLogger('BloomReach', 'BloomReach');
  } catch (e) {
    /* In event that daily limit is exceded, log to the generic custom log */
    log = Logger.getLogger('BloomReach');
  }
  log.error(msg);
}

module.exports = {
  logFailure: logFailure
};
