'use strict';
var preferences = require('*/cartridge/config/preferences');
var HashMap = require('dw/util/HashMap');
var URLUtils = require('dw/web/URLUtils');

var LOAD_PAGE_SIZE = preferences.defaultPageSize ? preferences.defaultPageSize : 24;
var TOTAL_PAGE_SIZE = preferences.totalPageSize ? preferences.totalPageSize : 96;

/**
 * Generates URLs for Pagination
 *
 * @param {number} productCount - Product Count
 * @param {number} pageNumber - current page number
 * @param {string} brThemeName - BR Theme name
 * @return {string} - More button URL
 */
function getPaginationUrls(productCount, pageNumber, brThemeName) {
  var themePagingUrls = new HashMap();
  var startPage;
  var endPage;
  // eslint-disable-next-line no-param-reassign
  var currentPageNumber = pageNumber;
  var pageCount = Math.ceil(productCount / TOTAL_PAGE_SIZE);

  for (var i = 1; i <= pageCount; i++) {
    themePagingUrls.put(i, URLUtils.url('Theme-Show', 'start', TOTAL_PAGE_SIZE * (i - 1), 'theme', brThemeName, 'page', i));
  }

  if (pageCount <= 3) {
    startPage = 1;
    endPage = pageCount;
  } else if (currentPageNumber <= 2) {
    startPage = 1;
    endPage = 4;
  } else if (currentPageNumber > 2) {
    if (currentPageNumber + 1 < pageCount) {
      startPage = currentPageNumber - 1;
      endPage = currentPageNumber + 1;
    } else {
      startPage = pageCount - 3;
      endPage = pageCount - 1;
    }
  }

  themePagingUrls.put('totalPages', pageCount);
  themePagingUrls.put('startPage', startPage);
  themePagingUrls.put('endPage', endPage);
  themePagingUrls.put('currentPage', currentPageNumber);
  if (currentPageNumber === 1) {
    themePagingUrls.put('disablePreviousButton', 'disabled');
  }
  if (currentPageNumber === pageCount) {
    themePagingUrls.put('disableNextButton', 'disabled');
  }

  themePagingUrls.put('firstPage', themePagingUrls.get(1));
  themePagingUrls.put('previous', themePagingUrls.get(currentPageNumber - 1));
  themePagingUrls.put('next', themePagingUrls.get(currentPageNumber + 1));
  themePagingUrls.put('lastPage', themePagingUrls.get(pageCount));
  themePagingUrls.put('pageSize', LOAD_PAGE_SIZE);

  return themePagingUrls;
}
/**
 * Generates URLs for Pagination
 *
 * @param {number} productCount - Product Count
 * @param {number} pageNumber - current page number
 * @param {string} brThemeName - BR Theme name
 * @param {number} start - BR Theme product start index
 * @param {string} page - BR Theme current page
 * @return {string} - More button URL
 */
function getLazyLoadingUrls(productCount, pageNumber, brThemeName, start, page) {
  var themeLazyLodingUrl = new HashMap();
  var currentPageNumber = pageNumber + 1;
  var pageCount = Math.ceil(productCount / LOAD_PAGE_SIZE);
  var maxPerPageLoad = Math.ceil(TOTAL_PAGE_SIZE / LOAD_PAGE_SIZE);
  var showLazyLoading;
  var themePagingUrls;

  if (currentPageNumber <= pageCount) {
    if (currentPageNumber <= maxPerPageLoad) {
      showLazyLoading = true;
    } else {
      showLazyLoading = false;
      if (pageCount >= currentPageNumber) {
        var themePagingUrls = getPaginationUrls(productCount, page, brThemeName);
      }
    }
  } else {
    showLazyLoading = false;
  }
  var startindex = start + pageNumber * LOAD_PAGE_SIZE;

  var showMoreUrl = URLUtils.url('Theme-UpdateGrid', 'start', startindex, 'theme', brThemeName, 'pageNumber', currentPageNumber, 'page', page);

  if (themePagingUrls) {
    themeLazyLodingUrl.put('themePagingUrls', themePagingUrls);
  }

  themeLazyLodingUrl.put('totalPages', pageCount);
  themeLazyLodingUrl.put('currentPage', currentPageNumber);
  themeLazyLodingUrl.put('showMoreUrl', showMoreUrl);
  themeLazyLodingUrl.put('firstPage', themeLazyLodingUrl.get(1));
  themeLazyLodingUrl.put('pageNumber', pageNumber);
  themeLazyLodingUrl.put('pageSize', LOAD_PAGE_SIZE);
  themeLazyLodingUrl.put('showLazyLoading', showLazyLoading);

  return themeLazyLodingUrl;
}

module.exports = {
  getPaginationUrls: getPaginationUrls,
  getLazyLoadingUrls: getLazyLoadingUrls
};
