'use strict';
/**
 * This script is responsible for fetching Thematic content from Bloomreach.
 * It also fetches and processes the BloomReach content cached on the Demandware instance,
 * retrieving specific portions of the Thematic page for rendering on a customer styled page.
 *
 * @input CurrentRequest : dw.system.Request
 * @input BRThemeName : String
 * @input ReferUrl : String
 * @output ThematicResponse : String
 *
 */

// Built-in B2C Commerce Script Packages
var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');
var preferences = require('*/cartridge/config/preferences');
// Cartridge scripts
var bloomReachServices = require('~/cartridge/scripts/services/bloomReachServices');
var logBloomReachError = require('*/cartridge/scripts/logBloomReachError');

/**
 * Default execution of the script calls out to BloomReach's server to fetch the content to be
 * Rendered on theme pages. The default execution will be used to fetch the content and cache it
 * in a Demandware pipeline so that it's components can be processed and placed properly on the
 * Theme page
 */

/**
 * This function fetches a specific Bloomreach theme via web service call and returns the response
 * @param {Object <dw.system.Request>} currentRequest - Request
 * @param {string} brThemeName - request.httpParameterMap.theme.value
 * @param {string} referUrl - request.httpParameterMap.refer.value
 * @returns {Object} returnObj
 */
function execute(currentRequest, brThemeName, referUrl) {
  var returnObj = {
    currentRequest: currentRequest,
    brThemeName: brThemeName,
    referUrl: referUrl,
    error: false
  };

  var brClient = Site.current.getCustomPreferenceValue('BR_ClientID') || null;
  var brAuth = Site.current.getCustomPreferenceValue('BR_Theme_AuthKey') || null;
  var brThemeUrl = Site.current.getCustomPreferenceValue('BR_Theme_URL') || null;

  if (brClient == null || brAuth == null || brThemeUrl == null) {
    logBloomReachError.logFailure('BloomReach theme settings not defined.');
    return returnObj;
  }

  // url string
  var url =
    brThemeUrl +
    '/' +
    brClient +
    '/' +
    brAuth +
    '/prod/desktop/v1/' +
    brThemeName +
    '.html?user_agent=' +
    escape(currentRequest.httpUserAgent) +
    '&url=' +
    referUrl;
  // <dw.svc.Service> - Service
  var getThemeService = bloomReachServices.getBloomreachThemeService();
  // dw.svc.Result
  var result = getThemeService.call(url);
  var response = result.object;

  if (result.status === 'OK') {
    if (response.statusCode === 200) {
      returnObj.ThematicResponse = response.text;
    } else {
      logBloomReachError.logFailure('BloomReach theme returned status code ' + response.statusCode + ' for ' + url, null);
      returnObj.error = true;
      return returnObj;
    }
  } else {
    logBloomReachError.logFailure('BloomReach Theme : An error occured from web service with status ' + result.errorMessage);
    returnObj.error = true;
    returnObj.errorMsg = result.errorMessage;
  }
  return returnObj;
}

/**
 * Parses returned content based on the part of the page to fetch. Currently only grabs
 * header and body data
 * @param {string} themeResp theme response
 * @param {string} themePart html element parameter either header or footer
 * @returns {string} args
 */
function parseThematicResponse(themeResp, themePart) {
  var objectToParse = String(themeResp);
  var parseHeaderStart = '<!-- BEGIN HTML-HEADER -->';
  var parseHeaderEnd = '<!-- END HTML-HEADER -->';
  var headerCommLength = parseHeaderStart.length;

  var parseBodyStart = '<!-- BEGIN HTML-BODY -->';
  var parseBodyEnd = '<!-- END HTML-BODY -->';
  var bodyCommLength = parseBodyStart.length;
  var relStartPos;
  var relEndPos;
  var returnString;

  if (objectToParse.indexOf(parseHeaderStart) !== -1 && themePart.toLowerCase() === 'header') {
    relStartPos = objectToParse.indexOf(parseHeaderStart) + headerCommLength;
    relEndPos = objectToParse.indexOf(parseHeaderEnd);
    var headerString = objectToParse.slice(relStartPos, relEndPos);
    returnString = headerString;
  }

  if (objectToParse.indexOf(parseBodyStart) !== -1 && themePart.toLowerCase() === 'body') {
    relStartPos = objectToParse.indexOf(parseBodyStart) + bodyCommLength;
    relEndPos = objectToParse.indexOf(parseBodyEnd);
    var bodyString = objectToParse.slice(relStartPos, relEndPos);
    returnString = bodyString;
  }

  return returnString;
}

/**
 * This function parses cached content from the BloomReach-Fetch Pipeline
 * to return and inject specific data from BloomReach. The purpose of this is to
 * be able to put specific content in portions of the page to match the style
 * of the rest of the site
 * @param {string} themePart a portion of the theme in context
 * @param {string} themeName Bloomreach theme name
 * @param {string} currentRequest dw.system.Request
 * @returns {string} parsedResponse
 */
function getBloomReachTheme(themeName, currentRequest, start) {
  // <dw.svc.Service> - Service
  var getThemeService = bloomReachServices.getBloomreachThemeService();
  // string
  var nonprodUrl =
    'BR_NonProdUrl' in Site.current.preferences.custom && Site.current.preferences.custom.BR_NonProdUrl
      ? JSON.parse(Site.current.preferences.custom.BR_NonProdUrl)
      : '';
  var referUrl =
    !empty(nonprodUrl) && nonprodUrl.referUrl != null
      ? nonprodUrl.referUrl
      : escape(currentRequest.httpProtocol + '://' + currentRequest.httpHost + currentRequest.httpPath + '?' + currentRequest.httpQueryString);
  var domainUrl =
    !empty(nonprodUrl) && nonprodUrl.domainUrl != null
      ? nonprodUrl.domainUrl
      : escape(currentRequest.httpProtocol + '://' + currentRequest.httpHost + currentRequest.httpPath);
  var parsedResponse = null;
  var currentRequest = currentRequest;
  var brThemeName = escape(currentRequest.httpParameterMap.theme.value);
  var requestID = escape(currentRequest.requestID);
  var user_agent = escape(currentRequest.httpUserAgent);
  var brClient = Site.current.getCustomPreferenceValue('BR_ClientID') || null;
  var brAuth = Site.current.getCustomPreferenceValue('BR_Theme_AuthKey') || null;
  var brThemeUrl = Site.current.getCustomPreferenceValue('BR_Theme_URL') || null;
  var domain_key = Site.current.getCustomPreferenceValue('BR_Domain_key') || 'saksfifthavenue';
  var rows = Site.current.getCustomPreferenceValue('BR_rows') || 24;
  var cookies = currentRequest.getHttpCookies();
  var brUid = '';

  if (cookies != null && !empty(cookies)) {
    Object.keys(cookies).forEach(function (key) {
      if (cookies[key].name == '_br_uid_2') {
        brUid = cookies[key].value;
      }
    });
  }

  var returnObj = {
    currentRequest: currentRequest,
    brThemeName: themeName,
    referUrl: referUrl,
    error: false
  };

  if (brClient == null || brAuth == null || brThemeUrl == null) {
    logBloomReachError.logFailure('BloomReach theme settings not defined.');
    return returnObj;
  }

  // url string
  var url =
    brThemeUrl +
    '?account_id=' +
    brClient +
    '&auth_key=' +
    brAuth +
    '&domain_key=' +
    domain_key +
    '&request_id=' +
    requestID +
    '&url=' +
    domainUrl +
    '&ref_url=' +
    referUrl +
    '&request_type=thematic' +
    '&rows=' +
    rows +
    '&start=' +
    start +
    '&fl=pid%2Ctitle%2Cbrand%2Cprice%2Csale_price%2Cpromotions%2Cthumb_image%2Csku_thumb_images%2Csku_swatch_images%2Csku_color_group%2Curl%2Cprice_range%2Csale_price_range%2Cdescription&q=' +
    brThemeName +
    '&search_type=keyword' +
    '&_br_uid_2=' +
    brUid;
  // url = "http://core.dxpapi.com/api/v1/core/?account_id=5427&auth_key=mgjv4855pb82hvv&domain_key=saksfifthavenue&request_id=5304407493549&url=www.bloomique.com&ref_url=www.bloomique.com&request_type=thematic&rows=24&start=0&fl=pid%2Ctitle%2Cbrand%2Cprice%2Csale_price%2Cpromotions%2Cthumb_image%2Csku_thumb_images%2Csku_swatch_images%2Csku_color_group%2Curl%2Cprice_range%2Csale_price_range%2Cdescription&q=Diamond%20Jewelry&search_type=keyword";

  // dw.svc.Result
  var result = getThemeService.call(url);
  var response = result.object;

  if (result.status === 'OK') {
    if (response.statusCode === 200) {
      returnObj.ThematicResponse = response.text;
    } else {
      logBloomReachError.logFailure('BloomReach theme returned status code ' + response.statusCode + ' for ' + url, null);
      returnObj.error = true;
      return returnObj;
    }
  } else {
    logBloomReachError.logFailure('BloomReach Theme : An error occured from web service with status ' + result.errorMessage);
    returnObj.error = true;
    returnObj.errorMsg = result.errorMessage;
  }
  return returnObj;
}

module.exports = {
  execute: execute,
  getBloomReachTheme: getBloomReachTheme
};
