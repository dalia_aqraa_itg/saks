'use strict';
/**
 * Controller that returns Thematic Content.
 *
 * @module controllers/Theme
 */

/* Script Module */
var Site = require('dw/system/Site');
var storeFrontName = !empty(Site.current.getCustomPreferenceValue('BR_App_Storefront_Name'))
  ? Site.current.getCustomPreferenceValue('BR_App_Storefront_Name')
  : 'storefront';
var appPath = 'app_' + storeFrontName + '_controllers/cartridge/scripts/app';
var app = require(appPath);

exports.Show = function () {
  if (Site.current.preferences.custom.BR_Enable_ThematicAPI === true) {
    var bloomReachTheme = require('~/cartridge/scripts/getBloomReachTheme.js');
    var themeResponse = {};
    var currentRequest = request;
    var brThemeName = request.httpParameterMap.theme.value;
    var headerThemeResponse = bloomReachTheme.getBloomReachTheme('header', brThemeName, currentRequest);
    var bodyThemeResponse = bloomReachTheme.getBloomReachTheme('body', brThemeName, currentRequest);
    themeResponse.headerThemeResponse = headerThemeResponse;
    themeResponse.bodyThemeResponse = bodyThemeResponse;
    app.getView({ themeResponse: themeResponse }).render('thematic/theme');
  } else {
    app.getView().render('thematic/redirect');
  }
};

exports.Show.public = true;
