'use strict';
/**
 *
 * @module controllers/BloomReachWidget
 */

/* Script Module */
var Site = require('dw/system/Site');
var storeFrontName = !empty(Site.current.getCustomPreferenceValue('BR_App_Storefront_Name'))
  ? Site.current.getCustomPreferenceValue('BR_App_Storefront_Name')
  : 'storefront';
var appPath = 'app_' + storeFrontName + '_controllers/cartridge/scripts/app';
var app = require(appPath);

/**
 * Parses Fetch to return the HTML for the specific widget requested.
 */
exports.GetWidget = function () {
  if (Site.current.preferences.custom.BR_Enable_Widget === true) {
    var brwidget = {};
    brwidget.BRWidgetType = !empty(request.httpParameterMap.type) ? request.httpParameterMap.type.stringValue : '';
    brwidget.BRWidgetAccountID = !empty(request.httpParameterMap.accountID) ? request.httpParameterMap.accountid.stringValue : '';
    brwidget.BRWidgetAccountKey = !empty(request.httpParameterMap.accountKey) ? request.httpParameterMap.accountkey.stringValue : '';
    brwidget.BRWidgetURL = !empty(request.httpParameterMap.url) ? request.httpParameterMap.url.stringValue : '';
    brwidget.BRWidgetUserAgent = request.httpUserAgent;
    brwidget.BRWidgetReferrer = request.httpReferer;
    brwidget.BRWidgetPageType = !empty(request.httpParameterMap.pageType) ? request.httpParameterMap.pagetype.stringValue : '';
    brwidget.BRWidgetProductID = !empty(request.httpParameterMap.productID) ? request.httpParameterMap.productid.stringValue : '';
    brwidget.BRWidgetProductName = !empty(request.httpParameterMap.productName) ? request.httpParameterMap.productname.stringValue : '';
    brwidget.BRWidgetPageStatus = !empty(request.httpParameterMap.pageStatus) ? request.httpParameterMap.pagestatus.stringValue : '';

    app.getView({ brwidget: brwidget }).render('getWidget');
  } else {
    app.getView().render('BRError_blank');
  }
};

/**
 * Fetches full content from BloomReach server and caches it for one day.
 */
exports.Fetch = function () {
  if (request.httpParameterMap.isParameterSubmitted('usestatic')) {
    app.getView().render('staticFetch');
  } else {
    app.getView().render('getFullWidgetHTML');
  }
};

exports.GetWidget.public = true;
exports.Fetch.public = true;
