'use strict';
/**
 *
 * Creates and analytics script on the page that provides browsing data to BloomReach.
 *
 * @module controllers/BloomReachPixel
 */

/* Script Module */
var Site = require('dw/system/Site');
var storeFrontName = !empty(Site.current.getCustomPreferenceValue('BR_App_Storefront_Name'))
  ? Site.current.getCustomPreferenceValue('BR_App_Storefront_Name')
  : 'storefront';

exports.Show = function () {
  // String
  var appPath = 'app_' + storeFrontName + '_controllers/cartridge/scripts/app';
  var app = require(appPath);
  if (Site.current.preferences.custom.BR_Enable_Pixel === true) {
    var args = {};
    args.pageType = request.httpParameterMap.pagetype;
    args.category = request.httpParameterMap.category;
    args.categoryID = request.httpParameterMap.categoryid;
    args.productID = request.httpParameterMap.productid;
    args.productName = request.httpParameterMap.productname;
    args.sku = request.httpParameterMap.sku;
    args.searchTerm = request.httpParameterMap.searchterm;
    args.isConversion = request.httpParameterMap.isconversion;
    args.basketValue = request.httpParameterMap.basketValue;
    args.orderID = request.httpParameterMap.orderid;

    var getBloomReachPixel = require('~/cartridge/scripts/getBloomReachPixelModel');
    args = getBloomReachPixel.execute(args);

    if (args.ErrorFlag === true) {
      app.getView().render('BRError_blank');
    } else {
      app.getView({ pixelModel: args.pixelModel }).render('bloomReachPixel');
    }
  } else {
    app.getView().render('BRError_blank');
  }
};

exports.Show.public = true;
