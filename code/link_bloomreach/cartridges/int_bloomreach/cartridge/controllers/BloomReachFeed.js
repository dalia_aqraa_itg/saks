'use strict';
/**
 * Controller that generates Product Feed and sends it to BloomReach via SFTP.
 * Also provides function that Deletes files older than a number of days specified the setting BR_FeedFile_Expiration
 *
 * @module controllers/BloomReachFeed
 */

exports.Start = function () {
  // Days since last changed product job parameter
  var lastMod = arguments[0].days;
  var createBloomReachFeed = require('*/cartridge/scripts/createBloomReachFeed');
  var FeedFileName = createBloomReachFeed.execute(lastMod);

  var args = {};
  args.source = 'bloomreach/' + FeedFileName;
  args.destination = FeedFileName;

  var sendFeedFTP = require('*/cartridge/scripts/sendFeedFTP');
  sendFeedFTP.execute(args);
};

exports.DeleteFilesByDate = function () {
  var deleteFile = require('*/cartridge/scripts/deleteFilesJob');
  deleteFile.execute();
};

exports.Start.public = false;
exports.DeleteFilesByDate.public = false;
