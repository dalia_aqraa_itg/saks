'use strict';
/**
 *
 * Controller that renders a sample Thematic template used for testing purposes.
 *
 * @module controllers/BloomReachTheme
 */

/* Script Module */
var Site = require('dw/system/Site');
var storeFrontName = !empty(Site.current.getCustomPreferenceValue('BR_App_Storefront_Name'))
  ? Site.current.getCustomPreferenceValue('BR_App_Storefront_Name')
  : 'storefront';
var appPath = 'app_' + storeFrontName + '_controllers/cartridge/scripts/app';
var app = require(appPath);

/**
 * Fetches raw Thematic data from BloomReach and caches it on Demandware instance for one day.
 */
exports.Fetch = function () {
  var args = {};
  args.CurrentRequest = request;
  args.BRThemeName = request.httpParameterMap.theme.value;
  args.ReferUrl = request.httpParameterMap.refer.value;

  var getBloomReachTheme = require('*/cartridge/scripts/getBloomReachTheme');
  args = getBloomReachTheme.execute(args);

  app.getView({ args: args }).render('staticTheme');
};

/**
 * Parses out the header portion of the Thematic data cached by Fetch pipeline, and renders it as HTML.
 */
exports.Header = function () {
  var args = {};
  args.BRThemePart = 'header';
  args.BRThemeName = request.httpParameterMap.theme.value;
  app.getView({ args: args }).render('getTheme');
};

/**
 * Parses out the body portion of the Thematic data cached by Fetch pipeline, and renders it as HTML.
 */
exports.Body = function () {
  var args = {};
  args.BRThemePart = 'body';
  args.BRThemeName = request.httpParameterMap.theme.value;
  app.getView({ args: args }).render('getTheme');
};

/**
 * Renders out the content as fetched from BloomReach, without custom styling for testing
 */
exports.Show = function () {
  var args = {};
  args.CurrentRequest = request;
  args.BRThemeName = request.httpParameterMap.theme.value;
  app.getView({ args: args }).render('bloomReachTheme');
};

exports.Fetch.public = true;
exports.Header.public = true;
exports.Body.public = true;
exports.Show.public = true;
