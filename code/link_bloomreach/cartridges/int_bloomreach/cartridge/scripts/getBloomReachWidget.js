'use strict';
/**
 *
 *
 * This script returns the widget requested from the
 * cached widget text of our fetch pipeline.
 * It parses the correct portion, based on the type parameter,
 * then returns the requested text.
 *
 *   @input CurrentRequest : dw.system.Request
 *   @input pageType : String
 *	@input productId : String
 *   @input productName : String
 *	@input pageStatus : String
 *
 *	@output bloomReachMoreResults : String
 *	@output bloomReachRelatedSearch : String
 *	@output bloomReachHtmlHeader : String
 */

var Logger = require('dw/system/Logger');
var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');
var HTTPService = require('dw/svc/HTTPService');
var Result = require('dw/svc/Result');
var List = require('dw/util/List');
var ClickStreamEntry = require('dw/web/ClickStreamEntry');
var URLAction = require('dw/web/URLAction');
var URL = require('dw/web/URL');
var clickStream = require('dw/web/ClickStream');
var Encoding = require('dw/crypto/Encoding');
var logBloomReachError = require('*/cartridge/scripts/logBloomReachError');

/*Default execution pulls in call widget content for Bloomreach, parses it into the individual widget components present,
and puts the HTML of each into its output parameters */
function execute(args: PipelineDictionary): Number {
  if (!empty(args.CurrentRequest)) {
    var request: Request = args.CurrentRequest;
    var url = getRequestedUrl(request);
    var userAgent = !empty(request.httpUserAgent) ? Encoding.toURI(request.httpUserAgent) : '';
    var referrer = !empty(request.httpReferer) ? Encoding.toURI(request.httpReferer) : '';
    var productName = !empty(args.productName) ? Encoding.toURI(args.productName) : '';

    var widgetText = getBloomReachFullWidgetHTML(url, userAgent, referrer, args.pageType, args.productId, productName, args.pageStatus);

    var patt = regExForType('related');
    var result = patt.exec(widgetText);
    args.bloomReachRelatedSearch = result != null && result.length > 1 ? result[1] : '';

    patt = regExForType('moreresults');
    result = patt.exec(widgetText);
    args.bloomReachMoreResults = result != null && result.length > 1 ? result[1] : '';

    patt = regExForType('htmlheader');
    result = patt.exec(widgetText);
    args.bloomReachHtmlHeader = result != null && result.length > 1 ? result[1] : '';

    return args;
  } else {
    logBloomReachError.logFailure('CurrentRequest not set');
    return args;
  }
}
/*
 * This function is responisble for calling BloomReach's servers and returning the full HTML for widgets
 * This function is expected to be called from the BloomReachWidget-GetWidget pipeline.
 */
function getBloomReachFullWidgetHTML(
  url: String,
  userAgent: String,
  referrer: String,
  pageType: String,
  productID: String,
  productName: String,
  pageStatus: String
) {
  var logger: Logger = Logger.getLogger('BloomReachWidget', 'BloomReachWidget');
  if (empty(Site.getCurrent().preferences.custom.BR_ClientID)) {
    logBloomReachError.logFailure('BR_ClientID missing from site preferences.');
    return '<!-- Configuration issue, see custom-BloomReach log for details -->';
  }
  var accountID = Site.getCurrent().preferences.custom.BR_ClientID;

  if (empty(Site.getCurrent().preferences.custom.BR_AuthID)) {
    logBloomReachError.logFailure('BR_AuthID missing from site preferences.');
    return '<!-- Configuration issue, see custom-BloomReach log for details -->';
  }
  var accountKey = Site.getCurrent().preferences.custom.BR_AuthID;

  var isValid = validateParametersForWidgetFetch(url, userAgent, referrer, pageType);
  if (!isValid) {
    logBloomReachError.logFailure('getBloomReachWidgetHTML called with invalid parameters, see previous messages');
    return '<!-- Some parameters are missing from Widget call. See your logs for details -->';
  }

  if (Site.getCurrent().preferences.custom.BR_Widget_URL == null) {
    logBloomReachError.logFailure('BR_Widget_URL missing from site preferences.');
    return '<!-- Some parameters are missing from Widget call. See your logs for details -->';
  }

  var brURL =
    Site.getCurrent().preferences.custom.BR_Widget_URL +
    '?url=' +
    url +
    '&acct_id=' +
    accountID +
    '&acct_auth=' +
    accountKey +
    '&ptype=' +
    pageType +
    '&user_agent=' +
    userAgent +
    //Removing referrer to help prevent cache fragmentation
    //+ '&ref='        + referrer
    '&prod_id=' +
    productID +
    '&prod_name=' +
    productName +
    '&pstatus=' +
    pageStatus;

  return fetchWidgetText(brURL, Site.getCurrent().preferences.custom.BR_Widget_API_Timeout);
}

/*
 * This funciton is responsible for calling a cached pipeline and returning only the widget html requested in the type parameter.
 * This function is expected to be called from the BloomReachWidget-GetWidget pipeline.
 */
function getBloomReachWidget(
  type: String,
  url: String,
  userAgent: String,
  referrer: String,
  pageType: String,
  productID: String,
  productName: String,
  pageStatus: String
) {
  if (empty(Site.getCurrent().preferences.custom.BR_ClientID)) {
    logBloomReachError.logFailure('BR_ClientID missing from site preferences.');
    return '<!-- Configuration issue, see custom-BloomReach log for details -->';
  }
  var accountID = Site.getCurrent().preferences.custom.BR_ClientID;

  if (empty(Site.getCurrent().preferences.custom.BR_AuthID)) {
    logBloomReachError.logFailure('BR_AuthID missing from site preferences.');
    return '<!-- Configuration issue, see custom-BloomReach log for details -->';
  }
  var accountKey = Site.getCurrent().preferences.custom.BR_AuthID;

  // verify required arguments
  var isValid: Boolean = validateParametersForWidgetFetch(url, userAgent, referrer, pageType);
  if (!isValid) {
    logBloomReachError.logFailure('getBloomReachWidget called with invalid parameters, see previous messages');
    return '<!-- Some parameters are missing from Widget call. See your logs for details -->';
  }

  // convert null arguments to empty strings
  referrer = null == referrer ? '' : referrer;
  productID = null == productID ? '' : productID;
  productName = null == productName ? '' : productName;
  pageStatus = null == pageStatus ? '' : pageStatus;

  url = escape(url);
  userAgent = escape(userAgent);
  referrer = escape(referrer);
  pageType = escape(pageType);
  productID = escape(productID);
  productName = escape(productName);
  pageStatus = escape(pageStatus);

  // build our URL
  var brURL = URLUtils.abs(
    'BloomReachWidget-Fetch',
    'accountid',
    accountID,
    'accountkey',
    accountKey,
    'url',
    url,
    'useragent',
    userAgent,
    'referrer',
    referrer,
    'pagetype',
    pageType,
    'productid',
    productID,
    'productname',
    productName,
    'pagestatus',
    pageStatus
  );

  if (Site.getCurrent().preferences.custom.BR_Widget_API_Timeout == null) {
    logBloomReachError.logFailure('Bloomreach widget settings not defined.');
    return '<!-- Some parameters are missing from Widget call. See your logs for details -->';
  }

  // make the call
  var text = fetchWidgetText(brURL, Site.getCurrent().preferences.custom.BR_Widget_API_Timeout);

  // prase the URL
  var patt = regExForType(type);
  var result = patt.exec(text);

  // Return result
  return result != null && result.length > 1 ? result[1] : '';
}

/*Provides simple validation that all the parameters necessary for the successful fetch*/
function validateParametersForWidgetFetch(url: String, userAgent: String, referrer: String, pageType: String) {
  var logger: Logger = Logger.getLogger('BloomReachWidget', 'BloomReachWidget');
  var isWidgetFetchParamsValid: Boolean = true;

  if (empty(url)) {
    logBloomReachError.logFailure('url is a required parameter');
    isWidgetFetchParamsValid = false;
  }
  if (empty(userAgent)) {
    logBloomReachError.logFailure('userAgent is a required parameter');
    isWidgetFetchParamsValid = false;
  }
  if (empty(pageType)) {
    logBloomReachError.logFailure('pageType is a required parameter');
    isWidgetFetchParamsValid = false;
  }
  return isWidgetFetchParamsValid;
}

/*Validates parameters used to make BloomReach HTML fetch*/
function validateParametersForBloomReachHTMLFetch(accountID: String, accountKey: String, url: String, userAgent: String, referrer: String, pageType: String) {
  var logger: Logger = Logger.getLogger('BloomReachWidget', 'BloomReachWidget');
  var isValid: Boolean = true;

  if (empty(accountID)) {
    logBloomReachError.logFailure('accountID is a required parameter');
    isValid = false;
  }
  if (empty(accountKey)) {
    logBloomReachError.logFailure('accountKey is a required parameter');
    isValid = false;
  }
  if (empty(url)) {
    logBloomReachError.logFailure('url is a required parameter');
    isValid = false;
  }
  if (empty(userAgent)) {
    logBloomReachError.logFailure('userAgent is a required parameter');
    isValid = false;
  }
  if (empty(pageType)) {
    logBloomReachError.logFailure('pageType is a required parameter');
    isValid = false;
  }

  return isValid;
}

/*Fetches the compiled content from BloomReach. This content is cached for 24 hours so that subsequent requests
 * against the content do not invoke another round trip to BloomReach Servers, and makes the experience more performant*/
function fetchWidgetText(url: String, timeout: Number) {
  var bloomReachServices = require('*/cartridge/scripts/services/bloomReachServices');
  var widgetService = bloomReachServices.getBloomReachWidgetService();

  var text: String;
  widgetService.setCachingTTL(86400000);

  var result: Result = widgetService.call(url);
  var response = result.object;
  if (result.status == 'OK') {
    if (response.statusCode == 200) {
      text = response.text;
    } else {
      text = '';
    }
    return text;
  } else {
    //Error handling
    logBloomReachError.logFailure('BloomReachWidget : An error occured from web service with status ' + result.errorMessage);
  }
}

/*Creates the pattern matches for the individual components of the BloomReach widgets*/
function regExForType(type: String) {
  var patt = /a^/; // match nothing

  if (type == 'related') {
    patt = /<!-- BEGIN RELATED -->([\s\S]+)<!-- END RELATED -->/;
  }
  if (type == 'moreresults') {
    patt = /<!-- BEGIN MORE-RESULTS -->([\s\S]+)<!-- END MORE-RESULTS -->/;
  }
  if (type == 'htmlheader') {
    patt = /<!-- BEGIN HTML-HEADER -->([\s\S]+)<!-- END HTML-HEADER -->/;
  }
  return patt;
}

/*Gets the URL for the page on which the widget is included.*/
function getRequestedUrl(request: Request): String {
  // Logic to obtain current pipeline name
  var currentPipeline = 'Home-Show';

  var clicks: List = session.clickStream.clicks;
  var path: String = request.httpPath;
  for (var i = clicks.length - 1; i >= 0; i--) {
    var click: ClickStreamEntry = clicks[i];
    if (click.path == path) {
      currentPipeline = click.pipelineName;
      break;
    }
  }

  // Rebuilding URL from current pipeline name, and current GET parameters (using query string to obtain the GET parameters)
  var action: URLAction = new URLAction(currentPipeline);
  var currentURL: URL = URLUtils.abs(action);
  var vars: Array = (request.httpQueryString || '').split('&');
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split('=');
    currentURL.append(decodeURIComponent(pair[0]), decodeURIComponent(pair[1]));
  }

  return currentURL.toString();
}

module.exports = {
  getBloomReachWidget: getBloomReachWidget,
  execute: execute,
  getBloomReachFullWidgetHTML: getBloomReachFullWidgetHTML
};
