'use strict';
/**
 *
 *   @input LOG : Object
 *
 */
var logBloomReachError = require('*/cartridge/scripts/logBloomReachError');

function execute(args: PipelineDictionary): Number {
  try {
    logBloomReachError.logFailure(args.LOG.toString(), null);
  } catch (e) {}
  return args;
}

module.exports = {
  execute: execute
};
