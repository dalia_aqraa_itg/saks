'use strict';
/**
 *
 *
 * This script is responsible for fetching Thematic content from Bloomreach.  It also fetches and processes the BloomReach content cached on the Demandware instance,
 * retrieving specific portions of the Thematic page for rendering on a customer styled page.
 *
 *	@input CurrentRequest : dw.system.Request
 *	@input BRThemeName : String
 *	@input ReferUrl : String
 *   @output ThematicResponse : String
 *
 */
var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');
var HTTPService = require('dw/svc/HTTPService');
var Result = require('dw/svc/Result');

// Cartridge scripts
var bloomReachServices = require('*/cartridge/scripts/services/bloomReachServices');
var logBloomReachError = require('*/cartridge/scripts/logBloomReachError');

/**
 * Default execution of the script calls out to BloomReach's server to fetch the content to be
 * Rendered on theme pages. The default execution will be used to fetch the content and cache it
 * in a Demandware pipeline so that it's components can be processed and placed properly on the
 * Theme page
 */
function execute(args: PipelineDictionary): Number {
  var brClient = Site.current.getCustomPreferenceValue('BR_ClientID') || null;
  var brAuth = Site.current.getCustomPreferenceValue('BR_Theme_AuthKey') || null;
  var brThemeUrl = Site.current.getCustomPreferenceValue('BR_Theme_URL') || null;

  if (brClient == null || brAuth == null || brThemeUrl == null) {
    logBloomReachError.logFailure('BloomReach theme settings not defined.');
    return args;
  }

  var url: String =
    Site.getCurrent().preferences.custom.BR_Theme_URL +
    '/' +
    brClient +
    '/' +
    brAuth +
    '/prod/desktop/v1/' +
    args.BRThemeName +
    '.html?user_agent=' +
    escape(args.CurrentRequest.httpUserAgent) +
    '&url=' +
    args.ReferUrl;

  // <dw.svc.Service> - Service
  var getThemeService = bloomReachServices.getBloomreachThemeService();
  var result: Result = getThemeService.call(url);
  var response = result.object;

  if (result.status == 'OK') {
    if (response.statusCode == 200) {
      args.ThematicResponse = new String(response.text);
    } else {
      logBloomReachError.logFailure('BloomReach theme returned status code ' + response.statusCode + ' for ' + url, null);
      return args;
    }
  } else {
    logBloomReachError.logFailure('BloomReach Theme : An error occured from web service with status ' + result.errorMessage);
  }
  return args;
}

/*This function parses cached content from the BloomReach-Fetch Pipeline
 * to return and inject specific data from BloomReach. The purpose of this is to
 * be able to put specific content in portions of the page to match the style
 * of the rest of the site
 */
function getBloomReachTheme(themePart: String, themeName: String, currentRequest: Object): String {
  // <dw.svc.Service> - Service
  var getThemeService = bloomReachServices.getBloomreachThemeService();

  var referUrl: String = escape(currentRequest.httpProtocol + '://' + currentRequest.httpHost + currentRequest.httpPath + '?' + currentRequest.httpQueryString);

  var url = URLUtils.abs('BloomReachTheme-Fetch', 'theme', themeName, 'refer', referUrl);
  var result: Result = getThemeService.call(url);
  var response = result.object;
  if (result.status == 'OK') {
    if (response.statusCode == 200) {
      return parseThematicResponse(new String(response.text), themePart);
    } else {
      logBloomReachError.logFailure('BloomReach theme part returned status code ' + response.statusCode + ' for ' + url, null);
      return '';
    }
  } else {
    logBloomReachError.logFailure('BloomReach Theme : An error occured from web service with status ' + result.errorMessage);
  }
}

/*Parses returend content based on the part of the page to fetch. Currently only grabs
 * header and body data*/
function parseThematicResponse(themeResp: String, themePart: String): String {
  var objectToParse: String = new String(themeResp);
  var parseHeaderStart: String = '<!-- BEGIN HTML-HEADER -->';
  var parseHeaderEnd: String = '<!-- END HTML-HEADER -->';
  var headerCommLength = parseHeaderStart.length;

  var parseBodyStart: String = '<!-- BEGIN HTML-BODY -->';
  var parseBodyEnd: String = '<!-- END HTML-BODY -->';
  var bodyCommLength = parseBodyStart.length;

  if (objectToParse.indexOf(parseHeaderStart) != -1 && themePart.toLowerCase() == 'header') {
    var relStartPos = objectToParse.indexOf(parseHeaderStart) + headerCommLength;
    var relEndPos = objectToParse.indexOf(parseHeaderEnd);
    var headerString = objectToParse.slice(relStartPos, relEndPos);
    return headerString;
  }

  if (objectToParse.indexOf(parseBodyStart) != -1 && themePart.toLowerCase() == 'body') {
    var relStartPos = objectToParse.indexOf(parseBodyStart) + bodyCommLength;
    var relEndPos = objectToParse.indexOf(parseBodyEnd);
    var bodyString = objectToParse.slice(relStartPos, relEndPos);
    return bodyString;
  }
}

/*Parses a query string and breaks it into key value pairs*/
function getQueryVariable(queryString: String, variable: String) {
  var query = queryString;
  var vars = query.split('&');
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split('=');
    if (decodeURIComponent(pair[0]) == variable) {
      return decodeURIComponent(pair[1]);
    }
  }
}

module.exports = {
  execute: execute,
  getBloomReachTheme: getBloomReachTheme
};
