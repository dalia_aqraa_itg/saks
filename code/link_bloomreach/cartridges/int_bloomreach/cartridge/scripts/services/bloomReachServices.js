'use strict';

// Import LocalServiceRegistry Lib
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

/**
 * Initilizes 'bloomReach.http.getTheme' LocalService service and returns it
 * Handles getTheme service and defines callbacks
 * @returns {Object <dw.svc.Service>} - Service
 */
function getBloomreachThemeService() {
  // Create HTTPService
  var createThemeService = LocalServiceRegistry.createService('bloomReach.http.getTheme', {
    createRequest: function (svc, requestURL) {
      svc.setURL(requestURL);
      // Set HTTP Method
      svc.setRequestMethod('GET');
      return '';
    },
    parseResponse: function (svc, response) {
      return response;
    },
    filterLogMessage: function (msg) {
      return msg;
    }
  });

  return createThemeService;
}

/**
 * Initiliaze 'bloomReach.http.widget' LocalService and returns it
 * Handles BloomReach Widget web call
 * @returns {Object <dw.svc.Service>} - Service
 */
function getBloomReachWidgetService() {
  // Create HTTPService
  var widgetService = LocalServiceRegistry.createService('bloomReach.http.widget', {
    createRequest: function (svc, requestURL) {
      svc.setURL(requestURL);
      // Set HTTP Method
      svc.setRequestMethod('GET');
      return '';
    },
    parseResponse: function (svc, response) {
      return response;
    },
    filterLogMessage: function (msg) {
      return msg;
    }
  });

  return widgetService;
}

/**
 * Initilizes 'bloomReach.ftp.sendFile' LocalService service and returns it
 * Handles FTP connection to Bloomreach Server
 * @returns {Object <dw.svc.Service>} - Service
 */
function getBloomreachFTPService() {
  // Create HTTPService
  var createFTPService = LocalServiceRegistry.createService('bloomReach.ftp.sendFile', {
    // eslint-disable-next-line no-unused-vars
    execute: function (service, request) {
      return service;
    },
    parseResponse: function (svc, response) {
      return response;
    },
    filterLogMessage: function (msg) {
      return msg;
    }
  });

  return createFTPService;
}

module.exports = {
  getBloomreachThemeService: getBloomreachThemeService,
  getBloomReachWidgetService: getBloomReachWidgetService,
  getBloomreachFTPService: getBloomreachFTPService
};
