'use strict';
/**
 *
 * Retrieve Thematic Parameter from Custom Thematic URL, to pass to Theme-Show. This is only used if not using URL mappings
 *
 *	@input OriginalURL : String
 *	@output BRThemeName : String
 */

function execute(args: PipelineDictionary): Number {
  if (!args.OriginalURL) {
    return args;
  }

  args.BRThemeName = getThematicParam(args.OriginalURL);
  return args;
}

// Expects URL in pattern "http://mysite.com/buy/brsample-0702035915"
// Returns content after last forward-slash
function getThematicParam(url: String) {
  var components = url.split('/');
  return components[components.length - 1]; // last segment in URL path.
}

module.exports = {
  execute: execute
};
