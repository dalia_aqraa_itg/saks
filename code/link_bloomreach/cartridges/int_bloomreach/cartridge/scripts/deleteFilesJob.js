'use strict';
/**
*
* Deletes files in the /bloomreach directory in IMPEX older than a number of days specified in site preferences.
*/

/* API Includes */
var File = require('dw/io/File');
var Site = require('dw/system/Site');
var Logger = require('dw/system/Logger');


/*Upon default execution, deletes files in the impex folder that are older than a configured number of days*/
function execute( args : PipelineDictionary ) : Number {
    //get the directory the feed files are in
    var dir : File = new File(File.getRootDirectory("IMPEX"), "/bloomreach");
    var isDir = dir.isDirectory();
    var MyDate = new Date();

    if(Site.getCurrent().preferences.custom.BR_FeedFile_Expiration == null) {
        logFailure("Bloomreach job settings not defined.");
        return args;
    }

    MyDate.setDate(MyDate.getDate() -dw.system.Site.getCurrent().preferences.custom.BR_FeedFile_Expiration);
    
    var dateStr : String;

    dateStr =  MyDate.getFullYear() + '' + ('0' + (MyDate.getMonth()+1)).slice(-2) + '' + ('0' + MyDate.getDate()).slice(-2);
    var olderThan : Number = parseInt(dateStr);	

    //make sure its a directory
    if(isDir){
        //for each File in the directory
        for each (var tempFile : File in dir.listFiles()) {
            //if the filename is long enough to contain the date
            if(tempFile.name.length >= 12){
                //if file is in correct format (feedXXXXXXX.zip) this should get the date as a number
                var dateNum = parseInt(tempFile.name.substr(4,8));
                //compare
                if(dateNum < olderThan){
                    //.remove returns boolean of attempt
                    if(!tempFile.remove()){
                        Logger.error("Failed to delete Feed File : {0}", tempFile.name);
                    }
                }
            }
        }
    } else {
        return args;
    }
    return args;
}
module.exports= {
    execute: execute
}
