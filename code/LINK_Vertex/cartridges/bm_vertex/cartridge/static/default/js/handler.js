(function ($) {
  $(document).ready(function () {
    var $isVATEnabled = $('#Vertex_isVATEnabled'),
      $requiredFields = $('#Vertex_TaxRegistrationNumber,#Vertex_ISOCountryCode'),
      $labels = $('label[for="Vertex_TaxRegistrationNumber"],label[for="Vertex_ISOCountryCode"]');

    var elem = $('<sup>').attr({ class: 'required' }).html('*');

    $isVATEnabled.on('click', function () {
      classToggle($isVATEnabled);
    });

    classToggle($isVATEnabled);

    function classToggle($target) {
      if ($target.get(0).checked) {
        $requiredFields.each(function (i, field) {
          $(field).attr({
            required: '',
            disabled: false,
            placeholder: ''
          });
        });
        elem.clone().appendTo($labels);
      } else {
        $requiredFields.each(function (i, label) {
          $(label)
            .attr({
              disabled: true,
              placeholder: 'VAT Calculation disabled'
            })
            .val('');
          $(label).removeAttr('required').parent().find('sup').remove();
        });
      }
    }
  });
})(jQuery);
