'use strict';

/**
 * Controller : VertexInc
 *
 * @module controllers/VertexInc
 */

/* API includes */
let ISML = require('dw/template/ISML');
let URLUtils = require('dw/web/URLUtils');
let System = require('dw/system/System');
let Site = require('dw/system/Site');
let Transaction = require('dw/system/Transaction');
let Resource = require('dw/web/Resource');
let API = require('bm_vertex/cartridge/scripts/libVertex');

function show() {
  let error,
    config = API.getConfig();

  var templateData = {
    configurationContinueUrl: URLUtils.https('VertexInc-HandleForm'),
    healthCheckUrl: URLUtils.https('VertexInc-Show'),
    form: config
  };

  if (request.httpParameterMap.job == 'lookupService' || request.httpParameterMap.job == 'taxService') {
    templateData.healthCheckResult = API.healthCheck();
  }
  ISML.renderTemplate('index', templateData);
}

function healthCheckTaxArea() {
  return handleJob(API.checkTaxAreaLookupService());
}

function healthCheckTaxCalculation(arg) {
  var productID = arg && arg.productID;
  return handleJob(API.checkTaxCalculateService(productID));
}

function handleJob(jobResult) {
  if (!jobResult.ok) {
    throw new Error(jobResult.msg || jobResult.errorMessage);
  }

  return jobResult.ok;
}

function handleForm() {
  let result = API.saveForm(request.httpParameterMap);

  response.redirect(URLUtils.https('VertexInc-Show', 'error', result || ''));
}

exports.Show = show;
exports.Show.public = true;

exports.HandleForm = handleForm;
exports.HandleForm.public = true;

exports.HealthCheckTaxArea = healthCheckTaxArea;
exports.HealthCheckTaxCalculation = healthCheckTaxCalculation;
