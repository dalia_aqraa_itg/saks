let constants = require('../constants');
let Transaction = require('dw/system/Transaction');
let Money = require('dw/value/Money');
var HashMap = require('dw/util/HashMap');
var vertexLogger = require('int_vertex/cartridge/scripts/lib/GeneralLogger');
var Logger = require('dw/system/Logger');
var moduleName = 'libVertexApi~';

function API() {
  this.isEnabled = constants.isEnabled;
  this.isInvoiceEnabled = constants.isInvoiceEnabled;
  this.isCleansingEnabled = constants.isAddressCleansingEnabled;
  this.isVATEnabled = constants.isVATEnabled;
}

function getJsonFromMap(map) {
  var jsonDataArray = [];
  var keySet = map.keySet();
  for (var i = 0; i < keySet.length; i++) {
    var key = keySet[i];
    var value = map.get(key);
    var jsonObj = {};
    jsonObj[key] = value.value;
    jsonDataArray.push(jsonObj);
  }
  return jsonDataArray;
}

API.prototype = {
  DeleteTransaction: function (transactionId, source) {
    var logLocation = moduleName + 'DeleteTransaction()',
      serviceResult = false;
    var TaxService = require('int_vertex/cartridge/scripts/init/initVertexApi.js').CalculateTax;

    try {
      TaxService.setThrowOnError().call('DeleteTransaction', { constants: constants, transactionId: transactionId, sender: source });
      serviceResult = true;
    } catch (serviceError) {
      vertexLogger.error(logLocation, 'DeleteTransaction call failed. Cause:' + serviceError.message);
    }

    return serviceResult;
  },
  LookupTaxArea: function (form, cart) {
    var logLocation = moduleName + 'LookupTaxArea()',
      response = {
        result: true,
        message: '',
        addresses: []
      },
      AreaService;
    var Helper = require('../helper/Helper');
    // Start logs block
    if (!this.isEnabled) {
      vertexLogger.error(logLocation, 'Vertex service is disabled');
      return response;
    }

    if (!this.isCleansingEnabled) {
      vertexLogger.error(logLocation, 'Vertex Address Cleansing is disabled');
      return response;
    }

    this.resetTaxes(cart);

    AreaService = require('int_vertex/cartridge/scripts/init/initVertexApi.js').LookupTaxAreas;
    let lookupResult = AreaService.call(form, constants);

    switch (lookupResult.status) {
      case 'ERROR':
        response.result = false;
        response.message = lookupResult.msg;
        vertexLogger.error(logLocation, response.message || response.msg || response.errorMessage);
        break;
      case 'SERVICE_UNAVAILABLE':
        vertexLogger.error(logLocation, response.message || response.msg || response.errorMessage);
        break;
      default:
        if (typeof lookupResult.object.response == 'string') {
          response.result = lookupResult.object.response == 'NORMAL';
        }

        if (response.result && lookupResult.object.addresses.length) {
          let normalizedAddresses = Helper.beautifyAddresses(form, lookupResult.object.addresses);
          response.result = Helper.isEqualAddresses(normalizedAddresses);
          normalizedAddresses.push(Helper.getCurrentNormalizedAddress());
          response.addresses = normalizedAddresses;
        } else {
          response.result = false;
        }
        response.message = lookupResult.object.message;
    }
    return response;
  },

  CalculateTax: function (requestType, cart) {
    var logLocation = moduleName + 'CalculateTax()',
      response = {
        result: true,
        message: ''
      },
      TaxService;

    if (!this.isEnabled) {
      vertexLogger.debug(logLocation, 'Vertex service is disabled');
      return response;
    }

    if (requestType == 'Invoice' && !this.isInvoiceEnabled) {
      vertexLogger.debug(logLocation, 'Vertex Invoice Request is disabled');
      return response;
    }

    // Delete IsVertexTaxCalculated custom attribute
    Transaction.wrap(function () {
      delete cart.custom.IsVertexTaxCalculated;
    });

    TaxService = require('int_vertex/cartridge/scripts/init/initVertexApi.js').CalculateTax;
    let calculationResponse = TaxService.call('CalculateTax', { constants: constants, requestType: requestType, cart: cart });

    if (calculationResponse.status) {
      switch (calculationResponse.status) {
        case 'ERROR':
          response.result = false;
          response.message = 'Invalid address';
          vertexLogger.debug(logLocation, 'calculationResponse.status == ERROR');
          this.resetTaxes(cart);
          break;
        case 'SERVICE_UNAVAILABLE':
          vertexLogger.error(logLocation, calculationResponse.msg || calculationResponse.errorMessage);
          this.resetTaxes(cart);
          break;
        default:
          /*
           * handle invoice request, if it is last we dont need to update order with empty parameters
           * so just fix in service side that it was invoice call and then return
           */
          if (requestType === 'Invoice') {
            return response;
          }
          var taxationDetails = [];
          var taxationImpositionTaxMap = new HashMap();
          if (calculationResponse.object && calculationResponse.object.lineItem) {
            var totalOrderTax = 0;
            var giftWrapTax = {};
            // Store ECo Fee Tax Response
            var ecoFeeTaxMap = new HashMap();
            for (var itemKey in calculationResponse.object.lineItem) {
              var ECOFeeVertexLineItem = calculationResponse.object.lineItem[itemKey];
              var ECOLineItemId = ECOFeeVertexLineItem.lineItemId.split('??');
              // Check If this Line Item is ECO Fee
              var ECOFEEProductID;
              if (ECOLineItemId && ECOLineItemId[1] && ECOLineItemId[1].indexOf('ECOFEE') > -1) {
                ECOFEEProductID = ECOLineItemId[1].split(':')[1];
                if (ECOFEEProductID) {
                  ecoFeeTaxMap.put(ECOFEEProductID, ECOFeeVertexLineItem);
                  continue;
                }
              }
            }

            for (var itemKey in calculationResponse.object.lineItem) {
              var vertexLineItem = calculationResponse.object.lineItem[itemKey],
                lineItemId = vertexLineItem.lineItemId.split('??'),
                taxRateCount = 0,
                totalTaxCount = 0,
                jurisdictionLevel = '',
                taxRate = 0,
                calculatedTax = 0,
                jurisdictionID = 0,
                taxable;
              var ItemLavelTaxationImpositionTaxList = [];

              if (lineItemId && lineItemId[1] && lineItemId[1].indexOf('ECOFEE') > -1) {
                continue;
              }

              for (var taxes in vertexLineItem.taxes) {
                var tax = vertexLineItem.taxes[taxes];

                jurisdictionID = tax.getJurisdiction().getJurisdictionId();
                jurisdictionLevel = tax.getJurisdiction().getJurisdictionLevel().value();

                taxRate = tax.getEffectiveRate() * 1;
                calculatedTax = new Money(tax.getCalculatedTax() * 1, session.getCurrency().currencyCode);
                taxationDetails.push('Line Item: ' + lineItemId[1]);
                taxationDetails.push('FairMarket Value: ' + vertexLineItem.getFairMarketValue());
                taxationDetails.push('Extended Price: ' + vertexLineItem.getExtendedPrice());
                taxationDetails.push('JurisdictionID: ' + jurisdictionID);
                taxationDetails.push('Jurisdiction Level: ' + jurisdictionLevel);
                if (tax.invoiceTextCode.size()) {
                  taxationDetails.push(' Invoice Tax Code: ' + tax.invoiceTextCode.join('|'));
                }
                taxationDetails.push('Tax Rate: ' + taxRate);
                taxationDetails.push('Calculated Tax: ' + calculatedTax);

                if (tax.imposition && tax.imposition.value) {
                  if (taxationImpositionTaxMap.containsKey(tax.imposition.value)) {
                    var updatedImpositionTax = taxationImpositionTaxMap.get(tax.imposition.value).add(calculatedTax);
                    taxationImpositionTaxMap.put(tax.imposition.value, updatedImpositionTax);
                  } else {
                    taxationImpositionTaxMap.put(tax.imposition.value, calculatedTax);
                  }

                  var lineItemTaxationObj = {};
                  lineItemTaxationObj.taxType = 'regular';
                  lineItemTaxationObj.imposition = tax.imposition.value;
                  lineItemTaxationObj.calculatedTax = calculatedTax.valueOrNull;
                  lineItemTaxationObj.effectiveRate = tax.getEffectiveRate().toString();
                  ItemLavelTaxationImpositionTaxList.push(lineItemTaxationObj);
                }

                taxable = new Money(tax.getTaxable() * 1, session.getCurrency().currencyCode);
                taxRateCount += tax.getEffectiveRate() * 1; // * 1 convert Decimal to Number;
                totalTaxCount += tax.getCalculatedTax();
                totalOrderTax += tax.getCalculatedTax();
              }

              if (vertexLineItem.lineItemId && vertexLineItem.lineItemId == 'GIFTWRAP') {
                giftWrapTax.taxRateCount = taxRateCount;
                giftWrapTax.taxable = taxable;
                giftWrapTax.totalTaxCount = totalTaxCount;
              }

              Transaction.wrap(function () {
                let shipments = cart.shipments.iterator();
                let taxAmount = new Money(totalTaxCount, session.getCurrency().currencyCode);
                while (shipments.hasNext()) {
                  var shipment = shipments.next();
                  var stateCode;
                  if (shipment.shippingAddress && shipment.shippingAddress.stateCode) {
                    stateCode = shipment.shippingAddress.stateCode;
                  }
                  let lineItems = shipment.allLineItems.iterator();

                  while (lineItems.hasNext()) {
                    let lineItem = lineItems.next();
                    let className = lineItem.constructor.name;
                    if (className == 'dw.order.ProductLineItem') {
                      if ((lineItem.productID == lineItemId[1] || lineItem.optionID == lineItemId[1]) && shipment.ID == vertexLineItem.projectNumber) {
                        lineItem.updateTax(taxRateCount, taxable);
                        // Logic to check if Product has ECO fee applicable
                        var ecoFee;
                        if (stateCode) {
                          ecoFee = require('../helper/Helper').getEcoFeeCharges(lineItem.product, stateCode);
                          if (ecoFee) {
                            ecoFee = new Money(ecoFee * lineItem.quantityValue, session.getCurrency().currencyCode);
                            // Update TaxAmount
                            taxationImpositionTaxMap.put('ECO', ecoFee);
                            taxAmount = taxAmount.add(ecoFee);
                            lineItem.custom.ecoFee = ecoFee.valueOrNull;

                            // Check if we have tax calculated for ECO Fee and So Store All the required details for ECO Fees
                            if (ecoFeeTaxMap && ecoFeeTaxMap.get(lineItem.productID)) {
                              var ecoFeeTax = ecoFeeTaxMap.get(lineItem.productID);
                              for (var taxes in ecoFeeTax.taxes) {
                                var ecoTax = ecoFeeTax.taxes[taxes];
                                var ecoFeeCalculatedTax = new Money(ecoTax.getCalculatedTax() * 1, session.getCurrency().currencyCode);
                                // Update the Line Item Tax as well
                                taxAmount = taxAmount.add(ecoFeeCalculatedTax);
                                if (ecoTax.imposition && ecoTax.imposition.value) {
                                  if (taxationImpositionTaxMap.containsKey(ecoTax.imposition.value)) {
                                    var updatedImpositionTax = taxationImpositionTaxMap.get(ecoTax.imposition.value).add(ecoFeeCalculatedTax);
                                    taxationImpositionTaxMap.put(ecoTax.imposition.value, updatedImpositionTax);
                                  } else {
                                    taxationImpositionTaxMap.put(ecoTax.imposition.value, ecoFeeCalculatedTax);
                                  }

                                  var lineItemTaxationObj = {};
                                  lineItemTaxationObj.taxType = 'ecoFee';
                                  lineItemTaxationObj.imposition = ecoTax.imposition.value;
                                  lineItemTaxationObj.calculatedTax = ecoFeeCalculatedTax.valueOrNull;
                                  lineItemTaxationObj.effectiveRate = ecoTax.getEffectiveRate().toString();
                                  ItemLavelTaxationImpositionTaxList.push(lineItemTaxationObj);
                                }
                              }
                            }
                          } else {
                            lineItem.custom.ecoFee = null;
                          }
                        }
                        lineItem.custom.vertex_taxation_details = JSON.stringify(ItemLavelTaxationImpositionTaxList);
                        lineItem.updateTaxAmount(taxAmount);
                      }
                    } else if (className == 'dw.order.ShippingLineItem') {
                      if (lineItem.ID == lineItemId[1] && shipment.ID == vertexLineItem.projectNumber) {
                        lineItem.updateTax(taxRateCount, taxable);
                        lineItem.custom.vertex_taxation_details = JSON.stringify(ItemLavelTaxationImpositionTaxList);
                        lineItem.updateTaxAmount(taxAmount);
                      }
                    } else if (className == 'dw.order.ProductShippingLineItem') {
                      if (shipment.ID == vertexLineItem.projectNumber) {
                        if (lineItem.adjustedPrice.decimalValue != 0.0) {
                          lineItem.updateTax(taxRateCount, taxable);
                          lineItem.updateTaxAmount(taxAmount);
                        } else {
                          lineItem.updateTax(0.0);
                        }
                      }
                    } else {
                      // price adjustments...
                      lineItem.updateTax(0.0);
                    }
                  }
                }

                if (!cart.getPriceAdjustments().empty || !cart.getShippingPriceAdjustments().empty) {
                  // calculate a mix tax rate from
                  let basketPriceAdjustmentsTaxRate = cart.getMerchandizeTotalGrossPrice().value / cart.getMerchandizeTotalNetPrice().value - 1;

                  let basketPriceAdjustments = cart.getPriceAdjustments().iterator();
                  while (basketPriceAdjustments.hasNext()) {
                    let basketPriceAdjustment = basketPriceAdjustments.next();
                    basketPriceAdjustment.updateTax(0.0);
                  }

                  let basketShippingPriceAdjustments = cart.getShippingPriceAdjustments().iterator();
                  while (basketShippingPriceAdjustments.hasNext()) {
                    let basketShippingPriceAdjustment = basketShippingPriceAdjustments.next();
                    basketShippingPriceAdjustment.updateTax(0.0);
                  }
                }
              });
            }

            if (giftWrapTax && giftWrapTax.totalTaxCount) {
              Transaction.wrap(function () {
                // Look for Gift Wrap Option at Order
                let lineItems = cart.getAllLineItems().iterator();
                let taxAmount = new Money(giftWrapTax.totalTaxCount, session.getCurrency().currencyCode);
                while (lineItems.hasNext()) {
                  var item = lineItems.next();
                  var itemClass = item.constructor.name;
                  if (itemClass == 'dw.order.PriceAdjustment' && !empty(item.promotionID) && item.promotionID === 'GiftOptions') {
                    if (item.basePrice.decimalValue != 0.0) {
                      item.updateTax(giftWrapTax.taxRateCount, giftWrapTax.taxable);
                      item.updateTaxAmount(taxAmount);
                    } else {
                      item.updateTax(0.0);
                    }
                  }
                }
              });
            }

            Transaction.wrap(function () {
              taxationDetails.push('Total Tax: ' + Math.round(totalOrderTax * 100) / 100);
              cart.custom.vertex_taxation_details = taxationDetails;
            });
            if (taxationImpositionTaxMap.length > 0) {
              var taxationImpositionTaxObj = getJsonFromMap(taxationImpositionTaxMap);
              Transaction.wrap(function () {
                cart.custom.vertex_taxation_imposition = JSON.stringify(taxationImpositionTaxObj);
              });
            }
          }
      } // end of switch (calculationResponse.status)
    } // if (calculationResponse.status)
    return response;
  },

  /**
   * Set Taxes to 0.00
   */
  resetTaxes: function (basket) {
    var cart;
    try {
      cart = basket.object;
    } catch (e) {
      cart = basket;
    }

    Transaction.wrap(function () {
      let shipments = cart.getShipments().iterator();
      while (shipments.hasNext()) {
        let shipment = shipments.next();
        let shipmentLineItems = shipment.getAllLineItems().iterator();

        while (shipmentLineItems.hasNext()) {
          let _lineItem = shipmentLineItems.next();

          _lineItem.updateTax(0.0);
        }
      }
      cart.custom.IsVertexTaxCalculated = false;
    });
  },
  /**
   * @example this.log('info' || constants.INFO_LOG, "some errors: {0}", string_variable)
   */
  log: function (level, message, data) {
    var Logger = require('dw/system/Logger').getLogger('VertexInc', 'Vertex.General');
    Logger[level](message, data);
  }
};

module.exports = new API();
