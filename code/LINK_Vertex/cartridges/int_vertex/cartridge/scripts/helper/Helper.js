let StringUtils = require('dw/util/StringUtils');
let SecureRandom = require('dw/crypto/SecureRandom');
let Resource = require('dw/web/Resource');
let Transaction = require('dw/system/Transaction');

function Helper() {}

function CanadaProvinenceMap() {
  return {
    AB: 'ALBERTA',
    BC: 'BRITISH_COLUMBIA',
    MB: 'MANITOBA',
    NB: 'NEW_BRUNSWICK',
    NL: 'NEWFOUNDLAND',
    NS: 'NOVA_SCOTIA',
    NT: 'NORTHWEST_TERRITORIES',
    NU: 'NUNAVUT',
    ON: 'ONTARIO',
    QC: 'QUEBEC',
    PE: 'PRINCE_EDWARD_ISLAND',
    SK: 'SASKATCHEWAN',
    YT: 'YUKON'
  };
}

function getTopLevelCategory(category) {
  if (category.parent.ID === 'root') {
    return category;
  }
  return getTopLevelCategory(category.parent);
}

Helper.prototype = {
  getEcoFeeCharges: function (product, stateCode) {
    try {
      if (product && stateCode) {
        var getEcoFee = 'hbFeeMatrix' in product.custom && !empty(product.custom.hbFeeMatrix) ? JSON.parse(product.custom.hbFeeMatrix) : null;
        if (!empty(getEcoFee)) {
          var ecoFeeState = CanadaProvinenceMap()[stateCode];
          if (ecoFeeState && getEcoFee[ecoFeeState]) {
            return parseFloat(getEcoFee[ecoFeeState]);
          }
        }
      }
    } catch (e) {
      var Logger = require('dw/system/Logger');
      Logger.info('Cart Calculation error while calculation eco fee for the product ' + product.ID + ' error: ' + e.message);
    }
    return null;
  },

  getFormattedDate: function () {
    let date = new Date();
    return StringUtils.format(
      '{0}-{1}-{2}',
      date.getFullYear().toString(),
      this.insertLeadingZero(date.getMonth() + 1),
      this.insertLeadingZero(date.getDate())
    );
  },

  insertLeadingZero: function (number) {
    return number < 10 ? '0' + number : number;
  },

  beautifyAddresses: function (form, addresses) {
    let random = new SecureRandom();
    if (!addresses.length || ('postalAddress' in addresses[0] && empty(addresses[0].postalAddress))) {
      return [];
    }
    let items = [];
    for (let i in addresses) {
      var address = addresses[i];

      try {
        if (address.postalAddress) {
          address = address.postalAddress[0];
        }
      } catch (e) {
        // do nothing
        // address@TaxLookupArea throw an exception in case undefined method instead undefined value
      }

      let newAddress = {
        UUID: random.nextInt(),
        ID: address.city,
        key: address.postalCode + address.mainDivision + address.streetAddress1 + address.streetAddress2 + address.city + address.country,
        countryCode: address.country.toLowerCase().substring(0, address.country.length - 1),
        postalCode: address.postalCode,
        stateCode: address.mainDivision,
        address1: address.streetAddress1 == null ? form.address1.value : address.streetAddress1,
        address2: address.streetAddress2,
        displayValue: address.city,
        city: address.city
      };

      items.push(newAddress);
    }
    return items;
  },

  getCurrentNormalizedAddress: function () {
    let form = null;
    let postal = null;
    let state = null;
    if (request.httpParameterMap.multishipping && request.httpParameterMap.multishipping.value) {
      if (session.forms.multishipping) {
        form = session.forms.multishipping.editAddress.addressFields;
        postal = form.postal.value;
        state = form.states.state.value;
      }
    } else {
      if (session.forms.singleshipping) {
        form = session.forms.singleshipping.shippingAddress.addressFields;
        postal = form.postal.value;
        state = form.states.state.value;
      } else if (session.forms.shipping.shippingAddress) {
        form = session.forms.shipping.shippingAddress.addressFields;
        postal = form.postalCode.value;
        state = form.states.stateCode.value;
      }
    }

    return {
      UUID: form.UUID,
      ID: form.city.value,
      key: Resource.msg('form.label.asis', 'vertex', null),
      address1: form.address1.value,
      address2: form.address2.value,
      city: form.city.value,
      postalCode: postal,
      stateCode: state,
      countryCode: form.country.value,
      displayValue: form.city.value
    };
  },

  /**
   * @description check if selected address fields and address fields in the form are identical.
   * address2 field isn't required, so we skip this field.
   */
  isEqualAddresses: function (normalizedAddresses) {
    let restrictedFields = ['ID', 'key', 'UUID', 'displayValue', 'address2'],
      normalizedForm = this.getCurrentNormalizedAddress();

    if (session.custom.VertexAddressSuggestions) {
      let previousForm = JSON.parse(session.custom.VertexAddressSuggestions)[0];
      let currentForm = normalizedForm;
      let formsIsEqual = true;
      let formKeys = Object.keys(previousForm);

      for (let i in Object.keys(previousForm)) {
        let formFieldValue = previousForm[formKeys[i]];
        if (restrictedFields.indexOf(formKeys[i]) == -1) {
          if (formFieldValue.toUpperCase() != currentForm[formKeys[i]].toUpperCase()) {
            formsIsEqual = false;
          }
        }
      }

      if (formsIsEqual) {
        normalizedAddresses.push(normalizedForm);
      }
    }

    for (let i in normalizedAddresses) {
      let address = normalizedAddresses[i];
      let formIsEqual = true;
      let formKeys = Object.keys(address);

      for (let k in formKeys) {
        let fieldKey = formKeys[k];
        if (restrictedFields.indexOf(fieldKey) == -1) {
          let fieldValue = address[fieldKey];

          if (normalizedForm[fieldKey].toUpperCase() != fieldValue.toUpperCase()) {
            formIsEqual = false;
          }
        }
      }

      if (formIsEqual) {
        return true;
      }
    }

    return false;
  },

  /**
   * @description get category name
   * @param product ProductLineItem
   * @param categoryDepth Integer 0: root, 1: product category
   * @example 'fruits-bananas-yellow_bananas'
   */
  getProductClass: function (productWrap) {
    var product;

    if (!empty(productWrap) && productWrap.product) {
      if (productWrap.product.variant) {
        product = productWrap.product.masterProduct;
      } else {
        product = productWrap.product;
      }
      if (product.getTaxClassID()) {
        var taxClass = product.getTaxClassID();
        if (taxClass.length === 1) {
          // Left Padding with 0 upto 3 digit.
          taxClass = '00' + taxClass;
        } else if (taxClass.length == 2) {
          taxClass = '0' + taxClass;
        }
        return taxClass;
      } else {
        // if product have no Classification category then get Primary category
        if (!product.classificationCategory && product.primaryCategory) {
          return product.primaryCategory.ID;
        }
        // In the Worst case when Product doesn't have primary or classification product.
        if (!product.classificationCategory && !product.primaryCategory) {
          var topLevelCategory = getTopLevelCategory(product.categories[0]);
          return topLevelCategory.ID;
        }

        return product.classificationCategory.ID;
      }
    }
  },

  prepareCart: function (cart) {
    var GCs = cart.getGiftCertificateLineItems();
    var Products = cart.getAllProductLineItems();

    if (GCs.length) {
      var GCsi = GCs.iterator();
      Transaction.wrap(function () {
        while (GCsi.hasNext()) {
          var GC = GCsi.next();
          GC.updateTax(0.0);
        }
      });
    }

    // if we have only GC in the cart we should set zero tax to the default shipment
    if (!Products.length) {
      var lineItems = cart.getAllLineItems().iterator();

      while (lineItems.hasNext()) {
        let item = lineItems.next();
        let itemClassName = item.constructor.name;

        if (itemClassName == 'dw.order.ShippingLineItem') {
          Transaction.wrap(function () {
            item.updateTax(0.0);
          });
        }
      }
    }
  }
};

module.exports = new Helper();
