let LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
let VertexHelper = require('../helper/Helper');
var vertexLogger = require('int_vertex/cartridge/scripts/lib/GeneralLogger');
var Logger = require('dw/system/Logger');
let Money = require('dw/value/Money');
var preferences = require('*/cartridge/config/preferences');
var moduleName = 'initVertexApi~';

function createDeleteTransactionEnvelope(transactionId, constants, sender) {
  /*
    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Body>
            <VertexEnvelope xmlns="urn:vertexinc:o-series:tps:7:0">
                <Login>
                    <TrustedId>6472904343785499</TrustedId>
                </Login>
                <DeleteRequest transactionId="T000123"/>
                <ApplicationData>
                    <Sender>SFCCPlatform</Sender>
                    <MessageLogging returnLogEntries="true"/>
                </ApplicationData>
            </VertexEnvelope>
        </soap:Body>
     </soap:Envelope>
     */

  var logLocation = moduleName + 'createDeleteTransactionEnvelope',
    vertexSoap = webreferences2.CalculateTax,
    Envelope = new vertexSoap.VertexEnvelope(),
    LoginType = new vertexSoap.LoginType(),
    DeleteRequestType = new vertexSoap.DeleteRequestType(),
    ApplicationData = new vertexSoap.VertexEnvelope.ApplicationData(),
    MessageLogging = new vertexSoap.VertexEnvelope.ApplicationData.MessageLogging(),
    selectedLevelType,
    OverrideLoggingThreshold;

  vertexLogger.begin(logLocation, 'Parameters:', { transactionId: transactionId, sender: sender });

  if (!empty(constants.TrustedId)) {
    LoginType.setTrustedId(constants.TrustedId);
  } else if (!empty(constants.Username) && !empty(constants.Password)) {
    LoginType.setUserName(constants.Username);
    LoginType.setPassword(constants.Password);
  }

  // Add request processing detailed logging
  if (constants.detailedLogLevel != 'NONE') {
    OverrideLoggingThreshold = new vertexSoap.VertexEnvelope.ApplicationData.MessageLogging.OverrideLoggingThreshold();
    OverrideLoggingThreshold.setThresholdScope('com.vertexinc.tps.common.domain.Transaction');
    selectedLevelType = vertexSoap.LogLevelType.fromValue(constants.detailedLogLevel);
    OverrideLoggingThreshold.setValue(selectedLevelType);

    MessageLogging.getOverrideLoggingThreshold().add(OverrideLoggingThreshold);
  }

  MessageLogging.setReturnLogEntries(true);
  ApplicationData.setMessageLogging(MessageLogging);
  ApplicationData.setSender(sender);

  DeleteRequestType.setTransactionId(transactionId);
  Envelope.setApplicationData(ApplicationData);
  Envelope.setLogin(LoginType);
  Envelope.setAccrualRequestOrAccrualResponseOrAccrualSyncRequest(DeleteRequestType);
  vertexLogger.end(logLocation);

  return Envelope;
}

function createSingleshippingCustomerBlock(service, cart, CustomerType, RequestType, shippingFormMock) {
  // If shippingForm is not null then it is a test call
  if (shippingFormMock) {
    var CustomerTaxRegistrationType = new service.TaxRegistrationType();
    CustomerTaxRegistrationType.setTaxRegistrationNumber(shippingFormMock.taxnumber);
    CustomerTaxRegistrationType.setIsoCountryCode(shippingFormMock.country.value);
    CustomerType.getTaxRegistration().add(CustomerTaxRegistrationType);

    var CustomerCodeType = new service.CustomerCodeType();
    CustomerCodeType.setValue(shippingFormMock.email);
    CustomerType.setCustomerCode(CustomerCodeType);
    RequestType.setCustomer(CustomerType);
  } else {
    var form = null;
    if (session.forms.singleshipping) {
      form = session.forms.singleshipping.shippingAddress;
    } else if (session.forms.shipping) {
      form = session.forms.shipping.shippingAddress;
    }
    // If unregistered customer filled Tax Registration Number on single shipping step
    // and it is not USA
    if (form.addressFields.taxnumber.value && !cart.defaultShipment.shippingAddress.countryCode.value.match(/us|usa/i)) {
      var CustomerTaxRegistrationType = new service.TaxRegistrationType();
      CustomerTaxRegistrationType.setTaxRegistrationNumber(form.addressFields.taxnumber.value);
      CustomerTaxRegistrationType.setIsoCountryCode(cart.defaultShipment.shippingAddress.countryCode.value);
      CustomerType.getTaxRegistration().add(CustomerTaxRegistrationType);
    }

    if (cart.billingAddress) {
      // If registered customer set customerID
      // else email from billing form
      var CustomerCodeType = new service.CustomerCodeType();
      if (session.forms.billing.billingAddress) {
        CustomerCodeType.setValue(session.forms.billing.billingAddress.email.emailAddress.value);
      } else if (session.forms.billing.contactInfoFields) {
        CustomerCodeType.setValue(session.forms.billing.contactInfoFields.email.value);
      }

      if (cart.customer.authenticated && cart.customer.registered) {
        CustomerCodeType.setValue(cart.customer.ID);
      }
      CustomerType.setCustomerCode(CustomerCodeType);
    }
    if (session.forms.singleshipping) {
      if (session.forms.singleshipping.fulfilled.value) {
        RequestType.setCustomer(CustomerType);
      }
    } else if (session.forms.shipping) {
      RequestType.setCustomer(CustomerType);
    }
  }
}

function createMultishippingCustomerBlock(service, cart, item, LineItem, CustomerType, constants) {
  //if (session.forms.multishipping && session.forms.multishipping.addressSelection.fulfilled.value && session.forms.multishipping.shippingOptions.fulfilled.value) {
  if (
    constants.isVATEnabled &&
    item.shipment &&
    item.shipment.shippingAddress &&
    !empty(item.shipment.shippingAddress.custom.taxnumber) &&
    item.shipment.shippingAddress.custom.taxnumber != 'undefined' &&
    item.shipment.shippingAddress.countryCode.value.match(/us|usa/i)
  ) {
    var CustomerTaxRegistrationShipping = new service.TaxRegistrationType();
    CustomerTaxRegistrationShipping.setTaxRegistrationNumber(item.shipment.shippingAddress.custom.taxnumber);
    CustomerTaxRegistrationShipping.setIsoCountryCode(item.shipment.shippingAddress.countryCode.value);
    CustomerType.getTaxRegistration().add(CustomerTaxRegistrationShipping);
  }
  var CustomerDestination = new service.LocationType();
  if (item.shipment && item.shipment.shippingAddress) {
    CustomerDestination.setStreetAddress1(item.shipment.shippingAddress.address1);
    CustomerDestination.setStreetAddress2(item.shipment.shippingAddress.address2);
    CustomerDestination.setCity(getCity(item.shipment.shippingAddress.city));
    CustomerDestination.setPostalCode(item.shipment.shippingAddress.postalCode);
    CustomerDestination.setCountry(item.shipment.shippingAddress.countryCode.value);
  }

  if (cart.billingAddress) {
    var CustomerCodeType = new service.CustomerCodeType();
    if (
      session.forms.billing &&
      session.forms.billing.billingAddress &&
      session.forms.billing.billingAddress.email &&
      session.forms.billing.billingAddress.email.emailAddress
    ) {
      CustomerCodeType.setValue(session.forms.billing.billingAddress.email.emailAddress.value);
    }
    if (cart.customer.authenticated && cart.customer.registered) {
      CustomerCodeType.setValue(cart.customer.ID);
    }
    CustomerType.setCustomerCode(CustomerCodeType);

    var CustomerTaxRegistrationBilling = new service.TaxRegistrationType();
    if (constants.isVATEnabled && cart.billingAddress.custom.taxnumber && cart.billingAddress.countryCode.value.match(/us|usa/i)) {
      CustomerTaxRegistrationBilling.setTaxRegistrationNumber(cart.billingAddress.custom.taxnumber);
      CustomerTaxRegistrationBilling.setIsoCountryCode(cart.billingAddress.countryCode.value);
    }

    var PhysicalLocation = new service.TaxRegistrationType.PhysicalLocation();

    PhysicalLocation.setCity(cart.billingAddress.city);
    PhysicalLocation.setCountry(cart.billingAddress.countryCode.value);
    PhysicalLocation.setPostalCode(cart.billingAddress.postalCode);
    PhysicalLocation.setStreetAddress1(cart.billingAddress.address1);
    CustomerTaxRegistrationBilling.getPhysicalLocation().add(PhysicalLocation);
    CustomerType.getTaxRegistration().add(CustomerTaxRegistrationBilling);
  }
  CustomerType.setDestination(CustomerDestination);
  LineItem.setCustomer(CustomerType);
  //}
}

function getCity(city) {
  var CharMap = {
    á: 'a',
    í: 'i',
    à: 'a',
    â: 'a',
    ä: 'a',
    è: 'e',
    é: 'e',
    ê: 'e',
    ë: 'e',
    î: 'i',
    ï: 'i',
    ô: 'o',
    œ: 'o',
    ù: 'u',
    û: 'u',
    ü: 'u',
    ÿ: 'y',
    À: 'A',
    Â: 'A',
    Ä: 'A',
    È: 'E',
    É: 'E',
    Ê: 'E',
    Ë: 'E',
    Î: 'I',
    Ï: 'I',
    Ô: 'O',
    Œ: 'O',
    Ù: 'U',
    Û: 'U',
    Ü: 'U',
    Ÿ: 'Y'
  };

  if (city) {
    for (key in CharMap) {
      if (city.indexOf(key) > 0) {
        city = city.replace(key, CharMap[key]);
      }
    }
  }
  return city;
}

function createCalculateTaxEnvelope(requestType, cart, constants, MOCK_DATA) {
  var logLocation = moduleName + 'createCalculateTaxEnvelope',
    service = webreferences2.CalculateTax,
    Request = constants.Request[requestType + 'Request'],
    Envelope = new service.VertexEnvelope(),
    LoginType = new service.LoginType(),
    RequestType = new service[Request.type](),
    SellerType = new service.SellerType(),
    LocationTypeSeller = new service.LocationType(),
    LocationTypeSellerAdmin = new service.LocationType(),
    LocationTypeCustomer = new service.LocationType(),
    CustomerType = new service.CustomerType(),
    lineItemCounter = 0,
    ApplicationData,
    MessageLogging,
    selectedLevelType,
    shippingStateCode,
    OverrideLoggingThreshold;

  vertexLogger.begin(logLocation, 'Parameters:', { requestType: requestType, MOCK_DATA: MOCK_DATA });

  if (!empty(constants.TrustedId)) {
    LoginType.setTrustedId(constants.TrustedId);
  } else if (!empty(constants.Username) && !empty(constants.Password)) {
    LoginType.setUserName(constants.Username);
    LoginType.setPassword(constants.Password);
  }

  /**
   *  Envelope
   *    LoginType
   *    InvoiceRequest/QuotationRequest
   *      SellerType
   *        Company
   *        PhysycalOrigin
   *      Customer
   *        Destination
   *      LineItem[]
   *        Product
   *        ExtendedPrice
   */

  RequestType.setDocumentDate(new dw.util.Calendar());
  RequestType.setTransactionType(service.SaleTransactionType.valueOf(constants.TransactionType));

  if (requestType == 'Quotation') {
    RequestType.setDocumentNumber(cart.UUID);
    RequestType.setTransactionId(cart.UUID);
  } else {
    RequestType.setDocumentNumber(cart.orderNo);
    RequestType.setTransactionId(cart.orderNo);
  }

  if (!empty(constants.DeliveryTerms.value)) {
    var DeliveryTermCodeType = service.DeliveryTermCodeType.valueOf(constants.DeliveryTerms.value);
    RequestType.setDeliveryTerm(DeliveryTermCodeType);
  }

  // START OF SELLER TYPE

  if (constants.Seller.city) {
    LocationTypeSeller.setCity(constants.Seller.city);
  }
  if (constants.Seller.address) {
    LocationTypeSeller.setStreetAddress1(constants.Seller.address);
  }
  if (constants.Seller.country) {
    LocationTypeSeller.setCountry(constants.Seller.country);
  }
  if (constants.Seller.mainDvision) {
    LocationTypeSeller.setMainDivision(constants.Seller.mainDvision);
  }
  if (constants.Seller.postalCode) {
    LocationTypeSeller.setPostalCode(constants.Seller.postalCode);
  }

  if (constants.Seller.city || constants.Seller.address || constants.Seller.country || constants.Seller.mainDvision || constants.Seller.postalCode) {
    SellerType.setPhysicalOrigin(LocationTypeSeller);
  }

  SellerType.setCompany(constants.Seller.company);
  SellerType.setDivision(constants.Seller.mainDvision);
  // Administrative Origin

  if (constants.SellerAdmin.city) {
    LocationTypeSellerAdmin.setCity(constants.SellerAdmin.city);
  }
  if (constants.SellerAdmin.address) {
    LocationTypeSellerAdmin.setStreetAddress1(constants.SellerAdmin.address);
  }
  if (constants.SellerAdmin.country) {
    LocationTypeSellerAdmin.setCountry(constants.SellerAdmin.country);
  }
  if (constants.SellerAdmin.mainDvision) {
    LocationTypeSellerAdmin.setMainDivision(constants.SellerAdmin.mainDvision);
  }
  if (constants.SellerAdmin.postalCode) {
    LocationTypeSellerAdmin.setPostalCode(constants.SellerAdmin.postalCode);
  }

  if (
    constants.SellerAdmin.city ||
    constants.SellerAdmin.address ||
    constants.SellerAdmin.country ||
    constants.SellerAdmin.mainDvision ||
    constants.SellerAdmin.postalCode
  ) {
    SellerType.setAdministrativeOrigin(LocationTypeSellerAdmin);
  } else {
    SellerType.setAdministrativeOrigin(LocationTypeSeller);
  }

  if (constants.isVATEnabled && !constants.Vertex_ISOCountryCode.match(/us|usa/i)) {
    var SellerTaxRegistrationType = new service.TaxRegistrationType();
    SellerTaxRegistrationType.setTaxRegistrationNumber(constants.Vertex_TaxRegistrationNumber);
    SellerTaxRegistrationType.setIsoCountryCode(constants.Vertex_ISOCountryCode);
    SellerType.getTaxRegistration().add(SellerTaxRegistrationType);
  }

  RequestType.setSeller(SellerType);
  // END OF SELLER TYPE

  // START OF CUSTOMER TYPE
  var CustomerType = new service.CustomerType();

  // MOCK_DATA needed for the Health Test JOB
  let shippingForm = MOCK_DATA;

  var forms = session.forms;
  var customerDefaultAddr;
  if (!empty(cart) && !empty(cart.defaultShipment) && !empty(cart.defaultShipment.shippingAddress)) {
    customerDefaultAddr = cart.defaultShipment.shippingAddress;
  }

  if (!empty(forms)) {
    if (forms.singleshipping) {
      // Check if singleshipping step passed then we have shipping data
      if (forms.singleshipping && forms.singleshipping.fulfilled.value) {
        shippingForm = forms.singleshipping.shippingAddress.addressFields;
      }
    } else if (forms.shippingAddress) {
      shippingForm = forms.shippingAddress.addressFields;
    } else if (forms.shipping) {
      shippingForm = forms.shipping.shippingAddress.addressFields;
    }
  }

  // MOCK_DATA needed for the Health Test JOB
  if (shippingForm) {
    var LocationTypeCustomer = new service.LocationType();
    LocationTypeCustomer.setStreetAddress1(!empty(customerDefaultAddr) ? customerDefaultAddr.address1 : shippingForm.address1.value);
    LocationTypeCustomer.setStreetAddress2(!empty(customerDefaultAddr) ? customerDefaultAddr.address2 : shippingForm.address2.value);

    var city = !empty(customerDefaultAddr) ? customerDefaultAddr.city : shippingForm.city.value;

    LocationTypeCustomer.setCity(getCity(city));
    LocationTypeCustomer.setCountry(!empty(customerDefaultAddr) ? customerDefaultAddr.countryCode.value : shippingForm.country.selectedOption.value);

    if (!empty(customerDefaultAddr)) {
      LocationTypeCustomer.setMainDivision(customerDefaultAddr.stateCode);
      shippingStateCode = customerDefaultAddr.stateCode;
    } else {
      if (shippingForm.states.state) {
        LocationTypeCustomer.setMainDivision(shippingForm.states.state.htmlValue);
        shippingStateCode = shippingForm.states.state.htmlValue;
      } else if (shippingForm.states.stateCode) {
        LocationTypeCustomer.setMainDivision(shippingForm.states.stateCode.htmlValue);
        shippingStateCode = shippingForm.states.stateCode.htmlValue;
      }
    }

    if (!empty(customerDefaultAddr)) {
      LocationTypeCustomer.setPostalCode(customerDefaultAddr.postalCode);
    } else {
      if (shippingForm.postal) {
        LocationTypeCustomer.setPostalCode(shippingForm.postal.value);
      } else if (shippingForm.postalCode) {
        LocationTypeCustomer.setPostalCode(shippingForm.postalCode.htmlValue);
      }
    }

    CustomerType.setDestination(LocationTypeCustomer);
    RequestType.setCustomer(CustomerType);
  }

  // Create Customer Block for VAT Singleshipping
  // if it's a test call we skip this conditions
  if (constants.isVATEnabled) {
    vertexLogger.debug(logLocation, 'Preparing customer VAT Data. isVATEnabled: true, MOCK_DATA: empty');
    var CurrencyType = new service.CurrencyType();
    CurrencyType.setIsoCurrencyCodeAlpha(cart.currencyCode);
    RequestType.setCurrency(CurrencyType);

    createSingleshippingCustomerBlock(service, cart, CustomerType, RequestType, MOCK_DATA);
  }
  // END OF CUSTOMER TYPE

  // START OF LineItemType

  let lineItems = cart.getAllLineItems().iterator(),
    items = [];
  // GIFT WRAP Prorated price Adjustment
  var giftWrapAdjustment;
  if (!cart.getPriceAdjustments().empty) {
    var basketPriceAdjustments = cart.getPriceAdjustments();
    for (var i = 0; i < basketPriceAdjustments.length; i++) {
      var pa = basketPriceAdjustments[i];
      if (pa.promotionID === 'GiftOptions') {
        giftWrapAdjustment = pa;
        break;
      }
    }
  }

  while (lineItems.hasNext()) {
    var product,
      LineItem,
      MeasureType,
      ProductType,
      productClass,
      objectFactory,
      discountObj,
      shipment,
      shipmentPrice,
      item = lineItems.next(),
      itemClass = item.constructor.name;

    switch (itemClass) {
      case 'dw.order.ProductLineItem':
        product = item;
        LineItem = new service[Request.lineItem]();
        CustomerType = new service.CustomerType();

        MeasureType = new service.MeasureType();
        MeasureType.setValue(new dw.util.Decimal(product.quantity.value));

        ProductType = new service.Product();
        productClass = VertexHelper.getProductClass(product);
        ProductType.setProductClass(productClass);
        ProductType.setValue(product.optionProductLineItem ? product.optionID : product.product.ID);

        var Discount = new service.Discount();
        var productPrice = product.price.decimalValue;
        LineItem.setLineItemId('PRODUCT??' + (product.optionProductLineItem ? product.optionID : product.product.ID));

        LineItem.setLineItemNumber(++lineItemCounter);
        LineItem.setQuantity(MeasureType);

        createMultishippingCustomerBlock(service, cart, item, LineItem, CustomerType, constants);

        if (productClass) {
          ProductType.setProductClass(productClass);
        }

        LineItem.setProduct(ProductType);

        // Gift Wrap Prarated Amount
        var gwProratedAmount;
        if (giftWrapAdjustment && !empty(giftWrapAdjustment)) {
          var proratedGiftWrapPA = product.proratedPriceAdjustmentPrices;
          if (!empty(proratedGiftWrapPA)) {
            gwProratedAmount = proratedGiftWrapPA.get(giftWrapAdjustment);
          }
        }

        if (productPrice != product.proratedPrice.decimalValue) {
          objectFactory = new service.ObjectFactory();
          var proratedPrice = product.proratedPrice;
          // Make Sure we are not passing the GIFT WRAP Prorated amount for TAX
          if (!empty(gwProratedAmount) && gwProratedAmount.value > 0) {
            proratedPrice = proratedPrice.subtract(gwProratedAmount);
          }
          discountObj = objectFactory.createAmount(productPrice.subtract(proratedPrice.decimalValue));
          Discount.setDiscountPercentOrDiscountAmount(discountObj);
          LineItem.setDiscount(Discount);
          productPrice = proratedPrice.decimalValue;
        }

        LineItem.setFairMarketValue(item.priceValue);
        LineItem.setExtendedPrice(productPrice);

        // Send the shipment ID as ProjectNumber, for the Multishipping purpose
        LineItem.setProjectNumber(product.shipment.ID ? product.shipment.ID : 'null');

        items.push(LineItem);

        // Set ECOFEE Line Item if required.
        var ecoProduct = item.product;
        var ecoPrice = VertexHelper.getEcoFeeCharges(ecoProduct, shippingStateCode);
        if (ecoPrice) {
          var ecoFee = new Money(ecoPrice * item.quantityValue, session.getCurrency().currencyCode);
          LineItem = new service[Request.lineItem]();
          CustomerType = new service.CustomerType();

          MeasureType = new service.MeasureType();
          MeasureType.setValue(new dw.util.Decimal(product.quantity.value));

          ProductType = new service.Product();
          productClass = 'ECO';
          ProductType.setProductClass(productClass);
          ProductType.setValue('ECOFEE');

          var Discount = new service.Discount();
          var productPrice = ecoFee.decimalValue;
          LineItem.setLineItemId('ECO??ECOFEE:' + product.product.ID);

          LineItem.setLineItemNumber(++lineItemCounter);
          LineItem.setQuantity(MeasureType);

          if (productClass) {
            ProductType.setProductClass(productClass);
          }

          LineItem.setProduct(ProductType);

          LineItem.setFairMarketValue(productPrice);
          LineItem.setExtendedPrice(productPrice);
          items.push(LineItem);
        }

        break;
      case 'dw.order.ShippingLineItem':
        var shipments = cart.getShipments().iterator();

        while (shipments.hasNext()) {
          shipment = shipments.next();
          if (shipment.getShippingLineItems().contains(item)) {
            LineItem = new service[Request.lineItem]();
            MeasureType = new service.MeasureType();
            ProductType = new service.Product();
            Discount = new service.Discount();
            CustomerType = new service.CustomerType();

            shipmentPrice = shipment.shippingLineItems[0].price.decimalValue;

            MeasureType.setValue(new dw.util.Decimal(1));
            LineItem.setLineItemId('SHIPPING??' + shipment.shippingLineItems[0].ID);

            LineItem.setLineItemNumber(++lineItemCounter);
            LineItem.setQuantity(MeasureType);
            if (preferences.vertexShippingProductClass) {
              ProductType.setProductClass(preferences.vertexShippingProductClass);
            }
            LineItem.setProduct(ProductType);

            createMultishippingCustomerBlock(service, cart, { shipment: shipment }, LineItem, CustomerType, constants);

            if (shipmentPrice != shipment.shippingLineItems[0].adjustedPrice.decimalValue) {
              objectFactory = new service.ObjectFactory();
              discountObj = objectFactory.createAmount(shipmentPrice.subtract(shipment.shippingLineItems[0].adjustedPrice.decimalValue));
              Discount.setDiscountPercentOrDiscountAmount(discountObj);
              LineItem.setDiscount(Discount);

              shipmentPrice = shipment.shippingLineItems[0].adjustedPrice.decimalValue;
            }

            if (isNaN(shipmentPrice)) {
              break;
            }

            LineItem.setFairMarketValue(shipment.shippingLineItems[0].priceValue);
            LineItem.setExtendedPrice(shipmentPrice);

            // Send the shipment ID as ProjectNumber, for the Multishipping purpose
            LineItem.setProjectNumber(shipment.ID ? shipment.ID : 'null');

            items.push(LineItem);
          }
        }
        break;
      case 'dw.order.ProductShippingLineItem':
        shipment = item;
        LineItem = new service[Request.lineItem]();
        MeasureType = new service.MeasureType();
        ProductType = new service.Product();
        CustomerType = new service.CustomerType();
        Discount = new service.Discount();
        shipmentPrice = shipment.adjustedPrice.decimalValue;

        var num = lineItemCounter++;

        MeasureType.setValue(new dw.util.Decimal(1));

        LineItem.setLineItemId('SHIPPING??PRODUCTLINEITEM:' + num);

        LineItem.setLineItemNumber(num);
        LineItem.setQuantity(MeasureType);
        if (preferences.vertexShippingProductClass) {
          ProductType.setProductClass(preferences.vertexShippingProductClass);
        }
        LineItem.setProduct(ProductType);

        createMultishippingCustomerBlock(service, cart, item, LineItem, CustomerType, constants);

        if (isNaN(shipmentPrice)) {
          break;
        }
        LineItem.setFairMarketValue(item.priceValue);
        LineItem.setExtendedPrice(shipmentPrice);

        // Send the shipment ID as ProjectNumber, for the Multishipping purpose
        LineItem.setProjectNumber(item.shipment.ID ? item.shipment.ID : 'null');

        items.push(LineItem);

        break;
      case 'dw.order.PriceAdjustment':
        var giftWrapItem = !empty(item.promotionID) && item.promotionID === 'GiftOptions' ? item : null;
        if (giftWrapItem) {
          var giftWrapPrice = item.basePrice;
          LineItem = new service[Request.lineItem]();
          CustomerType = new service.CustomerType();

          MeasureType = new service.MeasureType();
          MeasureType.setValue(new dw.util.Decimal(1));

          ProductType = new service.Product();
          productClass = '021';
          ProductType.setProductClass(productClass);
          ProductType.setValue('GIFTWRAP');

          var Discount = new service.Discount();
          var productPrice = giftWrapPrice.decimalValue;
          LineItem.setLineItemId('GIFTWRAP');

          LineItem.setLineItemNumber(++lineItemCounter);
          LineItem.setQuantity(MeasureType);

          if (productClass) {
            ProductType.setProductClass(productClass);
          }

          LineItem.setProduct(ProductType);

          LineItem.setFairMarketValue(productPrice);
          LineItem.setExtendedPrice(productPrice);
          items.push(LineItem);
        }

        break;
      default:
    }
  }

  RequestType.getLineItem().add(items);
  // END OF LineItemType

  // Add request processing detailed logging
  if (constants.detailedLogLevel != 'NONE') {
    ApplicationData = new service.VertexEnvelope.ApplicationData();
    MessageLogging = new service.VertexEnvelope.ApplicationData.MessageLogging();

    OverrideLoggingThreshold = new service.VertexEnvelope.ApplicationData.MessageLogging.OverrideLoggingThreshold();
    OverrideLoggingThreshold.setThresholdScope('com.vertexinc.tps.common.domain.Transaction');
    selectedLevelType = service.LogLevelType.fromValue(constants.detailedLogLevel);
    OverrideLoggingThreshold.setValue(selectedLevelType);

    MessageLogging.getOverrideLoggingThreshold().add(OverrideLoggingThreshold);
    MessageLogging.setReturnLogEntries(true);
    ApplicationData.setMessageLogging(MessageLogging);
    Envelope.setApplicationData(ApplicationData);
  }

  Envelope.setLogin(LoginType);
  Envelope.setAccrualRequestOrAccrualResponseOrAccrualSyncRequest(RequestType);

  vertexLogger.end(logLocation);

  return Envelope;
}

exports.CalculateTax = LocalServiceRegistry.createService('vertex.CalculateTax', {
  /**
   * 'params' object should have the following properties:
   *  CalculateTax operation
   *  {
   *      constants   : vertex constants object,  (REQUIRED)
   *      requestType : 'Invoice' || 'Quotation'  (REQUIRED)
   *      cart        : a cart or mock data       (REQUIRED)
   *      MOCK_DATA   : tax area lookup mock data (OPTIONAL)
   *  }
   *  DeleteTransaction operation
   *  {
   *      constants     : vertex constants object, (REQUIRED)
   *      transactionId : transaction to delete    (REQUIRED)
   *      sender        : sender name              (REQUIRED)
   *  }
   */
  createRequest: function (svc, operation, params) {
    var logLocation = moduleName + 'vertex.CalculateTax.createRequest',
      vertexEnvelope,
      mockData,
      service = webreferences2.CalculateTax;

    vertexLogger.begin(logLocation, 'Parameters:', { operation: operation, params: params });

    if (operation == 'DeleteTransaction') {
      vertexEnvelope = createDeleteTransactionEnvelope(params.transactionId, params.constants, params.sender);
    }

    // requestType, cart, constants, MOCK_DATA

    if (operation == 'CalculateTax') {
      mockData = 'MOCK_DATA' in params ? params.MOCK_DATA : null;
      vertexEnvelope = createCalculateTaxEnvelope(params.requestType, params.cart, params.constants, mockData);
    }

    svc.serviceClient = service.getDefaultService();
    svc.URL = params.constants.CalculateTaxURL;

    vertexLogger.trace(logLocation, ' Request Data:', { URL: params.constants.CalculateTaxURL, Envelope: vertexEnvelope.toString() });
    vertexLogger.end(logLocation);

    return vertexEnvelope;
  },

  execute: function (svc, Envelope) {
    vertexLogger.debug('vertex.CalculateTax.execute', ' - executing service call');
    return svc.serviceClient.calculateTax80(Envelope);
  },

  parseResponse: function (svc, Envelope) {
    vertexLogger.debug('vertex.CalculateTax.parseResponse', ' - parsing response');
    return Envelope.getAccrualRequestOrAccrualResponseOrAccrualSyncRequest();
  },

  filterLogMessage: function (logMsg) {
    return vertexLogger.jsonHideSensitiveInfo(logMsg);
  }
});

exports.LookupTaxAreas = LocalServiceRegistry.createService('vertex.LookupTaxAreas', {
  createRequest: function (svc, form, constants) {
    let logLocation = 'vertex.LookupTaxAreas.createRequest',
      service = webreferences2.LookupTaxAreas,
      Envelope = new service.VertexEnvelope(),
      TaxAreaLookupType = new service.TaxAreaLookupType(),
      TaxAreaRequestType = new service.TaxAreaRequestType(),
      PostalAddressType = new service.PostalAddressType(),
      LoginType = new service.LoginType(),
      ApplicationData,
      MessageLogging,
      selectedLevelType,
      OverrideLoggingThreshold;

    vertexLogger.begin(logLocation, 'Address form:', { form: form });

    if (!empty(constants.TrustedId)) {
      LoginType.setTrustedId(constants.TrustedId);
    } else if (!empty(constants.Username) && !empty(constants.Password)) {
      LoginType.setUserName(constants.Username);
      LoginType.setPassword(constants.Password);
    }

    /**
     * Envelope
     *    LoginType
     *    TaxAreaRequestType
     *        PostalAddressType
     */
    TaxAreaLookupType.setAsOfDate(new dw.util.Calendar());

    PostalAddressType.setStreetAddress1(form.address1.value);

    if (!empty(form.address2.value)) {
      PostalAddressType.setStreetAddress2(form.address2.value);
    }

    PostalAddressType.setCity(form.city.value);
    if (form.states.state) {
      PostalAddressType.setMainDivision(form.states.state.value);
    } else if (form.states.stateCode) {
      PostalAddressType.setMainDivision(form.states.stateCode.htmlValue);
    }
    if (form.postal) {
      PostalAddressType.setPostalCode(form.postal.value);
    } else if (form.postalCode) {
      PostalAddressType.setPostalCode(form.postalCode.htmlValue);
    }
    PostalAddressType.setCountry(form.country.value);

    TaxAreaLookupType.setTaxAreaIdOrPostalAddressOrExternalJurisdiction(PostalAddressType);

    TaxAreaRequestType.setTaxAreaLookup(TaxAreaLookupType);

    // Add request processing detailed logging
    if (constants.detailedLogLevel != 'NONE') {
      ApplicationData = new service.VertexEnvelope.ApplicationData();
      MessageLogging = new service.VertexEnvelope.ApplicationData.MessageLogging();

      OverrideLoggingThreshold = new service.VertexEnvelope.ApplicationData.MessageLogging.OverrideLoggingThreshold();
      OverrideLoggingThreshold.setThresholdScope('com.vertexinc.tps.common.domain.Transaction');
      selectedLevelType = service.LogLevelType.fromValue(constants.detailedLogLevel);
      OverrideLoggingThreshold.setValue(selectedLevelType);

      MessageLogging.getOverrideLoggingThreshold().add(OverrideLoggingThreshold);
      MessageLogging.setReturnLogEntries(true);
      ApplicationData.setMessageLogging(MessageLogging);
      Envelope.setApplicationData(ApplicationData);
    }

    Envelope.setLogin(LoginType);
    Envelope.setAccrualRequestOrAccrualResponseOrAccrualSyncRequest(TaxAreaRequestType);

    svc.serviceClient = service.getDefaultService();
    svc.URL = constants.LookupTaxAreaURL;

    vertexLogger.trace(logLocation, 'Parameters:', { URL: constants.LookupTaxAreaURL, Envelope: Envelope.toString() });
    vertexLogger.end(logLocation);

    return Envelope;
  },

  execute: function (svc, Envelope) {
    vertexLogger.debug('vertex.LookupTaxAreas.execute', '');
    return svc.serviceClient.lookupTaxAreas80(Envelope);
  },

  parseResponse: function (svc, Envelope) {
    let TaxAreaResponse = Envelope.getAccrualRequestOrAccrualResponseOrAccrualSyncRequest(),
      TaxAreaResultType = TaxAreaResponse.getTaxAreaResult(),
      result = {
        response: 'NORMAL',
        addresses: [],
        message: '',
        taxAreaId: '',
        confidenceIndicator: 100
      };

    vertexLogger.debug('vertex.LookupTaxAreas.parseResponse', '');

    if (!empty(TaxAreaResultType)) {
      if (TaxAreaResultType.length == 1) {
        let status = TaxAreaResultType[0].getStatus();

        result.taxAreaId = TaxAreaResultType[0].taxAreaId.toString();
        result.confidenceIndicator = TaxAreaResultType[0].confidenceIndicator;

        if (!empty(status)) {
          if (status.length == 1) {
            result.response = status[0].lookupResult.toString();
            result.addresses = TaxAreaResultType[0].postalAddress;
          } else if (status.length > 1) {
            for (let i = 0; i < status.length; i++) {
              let s = status[i].lookupResult.toString();

              if (s == 'NORMAL') {
                result.response = 'NORMAL';
                result.addresses = TaxAreaResultType[0].postalAddress;
                break;
              }

              if (s == 'BAD_REGION_FIELDS') {
                result.response = 'BAD_REGION_FIELDS';
                result.message = 'Please enter a valid address';
                break;
              }
            }
          }
        }
      } else if (TaxAreaResultType.length > 1) {
        for (let key in TaxAreaResultType) {
          let TaxArea = TaxAreaResultType[key];

          if (TaxArea && TaxArea.postalAddress) {
            result.addresses.push(TaxArea);
          }
        }
      }
    }

    return result;
  },

  filterLogMessage: function (logMsg) {
    return vertexLogger.jsonHideSensitiveInfo(logMsg);
  }
});
