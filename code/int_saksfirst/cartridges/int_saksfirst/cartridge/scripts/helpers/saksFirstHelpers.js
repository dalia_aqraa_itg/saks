'use strict';
var Transaction = require('dw/system/Transaction');
var saksConstants = require('*/cartridge/scripts/saksConstants.js');
var SaksLogger = require('dw/system/Logger').getLogger('SaksFirst', 'SaksFirstService');

var addServiceHeaders = function (service) {
  service.addHeader('x-api-key', saksConstants.API_KEY);
  service.addHeader('x-apigw-api-id', saksConstants.API_KEY_GW);
  return service;
};

var parseAccountSummaryResponse = function (response, profile) {
  try {
    if (response && profile) {
      var parseResponse = JSON.parse(response);
      if (parseResponse && parseResponse.response && parseResponse.response.results && parseResponse.response.results.member_info) {
        var memberInfo = parseResponse.response.results.member_info;
        Transaction.wrap(function () {
          if (memberInfo.linked && memberInfo.linked == true) {
            profile.custom.saksFirstStatus = response;
            profile.custom.saksFirstLinked = true;
            profile.custom.hasLoggedOn = true; //Changes for SFSX-2725 - Updating the has logged on flag to true
            if (memberInfo.tier_status && memberInfo.tier_status != '') {
              profile.custom.saksFirstMembershipType = memberInfo.tier_status;
            }
          } else {
            // Make Sure to remove all the previously saved Saks First Data
            delete profile.custom.saksFirstStatus;
            delete profile.custom.saksFirstMembershipType;
            profile.custom.saksFirstLinked = false;
          }
        });
      }
    }
  } catch (e) {
    SaksLogger.error('There was an error while parsing the saks first account summary response: ' + e.message);
  }
};

var getSaksFirstMemberInfo = function (profile) {
  try {
    var saksFirstProfile = {};
    if (profile.custom.saksFirstLinked && 'saksFirstStatus' in profile.custom && profile.custom.saksFirstStatus) {
      var saksFirstResponse = JSON.parse(profile.custom.saksFirstStatus);
      if (saksFirstResponse && saksFirstResponse.response && saksFirstResponse.response.results && saksFirstResponse.response.results.member_info) {
        var memberInfo = saksFirstResponse.response.results.member_info;
        saksFirstProfile.linked = memberInfo.linked;
        saksFirstProfile.availablePoints = memberInfo.available_points;
        saksFirstProfile.tierStatus = memberInfo.tier_status;
        saksFirstProfile.unenrolledTier = false;
        if (memberInfo.tier_status && memberInfo.tier_status.toLowerCase() == 'unenrolled') {
          saksFirstProfile.unenrolledTier = true;
        }
        saksFirstProfile.loyalty_id = memberInfo.loyalty_id;
        saksFirstProfile.tierStatusTooltip = 'saksfirst-point-multiplier-tooltip-content-' + memberInfo.tier_status;
        if (memberInfo.gift_card_amount && !empty(memberInfo.gift_card_amount)) {
          saksFirstProfile.giftcardAmount = memberInfo.gift_card_amount;
          var giftcardDollerAmount = memberInfo.gift_card_amount.replace(/\$/g, '');
          saksFirstProfile.giftcardDollerAmount = parseInt(giftcardDollerAmount, 10);
        } else {
          saksFirstProfile.giftcardAmount = '$0';
          saksFirstProfile.giftcardDollerAmount = 0;
        }

        saksFirstProfile.nextGiftCardAmount = memberInfo.next_gift_card_amount;
        saksFirstProfile.pointsToNextReward = memberInfo.points_to_next_reward;
        saksFirstProfile.redeemablePoints = memberInfo.redeemable_points;
        saksFirstProfile.pointsMultiplier = memberInfo.points_multiplier;

        // Progress Bar Logic
        if (
          memberInfo.available_points &&
          !empty(memberInfo.available_points) &&
          memberInfo.points_to_next_reward &&
          !empty(memberInfo.points_to_next_reward)
        ) {
          var currentPoint = parseInt(memberInfo.available_points.replace(/,/g, ''), 10);
          var progressBarEnd = currentPoint + parseInt(memberInfo.points_to_next_reward, 10);
          var giftcardAmountInt = 0;
          if (memberInfo.gift_card_amount && !empty(memberInfo.gift_card_amount)) {
            giftcardAmountInt = memberInfo.gift_card_amount.replace(/\$/g, '');
            giftcardAmountInt = parseInt(giftcardAmountInt, 10);
          }
          if (!isNaN(giftcardAmountInt) && !isNaN(progressBarEnd) && !isNaN(currentPoint)) {
            var progressBarStart = giftcardAmountInt * 100;
            saksFirstProfile.progressBarStart = progressBarStart.toFixed();
            saksFirstProfile.progressBarEnd = progressBarEnd.toFixed();
            saksFirstProfile.progressBarCurrent = currentPoint.toFixed();
            saksFirstProfile.progressBarCompleted = ((currentPoint / progressBarEnd) * 100).toFixed();
          }
        }
      }
      // User Info
      if (saksFirstResponse && saksFirstResponse.response && saksFirstResponse.response.results && saksFirstResponse.response.results.user_loyalty_info) {
        var userInfo = saksFirstResponse.response.results.user_loyalty_info;
        var userInfoObj = {};
        if (userInfo) {
          if (userInfo.first_name && !empty(userInfo.first_name)) {
            userInfoObj.firstName = userInfo.first_name;
          }
          if (userInfo.last_name && !empty(userInfo.last_name)) {
            userInfoObj.lastName = userInfo.last_name;
          }
          if (userInfo.line_one && !empty(userInfo.line_one)) {
            userInfoObj.address1 = userInfo.line_one;
          }
          if (userInfo.line_two && !empty(userInfo.line_two)) {
            userInfoObj.address2 = userInfo.line_two;
          }
          if (userInfo.city && !empty(userInfo.city)) {
            userInfoObj.city = userInfo.city;
          }
          if (userInfo.state && !empty(userInfo.state)) {
            userInfoObj.state = userInfo.state;
          }
          if (userInfo.zip && !empty(userInfo.zip)) {
            userInfoObj.zip = userInfo.zip;
          }
        }
        saksFirstProfile.userInfo = userInfoObj;
      }

      if (saksFirstResponse && saksFirstResponse.response && saksFirstResponse.response.results && saksFirstResponse.response.results.beauty) {
        var beautyBoxInfo = saksFirstResponse.response.results.beauty;
        saksFirstProfile.beautyBoxStatus = beautyBoxInfo.status;
        var boxes = [];
        if (beautyBoxInfo.boxes && beautyBoxInfo.boxes.length > 0) {
          beautyBoxInfo.boxes.forEach(function (box) {
            var boxObj = {};
            boxObj.reward = box.reward;
            boxObj.redemptionCode = box.redemption_code;
            boxObj.pin = box.pin;
            boxObj.status = box.status;
            boxes.push(boxObj);
          });
        }
        saksFirstProfile.boxes = boxes;
      }
    }
    return saksFirstProfile;
  } catch (e) {
    SaksLogger.error('There was an error while fetching saved saks first response: ' + e.message);
    return null;
  }
};

function saksFirstCheckoutView(profile, currentBasket) {
  try {
    var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
    var collections = require('*/cartridge/scripts/util/collections');
    var Money = require('dw/value/Money');
    var result = {};
    var saksFirstInfo = getSaksFirstMemberInfo(profile);
    if (currentBasket && saksFirstInfo && saksFirstInfo.giftcardDollerAmount && saksFirstInfo.giftcardDollerAmount > 0) {
      result.saksFirstInfo = saksFirstInfo;
      result.totalGiftCardAmount = saksFirstInfo.giftcardDollerAmount;
      result.unenrolledTier = false;
      if (saksFirstInfo.tier_status && saksFirstInfo.tier_status.toLowerCase() == 'unenrolled') {
        result.unenrolledTier = true;
      }
      var currencyCode = currentBasket.getCurrencyCode();
      var giftCardsTotal = new Money(0.0, currencyCode);
      var saksGCpaymentInstruments = currentBasket.getPaymentInstruments(ipaConstants.SAKS_GIFT_CARD);
      // add all the gift card applied totals
      if (saksGCpaymentInstruments) {
        collections.forEach(saksGCpaymentInstruments, function (paymentInstrument) {
          giftCardsTotal = giftCardsTotal.add(paymentInstrument.getPaymentTransaction().getAmount());
        });
      }
      if (giftCardsTotal.value > 0) {
        result.applied = true;
        result.appliedAmount = giftCardsTotal.value;
        if (giftCardsTotal.value == saksFirstInfo.giftcardDollerAmount) {
          result.allApplied = true;
        } else {
          result.allApplied = false;
        }
      } else {
        result.applied = false;
      }
      return result;
    }
    return null;
  } catch (e) {
    return null;
  }
}

function isSaksFirstFreeShipApplied(customer) {
  try {
    var preferences = require('*/cartridge/config/preferences');
    var SaksFirstFreeShipApplied = false;
    var saksFirstFreeShipCustomerGroup = preferences.saksFirstFreeShipCustomerGroup;
    var siteCustomerGrp = customer.customerGroups;
    if (saksFirstFreeShipCustomerGroup && siteCustomerGrp.length > 0) {
      for (var i = 0; i < siteCustomerGrp.length; i++) {
        var eachGroup = siteCustomerGrp[i];
        if (saksFirstFreeShipCustomerGroup.indexOf(eachGroup.ID) > -1) {
          SaksFirstFreeShipApplied = true;
          break;
        }
      }
    }
    return SaksFirstFreeShipApplied;
  } catch (e) {
    return false;
  }
}

module.exports = {
  addServiceHeaders: addServiceHeaders,
  parseAccountSummaryResponse: parseAccountSummaryResponse,
  getSaksFirstMemberInfo: getSaksFirstMemberInfo,
  saksFirstCheckoutView: saksFirstCheckoutView,
  isSaksFirstFreeShipApplied: isSaksFirstFreeShipApplied
};
