var Site = require('dw/system/Site');
var CurrentSite = Site.current.preferences;
var preferences = CurrentSite ? CurrentSite.custom : {};

const constants = {
  SAKS_FIRST_ENABLED: preferences['saksFirstEnabled'] || false,
  API_KEY: preferences['hbcAPIKey'],
  API_KEY_GW: preferences['hbcGWAPIKey'],
  PHYSICAL_GIFT_CARD: 'PHYSICAL_GIFT_CARD',
  ELECTRONIC_GIFT_CARD: 'ELECTRONIC_GIFT_CARD',
  BEAUTY_REDEMPTION_SHIP: 'BEAUTY_REDEMPTION_SHIP',
  BEAUTY_REDEMPTION_PICKUP: 'BEAUTY_REDEMPTION_PICKUP'
};

module.exports = constants;
