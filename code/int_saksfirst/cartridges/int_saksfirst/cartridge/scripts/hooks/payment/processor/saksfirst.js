'use strict';

/**
 * Verifies that Redeemed Saks First gift card information is a valid card. If the information is valid a
 * gift card payment instrument is created
 * @param {dw.order.Basket} basket Current users's basket
 * @param {number} balance - GC Card balance
 * @param {Object} cardNumber - the gift card number entered by user
 * @param {number} pin - GC pin
 * @param {boolean} handlePass - boolean to pass handle or not
 * @return {Object} returns an error object
 */
function Handle(basket, handlePass, saksFirstInfo) {
  var result = {};
  result.error = false;
  if (!handlePass) {
    var currentBasket = basket;
    var arrayHelper = require('*/cartridge/scripts/util/array');
    var Money = require('dw/value/Money');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var Transaction = require('dw/system/Transaction');
    var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
    var preferences = require('*/cartridge/config/preferences');
    var saksFirstHelpers = require('*/cartridge/scripts/helpers/saksFirstHelpers');
    result.error = true;
    var currencyCode = currentBasket.getCurrencyCode();
    var balance = new Money(0.0, currencyCode);
    var profile = customer.profile;
    if (saksFirstInfo && (!saksFirstInfo.giftcardDollerAmount || saksFirstInfo.giftcardDollerAmount == 0)) {
      result.error = true;
      return result;
    }

    var paypalPaymentInstrument = basket.getPaymentInstruments(ipaConstants.PAYPAL);
    arrayHelper.find(paypalPaymentInstrument, function (payPalInstr) {
      Transaction.wrap(function () {
        currentBasket.removePaymentInstrument(payPalInstr);
      });
    });

    var ccPaymentInstruments = currentBasket.getPaymentInstruments(ipaConstants.CREDIT_CARD_PAYMENT_METHOD);

    var paymentInstruments = currentBasket.getPaymentInstruments();
    var existingPaymentInstrument = arrayHelper.find(paymentInstruments, function (paymentInstrument) {
      return 'SaksGiftCard' in paymentInstrument.custom && paymentInstrument.custom.SaksGiftCard;
    });

    if (!existingPaymentInstrument) {
      var total = COHelpers.getNonGiftCardAmount(currentBasket);
      if (total.remainingAmount > 0 && total.orderTotal > 0) {
        Transaction.wrap(function () {
          var amountToRedeem = saksFirstInfo.giftcardDollerAmount;
          if (total.remainingAmount.value <= saksFirstInfo.giftcardDollerAmount) {
            amountToRedeem = total.remainingAmount.value;
          }
          var paymentInstrument = currentBasket.createPaymentInstrument(ipaConstants.SAKS_GIFT_CARD, new Money(amountToRedeem, currencyCode));
          balance = total.orderTotal.subtract(new Money(amountToRedeem, currencyCode));
          if (total.amountLeft) {
            paymentInstrument.custom.giftBalanceLeft = true;
          } else {
            paymentInstrument.custom.giftBalanceLeft = false;
          }
          result.error = false;
          result.type = 'APPLIED';
          result.amountApplied = paymentInstrument.getPaymentTransaction().getAmount();
        });
      } else {
        result.type = 'NO_CARD_REQUIRED';
      }
    } else {
      result.type = 'CARD_ALREADY_IN_CART';
    }
    // If there are existing Credit Card and applied the Saks Gift card as well, update the credit card amount.

    if (ccPaymentInstruments && balance) {
      arrayHelper.find(ccPaymentInstruments, function (ccPaymentIntr) {
        Transaction.wrap(function () {
          ccPaymentIntr.paymentTransaction.setAmount(balance);
        });
      });
    }
  }
  return result;
}

function RedeemGiftCard(basket, profile) {
  var arrayHelper = require('*/cartridge/scripts/util/array');
  var Money = require('dw/value/Money');
  var HookMgr = require('dw/system/HookMgr');
  var Transaction = require('dw/system/Transaction');
  var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
  var preferences = require('*/cartridge/config/preferences');
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  var collections = require('*/cartridge/scripts/util/collections');
  var saksFirstHelpers = require('*/cartridge/scripts/helpers/saksFirstHelpers');
  try {
    var result = {};
    if (
      preferences.saksFirstEnabled &&
      profile != null &&
      profile.customer.authenticated &&
      'saksFirstLinked' in profile.custom &&
      profile.custom.saksFirstLinked
    ) {
      var saksGCpaymentInstruments = basket.getPaymentInstruments(ipaConstants.SAKS_GIFT_CARD);
      if (saksGCpaymentInstruments) {
        if (HookMgr.hasHook('saksfirst.loyalty.service') && profile) {
          hooksHelper('saksfirst.loyalty.service', 'accountSummary', [profile], require('*/cartridge/scripts/hooks/SaksFirstFacade').accountSummary);
        }
        // Account Info
        var saksFirstInfo = saksFirstHelpers.getSaksFirstMemberInfo(profile);
        if (saksFirstInfo && (!saksFirstInfo.giftcardDollerAmount || saksFirstInfo.giftcardDollerAmount == 0)) {
          /*collections.forEach(saksGCpaymentInstruments, function (paymentInstrument) {
                        basket.removePaymentInstrument(paymentInstrument);
                    });*/
          // If Shopper doesn't have the available GC, Ask Shopper to remove the GIFT Card
          result.error = true;
          return result;
        }
        // Compare the Reward amount with GC Payment Instrument
        collections.forEach(saksGCpaymentInstruments, function (paymentInstrument) {
          var gcPIAmount = saksFirstInfo.giftcardDollerAmount;
          if (HookMgr.hasHook('saksfirst.loyalty.service') && profile) {
            var redeemRes = hooksHelper(
              'saksfirst.loyalty.service',
              'redeemCheckout',
              [profile, gcPIAmount],
              require('*/cartridge/scripts/hooks/SaksFirstFacade').redeemCheckout
            );
          }
        });
      }
    }
    result.error = false;
    return result;
  } catch (e) {
    result.error = true;
    return result;
  }
}

/**
 * Authorizes a payment using a gift card. Customizations may use other processors and custom
 *      logic to authorize GC & card payment.
 * @param {number} orderNumber - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current
 *      payment method
 * @param {Object} params -  request object
 * @return {Object} returns an error object
 */
function Authorize(orderNumber, paymentInstrument, paymentProcessor, params) {
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  return hooksHelper(
    'app.payment.processor.ipa_credit',
    'Authorize',
    [orderNumber, paymentInstrument, paymentProcessor, params],
    require('*/cartridge/scripts/hooks/payment/processor/ipa_credit').Authorize
  );
}
exports.Handle = Handle;
exports.Authorize = Authorize;
exports.RedeemGiftCard = RedeemGiftCard;
