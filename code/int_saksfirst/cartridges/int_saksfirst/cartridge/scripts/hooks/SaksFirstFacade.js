'use strict';

var Site = require('dw/system/Site');
var Resource = require('dw/web/Resource');
var SaksLogger = require('dw/system/Logger').getLogger('SaksFirst', 'SaksFirstService');
var RootLogger = require('dw/system/Logger').getRootLogger();
var Transaction = require('dw/system/Transaction');
var SaksFirstServices = require('*/cartridge/scripts/services/SaksFirstServices');
var SAKSServiceFactory = require('*/cartridge/scripts/services/SAKSServiceFactory');
var saksFirstHelpers = require('*/cartridge/scripts/helpers/saksFirstHelpers');
var saksConstants = require('*/cartridge/scripts/saksConstants.js');

const SAKSERRORS = {
  ValueSysException: Resource.msg('error.saksfirst.server.error', 'saksfirst', null),
  redemption_invalid: Resource.msg('error.saksfirst.redeem.error', 'saksfirst', null),
  'User is already linked': Resource.msg('error.saksfirst.linked.error', 'saksfirst', null),
  'could not link account': Resource.msg('error.saksfirst.server.error', 'saksfirst', null),
  default: Resource.msg('error.saksfirst.server.error', 'saksfirst', null)
};

function accountSummary(profile) {
  if (saksConstants.SAKS_FIRST_ENABLED) {
    if (profile) {
      var customerNumber = profile.customerNo;
      var service = SAKSServiceFactory.getInstance(SaksFirstServices.SAKS_FIRST_ACCOUNT_SUMMARY);
      var response = service.call(customerNumber);

      if (response && response.error === 0 && response.status === 'OK') {
        saksFirstHelpers.parseAccountSummaryResponse(response.object, profile);
        return response.object;
      } else {
        RootLogger.fatal('FATAL: There was an error with the SAKS service call: {0}: ', response.errorMessage);
        SaksLogger.error('ERROR: There was an error with the SAKS service call:' + response.errorMessage);
      }
    }
  }
}

function accountSummaryTest(customerNumber) {
  if (saksConstants.SAKS_FIRST_ENABLED) {
    var service = SAKSServiceFactory.getInstance(SaksFirstServices.SAKS_FIRST_ACCOUNT_SUMMARY);
    var response = service.call(customerNumber);

    if (response.object) {
      return response.object;
    } else {
      return response.errorMessage;
    }
  }
}

function linkReward(profile, zipCode, cardNumber) {
  if (saksConstants.SAKS_FIRST_ENABLED) {
    if (profile.customer.authenticated && profile) {
      var customerNumber = profile.customerNo;
      var service = SAKSServiceFactory.getInstance(SaksFirstServices.SAKS_FIRST_LINK_REWARD);
      var requestObj = {};
      requestObj.zip = zipCode;
      requestObj.rewards_card_number = cardNumber;
      var response = service.call(requestObj, customerNumber);
      var responseObj = {};
      if (response && response.error === 0 && response.status === 'OK') {
        var response = JSON.parse(response.object);
        if (
          response.response &&
          response.response.results &&
          response.response.results.member_info &&
          response.response.results.member_info.tier_status &&
          response.response.results.member_info.tier_status.toLowerCase() == 'unenrolled'
        ) {
          responseObj.success = false;
          responseObj.errors = SAKSERRORS['default'];
        } else if (response.response && response.response.results && response.response.results.member_info && response.response.results.member_info.linked) {
          responseObj.success = true;
          responseObj.linked = true;
        } else if (response && response.errors && response.errors.length > 0) {
          responseObj.success = false;
          var errorMsg = SAKSERRORS[response.errors[0].error];
          if (!errorMsg) {
            errorMsg = SAKSERRORS['default'];
          }
          responseObj.errors = errorMsg;
        }
        return responseObj;
      } else {
        RootLogger.fatal('FATAL: There was an error with the SAKS service call: {0}: ', response.errorMessage);
        SaksLogger.error('ERROR: There was an error with the SAKS service call:' + response.errorMessage);

        responseObj.success = false;
        var errorMsg = JSON.parse(response.errorMessage);
        if (errorMsg && errorMsg.errors && errorMsg.errors.length > 0) {
          var errorMsg = SAKSERRORS[errorMsg.errors[0].error];
          if (!errorMsg) {
            errorMsg = SAKSERRORS['default'];
          }
          responseObj.errors = errorMsg;
        }
        return responseObj;
      }
    }
  }
}

function linkRewardTest(customerNumber, zipCode, cardNumber) {
  if (saksConstants.SAKS_FIRST_ENABLED) {
    var service = SAKSServiceFactory.getInstance(SaksFirstServices.SAKS_FIRST_LINK_REWARD);
    var requestObj = {};
    requestObj.zip = zipCode;
    requestObj.rewards_card_number = cardNumber;
    var response = service.call(requestObj, customerNumber);
    if (response.object) {
      return response.object;
    } else {
      return response.errorMessage;
    }
  }
}

function unLinkReward(customerNo, cardNumber) {
  if (saksConstants.SAKS_FIRST_ENABLED) {
    var service = SAKSServiceFactory.getInstance(SaksFirstServices.SAKS_FIRST_UNLINK_REWARD);
    var requestObj = {};
    requestObj.rewards_card_number = cardNumber;
    var response = service.call(requestObj, customerNo);
    if (response.object) {
      return response.object;
    } else {
      return response.errorMessage;
    }
  }
}

function redeemGiftCard(profile, amount, type) {
  if (saksConstants.SAKS_FIRST_ENABLED) {
    if (profile.customer.authenticated && profile) {
      var customerNumber = profile.customerNo;
      var service = SAKSServiceFactory.getInstance(SaksFirstServices.SAKS_FIRST_REDEEM_GIFTCARD);
      var requestObj = {};
      requestObj.email = profile.email;
      if (type && type == 'EMAIL') {
        requestObj.redemption_type = saksConstants.ELECTRONIC_GIFT_CARD;
      } else {
        requestObj.redemption_type = saksConstants.PHYSICAL_GIFT_CARD;
      }
      requestObj.award_amount = amount;
      requestObj.email_last_updated_date = null;
      requestObj.rewards_card_number = null;
      var response = service.call(requestObj, customerNumber);
      var responseObj = {};
      if (response && response.error === 0 && response.status === 'OK') {
        var response = JSON.parse(response.object);
        if (response.response && response.response.results && response.response.results.member_info && response.response.results.member_info.linked) {
          responseObj.success = true;
        } else if (response && response.errors && response.errors.length > 0) {
          responseObj.success = false;
          var errorMsg = SAKSERRORS[response.errors[0].error];
          if (!errorMsg) {
            errorMsg = SAKSERRORS['default'];
          }
          responseObj.errors = errorMsg;
        }
        return responseObj;
      } else {
        RootLogger.fatal('FATAL: There was an error with the SAKS service call: {0}: ', response.errorMessage);
        SaksLogger.error('ERROR: There was an error with the SAKS service call:' + response.errorMessage);
        responseObj.success = false;
        var errorMsg = JSON.parse(response.errorMessage);
        if (errorMsg && errorMsg.errors && errorMsg.errors.length > 0) {
          var errorMsg = SAKSERRORS[errorMsg.errors[0].error];
          if (!errorMsg) {
            errorMsg = SAKSERRORS['default'];
          }
          responseObj.errors = errorMsg;
        }
        return responseObj;
      }
    }
  }
}

function redeemBeautyBox(profile, saksFirstInfo, type) {
  if (saksConstants.SAKS_FIRST_ENABLED) {
    if (profile.customer.authenticated && profile && saksFirstInfo.boxes.length > 0) {
      var customerNumber = profile.customerNo;
      var service = SAKSServiceFactory.getInstance(SaksFirstServices.SAKS_FIRST_REDEEM_BEAUTYBOX);
      var requestObj = {};
      if (type && type == 'PICKUP') {
        requestObj.redemption_type = saksConstants.BEAUTY_REDEMPTION_PICKUP;
      } else {
        requestObj.redemption_type = saksConstants.BEAUTY_REDEMPTION_SHIP;
      }
      var boxesArr = [];
      saksFirstInfo.boxes.forEach(function (item) {
        if (item.status && item.status.toLowerCase() == 'active') {
          var box = {};
          box.reward = item.reward;
          box.redemption_code = item.redemptionCode;
          box.pin = item.pin;
          box.status = item.status;

          boxesArr.push(box);
        }
      });
      requestObj.boxes = boxesArr;
      requestObj.customer_email = profile.email;
      //requestObj.rewards_card_number = cardNumber;

      var response = service.call(requestObj, customerNumber);
      var responseObj = {};
      if (response && response.error === 0 && response.status === 'OK') {
        responseObj.success = true;
        return responseObj;
      } else {
        RootLogger.fatal('FATAL: There was an error with the SAKS service call: {0}: ', response.errorMessage);
        SaksLogger.error('ERROR: There was an error with the SAKS service call:' + response.errorMessage);
        responseObj.success = false;
        var errorMsg = JSON.parse(response.errorMessage);
        if (errorMsg && errorMsg.errors && errorMsg.errors.length > 0) {
          var errorMsg = SAKSERRORS[errorMsg.errors[0].error];
          if (!errorMsg) {
            errorMsg = SAKSERRORS['default'];
          }
          responseObj.errors = errorMsg;
        }
        return responseObj;
      }
    }
  }
}

function redeemCheckout(profile, amount) {
  if (saksConstants.SAKS_FIRST_ENABLED) {
    if (profile.customer.authenticated && profile) {
      var customerNumber = profile.customerNo;
      var service = SAKSServiceFactory.getInstance(SaksFirstServices.SAKS_FIRST_REDEEM_CHECKOUT);
      var requestObj = {};
      requestObj.email = profile.email;
      requestObj.award_amount = amount;
      var response = service.call(requestObj, customerNumber);
      var responseObj = {};
      if (response && response.error === 0 && response.status === 'OK') {
        var response = JSON.parse(response.object);
        if (response.response && response.response.results && response.response.results.member_info && response.response.results.member_info.linked) {
          responseObj.success = true;
        } else if (response && response.errors && response.errors.length > 0) {
          responseObj.success = false;
          responseObj.errors = [Resource.msg('error.saksfirst.gc.redeem.card', 'saksfirst', null)];
        }
        return responseObj;
      } else {
        RootLogger.fatal('FATAL: There was an error with the SAKS service call: {0}: ', response.errorMessage);
        SaksLogger.error('ERROR: There was an error with the SAKS service call:' + response.errorMessage);
        responseObj.success = false;
        var errorMsg = JSON.parse(response.errorMessage);
        if (errorMsg && errorMsg.errors && errorMsg.errors.length > 0) {
          responseObj.errors = [Resource.msg('error.saksfirst.gc.redeem.card', 'saksfirst', null)];
        }
        return responseObj;
      }
    }
  }
}

module.exports = {
  accountSummary: accountSummary,
  linkReward: linkReward,
  unLinkReward: unLinkReward,
  redeemGiftCard: redeemGiftCard,
  redeemBeautyBox: redeemBeautyBox,
  redeemCheckout: redeemCheckout,
  linkRewardTest: linkRewardTest,
  accountSummaryTest: accountSummaryTest
};
