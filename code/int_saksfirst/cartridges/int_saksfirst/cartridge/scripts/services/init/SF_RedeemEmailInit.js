var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var SaksFirstServices = require('*/cartridge/scripts/services/SaksFirstServices');
var saksFirstHelpers = require('*/cartridge/scripts/helpers/saksFirstHelpers');

module.exports = {
  getInstance: function () {
    return LocalServiceRegistry.createService(SaksFirstServices.SAKS_FIRST_REDEEM_EMAIL, {
      createRequest: function (svc, args, customerNumber) {
        svc = svc.setRequestMethod('POST');
        svc = saksFirstHelpers.addServiceHeaders(svc);
        var serviceConfig = svc.getConfiguration();
        var serviceCredentials = serviceConfig.getCredential();
        var url = serviceCredentials.URL + '/' + customerNumber + '/loyalty-program/beauty/redeem';
        svc = svc.setURL(url);
        return JSON.stringify(args);
      },

      parseResponse: function (svc, client) {
        return client.text;
      },

      mockCall: function (svc, client) {
        var mockedReponse = '{"success": "true"}';
        return {
          statusCode: 200,
          statusMessage: 'Success',
          text: mockedReponse
        };
      },
      filterLogMessage: function (res) {
        return res.replace('headers', 'OFFWITHTHEHEADERS');
      }
    });
  }
};
