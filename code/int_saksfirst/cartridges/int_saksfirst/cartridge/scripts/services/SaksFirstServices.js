'use strict';

/**
 * Unique SAKS First service identifiers
 */
const SaksFirstServices = {
  SAKS_FIRST_ACCOUNT_SUMMARY: 'saksfirst.http.account.summary',
  SAKS_FIRST_LINK_REWARD: 'saksfirst.http.link.reward',
  SAKS_FIRST_UNLINK_REWARD: 'saksfirst.http.unlink.reward',
  SAKS_FIRST_REDEEM_GIFTCARD: 'saksfirst.http.redeem.giftcard',
  SAKS_FIRST_REDEEM_BEAUTYBOX: 'saksfirst.http.redeem.beautybox',
  SAKS_FIRST_REDEEM_EMAIL: 'saksfirst.http.beautybox.email',
  SAKS_FIRST_REDEEM_CHECKOUT: 'saksfirst.http.redeem.checkout'
};

module.exports = SaksFirstServices;
