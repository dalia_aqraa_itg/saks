'use strict';

const SaksFirstServices = require('*/cartridge/scripts/services/SaksFirstServices');

/**
 * Saks First Services
 */
const SF_AccountSummaryInit = require('*/cartridge/scripts/services/init/SF_AccountSummaryInit');
const SF_LinkRewardInit = require('*/cartridge/scripts/services/init/SF_LinkRewardInit');
const SF_UnlinkRewardInit = require('*/cartridge/scripts/services/init/SF_UnlinkRewardInit');
const SF_RedeemGiftCardInit = require('*/cartridge/scripts/services/init/SF_RedeemGiftCardInit');
const SF_RedeemBeautyBoxInit = require('*/cartridge/scripts/services/init/SF_RedeemBeautyBoxInit');
const SF_RedeemCheckoutInit = require('*/cartridge/scripts/services/init/SF_RedeemCheckoutInit');
const SF_RedeemEmailInit = require('*/cartridge/scripts/services/init/SF_RedeemEmailInit');

/**
 * Using the serviceName return an instance of the Service.
 * @param serviceName
 * @returns Instance of Service
 */
function _getServiceBuilder(serviceName) {
  switch (serviceName) {
    case SaksFirstServices.SAKS_FIRST_ACCOUNT_SUMMARY:
      return SF_AccountSummaryInit;
    case SaksFirstServices.SAKS_FIRST_LINK_REWARD:
      return SF_LinkRewardInit;
    case SaksFirstServices.SAKS_FIRST_UNLINK_REWARD:
      return SF_UnlinkRewardInit;
    case SaksFirstServices.SAKS_FIRST_REDEEM_GIFTCARD:
      return SF_RedeemGiftCardInit;
    case SaksFirstServices.SAKS_FIRST_REDEEM_BEAUTYBOX:
      return SF_RedeemBeautyBoxInit;
    case SaksFirstServices.SAKS_FIRST_REDEEM_EMAIL:
      return SF_RedeemEmailInit;
    case SaksFirstServices.SAKS_FIRST_REDEEM_CHECKOUT:
      return SF_RedeemCheckoutInit;
    default:
      throw new Error('SAKS First Service ID not recognized: ' + serviceName);
  }
}

exports.getInstance = function (serviceName) {
  const builder = _getServiceBuilder(serviceName);
  return builder.getInstance();
};
