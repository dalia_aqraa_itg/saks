'use strict';

var server = require('server');

var HookMgr = require('dw/system/HookMgr');
var Logger = require('dw/system/Logger');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var SaksFirstFacade = require('*/cartridge/scripts/hooks/SaksFirstFacade');
var saksFirstHelpers = require('*/cartridge/scripts/helpers/saksFirstHelpers');
var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');

/**
 * Creates a token. This should be replaced by utilizing a tokenization provider
 * @param {Object} encryptedCard - creating encryptedCard
 * @returns {string} token - a token
 */
function createToken(encryptedCard) {
  var TokenExFacade = require('*/cartridge/scripts/services/TokenExFacade');
  var token = TokenExFacade.tokenizeEncryptedCard(encryptedCard);
  return token;
}

/**
 * Creates an object from form values
 * @param {Object} paymentForm - form object
 * @returns {Object} a plain object of payment instrument
 */
function getSaksFirstObject(saksfirstform) {
  return {
    name: saksfirstform.cardOwner.value,
    cardNumber: saksfirstform.cardNumber.value,
    cardType: saksfirstform.cardType.value,
    zipCode: saksfirstform.postalCode.value,
    saksfirstform: saksfirstform
  };
}

server.get('Start', csrfProtection.generateToken, consentTracking.consent, userLoggedIn.validateLoggedIn, function (req, res, next) {
  var preferences = require('*/cartridge/config/preferences');
  var URLUtils = require('dw/web/URLUtils');
  var saksFirstInfo;
  var profile = req.currentCustomer.raw.profile;
  if (profile && preferences.saksFirstEnabled) {
    var saksfirstform = server.forms.getForm('saksfirst');
    saksfirstform.clear();
    saksFirstInfo = saksFirstHelpers.getSaksFirstMemberInfo(profile);
    // TokenEx API
    var tokenEnabled = preferences.enableTokenEx;
    var tokenPublicKey = preferences.tokenExPublicKey;
    res.setViewData({
      tokenEnabled: tokenEnabled,
      tokenPublicKey: tokenPublicKey
    });
    res.render('saksfirst/saksfirstLink', {
      includeRecaptchaJS: true,
      saksfirstform: saksfirstform,
      saksFirstInfo: saksFirstInfo,
      profile: profile,
      saksFirstLinked: 'saksFirstLinked' in profile.custom && profile.custom.saksFirstLinked ? true : false,
      pageType: 'saks-first',
      disableElectronicRedemption: preferences.disableElectronicRedemption,
      disableBeautyBoxPickRedemption: preferences.disableBeautyBoxPickRedemption,
      disableSaksFirstPointRedemption: preferences.disableSaksFirstPointRedemption,
      disableSaksFirstBeautyRewardRedemption: preferences.disableSaksFirstBeautyRewardRedemption,
      isCanadianCustomer: 'isCanadianCustomer' in profile.custom && profile.custom.isCanadianCustomer && tsysHelper.isTsysMode() ? true : false
    });
  } else {
    res.redirect(URLUtils.url('Account-Show'));
  }

  next();
});

server.get('PDP', function (req, res, next) {
  var preferences = require('*/cartridge/config/preferences');
  var saksFirstInfo = {};
  var profile = req.currentCustomer.raw.profile;
  if (profile && profile.customer.authenticated && preferences.saksFirstEnabled) {
    saksFirstInfo = saksFirstHelpers.getSaksFirstMemberInfo(profile);
  } else {
    saksFirstInfo.guest = true;
  }
  res.render('saksfirst/pdptooltip', {
    saksFirst: saksFirstInfo
  });
  next();
});

server.post('GiftCardRedeem', server.middleware.https, userLoggedIn.validateLoggedIn, function (req, res, next) {
  var formErrors = require('*/cartridge/scripts/formErrors');
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
  var Resource = require('dw/web/Resource');

  var profile = req.currentCustomer.raw.profile;
  if (!'saksFirstLinked ' in profile.custom || !profile.custom.saksFirstLinked) {
    res.json({
      success: false,
      saksError: Resource.msg('label.saksfirst.general.error', 'saksfirst', null)
    });
  }

  var saksfirstform = server.forms.getForm('saksfirst');
  var result = {};
  result.GCRedeemMode = req.form.gcRedeemMode;

  var saksFirstInfo = saksFirstHelpers.getSaksFirstMemberInfo(profile);
  if (!saksFirstInfo || !saksFirstInfo.giftcardDollerAmount || saksFirstInfo.giftcardDollerAmount == 0) {
    res.json({
      success: false,
      saksError: Resource.msg('label.saksfirst.general.error', 'payment', null)
    });
    return next();
  }
  result.giftcardDollerAmount = saksFirstInfo.giftcardDollerAmount;

  res.setViewData(result);
  this.on('route:BeforeComplete', function (req, res) {
    // eslint-disable-line no-shadow
    var URLUtils = require('dw/web/URLUtils');
    var Transaction = require('dw/system/Transaction');

    var formInfo = res.getViewData();

    Transaction.wrap(function () {
      if (HookMgr.hasHook('saksfirst.loyalty.service') && customer.profile) {
        var GCRedeemRes = hooksHelper(
          'saksfirst.loyalty.service',
          'redeemGiftCard',
          [customer.profile, formInfo.giftcardDollerAmount, formInfo.GCRedeemMode],
          require('*/cartridge/scripts/hooks/SaksFirstFacade').redeemGiftCard
        );
        if (GCRedeemRes && GCRedeemRes.success) {
          hooksHelper('saksfirst.loyalty.service', 'accountSummary', [customer.profile], require('*/cartridge/scripts/hooks/SaksFirstFacade').accountSummary);
          if ('saksFirstLinked' in customer.profile.custom && customer.profile.custom.saksFirstLinked) {
            customer.getProfile().custom.lastPaymentModified = new Date().toISOString();
            accountHelpers.updateAccLastModifiedDate(req.currentCustomer.raw);
          }
          res.json({
            success: true,
            amountRedeemed: true
          });
        } else if (GCRedeemRes && !GCRedeemRes.success && GCRedeemRes.errors.length > 0) {
          res.json({
            success: false,
            saksError: GCRedeemRes.errors
          });
        } else {
          res.json({
            success: false,
            redirectUrl: URLUtils.url('Account-Show').toString()
          });
        }
      }
    });
  });
  return next();
});

server.post('BeautyBoxRedeem', server.middleware.https, csrfProtection.validateAjaxAccountRequest, function (req, res, next) {
  var formErrors = require('*/cartridge/scripts/formErrors');
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
  var Resource = require('dw/web/Resource');

  var profile = req.currentCustomer.raw.profile;
  if (!'saksFirstLinked ' in profile.custom || !profile.custom.saksFirstLinked) {
    res.json({
      success: false,
      saksError: Resource.msg('label.saksfirst.general.error', 'saksfirst', null)
    });
  }

  var saksfirstform = server.forms.getForm('saksfirst');
  var result = {};
  result.BBRedeemMode = req.form.bbRedeemMode;

  var saksFirstInfo = saksFirstHelpers.getSaksFirstMemberInfo(profile);
  if (!saksFirstInfo || !saksFirstInfo.boxes || saksFirstInfo.boxes.length == 0) {
    res.json({
      success: false,
      saksError: Resource.msg('error.saksfirst.bb.redeem.card', 'saksfirst', null)
    });
    return next();
  }
  result.saksFirstInfo = saksFirstInfo;

  res.setViewData(result);
  this.on('route:BeforeComplete', function (req, res) {
    // eslint-disable-line no-shadow
    var URLUtils = require('dw/web/URLUtils');
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var Transaction = require('dw/system/Transaction');

    var formInfo = res.getViewData();

    Transaction.wrap(function () {
      var profile = customer.getProfile();
      if (HookMgr.hasHook('saksfirst.loyalty.service') && customer.profile && formInfo.saksFirstInfo.boxes.length > 0) {
        var BBRedeemRes = hooksHelper(
          'saksfirst.loyalty.service',
          'redeemBeautyBox',
          [customer.profile, formInfo.saksFirstInfo, formInfo.BBRedeemMode],
          require('*/cartridge/scripts/hooks/SaksFirstFacade').redeemBeautyBox
        );
        if (BBRedeemRes && BBRedeemRes.success) {
          hooksHelper('saksfirst.loyalty.service', 'accountSummary', [customer.profile], require('*/cartridge/scripts/hooks/SaksFirstFacade').accountSummary);
          if ('saksFirstLinked' in customer.profile.custom && customer.profile.custom.saksFirstLinked) {
            customer.getProfile().custom.lastPaymentModified = new Date().toISOString();
            accountHelpers.updateAccLastModifiedDate(req.currentCustomer.raw);
          }
          res.json({
            success: true,
            bbRedeemed: true
          });
        } else if (BBRedeemRes && !BBRedeemRes.success && BBRedeemRes.errors.length > 0) {
          res.json({
            success: false,
            saksError: BBRedeemRes.errors
          });
        } else {
          res.json({
            success: false,
            redirectUrl: URLUtils.url('Account-Show').toString()
          });
        }
      }
    });
  });
  return next();
});

server.post('Link', server.middleware.https, csrfProtection.validateAjaxAccountRequest, function (req, res, next) {
  var formErrors = require('*/cartridge/scripts/formErrors');
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
  var Resource = require('dw/web/Resource');

  var saksfirstform = server.forms.getForm('saksfirst');
  var result = getSaksFirstObject(saksfirstform);
  var tokenObj;

  // Generate Token
  if (result.cardNumber) {
    tokenObj = createToken(result.cardNumber);
  }
  if (tokenObj.error === true || tokenObj.token.length === 0) {
    res.json({
      success: false,
      saksError: Resource.msg('label.save.tokenex.error', 'payment', null)
    });
    return next();
  }
  // Save TokenEx Token For Luhn Checksum
  result.cardNumber = tokenObj.token;

  if (saksfirstform.valid) {
    res.setViewData(result);
    this.on('route:BeforeComplete', function (req, res) {
      // eslint-disable-line no-shadow
      var URLUtils = require('dw/web/URLUtils');
      var CustomerMgr = require('dw/customer/CustomerMgr');
      var Transaction = require('dw/system/Transaction');
      var dwOrderPaymentInstrument = require('dw/order/PaymentInstrument');

      var formInfo = res.getViewData();

      Transaction.wrap(function () {
        var profile = customer.getProfile();
        if (HookMgr.hasHook('saksfirst.loyalty.service') && customer.profile) {
          var linkRewardRes = hooksHelper(
            'saksfirst.loyalty.service',
            'linkReward',
            [customer.profile, formInfo.zipCode, formInfo.cardNumber],
            require('*/cartridge/scripts/hooks/SaksFirstFacade').linkReward
          );
          if (linkRewardRes && linkRewardRes.success && linkRewardRes.linked) {
            if (HookMgr.hasHook('saksfirst.loyalty.service')) {
              hooksHelper(
                'saksfirst.loyalty.service',
                'accountSummary',
                [customer.profile],
                require('*/cartridge/scripts/hooks/SaksFirstFacade').accountSummary
              );
              if ('saksFirstLinked' in customer.profile.custom && customer.profile.custom.saksFirstLinked) {
                customer.getProfile().custom.lastPaymentModified = new Date().toISOString();
                accountHelpers.updateAccLastModifiedDate(req.currentCustomer.raw);
              }
            }
            res.json({
              success: true,
              redirectUrl: URLUtils.url('SaksFirst-Start', 'linked', 'true').toString()
            });
          } else if (linkRewardRes && !linkRewardRes.success && linkRewardRes.errors.length > 0) {
            res.json({
              success: false,
              saksError: linkRewardRes.errors
            });
          } else {
            res.json({
              success: false,
              redirectUrl: URLUtils.url('Account-Show').toString()
            });
          }
        }
      });
    });
  } else {
    res.json({
      success: false,
      fields: formErrors.getFormErrors(saksfirstform)
    });
  }
  return next();
});

server.post('ApplyGiftCard', server.middleware.https, function (req, res, next) {
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
  var arrayHelper = require('*/cartridge/scripts/util/array');
  var OrderModel = require('*/cartridge/models/order');
  var preferences = require('*/cartridge/config/preferences');
  var Resource = require('dw/web/Resource');
  var BasketMgr = require('dw/order/BasketMgr');
  var Transaction = require('dw/system/Transaction');
  var URLUtils = require('dw/web/URLUtils');
  var currentBasket = BasketMgr.getCurrentBasket();
  if (!currentBasket || preferences.disableSaksFirstAtCheckout) {
    res.setStatusCode(500);
    res.json({
      success: false,
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }

  try {
    // Make Sure Customer is Logged IN And Have Gift Card Amount.

    var profile = req.currentCustomer.raw.profile;
    if (
      preferences.saksFirstEnabled &&
      profile != null &&
      profile.customer.authenticated &&
      'saksFirstLinked' in profile.custom &&
      profile.custom.saksFirstLinked
    ) {
      // Make Sure to update the Current Loyalty State
      if (HookMgr.hasHook('saksfirst.loyalty.service') && profile) {
        hooksHelper('saksfirst.loyalty.service', 'accountSummary', [profile], require('*/cartridge/scripts/hooks/SaksFirstFacade').accountSummary);
      }
      // Account Info
      var saksFirstInfo = saksFirstHelpers.getSaksFirstMemberInfo(profile);
      if (saksFirstInfo && (!saksFirstInfo.giftcardDollerAmount || saksFirstInfo.giftcardDollerAmount == 0)) {
        res.json({
          success: false,
          saksError: Resource.msg('label.checkout.apply.gc.error', 'saksfirst', null)
        });
        return next();
      }

      var error = false;
      var errorMessage = '';
      var addCardResponse;
      var handlePass = false;
      try {
        Transaction.wrap(function () {
          addSaksCardResponse = hooksHelper(
            'app.payment.processor.saksfirst',
            'Handle',
            [currentBasket, handlePass, saksFirstInfo],
            require('*/cartridge/scripts/hooks/payment/processor/saksfirst').Handle
          );
        });
      } catch (e) {
        Logger.error('Saks Gift Card addition error: {0} \n {1}', e.message, e.stack);
        error = true;
      }

      if (error || addSaksCardResponse.error) {
        var errorCodes = {
          CARD_ALREADY_IN_CART: 'error.saks.giftcard.already.in.cart',
          NO_CARD_REQUIRED: 'error.saks.giftcard.cannot.be.applied',
          default: 'saks.giftcard.default.error'
        };
        var errorMessageKey = addCardResponse != null ? errorCodes[addCardResponse.type] : errorCodes.default;
        errorMessage = Resource.msg(errorMessageKey, 'saksfirst', null);
        res.json({
          error: addCardResponse ? addCardResponse.error : error,
          errorMessage: errorMessage
        });
        return next();
      }

      Transaction.wrap(function () {
        basketCalculationHelpers.calculateTotals(currentBasket);
      });

      var total = COHelpers.getNonGiftCardAmount(currentBasket, null);
      var amountFinished;
      if (!total.error) {
        amountFinished = !(total.remainingAmount > 0);
      }
      var basketModel = new OrderModel(currentBasket, {
        containerView: 'basket'
      });
      hasPreOrderItems = basketModel.items.hasPreOrderItems;
      res.json({
        error: error,
        order: basketModel,
        amountFinished: amountFinished,
        hasPreOrderItems: hasPreOrderItems
      });
      return next();
    }
  } catch (e) {
    Logger.error('Gift Card addition error: {0} \n {1}', e.message, e.stack);
    res.setStatusCode(500);
    res.json({
      success: false,
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }
  return next();
});

server.post('RemoveGiftCard', server.middleware.https, function (req, res, next) {
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var arrayHelper = require('*/cartridge/scripts/util/array');
  var OrderModel = require('*/cartridge/models/order');
  var Resource = require('dw/web/Resource');
  var BasketMgr = require('dw/order/BasketMgr');
  var Transaction = require('dw/system/Transaction');
  var URLUtils = require('dw/web/URLUtils');
  var preferences = require('*/cartridge/config/preferences');
  var currentBasket = BasketMgr.getCurrentBasket();

  if (!currentBasket || preferences.disableSaksFirstAtCheckout) {
    res.setStatusCode(500);
    res.json({
      success: false,
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }

  try {
    // Make Sure Customer is Logged IN And Have Gift Card Amount.
    var existingPaymentInstrument;
    var paymentInstruments = currentBasket.getPaymentInstruments();

    if (currentBasket) {
      existingPaymentInstrument = arrayHelper.find(paymentInstruments, function (paymentInstrument) {
        return paymentInstrument.paymentMethod === 'SAKSGiftCard';
      });

      if (existingPaymentInstrument) {
        try {
          Transaction.wrap(function () {
            currentBasket.removePaymentInstrument(existingPaymentInstrument);
            basketCalculationHelpers.calculateTotals(currentBasket);
          });
        } catch (e) {
          Logger.error('SaksFirst Gift Card removal error: {0} \n {1}', e.message, e.stack);
          res.setStatusCode(500);
          res.json({
            error: true,
            errorMessage: Resource.msg('error.cannot.remove.giftcard', 'saksfirst', null)
          });
          return next();
        }
        var basketModel = new OrderModel(currentBasket, {
          containerView: 'basket'
        });

        res.json(basketModel);
        return next();
      }
    }
  } catch (e) {
    Logger.error('SaksFirst Gift Card addition error: {0} \n {1}', e.message, e.stack);
    res.setStatusCode(500);
    res.json({
      success: false,
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }
  return next();
});

server.get('AccountSummary', function (req, res, next) {
  var accountSummary = SaksFirstFacade.accountSummaryTest('3674240');
  res.json(JSON.parse(accountSummary));
  next();
});

server.get('LinkReward', function (req, res, next) {
  var profile = req.currentCustomer.raw.profile;
  var linkReward = SaksFirstFacade.linkRewardTest('0909093', '60750', '6003041695276157');
  res.json(JSON.parse(linkReward));
  next();
});

server.get('UnLinkReward', function (req, res, next) {
  var unLinkReward = SaksFirstFacade.unLinkReward('0909099', '6003042222615449');
  res.json(JSON.parse(unLinkReward));
  next();
});

server.get('GiftCardRedeemm', function (req, res, next) {
  var redeemGiftCard = SaksFirstFacade.redeemGiftCard(req, 5, '6003041475008419');
  res.json(JSON.parse(redeemGiftCard));
  next();
});

server.get('BeautyBoxRedeemm', function (req, res, next) {
  hooksHelper('saksfirst.loyalty.service', 'redeemBeautyBox', [req]);
  res.json({
    success: true
  });
  next();
});

/**
 * Added for CPMT-1703
 */
server.get('Apply', server.middleware.https, userLoggedIn.validateLoggedIn, function (req, res, next) {
  var Resource = require('dw/web/Resource');
  var URLUtils = require('dw/web/URLUtils');
  var Logger = require('dw/system/Logger');
  var capOneUtils = require('*/cartridge/scripts/util/capOneUtils');
  var accRedirectURL;
  var customerProfile = req.currentCustomer.raw.profile;
  var customerIsCanadian = false;

  if (customerProfile !== null && 'isCanadianCustomer' in customerProfile.custom && customerProfile.custom.isCanadianCustomer !== null) {
    customerIsCanadian = customerProfile.custom.isCanadianCustomer;
  }

  if (customerProfile !== null && !customerIsCanadian) {
    var result = capOneUtils.loginCapOnePrefill(req, customer);
    accRedirectURL = result.object.redirectUrl;
    res.json({
      success: true,
      redirectUrl: accRedirectURL
    });
  } else {
    res.redirect(URLUtils.url('SaksFirst-Start'));
  }
  next();
});

module.exports = server.exports();
