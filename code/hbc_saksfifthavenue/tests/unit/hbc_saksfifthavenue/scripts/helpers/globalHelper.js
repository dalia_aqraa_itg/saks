'use strict';

var assert = require('chai').assert;
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();

var specialCharsMock = '1!2<3?4)';
var sanitizedMock = '1234';

describe('replaceSpecialCharsAndEmojis', function () {
  var globalHelper = proxyquire('../../../../../cartridges/app_hbc_saksfifthavenue/cartridge/scripts/helpers/globalHelper.js', {});
  it('does not change sanitized values', function () {
    assert.equal(globalHelper.replaceSpecialCharsAndEmojis(sanitizedMock), sanitizedMock, 'Not equal');
  });
  it('removes special characters', function () {
    assert.equal(globalHelper.replaceSpecialCharsAndEmojis(specialCharsMock), sanitizedMock, 'Not equal');
  });
});
