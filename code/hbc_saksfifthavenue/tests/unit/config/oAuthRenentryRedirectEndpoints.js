'use strict';

var assert = require('chai').assert;
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();

describe('Configurations for Oauth Renentry Redirect Endpoints', function () {
  var oAuthRenentryRedirectEndpoints = proxyquire('../../../cartridges/app_hbc_saksfifthavenue/cartridge/config/oAuthRenentryRedirectEndpoints', {});

  it('Confirm Existing configurations are ok', function () {
    var numberOfProperties = Object.keys(oAuthRenentryRedirectEndpoints).length;
    assert.equal(4, numberOfProperties);
    assert.equal(oAuthRenentryRedirectEndpoints[1], 'Account-Show');
    assert.equal(oAuthRenentryRedirectEndpoints[2], 'Checkout-Begin');
    assert.equal(oAuthRenentryRedirectEndpoints[3], 'Loyalty-RequestPinReset');
    assert.equal(oAuthRenentryRedirectEndpoints[4], 'Cart-Show');
  });
});
