'use strict';

module.exports = {
  1: 'Account-Show',
  2: 'Checkout-Begin',
  3: 'Loyalty-RequestPinReset',
  4: 'Cart-Show'
};
