'use strict';

var base = module.superModule;
var Site = require('dw/system/Site');
var customPreferences = Site.current.preferences.custom;

//mapping of the chargeable amount versus the threshold above which the charge is applied
base.giftPackChargeMapping = 'giftPackChargeMapping' in customPreferences ? JSON.parse(customPreferences.giftPackChargeMapping) : {};

// Pre Order specific Site Preference
base.backinStockPaddinginDays = 'backinStockPaddinginDays' in customPreferences ? customPreferences.backinStockPaddinginDays : null;
base.preOrderEnabled = 'preOrderEnabled' in customPreferences ? customPreferences.preOrderEnabled : false;
// SDD preferences
base.defaultStoreLookUpRadius = 'defaultStoreLookUpRadius' in customPreferences ? customPreferences.defaultStoreLookUpRadius : 10000;
base.zipStoreIdMappingName = 'zipStoreIdMappingName' in customPreferences ? customPreferences.zipStoreIdMappingName : 'ZipStoreData';
base.isEnabledforSameDayDelivery = 'isEnabledforSameDayDelivery' in customPreferences ? customPreferences.isEnabledforSameDayDelivery : false;
base.getitfastRefinementName = 'getitfastRefinementName' in customPreferences ? customPreferences.getitfastRefinementName : '';
base.isQuickViewEnabled = 'isQuickViewEnabled' in customPreferences ? customPreferences.isQuickViewEnabled : false;
base.giftWrapEnabledforBopis = 'giftWrapEnabledforBopis' in customPreferences ? customPreferences.giftWrapEnabledforBopis : false;
base.click2CallCustomerGroupOverride = 'click2CallCustomerGroupOverride' in customPreferences ? customPreferences.click2CallCustomerGroupOverride : null;
base.saksFirstEnabled = 'saksFirstEnabled' in customPreferences ? customPreferences.saksFirstEnabled : false;
base.disableSaksFirstAtCheckout = 'disableSaksFirstAtCheckout' in customPreferences ? customPreferences.disableSaksFirstAtCheckout : true;
base.disableElectronicRedemption = 'disableElectronicRedemption' in customPreferences ? customPreferences.disableElectronicRedemption : true;
base.disableBeautyBoxPickRedemption = 'disableBeautyBoxPickRedemption' in customPreferences ? customPreferences.disableBeautyBoxPickRedemption : false;
base.disableSaksFirstPointRedemption = 'disableSaksFirstPointRedemption' in customPreferences ? customPreferences.disableSaksFirstPointRedemption : false;
base.disableSaksFirstBeautyRewardRedemption =
  'disableSaksFirstBeautyRewardRedemption' in customPreferences ? customPreferences.disableSaksFirstBeautyRewardRedemption : false;
base.isAmexPayEnabled = customPreferences.enableAmexPayPoints;
base.saksFirstPointsMultiplier = 'saksFirstPointsMultiplier' in customPreferences ? customPreferences.saksFirstPointsMultiplier : -1;
base.wlTilesPerPage = 'wlTilesPerPage' in customPreferences ? customPreferences.wlTilesPerPage : 9;
base.numberOfCouponsAllowed = 'numberOfCouponsAllowed' in customPreferences ? customPreferences.numberOfCouponsAllowed : 5;
base.restrictedStatesUSPS = 'restrictedStatesUSPS' in customPreferences ? customPreferences.restrictedStatesUSPS : '';
base.recommendationStripsDays = 'recommendationStripsDays' in customPreferences ? customPreferences.recommendationStripsDays : 10;
base.saksPlusProductID = 'saksPlusProductID' in customPreferences && customPreferences.saksPlusProductID ? customPreferences.saksPlusProductID : '';
base.saksPlusCouponCode = 'saksPlusCouponCode' in customPreferences ? customPreferences.saksPlusCouponCode : null;
base.PP_ShowExpressCheckoutButtonOnCart =
  'PP_ShowExpressCheckoutButtonOnCart' in customPreferences ? customPreferences.PP_ShowExpressCheckoutButtonOnCart : false;
base.saksFirstFreeShipCustomerGroup = 'saksFirstFreeShipCustomerGroup' in customPreferences ? customPreferences.saksFirstFreeShipCustomerGroup : null;
base.BR_Enable_Widget = 'BR_Enable_Widget' in customPreferences ? customPreferences.BR_Enable_Widget : false;
base.BR_Enable_ThematicAPI = 'BR_Enable_ThematicAPI' in customPreferences ? customPreferences.BR_Enable_ThematicAPI : false;
base.defaultPhoneNumber = 'defaultPhoneNumber' in customPreferences ? customPreferences.defaultPhoneNumber : '1-877-551-7257';

// SFSX-3542 Used to identify list of brands that restrict google ads
base.restrictedGoogleAdsBrands =
  'restrictedGoogleAdsBrands' in customPreferences && customPreferences.restrictedGoogleAdsBrands && customPreferences.restrictedGoogleAdsBrands.length > 0
    ? customPreferences.restrictedGoogleAdsBrands
    : [];

base.BEPerformanceChangesEnable = 'BEPerformanceChangesEnable' in customPreferences ? customPreferences.BEPerformanceChangesEnable : false;
base.CartBEPerformanceChangesEnable = 'CartBEPerformanceChangesEnable' in customPreferences ? customPreferences.CartBEPerformanceChangesEnable : false;
base.SearchBEPerformanceChangesEnable = 'SearchBEPerformanceChangesEnable' in customPreferences ? customPreferences.SearchBEPerformanceChangesEnable : false;
base.HomeBEPerformanceChangesEnable = 'HomeBEPerformanceChangesEnable' in customPreferences ? customPreferences.HomeBEPerformanceChangesEnable : false;
base.GlobalBEPerformanceChangesEnable = 'GlobalBEPerformanceChangesEnable' in customPreferences ? customPreferences.GlobalBEPerformanceChangesEnable : false;

module.exports = base;
