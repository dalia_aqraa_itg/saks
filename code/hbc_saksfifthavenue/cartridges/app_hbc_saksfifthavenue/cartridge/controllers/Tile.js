'use strict';

var server = require('server');

server.extend(module.superModule);

var cache = require('*/cartridge/scripts/middleware/cache');
var preferences = require('*/cartridge/config/preferences');

server.append('Show', cache.applyTwelveHoursPromotionSensitiveCache, function (req, res, next) {
  var isSlotTile = req.querystring.slotTile;
  var showShortDescription = req.querystring.showShortDescription;
  var fulllooktile = req.querystring.fulllooktile;
  var wishlistTile = req.querystring.wishlistTile;
  var viewData = res.getViewData();
  var display = viewData.display;
  var isGucciCTC = viewData && viewData.product && viewData.product.hbcProductType === 'GucciCTC';
  display.rotateImages = true;
  if (req.querystring.view == 'productGrid' || isGucciCTC) {
    display.swatches = false;
    display.ratings = false;
  }

  if (req.querystring.view == 'productGrid') {
    display.rotateImages = false;
  }

  res.setViewData({
    isSlotTile: isSlotTile,
    showShortDescription: showShortDescription,
    isQuickViewEnabled: preferences.isQuickViewEnabled,
    wishlistTile: wishlistTile,
    fulllooktile: fulllooktile,
    lazyLoad: req.querystring.lazyLoad === 'true',
    display: display
  });

  if (wishlistTile) {
    var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
    var list = productListHelper.getList(req.currentCustomer.raw, {
      type: 10
    });
    var config = {
      qty: 1,
      optionId: null,
      optionValue: null,
      req: req,
      type: 10
    };
    productListHelper.addItem(list, req.querystring.pid, config);
  }

  next();
});

module.exports = server.exports();
