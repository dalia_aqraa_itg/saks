'use strict';

var server = require('server');
server.extend(module.superModule);

var csrfProtection = require('*/cartridge/scripts/middleware/csrf');

server.replace('Logout', function (req, res, next) {
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var AgentUserMgr = require('dw/customer/AgentUserMgr');
  var URLUtils = require('dw/web/URLUtils');
  try {
    if (req.session.isUserAuthenticated()) {
      AgentUserMgr.logoutAgentUser();
    }
  } catch (e) {
    // error handlin g
  }
  CustomerMgr.logoutCustomer(false);
  res.redirect(URLUtils.url('Home-Show'));
  next();
});

server.get('AppCSRF', server.middleware.https, csrfProtection.validateAppUser, function (req, res, next) {
  var csrfProtection = require('dw/web/CSRFProtection');
  res.setViewData({
    csrf: {
      csrf_token_name: csrfProtection.getTokenName(),
      csrf_token: csrfProtection.generateToken()
    }
  });
  res.json({
    success: true
  });
  next();
});

server.get('AppLoginForm', server.middleware.https, csrfProtection.validateAppUser, csrfProtection.generateToken, function (req, res, next) {
  var URLUtils = require('dw/web/URLUtils');
  var profileForm = server.forms.getForm('profile');
  profileForm.clear();
  var actionUrl = URLUtils.url('Login-AppLogin');

  res.render('/account/appLogin', {
    actionUrl: actionUrl,
    profileForm: profileForm
  });
  next();
});

server.post('AppLogin', server.middleware.https, csrfProtection.validateAppUser, csrfProtection.validateAppRequest, function (req, res, next) {
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var Resource = require('dw/web/Resource');
  var Transaction = require('dw/system/Transaction');
  var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');

  var viewData = res.getViewData();
  if (viewData && viewData.csrfFail) {
    res.setStatusCode(500);
    res.json({
      success: false,
      errorMessage: Resource.msg('error.message.app.login.csrf.fail', 'login', null)
    });
    return next();
  } else {
    var email = req.form.username;
    var password = req.form.password;
    var rememberMe = true;

    var customer = CustomerMgr.getCustomerByLogin(email);
    var profile = null;

    if (customer !== null) {
      profile = customer.getProfile();
    }
    if (profile !== null && 'legacyPasswordHash' in profile.custom && profile.custom.legacyPasswordHash !== null) {
      var legacyLoginResult = legacyCustomerLogin.loginLegacyCustomer(customer, profile.custom.legacyPasswordHash, email, password, rememberMe);

      if (legacyLoginResult.authenticatedCustomer) {
        res.setStatusCode(200);
        res.json({
          success: true
        });
      } else if (legacyLoginResult.error) {
        res.setStatusCode(400);
        res.json({
          success: false,
          errorMessage: 'LEGACY: ' + Resource.msg(legacyLoginResult.errorMessage, 'login', null)
        });
      } else {
        res.setStatusCode(400);
        res.json({
          success: false,
          errorMessage: 'LEGACY: ' + Resource.msg('error.message.login.form', 'login', null)
        });
      }
      return next();
    } else {
      Transaction.wrap(function () {
        var authenticateCustomerResult = CustomerMgr.authenticateCustomer(email, password);

        if (authenticateCustomerResult.status !== 'AUTH_OK') {
          var errorCodes = {
            ERROR_CUSTOMER_DISABLED: 'error.message.account.disabled',
            ERROR_CUSTOMER_LOCKED: 'error.message.account.locked',
            ERROR_CUSTOMER_NOT_FOUND: 'error.message.login.form',
            ERROR_PASSWORD_EXPIRED: 'error.message.password.expired',
            ERROR_PASSWORD_MISMATCH: 'error.message.password.mismatch',
            ERROR_UNKNOWN: 'error.message.error.unknown',
            default: 'error.message.login.form'
          };

          var errorMessageKey = errorCodes[authenticateCustomerResult.status] || errorCodes.default;
          var errorMessage = Resource.msg(errorMessageKey, 'login', null);

          res.setStatusCode(400);
          res.json({
            success: false,
            errorMessage: errorMessage
          });

          return;
        }

        var authenticatedCustomer = CustomerMgr.loginCustomer(authenticateCustomerResult, rememberMe);
        if (authenticatedCustomer) {
          res.setStatusCode(200);
          res.json({
            success: true
          });
        } else {
          res.setStatusCode(400);
          res.json({
            success: false,
            errorMessage: Resource.msg('error.message.login.form', 'login', null)
          });
        }
        return;
      });
      return next();
    }
  }
  return next();
});

module.exports = server.exports();
