'use strict';

var server = require('server');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var cache = require('*/cartridge/scripts/middleware/cache');
var refineSearch = require('*/cartridge/models/bopis/refineSearch');
var preferences = require('*/cartridge/config/preferences');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');
var promotionHelper = require('*/cartridge/scripts/util/promotionHelper');
var URLUtils = require('dw/web/URLUtils');
var ACTION_HOME = 'Home-Show';
var SEARCH_SHOWAJAX = 'Search-ShowAjax';
var SEARCH_SHOW = 'Search-Show';
var TOTAL_PAGE_SIZE = preferences.totalPageSize ? preferences.totalPageSize : 96;
var LOAD_PAGE_SIZE = preferences.defaultPageSize ? preferences.defaultPageSize : 24;
var ACTION_REFINEMENT = 'Stores-ShowStoreRefinement';

server.extend(module.superModule);

server.replace(
  'ShowAjax',
  cache.applyFourHoursPromotionSensitiveCache,
  consentTracking.consent,
  function (req, res, next) {
    var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
    var result = searchHelper.search(req, res);
    var refinementActionUrl = 'Search-ShowAjax';
    var Resource = require('dw/web/Resource');

    if (result.searchRedirect) {
      res.redirect(result.searchRedirect);
      return next();
    }
    var productSearch = result.productSearch;
    var renderingTemplate = 'search/searchResultsNoDecorator';
    if (productSearch.isCategorySearch && !productSearch.isRefinedCategorySearch && result.categoryTemplate) {
      renderingTemplate = result.categoryTemplate;
    }

    /**
     *  The Search Controller is cached  1hr and the Tile cache is 4hrs currently.
     *  Calling this on the Tile-Show, would make an expense call to PromotionMgr.activeCustomerPromotions.getProductPromotions() for almost
     *  all the requests until the initial Requests.
     *
     *  It would be a lot more performative if the Requests are reduced to 1 per category vs 1 per every product tile on all category and search page.
     *
     * This is called on Search  to reduce the number of calls from Tile-Show.
     */
    promotionHelper.setPromoCache();

    var totalBlocks = Math.ceil(TOTAL_PAGE_SIZE / LOAD_PAGE_SIZE);
    var currentPage = productSearch.pageNumber + 1;
    var showPreviousPage =
      currentPage === totalBlocks * Math.ceil(currentPage / totalBlocks)
        ? true
        : !!(currentPage < totalBlocks * Math.ceil(currentPage / totalBlocks) && currentPage > totalBlocks * Math.floor(currentPage / totalBlocks) + 1);
    // prepare store refinement url loaded as url include in template
    var storeRefineUrl = refineSearch.getStoreRefinementUrl(req, ACTION_REFINEMENT);
    // url to reset and execute the search form the store select
    var storeRefineUrlForCheckBox = refineSearch.getStoreRefinementUrl(req, refinementActionUrl);

    res.render(renderingTemplate, {
      ajax: true,
      showPreviousPage: showPreviousPage,
      showMore: currentPage <= totalBlocks * Math.ceil(currentPage / totalBlocks),
      startSize: req.querystring.start ? req.querystring.start : '',
      storeRefineUrl: storeRefineUrl,
      productSearch: productSearch,
      maxSlots: result.maxSlots,
      reportingURLs: result.reportingURLs,
      refineurl: result.refineurl,
      isRefinedByStore: result.isRefinedByStore,
      isBopisEnabled: preferences.isBopisEnabled,
      storeRefineUrlForCheckBox: storeRefineUrlForCheckBox,
      noSDDResultMessage: Resource.msg('search.sdd.noresults', 'search', null),
      SDDResultMessage: Resource.msg('store.refinement.sdd', 'search', null)
    });

    return next();
  },
  pageMetaData.computedPageMetaData
);

/**
 * Block to identify known vs unkown user based on recent searches api - Which uses cquotient tracking cookie
 * Along with user having a valid basket object in the session
 */
server.get('NoResultsRecommendation', function (req, res, next) {
  var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');

  var knownUser = searchHelper.isKnownUser(req.session);
  res.render('search/noResultsRecommendation', {
    knownUser : knownUser
  });

  next();
});

//eslint-disable-next-line consistent-return
server.replace(
  'Show',
  cache.applyEightHoursPromotionSensitiveCache,
  consentTracking.consent,
  function (req, res, next) {
    var ProductSearchModel = require('dw/catalog/ProductSearchModel');
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var ContentMgr = require('dw/content/ContentMgr');
    var Logger = require('dw/system/Logger');
    var template = 'search/searchResults';

    var apiProductSearch = new ProductSearchModel();
    var viewData = {
      apiProductSearch: apiProductSearch
    };
    res.setViewData(viewData);
    var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
    var Resource = require('dw/web/Resource');

    // set store search url
    var storeRefineUrl = refineSearch.getStoreRefinementUrl(req, ACTION_REFINEMENT);
    var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
    var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');
    var result = searchHelper.search(req, res);
    var Site = require('dw/system/Site');
    var siteName = Site.getCurrent().getName();
    var hasBrand = req.querystring.preferences && req.querystring.preferences.brand;
    var isSale = req.querystring.preferences && req.querystring.preferences.isSale;
    var isClearance = req.querystring.preferences && req.querystring.preferences.isClearance;
    let refinementTitle = '';
    var doNotReset = req.querystring && req.querystring.doNotReset;
    var isSDDFilterApplied = false;
    var brandMetaTag = {};
    var brand = hasBrand ? req.querystring.preferences.brand : '';
    var restrictGoogleAds = false;
    if (!!hasBrand) {
      // eslint-disable-line
      // SFSX-3697 add restrictGoogleAds to disabel ads
      if (req.querystring.preferences && req.querystring.preferences.brand && preferences.restrictedGoogleAdsBrands) {
        restrictGoogleAds = productHelper.checkGoogleAdsRestrictedBrands(req.querystring.preferences.brand);
      }
      if (result.tempCategory && result.tempCategory.ID !== 'root') {
        pageMetaHelper.setPageMetaData(req.pageMetaData, result.productSearch);
        pageMetaHelper.setPageMetaTags(req.pageMetaData, result.productSearch);
      } else {
        brandMetaTag.pageTitle = req.querystring.preferences.brand + ' | ' + siteName; // eslint-disable-line
        brandMetaTag.pageDescription = brandMetaTag.pageTitle;
        brandMetaTag.keywords = brandMetaTag.pageTitle;
      }
      pageMetaHelper.setPageMetaData(req.pageMetaData, brandMetaTag);
      pageMetaHelper.setPageMetaTags(req.pageMetaData, brandMetaTag);
    } else if ((result.category && result.category.ID == 'root') || req.querystring.categoryfilter != null) {
      var designerSEO = ContentMgr.getContent('seo-designer');
      if (designerSEO) {
        pageMetaHelper.setPageMetaData(req.pageMetaData, designerSEO);
        pageMetaHelper.setPageMetaTags(req.pageMetaData, designerSEO);
      }
    } else if (!empty(req.querystring.q)) {
      var searchObj = {};
      searchObj.pageTitle = req.querystring.q + ' | ' + siteName;
      searchObj.pageDescription = req.querystring.q + ' | ' + siteName;
      pageMetaHelper.setPageMetaData(req.pageMetaData, searchObj);
      pageMetaHelper.setPageMetaTags(req.pageMetaData, searchObj);
    } else if (isSale && req.querystring && req.querystring.cgid !== preferences.saleCategoryID) {
      // The conditions are separated at  line #179 for isClearance. It is separated to give preference to isSale
      let titleObj = {};
      // eslint-disable-next-line no-nested-ternary
      refinementTitle = Resource.msg('refinement.sale.label', 'common', null);
      if ('pageMetaTags' in result.productSearch) {
        result.productSearch.pageMetaTags.forEach(function (item) {
          if (item.title) {
            titleObj.pageTitle = refinementTitle + ' ' + item.content;
          } else if (item.name && item.ID === 'description') {
            titleObj.pageDescription = item.content;
          } else if (item.name && item.ID === 'keywords') {
            titleObj.pageKeywords = item.content;
          }
        });
      }
      pageMetaHelper.setPageMetaData(req.pageMetaData, titleObj);
      pageMetaHelper.setPageMetaTags(req.pageMetaData, titleObj);
    } else if (isClearance && req.querystring && req.querystring.cgid !== preferences.clearanceCategoryID) {
      let titleObj = {};
      refinementTitle = Resource.msg('refinement.clearance.label', 'common', null);
      if ('pageMetaTags' in result.productSearch) {
        result.productSearch.pageMetaTags.forEach(function (item) {
          if (item.title) {
            titleObj.pageTitle = refinementTitle + ' ' + item.content;
          } else if (item.name && item.ID === 'description') {
            titleObj.pageDescription = item.content;
          } else if (item.name && item.ID === 'keywords') {
            titleObj.pageKeywords = item.content;
          }
        });
      }
      pageMetaHelper.setPageMetaData(req.pageMetaData, titleObj);
      pageMetaHelper.setPageMetaTags(req.pageMetaData, titleObj);
    } else if (!empty(req.querystring.cgid)) {
      var currentCat = CatalogMgr.getCategory(req.querystring.cgid);
      var apiContent = result.productSearch;

      if (currentCat.pageDescription != null && currentCat.pageTitle != null && currentCat.pageKeywords != null) {
        let cattitleObj = {};
        cattitleObj.pageKeywords = currentCat.pageKeywords;
        cattitleObj.pageDescription = currentCat.pageDescription;
        cattitleObj.pageTitle = currentCat.pageTitle;
        apiContent = cattitleObj;
      }
      pageMetaHelper.setPageMetaData(req.pageMetaData, apiContent);
      pageMetaHelper.setPageMetaTags(req.pageMetaData, apiContent);
    } else if (result.productSearch) {
      pageMetaHelper.setPageMetaData(req.pageMetaData, result.productSearch);
      pageMetaHelper.setPageMetaTags(req.pageMetaData, result.productSearch);
    }
    // url to reset and execute the search form the store select
    var storeRefineUrlForCheckBox = refineSearch.getStoreRefinementUrl(req, SEARCH_SHOWAJAX);

    /**
     *  The Search Controller is cached  1hr and the Tile cache is 4hrs currently.
     *  Calling this on the Tile-Show, would make an expense call to PromotionMgr.activeCustomerPromotions.getProductPromotions() for almost
     *  all the requests until the initial Requests.
     *
     *  It would be a lot more performative if the Requests are reduced to 1 per category vs 1 per every product tile on all category and search page.
     *
     * This is called on Search  to reduce the number of calls from Tile-Show.
     */
    promotionHelper.setPromoCache();

    /**
     * Force set user experience values for selected categories
     * Update cookie and session value
     */
    if (result && result.category && 'forcedShopExperience' in result.category.custom) {
      var experience = result.category.custom.forcedShopExperience;
      var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
      session.custom.shopPreference = experience;
      cookiesHelper.createWithoutHttpOnly('shopPreference', experience);
    }

    this.on('route:BeforeComplete', function (req, res) {
      // eslint-disable-line
      if (result.searchRedirect) {
        res.redirect(result.searchRedirect);
        return;
      }

      // redirect to error if any refinement is applied without these parameters
      if (!req.querystring.cgid && !req.querystring.q && !req.querystring.pid && !req.querystring.pmid && req.querystring.q != '') {
        res.redirect(URLUtils.https('Home-ErrorNotFound').toString());
        return;
      }
      //  SFDEV-3056 | Issue for redirecting to PDP in case only one product is found
      if (result.productSearch.count === 1 && !req.querystring.ajax && !hasBrand) {
        let product = result.productSearch.productIds[0].productID;
        res.redirect(URLUtils.url('Product-Show', 'pid', product));
        return;
      }
      if (result.category) {
        template = result.categoryTemplate || 'rendering/category/producthits';
      }

      if ((result.category && result.category.ID === 'root' && !(isSale || isClearance)) || req.querystring.categoryfilter != null) {
        template = 'rendering/category/designerbay';
      }
      // if the category is root, then set the brand as the category
      var categoryOnBrandPage;
      if (result.tempCategory) {
        categoryOnBrandPage = result.tempCategory.ID;
        if (result.tempCategory.ID === 'root') {
          categoryOnBrandPage = 'brand';
        }
      }

      // reset the clear All to fetch the results only with the brand in selection and not wipe brand selection
      // As brand selection does not show up on the filter bar for the brand pages.
      if (!doNotReset && hasBrand && hasBrand.indexOf('|') === -1) {
        result.productSearch.resetLink = URLUtils.https('Search-Show', 'cgid', categoryOnBrandPage, 'prefn1', 'brand', 'prefv1', hasBrand);
      }
      if (result.isRefinedByStore) {
        var storeHelper = require('*/cartridge/scripts/helpers/storeHelpers');
        var selectedFilters = result.productSearch.selectedFilters;
        if (selectedFilters) {
           selectedFilters = selectedFilters.filter(function(object){return object.displayValue !="$0+"});
        }
        var refinementActionUrl = 'Search-ShowAjax';
        var actionUrl = refineSearch.getStoreRefinementUrl(req, refinementActionUrl);
        actionUrl = storeHelper.getRefinementUrl(actionUrl);
        isSDDFilterApplied = storeHelper.isSDDFilterApplied(req);
      }

      var refinementapplied = 'all';
      if (isSDDFilterApplied) {
        refinementapplied = 'sameday';
      } else if (!isSDDFilterApplied && result.isRefinedByStore) {
        refinementapplied = 'bopis';
      }

      session.custom.refinementapplied = refinementapplied;
      if (isSDDFilterApplied && result.isRefinedByStore) {
        var sddFilter = {
          actionEndPoint: SEARCH_SHOW,
          displayValue: Resource.msg('ssmodal.header.text', 'search', null),
          selected: true,
          title: Resource.msg('ssmodal.header.text', 'search', null),
          url: actionUrl
        };
        selectedFilters.push(sddFilter);
        Object.defineProperty(result.productSearch, 'selectedFilters', {
          enumerable: true,
          value: selectedFilters
        });
      } else if (result.isRefinedByStore) {
        var bopisFilter = {
          actionEndPoint: SEARCH_SHOW,
          displayValue: Resource.msg('shippingoption.product.pickupinstore', 'storeLocator', null),
          selected: true,
          title: Resource.msg('shippingoption.product.pickupinstore', 'storeLocator', null),
          url: actionUrl
        };
        selectedFilters.push(bopisFilter);
        Object.defineProperty(result.productSearch, 'selectedFilters', {
          enumerable: true,
          value: selectedFilters
        });
      } else if (result.isRefinedByStore) {
        var bopisFilter = {
          actionEndPoint: SEARCH_SHOW,
          displayValue: Resource.msg('shippingoption.product.pickupinstore', 'storeLocator', null),
          selected: true,
          title: Resource.msg('shippingoption.product.pickupinstore', 'storeLocator', null),
          url: actionUrl
        };
        selectedFilters.push(bopisFilter);
        Object.defineProperty(result.productSearch, 'selectedFilters', {
          enumerable: true,
          value: selectedFilters
        });
      }
      if (!preferences.SearchBEPerformanceChangesEnable) {
        /**
         * Block to identify known vs unkown user based on recent searches api - Which uses cquotient tracking cookie
         * Along with user having a valid basket object in the session
         */
        var knownUser = searchHelper.isKnownUser(req.session);
        res.setViewData({
          knownUser: knownUser
        });
      }
      var qs = req.querystring.mydesigners;
      var fromMyDesigners = !!(req.querystring.mydesigners == 'true');
      var stopper = 'stop';
      res.render(template, {
        brand: brand,
        productSearch: result.productSearch,
        isRootCategory: (result.category && result.category.root) || (result.tempCategory && result.tempCategory.root),
        maxSlots: result.maxSlots,
        isSale: isSale,
        isClearance: isClearance,
        clearanceCategoryID: preferences.clearanceCategoryID,
        saleCategoryID: preferences.saleCategoryID,
        reportingURLs: result.reportingURLs,
        refineurl: result.refineurl,
        category: result.category ? result.category : result.tempCategory,
        canonicalUrl: result.canonicalUrl,
        schemaData: result.schemaData,
        isRefinedByStore: result.isRefinedByStore,
        storeRefineUrl: storeRefineUrl,
        storeRefineUrlForCheckBox: storeRefineUrlForCheckBox,
        showPriceFilter: req.querystring.pmin || req.querystring.pmax,
        fromMyDesigners: fromMyDesigners,
        noSDDResultMessage: Resource.msg('search.sdd.noresults', 'search', null),
        SDDResultMessage: Resource.msg('store.refinement.sdd', 'search', null),
        restrictGoogleAds: restrictGoogleAds
      });
      var designerObject = {};
      viewData = res.getViewData();
      var productSearch = viewData.productSearch;
      var breadcrumbs;
      var clearanceCatDisplayName;
      var saleCatDisplayName;
      var includeRecaptchaJS = true;

      // Search for Keyword banner content asset using the mapping saved in the "KeywordSearchBanner" Custom Object.
      var searchPromo;
      if (!empty(req.querystring.q)) {
        var searchKeyword = req.querystring.q.toLowerCase();
        searchKeyword = '*|' + searchKeyword + '|*';
        var CustomObjectMgr = require('dw/object/CustomObjectMgr');
        var searchBannerCO = CustomObjectMgr.queryCustomObject('KeywordSearchBanner', 'custom.keywords ILIKE {0}', searchKeyword);
        if (searchBannerCO) {
          if (!empty(searchBannerCO.custom.contentAssetId)) {
            searchPromo = ContentMgr.getContent(searchBannerCO.custom.contentAssetId);
          }
        }
      }

      // designer category changes
      if ((result.category && result.category.ID === 'root' && !(isSale || isClearance)) || req.querystring.categoryfilter != null) {
        var alphaNums = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '#'];
        designerObject.results = searchHelper.searchDesigner(productSearch);
        designerObject.alphaNums = alphaNums;
      }

      if (req.querystring && req.querystring.preferences && req.querystring.preferences['brand'] && req.querystring.cgid) {
        // eslint-disable-line
        // req.querystring.preferences['brand'] does not work in dot notation
        breadcrumbs = [];

        if (req.querystring.cgid !== 'brand' && req.querystring.cgid !== 'root') {
          breadcrumbs = productHelper.getAllBreadcrumbs(req.querystring.cgid, null, breadcrumbs);
        }
        if (req.querystring.preferences['brand'].indexOf('|') === -1) {
          // eslint-disable-line
          breadcrumbs.push({
            htmlValue: req.querystring.preferences['brand'], // eslint-disable-line
            url: URLUtils.https('Search-Show', 'cgid', 'brand', 'prefn1', 'brand', 'prefv1', hasBrand)
          });
        }

        breadcrumbs.reverse();
      } else if (req.querystring && req.querystring.preferences && req.querystring.preferences.isSale && req.querystring.cgid != preferences.saleCategoryID) {
        breadcrumbs = [];

        if (req.querystring.cgid !== 'brand' && req.querystring.cgid !== 'root') {
          breadcrumbs = productHelper.getAllBreadcrumbs(req.querystring.cgid, null, breadcrumbs);
        }
        var saleCategory = CatalogMgr.getCategory(preferences.saleCategoryID);
        if (saleCategory) {
          saleCatDisplayName = saleCategory.displayName;
          breadcrumbs.push({
            htmlValue: saleCategory.displayName, // eslint-disable-line
            url: searchHelper.getCategoryUrl(saleCategory)
          });
        }

        breadcrumbs.reverse();
      } else if (
        req.querystring &&
        req.querystring.preferences &&
        req.querystring.preferences.isClearance &&
        req.querystring.cgid != preferences.clearanceCategoryID
      ) {
        breadcrumbs = [];

        if (req.querystring.cgid !== 'brand' && req.querystring.cgid !== 'root') {
          breadcrumbs = productHelper.getAllBreadcrumbs(req.querystring.cgid, null, breadcrumbs);
        }
        var clearanceCategory = CatalogMgr.getCategory(preferences.clearanceCategoryID);
        if (clearanceCategory) {
          clearanceCatDisplayName = clearanceCategory.displayName;
          breadcrumbs.push({
            htmlValue: clearanceCategory.displayName, // eslint-disable-line
            url: searchHelper.getCategoryUrl(clearanceCategory)
          });
        }

        breadcrumbs.reverse();
      } else if (productSearch.category) {
        breadcrumbs = [];

        var categoryId = productSearch.category.id;
        breadcrumbs = productHelper.getAllBreadcrumbs(categoryId, null, breadcrumbs);
        if (productSearch.searchKeywords) {
          breadcrumbs.push({
            htmlValue: Resource.msg('label.search.all.results', 'search', null),
            url: URLUtils.url(SEARCH_SHOW, 'q', req.querystring.q)
          });
        }
        breadcrumbs.reverse();
      } else if (productSearch.searchKeywords) {
        breadcrumbs = productHelper.getAllBreadcrumbs(null, null, [
          {
            htmlValue: Resource.msg('label.search.all.results', 'search', null),
            url: null
          }
        ]);
      }
      var totalBlocks = Math.ceil(TOTAL_PAGE_SIZE / LOAD_PAGE_SIZE);
      var currentPage = productSearch.pageNumber + 1;
      var showPreviousPage =
        currentPage === totalBlocks * Math.ceil(currentPage / totalBlocks)
          ? true
          : !!(currentPage < totalBlocks * Math.ceil(currentPage / totalBlocks) && currentPage > totalBlocks * Math.floor(currentPage / totalBlocks) + 1);
      // Logic to retrieve category taxonomy for Chanel CLP/listing page
      var chanelCategory;
      var chanelCategories = [];
      var chanelcurrentCategory;
      var parentofselectedchanelCat;
      var selectedchanelCategory;
      if (
        productSearch.isCategorySearch &&
        productSearch.category !== null &&
        (productSearch.category.template === 'rendering/category/categorylandingchanel' ||
          productSearch.category.template === 'rendering/category/producthitschanel')
      ) {
        var categoryFromProductSearch = CatalogMgr.getCategory(productSearch.category.id);
        parentofselectedchanelCat = categoryFromProductSearch.getParent().displayName;
        chanelcurrentCategory = productSearch.category.id;
        selectedchanelCategory = categoryFromProductSearch;

        if (categoryFromProductSearch) {
          chanelCategory = searchHelper.getL1CategoryDetails(categoryFromProductSearch);
        }
        if (chanelCategory) {
          if (chanelCategory.hasOnlineSubCategories()) {
            chanelCategories = searchHelper.categories(chanelCategory.onlineSubCategories);
            if (!empty(chanelCategories) && chanelCategories.length > 0) {
              chanelCategories = chanelCategories.filter(function (category) {
                var name = category.name || '';
                return name.toLowerCase() === 'chanel';
              });
            }
          }
        }
      }

      // SFSX-3674 Added to get Gucci BLP left navigation
      var designerCategory;
      var currentCategory;
      var designerCategories = [];
      if (
        productSearch.isCategorySearch &&
        productSearch.category !== null &&
        (productSearch.category.template === 'rendering/category/categorylandinggucci' ||
          productSearch.category.template === 'rendering/category/producthitsgucci')
      ) {
        var categoryFromProductSearch = CatalogMgr.getCategory(productSearch.category.id);
        currentCategory = productSearch.category.id;
        if (categoryFromProductSearch) {
          designerCategory = searchHelper.getL1CategoryDetails(categoryFromProductSearch, 'gucci');
        }

        if (designerCategory && designerCategory.hasOnlineSubCategories()) {
          designerCategories = searchHelper.categories(designerCategory.onlineSubCategories);
        }
        // SFSX-3697 add restrictGoogleAds to disabel ads
        if (preferences.restrictedGoogleAdsBrands) {
          restrictGoogleAds = productHelper.checkGoogleAdsRestrictedBrands('gucci');
        }
        brand = "Gucci";
      }

      var args = {};
      try {
        var getBloomReachWidget = require('*/cartridge/scripts/getBloomReachWidget');
        var BR_Enable_Widget = preferences.BR_Enable_Widget;
        // BloomReach Widget parameters
        if (BR_Enable_Widget) {
          args.CurrentRequest = request;
          args.pageType = 'category';
          args.productId = '';
          args.productName = '';
          args.pageStatus = '';
          args = getBloomReachWidget.execute(args);
        }
      } catch (e) {
        var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Statck:' + e.stack;
        Logger.error('Bloom Reach producttile' + errorMsg);
      }
      viewData = {
        ajax: !!req.querystring.ajax,
        args: args,
        designerObject: designerObject,
        hasBrandRefinement: !!hasBrand,
        brandValue: hasBrand || '',
        clearanceCatDisplayName: clearanceCatDisplayName,
        saleCatDisplayName: saleCatDisplayName,
        showPreviousPage: showPreviousPage,
        showMore: currentPage <= totalBlocks * Math.ceil(currentPage / totalBlocks),
        breadcrumbs: breadcrumbs,
        startSize: req.querystring.start ? req.querystring.start : '',
        includeRecaptchaJS: includeRecaptchaJS,
        chanelCategories: chanelCategories,
        designerCategories: designerCategories,
        chanelcurrentCategory: chanelcurrentCategory,
        currentCategory: currentCategory,
        parentofselectedchanelCat: parentofselectedchanelCat,
        selectedchanelCategory: selectedchanelCategory,
        SearchPromo: searchPromo,
        restrictGoogleAds: restrictGoogleAds,
        searchTerm: req.querystring.term,
        searchType: req.querystring.type,
        chanelCatUrl: chanelCategory != null ? URLUtils.https('Search-Show', 'cgid', chanelCategory.ID).toString() : '',
        searchPageClass:
          !productSearch.isRefinedCategorySearch && (preferences.catRefinementLevel === 2 || !preferences.catRefinementLevel) ? 'search-results-page' : '',
        brand: brand
      };
      res.setViewData(viewData);
    });

    viewData = {
      previousWord: req.querystring.prevQ,
      searchWord: req.querystring.q,
      restrictGoogleAds: restrictGoogleAds,
      searchSuggestion: !!req.querystring.ss,
      s7APIForPDPZoomViewer: preferences.s7APIForPDPZoomViewer,
      s7APIForPDPVideoPlayer: preferences.s7APIForPDPVideoPlayer,
      displayQuickLook: preferences.displayQuickLook ? preferences.displayQuickLook : '',
      isBopisEnabled: preferences.isBopisEnabled,
      promoTrayEnabled: preferences.promoTrayEnabled,
      src: req.querystring.src,
      brand: brand
    };
    viewData.storeRefineUrl = storeRefineUrl;
    res.setViewData(viewData);
    next();
  },
  pageMetaData.computedPageMetaData
);

server.get('SlotCategoryProducts', cache.applyFourHoursPromotionSensitiveCache, function (req, res, next) {
  var CatalogMgr = require('dw/catalog/CatalogMgr');
  var ProductSearchModel = require('dw/catalog/ProductSearchModel');
  var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
  var ProductSearch = require('*/cartridge/models/search/productSearch');

  var slotID = '';
  if (!empty(req.querystring.slotID)) {
    slotID = req.querystring.slotID;
  }

  var apiProductSearch = new ProductSearchModel();
  apiProductSearch = searchHelper.setupSearch(apiProductSearch, req.querystring);
  var viewData = {
    apiProductSearch: apiProductSearch,
    slotID: slotID
  };

  res.setViewData(viewData);

  this.on('route:BeforeComplete', function (req, res) {
    // eslint-disable-line no-shadow
    // execute custom search and return the search model
    var storeRefineResult = refineSearch.search(apiProductSearch, req.querystring);

    var productSearch = new ProductSearch(
      apiProductSearch,
      req.querystring,
      req.querystring.srule,
      CatalogMgr.getSortingOptions(),
      CatalogMgr.getSiteCatalog().getRoot()
    );

    var template = 'search/slotproductGrid';
    res.render(template, {
      productSearch: productSearch
    });
  });

  next();
});

server.replace(
  'Refinebar',
  cache.applyPrefernceCenterCache,
  function (req, res, next) {
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var ProductSearchModel = require('dw/catalog/ProductSearchModel');
    var ProductSearch = require('*/cartridge/models/search/productSearch');
    var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
    var Resource = require('dw/web/Resource');
    var array = require('*/cartridge/scripts/util/array');
    var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
    var maxRefinementValueLimit = preferences.refinementValueMaxLimit ? preferences.refinementValueMaxLimit : 8;
    var viewData = res.getViewData();
    var hasBrand = req.querystring.preferences && req.querystring.preferences.brand;
    var storeRefineResult = null;
    var isSDDFilterApplied = false;
    var filterCount = 0;
    var refinementActionUrl = 'Search-ShowAjax';
    var apiProductSearch = new ProductSearchModel();
    apiProductSearch = searchHelper.setupSearch(apiProductSearch, req.querystring);

    // prepare store refinement url loaded as url include in template
    var storeRefineUrl = refineSearch.getStoreRefinementUrl(req, ACTION_REFINEMENT);
    // url to reset and execute the search form the store select
    var storeRefineUrlForCheckBox = refineSearch.getStoreRefinementUrl(req, refinementActionUrl);
    // if the search key has chanel, then skip the refinement of non-chanel products.
    if (req.querystring && req.querystring.q && req.querystring.q.toLowerCase().indexOf('chanel') === -1) {
      if (preferences.nonChanelProductTypes) {
        apiProductSearch.addRefinementValues('hbcProductType', preferences.nonChanelProductTypes);
      }
    }
    // PSM search with store refinement
    storeRefineResult = refineSearch.search(apiProductSearch, req.querystring);
    var productSearch = new ProductSearch(
      apiProductSearch,
      req.querystring,
      req.querystring.srule,
      CatalogMgr.getSortingOptions(),
      CatalogMgr.getSiteCatalog().getRoot(),
      true
    );
    var refinements = productSearch.refinements;
    var selectedFilters = productSearch.selectedFilters;
    var searchKeyword;
    filterCount = selectedFilters.length;
    if (storeRefineResult.isRefinedByStore) {
      var storeHelper = require('*/cartridge/scripts/helpers/storeHelpers');
      isSDDFilterApplied = storeHelper.isSDDFilterApplied(req);
      var refinementActionUrl = 'Search-ShowAjax';
      var actionUrl = refineSearch.getStoreRefinementUrl(req, refinementActionUrl);
      actionUrl = storeHelper.getRefinementUrl(actionUrl);
    }
    var cgid = '';
    var DisableMyPreferencesOnCategory = false;
    var DesignerPreferencesExist = false;
    var SizePreferencesExist = false;
    var customerLogin = false;
    var MyDesignerPreferencesArray = [];
    var MySizePreferencesArray = [];
    var CurrentCatTempObj = {};

    // Preference Center
    if (productSearch && productSearch.category) {
      cgid = productSearch.category.id;
      var category = CatalogMgr.getCategory(cgid);
      if ('DisableMyPreferences' in category.custom && category.custom.DisableMyPreferences == true) {
        DisableMyPreferencesOnCategory = true;
      }
      var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
      var showMySizesObj = searchHelper.showMySizesOnPLP(productSearch.category.id);
      var showMySizes = showMySizesObj['showMySizes'];
      var categoryToAddInto = showMySizesObj['categoryToAddInto'];
    } else if (productSearch.searchKeywords) {
      searchKeyword = productSearch.searchKeywords;
    }

    if (req.currentCustomer.raw.profile && req.currentCustomer.raw.authenticated) {
      customerLogin = true;
      var customerNo = req.currentCustomer.raw.profile.customerNo;
      var DesignerPreferences = req.currentCustomer.raw.profile.custom.DesignerPreferences;
      var SizePreferences = req.currentCustomer.raw.profile.custom.SizePreferences;
      var MyDesignerPreferences = '';
      var MySizePreferences = '';
      if (DesignerPreferences) {
        DesignerPreferences = JSON.parse(DesignerPreferences);
        DesignerPreferencesExist = true;
      }
      if (SizePreferences) {
        SizePreferences = JSON.parse(SizePreferences);
      }
      if (req.currentCustomer.raw.profile.custom.SizePreferences) {
        SizePreferencesExist = true;
      }
    }
    /*    var storeRefinement =  {
        displayName: preferences.getitfastRefinementName,
        attributeID: 'getitfast',
        hideRefinement: false,
        values: []
    };*/

    var storeRefinement = array.find(refinements, function (refinement) {
      return refinement.attributeID === 'onlineFlag';
    });

    if (storeRefinement && isSDDFilterApplied && storeRefineResult.isRefinedByStore) {
      var refimentValues = [];
      var sddFilter = {
        actionEndPoint: SEARCH_SHOW,
        displayValue: Resource.msg('ssmodal.header.text', 'search', null),
        selected: true,
        title: Resource.msg('ssmodal.header.text', 'search', null),
        url: actionUrl
      };

      refimentValues.push(sddFilter);

      storeRefinement.values = refimentValues;
      storeRefinement.selectedCount = 1;
      if (req.querystring.pmin && req.querystring.pmax) {
        filterCount++;
      }
    } else if (storeRefinement && storeRefineResult.isRefinedByStore) {
      var refimentValues = [];
      var bopisFilter = {
        actionEndPoint: SEARCH_SHOW,
        displayValue: Resource.msg('shippingoption.product.pickupinstore', 'storeLocator', null),
        selected: true,
        title: Resource.msg('shippingoption.product.pickupinstore', 'storeLocator', null),
        url: actionUrl
      };
      refimentValues.push(bopisFilter);
      storeRefinement.values = refimentValues;
      storeRefinement.selectedCount = 1;
      if (req.querystring.pmin && req.querystring.pmax) {
        filterCount++;
      }
    }

    // add getitfast filter
    /*    if (preferences.isBopisEnabled && preferences.isEnabledforSameDayDelivery) {
        refinements.splice(2, 0, storeRefinement);
    }*/

    Object.defineProperty(productSearch, 'refinements', {
      enumerable: true,
      value: refinements
    });
    Object.defineProperty(productSearch, 'selectedFilters', {
      enumerable: true,
      value: selectedFilters
    });
    viewData = {
      maxRefinementValueLimit: maxRefinementValueLimit,
      plpRefineViewMoreCatLimit: preferences.plpRefineViewMoreCatLimit
    };
    var sizePrefSelectedCount = 0;
    var designerPrefSelectedCount = 0;
    for (var i = 0; i < productSearch.refinements.length; i++) {
      var refinement = productSearch.refinements[i];
      refinement.prefSelectedCount = 0;
      if (customerLogin) {
        for (var j = 0; j < refinement.values.length; j++) {
          var refinementValue = refinement.values[j];
          if (refinement.attributeID === 'sizeRefinement') {
            if (
              !~refinementValue.value.toLowerCase().indexOf('intimate') &&
              ((!SizePreferencesExist && refinementValue.selected) ||
                (SizePreferencesExist &&
                  productSearch &&
                  productSearch.category &&
                  SizePreferences[categoryToAddInto] &&
                  !SizePreferences[categoryToAddInto][refinementValue.value] &&
                  refinementValue.selected) ||
                (SizePreferencesExist &&
                  productSearch &&
                  productSearch.category &&
                  !SizePreferences[categoryToAddInto] &&
                  showMySizes &&
                  refinementValue.selected))
            ) {
              sizePrefSelectedCount++;
              refinement.prefSelectedCount++;
            }
            if (showMySizes) {
              //If This category or its parent exist in enable categories for size option
              if (SizePreferences && SizePreferences.hasOwnProperty(categoryToAddInto)) {
                // check if catgeory exist in the user sizes selection
                CurrentCatTempObj = SizePreferences[categoryToAddInto] || {};
                if (CurrentCatTempObj.hasOwnProperty(refinementValue.value)) {
                  //Then check if curremt size refinement is selected in the user sizes selection
                  if (!MySizePreferences) {
                    MySizePreferences = refinementValue.value;
                    MySizePreferencesArray.push(refinementValue.value);
                  } else {
                    MySizePreferences = MySizePreferences + '|' + refinementValue.value;
                    MySizePreferencesArray.push(refinementValue.value);
                  }
                }
              }
            }
          } else if (refinement.attributeID === 'brand') {
            if (
              (!DesignerPreferencesExist && refinementValue.selected) ||
              (DesignerPreferencesExist && !DesignerPreferences[refinementValue.value] && refinementValue.selected)
            ) {
              designerPrefSelectedCount++;
              refinement.prefSelectedCount++;
            }
            if (DesignerPreferences && DesignerPreferences[refinementValue.value]) {
              if (!MyDesignerPreferences) {
                MyDesignerPreferences = refinementValue.value;
                MyDesignerPreferencesArray.push(refinementValue.value);
              } else {
                MyDesignerPreferences = MyDesignerPreferences + '|' + refinementValue.value;
                MyDesignerPreferencesArray.push(refinementValue.value);
              }
            }
          }
        }
      } else {
        if (refinement.attributeID === 'sizeRefinement') {
          refinement.prefSelectedCount = refinement.selectedCount;
          sizePrefSelectedCount = refinement.selectedCount;
        } else if (refinement.attributeID === 'brand') {
          refinement.prefSelectedCount = refinement.selectedCount;
          designerPrefSelectedCount = refinement.selectedCount;
        }
      }
    }

    // If on current PA page there are No Prefernces
    if (!MyDesignerPreferences) {
      DesignerPreferencesExist = false;
    }
    if (!MySizePreferences) {
      SizePreferencesExist = false;
    }

    var queryOn = ''; //It will be either for cat page or search page : cgid=154904214485 & q=jeans
    var queryParam = ''; //CatID or SearchKeyword
    if (cgid) {
      queryOn = 'cgid';
      queryParam = cgid;
    } else {
      queryOn = 'q';
      queryParam = searchKeyword;
    }

    var SelectedDesignersArray = [];
    var SelectedSizessArray = [];
    var myPreferencesData = {};

    // Check and build selectedFiltersData object to be used on my preference center links
    // So that multiple selected filters can be combined with My Deisgner and My Sizes option
    for (var s = 0; s < selectedFilters.length; s++) {
      if (selectedFilters[s].id == 'sizeRefinement') {
        MySizePreferences = MySizePreferences + '|' + selectedFilters[s].value;
        SelectedSizessArray.push(selectedFilters[s].value);
      }
      if (selectedFilters[s].id == 'brand') {
        MyDesignerPreferences = MyDesignerPreferences + '|' + selectedFilters[s].value;
        SelectedDesignersArray.push(selectedFilters[s].value);
      }
      if (myPreferencesData.hasOwnProperty(selectedFilters[s].id)) {// Other selected Filters
        var CurrentValue = myPreferencesData[selectedFilters[s].id];
        myPreferencesData[selectedFilters[s].id] = CurrentValue + "|" + selectedFilters[s].value;
      } else {
        myPreferencesData[selectedFilters[s].id] = selectedFilters[s].value;
      }
    }

    // Use case : If all Deisgners/Sizes from "My Designers and My Sizes" are currently selected
    // Use this to make Enable and Checkmark "My Designers and My Sizes"
    var allMyDesignerSelected = false;
    var allMySizesSelected = false;
    if (SelectedDesignersArray.length > 0 && MyDesignerPreferencesArray.length > 0) {
      allMyDesignerSelected = compareArray(SelectedDesignersArray, MyDesignerPreferencesArray);
    }
    if (SelectedSizessArray.length > 0 && MySizePreferencesArray.length > 0) {
      allMySizesSelected = compareArray(SelectedSizessArray, MySizePreferencesArray);
    }

    // Block to check if Size and Designers are selected togther
    var MyDesignerSelected = false;
    var MySizesSelected = false;
    var myDesignersUrlNotCombined = URLUtils.https(
      'Search-Show',
      queryOn,
      queryParam,
      'prefn1',
      'brand',
      'prefv1',
      Object.keys(DesignerPreferences || {}).join("|"),
      'srule',
      'featured_newest'
    ).toString();
    var mySizessUrlNotCombined = URLUtils.https(
      'Search-Show',
      queryOn,
      queryParam,
      'prefn1',
      'sizeRefinement',
      'prefv1',
      Object.keys(SizePreferences || {}).join("|"),
      'srule',
      'featured_newest'
    ).toString();

    if (req.querystring.MyDesignerSelected == 'true') {
      MyDesignerSelected = true;
    } else {
      MyDesignerSelected = allMyDesignerSelected; //IF my Deisgner is not selected - But all the Saved desigenrs are selected from the Filters
    }
    if (req.querystring.MySizesSelected == 'true') {
      MySizesSelected = true;
    } else {
      MySizesSelected = allMySizesSelected; //IF my Sizes is not selected - But all the Saved Sizes are selected from the Filters
    }

    if (!allMyDesignerSelected) {
      // check If all My Designers arent selected in the filters - make MyDesignerSelected = FALSE
      MyDesignerSelected = false;
    }
    if (!allMySizesSelected) {
      // check If all My Sizes arent selected in the filters - make MySizesSelected = FALSE
      MySizesSelected = false;
    }

    myPreferencesData[queryOn] = queryParam;
    myPreferencesData["srule"] = "featured_newest";

    var myPreferencesDataWithDesigners = JSON.parse(JSON.stringify(myPreferencesData));
    var myPreferencesDataWithSizes = JSON.parse(JSON.stringify(myPreferencesData));

    myPreferencesDataWithDesigners["brand"] = MyDesignerPreferences;
    var myDesignersUrl = refineSearch.getStoreRefinementUrl(req, 'Search-Show', myPreferencesDataWithDesigners);

    myPreferencesDataWithSizes["sizeRefinement"] = MySizePreferences;
    mySizesUrl = refineSearch.getStoreRefinementUrl(req, 'Search-Show', myPreferencesDataWithSizes);

    var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
    cookiesHelper.createWithoutHttpOnly('myDesignersUrl', myDesignersUrl);
    cookiesHelper.createWithoutHttpOnly('mySizesUrl', mySizesUrl);
    cookiesHelper.createWithoutHttpOnly('myDesignersUrlNotCombined', myDesignersUrlNotCombined);
    cookiesHelper.createWithoutHttpOnly('mySizessUrlNotCombined', mySizessUrlNotCombined);
    cookiesHelper.createWithoutHttpOnly('allMyDesignerSelected', allMyDesignerSelected);
    cookiesHelper.createWithoutHttpOnly('allMySizesSelected', allMySizesSelected);

    if (req.querystring.addToMyPef != 'true') {
      res.setViewData(viewData);
      res.render('/search/searchRefineBar', {
        productSearch: productSearch,
        querystring: req.querystring,
        isRefinedByStore: storeRefineResult.isRefinedByStore,
        hasBrandRefinement: !!hasBrand,
        brandValue: hasBrand || '',
        storeRefineUrl: storeRefineUrl,
        storeRefineUrlForCheckBox: storeRefineUrlForCheckBox,
        isBopisEnabled: preferences.isBopisEnabled,
        isEnabledforSameDayDelivery: preferences.isEnabledforSameDayDelivery,
        setStoreUrl: URLUtils.url('Stores-SetStore').toString(),
        setSDDPostalUrl: URLUtils.url('Stores-SetSDDZipCode').toString(),
        storeModalUrl: URLUtils.url('Stores-InitSearch').toString(),
        searchSource: storeRefineResult.isRefinedByStore ? 'search' : 'store',
        showPriceFilter: req.querystring.pmin || req.querystring.pmax,
        filterCount: filterCount,
        DisableMyPreferencesOnCategory: DisableMyPreferencesOnCategory,
        SizePreferencesExist: SizePreferencesExist,
        DesignerPreferencesExist: DesignerPreferencesExist,
        customerLogin: customerLogin,
        DesignerPreferences: DesignerPreferences || {},
        SizePreferences: SizePreferences || {},
        CurrentCatTempObj: CurrentCatTempObj || {},
        showMySizes: showMySizes,
        categoryToAddInto: categoryToAddInto,
        customerNo: customerNo,
        sizePrefSelectedCount: sizePrefSelectedCount,
        designerPrefSelectedCount: designerPrefSelectedCount,
        MySizesSelected: MySizesSelected,
        MyDesignerSelected: MyDesignerSelected,
        cgid: cgid,
        queryOn: queryOn,
        queryParam: queryParam,
        myDesignersUrl: myDesignersUrl,
        mySizesUrl: mySizesUrl
      });

      next();
    }
  },
  pageMetaData.computedPageMetaData
);

server.get('ShowLogin', server.middleware.https, consentTracking.consent, csrfProtection.generateToken, function (req, res, next) {
  if (req.currentCustomer.profile) {
    res.json({
      redirect: URLUtils.url('Search-Show').relative().toString()
    });
  } else {
    var rememberMe = false;
    var userName = '';
    var URLUtils = require('dw/web/URLUtils');
    var savePreferenceType = req.querystring.savepreferencetype || '';
    var loginActionUrl = URLUtils.url('Account-Login', 'paurl', true, 'savepreferencetype', savePreferenceType);
    var createAccountActionUrl = URLUtils.url('Login-Show', 'action', 'register', 'paurl', true, 'savepreferencetype', savePreferenceType);

    if (req.currentCustomer.credentials) {
      rememberMe = true;
      userName = req.currentCustomer.credentials.username;
    }

    var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
    var rememberMeCookieValue = cookiesHelper.read('RememberMeForced');
    if (rememberMeCookieValue) {
      rememberMe = false;
      userName = '';
    }
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
    var template = renderTemplateHelper.getRenderedHtml(
      {
        rememberMe: rememberMe,
        userName: userName,
        createAccountActionUrl: createAccountActionUrl,
        loginActionUrl: loginActionUrl,
        csrf: res.viewData.csrf,
        oAuthReentryEndpoint: 2
      },
      '/search/searchLogin'
    );
    res.json({
      template: template
    });
  }

  return next();
});
server.append("UpdateGrid", function (req, res, next) {
  this.on('route:BeforeComplete', function (req, res) {
    var viewData = res.getViewData();
    var productSearch = viewData.productSearch;
    if (productSearch && productSearch.category != null && productSearch.category.template === 'rendering/category/producthitsgucci') {
      viewData.enableBadges = false;
      viewData.ratings = false;
      viewData.swatches = false;
    }
  });

  next();
});
//Function to check if all elements in "target" Exist in "arr";
function compareArray(arr, target) {
  var elementsExist = true;
  for (var j = 0; j < target.length; j++) {
    if (arr.indexOf(target[j]) === -1) {
      elementsExist = false;
      break;
    }
  }
  return elementsExist;
}

module.exports = server.exports();
