'use strict';

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var preferences = require('*/cartridge/config/preferences');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var PAGE_SIZE_ITEMS = preferences.defaultPageSize ? preferences.defaultPageSize : 24;
var TOTAL_PAGE_SIZE = preferences.totalPageSize ? preferences.totalPageSize : 96;
var ProductPagination = require('*/cartridge/scripts/helpers/wishlistPagination');
var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
server.extend(module.superModule);

server.append('GetWishlistProduct', function (req, res, next) {
  res.setViewData({
    wishListedMsg: Resource.msg('button.pdp.removetowishlist', 'common', null),
    wishListMsg: Resource.msg('button.pdp.addtowishlist', 'common', null)
  });
  next();
});

server.append('AddProduct', function (req, res, next) {
  res.setViewData({
    wishListedMsg: Resource.msg('button.pdp.removetowishlist', 'common', null),
    wishListMsg: Resource.msg('button.pdp.addtowishlist', 'common', null)
  });
  next();
});

server.append('RemoveProduct', function (req, res, next) {
  res.setViewData({
    wishListedMsg: Resource.msg('button.pdp.removetowishlist', 'common', null),
    wishListMsg: Resource.msg('button.pdp.addtowishlist', 'common', null)
  });
  next();
});

server.get('AddProductModal', function (req, res, next) {
  var requestUuid = req.querystring.uuid;
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var ProductFactory = require('*/cartridge/scripts/factories/product');
  var productToAdd = {
    pid: req.querystring.productID,
    quantity: 1
  };
  var product = ProductFactory.get(productToAdd);
  var context = {
    product: product,
    isEdit: false,
    s7APIForPDPZoomViewer: preferences.s7APIForPDPZoomViewer,
    s7APIForPDPVideoPlayer: preferences.s7APIForPDPVideoPlayer,
    s7VideoServerURL: preferences.s7VideoServerURL,
    s7ImageHostURL: preferences.s7ImageHostURL,
    Zoom: preferences.Zoom,
    template: 'cart/productCard/editProduct',
    addToCartUrl: URLUtils.url('Cart-AddProduct'),
    wishlistAddToBag: true,
    wlPid: req.querystring.productID
  };
  res.setViewData(context);

  this.on('route:BeforeComplete', function (req, res) {
    // eslint-disable-line no-shadow
    var viewData = res.getViewData();

    res.json({
      renderedTemplate: renderTemplateHelper.getRenderedHtml(viewData, viewData.template)
    });
  });

  next();
});

server.append('TileShow', function (req, res, next) {
  var ProductMgr = require('dw/catalog/ProductMgr');
  var ImageModel = require('*/cartridge/models/product/productImages');
  var viewDate = res.viewData;
  var product = res.viewData.product;
  // tile image is fetched from first set product if ProductSet has no image configured
  try {
    var setProducts;
    if (product && product.productType === 'set' && (!product.images || !product.images.medium)) {
      setProducts = ProductMgr.getProduct(product.id);
      product.images = new ImageModel(setProducts, {
        types: ['medium'],
        quantity: 'all'
      });
      if (
        product.images.medium &&
        product.images.medium.length &&
        product.images.medium[0].url &&
        product.images.medium[0].url.indexOf('ImageNotAvailable') > -1 &&
        setProducts.productSetProducts.length > 0
      ) {
        product.images = new ImageModel(setProducts.productSetProducts[0], {
          types: ['medium'],
          quantity: 'all'
        });
      }
    } else if (
      product &&
      product.productType === 'set' &&
      product.images.medium &&
      product.images.medium.length &&
      product.images.medium[0].url &&
      product.images.medium[0].url.indexOf('ImageNotAvailable') > -1
    ) {
      setProducts = ProductMgr.getProduct(product.id);
      if (setProducts.productSetProducts.length > 0) {
        product.images = new ImageModel(setProducts.productSetProducts[0], {
          types: ['medium'],
          quantity: 'all'
        });
      }
    }
    var wishlistTile = req.querystring.wishlistTile;
    if (wishlistTile || req.querystring.view == 'cartWishlistLanding') {
      var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
      var list = productListHelper.getList(req.currentCustomer.raw, {
        type: 10
      });
      var config = {
        qty: 1,
        optionId: null,
        optionValue: null,
        req: req,
        type: 10
      };
      productListHelper.addItem(list, req.querystring.pid, config);
    }
  } catch (e) {
    var Logger = require('dw/system/Logger');
    Logger.error('Error fetching tile image for productSet' + e);
  }
  var viewData = res.getViewData();
  var display = viewData.display;
  if (req.querystring.view == 'cartWishlistLanding') {
    display.swatches = false;
    display.ratings = true;
  } else if (req.querystring.view == 'wishlistedProducts') {
    display.ratings = true;
    display.lazyLoad = true;
  }
  res.setViewData({
    wishlistTile: true,
    isQuickViewEnabled: preferences.isQuickViewEnabled,
    display: display,
    lazyLoad: req.querystring.view == 'wishlistedProducts'
  });
  next();
});

server.append('GetWishlistProducts', function (req, res, next) {
  res.setViewData({
    wishListedMsg: Resource.msg('button.pdp.removetowishlist', 'common', null),
    wishListMsg: Resource.msg('button.pdp.addtowishlist', 'common', null)
  });
  next();
});

server.append('Show', function (req, res, next) {
  res.setViewData({
    wlTilesPerPage: preferences.wlTilesPerPage
  });
  next();
});
module.exports = server.exports();
