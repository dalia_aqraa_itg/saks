'use strict';

var server = require('server');
server.extend(module.superModule);

var system = require('dw/system/System');
var Resource = require('dw/web/Resource');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');

var Status = require('dw/system/Status');
var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');

server.replace('Start', consentTracking.consent, function (req, res, next) {
  //Changes for SFSX-897 - https://hbcdigital.atlassian.net/browse/SFSX-897 - Start
  //This was introduced for SFSX-1504 - https://hbcdigital.atlassian.net/browse/SFSX-1504 - https://github.com/saksdirect/sfcc/tree/saks/bugfixes/SFSX-1504-XSS-SearchFix - Reusing!
  //:TODO - Need to implement for other brands.
  //logic to handle XSS
  //Check if XSS Check in enabled - Merchant Tools --> Site Preferences --> Custom Site Preference Groups --> XSS Input Validation Config -- Enable XSS Input Validation
  var enableXSS =
    Site.current.allowedLocales.length && !empty(Site.current.getCustomPreferenceValue('EnableXSSInputValidation'))
      ? Site.current.getCustomPreferenceValue('EnableXSSInputValidation')
      : false;
  if (enableXSS) {
    //Trigger the xssHelpers check method if XSS Check is enabled.
    var checkCrossSiteScript = require('*/cartridge/scripts/helpers/xssHelpers').check();
    if (checkCrossSiteScript) {
      //If XSS material found in query string forward to Home-ErrorNotFound page.
      response.redirect(URLUtils.url('Home-ErrorNotFound'));
      return new Status(Status.ERROR);
    }
  }
  //Changes for SFSX-897 - https://hbcdigital.atlassian.net/browse/SFSX-897 - end

  var showError = system.getInstanceType() !== system.PRODUCTION_SYSTEM && system.getInstanceType() !== system.STAGING_SYSTEM;
  if (req.httpHeaders.get('x-requested-with') === 'XMLHttpRequest') {
    res.setStatusCode(500);
    res.json({
      error: req.error || {},
      message: Resource.msg('subheading.error.general', 'error', null)
    });
  } else {
    res.setStatusCode(410);
    res.render('error', {
      error: req.error || {},
      showError: showError,
      message: Resource.msg('subheading.error.general', 'error', null)
    });
  }
  next();
});

module.exports = server.exports();
