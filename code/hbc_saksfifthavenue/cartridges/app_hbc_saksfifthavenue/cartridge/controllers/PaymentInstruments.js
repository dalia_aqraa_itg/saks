'use strict';

var server = require('server');
server.extend(module.superModule);

server.append('DeletePayment', function (req, res, next) {
  res.setViewData({ setNextDefaultPayment: true });

  return next();
});

module.exports = server.exports();
