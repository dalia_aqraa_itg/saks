'use strict';
/* eslint-disable no-undef */

var server = require('server');
server.extend(module.superModule);

var cache = require('*/cartridge/scripts/middleware/cache');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');
var preferences = require('*/cartridge/config/preferences');
var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');

server.get(
  'EncodeEmail',
  function (req, res, next) {
    var MessageDigest = require('dw/crypto/MessageDigest');
    var WeakMessageDigest = require('dw/crypto/WeakMessageDigest');
    var messageDigestMD5 = new WeakMessageDigest(WeakMessageDigest.DIGEST_MD5);
    var messageDigetstSHA_256 = new MessageDigest(MessageDigest.DIGEST_SHA_256);
    var email = req.querystring.email;
    var emailSHA256 = email ? dw.crypto.Encoding.toHex(messageDigetstSHA_256.digestBytes(new dw.util.Bytes(email))) : '';
    var eEmailMD5 = email ? dw.crypto.Encoding.toHex(messageDigestMD5.digestBytes(new dw.util.Bytes(email))) : '';
    res.json({
      md5: eEmailMD5,
      sha256: emailSHA256
    });
    next();
  }
);

server.get(
  'ShowMens',
  consentTracking.consent,
  cache.applyDefaultCache,
  function (req, res, next) {
    var Site = require('dw/system/Site');
    var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');

    pageMetaHelper.setPageMetaTags(req.pageMetaData, Site.current);
    res.setViewData({ promoTrayEnabled: preferences.promoTrayEnabled, action: 'Home-ShowMens' });
    res.render('/home/mensHomePage');
    next();
  },
  pageMetaData.computedPageMetaData
);

server.get('SwitchPreference', function (req, res, next) {
  var shopperPreference = req.querystring.shopperPreference;
  var URLUtils = require('dw/web/URLUtils');
  var returnUrl;
  if (shopperPreference === 'men') {
    returnUrl = URLUtils.url('Home-ShowMens').toString();
    cookiesHelper.createWithoutHttpOnly('shopPreference', 'men');
    session.custom.shopPreference = 'men';
  } else {
    returnUrl = URLUtils.url('Home-Show').toString();
    cookiesHelper.createWithoutHttpOnly('shopPreference', 'women');
    session.custom.shopPreference = 'women';
  }
  res.json({
    redirectUrl: returnUrl
  });
  next();
});

server.get('COMX', function (req, res, next) {
  var readSiteRefer = cookiesHelper.read('site_refer');
  if (readSiteRefer && readSiteRefer == 'COMX') {
    if (empty(session.custom.COMX)) {
      session.custom.COMX = true;
    }
  } else {
    delete session.custom.COMX;
  }
  res.json({});
  next();
});

server.append('CSRBanner', function (req, res, next) {
  var Resource = require('dw/web/Resource');
  var privateSaleLabel = Resource.msg('button.text.viewprivatesale', 'common', null);
  if ('csrPrivateSale' in session.custom && session.custom.csrPrivateSale) {
    privateSaleLabel = Resource.msg('button.text.disableprivatesale', 'common', null);
  } else if ('csrPrivateSale' in session.custom && !session.custom.csrPrivateSale) {
    privateSaleLabel = Resource.msg('button.text.noprivatesale', 'common', null);
  }

  res.setViewData({
    privateSaleLabel: privateSaleLabel
  });
  res.render('components/header/csrBanner');
  next();
});

server.get('CSRPrivateSale', function (req, res, next) {
  var URLUtils = require('dw/web/URLUtils');
  var redirectURL;
  if (req.session.raw.isUserAuthenticated()) {
    if ('csrPrivateSale' in session.custom && session.custom.csrPrivateSale) {
      // Toggle the CSR to disable private sale
      delete session.custom.csrPrivateSale;
    } else {
      // check if the private sale campaign is running
      var promotionHelper = require('*/cartridge/scripts/util/promotionHelper');
      promotionHelper.setPromoCache();
      if ('privateSalePromoID' in session.privacy && session.privacy.privateSalePromoID) {
        session.custom.csrPrivateSale = true;
      } else {
        session.custom.csrPrivateSale = false;
      }
    }
  }
  if (!session.custom.shopPreference || session.custom.shopPreference == 'women') {
    redirectURL = URLUtils.url('Home-Show');
  } else {
    redirectURL = URLUtils.url('Home-ShowMens');
  }
  res.redirect(redirectURL);
  next();
});

server.get('HomePageContentSlots', function (req, res, next) {
  res.render('/home/homePageContentSlots');
  next();
});

module.exports = server.exports();
