'use strict';

var server = require('server');
server.extend(module.superModule);

var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var addressHelper = require('*/cartridge/scripts/helpers/addressHelper');
var preferences = require('*/cartridge/config/preferences');
var ShippingHelper = require('*/cartridge/scripts/checkout/shippingHelpers');
var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');
var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');

/**
 * Main entry point for Checkout
 */

server.get('Login', server.middleware.https, consentTracking.consent, csrfProtection.generateToken, function (req, res, next) {
  var URLUtils = require('dw/web/URLUtils');
  var emptyCart = req.querystring.emptyCart ? req.querystring.emptyCart : false;
  // If this is BFX Order, Redirect customer to Cart Page.
  var bfxCountryCode = cookiesHelper.read('bfx.country');
  if (bfxCountryCode && bfxCountryCode !== 'US' && !emptyCart) {
    res.json({
      redirect: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }
  var saksplusHelper = require('*/cartridge/scripts/helpers/saksplusHelper');
  var BasketMgr = require('dw/order/BasketMgr');
  var basket = BasketMgr.getCurrentBasket();
  var hasSaksPlus = saksplusHelper.hasSaksPlusInBasket(basket);
  if (req.currentCustomer.profile && !emptyCart) {
    res.json({
      redirect: URLUtils.url('Checkout-Begin').toString()
    });
  } else {
    var rememberMe = false;
    var userName = '';
    var actionUrl = URLUtils.url('Account-Login', 'rurl', 2);

    if (req.currentCustomer.credentials) {
      rememberMe = true;
      userName = req.currentCustomer.credentials.username;
    }

    // Read the RememberMeForced cookies if true clear the remember me and user name
    var viewData = res.getViewData();
    var cookieValue = cookiesHelper.read('RememberMeForced');
    if (cookieValue) {
      rememberMe = false;
      userName = '';
    }
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
    var template = renderTemplateHelper.getRenderedHtml(
      {
        rememberMe: rememberMe,
        userName: userName,
        actionUrl: actionUrl,
        csrf: res.viewData.csrf,
        emptyCart: emptyCart,
        oAuthReentryEndpoint: 2,
        hasSaksPlus: hasSaksPlus
      },
      '/checkout/checkoutLogin'
    );
    res.json({
      template: template
    });
  }

  return next();
});

// Main entry point for Checkout
server.replace('Begin', server.middleware.https, consentTracking.consent, csrfProtection.generateToken, function (req, res, next) {
  var URLUtils = require('dw/web/URLUtils');
  // SFDEV-10355 | XSS check for the parameter stage
  var applicableStages = 'shipping|pickupperson|placeOrder|payment|createaccount|submitted';
  var stage = req.querystring.stage;
  if (stage != null && stage != undefined && applicableStages.indexOf(stage) === -1) {
    res.redirect(URLUtils.https('Error-Start'));
    return next();
  }
  var currentStage = req.querystring.stage ? req.querystring.stage : 'shipping';
  var BasketMgr = require('dw/order/BasketMgr');
  var Transaction = require('dw/system/Transaction');
  var AccountModel = require('*/cartridge/models/account');
  var OrderModel = require('*/cartridge/models/order');
  var Site = require('dw/system/Site');
  var reportingUrlsHelper = require('*/cartridge/scripts/reportingUrls');
  var Locale = require('dw/util/Locale');
  var collections = require('*/cartridge/scripts/util/collections');
  var validationHelpers = require('*/cartridge/scripts/helpers/basketValidationHelpers');
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
  var commissionCookiesHelper = require('*/cartridge/scripts/helpers/commissionCookies');
  var PropertyComparator = require('dw/util/PropertyComparator');
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var amexpayHelper = require('*/cartridge/scripts/helpers/amexpayHelper');
  var ArrayList = require('dw/util/ArrayList');
  var Money = require('dw/value/Money');
  var formatMoney = require('dw/util/StringUtils').formatMoney;
  var currentBasket = BasketMgr.getCurrentBasket();
  var hasInStoreItems = null;
  var giftWrapItems = [];
  var instorePickUpData = {},
    addressData = {};
  var customerProfile;
  var skipShipping = false;
  var giftMessageAction = '';
  var giftEligible = '';
  var sthShipment = null;
  var selectedAddress = null;
  var appliedDropShipID = null;
  var hasUSPSRestrictedItems = false;
  var restrictedStates = [];
  var restrictedStatesUSPS = '';
  var signatureRequired = false;
  var openAddressEntry = false;
  var hasPreOrderItems = false;
  var firstAddressCountry = '';
  var giftPackChargeAndThreshold = {};
  var srTokenCookieValue = cookiesHelper.read('sr_token');
  var hasDropShipItems = false;
  if (srTokenCookieValue === null || srTokenCookieValue === undefined || srTokenCookieValue === '') {
    srTokenCookieValue = false;
  }
  i;
  if (!currentBasket) {
    res.redirect(URLUtils.url('Cart-Show'));
    return next();
  }

  if (!currentBasket.getCustomer().authenticated && currentStage != 'placeOrder') {
    COHelpers.clearDataWhenRefresh();
  }

  // If this is BFX Order, Redirect customer to Cart Page.
  var bfxCountryCode = cookiesHelper.read('bfx.country');
  if (bfxCountryCode && bfxCountryCode !== 'US') {
    res.redirect(URLUtils.url('Cart-Show'));
    return next();
  }

  var siteReferer = cookiesHelper.read('site_refer');
  if (siteReferer && siteReferer == 'COMX') {
    var storedata = commissionCookiesHelper.getAllStoreData();
    if (storedata) {
      res.setViewData({
        comexEnabled: true,
        COMXstoredata: storedata
      });
    }
  } else {
    delete currentBasket.custom.COMXData;
  }

  // Clear up Canada Tax if not Save the shipping address
  cartHelper.clearCanadaTax(currentBasket);
  // Clear email optin
  req.session.privacyCache.set('email_opt_in', false);
  var validatedProducts = validationHelpers.validateCheckoutProducts(currentBasket, req, true);
  // validates SFCC inventory is already notified
  if (validatedProducts.notifyError) {
    res.redirect(URLUtils.url('Checkout-Begin'));
    return next();
  }
  if (!validatedProducts.error) {
    // validate inventory with source on cart submit, fall back is OOTB inventory check
    validatedProducts = hooksHelper('app.validate.validate', 'validateInventory', [currentBasket]);
    if (validatedProducts && !validatedProducts.errorInService && !validatedProducts.hasInventory) {
      if (validatedProducts.omsInventory && validatedProducts.omsInventory.length > 0) {
        var omsInventory = JSON.stringify(validatedProducts.omsInventory);
        session.custom.omsInventory = omsInventory;
        cartHelper.adjustBasketQuantities(currentBasket, validatedProducts.omsInventory);
      }
      res.redirect(URLUtils.url('Cart-Show'));
      return next();
    }

    validationHelpers.removeInvalidCoupon(currentBasket);
  }
  if (!currentBasket || validatedProducts.error) {
    res.redirect(URLUtils.url('Cart-Show'));
    return next();
  }
  if (session.custom.omsInventory && session.custom.omsInventory.length > 0) {
    delete session.custom.omsInventory;
  }
  var saksplusHelper = require('*/cartridge/scripts/helpers/saksplusHelper');
  var hasSaksPlus = saksplusHelper.hasSaksPlusInBasket(currentBasket);

  if (
    ('saksplusProduct' in session.custom && session.custom.saksplusProduct) ||
    (req.currentCustomer && req.currentCustomer.raw.profile && req.currentCustomer.raw.profile.custom['saks+Member'])
  ) {
    ShippingHelper.applySAKSPlusDefaultShipMethod(currentBasket.defaultShipment);
  }

  if (!(req.currentCustomer && req.currentCustomer.raw.authenticated) && hasSaksPlus && !('emailaddress' in session.custom) && currentBasket.customerEmail) {
    session.custom.emailaddress = currentBasket.customerEmail;
  }

  if (preferences.giftWrapAmount && preferences.freeGiftWrapThreshold) {
    giftPackChargeAndThreshold.charge = formatMoney(new Money(preferences.giftWrapAmount, currentBasket.getCurrencyCode()));
    giftPackChargeAndThreshold.limit = formatMoney(new Money(preferences.freeGiftWrapThreshold, currentBasket.getCurrencyCode()));
  }
  let shopRunnerEnabled = preferences.shopRunnerEnabled;
  // Check and reset the flag based on if basket qualifies for shop runner checkout.
  if (shopRunnerEnabled) {
    var shopRunnerHelper = require('*/cartridge/scripts/helpers/shopRunnerHelpers');
    shopRunnerEnabled = shopRunnerHelper.checkSRExpressCheckoutEligibility(currentBasket);
  }
  if (shopRunnerEnabled) {
    var shoprunnerShippingMethodSelection = require('*/cartridge/scripts/ShoprunnerShippingMethodSelection').ShoprunnerShippingMethodSelection;
    shoprunnerShippingMethodSelection();
  }
  // apply drop ship default shipping method on load
  //appliedDropShipID = ShippingHelper.applyDropShipDefaultMethod(currentBasket.defaultShipment);
  // Inauth code
  var HBCUtils = require('*/cartridge/scripts/HBCUtils');
  var inAuthDetails = HBCUtils.getInAuthDetails(req);
  res.setViewData({
    inAuthDetails: inAuthDetails
  });

  var orderModel = new OrderModel(currentBasket, {
    containerView: 'basket'
  });
  if (orderModel.shipping && orderModel.shipping.length > 0) {
    ShippingHelper.applyDropShipDefaultMethod(orderModel.shipping[orderModel.shipping.length - 1], srTokenCookieValue, shopRunnerEnabled);
  }

  var sddEligibleItems = orderModel.items.sddEligibleItems;
  res.setViewData({
    sddEligibleItems: sddEligibleItems
  });

  // Card Tokenization enabled
  var tokenEnabled =
    'enableTokenEx' in Site.current.preferences.custom && Site.current.preferences.custom.enableTokenEx ? Site.current.preferences.custom.enableTokenEx : false;
  var tokenPublicKey =
    'tokenExPublicKey' in Site.current.preferences.custom && Site.current.preferences.custom.tokenExPublicKey
      ? Site.current.preferences.custom.tokenExPublicKey
      : '';

  // Vertax Code
  delete session.custom.VertexAddressSelected;

  var isExpressCheckout = req.querystring.type && req.querystring.type === 'express';

  var billingAddress = currentBasket.billingAddress;

  var currentCustomer = req.currentCustomer.raw;
  var currentLocale = Locale.getLocale(req.locale.id);
  var preferredAddress;

  // check if the basket has drop ship items
  hasDropShipItems = COHelpers.hasDropShipItems(currentBasket);
  // Express Checkout

  // amex pwp modal on express checkout
  if (session.custom.isExpressCheckout) {
    // if the credit card submitted was Amex, check for the points.
    if (preferences.isAmexPayEnabled) {
      var pi = amexpayHelper.hasAmexCreditCard(currentBasket);
      var nonGCTotal = COHelpers.getNonGiftCardAmount(currentBasket, null);
      if (pi && nonGCTotal.remainingAmount.value > 0) {
        Transaction.wrap(function () {
          delete pi.custom.paywithPointsResponse;
          delete pi.custom.amexAmountApplied;
        });
        amexpayHelper.getRewardPoints(pi);
        delete session.custom.isExpressCheckout;
      }
    }
  }

  if (req.querystring && !req.querystring.mpstatus && req.querystring.type !== 'express' && req.querystring.stage !== 'placeOrder') {
    if (preferences.enableExpressCheckout && req.currentCustomer.raw.authenticated) {
      var expressCheckoutEligible = true;
      var basketHashHelper = require('*/cartridge/scripts/helpers/basketHashHelpers');
      var basketHashBeforeCustAddressUpdate = basketHashHelper.generateBasketHash(currentBasket);
      req.session.privacyCache.set('basketHashBeforeCustAddressUpdate', basketHashBeforeCustAddressUpdate);

      var basketHashBefore = basketHashBeforeCustAddressUpdate;
      var basketHashAfter = req.session.privacyCache.get('basketHashAfterCustAddressUpdate');

      if (basketHashAfter && basketHashAfter !== basketHashBefore) {
        expressCheckoutEligible = false;
      }

      if (expressCheckoutEligible && COHelpers.eligibleForExpressCheckout(req.currentCustomer.raw, currentBasket, req)) {
        // Check for Associate Discount
        if (currentBasket.paymentInstruments && currentBasket.paymentInstruments.length > 0) {
          var paymentInstruments = currentBasket.paymentInstruments;
          for (var i = 0; i < paymentInstruments.length; i++) {
            var expressPayment = paymentInstruments[i];
            if (
              expressPayment.creditCardType &&
              (expressPayment.creditCardType == 'SAKS' ||
                expressPayment.creditCardType == 'SAKSMC' ||
                expressPayment.creditCardType == 'HBC' ||
                expressPayment.creditCardType == 'HBCMC' ||
                expressPayment.creditCardType == 'MPA' ||
                expressPayment.creditCardType == 'TCC')
            ) {
              if (expressPayment.creditCardToken) {
                hooksHelper('ucid.middleware.service', 'associateDiscountCode', [req, expressPayment.creditCardToken]);
                req.session.raw.custom.isHBCTenderType = true;
              }
            } else {
              delete req.session.raw.custom.associateTier;
              delete req.session.raw.custom.specialVendorDiscountTier;
              req.session.raw.custom.isHBCTenderType = false;
            }
            // Set the entry mode for express checkout
            if (expressPayment.creditCardToken) {
              Transaction.wrap(function () {
                expressPayment.custom.entry_mode = ipaConstants.STORED_MODE;
              });
            }
          }
        }
        if (preferences.promptShippingEnabled) {
          res.redirect(URLUtils.https('Checkout-Begin', 'stage', 'shipping', 'type', 'express'));
        } else {
          res.redirect(URLUtils.https('Checkout-Begin', 'stage', 'placeOrder'));
        }
        delete session.custom.isExpressCheckout;
        session.custom.isExpressCheckout = true;
        return next();
      }
    }
  }

  // only true if customer is registered
  if (req.currentCustomer.addressBook && req.currentCustomer.addressBook.addresses.length === 0) {
    openAddressEntry = true;
  }

  if (req.currentCustomer.addressBook && req.currentCustomer.addressBook.addresses.length === 1) {
    firstAddressCountry = req.currentCustomer.addressBook.addresses[0].countryCode.value;
  }
  if (req.currentCustomer.addressBook && req.currentCustomer.addressBook.preferredAddress) {
    var shipments = currentBasket.shipments;
    preferredAddress = req.currentCustomer.addressBook.preferredAddress;

    // Vertax Specific Code
    var addressBook = customer.getProfile().getAddressBook();
    var rawAddress = addressBook.getAddress(preferredAddress.ID);
    if (rawAddress.custom.taxnumber) {
      preferredAddress.taxnumber = rawAddress.custom.taxnumber;
    }

    collections.forEach(shipments, function (shipment) {
      if (!shipment.shippingAddress || !shipment.shippingAddress.address1) {
        COHelpers.copyCustomerAddressToShipment(preferredAddress, shipment);
      }
      // get STH shipment
      if (!shipment.custom.shipmentType || shipment.custom.shipmentType !== 'instore') {
        sthShipment = shipment;
      }
      // Vertax Specif Code
      if (shipment.shippingAddress && !shipment.shippingAddress.custom.taxnumber && !!preferredAddress.taxnumber) {
        addressHelper.addTaxNumber(preferredAddress, shipment);
      }
    });

    /* if (!billingAddress || !billingAddress.address1) {
                COHelpers.copyCustomerAddressToBilling(preferredAddress);
            }*/
  }
  // prepare instore pickup info form profile if authenticated
  if (req.currentCustomer.profile) {
    customerProfile = req.currentCustomer.profile;
    instorePickUpData.fullName = customerProfile.firstName + ' ' + customerProfile.lastName;
    instorePickUpData.email = customerProfile.email;
    instorePickUpData.phone = customerProfile.phone;
    addressData.email = customerProfile.email;
  }
  // get address form address book that matches the shipping address
  if (req.currentCustomer.profile && req.currentCustomer.addresses && req.currentCustomer.addresses.length > 0 && sthShipment) {
    selectedAddress = COHelpers.getSelectedCustomerAddress(sthShipment.shipingAddress, req.currentCustomer.addresses);
  }
  // Calculate the basket
  Transaction.wrap(function () {
    COHelpers.ensureNoEmptyShipments(req);
  });

  if (currentBasket.shipments.length <= 1) {
    req.session.privacyCache.set('usingMultiShipping', false);
  }

  if (currentBasket.currencyCode !== req.session.currency.currencyCode) {
    Transaction.wrap(function () {
      currentBasket.updateCurrency();
    });
  }

  COHelpers.recalculateBasket(currentBasket);

  var shippingForm = COHelpers.prepareShippingForm(currentBasket);
  var billingForm = COHelpers.prepareBillingForm(currentBasket);
  var instorePickUpForm = COHelpers.prepareInstorePickupForm();
  var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
  var createaccountForm = server.forms.getForm('createaccount');

  if (preferredAddress) {
    shippingForm.copyFrom(preferredAddress);
    billingForm.copyFrom(preferredAddress);
  }

  // pre-populate data from profile
  if (instorePickUpData) {
    instorePickUpForm.copyFrom(instorePickUpData);
  }

  // Loop through all shipments and make sure all are valid
  var allValid = COHelpers.ensureValidShipments(currentBasket);

  // Changes done for Express paypal for cart with only bopis shipment
  var inStoreItemsInCart = [];
  if (currentBasket.productLineItems && currentBasket.productLineItems.length) {
    collections.forEach(currentBasket.productLineItems, function (productLineItem) {
      if (productLineItem.custom.fromStoreId) {
        inStoreItemsInCart.push(productLineItem);
      }
    });
  }

  var pickUpPersonData = {};
  if (currentStage === 'placeOrder') {
    var paypalPI = currentBasket.getPaymentInstruments('PayPal');
    if (
      paypalPI &&
      !empty(inStoreItemsInCart) &&
      inStoreItemsInCart.length === currentBasket.productLineItems.size() &&
      empty(currentBasket.custom.personInfoMarkFor)
    ) {
      pickUpPersonData = {
        fullName: billingAddress.fullName,
        email: currentBasket.customerEmail,
        phone: billingAddress.phone
      };
      Transaction.wrap(function () {
        currentBasket.custom.personInfoMarkFor = JSON.stringify(pickUpPersonData);
      });
    }
  }

  // Changes done for Express paypal for cart with only bopis shipment
  var inStoreItemsInCart = [];
  if (currentBasket.productLineItems && currentBasket.productLineItems.length) {
    collections.forEach(currentBasket.productLineItems, function (productLineItem) {
      if (productLineItem.custom.fromStoreId) {
        inStoreItemsInCart.push(productLineItem);
      }
    });
  }

  var pickUpPersonData = {};
  if (currentStage === 'placeOrder') {
    var paypalPI = currentBasket.getPaymentInstruments('PayPal');
    if (
      paypalPI &&
      !empty(inStoreItemsInCart) &&
      inStoreItemsInCart.length === currentBasket.productLineItems.size() &&
      empty(currentBasket.custom.personInfoMarkFor)
    ) {
      pickUpPersonData = {
        fullName: billingAddress.fullName,
        email: currentBasket.customerEmail,
        phone: billingAddress.phone
      };
      Transaction.wrap(function () {
        currentBasket.custom.personInfoMarkFor = JSON.stringify(pickUpPersonData);
      });
    }
  }

  var orderModel = new OrderModel(currentBasket, {
    customer: currentCustomer,
    usingMultiShipping: usingMultiShipping,
    shippable: allValid,
    countryCode: currentLocale.country,
    containerView: 'basket'
  });
  hasInStoreItems = orderModel.items.hasInStoreItems;
  // usps restricted items
  hasUSPSRestrictedItems = orderModel.items.hasUSPSRestrictedItems;
  // restricted states
  restrictedStates = orderModel.items.restrictedStates;
  hasPreOrderItems = orderModel.items.hasPreOrderItems;
  if (preferences.restrictedStatesUSPS && preferences.restrictedStatesUSPS.length > 0) {
    restrictedStatesUSPS = preferences.restrictedStatesUSPS;
  }

  var isshpmntSignatureRequired = false;
  if (
    shippingForm != null &&
    shippingForm.shippingAddress != null &&
    shippingForm.shippingAddress.signatureRequired != null &&
    shippingForm.shippingAddress.signatureRequired.checked
  ) {
    isshpmntSignatureRequired = true;
  }
  // set signature at shipment
  COHelpers.setSignatureRequired(orderModel, currentBasket, currentBasket.defaultShipment, isshpmntSignatureRequired);
  if (orderModel.shipping != null && !empty(orderModel.shipping)) {
    orderModel.shipping[0].signatureRequired =
      !empty(currentBasket.defaultShipment) &&
      'signatureRequired' in currentBasket.defaultShipment.custom &&
      currentBasket.defaultShipment.custom.signatureRequired
        ? currentBasket.defaultShipment.custom.signatureRequired
        : false;
  }

  // if signatre reqires
  signatureRequired = preferences.signatureRequired && orderModel.items.signatureRequired;
  if (hasInStoreItems.length > 0 && hasInStoreItems.length === currentBasket.productLineItems.size()) {
    skipShipping = true;
  }

  giftWrapItems = orderModel.items.giftWrapItems;

  if (preferences.giftWrapDisabled) {
    // If Gift Wrap is globally Disabled. Hide IT
    giftMessageAction = 'hide';
  } else if (giftWrapItems.length > 0 && giftWrapItems.length === currentBasket.productLineItems.length) {
    // If all the line item has gift wrap disabled.
    giftMessageAction = 'hide';
    giftEligible = 'none';
  } else if (giftWrapItems.length > 0 && giftWrapItems.length < currentBasket.productLineItems.length) {
    giftMessageAction = 'error';
  }
  // add new checkout stage if bopis is enabled and has instore items in basket
  if (preferences.isBopisEnabled) {
    currentStage = req.querystring.stage ? req.querystring.stage : hasInStoreItems && hasInStoreItems.length > 0 ? 'pickupperson' : 'shipping'; // eslint-disable-line
  }
  // Get rid of this from top-level ... should be part of OrderModel???
  var currentYear = new Date().getFullYear();
  var creditCardExpirationYears = [];

  for (var j = 0; j < 10; j++) {
    creditCardExpirationYears.push(currentYear + j);
  }

  var accountModel = new AccountModel(req.currentCustomer);
  var defaultCreditCard = null;
  if (accountModel.registeredUser && !empty(accountModel.customerPaymentInstruments)) {
    var paymentInstruments = new ArrayList(accountModel.customerPaymentInstruments);
    paymentInstruments.sort(new PropertyComparator('defaultCreditCard', false));
    defaultCreditCard = paymentInstruments[0];
  }

  var reportingURLs;
  reportingURLs = reportingUrlsHelper.getCheckoutReportingURLs(currentBasket.UUID, 2, 'Shipping');
  // Append PayPal Start
  if (!currentBasket) {
    return next();
  }

  var paypalForm = server.forms.getForm('billing').paypal;

  if (dw.web.Resource.msg('paypal.version.number', 'paypal_version', null) == '18.3.1') {
    var paypalHelper = require('*/cartridge/scripts/paypal/paypalHelper');
    var paypalPaymentInstrument = paypalHelper.getPaypalPaymentInstrument(currentBasket);
    var prefs = paypalHelper.getPrefs();
    var isUsedBillingAgreement = paypalPaymentInstrument && paypalPaymentInstrument.custom.paypalBillingAgreement === true;
    var customerBillingAgreement = paypalHelper.getCustomerBillingAgreement(currentBasket.getCurrencyCode());
    var customerBillingAgreementEmail = null;
    var summaryEmail = null;
    if (customerBillingAgreement.hasAnyBillingAgreement) {
      customerBillingAgreementEmail = paypalHelper.getCustomerBillingAgreement(currentBasket.getCurrencyCode()).getDefault().email;
    }
    if (isUsedBillingAgreement) {
      summaryEmail = customerBillingAgreementEmail;
    } else if (currentBasket.custom.paypalAlreadyHandledEmail) {
      summaryEmail = customerBillingAgreementEmail;
    }

    var buttonConfig = prefs.PP_Billing_Button_Config;
    buttonConfig.env = prefs.environmentType;

    res.setViewData({
      paypal: {
        prefs: prefs,
        buttonConfig: buttonConfig,
        form: paypalForm,
        currency: currentBasket.getCurrencyCode(),
        isCustomerHasAnyBillingAgreement: customerBillingAgreement.hasAnyBillingAgreement,
        customerBillingAgreementEmail: customerBillingAgreementEmail,
        isUsedBillingAgreement: isUsedBillingAgreement,
        alreadyHandledToken: currentBasket.custom.paypalAlreadyHandledToken,
        alreadyHandledEmail: currentBasket.custom.paypalAlreadyHandledEmail,
        isNeedHideContinueButton: !(
          currentBasket.custom.paypalAlreadyHandledToken ||
          (customerBillingAgreement.hasAnyBillingAgreement && paypalForm.useCustomerBillingAgreement.checked)
        ),
        summaryEmail: summaryEmail,
        hasDropShipItems: hasDropShipItems
      }
    });
    // PayPal Append End..
  }

  // Cart Merge
  var isCartMerged = false;
  if (req.session.privacyCache.get('basketMerged')) {
    isCartMerged = true;
    req.session.privacyCache.set('basketMerged', null);
  }

  var addressHelpers = require('*/cartridge/scripts/helpers/addressHelpers');
  var addressForm = server.forms.getForm('address');
  var countryRegion = addressHelpers.getCountriesAndRegions(addressForm.base);

  // identify agent
  var isAgentUser = false;
  if (req.session.raw.isUserAuthenticated()) {
    isAgentUser = true;
  }

  // code to show hudson reward number
  let hudsonRewardNumber =
    currentBasket.custom.hudsonRewardNumber ||
    (req.currentCustomer.raw.authenticated &&
    req.currentCustomer.raw.profile != null &&
    'hudsonReward' in req.currentCustomer.raw.profile.custom &&
    !!req.currentCustomer.raw.profile.custom.hudsonReward
      ? req.currentCustomer.raw.profile.custom.hudsonReward
      : '');
  if (hudsonRewardNumber) {
    Transaction.wrap(function () {
      currentBasket.custom.hudsonRewardNumber = hudsonRewardNumber;
    });
    hudsonRewardNumber = hudsonRewardNumber.toString().replace(/^([\d]{3})([\d]{3})/, '$1 $2 ');
  }

  res.render('checkout/checkout', {
    countryCode: Locale.getLocale(req.locale.id).ISO3Country,
    country2Code: Locale.getLocale(req.locale.id).country,
    countryRegion: JSON.stringify(countryRegion),
    order: orderModel,
    customer: accountModel,
    forms: {
      instorepickupform: instorePickUpForm,
      shippingForm: shippingForm,
      billingForm: billingForm,
      createaccountForm: createaccountForm
    },
    expirationYears: creditCardExpirationYears,
    srTokenCookieValue: srTokenCookieValue,
    currentStage: currentStage,
    reportingURLs: reportingURLs,
    tokenEnabled: tokenEnabled,
    tokenPublicKey: tokenPublicKey,
    isCartMerged: isCartMerged,
    shopRunnerEnabled: shopRunnerEnabled,
    counter: 'counter' in req.session.raw.custom ? req.session.raw.custom.counter : 0,
    isBopisEnabled: preferences.isBopisEnabled,
    skipShipping: skipShipping,
    hasInStoreItems: hasInStoreItems,
    selectedAddress: selectedAddress,
    giftMessageAction: giftMessageAction,
    appliedDropShipID: appliedDropShipID,
    currency: currentBasket.getCurrencyCode(),
    hasUSPSRestrictedItems: hasUSPSRestrictedItems,
    restrictedStates: restrictedStates,
    passwordRegex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#_?!@$%^()+=~`}{|&*-])^[^'<>/]{8,}$",
    signatureRequired: signatureRequired,
    includeRecaptchaJS: true,
    signatureThreshold: preferences.signatureOrderThreshold,
    defaultCreditCard: defaultCreditCard,
    isExpressCheckout: isExpressCheckout,
    hudsonRewardNumber: hudsonRewardNumber,
    hudsonRewardPrefix: preferences.hudsonReward,
    openAddressEntry: openAddressEntry,
    siteLocale: Locale.getLocale(req.locale.id).ID,
    firstAddressCountry: firstAddressCountry,
    addressData: addressData,
    giftEligible: giftEligible,
    giftPackChargeAndThreshold: giftPackChargeAndThreshold,
    hasPreOrderItems: hasPreOrderItems,
    hasSaksPlus: hasSaksPlus,
    giftWrapEnabledforBopis: preferences.giftWrapEnabledforBopis,
    isAgentUser: isAgentUser,
    isAmexPayEnabled: preferences.isAmexPayEnabled,
    saksFirstEnabled: preferences.saksFirstEnabled,
    disableSaksFirstAtCheckout: preferences.disableSaksFirstAtCheckout,
    csrFreeGiftUrl: URLUtils.https('Checkout-CSRGiftWrapFree'),
    restrictedStatesUSPS: restrictedStatesUSPS,
    showDSMsg: session.custom.showshippingmsg,
    isTsysMode: tsysHelper.isTsysMode()
  });

  return next();
});

// set session variable to set free gift wrap
server.get('CSRGiftWrapFree', function (req, res, next) {
  this.on('route:BeforeComplete', function (req, res) {
    // eslint-disable-line no-shadow
    session.custom.csrGiftWrap = true;
    res.json({
      success: true
    });
  });
  next();
});

module.exports = server.exports();
