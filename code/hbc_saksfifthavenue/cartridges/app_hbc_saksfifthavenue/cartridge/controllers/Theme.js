"use strict";
/**
 * Controller that returns Thematic Content.
 *
 * @module controllers/Theme
 */

var server = require("server");
server.extend(module.superModule);

var Site = require("dw/system/Site");
var refineSearch = require('*/cartridge/models/bopis/refineSearch');

server.append("Show", function (req, res, next) {
    var viewData = res.getViewData();
    var brThematic = viewData.brThematic;
    if(!empty(brThematic) && !empty(brThematic.page_header)) {
        viewData.robotsMeta = brThematic.page_header.robots_meta_tag || ''; 
    }
    next();
});

server.append("UpdateGrid", function (req, res, next) {
    var viewData = res.getViewData();
    var brThematic = viewData.brThematic;
    if(!empty(brThematic) && !empty(brThematic.page_header)) {
        viewData.robotsMeta = brThematic.page_header.robots_meta_tag || ''; 
    }
    next();
  
});

module.exports = server.exports();
