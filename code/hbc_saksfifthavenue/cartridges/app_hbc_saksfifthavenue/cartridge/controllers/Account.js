// @ts-check
'use strict';

var server = require('server');
server.extend(module.superModule);

var saksFirstHelpers = require('*/cartridge/scripts/helpers/saksFirstHelpers');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');
var MyAccountLogger = require('dw/system/Logger').getLogger('MyAccount', 'MyAccount');

server.replace('Login', server.middleware.https, csrfProtection.validateAjaxRequest, function (req, res, next) {
  var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var Resource = require('dw/web/Resource');
  var Transaction = require('dw/system/Transaction');
  var BasketMgr = require('dw/order/BasketMgr');
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
  var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
  var HookMgr = require('dw/system/HookMgr');
  var preferences = require('*/cartridge/config/preferences');
  var Logger = require('dw/system/Logger');
  var capOneUtils = require('*/cartridge/scripts/util/capOneUtils');

  if (session.custom.isbot != null && session.custom.isbot) {
    res.json({
      error: [Resource.msg('error.message.login.bot', 'login', null)],
      botError: true
    });

    return next();
  }

  // Logger.debug('start Login !!!' + req.form.token);
  var result = hooksHelper('app.google.recaptcha.verify', 'validateToken', req.form.token, require('*/cartridge/scripts/googleRecaptchaUtil').validateToken);
  // Logger.debug('end Login !!!' + result.score);
  // Logger.debug('session.custom.isbot' + session.custom.isbot);

  if (session.custom.isbot) {
    res.json({
      error: [Resource.msg('error.message.login.bot', 'login', null)],
      botError: true
    });

    return next();
  }

  var viewData = res.getViewData();
  var list = productListHelper.getList(req.currentCustomer.raw, {
    type: 10
  });
  viewData.list = list;
  res.setViewData(viewData);
  var email = req.form.loginEmail;
  var password = req.form.loginPassword;
  var rememberMe = req.form.loginRememberMe ? !!req.form.loginRememberMe : false;
  // Cart Merge Logic
  if (!rememberMe) {
    // Enforce remember me by setting cookies
    cookiesHelper.create('RememberMeForced', 'true');
    rememberMe = true;
  } else {
    // Already Remember me is selected.
    cookiesHelper.deleteCookie('RememberMeForced');
  }

  var customer = CustomerMgr.getCustomerByLogin(email);
  var profile = null;
  var customerLoginResult;
  var islocked = false;
  if (customer !== null) {
    profile = customer.getProfile();
  }

  if (tsysHelper.isTsysMode()) {
    /* Check if the user is applying for a SaksFirst Credit Card and is a Canadian customer */
    var customerIsApply = req.querystring.apply;
    var customerIsCanadian = false;
    if (profile !== null && 'isCanadianCustomer' in profile.custom && profile.custom.isCanadianCustomer !== null) {
      customerIsCanadian = profile.custom.isCanadianCustomer;
    }
    /* Display an error if the user is applying for a SaksFirst Credit Card and is a Canadian customer */
    if (customerIsApply && customerIsCanadian) {
      res.json({
        error: [Resource.msg('label.canada.customer.apply.msg', 'registration', null)]
      });
      return next();
    }
  }

  if (profile !== null && 'legacyPasswordHash' in profile.custom && profile.custom.legacyPasswordHash !== null) {
    customerLoginResult = {
      error: false,
      errorMessage: Resource.msg('error.message.password.mismatch', 'login', null),
      status: 'ERROR_PASSWORD_MISMATCH',
      authenticatedCustomer: null
    };
    var legacyLoginResult = legacyCustomerLogin.loginLegacyCustomer(customer, profile.custom.legacyPasswordHash, email, password, rememberMe);
    customerLoginResult.error = legacyLoginResult.error;
    customerLoginResult.authenticatedCustomer = legacyLoginResult.authenticatedCustomer;
    if (customerLoginResult.error) {
      MyAccountLogger.warn('Login  Error for SFSX-3859 1 =>' + email);
    }
    if (legacyLoginResult.errorMessage) {
      MyAccountLogger.warn('Login Error SFSX-3859 LegacyCustomer =>' + email);
      customerLoginResult.errorMessage = legacyLoginResult.errorMessage;
    }
    if (legacyLoginResult.islocked) {
      MyAccountLogger.warn('Login Error SFSX-3859 LegacyCustomer Locked  =>' + email);
      islocked = legacyLoginResult.islocked;
    }
  } else {
    MyAccountLogger.warn('Login  Error for SFSX-3859 1 - a =>' + email);

    customerLoginResult = Transaction.wrap(function () {
      var authenticateCustomerResult = CustomerMgr.authenticateCustomer(email, password);

      if (authenticateCustomerResult.status !== 'AUTH_OK') {
        var errorCodes = {
          ERROR_CUSTOMER_DISABLED: 'error.message.account.disabled',
          ERROR_CUSTOMER_LOCKED: 'error.message.account.locked',
          ERROR_CUSTOMER_NOT_FOUND: 'error.message.login.form',
          ERROR_PASSWORD_EXPIRED: 'error.message.password.expired',
          ERROR_PASSWORD_MISMATCH: 'error.message.password.mismatch',
          ERROR_UNKNOWN: 'error.message.error.unknown',
          default: 'error.message.login.form'
        };

        MyAccountLogger.warn('Login  Error for SFSX-3859 Not AUTH_OK  =>' + email + ' ' + authenticateCustomerResult.status);

        var errorMessageKey = errorCodes[authenticateCustomerResult.status] || errorCodes.default;
        var errorMessage = Resource.msg(errorMessageKey, 'login', null);
        MyAccountLogger.warn('Login  Error for SFSX-3859 errorMessageKey =>' + errorMessageKey + "-- " + errorMessage);
        if (profile && profile.credentials && profile.credentials.isLocked()) {
          islocked = true;
          errorMessage = Resource.msg('error.message.account.locked', 'login', null);
        }

        return {
          error: true,
          errorMessage: errorMessage,
          status: authenticateCustomerResult.status,
          authenticatedCustomer: null
        };
      }

      return {
        error: false,
        errorMessage: null,
        status: authenticateCustomerResult.status,
        authenticatedCustomer: CustomerMgr.loginCustomer(authenticateCustomerResult, rememberMe)
      };
    });
  }

  if (customerLoginResult.error) {
    MyAccountLogger.warn('Login Error customerLoginResult  =>' + email + " -- " + customerLoginResult.error);
    if (islocked) {
      var Locale = require('dw/util/Locale');
      var currentLanguage = Locale.getLocale(req.locale.id).getLanguage();
      hooksHelper(
        'app.customer.email.account.locked',
        'sendAccountLockedEmail',
        customer.profile,
        currentLanguage,
        require('*/cartridge/scripts/helpers/accountHelpers').sendAccountLockedEmail
      );
    }

    res.json({
      error: [customerLoginResult.errorMessage || Resource.msg('error.message.login.form', 'login', null)]
    });

    return next();
  }

  if (customerLoginResult.authenticatedCustomer) {
    res.setViewData({
      authenticatedCustomer: customerLoginResult.authenticatedCustomer
    });

    var preferences = require('*/cartridge/config/preferences');

    if (preferences.isABTestingOn) {
      var ProductListMgr = require('dw/customer/ProductListMgr');
      var CustomerMgr = require('dw/customer/CustomerMgr');

      var ProductList = require('dw/customer/ProductList');
      var Logger = require('dw/system/Logger');
      var customerLists = session.getCustomer().getProductLists(ProductList.TYPE_CUSTOM_1);
      if (customerLists) {
        var customerListIter = customerLists.iterator();
        Logger.debug('customerListIter FOUND' + customerLists.getLength());
        while (customerListIter.hasNext()) {
          productList = customerListIter.next();
          Logger.debug('productList FOUND');
          session.custom.cartProductListId = productList.ID;
          Logger.debug('req.session.custom.cartProductListId -->' + session.custom.cartProductListId);
        }
      }
      /* if (session.custom.cartProductListId == null) {
                    Transaction.wrap(function () {
                        cartProductList = ProductListMgr.createProductList(session.getCustomer(), ProductList.TYPE_CUSTOM_1);
                        session.custom.cartProductListId = cartProductList.ID;
                    });
                    Logger.debug(' NEW cartProductListId -->' + session.custom.cartProductListId);
                }*/
    }

    // Cart Merge Logic
    var isCartMerged = false;
    Transaction.wrap(function () {
      isCartMerged = cartHelper.mergeCart(req);
      if (isCartMerged) {
        req.session.privacyCache.set('basketMerged', true);
        res.setViewData({
          isCartMerged: isCartMerged
        });
        var currentBasket = BasketMgr.getCurrentBasket();
        if (currentBasket) {
          Transaction.wrap(function () {
            if (currentBasket.currencyCode !== req.session.currency.currencyCode) {
              currentBasket.updateCurrency();
            }
            cartHelper.ensureAllShipmentsHaveMethods(currentBasket);
            basketCalculationHelpers.calculateTotals(currentBasket);
          });
        }
      }

      // hook logic to send create loyalty profile in case more number was empty
      if (HookMgr.hasHook('app.customer.loyalty.rewards.profile') && !customer.profile.custom.moreCustomer) {
        var loyaltyResponse = hooksHelper(
          'app.customer.loyalty.rewards.profile',
          'callLoyaltyProfile',
          [customer.profile, true],
          require('*/cartridge/scripts/loyalty/loyaltyProfileUtil').callLoyaltyProfile
        );
        if (loyaltyResponse !== null && Number(loyaltyResponse.ResponseCode) === 0) {
          customer.profile.custom.moreCustomer =
            loyaltyResponse.LoyaltyProfile !== null &&
            loyaltyResponse.LoyaltyProfile.CustomerInfo !== null &&
            !!loyaltyResponse.LoyaltyProfile.CustomerInfo.moreNumber
              ? loyaltyResponse.LoyaltyProfile.CustomerInfo.moreNumber
              : '';
        }
      }
    });
    // set home store from customer profile
    if (customer.profile && customer.profile.custom.hasOwnProperty('myDefaultStore') && customer.profile.custom.myDefaultStore) {
      session.custom.homeStore = customer.profile.custom.myDefaultStore;
    }
    // set current store from home profile
    if (session.custom.homeStore && !session.custom.currentStore) {
      session.custom.currentStore = session.custom.homeStore;
    }
    var accRedirectURL;
    var URLUtils = require('dw/web/URLUtils');
    var StringUtils = require('dw/util/StringUtils');
    var capOneApply = req.querystring.apply;
    var test29 = req.querystring.test29; // test 29 digit
    var paUrlCookieValue = cookiesHelper.read('paurl');

    if (capOneApply || test29) {
      var result = capOneUtils.loginCapOnePrefill(req, customer);
      accRedirectURL = result.object.redirectUrl;
    } else if (req.querystring.orderhistory) {
      accRedirectURL = URLUtils.url('Order-History').relative().toString();
    } else if (req.querystring.myprefcenter) {
      accRedirectURL = URLUtils.url('PreferenceCenter-Show').relative().toString();
    } else if (paUrlCookieValue && req.querystring.paurl) {
      if (req.querystring.savepreferencetype) {
        var preferenceCenterHelper = require('*/cartridge/scripts/preferenceCenterHelper');
        var prefsToSave = JSON.parse(StringUtils.decodeBase64(cookiesHelper.read('prefstosave')));
        preferenceCenterHelper.updateCustomerPreferences(customer, req.querystring.savepreferencetype, prefsToSave, 'false', 'ADD');
      }
      accRedirectURL = paUrlCookieValue;
    } else if (req.session.privacyCache.get('basketLimitReached') && req.querystring.rurl && req.querystring.rurl === '2') {
      // redirect to cart in basket limit conflict during intermediate login
      accRedirectURL = URLUtils.url('Cart-Show').relative().toString();
    } else {
      accRedirectURL = accountHelpers.getLoginRedirectURL(req.querystring.rurl, req.session.privacyCache, false, isCartMerged);
    }
    res.json({
      success: true,
      redirectUrl: accRedirectURL
    });

    req.session.privacyCache.set('args', null);
    var listGuest = viewData.list;
    if (!empty(listGuest.items)) {
      req.session.privacyCache.set('mergeWishlist', true);
    }
    var listLoggedIn = productListHelper.getList(customerLoginResult.authenticatedCustomer, { type: 10 });
    // We need to do this check when the both lists will be the same in case of session expires.
    if (listLoggedIn.ID !== listGuest.ID) {
      productListHelper.mergelists(listLoggedIn, listGuest, req, {
        type: 10
      });
    }
    // productListHelper.mergelists(listLoggedIn, listGuest, req, { type: 10 });
    productListHelper.updateWishlistPrivacyCache(req.currentCustomer.raw, req, { type: 10 });

    //Changes for SFSX-2725 - This call will be made if the customer has a saks first linked account, or if the customer is not a legacy customer,
    //or if the customer is a legacy customer but has never logged in before.
    if (
      customer.profile.custom.saksFirstLinked ||
      empty(customer.profile.custom.isLegacyCustomer) ||
      !customer.profile.custom.isLegacyCustomer ||
      (!empty(customer.profile.custom.isLegacyCustomer) &&
        customer.profile.custom.isLegacyCustomer &&
        (empty(customer.profile.custom.hasLoggedOn) || !customer.profile.custom.hasLoggedOn))
    ) {
      // Saks First Loyalty Account Summary
      if (HookMgr.hasHook('saksfirst.loyalty.service') && customer.profile) {
        hooksHelper('saksfirst.loyalty.service', 'accountSummary', [customer.profile], require('*/cartridge/scripts/hooks/SaksFirstFacade').accountSummary);
      }
    }
    var saksplusHelper = require('*/cartridge/scripts/helpers/saksplusHelper');
    saksplusHelper.checkEligibilty(customer);
  } else {
    MyAccountLogger.warn('Login Error for SFSX-3859 authenticatedCustomer =>' + email);
    res.json({
      error: [Resource.msg('error.message.login.form', 'login', null)]
    });
  }

  return next();
});

server.replace('Header', function (req, res, next) {
  var Site = require('dw/system/Site');
  var preferences = require('*/cartridge/config/preferences');
  var storeLocatorURL =
    'storeLocatorURL' in Site.current.preferences.custom && !empty(Site.current.preferences.custom.storeLocatorURL)
      ? Site.current.preferences.custom.storeLocatorURL
      : '';
  var locale = req.locale.id;
  res.setViewData({
    storeLocatorURL: storeLocatorURL + locale.replace('_', '-').toLowerCase() + '?'
  });

  if (req.currentCustomer.raw.profile && !req.currentCustomer.raw.authenticated) {
    res.setViewData({
      softLoggedIn: true
    });
  }
  var cookieValue = cookiesHelper.read('RememberMeForced');
  if (!req.currentCustomer.raw.authenticated && cookieValue) {
    res.setViewData({
      softLoggedOut: true
    });
  }
  var profile = {};
  if (req.currentCustomer.raw.profile) {
    // These values not available for a soft logged in user
    profile.firstName = req.currentCustomer.raw.profile.firstName;
    profile.lastName = req.currentCustomer.raw.profile.lastName;
    profile.email = req.currentCustomer.raw.profile.email;
    profile.phone = req.currentCustomer.raw.profile.phoneHome;
  }

  var saksFirstInfo = saksFirstHelpers.getSaksFirstMemberInfo(req.currentCustomer.raw.profile);

  var template = req.querystring.mobile ? 'account/mobileHeader' : 'account/header';
  res.render(template, {
    name: req.currentCustomer.raw.profile ? req.currentCustomer.raw.profile.firstName : null,
    profile: profile,
    saksFirstEnabled: preferences.saksFirstEnabled,
    saksFirstInfo: saksFirstInfo
  });
  next();
});

/**
 * Added for SFSX-465
 * This method requires custom steps for Saks Fifth Avenue Banner
 * Hence copied it from app_hbc_core cartridge to the app_hbc_saksfifthavenue cartridge.
 *
 * The custom piece is to update the lastPasswordModified field in the Profile object.
 *
 */

server.replace('SavePassword', server.middleware.https, csrfProtection.validateAjaxAccountRequest, function (req, res, next) {
  var Transaction = require('dw/system/Transaction');
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var Resource = require('dw/web/Resource');
  var URLUtils = require('dw/web/URLUtils');
  var preferences = require('*/cartridge/config/preferences');
  var formErrors = require('*/cartridge/scripts/formErrors');
  var profileForm = server.forms.getForm('profile');
  var newPasswords = profileForm.login.newpasswords;

  // form validation
  if (profileForm.login.currentpassword.value === newPasswords.newpassword.value) {
    // Logic to not let customer use the same password again in the Password reset flow SFDEV-5349
    var email = req.currentCustomer.profile.email;

    if (preferences.isABTestingOn) {
      // While the AB Testing is ON, just compare the input password hash with the saved password hash
      var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
      var newPswdHash = legacyCustomerLogin.getPasswordLegacyHashed(newPasswords.newpassword.value);
      var customer = CustomerMgr.getCustomerByLogin(email);
      if (!empty(customer) && !empty(customer.profile) && customer.profile.custom.legacyPasswordHash === newPswdHash) {
        profileForm.valid = false;
        newPasswords.newpassword.valid = false;
        newPasswords.newpasswordconfirm.valid = false;
        newPasswords.newpassword.error = Resource.msg('error.message.same.password', 'forms', null);
      }
    } else {
      // If the AB Testing flag is OFF, authenticate the customer by using the new password to find out if the customer is trying to reset to the same password
      var authenticateCustomerResult;
      Transaction.wrap(function () {
        authenticateCustomerResult = CustomerMgr.authenticateCustomer(email, newPasswords.newpassword.value);
      });
      if (authenticateCustomerResult.authenticated) {
        profileForm.valid = false;
        newPasswords.newpassword.valid = false;
        newPasswords.newpasswordconfirm.valid = false;
        newPasswords.newpassword.error = Resource.msg('error.message.same.password', 'forms', null);
      }
    }
  } else if (newPasswords.newpassword.value !== newPasswords.newpasswordconfirm.value) {
    profileForm.valid = false;
    newPasswords.newpassword.valid = false;
    newPasswords.newpasswordconfirm.valid = false;
    newPasswords.newpasswordconfirm.error = Resource.msg('error.message.mismatch.newpassword', 'forms', null);
  } else if (!CustomerMgr.isAcceptablePassword(newPasswords.newpassword.value) || !CustomerMgr.isAcceptablePassword(newPasswords.newpasswordconfirm.value)) {
    profileForm.valid = false;
    newPasswords.newpassword.valid = false;
    newPasswords.newpasswordconfirm.valid = false;
    newPasswords.newpasswordconfirm.error = Resource.msg('error.message.password.constraints.not.matched', 'forms', null);
  }

  var result = {
    currentPassword: profileForm.login.currentpassword.value,
    newPassword: newPasswords.newpassword.value,
    newPasswordConfirm: newPasswords.newpasswordconfirm.value,
    profileForm: profileForm
  };

  if (profileForm.valid) {
    res.setViewData(result);
    this.on('route:BeforeComplete', function (req, res) {
      // eslint-disable-line no-shadow
      var formInfo = res.getViewData();
      var customer = CustomerMgr.getCustomerByCustomerNumber(req.currentCustomer.profile.customerNo);
      var status;
      var isError = false;

      if (!preferences.isABTestingOn) {
        Transaction.wrap(function () {
          status = customer.profile.credentials.setPassword(formInfo.newPassword, formInfo.currentPassword, true);
        });
        isError = status.error;
      } else {
        var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
        var newPswd = legacyCustomerLogin.getPasswordLegacyHashed(formInfo.newPassword);

        var oldPswdHash = legacyCustomerLogin.getPasswordLegacyHashed(formInfo.currentPassword);
        if (!empty(customer.profile.custom.legacyPasswordHash) && customer.profile.custom.legacyPasswordHash === oldPswdHash) {
          Transaction.wrap(function () {
            status = customer.profile.credentials.setPassword(preferences.tempLegacyLogin, preferences.tempLegacyLogin, true);
            customer.profile.custom.legacyPasswordHash = newPswd;
            //Added for SFSX-465 - start
            var accHelper = require('*/cartridge/scripts/helpers/accountHelpers');
            accHelper.updateLastPasswordModified(customer.profile);
            //Added for SFSX-465 - end
          });
          isError = status.error;
        } else {
          isError = true;
        }
      }
      if (isError) {
        formInfo.profileForm.login.currentpassword.valid = false;
        formInfo.profileForm.login.currentpassword.error = Resource.msg('error.message.currentpasswordnomatch', 'forms', null);

        delete formInfo.currentPassword;
        delete formInfo.newPassword;
        delete formInfo.newPasswordConfirm;
        delete formInfo.profileForm;

        res.json({
          success: false,
          fields: formErrors.getFormErrors(profileForm)
        });
      } else {
        var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
        // Update the custom attribute for last modified date as the system attribute lastModified is not true profile update from a customer perspective
        accountHelpers.updateAccLastModifiedDate(customer);

        delete formInfo.currentPassword;
        delete formInfo.newPassword;
        delete formInfo.newPasswordConfirm;
        delete formInfo.profileForm;

        res.json({
          success: true,
          redirectUrl: URLUtils.url('Account-Show').toString()
        });

        var Locale = require('dw/util/Locale');
        var currentLanguage = Locale.getLocale(req.locale.id).getLanguage();
        hooksHelper(
          'app.customer.email.update.password',
          'sendUpdatePasswordEmail',
          customer.profile,
          currentLanguage,
          require('*/cartridge/scripts/helpers/accountHelpers').sendUpdatePasswordEmail
        );
        var AgentUserMgr = require('dw/customer/AgentUserMgr');
        try {
          if (req.session.isUserAuthenticated()) {
            AgentUserMgr.logoutAgentUser();
          }
        } catch (e) {
          // error handlin g
        }
        CustomerMgr.logoutCustomer(false);
      }
    });
  } else {
    res.json({
      success: false,
      fields: formErrors.getFormErrors(profileForm)
    });
  }
  return next();
});

/**
 * Added for SFSX-465
 * This method requires custom steps for Saks Fifth Avenue Banner
 * Hence copied it from app_hbc_core cartridge to the app_hbc_saksfifthavenue cartridge.
 *
 * The custom piece is to update the lastPasswordModified field in the Profile object.
 * :TODO Remove Hudson Bay related code.
 *
 */

//Replaced the base route in order to send the registration email from CNS hook
server.replace('SubmitRegistration', server.middleware.https, csrfProtection.validateAjaxRequest, function (req, res, next) {
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var Resource = require('dw/web/Resource');
  var HookMgr = require('dw/system/HookMgr');
  var preferences = require('*/cartridge/config/preferences');
  var formErrors = require('*/cartridge/scripts/formErrors');
  var registrationForm = server.forms.getForm('profile');
  var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
  var Logger = require('dw/system/Logger');
  var capOneUtils = require('*/cartridge/scripts/util/capOneUtils');

  if (session.custom.isbot != null && session.custom.isbot) {
    res.json({
      error: [Resource.msg('error.message.login.bot', 'login', null)],
      botError: true
    });
    return next();
  }
  var result = hooksHelper('app.google.recaptcha.verify', 'validateToken', req.form.token, require('*/cartridge/scripts/googleRecaptchaUtil').validateToken);
  if (session.custom.isbot) {
    res.json({
      error: [Resource.msg('error.message.login.bot', 'login', null)],
      botError: true
    });
    return next();
  }

  if (registrationForm.login.password.htmlValue !== registrationForm.login.passwordconfirm.htmlValue) {
    registrationForm.login.password.valid = false;
    registrationForm.login.passwordconfirm.valid = false;
    registrationForm.login.passwordconfirm.error = Resource.msg('error.message.mismatch.password', 'forms', null);
    registrationForm.valid = false;
  }
  if (!CustomerMgr.isAcceptablePassword(registrationForm.login.password.htmlValue)) {
    registrationForm.login.password.valid = false;
    registrationForm.login.passwordconfirm.valid = false;
    registrationForm.login.password.error = Resource.msg('error.message.password.constraints.not.matched', 'forms', null);
    // If there is an error for Password Missmatch also, clear that.
    delete registrationForm.login.passwordconfirm.error;
    registrationForm.valid = false;
  }
  // changes done to merge the guest wishlist while the user is creating an acc
  var list = productListHelper.getList(req.currentCustomer.raw, {
    type: 10
  });

  var Locale = require('dw/util/Locale');
  var currentLanguage = Locale.getLocale(req.locale.id).getLanguage();
  var preferredLanguage = 'ENGLISH';
  if (currentLanguage == 'fr') {
    preferredLanguage = 'FRENCH';
  }

  // Check hbc_loyalty hook is available
  if (HookMgr.hasHook('app.customer.hbc.loyalty.enroll')) {
    // Import Loyalty Form Helper Script
    var loyaltyFormValidator = require('*/cartridge/scripts/util/enrollFormHelper');
    // Check which radio button was selected for Hudson's Bay Rewards Program
    // i.e, (Sign Up, Already A Member, No Thanks)
    const selectedRadio = registrationForm.customer.loyaltyEnroll.selectedLoyaltyRegistrationRadio.value;
    registrationForm = loyaltyFormValidator.validateLoyaltyFields(selectedRadio, registrationForm);
  }

  // setting variables for the BeforeComplete function
  var registrationFormObj = {
    firstName: registrationForm.customer.firstname.value,
    lastName: registrationForm.customer.lastname.value,
    phone: registrationForm.customer.phone.value,
    email: registrationForm.customer.email.value,
    emailConfirm: registrationForm.customer.emailconfirm.value,
    password: registrationForm.login.password.value,
    passwordConfirm: registrationForm.login.passwordconfirm.value,
    hudsonbayrewards: req.form.hbcRewardNumber,
    zipCode: registrationForm.customer.zipcode.value,
    addtoemaillist: registrationForm.customer.addtoemaillist.value,
    saksOptIn: req.form.saksOpt,
    saksAvenueOptIn: req.form.saksAvenueOpt,
    isCanadianCustomer: req.form.isCanadianCustomer,
    validForm: registrationForm.valid,
    form: registrationForm,
    list: list
  };

  if (registrationForm.valid) {
    res.setViewData(registrationFormObj);

    this.on('route:BeforeComplete', function (req, res) {
      // eslint-disable-line no-shadow
      var Transaction = require('dw/system/Transaction');
      var URLUtils = require('dw/web/URLUtils');
      var StringUtils = require('dw/util/StringUtils');
      var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
      var authenticatedCustomer;
      var newCustomerProfileObj;
      var serverError;

      // getting variables for the BeforeComplete function
      var registrationForm = res.getViewData(); // eslint-disable-line

      if (registrationForm.validForm) {
        var login = registrationForm.email;
        var password = registrationForm.password;

        // attempt to create a new user and log that user in.
        try {
          Transaction.wrap(function () {
            var error = {};
            var profilePassword;
            if (preferences.isABTestingOn) {
              profilePassword = preferences.tempLegacyLogin;
            } else {
              profilePassword = password;
            }
            var newCustomer = CustomerMgr.createCustomer(login, profilePassword);
            var authenticateCustomerResult = CustomerMgr.authenticateCustomer(login, profilePassword);
            if (authenticateCustomerResult.status !== 'AUTH_OK') {
              error = {
                authError: true,
                status: authenticateCustomerResult.status
              };
              throw error;
            }

            authenticatedCustomer = CustomerMgr.loginCustomer(authenticateCustomerResult, false);

            if (!authenticatedCustomer) {
              error = {
                authError: true,
                status: authenticateCustomerResult.status
              };
              throw error;
            } else {
              // assign values to the profile
              var newCustomerProfile = newCustomer.getProfile();

              newCustomerProfile.firstName = registrationForm.firstName;
              newCustomerProfile.lastName = registrationForm.lastName;
              newCustomerProfile.phoneHome = registrationForm.phone ? registrationForm.phone : '';
              newCustomerProfile.email = registrationForm.email;

              var listGuest = registrationForm.list;
              if (!empty(listGuest.items)) {
                req.session.privacyCache.set('mergeWishlist', true);
              }
              if (authenticatedCustomer) {
                var listLoggedIn = productListHelper.getList(authenticatedCustomer, { type: 10 });
                productListHelper.mergelists(listLoggedIn, listGuest, req, {
                  type: 10
                });
                productListHelper.updateWishlistPrivacyCache(req.currentCustomer.raw, req, { type: 10 });
              }

              if (preferences.isABTestingOn) {
                var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
                var newPswd = legacyCustomerLogin.getPasswordLegacyHashed(password);
                newCustomerProfile.custom.legacyPasswordHash = newPswd;
              }

              //Added for SFSX-465 - start
              accountHelpers.updateLastPasswordModified(newCustomerProfile);
              //Added for SFSX-465 - end

              if (registrationForm.hudsonbayrewards) {
                newCustomerProfile.custom.hudsonReward = registrationForm.hudsonbayrewards;
              }
              if (registrationForm.zipCode) {
                newCustomerProfile.custom.zipCode = registrationForm.zipCode;
              }
              accountHelpers.updateCustomerPreferences(req, newCustomerProfile.email, newCustomerProfile, registrationForm);

              // Update the custom attribute for last modified date as the system attribute lastModified is not true profile update from a customer perspective
              accountHelpers.updateAccLastModifiedDate(newCustomer);

              if (registrationForm.isCanadianCustomer) {
                newCustomerProfile.custom.isCanadianCustomer = registrationForm.isCanadianCustomer === 'T';
              }

              newCustomerProfile.custom.preferredLanguage = preferredLanguage;

              // Create new object for capital one apply
              newCustomerProfileObj = capOneUtils.capOneNewCustomer(registrationForm, newCustomerProfile);

              // hook logic to send create loyalty profile
              if (HookMgr.hasHook('app.customer.loyalty.rewards.profile')) {
                var loyaltyResponse = hooksHelper(
                  'app.customer.loyalty.rewards.profile',
                  'callLoyaltyProfile',
                  [newCustomerProfile, true],
                  require('*/cartridge/scripts/loyalty/loyaltyProfileUtil').callLoyaltyProfile
                );
                if (loyaltyResponse !== null && Number(loyaltyResponse.ResponseCode) === 0) {
                  newCustomerProfile.custom.moreCustomer =
                    loyaltyResponse.LoyaltyProfile !== null &&
                    loyaltyResponse.LoyaltyProfile.CustomerInfo !== null &&
                    !!loyaltyResponse.LoyaltyProfile.CustomerInfo.moreNumber
                      ? loyaltyResponse.LoyaltyProfile.CustomerInfo.moreNumber
                      : '';
                }
              }
            }
          });
        } catch (e) {
          if (e.authError) {
            serverError = true;
          } else {
            registrationForm.validForm = false;
            registrationForm.form.customer.email.valid = false;
            registrationForm.form.customer.emailconfirm.valid = false;
            registrationForm.form.customer.email.error = Resource.msg('error.message.username.invalid', 'forms', null);
          }
        }
      }

      delete registrationForm.password;
      delete registrationForm.passwordConfirm;
      formErrors.removeFormValues(registrationForm.form);

      if (serverError) {
        res.setStatusCode(500);
        res.json({
          success: false,
          errorMessage: Resource.msg('error.message.unable.to.create.account', 'login', null)
        });

        return;
      }

      if (registrationForm.validForm) {
        // send a registration email via CNS hook code change from accounthelpers
        hooksHelper(
          'app.customer.email.account.created',
          'sendCreateAccountEmail',
          authenticatedCustomer.profile,
          currentLanguage,
          require('*/cartridge/scripts/helpers/accountHelpers').sendCreateAccountEmail
        );

        // UCID Call to Sync
        hooksHelper('ucid.middleware.service', 'createUCIDCustomer', [authenticatedCustomer.profile, authenticatedCustomer.profile.customerNo, null]);

        var hasLoyaltyHook = HookMgr.hasHook('app.customer.hbc.loyalty.enroll');
        var loyaltyResponse;
        // Call Enroll API if HBC Loyalty Hook is available
        if (hasLoyaltyHook) {
          // Assign selected radio to var from form
          var loyaltyRegistrationOption = registrationForm.form.customer.loyaltyEnroll.selectedLoyaltyRegistrationRadio.value;
          // Check optout option is not selected
          if (loyaltyRegistrationOption !== 'optout') {
            // Get the newly created customer Profile - to save reward number later
            const customer = CustomerMgr.getCustomerByCustomerNumber(authenticatedCustomer.profile.customerNo);
            const profile = customer.getProfile();

            switch (loyaltyRegistrationOption) {
              case 'signup':
                // User clicked Sign Up
                loyaltyResponse = hooksHelper('app.customer.hbc.loyalty.enroll', 'callLoyaltyEnroll', [profile, registrationForm, true]);
                if (loyaltyResponse !== null && Number(loyaltyResponse.response_code) === 1) {
                  Transaction.wrap(function () {
                    // Assign loyalty_id to the customer profile
                    profile.custom.hudsonReward = loyaltyResponse.errorMessage == null ? String(loyaltyResponse.loyalty_id) : '';
                  });

                  // The session data is set up within callLoyaltyEnroll hook but also needs profile.custom.hudsonReward to verify enrollment; profile.custom.hudsonReward is assigned after session data is created - we must clear it.
                  req.session.privacyCache.set('loyalty', null);
                }

                break;
              case 'members':
                // User clicked Already A Member
                loyaltyResponse = hooksHelper('app.customer.hbc.loyalty.enroll', 'linkAccount', [profile, registrationForm]);

                if (loyaltyResponse !== null && Number(loyaltyResponse.response_code) === 1) {
                  Transaction.wrap(function () {
                    // Assign loyalty_id to the customer profile from the customer lookup response
                    profile.custom.hudsonReward = loyaltyResponse.loyaltyId;
                    profile.custom.isLinkedAccount = true;
                  });
                  // The session data is set up within linkAccount hook but also needs profile.custom.hudsonReward to verify enrollment; profile.custom.hudsonReward is assigned after session data is created - we must clear it.
                  req.session.privacyCache.set('loyalty', null);
                }
                break;
              default:
                // nothing to do - selected radio option does not match
                loyaltyResponse = {
                  success: false
                };
            }
          }
        }

        res.setViewData({
          authenticatedCustomer: authenticatedCustomer
        });

        var capOneApply = req.querystring.apply;
        var isCanadianCustomer = registrationForm.isCanadianCustomer && registrationForm.isCanadianCustomer === 'T';
        var paUrlCookieValue = cookiesHelper.read('paurl');
        if (capOneApply && !isCanadianCustomer) {
          var result = capOneUtils.prefillCapOne(req, newCustomerProfileObj);

          res.json({
            success: true,
            redirectUrl: result.object.redirectUrl
          });
        } else {
          if (hasLoyaltyHook && loyaltyResponse !== undefined && loyaltyResponse.success === false) {
            res.json({
              success: true,
              redirectUrl: URLUtils.url('Loyalty-Fail').relative().toString()
            });
          } else if (paUrlCookieValue && req.querystring.paurl) {
            if (req.querystring.savepreferencetype) {
              var preferenceCenterHelper = require('*/cartridge/scripts/preferenceCenterHelper');
              var prefsToSave = JSON.parse(StringUtils.decodeBase64(cookiesHelper.read('prefstosave')));
              if (authenticatedCustomer) {
                var customerProfile = CustomerMgr.getCustomerByCustomerNumber(authenticatedCustomer.profile.customerNo);
                preferenceCenterHelper.updateCustomerPreferences(customerProfile, req.querystring.savepreferencetype, prefsToSave, 'false', 'ADD');
              }
            }
            res.json({
              success: true,
              redirectUrl: paUrlCookieValue
            });
          } else {
            res.json({
              success: true,
              redirectUrl: accountHelpers.getLoginRedirectURL(req.querystring.rurl, req.session.privacyCache, true)
            });
          }
        }

        req.session.privacyCache.set('args', null);
      } else {
        res.json({
          fields: formErrors.getFormErrors(registrationForm)
        });
      }
    });
  } else {
    res.json({
      fields: formErrors.getFormErrors(registrationForm)
    });
  }

  return next();
});

/**
 * Added for SFSX-465
 * This method requires custom steps for Saks Fifth Avenue Banner
 * Hence copied it from app_hbc_core cartridge to the app_hbc_saksfifthavenue cartridge.
 *
 * Reason to override the controller - To clear the legacyPasswordHash custom attribute value if the first time customer login chooses to reset his password.
 * The hash value is cleared only if the password was successfully reset.
 */
server.replace('SaveNewPassword', server.middleware.https, function (req, res, next) {
  var Transaction = require('dw/system/Transaction');
  var Resource = require('dw/web/Resource');
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var preferences = require('*/cartridge/config/preferences');
  var passwordForm = server.forms.getForm('newPasswords');
  var token = req.querystring.Token;
  var URLUtils = require('dw/web/URLUtils');

  if (session.custom.isbot != null && session.custom.isbot) {
    passwordForm.clear();
    res.render('account/password/newPassword', {
      passwordForm: passwordForm,
      token: token,
      includeRecaptchaJS: true,
      error: Resource.msg('error.message.login.bot', 'login', null),
      botError: true,
      passwordRegex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#_?!@$%^()+=~`}{|&*-])^[^'<>/]{8,}$"
    });

    return next();
  }

  // Logger.debug('start SaveNewPassword !!!' + req.form.token);
  var result = hooksHelper('app.google.recaptcha.verify', 'validateToken', req.form.token, require('*/cartridge/scripts/googleRecaptchaUtil').validateToken);

  if (session.custom.isbot) {
    passwordForm.clear();
    res.render('account/password/newPassword', {
      passwordForm: passwordForm,
      token: token,
      includeRecaptchaJS: true,
      error: Resource.msg('error.message.login.bot', 'login', null),
      botError: true,
      passwordRegex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#_?!@$%^()+=~`}{|&*-])^[^'<>/]{8,}$"
    });

    return next();
  }
  var existingCustomer = CustomerMgr.getCustomerByToken(token);
  var currentPasswordMatch = false;
  if (!empty(existingCustomer)) {
    var email = existingCustomer.profile.email;

    if (preferences.isABTestingOn) {
      // While the AB Testing is ON, just compare the input password hash with the saved password hash
      var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
      var newPswdHash = legacyCustomerLogin.getPasswordLegacyHashed(passwordForm.newpassword.value);
      var customer = CustomerMgr.getCustomerByLogin(email);
      if (!empty(customer) && !empty(customer.profile) && customer.profile.custom.legacyPasswordHash === newPswdHash) {
        passwordForm.valid = false;
        passwordForm.newpassword.valid = false;
        passwordForm.newpasswordconfirm.valid = false;
        passwordForm.newpassword.error = Resource.msg('error.message.same.password', 'forms', null);
        currentPasswordMatch = true;
      }
    } else {
      // If the AB Testing flag is OFF, authenticate the customer by using the new password to find out if the customer is trying to reset to the same password
      var authenticateCustomerResult;
      Transaction.wrap(function () {
        authenticateCustomerResult = CustomerMgr.authenticateCustomer(email, passwordForm.newpassword.value);
      });
      if (authenticateCustomerResult.authenticated) {
        passwordForm.valid = false;
        passwordForm.newpassword.valid = false;
        passwordForm.newpasswordconfirm.valid = false;
        passwordForm.newpassword.error = Resource.msg('error.message.same.password', 'forms', null);
        currentPasswordMatch = true;
      }
    }
  }

  if (passwordForm.newpassword.value !== passwordForm.newpasswordconfirm.value) {
    passwordForm.valid = false;
    passwordForm.newpassword.valid = false;
    passwordForm.newpasswordconfirm.valid = false;
    passwordForm.newpasswordconfirm.error = Resource.msg('error.message.mismatch.newpassword', 'forms', null);
  }

  res.setViewData({
    passwordRegex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#_?!@$%^()+=~`}{|&*-])^[^'<>/]{8,}$"
  });

  if (passwordForm.valid) {
    var result = {
      newPassword: passwordForm.newpassword.value,
      newPasswordConfirm: passwordForm.newpasswordconfirm.value,
      token: token,
      passwordForm: passwordForm
    };
    res.setViewData(result);
    this.on('route:BeforeComplete', function (req, res) {
      // eslint-disable-line no-shadow
      var formInfo = res.getViewData();
      var status;
      var resettingCustomer;
      Transaction.wrap(function () {
        resettingCustomer = CustomerMgr.getCustomerByToken(formInfo.token);

        if (!preferences.isABTestingOn) {
          status = resettingCustomer.profile.credentials.setPasswordWithToken(formInfo.token, formInfo.newPassword);
        } else {
          var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
          var newPswd = legacyCustomerLogin.getPasswordLegacyHashed(formInfo.newPassword);
          status = resettingCustomer.profile.credentials.setPasswordWithToken(formInfo.token, preferences.tempLegacyLogin);
          resettingCustomer.profile.custom.legacyPasswordHash = newPswd;
          //Added for SFSX-465 - start
          var accHelper = require('*/cartridge/scripts/helpers/accountHelpers');
          accHelper.updateLastPasswordModified(resettingCustomer.profile);
          //Added for SFSX-465 - end
        }
      });

      if (status.error) {
        passwordForm.newpassword.valid = false;
        passwordForm.newpasswordconfirm.valid = false;
        passwordForm.newpasswordconfirm.error = Resource.msg('error.message.resetpassword.invalidformentry', 'forms', null);
        res.render('account/password/newPassword', {
          passwordForm: passwordForm,
          token: token,
          includeRecaptchaJS: true
        });
      } else {
        var comingFrom = 'forgotPassword';
        var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
        accountHelpers.updateAccLastModifiedDate(resettingCustomer, comingFrom);

        if (!preferences.isABTestingOn) {
          var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
          legacyCustomerLogin.removeLegacyFlag(resettingCustomer);
        }
        var Locale = require('dw/util/Locale');
        var currentLanguage = Locale.getLocale(req.locale.id).getLanguage();
        hooksHelper(
          'app.customer.email.update.password',
          'sendUpdatePasswordEmail',
          resettingCustomer.profile,
          currentLanguage,
          require('*/cartridge/scripts/helpers/accountHelpers').sendUpdatePasswordEmail
        );
        res.redirect(URLUtils.url('Login-Show'));
      }
    });
  } else {
    if (currentPasswordMatch){
      res.redirect(URLUtils.url('Login-Show'));
    } else {
      passwordForm.clear();
      res.render('account/password/newPassword', {
        passwordForm: passwordForm,
        token: token,
        includeRecaptchaJS: true
      });
    }
  }
  next();
});

module.exports = server.exports();
