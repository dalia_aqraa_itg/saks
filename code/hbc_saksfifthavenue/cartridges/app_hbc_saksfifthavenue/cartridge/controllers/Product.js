'use strict';

var server = require('server');
server.extend(module.superModule);
var preferences = require('*/cartridge/config/preferences');
var cache = require('*/cartridge/scripts/middleware/cache');

/**
 * Empty middleware in order to test the change of the task SFSX-4414
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
*/
function emptyMiddleware(req, res, next) {
  next();
}

server.get('ShowSaksPlus', function (req, res, next) {
  var saksplusHelper = require('*/cartridge/scripts/helpers/saksplusHelper');
  var URLUtils = require('dw/web/URLUtils');
  var saksPlusProductID = saksplusHelper.getSaksPlusProductID();
  var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
  var addToCartUrl = URLUtils.url('Cart-AddProduct').toString();
  var queryObj = {};
  queryObj.pid = saksPlusProductID;
  queryObj.pview = 'saks';
  var showProductPageHelperResult = productHelper.showProductPage(queryObj, req.pageMetaData);

  res.render('saksplus/saksplusproduct', {
    product: showProductPageHelperResult.product,
    addToCartUrl: addToCartUrl
  });
  next();
});

server.replace('SaveWaitList', function (req, res, next) {
  // eslint-disable-line
  var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
  var CustomObjectMgr = require('dw/object/CustomObjectMgr');
  var Transaction = require('dw/system/Transaction');
  var Resource = require('dw/web/Resource');
  var StringUtils = require('dw/util/StringUtils');
  var Calendar = require('dw/util/Calendar');
  var EmailSubscribeHelper = require('*/cartridge/scripts/helpers/EmailSubscribeHelper');
  var email = req.form.waitlistEmail;
  var productID = req.form.productID;
  var saksPlusProductID = preferences.saksPlusProductID;
  var submitMsg = Resource.msg('msg.wiatlist.submitted', 'product', null);
  if (productID === saksPlusProductID) {
    submitMsg = Resource.msg('msg.saksplusswiatlist.submitted', 'product', null);
  }
  if (email && productID) {
    try {
      Transaction.wrap(function () {
        // Query Custom Object and update it if Email and Mobile is same.
        var query = 'custom.emailId = {0} AND custom.productId = {1}';
        var waitListObj = CustomObjectMgr.queryCustomObject('WaitlistDetails', query, email, productID);
        if (waitListObj) {
          waitListObj.custom.phoneNumber = !empty(req.form.waitlistMobile) ? req.form.waitlistMobile : '';
          waitListObj.custom.date = StringUtils.formatCalendar(new Calendar(), 'MM/dd/yyyy HH:mm:ss');
          if (preferences.dataSubscription.BANNER && preferences.dataSubscription.BANNER === 'Saks') {
            waitListObj.custom.sourceId = EmailSubscribeHelper.isRequestFromMobile() ? 'mobileWaitlist' : 'desktopWaitlist';
          }
        } else {
          waitListObj = CustomObjectMgr.createCustomObject('WaitlistDetails', productHelper.generateUUID());
          if (waitListObj) {
            waitListObj.custom.productId = productID;
            waitListObj.custom.emailId = email;
            waitListObj.custom.phoneNumber = !empty(req.form.waitlistMobile) ? req.form.waitlistMobile : '';
            waitListObj.custom.date = StringUtils.formatCalendar(new Calendar(), 'MM/dd/yyyy HH:mm:ss');
            waitListObj.custom.exported = false;
            if (preferences.dataSubscription.BANNER && preferences.dataSubscription.BANNER === 'Saks') {
              waitListObj.custom.sourceId = EmailSubscribeHelper.isRequestFromMobile() ? 'mobileWaitlist' : 'desktopWaitlist';
            }
          }
        }
      });
    } catch (e) {
      res.json({
        success: false,
        msg: Resource.msg('msg.wiatlist.not.submitted', 'product', null)
      });
      return next();
    }
    res.json({
      success: true,
      msg: submitMsg
    });
  }
  next();
});

server.append('Show', function (req, res, next) {
  var URLUtils = require('dw/web/URLUtils');
  var viewData = res.getViewData();
  var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
  var product = viewData.product;
  var storeHelper = require('*/cartridge/scripts/helpers/storeHelpers');
  var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
  var Logger = require('dw/system/Logger');
  var store = null;
  var isSetWishListed = false;

  // get SDD eligibility
  if (empty(viewData.product)){
    return next();
  }

  if (product) {
    if (product.productType === 'set' && !product.online) {
      res.setStatusCode(404);
      res.render('error/notFound');
    }
    store = storeHelper.getDefaultSDDStore(product);
    if (product.productType === 'set') {
      var list = productListHelper.getList(req.currentCustomer.raw, {
        type: 10
      });
      list.items.toArray().forEach(function (item) {
        if (item.productID === product.id) {
          isSetWishListed = true;
        }
      });
    }

    // SFSX-3542 Prevent google ads to be load on restricted brands based on site preferences
    viewData.restrictGoogleAds = false;

    if (product.brand && product.brand.name && preferences.restrictedGoogleAdsBrands) {
      viewData.restrictGoogleAds = productHelper.checkGoogleAdsRestrictedBrands(product.brand.name);
    }
  }

  var args = {};
  try {
    var getBloomReachWidget = require('*/cartridge/scripts/getBloomReachWidget');
    var bREnableWidget = preferences.BR_Enable_Widget;
    // BloomReach Widget parameters
    if (bREnableWidget) {
      args.CurrentRequest = request;
      args.pageType = 'product';
      args.productId = product.id;
      args.productName = product.productName;
      args.pageStatus = '';
      args = getBloomReachWidget.execute(args);
    }
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Statck:' + e.stack;
    Logger.error('Bloom Reach producttile' + errorMsg);
  }
  if (preferences.saksFirstEnabled) {
    var pointsMultiplier = 1; // Default
    if (preferences.saksFirstPointsMultiplier !== -1) {
      pointsMultiplier = preferences.saksFirstPointsMultiplier;
    }
    var saksFirstHelpers = require('*/cartridge/scripts/helpers/saksFirstHelpers');
    var profile = req.currentCustomer.raw.profile;
    if (profile != null && profile.customer.authenticated && 'saksFirstLinked' in profile.custom && profile.custom.saksFirstLinked) {
      var saksProfileInfo = saksFirstHelpers.getSaksFirstMemberInfo(profile);
      if (saksProfileInfo && saksProfileInfo.pointsMultiplier) {
        pointsMultiplier = saksProfileInfo.pointsMultiplier;
      }
    }
    res.setViewData({
      pointsMultiplier: pointsMultiplier,
      saksFirstEnabled: true
    });
  }

  res.setViewData({
    isEnabledforSameDayDelivery: preferences.isEnabledforSameDayDelivery,
    sddStore: store,
    args: args,
    isAvailableForSDD: product.isAvailableForSDD,
    setSDDPostalUrl: URLUtils.url('Stores-SetSDDZipCode').toString(),
    sddStoreModalUrl: URLUtils.url('Stores-InitSDDSearch').toString(),
    sddPostal: session.custom.sddpostal ? session.custom.sddpostal : request.geolocation.postalCode,
    isSetWishListed: isSetWishListed,
    isEnabledForClick2OrderCSR: productHelper.isEnabledForClick2OrderCSR(req.session.raw.isUserAuthenticated()),
    designerOptions: productHelper.getDesignerOptions(product.hbcProductType),
    numberOfAvailableItems: product.numberOfAvailableItems,
    numberOfInStockItems: product.numberOfInStockItems
  });
  next();
});

server.append('Variation', preferences.BEPerformanceChangesEnable ? cache.applyPromotionSensitiveCache : emptyMiddleware , function (req, res, next) {
  var storeHelper = require('*/cartridge/scripts/helpers/storeHelpers');
  var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
  var product = res.viewData.product;
  var store = null;
  // get SDD eligiblity
  if (product) {
    store = storeHelper.getDefaultSDDStore(product);
  }
  res.setViewData({
    isEnabledforSameDayDelivery: preferences.isEnabledforSameDayDelivery,
    sddStore: store,
    isAvailableForSDD: product.isAvailableForSDD,
    isEnabledForClick2OrderCSR: productHelper.isEnabledForClick2OrderCSR(req.session.raw.isUserAuthenticated())
  });
  next();
});

server.get('ChangeZipCodeContent', function (req, res, next) {
  var Resource = require('dw/web/Resource');
  var URLUtils = require('dw/web/URLUtils');
  var Logger = require('dw/system/Logger');
  try {
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var params = req.querystring;
    var product = ProductFactory.baseProduct(params);
    var linkText = session.custom.sddpostal ? session.custom.sddpostal : request.geolocation.postalCode;
    if (!linkText || linkText === '' || linkText === null) {
      linkText = Resource.msg('shippingoption.product.shitonostore', 'product', null);
    }
    res.render('product/components/samedaydelivery/changezipcodePDP', {
      setSDDPostalUrl: URLUtils.url('Stores-SetSDDZipCode').toString(),
      product: product,
      sddStoreModalUrl: URLUtils.url('Stores-InitSDDSearch').toString(),
      linkText: linkText,
      ispdp: true
    });
  } catch (e) {
    Logger.error('Error in ChangeZipCodeContent ' + e.message);
  }
  next();
});

server.get('SDDVerbiage', function (req, res, next) {
  var Logger = require('dw/system/Logger');
  try {
    var store = null;
    var storeHelper = require('*/cartridge/scripts/helpers/storeHelpers');
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var params = req.querystring;
    var product = ProductFactory.get(params);
    if (product) {
      store = storeHelper.getDefaultSDDStore(product);
    }
    res.render('product/components/samedaydelivery/verbiageSDDVariant', {
      sddStore: store
    });
  } catch (e) {
    Logger.error('Error in SDDVerbiage ' + e.message);
  }
  next();
});

server.get('LoadAttributes', preferences.BEPerformanceChangesEnable ? cache.applyPromotionSensitiveCache : emptyMiddleware, function (req, res, next) {
  var ProductFactory = require('*/cartridge/scripts/factories/product');
  var pid = req.querystring.pid;
  var product = ProductFactory.getVariations({ pid: pid }) || {};
  var searchableIfUnavailable = (req.querystring.searchableIfUnavailable && String(req.querystring.searchableIfUnavailable) === 'true') || false;
  var isEdit = (req.querystring.isEdit && String(req.querystring.isEdit) === 'true') || false;
  var isBundle = (req.querystring.isBundle && String(req.querystring.isBundle) === 'true') || false;
  var wishlistAddToBag = (req.querystring.wishlistAddToBag && String(req.querystring.wishlistAddToBag) === 'true') || false;
  var isQuickview = (req.querystring.isQuickview && String(req.querystring.isQuickview) === 'true') || false;
  var loopState = req.querystring.loopState || '{count: 1}';
  var trueFit = (req.querystring.trueFit && String(req.querystring.trueFit) === 'true') || false;

  var template = 'product/components/variationAttribute';

  if (product.hbcProductType === 'bridal') {
    template = 'product/components/variationAttribute_bridal_home';
  }
  res.render('product/components/productVariationAttributes', {
    remoteVariant: true,
    template: template,
    trueFit: trueFit,
    product: product,
    searchableIfUnavailable: searchableIfUnavailable,
    isEdit: isEdit,
    wishlistAddToBag: wishlistAddToBag,
    isBundle: isBundle,
    isQuickview: isQuickview,
    loopState: loopState
  });
  next();
});

module.exports = server.exports();
