'use strict';

var server = require('server');
server.extend(module.superModule);

var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var Vertex = require('int_vertex/cartridge/scripts/Vertex');
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var preferences = require('*/cartridge/config/preferences');
var RootLogger = require('dw/system/Logger').getRootLogger();
var addressHelpers = require('*/cartridge/scripts/helpers/addressHelpers');
var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');
var TCCHelper = require('*/cartridge/scripts/checkout/TCCHelpers');
var Site = require('dw/system/Site');
var sitePrefs = require('dw/system/Site').getCurrent().getPreferences().getCustom();

var customPreferences = Site.current.preferences.custom;

/**
 *  Handle Ajax payment (and billing) form submit
 */
server.replace('SubmitPayment', server.middleware.https, csrfProtection.validateAjaxRequest, function (req, res, next) {
  var PaymentManager = require('dw/order/PaymentMgr');
  var HookManager = require('dw/system/HookMgr');
  var Resource = require('dw/web/Resource');
  var BasketMgr = require('dw/order/BasketMgr');

  var viewData = {};
  var paymentForm = server.forms.getForm('billing');
  // Veratax code
  delete session.custom.VertexAddressSuggestions;
  delete session.custom.VertexAddressSuggestionsError;
  delete session.custom.cardtype;
  // verify billing form data
  var billingFormErrors = COHelpers.validateBillingForm(paymentForm.addressFields);
  var contactInfoFormErrors = COHelpers.validateFields(paymentForm.contactInfoFields);
  // adreess validated
  var stateZipErrors = addressHelpers.validateStateVsZip(paymentForm);

  var currentBasket = BasketMgr.getCurrentBasket();

  var formFieldErrors = [];
  if (Object.keys(billingFormErrors).length) {
    formFieldErrors.push(billingFormErrors);
  } else if (Object.keys(stateZipErrors).length) {
    formFieldErrors.push(stateZipErrors);
  } else {
    viewData.address = {
      firstName: {
        value: paymentForm.addressFields.firstName.value
      },
      lastName: {
        value: paymentForm.addressFields.lastName.value
      },
      address1: {
        value: paymentForm.addressFields.address1.value
      },
      address2: {
        value: paymentForm.addressFields.address2.value
      },
      city: {
        value: paymentForm.addressFields.city.value
      },
      postalCode: {
        value: paymentForm.addressFields.postalCode.value
      },
      countryCode: {
        value: paymentForm.addressFields.country.value
      }
    };

    if (paymentForm.addressFields.country.value === 'UK') {
      viewData.address.stateCode = {
        value: paymentForm.addressFields.stateCodeUK.value
      };
    } else if (Object.prototype.hasOwnProperty.call(paymentForm.addressFields, 'states')) {
      viewData.address.stateCode = {
        value: paymentForm.addressFields.states.stateCode.value
      };
    }
  }
  // Setting the value in the billing form to update the order email and phone when the customer changes PI
  if (empty(paymentForm.contactInfoFields.email.value) || empty(paymentForm.contactInfoFields.phone.value)) {
    var defaultShipmentAddr = currentBasket.defaultShipment.shippingAddress;
    if (!empty(defaultShipmentAddr.custom.customerEmail) && !empty(defaultShipmentAddr.phone)) {
      paymentForm.contactInfoFields.email.value = defaultShipmentAddr.custom.customerEmail;
      paymentForm.contactInfoFields.phone.value = defaultShipmentAddr.phone;
    } else if (!empty(currentBasket.customerEmail) || (!empty(currentBasket.billingAddress) && !empty(currentBasket.billingAddress.phone))) {
      if (currentBasket.customerEmail && !empty(currentBasket.customerEmail)) {
        paymentForm.contactInfoFields.email.value = currentBasket.customerEmail;
      }
      if (currentBasket.billingAddress && currentBasket.billingAddress.phone && !empty(currentBasket.billingAddress.phone)) {
        paymentForm.contactInfoFields.phone.value = currentBasket.billingAddress.phone;
      }
    }
  }

  if (Object.keys(contactInfoFormErrors).length) {
    formFieldErrors.push(contactInfoFormErrors);
  } else {
    viewData.email = {
      value: paymentForm.contactInfoFields.email.value
    };

    viewData.phone = {
      value: paymentForm.contactInfoFields.phone.value
    };
  }

  var viewData = {};
  var paymentForm = server.forms.getForm('billing');
  // Veratax code
  delete session.custom.VertexAddressSuggestions;
  delete session.custom.VertexAddressSuggestionsError;
  delete session.custom.cardtype;

  // verify billing form data
  var billingFormErrors = COHelpers.validateBillingForm(paymentForm.addressFields);
  var contactInfoFormErrors = COHelpers.validateFields(paymentForm.contactInfoFields);
  // adreess validated
  var stateZipErrors = addressHelpers.validateStateVsZip(paymentForm);

  var currentBasket = BasketMgr.getCurrentBasket();

  var formFieldErrors = [];
  if (Object.keys(billingFormErrors).length) {
    formFieldErrors.push(billingFormErrors);
  } else if (Object.keys(stateZipErrors).length) {
    formFieldErrors.push(stateZipErrors);
  } else {
    viewData.address = {
      firstName: {
        value: paymentForm.addressFields.firstName.value
      },
      lastName: {
        value: paymentForm.addressFields.lastName.value
      },
      address1: {
        value: paymentForm.addressFields.address1.value
      },
      address2: {
        value: paymentForm.addressFields.address2.value
      },
      city: {
        value: paymentForm.addressFields.city.value
      },
      postalCode: {
        value: paymentForm.addressFields.postalCode.value
      },
      countryCode: {
        value: paymentForm.addressFields.country.value
      }
    };

    if (paymentForm.addressFields.country.value === 'UK') {
      viewData.address.stateCode = {
        value: paymentForm.addressFields.stateCodeUK.value
      };
    } else if (Object.prototype.hasOwnProperty.call(paymentForm.addressFields, 'states')) {
      viewData.address.stateCode = {
        value: paymentForm.addressFields.states.stateCode.value
      };
    }
  }
  // Setting the value in the billing form to update the order email and phone when the customer changes PI
  if (empty(paymentForm.contactInfoFields.email.value) || empty(paymentForm.contactInfoFields.phone.value)) {
    var defaultShipmentAddr = currentBasket.defaultShipment.shippingAddress;
    if (!empty(defaultShipmentAddr.custom.customerEmail) && !empty(defaultShipmentAddr.phone)) {
      paymentForm.contactInfoFields.email.value = defaultShipmentAddr.custom.customerEmail;
      paymentForm.contactInfoFields.phone.value = defaultShipmentAddr.phone;
    } else if (!empty(currentBasket.customerEmail) || (!empty(currentBasket.billingAddress) && !empty(currentBasket.billingAddress.phone))) {
      if (currentBasket.customerEmail && !empty(currentBasket.customerEmail)) {
        paymentForm.contactInfoFields.email.value = currentBasket.customerEmail;
      }
      if (currentBasket.billingAddress && currentBasket.billingAddress.phone && !empty(currentBasket.billingAddress.phone)) {
        paymentForm.contactInfoFields.phone.value = currentBasket.billingAddress.phone;
      }
    }
  }

  if (Object.keys(contactInfoFormErrors).length) {
    formFieldErrors.push(contactInfoFormErrors);
  } else {
    viewData.email = {
      value: paymentForm.contactInfoFields.email.value
    };

    viewData.phone = {
      value: paymentForm.contactInfoFields.phone.value
    };
  }

  var paymentMethodIdValue = paymentForm.paymentMethod.value;
  if (!PaymentManager.getPaymentMethod(paymentMethodIdValue).paymentProcessor) {
    throw new Error(Resource.msg('error.payment.processor.missing', 'checkout', null));
  }

  var paymentProcessor = PaymentManager.getPaymentMethod(paymentMethodIdValue).getPaymentProcessor();

  var paymentFormResult;
  if (HookManager.hasHook('app.payment.form.processor.' + paymentProcessor.ID.toLowerCase())) {
    paymentFormResult = HookManager.callHook('app.payment.form.processor.' + paymentProcessor.ID.toLowerCase(), 'processForm', req, paymentForm, viewData);
  } else {
    paymentFormResult = HookManager.callHook('app.payment.form.processor.default_form_processor', 'processForm');
  }

  if (paymentFormResult.error && paymentFormResult.fieldErrors) {
    formFieldErrors.push(paymentFormResult.fieldErrors);
  }

  if (formFieldErrors.length || paymentFormResult.serverErrors) {
    // respond with form data and errors
    res.json({
      form: paymentForm,
      fieldErrors: formFieldErrors,
      serverErrors: paymentFormResult.serverErrors ? paymentFormResult.serverErrors : [],
      error: true
    });
    return next();
  }

  var billForm = server.forms.getForm('billing');
  var cvv = billForm.creditCardFields.securityCode.htmlValue;

  if (!cvv) {
    cvv = request.httpParameterMap.securityCode;
  }

  res.setViewData(paymentFormResult.viewData);

  this.on('route:BeforeComplete', function (req, res) {
    // eslint-disable-line no-shadow
    var HookMgr = require('dw/system/HookMgr');
    var PaymentMgr = require('dw/order/PaymentMgr');
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var Transaction = require('dw/system/Transaction');
    var AccountModel = require('*/cartridge/models/account');
    var OrderModel = require('*/cartridge/models/order');
    var URLUtils = require('dw/web/URLUtils');
    var Locale = require('dw/util/Locale');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
    var validationHelpers = require('*/cartridge/scripts/helpers/basketValidationHelpers');
    var basketHashHelper = require('*/cartridge/scripts/helpers/basketHashHelpers');
    var commissionCookiesHelper = require('*/cartridge/scripts/helpers/commissionCookies');
    var isHBCCard = false;
    var Site = require('dw/system/Site');
    var Calendar = require('dw/util/Calendar');
    var preferences = require('*/cartridge/config/preferences');
    var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');

    currentBasket = BasketMgr.getCurrentBasket();
    var validatedProducts = validationHelpers.validateProducts(currentBasket);

    var billingData = res.getViewData();

    if (!currentBasket || validatedProducts.error) {
      delete billingData.paymentInformation;

      res.json({
        error: true,
        cartError: true,
        fieldErrors: [],
        serverErrors: [],
        redirectUrl: URLUtils.url('Cart-Show').toString()
      });
      return;
    }

    var billingAddress = currentBasket.billingAddress;
    var billingForm = server.forms.getForm('billing');
    var paymentMethodID = billingData.paymentMethod.value;
    var result;
    var shipments = currentBasket.shipments;

    billingForm.creditCardFields.cardNumber.htmlValue = '';
    billingForm.creditCardFields.securityCode.htmlValue = '';

    Transaction.wrap(function () {
      if (!billingAddress) {
        billingAddress = currentBasket.createBillingAddress();
      }

      billingAddress.setFirstName(billingData.address.firstName.value);
      billingAddress.setLastName(billingData.address.lastName.value);
      billingAddress.setAddress1(billingData.address.address1.value);
      billingAddress.setAddress2(billingData.address.address2.value);
      billingAddress.setCity(billingData.address.city.value);
      billingAddress.setPostalCode(billingData.address.postalCode.value);
      if (Object.prototype.hasOwnProperty.call(billingData.address, 'stateCode')) {
        billingAddress.setStateCode(billingData.address.stateCode.value);
      }
      billingAddress.setCountryCode(billingData.address.countryCode.value);

      billingAddress.setPhone(billingData.phone.value);
      if (billingData.email.value) {
        currentBasket.setCustomerEmail(billingData.email.value);
      }
      // for BOPIS shipments, set first name and last name from billing address
      for (let k = 0; k < shipments.length; k++) {
        let shipmentIndividual = shipments[k];
        if (
          'shipmentType' in shipmentIndividual.custom &&
          shipmentIndividual.custom.shipmentType === 'instore' &&
          shipmentIndividual.shippingAddress !== null
        ) {
          shipmentIndividual.shippingAddress.setFirstName(billingData.address.firstName.value);
          shipmentIndividual.shippingAddress.setLastName(billingData.address.lastName.value);
        }
      }
    });

    // Delete the masterpass basket attributes, if the payment is edited
    if ('masterpassTxnId' in currentBasket.custom && currentBasket.custom.masterpassTxnId) {
      Transaction.wrap(function () {
        currentBasket.custom.masterpassTxnId = null;
        currentBasket.custom.masterpassPostBackAttempted = null;
        currentBasket.custom.masterpassWalletId = null;
      });
    }

    // if there is no selected payment option and balance is greater than zero
    if (!paymentMethodID && currentBasket.totalGrossPrice.value > 0) {
      var noPaymentMethod = {};

      noPaymentMethod[billingData.paymentMethod.htmlName] = Resource.msg('error.no.selected.payment.method', 'payment', null);

      delete billingData.paymentInformation;

      res.json({
        form: billingForm,
        fieldErrors: [noPaymentMethod],
        serverErrors: [],
        error: true
      });
      return;
    }

    // Validate payment instrument if CC is used in basket
    if (paymentMethodIdValue === PaymentInstrument.METHOD_CREDIT_CARD) {
      var creditCardPaymentMethod = PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_CREDIT_CARD);
      var paymentCard = PaymentMgr.getPaymentCard(billingData.paymentInformation.cardType.value);

      var applicablePaymentCards = creditCardPaymentMethod.getApplicablePaymentCards(req.currentCustomer.raw, req.geolocation.countryCode, null);

      if (!applicablePaymentCards.contains(paymentCard)) {
        // Invalid Payment Instrument
        var invalidPaymentMethod = Resource.msg('error.payment.not.valid', 'checkout', null);
        delete billingData.paymentInformation;
        res.json({
          form: billingForm,
          fieldErrors: [],
          serverErrors: [invalidPaymentMethod],
          error: true
        });
        return;
      }

      // check if MPA payment is enabled
      var mpaEnabled = preferences.mpaEnabled;
      var mpaEnablePayment = false;

      if (preferences.mpaStartDate !== null && preferences.mpaEndDate !== null) {
        var mpaStartDate = preferences.mpaStartDate.toLocaleDateString();
        var mpaEndDate = preferences.mpaEndDate.toLocaleDateString();

        var todayDate = new Date();
        todayDate = todayDate.toLocaleDateString();
        todayDate = new Date(todayDate);
        var today = new Calendar(todayDate);

        mpaStartDate = new Date(mpaStartDate);
        var startDate = new Calendar(mpaStartDate);
        startDate.add(Calendar.HOUR, -24);

        mpaEndDate = new Date(mpaEndDate);
        var endDate = new Calendar(mpaEndDate);

        if (mpaEnabled && (today.after(startDate) || today.equals(startDate)) && (today.before(endDate) || today.equals(endDate))) {
          mpaEnablePayment = true;
        }
      }

      if (billingData.paymentInformation.cardType.value === 'MPA' && !mpaEnablePayment) {
        // Invalid MPA Payment Instrument
        var invalidPaymentMethod = Resource.msg('error.mpapayment.not.valid', 'checkout', null);
        delete billingData.paymentInformation;
        res.json({
          form: billingForm,
          mpaEnablePaymentError: true,
          fieldErrors: [],
          serverErrors: [invalidPaymentMethod],
          error: true
        });
        return;
      }
    }

    // check to make sure there is a payment processor
    if (!PaymentMgr.getPaymentMethod(paymentMethodID).paymentProcessor) {
      throw new Error(Resource.msg('error.payment.processor.missing', 'checkout', null));
    }

    var processor = PaymentMgr.getPaymentMethod(paymentMethodID).getPaymentProcessor();

    // handle payment processor
    var handlePass = true;
    if (HookMgr.hasHook('app.payment.processor.' + processor.ID.toLowerCase())) {
      result = HookMgr.callHook('app.payment.processor.' + processor.ID.toLowerCase(), 'Handle', currentBasket, billingData.paymentInformation, handlePass);
    } else {
      result = HookMgr.callHook('app.payment.processor.default', 'Handle');
    }

    // if the credit card submitted was Amex, check for the points.
    if (preferences.isAmexPayEnabled) {
      var amexpayHelper = require('*/cartridge/scripts/helpers/amexpayHelper');
      var pi = amexpayHelper.hasAmexCreditCard(currentBasket);
      var nonGCTotal = COHelpers.getNonGiftCardAmount(currentBasket, null);
      if (pi && nonGCTotal.remainingAmount.value > 0) {
        Transaction.wrap(function () {
          delete pi.custom.paywithPointsResponse;
          delete pi.custom.amexAmountApplied;
        });
        amexpayHelper.getRewardPoints(pi, cvv);
      }
    }
    // need to invalidate credit card fields
    if (result.error) {
      delete billingData.paymentInformation;

      res.json({
        form: billingForm,
        fieldErrors: result.fieldErrors,
        serverErrors: result.serverErrors,
        paymentErrors: result.paymentErrors,
        error: true
      });
      return;
    }

    if (tsysHelper.isTsysMode()) {
      if (billingData.paymentInformation && billingData.paymentInformation.cardType && billingData.paymentInformation.cardType.value === 'TCC') {
        var cardNumber = billingData.paymentInformation.cardNumber.value;
        if (billingData.paymentInformation.entryMode === ipaConstants.STORED_MODE) {
          cardNumber = billingData.paymentInformation.creditCardToken;
        }

        var date = TCCHelper.getExpirationDate(cardNumber);
        billingData.paymentInformation.tccExpirationDate = '' + date;
      }
    }

    // Set Token From token
    if (result.token) {
      billingData.paymentInformation.token = result.token;
    }

    // Save TCC if TSYS Mode
    if (!tsysHelper.isTsysMode()) {
      // Double make sure that we are not saving TCC in account.
      if (billingData.paymentInformation && billingData.paymentInformation.cardType && billingData.paymentInformation.cardType.value === 'TCC') {
        billingData.saveCard = false;
        billingData.saveCardDefault = false;
      }
    }

    if (HookMgr.hasHook('app.payment.form.processor.' + processor.ID.toLowerCase())) {
      HookMgr.callHook('app.payment.form.processor.' + processor.ID.toLowerCase(), 'savePaymentInformation', req, currentBasket, billingData);
    } else {
      HookMgr.callHook('app.payment.form.processor.default', 'savePaymentInformation');
    }

    // Save customer address in Address book while adding a new address in billing Step
    if (req.currentCustomer.addressBook) {
      if (!addressHelpers.checkIfAddressStored(billingAddress, req.currentCustomer.addressBook.addresses)) {
        addressHelpers.saveBillingAddress(billingAddress, req.currentCustomer, addressHelpers.generateAddressName(billingAddress));
      }
    }

    // Call Associate Discount Service
    if (
      !empty(billingData.paymentInformation) &&
      !empty(billingData.paymentInformation.cardType) &&
      (billingData.paymentInformation.cardType.value === 'SAKS' ||
        billingData.paymentInformation.cardType.value === 'SAKSMC' ||
        billingData.paymentInformation.cardType.value === 'HBC' ||
        billingData.paymentInformation.cardType.value === 'HBCMC' ||
        billingData.paymentInformation.cardType.value === 'MPA' ||
        billingData.paymentInformation.cardType.value === 'TCC')
    ) {
      isHBCCard = true;
    }
    if (isHBCCard) {
      if (billingData.paymentInformation.token) {
        // Call Associate Discount Service and set the associate tier in session
        hooksHelper('ucid.middleware.service', 'associateDiscountCode', [req, billingData.paymentInformation.token]);
        hooksHelper('ucid.middleware.service', 'applyFDDDiscount', [req, billingData]);
      }

      // eslint-disable-next-line no-param-reassign
      req.session.raw.custom.isHBCTenderType = true; // Setting the session attribute for the HBC Card dependednt promotions
    } else {
      delete req.session.raw.custom.associateTier; //eslint-disable-line
      delete req.session.raw.custom.specialVendorDiscountTier; //eslint-disable-line
      // eslint-disable-next-line no-param-reassign
      req.session.raw.custom.isHBCTenderType = false;
    }

    // Calculate the basket
    Transaction.wrap(function () {
      basketCalculationHelpers.calculateTotals(currentBasket);
    });

    // Re-calculate the payments.
    var calculatedPaymentTransaction = COHelpers.calculatePaymentTransaction(currentBasket);

    if (calculatedPaymentTransaction.error) {
      res.json({
        form: paymentForm,
        fieldErrors: [],
        serverErrors: [Resource.msg('error.technical', 'checkout', null)],
        error: true,
        orderTotalMismatch: true,
        maxGCLimitReached: result.maxGCLimitReached
      });
      return;
    }

    var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
    if (usingMultiShipping === true && currentBasket.shipments.length < 2) {
      req.session.privacyCache.set('usingMultiShipping', false);
      usingMultiShipping = false;
    }

    hooksHelper('app.customer.subscription', 'subscribeTo', [paymentForm.subscribe.checked, paymentForm.contactInfoFields.email.htmlValue], function () {});

    var currentLocale = Locale.getLocale(req.locale.id);

    commissionCookiesHelper.handleComxData(req, currentBasket);

    var basketModel = new OrderModel(currentBasket, {
      usingMultiShipping: usingMultiShipping,
      countryCode: currentLocale.country,
      containerView: 'basket'
    });
    // identify agent
    var isAgentUser = false;
    if (req.session.raw.isUserAuthenticated()) {
      isAgentUser = true;
    }
    // if the credit card submitted was Amex, check for the points.
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
    var amexHTML = null;
    if (preferences.isAmexPayEnabled && !isAgentUser) {
      var amexpayHelper = require('*/cartridge/scripts/helpers/amexpayHelper');
      var hasPointsOnAmexCard = amexpayHelper.hasPointsOnAmexCard(currentBasket);
      if (hasPointsOnAmexCard) {
        amexHTML = renderTemplateHelper.getRenderedHtml(
          {
            order: basketModel
          },
          'amex/checkpointbalance'
        );
      }
    }

    var accountModel = new AccountModel(req.currentCustomer);
    var renderedStoredPaymentInstrument = COHelpers.getRenderedPaymentInstruments(req, accountModel, basketModel);

    var customerSavedAddressesHtml = COHelpers.getCustomerSavedAddresses(req, accountModel, basketModel, currentLocale.country);
    session.custom.cardtype = !empty(paymentCard) && !empty(paymentCard.cardType) ? paymentCard.cardType : '';

    var orderProductSummary = renderTemplateHelper.getRenderedHtml(
      {
        order: basketModel
      },
      'checkout/orderProductSummary'
    );

    commissionCookiesHelper.handleComxData(req, currentBasket);

    res.json({
      renderedPaymentInstruments: renderedStoredPaymentInstrument,
      customer: accountModel,
      order: basketModel,
      form: billingForm,
      orderProductSummary: orderProductSummary,
      amexHTML: amexHTML,
      isBasketUpdated: basketHashHelper.validateBasketHashChange(currentBasket, req),
      error: false,
      customerSavedAddressesHtml: customerSavedAddressesHtml
    });
  });

  return next();
});

server.post('CreateAccount', server.middleware.https, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var AccountModel = require('*/cartridge/models/account');
  var Resource = require('dw/web/Resource');
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var OrderModel = require('*/cartridge/models/order');
  var formErrors = require('*/cartridge/scripts/formErrors');
  var Locale = require('dw/util/Locale');
  var currentLocale = Locale.getLocale(req.locale.id);

  var currentBasket = BasketMgr.getCurrentBasket();
  if (!currentBasket) {
    res.json({
      error: true,
      cartError: true,
      fieldErrors: [],
      serverErrors: [],
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }

  var createaccountForm = server.forms.getForm('createaccount');
  var registerEmail = createaccountForm.createaccountemail.value;
  var password = createaccountForm.password.value;

  if (createaccountForm.password.value !== createaccountForm.passwordconfirm.value) {
    createaccountForm.password.valid = false;
    createaccountForm.passwordconfirm.valid = false;
    createaccountForm.passwordconfirm.error = Resource.msg('error.message.mismatch.password', 'forms', null);
    createaccountForm.valid = false;
    delete createaccountForm.createaccountemail.error;
    delete createaccountForm.password.error;
  }
  if (!createaccountForm.valid) {
    res.json({
      success: false,
      formError: true,
      fields: formErrors.getFormErrors(createaccountForm)
    });
    return next();
  }
  if (!CustomerMgr.isAcceptablePassword(createaccountForm.password.value)) {
    createaccountForm.password.valid = false;
    createaccountForm.passwordconfirm.valid = false;
    createaccountForm.password.error = Resource.msg('error.message.password.constraints.not.matched', 'forms', null);
    delete createaccountForm.passwordconfirm.error;
    delete createaccountForm.createaccountemail.error;
    createaccountForm.valid = false;
  }
  if (!createaccountForm.valid) {
    res.json({
      success: false,
      formError: true,
      fields: formErrors.getFormErrors(createaccountForm)
    });
    return next();
  }

  var cust = CustomerMgr.getCustomerByLogin(registerEmail);
  if (cust !== null) {
    delete createaccountForm.password.error;
    delete createaccountForm.passwordconfirm.error;
    createaccountForm.valid = false;
    createaccountForm.createaccountemail.valid = false;
    createaccountForm.createaccountemail.error = Resource.msg('error.message.username.invalid', 'forms', null);
  }

  if (!createaccountForm.valid) {
    res.json({
      success: false,
      formError: true,
      fields: formErrors.getFormErrors(createaccountForm)
    });
    return next();
  }
  if (cust !== null) {
    res.json({
      success: false,
      error: true,
      errorMessage: Resource.msg('error.message.username.invalid', 'forms', null)
    });
    return next();
  }

  // Exclude Associates - Saks Associates are excluded
  if ('associateTier' in session.custom && session.custom.associateTier) {
    res.json({
      success: false,
      error: true,
      errorMessage: Resource.msg('saks.plus.associate.msg', 'checkout', null)
    });
    return next();
  }

  session.custom.emailaddress = registerEmail;
  var basketModel = new OrderModel(currentBasket, {
    countryCode: currentLocale.country,
    containerView: 'basket'
  });

  var accountModel = new AccountModel(req.currentCustomer);
  res.json({
    customer: accountModel,
    order: basketModel,
    error: false
  });
  return next();
});

server.replace('PlaceOrder', server.middleware.https, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var OrderMgr = require('dw/order/OrderMgr');

  var Resource = require('dw/web/Resource');
  var Transaction = require('dw/system/Transaction');
  var URLUtils = require('dw/web/URLUtils');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  var validationHelpers = require('*/cartridge/scripts/helpers/basketValidationHelpers');
  var addressHelpers = require('*/cartridge/scripts/helpers/addressHelpers');
  var Logger = require('dw/system/Logger');
  var OrderAPIUtils = require('*/cartridge/scripts/util/OrderAPIUtil');
  var Order = require('dw/order/Order');
  var preferences = require('*/cartridge/config/preferences');
  var HookMgr = require('dw/system/HookMgr');
  var CommissionCookiesHelper = require('*/cartridge/scripts/helpers/commissionCookies');

  var currentBasket = BasketMgr.getCurrentBasket();

  // Accertify InAuth logic
  var inAuthTransactionID = request.httpParameterMap.inAuthTID.stringValue;
  var HBCUtils = require('*/cartridge/scripts/HBCUtils');
  HBCUtils.setTransactionUUID(req, inAuthTransactionID);

  if (!currentBasket) {
    res.json({
      error: true,
      cartError: true,
      fieldErrors: [],
      serverErrors: [],
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }

  var validatedProducts = validationHelpers.validateProducts(currentBasket);

  var Locale = require('dw/util/Locale');
  var currentLocale = Locale.getLocale(req.locale.id);

  let shopRunnerEnabled = require('*/cartridge/scripts/helpers/shopRunnerHelpers').checkSRExpressCheckoutEligibility(currentBasket);

  if (shopRunnerEnabled) {
    var shoprunnerShippingMethodSelection = require('*/cartridge/scripts/ShoprunnerShippingMethodSelection').ShoprunnerShippingMethodSelection;
    shoprunnerShippingMethodSelection(true);

    var ShopRunner = require('*/cartridge/scripts/ShopRunner'); // eslint-disable-line
    ShopRunner.PlaceOrderPrepend(req, res, next); // eslint-disable-line
  }

  if (!currentBasket || validatedProducts.error) {
    res.json({
      error: true,
      cartError: true,
      fieldErrors: [],
      serverErrors: [],
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }

  if (req.session.privacyCache.get('fraudDetectionStatus')) {
    res.json({
      error: true,
      cartError: true,
      redirectUrl: URLUtils.url('Error-ErrorCode', 'err', '01').toString(),
      errorMessage: Resource.msg('error.technical', 'checkout', null)
    });

    return next();
  }

  var validationOrderStatus = hooksHelper(
    'app.validate.order',
    'validateOrder',
    currentBasket,
    require('*/cartridge/scripts/hooks/validateOrder').validateOrder
  );
  if (validationOrderStatus.error) {
    res.json({
      error: true,
      errorMessage: validationOrderStatus.message
    });
    return next();
  }

  // Check to make sure there is a shipping address
  if (currentBasket.defaultShipment.shippingAddress === null) {
    res.json({
      error: true,
      errorStage: {
        stage: 'shipping',
        step: 'address'
      },
      errorMessage: Resource.msg('error.no.shipping.address', 'checkout', null)
    });
    return next();
  }

  // Check to make sure there is a shipping address and the shipping Address Country matches the locale Country code else redirect the user to shipping page
  if (currentLocale.country !== currentBasket.defaultShipment.shippingAddress.countryCode.value) {
    res.json({
      error: true,
      errorStage: {
        stage: 'shipping',
        step: 'address'
      },
      errorMessage: Resource.msg('error.shipping.address.nomatch', 'checkout', null)
    });
    return next();
  }

  // Check to make sure billing address exists
  if (!currentBasket.billingAddress) {
    res.json({
      error: true,
      errorStage: {
        stage: 'payment',
        step: 'billingAddress'
      },
      errorMessage: Resource.msg('error.no.billing.address', 'checkout', null)
    });
    return next();
  }

  // Calculate the basket
  Transaction.wrap(function () {
    basketCalculationHelpers.calculateTotals(currentBasket);
  });

  // Calculate FDD, Associate and discount values for the product line items
  COHelpers.prorateLineItemDiscounts(currentBasket, req.currentCustomer.raw);

  // Re-validates existing payment instruments
  var validPayment = COHelpers.validatePayment(req, currentBasket);
  if (validPayment.error) {
    res.json({
      error: true,
      errorStage: {
        stage: 'payment',
        step: 'paymentInstrument'
      },
      errorMessage: validPayment.errorMessage
    });
    return next();
  }

  // Re-calculate the payments.
  var calculatedPaymentTransactionTotal = COHelpers.calculatePaymentTransaction(currentBasket);
  if (calculatedPaymentTransactionTotal.error) {
    res.json({
      error: true,
      errorStage: {
        stage: 'payment',
        step: 'paymentInstrument'
      },
      errorMessage: Resource.msg('error.payment.not.valid', 'checkout', null)
    });
    return next();
  }

  /**
   *  This block is to register the user to the site if purchasing SAKS Plus membership.
   *  Condition to execute the below block : basket should have saksplus product and customer should be a guest user
   */
  var saksplusHelper = require('*/cartridge/scripts/helpers/saksplusHelper');
  var hasSaksPlus = saksplusHelper.hasSaksPlusInBasket(currentBasket);
  var authenticated = req.currentCustomer.raw && req.currentCustomer.raw.authenticated ? true : false;
  var authenticatedCust = req.currentCustomer.raw;
  var newCustomer;
  if (!authenticated && hasSaksPlus) {
    var createaccountForm = server.forms.getForm('createaccount');
    var registerEmail = createaccountForm.createaccountemail.value;
    var password = createaccountForm.password.value;
    if (createaccountForm.password.value !== createaccountForm.passwordconfirm.value) {
      createaccountForm.password.valid = false;
      createaccountForm.passwordconfirm.valid = false;
      createaccountForm.passwordconfirm.error = Resource.msg('error.message.mismatch.password', 'forms', null);
      createaccountForm.valid = false;
      delete createaccountForm.password;
      delete createaccountForm.passwordConfirm;
      res.json({
        error: true,
        errorMessage: Resource.msg('error.message.mismatch.password', 'forms', null)
      });
      return next();
    }
    // changes done to merge the guest wishlist while the user is creating an acc
    var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
    var list = productListHelper.getList(req.currentCustomer.raw, {
      type: 10
    });
    var Locale = require('dw/util/Locale');
    var currentLanguage = Locale.getLocale(req.locale.id).getLanguage();
    var preferredLanguage = 'ENGLISH';
    if (currentLanguage == 'fr') {
      preferredLanguage = 'FRENCH';
    }
    // setting variables for the BeforeComplete function
    var registrationFormObj = {
      firstName: currentBasket.billingAddress.firstName,
      lastName: currentBasket.billingAddress.lastName,
      phone: currentBasket.billingAddress.phone,
      email: registerEmail,
      emailConfirm: registerEmail,
      password: createaccountForm.password.value,
      passwordConfirm: createaccountForm.passwordconfirm.value,
      zipCode: currentBasket.billingAddress.postalCode,
      isCanadianCustomer: 'F',
      validForm: createaccountForm.valid,
      form: createaccountForm,
      list: list
    };
    try {
      if (createaccountForm.valid) {
        Transaction.wrap(function () {
          var CustomerMgr = require('dw/customer/CustomerMgr');

          //Changes for SFSX-1704 - https://hbcdigital.atlassian.net/browse/SFSX-1704 - start
          //var newCustomer = CustomerMgr.createCustomer(registerEmail, password);
          //var authenticateCustomerResult = CustomerMgr.authenticateCustomer(registerEmail, password);

          //Updating the logic to allow legacy hash password login - refer to Order-CreateAccount flow.
          var profilePassword;
          //If AB Testing is still on, set the password from the site preference - Merchant Tools > Site Preferences > Custom Site Preference Groups > Storefront - tempLegacyLogin value.
          if (preferences.isABTestingOn) {
            profilePassword = preferences.tempLegacyLogin;
          } else {
            //else set the password entered by the customer to create the account.
            profilePassword = password;
          }
          //Create and authenticate the customer using the profilePassword value created above.
          newCustomer = CustomerMgr.createCustomer(registerEmail, profilePassword);
          var authenticateCustomerResult = CustomerMgr.authenticateCustomer(registerEmail, profilePassword);
          //Changes for SFSX-1704 - https://hbcdigital.atlassian.net/browse/SFSX-1704 - end

          if (authenticateCustomerResult.status !== 'AUTH_OK') {
            error = {
              authError: true,
              status: authenticateCustomerResult.status
            };
            throw error;
          }

          authenticatedCustomer = CustomerMgr.loginCustomer(authenticateCustomerResult, true);
          if (!authenticatedCustomer) {
            error = {
              authError: true,
              status: authenticateCustomerResult.status
            };
            throw error;
          } else {
            // assign values to the profile
            var newCustomerProfile = newCustomer.getProfile();

            newCustomerProfile.firstName = registrationFormObj.firstName;
            newCustomerProfile.lastName = registrationFormObj.lastName;
            newCustomerProfile.phoneHome = registrationFormObj.phone ? registrationFormObj.phone : '';
            newCustomerProfile.email = registrationFormObj.email;

            //Fix for SFSX-1435 - https://hbcdigital.atlassian.net/browse/SFSX-1435 - Start
            //This fix is to set legacyPasswordHash when a customer applies for a Saks Plus card and is required to create an account with place order.
            if (preferences.isABTestingOn) {
              var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
              var newPswd = legacyCustomerLogin.getPasswordLegacyHashed(password);
              newCustomerProfile.custom.legacyPasswordHash = newPswd;
            } //Fix for SFSX-1435 - https://hbcdigital.atlassian.net/browse/SFSX-1435 - End

            req.session.privacyCache.set('mergeWishlist', true);

            if (authenticatedCustomer) {
              var listLoggedIn = productListHelper.getList(authenticatedCustomer, { type: 10 });
              productListHelper.mergelists(listLoggedIn, registrationFormObj.list, req, { type: 10 });
              productListHelper.updateWishlistPrivacyCache(req.currentCustomer.raw, req, { type: 10 });
            }

            if (registrationFormObj.zipCode) {
              newCustomerProfile.custom.zipCode = registrationFormObj.zipCode;
            }
            var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
            accountHelpers.updateCustomerPreferences(req, newCustomerProfile.email, newCustomerProfile, registrationFormObj);

            // Update the custom attribute for last modified date as the system attribute lastModified is not true profile update from a customer perspective
            accountHelpers.updateAccLastModifiedDate(newCustomer);
            newCustomerProfile.custom.preferredLanguage = preferredLanguage;
            authenticatedCust = authenticatedCustomer;
          }
          // hook logic to send create loyalty profile
          if (HookMgr.hasHook('app.customer.loyalty.rewards.profile')) {
            var loyaltyResponse = hooksHelper(
              'app.customer.loyalty.rewards.profile',
              'callLoyaltyProfile',
              [authenticatedCustomer.profile, true],
              require('*/cartridge/scripts/loyalty/loyaltyProfileUtil').callLoyaltyProfile
            );
            if (loyaltyResponse !== null && Number(loyaltyResponse.ResponseCode) === 0) {
              newCustomerProfile.custom.moreCustomer =
                loyaltyResponse.LoyaltyProfile !== null &&
                loyaltyResponse.LoyaltyProfile.CustomerInfo !== null &&
                !!loyaltyResponse.LoyaltyProfile.CustomerInfo.moreNumber
                  ? loyaltyResponse.LoyaltyProfile.CustomerInfo.moreNumber
                  : '';
            }
          }

          //Change for SFSX-465 - start
          //Updating lastPasswordModified field in Profile system object.
          accountHelpers.updateLastPasswordModified(newCustomerProfile);
          //Change for SFSX-465 - end
          // send a registration email via CNS hook code change from accounthelpers
          hooksHelper(
            'app.customer.email.account.created',
            'sendCreateAccountEmail',
            authenticatedCustomer.profile,
            currentLanguage,
            require('*/cartridge/scripts/helpers/accountHelpers').sendCreateAccountEmail
          );

          // UCID Call to Sync
          hooksHelper('ucid.middleware.service', 'createUCIDCustomer', [authenticatedCustomer.profile, authenticatedCustomer.profile.customerNo, null]);
          authenticated = true;
        });
      }
    } catch (e) {
      delete createaccountForm.password;
      delete createaccountForm.passwordConfirm;
      if (e.authError) {
        res.setStatusCode(500);
        res.json({
          success: false,
          error: true,
          errorMessage: Resource.msg('error.message.username.invalid', 'forms', null)
        });
        return next();
      } else {
        createaccountForm.validForm = false;
        createaccountForm.createaccountemail.valid = false;
        createaccountForm.createaccountemail.error = Resource.msg('error.message.unable.to.create.account', 'login', null);
        res.json({
          success: false,
          error: true,
          errorMessage: Resource.msg('error.message.unable.to.create.account', 'login', null)
        });
        return next();
      }
    }
    delete createaccountForm.password;
    delete createaccountForm.passwordConfirm;
  } // end of registration block in the checkout flow for saksplus

  if (authenticated && hasSaksPlus) {
    // Exclude Associates - Saks Associates are excluded
    if ('associateTier' in session.custom && session.custom.associateTier) {
      res.json({
        success: false,
        error: true,
        errorMessage: Resource.msg('saks.plus.associate.msg', 'checkout', null)
      });
      return next();
    }
    // update the saksplus attributes
    saksplusHelper.updateSaksPlusAttrs(authenticatedCust);
  }

  CommissionCookiesHelper.handleComxData(req, currentBasket);
  // SaksFirst Redeem GiftCard
  //if (HookMgr.hasHook('app.payment.processor.saksfirst')) {
  //var RedeemGiftCardRes = hooksHelper('app.payment.processor.saksfirst', 'RedeemGiftCard', [currentBasket, req.currentCustomer.raw.profile], require('*/cartridge/scripts/hooks/payment/processor/saksfirst').RedeemGiftCard);
  //}

  //update the carrier service code
  COHelpers.updateCarrierCodes(currentBasket);

  // Creates a new order.
  var order = COHelpers.createOrder(currentBasket);
  if (!order) {
    res.json({
      error: true,
      errorMessage: Resource.msg('error.technical', 'checkout', null)
    });
    return next();
  }

  // changes for SFSX-3585 --start
  if (order && order.paymentInstrument && order.paymentInstrument.paymentMethod && order.paymentInstrument.paymentMethod === 'PayPal') {
    var paypalToken = order.paymentInstrument.custom && order.paymentInstrument.custom.paypalToken ? order.paymentInstrument.custom.paypalToken : '';
    if (empty(paypalToken)) {
      res.json({
        error: true,
        errorMessage: Resource.msg('error.payment.not.valid', 'checkout', null)
      });
      return next();
    } else if (!empty(paypalToken) && order.customerEmail === null) {
      if (order.paymentInstrument.custom && order.paymentInstrument.custom.paypalEmail) {
        Transaction.wrap(function () {
          order.customerEmail = order.paymentInstrument.custom.paypalEmail;
        });
      }
    }
  }
  // changes for SFSX-3585 --end

  // retrieve commission vendor cookies and store the values in order.
  // var preferences = dw.system.Site.current.preferences.custom;
  var site_referCookieName = preferences.site_referCookieName;
  var sf_storeidCookieName = preferences.sf_storeidCookieName;
  var sf_associdCookieName = preferences.sf_associdCookieName;
  var commissionCookies = CommissionCookiesHelper.getCommissionCookies(request, [site_referCookieName, sf_storeidCookieName, sf_associdCookieName]);
  if (commissionCookies && commissionCookies.length > 0) {
    if (
      commissionCookies.get(site_referCookieName) &&
      commissionCookies.get(site_referCookieName) !== '' &&
      commissionCookies.get(site_referCookieName).value === 'COMX'
    ) {
      try {
        var ComXData = 'COMXData' in order.custom && !empty(order.custom.COMXData) ? JSON.parse(order.custom.COMXData) : null;
        //Changes for SFSX-1833 - https://hbcdigital.atlassian.net/browse/SFSX-1833 - start
        var associateStoreAssociateNumber, associate1, associate2, store1, store2;
        /****
         * associateStoreAssociateNumber = XXXXXXYYYZZZZZZ,AAAAAABBBCCCCCC
         *
         * XXXXXX = This should be all zero’s, Length:6 Characters
         * store1 => YYY - Last 3 Digits of Store # of the Primary associate id, if length shorter than 3 it should be pre-fixed with zero like 083, 001
         * 			 In case of no primary associate this will be 000
         * associate1 => ZZZZZZ - 6 Digits of Primary Associate id, if length shorter than 6 it should be pre-fixed with zero like 000083, 001001
         * 				 In case of no Primary associate this will be 000000
         *
         * AAAAAA = This should be all zero’s, Length:6 Characters
         * store2 => BBB - Last 3 Digits of Store # of the Secondary associate id, if length shorter than 3 it should be pre-fixed with zero like 083, 001.
         * 		 	 In case of no Secondary associate this will be 000
         *
         * associate2 => CCCCCC - 6 Digits of Secondary Associate id, if length shorter than 6 it should be pre-fixed with zero like 000083, 001001.
         * 				 In case of no Secondary associate this will be 000000
         *
         * Total length will be 31 characters, In case of no primary associate/secondary associate, this filed should be 000000000000000,000000000000000
         *
         * */
        if (ComXData) {
          if (!empty(ComXData.comxAssociate1 && ComXData.comxStore1)) {
            store1 = '000' + ComXData.comxStore1.trim();
            store1 = store1.substr(store1.length - 3);
          } else {
            store1 = '000';
          }

          if (!empty(ComXData.comxAssociate1)) {
            associate1 = '000000' + ComXData.comxAssociate1.trim();
            associate1 = associate1.substr(associate1.length - 6);
          } else {
            associate1 = '000000';
          }

          if (!empty(ComXData.comxAssociate2 && ComXData.comxStore2)) {
            store2 = '000' + ComXData.comxStore2.trim();
            store2 = store2.substr(store2.length - 3);
          } else {
            store2 = '000';
          }

          if (!empty(ComXData.comxAssociate2)) {
            associate2 = '000000' + ComXData.comxAssociate2.trim();
            associate2 = associate2.substr(associate2.length - 6);
          } else {
            associate2 = '000000';
          }

          associateStoreAssociateNumber = '000000' + store1 + associate1 + ',' + ('000000' + store2 + associate2);
          Logger.debug(
            'CheckoutService-PlaceOrder -- associateStoreAssociateNumber -->, {0} and length --> {1}',
            associateStoreAssociateNumber,
            associateStoreAssociateNumber.length
          );
        } else {
          associateStoreAssociateNumber = '000000000000000' + ',' + '000000000000000';
          Logger.debug(
            'CheckoutService-PlaceOrder -- associateStoreAssociateNumber -->, {0} and length --> {1}',
            associateStoreAssociateNumber,
            associateStoreAssociateNumber.length
          );
        }

        if (!empty(associateStoreAssociateNumber)) {
          Transaction.wrap(function () {
            order.custom.site_refer = commissionCookies.get(site_referCookieName).value;
            order.custom.createUserid = associateStoreAssociateNumber;
            if (ComXData && ComXData.comxNote && !empty(ComXData.comxNote)) {
              order.custom.comxNote = ComXData.comxNote;
            }
          });
        }
        //Changes for SFSX-1833 - https://hbcdigital.atlassian.net/browse/SFSX-1833 - end
      } catch (e) {
        Logger.error('There was an error while Comx Order data: ' + e.message);
      }
    } else {
      if (
        commissionCookies.get(site_referCookieName) &&
        commissionCookies.get(site_referCookieName) !== '' &&
        commissionCookies.get(sf_storeidCookieName) &&
        commissionCookies.get(sf_associdCookieName)
      ) {
        var createUserIdPrefix = preferences.createUserIdPrefix;
        //Changes for SFSX-1833 - https://hbcdigital.atlassian.net/browse/SFSX-1833 - start
        //when the site referrer is salesfloor and the query param looks like this --
        //?site_refer=salesfloor&sf_rep=kingsley4&sf_storeid=0643&sf_associd=72312968&sf_source_origin=storefront
        //We still need to need to set the createUserId
        var createUserId, sfStoreid, sfAssocid;

        sfStoreid = commissionCookies.get(sf_storeidCookieName).value;
        if (sfStoreid) {
          sfStoreid = '000' + sfStoreid.trim();
          sfStoreid = sfStoreid.substr(sfStoreid.length - 3);
        } else {
          sfStoreid = '000';
        }

        sfAssocid = commissionCookies.get(sf_associdCookieName).value;
        if (sfAssocid) {
          sfAssocid = '000000' + sfAssocid.trim();
          sfAssocid = sfAssocid.substr(sfAssocid.length - 6);
        } else {
          sfAssocid = '000000';
        }

        //Since associate 2 and store 2 are missing we set it to all zeros, six 0's for the prefix, 3 for storeid, and 6 more for associate id.
        createUserId = createUserIdPrefix + sfStoreid + sfAssocid + ',' + createUserIdPrefix + '000000000';
        Logger.debug('CheckoutService-PlaceOrder -- createUserId -->, {0} and length --> {1}', createUserId, createUserId.length);

        Transaction.wrap(function () {
          order.custom.site_refer = commissionCookies.get(site_referCookieName).value;
          order.custom.createUserid = createUserId;
        }); //Changes for SFSX-1833 - https://hbcdigital.atlassian.net/browse/SFSX-1833 - end
      }
    }
  }

  // Call inventory reservation service.
  var reserveInventoryResult = hooksHelper('app.inventory.reservation', 'reserveInventory', [order]);
  // Fail the order and throw user to previous checkout step, only if service is up and any of the items doesn't have sufficient inventory in OMS.
  if (!reserveInventoryResult.hasInventory) {
    Transaction.wrap(function () {
      order.trackOrderChange('Failed to reserve inventory');
      OrderMgr.failOrder(order);
    });
    if (reserveInventoryResult.omsInventory && reserveInventoryResult.omsInventory.length > 0) {
      var omsInventory = JSON.stringify(reserveInventoryResult.omsInventory);
      session.custom.omsInventory = omsInventory;
      var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
      cartHelper.adjustBasketQuantities(currentBasket, reserveInventoryResult.omsInventory);
    }
    res.json({
      error: true,
      errorMessage: Resource.msg('error.technical.inventory', 'checkout', null)
    });
    return next();
  }

  // update the proratedPriceTotal
  try {
    var prorationHelper = require('*/cartridge/scripts/checkout/prorationHelper');
    prorationHelper.doLineItemCalculations(order);
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    Transaction.wrap(function () {
      order.trackOrderChange(errorMsg);
    });
  }

  // Handles payment authorization
  var handlePaymentResult = COHelpers.handlePayments(order, order.orderNo, req);
  if (handlePaymentResult.error) {
    // Call cancel reservation.
    hooksHelper('app.inventory.reservation', 'cancelInventoryReservation', [order.custom.reservationID]);

    var errorMsg = 'error.payment.failure';

    if(order && order.custom && order.custom.forterDecision && order.custom.forterDecision.value === "DECLINED") {
      errorMsg = 'error.payment.failure.frauddecline';
    }

    res.json({
      error: true,
      errorMessage: Resource.msg(errorMsg, 'checkout', null)
    });
    return next();
  }

  // Vertax Specific code
  Vertex.CalculateTax('Invoice', order); // eslint-disable-line

  var fraudDetectionStatus = hooksHelper(
    'app.fraud.detection',
    'fraudDetection',
    currentBasket,
    require('*/cartridge/scripts/hooks/fraudDetection').fraudDetection
  );
  if (fraudDetectionStatus.status === 'fail') {
    Transaction.wrap(function () {
      order.trackOrderChange('Failed due to fraud');
      OrderMgr.failOrder(order);
    });

    // fraud detection failed
    req.session.privacyCache.set('fraudDetectionStatus', true);

    res.json({
      error: true,
      cartError: true,
      redirectUrl: URLUtils.url('Error-ErrorCode', 'err', fraudDetectionStatus.errorCode).toString(),
      errorMessage: Resource.msg('error.technical', 'checkout', null)
    });

    return next();
  }

  // Places the order
  var placeOrderResult = COHelpers.placeOrder(order, fraudDetectionStatus);
  if (placeOrderResult.error) {
    res.json({
      error: true,
      errorMessage: Resource.msg('error.technical', 'checkout', null)
    });
    var args = {};
    args.session = req.session;
    args.Order = order;
    args.remoteAddress =
      req.session.raw && req.session.raw.clickStream && req.session.raw.clickStream.last ? req.session.raw.clickStream.last.remoteAddress : req.remoteAddress;
    args.customer = req.currentCustomer.raw;
    args.deviceFingerPrintID = req.session.privacyCache.get('transactionUUID');
    // Capture the no of failure attempts executed by the customer in the same session
    var attempts = req.session.privacyCache.get('failAttempts') || 0;
    attempts += 1;
    req.session.privacyCache.set('failAttempts', attempts);

    args.failAttempts = attempts;
    // call Reversal for any technical error
    hooksHelper('app.payment.processor.ipa_credit', 'Reverse', [order.orderNo, args]);
    return next();
  }

  var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
  cartHelper.removeAllItemsFromProductList();
  // save the device finger print and fail attempts to the order object
  Transaction.wrap(function () {
    if (req.session.privacyCache.get('transactionUUID')) {
      order.custom.deviceFingerPrintID = req.session.privacyCache.get('transactionUUID');
    }
    if (req.session.privacyCache.get('failAttempts')) {
      order.custom.failAttempts = req.session.privacyCache.get('failAttempts');
    }
    // mark reversal for Cancelled orders as false
    order.custom.reversalForCancelOrder = false;
  });

  // Update EDD Date
  var shippingHelpers = require('*/cartridge/scripts/checkout/shippingHelpers');
  // Delete the EDD Save Zip code
  delete session.privacy.EDDZipCode;
  var orderShipments = order.shipments;
  Transaction.wrap(function () {
    for (var i = 0; i < orderShipments.length; i++) {
      var thisShipment = orderShipments[i];
      var EDDMap;
      if ('EDDResponse' in thisShipment.custom && !empty(thisShipment.custom.EDDResponse)) {
        EDDMap = shippingHelpers.getEDDDateMap(JSON.parse(thisShipment.custom.EDDResponse));
      }
      var orderEstimateddate = shippingHelpers.getEDDdate(thisShipment.shippingMethod, EDDMap);
      if (orderEstimateddate.EstimatedDate) {
        thisShipment.custom.selectedEstimatedDate = orderEstimateddate.EstimatedDate;
      }
      if (orderEstimateddate.OrdercreationEstimatedDate) {
        thisShipment.custom.selectedEstimatedDateforOMS = orderEstimateddate.OrdercreationEstimatedDate;
      }

      delete thisShipment.custom.EDDResponse;
    }
  });

  //Moving this Shoprunner section up to fix ticket SFDEV-9508 : To pass missing sr_token value in order xml file for SR orders
  //setting the order no. in response as its required by the Shoprunner to set the SRToken at order level at different scenarios
  res.setViewData({
    orderID: order.orderNo
  });

  if (shopRunnerEnabled) {
    var ShopRunner = require('*/cartridge/scripts/ShopRunner'); // eslint-disable-line
    ShopRunner.PlaceOrderAppend(req, res, next); // eslint-disable-line
  }

  Transaction.wrap(function () {
    //SFDEV-11204 | Generate OMS Create Order XML to be used in ExportOrders batch job
    order.custom.omsCreateOrderXML = OrderAPIUtils.buildRequestXML(order);
  });

  if (customPreferences.orderCreateBatchEnabled !== true) {
    try {
      var responseOrderXML = OrderAPIUtils.createOrderInOMS(order);
      // eslint-disable-next-line no-undef
      var resXML = new XML(responseOrderXML);
      Logger.debug('resXML.child(ResponseMessage) -->' + resXML.child('ResponseMessage'));

      if (resXML.child('ResponseMessage').toString() === 'Success') {
        Transaction.wrap(function () {
          order.exportStatus = Order.EXPORT_STATUS_EXPORTED;
          order.trackOrderChange('Order Export Successful');
          order.trackOrderChange(responseOrderXML);
        });
      } else {
        Transaction.wrap(function () {
          order.exportStatus = Order.EXPORT_STATUS_FAILED;
          order.trackOrderChange('Order Exported Failed');
          order.trackOrderChange(responseOrderXML);
        });
      }
    } catch (e) {
      var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
      Transaction.wrap(function () {
        order.trackOrderChange(errorMsg);
      });
      RootLogger.fatal('Error while calling the Order Create Service for Order ' + order.orderNo + ' error message ' + e.message);
    }
  }

  try {
    var amexpayHelper = require('*/cartridge/scripts/helpers/amexpayHelper');
    amexpayHelper.writeCOForOfflineRedemption(order);
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    RootLogger.error('Error while creating the amex pay custom objects for order' + order.orderNo + ' error message ' + errorMsg);
  }
  // If the Order has the First Day Discount applied, call the FDDNotify Service
  var collections = require('*/cartridge/scripts/util/collections');
  var issFDDApplied = false;
  var priceAdjs = order.getPriceAdjustments();
  if (!empty(priceAdjs)) {
    collections.forEach(priceAdjs, function (priceAdj) {
      var promo = priceAdj.getPromotion();
      if (!empty(promo) && 'promotionType' in promo.custom && promo.custom.promotionType !== null && promo.custom.promotionType.value.toString() === 'FDD') {
        issFDDApplied = true;
      }
    });
  }
  // If FDD Promotion applied to this Order, call the Service
  if (issFDDApplied) {
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var paymentToken = '';
    var paymentInstruments = order.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);
    collections.forEach(paymentInstruments, function (paymentInstr) {
      paymentToken = paymentInstr.getCreditCardToken();
    });

    if (!empty(paymentToken)) {
      hooksHelper('ucid.middleware.service', 'fddNotifyService', [paymentToken]);

      var profile = customer.profile;
      if (profile) {
        Transaction.wrap(function () {
          profile.custom.fddUsedDate = new Date();
        });
      }
    }
  }

  // reset the fail attempts after the order is placed successful
  req.session.privacyCache.set('failAttempts', null);
  req.session.privacyCache.set('basketHashAfterCustAddressUpdate', null);

  if (req.currentCustomer.addressBook) {
    // save all used shipping addresses to address book of the logged in customer
    var allAddresses = addressHelpers.gatherShippingAddresses(order);
    allAddresses.forEach(function (address) {
      if (!addressHelpers.checkIfAddressStored(address, req.currentCustomer.addressBook.addresses)) {
        addressHelpers.saveAddress(address, req.currentCustomer, addressHelpers.generateAddressName(address));
      }
    });
  }
  try {
    if (hasSaksPlus && newCustomer && newCustomer.authenticated) {
      Transaction.wrap(function () {
        var newCustomerProfile = newCustomer.getProfile();
        order.setCustomer(newCustomer);
        var allAddresses = addressHelpers.gatherShippingAddresses(order);
        allAddresses.forEach(function (address) {
          addressHelpers.saveAddress(address, { raw: newCustomer }, addressHelpers.generateAddressName(address));
        });
        var registrationObj = {};
        var creditCardPaymentDetails = COHelpers.getOrderCreditCardPaymentDetails(order);
        if (creditCardPaymentDetails) {
          registrationObj.payment = creditCardPaymentDetails;
        }
        if (registrationObj.payment) {
          COHelpers.saveOrderPaymentToProfile(newCustomerProfile, registrationObj.payment);
        }
      });
    }
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    RootLogger.error('Error while creating the Saks + account for order' + order.orderNo + ' error message ' + errorMsg);
  }

  // removed order confirmation email as it would be sent via OMS.

  // Reset usingMultiShip after successful Order placement
  req.session.privacyCache.set('usingMultiShipping', false);

  // This is to clear the payment form after place order. Earlier this was done during submission, beyond which we will not be able to access the security code of the card
  var paymentForm = server.forms.getForm('billing');
  delete paymentForm.creditCardFields;
  req.session.privacyCache.set('failAttempts', 0);

  delete session.privacy.skipRealTimeTaxes;
  delete session.privacy.cartStateHash;
  delete session.custom.emailaddress;
  delete session.custom.saksplusProduct;
  delete session.custom.csrPrivateSale;
  delete session.custom.isExpressCheckout;

  // TODO: Exposing a direct route to an Order, without at least encoding the orderID
  //  is a serious PII violation.  It enables looking up every customers orders, one at a
  //  time.
  res.json({
    error: false,
    orderID: order.orderNo,
    orderToken: order.orderToken,
    continueUrl: URLUtils.url('Order-Confirm').toString()
  });

  return next();
});

/**
 * PAYPAL CODE prepended
 * Adding this particular code in the site specific cartridge as VERTEX is replace the specific route
 * Whereas paypal is prepending the data to the same route.
 * Hence, to avoid the conflicts in core we are prepending the the PAYPAL logic in the site specific cartridge
 * Note: This only executes for PayPal cartridge version 18.3.1 since there is no isPaypal parameter in 21.1.1
 */

server.prepend('SubmitPayment', server.middleware.https, csrfProtection.validateAjaxRequest, function (req, res, next) {
  var isPaypal = request.httpParameterMap.isPaypal.booleanValue;

  if (!isPaypal) {
    next();
    return;
  }

  delete req.session.raw.custom.associateTier;
  delete req.session.raw.custom.specialVendorDiscountTier;
  req.session.raw.custom.isHBCTenderType = false;

  var data = res.getViewData();
  if (data && data.csrfError) {
    res.json();
    this.emit('route:Complete', req, res);
    return;
  }

  var HookMgr = require('dw/system/HookMgr');
  var Resource = require('dw/web/Resource');
  var PaymentMgr = require('dw/order/PaymentMgr');
  var Transaction = require('dw/system/Transaction');
  var AccountModel = require('*/cartridge/models/account');
  var OrderModel = require('*/cartridge/models/order');
  var Locale = require('dw/util/Locale');
  var URLUtils = require('dw/web/URLUtils');
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();

  if (!currentBasket) {
    res.json({
      error: true,
      cartError: true,
      fieldErrors: [],
      serverErrors: [],
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    this.emit('route:Complete', req, res);
    return;
  }

  var paypalForm = session.forms.billing.paypal;
  var billingForm = server.forms.getForm('billing');
  var isUseBillingAgreement = paypalForm.useCustomerBillingAgreement.checked;
  var viewData = {};

  var paymentMethodID = 'PayPal';
  billingForm.paymentMethod.value = paymentMethodID;
  viewData.paymentMethod = {
    value: paymentMethodID,
    htmlName: billingForm.paymentMethod.htmlName
  };

  var billingFormErrors = COHelpers.validateBillingForm(billingForm.addressFields);

  if (Object.keys(billingFormErrors).length) {
    res.json({
      form: billingForm,
      fieldErrors: [billingFormErrors],
      serverErrors: [],
      error: true,
      paymentMethod: viewData.paymentMethod
    });
    this.emit('route:Complete', req, res);
    return;
  }

  viewData.address = {
    firstName: { value: billingForm.addressFields.firstName.value },
    lastName: { value: billingForm.addressFields.lastName.value },
    address1: { value: billingForm.addressFields.address1.value },
    address2: { value: billingForm.addressFields.address2.value },
    city: { value: billingForm.addressFields.city.value },
    postalCode: { value: billingForm.addressFields.postalCode.value },
    countryCode: { value: billingForm.addressFields.country.value }
  };

  if (billingForm.addressFields.country.value === 'UK') {
    viewData.address.stateCode = {
      value: billingForm.addressFields.stateCodeUK.value
    };
  } else if (Object.prototype.hasOwnProperty.call(billingForm.addressFields, 'states')) {
    viewData.address.stateCode = {
      value: billingForm.addressFields.states.stateCode.value
    };
  }

  var email = billingForm.contactInfoFields.email.value;
  if (isUseBillingAgreement) {
    email = customer.getProfile().getEmail();
  }
  viewData.email = {
    value: email
  };

  res.setViewData(viewData);

  var billingAddress = currentBasket.billingAddress;
  Transaction.wrap(function () {
    if (!billingAddress) {
      billingAddress = currentBasket.createBillingAddress();
    }

    billingAddress.setFirstName(billingForm.addressFields.firstName.value);
    billingAddress.setLastName(billingForm.addressFields.lastName.value);
    billingAddress.setAddress1(billingForm.addressFields.address1.value);
    billingAddress.setAddress2(billingForm.addressFields.address2.value);
    billingAddress.setCity(billingForm.addressFields.city.value);
    billingAddress.setPostalCode(billingForm.addressFields.postalCode.value);
    billingAddress.setCountryCode(billingForm.addressFields.country.value);

    if (billingForm.addressFields.country.value === 'UK') {
      billingAddress.setStateCode(billingForm.addressFields.stateCodeUK.value);
    } else if (Object.prototype.hasOwnProperty.call(billingForm.addressFields, 'states')) {
      billingAddress.setStateCode(billingForm.addressFields.states.stateCode.value);
    }
    if (email) {
      currentBasket.setCustomerEmail(email);
    }
    /* TODO
        if (viewData.storedPaymentUUID) {
            billingAddress.setPhone(req.currentCustomer.profile.phone);
            currentBasket.setCustomerEmail(req.currentCustomer.profile.email);
        } else {
            billingAddress.setPhone(viewData.phone.value);
            currentBasket.setCustomerEmail(viewData.email.value);
        }
        */
  });

  Transaction.wrap(function () {
    HookMgr.callHook('dw.order.calculate', 'calculate', currentBasket);
  });

  var processor = PaymentMgr.getPaymentMethod(paymentMethodID).getPaymentProcessor();
  if (!processor) {
    throw new Error(Resource.msg('error.payment.processor.missing', 'checkout', null));
  }

  var processorResult = null;
  if (HookMgr.hasHook('app.payment.processor.' + processor.ID.toLowerCase())) {
    processorResult = HookMgr.callHook('app.payment.processor.' + processor.ID.toLowerCase(), 'Handle', currentBasket);
  } else {
    throw new Error('hooks/payment/processor/paypal.js file is missing or "app.payment.processor.paypal" hook is wrong setted');
  }

  if (processorResult.error) {
    res.json({
      form: billingForm,
      fieldErrors: [],
      serverErrors: [],
      error: true
    });
    if (processorResult.paypalBillingAgreementNotActaual) {
      res.json({
        fieldErrors: [],
        serverErrors: [],
        cartError: true,
        redirectUrl: ''
      });
    }
    this.emit('route:Complete', req, res);
    return;
  }

  var usingMultiShipping = false; // Current integration support only single shpping
  req.session.privacyCache.set('usingMultiShipping', usingMultiShipping);

  var currentLocale = Locale.getLocale(req.locale.id);
  var basketModel = new OrderModel(currentBasket, {
    usingMultiShipping: usingMultiShipping,
    countryCode: currentLocale.country,
    containerView: 'basket'
  });
  var accountModel = new AccountModel(req.currentCustomer);

  // The hack for MFRA renders right data in function updatePaymentInformation(order). TODO need to find better solution
  basketModel.resources.cardType = '';
  basketModel.resources.cardEnding = '';
  basketModel.billing.payment.selectedPaymentInstruments[0].type = '';
  basketModel.billing.payment.selectedPaymentInstruments[0].maskedCreditCardNumber = processorResult.paypalEmail;
  basketModel.billing.payment.selectedPaymentInstruments[0].expirationMonth = 'PayPal ';
  basketModel.billing.payment.selectedPaymentInstruments[0].expirationYear =
    ' ' +
    dw.util.StringUtils.formatMoney(new dw.value.Money(basketModel.billing.payment.selectedPaymentInstruments[0].amount, currentBasket.getCurrencyCode()));
  // End the hack

  res.json({
    renderedPaymentInstruments: COHelpers.getRenderedPaymentInstruments(req, accountModel),
    customer: accountModel,
    order: basketModel,
    paypalProcessorResult: processorResult,
    form: billingForm,
    error: false
  });
  this.emit('route:Complete', req, res);
});

/**
 * Handle Ajax on submit of Instore Pickup person form
 */
server.replace('SubmitInstorePickUp', server.middleware.https, csrfProtection.validateAjaxRequest, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();
  var Transaction = require('dw/system/Transaction');

  var form = server.forms.getForm('instorepickup');
  var pickUpPersonData = {};
  var skipShipping = false;

  // verify instorepickup form data
  var instorePickUpFormErrors = COHelpers.validateInstorePickUpForm(form);
  // check for errors
  if (Object.keys(instorePickUpFormErrors).length > 0) {
    res.json({
      form: form,
      fieldErrors: [instorePickUpFormErrors],
      serverErrors: [],
      error: true
    });
  } else {
    pickUpPersonData = {
      fullName: form.fullName.value,
      email: form.email.value,
      phone: form.phone.value
    };

    var billingForm = server.forms.getForm('billing');
    billingForm.contactInfoFields.email.value = form.email.value;
    billingForm.contactInfoFields.phone.value = form.phone.value;
    this.on('route:BeforeComplete', function (req, res) {
      // eslint-disable-line no-shadow
      var AccountModel = require('*/cartridge/models/account');
      var OrderModel = require('*/cartridge/models/order');
      var Locale = require('dw/util/Locale');

      var currentLocale = Locale.getLocale(req.locale.id);
      // save detail as json string
      Transaction.wrap(function () {
        currentBasket.custom.personInfoMarkFor = JSON.stringify(pickUpPersonData);
        currentBasket.custom.smsOptIn = form.smsOptIn.value;
        if (!currentBasket.customerEmail && pickUpPersonData && pickUpPersonData.email && pickUpPersonData.email !== '') {
          currentBasket.setCustomerEmail(pickUpPersonData.email);
        }
      });

      var basketModel = new OrderModel(currentBasket, {
        usingMultiShipping: false,
        countryCode: currentLocale.country,
        containerView: 'basket'
      });
      var hasInStoreItems = basketModel.items.hasInStoreItems;
      if (hasInStoreItems.length > 0 && hasInStoreItems.length === currentBasket.productLineItems.size()) {
        skipShipping = true;
      }
      res.json({
        error: false,
        order: basketModel,
        customer: new AccountModel(req.currentCustomer),
        instorepickup: form,
        skipShipping: skipShipping,
        giftWrapEnabledforBopis: preferences.giftWrapEnabledforBopis
      });
    });
  }

  return next();
});

server.post('SubmitGiftOptions', server.middleware.https, csrfProtection.validateAjaxRequest, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var URLUtils = require('dw/web/URLUtils');
  var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
  var validationHelpers = require('*/cartridge/scripts/helpers/basketValidationHelpers');
  var AccountModel = require('*/cartridge/models/account');
  var OrderModel = require('*/cartridge/models/order');
  var Transaction = require('dw/system/Transaction');
  var Locale = require('dw/util/Locale');
  var currentLocale = Locale.getLocale(req.locale.id);

  var customer = new AccountModel(req.currentCustomer);
  var currentBasket = BasketMgr.getCurrentBasket();

  var form = server.forms.getForm('shipping');
  var result = {};
  result.isGift = form.shippingAddress.isGift.checked;
  result.giftMessage = result.isGift ? form.shippingAddress.giftMessage.value : null;
  result.giftRecipientName = result.isGift ? form.shippingAddress.giftRecipientName.value : null;
  result.giftWrapType = req.form.addgift ? req.form.addgift : null;

  var giftResult = COHelpers.setGift(currentBasket.defaultShipment, result.isGift, result.giftMessage, result.giftRecipientName, result.giftWrapType, null);

  if (giftResult.error) {
    res.json({
      error: giftResult.error,
      fieldErrors: [],
      serverErrors: [giftResult.errorMessage]
    });
    return;
  }

  var basketModel = new OrderModel(currentBasket, {
    usingMultiShipping: false,
    shippable: true,
    countryCode: currentLocale.country,
    containerView: 'basket'
  });

  res.json({
    customer: customer,
    order: basketModel,
    form: server.forms.getForm('shipping')
  });
  return next();
});

server.replace('TCCVerify', function (req, res, next) {
  var Resource = require('dw/web/Resource');
  var TCCHelper = require('*/cartridge/scripts/checkout/TCCHelpers');
  var tcc = req.querystring.tcc;
  var validateSecurityCode = TCCHelper.validateSecurityCode(tcc);
  var isValid = true;
  var validationMessage = '';

  if (req.querystring.verifydate == 'true') {
    var isTCCDateExpired = TCCHelper.isTCCDateExpired(tcc);
    if (isTCCDateExpired) {
      isValid = false;
      validationMessage = Resource.msg('error.message.tcc.date', 'checkout', null);
    }
  }

  if (!validateSecurityCode) {
    isValid = false;
  }

  res.json({
    validTCC: isValid,
    validationMessage: validationMessage
  });

  next();
});

module.exports = server.exports();
