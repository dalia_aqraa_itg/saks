'use strict';

var server = require('server');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var preferences = require('*/cartridge/config/preferences');
var collections = require('*/cartridge/scripts/util/collections');
var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');
var Resource = require('dw/web/Resource');
var Logger = require('dw/system/Logger');
var cache = require('*/cartridge/scripts/middleware/cache');
server.extend(module.superModule);

var CartModelGlobal = preferences.CartBEPerformanceChangesEnable ? require('*/cartridge/models/cart') : null;

/**
 * @function
 * @description appends the parameters to the given url and returns the changed url
 * @param {string} url the url to which the parameters will be added
 * @param {Object} name of the anchor tag
 * @param {string} value the url to which the parameters will be added
 * @returns {Object} url element.
 */
function appendParamToURL(url, name, value) {
    // quit if the param already exists
    /* eslint-disable */
    if (url.indexOf(name + '=') !== -1) {
        return url;
    }
    value = decodeURIComponent(value);
    var separator = url.indexOf('?') !== -1 ? '&' : '?';
    return url + separator + name + '=' + encodeURIComponent(value);
    /* eslint-disable */
}

server.replace(
    'Show',
    server.middleware.https,
    consentTracking.consent,
    csrfProtection.generateToken,
    function (req, res, next) {
        var BasketMgr = require('dw/order/BasketMgr');
        var Transaction = require('dw/system/Transaction');
        var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
        var reportingUrlsHelper = require('*/cartridge/scripts/reportingUrls');
        var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
        var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
        var checkoutHelper = require('*/cartridge/scripts/checkout/checkoutHelpers');
        var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
        var URLUtils = require('dw/web/URLUtils');
        var OrderMgr = require('dw/order/OrderMgr');
        var KlarnaUtils = require('*/cartridge/scripts/klarna/klarnaUtils');
        var inventoryError = req.querystring.inventoryerror ? req.querystring.inventoryerror : null;
        var currentBasket = BasketMgr.getCurrentBasket();
        var CartModel = preferences.CartBEPerformanceChangesEnable ? CartModelGlobal : require('*/cartridge/models/cart');

        if (preferences.CartBEPerformanceChangesEnable) {
            var shopRunnerHelpers = require('*/cartridge/scripts/helpers/shopRunnerHelpers');
        }

        // changes for SFDEV-5099
        var updatedLineItems = cartHelper.adjustLineItemQuantities(currentBasket);
        var reportingURLs;
        var purchaseLimitedSKUMap = cartHelper.resetPLISPurchaseLimit(currentBasket);
        var basketLimitReached = false;

        /**
         * On a private sale, the promo price and the price book are same
         * Platform randomly sometimes considers the private-sale pricebook price and other time as a promotion discount
         * Since the promotionalDiscount was also a Price from pricebook, it is benefial to force promotional price and get the PIP to display
         */
        if (session.privacy.privateSalePromoID) {
            var PriceBookMgr = require('dw/catalog/PriceBookMgr');
            PriceBookMgr.setApplicablePriceBooks(null);
            // setting a different session variable to not apply pricebooks to the session on the common model
            session.privacy.skipPricebookCal = true;
        }

        var HBCUtils = require('*/cartridge/scripts/HBCUtils');
        var inAuthDetails = HBCUtils.getInAuthDetails(req);
        if (inAuthDetails) {
            // Setting up this session value to pass InAuth ID for SR orders. SFDEV-11458
            if (inAuthDetails.TransactionUUID) {
                req.session.privacyCache.set('inAuthTransactionUUID', inAuthDetails.TransactionUUID);
            }
            res.setViewData({
                inAuthDetails: inAuthDetails
            });
        }

        // Clear up Canada Tax if not Save the shipping address
        cartHelper.clearCanadaTax(currentBasket);
        delete session.custom.cardtype;

        // Setting up the Payment Type Session attribute so that promotions dependent on Regular Payment Methods (Customer Group RegularTenderUsers) always apply in the cart
        if (req.session.raw.custom.isHBCTenderType === null || req.session.raw.custom.isHBCTenderType === undefined) {
            // eslint-disable-next-line no-param-reassign
            req.session.raw.custom.isHBCTenderType = false;
        }

        req.session.privacyCache.set('EDDZipCode', null);
        if (tsysHelper.isTsysMode()) {
            hooksHelper('ucid.middleware.service', 'cleanFDD', [req]);
        }


        if (dw.web.Resource.msg('paypal.version.number', 'paypal_version', null) == '18.3.1') {
            var paypalHelper = require('*/cartridge/scripts/paypal/paypalHelper');
            var customerBillingAgreement;
            var isAddressExistForBillingAgreementCheckout;
            var prefs = paypalHelper.getPrefs();
            if (currentBasket && currentBasket.getCurrencyCode()) {
                customerBillingAgreement = paypalHelper.getCustomerBillingAgreement(currentBasket.getCurrencyCode());
                isAddressExistForBillingAgreementCheckout = !!customerBillingAgreement.getDefaultShippingAddress();
            }

            // In this case no need shipping address
            if (currentBasket && currentBasket.getDefaultShipment().productLineItems.length <= 0) {
                isAddressExistForBillingAgreementCheckout = true;
            }

            var buttonConfig;
            if (customerBillingAgreement && customerBillingAgreement.hasAnyBillingAgreement && prefs.PP_BillingAgreementState !== 'DoNotCreate') {
                buttonConfig = prefs.PP_Cart_Button_Config;
                buttonConfig.env = prefs.environmentType;
                buttonConfig.billingAgreementFlow = {
                    startBillingAgreementCheckoutUrl: URLUtils.https('Paypal-StartBillingAgreementCheckout').toString(),
                    isShippingAddressExist: isAddressExistForBillingAgreementCheckout
                };
            } else {
                buttonConfig = prefs.PP_Cart_Button_Config;
                buttonConfig.env = prefs.environmentType;
                buttonConfig.createPaymentUrl = URLUtils.https('Paypal-StartCheckoutFromCart', 'isAjax', 'true').toString();
            }

            buttonConfig.locale = req.locale.id;

            res.setViewData({
                paypal: {
                    prefs: prefs,
                    buttonConfig: buttonConfig
                }
            });
        }

        // apply SDD postal code to basket to calculate tax
        checkoutHelper.setSDDPostalToBasket(currentBasket, false);

        if (currentBasket) {
            Transaction.wrap(function () {
                if (currentBasket.currencyCode !== req.session.currency.currencyCode) {
                    currentBasket.updateCurrency();
                }
                cartHelper.ensureAllShipmentsHaveMethods(currentBasket);

                basketCalculationHelpers.calculateTotals(currentBasket);
            });
        }

        if (currentBasket && currentBasket.allLineItems.length) {
            reportingURLs = reportingUrlsHelper.getBasketOpenReportingURLs(currentBasket);
        }

        var saksplusHelper = require('*/cartridge/scripts/helpers/saksplusHelper');
        var hasSaksPlus = saksplusHelper.hasSaksPlusInBasket(currentBasket);

        /** script module */
        var bopisHelper = require('*/cartridge/scripts/helpers/bopisHelpers');
        var defaltStore = null;
        var viewData = res.getViewData();
        viewData.storeModalUrl = URLUtils.url('Stores-InitSearch', 'source', 'cart').toString();
        viewData.toggleShitoUrl = URLUtils.url('Cart-ToggleShippingOption').toString();
        if (preferences.isBopisEnabled) {
            defaltStore = bopisHelper.getDefaultStore();
            viewData.storeInfo = defaltStore;
        }

        var address = server.forms.getForm('address');
        address.clear();
        viewData.addressForm = address;
        res.setViewData(viewData);
        if (req.session.privacyCache.get('basketLimitReached')) {
        	basketLimitReached = true;
        	req.session.privacyCache.set('basketLimitReached', null);
        }

        if (preferences.CartBEPerformanceChangesEnable) {
            var shopRunnerEnabled = shopRunnerHelpers.checkSRExpressCheckoutEligibility(currentBasket);
            var isAllProductsSREligible = shopRunnerHelpers.checkSRCheckoutEligibilityForCart(currentBasket);
        } else {
            var shopRunnerEnabled = require('*/cartridge/scripts/helpers/shopRunnerHelpers').checkSRExpressCheckoutEligibility(currentBasket);
            var isAllProductsSREligible = require('*/cartridge/scripts/helpers/shopRunnerHelpers').checkSRCheckoutEligibilityForCart(currentBasket);
        }

        var gwp = {
            isMultipleChoiceExists: cartHelper.isMultiChoiceBonusProduct(currentBasket),
            buttonName: cartHelper.provideButtonName(currentBasket)
        };

        var mpPreferences = preferences.masterpass;
        var masterPassJSLibrary = mpPreferences.masterPassJSLibrary;
        var locale = req.locale.id;
        var checkoutID = mpPreferences.checkoutId[req.locale.id];
        var cardTypes = mpPreferences.allowedCardTypes;
        masterPassJSLibrary = masterPassJSLibrary.replace('XX_XX', locale);
        masterPassJSLibrary = masterPassJSLibrary.replace('<checkoutID>', checkoutID);
        masterPassJSLibrary = masterPassJSLibrary.replace('<accepted_card_brands>', cardTypes);
        var masterPassButtonImagePath = mpPreferences.masterPassButtonImagePath;
        masterPassButtonImagePath = masterPassButtonImagePath.replace('XX_XX',locale);
        masterPassButtonImagePath = masterPassButtonImagePath.replace('<checkoutID>', checkoutID);
        masterPassButtonImagePath = masterPassButtonImagePath.replace('<accepted_card_brands>', cardTypes);
        mpPreferences.masterPassJSLibrary = masterPassJSLibrary;
        mpPreferences.masterPassButtonImagePath = masterPassButtonImagePath;

        res.setViewData({
            isKlarnaEnabled: KlarnaUtils.isKlarnaEnabled(req),
            addressForm: preferences.CartBEPerformanceChangesEnable ? address : server.forms.getForm('address'),
            paypalCalculatedCost: currentBasket ? currentBasket.totalGrossPrice : 0,
            reportingURLs: reportingURLs,
            shopRunnerEnabled: shopRunnerEnabled,
            isAllProductsSREligible: isAllProductsSREligible,
            updatedLineItems: updatedLineItems,
            inventoryError: inventoryError,
            invErroMsg: inventoryError ? Resource.msg('error.cart.or.checkout.error', 'cart', null) : null,
            promoTrayEnabled: preferences.promoTrayEnabled,
            mpPreferences: mpPreferences,
            mpCartID: currentBasket ? currentBasket.getUUID() : -1,
            purchaseLimitedSKUMap: purchaseLimitedSKUMap,
            gwp: gwp,
            hasSaksPlus: hasSaksPlus,
            s7APIForPDPZoomViewer: preferences.s7APIForPDPZoomViewer,
            s7APIForPDPVideoPlayer: preferences.s7APIForPDPVideoPlayer,
            basketLimitReached: basketLimitReached,
            BasketLimitMsg: Resource.msg('commom.basketlimit.message.merge', 'common', null),
            saksFirstEnabled: preferences.saksFirstEnabled
        });
        // BFX Order Creation Number
        // Release Inventory if someone reserve it already and changing the country/loading the cart page again.
        if (currentBasket != null) {
            currentBasket.releaseInventory();
        }
        res.setViewData({
            BorderFreeEnabled: preferences.borderFreeEnabled
        });
        var bfxCountryCode = cookiesHelper.read('bfx.country');
        if (bfxCountryCode && bfxCountryCode !== 'US') {
            var bfxOrderNumber = req.session.privacyCache.get('bfxOrderNumber');
            if (!bfxOrderNumber) {
                bfxOrderNumber = OrderMgr.createOrderNo();
                req.session.privacyCache.set('bfxOrderNumber', bfxOrderNumber);
            }
            res.setViewData({
                bfxOrderNumber: bfxOrderNumber
            });
        } else {
            req.session.privacyCache.set('bfxOrderNumber', null);
        }

        var bfxcurrencyCode = cookiesHelper.read('bfx.currency');
        if (bfxcurrencyCode && bfxcurrencyCode !== 'USD') {
            res.setViewData({
                borderFreeClass: 'adobelaunch__borderfree_checkout'
            });

        }

        // Price Overrride/Price adjustment for OOBO
        var isAgentUser = false;
        if (req.session.raw.isUserAuthenticated()) {
            isAgentUser = true;
        }
        res.setViewData({
            isAgentUser: isAgentUser,
            priceOverrideForm: preferences.CartBEPerformanceChangesEnable || isAgentUser ? server.forms.getForm('priceOverride') : null,
            isCartPage: true,
            priceOverrideReasonCode: preferences.cscPriceoverrideReasonCode
        });

        // Setting the CSRF token details in session, so that it can be used in the Add Coupon method (cartPromoCode.isml)
        var tokenName = viewData.csrf.tokenName;
        var tokenValue = viewData.csrf.token;
        req.session.privacyCache.set('csrftokenName', tokenName);
        req.session.privacyCache.set('csrftokenValue', tokenValue);

        // Cart Tax Calculation
        var postalCode;
        if (currentBasket && currentBasket.defaultShipment && currentBasket.defaultShipment.shippingAddress && currentBasket.defaultShipment.shippingAddress.postalCode) {
            postalCode = currentBasket.defaultShipment.shippingAddress.postalCode;
        } else if (currentBasket && currentBasket.customer.authenticated &&
            currentBasket.customer.addressBook &&
            currentBasket.customer.addressBook.preferredAddress &&
            currentBasket.customer.addressBook.preferredAddress.postalCode) {
            postalCode = currentBasket.customer.addressBook.preferredAddress.postalCode;
        } else {
            postalCode = Resource.msg('label.enter.postal.code', 'cart', null);
        }
        res.setViewData({
            taxPostalCode: postalCode
        });

        var isCartMerged = false;
        if (req.session.privacyCache.get('basketMerged')) {
            isCartMerged = true;
            req.session.privacyCache.set('basketMerged', null);
        }
        res.setViewData({
            isCartMerged: isCartMerged
        });

        var basketModel = new CartModel(currentBasket, false);
        res.render('cart/cart', basketModel);

        // Handling Empty Cart
        if (!res.viewData.numItems) {
            var emptyCart = {
                Redirecturl: URLUtils.url('Home-Show')
            };
            res.setViewData({
                emptyCart: emptyCart
            });
        }

        var target = req.querystring.rurl || 1;
        var actionUrl = URLUtils.url('Account-Login', 'rurl', target);
        res.setViewData({
            includeRecaptchaJS: true,
            actionUrl: actionUrl
        });
        res.setViewData({
        	isEnabledforSameDayDelivery: preferences.isEnabledforSameDayDelivery,
        	setSDDPostalUrl: URLUtils.url('Stores-SetSDDZipCode').toString(),
        	sddStoreModalUrl: URLUtils.url('Stores-InitSDDSearch').toString(),
        	sddPostal: session.custom.sddpostal ? session.custom.sddpostal : request.geolocation.postalCode,
        	isBopisEnabled: preferences.isBopisEnabled,
        	setZipCartUrl: URLUtils.url('Cart-SetSDDDetailsCart').toString()
         });
        if (session.privacy.privateSalePromoID) {
            // setting a different session variable to not apply pricebooks to the session on the common model
            delete session.privacy.skipPricebookCal;
        }
        next();
    }
);

server.replace('UpdateQuantity', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Transaction = require('dw/system/Transaction');
    var URLUtils = require('dw/web/URLUtils');
    var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');
    var productModel = null, store = null;
    var ssdQtyUpdated = false;
    var isSDDEligible = false;
    var CartModel = preferences.CartBEPerformanceChangesEnable ? CartModelGlobal : require('*/cartridge/models/cart');

    var currentBasket = BasketMgr.getCurrentBasket();

     /**
     * On a private sale, the promo price and the price book are same
     * Platform randomly sometimes considers the private-sale pricebook price and other time as a promotion discount
     * Since the promotionalDiscount was also a Price from pricebook, it is benefial to force promotional price and get the PIP to display
     */
    if (session.privacy.privateSalePromoID) {
        // setting a different session variable to not apply pricebooks to the session on the common model
        session.privacy.skipPricebookCal = true;
    }

    if (!currentBasket) {
        res.setStatusCode(500);
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });
        return next();
    }

    var productId = req.querystring.pid;
    var updateQuantity = parseInt(req.querystring.quantity, 10);
    var uuid = req.querystring.uuid;
    var selectedShipto = req.querystring.shipto;
    var productLineItems = currentBasket.productLineItems;
    var matchingLineItem = collections.find(productLineItems, function (item) {
        return (item.productID === productId) && (item.UUID === uuid);
    });
    var CSRAdjustmentAmount = 0;
    var availableToSell = 0;

    var totalQtyRequested = 0;
    var qtyAlreadyInCart = 0;
    var minOrderQuantity = 0;
    var previousQuantity = 0;
    var perpetual = false;
    var canBeUpdated = false;
    var bundleItems;
    var bonusDiscountLineItemCount = currentBasket.bonusDiscountLineItems.length;
    var errorMsg = Resource.msg('error.cannot.update.product.quantity', 'cart', null);
    var availableATS = 1;

    if (matchingLineItem) {
        if (matchingLineItem.product.bundle) {
            bundleItems = matchingLineItem.bundledProductLineItems;
            canBeUpdated = collections.every(bundleItems, function (item) {
                var quantityToUpdate = updateQuantity *
                    matchingLineItem.product.getBundledProductQuantity(item.product).value;
                qtyAlreadyInCart = cartHelper.getQtyAlreadyInCart(
                    item.productID,
                    productLineItems,
                    item.UUID
                );
                totalQtyRequested = quantityToUpdate + qtyAlreadyInCart;
                availableToSell = item.product.availabilityModel.inventoryRecord.ATS.value;
                perpetual = item.product.availabilityModel.inventoryRecord.perpetual;
                minOrderQuantity = item.product.minOrderQuantity.value;
                var isInStock = (totalQtyRequested <= availableToSell || perpetual) && (quantityToUpdate >= minOrderQuantity);
                var isInPurchaseLimit = productHelper.isInPurchaselimit(item.product.custom.purchaseLimit, updateQuantity);
                if (!isInPurchaseLimit) {
                    errorMsg = Resource.msgf('label.notinpurchaselimit', 'common', null, item.product.custom.purchaseLimit);
                    availableATS = item.product.custom.purchaseLimit;
                } else if (!isInStock) {
                    errorMsg = Resource.msg('label.not.available.items', 'common', null);
                    availableATS = availableToSell;
                }
                canBeUpdated = (isInPurchaseLimit && isInStock);
            });
        } else {
            availableToSell = matchingLineItem.product.availabilityModel.inventoryRecord.ATS.value;
            perpetual = matchingLineItem.product.availabilityModel.inventoryRecord.perpetual;
            qtyAlreadyInCart = cartHelper.getQtyAlreadyInCart(
                productId,
                productLineItems,
                matchingLineItem.UUID
            );
            totalQtyRequested = updateQuantity + qtyAlreadyInCart;
            minOrderQuantity = matchingLineItem.product.minOrderQuantity.value;

            var isInStock = (totalQtyRequested <= availableToSell || perpetual) && (updateQuantity >= minOrderQuantity);
            var isInPurchaseLimit = productHelper.isInPurchaselimit(matchingLineItem.product.custom.purchaseLimit, updateQuantity);
            if (!isInPurchaseLimit && matchingLineItem.product.custom.purchaseLimit <=availableToSell) {
                errorMsg = Resource.msgf('label.notinpurchaselimit', 'common', null, (matchingLineItem.product.custom.purchaseLimit));
                availableATS = matchingLineItem.product.custom.purchaseLimit;
            } else if (!isInStock) {
                errorMsg = Resource.msg('label.not.available.items', 'common', null);
                availableATS = availableToSell;
            }
            canBeUpdated = (isInPurchaseLimit && isInStock);
        }
    }

    // changes for SFDEV-6215
    if (canBeUpdated) {
        if (session.custom.omsInventory && session.custom.omsInventory.length > 0) {
            var omsInventoryArray = JSON.parse(session.custom.omsInventory);
            if (omsInventoryArray && omsInventoryArray.length > 0) {
                for (var i = 0; i < omsInventoryArray.length; i++) {
                    if (omsInventoryArray[i].itemID === matchingLineItem.productID) {
                        if (omsInventoryArray[i].quantity < updateQuantity && omsInventoryArray[i].quantity > 0) {
                            updateQuantity = omsInventoryArray[i].quantity;
                        }
                        break;
                    }
                }
            }
        }

        // set available quantity for SDD
        if (selectedShipto && selectedShipto !== 'null' && selectedShipto !== 'undefined' && selectedShipto === 'shipto') {
        	productModel = ProductFactory.getSDD({apiProduct: matchingLineItem.product});
        	productModel.desiredQuantity = updateQuantity;
        	isSDDEligible = productModel.isAvailableForSDD && preferences.isEnabledforSameDayDelivery;
        	if (isSDDEligible) {
            	store = storeHelpers.getDefaultSDDStore(productModel);
            	if (store && store.unitsAvailable && store.unitsAvailable < updateQuantity) {
            		updateQuantity = store.unitsAvailable;
            		ssdQtyUpdated = true;
            	}
        	}
        }

        previousQuantity = (matchingLineItem && matchingLineItem.quantity.available) ? matchingLineItem.quantity.value : 0;
        Transaction.wrap(function () {
            matchingLineItem.setQuantityValue(updateQuantity);

            var previousBounsDiscountLineItems = collections.map(currentBasket.bonusDiscountLineItems, function (bonusDiscountLineItem) {
                return bonusDiscountLineItem.UUID;
            });

            basketCalculationHelpers.calculateTotals(currentBasket);
            if (currentBasket.bonusDiscountLineItems.length > bonusDiscountLineItemCount) {
                var prevItems = JSON.stringify(previousBounsDiscountLineItems);

                collections.forEach(currentBasket.bonusDiscountLineItems, function (bonusDiscountLineItem) {
                    if (prevItems.indexOf(bonusDiscountLineItem.UUID) < 0) {
                        bonusDiscountLineItem.custom.bonusProductLineItemUUID = matchingLineItem.UUID; // eslint-disable-line no-param-reassign
                        matchingLineItem.custom.bonusProductLineItemUUID = 'bonus';
                        matchingLineItem.custom.preOrderUUID = matchingLineItem.UUID;
                    }
                });
            }
        });

        if (matchingLineItem && 'priceOverrided' in matchingLineItem.custom && matchingLineItem.custom.priceOverrided) {
            var CSRAdjustments = matchingLineItem.priceAdjustments.iterator();
            var UUIDUtils = require('dw/util/UUIDUtils');
            var AmountDiscount = require('dw/campaign/AmountDiscount');
            Transaction.wrap(function () {
                while (CSRAdjustments.hasNext()) {
                    var existingAdjustment = CSRAdjustments.next();
                    if ('isCSRAdjustment' in existingAdjustment.custom && existingAdjustment.custom.isCSRAdjustment) {
                        CSRAdjustmentAmount = previousQuantity ? Math.abs(existingAdjustment.priceValue / previousQuantity) : 0;
                        matchingLineItem.removePriceAdjustment(existingAdjustment);
                        var newCSRAdjustment = matchingLineItem.createPriceAdjustment(UUIDUtils.createUUID(), new AmountDiscount(CSRAdjustmentAmount * matchingLineItem.quantityValue));
                        newCSRAdjustment.setManual(true);
                        newCSRAdjustment.custom.isCSRAdjustment = true;
                    }
                }
                basketCalculationHelpers.calculateTotals(currentBasket);
            });
        }
    }

    if (matchingLineItem && canBeUpdated) {
        var recentlyUpdatedProduct = {
            product: { id: req.querystring.pid }
        };
        res.setViewData({
            previousQuantity: previousQuantity
        });
        res.setViewData(recentlyUpdatedProduct);
        var basketModel = new CartModel(currentBasket);
        // change for SFDEV-6215
        basketModel.valid.error = false;
        basketModel.ssdQtyUpdated = ssdQtyUpdated;
        basketModel.isSDDEligible = isSDDEligible;
        basketModel.ssdQtyErrorMsg = Resource.msg('error.cart.or.checkout.inventory.error', 'cart', null);
        res.json(basketModel);
    } else {
        res.json({
            error: true,
            errorMessage: errorMsg,
            isInStock: isInStock,
            availableATS: availableATS,
            isInPurchaseLimit: isInPurchaseLimit
        });
    }
    if (session.privacy.privateSalePromoID) {
        // setting a different session variable to not apply pricebooks to the session on the common model
        delete session.privacy.skipPricebookCal;
    }
    return next();
});

server.append('AddProduct', function (req, res, next) {
	var BasketMgr = require('dw/order/BasketMgr');
    var loadBasketModel = req.form.loadBasketModel;
    var viewData = res.viewData;
    var currentBasket = BasketMgr.getCurrentOrNewBasket();
    var saksplusHelper = require('*/cartridge/scripts/helpers/saksplusHelper');
    var Transaction = require('dw/system/Transaction');
    var pliUUID = viewData.pliUUID;
    if (currentBasket) {
    	var allLineItems = currentBasket.allProductLineItems;
        var collections = require('*/cartridge/scripts/util/collections');
        collections.forEach(allLineItems, function (pli) {
            if (pli.UUID === pliUUID) {
                // add item's pid that adds the bonus pli to basket to show message on cart when bonus is removed
                try {
                    if (pli.relatedBonusProductLineItems && pli.relatedBonusProductLineItems.length > 0) {
                    	if (session.custom.relatedBonusPlis) {
                    		let relatedBonusItems = session.custom.relatedBonusPlis;
                    		let itemArray = new Array(relatedBonusItems);
                    		itemArray.push(pli.productID);
                    		session.custom.relatedBonusPlis = itemArray.toString();
                    	} else {
                    		let itemArray = new Array();
                    		itemArray.push(pli.productID);
                    		session.custom.relatedBonusPlis = itemArray.toString();
                    	}
                    }

                } catch (e) {
                	Logger.error('Error adding BONUS PLI to session' + e.message);
                }
            }
        });
    	saksplusHelper.hasSaksPlusInBasket(currentBasket);
    }

    if (!res.viewData.error && loadBasketModel && currentBasket) {
        var CartModel = preferences.CartBEPerformanceChangesEnable ? CartModelGlobal : require('*/cartridge/models/cart');
        var cartModel = new CartModel(currentBasket);
        res.setViewData(cartModel);
    }
    next();
});


server.replace('RemoveProductLineItem', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Transaction = require('dw/system/Transaction');
    var URLUtils = require('dw/web/URLUtils');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var saksplusHelper = require('*/cartridge/scripts/helpers/saksplusHelper');
    var CartModel = preferences.CartBEPerformanceChangesEnable ? CartModelGlobal : require('*/cartridge/models/cart');

    var currentBasket = BasketMgr.getCurrentBasket();

     /**
     * On a private sale, the promo price and the price book are same
     * Platform randomly sometimes considers the private-sale pricebook price and other time as a promotion discount
     * Since the promotionalDiscount was also a Price from pricebook, it is benefial to force promotional price and get the PIP to display
     */
    if (session.privacy.privateSalePromoID) {
        // setting a different session variable to not apply pricebooks to the session on the common model
        session.privacy.skipPricebookCal = true;
    }

    if (!currentBasket) {
        res.setStatusCode(500);
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });

        return next();
    }

    var isProductLineItemFound = false;
    var bonusProductsUUIDs = [];

    Transaction.wrap(function () {
        if (req.querystring.pid && req.querystring.uuid) {
            var ProductFactory = require('*/cartridge/scripts/factories/product');
            var productLineItems = currentBasket.getAllProductLineItems(req.querystring.pid);
            var bonusProductLineItems;
            if (!preferences.CartBEPerformanceChangesEnable){
                bonusProductLineItems = currentBasket.bonusLineItems;
            }
            var deletedBonusProductsIDs = [];
            var mainProdItem;
            for (var i = 0; i < productLineItems.length; i++) {
                var item = productLineItems[i];
                if ((item.UUID === req.querystring.uuid)) {
                    if (preferences.CartBEPerformanceChangesEnable){
                        bonusProductLineItems = item.relatedBonusProductLineItems;
                    }
                    if (bonusProductLineItems && bonusProductLineItems.length > 0) {
                        for (var j = 0; j < bonusProductLineItems.length; j++) {
                            var bonusItem = bonusProductLineItems[j];
                            if (preferences.CartBEPerformanceChangesEnable){
                                mainProdItem = item;
                                bonusProductsUUIDs.push(bonusItem.UUID);
                                deletedBonusProductsIDs.push(ProductFactory.get({
                                    pid: bonusItem.productID
                                }));
                            }
                            else {
                                mainProdItem = bonusItem.getQualifyingProductLineItemForBonusProduct();
                                if (mainProdItem !== null &&
                                    (mainProdItem.productID === item.productID)) {
                                    bonusProductsUUIDs.push(bonusItem.UUID);
                                    deletedBonusProductsIDs.push(ProductFactory.get({
                                        pid: bonusItem.productID
                                    }));
                                }
                            }
                        }
                    }

                    var shipmentToRemove = item.shipment;
                    if (item.bonusProductLineItem) {
                        // saving attribute in session, since this attribute should be independent of login and cart merge we are saving in custom against privacy
                        session.custom.isBonusProRemoved = true;
                        // Saving bonus line item
                        var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
                        cartHelper.saveBonusLineItemInfo(currentBasket, item);
                    }
                    var product = {
                        pid: req.querystring.pid
                    };
                    var deletedItem = ProductFactory.getSimpleLineItem({ item: item, id: req.querystring.pid });

                    var previousQuantity = item.quantity.value;
                    if (preferences.CartBEPerformanceChangesEnable) {
                        var mainProdItemProductID = {};
                        if (mainProdItem){
                            mainProdItemProductID.productID = mainProdItem.productID;
                        }
                        res.setViewData({
                            deletedItem: deletedItem,
                            deletedBonusProductsIDs: deletedBonusProductsIDs,
                            previousQuantity: previousQuantity,
                            mainProdItem: mainProdItemProductID
                        });
                    } else {
                        res.setViewData({
                            deletedItem: deletedItem,
                            deletedBonusProductsIDs: deletedBonusProductsIDs,
                            previousQuantity: previousQuantity,
                            mainProdItem: mainProdItem
                        });
                    }

                    currentBasket.removeProductLineItem(item);
                    var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
					          var pliProduct = item.product;
                    cartHelper.removeCartItemFromProductList(item.product);

                    if (shipmentToRemove.productLineItems.empty && !shipmentToRemove.default) {
                        currentBasket.removeShipment(shipmentToRemove);
                    }
                    isProductLineItemFound = true;
                    // changes for SFDEV-6215 -update the OMS inventory in session if the product line item from the list is removed.
                    if (session.custom.omsInventory && session.custom.omsInventory.length > 0) {
                        var itemRemoved = false;
                        var omsInventoryArray = JSON.parse(session.custom.omsInventory);
                        var inventoryList = new dw.util.ArrayList(omsInventoryArray);
                        if (inventoryList && inventoryList.length > 0) {
                            for (var i = 0; i < inventoryList.length; i++) {
                                if (inventoryList.get(i).itemID === item.productID) {
                                    inventoryList.remove(inventoryList.get(i));
                                    itemRemoved = true;
                                    break;
                                }
                            }
                        }
                        // update OMS inventory list in session
                        if (itemRemoved) {
                            var currentInventory = inventoryList.toArray();
                            session.custom.omsInventory = JSON.stringify(currentInventory);
                        }
                    }
                 	try {
                		var dropShipItemDetails = {};
                    	if (pliProduct.custom.hasOwnProperty('pdRestrictedShipTypeText') && pliProduct.custom.pdRestrictedShipTypeText) {
                    		let productDropShipAttrValue = pliProduct.custom.pdRestrictedShipTypeText;
                    		if (currentBasket.custom.hasOwnProperty('dropShipItemsDetails') && !empty(currentBasket.custom.dropShipItemsDetails)) {
                    			dropShipItemDetails = currentBasket.custom.dropShipItemsDetails;
                    			dropShipItemDetails = JSON.parse(dropShipItemDetails);
                    			if (dropShipItemDetails[productDropShipAttrValue]) {
                    				let value = dropShipItemDetails[productDropShipAttrValue];
                    				if (isNaN(value)) {
                    					value = Number(value);
                    				}
                    				if (value === 1) {
                    					delete dropShipItemDetails[productDropShipAttrValue];
                    				} else if (value > 1) {
                    					value = value - 1;
                    					dropShipItemDetails[productDropShipAttrValue] = value;
                    				}
                    				Transaction.wrap(function () {
                    					currentBasket.custom.dropShipItemsDetails = JSON.stringify(dropShipItemDetails);
                    				});
                    			}
                    		}
                    	}
                	} catch (e) {
                		Logger.error('Error adding drop ship details to basket' + e.message);
                	}
                    break;
                }
            }
            try {
                if (mainProdItem && mainProdItem.productID) {
                	var mainProdID = mainProdItem.productID;
                	if (session.custom.relatedBonusPlis && session.custom.relatedBonusPlis.length > 0 && session.custom.relatedBonusPlis.indexOf(mainProdID) > -1) {
                		let relatedBonusItems = session.custom.relatedBonusPlis;
                		let itemArray = new Array(relatedBonusItems);
                		let itemIndex = itemArray.indexOf(mainProdID);
                		itemArray.splice(itemIndex, 1);
                		session.custom.relatedBonusPlis = itemArray.toString();
                	}
                }
                res.setViewData({
                    releatedBonusPLIs: session.custom.relatedBonusPlis
                });
            } catch (e) {
            	Logger.error('Error adding BONUS PLI to session' + e.message);
            }
        }
        if (isProductLineItemFound && currentBasket.getAllProductQuantities().size() === 0) {
            currentBasket.removeAllPaymentInstruments();
        }
        saksplusHelper.hasSaksPlusInBasket(currentBasket);
        basketCalculationHelpers.calculateTotals(currentBasket);
    });

    if (isProductLineItemFound) {
        var basketModel = new CartModel(currentBasket);

        // change for SFDEV-5099
        var updatedLineItems = 'updatedLineItems' in req.querystring ? req.querystring.updatedLineItems : null;
        if (updatedLineItems) {
            var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
            basketModel.updatedLineItems = updatedLineItems;
            basketModel.itemsHTML = renderTemplateHelper.getRenderedHtml(basketModel, 'cart/cartItems');
        }
        // change for SFDEV-6215
        basketModel.valid.error = false;
        var basketModelPlus = {
            basket: basketModel,
            toBeDeletedUUIDs: bonusProductsUUIDs
        };
        res.json(basketModelPlus);
    } else {
        res.setStatusCode(500);
        res.json({
            errorMessage: Resource.msg('error.cannot.remove.product', 'cart', null)
        });
    }
    if (session.privacy.privateSalePromoID) {
        // setting a different session variable to not apply pricebooks to the session on the common model
        delete session.privacy.skipPricebookCal;
    }
    return next();
});

server.replace('AddBonusProducts', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var ProductMgr = require('dw/catalog/ProductMgr');
    var Transaction = require('dw/system/Transaction');
    var currentBasket = BasketMgr.getCurrentOrNewBasket();
    var data = JSON.parse(req.form.pids);
    var pliUUID = req.form.pliuuid;
    var newBonusDiscountLineItems = currentBasket.getBonusDiscountLineItems();
    var qtyAllowed = data.totalQty;
    var totalQty = 0;
    var CartModel = preferences.CartBEPerformanceChangesEnable ? CartModelGlobal : require('*/cartridge/models/cart');

    if (session.privacy.privateSalePromoID) {
        // setting a different session variable to not apply pricebooks to the session on the common model
        session.privacy.skipPricebookCal = true;
    }

    for (var i = 0; i < data.bonusProducts.length; i++) {
        totalQty += data.bonusProducts[i].qty;
    }

    if (totalQty === 0) {
        res.json({
            errorMessage: Resource.msg(
                'error.alert.choiceofbonus.no.product.selected',
                'product',
                null),
            error: true,
            success: false
        });
    } else if (totalQty > qtyAllowed) {
        res.json({
            errorMessage: Resource.msgf(
                'error.alert.choiceofbonus.max.quantity',
                'product',
                null,
                qtyAllowed,
                totalQty),
            error: true,
            success: false
        });
    } else {
        var bonusDiscountLineItem = collections.find(newBonusDiscountLineItems, function (item) {
            return item.UUID === req.form.uuid;
        });
        var bonuslineitems = [];
        if (currentBasket) {
            Transaction.wrap(function () {
                collections.forEach(bonusDiscountLineItem.getBonusProductLineItems(), function (dli) {
                    if (dli.product) {
                        currentBasket.removeProductLineItem(dli);
                    }
                });


                data.bonusProducts.forEach(function (bonusProduct) {
                	var bonusObj={}
                    var product = ProductMgr.getProduct(bonusProduct.pid);
                    var selectedOptions = bonusProduct.options;
                    var optionModel = productHelper.getCurrentOptionModel(
                        product.optionModel,
                        selectedOptions
                    );
                    var pli = currentBasket.createBonusProductLineItem(
                        bonusDiscountLineItem,
                        product,
                        optionModel,
                        null
                    );
                    pli.setQuantityValue(bonusProduct.qty);
                    pli.custom.bonusProductLineItemUUID = pliUUID;
                    if(!empty(pli.product)){
                    	 bonusObj.brand = pli.product.brand;
                         bonusObj.bopus_store_id = 'fromStoreId' in pli.custom && pli.custom.fromStoreId ? pli.custom.fromStoreId : '';
                         bonusObj.code = pli.product.masterProduct.ID;
                         bonusObj.gwp_flag = 'true';
                         bonusObj.name = pli.product.name;
                         bonusObj.original_price = '0';
                         bonusObj.price = '0';
                         bonusObj.quantity = pli.quantityValue.toString();
                         bonusObj.selected_sku = pli.product.ID;
                         bonusObj.ship_from_store_id = '';
                    }
                    bonuslineitems.push(bonusObj);
                });
                collections.forEach(currentBasket.getAllProductLineItems(), function (productLineItem) {
                    if (productLineItem.UUID === pliUUID) {
                        productLineItem.custom.bonusProductLineItemUUID = 'bonus';// eslint-disable-line no-param-reassign
                        productLineItem.custom.preOrderUUID = productLineItem.UUID;// eslint-disable-line no-param-reassign
                    }
                });
            });
        }
        res.json({
            totalQty: currentBasket.productQuantityTotal,
            selectedbonusitems : bonuslineitems,
            msgSuccess: Resource.msg('text.alert.choiceofbonus.addedtobasket', 'product', null),
            success: true,
            error: false
        });
    }
    if (session.privacy.privateSalePromoID) {
        // setting a different session variable to not apply pricebooks to the session on the common model
        delete session.privacy.skipPricebookCal;
    }
    next();
});



server.replace('EditProductLineItem', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var ProductMgr = require('dw/catalog/ProductMgr');
    var URLUtils = require('dw/web/URLUtils');
    var Transaction = require('dw/system/Transaction');
    var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var productFactory = require('*/cartridge/scripts/factories/product');
    var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');
    var isSDDEligible = false;
    var CartModel = preferences.CartBEPerformanceChangesEnable ? CartModelGlobal : require('*/cartridge/models/cart');

    var currentBasket = BasketMgr.getCurrentBasket();

     /**
     * On a private sale, the promo price and the price book are same
     * Platform randomly sometimes considers the private-sale pricebook price and other time as a promotion discount
     * Since the promotionalDiscount was also a Price from pricebook, it is benefial to force promotional price and get the PIP to display
     */
    if (session.privacy.privateSalePromoID) {
        // setting a different session variable to not apply pricebooks to the session on the common model
        session.privacy.skipPricebookCal = true;
    }

    if (!currentBasket) {
        res.setStatusCode(500);
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });
        return next();
    }

    var uuid = req.form.uuid;
    var productId = req.form.pid;
    var selectedOptionValueId = req.form.selectedOptionValueId;
    var updateQuantity = parseInt(req.form.quantity, 10);
    var selectedShipto = req.form.selectedShipto;
    var productModel = null, store = null;
    var ssdQtyUpdated = false;

    var productLineItems = currentBasket.allProductLineItems;
    var requestLineItem = collections.find(productLineItems, function (item) {
        return item.UUID === uuid;
    });

    var uuidToBeDeleted = null;
    var pliToBeDeleted;
    var newPidAlreadyExist = collections.find(productLineItems, function (item) {
        if (item.productID === productId && item.UUID !== uuid) {
            uuidToBeDeleted = item.UUID;
            pliToBeDeleted = item;
            updateQuantity += parseInt(item.quantity, 10);
            return true;
        }
        return false;
    });

    var availableToSell = 0;
    var totalQtyRequested = 0;
    var qtyAlreadyInCart = 0;
    var minOrderQuantity = 0;
    var canBeUpdated = false;
    var perpetual = false;
    var bundleItems;

    if (requestLineItem) {
        if (requestLineItem.product.bundle) {
            bundleItems = requestLineItem.bundledProductLineItems;
            canBeUpdated = collections.every(bundleItems, function (item) {
                var quantityToUpdate = updateQuantity *
                    requestLineItem.product.getBundledProductQuantity(item.product).value;
                qtyAlreadyInCart = cartHelper.getQtyAlreadyInCart(
                    item.productID,
                    productLineItems,
                    item.UUID
                );
                totalQtyRequested = quantityToUpdate + qtyAlreadyInCart;
                availableToSell = item.product.availabilityModel.inventoryRecord.ATS.value;
                perpetual = item.product.availabilityModel.inventoryRecord.perpetual;
                minOrderQuantity = item.product.minOrderQuantity.value;
                return (totalQtyRequested <= availableToSell || perpetual) &&
                    (quantityToUpdate >= minOrderQuantity) && productHelper.isInPurchaselimit(item.product.custom.purchaseLimit, updateQuantity);
            });
        } else {
            availableToSell = requestLineItem.product.availabilityModel.inventoryRecord.ATS.value;
            perpetual = requestLineItem.product.availabilityModel.inventoryRecord.perpetual;
            qtyAlreadyInCart = cartHelper.getQtyAlreadyInCart(
                productId,
                productLineItems,
                requestLineItem.UUID
            );
            totalQtyRequested = updateQuantity + qtyAlreadyInCart;
            minOrderQuantity = requestLineItem.product.minOrderQuantity.value;
            canBeUpdated = (totalQtyRequested <= availableToSell || perpetual) &&
                (updateQuantity >= minOrderQuantity) && productHelper.isInPurchaselimit(requestLineItem.product.custom.purchaseLimit, updateQuantity);
            // This scenario will come when customer tries to update which is in stock but not the current product.
            if (availableToSell === 0 && !newPidAlreadyExist) {
                canBeUpdated = true;
            }
        }
    }
    var omsSoldOut = false;
    // changes for SFDEV-6215
    if (canBeUpdated) {
        if (session.custom.omsInventory && session.custom.omsInventory.length > 0) {
            var omsInventoryArray = JSON.parse(session.custom.omsInventory);
            if (omsInventoryArray && omsInventoryArray.length > 0) {
                for (var i = 0; i < omsInventoryArray.length; i++) {
                    if (omsInventoryArray[i].itemID === productId) {
                        if (omsInventoryArray[i].quantity <= 0) {
                            omsSoldOut = true;
                            break;
                        }
                    }
                }
            }
        }
    }

    // set available quantity for SDD
    if (canBeUpdated) {
        if (selectedShipto && selectedShipto !== 'null' && selectedShipto !== 'undefined' && selectedShipto === 'shipto') {
        	productModel = productFactory.get({pid:requestLineItem.productID});
        	isSDDEligible = productModel.isAvailableForSDD && preferences.isEnabledforSameDayDelivery;
        	productModel.desiredQuantity = updateQuantity;
        	if (isSDDEligible) {
            	store = storeHelpers.getDefaultSDDStore(productModel);
            	if (store && store.unitsAvailable && store.unitsAvailable < updateQuantity) {
            		updateQuantity = store.unitsAvailable;
            		ssdQtyUpdated = true;
            	}
        	}
        }
    }
    var error = false;
    if (canBeUpdated) {
        var product = ProductMgr.getProduct(productId);
        try {
            Transaction.wrap(function () {
                if (newPidAlreadyExist) {
                    var shipmentToRemove = pliToBeDeleted.shipment;
                    currentBasket.removeProductLineItem(pliToBeDeleted);
                    if (shipmentToRemove.productLineItems.empty && !shipmentToRemove.default) {
                        currentBasket.removeShipment(shipmentToRemove);
                    }
                }

                if (!requestLineItem.product.bundle) {
                    requestLineItem.replaceProduct(product);
                }

                // If the product has options
                var optionModel = product.getOptionModel();
                if (optionModel && optionModel.options && optionModel.options.length) {
                    var productOption = optionModel.options.iterator().next();
                    var productOptionValue = optionModel.getOptionValue(productOption, selectedOptionValueId);
                    var optionProductLineItems = requestLineItem.getOptionProductLineItems();
                    var optionProductLineItem = optionProductLineItems.iterator().next();
                    optionProductLineItem.updateOptionValue(productOptionValue);
                }

                requestLineItem.setQuantityValue(updateQuantity);
                basketCalculationHelpers.calculateTotals(currentBasket);
            });
        } catch (e) {
            error = true;
        }
    }
    // changes for SFDEV-6215
    if (omsSoldOut) {
        res.setStatusCode(500);
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });

        return next();
    }

    if (!error && requestLineItem && canBeUpdated) {
        var cartModel = new CartModel(currentBasket);
        // change for SFDEV-6215
        if (cartModel.valid) {
            cartModel.valid.error = false;
        }
        cartModel.ssdQtyUpdated = ssdQtyUpdated;
        cartModel.isSDDEligible = isSDDEligible;
        cartModel.ssdQtyErrorMsg = Resource.msg('error.cart.or.checkout.inventory.error', 'cart', null);
        var responseObject = {
            cartModel: cartModel,
            newProductId: productId
        };

        if (uuidToBeDeleted) {
            responseObject.uuidToBeDeleted = uuidToBeDeleted;
        }

        var priceHelper = require('*/cartridge/scripts/helpers/pricing');
        var ProductFactory = require('*/cartridge/scripts/factories/product');
        var product = ProductFactory.get({
            pid: requestLineItem.product.ID
        });
        var context = {
            price: product.price,
            product: product
        };
        var priceHTML = priceHelper.renderHtml(priceHelper.getHtmlContext(context));
        res.setViewData({
            priceHTML: priceHTML
        });
        res.json(responseObject);
    } else {
        res.setStatusCode(500);
        res.json({
            errorMessage: Resource.msg('error.cannot.update.product', 'cart', null)
        });
    }

    if (session.privacy.privateSalePromoID) {
        // setting a different session variable to not apply pricebooks to the session on the common model
        delete session.privacy.skipPricebookCal;
    }
    return next();
});


server.get('SetSDDDetailsCart', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var checkoutHelper = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var Transaction = require('dw/system/Transaction');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var currentBasket = BasketMgr.getCurrentBasket();
    var CartModel = preferences.CartBEPerformanceChangesEnable ? CartModelGlobal : require('*/cartridge/models/cart');

    // calculate tax to applied postal
    checkoutHelper.setSDDPostalToBasket(currentBasket, true);
    Transaction.wrap(function () {
        basketCalculationHelpers.calculateTotals(currentBasket);
    });
    var cartModel = new CartModel(currentBasket);
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
    cartModel.itemsHTML = renderTemplateHelper.getRenderedHtml(cartModel, 'cart/cartItems');
    res.json({
    	cartModel: cartModel,
    	ssdPostal: session.custom.sddpostal,
    	ssdstoreid : session.custom.sddstoreid
    });
    next();
});


server.replace(
	    'AddCoupon',
	    csrfProtection.validateAjaxRequest,
	    server.middleware.https,
	    csrfProtection.validateAjaxRequest,
	    function (req, res, next) {
	        var BasketMgr = require('dw/order/BasketMgr');
	        var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
	        var Transaction = require('dw/system/Transaction');
	        var URLUtils = require('dw/web/URLUtils');
	        var OrderModel = require('*/cartridge/models/order');
	        var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
	        var ProductMgr = require('dw/catalog/ProductMgr');
            var PromotionHelper = require('*/cartridge/scripts/util/promotionHelper');
            var CartModel = preferences.CartBEPerformanceChangesEnable ? CartModelGlobal : require('*/cartridge/models/cart');

            var currentBasket = BasketMgr.getCurrentBasket();

             /**
             * On a private sale, the promo price and the price book are same
             * Platform randomly sometimes considers the private-sale pricebook price and other time as a promotion discount
             * Since the promotionalDiscount was also a Price from pricebook, it is benefial to force promotional price and get the PIP to display
             */
            if (session.privacy.privateSalePromoID) {
                // setting a different session variable to not apply pricebooks to the session on the common model
                session.privacy.skipPricebookCal = true;
            }

	        if (!currentBasket) {
	            res.setStatusCode(500);
	            res.json({
	                error: true,
	                redirectUrl: URLUtils.url('Cart-Show').toString()
	            });

	            return next();
	        }

	        if (!currentBasket) {
	            res.setStatusCode(500);
	            res.json({
	                errorMessage: Resource.msg('error.add.coupon', 'cart', null)
	            });
	            return next();
	        }

	        if (preferences.numberOfCouponsAllowed && currentBasket.couponLineItems && currentBasket.couponLineItems.length === preferences.numberOfCouponsAllowed) {
	            res.json({
	                error: true,
	                errorMessage: Resource.msg('error.couponlimit.reached', 'cart', null)
	            });
	            return next();
	        }
	        // Setting up the Payment Type Session attribute so that promotions dependent on Regular Payment Methods (Customer Group RegularTenderUsers) always apply in the cart
	        if (req.session.raw.custom.isHBCTenderType === null || req.session.raw.custom.isHBCTenderType === undefined) {
	            // eslint-disable-next-line no-param-reassign
	            req.session.raw.custom.isHBCTenderType = false;
	        }

	        var error = false;
	        var errorMessage;
	        var cpnCode = req.querystring.couponCode.trim().toUpperCase();

	        try {
	            Transaction.wrap(function () {
	                return currentBasket.createCouponLineItem(cpnCode, true);
	            });
	        } catch (e) {
	            error = true;
	            var errorCodes = {
	                COUPON_CODE_ALREADY_IN_BASKET: 'error.coupon.already.in.cart',
	                COUPON_ALREADY_IN_BASKET: 'error.coupon.cannot.be.combined',
	                COUPON_CODE_ALREADY_REDEEMED: 'error.coupon.already.redeemed',
	                COUPON_CODE_UNKNOWN: 'error.unable.to.add.coupon',
	                COUPON_DISABLED: 'error.unable.to.add.coupon',
	                REDEMPTION_LIMIT_EXCEEDED: 'error.limit.exceeded',
	                TIMEFRAME_REDEMPTION_LIMIT_EXCEEDED: 'error.limit.exceeded',
	                NO_ACTIVE_PROMOTION: 'error.unable.to.add.coupon',
	                default: 'error.unable.to.add.coupon'
	            };

	            var errorMessageKey = errorCodes[e.errorCode] || errorCodes.default;
	            errorMessage = Resource.msg(errorMessageKey, 'cart', null);
	        }

	        if (error) {
	            res.json({
	                error: error,
	                errorMessage: errorMessage
	            });
	            return next();
	        }

	        Transaction.wrap(function () {
	            basketCalculationHelpers.calculateTotals(currentBasket);
	        });

	        try {
	        	var appliedpromo;
		        var iseligibledbonusitem = false;
		        var eligibledbonusitem;

		        for (var couponLineItem in currentBasket.couponLineItems) {
		        	if(cpnCode == couponLineItem.couponCode.toString()){
		        		appliedpromo = PromotionHelper.getCouponLineItemPromotion(couponLineItem).ID;
              }
		        }

		        if(!empty(appliedpromo) && appliedpromo != undefined) {
		        	var bonusProductLineItems = currentBasket.bonusLineItems;
		        	for (var bonusProductLineItem in bonusProductLineItems) {
		        		 for (var priceAdjustment: PriceAdjustment in bonusProductLineItem.priceAdjustments) {
		        			 if(priceAdjustment.promotion.ID == appliedpromo){
		        				 iseligibledbonusitem = true;
		        				 var product = ProductMgr.getProduct(bonusProductLineItem.productID);
		        				 var bonusObj ={}
		        				 bonusObj.brand = product.brand;
		                         bonusObj.bopus_store_id = '';
		                         bonusObj.code = product.masterProduct.getID();
		                         bonusObj.gwp_flag = 'true';
		                         bonusObj.name = product.name;
		                         bonusObj.original_price = '0';
		                         bonusObj.price = '0';
		                         bonusObj.quantity = '1';
		                         bonusObj.selected_sku = product.ID;
		                         bonusObj.ship_from_store_id = '';
		                         eligibledbonusitem = bonusObj;
		        			 }
		        		 }
		        	}
		        }
	        } catch(e){
	        	var test= e.message;
	        	var iseligibledbonusitem = false;
	        }

	        // Re-calculate the payments.
	        var calculatedPaymentTransactionTotal = COHelpers.calculatePaymentTransaction(currentBasket);

	        var basketModel = new CartModel(currentBasket);
	        basketModel.valid.error = false;
	        basketModel.order = new OrderModel(currentBasket, {containerView: 'basket'});
	        basketModel.iseligibledbonusitem = iseligibledbonusitem;
	        if(iseligibledbonusitem){
	        	basketModel.eligibledbonusitem = eligibledbonusitem;
	        }
            if (preferences.CartBEPerformanceChangesEnable) {
                var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
                basketModel.orderProductSummary = renderTemplateHelper.getRenderedHtml(
                {
                    order: basketModel.order
                },
                'checkout/orderProductSummary'
                );
            }
            res.json(basketModel);
            if (session.privacy.privateSalePromoID) {
                // setting a different session variable to not apply pricebooks to the session on the common model
                delete session.privacy.skipPricebookCal;
            }
	        return next();
	    }
	);

server.append('RemoveCouponLineItem', function (req, res, next) {
	var BasketMgr = require('dw/order/BasketMgr');
	var OrderModel = require('*/cartridge/models/order');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var currentBasket = BasketMgr.getCurrentBasket();
    // Re-calculate the payments.
    var calculatedPaymentTransactionTotal = COHelpers.calculatePaymentTransaction(currentBasket);
    if (calculatedPaymentTransactionTotal.error) {
        res.json({
            error: true,
            errorMessage: Resource.msg('error.cannot.remove.coupon', 'cart', null)
        });
        if (!preferences.CartBEPerformanceChangesEnable) {
            return next();
        }
    } else {
        var amexpayHelper = require('*/cartridge/scripts/helpers/amexpayHelper');
        var result = amexpayHelper.getAmexPayAmount(currentBasket);
    }

    var orderModel = new OrderModel(currentBasket, {containerView: 'basket'});

    if (preferences.CartBEPerformanceChangesEnable) {
        var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
        var orderProductSummary = renderTemplateHelper.getRenderedHtml(
          {
            order: orderModel
          },
          'checkout/orderProductSummary'
        );

        res.setViewData({
            orderProductSummary : orderProductSummary
        });
    }

    if (!calculatedPaymentTransactionTotal.error) {
        res.setViewData({
            order : orderModel
        });
    }
    next();
});

server.append('ToggleShippingOption', function (req, res, next) {
	var BasketMgr = require('dw/order/BasketMgr');
    var currentBasket = BasketMgr.getCurrentBasket();
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var CartModel = preferences.CartBEPerformanceChangesEnable ? CartModelGlobal : require('*/cartridge/models/cart');
    COHelpers.ensureNoEmptyShipments(req);
	var basketModel = new CartModel(currentBasket);
	var viewData = res.viewData;
	viewData.basketModel = basketModel;
	res.setViewData(viewData);
	next();
});

server.replace('AdjustLineItemPrice', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var UUIDUtils = require('dw/util/UUIDUtils');
    var Transaction = require('dw/system/Transaction');
    var Money = require('dw/value/Money');
    var PercentageDiscount = require('dw/campaign/PercentageDiscount');
    var AmountDiscount = require('dw/campaign/AmountDiscount');
    var Status = require('dw/system/Status');
    var PriceAdjustmentLimitTypes = require('dw/order/PriceAdjustmentLimitTypes');
    var formatMoney = require('dw/util/StringUtils').formatMoney;
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var currentBasket = BasketMgr.getCurrentBasket();
    var CartModel = preferences.CartBEPerformanceChangesEnable ? CartModelGlobal : require('*/cartridge/models/cart');

    if (session.privacy.privateSalePromoID) {
        // setting a different session variable to not apply pricebooks to the session on the common model
        session.privacy.skipPricebookCal = true;
    }

    if (!currentBasket) {
        res.setStatusCode(500);
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });
        return next();
    }
    var priceOverrideForm = server.forms.getForm('priceOverride');
    var priceOverrideObj = priceOverrideForm.toObject();
    var pliUUID = req.form.pliuuid;
    var priceOverrided = false;
    priceOverrideObj.pliUUID = pliUUID;
    if (priceOverrideForm.valid && pliUUID) {
        res.setViewData(priceOverrideObj);
        var formInfo = res.getViewData();
        Transaction.wrap(function () {
            var uuid = formInfo.pliUUID;
            var updatedPrice = parseFloat(formInfo.newPrice);
            if (updatedPrice >= 0) {
                var productLineItems = currentBasket.allProductLineItems;
                var reqlineItem = collections.find(productLineItems, function (item) {
                    return item.UUID === uuid;
                });
                if (reqlineItem) {
                    // If the Overriding Price is more than Adjusted Price.
                    var currentPrice = reqlineItem.getBasePrice().value;
                    if (updatedPrice > currentPrice) {
                        var basketModel = new CartModel(currentBasket);
                        basketModel.CSRSuccess = false;
                        basketModel.CSRerrorMessage = Resource.msgf('label.exceed.base.amount', 'cart', null);
                        res.json(basketModel);
                        return;
                    }
                    // Remove the existing CSR Adjustment if already applied.
                    var adjustments = reqlineItem.priceAdjustments.iterator();
                    while (adjustments.hasNext()) {
                        var existingAdjustment = adjustments.next();
                        if ('isCSRAdjustment' in existingAdjustment.custom && existingAdjustment.custom.isCSRAdjustment) {
                            reqlineItem.removePriceAdjustment(existingAdjustment);
                            delete reqlineItem.custom.priceOverrided;
                            delete reqlineItem.custom.cscPriceoverrideReason;
                            delete reqlineItem.custom.cscPriceoverrideNote;
                            delete reqlineItem.custom.cscPriceoverrideNewprice;
                            // refresh Basket Calculation
                            basketCalculationHelpers.calculateTotals(currentBasket);
                        }
                    }
                    var newPrice = new Money(updatedPrice, currentBasket.getCurrencyCode());
                    var oldPrice = reqlineItem.adjustedPrice.divide(reqlineItem.quantityValue);
                    var adjustment = reqlineItem.createPriceAdjustment(UUIDUtils.createUUID(), new AmountDiscount(new Number(oldPrice.subtract(newPrice).value) * reqlineItem.quantityValue));
                    adjustment.setManual(true);
                    basketCalculationHelpers.calculateTotals(currentBasket);
                    var limitStatus = currentBasket.verifyPriceAdjustmentLimits();
                    if (limitStatus.status === Status.ERROR) {
                        var errorItems = limitStatus.items;
                        for (var i = 0; i < errorItems.length; i++) {
                            var statusItem = errorItems[i];
                            var statusDetail = statusItem.details.entrySet().iterator().next();
                            var maxAllowedLimit = (Number)(statusDetail.key);
                            if (statusItem.code == PriceAdjustmentLimitTypes.TYPE_ITEM) {
                                // Remove the adjustment and display the error message to CSR.
                                reqlineItem.removePriceAdjustment(adjustment);
                                basketCalculationHelpers.calculateTotals(currentBasket);
                                var maxLimit = formatMoney(new Money(maxAllowedLimit, currentBasket.getCurrencyCode()));
                                var basketModel = new CartModel(currentBasket);
                                basketModel.CSRSuccess = false;
                                basketModel.CSRerrorMessage = Resource.msgf('label.exceed.allowed.amount', 'cart', null, maxLimit);
                                res.json(basketModel);
                            }
                        }
                    } else {
                        adjustment.custom.isCSRAdjustment = true;
                        reqlineItem.custom.priceOverrided = true;
                        reqlineItem.custom.cscPriceoverrideReason = formInfo.priceOverrideReason;
                        reqlineItem.custom.cscPriceoverrideNote = formInfo.addNote;
                        reqlineItem.custom.cscPriceoverrideNewprice = updatedPrice;
                        priceOverrided = true;
                    }
                }
            }
        });
    }
    if (priceOverrided) {
        // eslint-disable-next-line
        var basketModel = new CartModel(currentBasket);
        // eslint-disable-next-line block-scoped-var
        res.json(basketModel);
    }
    if (session.privacy.privateSalePromoID) {
        // setting a different session variable to not apply pricebooks to the session on the common model
        delete session.privacy.skipPricebookCal;
    }
    next();
});

server.get('Recommendations', function (req, res, next) {
    res.render('cart/cartRecommendations');
    next();
});

server.post('PromoCodeDetails', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var URLUtils = require('dw/web/URLUtils');
    var TotalsModel = require('*/cartridge/models/totals');
    var actionUrls = {submitCouponCodeUrl: URLUtils.url('Cart-AddCoupon').toString()};
    var basket = BasketMgr.getCurrentBasket();
    var totalsModel = new TotalsModel(basket);
    res.render('cart/cartPromoCodeDetailsSection', {
        actionUrls: actionUrls,
        totals: totalsModel
    });
    next();
});

server.get('CachedMiniCartShow', cache.applyDefaultCache, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var CartModel = require('*/cartridge/models/cart');
  var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');

  var currentBasket = BasketMgr.getCurrentBasket();

  // Setting up the Payment Type Session attribute so that promotions dependent on Regular Payment Methods (Customer Group RegularTenderUsers) always apply in the cart
  if (req.session.raw.custom.isHBCTenderType === null || req.session.raw.custom.isHBCTenderType === undefined) {
    // eslint-disable-next-line no-param-reassign
    req.session.raw.custom.isHBCTenderType = false;
  }

  var basketModel = new CartModel(currentBasket);
  var viewData = cartHelper.handleMiniCartShow(currentBasket, req.session.currency.currencyCode, basketModel);
  res.setViewData(viewData);
  viewData.shopRunnerEnabled = preferences.shopRunnerEnabled;
  res.render('checkout/cart/miniCart', basketModel);
  next();
});
server.prepend('MiniCartShow', function (req, res, next) {
  if (preferences.CartBEPerformanceChangesEnable) {
    var BasketMgr = require('dw/order/BasketMgr');
    var CartModel = require('*/cartridge/models/cart');
    var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
    var URLUtils = require('dw/web/URLUtils');
    var Transaction = require('dw/system/Transaction');

    var currentBasket = BasketMgr.getCurrentBasket();
    var savedBasketHash = req.session.privacyCache.get('customerBasketHash');
    var currentBasketHash = cartHelper.getBasketHash(currentBasket, req.session.currency.currencyCode);

    if (savedBasketHash && currentBasketHash && savedBasketHash !== currentBasketHash) {
        req.session.privacyCache.set('customerBasketHash', currentBasketHash);
    }
    res.redirect(URLUtils.url('Cart-CachedMiniCartShow', 'hash', currentBasketHash));
    return next();
  }
  next();
});
module.exports = server.exports();
