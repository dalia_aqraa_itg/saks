'use strict';

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');
server.extend(module.superModule);
var URLUtils = require('dw/web/URLUtils');

/**
 *
 * @param {string} products - list of product details info in the form of "productId:quantity,productId:quantity,... "
 * @returns {Object} a object containing product ID and quantity
 */
function buildProductListAsJson(products) {
  if (!products) {
    return null;
  }

  return products.split(',').map(function (item) {
    var properties = item.split(':');
    return { id: properties[0], quantity: properties[1] };
  });
}

/**
 * route: Initiates the store search modal
 * renders: StoreModel {Object} containing array of store models
 */
server.replace('InitSearch', function (req, res, next) {
  var sessionCurrentStoreID = session.custom.currentStore ? session.custom.currentStore : null;

  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var storeHelper = require('*/cartridge/scripts/helpers/storeHelpers');
  var preferences = require('*/cartridge/config/preferences');
  var bopisHelper = require('*/cartridge/scripts/helpers/bopisHelpers')
  var showMap = false;
  var myStore = null;
  var horizontalView = true;
  var isForm = true;
  var source = req.querystring.source;
  var storesModel = {
    stores: null
  };
  var url = null;
  var products = buildProductListAsJson(req.querystring.products);
  var storesModel = storeHelper.getStores(null, null, null, null, request.geolocation, false, url, products);
  var currentCustomerProfile = customer.profile;
  if (currentCustomerProfile && currentCustomerProfile.custom.hasOwnProperty('myDefaultStore') && currentCustomerProfile.custom.myDefaultStore) {
    myStore = currentCustomerProfile.custom.myDefaultStore;
  } else {
    var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
    var homeStore = cookiesHelper.read('homeStore');
    if (homeStore) {
      myStore = homeStore;
    }
  }
  storesModel.radiusOptions = [15, 30, 50, 100, 300];
  storesModel.actionUrl = URLUtils.url('Stores-FindStores', 'showMap', showMap).toString();
  storesModel.setMyStoreUrl = URLUtils.url('Stores-SetMyHomeStore').toString();
  var currentStore = bopisHelper.getDefaultStore(null);
  var currentStoreID = currentStore ? currentStore.ID : '';
  var viewData = {
    stores: storesModel,
    horizontalView: horizontalView,
    isForm: isForm,
    showMap: showMap,
    executeSearch: false,
    distance: preferences.defaultStoreLookUpRadius,
    bopisPostal: request.geolocation.postalCode,
    myStore: myStore,
    source: source,
    currentStoreID: currentStoreID
  };
  if (req.querystring.products) {
    viewData.products = req.querystring.products;
  }
  var storesResultsHtml = renderTemplateHelper.getRenderedHtml(viewData, 'storeLocator/storeLocatorNoDecorator');

  storesModel.storesResultsHtml = storesResultsHtml;
  res.json(storesModel);
  next();
});
//* EXTENTED*//

/**
 * route: Initiates the zip search modal
 */
server.get('InitSDDSearch', cache.applyDefaultCache, function (req, res, next) {
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var sddModel = {};

  var viewData = {
    actionUrl: URLUtils.url('Stores-SetSDDZipCode').toString()
  };
  if (req.querystring.products) {
    viewData.products = req.querystring.products;
  }
  var zipCodeSearchHtml = renderTemplateHelper.getRenderedHtml(viewData, 'product/components/samedaydelivery/sddsearch');
  sddModel.zipCodeSearchHtml = zipCodeSearchHtml;
  res.json(sddModel);
  next();
});

/** Add store refiment bar in the search pages****
 *** This is a url include in the template same****
 *** as refiments***/
server.replace('ShowStoreRefinement', server.middleware.include, function (req, res, next) {
  var CatalogMgr = require('dw/catalog/CatalogMgr');
  var bopisHelper = require('*/cartridge/scripts/helpers/bopisHelpers');
  var ProductSearchModel = require('dw/catalog/ProductSearchModel');
  var ProductSearch = require('*/cartridge/models/search/productSearch');
  var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
  var refineSearch = require('*/cartridge/models/bopis/refineSearch');
  var refinementActionUrl = 'Search-Show';
  var preferences = require('*/cartridge/config/preferences');
  var storeHelper = require('*/cartridge/scripts/helpers/storeHelpers');
  var isRefinedByStore = false;
  var isStoreFilterApplied = false,
    isSDDFilterApplied = false;
  var storeObj = {};

  var storeRefineResult = null;
  // get defalt store from session or with geo location
  var defaultStore = null;

  var defaultSDDStore = storeHelper.getDefaultSDDStore();
  isStoreFilterApplied = bopisHelper.isStoreFilterApplied(req);
  isSDDFilterApplied = storeHelper.isSDDFilterApplied(req);
  if (req.querystring.storeid && !isSDDFilterApplied) {
    defaultStore = bopisHelper.getDefaultStore(null, req.querystring.storeid);
  } else {
    defaultStore = bopisHelper.getDefaultStore();
  }
  // set store search url
  var storeRefineUrl = refineSearch.getStoreRefinementUrl(req, refinementActionUrl);
  var sddRefineUrl = refineSearch.getStoreRefinementUrl(req, refinementActionUrl);
  var refineUrl = refineSearch.getStoreRefinementUrl(req, refinementActionUrl);
  refineUrl = storeHelper.getRefinementUrl(refineUrl);
  sddRefineUrl = storeHelper.getSDDRefinementUrl(sddRefineUrl);
  if (defaultStore && req.querystring.ajax) {
    storeObj.storeid = defaultStore.ID;
  }
  if (req.querystring.storeid) {
    storeObj.storeid = req.querystring.storeid;
  }
  // add or remove the store if in search query
  storeRefineUrl = bopisHelper.toggleStoreRefinement(storeRefineUrl, defaultStore);
  var apiProductSearch = new ProductSearchModel();
  apiProductSearch = searchHelper.setupSearch(apiProductSearch, req.querystring);
  // PSM search with store refinement
  storeRefineResult = refineSearch.search(apiProductSearch, storeObj);
  isRefinedByStore = storeRefineResult.isRefinedByStore;
  apiProductSearch = storeRefineResult.apiProductsearch;

  var productSearch = new ProductSearch(
    apiProductSearch,
    req.querystring,
    req.querystring.srule,
    CatalogMgr.getSortingOptions(),
    CatalogMgr.getSiteCatalog().getRoot()
  );
  res.render('store/storeRefineBar', {
    defaultStore: defaultStore,
    isRefinedByStore: isRefinedByStore,
    productSearch: productSearch,
    storeRefineUrl: storeRefineUrl,
    sddRefineUrl: sddRefineUrl,
    refineUrl: refineUrl,
    isStoreFilterApplied: isStoreFilterApplied,
    storeModalUrl: URLUtils.url('Stores-InitSearch').toString(),
    setStoreUrl: URLUtils.url('Stores-SetStore').toString(),
    sddStoreModalUrl: URLUtils.url('Stores-InitSDDSearch').toString(),
    isBopisEnabled: preferences.isBopisEnabled,
    isEnabledforSameDayDelivery: preferences.isEnabledforSameDayDelivery,
    sddPostal: session.custom.sddpostal ? session.custom.sddpostal : request.geolocation.postalCode,
    sddStoreId: session.custom.sddstoreid ? session.custom.sddstoreid : null,
    isSDDFilterApplied: isSDDFilterApplied,
    isRefinedWithSDD: storeRefineResult.isRefinedWithSDD
  });
  next();
});

/**
 * route: Sets the selected zip in to session
 */
server.get('SetSDDZipCode', function (req, res, next) {
  var params = req.querystring;
  var storeHelper = require('*/cartridge/scripts/helpers/storeHelpers');
  var ProductFactory = require('*/cartridge/scripts/factories/product');
  var product = null;
  var Resource = require('dw/web/Resource');
  var productID = null,
    store = null;
  if (params.pidquan && params.pidquan.length > 0 && params.pidquan.split(':')[0].length > 0 && params.pidquan.split(':')[1].length > 0) {
    product = ProductFactory.get({ pid: params.pidquan.split(':')[0] });
    product.desiredQuantity = params.pidquan.split(':')[1];
  }
  if (params.postalCode) {
    session.custom.sddpostal = params.postalCode;
    store = storeHelper.getEligibleStoreForSDD(params.postalCode, product);
    store.postalCode = params.postalCode;
    store.message = store.storeid ? Resource.msg('store.refinement.sdd', 'search', null) : Resource.msg('search.sdd.noresults', 'search', null);
    store.productType = product ? product.productType : null;
  }
  res.json(store);
  next();
});

/**
 * route: Sets the selected store in to session or profile
 */
server.get('SetMyHomeStore', function (req, res, next) {
  var bopisHelper = require('*/cartridge/scripts/helpers/bopisHelpers');
  var Resource = require('dw/web/Resource');
  var Transaction = require('dw/system/Transaction');
  var params = req.querystring;
  var resp = {};
  var storeID = params.storeID ? params.storeID : null;
  var distance = params.distance ? params.distance : null;
  if (storeID && distance) {
    var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
    cookiesHelper.create('homeStore', storeID);
    session.custom.homeStore = storeID;
    if (req.currentCustomer.profile && storeID) {
      Transaction.wrap(function () {
        customer.profile.custom.myDefaultStore = storeID;
      });
    }
  }
  resp.otherStoreMsg = Resource.msg('storesearch.setmystore.msg', 'storeLocator', null);
  resp.myStoreMsg = Resource.msg('storesearch.mystore.msg', 'storeLocator', null);
  res.json(resp);
  next();
});

/**
 * route: Sets the selected store in to session
 * renders: ViewData containing store details
 */
server.replace('SetStore', function (req, res, next) {
  var params = req.querystring;
  var bopisHelper = require('*/cartridge/scripts/helpers/bopisHelpers');
  var ProductFactory = require('*/cartridge/scripts/factories/product');
  var Resource = require('dw/web/Resource');
  var product = ProductFactory.get(params);
  res.setViewData({
    product: product,
    storeid: params.storeId ? params.storeId : null,
    bopisDefaultLabel: Resource.msg('store.refinement.bopis', 'search', null)
  });
  if (params.storeId && params.storeDistance) {
    // set store to session only in search page
    if (params.savetosession && params.savetosession === 'true') {
      bopisHelper.setStoreFromSession(params.storeId, params.storeDistance);
    }
    /** ************BOPIS******************
     *****Store Info, Inventory Detail******
     ****Availability Message only in PDP***/

    if (!params.noviewdata) {
      bopisHelper.setBopisViewData(req, res);
    }
  }
  res.json(res.getViewData());
  next();
});

server.replace('FindStores', function (req, res, next) {
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var preferences = require('*/cartridge/config/preferences');
  var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');
  var radius = req.querystring.radius;
  var postalCode = req.querystring.postalCode;
  var source = req.querystring.source;
  var lat = req.querystring.lat;
  var long = req.querystring.long;
  var showMap = req.querystring.showMap || false;
  var horizontalView = req.querystring.horizontalView || false;
  var isForm = req.querystring.isForm || false;
  var myStore = null;
  var url = null;
  var products = buildProductListAsJson(req.querystring.products);

  var currentCustomerProfile = customer.profile;
  if (currentCustomerProfile && currentCustomerProfile.custom.hasOwnProperty('myDefaultStore') && currentCustomerProfile.custom.myDefaultStore) {
    myStore = currentCustomerProfile.custom.myDefaultStore;
  } else {
    var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
    var homeStore = cookiesHelper.read('homeStore');
    if (homeStore) {
      myStore = homeStore;
    }
  }

  var storesModel = storeHelpers.getStores(radius, postalCode, lat, long, req.geolocation, showMap, url, products);
  storesModel.setMyStoreUrl = URLUtils.url('Stores-SetMyHomeStore').toString();
  if (products) {
    var context = {
      stores: storesModel,
      horizontalView: horizontalView,
      isForm: isForm,
      showMap: showMap,
      distance: preferences.defaultStoreLookUpRadius,
      bopisPostal: postalCode ? postalCode : request.geolocation.postalCode,
      myStore: myStore,
      source: source
    };

    var storesResultsHtml = storesModel.stores ? renderTemplateHelper.getRenderedHtml(context, 'storeLocator/storeLocatorResults') : null;

    storesModel.storesResultsHtml = storesResultsHtml;
  }

  res.json(storesModel);
  next();
});

module.exports = server.exports();
