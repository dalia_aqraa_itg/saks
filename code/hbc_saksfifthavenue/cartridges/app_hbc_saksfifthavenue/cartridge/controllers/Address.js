'use strict';

var server = require('server');
server.extend(module.superModule);

server.append('AddAddress', function (req, res, next) {
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var customer = CustomerMgr.getCustomerByCustomerNumber(req.currentCustomer.profile.customerNo);
  var addresses = customer.addressBook.addresses;
  res.setViewData({ addressCount: addresses.length });
  next();
});

module.exports = server.exports();
