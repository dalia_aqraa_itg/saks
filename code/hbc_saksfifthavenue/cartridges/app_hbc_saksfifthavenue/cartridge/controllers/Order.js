'use strict';

var server = require('server');

var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var security = require('*/cartridge/scripts/middleware/security');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var preferences = require('*/cartridge/config/preferences');
var OMSConstants = require('*/cartridge/scripts/config/OMSServiceConfig');
var Logger = require('dw/system/Logger');
var OMSLogger = Logger.getLogger('OMS', 'OMSService');
var TurnToHelper = require('*/cartridge/scripts/util/HelperUtil');
var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
var HookMgr = require('dw/system/HookMgr');
var Calendar = require('dw/util/Calendar');

server.extend(module.superModule);

server.replace(
  'Details',
  consentTracking.consent,
  server.middleware.https,
  security.validateLoggedInCustomerOnOrder, // /* Fix for SFSX-1422 - https://hbcdigital.atlassian.net/browse/SFSX-1422 */
  function (req, res, next) {
    if (session.custom.isbot != null && session.custom.isbot) {
      res.json({
        error: [Resource.msg('error.message.login.bot', 'login', null)],
        botError: true
      });

      return next();
    }
    // Logger.debug('start OrderDetails !!!' + req.querystring.token);
    hooksHelper('app.google.recaptcha.verify', 'validateToken', req.querystring.token, require('*/cartridge/scripts/googleRecaptchaUtil').validateToken);
    // Logger.debug('end OrderDetails !!!' + result.score);

    if (session.custom.isbot) {
      res.json({
        error: [Resource.msg('error.message.login.bot', 'login', null)],
        botError: true
      });

      return next();
    }

    if (!req.querystring.detailsInclude && !(req.querystring.trackOrderPostal && req.querystring.trackOrderNumber)) {
      res.json({
        error: true
      });
    } else {
      var OrderDetailsHelper = require('*/cartridge/scripts/helpers/OrderDetailsHelper');
      var orderDetails = '';
      var errrorMsg = '';
      try {
        var orderDetailsRequest = {};
        orderDetailsRequest.BillToID = req.currentCustomer && req.currentCustomer.profile ? req.currentCustomer.profile.customerNo : '';
        var orderNumber = req.querystring.trackOrderNumber ? req.querystring.trackOrderNumber.trim() : '';
        if (!empty(orderNumber) && OMSConstants.OMS_RemovedZeroPrefix) {
          orderNumber = ('0' + orderNumber).slice(-9);
        }
        orderDetailsRequest.OrderNumber = orderNumber;
        orderDetailsRequest.ZipCode = req.querystring.trackOrderPostal ? req.querystring.trackOrderPostal.trim() : '';
        orderDetailsRequest.InvokedFrom = OMSConstants.OMS_ORDER_DETAILS_INVOKEDFROM;
        orderDetailsRequest.BusinessUnit = OMSConstants.BANNER_CODE;
        orderDetails = OrderDetailsHelper.getOrderDetails(orderDetailsRequest);
        if (orderDetails) {
          var breadcrumbs = [
            {
              htmlValue: Resource.msg('global.home', 'common', null),
              url: URLUtils.home().toString()
            },
            {
              htmlValue: Resource.msg('page.title.myaccount', 'account', null),
              url: URLUtils.url('Account-Show').toString()
            },
            {
              htmlValue: Resource.msg('label.orderhistory', 'account', null),
              url: URLUtils.url('Order-History').toString()
            }
          ];

          var exitLinkText = Resource.msg('link.orderdetails.orderhistory', 'account', null);
          var exitLinkUrl = URLUtils.https('Order-History', 'orderFilter', req.querystring.orderFilter);

          if (orderDetails.shippingDetails.name === 'SameDay') {
            orderDetails.cancellable = 'false';
          }

          var template = 'account/order/accountOrderDetails';
          if (req.querystring.detailType != 'STATUS' && req.querystring.trackOrderNumber && req.querystring.trackOrderPostal) {
            template = 'account/order/orderDetails';
          }
          res.render(template, {
            order: orderDetails,
            exitLinkText: exitLinkText,
            exitLinkUrl: exitLinkUrl,
            breadcrumbs: breadcrumbs,
            enableNarvar: preferences.enableNarvar,
            narvarReturnURL: preferences.returnURL
              ? preferences.returnURL + 'order=' + orderDetails.orderNo + '&bzip=' + orderDetails.billingAddress.zipCode + '&locale=' + req.locale.id
              : '',
            source: !req.querystring.detailsInclude ? 'track' : 'history',
            page: req.querystring.page
          });
        } else {
          if (!empty(session.custom.orderDetailsStatus)) {
            if (session.custom.orderDetailsStatus === 'SERVICE_DOWN') {
              errrorMsg = errrorMsg = Resource.msg('order.service.down.error', 'order', null);
            } else if (session.custom.orderDetailsStatus === 'NO_ORDER') {
              errrorMsg = Resource.msg('order.not.found.error', 'order', null);
            }
          }
          res.json({
            error: true,
            message: errrorMsg
          });
        }
      } catch (ex) {
        OMSLogger.error('[OMS] Error : {0} {1}', ex, ex.stack);

        if (!empty(session.custom.orderDetailsStatus)) {
          if (session.custom.orderDetailsStatus === 'SERVICE_DOWN') {
            errrorMsg = Resource.msg('order.service.down.error', 'order', null);
          } else if (session.custom.orderDetailsStatus === 'NO_ORDER') {
            errrorMsg = Resource.msg('order.not.found.error', 'order', null);
          }
        }

        res.json({
          error: true,
          message: errrorMsg
        });
      }
    }
    return next();
  }
);

server.replace('History', consentTracking.consent, server.middleware.https, function (req, res, next) {
  var customer = req.currentCustomer;
  var orderHistoryResponse = '';

  if (!customer || !customer.profile) {
    if (req.querystring.args) {
      req.session.privacyCache.set('args', req.querystring.args);
    }

    var target = req.querystring.rurl || 1;

    res.redirect(URLUtils.url('Login-Show', 'rurl', target, 'orderhistory', true));
  } else {
    var filterValues = [
      {
        displayValue: Resource.msg('orderhistory.threemonths.option', 'order', null),
        optionValue: URLUtils.url('Order-History', 'orderFilter', '3', 'filter', true).abs().toString()
      },
      {
        displayValue: Resource.msg('orderhistory.sixmonths.option', 'order', null),
        optionValue: URLUtils.url('Order-History', 'orderFilter', '6', 'filter', true).abs().toString()
      },
      {
        displayValue: Resource.msg('orderhistory.twelvemonths.option', 'order', null),
        optionValue: URLUtils.url('Order-History', 'orderFilter', '12', 'filter', true).abs().toString()
      },
      {
        displayValue: Resource.msg('orderhistory.all.option', 'order', null),
        optionValue: URLUtils.url('Order-History', 'orderFilter', 'none', 'filter', true).abs().toString()
      }
    ];
    var breadcrumbs = [
      {
        htmlValue: Resource.msg('global.home', 'common', null),
        url: URLUtils.home().toString()
      },
      {
        htmlValue: Resource.msg('page.title.myaccount', 'account', null),
        url: URLUtils.url('Account-Show').toString()
      }
    ];

    // logic to add date from and to for order based on last few month drop down selection
    var orderHistoryRequest = {};
    var currentDate = new Date();
    var currentMonth = currentDate.getMonth();
    var startMonth;
    var adjustYear = true; // a little confusing but: If we are in June, going 6 months back, we want to know about orders in December, if we are Feb going 3 months back, we want to know about orders in November
    var fromDate = currentDate.getDate() > 28 ? 28 : currentDate.getDate(); // round down to 28 to avoid issues with February and 30-day months

    if (req.querystring.orderFilter === '12') {
      startMonth = currentMonth;
    } else if (req.querystring.orderFilter === '6') {
      startMonth = (currentMonth + 6) % 12;
      adjustYear = currentMonth - 6 < 0;
    } else if (req.querystring.orderFilter === '3' || req.querystring.orderFilter === 'none' || !req.querystring.orderFilter) {
      startMonth = (currentMonth + 9) % 12;
      adjustYear = currentMonth - 3 < 0;
    }
    var adjustYearValue = adjustYear ? 1 : 0;

    var startDate = currentDate.getYear() - adjustYearValue + '-' + (startMonth + 1).toLocaleString() + '-' + fromDate.toLocaleString();
    // code for pagination and order by month
    orderHistoryRequest.PageInput = {
      PageNumber: Number(req.querystring.pageNumber) || 1,
      OrderHeaderKey: req.querystring.prevOrder || ''
    };
    if (req.querystring.orderFilter && req.querystring.orderFilter == 'none') {
      // Don't set the Start date
    } else {
      orderHistoryRequest.FromOrderDate = startDate;
    }
    var OrderHistoryHelper = require('*/cartridge/scripts/helpers/OrderHistoryHelper');

    // Filter by Pendings Order
    var pendingOrders = req.querystring.pendingOrders && req.querystring.pendingOrders == 'true' ? true : false;
    var pendingOrderURL = URLUtils.url('Order-History', 'filter', true).abs().toString();

    try {
      orderHistoryResponse = OrderHistoryHelper.getOrderHistory(customer, orderHistoryRequest, req.locale.id);
      if (pendingOrders) {
        orderHistoryResponse.orders = orderHistoryResponse.orders.filter(function (order) {
          return order.OrderComplete === 'N';
        });
      }
    } catch (ex) {
      OMSLogger.error('[OMS] Error : {0} {1}', ex, ex.stack);
      res.json({
        error: 'Error in processing response from Order Management'
      });
    }
    let template = 'account/order/history';
    let siteKey = TurnToHelper.getLocalizedSitePreferenceFromRequestLocale().turntoSiteKey;
    let turntoUrl = TurnToHelper.getLocalizedSitePreferenceFromRequestLocale().domain;
    if (req.querystring.filter) {
      template = 'account/order/orderList';
    }

    res.render(template, {
      includeRecaptchaJS: true,
      orders: orderHistoryResponse.orders,
      siteKey: siteKey,
      turntoUrl: turntoUrl,
      isLastPage: orderHistoryResponse.IsLastPage,
      nextPageNumber: (req.querystring.pageNumber ? Number(req.querystring.pageNumber) : 1) + 1,
      lastOrderHeaderKey: orderHistoryResponse.lastOrderHeaderKey || '',
      filterValues: filterValues,
      pendingOrderURL: pendingOrderURL,
      orderFilter: req.querystring.orderFilter || '3',
      accountlanding: false,
      breadcrumbs: breadcrumbs,
      pageType: 'order-history',
      historyStatus: orderHistoryResponse.historyStatus,
      borderFreeEnabled: preferences.borderFreeEnabled,
      bfxOrderStatusUrl: preferences.bfxOrderStatusUrl,
      isABTestingOn: preferences.isABTestingOn
    });
  }
  next();
});

/**
 * Added for SFSX-465
 * This method requires custom steps for Saks Fifth Avenue Banner
 * Hence copied it from app_hbc_core cartridge to the app_hbc_saksfifthavenue cartridge.
 *
 * The custom piece is to update the lastPasswordModified field in the Profile object.
 *
 */
server.replace(
  'CreateAccount',
  server.middleware.https,
  csrfProtection.validateAjaxRequest,
  security.validateGuestCustomerOnOrder, // /* Fix for SFSX-1552 - https://hbcdigital.atlassian.net/browse/SFSX-1552 */
  function (req, res, next) {
    var OrderMgr = require('dw/order/OrderMgr');
    var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');

    var formErrors = require('*/cartridge/scripts/formErrors');

    var passwordForm = server.forms.getForm('newPasswords');
    var newPassword = passwordForm.newpassword.htmlValue;
    var confirmPassword = passwordForm.newpasswordconfirm.htmlValue;
    if (newPassword !== confirmPassword) {
      passwordForm.valid = false;
      passwordForm.newpasswordconfirm.valid = false;
      passwordForm.newpasswordconfirm.error = Resource.msg('error.message.mismatch.newpassword', 'forms', null);
    }

    // changes from prepend method of plugin_wishlist cartridge
    var list = productListHelper.getList(req.currentCustomer.raw, {
      type: 10
    });
    res.setViewData({
      list: list
    });

    //Changes for SFSX-1552 - https://hbcdigital.atlassian.net/browse/SFSX-1552 - start
    /*var order = OrderMgr.getOrder(req.querystring.ID);
        res.setViewData({ orderID: req.querystring.ID });*/
    //Changed to pull order number available in the session vs from the request.
    //This will prevent order number being changed during HTTP POST.
    var order = OrderMgr.getOrder(req.session.raw.custom.orderID);
    /**
     * Security patch. Check order UUID and bail out if mismatch
     */
    if (!order || order.customer.ID !== req.currentCustomer.raw.ID || order.getUUID() !== req.querystring.UUID) {
      res.json({ error: [Resource.msg('error.message.unable.to.create.account', 'login', null)] });
      return next();
    }
    res.setViewData({
      orderID: req.session.raw.custom.orderID
    });
    //Changes for SFSX-1552 - https://hbcdigital.atlassian.net/browse/SFSX-1552 - end
    var registrationObj = {
      firstName: order.billingAddress.firstName,
      lastName: order.billingAddress.lastName,
      phone: order.billingAddress.phone,
      email: order.customerEmail,
      password: newPassword
    };

    var creditCardPaymentDetails = COHelpers.getOrderCreditCardPaymentDetails(order);
    if (creditCardPaymentDetails) {
      registrationObj.payment = creditCardPaymentDetails;
    }

    if (passwordForm.valid) {
      res.setViewData(registrationObj);

      this.on('route:BeforeComplete', function (req, res) {
        // eslint-disable-line no-shadow
        var CustomerMgr = require('dw/customer/CustomerMgr');
        var Transaction = require('dw/system/Transaction');
        var addressHelpers = require('*/cartridge/scripts/helpers/addressHelpers');

        var registrationData = res.getViewData();

        var login = registrationData.email;
        var password = registrationData.password;
        var newCustomer;
        var authenticatedCustomer;
        var newCustomerProfile;
        var errorObj = {};

        delete registrationData.email;
        delete registrationData.password;

        // attempt to create a new user and log that user in.
        try {
          Transaction.wrap(function () {
            var error = {};
            var profilePassword;

            if (preferences.isABTestingOn) {
              profilePassword = preferences.tempLegacyLogin;
            } else {
              profilePassword = password;
            }
            newCustomer = CustomerMgr.createCustomer(login, profilePassword);

            var authenticateCustomerResult = CustomerMgr.authenticateCustomer(login, profilePassword);
            if (authenticateCustomerResult.status !== 'AUTH_OK') {
              error = {
                authError: true,
                status: authenticateCustomerResult.status
              };
              throw error;
            }

            authenticatedCustomer = CustomerMgr.loginCustomer(authenticateCustomerResult, false);

            if (!authenticatedCustomer) {
              error = {
                authError: true,
                status: authenticateCustomerResult.status
              };
              throw error;
            } else {
              // assign values to the profile
              newCustomerProfile = newCustomer.getProfile();

              newCustomerProfile.firstName = registrationData.firstName;
              newCustomerProfile.lastName = registrationData.lastName;
              newCustomerProfile.phoneHome = registrationData.phone;
              newCustomerProfile.email = login;
              if (preferences.isABTestingOn) {
                var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
                var newPswd = legacyCustomerLogin.getPasswordLegacyHashed(password);
                newCustomerProfile.custom.legacyPasswordHash = newPswd;
              }

              //Added for SFSX-465 - start
              var accHelper = require('*/cartridge/scripts/helpers/accountHelpers');
              accHelper.updateLastPasswordModified(newCustomerProfile);
              //Added for SFSX-465 - end

              order.setCustomer(newCustomer);

              // save all used shipping addresses to address book of the logged in customer
              var allAddresses = addressHelpers.gatherShippingAddresses(order);
              allAddresses.forEach(function (address) {
                addressHelpers.saveAddress(
                  address,
                  {
                    raw: newCustomer
                  },
                  addressHelpers.generateAddressName(address)
                );
              });
              // Save the Credit Card used to place the order.
              if (registrationData.payment) {
                COHelpers.saveOrderPaymentToProfile(newCustomerProfile, registrationData.payment);
              }
              // Maybe enroll in loyalty here
              if (HookMgr.hasHook('app.customer.hbc.loyalty.enroll')) {
                var loyaltyEnrollForm = server.forms.getForm('loyaltyEnrollCheckoutConfirm');

                if (loyaltyEnrollForm && loyaltyEnrollForm.enroll.htmlValue) {
                  // Call Enroll API if Enroll checkbox is true
                  var loyaltyResponse = hooksHelper('app.customer.hbc.loyalty.enroll', 'expressEnroll', [newCustomerProfile, order.billingAddress.postalCode]);

                  // Assign loyalty_id to the customer profile
                  if (loyaltyResponse && loyaltyResponse.success === true) {
                    newCustomerProfile.custom.hudsonReward = String(loyaltyResponse.loyalty_id); // IMPORTANT - MUST CAST TO STRING
                  }
                  if (loyaltyResponse.errorMessage) {
                    Logger.error('Error registering customer for Loyalty: \n{0}', loyaltyResponse.errorMessage);
                  }
                }
              }
            }
          });
        } catch (e) {
          errorObj.error = true;
          errorObj.errorMessage = e.authError
            ? Resource.msg('error.message.unable.to.create.account', 'login', null)
            : Resource.msg('error.message.username.invalid', 'forms', null);
        }
        // Security Patch
        delete registrationData.firstName;
        delete registrationData.lastName;
        delete registrationData.phone;

        if (errorObj.error) {
          res.json({
            error: [errorObj.errorMessage]
          });

          return;
        }

        // send a registration email via CNS hook code change from accounthelpers
        // account creation email via CNS
        var Locale = require('dw/util/Locale');
        var currentLanguage = Locale.getLocale(req.locale.id).getLanguage();
        hooksHelper(
          'app.customer.email.account.created',
          'sendCreateAccountEmail',
          authenticatedCustomer.profile,
          currentLanguage,
          require('*/cartridge/scripts/helpers/accountHelpers').sendCreateAccountEmail
        );

        // UCID Call to Sync
        hooksHelper('ucid.middleware.service', 'createUCIDCustomer', [authenticatedCustomer.profile, authenticatedCustomer.profile.customerNo, null]);

        // changes added from append code in plugin_wishlists
        var listLoggedIn = productListHelper.getList(newCustomer, {
          type: 10
        });
        productListHelper.mergelists(listLoggedIn, list, req, {
          type: 10
        });
        res.json({
          success: true,
          redirectUrl: URLUtils.url('Account-Show', 'registration', 'submitted').toString()
        });
      });
    } else {
      res.json({
        fields: formErrors.getFormErrors(passwordForm)
      });
    }

    return next();
  }
);

server.replace('Status', consentTracking.consent, server.middleware.https, csrfProtection.generateToken, function (req, res, next) {
  var ContentMgr = require('dw/content/ContentMgr');
  var target = req.querystring.rurl || 1;
  var narvarOrderNum = request.httpParameterMap.order_num;
  var narvarPostalcode = request.httpParameterMap.billing_zip_code;
  var siteRef = request.httpParameterMap.site_refer;

  if (isNarvarParametersPresent(narvarOrderNum, narvarPostalcode)) {
    var viewData = res.getViewData();
    res.setViewData(viewData);
    res.redirect(URLUtils.url('Order-Details', 'trackOrderPostal', narvarPostalcode, 'trackOrderNumber', narvarOrderNum, 'detailsInclude', true, 'site_refer', siteRef));
  }
  var OrderDetailsHelper = require('*/cartridge/scripts/helpers/OrderDetailsHelper');
  var orderDetails = '';
  var errrorMsg = '';

  try {
    var orderDetailsRequest = {};
    orderDetailsRequest.BillToID = req.currentCustomer && req.currentCustomer.profile ? req.currentCustomer.profile.customerNo : '';
    var orderNumber = req.querystring.trackOrderNumber ? req.querystring.trackOrderNumber.trim() : '';
    if (!empty(orderNumber) && OMSConstants.OMS_RemovedZeroPrefix) {
      orderNumber = ('0' + orderNumber).slice(-9);
    }
    orderDetailsRequest.OrderNumber = orderNumber;
    orderDetailsRequest.ZipCode = req.querystring.trackOrderPostal ? req.querystring.trackOrderPostal.trim() : '';
    orderDetailsRequest.InvokedFrom = OMSConstants.OMS_ORDER_DETAILS_INVOKEDFROM;
    orderDetailsRequest.BusinessUnit = OMSConstants.BANNER_CODE;
    orderDetails = OrderDetailsHelper.getOrderDetails(orderDetailsRequest);
    if (orderDetails) {
      var breadcrumbs = [
        {
          htmlValue: Resource.msg('global.home', 'common', null),
          url: URLUtils.home().toString()
        },
        {
          htmlValue: Resource.msg('page.title.myaccount', 'account', null),
          url: URLUtils.url('Account-Show').toString()
        },
        {
          htmlValue: Resource.msg('label.orderhistory', 'account', null),
          url: URLUtils.url('Order-History').toString()
        }
      ];

      var exitLinkText = Resource.msg('link.orderdetails.orderhistory', 'account', null);
      var exitLinkUrl = URLUtils.https('Order-History', 'orderFilter', req.querystring.orderFilter);

      if (orderDetails.shippingDetails.name === 'SameDay') {
        orderDetails.cancellable = 'false';
      }

      res.render('account/order/accountOrderDetails', {
        order: orderDetails,
        exitLinkText: exitLinkText,
        exitLinkUrl: exitLinkUrl,
        breadcrumbs: breadcrumbs,
        enableNarvar: preferences.enableNarvar,
        narvarReturnURL: preferences.returnURL
          ? preferences.returnURL + 'order=' + orderDetails.orderNo + '&bzip=' + orderDetails.billingAddress.zipCode + '&locale=' + req.locale.id
          : '',
        source: !req.querystring.detailsInclude ? 'track' : 'history',
        page: req.querystring.page
      });
    } else {
      if (!empty(session.custom.orderDetailsStatus)) {
        if (session.custom.orderDetailsStatus === 'SERVICE_DOWN') {
          errrorMsg = errrorMsg = Resource.msg('order.service.down.error', 'order', null);
        } else if (session.custom.orderDetailsStatus === 'NO_ORDER') {
          errrorMsg = Resource.msg('order.not.found.error', 'order', null);
        }
      }
      res.json({
        error: true,
        message: errrorMsg
      });
    }
  } catch (ex) {
    OMSLogger.error('[OMS] Error : {0} {1}', ex, ex.stack);

    if (!empty(session.custom.orderDetailsStatus)) {
      if (session.custom.orderDetailsStatus === 'SERVICE_DOWN') {
        errrorMsg = Resource.msg('order.service.down.error', 'order', null);
      } else if (session.custom.orderDetailsStatus === 'NO_ORDER') {
        errrorMsg = Resource.msg('order.not.found.error', 'order', null);
      }
    }

    res.json({
      error: true,
      message: errrorMsg
    });
  }

  if (req.currentCustomer.profile && !isNarvarParametersPresent(narvarOrderNum, narvarPostalcode)) {
    res.redirect(URLUtils.url('Order-History'));
  } else if (req.currentCustomer.profile && isNarvarParametersPresent(narvarOrderNum, narvarPostalcode)) {
    var viewData = res.getViewData();
    res.setViewData(viewData);
    res.redirect(URLUtils.url('Order-Details', 'trackOrderPostal', narvarPostalcode, 'trackOrderNumber', narvarOrderNum, 'detailsInclude', true, 'site_refer', siteRef));
  }

  var rememberMe = false;
  var userName = '';
  var actionUrl = URLUtils.url('Account-Login', 'rurl', target);
  var createAccountUrl = URLUtils.url('Account-SubmitRegistration', 'rurl', target).relative().toString();
  var navTabValue = req.querystring.action;

  if (req.currentCustomer.credentials) {
    rememberMe = true;
    userName = req.currentCustomer.credentials.username;
  }

  var breadcrumbs = [
    {
      htmlValue: Resource.msg('global.home', 'common', null),
      url: URLUtils.home().toString()
    }
  ];

  let siteKey = TurnToHelper.getLocalizedSitePreferenceFromRequestLocale().turntoSiteKey;
  let turntoUrl = TurnToHelper.getLocalizedSitePreferenceFromRequestLocale().domain;
  var borderFreeStatusMsg = ContentMgr.getContent('checkorder-international');
  var internationalMessage = ContentMgr.getContent('checkorder-international-message');
  var profileForm = server.forms.getForm('profile');
  profileForm.clear();
  res.render('account/order/orderStatus', {
    navTabValue: navTabValue || 'login',
    siteKey: siteKey,
    turntoUrl: turntoUrl,
    rememberMe: rememberMe,
    userName: userName,
    actionUrl: actionUrl,
    profileForm: profileForm,
    breadcrumbs: breadcrumbs,
    oAuthReentryEndpoint: 1,
    createAccountUrl: createAccountUrl,
    includeRecaptchaJS: true,
    borderFreeStatusMsg: borderFreeStatusMsg ? borderFreeStatusMsg.custom.body : null,
    internationalMessage: internationalMessage && internationalMessage.custom.body ? internationalMessage.custom.body.markup : null
  });
  return next();
});

function isNarvarParametersPresent(narvarOrderNum, narvarPostalcode) {
  return !narvarOrderNum.empty && !narvarPostalcode.empty;
}
module.exports = server.exports();
