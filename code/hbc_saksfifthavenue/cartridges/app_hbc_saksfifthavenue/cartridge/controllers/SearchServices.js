'use strict';

var server = require('server');
server.extend(module.superModule);

var cache = require('*/cartridge/scripts/middleware/cache');

server.replace('GetSuggestions', cache.applyDefaultCache, function (req, res, next) {
  var SuggestModel = require('dw/suggest/SuggestModel');
  var CategorySuggestions = require('*/cartridge/models/search/suggestions/category');
  var ContentSuggestions = require('*/cartridge/models/search/suggestions/content');
  var ProductSuggestions = require('*/cartridge/models/search/suggestions/product');
  var SearchPhraseSuggestions = require('*/cartridge/models/search/suggestions/searchPhrase');
  var Site = require('dw/system/Site');
  var showResultsCountSearchSuggestion = Site.getCurrent().getCustomPreferenceValue('showResultsCountSearchSuggestion');
  var categorySuggestions;
  var contentSuggestions;
  var productSuggestions;
  var recentSuggestions;
  var popularSuggestions;
  var brandSuggestions;
  var searchTerms = req.querystring.q;
  var suggestions;
  // TODO: Move minChars and maxSuggestions to Site Preferences when ready for refactoring
  var maxProdSuggestions = 5;
  // Unfortunately, by default, max suggestions is set to 10 and is not configurable in Business
  // Manager.
  var maxSuggestions = 5;

  suggestions = new SuggestModel();
  suggestions.setSearchPhrase(searchTerms);
  suggestions.setMaxSuggestions(maxSuggestions);
  categorySuggestions = new CategorySuggestions(suggestions, maxSuggestions);
  contentSuggestions = new ContentSuggestions(suggestions, maxSuggestions);
  productSuggestions = new ProductSuggestions(suggestions, maxProdSuggestions);
  recentSuggestions = new SearchPhraseSuggestions(suggestions.recentSearchPhrases, maxSuggestions);
  popularSuggestions = new SearchPhraseSuggestions(suggestions.popularSearchPhrases, maxSuggestions, true);
  brandSuggestions = new SearchPhraseSuggestions(suggestions.brandSuggestions, maxSuggestions);

  var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
  var knownUser = searchHelper.isKnownUser(req.session);

  var showPopularSearch = true;
  if (req.querystring.q && /^[0-9]*$/.test(req.querystring.q)) {
    var ProductMgr = require('dw/catalog/ProductMgr');
    var apiProduct = ProductMgr.getProduct(req.querystring.q);
    if (apiProduct) {
      showPopularSearch = false;
    }
  }
  var productSearch = null;
  var refinements = null;
  var resultsFound = productSuggestions.available || categorySuggestions.available || brandSuggestions.available;
  /**
        a site preference that is a number that defines the number of letters entered to show the number of search results
        0 will not show search results
        any other number will show search results at the number of letter entered
     */
  if (showResultsCountSearchSuggestion && searchTerms && searchTerms.length >= showResultsCountSearchSuggestion.value) {
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var ProductSearchModel = require('dw/catalog/ProductSearchModel');
    var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
    var ProductSearch = require('*/cartridge/models/search/productSearch');
    var refineSearch = require('*/cartridge/models/bopis/refineSearch');

    var apiProductSearch = new ProductSearchModel();
    apiProductSearch = searchHelper.setupSearch(apiProductSearch, req.querystring);
    var viewData = {
      apiProductSearch: apiProductSearch
    };

    res.setViewData(viewData);

    var storeRefineResult = refineSearch.search(apiProductSearch, req.querystring);
    apiProductsearch = storeRefineResult.apiProductsearch; // eslint-disable-line

    productSearch = new ProductSearch(
      apiProductSearch,
      req.querystring,
      req.querystring.srule,
      CatalogMgr.getSortingOptions(),
      apiProductSearch.deepestCommonCategory
    );
    refinements = productSearch.refinements;
  }
  var hasCategorySuggestion = categorySuggestions && categorySuggestions.available && categorySuggestions.categories.length > 0;
  var noProductSuggestion = !(productSuggestions && productSuggestions.available && productSuggestions.products.length > 0);

  res.render('search/suggestions', {
    suggestions: {
      product: productSuggestions,
      category: categorySuggestions,
      content: contentSuggestions,
      recent: recentSuggestions,
      popular: popularSuggestions,
      brand: brandSuggestions,
      searchTerms: searchTerms,
      knownUser: knownUser,
      productSearch: productSearch,
      resultsFound: resultsFound,
      showPopularSearch: showPopularSearch,
      refinements: refinements,
      hasCategorySuggestion: hasCategorySuggestion,
      noProductSuggestion: noProductSuggestion
    }
  });
  next();
});

server.get('GetPopularSuggestions', function (req, res, next) {
  var SuggestModel = require('dw/suggest/SuggestModel');
  var CategorySuggestions = require('*/cartridge/models/search/suggestions/category');
  var ContentSuggestions = require('*/cartridge/models/search/suggestions/content');
  var ProductSuggestions = require('*/cartridge/models/search/suggestions/product');
  var SearchPhraseSuggestions = require('*/cartridge/models/search/suggestions/searchPhrase');
  var Site = require('dw/system/Site');
  var categorySuggestions;
  var productSuggestions;
  var recentSuggestions;
  var popularSuggestions;
  var searchTerms = req.querystring.q;
  var suggestions;
  // TODO: Move minChars and maxSuggestions to Site Preferences when ready for refactoring
  var maxProdSuggestions = 5;
  // Unfortunately, by default, max suggestions is set to 10 and is not configurable in Business
  // Manager.
  var maxSuggestions = 5;

  suggestions = new SuggestModel();
  suggestions.setSearchPhrase(searchTerms);
  suggestions.setMaxSuggestions(maxSuggestions);
  categorySuggestions = new CategorySuggestions(suggestions, maxSuggestions);
  productSuggestions = new ProductSuggestions(suggestions, maxProdSuggestions);
  recentSuggestions = new SearchPhraseSuggestions(suggestions.recentSearchPhrases, maxSuggestions);
  popularSuggestions = new SearchPhraseSuggestions(suggestions.popularSearchPhrases, maxSuggestions, true);
  
  var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
  var knownUser = searchHelper.isKnownUser(req.session);

  var resultsFound = productSuggestions.available || categorySuggestions.available;
  /**
        a site preference that is a number that defines the number of letters entered to show the number of search results
        0 will not show search results
        any other number will show search results at the number of letter entered
     */
  res.render('search/suggestions', {
    suggestions: {
      product: productSuggestions,
      category: categorySuggestions,
      recent: recentSuggestions,
      popular: popularSuggestions,
      searchTerms: searchTerms,
      knownUser: knownUser,
      resultsFound: resultsFound
    }
  });
  next();
});

server.get('GetSearchRecommendations', cache.applyDefaultCache, function (req, res, next) {
  var template = 'components/header/searchRecommendations';
  res.render(template);
  next();
});

module.exports = server.exports();
