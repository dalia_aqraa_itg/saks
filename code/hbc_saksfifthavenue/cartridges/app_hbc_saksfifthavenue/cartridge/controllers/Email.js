'use strict';
var server = require('server');

var HashMap = require('dw/util/HashMap');
var URLUtils = require('dw/web/URLUtils');
var Resource = require('dw/web/Resource');

server.get('Sweepstakes', function (req, res, next) {
  var emailSignUpForm = server.forms.getForm('emailSignUp');
  var emailSignUpAction = URLUtils.url('Email-SweepstakesSubmit').relative().toString();
  emailSignUpForm.clear();
  var currentYear = new Date().getFullYear();
  var birthYears = [];

  for (var j = 0; j <= 100; j++) {
    birthYears.push(currentYear - j);
  }
  res.render('/common/emailSignUp', {
    emailSignUpForm: emailSignUpForm,
    emailSignUpAction: emailSignUpAction,
    birthYears: birthYears
  });
  next();
});

function validateEmail(email) {
  var regex = /^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/;
  return regex.test(email);
}

server.post('SweepstakesSubmit', function (req, res, next) {
  var EmailSubscribeHelper = require('*/cartridge/scripts/helpers/EmailSubscribeHelper');
  var emailSignUpForm = server.forms.getForm('emailSignUp');
  var email = emailSignUpForm.customer.email.value;
  var addToEmailList = emailSignUpForm.customer.addtoemaillist.value;
  var preferences = require('*/cartridge/config/preferences');
  var isValidEmailid;
  var success;
  if (email) {
    isValidEmailid = validateEmail(email);

    if (!isValidEmailid) {
      res.json({
        success: false,
        msg: Resource.msg('emailsignup.Error', 'forms', null)
      });
    } else {
      var hashMap = new HashMap();
      hashMap = EmailSubscribeHelper.getInitialData(req.locale.id);
      hashMap.put('sourceId', 'sweepstakes');
      hashMap.put('saksOptStatus', 'Y');
      if (addToEmailList) {
        hashMap.put('offFiveThOptStatus', 'Y');
      } else {
        hashMap.put('offFiveThOptStatus', 'N');
      }

      hashMap.put('canadaFlag', 'N');
      EmailSubscribeHelper.createSubscriptionFromEmailSignUp(email, hashMap, emailSignUpForm, req.form.emailsignupgender);
      res.json({
        success: true
      });
    }
  } else {
    res.json({
      success: false,
      msg: Resource.msg('emailsignup.Error', 'forms', null)
    });
  }

  return next();
});

module.exports = server.exports();
