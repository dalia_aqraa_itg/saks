'use strict';

var server = require('server');
server.extend(module.superModule);

server.get('LoadLevelZeroCat', function (req, res, next) {
  var shopPreference = 'women';
  if ('shopPreference' in session.custom) {
    // eslint-disable-line
    shopPreference = session.custom.shopPreference; // eslint-disable-line
  }
  res.render('components/header/menWomenMenu', {
    shopPreference: shopPreference
  });
  next();
});

module.exports = server.exports();
