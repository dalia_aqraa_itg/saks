'use strict';

var base = module.superModule;
var URLUtils = require('dw/web/URLUtils');
var collections = require('*/cartridge/scripts/util/collections');
var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
var preferences = require('*/cartridge/config/preferences');
var saksplusHelper = require('*/cartridge/scripts/helpers/saksplusHelper');
var saksFirstHelpers = require('*/cartridge/scripts/helpers/saksFirstHelpers');

/**
 * returns the applied Saks GC object
 *
 * @param {dw.util.Collection} giftCardPaymentInstruments - applied Saks GC payment instruments in current basket
 * @returns {Object} applied gc object
 */
function getSaksGiftCardData(SaksGiftCardPaymentInstruments) {
  var arrayHelper = require('*/cartridge/scripts/util/array');
  var Resource = require('dw/web/Resource');
  var formatMoney = require('dw/util/StringUtils').formatMoney;

  var SaksgiftCardData = [];
  arrayHelper.find(SaksGiftCardPaymentInstruments, function (saksGCPaymentIntr) {
    SaksgiftCardData.push({
      type: Resource.msg('label.order.summary.saks.gift.card', 'checkout', null),
      giftCardAmount: formatMoney(saksGCPaymentIntr.getPaymentTransaction().getAmount())
    });
  });
  return SaksgiftCardData;
}

function getSaksGiftCardData(currentBasket) {
  var saksFirstHelpers = require('*/cartridge/scripts/helpers/saksFirstHelpers');
  var profile = customer.profile;
  var saksFirstData;
  if (
    preferences.saksFirstEnabled &&
    profile != null &&
    profile.customer.authenticated &&
    'saksFirstLinked' in profile.custom &&
    profile.custom.saksFirstLinked
  ) {
    saksFirstData = saksFirstHelpers.saksFirstCheckoutView(profile, currentBasket);
  }
  return saksFirstData;
}

function getSaksGiftCardDataHtml(order) {
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var context = {
    order: order
  };
  var renderedTemplate = renderTemplateHelper.getRenderedHtml(context, 'checkout/checkoutSaksGiftCard');
  return renderedTemplate;
}

function getComxData(basket) {
  try {
    if ('COMXData' in basket.custom && basket.custom.COMXData && !empty(basket.custom.COMXData)) {
      var comxData = JSON.parse(basket.custom.COMXData);
      return comxData;
    }
    return null;
  } catch (e) {
    return null;
  }
}
/**
 * return thr html for order summary payment applied
 *
 * @param {Object} giftCardData - giftcard data
 * @returns {html} return order summary payment html
 */
function getOrderSummaryPaymentHtml(order) {
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var context = {
    order: order,
    currency: session.currency.currencyCode
  };
  var renderedTemplate = renderTemplateHelper.getRenderedHtml(context, 'checkout/orderTotalPaymentMethodsApplied');
  return renderedTemplate;
}

/**
 * Order class that represents the current order
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket/order
 * @param {Object} options - The current order's line items
 * @param {Object} options.config - Object to help configure the orderModel
 * @param {string} options.config.numberOfLineItems - helps determine the number of lineitems needed
 * @param {string} options.countryCode - the current request country code
 * @constructor
 */
function OrderModel(lineItemContainer, options) {
  var Money = require('dw/value/Money');
  var Resource = require('dw/web/Resource');
  var PaymentInstrument = require('dw/order/PaymentInstrument');
  base.call(this, lineItemContainer, options);
  if (preferences.saksFirstEnabled) {
    var SaksGiftCardPaymentInstruments = lineItemContainer.getPaymentInstruments(ipaConstants.SAKS_GIFT_CARD);
    var hasSaksGiftCard = SaksGiftCardPaymentInstruments.length > 0;
    var saksGiftCardData = hasSaksGiftCard ? getSaksGiftCardData(lineItemContainer) : null;
    var saksGCTotal = new Money(0, lineItemContainer.currencyCode);
    collections.forEach(SaksGiftCardPaymentInstruments, function (saksGCPaymentInstr) {
      saksGCTotal = saksGCTotal.add(saksGCPaymentInstr.paymentTransaction.amount);
    });
    var saksFirstData = getSaksGiftCardData(lineItemContainer);
    this.saksFirstData = saksFirstData;
    this.saksFirstCheckoutViewHtml = getSaksGiftCardDataHtml(this);
    this.saksGiftCard = {
      hasSaksGiftCard: hasSaksGiftCard,
      saksGiftCardData: saksGiftCardData,
      amountReached: !(saksGCTotal < lineItemContainer.totalGrossPrice)
    };
  }
  this.ccIntruments = lineItemContainer.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD).length > 0 ? true : false;
  this.COMXData = getComxData(lineItemContainer);
  this.orderSummaryPaymentHtml = getOrderSummaryPaymentHtml(this);
  this.isSaksPlusMember = saksplusHelper.isSaksPlusMember(customer);
  // find is it is a mixed cart
  var shippingHelpers = require('*/cartridge/scripts/checkout/shippingHelpers');
  this.isMixedCart = shippingHelpers.isMixedCart(lineItemContainer);
  if (this.isMixedCart) {
    // Get Sorted Shipments
    var safeOptions = options || {};
    var shippingModels = shippingHelpers.getSortedShippingModels(lineItemContainer, customer, safeOptions.containerView, safeOptions);
    this.shipping = shippingModels;
  }
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  this.orderProductSummary = renderTemplateHelper.getRenderedHtml(
    {
      order: this
    },
    'checkout/orderProductSummary'
  );
  this.isSaksFirstFreeShip = saksFirstHelpers.isSaksFirstFreeShipApplied(customer);
}

module.exports = OrderModel;
