'use strict';

var base = module.superModule;
var AddressModel = require('*/cartridge/models/address');
var ProductLineItemsModel = require('*/cartridge/models/cartProductLineItems');
var ShippingMethodModel = require('*/cartridge/models/shipping/shippingMethod');

/**
 * Loops through all of the product line items and adds new properties.
 * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - All product
 * line items of the basket
 * Updates the prototype with new property, if basket has items with SDD eligibility
 */
function sortedItemsWithShipType(productLineItems) {
  var HashMap = require('dw/util/HashMap');
  var inStoreItems = new HashMap();
  var inStoreItemsJson = {};
  var shitToHomeItems = [];
  var sortedItemsWithShipType = {};
  var StoreMgr = require('dw/catalog/StoreMgr');

  if (productLineItems.items && productLineItems.items.length) {
    productLineItems.items.forEach(function (productLineItem) {
      if (productLineItem.fromStoreId) {
        let storeName = StoreMgr.getStore(productLineItem.fromStoreId).name;
        let storeValue = inStoreItems.get(storeName);
        if (storeValue) {
          storeValue = storeValue.toArray();
          storeValue.push(productLineItem);
          inStoreItems.put(storeName, storeValue);
          inStoreItemsJson[storeName] = storeValue;
        } else {
          let itemArray = [];
          itemArray.push(productLineItem);
          inStoreItems.put(storeName, itemArray);
          inStoreItemsJson[storeName] = itemArray;
        }
      } else {
        shitToHomeItems.push(productLineItem);
      }
    });
  }
  sortedItemsWithShipType.instoreItems = inStoreItems;
  sortedItemsWithShipType.shiptohomeItems = shitToHomeItems;
  sortedItemsWithShipType.inStoreItemsJson = inStoreItemsJson;
  return sortedItemsWithShipType;
}

/**
 * Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * @param {dw.order.Shipment} shipment - the target Shipment
 * @param {string} containerView - the view of the product line items (order or basket)
 * @returns {ProductLineItemsModel} an array of ShippingModels
 */
function getProductLineItemsModel(shipment, containerView) {
  if (!shipment) return null;
  var productLineItems = new ProductLineItemsModel(shipment.productLineItems, containerView);
  productLineItems.items.reverse();
  return productLineItems;
}

/**
 * ppingMethod object
 * @param {dw.order.Shipment} shipment - the target Shipment
 * @returns {string} the Shipment UUID or null
 */
function getShipmentUUID(shipment) {
  if (!shipment) return null;

  return shipment.UUID;
}

/**
 * Returns the matching address ID or UUID for a shipping address
 * @param {dw.order.Shipment} shipment - line items model
 * @param {Object} customer - customer model
 * @return {string|boolean} returns matching ID or false
 */
function getAssociatedAddress(shipment, customer) {
  var address = shipment ? shipment.shippingAddress : null;
  var matchingId;
  var anAddress;

  if (!address) return false;

  // If we still haven't found a match, then loop through customer addresses to find a match
  if (!matchingId && customer && customer.addressBook && customer.addressBook.addresses) {
    for (var j = 0, jj = customer.addressBook.addresses.length; j < jj; j++) {
      anAddress = customer.addressBook.addresses[j];

      if (anAddress && anAddress.isEquivalentAddress(address)) {
        matchingId = anAddress.ID;
        break;
      }
    }
  }

  return matchingId;
}

/**
 * Returns a boolean indicating if the address is empty
 * @param {dw.order.Shipment} shipment - A shipment from the current basket
 * @returns {boolean} a boolean that indicates if the address is empty
 */
function emptyAddress(shipment) {
  if (shipment && shipment.shippingAddress) {
    return ['firstName', 'lastName', 'address1', 'address2', 'phone', 'city', 'postalCode', 'stateCode'].some(function (key) {
      return shipment.shippingAddress[key];
    });
  }
  return false;
}

/**
 * @constructor
 * @classdesc Model that represents shipping information
 *
 * @param {dw.order.Shipment} shipment - the default shipment of the current basket
 * @param {Object} address - the address to use to filter the shipping method list
 * @param {Object} customer - the current customer model
 * @param {string} containerView - the view of the product line items (order or basket)
 */
base.ShippingModel = function (shipment, address, customer, containerView) {
  var shippingHelpers = require('*/cartridge/scripts/checkout/shippingHelpers');
  var Resource = require('dw/web/Resource');
  var preferences = require('*/cartridge/config/preferences');
  var EDDMap;
  if ('EDDResponse' in shipment.custom && !empty(shipment.custom.EDDResponse)) {
    EDDMap = shippingHelpers.getEDDDateMap(JSON.parse(shipment.custom.EDDResponse));
  }

  // Simple properties
  this.UUID = getShipmentUUID(shipment);
  this.raw = shipment;
  // Derived properties
  this.productLineItems = getProductLineItemsModel(shipment, containerView);
  this.applicableShippingMethods = shippingHelpers.getApplicableShippingMethods(shipment, address);
  this.selectedShippingMethod = shippingHelpers.getSelectedShippingMethod(shipment, EDDMap);
  this.matchingAddressId = getAssociatedAddress(shipment, customer);
  this.defaultDropShippingMethod = shippingHelpers.getDefaultDropShippingMethod(this.applicableShippingMethods);
  // Optional properties
  if (emptyAddress(shipment)) {
    var shippingAddress = new AddressModel(shipment.shippingAddress).address;
    // add email address
    shippingAddress.email =
      shipment.shippingAddress && Object.hasOwnProperty.call(shipment.shippingAddress.custom, 'customerEmail')
        ? shipment.shippingAddress.custom.customerEmail
        : null;
    this.shippingAddress = shippingAddress;
  } else {
    this.shippingAddress = address;
  }

  this.isGift = shipment ? shipment.gift : null;
  this.signatureRequired = shipment.custom.signatureRequired;
  this.giftMessage = shipment ? shipment.giftMessage : null;
  this.giftRecipientName = shipment ? shipment.custom.giftRecipientName : null;
  this.shipSignatureRequired = preferences.signatureRequired && this.productLineItems.signatureRequired;
  this.shipSignatureThreshold = preferences.signatureOrderThreshold;
  this.giftWrapType = shipment && shipment.custom.giftWrapType ? shipment.custom.giftWrapType : null;
  this.deliveryInstructions = shipment && shipment.custom.deliveryInstructions ? shipment.custom.deliveryInstructions : null;
  this.sortedItemsWithShipType = sortedItemsWithShipType(this.productLineItems);
  this.minisummaryInstoreLabel = Resource.msg('summary.itemspickedup.label', 'checkout', null);
  this.minisummaryShipLabel = Resource.msg('summary.itemsshipped.label', 'checkout', null);
  this.isComplimentary = 'csrGiftWrap' in session.custom && session.custom.csrGiftWrap;
};

module.exports = base.ShippingModel;
