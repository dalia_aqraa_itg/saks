'use strict';

var base = module.superModule;
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var Resource = require('dw/web/Resource');

/**
 * get EDD date
 *
 * @param {dw.order.ShippingMethod} shippingMethod - the default shipment of the current basket
 * @param {Object} EDD - EDD object
 * @returns {Object} - return EDD date formatted
 */
function getEDDdate(shippingMethod, EDD) {
  if (shippingMethod) {
    if (EDD && EDD.containsKey(shippingMethod.ID)) {
      if (EDD.get(shippingMethod.ID)) {
        var eddDate = EDD.get(shippingMethod.ID).split('-');
        var dareStr = '' + eddDate[1] + '/' + eddDate[2] + '/' + eddDate[0];
        var date = new Date(dareStr);
        var formattedDate = Resource.msg('label.estimated.delivery', 'checkout', null) + ' ' + StringUtils.formatCalendar(new Calendar(date), 'MM/dd/yy');
        return formattedDate;
      }
    } else {
      return shippingMethod && shippingMethod.custom
        ? shippingMethod.custom.hasOwnProperty('sddEnabled') && shippingMethod.custom.sddEnabled === true
          ? shippingMethod.custom.estimatedArrivalTime
          : Resource.msg('label.estimated.delivery', 'checkout', null) + ' ' + shippingMethod.custom.estimatedArrivalTime
        : null;
    }
  }
}

/**
 * @constructor
 * @classdesc ShippingMethod class that represents a single shipping method
 *
 * @param {dw.order.ShippingMethod} shippingMethod - the default shipment of the current basket
 * @param {dw.order.Shipment} [shipment] - a Shipment
 */
function ShippingMethodModel(shippingMethod, shipment, EDD, shippingCostsMap) {
  base.call(this, shippingMethod, shipment, EDD, shippingCostsMap);
  this.sddShippingMethod = shippingMethod.custom && shippingMethod.custom.sddEnabled ? shippingMethod.custom.sddEnabled : false;

  this.estimatedArrivalTime =
    'selectedEstimatedDate' in shipment.custom && !empty(shipment.custom.selectedEstimatedDate)
      ? Resource.msg('label.estimated.delivery', 'checkout', null) + ' ' + shipment.custom.selectedEstimatedDate
      : getEDDdate(shippingMethod, EDD);
  this.saksplusShippingMethod = shippingMethod.custom && shippingMethod.custom.saksplusShippingMethod ? shippingMethod.custom.saksplusShippingMethod : false;
}

module.exports = ShippingMethodModel;
