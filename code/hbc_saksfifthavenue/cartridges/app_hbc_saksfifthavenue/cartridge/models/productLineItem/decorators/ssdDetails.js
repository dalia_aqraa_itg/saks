'use strict';
var preferences = require('*/cartridge/config/preferences');
var storeHelper = require('*/cartridge/scripts/helpers/storeHelpers');
var Resource = require('dw/web/Resource');
var Logger = require('dw/system/Logger');

function prepareSddDetails(product, apiProduct, lineItem) {
  try {
    var productModel = product;
    var store = null;
    var sddDetails = {};
    if (productModel && productModel.isAvailableForSDD && preferences.isEnabledforSameDayDelivery) {
      productModel.desiredQuantity = lineItem.quantityValue;
      store = storeHelper.getDefaultSDDStore(productModel);
      if (store) {
        sddDetails.unitsAvailable = store.unitsAvailable ? store.unitsAvailable : 0;
        sddDetails.cartSDDAvailabilityMessage = store.cartSDDAvailabilityMessage;
        sddDetails.summarySDDAvailabilityMessage =
          lineItem.quantityValue <= sddDetails.unitsAvailable ? Resource.msg('shippingoption.summary.availablesdd', 'product', null) : '';
      }
    }
    return sddDetails;
  } catch (e) {
    Logger.error('Error occured in ssdDetails.js ' + e.message);
  }
}

/**
 * Adds the object with neccessary attribute if item is eligible for SDD
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @returns {Object} - lineItem form the Basket
 */

module.exports = function (product, apiProduct, lineItem) {
  if (!empty(preferences.preOrderEnabled) && preferences.preOrderEnabled === true) {
    Object.defineProperty(product, 'sddDetails', {
      enumerable: true,
      value: prepareSddDetails(product, apiProduct, lineItem)
    });
  }
};
