'use strict';

var base = module.superModule;
var productDecorators = require('*/cartridge/models/product/decorators/index');
var sameDayDeliveryDetails = require('*/cartridge/models/productLineItem/decorators/ssdDetails');

/**
 * Decorate product with product line item information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {Object} options - Options passed in from the factory
 * @returns {Object} - Decorated product model
 */
function orderLineItem(product, apiProduct, options) {
  base.call(this, product, apiProduct, options);
  productDecorators.preorder(product, apiProduct);
  productDecorators.availableForSDD(product, apiProduct);
  sameDayDeliveryDetails(product, apiProduct, options.lineItem);
  return product;
}

module.exports = orderLineItem;
