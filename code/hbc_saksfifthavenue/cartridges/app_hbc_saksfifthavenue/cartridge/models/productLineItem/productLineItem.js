'use strict';

var base = module.superModule;
var productDecorators = require('*/cartridge/models/product/decorators/index');
var sameDayDeliveryDetails = require('*/cartridge/models/productLineItem/decorators/ssdDetails');
/**
 * Decorate product with product line item information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {Object} options - Options passed in from the factory
 * @returns {Object} - Decorated product model
 */
function productLineItem(product, apiProduct, options) {
  base.call(this, product, apiProduct, options);
  productDecorators.preorder(product, apiProduct);
  // shop runner eligible product
  Object.defineProperty(product, 'isShopRunnerEligible', {
    enumerable: true,
    value: apiProduct.custom.hasOwnProperty('shopRunnerEligible') && apiProduct.custom.shopRunnerEligible === 'true' ? 'true' : 'false'
  });
  Object.defineProperty(product, 'relatedBonusProductLineItems', {
    enumerable: true,
    value:
      options.lineItem && options.lineItem.relatedBonusProductLineItems && options.lineItem.relatedBonusProductLineItems.length > 0
        ? options.lineItem.relatedBonusProductLineItems
        : null
  });
  productDecorators.availableForSDD(product, apiProduct);
  sameDayDeliveryDetails(product, apiProduct, options.lineItem);

  return product;
}

module.exports = productLineItem;
