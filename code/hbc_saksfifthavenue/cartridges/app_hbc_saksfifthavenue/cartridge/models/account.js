'use strict';
var base = module.superModule;

var saksFirstHelpers = require('*/cartridge/scripts/helpers/saksFirstHelpers');
var URLUtils = require('dw/web/URLUtils');
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');
var TCCHelpers = require('*/cartridge/scripts/checkout/TCCHelpers');

/**
 * Get current customer's Saks First Information
 *
 * @param {dw/customer/Profile} profile - current customer's profile
 * @returns {Object} customers's preferences object based on its setting
 */
function getSaksFirstInfo(profile) {
  try {
    var saksInfo;
    if ('saksFirstLinked' in profile.custom && profile.custom.saksFirstLinked) {
      saksInfo = saksFirstHelpers.getSaksFirstMemberInfo(profile);
    }
    return saksInfo;
  } catch (e) {
    return null;
  }
}

/**
 * Creates a plain object that contains payment instrument information
 * @param {Object} userPaymentInstruments - current customer's paymentInstruments
 * @returns {Object} object that contains info about the current customer's payment instruments
 */
function getCustomerPaymentInstruments(userPaymentInstruments) {
  var paymentInstruments;

  paymentInstruments = userPaymentInstruments.map(function (paymentInstrument) {
    var ccLength = paymentInstrument.maskedCreditCardNumber.toString().length;
    var result = {
      creditCardHolder: paymentInstrument.creditCardHolder,
      maskedCreditCardNumber: paymentInstrument.maskedCreditCardNumber,
      creditCardType: paymentInstrument.creditCardType,
      creditCardExpirationMonth: paymentInstrument.creditCardExpirationMonth,
      creditCardExpirationYear: paymentInstrument.creditCardExpirationYear,
      creditCardLastFourDigit: paymentInstrument.raw.creditCardNumberLastDigits,
      UUID: paymentInstrument.UUID,
      defaultCreditCard: !!('defaultCreditCard' in paymentInstrument.raw.custom && paymentInstrument.raw.custom.defaultCreditCard),
      creationDate: paymentInstrument.raw.getCreationDate(),
      authorizedCard: !!paymentInstrument.raw.custom.authorizedCard,
      creditCardLength: paymentInstrument.maskedCreditCardNumber.length,
      creditCardToken: paymentInstrument.raw.creditCardToken,
      shortMaskedNumber: paymentInstrument.maskedCreditCardNumber.toString().substr(ccLength - 8, ccLength),
      isCreditCardExpired: paymentInstrument.raw.isCreditCardExpired()
    };

    result.cardTypeImage = {
      src: URLUtils.staticURL('/images/' + paymentInstrument.creditCardType.toLowerCase().replace(/\s/g, '') + '-dark.svg'),
      alt: paymentInstrument.creditCardType
    };

    if (paymentInstrument.creditCardType === 'TCC') {
      // Format TCC expiration date.
      var tccExpirationDate = new Date(paymentInstrument.raw.custom.tccExpirationDate);
      var today = new Date();
      result.tccExpDate = StringUtils.formatCalendar(new Calendar(tccExpirationDate), 'MM/dd/yyyy');
      result.isExpired = tccExpirationDate <= today ? 'tcc-expired' : '';

      // Determine TCC label name and token type (SAKS or SAKSMC).
      var tccNumber = paymentInstrument.raw.getCreditCardToken();
      result.tokenNumberTypeName = TCCHelpers.getTCCTokenType(tccNumber) || 'unknown';
      result.tokenLabel = TCCHelpers.getTCCTokenLabel(tccNumber);
    }

    return result;
  });

  return paymentInstruments;
}

/**
 * Get customer payment DOM HTML
 *
 * @param {dw/customer/Customer} currentCustomer - current customer
 * @returns {string} - rendered isml
 */
function getCustomerPaymentHtml(currentCustomer) {
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var ArrayList = require('dw/util/ArrayList');
  var PropertyComparator = require('dw/util/PropertyComparator');
  var paymentInstruments = getCustomerPaymentInstruments(currentCustomer.wallet.paymentInstruments);
  paymentInstruments = new ArrayList(paymentInstruments);
  paymentInstruments.sort(new PropertyComparator('defaultCreditCard', false));
  var paymentAjaxTemplate = 'account/payment/paymentAjax';

  var paymentContext = {
    paymentInstruments: paymentInstruments,
    actionUrl: URLUtils.url('PaymentInstruments-DeletePayment').toString()
  };

  var paymentHtml = renderTemplateHelper.getRenderedHtml(paymentContext, paymentAjaxTemplate);
  return paymentHtml;
}

/**
 * Account class that represents the current customer's profile dashboard
 * @param {dw.customer.Customer} currentCustomer - Current customer
 * @param {Object} addressModel - The current customer's preferred address
 * @param {Object} orderModel - The current customer's order history
 * @constructor
 */
function account(currentCustomer, addressModel, orderModel) {
  base.call(this, currentCustomer, addressModel, orderModel);
  this.saksfirst = getSaksFirstInfo(currentCustomer.raw.profile);
}

account.getCustomerPaymentInstruments = getCustomerPaymentInstruments;
account.getCustomerPaymentHtml = getCustomerPaymentHtml;

module.exports = account;
