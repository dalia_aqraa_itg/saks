'use strict';
var searchRefinementsFactory = require('*/cartridge/scripts/factories/searchRefinements');
var ProductSortOptions = require('*/cartridge/models/search/productSortOptions');
var urlHelper = require('*/cartridge/scripts/helpers/urlHelpers');
var URLUtils = require('dw/web/URLUtils');
var ProductPagination = require('*/cartridge/scripts/helpers/productPagination');
var productSearchHelper = require('*/cartridge/scripts/helpers/productSearchHelper');
var preferences = require('*/cartridge/config/preferences');
var ACTION_ENDPOINT = 'Search-Show';
var TOTAL_PAGE_SIZE = preferences.totalPageSize ? preferences.totalPageSize : 96;
var LOAD_PAGE_SIZE = preferences.defaultPageSize ? preferences.defaultPageSize : 24;
/**
 * Compile a list of relevant suggested phrases
 *
 * @param {dw.util.Iterator.<dw.suggest.SuggestedPhrase>} suggestedPhrases - Iterator to retrieve suggestedPhrases
 * @return {SuggestedPhrase[]} - Array of suggested phrases
 */
function getPhrases(suggestedPhrases) {
  var phrase = null;
  var phrases = [];

  while (suggestedPhrases.hasNext()) {
    phrase = suggestedPhrases.next();
    phrases.push({
      value: phrase.phrase,
      exactMatch: phrase.exactMatch,
      url: URLUtils.url(ACTION_ENDPOINT, 'q', phrase.phrase, 'ss', 'true')
    });
  }
  return phrases;
}
/**
 * Compile a list of relevant suggested phrases
 *
 * @param {dw.util.Iterator.<dw.suggest.SuggestedPhrase>} suggestedTerms - Iterator to retrieve suggestedPhrases
 * @return {SuggestedTerms[]} - Array of suggested Terms
 */
function getSuggestedTerms(suggestedTerms) {
  var suggestedTerm = null;
  var terms = [];
  while (suggestedTerms.hasNext()) {
    suggestedTerm = suggestedTerms.next();
    var suggestTerm = suggestedTerm ? suggestedTerm.firstTerm : null;
    if (suggestTerm && suggestTerm.additional) {
      terms.push('additional');
    } else if (suggestTerm && suggestTerm.completed) {
      terms.push('completed');
    } else if (suggestTerm && suggestTerm.corrected) {
      terms.push('corrected');
    } else if (suggestTerm && suggestTerm.exactMatch) {
      terms.push('exact match');
    }
  }
  return terms;
}

/**
 * Forms a URL that can be used as a permalink with filters, sort, and page size preserved
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {number} pageSize - 'sz' query param
 * @param {number} startIdx - 'start' query param
 * @return {string} - Permalink URL
 */
function getPermalink(productSearch, pageSize, startIdx) {
  var showMoreEndpoint = 'Search-Show';
  var params = {
    start: '0',
    sz: pageSize + startIdx
  };
  var url = productSearch.url(showMoreEndpoint).toString();
  var appended = urlHelper.appendQueryParams(url, params).toString();
  return appended;
}

/**
 * Validates whether all the refined category is NonShowInRefinementMenu
 * @param {Object} categories refined categories
 * @returns {boolean} returns does all refined category is set as NonShowInRefinementMenu
 */
function isAllNonShowInRefinementMenu(categories, isAlreadySet) {
  var ArrayList = require('dw/util/ArrayList');
  var categoriesList = new ArrayList(categories).iterator();
  var isShowInRefinement = typeof isAlreadySet === 'undefined' ? true : isAlreadySet;
  while (categoriesList.hasNext()) {
    // break the loop if already set
    if (!isShowInRefinement) {
      break;
    }
    var category = categoriesList.next();
    if (category.showInRefinementMenu) {
      isShowInRefinement = false;
    } else if (category.subCategories) {
      isShowInRefinement = isAllNonShowInRefinementMenu(category.subCategories, isShowInRefinement);
    }
  }
  return isShowInRefinement;
}

/**
 * Retrieves search refinements
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {dw.catalog.ProductSearchRefinements} refinements - Search refinements
 * @param {ArrayList.<dw.catalog.ProductSearchRefinementDefinition>} refinementDefinitions - List of product serach refinement definitions
 * @param {Object} httpParams - Http parameter query string object
 * @return {Refinement[]} - List of parsed refinements
 */
function getRefinements(productSearch, refinements, refinementDefinitions, httpParams) {
  var collections = require('*/cartridge/scripts/util/collections');
  var designerFilter = preferences.designerFilters;
  var params = httpParams;
  return collections.map(refinementDefinitions, function (definition) {
    let refinementValues;
    if (definition.categoryRefinement && productSearch.categorySearch) {
      refinementValues = refinements.getNextLevelCategoryRefinementValues(productSearch.category);
    } else {
      refinementValues = refinements.getAllRefinementValues(definition);
    }
    var values = searchRefinementsFactory.get(productSearch, definition, refinementValues, httpParams);
    let hideRefinement = false;
    if (
      (definition.categoryRefinement && refinementValues.length === 1 && !values[0].showInRefinementMenu) ||
      (httpParams.cgid && httpParams.cgid === 'brand' && definition.attributeID === 'brand')
    ) {
      hideRefinement = true;
    }
    let selectedCount = 0;
    values.forEach(function (value) {
      if (value.selected) {
        selectedCount++;
      }
    });
    return {
      addSearchBar: designerFilter.indexOf(definition.attributeID) > -1,
      isDesigner: designerFilter.indexOf(definition.attributeID) > -1,
      displayName: definition.displayName,
      attributeID: definition.attributeID,
      colorRefinement: definition.attributeID === 'colorRefinement',
      isCategoryRefinement: definition.categoryRefinement,
      isAttributeRefinement: definition.attributeRefinement,
      isPriceRefinement: definition.priceRefinement,
      isPromotionRefinement: definition.promotionRefinement,
      hideRefinement: hideRefinement,
      allNonShowInRefinementMenu: definition.categoryRefinement ? isAllNonShowInRefinementMenu(values) : false,
      values: values,
      selectedCount: selectedCount
    };
  });
}
/**
 * Configures and returns a PagingModel instance
 *
 * @param {dw.util.Iterator} productHits - Iterator for product search results
 * @param {number} count - Number of products in search results
 * @param {number} pageSize - Number of products to display
 * @param {number} startIndex - Beginning index value
 * @return {dw.web.PagingModel} - PagingModel instance
 */
function getPagingModel(productHits, count, pageSize, startIndex) {
  var PagingModel = require('dw/web/PagingModel');
  var paging = new PagingModel(productHits, count);

  paging.setStart(startIndex || 0);
  paging.setPageSize(pageSize || LOAD_PAGE_SIZE);

  return paging;
}

/**
 * Adds the store id to the url if found in query
 *
 * @param {Object} req - Query params
 * @param {dw.web.URL} actionUrl - to set query to original search
 * @return {storeRefineUrl} - final url
 */
function addStoreRefinementToUrl(req, actionUrl) {
  var storeRefineUrl = actionUrl;
  var whitelistedParams = ['storeid'];

  Object.keys(req).forEach(function (element) {
    if (whitelistedParams.indexOf(element) > -1) {
      storeRefineUrl.append(element, req[element]);
    }
  });

  return storeRefineUrl;
}

/**
 * Generates URL for [Show] Less button
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {Object} httpParams - HTTP query parameters
 * @return {string} - More button URL
 */
function getShowPreviousURL(productSearch, httpParams) {
  var showLessEndPoint = 'Search-UpdateGrid';
  var currentStart = httpParams.start - LOAD_PAGE_SIZE || 0;
  var hitsCount = productSearch.count;

  var paging = getPagingModel(productSearch.productSearchHits, hitsCount, httpParams.sz || LOAD_PAGE_SIZE, currentStart);

  var baseUrl = productSearch.url(showLessEndPoint);
  // add store id to url
  baseUrl = addStoreRefinementToUrl(httpParams, baseUrl);
  var finalUrl = paging.appendPaging(baseUrl);
  if (httpParams.filter) {
    finalUrl = finalUrl.append('filter', true);
  }
  return finalUrl;
}

/**
 * Generates URL that removes refinements, essentially resetting search criteria
 *
 * @param {dw.catalog.ProductSearchModel} search - Product search object
 * @param {Object} httpParams - Query params
 * @return {string} - URL to reset query to original search
 */
function getResetLink(search, httpParams) {
  return search.categorySearch ? URLUtils.url(ACTION_ENDPOINT, 'cgid', httpParams.cgid) : URLUtils.url(ACTION_ENDPOINT, 'q', httpParams.q);
}

/**
 * Returns the refinement values that have been selected
 *
 * @param {Array.<CategoryRefinementValue|AttributeRefinementValue|PriceRefinementValue>}
 *     refinements - List of all relevant refinements for this search
 * @param {string} type - type of selected filter
 * @return {Object[]} - List of selected filters
 */
function getSelectedFilters(refinements) {
  var selectedFilters = [];
  var selectedValues = [];

  refinements.forEach(function (refinement) {
    if (!refinement.isCategoryRefinement) {
      selectedValues = refinement.values.filter(function (value) {
        return value.selected;
      });
    }
    if (selectedValues.length) {
      selectedFilters.push.apply(selectedFilters, selectedValues);
    }
  });

  return selectedFilters;
}

/**
 * Generates URL for [Show] More button
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {Object} httpParams - HTTP query parameters
 * @param {number} pgSize - page size
 * @return {string} - More button URL
 */
function getShowMoreUrl(productSearch, httpParams, pgSize) {
  var showMoreEndpoint = 'Search-UpdateGrid';
  var currentStart = httpParams.pageStart || httpParams.start || 0;
  var pageSize = pgSize || httpParams.sz || LOAD_PAGE_SIZE;
  var hitsCount = productSearch.count;

  var paging = getPagingModel(productSearch.productSearchHits, hitsCount, LOAD_PAGE_SIZE, currentStart);

  currentStart = Math.ceil(Number(currentStart) / Number(pageSize)) * pageSize;
  paging.setStart(Number(currentStart) + Number(pageSize));

  var baseUrl = productSearch.url(showMoreEndpoint);
  // Append SDD Filters
  if (httpParams.srchsrc && httpParams.storeid) {
    baseUrl.append('srchsrc', httpParams.srchsrc);
  }
  // add store id to url
  baseUrl = addStoreRefinementToUrl(httpParams, baseUrl);
  var finalUrl = paging.appendPaging(baseUrl);
  if (httpParams.filter) {
    finalUrl = finalUrl.append('filter', true);
  }
  return finalUrl;
}
/**
 * Retrieves banner image URL
 *
 * @param {dw.catalog.Category} category - Subject category
 * @return {string} - Banner's image URL
 */
function getBannerImageUrl(category) {
  var url = null;

  if (category.custom && 'slotBannerImage' in category.custom && category.custom.slotBannerImage) {
    url = category.custom.slotBannerImage;
  } else if (category.image) {
    url = category.image.getURL();
  }

  return url;
}

function checkRefinedSearch(productSearch) {
  return productSearch.refinedSearch && (productSearch.refinedByAttribute || productSearch.refinedByPrice || productSearch.refinedByPromotion);
}

function getCategoryLevel(category) {
  var catLevel = 0;
  var tCat = category;
  while (tCat && !tCat.root) {
    catLevel++;
    tCat = tCat.parent;
  }
  return catLevel;
}

/**
 * @constructor
 * @classdesc ProductSearch class
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {Object} httpParams - HTTP query parameters
 * @param {string} sortingRule - Sorting option rule ID
 * @param {dw.util.ArrayList.<dw.catalog.SortingOption>} sortingOptions - Options to sort search
 *     results
 * @param {dw.catalog.Category} rootCategory - Search result's root category if applicable
 */
function ProductSearch(productSearch, httpParams, sortingRule, sortingOptions, rootCategory, filterSearch) {
  var startIdx = httpParams.start || 0;
  let isRefinedSearch = checkRefinedSearch(productSearch);
  this.pageSize =
    !httpParams.filter && !isRefinedSearch
      ? productSearchHelper.getPageSize(productSearch, httpParams, startIdx)
      : parseInt(httpParams.sz, 10) || LOAD_PAGE_SIZE;
  this.productSearch = productSearch;

  var paging = getPagingModel(productSearch.productSearchHits, productSearch.count, this.pageSize, startIdx);

  var searchSuggestions = productSearch.searchPhraseSuggestions;
  this.isSearchSuggestionsAvailable = searchSuggestions ? searchSuggestions.hasSuggestedPhrases() : false;

  if (this.isSearchSuggestionsAvailable) {
    this.suggestionPhrases = getPhrases(searchSuggestions.suggestedPhrases);
    this.suggestedTerms = getSuggestedTerms(searchSuggestions.suggestedTerms);
  }

  this.pageNumber = paging.currentPage;
  this.count = productSearch.count;
  this.isRefinedSearch = productSearch.refinedSearch;
  this.isCategorySearch = productSearch.categorySearch;
  this.isRefinedCategorySearch = productSearch.refinedCategorySearch;
  this.refinedByCategory = productSearch.refinedByCategory;
  this.searchKeywords = productSearch.searchPhrase;
  this.cgid = httpParams.cgid || '';
  this.resetLink = getResetLink(productSearch, httpParams);
  this.bannerImageUrl = productSearch.category ? getBannerImageUrl(productSearch.category) : null;
  var productGrid = productSearchHelper.getProductGridObject(paging.pageElements, productSearch, startIdx, httpParams, isRefinedSearch, filterSearch);
  this.productIds = productGrid.productGridData;
  this.isPreOrderSearch = productGrid.isPreOrderSearch;

  // SFDEV-8522 | creating relax url for category refinements for o5 search | Never change
  let relaxedURL = '';
  if (productSearch.refinedByCategory) {
    if (productSearch.category && productSearch.category.parent != null && productSearch.category.parent.root) {
      relaxedURL = this.resetLink;
    } else if (productSearch.category && productSearch.category.parent) {
      relaxedURL = productSearch.urlRefineCategory(ACTION_ENDPOINT, productSearch.category.parent.ID).relative().toString();
    }
  }
  this.relaxedURL = relaxedURL;

  this.productSort = new ProductSortOptions(productSearch, sortingRule, sortingOptions, rootCategory, paging);
  this.showMoreUrl = getShowMoreUrl(productSearch, httpParams, this.pageSize);
  this.permalink = getPermalink(productSearch, parseInt(this.pageSize, 10), parseInt(startIdx, 10));

  if (productSearch.category) {
    this.category = {
      name: productSearch.category.displayName,
      categoryNameOverwrite:
        !empty(productSearch.category) && 'categoryh1title' in productSearch.category.custom && productSearch.category.custom.categoryh1title
          ? productSearch.category.custom.categoryh1title
          : productSearch.category.custom && 'categoryNameOverwrite' in productSearch.category.custom
          ? productSearch.category.custom.categoryNameoverwrite
          : productSearch.category.displayName,
      id: productSearch.category.ID,
      pageTitle: productSearch.category.pageTitle,
      pageDescription: productSearch.category.pageDescription,
      pageKeywords: productSearch.category.pageKeywords,
      template: productSearch.category.template,
      hidePromotray:
        'hidePromotray' in productSearch.category.custom && productSearch.category.custom.hidePromotray ? productSearch.category.custom.hidePromotray : false,
      removeCategorytitle: 'removeCategorytitle' in productSearch.category.custom && productSearch.category.custom.removeCategorytitle,
      showModelViewToggle: 'showModelViewToggle' in productSearch.category.custom && productSearch.category.custom.showModelViewToggle,
      hideSortBy: 'hideSortBy' in productSearch.category.custom && productSearch.category.custom.hideSortBy
    };
  }
  this.pageMetaTags = productSearch.pageMetaTags;
  this.loadPageSize = LOAD_PAGE_SIZE;

  // current main page number of pagination
  var currentPage = Math.floor(this.pageNumber / Math.ceil(TOTAL_PAGE_SIZE / LOAD_PAGE_SIZE));
  this.paginationUrls = ProductPagination.getPaginationUrls(productSearch, currentPage, productSearch.category, httpParams);

  var maxPerPageLoad = Math.ceil(TOTAL_PAGE_SIZE / LOAD_PAGE_SIZE);
  var showLazyLoading;
  if (this.paginationUrls.currentPage < this.paginationUrls.totalPages) {
    if (this.pageNumber + 1 < this.paginationUrls.currentPage * maxPerPageLoad) {
      showLazyLoading = true;
    } else {
      showLazyLoading = false;
    }
  } else {
    var productCount = httpParams.start ? this.count - httpParams.start : this.count;
    if (productCount <= LOAD_PAGE_SIZE) {
      showLazyLoading = false;
    } else {
      showLazyLoading = true;
    }
  }
  this.showLazyLoading = showLazyLoading;
  this.showPreviousURL = getShowPreviousURL(productSearch, httpParams);
  this.startPageSize = this.pageNumber * this.pageSize;
  this.type = httpParams.type;
  this.httpParams = httpParams;
  this.catLevel = getCategoryLevel(productSearch.category);
}

Object.defineProperty(ProductSearch.prototype, 'refinements', {
  get: function () {
    if (!this.cachedRefinements) {
      this.cachedRefinements = getRefinements(
        this.productSearch,
        this.productSearch.refinements,
        this.productSearch.refinements.refinementDefinitions,
        this.httpParams
      );
    }

    return this.cachedRefinements;
  }
});

Object.defineProperty(ProductSearch.prototype, 'selectedFilters', {
  get: function () {
    return getSelectedFilters(this.refinements);
  }
});

module.exports = ProductSearch;
