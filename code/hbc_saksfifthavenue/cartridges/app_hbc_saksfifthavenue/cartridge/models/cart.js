'use strict';
var base = module.superModule;
var preferences = require('*/cartridge/config/preferences');

function sortPreOrderCartItem(cart) {
  try {
    var sortedCartItem = {};
    var regularItem = [];
    var preOrderItem = [];
    if (cart.items && cart.items.length > 0) {
      for (var i = 0; i < cart.items.length; i++) {
        var item = cart.items[i];
        if (!empty(item.preOrder) && !empty(item.preOrder.applicable) && item.preOrder.applicable == true) {
          preOrderItem.push(item);
        } else {
          regularItem.push(item);
        }
      }
    }
    if (regularItem.length > 0 && preOrderItem.length > 0) {
      sortedCartItem.regularItem = regularItem;
      sortedCartItem.preorderItems = preOrderItem;
      return sortedCartItem;
    }
    return null;
  } catch (e) {
    Logger.error('There was an error with SAKS Pre Order Model');
  }
}


if (!preferences.CartBEPerformanceChangesEnable) {
  function preOrderCartItem(cart) {
    try {
      var preOrderItem = [];
      if (cart.items && cart.items.length > 0) {
        for (var i = 0; i < cart.items.length; i++) {
          var item = cart.items[i];
          if (!empty(item.preOrder) && !empty(item.preOrder.applicable) && item.preOrder.applicable == true) {
            preOrderItem.push(item);
          }
        }
      }
      return preOrderItem;
    } catch (e) {
      Logger.error('There was an error with SAKS Pre Order Model');
    }
  }
}

/**
 * @constructor
 * @classdesc CartModel class that represents the current basket
 * @param {dw.order.Basket} basket - Current users's basket
 */
function CartModel(basket, displayHtml) {
  var preferences = require('*/cartridge/config/preferences');
  var bopisHelper = require('*/cartridge/scripts/helpers/bopisHelpers');
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var shopRunnerEnabled = require('*/cartridge/scripts/helpers/shopRunnerHelpers').checkSRExpressCheckoutEligibility(basket);
  var displayHtml = displayHtml === false ? false : true;

  base.call(this, basket, displayHtml);
  this.sortedCartItem = sortPreOrderCartItem(this);
  this.shopRunnerEnabled = shopRunnerEnabled;
  if (!preferences.CartBEPerformanceChangesEnable) {
    this.cartPreOrderItem = preOrderCartItem(this);
  }

  if (preferences.isBopisEnabled) {
    this.storeInfo = bopisHelper.getDefaultStore();
  }

  if (!this.numItems || this.numItems === 0) {
    if (displayHtml || !preferences.CartBEPerformanceChangesEnable) {
      this.emptyCartHtml = renderTemplateHelper.getRenderedHtml(this, 'cart/emptyCart');
      this.itemsHTML = renderTemplateHelper.getRenderedHtml(this, 'cart/emptyCart');
    }
    this.valid = { error: false, message: null };
  } else {
    if (displayHtml || !preferences.CartBEPerformanceChangesEnable) {
      this.itemsHTML = renderTemplateHelper.getRenderedHtml(this, 'cart/cartItems');
      this.minicart = renderTemplateHelper.getRenderedHtml(this, 'checkout/cart/miniCart');
      if (!preferences.CartBEPerformanceChangesEnable) {
        var OrderModel = require('*/cartridge/models/order');
        var orderModel = new OrderModel(basket, { containerView: 'basket' });
        this.orderProductSummary = renderTemplateHelper.getRenderedHtml(
          {
            order: orderModel
          },
          'checkout/orderProductSummary'
        );
      }
    }
  }

}

module.exports = CartModel;
