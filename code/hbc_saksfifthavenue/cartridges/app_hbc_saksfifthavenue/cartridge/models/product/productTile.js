'use strict';
var base = module.superModule;
var decorators = require('*/cartridge/models/product/decorators/index');
var PromotionMgr = require('dw/campaign/PromotionMgr');
var Promotion = require('dw/campaign/Promotion');
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');

function getProductTileSwatchHtml(product) {
  var context = new HashMap();
  var object = { product: product };

  Object.keys(object).forEach(function (key) {
    context.put(key, object[key]);
  });

  var template = new Template('product/components/productTileSwatch');
  return template.render(context).text;
}

/**
 * Decorate product with product tile information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {string} productType - Product type information
 * @param {string} frpid - First represented product ID
 *
 * @returns {Object} - Decorated product model
 */
function productTile(product, vApiProduct, productType, frpid, apiOptions) {
  base.call(this, product, vApiProduct, productType, frpid, apiOptions);
  var apiProduct = apiOptions.apiProduct ? apiOptions.apiProduct : vApiProduct;
  var firstPromotion = null;

  firstPromotion = PromotionMgr.activeCustomerPromotions.getProductPromotions(apiProduct);
  if (firstPromotion && firstPromotion.length > 0) {
    for (var i = 0; i < firstPromotion.length; i++) {
      if (firstPromotion[i].getPromotionClass() === Promotion.PROMOTION_CLASS_PRODUCT) {
        product.plpPromos = firstPromotion[i];
        break;
      }
    }
  }
  decorators.preorder(product, vApiProduct, 'tile', frpid);
  // identify if the product has the hi-res-model image
  var hasModelImages = false;
  if (product && product.images && product.images.medium) {
    product.images.medium.forEach(function (image) {
      if (image && image.hiresModelURL) {
        hasModelImages = true;
      }
    });
  }
  product.hasModelImages = hasModelImages; // eslint-disable-line
  product.productTileSwatch = getProductTileSwatchHtml(product);
  return product;
}

module.exports = productTile;
