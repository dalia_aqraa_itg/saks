'use strict';

var base = module.superModule;

base.preorder = require('*/cartridge/models/product/decorators/preorder');

//include product eligibility for sdd
base.availableForSDD = require('*/cartridge/models/product/decorators/availableForSDD');

module.exports = base;
