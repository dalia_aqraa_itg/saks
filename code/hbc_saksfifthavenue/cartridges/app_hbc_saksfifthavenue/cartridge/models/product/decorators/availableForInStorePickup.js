'use strict';

/**
 * Get availability for in store pickup
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {dw.catalog.ProductVariationModel} variationModel - The product's variation model
 * @returns {boolean} - if selected variant product is available return selected variant
 *    availability otherwise return master product availability
 */
function getAvailableForInStorePickup(apiProduct, variationModel) {
    var isAvailableForInstore = false;
    var actualProduct = apiProduct;
    var getItFast = null;
    if (actualProduct) {
    	getItFast = actualProduct.custom.hasOwnProperty('getItFast') && actualProduct.custom.getItFast ? actualProduct.custom.getItFast : null;
    	if (getItFast && getItFast.length > 0) {
    		for each (let eligibility in getItFast) {
    			if (eligibility.value === 'bopis') {
    				isAvailableForInstore = true;
    			}
    		}
    	}
    	
    }
    return isAvailableForInstore;
}


module.exports = function (object, apiProduct, variationModel) {
    Object.defineProperty(object, 'isAvailableForInstore', {
        enumerable: true,
        value: getAvailableForInStorePickup(apiProduct, variationModel)
    });
};
