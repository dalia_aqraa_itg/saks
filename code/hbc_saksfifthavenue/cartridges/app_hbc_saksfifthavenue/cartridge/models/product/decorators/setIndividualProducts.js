'use strict';

var collection = require('*/cartridge/scripts/util/collections');

function getSetProductTemplate(hbcProductType) {
  var template = 'product/productDetails';
  switch (hbcProductType) {
    case 'gwp':
      return 'product/components/productset/productDetails_gwp';
    case 'giftcard':
      return 'product/components/productset/productDetails_giftcard';
    case 'chanel':
      return 'product/components/productset/productDetails_chanel';
    case 'CSRonly':
      return 'product/components/productset/productDetails_click2Order';
    case 'CSRstores':
      return 'product/components/productset/productDetails_click2Chanel';
    default:
      return 'product/components/productset/setStandard';
  }
}

/**
 *
 * @param {dw.catalog.Product} apiProduct - Product returned by the API
 * @param {Object} factory - Product Factory object
 *
 * @returns {Array<Object>} - Array of sub-product models
 */
function getIndividualProducts(apiProduct, factory) {
  return collection.map(apiProduct.bundledProducts, function (product) {
    var setProduct = factory.get({ pid: product.ID });
    if (product.online) {
      Object.defineProperty(setProduct, 'indTemplate', {
        enumerable: true,
        value: setProduct.template ? setProduct.template : getSetProductTemplate(setProduct.hbcProductType)
      });
      return setProduct;
    }
  });
}

module.exports = function (object, apiProduct, factory) {
  Object.defineProperty(object, 'individualProducts', {
    enumerable: true,
    value: getIndividualProducts(apiProduct, factory)
  });
};
