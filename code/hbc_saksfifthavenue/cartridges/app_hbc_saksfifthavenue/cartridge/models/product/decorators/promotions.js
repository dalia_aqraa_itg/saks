'use strict';
var collections = require('*/cartridge/scripts/util/collections');
var preferences = require('*/cartridge/config/preferences');
module.exports = function (object, promotions) {
  Object.defineProperty(object, 'promotions', {
    enumerable: true,
    value:
      promotions.length === 0
        ? null
        : collections.map(promotions, function (promotion) {
            return {
              calloutMsg: promotion.calloutMsg ? promotion.calloutMsg.markup : '',
              details: promotion.details ? promotion.details.markup : '',
              enabled: promotion.enabled,
              id: promotion.ID,
              name: promotion.name,
              promotionClass: promotion.promotionClass,
              rank: promotion.rank,
              hexColorCode: JSON.parse(preferences.product.BADGE_HEX_COLOR) ? JSON.parse(preferences.product.BADGE_HEX_COLOR).badgePip : null,
              discountAppliedInCheckout: 'discountAppliedAtCheckout' in promotion.custom ? promotion.custom.discountAppliedAtCheckout : null,
              customType: promotion.custom.hasOwnProperty('promotionType') && promotion.custom.promotionType ? promotion.custom.promotionType : null
            };
          })
  });
};
