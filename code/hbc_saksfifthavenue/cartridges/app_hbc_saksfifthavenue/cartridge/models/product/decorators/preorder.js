'use strict';
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');
var Logger = require('dw/system/Logger');
var Resource = require('dw/web/Resource');
var Calendar = require('dw/util/Calendar');
var preferences = require('*/cartridge/config/preferences');
var preorderHelper = require('*/cartridge/scripts/helpers/preorderHelper');
var ProductMgr = require('dw/catalog/ProductMgr');

function paddingPreOrderShippingDate(calendar) {
  try {
    var backinStockPaddinginDays = preferences.backinStockPaddinginDays;
    if (backinStockPaddinginDays) {
      calendar.add(Calendar.DAY_OF_MONTH, backinStockPaddinginDays);
    }
    return calendar;
  } catch (e) {
    Logger.error('There was an error while calculating the lastest preorder ship date: ' + e.message);
  }
}

function getMasterShipDate(product) {
  try {
    var latestDate;
    var variants = product.variationModel.variants;
    if (variants.length > 0) {
      for (var i = 0; i < variants.length; i++) {
        var variant = variants[i];
        var availabilityModel = variant.availabilityModel;
        if (availabilityModel && !empty(availabilityModel)) {
          var inventoryRecord = availabilityModel.inventoryRecord;
          if (!empty(inventoryRecord) && inventoryRecord.getStockLevel() > 0) {
            latestDate = '';
            break;
          }
          if (
            !empty(inventoryRecord) &&
            (inventoryRecord.preorderable || inventoryRecord.backorderable) &&
            inventoryRecord.preorderBackorderAllocation.value > 0 &&
            !empty(inventoryRecord.inStockDate)
          ) {
            if (!latestDate) {
              latestDate = new Calendar(inventoryRecord.inStockDate);
            } else {
              var inStockDate = new Calendar(inventoryRecord.inStockDate);
              if (inStockDate.compareTo(latestDate) <= 0) {
                latestDate = inStockDate;
              }
            }
          }
        }
      }
    }

    if (latestDate) {
      latestDate = paddingPreOrderShippingDate(latestDate);
    }
    return latestDate;
  } catch (e) {
    Logger.error('There was an error while calculating the lastest preorder ship date: ' + e.message);
  }
}

function checkPreOrderAvailability(product, preOrder) {
  var isPreOrderProduct = preorderHelper.isPreorder(product);
  var availabilityModel = product.availabilityModel;
  if (isPreOrderProduct && availabilityModel && !empty(availabilityModel)) {
    var inventoryRecord = availabilityModel.inventoryRecord;
    if (!empty(inventoryRecord.inStockDate)) {
      var currentTime = new Calendar();
      var inStockTime = new Calendar(inventoryRecord.inStockDate);
      if (inStockTime.compareTo(currentTime) >= 0) {
        // Padding inShip Date
        inStockTime = paddingPreOrderShippingDate(inStockTime);
        var shipDate = StringUtils.formatCalendar(inStockTime, 'MM/dd/yyyy');
        preOrder.applicable = true;
        preOrder.shipDate = Resource.msgf('msg.preorder.ship.date', 'product', null, shipDate);
        preOrder.shipDateRaw = shipDate;
      }
    }
  }
}

function defineProductPreOrder(apiProduct, view, frpid) {
  try {
    var preOrder = {};
    preOrder.preorderButtonName = Resource.msg('product.tile.preordertobag', 'product', null);
    if (apiProduct && apiProduct.master) {
      // If Master Product and All variants are preorderable set, display badges.
      preOrder.applicable = 'sfccPreorder' in apiProduct.custom && apiProduct.custom.sfccPreorder === 'T' ? true : false;
      if (!preOrder.applicable && frpid) {
        var firstVariant = ProductMgr.getProduct(frpid);
        if (firstVariant) {
          if ('sfccPreorder' in firstVariant.custom && firstVariant.custom.sfccPreorder === 'T') {
            checkPreOrderAvailability(firstVariant, preOrder);
          }
        }
      }
      
      if (view && view === 'pdp') {
        var latestShipDate = getMasterShipDate(apiProduct);
        if (latestShipDate && !empty(latestShipDate)) {
          latestShipDateFormatted = StringUtils.formatCalendar(latestShipDate, 'MM/dd/yyyy');
          preOrder.latestShipDate = Resource.msgf('msg.preorder.ship.date', 'product', null, latestShipDateFormatted);
          preOrder.latestShipDateRaw = latestShipDateFormatted;
        }
      }
    } else {
      // For other types of product, check sfccoreorder and inventory record.
      if ('sfccPreorder' in apiProduct.custom && apiProduct.custom.sfccPreorder === 'T') {
        checkPreOrderAvailability(apiProduct, preOrder);
      }
    }
    return preOrder;
  } catch (e) {
    var a = e;
    Logger.error('There was an error with SAKS Pre Order Model: ' + e.message);
  }
}

/**
 * Determine if the product is available for preOrder.
 * If the Product is master and PreOrder Job set the sfccPreorder as T, which means all variation are preorderable
 * For Other products, if sfccPreorder is set, display badges on PA
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @returns {Object} - Decorated product model
 */

module.exports = function (product, apiProduct, view, frpid) {
  if (!empty(preferences.preOrderEnabled) && preferences.preOrderEnabled === true) {
    Object.defineProperty(product, 'preOrder', {
      enumerable: true,
      value: defineProductPreOrder(apiProduct, view, frpid)
    });
  }
};
