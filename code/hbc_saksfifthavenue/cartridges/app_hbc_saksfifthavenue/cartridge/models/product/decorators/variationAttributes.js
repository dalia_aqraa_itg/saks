'use strict';

var VariationAttributesModel = require('*/cartridge/models/product/productAttributes');

/**
 * Get Product Variation Attributes
 * @param {Object} object product object
 * @param {dw.catalog.ProductVariationModel} variationModel - A product's variation model
 * @param {Object} config config
 * @returns {Object} Variation attributes model
 */
function getVariationAttributes(object, variationModel, config) {
  var variationAttributesObject = {
    variationAttributes: [],
    attrIDValues: ''
  };
  if (variationModel) {
    variationAttributesObject = new VariationAttributesModel(variationModel, config, config.selectedOptionsQueryParams, object.selectedQuantity, object);
  }
  return variationAttributesObject;
}

module.exports = function (object, variationModel, config) {
  var attrConfig = !empty(config) ? config : {};
  var variationAttributesObject = getVariationAttributes(object, variationModel, attrConfig);
  Object.defineProperty(object, 'variationAttributes', {
    enumerable: true,
    value: variationAttributesObject.variationAttributes.slice(0)
  });

  Object.defineProperty(object, 'attrIDValues', {
    enumerable: true,
    value: variationAttributesObject.attrIDValues
  });
};
