'use strict';

/**
 * Check if any of the product promotion is of PWP type
 */
function isEligibleForPWP(product) {
  var hasPWPPromotion = false;
  if (product.promotions && product.promotions.length > 0) {
    for (var i = 0; i < product.promotions.length; i++) {
      var eachPromotion = product.promotions[i];
      if (eachPromotion.customType && eachPromotion.customType === 'PWP') {
        hasPWPPromotion = true;
      }
    }
  }
  return hasPWPPromotion;
}

module.exports = function (product) {
  Object.defineProperty(object, 'isEligibleForPWP', {
    enumerable: true,
    value: isEligibleForPWP(product)
  });
};
