'use strict';

/**
 * Get availability for in store pickup
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {dw.catalog.ProductVariationModel} variationModel - The product's variation model
 * @returns {boolean} - if selected variant product is available return selected variant
 *    availability otherwise return master product availability
 */
function getAvailableForSDD(apiProduct) {
    var isAvailableForSDD = false;
    var getItFast = null;
    if (apiProduct) {
    	getItFast = apiProduct.custom.hasOwnProperty('getItFast') && apiProduct.custom.getItFast ? apiProduct.custom.getItFast : null;
    	if (getItFast && getItFast.length > 0) {
    		for each (let eligibility in getItFast) {
    			if (eligibility.value === 'sdd') {
    				isAvailableForSDD = true;
    			}
    		}
    	}
    	
    }
    return isAvailableForSDD;
}


module.exports = function (object, apiProduct) {
    Object.defineProperty(object, 'isAvailableForSDD', {
        enumerable: true,
        value: getAvailableForSDD(apiProduct)
    });
};
