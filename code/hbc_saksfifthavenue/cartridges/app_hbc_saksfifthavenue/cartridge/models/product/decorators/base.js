'use strict';
var base = module.superModule;
var collections = require('*/cartridge/scripts/util/collections');
var preferences = require('*/cartridge/config/preferences');

function fullLookRecommendation(apiProduct) {
  var recommendations = [];
  var masterProduct;
  if (apiProduct.orderableRecommendations && apiProduct.orderableRecommendations.length > 0) {
    masterProduct = apiProduct;
  } else {
    masterProduct = apiProduct.variant ? apiProduct.variationModel.master : apiProduct;
  }

  if (masterProduct && masterProduct.orderableRecommendations) {
    collections.forEach(masterProduct.orderableRecommendations, function (recommendation) {
      if (recommendation) {
        var params = {
          id: recommendation.recommendedItemID
        };
        recommendations.push(params);
      }
    });
  }
  return recommendations;
}

function recommendationStrips(apiProduct) {
  var Calendar = require('dw/util/Calendar');
  if ('isNew' in apiProduct.custom && apiProduct.custom.isNew === 'true') {
    if ('publishedDate' in apiProduct.custom && !empty(apiProduct.custom['publishedDate'])) {
      var publishedDate = new Calendar(apiProduct.custom['publishedDate']);
      var todayMinusStripDays = new Calendar();
      var recommendationStripsDays = preferences.recommendationStripsDays;
      todayMinusStripDays.add(Calendar.DAY_OF_YEAR, -1 * recommendationStripsDays);
      if (publishedDate.after(todayMinusStripDays)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  } else {
    return false;
  }
}

module.exports = function (object, apiProduct, type) {
  base.call(this, object, apiProduct, type);

  Object.defineProperty(object, 'mRecommendations', {
    enumerable: true,
    writable: true,
    value: fullLookRecommendation(apiProduct)
  });

  Object.defineProperty(object, 'isDisplayNewBadge', {
    enumerable: true,
    writable: true,
    value: recommendationStrips(apiProduct)
  });

  Object.defineProperty(object, 'sku', {
    enumerable: true,
    value: apiProduct.manufacturerSKU
});
};
