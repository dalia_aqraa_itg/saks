// Add any product custom attribute in this decorator
'use strict';
var Site = require('dw/system/Site');
var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
/**
 * saves payment instruemnt to customers wallet
 * @param {Object} apiProduct - The apiProduct
 * @returns {boolean} true - return true for waitListable
 */
function isWaitListAble(apiProduct) {
  var result = false;
  if ('isWaitlistEnabled' in Site.current.preferences.custom && Site.current.preferences.custom.isWaitlistEnabled) {
    if (apiProduct && !apiProduct.master) {
      if ('waitlist' in apiProduct.custom && apiProduct.custom.waitlist === 'true') {
        result = true;
      }
    }
  }
  return result;
}

module.exports = function (object, apiProduct, isProductilePage, isRequestEstimatedEarns) {
  if (isProductilePage) {
    // Only "discountAppliedInCheckout" attribute is required for every product on the PLP page
    Object.defineProperty(object, 'discountAppliedInCheckout', {
      enumerable: true,
      value: productHelper.discountAppliedInCheckout(object)
    });
    Object.defineProperty(object, 'spdCollectionName', {
      enumerable: true,
      value: 'spdCollectionName' in apiProduct.custom ? apiProduct.custom.spdCollectionName : ''
    });
  } else {
    // Set these attributes for product object on all the pages except PLP.
    Object.defineProperty(object, 'waitlistable', {
      enumerable: true,
      value: isWaitListAble(apiProduct)
    });
    Object.defineProperty(object, 'dropShipInd', {
      enumerable: true,
      value: 'dropShipInd' in apiProduct.custom ? apiProduct.custom.dropShipInd && apiProduct.custom.dropShipInd === 'true' : false
    });
    Object.defineProperty(object, 'discountAppliedInCheckout', {
      enumerable: true,
      value: productHelper.discountAppliedInCheckout(object)
    });
    Object.defineProperty(object, 'hudsonPoint', {
      enumerable: true,
      value: productHelper.getHudsonPoints(apiProduct, false, object.price, isRequestEstimatedEarns)
    });
    Object.defineProperty(object, 'spdCollectionName', {
      enumerable: true,
      value: 'spdCollectionName' in apiProduct.custom ? apiProduct.custom.spdCollectionName : ''
    });
  }
};
