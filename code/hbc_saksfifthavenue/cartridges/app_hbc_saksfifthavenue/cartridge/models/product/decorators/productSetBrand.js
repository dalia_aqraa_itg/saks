'use strict';
var collections = require('*/cartridge/scripts/util/collections');

/**
 *
 * @param {Object} factory - Product Factory object
 *
 * @returns {string} - brand name if all the product has same brand, null otherwise
 */
function getBrandName(product) {
  var firstProduct = product.individualProducts[0];
  var brandName = firstProduct.brand && firstProduct.brand.name ? firstProduct.brand.name : null;
  for (var i = 0; i < product.individualProducts.length; i++) {
    var setProduct = product.individualProducts[i];
    if (!brandName || !setProduct.brand || !setProduct.brand.name || brandName !== setProduct.brand.name) {
      brandName = null;
    }
  }
  return brandName;
}

/**
 * Adds the object with brandName attribute
 * @param {Object} product - Product Model to be decorated
 * @returns {Object} product - Product Model to be decorated
 */

module.exports = function (product) {
  Object.defineProperty(product, 'brandName', {
    enumerable: true,
    value: getBrandName(product)
  });
};
