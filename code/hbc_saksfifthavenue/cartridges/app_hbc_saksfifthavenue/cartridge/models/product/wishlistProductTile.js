'use strict';
var base = module.superModule;

/**
 * Decorate product with product tile information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {string} productType - Product type information
 * @param {string} frpid - First represented product ID
 *
 * @returns {Object} - Decorated product model
 */
function wishlistProductTile(product, vApiProduct, productType) {
  base.call(this, product, vApiProduct, productType);
  // identify if the product has the hi-res-model image
  var hasModelImages = false;
  if (product && product.images && product.images.medium) {
    product.images.medium.forEach(function (image) {
      if (image && image.hiresModelURL) {
        hasModelImages = true;
      }
    });
  }
  product.hasModelImages = hasModelImages; // eslint-disable-line
  return product;
}

module.exports = wishlistProductTile;
