'use strict';

var decorators = require('*/cartridge/models/product/decorators/index');

module.exports = function sddProduct(product, apiProduct) {
  decorators.base(product, apiProduct, '');
  decorators.availableForSDD(product, apiProduct);
  return product;
};
