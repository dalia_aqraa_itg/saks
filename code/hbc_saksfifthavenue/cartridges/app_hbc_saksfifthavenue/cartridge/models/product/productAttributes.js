'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var urlHelper = require('*/cartridge/scripts/helpers/urlHelpers');
var ImageModel = require('*/cartridge/models/product/productImages');
var HashMap = require('dw/util/HashMap');
var preferences = require('*/cartridge/config/preferences');

/**
 * Validates varaition attribute in of no_size or no_color
 * @param {Object} selectedProductVariationAttributeValue Vairaion Value Object
 * @returns {boolean} hideAttribute returns the status
 */
function isNoVariant(selectedProductVariationAttributeValue) {
  var hideAttribute = false;
  switch (selectedProductVariationAttributeValue.displayValue.toUpperCase()) {
    case 'NO SIZE':
    case 'NO COLOR':
    case 'NO COLOUR':
      hideAttribute = true;
      break;
  }
  return hideAttribute;
}


/**
 * validates whether the variant is waitlistable
 * @param {String} attrId variation Attribute ID
 * @param {String} attrValue Variation attribute Value
 * @param {Object} variationModel variation attribute Model
 * @returns {boolean} status returns whether the variant is waitlistable
 */
 function isWaitListable(attrId, attrValue, variationModel, selectedColor) {
  var HashMap = require('dw/util/HashMap');
  var waitlistable = false;
  var hashMap = new HashMap();
  hashMap.put(attrId, attrValue);
  if (selectedColor) {
    hashMap.put(COLOR_ATTRIBUTE_NAME, selectedColor);
  }
  var productVariants = variationModel.getVariants(hashMap);
  Object.keys(productVariants).forEach(function (key) {
    if (productVariants[key].custom.waitlist && productVariants[key].custom.waitlist == 'true') {
      waitlistable = true;
    }
  });
  return waitlistable;
}

/**
 * Determines whether a product attribute has image swatches.  Currently, the only attribute that
 *     does is Color.
 * @param {string} dwAttributeId - Id of the attribute to check
 * @returns {boolean} flag that specifies if the current attribute should be displayed as a swatch
 */
function isSwatchable(dwAttributeId) {
  var imageableAttrs = ['color'];
  return imageableAttrs.indexOf(dwAttributeId) > -1;
}

if (preferences.CartBEPerformanceChangesEnable) {
  /**
   * Retrieve all attribute values
   *
   * @param {dw.catalog.ProductVariationModel} variationModel - A product's variation model
   * @param {dw.catalog.ProductVariationAttributeValue} selectedValue - Selected attribute value
   * @param {dw.catalog.ProductVariationAttribute} attr - Attribute value'
   * @param {string} endPoint - The end point to use in the Product Controller
   * @param {string} selectedOptionsQueryParams - Selected options query params
   * @param {string} quantity - Quantity selected
   * @returns {Object} - List of attribute value objects for template context
   */
  function getAllAttrValues(variationModel, selectedValue, attr, endPoint, selectedOptionsQueryParams, quantity, selectedColorAttr, object) {
    var ArrayList = require('dw/util/ArrayList');
    var attrValues = variationModel.getAllValues(attr);
    var actionEndpoint = 'Product-' + endPoint;
    var pdpEndpoint = 'Product-Show';
    var maxLength = 0;
    var masterProd = variationModel.master;
    // Filterout NoVariation Attribute
    var nonVaraintList = attrValues;
    var time1 = new Date();
    if (attrValues) {
      var nonVaraint = attrValues.toArray().filter(function (tAttrValue) {
        return !isNoVariant(tAttrValue);
      });
      nonVaraintList = new ArrayList(nonVaraint);
    }
    var isMasterSelectable = !empty(masterProd) && !empty(masterProd.custom.hbcProductType) &&
    (masterProd.custom.hbcProductType === 'home' || masterProd.custom.hbcProductType === 'bridal') && masterProd.searchableIfUnavailableFlag;
    var urlUnselectVariationValue = variationModel.urlUnselectVariationValue(actionEndpoint, attr);
    var isSwatchableAttribute = isSwatchable(attr.attributeID);

    var processedAttrs = collections.map(nonVaraintList, function (value) {
      var isSelectable;
      var valueUrl = '';
      var pdpUrl = '';
      var waitlistable = false;
      var hasAvailableInventory = false;

      var hashMap = new HashMap();
      hashMap.put(attr.ID, value.ID);
      
      if (attr.ID != 'color' && selectedColorAttr && selectedColorAttr.length > 0) {
        hashMap.put('color', selectedColorAttr.color);
      }
  
      if (attr.attributeID === 'size' && value.displayValue.length >= maxLength) {
        maxLength = value.displayValue.length;
      }
  
      if (isMasterSelectable) {
        isSelectable = true;
      } else {
        isSelectable = variationModel.hasOrderableVariants(attr, value);
      }

      var isSelected = (selectedValue && selectedValue.equals(value)) || false;
      
      var attVariants = variationModel.getVariants(hashMap);
      var variantToSelect = attVariants && attVariants.length == 1 ? attVariants[0] : null;
      var variantAvailabilityStatus = attVariants && attVariants.length ? attVariants[0].availabilityModel.availabilityStatus : '';
      var sfccPreorder = variantToSelect && 'sfccPreorder' in variantToSelect.custom ? variantToSelect.custom.sfccPreorder : masterProd && masterProd.custom ? masterProd.custom.sfccPreorder : '';
      var hasAllInPreOrder = true;
      Object.keys(attVariants).forEach(function (key) {
        var variant = attVariants[key];
        if (variant.custom.waitlist == 'true') {
          waitlistable = true;
        }
        var tempVariantAvailabilityStatus = variant.availabilityModel.availabilityStatus;
        var sfccPreorderVariant = variant && 'sfccPreorder' in variant.custom ? variant.custom.sfccPreorder : masterProd && masterProd.custom ? masterProd.custom.sfccPreorder : '';
        if (sfccPreorderVariant == 'T') {
          sfccPreorder = sfccPreorderVariant;
        }
        if (((tempVariantAvailabilityStatus == 'BACKORDER' || tempVariantAvailabilityStatus == 'PRE_ORDER') && sfccPreorderVariant != 'T') || tempVariantAvailabilityStatus == 'NOT_AVAILABLE' || tempVariantAvailabilityStatus == 'IN_STOCK') {
          hasAllInPreOrder = false;
        }
        if (hasAvailableInventory && (variantAvailabilityStatus == 'IN_STOCK' || sfccPreorderVariant == 'T')) {
          return;
        } else {
          var inventoryRecord = variant.availabilityModel.inventoryRecord;
          if(inventoryRecord && inventoryRecord.ATS.available) {
            hasAvailableInventory = true;
            variantAvailabilityStatus = attVariants[key].availabilityModel.availabilityStatus;
          }
        }
      });
      
      var isSelectedSwatchWaitlistable = isWaitListable('color', selectedColorAttr.color, variationModel) || (masterProd && masterProd.custom.waitlist);
      var canPreOrderOrBuy = variantAvailabilityStatus == 'IN_STOCK' || (variantAvailabilityStatus != 'NOT_AVAILABLE' && sfccPreorder == 'T');
      object.showWaitlistButton = isSelectedSwatchWaitlistable;
      if (selectedValue && selectedValue.ID == value.ID) {
        object.variantAvailabilityStatus = variantAvailabilityStatus;
        object.showWaitlistButton = isSelectedSwatchWaitlistable && !canPreOrderOrBuy;
        object.variantToSelect = variantToSelect ? variantToSelect.ID : '';
        object.hasAllInPreOrder = hasAllInPreOrder;
      } else if (empty(object.hasAllInPreOrder) && empty(selectedValue) && attVariants.length > 1) {
        object.hasAllInPreOrder = hasAllInPreOrder;
      }
      isSelectable = isSelectable && !(variantAvailabilityStatus != 'IN_STOCK' && sfccPreorder != 'T');
      var processedAttr = {
        id: value.ID,
        description: value.description,
        displayValue: value.displayValue,
        value: value.value,
        selected: isSelected,
        selectable: isSelectable,
        variantAvailabilityStatus: variantAvailabilityStatus,
        sfccPreorder: sfccPreorder,
        waitlist: waitlistable
      };
      pdpUrl = variationModel.urlSelectVariationValue(pdpEndpoint, attr, value);
      valueUrl = isSelected && endPoint !== 'Show' ? urlUnselectVariationValue : variationModel.urlSelectVariationValue(actionEndpoint, attr, value);
      processedAttr.url = urlHelper.appendQueryParams(valueUrl, [selectedOptionsQueryParams, 'quantity=' + quantity]);
      processedAttr.pdpUrl = urlHelper.appendQueryParams(pdpUrl, [selectedOptionsQueryParams]);
      if (isSwatchableAttribute) {
        processedAttr.images = new ImageModel(value, {
          types: ['swatch'],
          quantity: 'all'
        });
      }
  
      return processedAttr;
    });
  
    return {
      processedAttr: processedAttrs,
      maxLength: maxLength
    };
  }
} else {
/**
 * Retrieve all attribute values
 *
 * @param {dw.catalog.ProductVariationModel} variationModel - A product's variation model
 * @param {dw.catalog.ProductVariationAttributeValue} selectedValue - Selected attribute value
 * @param {dw.catalog.ProductVariationAttribute} attr - Attribute value'
 * @param {string} endPoint - The end point to use in the Product Controller
 * @param {string} selectedOptionsQueryParams - Selected options query params
 * @param {string} quantity - Quantity selected
 * @returns {Object} - List of attribute value objects for template context
 */
function getAllAttrValues(variationModel, selectedValue, attr, endPoint, selectedOptionsQueryParams, quantity, selectedColorAttr, object) {
  var attrValues = variationModel.getAllValues(attr);
  var actionEndpoint = 'Product-' + endPoint;
  var maxLength = 0;
  var pdpEndpoint = 'Product-Show';
  var masterProd = variationModel.master;
  // Filterout NoVariation Attribute
  var ArrayList = require('dw/util/ArrayList');
  var nonVaraintList = attrValues;
  if (attrValues) {
    var nonVaraint = attrValues.toArray().filter(function (tAttrValue) {
      return !isNoVariant(tAttrValue);
    });
    nonVaraintList = new ArrayList(nonVaraint);
  }
  var processedAttrs = collections.map(nonVaraintList, function (value) {
    var isSelectable;
    if (
      !empty(masterProd) &&
      !empty(masterProd.custom.hbcProductType) &&
      (masterProd.custom.hbcProductType === 'home' || masterProd.custom.hbcProductType === 'bridal') &&
      masterProd.searchableIfUnavailableFlag
    ) {
      isSelectable = true;
    } else {
      isSelectable = variationModel.hasOrderableVariants(attr, value);
    }

    var isSelected = (selectedValue && selectedValue.equals(value)) || false;
    var valueUrl = '';
    var pdpUrl = '';
    var attrMap = new HashMap();
    attrMap.put(attr.ID, value.ID);
    if (attr.ID != 'color' && selectedColorAttr && !selectedColorAttr.empty) {
      attrMap.put('color', selectedColorAttr.color);
    }
    var attVariants = variationModel.getVariants(attrMap);
    var variantToSelect = attVariants && attVariants.length == 1 ? attVariants[0] : null;
    var variantAvailabilityStatus = !empty(variantToSelect) ? variantToSelect.availabilityModel.availabilityStatus : '';
    var sfccPreorder = variantToSelect && 'sfccPreorder' in variantToSelect.custom ? variantToSelect.custom.sfccPreorder : masterProd && masterProd.custom ? masterProd.custom.sfccPreorder : '';
    var hasAllInPreOrder = true;
    var variant;
    for (var attIndex = 0; attIndex < attVariants.length; attIndex++) {
      variant = attVariants[attIndex];
      var inventoryRecord = variant.availabilityModel.inventoryRecord;
      var tempVariantAvailabilityStatus = variant.availabilityModel.availabilityStatus;
      var sfccPreorderVariant = variant && 'sfccPreorder' in variant.custom ? variant.custom.sfccPreorder : masterProd && masterProd.custom ? masterProd.custom.sfccPreorder : '';
      if (sfccPreorderVariant == 'T') {
        sfccPreorder = sfccPreorderVariant;
      }
      if (((tempVariantAvailabilityStatus == 'BACKORDER' || tempVariantAvailabilityStatus == 'PRE_ORDER') && sfccPreorderVariant != 'T') || tempVariantAvailabilityStatus == 'NOT_AVAILABLE' || tempVariantAvailabilityStatus == 'IN_STOCK') {
        hasAllInPreOrder = false;
      }
      if(inventoryRecord && inventoryRecord.ATS.available){
        variantAvailabilityStatus = variant.availabilityModel.availabilityStatus;
      }
    }

    var isSelectedSwatchWaitlistable = isWaitListable('color', selectedColorAttr.color, variationModel) || (masterProd && masterProd.custom.waitlist);
    var canPreOrderOrBuy = variantAvailabilityStatus == 'IN_STOCK' || (variantAvailabilityStatus != 'NOT_AVAILABLE' && sfccPreorder == 'T');
    object.showWaitlistButton = isSelectedSwatchWaitlistable;
    if (selectedValue && selectedValue.ID == value.ID) {
      object.showWaitlistButton = isSelectedSwatchWaitlistable && !canPreOrderOrBuy;
      object.variantToSelect = variantToSelect ? variantToSelect.ID : '';
      object.variantAvailabilityStatus = variantAvailabilityStatus;
      object.hasAllInPreOrder = hasAllInPreOrder;
    } else if (empty(object.hasAllInPreOrder) && empty(selectedValue) && attVariants.length > 1) {
      object.hasAllInPreOrder = hasAllInPreOrder;
    }
    isSelectable = isSelectable && !(variantAvailabilityStatus != 'IN_STOCK' && sfccPreorder != 'T');
    
    var processedAttr = {
      id: value.ID,
      description: value.description,
      displayValue: value.displayValue,
      value: value.value,
      selected: isSelected,
      selectable: isSelectable,
      variantAvailabilityStatus: variantAvailabilityStatus,
      sfccPreorder: sfccPreorder
    };
    pdpUrl = variationModel.urlSelectVariationValue(pdpEndpoint, attr, value);
    valueUrl =
      isSelected && endPoint !== 'Show'
        ? variationModel.urlUnselectVariationValue(actionEndpoint, attr)
        : variationModel.urlSelectVariationValue(actionEndpoint, attr, value);
    processedAttr.url = urlHelper.appendQueryParams(valueUrl, [selectedOptionsQueryParams, 'quantity=' + quantity]);

    processedAttr.pdpUrl = urlHelper.appendQueryParams(pdpUrl, [selectedOptionsQueryParams]);
    if (isSwatchable(attr.attributeID)) {
      processedAttr.images = new ImageModel(value, {
        types: ['swatch'],
        quantity: 'all'
      });
    }

    if (attr.attributeID === 'size' && value.displayValue.length >= maxLength) {
      maxLength = value.displayValue.length;
    }
    var waitlistable = false;
    var hashMap = new HashMap();
    hashMap.put(attr.ID, value.ID);
    if (attr.attributeID === 'size' && selectedColorAttr && selectedColorAttr.length > 0) {
      hashMap.putAll(selectedColorAttr);
    }
    var productVariants = variationModel.getVariants(hashMap);
    Object.keys(productVariants).forEach(function (key) {
      if (productVariants[key].custom.waitlist == 'true') {
        waitlistable = true;
      }
    });

    processedAttr.waitlist = waitlistable;

    return processedAttr;
  });
  return {
    processedAttr: processedAttrs,
    maxLength: maxLength
  };
}
}

/**
 * Gets the Url needed to relax the given attribute selection, this will not return
 * anything for attributes represented as swatches.
 *
 * ALTERED : definition changed to add a selected attribute value, this is to avoid another
 * iteration over the value
 * @param {Array} values - Attribute values
 * @param {string} attrID - id of the attribute
 * @returns {Object} -the Url that will remove the selected attribute and the selected attribute display value.
 */
function getAttrResetUrlAndValue(values, attrID) {
  var urlReturned;
  var selectedValue = null;
  var value;
  var returnValue = {};

  for (var i = 0; i < values.length; i++) {
    value = values[i];
    if (!value.images) {
      if (value.selected) {
        urlReturned = value.url;
        break;
      }

      if (value.selectable) {
        urlReturned = value.url.replace(attrID + '=' + value.value, attrID + '=');
        break;
      }
    }
    if (value.selected) {
      selectedValue = value.displayValue;
    }
  }
  returnValue.urlReturned = urlReturned;
  returnValue.selectedValue = selectedValue;

  return returnValue;
}

/**
 *
 * @param {Object} attr variation Attribute model
 * @param {Object} selectedProductVariationAttributeValue Selected variants
 * @returns {string} Returns text to display
 */
function provideAttrText(attr, selectedProductVariationAttributeValue, values) {
  if (selectedProductVariationAttributeValue && attr) {
    if (isNoVariant(selectedProductVariationAttributeValue)) {
      return;
    }
  }
  var attrText = '<span class="text1">' + attr.displayName + '</span>';
  var colorSwatchDisplayLimit = dw.system.Site.current.getCustomPreferenceValue('colorSwatchDisplayLimit');
  if (selectedProductVariationAttributeValue && attr && values.length <= 9) {
    if (
      selectedProductVariationAttributeValue.displayValue.replace(/[-_ ]/g, '').toLowerCase() === 'nocolor'
        ? true
        : selectedProductVariationAttributeValue.displayValue.replace(/[-_ ]/g, '').toLowerCase() === 'nosize'
        ? true
        : false
    ) {
      attrText = '';
    } else {
      attrText = attrText
        .concat(' ')
        .concat('<span class="text2 dropdown-hide-attribute">' + selectedProductVariationAttributeValue.displayValue.toLocaleLowerCase() + '</span>');
    }
  } else {
      if (selectedProductVariationAttributeValue && attr.ID ==='color' && values.length > colorSwatchDisplayLimit) {
        attrText = attrText
          .concat(' ')
          .concat('<span class="text2 dropdown-hide-attribute d-none">' + selectedProductVariationAttributeValue.displayValue.toLocaleLowerCase() + '</span>');
      }
      else if (selectedProductVariationAttributeValue) {
        attrText = attrText
          .concat(' ')
          .concat('<span class="text2 dropdown-hide-attribute">' + selectedProductVariationAttributeValue.displayValue.toLocaleLowerCase() + '</span>');
      }
  }
  return attrText;
}

/**
 *
 * @param {Object} attr variation Attribute model
 * @param {Object} selectedProductVariationAttributeValue Selected variants
 * @returns {string} Returns text to display
 */
function provideEditAttrText(attr, selectedProductVariationAttributeValue) {
  if (selectedProductVariationAttributeValue && attr) {
    return attr.displayName + ' - ' + selectedProductVariationAttributeValue.displayValue;
  }
  return attr.displayName;
}

/**
 * @param {integer} maxCount maximum character size of variation attribute value
 * @returns {string} returns respective class mapped in site preference
 */
function provideSelectedSizeClass(maxCount) {
  var matchClass = '';
  if (maxCount && maxCount > 0) {
    var preferences = require('*/cartridge/config/preferences');
    var isMatchFound = false;
    var sizeTreatment = preferences.product.SIZE_TREATMENT;
    if (sizeTreatment) {
      sizeTreatment.forEach(function (enumVal) {
        if (!isMatchFound && maxCount <= enumVal.value) {
          isMatchFound = true;
          matchClass = enumVal.displayValue;
        }
      });
    }
  }
  return matchClass;
}

/**
 * @constructor
 * @classdesc Get a list of available attributes that matches provided config
 *  We are not overriding this model since, same logic has to implement with adiditional loops
 * @param {dw.catalog.ProductVariationModel} variationModel - current product variation
 * @param {Object} attrConfig - attributes to select
 * @param {Array} attrConfig.attributes - an array of strings,representing the
 *                                        id's of product attributes.
 * @param {string} attrConfig.attributes - If this is a string and equal to '*' it signifies
 *                                         that all attributes should be returned.
 *                                         If the string is 'selected', then this is comming
 *                                         from something like a product line item, in that
 *                                         all the attributes have been selected.
 *
 * @param {string} attrConfig.endPoint - the endpoint to use when generating urls for
 *                                       product attributes
 * @param {string} selectedOptionsQueryParams - Selected options query params
 * @param {string} quantity - Quantity selected
 */
function VariationAttributesModel(variationModel, attrConfig, selectedOptionsQueryParams, quantity, object) {
  var allAttributes = variationModel.productVariationAttributes;
  var result = [];
  var selectedColorAttr = new HashMap();
  var attrIDValues = '';
  collections.forEach(allAttributes, function (attr) {
    var selectedValue = variationModel.getSelectedValue(attr);
    if (!selectedValue) {
      attrIDValues += attr.ID + ' ,';
    }
    if (attr.ID == 'color' && !empty(selectedValue) && !empty(selectedValue.ID)) {
      selectedColorAttr.put(attr.ID, selectedValue.ID);
    }
    var valuesAttr = getAllAttrValues(variationModel, selectedValue, attr, attrConfig.endPoint, selectedOptionsQueryParams, quantity, selectedColorAttr, object);
    var values = valuesAttr.processedAttr;
    var maxCount = valuesAttr.maxLength;
    var selectedSizeClass = provideSelectedSizeClass(maxCount);
    var attributeResetValue = getAttrResetUrlAndValue(values, attr.ID);
    var resetUrl = attributeResetValue.urlReturned;

    // If no variation value skip the loop
    if (
      values &&
      values.length &&
      ((Array.isArray(attrConfig.attributes) && attrConfig.attributes.indexOf(attr.attributeID) > -1) || attrConfig.attributes === '*')
    ) {
      result.push({
        attributeId: attr.attributeID,
        displayName: attr.displayName,
        displayValue: selectedValue && selectedValue.displayValue ? selectedValue.displayValue : '',
        id: attr.ID,
        swatchable: isSwatchable(attr.attributeID),
        values: values,
        resetUrl: resetUrl,
        selectedAttribute: selectedValue,
        attrDisplay: provideAttrText(attr, selectedValue, values),
        attrEditDisplay: provideEditAttrText(attr, selectedValue),
        selectedSizeClass: selectedSizeClass,
        attributeSelectedValue: attributeResetValue.selectedValue
      });
    } else if (attrConfig.attributes === 'selected') {
      result.push({
        displayName: attr.displayName,
        displayValue: selectedValue && selectedValue.displayValue ? selectedValue.displayValue : '',
        attributeId: attr.attributeID,
        id: attr.ID,
        resetUrl: resetUrl,
        selectedSizeClass: selectedSizeClass,
        attributeSelectedValue: attributeResetValue.selectedValue,
        attrDisplay: provideAttrText(attr, null),
        attrEditDisplay: provideEditAttrText(attr, selectedValue)
      });
    }
  });
  this.variationAttributes = result;
  this.attrIDValues = attrIDValues;
}

module.exports = VariationAttributesModel;
