'use strict';

var decorators = require('*/cartridge/models/product/decorators/index');

module.exports = function productVariations(product, apiProduct, options) {
  decorators.quantity(product, apiProduct, options.quantity);
  decorators.variationAttributes(product, options.variationModel, {
    attributes: '*',
    endPoint: 'Variation'
  });

  decorators.getHBCProductType(product, apiProduct);

  Object.defineProperty(product, 'masterProductID', {
    value: apiProduct.variant ? apiProduct.variationModel.master.ID : apiProduct.ID,
    enumerable: true
  });

  Object.defineProperty(product, 'productType', {
    enumerable: true,
    value: options.productType
  });

  Object.defineProperty(product, 'sizeChartTemplate', {
    enumerable: true,
    value: 'sizeChartSubType' in apiProduct.custom ? apiProduct.custom.sizeChartSubType : null
  });

  return product;
};
