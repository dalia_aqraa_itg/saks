'use strict';
var PriceBookMgr = require('dw/catalog/PriceBookMgr');
var StoreMgr = require('dw/catalog/StoreMgr');
var URLUtils = require('dw/web/URLUtils');
var preferences = require('*/cartridge/config/preferences');

/**
 * Executes a custom search with the store id
 * @param {dw.catalog.ProductSearchModel} apiProductsearch - product search model
 * @param {Object} params - request params
 * @returns {Object} - containing a boolena attribute and search model
 */
function search(apiProductsearch, params) {
  var bopisEnabled = preferences.isBopisEnabled;
  var isRefinedByStore = false,
    isRefinedWithSDD = false;
  var store = null;
  var storePriceBook = null;
  var priceBoodId = '';
  var searchSrc = params.srchsrc ? params.srchsrc : null;
  PriceBookMgr.setApplicablePriceBooks(null);
  // get defalt store from session or with geo location
  let storeID = params.storeid;
  if (bopisEnabled && storeID) {
    store = StoreMgr.getStore(storeID);
    priceBoodId = storeID;
    if (store) {
      // set store price book to filter the products available for a store
      storePriceBook = PriceBookMgr.getPriceBook(priceBoodId);
      if (storePriceBook) {
        PriceBookMgr.setApplicablePriceBooks(storePriceBook);
        // TO-FIX : displays the price set in the filter bar
        apiProductsearch.setPriceMin(0);
        if (searchSrc === 'sdd') {
          isRefinedWithSDD = true;
        }
        isRefinedByStore = true;
      }
    }
  }
  // PSM search
  apiProductsearch.search();
  // set back the price to default to avoid price overriding
  PriceBookMgr.setApplicablePriceBooks(null);

  return {
    isRefinedByStore: isRefinedByStore,
    apiProductsearch: apiProductsearch,
    isRefinedWithSDD: isRefinedWithSDD
  };
}

/**
 * Adds store refinement to url
 * @param {Object} req - request object
 * @param {dw.web.URLUtils} actionUrl - request url
 * @returns {dw.web.URLUtils} - store added url
 */
function getStoreRefinementUrl(req, actionUrl, myPreferencesData) {
  var storeRefineUrl = URLUtils.url(actionUrl);
  var whitelistedParams = ['q', 'cgid', 'pmin', 'pmax', 'srule', 'storeid', 'srchsrc'];
  var i = 1;
  if (myPreferencesData) {// For My Designer and My Sizes link 
      Object.keys(myPreferencesData).forEach(function (element) {
        if (whitelistedParams.indexOf(element) > -1) {
          storeRefineUrl.append(element, myPreferencesData[element]);
          delete myPreferencesData[element];
        }
    });

    Object.keys(myPreferencesData).forEach(function (element) {
      storeRefineUrl.append('prefn' + i, element);
      storeRefineUrl.append('prefv' + i, myPreferencesData[element + '']);
    i++;
  });
  } else{
  Object.keys(req.querystring).forEach(function (element) {
    if (whitelistedParams.indexOf(element) > -1) {
      storeRefineUrl.append(element, req.querystring[element]);
    }

    if (element === 'preferences') {
      var i = 1;
      Object.keys(req.querystring[element]).forEach(function (preference) {
        storeRefineUrl.append('prefn' + i, preference);
        storeRefineUrl.append('prefv' + i, req.querystring[element][preference]);
        i++;
      });
    }
  });
}
  return storeRefineUrl;
}

function getCategoryID(category, stringID) {
  if (!category.root) {
    var catName =
      'contentSlotCategoryName' in category.custom && category.custom.contentSlotCategoryName
        ? category.custom.contentSlotCategoryName
        : 'contentSlotArticleName' in category.custom && category.custom.contentSlotArticleName
        ? category.custom.contentSlotArticleName
        : category.displayName;
    var CategoryName = !empty(stringID) ? catName + '|' + stringID : catName;
    return getCategoryID(category.parent, CategoryName);
  }
  return stringID.trim();
}

module.exports = {
  search: search,
  getStoreRefinementUrl: getStoreRefinementUrl,
  getCategoryID: getCategoryID
};
