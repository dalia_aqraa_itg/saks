'use strict';

var base = module.superModule;
var collections = require('*/cartridge/scripts/util/collections');
var preferences = require('*/cartridge/config/preferences');

if (preferences.CartBEPerformanceChangesEnable) {
  /**
   * Loops through all of the product line items and adds new properties.
   * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - All product
   * line items of the basket
   * Updates the prototype with new properties
   */
  function additionalAttributes(productLineItems) {
    var sddItems = [];
    var giftWraps = [];
    var restrictedSt = '';
    var inStoreItems = [];
    var dropShipItems = [];
    var hasPreOrderItems = false;
    var signatureRequired = false;
    var hasUSPSRestricted = false;

    if (productLineItems.items && productLineItems.items.length) {
      productLineItems.items.forEach(function (productLineItem) {
        if (productLineItem.sddDetails && productLineItem.sddDetails.unitsAvailable && productLineItem.sddDetails.unitsAvailable >= productLineItem.quantity) {
            sddItems.push(productLineItem);
        }
        if (productLineItem.fromStoreId) {
            inStoreItems.push(productLineItem);
        }
        if (productLineItem.isDropShipItem) {
            dropShipItems.push(productLineItem);
        }
        if (!productLineItem.giftWrapEligible) {
            giftWraps.push(productLineItem);
        }
        if (productLineItem.pdRestrictedStateText) {
            restrictedSt += productLineItem.pdRestrictedStateText + ',';
        }
        if (!productLineItem.uspsShippingOK) {
            hasUSPSRestricted = true;
        }
        if (productLineItem.signatureRequired) {
            signatureRequired = true;
        }
        if (!empty(productLineItem.preOrder) && !empty(productLineItem.preOrder.applicable) && productLineItem.preOrder.applicable == true) {
            hasPreOrderItems = true;
        }
      });
    }

    Object.defineProperty(productLineItems, 'hasInStoreItems', {
        enumerable: true,
        value: inStoreItems // eslint-disable-line
    });
    Object.defineProperty(productLineItems, 'sddEligibleItems', {
        enumerable: true,
        value: sddItems // eslint-disable-line
    });
    Object.defineProperty(productLineItems, 'dropShipItems', {
        enumerable: true,
        value: dropShipItems
    });
    Object.defineProperty(productLineItems, 'giftWrapItems', {
        enumerable: true,
        value: giftWraps
    });
    Object.defineProperty(productLineItems, 'restrictedStates', {
        enumerable: true,
        value: restrictedSt
    });
    Object.defineProperty(productLineItems, 'hasUSPSRestrictedItems', {
        enumerable: true,
        value: hasUSPSRestricted
    });
    Object.defineProperty(productLineItems, 'signatureRequired', {
        enumerable: true,
        value: signatureRequired
    });
    Object.defineProperty(productLineItems, 'hasPreOrderItems', {
        enumerable: true,
        value: hasPreOrderItems
    });
  }
} else {
  /**
  * Loops through all of the product line items and adds new properties.
  * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - All product
  * line items of the basket
  * Updates the prototype with new property, if basket has items with SDD eligibility
  * Updates the prototype with new properties
  */
  function sddEligibleItems(productLineItems) {
    var sddItems = [];
    if (productLineItems.items && productLineItems.items.length) {
      productLineItems.items.forEach(function (productLineItem) {
        if (productLineItem.sddDetails && productLineItem.sddDetails.unitsAvailable && productLineItem.sddDetails.unitsAvailable >= productLineItem.quantity) {
          sddItems.push(productLineItem);
        }
      });
    }
  }
}

/**
 * Creates an array of product line items
 * @param {dw.util.Collection<dw.order.ProductLineItem>} allLineItems - All product
 * line items of the basket
 * @param {string} view - the view of the line item (basket or order)
 * @returns {Array} an array of product line items.
 */
function createProductLineItemsObject(allLineItems, view) {
  var lineItems = [];

  collections.forEach(allLineItems, function (item) {
    // when item's category is unassigned, return a lineItem with limited attributes
    if (!item.product) {
      lineItems.push({
        id: item.productID,
        quantity: item.quantity.value,
        productName: item.productName,
        UUID: item.UUID,
        noProduct: true,
        images: {
          small: [
            {
              url: URLUtils.staticURL('/images/noimagelarge.png'),
              alt: Resource.msgf('msg.no.image', 'common', null),
              title: Resource.msgf('msg.no.image', 'common', null)
            }
          ]
        },
        price: {
          sales: {
            formatted: item.netPrice.decimalValue
          }
        }
      });
      return;
    }
    var options = collections.map(item.optionProductLineItems, function (optionItem) {
      return {
        optionId: optionItem.optionID,
        selectedValueId: optionItem.optionValueID
      };
    });

    var bonusProducts = null;

    if (!item.bonusProductLineItem && item.custom.bonusProductLineItemUUID && item.custom.preOrderUUID) {
      bonusProducts = [];
      collections.forEach(allLineItems, function (bonusItem) {
        if (!!item.custom.preOrderUUID && bonusItem.custom.bonusProductLineItemUUID === item.custom.preOrderUUID) {
          var bpliOptions = collections.map(bonusItem.optionProductLineItems, function (boptionItem) {
            return {
              optionId: boptionItem.optionID,
              selectedValueId: boptionItem.optionValueID
            };
          });
          var params = {
            pid: bonusItem.product.ID,
            quantity: bonusItem.quantity.value,
            variables: null,
            pview: 'bonusProductLineItem',
            containerView: view,
            lineItem: bonusItem,
            options: bpliOptions
          };

          bonusProducts.push(ProductFactory.get(params));
        }
      });
    }

    var params = {
      pid: item.product.ID,
      quantity: item.quantity.value,
      variables: null,
      pview: 'productLineItem',
      containerView: view,
      lineItem: item,
      options: options
    };
    var newLineItem = ProductFactory.get(params);
    newLineItem.bonusProducts = bonusProducts;
    if (newLineItem.bonusProductLineItemUUID === 'bonus' || !newLineItem.bonusProductLineItemUUID) {
      lineItems.push(newLineItem);
    }
  });
  return lineItems;
}

/**
 * CONSTRUCTOR OVERRIDEN : to include extra properties
 * @constructor
 * @classdesc class that represents a collection of line items and total quantity of
 * items in current basket or per shipment
 *
 * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - the product line items
 *                                                       of the current line item container
 * @param {string} view - the view of the line item (basket or order)
 */
function ProductLineItems(productLineItems, view) {
  base.call(this, productLineItems, view);
  if (preferences.CartBEPerformanceChangesEnable) {
    additionalAttributes(this);
  } else {
    sddEligibleItems(this);
  }
}

/**
 * Loops through all of the product line items and adds the quantities together.
 * @param {dw.util.Collection<dw.order.ProductLineItem>} items - All product
 * line items of the basket
 * @returns {number} a number representing all product line items in the lineItem container.
 */
function getTotalQuantity(items) {
  // TODO add giftCertificateLineItems quantity
  var totalQuantity = 0;
  collections.forEach(items, function (lineItem) {
    totalQuantity += lineItem.quantity.value;
  });

  return totalQuantity;
}

ProductLineItems.getTotalQuantity = getTotalQuantity;
ProductLineItems.createProductLineItemsObject = createProductLineItemsObject;
module.exports = ProductLineItems;
