'use strict';

var OMSConstants = require('*/cartridge/scripts/config/OMSServiceConfig');

/**
 * Fetch the web status based on locale
 *
 * @param {string} status - order status
 * @returns {string} locale status
 */
function getWebStatus(status) {
  var Resource = require('dw/web/Resource');
  var orderStatusPrefix = 'ORDER_STATUS_';
  let statusMapping = OMSConstants.OMS_ORDER_DETAILS_STATUS;
  let localeStatus = Resource.msg(statusMapping[status] ? orderStatusPrefix + statusMapping[status] : orderStatusPrefix + 'IN_PROGRESS', 'order', status);
  return localeStatus;
}

/**
 * Fetch order date based on the locale
 *
 * @param {Date} orderDate - order Date from oms
 * @returns {string} - locale date
 */
function getOrderDate(orderDate) {
  var StringUtils = require('dw/util/StringUtils');
  var Calendar = require('dw/util/Calendar');
  return StringUtils.formatCalendar(new Calendar(orderDate), request.locale, 1);
}
function getOrderDateFormatted(orderDate) {
  var StringUtils = require('dw/util/StringUtils');
  var Calendar = require('dw/util/Calendar');
  return StringUtils.formatCalendar(new Calendar(orderDate), 'MMMM dd, yyyy');
}

function getOrderLinesImages(order) {
  var orderLinesImage = [];
  if (order.OrderLines && order.OrderLines.OrderLine) {
    var orderLines = order.OrderLines.OrderLine;
    orderLines.forEach(function (orderLine) {
      var imageURL;
      if (orderLine.ItemDetails) {
        if (
          orderLine.ItemDetails.PrimaryInformation &&
          orderLine.ItemDetails.PrimaryInformation.ImageID &&
          orderLine.ItemDetails.PrimaryInformation.ImageLocation
        ) {
          imageURL = orderLine.ItemDetails.PrimaryInformation.ImageLocation + orderLine.ItemDetails.PrimaryInformation.ImageID;
        }
      }
      orderLinesImage.push(imageURL);
    });
  }
  return orderLinesImage;
}

function totalNumberOfRecords(order) {
  if (order.OrderLines && order.OrderLines.TotalNumberOfRecords) {
    return parseInt(order.OrderLines.TotalNumberOfRecords, 10);
  }
  return null;
}

function totalNumberOfItem(order) {
  var itemCount = 0;
  if (order.OrderLines && order.OrderLines.OrderLine && order.OrderLines.OrderLine.length > 0) {
    order.OrderLines.OrderLine.forEach(function (line) {
      if (line.OriginalOrderedQty && !empty(line.OriginalOrderedQty)) {
        itemCount = itemCount + parseInt(line.OriginalOrderedQty, 10);
      }
    });
  } else if (order.OrderLines && order.OrderLines.OriginalOrderedQty && !empty(order.OrderLines.OriginalOrderedQty)) {
    itemCount = itemCount + parseInt(order.OrderLines.OriginalOrderedQty, 10);
  }
  return itemCount;
}

/**
 * Order history model for storefront
 *
 * @param {Object} orders - OMS order model
 */
function OrderHistoryModel(orders) {
  var orderHistory = [];
  orders.forEach(function (order) {
    var orderObject = {};
    var ordNo = order.OrderNo;
    if (!empty(ordNo) && OMSConstants.OMS_RemovedZeroPrefix) {
      ordNo = ordNo.replace(/^0+/, '');
    }
    orderObject.orderNumber = ordNo;
    orderObject.status = getWebStatus(order.Status);
    let orderDate = order.OrderDate.split('T')[0];
    let DateArray = orderDate.split('-');
    orderDate = new Date(DateArray[0] + '/' + DateArray[1] + '/' + DateArray[2]);
    orderObject.creationDate = getOrderDate(orderDate);
    orderObject.creationDateFormatted = getOrderDateFormatted(orderDate);
    orderObject.orderTotal = order.PriceInfo.TotalAmount;
    orderObject.orderTotalCurrency = order.PriceInfo.Currency;
    orderObject.orderStatus = order.orderStatus;
    orderObject.orderLinesImages = getOrderLinesImages(order);
    orderObject.totalNumberOfRecords = totalNumberOfRecords(order);
    orderObject.totalNumberOfItems = totalNumberOfItem(order);
    orderObject.OrderComplete = order.OrderComplete;
    orderHistory.push(orderObject);
  });

  this.orders = orderHistory;
}

module.exports = OrderHistoryModel;
