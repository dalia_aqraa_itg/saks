'use strict';

var base = module.superModule;

/**
 * Represents content model
 * @param  {dw.content.Content} contentValue - result of ContentMgr.getContent call
 * @param  {string} renderingTemplate - rendering template for the given content
 * @return {void}
 * @constructor
 */
function content(contentValue, renderingTemplate) {
  base.call(this, contentValue, renderingTemplate);
  this.navHeader = 'navHeader' in contentValue.custom && contentValue.custom.navHeader ? contentValue.custom.navHeader : null;
  this.navSubHeader = 'navSubHeader' in contentValue.custom && contentValue.custom.navSubHeader ? contentValue.custom.navSubHeader : null;
  this.navHeaderLink = 'navHeaderLink' in contentValue.custom && contentValue.custom.navHeaderLink ? contentValue.custom.navHeaderLink.markup : null;
  this.navSubHeaderLink =
    'navSubHeaderLink' in contentValue.custom && contentValue.custom.navSubHeaderLink ? contentValue.custom.navSubHeaderLink.markup : null;
  this.loadRecaptcha =
    'includeGoogleRecaptcha' in contentValue.custom && contentValue.custom.includeGoogleRecaptcha ? contentValue.custom.includeGoogleRecaptcha : false;
  return this;
}

module.exports = content;
