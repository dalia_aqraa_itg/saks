/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
'use strict';

var base = module.superModule;
var formatMoney = require('dw/util/StringUtils').formatMoney;
var collections = require('*/cartridge/scripts/util/collections');
var Money = require('dw/value/Money');
var URLUtils = require('dw/web/URLUtils');
var HashMap = require('dw/util/HashMap');
var Resource = require('dw/web/Resource');
var Template = require('dw/util/Template');

/**
 * create the discount results html
 * @param {Array} discounts - an array of objects that contains coupon and priceAdjustment
 * information
 * @returns {string} The rendered HTML
 */
function getDiscountsHtml(discounts) {
  var context = new HashMap();
  var object = {
    totals: {
      discounts: discounts
    }
  };

  Object.keys(object).forEach(function (key) {
    context.put(key, object[key]);
  });

  var template = new Template('cart/cartCouponDisplay');
  return template.render(context).text;
}

/**
 * Adds discounts to a discounts object
 * @param {dw.util.Collection} collection - a collection of price adjustments
 * @param {Object} discounts - an object of price adjustments
 * @returns {Object} an object of price adjustments
 */
function createDiscountObject(collection, discounts) {
  var result = discounts;
  collections.forEach(collection, function (item) {
    if (!item.basedOnCoupon && item.promotionID !== 'GiftOptions') {
      result[item.UUID] = {
        UUID: item.UUID,
        lineItemText: item.lineItemText,
        price: formatMoney(item.price),
        type: 'promotion',
        callOutMsg: item.promotion && item.promotion.calloutMsg ? item.promotion.calloutMsg.markup : ''
      };
    }
  });

  return result;
}

/**
 * creates an array of discounts.
 * @param {dw.order.LineItemCtnr} lineItemContainer - the current line item container
 * @returns {Array} an array of objects containing promotion and coupon information
 */
function getDiscounts(lineItemContainer) {
  var Resource = require('dw/web/Resource');
  var CouponStatusCodes = require('dw/campaign/CouponStatusCodes');
  var PromotionHelper = require('*/cartridge/scripts/util/promotionHelper');
  var discounts = {};
  collections.forEach(lineItemContainer.couponLineItems, function (couponLineItem) {
    var cpnMessage = '';
    var cpnPromo = PromotionHelper.getCouponLineItemPromotion(couponLineItem);
    var cpnPriceAdjs = couponLineItem.priceAdjustments;
    // Looking up Promotion through Price Adjustment to solve the issue SFDEV-6865 where same coupon is attached to multiple promotions
    collections.forEach(couponLineItem.priceAdjustments, function (priceAdjustment) {
      var couponPromo = priceAdjustment.promotion;
      // If the promotion is dependent on Tender Type and promo code is applied, we need to show the discout description message in the coupon code section
      // If the promotions is dependent on Tender Type and promo code is NOT applied yet, we need to show the message from plccPromoMessage custom attribute
      // If the promotion is NOT dependent on Tender Type  and promo code is applied, NO message will be shown in the coupon code section
      if (!empty(couponPromo)) {
        if (couponPromo.custom.isTenderTypeDependent) {
          if (couponLineItem.applied) {
            cpnMessage = !empty(couponPromo.custom.discountDescription) ? couponPromo.custom.discountDescription : '';
          } else {
            cpnMessage = !empty(couponPromo.custom.plccPromoMessage) ? couponPromo.custom.plccPromoMessage : '';
          }
        } else if (!couponPromo.custom.isTenderTypeDependent && !couponLineItem.applied) {
          // SFDEV-6948 | Coupon is simply Not Applied
          cpnMessage = Resource.msg('error.notapplied.coupon', 'cart', null);
        }
      }
    });

    // As tender dependent dummy promotion will not have price adjustment, we need to call this method below to set the PLCC message SFDEV-7919
    if (!empty(cpnPromo) && cpnPromo.active && cpnPriceAdjs.length === 0) {
      if (cpnPromo.custom.isTenderTypeDependent) {
        if (couponLineItem.applied) {
          cpnMessage = !empty(cpnPromo.custom.discountDescription) ? cpnPromo.custom.discountDescription : '';
        } else {
          cpnMessage = !empty(cpnPromo.custom.plccPromoMessage) ? cpnPromo.custom.plccPromoMessage : '';
        }
      } else if (!cpnPromo.custom.isTenderTypeDependent && !couponLineItem.applied) {
        // SFDEV-6948 | Coupon is simply Not Applied
        cpnMessage = Resource.msg('error.notapplied.coupon', 'cart', null);
      }
    }
    // for gwp promo
    if (
      !couponLineItem.applied &&
      couponLineItem.valid &&
      couponLineItem.statusCode === CouponStatusCodes.NO_APPLICABLE_PROMOTION &&
      cpnPromo.custom.hasOwnProperty('promotionType') &&
      cpnPromo.custom.promotionType.value &&
      cpnPromo.custom.promotionType.value === 'GWP'
    ) {
      cpnMessage = Resource.msg('error.coupon.gwp.soldout', 'cart', null);
    }
    discounts[couponLineItem.UUID] = {
      type: 'coupon',
      UUID: couponLineItem.UUID,
      couponCode: couponLineItem.couponCode,
      applied: couponLineItem.applied,
      valid: couponLineItem.valid,
      cpnMessage: cpnMessage,
      removeCouponURL: URLUtils.url('Cart-RemoveCouponLineItem').toString()
    };
  });

  discounts = createDiscountObject(lineItemContainer.priceAdjustments, discounts);
  discounts = createDiscountObject(lineItemContainer.allShippingPriceAdjustments, discounts);

  return Object.keys(discounts).map(function (key) {
    return discounts[key];
  });
}

/**
 * @constructor
 * @classdesc totals class that represents the order totals of the current line item container
 *
 * @param {dw.order.lineItemContainer} lineItemContainer - The current user's line item container
 */
function totals(lineItemContainer) {
  base.call(this, lineItemContainer);
  this.discounts = getDiscounts(lineItemContainer);
  this.discountsHtml = getDiscountsHtml(this.discounts);
  this.amexpay = null;
  var amexpayHelper = require('*/cartridge/scripts/helpers/amexpayHelper');
  var result = amexpayHelper.getAmexPayAmount(lineItemContainer);
  if (result) {
    this.amexpay = result;
  }
  this.giftWrapObj = getGiftWrapDetails(lineItemContainer);
  
}

/**
 * Get giftwrap details
 * @param {Object} lineItemContainer - Order
 * @returns {Object} return object with giftwrap details
 */
 function getGiftWrapDetails(lineItemContainer) {
  var Money = require('dw/value/Money');
  var Resource = require('dw/web/Resource');
  var formatMoney = require('dw/util/StringUtils').formatMoney;
  var preferences = require('*/cartridge/config/preferences');
  var defaultShipment = lineItemContainer.defaultShipment;
  var giftWrapObj = {};
  var giftOptionAdjustment = null;
  giftWrapObj.applied = false;
  if (defaultShipment.custom.hasOwnProperty('giftWrapType') && defaultShipment.custom.giftWrapType === 'giftpack') {
    giftWrapObj.applied = true;
    giftOptionAdjustment = lineItemContainer.getPriceAdjustmentByPromotionID('GiftOptions');
    if (giftOptionAdjustment) {
      giftWrapObj.charge =
        preferences.giftWrapAmount && !isNaN(preferences.giftWrapAmount)
          ? formatMoney(new Money(preferences.giftWrapAmount, lineItemContainer.currencyCode))
          : null;
    } else {
      giftWrapObj.charge = Resource.msg('summary.giftoption.free', 'checkout', null);
    }
  } else if (defaultShipment.custom.hasOwnProperty('giftWrapType') && defaultShipment.custom.giftWrapType === 'giftnote') {
    giftWrapObj.applied = true;
    giftWrapObj.charge = Resource.msg('summary.giftoption.free', 'checkout', null);
  }
  return giftWrapObj;
}
module.exports = totals;
