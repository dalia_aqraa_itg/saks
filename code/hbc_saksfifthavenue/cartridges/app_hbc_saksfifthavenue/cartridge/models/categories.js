/* eslint-disable no-undef */
'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var URLUtils = require('dw/web/URLUtils');

/**
 * Get category url
 * @param {dw.catalog.Category} category - Current category
 * @returns {string} - Url of the category
 */
function getCategoryUrl(category) {
  if (Object.hasOwnProperty.call(category.custom, 'topNavalturl') && category.custom.topNavalturl) {
    return category.custom.topNavalturl.markup;
  } else if (Object.hasOwnProperty.call(category.custom, 'alternativeUrl') && category.custom.alternativeUrl) {
    return category.custom.alternativeUrl.markup;
  }
  return URLUtils.url('Search-Show', 'cgid', category.getID()).toString();
}

/**
 * @Description Iterate the category to find l1 Category
 * @param {dw.catalog.Category} category - Get Top Level Category
 * @return {Object} category - returns category who's parent categoryID is root
 */
function getTopLevelCategory(category) {
  if (category.parent.ID === 'root') {
    return category;
  }
  return getTopLevelCategory(category.parent);
}

/**
 * Identifies the category level
 * @param {dw.catalog.Category} category - A single category
 * @param {number} level - Level of the category at the initial stage
 * @returns {number} level - Level of the category
 */
function getCategoryLevel(category, level) {
  if (!category.root) {
    return getCategoryLevel(category.parent, level + 1);
  }
  return level;
}

function getContentString(contentId) {
  var ContentMgr = require('dw/content/ContentMgr');
  var ContentModel = require('*/cartridge/models/content');

  var contentString = "";
  var apiContent = ContentMgr.getContent(contentId);

  if (apiContent) {
    var content = new ContentModel(
      apiContent,
      "components/content/contentAssetInc"
    );
    if (content.template) {
      var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
      contentString = renderTemplateHelper.getRenderedHtml({ content: content }, content.template);
    }
  }

  return contentString;
}

/**
 * Converts a given category from dw.catalog.Category to plain object
 * @param {dw.catalog.Category} category - A single category
 * @returns {Object} plain object that represents a category
 */
function categoryToObject(category) {
  if (!category.custom) {
    return null;
  }
  var result = {
    name:
      'categoryh1title' in category.custom && category.custom.categoryh1title
        ? category.custom.categoryh1title
        : 'categoryNameoverwrite' in category.custom && !!category.custom.categoryNameoverwrite
        ? category.custom.categoryNameoverwrite
        : category.getDisplayName(),
    url: getCategoryUrl(category),
    id: category.ID,
    level: getCategoryLevel(category, 0),
    parentID: category.parent.ID,
    parentName: getTopLevelCategory(category).displayName,
    showInMens: 'showInMens' in category.custom && category.custom.showInMens ? category.custom.showInMens : false,
    showInWomens: 'showInWomens' in category.custom && category.custom.showInWomens ? category.custom.showInWomens : false,
    showInDesktopNav: 'showInDesktopNav' in category.custom && category.custom.showInDesktopNav ? category.custom.showInDesktopNav : false,
    showInMobileNav: 'showInMobileNav' in category.custom && category.custom.showInMobileNav ? category.custom.showInMobileNav : false,
    showInRefinementMenu: 'showInRefinementMenu' in category.custom && category.custom.showInRefinementMenu ? category.custom.showInRefinementMenu : false,
    isClickable: 'isClickable' in category.custom && category.custom.isClickable ? category.custom.isClickable : false,
    OnlineWithoutProduct: 'OnlineWithoutProduct' in category.custom && category.custom.OnlineWithoutProduct ? category.custom.OnlineWithoutProduct : false,
    hexColor: 'hexColor' in category.custom && category.custom.hexColor ? category.custom.hexColor : '',
    imageIconnexttocategory:
      'imageIconnexttocategory' in category.custom && category.custom.imageIconnexttocategory ? category.custom.imageIconnexttocategory : null,
    iconCategorytitlereplacement:
      'iconCategorytitlereplacement' in category.custom && category.custom.iconCategorytitlereplacement ? category.custom.iconCategorytitlereplacement : null,
    columnNumber: 'columnNumber' in category.custom && category.custom.columnNumber ? category.custom.columnNumber : null,
    contentInnav1: 'contentInnav1' in category.custom && category.custom.contentInnav1 ? getContentString(category.custom.contentInnav1) : null,
    contentInnav2: 'contentInnav2' in category.custom && category.custom.contentInnav2 ? getContentString(category.custom.contentInnav2) : null,
    contentInnav3: 'contentInnav3' in category.custom && category.custom.contentInnav3 ? getContentString(category.custom.contentInnav3) : null,
    contentInnav4: 'contentInnav4' in category.custom && category.custom.contentInnav4 ? getContentString(category.custom.contentInnav4) : null,
    contentInnav5: 'contentInnav5' in category.custom && category.custom.contentInnav5 ? getContentString(category.custom.contentInnav5) : null,
    contentTemplatesize1: 'contentTemplatesize1' in category.custom && category.custom.contentTemplatesize1 ? category.custom.contentTemplatesize1 : null,
    contentTemplatesize2: 'contentTemplatesize2' in category.custom && category.custom.contentTemplatesize2 ? category.custom.contentTemplatesize2 : null,
    contentTemplatesize3: 'contentTemplatesize3' in category.custom && category.custom.contentTemplatesize3 ? category.custom.contentTemplatesize3 : null,
    contentTemplatesize4: 'contentTemplatesize4' in category.custom && category.custom.contentTemplatesize4 ? category.custom.contentTemplatesize4 : null,
    contentTemplatesize5: 'contentTemplatesize5' in category.custom && category.custom.contentTemplatesize5 ? category.custom.contentTemplatesize5 : null,
    hideFromSearchSuggestions: 'hideFromSearchSuggestions' in category.custom ? category.custom.hideFromSearchSuggestions : false
  };
  var subCategories = category.hasOnlineSubCategories() // eslint-disable-line
    ? category.getOnlineSubCategories()
    : result.OnlineWithoutProduct
    ? category.getOnlineSubCategories()
    : null;

  if (subCategories) {
    collections.forEach(subCategories, function (subcategory) {
      var converted = null;
      var onlineWithoutProduct =
        'OnlineWithoutProduct' in subcategory.custom && subcategory.custom.OnlineWithoutProduct ? subcategory.custom.OnlineWithoutProduct : false;
      if (subcategory.hasOnlineProducts() || subcategory.hasOnlineSubCategories() || onlineWithoutProduct) {
        converted = categoryToObject(subcategory);
      }
      if (converted) {
        if (!result.subCategories) {
          result.subCategories = [];
        }
        result.subCategories.push(converted);
      }
    });
    if (result.subCategories) {
      result.subCategories.sort(function (a, b) {
        return a.columnNumber - b.columnNumber;
      });
      result.complexSubCategories = result.subCategories.some(function (item) {
        return !!item.subCategories;
      });
    }
  }

  return result;
}

/**
 * Represents a single category with all of it's children
 * @param {dw.util.ArrayList<dw.catalog.Category>} items - Top level categories
 * @constructor
 */
function categories(items) {
  this.categories = [];
  collections.forEach(
    items,
    function (item) {
      var onlineWithoutProduct = 'OnlineWithoutProduct' in item.custom && item.custom.OnlineWithoutProduct ? item.custom.OnlineWithoutProduct : false;
      if (item.hasOnlineProducts() || item.hasOnlineSubCategories() || onlineWithoutProduct) {
        this.categories.push(categoryToObject(item));
      }
    },
    this
  );
}

module.exports = categories;
