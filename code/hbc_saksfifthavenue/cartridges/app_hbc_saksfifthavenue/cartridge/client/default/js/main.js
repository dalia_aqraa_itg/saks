window.jQuery = window.$ = require('jquery');
window.slick = window.Slick = require('slick-carousel/slick/slick');
window.perfectScrollbar = require('perfect-scrollbar/dist/perfect-scrollbar');
window.hoverintent = require('hoverintent/dist/hoverintent.min');
window.maskFun = require('jquery-mask-plugin/dist/jquery.mask.min');
require('focus-visible/dist/focus-visible.min');
const smoothscroll = require('smoothscroll-polyfill');
const processInclude = require('base/util');
const scrollDir = require('./util/scrollDirection');
window.isTouchscreen = require('core/util/isTouchscreen');
window.hbcSlider = require('./hbcSlider');

$(document).ready(function () {
  window.base = require('./product/baseV2');
  processInclude(require('core/util/loginPopup'));
  processInclude(require('./components/menu'));
  processInclude(require('./components/header'));
  processInclude(require('./components/stickyColumnTop'));
  processInclude(require('base/components/cookie'));
  processInclude(require('./components/consentTracking'));
  processInclude(require('./components/commissionCookies'));
  processInclude(require('./components/footer'));
  processInclude(require('./components/miniCart'));
  processInclude(require('core/components/collapsibleItem'));
  processInclude(require('core/components/clientSideValidation'));
  processInclude(require('base/components/toolTip'));
  processInclude(require('./formFields/formFields'));
  processInclude(require('./hbcSlider'));
  processInclude(require('core/components/slot'));
  processInclude(require('core/components/promo-tray-widget'));
  processInclude(require('./product/tileFocusManager'));
  //processInclude(require("./components/chanel"));
  processInclude(require('./util/viewportHeightCalculator'));
  if ($('input[name="isAtmEnabled"]').val() === 'true') {
    var atmHelper = require('./atm/atmHelper');
    var adobeTagManger = require('core/atm/atm');
    adobeTagManger(atmHelper);
  }
  smoothscroll.polyfill();
  scrollDir();
});

require('base/thirdParty/bootstrap');
require('base/components/spinner');

/**
 * Extend jQuery selectors to allow for :focusable
 */
jQuery.extend(jQuery.expr[':'], {
  focusable: function (el) {
    return $(el).is('a, button, :input, [tabindex]');
  }
});
jQuery.event.special.touchstart = {
  setup: function (_, ns, handle) {
    this.addEventListener('touchstart', handle, { passive: true });
  }
};
jQuery.event.special.touchend = {
  setup: function (_, ns, handle) {
    this.addEventListener('touchend', handle, { passive: true });
  }
};
jQuery.event.special.touchmove = {
  setup: function (_, ns, handle) {
    this.addEventListener('touchmove', handle, { passive: true });
  }
};
