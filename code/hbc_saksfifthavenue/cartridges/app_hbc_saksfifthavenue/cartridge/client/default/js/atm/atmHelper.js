'use strict';
var atmHelper = require('core/atm/atmHelper');

/**
 * Updates product array
 *
 * @param {function} callback - callback function
 */

/**
 * Provide product info
 * @returns {Object} product get product info from Cart
 */
function provideProductInfoInCart(pid) {
  var cartProdID = pid;
  var cartProd = $('.prdt-cart-details').find('[data-atm-selectedsku="' + cartProdID + '"]');
  var product = {};
  product.brand = cartProd.find('.brand-name').length ? $('.brand-name').first().text().trim() : '';
  product.code = cartProd.length ? cartProd.data('atm-code') : '';
  product.name = cartProd.find('.bfx-product-name').first().text().trim();
  var orgPrice = null;
  var tprice = null;
  if ($('.price .range').length) {
    orgPrice = cartProd.find('.bfx-list-price.formatted_price').first().data('unformatted-price').toString();
    tprice = cartProd.find('.bfx-list-price.formatted_price').last().data('unformatted-price').toString();
  } else {
    orgPrice = cartProd.find('.bfx-list-price.formatted_price').data('unformatted-price').toString();
    tprice = cartProd.find('.bfx-sale-price.formatted_price').length
      ? cartProd.find('.bfx-sale-price.formatted_price').data('unformatted-price').toString()
      : cartProd.find('.bfx-list-price.formatted_price').data('unformatted-price').toString();
  }
  product.original_price = orgPrice;
  product.price = tprice;
  product.quantity = cartProd.find('.bfx-product-qty').val().toString();
  product.selected_sku = cartProdID.toString();
  return product;
}

/**
 * Load product object
 * @param {Object} qproduct product from response
 * @returns {Object} product updated product
 */
function loadProductObject(qproduct) {
  var product = {};
  if (qproduct) {
    product.brand = qproduct.brand.name ? qproduct.brand.name : '';
    product.code = qproduct.masterProductID;
    product.name = qproduct.productName;
    var orgPrice = null;
    if (qproduct.price.type && qproduct.price.type === 'range') {
      orgPrice = qproduct.price.max.list === null ? qproduct.price.max.sales.value : qproduct.price.max.list.value;
    } else if (qproduct.productType === 'set') {
      orgPrice = qproduct.price.list.value === null ? qproduct.price.sales.value : qproduct.price.list.value;
    } else {
      orgPrice = qproduct.price.list === null ? qproduct.price.sales.value : qproduct.price.list.value;
    }
    product.original_price = orgPrice !== null ? orgPrice.toString() : '';
    product.price = qproduct.price.type && qproduct.price.type === 'range' ? qproduct.price.max.sales.value.toString() : qproduct.price.sales.value.toString();
    product.selected_sku = qproduct.id;
  }
  return product;
}

/**
 * Provide product info
 * @returns {Object} same day delivery product get product info from Cart
 */

function getsddProductsArray(data) {
  var productsarray = [];

  if (data.cartModel && data.cartModel.items && data.cartModel.items.length) {
    $.each(data.cartModel.items, function (index, item) {
      if (typeof item.sddDetails.unitsAvailable !== 'undefined' && item.sddDetails.unitsAvailable >= item.quantity && typeof ssdstoreid !== 'null') {
        var product = {};
        var orgPrice = null;

        product.sdd_stores_available = '1';
        product.sdd_stores_found = '1';
        product.sdd_zip_code = data.ssdPostal;
        product.brand = item.brand.name;
        product.code = item.masterProductID;
        product.name = item.productName;
        product.quantity = item.quantity.toString();
        product.selected_sku = item.id.toString();
        if (typeof item.price.type !== 'undefined' && item.price.type === 'range' && typeof item.price.max !== 'undefined') {
          product.price = item.price.max.sales.value.toString();
        } else {
          product.price = item.price.sales.value.toString();
        }

        if (typeof item.price.type !== 'undefined' && item.price.type === 'range' && typeof item.price.max !== 'undefined') {
          orgPrice = item.price.max.list !== null ? item.price.max.list.value : tem.price.max.sales.value;
        } else if (item.productType === 'set') {
          orgPrice = item.price.list.value === null ? item.price.sales.value : item.price.list.value;
        } else {
          orgPrice = item.price.list === null ? item.price.sales.value : item.price.list.value;
        }

        item.original_price = orgPrice !== null ? orgPrice.toString() : '';

        productsarray.push(product);
      }
    });
  }

  return productsarray;
}

/**
 * get selected stores
 * @returns {Object} array return all stores
 */
function getStores() {
  var refinementValuesArray = {};
  if ($('#bopisCheck:checked').length) {
    refinementValuesArray.name = $('a.change-store').text().trim();
    refinementValuesArray.type = 'selected';
  }
  return refinementValuesArray;
}

/**
 * Get refinement
 * @param {string} $this - current element
 * @returns {Object} - refinement object
 */
function getRefinement($this) {
  var refinements = [];
  $.each($('.refinements .refinement'), function (index, refinement) {
    var trefinement = {};
    trefinement.name = $(refinement).find('.card-header h2').text().trim();
    trefinement.values = [];
    var refinementValuesArray = {};
    if ($(refinement).hasClass('refinement-category') && $($this).find('.refinement-category').text()) {
      refinementValuesArray.name = $($this).find('.refinement-category').text().trim();
      refinementValuesArray.type = 'selected';
      trefinement.values.push(refinementValuesArray);
    } else if ($(refinement).hasClass('refinement-color')) {
      $.each($('.refinement-color .selected'), function (indexColor, colorRefinement) {
        refinementValuesArray = {};
        refinementValuesArray.name = $(colorRefinement).attr('title');
        refinementValuesArray.type = 'selected';
        trefinement.values.push(refinementValuesArray);
      });
    } else if ($(refinement).hasClass('refinement-price')) {
      $.each($('.refinement-price .selected'), function (indexPrice, priceRange) {
        refinementValuesArray.name = $(priceRange).closest('li').find('.bfx-price').text().trim();
        refinementValuesArray.type = 'selected';
        trefinement.values.push(refinementValuesArray);
      });

      if (trefinement.values.length === 0 && $('input[name="filterMinprice"]').length > 0 && $('input[name="filterMaxprice"]').length > 0) {
        var minVal = parseInt($('input[name$="filterMinprice"]').val());
        var maxVal = parseInt($('input[name$="filterMaxprice"]').val());
        if (!isNaN(minVal) && !isNaN(maxVal)) {
          refinementValuesArray.name = '$' + minVal + '-' + '$' + maxVal;
          refinementValuesArray.type = 'selected';
          trefinement.values.push(refinementValuesArray);
        }
      }

      if (trefinement.values.length === 0 && $('input[name$="minPrice"]').val() != '' && $('input[name$="maxPrice"]').val() != '') {
        var minVal = parseInt($('input[name$="minPrice"]').val());
        var maxVal = parseInt($('input[name$="maxPrice"]').val());
        if (!isNaN(minVal) && !isNaN(maxVal)) {
          refinementValuesArray.name = '$' + minVal + '-' + '$' + maxVal;
          refinementValuesArray.type = 'selected';
          trefinement.values.push(refinementValuesArray);
        }
      }
    } else if ($(refinement).hasClass('refinement-promotion')) {
      $.each($('.refinement-promotion .selected'), function (indexPromo, promoRefinement) {
        refinementValuesArray.name = $(promoRefinement).next('span').text().trim();
        refinementValuesArray.type = 'selected';
        trefinement.values.push(refinementValuesArray);
      });
    } else if ($(refinement).hasClass('refinement-sales--offers')) {
      $.each($('.refinement-sales--offers .selected'), function (indexPrice, promoRefinement) {
        refinementValuesArray.name = $(promoRefinement).next('span').text().trim();
        refinementValuesArray.type = 'selected';
        trefinement.values.push(refinementValuesArray);
      });
    } else if ($(refinement).hasClass('refinement-get-it-fast')) {
      $.each($('#refinement-get-it-fast input:checked'), function (indexPrice, getitfastRefinement) {
        refinementValuesArray.name = $(getitfastRefinement).val().trim();
        refinementValuesArray.type = 'selected';
        trefinement.values.push(refinementValuesArray);
      });
    } else {
      $.each($(refinement).find('ul li .selected'), function (indexother, attrRefinement) {
        refinementValuesArray = {};
        refinementValuesArray.name = $(attrRefinement).clone().children().remove().end().text().trim();
        refinementValuesArray.type = 'selected';
        trefinement.values.push(refinementValuesArray);
      });
    }
    refinements.push(trefinement);
  });
  var storeRefinement = {};
  storeRefinement.name = 'stores';
  storeRefinement.values = [];
  var storesArray = getStores();
  if (!jQuery.isEmptyObject(storesArray)) {
    storeRefinement.values.push(storesArray);
  }
  refinements.push(storeRefinement);
  return refinements;
}
/**
 * Get All Products
 * @returns {Object} - return all products
 */
function getProducts() {
  var products = [];
  $.each($('div[data-pid].product'), function (index, item) {
    var product = {};
    product.code = $(item).data('pid') ? $(item).data('pid').toString() : '';
    products.push(product);
  });
  return products;
}

/**
 * Provide product info
 * @returns {Object} product get product info from PDP
 */
function provideProductInfoInPdp() {
  var product = {};
  product.brand = $('.product-detail h1.product-brand-name').length ? $('.product-detail h1.product-brand-name').first().text().trim() : '';
  product.code = $('div[data-atm-code]').length ? $('div[data-atm-code]').data('atm-code') : '';
  product.name = $('.product-detail .product-name').first().text().trim();
  var orgPrice = null;
  var tprice = null;
  if ($('.price .range').length) {
    orgPrice = $('.product-detail .bfx-list-price.formatted_price').first().data('unformatted-price').toString();
    tprice = $('.product-detail .bfx-list-price.formatted_price').last().data('unformatted-price').toString();
  } else {
    orgPrice = $('.product-detail .bfx-list-price.formatted_price').data('unformatted-price').toString();
    tprice = $('.product-detail .bfx-sale-price.formatted_price').length
      ? $('.product-detail .bfx-sale-price.formatted_price').data('unformatted-price').toString()
      : $('.product-detail .bfx-list-price.formatted_price').data('unformatted-price').toString();
  }
  product.original_price = orgPrice;
  product.price = tprice;
  product.quantity = '1';
  product.selected_sku = $('.product-detail').data('pid') ? $('.product-detail').data('pid').toString() : '';
  return product;
}

/**
 * Provide product info
 * @returns {Object} product get product info from Saks landing page
 */
function provideProductInfoInSaksplus() {
  var product = {};
  product.brand = $('input[class*="saksplus-product-brandname"]').data('brandname').toString();
  product.code = $('input[class*="saksplus-product-atm-code"]').data('atm-code').toString();
  product.name = $('input[class*="saksplus-product-name"]').data('product-name').toString();
  product.original_price = $('input[class*="saksplus-product-price"]').data('price').toString();
  product.price = $('input[class*="saksplus-product-price"]').data('price').toString();
  product.quantity = '1';
  product.selected_sku = $('input[class*="product-detail"]').data('pid').toString();
  return product;
}

atmHelper.productArrayUpdate = function (callback) {
  $('body').on('adobeTagManager:productArrayUpdate', function (e, $this) {
    var refinements = getRefinement($this);
    var sortType = 'default';
    var selectedOption = $('.sort-selection-block').find('select[name="sort-order"]')[0].selectedOptions[0];
    if ($(selectedOption).index() > 0) {
      sortType = 'selected';
    }
    var productArray = {
      array_page_number: pageData.product_array ? pageData.product_array.array_page_number : '1', //eslint-disable-line
      model_view: pageData.product_array ? pageData.product_array.model_view : 'off',
      refinements: refinements,
      results_per_page: getProducts().length ? getProducts().length.toString() : '0',
      sort: {
        name: $(selectedOption).data('id'),
        type: sortType
      },
      total_results: $('.search-count').data('search-count') ? $('.search-count').data('search-count').toString() : '0',
      results_across: pageData.product_array ? pageData.product_array.results_across : '1' //eslint-disable-line
    };
    if (callback) {
      callback(productArray, getProducts());
    }
    if (window.pageData) {
      window.pageData.product_array = productArray;
      window.pageData.products = getProducts();
    }
    if (window.pageDataObj) {
      window.pageDataObj.product_array = productArray;
      window.pageDataObj.products = getProducts();
    }
  });
};

/**
 * On step change of checkout
 * @param {function} callback - callback function
 */

atmHelper.checkoutStepChange = function (callback) {
  $('body').on('adobeTagManager:checkoutStepChange', function (e, steps) {
    if ($('body').find('.is-invalid').length > 0) {
      return;
    }
    var page = {};
    if (!pageData.visitor) {
      pageData.visitor = {};
    }
    pageData.visitor.amex_points_available = steps.hasAmexPoint;
    pageData.visitor.amex_points_check = false;
    pageData.visitor.amex_points_apply = false;
    page.checkoutStep = steps.step;
    pageData.visitor.email_opt_in = $('#add-to-email-list').length && steps.step === 'payment' ? $('#add-to-email-list').prop('checked').toString() : 'false';
    if (steps.optIn) {
      pageData.visitor.email_opt_in = 'true';
    }
    pageData.visitor.new_account = steps.new_account != 'undefined' && steps.new_account ? 'true' : 'false';
    pageData.visitor.new_payment = steps.newpayment != 'undefined' && steps.newpayment ? 'true' : 'false';
    if (callback) {
      callback(page, pageData.products, pageData.visitor);
    }
  });
};

atmHelper.checkAmexPoints = function (callback) {
  $("body").on("checkout:updateAmexForm", function (e) {
    var orderTotal = parseFloat(
      $(".grand-total-sum").text().replace("$", "").replace(",", "")
    );
    var amexAmount = parseFloat($("#amex-amount").data("amount"));
    var maxTotal = orderTotal;

    $("#amex-amount").data("amount", maxTotal);
    $(".amex-order-total").text("$" + maxTotal);
    if (!pageData.visitor) {
      pageData.visitor = {};
    }
    pageData.visitor.amex_points_check = true;
    if (callback) {
      callback();
    }
  });
};

atmHelper.applyAmexPoints = function (callback) {
  $('body').on('checkout:applyAmex', function (e) {
    if ($('body').find('.is-invalid').length > 0) {
      return;
    }
    if (!pageData.visitor) {
      pageData.visitor = {};
    }
    pageData.visitor.amex_points_apply = true;
    if (callback) {
      callback();
    }
  });
};

/**
 * Quick view events
 * @param {function} callback - Callback function
 */
atmHelper.showQuickView = function (callback) {
  $('body').on('adobeTagManager:showQuickView', function (e, qproduct) {
    var products = [];
    var product = loadProductObject(qproduct);
    product.average_rating = qproduct.starRating ? qproduct.starRating.toString() : '';
    delete product.selected_sku;
    product.skus = qproduct.allAvailableProducts;
    var priceType = '';
    if (qproduct.badge.isClearance) {
      priceType = 'clearance';
    } else if (qproduct.badge.isFinalSale) {
      priceType = 'final sale';
    }
    product.tags = {
      feature_type: qproduct.featuredType.value ? qproduct.featuredType.value : '',
      inventory_label: qproduct.availability.messages && qproduct.availability.messages.length > 0 ? qproduct.availability.messages[0] : '',
      pip_text: qproduct.promotions && qproduct.promotions.length > 0 ? qproduct.promotions[0].calloutMsg : '',
      price_type: priceType,
      publish_date: qproduct.preOrder && qproduct.preOrder.applicable == true ? 'preorder' : 'new',
      returnable: (!qproduct.isNotReturnable.value).toString()
    };
    product.total_reviews = qproduct.turntoReviewCount ? qproduct.turntoReviewCount.toString() : '';
    products.push(product);
    if (callback) {
      callback(products);
    }
  });
};

/**
 * trigger on select modelview
 * @param {function} callback function
 */

atmHelper.toggleModelView = function (callback) {
  $('body').on('adobeTagManager:toggleModelView', function (e, $this) {
    var enablemodelview = 'off';
    if ($($this).hasClass('model-view-on')) {
      enablemodelview = 'on';
    }
    var refinements = getRefinement($this);
    var sortType = 'default';
    if ($('.sort-selection-block').find('select[name="sort-order"] option:selected').index() > 0) {
      sortType = 'selected';
    }
    if ($($this).closest('.wishlist-header-wrap').length > 0) {
      var productArray = {
        array_page_number: pageData.product_array ? pageData.product_array.array_page_number : '1', //eslint-disable-line
        model_view: enablemodelview,
        results_per_page: getProducts().length ? getProducts().length.toString() : '0',
        total_results: getProducts().length ? getProducts().length.toString() : '0',
        results_across: pageData.product_array ? pageData.product_array.results_across : '1' //eslint-disable-line
      };
    } else {
      var productArray = {
        array_page_number: pageData.product_array ? pageData.product_array.array_page_number : '1', //eslint-disable-line
        model_view: enablemodelview,

        refinements: refinements,
        results_per_page: getProducts().length ? getProducts().length.toString() : '0',
        sort: {
          name: $('.sort-selection-block').find('select[name="sort-order"] option:selected').data('id'),
          type: sortType
        },
        total_results: $('.search-count').data('search-count') ? $('.search-count').data('search-count').toString() : '0',
        results_across: pageData.product_array ? pageData.product_array.results_across : '1' //eslint-disable-line
      };
    }

    if (callback) {
      callback(productArray, getProducts());
    }
  });
};

/**
 * Wait list start
 * @param {function} callback function
 */
atmHelper.waitListStart = function (callback) {
  $('body').on('adobe:waitListStart', function () {
    var products = [];
    if ($('#waitlist-form-saksplus').length) {
      var product = provideProductInfoInSaksplus();
    } else {
      var product = provideProductInfoInPdp();
      product.quantity = $('.quantity-val').val();
    }
    products.push(product);
    if (callback) {
      callback(products);
    }
  });
};

/**
 * Wait list complete
 * @param {function} callback function
 */

atmHelper.waitListComplete = function (callback) {
  $('body').on('adobe:waitListComplete', function () {
    var products = [];
    if ($('#waitlist-form-saksplus').length) {
      var product = provideProductInfoInSaksplus();
    } else {
      var product = provideProductInfoInPdp();
      product.quantity = $('.quantity-val').val();
    }
    products.push(product);
    if (callback) {
      callback(products);
    }
  });
};

/**
 * trigger on cancel order
 * @param {function} callback - callback function
 */
atmHelper.cancelOrder = function (callback) {
  $('body').on('adobeTagManager:cancelOrder', function (e, orderNo) {
    if (callback) {
      callback(orderNo);
    }
  });
};

/**
 * Move cart to wishlist
 * @param {function} callback - callback function
 */
atmHelper.cartMoveToFav = function (callback) {
  $('body').on('adobeTagManager:cartMoveToFav', function (e, product) {
    var products = [];
    products.push(product);
    if (callback) {
      callback(products);
    }
  });
};

atmHelper.sddPDPStart = function (callback) {
  $('body').on('click', '.atm-sdd', function () {
    var products = [];
    var product;
    product = provideProductInfoInPdp();
    product.quantity = $('.quantity-val').val();
    products.push(product);
    if (callback) {
      callback(products);
    }
  });
};

atmHelper.sddPDPSearch = function (callback) {
  $('body').on('adobe:sddSearch', function (e, data) {
    var products = [];
    var storesfound = '0';
    var availableStore = '';
    var product = provideProductInfoInPdp();
    if (data && data.storeid && typeof data.unitsAvailable !== 'undefined' && data.unitsAvailable > 0) {
      availableStore = data.storeid;
      storesfound = '1';
    }
    product.sdd_stores_available = storesfound;
    product.sdd_stores_found = storesfound;
    product.sdd_zip_code = data.postalCode;
    products.push(product);
    if (callback) {
      callback(products);
    }
  });
};

atmHelper.sddCartSearch = function (callback) {
  $('body').on('adobe:sddCartSearch', function (e, data) {
    if (data) {
      var products = getsddProductsArray(data);
    }
    if (callback) {
      callback(products);
    }
  });
};

atmHelper.sddCartSelect = function (callback) {
  $('body').on('adobe:sddCartSelect', function (e) {
    var products = [];
    if ($('.product-info').length > 0) {
      $('.card')
        .closest('.product-info')
        .each(function () {
          if ($(this).data('issddeligibile')) {
            var pid = $(this).data('atm-selectedsku');
            var product = provideProductInfoInCart(pid);
            products.push(product);
          }
        });
    }
    if (callback) {
      callback(products);
    }
  });
};

atmHelper.selectbonusProduct = function (callback) {
  $('body').on('adobe:selectbonusProduct', function (e, response) {
    var products = [];
    $.each(response.selectedbonusitems, function (index, item) {
      products.push(item);
    });
    if (callback) {
      callback(products);
    }
  });
};

atmHelper.getbonusproductonAppliedCoupon = function (callback) {
  $('body').on('adobe:getbonusproductonAppliedCoupon', function (e, item) {
    var products = [];
    products.push(item);
    if (callback) {
      callback(products);
    }
  });
};

/**
 * Used for search redirects
 * @param {function} callback - callback function
 */
atmHelper.searchRedirect = function (callback) {
  $('body').on('click', 'form.search-form button[name="search-button"]', function (e) {
    var searchTerm = $(this).siblings('input.search-field').val();
    var search = {
      term: searchTerm
    };
    if (callback) {
      callback(search);
    }
  });
};

/**
 * This ATM is used for search sugestion objec
 * @param {function} callback - callback function
 */
atmHelper.suggestedSearch = function (callback) {
  $('body').on('click', '.search-suggestions .search-atm', function () {
    var $this = $(this);
    var search = {
      term: $this.data('term'),
      type: $(this).closest('.search-suggestions').find('header h3').data('atm-searchtype'),
      typedText: $('.search-form .search-field').val()
    };
    if (callback) {
      callback(search);
    }
    return true;
  });
};

module.exports = atmHelper;
