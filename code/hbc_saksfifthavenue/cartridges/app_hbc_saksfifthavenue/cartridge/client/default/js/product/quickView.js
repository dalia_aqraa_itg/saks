'use strict';
var quickview = require('core/product/quickView');
var base = require('./base');

quickview.updateAddToCart = function () {
  $('body').on('product:updateAddToCart', function (e, response) {
    // update local add to cart (for sets)
    $('button.add-to-cart', response.$productContainer).attr('disabled', !response.product.readyToOrder || !response.product.available);

    // update global add to cart (single products, bundles)
    var dialog = $(response.$productContainer).closest('.quick-view-dialog');
    $('.add-to-cart-global', dialog).data('readytoorder', response.product.readyToOrder && response.product.available);
    $('.add-to-cart-global', dialog).data('readytoordertext', response.product.readyToOrderMsg);
    if (response.product.preOrder && response.product.preOrder.applicable && response.product.preOrder.applicable === true) {
      $('button.add-to-cart-global').text(response.product.preOrder.preorderButtonName);
      if (response.product.preOrder.shipDate) {
        $('div .preorder-ship-date').text(response.product.preOrder.shipDate);
      }
    } else {
      $('button.add-to-cart-global').text(response.product.availability.buttonName);
      $('div .preorder-ship-date').empty();
    }
  });
};

quickview.addToCart = base.addToCart;
quickview.availability = base.availability;

module.exports = quickview;
