'use strict';

$('.catlanding-chanel .chanel-categories')
  .find('.dropsown-content .dropdown-item:not(:first-of-type) .cat-link, .dropdown-menu2 > .dropdown.dropdown-item > .dropdown-toggle')
  .on('click', function (e) {
    e.preventDefault();
    $(this).closest('.dropdown-item').siblings('li').removeClass('show').find('li').removeClass('show');
    $(this).closest('.dropdown-item').toggleClass('show');
  });
