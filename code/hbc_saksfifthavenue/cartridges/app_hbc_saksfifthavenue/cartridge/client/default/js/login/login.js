'use strict';
var base = require('core/login/login');
var formValidation = require('core/components/formValidation');

base.resetPassword = function () {
  $('body .reset-password-form').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    // eslint-disable-next-line no-undef
    grecaptcha.ready(function () {
      // eslint-disable-next-line no-undef
      grecaptcha
        .execute($('.google-recaptcha-key').html(), {
          action: 'forgotpassword'
        })
        .then(function (token) {
          $('.g-recaptcha-token').val(token);

          var url = form.attr('action');
          $('.forgot-password-error').empty().removeClass('invalid-email-alert');

          form.spinner().start();
          $('.reset-password-form').trigger('login:register', e);
          $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: form.serialize(),
            success: function (data) {
              form.spinner().stop();
              if (!data.success && !data.invalid) {
                formValidation(form, data);
              } else if (data.invalid) {
                $('.forgot-password-error').text(data.invalidMsg).addClass('invalid-email-alert').show();
                $('#reset-password-email').val('');
                $('#reset-password-email').parent().find('label').removeClass('input-focus');
                $('#reset-password-email').next('span').remove();
              } else {
                $('.js-forgot-container').remove();
                $('#reset-message1').text(data.resetMessage1);
                $('#reset-message2').text(data.resetMessage2);
                $('.js-container-success').removeClass('d-none');
              }
              if (data.botError) {
                form.find('[id="submitEmailButton"]').attr('disabled', 'disabled');
              }
            },
            error: function () {
              if (data.botError) {
                form.find('[id="submitEmailButton"]').attr('disabled', 'disabled');
              }

              form.spinner().stop();
            }
          });
          return false;
        });
    });
  });
  $('body .reset-password-form')
    .find('#reset-password-email')
    .on('keypress', function () {
      if (!$('.forgot-password-error').is(':empty')) {
        $('.forgot-password-error').slideUp('slow');
      }
    });
};

base.enableOptInCheckbox = function () {
  $('.js-canadian-customer').on('change', function () {
    var zipCodeField = $(this).closest('form').find('.saks-zip-code');
    if (zipCodeField.val()) {
      zipCodeField.val('');
      zipCodeField.hasClass('is-invalid') ? zipCodeField.removeClass('is-invalid') : '';
      zipCodeField.next('label').removeClass('is-invalid').removeClass('input-focus');
      zipCodeField.next('span') ? zipCodeField.next('span').remove() : '';
      zipCodeField.parents('.form-group').find('.invalid-feedback').empty();
    }
    if ($('.js-canadian-customer:checked').val() === 'T') {
      $('.js-email-pref-checkobox').removeClass('d-none');
      $('.saks-canada-opt').prop('checked', false);
      $('.js-saks-canadat-opt').addClass('d-none');
      $('.customer-apply-msg').removeClass('d-none');
      $('.create-apply-btn-label').addClass('d-none');
      $('.create-btn-label').removeClass('d-none');
      if (zipCodeField) {
        zipCodeField.closest('div').find('.saks-zip-text').text($(this).attr('data-ziptext'));
        zipCodeField.removeClass('us-pattern').addClass('ca-pattern');
        zipCodeField.attr('data-pattern-mismatch', $(this).attr('data-postal-error'));
      }
    } else {
      $('.js-email-pref-checkobox').addClass('d-none');
      $('.js-saks-canadat-opt').removeClass('d-none');
      $('#add-to-email-list').prop('checked', false);
      $('.customer-apply-msg').addClass('d-none');
      $('.create-apply-btn-label').removeClass('d-none');
      $('.create-btn-label').addClass('d-none');
      if (zipCodeField) {
        zipCodeField.closest('div').find('.saks-zip-text').text($(this).attr('data-ziptext'));
        zipCodeField.removeClass('us-pattern').addClass('us-pattern');
        zipCodeField.attr('data-pattern-mismatch', $(this).attr('data-postal-error'));
      }
    }
  });
};

base.submitNewPassword = function () {
  $('body').on('click', '.submit-new-password', function (e) {
    e.preventDefault();
    // eslint-disable-next-line no-undef
    grecaptcha.ready(function () {
      // eslint-disable-next-line no-undef
      grecaptcha
        .execute($('.google-recaptcha-key').html(), {
          action: 'updatepassword'
        })
        .then(function (token) {
          $('.g-recaptcha-token').val(token);
          var regex = new RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/);
          var password = $('#newPassword').val();
          var cnfPassword = $('#newPasswordConfirm').val();
          var $this = $('.confirm-password #newPasswordConfirm');
          if (password !== '') {
            if (password === cnfPassword) {
              if ($this.hasClass('is-invalid')) {
                $this.removeClass('is-invalid');
                $this.prev('label').removeClass('is-invalid');
                if ($this.next('span').hasClass('invalid')) {
                  $this.next('span').removeClass('invalid').addClass('valid');
                }
                $this.parents('.form-group').find('.invalid-feedback').empty();
              }
              if (!regex.test(password)) {
                $('#newPassword').addClass('is-invalid');
              } else {
                $('form.password-reset-form').submit();
              }
            } else {
              $this.addClass('is-invalid');
              $this.prev('label').addClass('is-invalid');
              if ($this.next('span').length === 0) {
                $('<span></span>').insertAfter($this);
              }
              $this.next('span').addClass('invalid');
              if ($this.next('span').hasClass('valid')) {
                $this.next('span').removeClass('valid').addClass('invalid');
              }
              $this.parents('.form-group').find('.invalid-feedback').text($this.data('pattern-pwd-mismatch')).show();
            }
          } else {
            $('.password-condition').addClass('d-none');
          }
        });
    });
  });
};

base.phoneFocusKey = function () {
  $('body').on('focus keyup', '#registration-form-phone', function () {
    if ($(this).val() !== '') {
      $(this).prop('required', true);
    } else {
      $(this).prop('required', false);
    }
  });
};

base.validatePhoneNumber = function () {
  $('#registration-form-phone').on('blur', function () {
    if ($(this).val() !== '') {
      $(this).prop('required', true);
    } else if ($(this).hasClass('is-invalid')) {
      $(this).prop('required', false);
      $(this).removeClass('is-invalid');
      $(this).next('.invalid').remove();
      $(this).next('.valid').remove();
      $(this).next('label').removeClass('is-invalid');
    } else {
      $(this).prop('required', false);
      $(this).next('.invalid').remove();
      $(this).next('.valid').remove();
      $(this).next('label').removeClass('is-invalid');
    }
  });
};

base.login = function () {
  $('form.login').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    // eslint-disable-next-line no-undef
    grecaptcha.ready(function () {
      // eslint-disable-next-line no-undef
      grecaptcha.execute($('.google-recaptcha-key').html(), { action: 'login' }).then(function (token) {
        $('.g-recaptcha-token').val(token);
        var url = form.attr('action');
        form.spinner().start();
        $('form.login').trigger('login:submit', e);
        $.ajax({
          url: url,
          type: 'post',
          dataType: 'json',
          data: form.serialize(),
          success: function (data) {
            form.spinner().stop();
            if (!data.success) {
              formValidation(form, data);
              $('form.login').trigger('login:error', data);
            } else {
              $('form.login').trigger('login:success', data);
              location.href = data.redirectUrl;
            }
            if (data.botError) {
              $('form.login button.account-btn').attr('disabled', 'disabled');
              $('form.login button.sign-up-btn').attr('disabled', 'disabled');
            }
          },
          error: function (data) {
            if(data.responseJSON.action == 'CSRF-AjaxFail') {
              window.location.href = data.responseJSON.failLoginRedirectURL;
            } else if (data.responseJSON.redirectUrl) {
              window.location.href = data.responseJSON.redirectUrl;
            } else {
              $('form.login').trigger('login:error', data);
              form.spinner().stop();
            }
            if (data.botError) {
              $('form.login button.account-btn').attr('disabled', 'disabled');
              $('form.login button.sign-up-btn').attr('disabled', 'disabled');
            }
          }
        });
        return false;
      });
    });
  });
};

module.exports = base;
