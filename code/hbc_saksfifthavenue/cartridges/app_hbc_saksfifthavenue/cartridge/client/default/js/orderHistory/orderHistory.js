'use strict';

var hbcTooltip = require('core/tooltip');
var orderCancel = require('../orderStatus/orderCancel');
var teaser = require('core/teaserOrderDetails/teasersOD');

module.exports = function () {
  $('body').on('change', '.order-history-selectbox', function (e) {
    var $ordersContainer = $('.order-list-container');
    $ordersContainer.empty();
    $.spinner().start();
    $('.order-history-select').trigger('orderHistory:sort', e);
    $.ajax({
      url: e.currentTarget.value,
      method: 'GET',
      success: function (data) {
        $ordersContainer.html(data);
        $.spinner().stop();
      },
      error: function (err) {
        if (err.responseJSON.redirectUrl) {
          window.location.href = err.responseJSON.redirectUrl;
        }
        $.spinner().stop();
      }
    });
  });

  $('body').on('change', '.pending-orders', function (e) {
    if ($(this).is(':checked')) {
      var url = $(this).attr('data-url') + '&pendingOrders=true';
    } else {
      var url = $(this).attr('data-url') + '&pendingOrders=false';
    }
    var $ordersContainer = $('.order-list-container');
    $ordersContainer.empty();
    $.spinner().start();
    $.ajax({
      url: url,
      method: 'GET',
      success: function (data) {
        $ordersContainer.html(data);
        $.spinner().stop();
      },
      error: function (err) {
        if (err.responseJSON.redirectUrl) {
          window.location.href = err.responseJSON.redirectUrl;
        }
        $.spinner().stop();
      }
    });
  });

  $('body').on('click', '.view-more-orders button', function (e) {
    e.preventDefault();
    $.spinner().start();
    $.ajax({
      url: $(this).data('action'),
      data: {
        prevOrder: $(this).data('prevorder')
      },
      method: 'GET',
      success: function (data) {
        $('.view-more-orders').replaceWith(data);
        $.spinner().stop();
      },
      error: function () {
        $.spinner().stop();
      }
    });
  });

  $('body').on('click', '.order-detail-link', function (e) {
    $('.order-cannot-cancel').addClass('d-none');
    $('.order-cancel-oms-fail').addClass('d-none');
    e.preventDefault();
    $.spinner().start();
    var orderNo = $(this).data('orderno');
    var $detailsContainer = $('.order-details-section');
    var $detailsSection = $('.order-details');
    var $historyContainer = $('.order-history-section');

    $detailsSection.empty();
    $.spinner().start();
    $.ajax({
      url: $(this).data('href'),
      method: 'GET',
      data: {
        ajax: true
      },
      success: function (data) {
        if (!data.error) {
          $(window).scrollTop(0);
          $detailsSection.html(data);
          $detailsContainer.removeClass('d-none');
          $historyContainer.addClass('d-none');
          teaser.createReview();
          hbcTooltip.tooltipInit();
        }
        $.spinner().stop();
      },
      error: function () {
        $.spinner().stop();
      }
    });
  });

  $('body').on('click', '.go-back-to-history', function (e) {
    e.preventDefault();
    $.spinner().start();
    var $detailsContainer = $('.order-details-section');
    var $historyContainer = $('.order-history-section');
    var $detailsSection = $('.order-details');
    if (!$('.order-cannot-cancel').hasClass('d-none')) {
      $('.order-cannot-cancel').addClass('d-none');
    }
    $detailsSection.empty();
    $detailsContainer.addClass('d-none');
    $historyContainer.removeClass('d-none');
    $.spinner().stop();
  });

  orderCancel();
};
