'use strict';
var floatLabel = require('core/floatLabel');
var scrollAnimate = require('base/components/scrollAnimate');

/**
 * fill modal gc form
 *
 * @param {string} checkBalanceUrl - check balance url to be hit
 * @param {string} addGiftCardUrl - add gift card url to be hit
 *
 */
function applySaksGiftCard(applyGCUrl) {
  $.spinner().start();

  $.ajax({
    url: applyGCUrl,
    method: 'POST',
    success: function (resp) {
      if (resp.error) {
        $('.error-message-text').empty();
        $('.error-message').show();
        $('.error-message-text').text(resp.error);
        scrollAnimate($('.error-message'));
        $.spinner().stop();
      } else {
        if (resp.order.saksGiftCard.hasSaksGiftCard) {
          $('.saks-rewards-container').empty().html(resp.order.saksFirstCheckoutViewHtml);
          $('.order-summary-payment-applied').empty();
          $('.order-summary-payment-applied').html(resp.order.orderSummaryPaymentHtml);
          $('li.nav-item.paypal').addClass('paypal-disable');
          $('#payPalRadioButton').attr('disabled', true);
        }
        if (resp.amountFinished) {
          $('.gift-card-payment input').val('');
          $('.gift-card-payment input').next('span').remove();
          $('.gift-card-payment input').prev('label').removeClass('is-invalid').removeClass('input-focus');
          $('.gift-card-form-group input').attr('disabled', 'disabled');
          $('.gift-card-form-group .giftcard-apply-submit button').attr('disabled', 'disabled');
        }
        $('.generic-error').empty();
        $('.generic-error').addClass('d-none');
        floatLabel.resetFloatLabel();
        $.spinner().stop();
      }
    },
    error: function (errorResp) {
      if (errorResp.responseJSON.redirectUrl) {
        window.location.href = errorResp.responseJSON.redirectUrl;
      }
      // reCaptcha.callToken('payment');
      $('.gift-card-payment input').val('');
      $('.gift-card-payment input').prev('label').removeClass('input-focus');
      $.spinner().stop();
    }
  });
}

/**
 * remove gift card from basket
 *
 * @param {url} removeGiftCardUrl - remove gift card url
 * @param {number} giftCard - gift card number to be removed
 */
function removeGiftCard(removeGCUrl) {
  $.spinner().start();
  $.ajax({
    url: removeGCUrl,
    method: 'POST',
    success: function (responseData) {
      if (responseData.error) {
        $('.error-message-text').empty();
        $('.error-message').show();
        $('.error-message-text').text(responseData.errorMessage);
        scrollAnimate($('.error-message'));
        $.spinner().stop();
      } else {
        $('.saks-rewards-container').empty().html(responseData.saksFirstCheckoutViewHtml);
      }
      if (responseData.giftCard.hasGiftCard) {
        $('.gift-card-applied').empty();
        $('.gift-card-applied').html(responseData.giftCard.giftCardHtml);
        if (!$('li.nav-item.paypal').hasClass('paypal-disable')) {
          $('li.nav-item.paypal').addClass('paypal-disable');
          $('#payPalRadioButton').attr('disabled', true);
        }
      } else {
        $('.gift-card-applied').empty();
        $('li.nav-item.paypal').removeClass('paypal-disable');
        $('#payPalRadioButton').removeAttr('disabled');
      }
      $('.order-summary-payment-applied').empty();
      $('.order-summary-payment-applied').html(responseData.orderSummaryPaymentHtml);
      $('.gift-card-payment input').prev('label').removeClass('input-focus');
      $('.gift-card-payment input').val('');
      $('.credit-card-form input').removeAttr('disabled');
      $('.credit-card-content [name=dwfrm_billing_paymentMethod]').val('CREDIT_CARD');
      $('.payment-information').data('payment-method-id', 'CREDIT_CARD');
      $('.gift-card-form-group input').removeAttr('disabled');
      $('.gift-card-form-group .giftcard-apply-submit button').removeAttr('disabled');
      if ($('.saved-payment-instrument-section').length) {
        $('.saved-payment-security-code').removeAttr('disabled');
        $('.credit-card-selection-new').removeClass('disabled');
      }
      // $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
      $('.enableBillingButton').val(false);
      $.spinner().stop();
    },
    error: function (errorRsp) {
      if (errorRsp.responseJSON.redirectUrl) {
        window.location.href = errorRsp.responseJSON.redirectUrl;
      }
      $.spinner().stop();
    }
  });
}

module.exports = {
  addSaksFirstGiftCard: function () {
    $('body').on('click', '.js-apply-saks-gc', function (e) {
      e.preventDefault();

      var applyGCUrl = $(this).data('action');
      // eslint-disable-next-line no-undef
      grecaptcha.ready(function () {
        // eslint-disable-next-line no-undef
        grecaptcha
          .execute($('.google-recaptcha-key').html(), {
            action: 'SaksGCpayment'
          })
          .then(function (token) {
            $('.g-recaptcha-token').val(token);
            applySaksGiftCard(applyGCUrl);
          });
      });
    });
  },

  removeGiftCardAction: function () {
    $('body').on('click', '.saks-rewards-container .js-remove-saks-gc', function (e) {
      e.preventDefault();
      var removeGCUrl = $(this).data('action');
      // eslint-disable-next-line no-undef
      grecaptcha.ready(function () {
        // eslint-disable-next-line no-undef
        grecaptcha.execute($('.google-recaptcha-key').html(), { action: 'gcpayment' }).then(function (token) {
          $('.g-recaptcha-token').val(token);
          removeGiftCard(removeGCUrl);
        });
      });
    });
  }
};
