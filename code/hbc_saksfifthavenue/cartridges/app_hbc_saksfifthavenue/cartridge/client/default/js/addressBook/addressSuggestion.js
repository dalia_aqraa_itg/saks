'use strict';

/**
 * Loads the js again in the page
 * @param {string} src Script to be reloaded on the page
 */
function loadJS(src) {
  $('script[src="' + src + '"]').remove();
  $('<script>').attr('src', src).appendTo('head');
  $('.edq-global-intuitive-address-suggestions ').remove();
}

module.exports = {
  loadSuggestions: function () {
    $('#address1').on('click', function () {
      window.EdqConfig.GLOBAL_INTUITIVE_ELEMENT = document.getElementById(this.id);
      var addressBlock = $(this).parents("[class*='address-form']");
      window.EdqConfig.GLOBAL_INTUITIVE_MAPPING = [
        {
          field: addressBlock.find("input#address1")[0],
          elements: ['address.addressLine1']
        },
        {
          field: addressBlock.find("input#address2")[0],
          elements: ['address.addressLine2']
        },
        {
          field: addressBlock.find("input#city")[0],
          elements: ['address.locality']
        },
        {
          field: addressBlock.find("select#state")[0],
          elements: ['address.province']
        },
        {
          field: addressBlock.find("input#zipCode")[0],
          elements: ['address.postalCode']
        }
      ];
      loadJS(window.edq.src);
    });
  }
}
