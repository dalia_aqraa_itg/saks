'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
  processInclude(require('core/addressBook/addressBook'));
  processInclude(require('./addressBook/addressSuggestion'));
});
