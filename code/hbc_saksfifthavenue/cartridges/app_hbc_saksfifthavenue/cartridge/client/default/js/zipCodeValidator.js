'use strict';

module.exports = {
  validateZip: function ($target) {
    $target.on('keyup', function () {
      var zipCode = $(this).val();
      //For US zip code only
      if (/^[0-9]{5}(-[0-9]{4})?$/.test(zipCode.substring(0, 5)) && $(this).attr('pattern') === '(^[0-9]{5}(-[0-9]{4})?$)') {
        if (zipCode.length === 6 && zipCode.indexOf('-') === 5) {
          return;
        }
        if (zipCode.length === 7 && zipCode.lastIndexOf('-') === 6) {
          $(this).val(zipCode.substring(0, zipCode.length - 1));
          return;
        }
        //For replacing/adding - after 6 digits
        if (zipCode.length >= 6 && zipCode.indexOf('-') === -1) {
          var formattedString = zipCode
            .replace(/ /g, '')
            .replace(/(\w{5})/, '$1-')
            .replace(/(-+)/, '-')
            .trim();
          $(this).val(formattedString);
        }
      }
    });
  }
};
