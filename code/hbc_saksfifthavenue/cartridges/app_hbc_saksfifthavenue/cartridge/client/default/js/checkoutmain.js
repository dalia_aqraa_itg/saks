window.jQuery = window.$ = require('jquery');
const processInclude = require('base/util');
const scrollDir = require('./util/scrollDirection');
window.isTouchscreen = require('core/util/isTouchscreen');

$(document).ready(function () {
  processInclude(require('./components/stickyColumnTop'));
  processInclude(require('base/components/cookie'));
  processInclude(require('./components/commissionCookies'));
  processInclude(require('./components/footer'));
  processInclude(require('./components/miniCart'));
  processInclude(require('core/components/collapsibleItem'));
  processInclude(require('core/components/clientSideValidation'));
  processInclude(require('base/components/toolTip'));
  processInclude(require('./formFields/formFields'));
  processInclude(require('./hbcSlider'));
  processInclude(require('./giftcard/giftcard'));
  processInclude(require('./util/viewportHeightCalculator'));
  if ($('input[name="isAtmEnabled"]').val() === 'true') {
    var atmHelper = require('./atm/atmHelper');
    var adobeTagManger = require('core/atm/atm');
    adobeTagManger(atmHelper);
  }
  scrollDir();
});

require('base/thirdParty/bootstrap');
require('base/components/spinner');

/**
 * Extend jQuery selectors to allow for :focusable
 */
jQuery.extend(jQuery.expr[':'], {
  focusable: function (el) {
    return $(el).is('a, button, :input, [tabindex]');
  }
});
