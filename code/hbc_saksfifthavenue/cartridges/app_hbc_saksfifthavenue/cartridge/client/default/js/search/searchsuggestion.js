'use strict';

const base = {};
const debounce = require('lodash/debounce');
const pageScrollManager = require('../util/pageScrollManager');
const htmlFragments = require('../util/htmlFragments');

const MIN_CHARS = 1;

const headerSearchForms = document.querySelectorAll('.search-form');
const searchOverlayTemplate = document.getElementById('search-overlay-template');
let searchOverlay;
let scrim;
let resultsContainer;
let overlayInput;

let SUGGESTION_ENDPOINT;
let POPULARSEARCH_ENDPOINT;
let RECOMMENDATIONS_ENDPOINT;

let checkRecommendation = true;
if (searchOverlayTemplate !== null) {
  SUGGESTION_ENDPOINT = searchOverlayTemplate.dataset.suggestionEndpoint;
  POPULARSEARCH_ENDPOINT = searchOverlayTemplate.dataset.popularsearchEndpoint;
  RECOMMENDATIONS_ENDPOINT = searchOverlayTemplate.dataset.recommendationsEndpoint;
}

/**
 * Process Ajax response for SearchServices-GetSuggestions
 *
 * @param {Object|string} response - Empty object literal if null response or string with rendered
 *                                   suggestions template contents
 */
function processResponse(response) {
  $.spinner().stop();

  if (typeof response === 'string') {
    // Render results
    const sanitizedResponse = removeEmptyResultSections(response);
    resultsContainer.innerHTML = htmlFragments.fragmentToString(sanitizedResponse);
    if (checkRecommendation) {
      handleFirstRecommendation();
    } else {
      appendRecommendation(resultsContainer);
    }
  } else {
    // Handle no results
    console.dir(response);
  }
}

function appendRecommendation(resultsContainer) {
  let responseDOM = resultsContainer;
  [].forEach.call(responseDOM.querySelectorAll('.search-suggestions'), block => {
    if (block.classList.contains('recently-viewed')) {
      document.querySelector('.recently-viewed.search-suggestions').innerHTML = document.querySelector('.recently-viewed-search-suggestion').innerHTML;
    }

    if (block.classList.contains('you-might-like')) {
      document.querySelector('.you-might-like.search-suggestions').innerHTML = document.querySelector('.you-might-like-search-suggestion').innerHTML;
    }

    if (block.classList.contains('best-sellers')) {
      document.querySelector('.best-sellers.search-suggestions').innerHTML = document.querySelector('.best-sellers-search-suggestion').innerHTML;
    }
  });
}

/**
 * Looks for empty search-suggestion lists and removes those blocks from the HTML.
 * @param {String} response The response HTML string
 * @returns {DocumentFragment} The response with emtpy blocks removed.
 */
function removeEmptyResultSections(response) {
  let responseDOM = htmlFragments.createFragment(response);
  [].forEach.call(responseDOM.querySelectorAll('.search-suggestions'), block => {
    const list = block.querySelector('ul');
    if (list && !list.childElementCount) {
      block.parentElement.removeChild(block);
    }
  });
  return responseDOM;
}

/**
 * Triggers a search suggestion Ajax Request
 * @param {String} endpoint The URL to GET
 */
function doSuggestionRequest(endpoint) {
  $.spinner().start();
  $.ajax({ url: endpoint, method: 'GET' })
    .done(processResponse)
    .always(() => $.spinner().stop());
}

/**
 * Gets popular suggests. Used when there's no
 * @param {Event} evt The `input` event on the search input element
 */
function fetchPopularSuggestions() {
  doSuggestionRequest(POPULARSEARCH_ENDPOINT);
}

/**
 * Kicks off the request for search suggestions.
 * @param {Event} evt The `input` event on the search input element
 */
function retrieveSuggestions(evt) {
  const input = evt.target;
  if (input.value.trim().length < MIN_CHARS) {
    return;
  }
  doSuggestionRequest(SUGGESTION_ENDPOINT + encodeURIComponent(input.value.trim()));
}

/**
 * Adds a debounce to the retrieveSuggestions function
 * to wait for a gap in typing before kicking off the ajax request.
 */
const retrieveSuggestionsAfterInput = debounce(retrieveSuggestions, 300);

/**
 * Keeps the input values of different instances of search forms in sync.
 * Bound to the `input` event of each search form instance.
 * @param {Event} evt
 */
function mirrorQueryInput(evt) {
  document.querySelectorAll('.search-form input[name="q"]').forEach(input => {
    if (input === evt.target) {
      return;
    }
    input.value = evt.target.value;
  });
}

/**
 * Shows or hides the "Clear" reset button on a search form.
 * Bound to either the form `reset` event and the search field `input` event.
 * @param {Event} evt The event object
 */
function setResetButtonVisiblity(evt) {
  let form;
  let input;
  if (evt.target.matches('.search-form')) {
    form = evt.target;
    input = form.querySelector('input[name="q"]');
  } else {
    input = evt.target;
    form = input.form;
  }
  const resetButton = form.querySelector('button[type="reset"]');
  if (input.value === '') {
    resetButton.classList.add('d-none');
  } else {
    resetButton.classList.remove('d-none');
  }
}

/**
 * Handles resetting the current search form. Bound to the form's `reset` event.
 * @param {Event} evt The event object
 * @param {Boolean} resetOthers Do we want to reset the other forms? true by default
 */
function doFormReset(evt, resetOthers = true) {
  if (typeof evt.preventDefault === 'function') {
    evt.preventDefault();
  }
  const resetButton = evt.target;
  const form = resetButton.form;
  const input = form.querySelector('input[name="q"]');

  form.reset();
  resetButton.classList.add('d-none');

  // Reset other search forms too.
  if (resetOthers) {
    [].slice
      .call(document.querySelectorAll('.search-form button[type="reset"]'))
      .filter(el => resetButton !== el)
      .forEach(el => doFormReset({ target: el }, false));
  }

  input.focus();
  if (searchOverlay) {
    fetchPopularSuggestions();
  }
}

function dismissKeyboard() {
  if (document.activeElement === overlayInput) {
    overlayInput.blur();
  }
}

/**
 * Opens up the search overlay, building it based on the searchOverlayTemplate HTML.
 * Bound to the triggering search input's 'focus' event.
 * @param {Event} evt The event object
 */
function showSearchOverlay(evt) {
  document.body.appendChild(htmlFragments.createFragment(searchOverlayTemplate.innerHTML));

  searchOverlay = document.querySelector('.search-overlay');
  scrim = document.querySelector('.search-overlay + .scrim');

  const searchForm = searchOverlay.querySelector('.search-form');
  searchForm.addEventListener('reset', setResetButtonVisiblity);

  const closeButton = searchOverlay.querySelector('button.close');
  closeButton.addEventListener('click', dismissSearchOverlay);

  scrim.addEventListener('click', dismissSearchOverlay);

  overlayInput = searchForm.querySelector('input[name="q"]');
  overlayInput.addEventListener('input', mirrorQueryInput);
  overlayInput.addEventListener('input', setResetButtonVisiblity);
  overlayInput.addEventListener('input', evt => retrieveSuggestionsAfterInput(evt));
  mirrorQueryInput(evt);

  const resetButton = searchForm.querySelector('button[type="reset"]');
  resetButton.addEventListener('click', doFormReset);
  setResetButtonVisiblity({ target: overlayInput });

  resultsContainer = searchOverlay.querySelector('.suggestions-wrapper');
  resultsContainer.addEventListener('scroll', dismissKeyboard);

  pageScrollManager.preventPageScroll(false);

  const focusSearch = () => {
    overlayInput.focus();
    searchOverlay.removeEventListener('animationend', focusSearch);
  };

  searchOverlay.addEventListener('animationend', focusSearch);

  // Do the initial population of the search panel
  if (overlayInput.value.trim().length >= MIN_CHARS) {
    retrieveSuggestions({ target: overlayInput });
  } else {
    fetchPopularSuggestions();
  }
}

/**
 * Append recommendation to search overlay on first time search overlay opened.
 */
function handleFirstRecommendation() {
  var recommendationClasses = ['recently-viewed', 'you-might-like', 'best-sellers'];
  checkRecommendation = false;
  $.each(recommendationClasses, (index, recommendClass) => {
    if ($('.search-suggestions.' + recommendClass).length === 0) {
      return;
    }
    var recommendedDiv = $('.' + recommendClass + '-search-suggestion');
    if (recommendedDiv.length > 0 && recommendedDiv.find('div').length > 0) {
      var endTime = Date.now() + 5000;
      var checkTimer = setInterval(function () {
        if (recommendedDiv.find('div').html().trim().length > 0 || Date.now() > endTime) {
          appendRecommendation(resultsContainer);
          clearInterval(checkTimer);
        }
      }, 1000);
    }
  });
}

/**
 * Opens up the search overlay, building it based on the searchOverlayTemplate HTML.
 * Bound to the triggering search input's 'focus' event.
 * @param {Event} evt The event object
 */
function openSearchOverlay(evt) {
  if (searchOverlay) {
    return;
  }
  if ($('.search-recommendations').is(':empty')) {
    $.ajax({
      url: RECOMMENDATIONS_ENDPOINT,
      method: 'GET',
      success: data => {
        if (data !== null) {
          $('.search-recommendations').append(data);
          showSearchOverlay(evt);
        }
      }
    });
  } else {
    showSearchOverlay(evt);
  }
}
/**
 * Dismisses the search overlay, with a fade out effect, driven by CSS on the .search-overlay element.
 */
function dismissSearchOverlay() {
  searchOverlay.addEventListener('animationend', closeSearchOverlay);
  [searchOverlay, scrim].forEach(el => el.classList.add('exit'));
}

/**
 * Destroys the search overlay.
 * Called after the fade out completes in the `dismissSearchOverlay` function
 */
function closeSearchOverlay() {
  if (!searchOverlay) {
    return;
  }
  pageScrollManager.allowPageScroll();
  [searchOverlay, scrim].forEach(el => el.parentNode.removeChild(el));
  searchOverlay = null;
}

/**
 * Sets up all search forms present on the page.
 */
base.initSearch = () => {
  if (searchOverlayTemplate === null) {
    return;
  }

  headerSearchForms.forEach(form => {
    const input = form.querySelector('input[name="q"]');
    const reset = form.querySelector('button[type="reset"]');
    form.addEventListener('reset', setResetButtonVisiblity);
    input.addEventListener('focus', openSearchOverlay);
    input.addEventListener('input', mirrorQueryInput);
    input.addEventListener('input', setResetButtonVisiblity);
    reset.addEventListener('click', doFormReset);
  });

  let searchButtons = document.querySelectorAll('.site-search .search-form button[name="search-button"]');
  if (searchButtons.length > 0) {
    for (var i = 0; i < searchButtons.length; i++) {
      searchButtons[i].addEventListener('click', function (evt) {
        evt.preventDefault();
        openSearchOverlay(evt);
      });
    }
  }
};

module.exports = base;
