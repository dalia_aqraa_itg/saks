'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
  var cookieUtil = require('core/components/utilhelper');
  var bfxCookieCountryCode = cookieUtil.getCookie('bfx.country');
  if (bfxCookieCountryCode !== 'US') {
    $('.klarna-container').hide();
  }
  processInclude(require('./product/detail'));
  processInclude(require('./product/detailPrimaryImages'));
  processInclude(require('./product/detailThumbnailImageCarousel'));
  processInclude(require('./product/detailProductVideo'));
  processInclude(require('./product/pdpInstoreInventory'));
  processInclude(require('./product/detailCompleteTheLook'));
  processInclude(require('./product/wishlist'));
  processInclude(require('./product/wishlistHeart'));
});
