'use strict';

var scrollAnimate = require('base/components/scrollAnimate');

/**
 * appends params to a url
 * @param {string} data - data returned from the server's ajax call
 * @param {Object} button - button that was clicked for email sign-up
 */
function displayMessage(data, button) {
  $.spinner().stop();
  var status;
  var $this = $('.home-email-signup').find('.email-signup-input input');
  $('.home-email-signup').find('.error-container .error').remove();
  $('.email-signup-input').removeClass('error');
  if (data.success) {
    status = 'alert-success';
  } else {
    status = 'alert-danger';
  }

  if ($('.email-signup-message-footer').length === 0) {
    $('div.email-signup-input').append('<div class="email-signup-message-footer"></div>');
  }
  if (data.success) {
    $('.email-signup-message-footer').append(
      '<div class="d-flex align-items-center ' +
        status +
        '"><span class="success-content-ave">' +
        data.msg +
        '<span class="svg-21-avenue-thick-success-dims svg-21-avenue-thick-success success-img"></span></span></div>'
    );
    $('.email-signup-input').addClass('success');
    $this.next('span').remove();
    button.closest('.signup-wrap').find('input,label,button').addClass('invisible');
    button.closest('form').find('.sign-up-bottom').addClass('invisible');
    setTimeout(function () {
      $('.email-signup-message-footer').remove();
      $('.email-signup-input').removeClass('success');
      $('.email-signup-input').find('input').val(''); // remove the email prefill as he has already submitted
      $('.email-signup-input').find('input').next('label').removeClass('input-focus');
      button.closest('.signup-wrap').find('input,label,button').removeClass('invisible');
      button.closest('form').find('.sign-up-bottom').removeClass('invisible');
      button.removeAttr('disabled');
    }, 3000);
  } else {
    $('.home-email-signup')
      .find('.error-container')
      .append('<span class="error">' + data.msg + '</span>');
    if ($this.next('span').length === 0) {
      $('<span class="invalid"></span>').insertAfter($this);
    }
    $('.email-signup-input').addClass('error');
    button.removeAttr('disabled');
  }
}

module.exports = function () {
  $(window).scroll(function () {
    if ($(window).scrollTop() > 100 && ($('.grecaptcha-badge').css('visibility') == 'hidden' || $('.grecaptcha-badge:visible').length <= 0)) {
      $('.back-to-top').addClass('active');
    } else $('.back-to-top').removeClass('active');
  });

  $('.back-to-top').click(function () {
    scrollAnimate();
  });

  $('.subscribe-email').on('click', function (e) {
    e.preventDefault();
    var url = $(this).data('href');
    var button = $(this);
    var subscribeForm = $(this).closest('form');
    var emailId = $('input[name=hpEmailSignUp]').val();
    var reg = /^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/;
    if (!reg.test($('input[name=hpEmailSignUp]').val())) return false;
    $.spinner().start();
    $(this).attr('disabled', true);
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: {
        emailId: emailId
      },
      success: function (data) {
        displayMessage(data, button);
        if (data.success) {
          $('body').trigger('adobeTagManager:emailsignupmodal', 'footer');
        }
      },
      error: function (err) {
        displayMessage(err, button);
      }
    });
  });
  $('.signup-wrap').on('input', '.email-signup-input input', function () {
    if ($(this).val() == 0 && $(this).parent().hasClass('error')) {
      $(this).next('span').remove();
      $(this).closest('.email-input-box').find('.error-container span').empty();
      $(this).parent().removeClass('error');
    }
  });

  $('.liveChat-btn').on('click', function () {
    var isInternalUser = $('#isInternalUser').data('internaluser');
    // Make sure that User is external using SFCC Geolaction along with Salesfloor Geolocation
    if (!sessionStorage.sf_within_geo && !isInternalUser) {
      $('.arrow-left').toggleClass('d-sm-block');
      $('.live-chat-view-3').removeClass('d-none');
      $('.live-chat-view-2').addClass('d-none');
      $('.chat-img').toggleClass('d-none');
      $('.chat-img-cross').toggleClass('d-none');
    } else {
      $('.live-chat-view-2').removeClass('d-none');
      $('.live-chat-view-3').addClass('d-none');
      $('.chat-img').toggleClass('d-none');
      $('.chat-img-cross').toggleClass('d-none');
    }

    $('body').toggleClass('chat-overlay');
    $('.live-chat-content').toggleClass('d-none');

    if (!$('.chat-img-cross').hasClass('d-none')) {
      // Make sure if user is internal,should see the connect with stylist.
      if (sessionStorage.sf_within_geo || isInternalUser) {
        $('.live-chat-view-2').removeClass('d-none');
        $('.live-chat-view-3').addClass('d-none');
        $('.live-chat-view-4').addClass('d-none');
      }
    }

    if ($('.embeddedServiceHelpButton .helpButton .uiButton.helpButtonDisabled').length > 0) {
      $('.chat-service-links .customer-service-chat-link').addClass('live-chat-offline');
    } else if ($('.embeddedServiceHelpButton .helpButton .uiButton.helpButtonEnabled').length > 0) {
      $('.chat-service-links .customer-service-chat-link').addClass('live-chat-online');
    }
  });

  $('.stylist-chat-btn').on('click', function () {
    $('.live-chat-content').toggleClass('d-none');
    $('.live-chat-view-2').toggleClass('d-none');
    $('.chat-img').toggleClass('d-none');
    $('.chat-img-cross').toggleClass('d-none');
  });

  $('.customer-service-button').on('click', function () {
    $('.live-chat-view-2').toggleClass('d-none');
    $('.live-chat-view-3').toggleClass('d-none');
  });

  $('.customer-service').on('click', function () {
    $('.live-chat-view-2').toggleClass('d-none');
    $('.live-chat-view-3').toggleClass('d-none');
  });

  $('.customer-service-call-link').on('click', function () {
    $('.live-chat-view-3').toggleClass('d-none');
    $('.live-chat-view-4').toggleClass('d-none');
  });

  $('.customer-service-call-link-saks').on('click', function () {
    $('.live-chat-view-2').toggleClass('d-none');
    $('.live-chat-view-3').toggleClass('d-none');
  });

  $('.call-customer-service').on('click', function () {
    $('.live-chat-view-3').toggleClass('d-none');
    $('.live-chat-view-4').toggleClass('d-none');
  });

  $('.call-customer-service-saks').on('click', function () {
    $('.live-chat-view-2').toggleClass('d-none');
    $('.live-chat-view-3').toggleClass('d-none');
  });

  $('.chat-img-cross-black').on('click', function () {
    $('body').toggleClass('chat-overlay');
    $('.chat-img-cross').addClass('d-none');
    $('.chat-img').removeClass('d-none');
    $('.live-chat-content').addClass('d-none');
    $('.live-chat-view-2').addClass('d-none');
    $('.live-chat-view-3').addClass('d-none');
  });

  $('body').on('click', '.customer-service-chat-link', function () {
    if ($('.embeddedServiceHelpButton .helpButton .uiButton.helpButtonEnabled').length > 0) {
      $('.live-chat-content').toggleClass('d-none');
      $('.live-chat-view-3').toggleClass('d-none');
      $('.chat-img').toggleClass('d-none');
      $('.chat-img-cross').toggleClass('d-none');
      $('.embeddedServiceHelpButton .helpButton .uiButton.helpButtonEnabled').trigger('click');
    }
  });

  $('body').on('click', '#bfx-cc-btn', function (e) {
    e.preventDefault();
    var bfxCountryCode = $('.bfx-cc-countries').val();
    var bfxURL = $('.bfxcountrycodeURL').attr('data-bfxcountrycode');
    $.ajax({
      url: bfxURL,
      type: 'post',
      dataType: 'json',
      data: {
        bfxCountryCode: bfxCountryCode
      },
      success: function () {
        // Just Redirect
        return true;
      }
    });
  });

  $(document).ready(function(){
    $('.stylist-btn button#sf-contextual-widget-landing-page').prop( "disabled", false );
  });
};
