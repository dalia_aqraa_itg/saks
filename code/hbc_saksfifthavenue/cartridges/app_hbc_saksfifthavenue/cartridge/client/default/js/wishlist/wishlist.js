'use strict';
/**
 * Update the pagination arrow
 *
 */
function updateArrow() {
  if ($('.pagination .page-num-wrap').first().hasClass('d-none')) {
    $('.pagination .prev').removeClass('d-none');
  } else {
    $('.pagination .prev').addClass('d-none');
  }
  if ($('.pagination .page-num-wrap').last().hasClass('d-none')) {
    $('.pagination .next').removeClass('d-none');
  } else {
    $('.pagination .next').addClass('d-none');
  }
}
$('body').on('click', '.pagination .wishlist-pages', function (e) {
  e.preventDefault();
  $(window).scrollTop(0);
  $('.wishlistItemCards .paging').addClass('d-none');
  var showWishlistCardClass = $(this).data('page');
  $('.wishlistItemCards')
    .find('.' + showWishlistCardClass)
    .removeClass('d-none');
  $('.pagination .wishlist-pages').removeClass('disabled current');
  $(this).addClass('disabled current');
});

/**
 * click event to show next and previous pages
 */
$('body').on('click', '.pagination .arrow-next', function () {
  if ($(window).width() >= 1024) {
    $('.pagination .page-num-wrap').each(function () {
      if (!$(this).hasClass('d-none')) {
        $(this).addClass('d-none');
        $(this).next().removeClass('d-none');
        return false;
      }
    });
    updateArrow();
  }
});

$('body').on('click', '.pagination .arrow-prev', function () {
  if ($(window).width() >= 1024) {
    $('.pagination .page-num-wrap').each(function () {
      if (!$(this).hasClass('d-none')) {
        $(this).addClass('d-none');
        $(this).prev().removeClass('d-none');
        return false;
      }
    });
    updateArrow();
  }
});

$(document).on('click', '.load-more', function () {
  if ($('.wishlist-items-avenue .paging').hasClass('d-none')) {
    $($('.wishlist-items-avenue .paging.d-none')[0]).removeClass('d-none');
  }
  if (!$('.wishlist-items-avenue .paging').last().hasClass('d-none')) {
    $(this).hide();
  }
});
