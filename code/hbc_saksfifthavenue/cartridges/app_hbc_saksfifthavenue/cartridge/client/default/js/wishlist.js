'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
  processInclude(require('core/wishlist/wishlist'));
  processInclude(require('./wishlist/wishlist'));
  processInclude(require('./product/wishlistHeart'));
  processInclude(require('./product/wishlist')); // this is for the quickview heart functionality
  processInclude(require('./search/search'));
  processInclude(require('./product/quickView'));
});
