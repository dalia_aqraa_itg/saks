'use strict';

var storeLocator = require('../storeLocator/storeLocator');
var base = require('core/product/pdpInstoreInventory');
var formFields = require('core/formFields/formFields');
var hbcTooltip = require('core/tooltip');
var zipCodeValidator = require('../zipCodeValidator');
var klarnaClient = require('../klarnaClientUtils');

/**
 * remove the store selector modal when a store is selected out of results
 */
function removeSelectStoreModal() {
  if ($('#inStoreInventoryModal').length > 0) {
    $('#inStoreInventoryModal').modal('hide');
    $('#inStoreInventoryModal').remove();
    $('#inStoreInventoryModal').attr('aria-modal', 'false');
  }
}

/**
 * Updates details of a product line item in the Cart Section
 * @param {Object} data - AJAX response from the server
 */
function updateItemsHTML(data) {
  if (data.ssdPostal && data.ssdPostal !== 'null') {
    $('.change-zipcode ').html(data.ssdPostal);
  }
  if (data.cartModel && data.cartModel.itemsHTML) {
    $('.container.cart-page-content .cart-plis.product-line-item').empty().html($(data.cartModel.itemsHTML).children().html());
    formFields.updateSelect();
    hbcTooltip.tooltipInit();
  }
}
/**
 * CUSTOM EVENT
 * event handler triggered with the store selection on modal
 */
base.selectZipWithInventory = function () {
  $('body').on('zip:selected', function (e, data) {
    var searchPIDQuantity = $('.btn-sddzipcode-search').attr('data-search-pid');
    var searchPID = searchPIDQuantity.split(':')[0];
    var searchPlid = $('.btn-storelocator-search').attr('data-product-pliuuid');
    var productElement =
      $('.product-detail[data-pid="' + searchPID + '"]') === undefined
        ? $('.cart-options[data-pid="' + searchPID + '"]')
        : $('.product-detail[data-pid="' + searchPID + '"]'); // fall back from cart, pick product id
    var $changeZipCodeElement = $(productElement).find('.change-zipcode');
    if (data.postalCode && $changeZipCodeElement.length > 0) {
      $changeZipCodeElement.html(data.postalCode);
    }
    if (data.productType && data.productType !== 'variant') {
      $('.zip-information .available-messaging').removeClass('d-none');
      $('.zip-information .available-messaging').find('span.variation-msg').removeClass('d-none');
      $('.zip-information .available-messaging').find('span.server-msg').html('');
    } else if (data.productType && data.productType === 'variant') {
      $('.zip-information .available-messaging').removeClass('d-none');
      $('.zip-information .available-messaging').find('span.variation-msg').addClass('d-none');
      $('.zip-information .available-messaging').find('span.server-msg').html(data.pdpSDDAvailabilityMessage);
    }
    if ($('body').find('.product-detail').length) {
      $('body').trigger('adobe:sddSearch', data);
    }
    removeSelectStoreModal();
  });
};

/**
 * Update tax totals
 *
 * @param {Object} data - Tax totals data
 */
function updateTaxTotal(data) {
  if (data.totals.canadaTaxation && data.totals.canadaTaxation.PST) {
    $('.tax-PST-total').empty().append(data.totals.canadaTaxation.PST);
    $('.tax-pst').removeClass('d-none');
  } else if (!$('.tax-pst').hasClass('d-none')) {
    $('.tax-pst').addClass('d-none');
  }

  if (data.totals.canadaTaxation && data.totals.canadaTaxation['GST/HST']) {
    $('.tax-GST-total').empty().append(data.totals.canadaTaxation['GST/HST']);
    $('.tax-gst').removeClass('d-none');
  } else if (!$('.tax-gst').hasClass('d-none')) {
    $('.tax-gst').addClass('d-none');
  }

  if (data.totals.canadaTaxation && data.totals.canadaTaxation.QST) {
    $('.tax-QST-total').empty().append(data.totals.canadaTaxation.QST);
    $('.tax-qst').removeClass('d-none');
  } else if (!$('.tax-qst').hasClass('d-none')) {
    $('.tax-qst').addClass('d-none');
  }

  if (data.totals.canadaTaxation && data.totals.canadaTaxation.RST) {
    $('.tax-RST-total').empty().append(data.totals.canadaTaxation.RST);
    $('.tax-rst').removeClass('d-none');
  } else if (!$('.tax-rst').hasClass('d-none')) {
    $('.tax-rst').addClass('d-none');
  }

  if (data.totals.canadaTaxation && data.totals.canadaTaxation.ECO) {
    $('.tax-ECO-total').empty().append(data.totals.canadaTaxation.ECO);
    $('.tax-eco').removeClass('d-none');
  } else if (!$('.tax-eco').hasClass('d-none')) {
    $('.tax-eco').addClass('d-none');
  }

  if (!data.totals.canadaTaxation) {
    $('.tax-total').empty().append(data.totals.totalTax);
    $('.tax-normal').removeClass('d-none');
  } else if (!$('.tax-normal').hasClass('d-none')) {
    $('.tax-normal').addClass('d-none');
  }
}

/**
 * re-renders the order totals and the number of items in the cart
 * @param {Object} data - AJAX response from the server
 */
function updateCartTotals(data) {
  klarnaClient.toggleKlarnaOrderSummary(data.totals.grandTotalValue, data.items);

  $('.number-of-items').empty().append(data.resources.numberOfItems);
  $('.shipping-total-cost').empty().append(data.totals.totalShippingCost);

  $('.shipping-method-price')
    .empty()
    .append('- ' + data.totals.totalShippingCost);

  updateTaxTotal(data);

  $('.grand-total-sum').empty().append(data.totals.grandTotal);
  $('.grand-total-value').empty().append(data.totals.grandTotalValue);
  $('.sub-total').empty().append(data.totals.subTotal);
  $('.mini-sub-total').empty().append(data.totals.miniCartEstimatedTotal);
  $('.minicart-quantity').empty().append(data.numItems);
  $('.minicart-link').attr({
    'aria-label': data.resources.minicartCountOfItems,
    title: data.resources.minicartCountOfItems
  });
  if (data.totals.orderLevelDiscountTotal.value > 0) {
    $('.order-discount').removeClass('hide-order-discount');
    $('.order-discount-total')
      .empty()
      .append('- ' + data.totals.orderLevelDiscountTotal.formatted);
  } else {
    $('.order-discount').addClass('hide-order-discount');
    $('.order-discount-total')
      .empty()
      .append('- ' + data.totals.orderLevelDiscountTotal.formatted);
  }

  if (data.totals.shippingLevelDiscountTotal.value > 0) {
    $('.shipping-total-cost').empty().append(data.totals.freeShippingText);

    $('.shipping-method-price')
      .empty()
      .append('- ' + data.totals.freeShippingText);
  }

  if (data.totals.totalSavings.value > 0) {
    $('.grand-total-saving-container').removeClass('d-none');
    $('.checkout-total-savings').empty().append(data.totals.totalSavings.formatted);
  } else {
    $('.grand-total-saving-container').addClass('d-none');
  }

  data.items.forEach(function (item) {
    if (item.checkoutDiscountTotalHtml) {
      $('.checkout_discount-' + item.UUID).html(item.checkoutDiscountTotalHtml);
    }
  });

  if (data.totals.associateOrFDDMsg !== '') {
    $('.associate-fdd-promo').removeClass('d-none');
    $('.associate-promo-msg').empty().append(data.totals.associateOrFDDMsg);
  } else {
    $('.associate-fdd-promo').addClass('d-none');
  }

  if (data.totals.hudsonpoint > 0) {
    $('.hudson-reward-points .hudson-point').html(data.totals.hudsonpoint);
  } else {
    $('.hudson-reward-points').addClass('d-none');
  }
}

/**
 * updates ship to home and ship to store option on toggle of radio
 * updates item with the store info
 * */
$('body').on('sdd:cart', function (e, reqdata) {
  e.stopImmediatePropagation();
  var changeShiptoUrl = reqdata.url;
  $.spinner().start();
  if (changeShiptoUrl) {
    $.ajax({
      url: changeShiptoUrl,
      success: function (data) {
        $('body').trigger('adobe:sddCartSearch', data);
        updateItemsHTML(data);
        removeSelectStoreModal();
        updateCartTotals(data.cartModel);
        $.spinner().stop();
      },
      error: function () {
        $.spinner().stop();
      }
    });
  }
});

/**
 * Postal code formatting for in store
 */
function bopisPostalCodeValidation() {
  $('#store-postal-code').on('keyup', function () {
    var postalCode = $(this).val();
    if (/[a-zA-Z][0-9][a-zA-Z]/.test(postalCode.substring(0, 3))) {
      if (postalCode.length === 4 && postalCode.indexOf(' ') === 3) {
        return;
      }
      if (postalCode.length === 5 && postalCode.lastIndexOf(' ') === 4) {
        $(this).val(postalCode.substring(0, postalCode.length - 1));
        return;
      }
      var formattedString = postalCode
        .replace(/ /g, '')
        .replace(/(\w{3})/, '$1 ')
        .replace(/(^\s+|\s+$)/, ' ')
        .toUpperCase()
        .trim();
      $(this).val(formattedString);
    }
  });
}

/**
 * Removes the specified parameter from url
 *
 * @param {string} url - the url
 * @param {string} parameter - parameter to be removed
 * @returns {string} url - url after removing the parameter
 */
function removeURLParameter(url, parameter) {
  // prefer to use l.search if you have a location/link object
  var urlparts = url.split('?');
  if (urlparts.length >= 2) {
    var prefix = encodeURIComponent(parameter) + '=';
    var pars = urlparts[1].split(/[&;]/g);
    // reverse iteration as may be destructive
    for (var i = pars.length; i-- > 0; ) {
      // idiom for string.startsWith
      if (pars[i].lastIndexOf(prefix, 0) !== -1) {
        pars.splice(i, 1);
      }
    }

    // eslint-disable-next-line no-param-reassign
    url = urlparts[0] + '?' + pars.join('&');
    return url;
  }
  return url;
}

/**
 * appends params to a url
 * @param {string} url - Original url
 * @param {Object} params - Parameters to append
 * @returns {string} result url with appended parameters
 */
function appendToUrl(url, params) {
  var newUrl = url;
  newUrl +=
    (newUrl.indexOf('?') !== -1 ? '&' : '?') +
    Object.keys(params)
      .map(function (key) {
        return key + '=' + encodeURIComponent(params[key]);
      })
      .join('&');

  return newUrl;
}

/**
 * Search for stores with new zip code
 * @param {HTMLElement} element - the target html element
 * @returns {boolean} false to prevent default event
 */
function search(element) {
  var dialog = element.closest('.in-store-inventory-dialog');
  //var spinner = $("#inStoreInventoryModal").spinner();
  $.spinner().start();
  var $form = element.closest('.zipcode-locator');
  var url = $form.attr('action');
  var searchPID = $('.btn-sddzipcode-search').attr('data-search-pid');
  var payload = {};
  payload.postalCode = $form.find('[name="postalCode"]').val();
  // extra payload carrying the product ids with quantity in json serialized
  if (
    element.data('products') !== undefined &&
    element.data('products') !== 'null' &&
    element.data('products') !== null &&
    element.data('products').indexOf('undefined') === -1
  ) {
    payload.pidquan = element.data('products');
  }
  /*    if (searchPID && searchPID !== '') {
    	payload.pidquan = searchPID;
    }*/
  $.ajax({
    url: url,
    type: $form.attr('method'),
    data: payload,
    dataType: 'json',
    success: function (data) {
      /** ** CUSTOM EVENT TRIGGER ****/
      if ($('.btn-sddzipcode-search').data('cart-source') !== undefined && $('.btn-sddzipcode-search').data('cart-source') === true) {
        // store select cart
        data.url = $('.btn-sddzipcode-search').data('cart-setzipurl');
        $('body').trigger('sdd:cart', data);
        $.spinner().stop();
      } else if ($('.btn-sddzipcode-search').data('source') === 'search') {
        // store select search page
        var storeRefine = $('.delivery-refinements').find('input[id="sddCheck"]');
        var valueSelected = $('input[name="refinestore"]:checked').val();
        if (storeRefine.length > 0) {
          if ($('.delivery-refinements').find('input[id="sddCheck"]').siblings('div.store-change-link-sdd').find('a.change-zipcode').length > 0) {
            $('.delivery-refinements').find('input[id="sddCheck"]').siblings('div.store-change-link-sdd').find('a.change-zipcode').html(data.postalCode);
          }
          if (storeRefine.siblings('label[for=sddCheck]').length > 0) {
            storeRefine.siblings('label[for=sddCheck]').find('span').html(data.message);
          }
          if (data.storeid) {
            storeRefine.removeAttr('disabled');
            var dataUrl = storeRefine.attr('data-href');
            var storeUrl = removeURLParameter(dataUrl, 'storeid');
            storeUrl = removeURLParameter(storeUrl, 'srchsrc');
            storeUrl = appendToUrl(storeUrl, { storeid: data.storeid, srchsrc: 'sdd' });
            storeRefine.attr('data-href', storeUrl);
            $('input[id="sddCheck"]').trigger('click');
          } else {
            storeRefine.attr('disabled', 'disabled');
            if (valueSelected === 'sameday') {
              $('input[id="noStore"]').trigger('click');
            }
            $.spinner().stop();
          }
        } else {
          $.spinner().stop();
        }
        removeSelectStoreModal();
      } else {
        // store select PDP
        $('body').trigger('zip:selected', data);
        $.spinner().stop();
      }
    },
    error: function () {
      $.spinner().stop();
    }
  });
  return false;
}
/**
 * Generates the modal window on the first call. Modal head and
 * main text is appended through parameter
 * @param {string} modalBodyText - main text in the modal
 * @param {string} modalCloseText - close title
 * @param {string} modalHeadText - modal header text
 */
function getModalHtmlElement(modalBodyText, modalCloseText, modalHeadText, event) {
  if ($('#inStoreInventoryModal').length !== 0) {
    $('#inStoreInventoryModal').modal('hide');
    $('#inStoreInventoryModal').remove();
  }
  var modalHead =
    modalHeadText !== undefined
      ? modalHeadText
      : $(event.currentTarget).hasClass('change-zipcode')
      ? $('.store-change-link-sdd').data('modal-header-text-sdd')
      : $('.store-change-link').data('modal-header-text'); // fallback to get content from DOM, PDP
  var modalClose = modalCloseText !== undefined ? modalCloseText : $('.store-change-link').data('modal-header-text'); // fallback to get content from DOM, PDP
  var modalBody =
    modalBodyText !== undefined
      ? modalBodyText
      : $(event.currentTarget).hasClass('change-zipcode')
      ? $('.store-change-link-sdd').data('modal-body-text-sdd')
      : $('.store-change-link').data('modal-body-text'); // fallback to get content from DOM, PDP
  // set empty to avoid display of null
  modalHead = modalHead === null ? '' : modalHead;
  modalClose = modalClose === null ? '' : modalClose;
  modalBody = modalBody === null ? '' : modalBody;
  var htmlString =
    '<!-- Modal -->' +
    '<div class="modal inStoreInventoryModal" id="inStoreInventoryModal" role="dialog" aria-modal="true">' +
    '<div class="modal-dialog in-store-inventory-dialog">' +
    '<!-- Modal content-->' +
    '<div class="modal-content">' +
    '<div class="modal-header">' +
    '<span>' +
    modalHead +
    '</span>' +
    '    <button type="button" class="close svg-36-avenue-Up_Copy svg-36-avenue-Up_Copy-dims" data-dismiss="modal" aria-label="Close inventory modal" title="' +
    modalClose +
    '">' + // eslint-disable-line
    '    </button>' +
    '</div>' +
    '<div class="change-a-store">' +
    modalBody +
    '</div>' +
    '<div class="modal-body"></div>' +
    '<div class="modal-footer"></div>' +
    '</div>' +
    '</div>' +
    '</div>';
  $('body').append(htmlString);
  $('#inStoreInventoryModal').modal('show');
  $('.change-store[aria-haspopup=true]').attr('aria-controls', 'inStoreInventoryModal');
  formFields.inputfocusEvent();
}

/**
 * Replaces the content in the modal window with find stores components and
 * the result store list.
 * @param {string} pid - The product ID to search for
 * @param {number} quantity - Number of products to search inventory for
 * @param {number} selectedPostalCode - The postal code to search for inventory
 * @param {number} selectedRadius - The radius to search for inventory
 * @param {string} pliUUID - unid id of an item
 * @param {string} selectedOption - selected option between shipto and instore
 * @param {string} instoreFieldLabel - default lable for instore
 * @param {string} source - source of invoke
 */
function fillModalElement(pid, quantity, selectedPostalCode, selectedRadius, pliUUID, selectedOption, instoreFieldLabel, source) {
  var requestData = {};
  if (pid && quantity) {
    requestData.products = pid + ':' + quantity;
  }
  if (selectedRadius) {
    requestData.radius = selectedRadius;
  }

  if (selectedPostalCode) {
    requestData.postalCode = selectedPostalCode;
  }

  $('#inStoreInventoryModal').spinner().start();
  $.ajax({
    url: $('.change-store').data('open-action'),
    data: requestData,
    method: 'GET',
    success: function (response) {
      $('.inStoreInventoryModal .modal-body').empty();
      $('.inStoreInventoryModal .modal-body').html(response.storesResultsHtml);
      storeLocator.search();
      storeLocator.changeRadius();
      //storeLocator.selectStore();
      //storeLocator.updateSelectStoreButton();
      // storeLocator.setMyHomeStore();
      formFields.updateSelect();
      formFields.adjustForAutofill();
      $('.btn-storelocator-search').attr('data-search-pid', pid);
      $('.btn-storelocator-search').attr('data-product-pliuuid', pliUUID);
      $('.btn-storelocator-search').attr('data-shipto-selected', selectedOption);
      $('.btn-storelocator-search').attr('data-instorefieldlabel', instoreFieldLabel);
      $('.btn-storelocator-search').attr('data-source', source);
      if (selectedRadius) {
        $('#radius').val(selectedRadius);
      }

      if (selectedPostalCode) {
        $('#store-postal-code').val(selectedPostalCode);
      }

      if (!$('.results').data('has-results')) {
        $('.store-locator-no-results').show();
      }
      // do not show error message with the modal open
      if (!$('.results').data('searchexecuted')) {
        $('.store-locator-no-results').hide();
      }
      $('#inStoreInventoryModal').modal('show');
      $('#inStoreInventoryModal').spinner().stop();
      $('#inStoreInventoryModal').find('#store-postal-code').focus();
      bopisPostalCodeValidation();
    },
    error: function () {
      $('#inStoreInventoryModal').spinner().stop();
    }
  });
}

/**
 * Search for stores with new zip code
 */
base.searchSDDWithPostal = function () {
  $('.zipcode-locator .btn-sddzipcode-search[type="button"]').click(function (e) {
    e.preventDefault();
    if (!$('form.zipcode-locator').is(':valid')) {
      return;
    }
    var input = $('#inStoreInventoryModal').find('#search-sdd-postal');
    var inputVal = input.val();
    if (inputVal === '') {
      $('#inStoreInventoryModal').find('.zipcode-locator').addClass('error');
      input.focus();
      var formData = {};
      formData.errorFields = ['postalCode'];
    } else {
      search($(this));
      $('#inStoreInventoryModal').find('.zipcode-locator').removeClass('error');
    }
  });
  $('form.zipcode-locator').on('submit', function (e) {
    e.preventDefault();
    $('.zipcode-locator .btn-sddzipcode-search[type="button"]').trigger('click');
  });
  zipCodeValidator.validateZip($('#search-sdd-postal'));
};

/**
 * Replaces the content in the modal window with find stores components and
 * the result store list.
 * @param {string} pid - The product ID to search for
 * @param {number} quantity - Number of products to search inventory for
 * @param {number} selectedPostalCode - The postal code to search for inventory
 * @param {number} selectedRadius - The radius to search for inventory
 * @param {string} pliUUID - unid id of an item
 * @param {string} selectedOption - selected option between shipto and instore
 * @param {string} instoreFieldLabel - default lable for instore
 * @param {string} source - source of invoke
 */
function fillSDDModalElement(pid, quantity, selectedPostalCode, source, cartSetZipUrl, cartSource) {
  var requestData = {
    products: pid + ':' + quantity
  };

  if (selectedPostalCode) {
    requestData.postalCode = selectedPostalCode;
  }

  $('#inStoreInventoryModal').spinner().start();
  $.ajax({
    url: $('.change-zipcode').data('open-action-sdd'),
    data: requestData,
    method: 'GET',
    success: function (response) {
      $('.inStoreInventoryModal .modal-body').empty();
      $('.inStoreInventoryModal .modal-body').html(response.zipCodeSearchHtml);
      base.searchSDDWithPostal(); // change store modal
      $('.btn-sddzipcode-search').attr('data-search-pid', requestData.products);
      $('.btn-sddzipcode-search').attr('data-source', source);
      $('.btn-sddzipcode-search').data('cart-setzipurl', cartSetZipUrl);
      $('.btn-sddzipcode-search').data('cart-source', cartSource);
      $('#inStoreInventoryModal').modal('show');
      $('#inStoreInventoryModal').spinner().stop();
      $('#inStoreInventoryModal').find('#store-postal-code').focus();
    },
    error: function () {
      $('#inStoreInventoryModal').spinner().stop();
    }
  });
}

/**
 * CLICK EVENT
 * event handler triggered with the click of change store in PDP
 */
base.changeStore = function () {
  $('body').on('click', '.change-store, .change-zipcode', function (e) {
    window.sessionStorage.setItem('change-store-click', true);
    var pid =
      $(this).closest('.product-detail').attr('data-pid') === undefined
        ? $(this).closest('.cart-options').data('product-id')
        : $(this).closest('.product-detail').attr('data-pid');
    var quantity =
      $(this).closest('.product-detail').find('.quantity-select').val() === undefined
        ? $(this).closest('.cart-options').data('product-qty')
        : $(this).closest('.product-detail').find('.quantity-select').val();
    var modalBodyText = $(this).closest('.cart-options').data('modal-body-text');
    var modalCloseText = $(this).closest('.cart-options').data('modal-close-text');
    var modalHeadText = $(this).closest('.cart-options').data('modal-header-text');
    var source = $('.change-store').parent().siblings('.store-refine').length > 0 ? 'search' : null;
    var cartSetZipUrl = $('.change-zipcode').parent().attr('data-setcart');
    var cartSource = $('.change-zipcode').parent().hasClass('source-cart') ? true : false;
    var selectedOption = $(this).closest('.cart-options').find('input[name*="shipto"]:checked').val();
    var pliUUID = $(this).closest('.cart-options').data('product-uuid');
    var instoreFieldLabel = $(this).closest('.cart-options').data('instorefieldlabel');
    getModalHtmlElement(modalBodyText, modalCloseText, modalHeadText, e);
    if ($(e.currentTarget).hasClass('change-zipcode')) {
      source = $('.change-zipcode').parent().closest('.sdd-refine').length > 0 ? 'search' : null;
      fillSDDModalElement(pid, quantity, $(this).data('postal'), source, cartSetZipUrl, cartSource);
      if (cartSource) {
        $('body').trigger('adobe:sddCartSelect');
      }
    } else {
      source = $('.change-store').parent().closest('.store-refine').length > 0 ? 'search' : null;
      fillModalElement(pid, quantity, $(this).data('postal'), $(this).data('radius'), pliUUID, selectedOption, instoreFieldLabel, source);
    }
  });
};

/**
 * Changes add to bag text dynamically on toggle of shipping options
 */
base.changeButtonText = function () {
  $('.shipping-option').on('click', 'input:not(.change-store)', function () {
    var selectedValue = $(this).val();
    var addToBagText = $('.shipping-option').data('addtobag-text');
    var addToStoreText = $('.shipping-option').data('addtostore-text');
    var addToCartButton = $(this).closest('.product-detail').find('.add-to-cart');
    if (addToStoreText !== undefined && addToStoreText.length && addToStoreText !== '') {
      if (selectedValue === 'instore') {
        addToCartButton.html(addToStoreText);
        $('body').trigger('adobe:bopusStart');
        $('.zip-information .available-messaging').addClass('d-none');
      } else {
        $('.zip-information .available-messaging').removeClass('d-none');
        addToCartButton.html(addToBagText);
      }
    }
  });
};

base.setMyHomeStore = storeLocator.setMyHomeStore();
base.selectStore = storeLocator.selectStore();
base.updateSelectStoreButton = storeLocator.updateSelectStoreButton();

module.exports = base;
