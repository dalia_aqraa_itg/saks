'use strict';
var floatLabel = require('core/floatLabel');
var clientSideValidation = require('core/components/clientSideValidation');
var klarnaClientUtils = require('../klarnaClientUtils');

/**
 * fill modal gc form
 *
 * @param {string} checkBalanceUrl - check balance url to be hit
 * @param {string} addGiftCardUrl - add gift card url to be hit
 *
 */
function checkBalance(checkBalanceUrl, addGiftCardUrl) {
  $.spinner().start();
  var gcNumb = $('.gift-card-number .cardNumber').val();
  var gcPin = $('.gift-card-pin .accessNumber').val();
  $('.number-error-required').toggleClass('d-none', !!gcNumb); // ajay to work when to add/remove is-invalid class and span
  $('.pin-error-required').toggleClass('d-none', !!gcPin); // ajay to work when to add/remove is-invalid class and span
  if (!gcNumb || !gcPin) {
    clientSideValidation.checkValidationOnAjax($('.gift-card-form-group'), false, true);
    $('.generic-error').empty();
    $('.generic-error').addClass('d-none');
    $.spinner().stop();
    return;
  }
  $.ajax({
    url: checkBalanceUrl,
    method: 'POST',
    data: {
      token: $('.g-recaptcha-token').val(),
      apiKey: $('.g-recaptcha-token').data('secret'),
      gcNumber: gcNumb,
      gcPin: gcPin,
      checkout: true
    },
    success: function (responseData) {
      if (responseData.success) {
        $.ajax({
          url: addGiftCardUrl,
          method: 'POST',
          data: {
            gcNumber: gcNumb,
            gcPin: gcPin,
            balance: responseData.balance
          },
          success: function (resp) {
            // logic to show back the gift card added
            if (resp.error) {
              $('.generic-error').html(resp.errorMessage);
              $('.generic-error').removeClass('d-none');
            } else {
              $('.generic-error').addClass('d-none');
              if (resp.order.giftCard.hasGiftCard) {
                $('.gift-card-applied').empty();
                $('.gift-card-applied').html(resp.order.giftCard.giftCardHtml);
                $('.order-summary-payment-applied').empty();
                $('.order-summary-payment-applied').html(resp.order.orderSummaryPaymentHtml);
                $('li.nav-item.paypal').addClass('paypal-disable');
                $('#payPalRadioButton').attr('disabled', true);
              }

              if (resp.limitReached || resp.amountFinished) {
                $('.gift-card-form-group input').attr('disabled', 'disabled');
                $('.gift-card-form-group .giftcard-apply-submit button').attr('disabled', 'disabled');
              }
            }

            // update Klarna tab
            klarnaClientUtils.toggleKlarnaCheckoutTab(resp.error || resp.isKlarnaEnabled);

            // reCaptcha.callToken('payment');
            $('.gift-card-payment input').val('');
            $('.gift-card-payment input').next('span').remove();
            $('.gift-card-payment input').prev('label').removeClass('is-invalid').removeClass('input-focus');
            $.spinner().stop();
          },
          error: function (errorResp) {
            if (errorResp.responseJSON.redirectUrl) {
              window.location.href = errorResp.responseJSON.redirectUrl;
            }
            // reCaptcha.callToken('payment');
            $('.gift-card-payment input').val('');
            $('.gift-card-payment input').prev('label').removeClass('input-focus');
            $.spinner().stop();
          }
        });
        $('.gift-card-payment input').prev('label').removeClass('input-focus');
        $('.gift-card-payment').val('');
        clientSideValidation.checkValidationOnAjax($('.gift-card-form-group'), true, true);
        $('.generic-error').empty();
        $('.generic-error').addClass('d-none');
        floatLabel.resetFloatLabel();
      } else {
        if (responseData.limitReached) {
          $('.gift-card-form-group input').attr('disabled', 'disabled');
          $('.gift-card-form-group .giftcard-apply-submit button').attr('disabled', 'disabled');
        }
        // reCaptcha.callToken('payment');
        $('.gift-card-payment input').prev('label').removeClass('input-focus');
        $('.gift-card-payment input').val('');
        $.spinner().stop();
        clientSideValidation.checkValidationOnAjax($('.gift-card-form-group'), true, true);
        $('.generic-error').html(responseData.message);
        $('.generic-error').removeClass('d-none');
        floatLabel.resetFloatLabel();
      }
      if (responseData.botError) {
        $('.giftcard-apply-submit .giftcard-apply-btn').attr('disabled', 'disabled');
        $('.generic-error').html(responseData.error);
        $('.generic-error').removeClass('d-none');
      }
    },
    error: function (err) {
      // reCaptcha.callToken('payment');
      $('.gift-card-payment input').prev('label').removeClass('input-focus');
      $('.gift-card-payment input').val('');
      $.spinner().stop();
      clientSideValidation.checkValidationOnAjax($('.gift-card-form-group'), true, true);
      $('.generic-error').html(err.message);
      $('.generic-error').removeClass('d-none');
      if (err.botError) {
        $('.giftcard-apply-submit .giftcard-apply-btn').attr('disabled', 'disabled');
        $('.generic-error').html(err.error);
        $('.generic-error').removeClass('d-none');
      }

      floatLabel.resetFloatLabel();
    }
  });
}

/**
 * remove gift card from basket
 *
 * @param {url} removeGiftCardUrl - remove gift card url
 * @param {number} giftCard - gift card number to be removed
 */
function removeGiftCard(removeGiftCardUrl, giftCard) {
  $.spinner().start();
  $.ajax({
    url: removeGiftCardUrl,
    method: 'POST',
    data: {
      uuid: giftCard
    },
    success: function (responseData) {
      $('.generic-error').addClass('d-none');
      if (responseData.giftCard.hasGiftCard) {
        $('.gift-card-applied').empty();
        $('.gift-card-applied').html(responseData.giftCard.giftCardHtml);
        if (!$('li.nav-item.paypal').hasClass('paypal-disable')) {
          $('li.nav-item.paypal').addClass('paypal-disable');
          $('#payPalRadioButton').attr('disabled', true);
        }
      } else {
        $('.gift-card-applied').empty();
        $('li.nav-item.paypal').removeClass('paypal-disable');
        $('#payPalRadioButton').removeAttr('disabled');
      }
      $('.order-summary-payment-applied').empty();
      $('.order-summary-payment-applied').html(responseData.orderSummaryPaymentHtml);
      $('.gift-card-payment input').prev('label').removeClass('input-focus');
      $('.gift-card-payment input').val('');
      $('.credit-card-form input').removeAttr('disabled');
      $('.credit-card-content [name=dwfrm_billing_paymentMethod]').val('CREDIT_CARD');
      $('.payment-information').data('payment-method-id', 'CREDIT_CARD');
      $('.gift-card-form-group input').removeAttr('disabled');
      $('.gift-card-form-group .giftcard-apply-submit button').removeAttr('disabled');
      if ($('.saved-payment-instrument-section').length) {
        $('.saved-payment-security-code').removeAttr('disabled');
        $('.credit-card-selection-new').removeClass('disabled');
      }
      // $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
      $('.enableBillingButton').val(false);
      // reCaptcha .callToken('payment');
      if (responseData.botError) {
        $('.giftcard-apply-submit .giftcard-apply-btn').attr('disabled', 'disabled');
      }

      // update klarna tab
      klarnaClientUtils.toggleKlarnaCheckoutTab(responseData.isKlarnaEnabled);

      $.spinner().stop();
    },
    error: function (errorRsp) {
      if (errorRsp.responseJSON.redirectUrl) {
        window.location.href = errorRsp.responseJSON.redirectUrl;
      }
      $('.generic-error').html(errorRsp.responseJSON.message);
      $('.generic-error').removeClass('d-none');
      // reCaptcha .callToken('payment');
      $('.gift-card-payment input').prev('label').removeClass('input-focus');
      $('.gift-card-payment input').val('');
      // reCaptcha .callToken('payment');
      if (errorRsp.botError) {
        $('.giftcard-apply-submit .giftcard-apply-btn').attr('disabled', 'disabled');
      }

      $.spinner().stop();
    }
  });
}

module.exports = {
  giftCardBalanceCheck: function () {
    $('body').on('click', '.giftcard-apply-submit .giftcard-apply-btn', function (e) {
      e.preventDefault();
      $.each($('.gift-card-form-group .invalid-feedback'), function () {
        if ($(this).html()) {
          return;
        }
      });
      var checkBalanceUrl = $(this).data('action');
      var addGiftCardUrl = $(this).data('add-card');
      // eslint-disable-next-line no-undef
      grecaptcha.ready(function () {
        // eslint-disable-next-line no-undef
        grecaptcha.execute($('.google-recaptcha-key').html(), { action: 'gcpayment' })
          .then(function (token) {
            $('.g-recaptcha-token').val(token);
            checkBalance(checkBalanceUrl, addGiftCardUrl);
          });
      });
    });
  },
  removeGiftCardAction: function () {
    $('body').on('click', '.applied-gift-card button.remove-gift-card', function (e) {
      e.preventDefault();
      var removeGiftCardUrl = $(this).data('action');
      var giftCard = $(this).data('uuid');
      // eslint-disable-next-line no-undef
      grecaptcha.ready(function () {
        // eslint-disable-next-line no-undef
        grecaptcha.execute($('.google-recaptcha-key').html(), { action: 'gcpayment' })
          .then(function (token) {
            $('.g-recaptcha-token').val(token);
            removeGiftCard(removeGiftCardUrl, giftCard);
          });
      });
    });
  }
};
