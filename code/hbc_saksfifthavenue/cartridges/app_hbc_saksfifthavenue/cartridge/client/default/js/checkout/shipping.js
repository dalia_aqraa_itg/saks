'use strict';
var base = require('core/checkout/shipping');
var baseBase = require('base/checkout/shipping');
var clientSideValidation = require('core/components/clientSideValidation');
var addressHelpers = require('base/checkout/address');
var summaryHelpers = require('./summary');
var floatLabel = require('core/floatLabel');

/**
 * Check PO address and display the error message
 * @param {string} data - string to be validated
 */
function poCheck(data) {
  // var data = ($(this).val()).toLowerCase();
  if (data && data.length > 0 && $('.shipping-address-block').data('poenabled')) {
    var reGroups = [
      '^ *((#\\d+))',
      '((box|bin)[-. \\/\\\\]?\\d+)',
      '(.*p[ \\.]? ?(o|0)[-. \\/\\\\]? *-?((box|bin)|b|(#|num)?\\d+))',
      '(p(ost)? *(o(ff(ice)?)?)? *((box|bin)|b)? *\\d+)',
      '(p *-?\\/?(o)? *-?box)',
      'post office box',
      '(((box|bin)|b) *(number|num|#)? *\\d+)$',
      '(\\d+ *(box|post box|post office box)$)',
      '(((num|number|#) *\\d+)$)',
      '^[afd][ .]?p[ .]?o'
    ];
    for (var i = 0; i < reGroups.length; i++) {
      var pattern = new RegExp(reGroups[i], 'i');
      var poResult = pattern.test(data);
      if (poResult) {
        $('.po-check-message').removeClass('d-none');
        $('.po-check-message').addClass('error-added');
        // $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
        break;
      } else {
        $('.po-check-message').addClass('d-none');
        $('.po-check-message').removeClass('error-added');
      }
    }
  }
}

/**
 * Check FPO,APO, DPO address and display the error message
 * @param {string} data - string to be validated
 */
function fpoCheck(data) {
  var errorSet = false;
  if (!errorSet && data && data.address1 && data.address1.length > 0 && $('.shipping-address-block').data('poenabled')) {
    var reGroups = [
      '^ *((#\\d+))',
      '((box|bin)[-. \\/\\\\]?\\d+)',
      '(.*p[ \\.]? ?(o|0)[-. \\/\\\\]? *-?((box|bin)|b|(#|num)?\\d+))',
      '(p(ost)? *(o(ff(ice)?)?)? *((box|bin)|b)? *\\d+)',
      '(p *-?\\/?(o)? *-?box)',
      'post office box',
      '(((box|bin)|b) *(number|num|#)? *\\d+)$',
      '(\\d+ *(box|post box|post office box)$)',
      '(((num|number|#) *\\d+)$)',
      '^[afd][ .]?p[ .]?o'
    ];
    for (var i = 0; i < reGroups.length; i++) {
      var pattern = new RegExp(reGroups[i], 'i');
      var poResult = pattern.test(data.address1);
      if (poResult) {
        $('.po-check-message').removeClass('d-none');
        errorSet = true;
        break;
      } else {
        $('.po-check-message').addClass('d-none');
      }
    }
  }

  if (!errorSet && data && data.city && data.city.length > 0 && $('.shipping-address-block').data('poenabled')) {
    var reGroups = ['^[afd][ .]?p[ .]?o'];
    for (var i = 0; i < reGroups.length; i++) {
      var pattern = new RegExp(reGroups[i], 'i');
      var poResult = pattern.test(data.city);
      if (poResult) {
        $('.po-check-message').removeClass('d-none');
        errorSet = true;
        break;
      } else {
        $('.po-check-message').addClass('d-none');
      }
    }
  }

  if (!errorSet && data && data.stateCode && data.stateCode.length > 0 && $('.shipping-address-block').data('poenabled')) {
    var restrictedStates = $('.shipping-address-block').data('restricted-sates-usps');
    if (restrictedStates && restrictedStates !== null && restrictedStates.length > 0) {
      if (
        data.stateCode != '' &&
        (restrictedStates.indexOf(data.stateCode.toLowerCase()) >= 0 || restrictedStates.indexOf(data.stateCode.toUpperCase()) >= 0)
      ) {
        $('.po-check-message').removeClass('d-none');
        errorSet = true;
      } else {
        $('.po-check-message').addClass('d-none');
      }
    }
    checkRestrictedState(data.stateCode);
  }
}

/**
 * Check PO address and display the error message
 * @param {string} data - string to be validated
 */
function poFpoCheck(data) {
  // var data = ($(this).val()).toLowerCase();
  var messageSet = false;
  if (data && data.address1 && data.address1.length > 0 && $('.shipping-address-block').data('poenabled')) {
    var reGroups = [
      '^ *((#\\d+))',
      '((box|bin)[-. \\/\\\\]?\\d+)',
      '(.*p[ \\.]? ?(o|0)[-. \\/\\\\]? *-?((box|bin)|b|(#|num)?\\d+))',
      '(p(ost)? *(o(ff(ice)?)?)? *((box|bin)|b)? *\\d+)',
      '(p *-?\\/?(o)? *-?box)',
      'post office box',
      '(((box|bin)|b) *(number|num|#)? *\\d+)$',
      '(\\d+ *(box|post box|post office box)$)',
      '(((num|number|#) *\\d+)$)',
      '^[afd][ .]?p[ .]?o'
    ];
    for (var i = 0; i < reGroups.length; i++) {
      var pattern = new RegExp(reGroups[i], 'i');
      var poResult = pattern.test(data.address1.toLowerCase());
      if (poResult) {
        $('.po-check-message').removeClass('d-none');
        messageSet = true;
        // $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
        break;
      } else {
        $('.po-check-message').addClass('d-none');
      }
    }
  }

  if (data && data.city && data.city.length > 0 && $('.shipping-address-block').data('poenabled')) {
    var reGroups = ['^[afd][ .]?p[ .]?o'];
    for (var i = 0; i < reGroups.length; i++) {
      var pattern = new RegExp(reGroups[i], 'i');
      var poResult = pattern.test(data.city.toLowerCase());
      if (poResult) {
        $('.po-check-message').removeClass('d-none');
        messageSet = true;
        break;
      } else if (!messageSet) {
        $('.po-check-message').addClass('d-none');
      }
    }
  }

  if (data && data.stateCode && data.stateCode.length > 0 && $('.shipping-address-block').data('poenabled')) {
    var restrictedStates = $('.shipping-address-block').data('restricted-sates-usps');
    if (restrictedStates && restrictedStates !== null && restrictedStates.length > 0) {
      if (
        data.stateCode != '' &&
        (restrictedStates.indexOf(data.stateCode.toLowerCase()) >= 0 || restrictedStates.indexOf(data.stateCode.toUpperCase()) >= 0)
      ) {
        $('.po-check-message').removeClass('d-none');
        messageSet = true;
      } else if (!messageSet) {
        $('.po-check-message').addClass('d-none');
      }
    }
  }
}

/**
 * Check restricted state and display the error message
 * @param {string} data - string to be validated
 */
function checkRestrictedState(data) {
  var restrictedStates = $('.shipping-address-block').data('restricted-sates');
  if (restrictedStates && restrictedStates !== null && restrictedStates.length > 0) {
    if (data && data != '' && (restrictedStates.indexOf(data.toLowerCase()) >= 0 || restrictedStates.indexOf(data.toUpperCase()) >= 0)) {
      $('.restricted-state-message').removeClass('d-none');
      // $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
    } else {
      $('.restricted-state-message').addClass('d-none');
    }
  }
}

/**
 * Update list of available shipping methods whenever user modifies shipping address details.
 * @param {jQuery} $shippingForm - current shipping form
 */
function updateShippingMethodList($shippingForm) {
  // delay for autocomplete!
  setTimeout(function () {
    var $shippingMethodList = $shippingForm.find('.shipping-method-list');
    var urlParams = addressHelpers.methods.getAddressFieldsFromUI($shippingForm);
    var shipmentUUID = $shippingForm.find('[name=shipmentUUID]').val();
    var url = $shippingMethodList.data('actionUrl');
    urlParams.shipmentUUID = shipmentUUID;

    $.spinner().start();
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: urlParams,
      success: function (data) {
        if (data.error) {
          window.location.href = data.redirectUrl;
        } else {
          $('body').trigger('checkout:updateCheckoutView', {
            order: data.order,
            customer: data.customer,
            options: { keepOpen: true }
          });
          // toggle no shipping method message based on shipping form validation
          var result = clientSideValidation.checkFormvalidCheckout.call($('form.shipping-form'));
          if (result && $('form.shipping-form').closest('.checkout-primary-section').length !== 0) {
            if ($('.no-shipping-method-msg').attr('data-has-methods') != undefined && $('.no-shipping-method-msg').attr('data-has-methods') === 'true') {
              $('.no-shipping-method-msg').addClass('d-none');
              $('.next-step-button:visible').find('button[type="submit"]').removeAttr('disabled');
            } else if (
              $('.no-shipping-method-msg').attr('data-has-methods') != undefined &&
              $('.no-shipping-method-msg').attr('data-has-methods') === 'false'
            ) {
              $('.no-shipping-method-msg').removeClass('d-none');
              $('.next-step-button-disable:visible').find('button[type="submit"]').attr('disabled', 'disabled');
            }
          } else {
            $('.no-shipping-method-msg').addClass('d-none');
            $('.next-step-button:visible').find('button[type="submit"]').removeAttr('disabled');
          }
          /*          if (data.showDSMsg && data.showDSMsg !== '') {
        	  $(".dr-msg").text($(".dr-msg").text().replace("-", data.showDSMsg));
              $(".dr-msg").removeClass('d-none');
          } else {
        	  $(".dr-msg").addClass('d-none');
          }*/
          summaryHelpers.updateOrderProductSummaryInformation(data.order, data.options);
          if ($('.card.order-product-summary').length > 0) {
            $('.card.order-product-summary').html($(data.order.orderProductSummary).children());
          }
          $.spinner().stop();
        }
      }
    });
  }, 300);
}

function appendCommaAfterCity($addressContainer, city) {
  if (
    $addressContainer.length > 0 &&
    $($addressContainer).find('.city').length > 0 &&
    $($addressContainer).find('.city').text() !== '' &&
    city &&
    city.length > 0 &&
    city !== ''
  ) {
    var updatedCity = city + ',';
    $($addressContainer).find('.city').text(updatedCity);
  }
}

/**
 * updates the order shipping summary for an order shipment model
 * @param {Object} shipping - the shipping (shipment model) model
 * @param {Object} order - the order model
 */
function updateShippingSummaryInformation(shipping, order) {
  $('[data-shipment-summary=' + shipping.UUID + ']').each(function (i, el) {
    var $container = $(el);
    var $shippingAddressLabel = $container.find('.shipping-addr-label');
    var $addressContainer = $container.find('.address-summary');
    var $shippingPhone = $container.find('.shipping-phone');
    var $methodTitle = $container.find('.shipping-method-title');
    var $methodArrivalTime = $container.find('.shipping-method-arrival-time');
    var $methodPrice = $container.find('.shipping-method-price');
    var $shippingSummaryLabel = $container.find('.shipping-method-label');
    var $summaryDetails = $container.find('.row.summary-details');
    var giftMessageSummary = $container.find('.gift-summary');
    var signatureReq = $container.find('.signature-is-required');
    var $shippingEmail = $container.find('.shipping-email');

    var address = shipping.shippingAddress;
    var selectedShippingMethod = shipping.selectedShippingMethod;
    var isInStoreShipment = selectedShippingMethod && selectedShippingMethod.storePickupEnabled ? selectedShippingMethod.storePickupEnabled : false;
    var isGift = shipping.isGift;
    var signatureRequired = shipping.signatureRequired;
    var shipSignatureRequired = shipping.shipSignatureRequired;
    var shipSignatureThreshold = shipping.shipSignatureThreshold;

    addressHelpers.methods.populateAddressSummary($addressContainer, address);

    if (address && address.city) {
      appendCommaAfterCity($addressContainer, address.city);
    }

    if (address && address.phone) {
      $shippingPhone.text(address.phone);
    } else {
      $shippingPhone.empty();
    }
    if (address && address.email) {
      $shippingEmail.text(address.email);
    } else {
      $shippingEmail.empty();
    }

    if (selectedShippingMethod) {
      $('body').trigger('shipping:updateAddressLabelText', {
        selectedShippingMethod: selectedShippingMethod,
        resources: order.resources,
        shippingAddressLabel: $shippingAddressLabel
      });
      $shippingSummaryLabel.show();
      $summaryDetails.show();
      $methodTitle.text(selectedShippingMethod.displayName);
      if (selectedShippingMethod.estimatedArrivalTime) {
        $methodArrivalTime.text('( ' + selectedShippingMethod.estimatedArrivalTime + ' )');
      } else {
        $methodArrivalTime.empty();
      }
      $methodPrice.text(' - ' + selectedShippingMethod.finalShippingCost);
    }

    if (isGift) {
      //saks only change
      $('.saks-only.no-giftoptions').addClass('d-none');
      $container.find('.gift-message-recipientname.saks-only').closest('div').removeClass('d-none');

      giftMessageSummary.find('.gift-message-recipientname').text(shipping.giftRecipientName);
      giftMessageSummary.find('.gift-message-summary').text(shipping.giftMessage);
      if (shipping.giftWrapType) {
        if (shipping.giftWrapType === 'giftnote') {
          giftMessageSummary.find('.gift-note-only').removeClass('d-none');
          giftMessageSummary.find('.gift-wrap-only').addClass('d-none');
        } else if (shipping.giftWrapType === 'giftpack') {
          giftMessageSummary.find('.gift-note-only').addClass('d-none');
          giftMessageSummary.find('.gift-wrap-only').removeClass('d-none');
        }
      }
      giftMessageSummary.removeClass('d-none');
    } else {
      giftMessageSummary.addClass('d-none');
      //saks only change
      if ($('.saks-only.no-giftoptions').hasClass('show-go-step')) {
        $('.saks-only.no-giftoptions').removeClass('d-none');
      }
    }

    if (!isInStoreShipment && (signatureRequired || order.totals.grandTotalValue > shipSignatureThreshold)) {
      signatureReq.removeClass('d-none');
    } else {
      signatureReq.addClass('d-none');
    }

    if (shipping.applicableShippingMethods && shipping.applicableShippingMethods.length > 0) {
      $('.no-shipping-method-msg').attr('data-has-methods', true);
    } else {
      $('.no-shipping-method-msg').attr('data-has-methods', false);
    }
  });

  $('body').trigger('shipping:updateShippingSummaryInformation', { shipping: shipping, order: order });
}

/**
 * Update the shipping UI for a single shipping info (shipment model)
 * @param {Object} shipping - the shipping (shipment model) model
 * @param {Object} order - the order/basket model
 * @param {Object} customer - the customer model
 * @param {Object} [options] - options for updating PLI summary info
 * @param {Object} [options.keepOpen] - if true, prevent changing PLI view mode to 'view'
 */
baseBase.methods.updateShippingInformation = function (shipping, order, customer, options) {
  // First copy over shipmentUUIDs from response, to each PLI form
  order.shipping.forEach(function (aShipping) {
    aShipping.productLineItems.items.forEach(function (productLineItem) {
      baseBase.methods.updateProductLineItemShipmentUUIDs(productLineItem, aShipping);
    });
  });

  // Now update shipping information, based on those associations
  baseBase.methods.updateShippingMethods(shipping);

  baseBase.methods.updateShippingAddressFormValues(shipping);
  updateShippingSummaryInformation(shipping, order);

  // And update the PLI-based summary information as well
  shipping.productLineItems.items.forEach(function (productLineItem) {
    baseBase.methods.updateShippingAddressSelector(productLineItem, shipping, order, customer);
    baseBase.methods.updatePLIShippingSummaryInformation(productLineItem, shipping, order, options);
  });

  $('body').trigger('shipping:updateShippingInformation', {
    order: order,
    shipping: shipping,
    customer: customer,
    options: options
  });
};

/**
 * Does Ajax call to select shipping method
 * @param {string} url - string representation of endpoint URL
 * @param {Object} urlParams - url params
 * @param {Object} el - element that triggered this call
 */
function selectShippingMethodAjax(url, urlParams, el) {
  $.spinner().start();
  $.ajax({
    url: url,
    type: 'post',
    dataType: 'json',
    data: urlParams
  })
    .done(function (data) {
      var dropshipDefaultMethod = el.attr('data-dropshipdefault-id');
      if (data.error) {
        window.location.href = data.redirectUrl;
      } else {
        $('body').trigger('checkout:updateCheckoutView', {
          order: data.order,
          customer: data.customer,
          options: { keepOpen: true },
          urlParams: urlParams
        });
        $('body').trigger('checkout:postUpdateCheckoutView', {
          el: el
        });
        // update drop ship restriction message
        data.order.shipping.forEach(function (shipping) {
          if (
            data.hasDropShipItems &&
            dropshipDefaultMethod &&
            dropshipDefaultMethod !== 'null' &&
            shipping.selectedShippingMethod &&
            shipping.selectedShippingMethod.ID !== dropshipDefaultMethod &&
            shipping.selectedShippingMethod.ID !== 'sameDayDelivery' &&
            shipping.selectedShippingMethod.ID !== 'sameDayDeliveryShoprunner'
          ) {
            let id1 = el.siblings('.dropship-msg').attr('data-shipping-id1');
            let id2 = el.siblings('.dropship-msg').attr('data-shipping-id2');
            let dropShipMsg = el.siblings('.dropship-msg').text().replace('-', shipping.selectedShippingMethod.displayName);
            if (id1 !== undefined && id1 !== '' && id2 !== undefined && id2 !== '') {
              dropShipMsg = dropShipMsg.replace(id1, id2);
            }
            el.siblings('.hbc-alert-info').removeClass('d-none');
            el.siblings('.hbc-alert-info').html(dropShipMsg);
            $('.dr-msg').addClass('d-none');
          } else if (
            data.hasDropShipItems &&
            dropshipDefaultMethod &&
            dropshipDefaultMethod !== 'null' &&
            shipping.selectedShippingMethod &&
            shipping.selectedShippingMethod.ID === dropshipDefaultMethod &&
            shipping.selectedShippingMethod.ID !== 'sameDayDelivery' &&
            shipping.selectedShippingMethod.ID !== 'sameDayDeliveryShoprunner'
          ) {
            $('.dr-msg').removeClass('d-none');
            el.siblings('.hbc-alert-info').addClass('d-none');
            el.siblings('.hbc-alert-info').html('');
          } else {
            el.siblings('.hbc-alert-info').addClass('d-none');
            el.siblings('.hbc-alert-info').html('');
            $('.dr-msg').addClass('d-none');
          }
          if (shipping.selectedShippingMethod) {
            el.siblings('.hbc-shipping-method-description').removeClass('d-none');
            el.siblings('.hbc-shipping-method-description').html(shipping.selectedShippingMethod.description);
          }
        });
        // update signature required
        if (data.signatureRequired && data.signatureThreshold) {
          if (data.order.totals.grandTotalValue > data.signatureThreshold) {
            $('.signature-required').removeClass('d-none');
            $('.signature-checkbox').addClass('d-none');
            $('.signature-is-required').removeClass('d-none');
          } else {
            $('.signature-required').addClass('d-none');
            $('.signature-checkbox').removeClass('d-none');
            $('.signature-is-required').addClass('d-none');
          }
        }
      }
      // show error message on non-canadian address entry on shipping form
      var addressInfo = $('input[name="address"]:checked').data('address-info');
      if (addressInfo) {
        // eslint-disable-next-line no-undef
        if (defaultShippingCountry && addressInfo.countryCode && addressInfo.countryCode.value !== defaultShippingCountry && $('.coutry-rest-msg').length > 0) {
          $('.coutry-rest-msg').removeClass('d-none');
          // $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
          // disable address edit for non candaian address
          if ($('input[name="address"]:checked').closest('.customer-addresses-section').find('.edit-customer-address').length > 0) {
            $('input[name="address"]:checked').closest('.customer-addresses-section').find('.edit-customer-address').addClass('no-mouse-events');
          }
        } else {
          $('.coutry-rest-msg').addClass('d-none');
        }
      }
      $.spinner().stop();
    })
    .fail(function () {
      $.spinner().stop();
    });
}

(base.isGift = function () {
  $('.gift').on('change', function (e) {
    e.preventDefault();
    var form = $(this).closest('form');
    var giftEligible = $(this).attr('data-giftEligible');
    if (this.checked) {
      //$(this).next('label').find('span.closed').addClass('d-none').next('span.opened').removeClass('d-none');
      $(form).find('.gift-option-container').removeClass('d-none');
      if (giftEligible && giftEligible === 'none') {
        $(form).find('input[name$=_shippingAddress_isGift]').removeAttr('checked').val('');
      } else {
        $(form).find('input[name$=_shippingAddress_isGift]').attr('checked', true).val('true');
      }
      $(form).find('input[value=giftnote]').prop('checked', true);
      $(form).find('input[name*=_giftRecipientName]').parents('div .form-group').addClass('required');
      $(form).find('input[name*=_giftRecipientName]').prop('required', true);
      //$(form).find('input[name*=_giftRecipientName]').focus();
      clientSideValidation.checkValidationOnAjax($('.shipping-form'), true, true);
      floatLabel.resetFloatLabel();
    } else {
      $(form).find('input[name$=_shippingAddress_isGift]').removeAttr('checked');
      //$(this).next('label').find('span.closed').removeClass('d-none').next('span.opened').addClass('d-none');
      $(form).find('.gift-option-container').addClass('d-none');
      $(form).find('input[name*=_giftRecipientName]').val('');
      $(form).find('textarea[name*=_giftMessage]').val('');
      $(form).find('input[value=giftnote]').prop('checked', true);
      $(form).find('input[name*=_gift-message]').parents('div .form-group').removeClass('required');
      $(form).find('input[name*=_gift-message]').prop('required', false);
      clientSideValidation.checkValidationOnAjax($('.shipping-form'), true, true);
    }
  });

  $('.remove-gift-options').on('click', function () {
    $(document).find('.gift-message-block input.gift').prop('checked', false);
    $('.gift').trigger('change');
  });

  if ($('.gift-option-container').is(':visible')) {
    $('form.shipping-form').find('input[name*=_giftRecipientName]').parents('div .form-group').addClass('required');
    $('form.shipping-form').find('input[name*=_giftRecipientName]').prop('required', true);
  }
}),
  (base.handleNewAddressLabel = function () {
    $(document).on('click', '.edit-customer-address, .customer-addresses-section', function () {
      $('.shipping-address-block').addClass('enable-new-address-label');
    });
  }),
  (base.selectShippingMethod = function () {
    $('.shipping-method-list').change(function (e) {
      // skip for delivery instruction textarea
      if ($(e.target).hasClass('delivery-instructions')) {
        return;
      }
      var $shippingForm = $(this).parents('form');
      var methodID = $(':checked', this).val();
      var shipmentUUID = $shippingForm.find('[name=shipmentUUID]').val();
      var urlParams = addressHelpers.methods.getAddressFieldsFromUI($shippingForm);
      urlParams.shipmentUUID = shipmentUUID;
      urlParams.methodID = methodID;
      urlParams.isGift = $shippingForm.find('.gift').prop('checked');
      urlParams.giftMessage = $shippingForm.find('textarea[name$=_giftMessage]').val();
      urlParams.giftRecipientName = $shippingForm.find('input[name$=_giftRecipientName]').val();

      var url = $(this).data('select-shipping-method-url');
      selectShippingMethodAjax(url, urlParams, $(this));
    });
  }),
  (base.updateShippingList = function () {
    var baseObj = this;

    $('input[name$="shippingAddress_addressFields_postalCode"]').on('blur', function (e) {
      if ($(this).val() !== '') {
        updateShippingMethodList($(e.currentTarget.form));
      }
    });
  }),
  (base.updateShippingMethodsOnLoad = function () {
    var baseObj = this;
    // set invalidShipping Country if unable to ship there
    var $addressSection = $('.shipping-content .form-check.customer-addresses-section.selected');
    var $addressInfo = $addressSection.find('input[name=address]').data('address-info');
    var $shippingMethods = $('.shipping-content .shipping-method-list').find('input');

    var invalidShippingCountry = false;
    if (
      defaultShippingCountry &&
      !!$addressInfo &&
      $addressInfo.countryCode &&
      $addressInfo.countryCode.value &&
      $addressInfo.countryCode.value !== defaultShippingCountry
    ) {
      invalidShippingCountry = true;
    }
    if (
      !invalidShippingCountry &&
      $('.data-checkout-stage').data('customer-type') === 'registered' &&
      $('form.shipping-form .customer-addresses-section:visible').length > 1
    ) {
      updateShippingMethodList($('form.shipping-form'));
    }
    if (invalidShippingCountry && $('.coutry-rest-msg').length > 0) {
      $('.coutry-rest-msg').removeClass('d-none');
      // disable address edit for non canadian address
      if ($addressSection.find('input[name=address]').closest('.customer-addresses-section').find('.edit-customer-address').length > 0) {
        $addressSection.find('input[name=address]').closest('.customer-addresses-section').find('.edit-customer-address').addClass('no-mouse-events');
      }
      $shippingMethods.each(function () {
        $(this).attr('disabled', 'disabled');
      });
      // $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
    } else {
      $('.coutry-rest-msg').addClass('d-none');
      $shippingMethods.each(function () {
        $(this).removeAttr('disabled');
      });
      $('.next-step-button:visible').find('button[type="submit"]').removeAttr('disabled');
    }
    if ($('.no-shipping-method-msg').attr('data-has-methods') != undefined && $('.no-shipping-method-msg').attr('data-has-methods') === 'true') {
      $('.no-shipping-method-msg').addClass('d-none');
      $('.next-step-button:visible').find('button[type="submit"]').removeAttr('disabled');
    } else if ($('.no-shipping-method-msg').attr('data-has-methods') != undefined && $('.no-shipping-method-msg').attr('data-has-methods') === 'false') {
      $('.no-shipping-method-msg').removeClass('d-none');
      //$('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
    }
  }),
  (base.addNewAddressRegistered = function () {
    $(document).on('click', 'form.shipping-form .address-add-registered .customer-addresses-section', function () {
      var shipmentUUID = $('input[name="shipmentUUID"]').val();
      var form = $('form.shipping-form');
      var postalCode = $('input[name$=_postalCode]', form).val();
      var addressSubmit = form.data('address-submit');
      var shippingArray = [];
      var shipping = {};
      var order = {};
      if (shipmentUUID) {
        shipping.UUID = shipmentUUID;
        shippingArray.push(shipping);
        order.shipping = shippingArray;

        base.methods.clearShippingForms(order);
        // commented below lines to fix SFSX-2430
        /*            if (postalCode !== undefined && postalCode !== 'undefined' && postalCode !== '') {
            	$('input[name$=_postalCode]', form).val(postalCode);
            }*/
        // Add Country Code Selected
        $('select[name$=_country]', form).val($('select[name$=_country] option:eq(1)', form).val());
        $('input[name$=_firstName]', form).val();
        // set default address unchecked
        // eslint-disable-next-line no-unused-vars
        $('input[value=' + shipping.UUID + ']').each(function (formIndex, el) {
          $('input[name$=_setAsDefault]', form).prop('checked', false);
        });
      }
      if (addressSubmit) {
        form.attr('action', addressSubmit);
      }
      $('.shipping-method-block').addClass('d-none');
      $('.gift-message-block').addClass('d-none');
      $(this).closest('.address-add-registered').addClass('d-none');
      $('.shipping-content').find('div.shipping-next-step-button-row').removeClass('d-none');
      $('.shipping-content').find('div.shipping-next-step-button-row:not(.shipping-save-address)').addClass('d-none');
      $('.shipping-address-block').removeClass('d-none');
      $('#customer-addresses').addClass('d-none');
      $('.coutry-rest-msg').addClass('d-none');
      $('.signature-required').addClass('d-none');
      $('.signature-checkbox').addClass('d-none');
      floatLabel.resetFloatLabel();
      clientSideValidation.checkValidationOnAjax($('.shipping-form'), true);
    });
  }),
  (base.setFreeGiftWrapCSR = function () {
    // set session variable to set free gift wrap
    $(document).on('click', 'form.shipping-form .csr-set-giftwrap', function () {
      var url = $(this).attr('data-url');
      if (url && url !== '') {
        $.ajax({
          url: url,
          success: function (data) {
            window.location.reload();
          }
        });
      }
    });
  }),
  (base.selectAddress = function () {
    var baseObj = this;
    $(document).on('click', '.shipping-content .form-check.customer-addresses-section', function (e) {
      e.preventDefault();
      var shippingAddress = {};
      var shipmentUUID = $('[name=shipmentUUID]').val();
      var address = {};
      var addressInfo = $(this).find('input[name=address]').data('address-info');

      // set invalidShipping Country if unable to ship there
      var invalidShippingCountry = false;
      if (defaultShippingCountry && addressInfo.countryCode && addressInfo.countryCode.value && addressInfo.countryCode.value !== defaultShippingCountry) {
        invalidShippingCountry = true;
      }

      $('.shipping-content .form-check.customer-addresses-section').removeClass('selected');
      $(this).addClass('selected');
      $(this).find('input[name="address"]').prop('checked', true);
      if (addressInfo && shipmentUUID) {
        address.UUID = shipmentUUID;
        shippingAddress = addressInfo;
        address.shippingAddress = shippingAddress;
        baseBase.methods.updateShippingAddressFormValues(address);

        if (!invalidShippingCountry) {
          updateShippingMethodList($('form.shipping-form'), true, addressInfo);
        }

        $('input[name$=_shippingAddress_addressFields_address2]', 'form.shipping-form').val(addressInfo.address2);
        if (addressInfo.email) {
          $('input[name$=_shippingAddress_email]', 'form.shipping-form').val(addressInfo.email);
        } else if ($(this).find('input[name=address]').data('customer-email')) {
          $('input[name$=_shippingAddress_email]', 'form.shipping-form').val($(this).find('input[name=address]').data('customer-email'));
        }
        // restricted state message
        checkRestrictedState(addressInfo.stateCode);
        // po message
        if (addressInfo) {
          poFpoCheck(addressInfo);
        }
        var $shippingMethods = $('.shipping-content .shipping-method-list').find('input');
        // eslint-disable-next-line no-undef
        if (invalidShippingCountry && $('.coutry-rest-msg').length > 0) {
          $('.coutry-rest-msg').removeClass('d-none');
          // disable address edit for non canadian address
          if ($(this).find('input[name=address]').closest('.customer-addresses-section').find('.edit-customer-address').length > 0) {
            $(this).find('input[name=address]').closest('.customer-addresses-section').find('.edit-customer-address').addClass('no-mouse-events');
          }
          $shippingMethods.each(function () {
            $(this).attr('disabled', 'disabled');
          });
          $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
        } else {
          $('.coutry-rest-msg').addClass('d-none');
          $shippingMethods.each(function () {
            $(this).removeAttr('disabled');
          });
          $('.next-step-button:visible').find('button[type="submit"]').removeAttr('disabled');
        }
        if ($('.no-shipping-method-msg').attr('data-has-methods') != undefined && $('.no-shipping-method-msg').attr('data-has-methods') === 'true') {
          $('.no-shipping-method-msg').addClass('d-none');
          $('.next-step-button:visible').find('button[type="submit"]').removeAttr('disabled');
        } else if ($('.no-shipping-method-msg').attr('data-has-methods') != undefined && $('.no-shipping-method-msg').attr('data-has-methods') === 'false') {
          $('.no-shipping-method-msg').removeClass('d-none');
          $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
        }
      }
    });
  }),
  (base.poAddressCheck = function () {
    $('.shippingAddressOne').on('keyup input change', function () {
      var data = $(this).val().toLowerCase();
      poCheck(data);
    });
  }),
  (base.fpoAddressCheck = function () {
    $('.shippingAddressCity, .shippingAddressOne, .shippingState').on('keyup input change blur', function () {
      var data = {};
      var address1 = $('.shippingAddressOne').val();
      var city = $('.shippingAddressCity').val();
      var stateCode = $('.shippingState').val();
      if (address1 !== undefined && address1 !== null && address1 !== 'null') {
        address1 = address1.toLowerCase();
      }
      if (city !== undefined && city !== null && city !== 'null') {
        city = city.toLowerCase();
      }
      data.stateCode = stateCode;
      data.address1 = address1;
      data.city = city;
      fpoCheck(data);
    });
  });

module.exports = base;
