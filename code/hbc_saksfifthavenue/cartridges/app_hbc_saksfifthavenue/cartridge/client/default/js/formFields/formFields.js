'use strict';

var clientSideValidation = require('core/components/clientSideValidation');
var focusEnabled = false;

/**
 * Needed because there was the autofill suggestion occurs.
 */
function adjustForAutofill() {
  $('.form-group:visible').each(function () {
    var $self = $(this);
    var $input;
    var $inputLabel = $self.find('label');
    var $inputLength;
    if ($self.find('select').length) {
      if ($self.find('.selected-option').length > 0) {
        $input = $self.find('.selected-option');
        $inputLength = $input.text().trim();
      } else {
        $input = $self.find('select option:selected');
        $inputLength = $input.val();
      }
    } else if ($self.find('input').length) {
      $input = $self.find('input');
      $inputLength = $input.val();
    } else {
      $input = $self.find('textarea');
      $inputLength = $input.val();
    }
    if ($inputLength && $inputLength.length && !$inputLabel.hasClass('input-focus')) {
      $inputLabel.addClass('input-focus');
    }
    if (
      ($input.hasClass('shippingZipCode') || $input.hasClass('billingZipCode') || $input.hasClass('addressFromZipcode')) &&
      $inputLabel.hasClass('input-focus')
    ) {
      focusEnabled = true;
    }
  });
}

function quantityHoverBorder() {
  $('body')
    .find('.quantity-field')
    .on('focus mouseover', ' .btn-number', function () {
      if ($(this).attr('disabled') !== 'disabled') {
        $(this).closest('.quantity-field').find('.input-number').addClass('hover-border');
      }
    })
    .on('focusout mouseout', ' .btn-number', function () {
      $(this).closest('.quantity-field').find('.input-number').removeClass('hover-border');
    });
}
/**
 * When clicking on the document we are closing the custom select dropdown.
 */
function closeCustomSelect() {
  $('.custom-select.current_item select').trigger('blur');
  $('.custom-select').removeClass('current_item');
  $('.custom-select li').removeClass('hover');
}

function findInsiders($element) {
  if (typeof $element === 'undefined') {
    return;
  }

  var tabbable = $element.find('select, input, textarea, button, a').filter(':visible');

  var firstTabbable = tabbable.first();
  var lastTabbable = tabbable.last();
  /*set focus on first input*/
  firstTabbable.focus();

  /*redirect last tab to first input*/
  lastTabbable.on('keydown', function (e) {
    if (e.which === 9 && !e.shiftKey) {
      e.preventDefault();
      firstTabbable.focus();
    }
  });

  /*redirect first shift+tab to last input*/
  firstTabbable.on('keydown', function (e) {
    if (e.which === 9 && e.shiftKey) {
      e.preventDefault();
      lastTabbable.focus();
    }
  });
}

/**
 * Converting the default select dropdown to unorder list HTML structure.
 */
function updateSelect() {
  $('.custom-select').each(function () {
    var $this = $(this);
    if ($this.find('.selected-option').length === 0) {
      $this.append('<div class="selected-option"></div>');
    }
    if ($this.find('.selection-list').length === 0) {
      $this.append('<ul class="selection-list"></ul>');
      var $list = '';
      var $listClass = '';

      $this.find('option').each(function () {
        if ($(this).attr('class') && $(this).attr('class') !== undefined && $(this).attr('class') !== '') {
          $listClass = $(this).attr('class');
        }
        if ($(this).attr('disabled') === 'disabled') {
          $listClass = 'option-disabled';
        }
        var styleAttr;
        if ($(this).data('background-color')) {
          styleAttr = 'style="background-color:' + $(this).data('background-color') + '"';
        } else if ($(this).data('background-image')) {
          styleAttr = 'style="background-image: url(' + $(this).data('background-image') + ')"';
        }
        if ($(this).closest('.custom-select').hasClass('custom-color-dropdown')) {
          if (!styleAttr) {
            $list +=
              '<li label="' +
              $(this).text().toLocaleLowerCase() +
              '" class="' +
              $listClass +
              '"><div class="item" style="text-align:center;"><div class="inner-item"></div>' +
              $(this).text() +
              '</div></li>';
          } else {
            $list +=
              '<li label="' +
              $(this).text().toLocaleLowerCase() +
              '" class="' +
              $listClass +
              '"><div class="item"><div class="inner-item"></div><span class="swatch-color-holder swatch-circle-' +
              $(this).text().toLocaleLowerCase().trim() +
              '"' +
              styleAttr +
              '></span>' +
              $(this).text().toLocaleLowerCase() +
              '</div></li>';
          }
        } else {
          $list += '<li label="' + $(this).text().toLocaleLowerCase() + '" class="' + $listClass + '">' + $(this).text() + '</li>';
        }
      });
      $this.find('.selection-list').append($list);
    } else {
      $this.find('.selection-list li').each(function () {
        $this.attr('label', $.trim($(this).text().toLocaleLowerCase()));
      });
    }

    if ($(this).hasClass('custom-color-dropdown')) {
      var styleAttr;
      var currElement = $(this).find('select option:selected');
      if (currElement.data('background-color')) {
        styleAttr = 'style="background-color:' + currElement.data('background-color') + '"';
      } else if (currElement.data('background-image')) {
        styleAttr = 'style="background-image: url(' + currElement.data('background-image') + ')"';
      }
      if (!styleAttr) {
        $this.find('.selected-option').html($(this).find('select option:selected').text());
      } else {
        $this
          .find('.selected-option')
          .html(
            '<span class="swatch-color-holder swatch-circle-' +
              currElement.text().toLocaleLowerCase().trim() +
              '"' +
              styleAttr +
              '></span>' +
              $(this).find('select option:selected').text().toLocaleLowerCase()
          );
      }
    } else {
      $this.find('.selected-option').text($(this).find('select option:selected').text());
    }
    $this.find('.selection-list li').removeClass('selected').removeClass('hover').eq($(this).find('select option:selected').index()).addClass('selected');

    if ($(this).find('select option:selected').val() !== '') {
      $this.find('.selected-option').addClass('selected');
    } else {
      $this.find('.selected-option').removeClass('selected');
    }
    if ($this.find('.selection-list li .bold').length > 0) {
      $this.find('.selected-option').html($this.find('.selection-list li').eq($this.find('select option:selected').index()).html());
    }

    $this.find('li').removeClass('selected');
    $this.find('li').eq($(this).find('select option:selected').index()).addClass('selected');
    $this.find('.invalid-feedback').appendTo(this);

    /* if ($(this).hasClass("custom-color-dropdown")) {
      if (window.innerWidth >= 544) {
        activatePerfectScroll(
          $(".custom-color-dropdown").find(".selection-list")[0]
        );
      }

    }*/
  });
}

function addMenuOverlay() {
  $('.modal-background').addClass('background').css('top', 0);
}

function removeMenuOverlay() {
  $('.modal-background').removeClass('background').removeAttr('style');
}

module.exports = {
  updateSelect: updateSelect,
  adjustForAutofill: adjustForAutofill,
  addMenuOverlay: addMenuOverlay,
  removeMenuOverlay: removeMenuOverlay,
  quantityHoverBorder: quantityHoverBorder,
  findInsiders: findInsiders,
  handleEDQCallbackAddress: function () {
    if ($('#edq-verification-suggestion-box').length > 0) {
      window.MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
      var target = document.querySelector('#edq-verification-suggestion-box'),
        observer = new MutationObserver(function (mutation) {
          if (!$('#edq-verification-suggestion-box').is(':visible')) {
            setTimeout(function () {
              $('form.address-form')
                .find('input:visible, select:visible')
                .each(function () {
                  if ($(this).val() !== '') {
                    clientSideValidation.validateFormonBlur.call(this);
                  }
                });
              adjustForAutofill();
            }, 1500);
          }
        }),
        config = {
          attributes: true
        };
      observer.observe(target, config);
    }
  },
  adjustAddressAutoFill: function () {
    $('body').on('mousedown', '.edq-global-intuitive-address-suggestions div', function () {
      // SFDEV-4665 | Changes for QAS float foxus and shipping blur
      var setFocus = setInterval(changeFocus, 200);

      function changeFocus() {
        if (focusEnabled) {
          clearInterval(setFocus);
          focusEnabled = false;
        } else {
          adjustForAutofill();
          if ($('body').find('.checkout-primary-section').length) {
            $('form:visible input.shippingAddressOne:visible, form:visible input.billingAddressOne:visible')
              .closest('form')
              .find('input:visible, select:visible')
              .each(function () {
                if ($(this).val() !== '') {
                  clientSideValidation.validateFormonBlur.call(this);
                  if (this.name === 'dwfrm_shipping_shippingAddress_addressFields_postalCode') {
                    $('input[name$="shippingAddress_addressFields_postalCode"]').blur();
                  }
                }
                var result = clientSideValidation.checkFormvalidCheckout.call(this);
                if (result && $(this).closest('.checkout-primary-section').length !== 0) {
                  $('.next-step-button:visible').find('button[type="submit"]').removeAttr('disabled');
                } else {
                  //$('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
                }
              });
          } else {
            $('form:visible input#address1:visible')
              .closest('form')
              .find('input:visible, select:visible')
              .each(function () {
                if ($(this).val() !== '') {
                  clientSideValidation.validateFormonBlur.call(this);
                }
              });
          }
          setTimeout(function () {
            adjustForAutofill();
          }, 1000);
        }
      }
    });
  },
  inputfocusEvent: function () {
    // Needs the focus event to do the floating label value.
    $('body')
      .off('focus', '.form-group input, .form-group select, .form-group textarea')
      .on('focus', '.form-group input, .form-group select, .form-group textarea', function () {
        var $label = $(this).closest('.form-group').find('label');
        if ($label.length && !$label.hasClass('input-focus')) {
          $label.addClass('input-focus');
        }
      });
    // Needs blur event to set the float the label value as normal position.
    $('body')
      .off('blur', '.form-group input, .form-group select, .form-group textarea')
      .on('blur', '.form-group input, .form-group select, .form-group textarea', function () {
        // eslint-disable-line
        var $label = $(this).closest('.form-group').find('label');
        // Handled auto-focus issue only in case of IE for address select box CO flow.
        if ($(this).hasClass('selected-option') && $(this).text().length !== '') {
          return false;
        }
        if ($(this).attr('id') === 'registration-form-hudsonbay-rewards' && $(this).next('span.hbc-loyalty-number-prefix:visible').length !== 0) {
          return false;
        }
        if ($(this).val() !== null) {
          if (!$(this).hasClass('do-not-float')) {
            if (!$(this).val().length && $label.hasClass('input-focus')) {
              $label.removeClass('input-focus');
            }
          }
        } else if ($label.hasClass('input-focus')) {
          $label.removeClass('input-focus');
        }
      });
  },
  // Animating the input changes periodically. Needed because there is no standard cross-browser event that fires when autofilling/autocorrecting occurs.
  setAdjustForAutofill: function () {
    $(document).ready(function () {
      if (navigator.userAgent.indexOf('Firefox') > -1) {
        adjustForAutofill();
      } else {
        $('input[type="password"]:-webkit-autofill, input[type="email"]:-webkit-autofill, input[type="text"]:-webkit-autofill').each(function () {
          $(this).closest('.form-group').find('label').addClass('input-focus');
        });
      }
    });
  },
  selectbox: function () {
    // var $windowMedia = window.matchMedia('(min-width: 1025px)').matches;
    var $windowMedia = true;
    this.updateSelect();

    $(document)
      .on('click', '.selected-option', function () {
        var $h = 0;
        var $this = $(this);
        if ($this.siblings('select').is(':disabled')) {
          return false;
        }

        if ($windowMedia) {
          $('.custom-select.current_item select').trigger('blur');
          window.$currentkeycode = '';
          $('.custom-select').not($this.closest('.custom-select')).removeClass('current_item');
          if (window.innerWidth < 600) {
            //Mobile fix
            $this.siblings('.selection-list').css('top', '5px');
          } else {
            $this.siblings('.selection-list').css('top', $this.outerHeight());
          }
          $this.closest('.custom-select').toggleClass('current_item');

          if (!$this.closest('.custom-select').hasClass('current_item')) {
            $this.siblings('select').trigger('blur');
          }

          if ($this.siblings('.selection-list').find('li').length > 10) {
            $.each($this.siblings('.selection-list').find('li:visible').slice(0, 10), function () {
              $h += $(this).outerHeight();
              $this.siblings('.selection-list').height($h - 1);
            });

            var h = 0;
            $.each(
              $this
                .closest('.custom-select')
                .find('.selection-list li:visible')
                .splice(0, $this.closest('.custom-select').find('.selection-list li.selected:visible').index() - 1),
              function () {
                h += $(this).outerHeight();
              }
            );
            $this.siblings('.selection-list').scrollTop(h);
          } else {
            $this.siblings('.selection-list').height('auto');
          }
        }
        return true;
      })
      .on('click', '.selection-list li', function () {
        if ($windowMedia) {
          var $item = $(this).closest('.custom-select');
          if ($item.find('select option').eq($(this).index()).text() === $item.find('.selected-option.selected').text()) {
            $item.removeClass('current_item');
            return false;
          }
          //removing as per delta in avenue to make unavailable color clickable

          /*if ($(this).hasClass('option-disabled')) {
                    return false;
                }*/

          $item.find('li').removeClass('hover selected');
          $(this).addClass('selected');
          $item.find('select option').eq($(this).index()).prop('selected', true);
          $item.find('.selected-option').text($(this).text());

          $item.removeClass('current_item');
          $item.find('select').trigger('change');
          $item.find('select').trigger('blur');
          if ($item.find('.selected-option').text().trim() === '') {
            $item.find('label').removeClass('input-focus');
          } else {
            $item.find('label:not(".input-focus")').addClass('input-focus');
          }
        }
        return true;
      })
      .on('change input', 'select', function () {
        updateSelect();
      });

    $(document).on('click', function (e) {
      if ($windowMedia && !$('.custom-select.current_item').is(e.target) && !$('.custom-select.current_item').find('*').is(e.target)) {
        closeCustomSelect();
      }
    });

    $('body')
      .off('keydown')
      .on('keydown', function (e) {
        /* eslint-disable */
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        var $activeSelect = $('.custom-select.current_item');
        var $mac = navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i) ? true : false;
        /* eslint-enable */

        if ($activeSelect.length > 0 && $windowMedia) {
          $activeSelect.find('select').focus();
          $activeSelect.addClass('current_item');
          var $list = $activeSelect.find('li');
          var $active;

          if (!$activeSelect.find('li.selected').length) {
            $activeSelect.find('li').eq($activeSelect.find('option:selected').index()).addClass('selected');
          }

          if (key === 8) {
            // Delete key
            window.$currentkeycode = window.$currentkeycode === undefined ? '' : window.$currentkeycode.slice(0, -1);
          } else {
            window.$currentkeycode += window.$currentkeycode === undefined ? '' : $.trim(String.fromCharCode(key).toLowerCase());
          }
          $active = $activeSelect.find('li[label^="' + window.$currentkeycode + '"]');

          if ($active.length === 0) {
            window.$currentkeycode += window.$currentkeycode === undefined ? '' : $.trim(String.fromCharCode(key));
            window.$currentkeycode = window.$currentkeycode.substr(window.$currentkeycode.length - 1, 1);
            $active = $activeSelect.find('li[label^="' + window.$currentkeycode + '"]');
          }

          if (key === 40) {
            if ($activeSelect.find('li.selected').length > 0 && $activeSelect.find('li.hover').length === 0) {
              $activeSelect.find('li.selected').addClass('hover');
            }
            $active = $activeSelect.find('li.hover');
            $activeSelect.find('li').removeClass('hover');
            $active.next().addClass('hover');
          } else if (key === 38) {
            if ($activeSelect.find('li.selected').length > 0 && $activeSelect.find('li.hover').length === 0) {
              $activeSelect.find('li.selected').addClass('hover');
            }
            $active = $activeSelect.find('li.hover');
            if (!$active.prev().hasClass('hide')) {
              $activeSelect.find('li').removeClass('hover');
              $active.prev().addClass('hover');
            }
          } else if (key === 13) {
            if ($active.length === 0 && $activeSelect.find('li.hover').length > 0) {
              $activeSelect.find('li.hover').trigger('click');
            } else {
              $active.eq(0).trigger('click');
              if ($mac) {
                $('.custom-select').find('select').css({
                  /* eslint-disable */
                  left: '0px'
                  /* eslint-enable */
                });
              } else {
                $('.custom-select').find('select').css({
                  /* eslint-disable */
                  left: '-9999px'
                  /* eslint-enable */
                });
              }
            }
          } else if (key === 9 || key === 27) {
            closeCustomSelect();
            if ($mac) {
              $('.custom-select').find('select').css({
                /* eslint-disable */
                left: '0px'
                /* eslint-enable */
              });
            } else {
              $('.custom-select').find('select').css({
                /* eslint-disable */
                left: '-9999px'
                /* eslint-enable */
              });
            }
          } else if ($active.length !== 0) {
            $list.removeClass('hover');
            $active.eq(0).addClass('hover');
          }
          if (key === 8) {
            // Delete key
            return false;
          }

          var h = 0;
          if ($activeSelect.find('.selection-list li.hover').length === 0) {
            $.each(
              $activeSelect.find('.selection-list li:visible').splice(0, $activeSelect.find('.selection-list li.selected:visible').index() - 1),
              function () {
                h += $(this).outerHeight();
              }
            );
          } else {
            $.each($activeSelect.find('.selection-list li:visible').splice(0, $activeSelect.find('.selection-list li.hover:visible').index() - 1), function () {
              h += $(this).outerHeight();
            });
          }

          $activeSelect.find('.selection-list').scrollTop(h);
        }
        return true;
      });
  }
};
