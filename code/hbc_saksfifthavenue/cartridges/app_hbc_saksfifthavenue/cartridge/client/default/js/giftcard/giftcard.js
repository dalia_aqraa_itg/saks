'use strict';

/**
 * Generates the modal window on the first call.
 *
 */
function getModalHtmlElement() {
  if ($('#giftcard-modal').length !== 0) {
    $('#giftcard-modal').remove();
  }
  if ($('#consent-tracking').length !== 0) {
    $('#consent-tracking').remove();
  }
  var htmlString =
    '<!-- Modal -->' +
    '<div class="modal fade giftcard-modal-sec" id="giftcard-modal" role="dialog" aria-modal="true">' +
    '<span class="giftcard-message sr-only" ></span>' +
    '<div class="modal-dialog giftcard-dialog modal-dialog-centered">' +
    '<!-- Modal content-->' +
    '<div class="modal-content">' +
    '<div class="modal-body"></div>' +
    '</div>' +
    '</div>' +
    '</div>';
  $('body').append(htmlString);
}

/**
 * wherearegc model html .
 *
 **/
function getGCCModalHtmlElement() {
  if ($('#wherearegcmodal').length !== 0) {
    $('#wherearegcmodal').remove();
  }
  var htmlString =
    '<!-- Modal -->' +
    '<div class="modal fade wherearegcmodal" id="wherearegcmodal" role="dialog" aria-modal="true">' +
    '<div class="modal-dialog wherearegcmodal-dialog">' +
    '<!-- Modal content-->' +
    '<div class="modal-content">' +
    '<div class="modal-body">' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';
  $('body').append(htmlString);
}
/**
 * Validation check and actions on the form elements
 *
 * @param {Object} elem - DOM element on which validation should be performed
 */
function validationOfElements(elem) {
  var val = elem.val();
  elem.parent().toggleClass('error', !val).toggleClass('success', !!val);
  elem.parent().find('.error-img').toggleClass('d-none', !!val);
  elem.parent().find('.success-img').toggleClass('d-none', !val);
  elem.parent().find('.missing-error-required').toggleClass('d-none', !!val);
}
/**
 * validate GC data on blur
 *
 */
function validateOnBlur(elem) {
  if (!elem) {
    $('.giftcard-dialog-body .giftcard-form input:visible').each(function () {
      validationOfElements($(this));
    });
  } else {
    validationOfElements(elem);
  }
}

/**
 * Gift card error notifications
 * @param {Object} message - Error message to display
 */
function createErrorNotification(message) {
  var errorHtml = '<div class="alert alert-danger" role="alert">' + message + '</div>';
  $('.giftcard-error').html(errorHtml);
}

/**
 * fill modal gc form
 *
 * @param {string} urlHit - url to be hit
 */
function fillModalElement(urlHit) {
  $.spinner().start();
  $.ajax({
    url: urlHit,
    method: 'GET',
    dataType: 'json',
    success: function (data) {
      var template = data.renderedTemplate;
      $('#giftcard-modal .modal-body').empty();
      $('#giftcard-modal .modal-body').html(template);
      $('#giftcard-modal').modal('show');

      // reCaptcha.callToken('giftcardpage');

      $('.svg-svg-22-cross-dims').on('click', function () {
        $('.modal-backdrop').remove();
        $('#giftcard-modal').modal('hide');
      });

      $('body .giftcard-dialog')
        .off('blur')
        .on(' blur', '.giftcard-form input', function () {
          validateOnBlur($(this));
        });

      $('.gift-card-check-submit button').on('click', function (e) {
        e.preventDefault();
        $.spinner().start();
        // eslint-disable-next-line no-undef
        grecaptcha.ready(function () {
          // eslint-disable-next-line no-undef
          grecaptcha
            .execute($('.google-recaptcha-key').html(), {
              action: 'gcbalancecheck'
            })
            .then(function (token) {
              $('.giftcard-form .g-recaptcha-token').val(token);
              validateOnBlur();
              var gcNumb = $('.gc-numb').val();
              var gcPin = $('.gc-pin').val();
              if (!gcNumb || !gcPin) {
                $('.generic-error').addClass('d-none');
                $.spinner().stop();
                return;
              }
              $.ajax({
                url: $('.gift-card-check-submit button').data('action'),
                method: 'POST',
                data: {
                  token: $('.g-recaptcha-token').val(),
                  apiKey: $('.g-recaptcha-token').data('secret'),
                  gcNumber: gcNumb,
                  gcPin: gcPin,
                  checkbalance: true
                },
                success: function (responseData) {
                  if (responseData.success) {
                    var htmlData = responseData.renderedTemplate;
                    $('#giftcard-modal .modal-body').empty();
                    $('#giftcard-modal .modal-body').html(htmlData);
                    $('.svg-svg-22-cross-dims').on('click', function () {
                      $('#giftcard-modal').modal('hide');
                      $('body').removeClass('modal-open');
                      $('.modal-backdrop').remove();
                      $('#giftcard-modal').remove();
                    });
                    $('.generic-error').addClass('d-none');
                    $.spinner().stop();
                    $('.giftcard-continue button').on('click', function (event) {
                      event.preventDefault();
                      $('#giftcard-modal').modal('hide');
                      $('body').removeClass('modal-open');
                      $('.modal-backdrop').remove();
                      $('#giftcard-modal').remove();
                    });
                    $('.giftcard-check-balance button').on('click', function (eve) {
                      eve.preventDefault();
                      var url = $(this).data('action');
                      fillModalElement(url);
                    });
                  } else if (typeof responseData.serviceError !== 'undefined' && responseData.serviceError) {
                    $('.generic-error').addClass('d-none');
                    createErrorNotification(responseData.message);
                    // reCaptcha.callToken('giftcardpage');
                    $.spinner().stop();
                  } else {
                    $('.giftcard-error').empty();
                    $('.gc-numb').parent().find('.success-img').addClass('d-none');
                    $('.gc-pin').parent().find('.success-img').addClass('d-none');
                    $('.generic-error').html(responseData.message);
                    $('.generic-error').removeClass('d-none');
                    $('.gc-numb').parent().addClass('error');
                    $('.gc-numb').parent().find('.error-img').removeClass('d-none');
                    $('.gc-pin').parent().addClass('error');
                    $('.gc-pin').parent().find('.error-img').removeClass('d-none');
                    // reCaptcha.callToken('giftcardpage');
                    $.spinner().stop();
                  }

                  if (responseData.botError) {
                    $('.check-bal-bttn').attr('disabled', 'disabled');
                    createErrorNotification(responseData.error);
                  }
                },
                error: function (responseData) {
                  $('.gc-numb').parent().find('.success-img').addClass('d-none');
                  $('.gc-pin').parent().find('.success-img').addClass('d-none');
                  // $('.generic-error').html(responseData.message);
                  $('.generic-error').removeClass('d-none');
                  $('.gc-numb').parent().addClass('error');
                  $('.gc-numb').parent().find('.error-img').removeClass('d-none');
                  $('.gc-pin').parent().addClass('error');
                  $('.gc-pin').parent().find('.error-img').removeClass('d-none');
                  // reCaptcha.callToken('giftcardpage');
                  $.spinner().stop();

                  if (responseData.botError) {
                    $('.check-bal-bttn').attr('disabled', 'disabled');
                    createErrorNotification(responseData.error);
                  }
                }
              });
            });
        });
      });
      $.spinner().stop();
    },
    error: function () {
      $.spinner().stop();
    }
  });
}

/**
 * wherearegc model actions.
 *
 **/
function fillGCCModalElement() {
  $.spinner().start();
  $.ajax({
    url: $('a.wherearegc').attr('href'),
    method: 'GET',
    success: function (data) {
      var template = data.renderedTemplate;
      $('.wherearegcmodal-dialog .modal-body').html('<button class="close gc-close-btn svg-svg-22-cross svg-svg-22-cross-dims" type="button"/>' + template);
      $('#wherearegcmodal').modal('show');

      $('.svg-svg-22-cross-dims').on('click', function () {
        $('#wherearegcmodal').attr('aria-modal', 'false');
        $('#wherearegcmodal').next('.modal-backdrop').remove();
        $('#wherearegcmodal').modal('hide');
      });

      $.spinner().stop();
    },
    error: function () {
      $('#wherearegcmodal').attr('aria-modal', 'false');
      $('#wherearegcmodal').next('.modal-backdrop').remove();
      $('#wherearegcmodal').modal('hide');
      $.spinner().stop();
    }
  });
}

module.exports = {
  giftcardPopup: function () {
    $('body').on('click', '.check-balance', function (e) {
      e.preventDefault();
      var urlHit = $(this).attr('href');
      getModalHtmlElement();
      fillModalElement(urlHit);
    });
  },
  wherearegc: function () {
    $('body').on('click', '.wherearegc', function (e) {
      e.preventDefault();
      getGCCModalHtmlElement();
      fillGCCModalElement();
    });
  }
};
