'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
  var persistentWishlist = require('core/product/persistentWishlist');
  persistentWishlist.markProductArrWishlisted();
  processInclude(require('./search/search'));
  processInclude(require('./product/wishlistHeart'));
  processInclude(require('./product/wishlist')); // this is for the quickview heart functionality
  processInclude(require('core/product/quickView'));
  processInclude(require('./components/refinementBar'));
});
