'use strict';

var base = require('core/components/utilhelper');

base.eventListener = function (eventName, eventHandler) {
  if (window.addEventListener) {
    window.addEventListener(eventName, eventHandler, false);
  } else if (window.attachEvent) {
    //IE 8 and older
    if (eventName.indexOf('bfx') > -1) {
      //custom bfx events
      window.attachEvent('onpropertychange', function (event) {
        if (event.propertyName == eventName) {
          eventHandler(eventHandler);
        }
      });
    } else {
      //standard js events
      window.attachEvent(eventName, eventHandler);
    }
  }
};

module.exports = base;
