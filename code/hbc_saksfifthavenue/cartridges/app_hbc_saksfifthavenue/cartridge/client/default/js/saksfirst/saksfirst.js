'use strict';

var formValidation = require('base/components/formValidation');
var cleave = require('core/components/cleave');
var tokenEx = require('tokenex/tokenEx');
var isTsysMode = require('core/util/isTsys');

function serializeData(form, saksCardNumber) {
  var serializedArray = form.serializeArray();

  serializedArray.forEach(function (item) {
    if (item.name.indexOf('cardNumber') > -1) {
      var isTokenEnabled = $('input.tokenEanbled').val();
      if (isTokenEnabled !== undefined && isTokenEnabled === 'true') {
        var cardNumber = $(saksCardNumber).data('cleave').getRawValue();
        var publicKey = $('input.tokenExPubKey').val();

        if (cardNumber && publicKey) {
          var token = tokenEx.encryptCC(publicKey, cardNumber);
          item.value = token; // eslint-disable-line
        } else {
          item.value = $(saksCardNumber).data('cleave').getRawValue(); // eslint-disable-line
        }
      } else {
        item.value = $(saksCardNumber).data('cleave').getRawValue(); // eslint-disable-line
      }
    }
  });

  return $.param(serializedArray);
}

module.exports = {
  submitSaksFirst: function () {
    $('form.saksfirst-form').submit(function (e) {
      var $form = $(this);
      e.preventDefault();
      var saksCardNumber = $form.find('#saksAccountNumber');
      var url = $form.attr('action');
      if (!$('.saks-general-error').hasClass('d-none')) {
        $('.saks-general-error').addClass('d-none');
      }
      $('.saks-general-error p').empty();
      $form.spinner().start();
      var formData = serializeData($form, saksCardNumber);

      $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        data: formData,
        success: function (data) {
          $form.spinner().stop();
          if (!data.success) {
            if (data.saksError) {
              $('.saks-general-error p').text(data.saksError);
              $('.saks-general-error').removeClass('d-none');
            } else if (data.error) {
              var validationMessage = data.error;
              $(saksCardNumber).parents('.form-group').find('.invalid-feedback').text(validationMessage);
              $(saksCardNumber).addClass('is-invalid');
              if ($(saksCardNumber).next('span').length === 0) {
                $('<span></span>').insertAfter($(saksCardNumber));
                $(saksCardNumber).next('span').addClass('invalid');
              }
              if ($(saksCardNumber).next('span').hasClass('valid')) {
                $(saksCardNumber).next('span').removeClass('valid').addClass('invalid');
              }
            } else if (data.redirectUrl) {
              location.href = data.redirectUrl;
            } else {
              formValidation($form, data);
            }
          } else {
            location.href = data.redirectUrl;
          }
        },
        error: function (err) {
          if (err.responseJSON.redirectUrl) {
            window.location.href = err.responseJSON.redirectUrl;
          }
          $form.spinner().stop();
        }
      });
      return false;
    });
  },
  submitSaksFirstGCRedeem: function () {
    $('form.saksfirst-redeem-form').submit(function (e) {
      var $form = $(this);
      e.preventDefault();
      var url = $form.attr('action');
      if (!$('.saks-general-error').hasClass('d-none')) {
        $('.saks-general-error').addClass('d-none');
      }
      $('.saks-general-error p').empty();
      $form.spinner().start();
      var formData = $form.serialize();
      var gcRedeemMode = $('.js-gf-redemption-mode:checked').val();
      if (gcRedeemMode) {
        formData = formData + '&gcRedeemMode=' + gcRedeemMode;
      }

      $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        data: formData,
        success: function (data) {
          $form.spinner().stop();
          if (!data.success) {
            if (data.saksError) {
              $('.saks-general-error p').text(data.saksError);
              $('.saks-general-error').removeClass('d-none');
            } else if (data.redirectUrl) {
              location.href = data.redirectUrl;
            } else {
              formValidation($form, data);
            }
          } else {
            $('.saksfirstGiftcard-redemption-success').removeClass('d-none');
            $('.saks-tier-details').addClass('d-none');
            $('.saks-gift-card-redemption').addClass('d-none');
            if (gcRedeemMode && gcRedeemMode == 'EMAIL') {
              $('.saks-gf-email-redeem').removeClass('d-none');
              if (!$('.saks-gf-mail-redeem').hasClass('d-none')) {
                $('.saks-gf-mail-redeem').addClass('d-none');
              }
            } else {
              $('.saks-gf-mail-redeem').removeClass('d-none');
              if (!$('.saks-gf-email-redeem').hasClass('d-none')) {
                $('.saks-gf-email-redeem').addClass('d-none');
              }
            }
          }
        },
        error: function (err) {
          if (err.responseJSON.redirectUrl) {
            window.location.href = err.responseJSON.redirectUrl;
          }
          $form.spinner().stop();
        }
      });
      return false;
    });
  },
  submitSaksFirstBBRedeem: function () {
    $('form.saksfirst-beauty-redeem-form').submit(function (e) {
      var $form = $(this);
      e.preventDefault();
      var url = $form.attr('action');
      if (!$('.saks-general-error').hasClass('d-none')) {
        $('.saks-general-error').addClass('d-none');
      }
      $('.saks-general-error p').empty();
      $form.spinner().start();
      var formData = $form.serialize();
      var bbRedeemMode = $('.js-bb-redemption-mode:checked').val();
      if (bbRedeemMode) {
        formData = formData + '&bbRedeemMode=' + bbRedeemMode;
      }

      $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        data: formData,
        success: function (data) {
          $form.spinner().stop();
          if (!data.success) {
            if (data.saksError) {
              $('.saks-general-error p').text(data.saksError);
              $('.saks-general-error').removeClass('d-none');
            } else if (data.error) {
              var validationMessage = data.error;
              $(saksCardNumber).parents('.form-group').find('.invalid-feedback').text(validationMessage);
              $(saksCardNumber).addClass('is-invalid');
              if ($(saksCardNumber).next('span').length === 0) {
                $('<span></span>').insertAfter($(saksCardNumber));
                $(saksCardNumber).next('span').addClass('invalid');
              }
              if ($(saksCardNumber).next('span').hasClass('valid')) {
                $(saksCardNumber).next('span').removeClass('valid').addClass('invalid');
              }
            } else if (data.redirectUrl) {
              location.href = data.redirectUrl;
            } else {
              formValidation($form, data);
            }
          } else {
            $('.saksfirstbeautybox-redemption-success').removeClass('d-none');
            $('.saks-tier-details').addClass('d-none');
            $('.saksfirstbeautybox-redemption').addClass('d-none');
            if (bbRedeemMode && bbRedeemMode == 'PICKUP') {
              $('.saks-bb-email-redeem').removeClass('d-none');
              if (!$('.saks-bb-mail-redeem').hasClass('d-none')) {
                $('.saks-bb-mail-redeem').addClass('d-none');
              }
            } else {
              $('.saks-bb-mail-redeem').removeClass('d-none');
              if (!$('.saks-bb-email-redeem').hasClass('d-none')) {
                $('.saks-bb-email-redeem').addClass('d-none');
              }
            }
          }
        },
        error: function (err) {
          if (err.responseJSON.redirectUrl) {
            window.location.href = err.responseJSON.redirectUrl;
          }
          $form.spinner().stop();
        }
      });
      return false;
    });
  },
  handleSaksFirstCardNumber: function () {
    if ($('#saksAccountNumber').length && $('#cardType').length) {
      cleave.handleSaksFirstAccountNumber('#saksAccountNumber', '#cardType');
    }
  },
  handleSaksGCFirstCardNumber: function () {
    if ($('#saksGCAccountNumber').length && $('#cardGCType').length) {
      cleave.handleSaksFirstAccountNumber('#saksGCAccountNumber', '#cardGCType');
    }
  },
  handleSaksBBFirstCardNumber: function () {
    if ($('#saksBBAccountNumber').length && $('#cardBBType').length) {
      cleave.handleSaksFirstAccountNumber('#saksBBAccountNumber', '#cardBBType');
    }
  },
  handleRedeemButton: function () {
    $('button.gf-redeem-btn').on('click', function (e) {
      e.preventDefault();
      $('.saks-tier-details').addClass('d-none');
      $('.saksfirstGiftcard-redemption').removeClass('d-none');
    });
  },
  handleRedeemCancel: function () {
    $('a.js-gf-cancel').on('click', function (e) {
      e.preventDefault();
      if (!$('.saks-general-error').hasClass('d-none')) {
        $('.saks-general-error').addClass('d-none');
      }
      $('.saks-general-error p').empty();
      $('.saks-tier-details').removeClass('d-none');
      $('.saksfirstGiftcard-redemption').addClass('d-none');
      $('.js-gf-redemption-mode[value="EMAIL"]').prop('checked', true).trigger('change');
    });
  },
  handleBBRedeemButton: function () {
    $('button.bb-redeem-btn').on('click', function (e) {
      e.preventDefault();
      $('.saks-tier-details').addClass('d-none');
      $('.saksfirstbeautybox-redemption').removeClass('d-none');
    });
  },
  handleBBRedeemCancel: function () {
    $('a.js-bb-cancel').on('click', function (e) {
      e.preventDefault();
      if (!$('.saks-general-error').hasClass('d-none')) {
        $('.saks-general-error').addClass('d-none');
      }
      $('.saks-general-error p').empty();
      $('.saks-tier-details').removeClass('d-none');
      $('.saksfirstbeautybox-redemption').addClass('d-none');
      $('.js-bb-redemption-mode[value="PICKUP"]').prop('checked', true).trigger('change');
    });
  },
  handleRedeemOption: function () {
    $('.js-gf-redemption-mode').on('change', function () {
      if ($('.js-gf-redemption-mode:checked').val() === 'EMAIL') {
        $('.redemption-email-msg').removeClass('d-none');
        $('.redemption-mail-msg').addClass('d-none');
      } else {
        $('.redemption-mail-msg').removeClass('d-none');
        $('.redemption-email-msg').addClass('d-none');
      }
    });
  },
  handleBeautyBoxRedeemOption: function () {
    $('.js-bb-redemption-mode').on('change', function () {
      if ($('.js-bb-redemption-mode:checked').val() === 'SHIP') {
        $('.redemption-ship-msg').removeClass('d-none');
        $('.ship-submit').removeClass('d-none');
        $('.pickup-submit').addClass('d-none');
        $('.redemption-pickup-msg').addClass('d-none');
      } else {
        $('.redemption-pickup-msg').removeClass('d-none');
        $('.redemption-ship-msg').addClass('d-none');
        $('.ship-submit').addClass('d-none');
        $('.pickup-submit').removeClass('d-none');
      }
    });
  },
  handleCloseDynamicRewardMessage: function () {
    $('span.close').on('click', function () {
      $(this).parent().addClass('d-none');
    });
  },
  applySaksFirstCreditCard: function () {
    $('.saks-become-first-member-message a').click(function (event) {
      if (isTsysMode()) {
        event.preventDefault();
        var url = $(this).data('url');
        $('body').spinner().start();
        $.ajax({
          url: url,
          dataType: 'json',
          success: function (data) {
            $('body').spinner().stop();
            if (data.redirectUrl) {
              window.location.href = data.redirectUrl;
            }
          },
          error: function (data) {
            $('body').spinner().stop();
          }
        });
      }
    });
  }
};
