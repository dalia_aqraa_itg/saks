'use strict';

var base = require('core/product/detail');
var baseBase = require('./base');
var formFields = require('../formFields/formFields');

base.addToCart = baseBase.addToCart;

$(document).ready(function () {
  var url = $('.saks-first-card .tooltip-content').data('url');
  if (url) {
    $.ajax({
      url: url,
      type: 'get',
      success: function (data) {
        $('.saks-first-card .custom-tooltip .tooltip-content').empty().append(data);
      }
    });
  }

  if ($('.page').data('producttype') === 'variant') {
    var productSDDVerbiageURL = $('#productSDDVerbiage').data('url');
    if (productSDDVerbiageURL && productSDDVerbiageURL !== '') {
      $.ajax({
        url: productSDDVerbiageURL,
        type: 'get',
        dataType: 'html',
        data: {},
        success: function (response) {
          if (response) {
            $('#productSDDVerbiage').html(response);
          }
        }
      });
    }
  }

  var $changeZipPdpWrapper = $('.change-zip-pdp-wrapper');
  var changeZipPdpWrapperURL = $changeZipPdpWrapper.data('url');

  if (changeZipPdpWrapperURL) {
    $.ajax({
      url: changeZipPdpWrapperURL,
      type: 'get',
      success: function (data) {
        if (data) {
          $changeZipPdpWrapper.append(data);
        }
      }
    });
  }
});

// To override complete hiding of recommendation on the chanel from core detail.js
base.showChanelRecommendations = function () {};
base.updateAddToCart = function () {
  $('body').on('product:updateAddToCart', function (e, response) {
    // update local add to cart (for sets)
    var $buttonAddToCart = $('button.add-to-cart');
    if (response.$productContainer && response.$productContainer.hasClass('set-item')) {
      $buttonAddToCart = response.$productContainer.find('button.add-to-cart');
    }
    $buttonAddToCart.data('readytoorder', response.product.readyToOrder && response.product.orderableNotInPurchaselimit);
    $buttonAddToCart.data('readytoordertext', response.product.readyToOrderMsg);
    if (
      response.product.waitlistable &&
      response.product.availability.ats <= 0 &&
      response.product.productType !== 'master' &&
      (!response.product.readyToOrder || !response.product.available)
    ) {
      if ($('.page').data('producttype') === 'set') {
        var $productContainer = response.$productContainer;
        $productContainer.find('.js-add-to-cart ').find('.hideInWaitList').addClass('d-none');
        $productContainer.find('.js-product-availability-qty').addClass('d-none');
        $productContainer.find('.js-wait-list-form').removeClass('d-none');
        $productContainer.find('.js-wait-list-form').find('.waitlist-product-id').val(response.product.id);
        $productContainer.find('.wait-list-success').empty();
      } else {
        //$('.js-add-to-cart').addClass('d-none');
        $('.js-add-to-cart ').find('.hideInWaitList').addClass('d-none');
        $('.js-product-availability-qty').addClass('d-none');
        $('.js-wait-list-form').removeClass('d-none');
        $('.js-wait-list-form').find('.waitlist-product-id').val(response.product.id);
        $('.wait-list-success').empty();
      }
      formFields.adjustForAutofill();
    } else {
      $('.js-add-to-cart ').find('.hideInWaitList').removeClass('d-none');
      $('.js-waitlist-wrapper').addClass('d-none');
      $('.js-wait-list-form').addClass('d-none');
      $('.js-product-availability-qty').removeClass('d-none');
      $('.wait-list-success').empty();
    }
    /* var enable = $('.product-availability').toArray().every(function (item) {
            return $(item).data('available') && $(item).data('ready-to-order');
        }); */
    if ($('.page').data('producttype') === 'set') {
      $('button.add-to-cart', response.$productContainer).text(response.product.availability.buttonName);
      $('button.add-to-cart', response.$productContainer).attr('disabled', !response.product.available && response.product.availability.isInPurchaselimit);
      $('button.add-to-cart-global', response.$productContainer).text(response.product.availability.buttonName);
      $('div [id^="collapsible-details-"] .product-detail-id', response.$productContainer).html(response.product.longDescriptionStyle);
    } else {
      // change button text only if pick up is not selected;
      if (
        !(
          $('.shipping-option').length > 0 &&
          $('.shipping-option').find('input:checked').length > 0 &&
          $('.shipping-option').find('input:checked').val().length > 0 &&
          $('.shipping-option').find('input:checked').val() === 'instore'
        )
      ) {
        if (response.product.preOrder && response.product.preOrder.applicable && response.product.preOrder.applicable === true) {
          $buttonAddToCart.text(response.product.preOrder.preorderButtonName);
          if (response.product.preOrder.shipDate) {
            $('div .preorder-ship-date').html("<span class='preorder-text'>" + response.product.preOrder.shipDate + '</span>');
          }
          $('.shipping-option').addClass('d-none');
        } else {
          $buttonAddToCart.text(response.product.availability.buttonName);
          $('div .preorder-ship-date').empty();
          $('.shipping-option').removeClass('d-none');
        }
      }
      $buttonAddToCart.attr('disabled', !response.product.available && response.product.availability.isInPurchaselimit);
      if (response.product.preOrder && response.product.preOrder.applicable && response.product.preOrder.applicable === true) {
        $('button.add-to-cart-global').text(response.product.preOrder.preorderButtonName);
        if (response.product.preOrder.shipDate) {
          $('div .preorder-ship-date').html("<span class='preorder-text'>" + response.product.preOrder.shipDate + '</span>');
        }
        $('.shipping-option').addClass('d-none');
      } else {
        $('button.add-to-cart-global').text(response.product.availability.buttonName);
        $('div .preorder-ship-date').empty();
        $('.shipping-option').removeClass('d-none');
        var isWaitListVisible = $('form.waitlistForm:visible').length > 0;
        if (isWaitListVisible) {
          $('.shipping-option').addClass('d-none');
        }
      }
      $('div [id^="collapsible-details-"] .product-detail-id').html(response.product.longDescriptionStyle);
    }
  });
};
base.submitWaitList = function () {
  $('body').on('submit', 'form.waitlistForm', function (e) {
    var form = $(this);
    e.preventDefault();
    var url = form.attr('action');
    form.spinner().start();
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: form.serialize(),
      success: function (data) {
        var $productContainer = $(form).closest('.container.product-detail');
        if (data.success) {
          $productContainer
            .find('.wait-list-success')
            .removeClass('d-none')
            .empty()
            .html('<div class="alert-success"><div class="success-msg"><span class="message">' + data.msg + '</span></div></div>');
          $productContainer.find('.js-wait-list-form').addClass('d-none');
          $productContainer.find('.waitlistForm')[0].reset();
          $productContainer.find('.waitlistForm').find('.input-focus').removeClass('input-focus');
          $productContainer.find('.waitlistForm').find('input').next('span').remove();
          $('body').trigger('adobe:waitListComplete');
        } else {
          $productContainer
            .find('.wait-list-success')
            .empty()
            .text("<div class='alert-success'>" + data.msg + '</div>');
        }
        form.spinner().stop();
      },
      error: function (data) {
        $productContainer
          .find('.wait-list-success')
          .removeClass('d-none')
          .empty()
          .text("<div class='alert-success'>" + data.msg + '</div>');
        form.spinner().stop();
      }
    });
    return false;
  });
};

base.buttonToggle = function () {};

$(document).on('click', '.product-details-page .custom-color-dropdown .selected-option', function () {
  if (window.innerWidth < 544) {
    //stop body from scrolling when opening the color list
    $('body').addClass('select-opened');
    var pageTop = `-${window.scrollY}px`;
    document.body.style.position = 'fixed';
    document.body.style.top = pageTop;
  }
});

$(document).on('click', '.product-details-page .custom-color-dropdown .selection-list', function () {
  if (window.innerWidth < 544) {
    $('body').removeClass('select-opened');
    const scrollY = document.body.style.top;
    document.body.style.position = '';
    document.body.style.top = '';
    window.scrollTo(0, parseInt(scrollY || '0') * -1);
  }
});
base.closeColorDropDown = function () {
  $(document).on('click', '.custom-color-dropdown .selection-list', function () {
    if (window.innerWidth < 544) {
      $(this).closest('.current_item').removeClass('current_item');
    }
  });
};

base.displayWaitListOptMsg = function () {
  $('body').on('focus keyup', '.js-waitlist-mobile, .js-waitlist-email', function () {
    if ($('.js-waitlist-mobile').val() !== '' || $('.js-waitlist-email').val() !== '') {
      $('.js-mobile-opt-msg').removeClass('d-none');
      if ($('.js-waitlist-mobile').val() !== '') {
        $('.js-waitlist-mobile').attr('required', true);
        $('.js-waitlist-mobile').attr('pattern', $(this).attr('data-pattern'));
      } else {
        $('.js-waitlist-mobile').removeAttr('required');
        $('.js-waitlist-mobile').removeAttr('pattern');
      }
    } else {
      $('.js-mobile-opt-msg').addClass('d-none');
      if ($('.js-waitlist-mobile').val() == '') {
        $('.js-waitlist-mobile').removeAttr('required');
        $('.js-waitlist-mobile').removeAttr('pattern');
      }
    }
  });
  $('body').on('blur', '.js-waitlist-mobile', function () {
    if ($(this).val() == '') {
      $('.js-waitlist-mobile').removeAttr('required');
      $('.js-waitlist-mobile').removeAttr('pattern');
      $('.js-waitlist-mobile').removeClass('is-invalid').next('span').remove();
      $('.js-waitlist-mobile').closest('.form-group').find('.invalid-feedback').empty();
      $('.js-waitlist-mobile').closest('.form-group').find('label').removeClass('is-invalid').removeClass('input-focus');
    }
  });
};

base.showSpinner = function () {
  $('body').on('product:beforeAddToCart product:beforeAttributeSelect', function (evt) {
    if (evt.target.classList.contains('modal-open')) {
      return;
    }
    $.spinner().start();
  });
};

function isDesktop() {
  return window.innerWidth >= 1025;
}

base.reviewsMobilePlacement = function () {
  if (!isDesktop()) {
    if ($('.pdp-right-section').find('.product-number-rating').length) {
      var review = $('.product-number-rating').clone();
      $('.product-detail').find('.mobile-product-brand-name').append(review);
    }
  }
};

/**
 * PDP Size chart model actions.
 **/
base.sizeChartModel = function () {
  $('.size_guide').on('click', 'button', function (e) {
    e.preventDefault();
    var masterID = $(this).closest('.size_guide').attr('data-masterid');
    var $sizeModel = $('.size-modal');
    if (masterID && masterID != '' && $("[data-sizeguidemasterid='" + masterID + "']").length > 0) {
      $sizeModel = $("[data-sizeguidemasterid='" + masterID + "']");
    }
    $sizeModel.modal('show');
  });
};

/**
 * PDP Size chart model actions.
 **/
base.sizeChartModel = function () {
  $('.size_guide').on('click', 'button', function (e) {
    e.preventDefault();
    var masterID = $(this).closest('.size_guide').attr('data-masterid');
    var $sizeModel = $('.size-modal');
    if (masterID && masterID != '' && $("[data-sizeguidemasterid='" + masterID + "']").length > 0) {
      $sizeModel = $("[data-sizeguidemasterid='" + masterID + "']");
    }
    $sizeModel.modal('show');
  });
};

/**
 * PDP zoom window slick
 *
 * @param {params} $primaryImgZoomWindow - Primary zoom window
 **/
var zoomWindowSlider = function ($primaryImgZoomWindow) {
  if ($primaryImgZoomWindow.is('.slick-initialized')) {
    $primaryImgZoomWindow.slick('unslick');
  }

  $primaryImgZoomWindow.slick({
    slidesToShow: 100,
    dots: false,
    arrows: true,
    centerMode: false,
    infinite: true,
    focusOnSelect: true,
    speed: 300,
    variableWidth: false,
    swipe: true,
    swipeToSlide: true,
    vertical: true,
    draggable: true
  });
};

/**
 * PDP scene 7 image zoom window
 *
 * @param {params} params - Params for scene 7
 * @param {product} pid - Product id
 **/
var enableScene7Zoom = function (params, pid) {
  if ($('.enablePDPZoomer').length > 0) {
    var prodID = pid || $('div.s7-viewer').attr('data-productID');
    var zoomViewer = new s7viewers.FlyoutViewer({
      // eslint-disable-line
      containerId: 's7viewer-' + prodID, // eslint-disable-line
      params: params // eslint-disable-line
    }).init();
  }
};

/**
 * PDP activate zoom window, slider on zoom window, call scene 7 zoom functionality
 *
 * @param {element} $element - Slick element
 * @param {product} pid - Product id
 * @param {slide} currentSlide - Current slide
 **/
var activateZoomSlick = function ($element, pid, currentSlide) {
  var $primaryImgZoomWindow = $element.find('.zoom-thumbnails');

  zoomWindowSlider($primaryImgZoomWindow);

  setTimeout(function () {
    $primaryImgZoomWindow.slick('slickGoTo', currentSlide);
  }, 500);

  $primaryImgZoomWindow.on('afterChange', function () {
    $(this).find('.thumb-nail').removeClass('active');
    $(this).find('.slick-current .thumb-nail').addClass('active');
    var serverUrl = $element.find('div.pdp-carousel').data('scene7hosturl');
    var asset;
    var image;
    var params = {};
    image = $(this).find('.slick-current .thumb-nail.active').find('img').attr('src').split('image/'); // eslint-disable-line
    asset = image[image.length - 1];
    params.asset = asset;
    params.serverurl = serverUrl;
    $('.s7-viewer').empty();
    enableScene7Zoom(params, pid);
  });
};

/**
 * PDP activate primary selected image zoom
 *
 * @param {this} $this - current element
 **/
var activatePrimaryImageZoom = function ($this) {
  var $element = $this.closest('.product-detail');
  var pid = $element.find('div.s7-viewer').attr('data-productID');
  $('.s7-viewer').empty();
  $('.s7-modal.' + pid).modal('show');
  setTimeout(function () {
    var currentSlide = $this.parent().data('index') || 0;
    activateZoomSlick($element, pid, currentSlide);
  }, 500);
};

var imgActive = false;

var pdpImageActions = function () {
  var enterKeyPressed = function enterKeyPressed(e) {
    var code = e.charCode || e.keyCode;
    var ENTER_KEY = 13;
    return code === ENTER_KEY;
  };

  $('.product-detail').on('click keypress', '.primary-image img', function (e) {
    if (e.type === 'keypress') {
      if (enterKeyPressed(e)) {
        $(this).trigger('click');
      }
      return;
    }
    activatePrimaryImageZoom($(this));
  });

  $('.zoom-thumbnails').on('keypress', '.thumb-nail', function (e) {
    if (e.type === 'keypress') {
      if (enterKeyPressed(e)) {
        $(this).trigger('click');
      }
      return;
    }
  });

  $('.primary-thumbnails').on('click keypress', '.thumb-nail', function (e) {
    if (e.type === 'keypress') {
      if (enterKeyPressed(e)) {
        $(this).trigger('click');
      }
      return;
    }

    imgActive = true;
    if ($('.primary-images').find('.slick-current').offset()) {
      $('html, body').animate(
        {
          scrollTop: $('.primary-images').find('.slick-current').offset().top - 200
        },
        500
      );
    }
    var pid = $(this).closest('.product-detail').attr('data-pid');
    if ($(this).is('.video-player')) {
      baseBase.playVideoPlayer(pid);
    }
    setTimeout(function () {
      imgActive = false;
    }, 500);
    $('body').trigger('adobeTagManager:altImageView');
  });

  $('.zoom-thumbnails').on('click keypress', '.thumb-nail', function (e) {
    $('body').trigger('adobeTagManager:altImageView');
  });

  $(window).on('resize load', function () {
    $('.primary-images').off('afterChange');
    if ($(window).width() < 1024) {
      $('.primary-images').on('afterChange', function (event, slick, currentSlide, nextSlide) {
        $('body').trigger('adobeTagManager:altImageView');
      });
    }
  });
};

base.updateAttribute = function () {
  $('body').on('product:afterAttributeSelect', function (e, response) {
    if ($('.product-detail>.bundle-items').length) {
      response.container.data('pid', response.data.product.id);
      response.container.find('.product-id').text(response.data.product.id);
      response.container.find('.bf-product-id').text(response.data.product.id);
    } else if ($('.product-set-detail').eq(0)) {
      response.container.data('pid', response.data.product.id);
      response.container.find('.product-id').text(response.data.product.id);
      response.container.find('.bf-product-id').empty().text(response.data.product.id);
    } else {
      $('.product-id').text(response.data.product.id);
      $('.product-detail:not(".bundle-item")').data('pid', response.data.product.id);
      response.container.find('.bf-product-id').empty().text(response.data.product.id);
    }
    TurnToCmd('set', {
      sku: response.data.product.masterProductID || response.data.product.id
    }); //eslint-disable-line
  });
};

base.availability = baseBase.availability;

base.AfterWindowLoadStart = function () {
    if ($('.product-detail .attributes .size-attribute').length > 0) {
      $('.product-detail .attributes .size-attribute').css('pointer-events', 'all');
    }
};

base.pdpImageActions = pdpImageActions;
module.exports = base;
