'use strict';

$(document).ready(function() {
    var url = $('.home-page-content-slots').data('url');
    if (url) {
      $.ajax({
        url: url,
        method: 'GET',
        success: data => {
          $('.home-page-content-slots').append(data);
        }
      });
    }
  });