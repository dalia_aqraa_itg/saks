'use strict';

$('form.email-signup').submit(function (e) {
  var form = $(this);
  e.preventDefault();
  var url = form.attr('action');
  var $this = $('.invalid-email-erro');
  $('.invalid-email-erro').next('span').empty();
  form.spinner().start();
  $.ajax({
    url: url,
    type: 'post',
    data: form.serialize(),
    success: function (data) {
      form.spinner().stop();
      if (data.success === 'false') {
        $('<span class="error">' + data.msg + '</span>').insertAfter($this);
        $('.invalid-email-erro').addClass('error');
      } else {
        $('.email-signup-form').addClass('d-none');
        $('.email-signup-form-success').removeClass('d-none');
      }
    },
    error: function (err) {
      $('.invalid-email-erro').append('<span class="error">' + data.msg + '</span>');
      $('.invalid-email-erro').addClass('error');
      form.spinner().stop();
    }
  });
  return false;
});
