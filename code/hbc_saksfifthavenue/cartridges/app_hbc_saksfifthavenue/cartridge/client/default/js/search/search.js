'use strict';

var base = require('core/search/search');
// to invoke store change modal in search
var pdpInstoreInventory = require('../product/pdpInstoreInventory');
var persistentWishlist = require('core/product/persistentWishlist');
var productGridTeasers = require('core/teasersproductgrid');
var hbcTooltip = require('core/tooltip');
var login = require('../login/login');
var formField = require('core/formFields/formFields');
var lazyLoad = require('core/util/lazyLoadImages');

base.changeStore = pdpInstoreInventory.changeStore; // change store modal
var formField = require('../formFields/formFields');

function commonSortWidthHandler() {
  var text = $('.sort-selection-block .select-sort-order').find('option:selected').text();
  var $aux = $('<select/>').append($('<option/>').text(text));
  $('.sort-selection-block .select-sort-order').after($aux);
  $('.sort-selection-block .select-sort-order').width($aux.width() + 10);
  $aux.remove();
}

base.modelViewToggle = function () {
  $('body').on('category:set:model:view', function (e, data) {
    var modelView = $('body').find('.model-view');
    if (modelView.length > 0) {
      var toggle = modelView.find('.selected').text();
      if (toggle && toggle.toLowerCase().indexOf('on') > -1) {
        $('body').trigger('category:modelView:toggle:on', data);
      } else {
        $('body').trigger('category:modelView:toggle:off', data);
      }
    }
  });

  $('body').on('category:modelView:toggle:on', function (e, data) {
    var mvTiles = $('.js-product-model-img');
    if (data && data.productContainer) {
      var productContainer = data.productContainer;
      mvTiles = $(productContainer).find('.js-product-model-img');
    }

    var oldIndex;
    if (data && data.oldImgs && data.oldImgs.length > 0) {
      oldIndex = data.oldImgs.length;
    }
    var modelView = $('body').find('.model-view');
    if (mvTiles && mvTiles.length > 0 && modelView.length > 0) {
      mvTiles.toArray().forEach(function (mvTile) {
        mvTile = $(mvTile); // eslint-disable-line
        if (!mvTile.hasClass('image-swapped')) {
          var hiresmodalImage = null;
          mvTile
            .find('img.tile-image')
            .toArray()
            .forEach(function (img, index) {
              img = $(img); // eslint-disable-line
              var additionalImages = img.data('additional-images');
              if (additionalImages && additionalImages.length > 0) {
                for (var i = 0; i <= additionalImages.length - 1; i++) {
                  if (additionalImages[i]['data-hiresmodel'] != null) {
                    //Loop through object from data-additional-images and assign attributes to newly created image.
                    const el = document.createElement('img');
                    Object.keys(additionalImages[i]).forEach(function (att) {
                      el.setAttribute(att, additionalImages[i][att]);
                    });

                    //Change src, hiresmodel, and srcset data with original image urls
                    additionalImages[i]['src'] = img.attr('src');
                    additionalImages[i]['data-hiresmodel'] = img.attr('src');
                    additionalImages[i]['srcset'] = img.attr('srcset');

                    el.setAttribute('data-additional-images', JSON.stringify(additionalImages));
                    //Add model view image we just created before original image and then remove the original image.
                    mvTile.find('img.tile-image').eq(0).before(el).remove();
                    mvTile.addClass('image-swapped');

                    break;
                  }
                }
              } else if (oldIndex && oldIndex > 0) {
                if (index == oldIndex + 1) {
                  hiresmodalImage = img.clone();
                  img.remove();
                }
              } else {
                if (index == 1) {
                  hiresmodalImage = img.clone();
                  img.remove();
                }
              }
            });

          if (hiresmodalImage) {
            mvTile.find('img.tile-image').eq(0).before(hiresmodalImage);
            mvTile.addClass('image-swapped');
          }
        }
      });
    }
  });

  $('body').on('category:modelView:toggle:off', function (e, data) {
    var mvTiles = $('.js-product-model-img');
    if (data && data.productContainer) {
      var productContainer = data.productContainer;
      mvTiles = $(productContainer).find('.js-product-model-img');
    }
    var oldIndex;
    if (data && data.oldImgs && data.oldImgs.length > 0) {
      oldIndex = data.oldImgs.length;
    }

    if (mvTiles && mvTiles.length > 0) {
      mvTiles.toArray().forEach(function (mvTile) {
        // remove any already appended model view image
        mvTile = $(mvTile); // eslint-disable-line
        var hiresmodalImage = null;
        if (mvTile.hasClass('image-swapped')) {
          mvTile
            .find('img.tile-image')
            .toArray()
            .forEach(function (img, index) {
              img = $(img); // eslint-disable-line
              var additionalImages = img.data('additional-images');
              if (additionalImages && additionalImages.length > 0) {
                for (var i = 0; i <= additionalImages.length - 1; i++) {
                  console.log('OFF number: ' + i);
                  console.log('is data-hiresmodel null: ' + (additionalImages[i]['data-hiresmodel'] == null));
                  if (additionalImages[i]['data-hiresmodel'] != null) {
                    //Loop through object from data-additional-images and assign attributes to newly created image.
                    const el = document.createElement('img');
                    Object.keys(additionalImages[i]).forEach(function (att) {
                      el.setAttribute(att, additionalImages[i][att]);
                    });

                    //Change src, hiresmodel, and srcset data with original image urls
                    additionalImages[i]['src'] = img.attr('src');
                    additionalImages[i]['data-hiresmodel'] = img.attr('src');
                    additionalImages[i]['srcset'] = img.attr('srcset');

                    el.setAttribute('data-additional-images', JSON.stringify(additionalImages));
                    //Add model view image we just created before original image and then remove the original image.
                    mvTile.find('img.tile-image').eq(0).before(el).remove();
                    mvTile.removeClass('image-swapped');

                    break;
                  }
                }
              } else if (oldIndex && oldIndex > 0) {
                if (index == oldIndex) {
                  hiresmodalImage = img.clone();
                  img.remove();
                }
              } else {
                if (index == 0) {
                  hiresmodalImage = img.clone();
                  img.remove();
                }
              }
            });

          if (hiresmodalImage) {
            mvTile.find('img.tile-image').eq(0).after(hiresmodalImage);
            mvTile.removeClass('image-swapped');
          }
        }
      });
    }
  });

  $('body').on('click', '.model-view-on', function () {
    $('body').find('.model-view-off').removeClass('selected');
    $(this).addClass('selected');
    $('body').trigger('category:modelView:toggle:on');
    $('body').trigger('adobeTagManager:toggleModelView', $(this));
  });

  $('body').on('click', '.model-view-off', function () {
    $('body').find('.model-view-on').removeClass('selected');
    $(this).addClass('selected');
    $('body').trigger('category:modelView:toggle:off');
    $('body').trigger('adobeTagManager:toggleModelView', $(this));
  });
};

/**
 * Is the window large-ish?
 * @returns {Boolean} true if large, otherwise false.
 */
function isDesktop() {
  return window.innerWidth >= 1024;
}
base.cloneCategoryRefinement = function () {
  $('body').on('category:category:refinement:clone', function () {
    if (!isDesktop() && !$('.category-refinement-bar').hasClass('slick-initialized')) {
      var refinement = $('#refinement-category');
      if (refinement.length === 0) {
        refinement = $('#refinement-by-category');
      }
      var hasAppendedfirstLi = true;
      var isSearchLanding = $('body').hasClass('search-landing-page');
      if (refinement && refinement.length > 0 && refinement.find('ul').length > 0) {
        refinement.find('.all-results-link').remove();
        $('.category-refinment-mobile').empty().html('<ul class="category-refinement-bar responsive-slider hbc-slider"></ul>');
        var $this = isSearchLanding ? refinement.find('ul li').first().parent() : refinement.find('ul li').last().parent();
        var result = $($this).find('li').clone();
        result.find('ul').remove();
        result.find('.all-results-link').remove();
        if (result.length > 0 && result !== undefined) {
          $('.category-refinement-bar').html(result);
        }
        var firstSelected = refinement.find('.selected-category-search');
        var firstSelectedHtml;
        if (firstSelected && firstSelected.length > 0) {
          refinement.find('.selected-category-search').eq(0).find('ul').remove();
          firstSelectedHtml = refinement.find('.selected-category-search').eq(0).clone();
        }
        var allLabel = 'All';
        var firstLiURL =
          $('div[data-parent-url]').length > 0 && $('div[data-parent-url]').data('parent-url')
            ? $('div[data-parent-url]').data('parent-url')
            : window.location.href;
        if (isSearchLanding) {
          allLabel = 'All Results';
          firstLiURL = $('body').data('search-reseturl');
        }
        if (hasAppendedfirstLi && $('.category-refinement-bar li').length > 0) {
          if (isSearchLanding && firstSelected && firstSelected.length > 0) {
            $('.category-refinement-bar li')
              .eq($('div[data-cat-position]').length > 0 ? $('div[data-cat-position]').data('data-cat-position') : 0)
              .before(firstSelectedHtml);
          }
          $('.category-refinement-bar li')
            .eq(0)
            .before(
              '<li class="catgories-values adobelaunch__subnav  show-in-mens   show-in-womens  " aria-label="All"><a  href="' +
                firstLiURL +
                '"data-type="category" class="text-left "> <span title="Refine by By Category: All" class="refinement-category " aria-hidden="true">' +
                allLabel +
                '</span><span class="sr-only selected-assistive-text">Refine by By Category: All</span></a>'
            );

          hasAppendedfirstLi = false;
        }

        // Remove duplicate category
        try {
          if (firstSelectedHtml && firstSelectedHtml.length > 0) {
            var count = 0;
            if ($(firstSelectedHtml).eq(0).find('span').length > 0) {
              var selectedFirstItem = $(firstSelectedHtml).eq(0).find('span').html().trim().toLowerCase();

              $('.category-refinement-bar .refinement-category').each(function (index) {
                if ($(this).html().trim().toLowerCase() == selectedFirstItem) {
                  if (count == 1) {
                    $(this).closest('li').remove();
                  }
                  count = 1;
                }
              });
            }
          }
        } catch (e) {}

        window.hbcSlider.hbcSliderInit('category-refinement-bar');

        //Activate the current category
        var selected = false;
        if ($('.search-results-header').length > 0) {
          $('.category-refinement-bar .refinement-category').each(function (index) {
            if ($(this).closest('.text-left').hasClass('disabled')) {
              selected = true;
            }
            if (
              $(this).html().trim().toLowerCase() === $('.search-results-header').html().trim().toLowerCase() ||
              (isSearchLanding && $(this).html().trim().toLowerCase() === $('.last-item').text().trim().toLowerCase())
            ) {
              if (!selected) {
                $('.category-refinement-bar .refinement-category').closest('.text-left').removeClass('disabled');
                $(this).closest('.text-left').addClass('disabled');
                selected = true;
              }
            }
          });
        }
        if (!selected) {
          if (!$($('.category-refinement-bar .refinement-category')[0]).closest('.text-left').hasClass('disabled')) {
            $($('.category-refinement-bar .refinement-category')[0]).closest('.text-left').addClass('disabled');
          }
        }
      } // refinement condition
    } // desktop condition

    if ($(window).width() >= 1024) {
      if ($('.category-refinement-bar').hasClass('slick-initialized')) {
        $('.category-refinement-bar').slick('unslick');
      }
    }
  });
  $('body').trigger('category:category:refinement:clone');
  $(window).on('resize orientationchange', function () {
    $('body').trigger('category:category:refinement:clone');
  });
};

base.catRefineBarActiveHandler = function () {
  $('body').on('click', '.category-refinement-bar .catgories-values a', function () {
    if (!isDesktop()) {
      $('.category-refinement-bar .catgories-values a').removeClass('disabled');
      $(this).addClass('disabled');
    }
  });
};

base.handleFilterShowOnScroll = function () {
  $(document)
    .on('page:scrollUp', function () {
      if ($('.search-results .product-search-results').length > 0) {
        $('.search-results .product-search-results').addClass('no-top');
      }
    })
    .on('page:scrollDown', function () {
      if ($('.search-results .product-search-results').length > 0) {
        $('.search-results .product-search-results').removeClass('no-top');
      }
    });
};

base.filter = function () {
  // Display refinements bar when Menu icon clicked
  $('.container').on('click', 'button.filter-results', function () {
    $('.refinement-bar').addClass('d-block');
    $('.refinement-bar').siblings().attr('aria-hidden', true);
    $('.refinement-bar').closest('.row').siblings().attr('aria-hidden', true);
    $('.refinement-bar').closest('.tab-pane.active').siblings().attr('aria-hidden', true);
    $('.refinement-bar').closest('.container.search-results').siblings().attr('aria-hidden', true);
    $('.refinement-bar .close').focus();
    $('body').addClass('filters-visible modal-open');
    $('.refinement-bar').find(':focusable').first().focus();
    if ($(window).width() < 1023 && $('.refinement-wrapper .refinements').height() > $('.refinement-wrapper').height()) {
      $('.collapsible-xl.refinement .card-body').height($('.refinement-wrapper .refinements').height());
    }

    if ($(window).width() <= 1023) {
      if (!$('.refinement-bar-mob').find('.refinements .refinement:visible').hasClass('active')) {
        $('.refinement-bar-mob').find('.refinements .refinement:eq(1)').addClass('active');
      }
    }

    formField.findInsiders($('.refinement-bar'));
  });
};

/**
 * Routes the handling of attribute processing depending on whether the attribute has image
 *     swatches or not
 *
 * @param {Object} attrs - Attribute
 * @param {string} attr.id - Attribute ID
 * @param {jQuery} $productContainer - DOM element for a given product
 */
function updateAttrs(attrs, $productContainer, product) {
  if (product.productTileSwatch) {
    var $tileSizeSection = $(product.productTileSwatch).find('.tile-size-container');
    if ($tileSizeSection.length > 0) {
      $productContainer.find('.swatch-container .tile-size-container').empty().append($tileSizeSection.html());
    }
  }

  attrs.forEach(function (attr) {
    attr.values.forEach(function (attrValue) {
      var $attrValue = $productContainer.find('a[data-attr-value="' + attrValue.value + '"]');
      if (attrValue.valueUrl) {
        $attrValue.data('valueurl', attrValue.valueUrl);
      } else if (attrValue.url) {
        $attrValue.data('valueurl', attrValue.url);
      }
      if (attrValue.selectable || (!attrValue.selectable && attrValue.waitlist)) {
        $attrValue.removeClass('hide-this-size-swatch');
      } else {
        $attrValue.addClass('hide-this-size-swatch');
      }
      $attrValue.removeClass('selectable swatch-not-available size-not-available');
      var notAvailableClass = $attrValue.hasClass('colorswatch') ? 'swatch-not-available' : 'size-not-available';
      $attrValue.addClass(attrValue.selectable ? '' : notAvailableClass);
    });
  });
}

/**
 * updates the product view when a product attribute is selected or deselected or when
 *         changing quantity
 * @param {string} selectedValueUrl - the Url for the selected variation value
 * @param {jQuery} $productContainer - DOM element for current product
 * @param {Array} oldImgs - Old images array
 */
function attributeSelect(selectedValueUrl, $productContainer, oldImgs, callback) {
  if (selectedValueUrl) {
    selectedValueUrl = selectedValueUrl;
    $productContainer.spinner().start();
    $.ajax({
      url: selectedValueUrl,
      method: 'GET',
      success: function (data) {
        $productContainer.spinner().stop();
        updateAttrs(data.product.variationAttributes, $productContainer, data.product);
        persistentWishlist.makrSingleProductWishlisted(data.product.id, $productContainer);
        $productContainer.closest('.product').data('pid', data.product.id);
        $productContainer.closest('.product').attr('data-pid', data.product.id);
        $productContainer.find('.bf-product-id').empty().text(data.product.id);
        $productContainer.closest('.product').data('producttype', data.product.productType);
        $productContainer
          .closest('.product')
          .find('.prdt_tile_btn')
          .data('readytoorder', data.product.readyToOrder && data.product.available);
        if (data.product.readyToOrder && data.product.available) {
          $productContainer.closest('.product').find('.prdt_tile_btn').data('view', 'selected');
        }

        // update the price
        /* var $priceSelector = $('.price', $productContainer);
              var $priceRange = $('.range', $productContainer);
              if (data.product.price.type != null && data.product.price.type == 'range') {
                  $priceRange.empty().html(data.product.price.html);
              } else {
                  $priceSelector.empty().html(data.product.price.html);
              }*/
        var $priceSelector = $('.prod-price', $productContainer);
        $priceSelector.empty().html(data.product.price.html);
        $priceSelector.find('.prod-price').addClass('prod-price-remove-space');

        // Update promotions
        if (data.product.promotionalPricing && data.product.promotionalPricing.isPromotionalPrice && data.product.promotionalPricing.promoMessage !== '') {
          $('.promotion-pricing', $productContainer).empty().html(data.product.promotionalPricing.priceHtml);
          $('.promotion-pricing', $productContainer).removeClass('d-none');
          $('.promotions', $productContainer).addClass('d-none');
        } else {
          $('.promotion-pricing', $productContainer).addClass('d-none');
          $('.promotions', $productContainer).removeClass('d-none');
          $('.promotions', $productContainer).empty().html(data.product.promotionsHtml);
        }

        // Update limited inventory message
        if (data.product.availability.isAboveThresholdLevel) {
          $('.limited-inventory', $productContainer).empty().text(data.resources.limitedInventory);
        } else {
          $('.limited-inventory', $productContainer).empty();
        }

        if ((!data.product.available || !data.product.readyToOrder) && data.product.waitlistable && data.product.productType !== 'master') {
          $productContainer.find('button.prdt_tile_btn').addClass('d-none');
          $productContainer.find('a.prdt_tile_btn').removeClass('d-none');
          $productContainer.find('a.prdt_tile_btn').attr('href', data.product.pdpURL + '#waitlistenabled');
        } else if ((!data.product.available || !data.product.readyToOrder) && data.product.productType !== 'master') {
          $productContainer.find('button.prdt_tile_btn').addClass('soldout');
          $productContainer.find('button.prdt_tile_btn').removeClass('add-to-cart');
          $productContainer.find('button.prdt_tile_btn').removeClass('d-none');
          $productContainer.find('button.prdt_tile_btn').text(data.resources.soldout);
          $productContainer.find('button.prdt_tile_btn').attr('disabled', true);
          $productContainer.find('a.prdt_tile_btn').addClass('d-none');
        } else {
          $productContainer.find('button.prdt_tile_btn').addClass('add-to-cart');
          $productContainer.find('button.prdt_tile_btn').removeClass('soldout');
          $productContainer.find('button.prdt_tile_btn').removeClass('d-none');
          $productContainer.find('a.prdt_tile_btn').addClass('d-none');
          $productContainer.find('button.prdt_tile_btn').removeAttr('disabled');
          if (data.product.preOrder && data.product.preOrder.applicable && data.product.preOrder.applicable === true) {
            $productContainer.find('button.prdt_tile_btn').text(data.resources.preordertocart);
          } else if ($('body').hasClass('cart-page')) {
            $productContainer.find('button.prdt_tile_btn').text(data.resources.movetobag);
          } else {
            $productContainer.find('button.prdt_tile_btn').text(data.resources.addtocart);
          }
        }
        var initialIndex = 0;
        if ($productContainer.find('.firstSelectableIndex').length > 0) {
          var initialIndex = $productContainer.find('.firstSelectableIndex').eq(0).attr('data-firstSelectableIndex');
        }
        if (!$productContainer.find('.size').hasClass('slick-initialized')) {
          /* Filter the unselectable slides*/

          $productContainer.find('.size').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: true,
            autoplay: false
          });

          if ($productContainer.find('.size').eq(0).find('.slick-slide').length - 1 == Number(initialIndex)) {
            $productContainer.find('.size').slick('slickGoTo', Number(initialIndex) - 1, true);
          } else {
            $productContainer.find('.size').slick('slickGoTo', Number(initialIndex), true);
          }
        }
        base.plpSwatchesEvents();
        // append model images if it is enabled
        $('body').trigger('category:set:model:view', {
          productContainer: $productContainer,
          oldImgs: oldImgs
        });

        if (typeof oldImgs !== 'undefined') {
          for (let oi in oldImgs) {
            //eslint-disable-line
            oldImgs[oi].remove();
          }
        }

        if (callback) {
          callback();
        }
      },
      error: function () {
        if (callback) {
          callback();
        }
        $productContainer.spinner().stop();
      }
    });
  }
}

base.plpSwatchesEvents = function () {
  $('body').on('click', '.sizeSwatch', function (e) {
    e.preventDefault();
    if ($(this).hasClass('selected')) {
      return;
    }
    var attrValue = $(this).attr('data-attr-value');
    var $productContainer = $(this).closest('.product-tile');
    var slickCounter = $(this).attr('data-slick-counter');
    var $selectedSize = $(this);
    var $prevSelectedSize = $selectedSize.closest('.size').find('.sizeSwatch.selected');
    $prevSelectedSize.removeClass('selected');
    $selectedSize.addClass('selected');
    $(this).closest('.product-tile').find('.prdt_tile_btn').data('view', 'selected');
    attributeSelect($(this).data('valueurl'), $(this).closest('.product-tile'), null, function () {
      var $selectedSizeSwatch = $productContainer.find('a[data-attr-value="' + attrValue + '"]');
      if ($selectedSizeSwatch && $selectedSizeSwatch.length > 0) {
        slickCounter = $selectedSizeSwatch.attr('data-slick-counter');
        $selectedSizeSwatch.addClass('selected');
        $productContainer.find('.size').slick('slickGoTo', Number(slickCounter) - 1, true);
        $productContainer.find('.prdt_tile_btn').data('view', 'selected');
      }
    });
  });

  $('body').on('click', '.color-swatches .swatches .colorswatch', function (e) {
    e.preventDefault();
    if ($(this).hasClass('selected')) {
      return;
    }

    var $selectedSwatch = $(this);
    var $tile = $selectedSwatch.closest('.product-tile');
    $tile.find('.color-swatches .swatches .colorswatch.selected').each(function () {
      $(this).removeClass('selected');
    });
    $selectedSwatch.addClass('selected');
    var color = $selectedSwatch.data('attr-value');
    $tile.find(".color-swatches .swatches .colorswatch[data-attr-value='" + color + "']").addClass('selected');
    $tile.find('.image-container').removeClass('prevent-slick');
    $tile.find('.thumb-link').attr('href', $selectedSwatch.attr('href'));
    $tile.find('.link').attr('href', $selectedSwatch.attr('href'));
    var data = $(this).find('.swatch-circle').data('imgsource');
    var oldImgs = [];
    $tile.find('.image-container .thumb-link img').each(function () {
      oldImgs.push($(this));
    });

    if ($selectedSwatch.data('qvurl')) {
      $tile.find('.image-container .quickview').attr('href', $selectedSwatch.data('qvurl'));
    }


    var url = $(this).data('valueurl');
    if ($tile.find('.tile-size-container').find('.sizeSwatch.tile-swatch.selected:visible').length > 0) {
      var $sizeSelected = $tile.find('.tile-size-container').find('.sizeSwatch.tile-swatch.selected:visible');
      var tilePid = $tile.closest('.bfx-disable-product').data('tile-pid');
      url = url.concat('&').concat('dwvar_').concat(tilePid).concat('_').concat($sizeSelected.data('attr')).concat('=').concat($sizeSelected.data('attr-value'));
    }

    // TODO refactor this or remove entirely. Currently causing a loop
    attributeSelect(url, $(this).closest('.product-tile'), oldImgs);

    return new Promise((resolve) => {
      for (var i = 0; i < data.length; i++) {
        var $thumblink = $tile.find('.image-container .thumb-link');
        var setImg = data[i];
        var imgEle = $('<img loading="lazy" class="tile-image" itemprop="image"/>');

        if (i === 0) {
          // only check load for first thumb (initial view)
          imgEle[0].addEventListener('load', () => resolve());
          imgEle[0].addEventListener('error', () => resolve());
          // fallback resolve to not hang tile spinner
          setTimeout(() => resolve(), 3000);
        }

        imgEle.attr('src', '' + setImg.url + '') // ADD IMAGE PROPERTIES.
          .attr('title', setImg.title)
          .attr('alt', setImg.alt)
          .appendTo($thumblink); // ADD THE IMAGE TO DIV
      }
    })
      // remove spinner only once first image is finished loading
      .then($tile.spinner().stop);

    // $thumb.data('current', currentAttrs);
  });

  if (window.FABuild) {
    $('body').on('touchstart', '.thumb-link', function () {
      const $this = $(this);

      if ($this.closest('.image-container').hasClass('prevent-slick')) {
        return false;
      }

      const $firstImage = $this.find('.tile-image:first');
      const firstImageEl = $firstImage.get(0);
      const additionalImageData = getAdditionalImageData($firstImage);

      if (additionalImageData instanceof Array) {
        const restOfImagesArray = additionalImageData.slice(1);
        const additionalImageHTML = getElementImagesHTML(restOfImagesArray);
        firstImageEl.insertAdjacentHTML('afterend', additionalImageHTML);
      }

      firstImageEl.removeAttribute('data-additional-images');
    });
  }
  /* eslint-disable */
  if ($(window).width() > 1023.99) {
    $('body')
      .on('mouseenter', '.thumb-link', function () {
        let initialIndex = 0;
        const $this = $(this);
        if ($this.closest('.product-tile').find('.firstSelectableIndex').length > 0) {
          initialIndex = $this.closest('.product-tile').find('.firstSelectableIndex').eq(0).attr('data-firstSelectableIndex');
        }
        if (!$this.closest('.product-tile').find('.size').hasClass('slick-initialized')) {
          /* Filter the unselectable slides*/

          $this.closest('.product-tile').find('.size').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: true,
            autoplay: false
          });

          if ($this.closest('.product-tile').find('.size').eq(0).find('.slick-slide').length - 1 == Number(initialIndex)) {
            $this
              .closest('.product-tile')
              .find('.size')
              .slick('slickGoTo', Number(initialIndex) - 1, true);
          } else {
            $this.closest('.product-tile').find('.size').slick('slickGoTo', Number(initialIndex), true);
          }
        }
        if ($this.closest('.image-container').hasClass('prevent-slick')) {
          return false;
        }
        // Render the additional images if necessary.
        const $firstImage = $this.find('.tile-image:first');
        const firstImageEl = $firstImage.get(0);
        const additionalImageData = $firstImage.data('additional-images');
        if (additionalImageData instanceof Array) {
          const additionalImageHTML = additionalImageData.reduce((acc, image) => {
            const el = document.createElement('img');
            Object.keys(image).forEach(att => el.setAttribute(att, image[att]));
            return acc + el.outerHTML;
          }, '');
          firstImageEl.removeAttribute('data-additional-images');
          firstImageEl.insertAdjacentHTML('afterend', additionalImageHTML);
        }

        // Initialize the slick carousel on images.
        if ($this.hasClass('slick-initialized')) {
          $this.slick('unslick');
        }
        $this.find('img').each(function () {
          $this.attr('src', $this.attr('data-src'));
        });
        if (!/msie\s|trident\/|Edge\//i.test(navigator.userAgent)) {
          $this.slick({
            dots: false,
            infinite: true,
            speed: 600,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 500
          });
        }
        if ($this.closest('.image-container').find('.tile-image').hasClass('guest-user')) {
          $this.slick('unslick');
        }
      })
      .on('mouseleave', '.thumb-link', function () {
        if ($(this).hasClass('slick-initialized')) {
          $(this).slick('unslick');
        }
      });
    /* eslint-enable */

    $('body').on('mouseenter', '.product-tile', function () {
      var initialIndex = 0;
      if ($(this).find('.firstSelectableIndex').length > 0) {
        var initialIndex = $(this).find('.firstSelectableIndex').eq(0).attr('data-firstSelectableIndex');
      }
      if (!$(this).find('.size').hasClass('slick-initialized')) {
        $(this).find('.size').slick({
          dots: false,
          infinite: false,
          speed: 300,
          slidesToShow: 2,
          slidesToScroll: 1,
          arrows: true,
          autoplay: false
        });

        if ($(this).find('.size').eq(0).find('.slick-slide').length - 1 == Number(initialIndex)) {
          $(this)
            .find('.size')
            .slick('slickGoTo', Number(initialIndex) - 1, true);
        } else {
          $(this).find('.size').slick('slickGoTo', Number(initialIndex), true);
        }
      }
    });
  }
};

var thumbWidth = function () {
  if ($(window).width() > 1023.99) {
    $('.search-page .product-tile .thumb-link').each(function () {
      $(this).attr('style', '');
      $(this).css('width', Math.floor($(this).outerWidth()));
    });
  }
};

/**
 * Update DOM elements with Ajax results
 *
 * @param {Object} $results - jQuery DOM element
 * @param {string} selector - DOM element to look up in the $results
 * @return {undefined}
 */
function updateDom($results, selector) {
  var $updates = $results.find(selector);
  $(selector).empty().html($updates.html());
  // add reviews on update
  productGridTeasers.addReviews();
}

/**
 * Keep refinement panes expanded/collapsed after Ajax refresh
 *
 * @param {Object} $results - jQuery DOM element
 * @return {undefined}
 */
function handleRefinements($results) {
  $('.refinement.active').each(function () {
    $(this).removeClass('active');
    var activeDiv = $results.find('.' + $(this)[0].className.replace('/', '').replace('?', '').replace('&', '').replace(/ /g, '.'));
    activeDiv.addClass('active');
    activeDiv.find('button.title').attr('aria-expanded', 'true');
  });

  updateDom($results, '.refinements');
}

/**
 * Parse Ajax results and updated select DOM elements
 *
 * @param {string} response - Ajax response HTML code
 * @param {event} event - function event
 * @param {string} type - Type of filter event
 * @returns {boolean}- return state if there were any search results
 */
function parseResults(response, event, type) {
  var $results = $(response);
  if ($results.find('.search-results-total-count').html() === '0') {
    if (
      event &&
      event.currentTarget &&
      event.currentTarget &&
      $(event.currentTarget).closest('input[id="bopisCheck"]').length > 0 &&
      $('.no-products-store').length &&
      $('.no-products-store').hasClass('d-none')
    ) {
      $('.no-products-store').removeClass('d-none');
      $('input[id="bopisCheck"]').attr('disabled', true);
    }
    // for saks only
    if (
      event &&
      event.currentTarget &&
      event.currentTarget &&
      $(event.currentTarget).closest('input[id="bopisCheck"]').length > 0 &&
      $(event.currentTarget).closest('input[id="bopisCheck"]').hasClass('saks-only') &&
      $(event.currentTarget).closest('input[id="bopisCheck"]').attr('data-nostores-msg') !== undefined
    ) {
      $('input[id="bopisCheck"]').attr('disabled', 'disabled');
      $('input[id="bopisCheck"]').siblings('label').find('span').html($(event.currentTarget).closest('input[id="bopisCheck"]').attr('data-nostores-msg'));
    }
    // change label of SDD if no product are not
    if (event && event.currentTarget && event.currentTarget && $(event.currentTarget).closest('input[id="sddCheck"]').length > 0) {
      $('input[id="sddCheck"]').attr('disabled', true);
      $('input[id="sddCheck"]').prop('checked', true);
      if (
        $results.find('.search-results').attr('data-noresults-message') !== '' &&
        $results.find('.search-results').attr('data-noresults-message') !== 'null' &&
        $('input[id="sddCheck"]').siblings('label[for=sddCheck]').length > 0
      ) {
        $('input[id="sddCheck"]').siblings('label[for=sddCheck]').html($results.find('.search-results').attr('data-noresults-message'));
      }
    }
    return false;
  }
  var searchSource = $(document).find('.store-refine').attr('data-search-source');
  $('.refinement-price .no-results').addClass('d-none');
  var specialHandlers = {
    '.refinements': handleRefinements
  };
  var domElements = [
    '.grid-header',
    '.header-bar',
    '.header.page-title',
    '.product-grid',
    '.show-more',
    '.filter-bar',
    '.search-count',
    '.sort-results-container',
    '.filter-results-container',
    '.sort-selection-block',
    '.shop-item',
    '.search-results-count',
    '.store-bar',
    '.product-search-results .breadcrumb',
    '.product-search-results .search-results-header',
    '.filter-header',
    '.applied-filters-bar .filter-header',
    '.my-designers-bar',
    '.my-sizes-bar',
    '#refinement-category',
  ];

  if (type !== 'applied') {
    domElements.push('.applied-filters-bar .applied-filters-wrapper');
  }
  // for store refinement, the .filter-bar is not neccessary to be refreshed
  if (
    event &&
    event.currentTarget &&
    event.currentTarget &&
    $(event.currentTarget).closest('input[id="bopisCheck"]').length > 0 &&
    !$(event.currentTarget).closest('input[id="bopisCheck"]').hasClass('saks-only')
  ) {
    domElements.splice(5, 1);
  }
  // Update DOM elements that do not require special handling
  domElements.forEach(function (selector) {
    updateDom($results, selector);
  });
  $('body').trigger('category:category:refinement:clone');

  $('body').trigger('category:showhide:refinement');

  // dup header elements
  var dupHeaders = $results.find('.product-search-results .search-results-header');
  Object.keys(dupHeaders).forEach(function (elm) {
    var headerElement = dupHeaders.eq(elm);
    if (headerElement.text().trim() !== '') {
      $('.product-search-results .search-results-header').empty().html(headerElement.html());
    }
  });

  if ($('.product-search-results').find('.search-results-header').length > 1) {
    $('.product-search-results').find('.search-results-header').eq(1).remove();
  }

  // append model images if it is enabled
  $('body').trigger('category:set:model:view');

  // toggle pick up check box
  if (
    event &&
    event.currentTarget &&
    event.currentTarget &&
    $(event.currentTarget).closest('input[id="sddCheck"]').length === 0 &&
    $results.find('.search-results').data('isrefinedby-store')
  ) {
    $('input[id="bopisCheck"]').attr('checked', true);
  } else {
    $('input[id="bopisCheck"]').removeAttr('checked');
    // don not diable the button and message when the search is from store select
    if (
      event &&
      event.currentTarget &&
      event.currentTarget &&
      $(event.currentTarget).closest('input[id="bopisCheck"]').length > 0 &&
      $('.no-products-store').length &&
      $('.no-products-store').hasClass('d-none') &&
      searchSource === 'search'
    ) {
      $('.no-products-store').removeClass('d-none');
      $('input[id="bopisCheck"]').attr('disabled', true);
    } else {
      $(document).find('.store-refine').attr('data-search-source', 'search');
    }
  }
  // update url if the trigger is form the store select from the store change modal
  if ($results.siblings('.search-results').data('storecheck-url') && $(event.currentTarget).closest('input[id="bopisCheck"]').length === 0) {
    $('input[id="bopisCheck"]').attr('data-href', $results.siblings('.search-results').data('storecheck-url'));
  }
  Object.keys(specialHandlers).forEach(function (selector) {
    specialHandlers[selector]($results);
  });
  // update result count
  if (event && event.currentTarget && event.currentTarget && $(event.currentTarget).closest('input[id="sddCheck"]').length > 0) {
    $('input[id="sddCheck"]').prop('checked', true);
    $('input[id="sddCheck"]').removeAttr('disabled');
    if (
      $results.find('.search-results').attr('data-results-message') !== '' &&
      $results.find('.search-results').attr('data-results-message') !== 'null' &&
      $('input[id="sddCheck"]').siblings('label[for=sddCheck]').length > 0
    ) {
      $('input[id="sddCheck"]').siblings('label[for=sddCheck]').html($results.find('.search-results').attr('data-results-message'));
    }
  }
  formField.updateSelect();
  if ($(window).width() >= 1024) {
    if (event !== undefined && !$(event.currentTarget).hasClass('bopis-refinement')) {
      $(window).scrollTop($('.search-result-wrapper').offset().top - $('.navigation-section .main-menu').outerHeight() - 18);
    }
  } else {
    $(window).scrollTop($('.product-tile-section').offset().top - document.querySelector('header').offsetHeight);
  }
  return true;
}

function prepActiveRefinementTrigger() {
  var activeRefinement = document.querySelector('.refinement.card.active');
  var trigger;
  if (!activeRefinement) {
    return;
  }
  trigger = activeRefinement.querySelector('.card-header button[aria-controls]');
  trigger.setAttribute('aria-expanded', 'true');
}
var viewmoreshow = function () {
  if ($(window).width() > 1023.99) {
    $('.catgories-values').each(function () {
      if ($(this).hasClass('category-hide')) {
        $(this).addClass('d-none');
      }
    });
  } else {
    $('.catgories-values').each(function () {
      if ($(this).hasClass('category-hide')) {
        $(this).removeClass('d-none');
      }
    });
  }
};

viewmoreshow();

/**
 * Update the links for applied filter in case of selection/deselection
 *
 */
function updateAppliedRefinement() {
  $('.applied-filters-wrapper .selected-filter button').each(function () {
    let refinementType = $(this).data('refinement-type');
    let refinementValue = $(this).data('refinement-value');
    let refinementEl = $('.refinements')
      .find('.' + refinementType)
      .find('[data-refinement-value="' + refinementValue + '"]');
    let href = refinementEl.attr('href') || refinementEl.data('href');
    $(this).attr('data-href', href);
  });
}
function updateGrid(url, type, e, $this) {
  const refinementBar = document.querySelector('.refinement-bar');
  const refinementWrapperScrollTop = refinementBar ? refinementBar.scrollTop : 0;

  if ($(window).width() <= 1024 && $('.refinement-bar-mob:visible').length) {
    var scrollvalue = $this.closest('div.card-body ul.values').scrollTop();
  }

  //PrefCenter My Designer and My Sizes Event Block
  var MyDesignerSelected = false;
  var MySizesSelected = false;
  var haveBothFiltersSelected = false;
  var haveBrandSelected = false;
  var haveSizeSelected = false;

  if (sessionStorage.MyDesignerSelected === 'true' && sessionStorage.MySizesSelected === 'true') {
    // iF both My Designer and My Sizes selected together
    haveBothFiltersSelected = true;
  }
  if (sessionStorage.MyDesignerSelected === 'true') {
    haveBrandSelected = true;
  }
  if (sessionStorage.MySizesSelected === 'true') {
    haveSizeSelected = true;
  }
  var cookieUtil = require('core/components/utilhelper');

  var elemId = $this.attr('id');
  //mobile block
  if (elemId == 'MyDesigners-mobile-url' || elemId == 'MySizes-mobile-url') {
    if ($this.hasClass('selected')) {
      // When My Designer or My Sizes were clicked and applied - Remove the check mark and clear the search
      $this.removeClass('selected');
      if (haveBothFiltersSelected) {
        // IF Both were Selected and now Removing one
        if (elemId == 'MyDesigners-mobile-url') {
          // If UnSelcting My Designer - Get the MySizesURl from Cookie ad make My DesignerSelected = False - So checkmark wil also be removed from ISML
          url = cookieUtil.getCookie('mySizessUrlNotCombined');
          MyDesignerSelected = false;
          MySizesSelected = true;
        } else {
          url = cookieUtil.getCookie('myDesignersUrlNotCombined');
          MyDesignerSelected = true;
          MySizesSelected = false;
        }
      } else {
        url = $('li.clear-refinement').find('button').attr('data-href');
      }
    } else {
      $this.addClass('selected');

      if ((haveBrandSelected && elemId === 'MySizes-mobile-url') || (haveSizeSelected && elemId === 'MyDesigners-mobile-url')) {
        MyDesignerSelected = true;
        MySizesSelected = true;
      } else if (elemId === 'MyDesigners-mobile-url') {
        MyDesignerSelected = true;
      } else if (elemId === 'MySizes-mobile-url') {
        MySizesSelected = true;
      }
    }
  } else if (elemId == 'MyDesigners-url' || elemId == 'MySizes-url') {
    var currentL = $this.find('.pc-li');
    if (currentL.hasClass('fa-check-square')) {
      // When My Designer or My Sizes were clicked and applied - Remove the check mark and clear the search
      currentL.removeClass('fa-check-square');
      if (haveBothFiltersSelected) {
        // IF Both were Selected and now Removing one
        if (elemId == 'MyDesigners-url') {
          // If UnSelcting My Designer - Get the MySizesURl from Cookie ad make My DesignerSelected = False - So checkmark wil also be removed from ISML
          url = cookieUtil.getCookie('mySizessUrlNotCombined');
          MyDesignerSelected = false;
          MySizesSelected = true;
        } else {
          url = cookieUtil.getCookie('myDesignersUrlNotCombined');
          MyDesignerSelected = true;
          MySizesSelected = false;
        }
      } else {
        url = $('li.clear-refinement').find('button').attr('data-href');
      }
    } else {
      //When clicked first time - Add the checkmark and set PrefCenterSelected = True
      currentL.addClass('fa-check-square');
      if ((haveBrandSelected && elemId === 'MySizes-url') || (haveSizeSelected && elemId === 'MyDesigners-url')) {
        MyDesignerSelected = true;
        MySizesSelected = true;
      } else if (elemId === 'MyDesigners-url') {
        MyDesignerSelected = true;
      } else if (elemId === 'MySizes-url') {
        MySizesSelected = true;
      }
    }
  }

  if (elemId != 'MyDesigners-url' && elemId != 'MySizes-url' && elemId != 'MyDesigners-mobile-url' && elemId != 'MySizes-mobile-url') {
    // Check My Designer or MySize was selected - And now selecting regular filter
    var myDesignersElemSelection = $('#MyDesigners-url').find('.pc-li');
    var myMySizesElemSelection = $('#MySizes-url').find('.pc-li');

    if (myDesignersElemSelection.hasClass('fa-check-square')) {
      //Desginder was already selected - So dont remove the checkmark
      if (haveBrandSelected) {
        MyDesignerSelected = true;
      }
    }
    if (myMySizesElemSelection.hasClass('fa-check-square')) {
      //Desginder was already selected - So dont remove the checkmark
      if (haveSizeSelected) {
        MySizesSelected = true;
      }
    }

    //Mobile
    var myDesignersMobElem = $('#MyDesigners-mobile-url');
    var myMySizesMobElem = $('#MySizes-mobile-url');
    if (myDesignersMobElem.hasClass('selected')) {
      if (haveBrandSelected) {
        MyDesignerSelected = true;
      }
    }
    if (myMySizesMobElem.hasClass('selected')) {
      if (haveSizeSelected) {
        MySizesSelected = true;
      }
    }
  }

  if (MySizesSelected) {
    sessionStorage.setItem('MySizesSelected', true);
  } else {
    sessionStorage.setItem('MySizesSelected', false);
  }

  if (MyDesignerSelected) {
    sessionStorage.setItem('MyDesignerSelected', true);
  } else {
    sessionStorage.setItem('MyDesignerSelected', false);
  }

  $.ajax({
    url: url,
    data: {
      page: $('.grid-footer').data('page-number'),
      selectedUrl: $(this).data('href'),
      type: type,
      filter: true,
      MyDesignerSelected: MyDesignerSelected,
      MySizesSelected: MySizesSelected,
      ajax: true
    },
    method: 'GET',
    success: function (response) {
      $('.category-refinement-bar').removeClass('slick-initialized')
      const result = parseResults(response, e, type);
      if (result) {
        lazyLoad.hydrateLazyLoadedImages();
        if (type === 'applied') {
          if ($this.find('i').length) {
            $this.find('i').remove();
          } else {
            $this.append('<i class="fa fa-check-square"></i>');
          }
          updateAppliedRefinement();
        }
        // remove the brand parameter appended on the refinement url, this will impact SEO url
        // also impacts the clearAll link being set wrongly on the page load. This param is intended to be used only on ajax
        if (url && url.indexOf('doNotReset=true') > -1) {
          url = url.replace('&doNotReset=true', '').replace('doNotReset=true&', '').replace('?doNotReset=true', '').replace('doNotReset=true', ''); // eslint-disable-line
        }
        history.pushState({}, '', url);
        persistentWishlist.markProductArrWishlisted();
        $('.card-header button').removeClass('clicked');
        // eslint-disable-next-line newline-per-chained-call
        $('.card-body').find('.selected').parents('.refinement').find('.card-header button').addClass('clicked');
        $('.view-more-less').text('View More');
        $('.refinement-category').removeClass('viewmore');
        // customscrollbar();
        viewmoreshow();
        prepActiveRefinementTrigger();
        commonSortWidthHandler();
        // append model images if it is enabled
        $('body').trigger('category:category:refinement:clone');
        $('body').trigger('category:set:model:view');
        $('body').trigger('adobeTagManager:productArrayUpdate', $this);
      }
      $.spinner().stop();
      $('.filter-results-container span.filter-count').text(
        $('.filter-bar').find('ul li.filter-value').length ? ' ' + '(' + $('.filter-bar').find('ul li.filter-value').length + ')' + ' ' : ''
      );

      if (refinementWrapperScrollTop && !$(window).width() <= 1024) {
        window.setTimeout(() => {
          refinementBar.scrollTop = refinementWrapperScrollTop;
        }, 10);
      } else {
        window.setTimeout(() => {
          $('.refinement.active').find('div.card-body ul.values').scrollTop(scrollvalue);
        }, 10);
      }

      if (refinementBar && !refinementBar.contains(document.activeElement)) {
        $('.refinement-bar').find(':visible:focusable').first().focus();
      }
      hbcTooltip.tooltipInit();
    },
    error: function () {
      hbcTooltip.tooltipInit();
      $.spinner().stop();
    }
  });
}

function replaceQueryParam(param, newval, search) {
  var regex = new RegExp('([?;&])' + param + '[^&;]*[;&]?');
  var query = search.replace(regex, '$1').replace(/&$/, '');

  return (query.length > 2 ? query + '&' : '?') + (newval ? param + '=' + newval : '');
}

function updateRefinementURL(url) {
  var sortOption = $('[name=sort-order]').find('option:selected').data('id');
  var index = url.indexOf('?');
  var search = index > -1 ? url.substring(index) : '';
  var result = index > -1 ? url.substring(0, index) : url;
  return result + replaceQueryParam('srule', sortOption, search);
}


base.applyFilter = function () {
  // Handle refinement value selection and reset click
  $('.container').on(
    'click',
    '.refinements li button, .refinement-bar button.reset, .filter-value button, .swatch-filter button, .clear-refinement button.reset, .store-refine input, .refinements li a, .sdd-refine input, .all-refine input, .applied-filters-wrapper li button, .applied-filters-wrapper li a',
    function (e) {
      var elemId = $(this).attr('id');
      if ($(this).hasClass('my-preferences-array')) {
        return;
        }       
      if (elemId != 'tooltip-info-btn' && elemId != 'tooltip-pc-plp-signin') {
        e.preventDefault();
        e.stopPropagation();
        $.spinner().start();

        var url = $(this).attr('data-href') || $(this).attr('href');
        url = updateRefinementURL(url);

        /* identify if the page has 'hide-designer'
                                If the page has hide-designer class, then we are passing an addition query parameter to avoid
                                setting the clear All link to brand filter.
                              */
        if (!$(this).closest('.search-results').hasClass('hide-designer')) {
          url += url && url.indexOf('?') > -1 ? '&doNotReset=true' : '?doNotReset=true';
        }

        if (window.matchMedia('(max-width: 1023px)').matches) {
          window.lastFilter = $(this).data('href') || $(this).attr('href');
        }
        if (!$(this).hasClass('bopis-refinement')) {
          window.params = url.split('?')[1];
        }

        // logic added for clearing of selected category from refinement
        var type = $(this).data('type') || '';

        if (type === 'category' && $(window).width() >= 1024) {
          // remove the brand parameter appended on the refinement url, this will impact SEO url
          // also impacts the clearAll link being set wrongly on the page load. This param is intended to be used only on ajax
          if (url && url.indexOf('doNotReset=true') > -1) {
            url = url.replace('&doNotReset=true', '').replace('doNotReset=true&', '').replace('?doNotReset=true', '').replace('doNotReset=true', ''); // eslint-disable-line
          }
          window.location = url;
        } else {
          if (type === 'category') {
            window.filterType = true;
          }
          $(this).trigger('search:filter', e);
          var $this = $(this);
          updateGrid(url, type, e, $this);
        }
      }
    }
  );
  hbcTooltip.tooltipInit();
};

thumbWidth();

// add an event to be able to control the display of the categories refinement when all sub-categories are going to be hidden.
// SFSX-1936
base.categoryRefinementToggle = function () {
  $('body').on('category:showhide:refinement', function () {
    var hasChildElements = $('body').find('.show-category-refinement').length > 0;
    if (!hasChildElements) {
      $('body').find('fieldset.refinement-category').toggleClass('d-none');
    }
  });
  $('body').trigger('category:showhide:refinement');
};

base.appliedDesigner = function() {
  $(document).on('click', '#my-preference-plp-designers-mobile a', function (e) {
    e.preventDefault();
    $('.refinement-bar-mob').addClass('hide-refine');
    $('.my-designers-bar').removeClass('d-none');
  });
  $(document).on('click', '.my-designers-bar .filter-header button', function (e) {
    e.preventDefault();
    $('.refinement-bar-mob').removeClass('hide-refine');
    $('.my-designers-bar').addClass('d-none');
  });
};

base.appliedSize = function() {
  $(document).on('click', '#my-preference-plp-sizes-mobile a', function (e) {
    e.preventDefault();
    $('.refinement-bar-mob').addClass('hide-refine');
    $('.my-sizes-bar').removeClass('d-none');
  });
  $(document).on('click', '.my-sizes-bar .filter-header button', function (e) {
    e.preventDefault();
    $('.refinement-bar-mob').removeClass('hide-refine');
    $('.my-sizes-bar').addClass('d-none');
  });
};


base.appliedFilter = function () {
  $('body').on('click', '.applied-refinement button', function () {
    $('.refinement-bar-mob').addClass('hide-refine');
    $('.applied-filters-bar').removeClass('d-none');
  });
  $('body').on('click', '.applied-filters-bar .filter-header button', function () {
    $('.refinement-bar-mob').removeClass('hide-refine');
    $('.applied-filters-bar').addClass('d-none');
  });
  $('body').on('click', '.applied-filters-bar .update-all-btn button', function () {
    $('.refinement-bar-mob').removeClass('hide-refine');
    $('.applied-filters-bar').addClass('d-none');
    $('.refinement-wrapper .shop-item').click();
    $('.applied-filters-wrapper .selected-filter').each(function () {
      $(this)
        .find('li')
        .each(function () {
          if (!$(this).find('i').length) {
            $(this).remove();
          }
        });
      if (!$(this).find('li').length) {
        $(this).remove();
      }
    });
  });
  $('body').on('click', '.applied-filters-bar .clear-all button', function () {
    window.location = $('.container .clear-refinement button.reset').data('href');
  });
};

base.sort = function () {
  // code changes for adding sort parameter to url when moving out of the page for scroll persistence
  $('.container').on('change', '[name=sort-order]', function (e) {
    e.preventDefault();
    e.stopPropagation();
    var isMobileSelect = !!this.closest('.sort-results-container');
    if (isMobileSelect) {
      var desktopSelect = $('.sort-selection-block select[name="sort-order"]');
      desktopSelect.val(this.value);
    }
    window.params = this.value.split('?')[1];
    $.spinner().start();
    $(this).trigger('search:sort', this.value);
    var $this = $(this);
    $.ajax({
      url: this.value,
      data: {
        selectedUrl: this.value
      },
      method: 'GET',
      success: function (response) {
        $('.product-grid').empty().html(response);
        $('.card-header button').removeClass('clicked');
        $('.card-body').find('.selected').parents('.refinement').find('.card-header button').addClass('clicked');
        $('.view-more-less').text($('.view-more-less').data('viewmore'));
        $('.refinement-category').removeClass('viewmore');
        // customscrollbar();
        viewmoreshow();
        if ($(window).width() < 1023 && $('.refinement-wrapper .refinements').height() > $('.refinement-wrapper').height()) {
          $('.collapsible-xl.refinement .card-body').height($('.refinement-wrapper .refinements').height());
        }
        $('body').trigger('adobeTagManager:productArrayUpdate', $this);
        commonSortWidthHandler();
        $('body').trigger('category:set:model:view');
        lazyLoad.hydrateLazyLoadedImages();
        productGridTeasers.addReviews();
        $.spinner().stop();
      },
      error: function () {
        $.spinner().stop();
      }
    });
  });
};

base.sortWidthHandler = function () {
  commonSortWidthHandler();

  $('.sort-selection-block .select-sort-order').change(function () {
    var text = $(this).find('option:selected').text();
    var $aux = $('<select/>').append($('<option/>').text(text));
    $(this).after($aux);
    $(this).width($aux.width() + 10);
    $aux.remove();
  });
};

/**
 * Clears the designer search
 *
 * @param {Object} element - current element
 */
function clearSearch(element) {
  var allRefValues = element.parent().siblings('ul').find('li');
  allRefValues.removeClass('d-none');
  $('.refinement-search-bar').siblings('.no-results').addClass('d-none');
}

base.filterBrands = function () {
  $('body').on('keyup', '.refinement-search .refinement-search-bar input', function () {
    let $this = $(this);
    var searchKeyWord = $this.val();
    if (searchKeyWord) {
      $this.siblings('button.search-button').addClass('d-none');
      $this.siblings('button.close-button').removeClass('d-none');
      var allRefValues = $this.parent().siblings('ul').find('li');
      var searchFound = false;
      $.each(allRefValues, function () {
        var html = $(this).find('span.value').html();
        if (html.trim().toLowerCase().indexOf(searchKeyWord.toLowerCase()) != -1) {
          searchFound = true;
          $(this).removeClass('d-none');
        } else {
          $(this).addClass('d-none');
        }
      });
      $this.parent().siblings('.no-results').toggleClass('d-none', searchFound);
    } else {
      $this.siblings('button.search-button').removeClass('d-none');
      $this.siblings('button.close-button').addClass('d-none');
      clearSearch($this);
    }
  });
};
base.designerPage = function () {
  // designer index
  try {
    $('.all-designs a').each(function () {
      $(this).removeClass('selected');
      var querystring = $('.page').data('querystring');
      var categoryID = $(this).data('category');
      if (categoryID && querystring && querystring.indexOf(categoryID) > -1) {
        $(this).addClass('selected');
      }
    });
  } catch (e) {
    console.log('Could not flag selected category');
  }
};
function addCustomerPreferences(preferenceType, value) {
  var customerNo = document.getElementById('CustomerPreferencesCustomerNo').innerHTML;
  var updateCustomerPreferencesUrl = document.getElementById('updateCustomerPreferencesUrl').href;
  $.ajax({
    url: updateCustomerPreferencesUrl,
    type: 'post',
    dataType: 'json',
    data: {
      customerNo: customerNo,
      preferenceType: preferenceType,
      value: JSON.stringify(value),
      updatingFromPA: true,
      action: 'ADD'
    },
    success: function (data) {},
    error: function (err) {}
  });

  //Ajax call to get the latest My Deisgner and My Sizes URL after adding new data from PA
  // and Add a check and remove disbale from "My Sizes and My Designers"
  var url = window.location.href; //Getting current page URL
  $.ajax({
    url: url,
    type: 'GET',
    data: {
      addToMyPef: true
    },
    success: function (data) {
      var cookieUtil = require('core/components/utilhelper');
      var myDesignersUrl = cookieUtil.getCookie('myDesignersUrl');
      var mySizesUrl = cookieUtil.getCookie('mySizesUrl');
      var allMyDesignerSelected = cookieUtil.getCookie('allMyDesignerSelected');
      var allMySizesSelected = cookieUtil.getCookie('allMySizesSelected');

      var myDesignersUrlA = document.getElementById('MyDesigners-url'); //or grab it by tagname etc
      myDesignersUrlA.href = myDesignersUrl;

      var myDesignersUrlMobileA = document.getElementById('MyDesigners-mobile-url'); //or grab it by tagname etc
      myDesignersUrlMobileA.href = myDesignersUrl;

      var mySizesUrlA = document.getElementById('MySizes-url'); //or grab it by tagname etc
      mySizesUrlA.href = mySizesUrl;

      var mySizesUrlMobileA = document.getElementById('MySizes-mobile-url'); //or grab it by tagname etc
      mySizesUrlMobileA.href = mySizesUrl;

      //Adding checkmark after adding "My Deisgners" and "My SIzes" from PA "Save to .."
      if (preferenceType == 'DesignerPreferences' && allMyDesignerSelected === 'true') {
        $('#MyDesigners-url').removeClass('disabled-pc-plp');
        $('#MyDesigners-url').removeClass('disabled');
        var myDesignersElemSelection = $('#MyDesigners-url').find('.pc-li');
        myDesignersElemSelection.addClass('fa-check-square');

        //For Mobile Screen
        var myMyDesignersMobElem = $('#MyDesigners-mobile-url');
        myMyDesignersMobElem.removeClass('disabled');
        myMyDesignersMobElem.removeClass('disabled-pc-plp');
        $('#my-preference-plp-designers-mobile a').removeClass('disabled-pc-plp');
        $('#my-preference-plp-designers-mobile a').removeClass('disabled');
        myMyDesignersMobElem.addClass('selected');
        $('#MyDesigners-mobile-url u').text('Deselect');
      } else if (preferenceType == 'SizePreferences' && allMySizesSelected === 'true') {
        $('#MySizes-url').removeClass('disabled-pc-plp');
        $('#MySizes-url').removeClass('disabled');
        var myMySizesElemSelection = $('#MySizes-url').find('.pc-li');
        myMySizesElemSelection.addClass('fa-check-square');

        //For Mobile Screen
        var myMySizesMobElem = $('#MySizes-mobile-url');
        myMySizesMobElem.removeClass('disabled');
        myMySizesMobElem.removeClass('disabled-pc-plp');
        $('#my-preference-plp-sizes-mobile a').removeClass('disabled-pc-plp');
        $('#my-preference-plp-sizes-mobile a').removeClass('disabled');
        myMySizesMobElem.addClass('selected');
        $('#MySizes-mobile-url u').text('Deselect');
      }
    },
    error: function (err) {}
  });
}

base.saveRefinementsToMyPreferences = function () {
  $('body').on('click', '.pc-info-cta-save:not(.sign-in)', function (event) {
    var value = [];
    var pcSection = event.currentTarget.closest(['li.pc-designers', 'li.pc-sizes']);
    var preferenceType = $(pcSection).hasClass('pc-designers') ? 'DesignerPreferences' : 'SizePreferences';
    var allRefinements = $(pcSection).siblings('li');
    var selectedRefinements = $(allRefinements).find('span.selected');
    var saveCta = $(pcSection).find('.pc-info-cta-save');
    var savedMsg = $(pcSection).find('.pc-info-cta-saved');
    var saveCount = $(pcSection).find('.pc-info-count');
    window.designerPreferences = window.designerPreferences || {};
    window.sizePreferences = window.sizePreferences || {};
    saveCount.innerHTML = '0 Selected';
    saveCta.fadeOut(1);
    savedMsg.css('display', 'flex');
    savedMsg.fadeIn(1);
    setTimeout(function () {
      savedMsg.fadeOut(400);
    }, 3000);
    selectedRefinements.each(function (i, el) {
      if (preferenceType === 'DesignerPreferences') {
        value.push(el.dataset.pcId);
        window.designerPreferences[el.dataset.pcValue] = el.dataset.pcValue;
      } else if (preferenceType === 'SizePreferences' && !~el.dataset.pcId.toLowerCase().indexOf('intimate')) {
        value.push(el.dataset.pcId);
        window.sizePreferences[window.categoryToAddInto] = window.sizePreferences[window.categoryToAddInto] || {};
        window.sizePreferences[window.categoryToAddInto][el.dataset.pcValue] = el.dataset.pcValue;
      }
    });
    window.designerPrefSelectedCount = 0;
    window.sizePrefSelectedCount = 0;
    addCustomerPreferences(preferenceType, value);
  });
};

base.mobileSaveRefinementsToMyPreferences = function () {
  $('body').on('click', '.mobile-pc-info-cta-save:not(.sign-in)', function (event) {
    var value = [];
    var preferenceType = $(event.currentTarget).hasClass('mobile-pc-designers') ? 'DesignerPreferences' : 'SizePreferences';
    var pcSection = preferenceType === 'DesignerPreferences' ? $('li.pc-designers') : $('li.pc-sizes');
    var allRefinements = $(pcSection).siblings('li');
    var selectedRefinements = $(allRefinements).find('span.selected');
    var saveCta = $('.mobile-pc-info-cta-save');
    var savedMsg = $('.mobile-pc-info-cta-saved');
    window.designerPreferences = window.designerPreferences || {};
    window.sizePreferences = window.sizePreferences || {};
    saveCta.fadeOut(1, function () {
      saveCta.addClass('d-none');
      saveCta.css('display', '');
    });
    savedMsg.css('display', 'flex');
    savedMsg.fadeIn(1);
    setTimeout(function () {
      savedMsg.fadeOut(400);
    }, 3000);

    selectedRefinements.each(function (i, el) {
      if (preferenceType === 'DesignerPreferences') {
        value.push(el.dataset.pcId);
        window.designerPreferences[el.dataset.pcValue] = el.dataset.pcValue;
      } else if (preferenceType === 'SizePreferences' && !~el.dataset.pcId.toLowerCase().indexOf('intimate')) {
        value.push(el.dataset.pcId);
        window.sizePreferences[window.categoryToAddInto] = window.sizePreferences[window.categoryToAddInto] || {};
        window.sizePreferences[window.categoryToAddInto][el.dataset.pcValue] = el.dataset.pcValue;
      }
    });
    window.designerPrefSelectedCount = 0;
    window.sizePrefSelectedCount = 0;
    addCustomerPreferences(preferenceType, value);
  });
};

base.toolkitOpenedPageLoad = function () {
  $('#refinement-brand').on('click', function (event) {
    if (
      document.querySelector('.tooltip-info.tooltip-pc-brands-plp') &&
      (sessionStorage.PCBrandTooltipOpened === undefined || sessionStorage.PCBrandTooltipOpened === null)
    ) {
      document.querySelector('.tooltip-info.tooltip-pc-brands-plp').click();
      sessionStorage.setItem('PCBrandTooltipOpened', true);
    }
  });

  $('#refinement-sizeRefinement').on('click', function (event) {
    if (
      (sessionStorage.PCSizeTooltipOpened === undefined || sessionStorage.PCSizeTooltipOpened === null) &&
      document.querySelector('.tooltip-info.tooltip-pc-sizes-plp')
    ) {
      document.querySelector('.tooltip-info.tooltip-pc-sizes-plp').click();
      sessionStorage.setItem('PCSizeTooltipOpened', true);
    }
  });
};

base.mobileSaveToMyPreferencesButtonDisplay = function () {
  var mobileSaveToMyDesignersBtn = $('.mobile-pc-info-cta-save.mobile-pc-designers')[0];
  var mobileSaveToMySizesBtn = $('.mobile-pc-info-cta-save.mobile-pc-sizes')[0];
  var mobileDesignerTab = $('.refinement-bar-mob .refinements .refinement-id-brand')[0];
  var mobileSizeTab = $('.refinement-bar-mob .refinements .refinement-id-sizerefinement')[0];
  var refinementBarMob = $('.refinement-bar-mob');
  window.designerPreferences = window.designerPreferences || {};
  window.sizePreferences = window.sizePreferences || {};

  $('.container').on('click', 'button.filter-results', function () {
    if ($(mobileDesignerTab).hasClass('active') && window.designerPrefSelectedCount) {
      $(refinementBarMob).addClass('with-save-pref-btn');
      $(mobileSaveToMySizesBtn).addClass('d-none');
      $(mobileSaveToMyDesignersBtn).removeClass('d-none');
    } else if ($(mobileSizeTab).hasClass('active') && window.sizePrefSelectedCount) {
      $(refinementBarMob).addClass('with-save-pref-btn');
      $(mobileSaveToMySizesBtn).removeClass('d-none');
      $(mobileSaveToMyDesignersBtn).addClass('d-none');
    } else {
      $(refinementBarMob).removeClass('with-save-pref-btn');
      $(mobileSaveToMySizesBtn).addClass('d-none');
      $(mobileSaveToMyDesignersBtn).addClass('d-none');
    }
  });

  $('.container').on(
    'click',
    '.refinement-bar-mob .refinements .refinement-id-brand li:not(.pc-designers) a, .refinement-bar-mob .refinements .refinement-id-sizerefinement li:not(.pc-sizes) button',
    function (event) {
      mobileDesignerTab = $('.refinement-bar-mob .refinements .refinement-id-brand')[0];
      mobileSizeTab = $('.refinement-bar-mob .refinements .refinement-id-sizerefinement')[0];

      if ($(event.currentTarget).children('span.value').hasClass('selected')) {
        if ($(mobileDesignerTab).hasClass('active') && !(event.currentTarget.dataset.pcValue in window.designerPreferences)) {
          window.designerPrefSelectedCount--;
        } else if (
          $(event.currentTarget).children('span.value').data('pcId') &&
          !~$(event.currentTarget).children('span.value').data('pcId').toLowerCase().indexOf('intimate') &&
          $(mobileSizeTab).hasClass('active') &&
          (!window.sizePreferences[window.categoryToAddInto] ||
            (window.sizePreferences[window.categoryToAddInto] && !(event.currentTarget.dataset.pcValue in window.sizePreferences[window.categoryToAddInto])))
        ) {
          window.sizePrefSelectedCount--;
        }
      } else {
        if ($(mobileDesignerTab).hasClass('active') && !(event.currentTarget.dataset.pcValue in window.designerPreferences)) {
          window.designerPrefSelectedCount++;
        } else if (
          $(event.currentTarget).children('span.value').data('pcId') &&
          !~$(event.currentTarget).children('span.value').data('pcId').toLowerCase().indexOf('intimate') &&
          $(mobileSizeTab).hasClass('active') &&
          (!window.sizePreferences[window.categoryToAddInto] ||
            (window.sizePreferences[window.categoryToAddInto] && !(event.currentTarget.dataset.pcValue in window.sizePreferences[window.categoryToAddInto])))
        ) {
          window.sizePrefSelectedCount++;
        }
      }

      if (window.designerPrefSelectedCount && $(mobileDesignerTab).hasClass('active')) {
        $(refinementBarMob).addClass('with-save-pref-btn');
        $(mobileSaveToMyDesignersBtn).removeClass('d-none');
      } else {
        $(refinementBarMob).removeClass('with-save-pref-btn');
        $(mobileSaveToMyDesignersBtn).addClass('d-none');
      }

      if (window.sizePrefSelectedCount && $(mobileSizeTab).hasClass('active')) {
        $(refinementBarMob).addClass('with-save-pref-btn');
        $(mobileSaveToMySizesBtn).removeClass('d-none');
      } else {
        $(refinementBarMob).removeClass('with-save-pref-btn');
        $(mobileSaveToMySizesBtn).addClass('d-none');
      }
    }
  );

  $('body').on('click', '.refinement-bar-mob .refinements .refinement .card-header button', function (event) {
    if (event.currentTarget.id === 'refinement-sizeRefinement' && window.sizePrefSelectedCount) {
      $(refinementBarMob).addClass('with-save-pref-btn');
      $(mobileSaveToMySizesBtn).removeClass('d-none');
      $(mobileSaveToMyDesignersBtn).addClass('d-none');
    } else if (event.currentTarget.id === 'refinement-brand' && window.designerPrefSelectedCount) {
      $(refinementBarMob).addClass('with-save-pref-btn');
      $(mobileSaveToMySizesBtn).addClass('d-none');
      $(mobileSaveToMyDesignersBtn).removeClass('d-none');
    } else {
      $(refinementBarMob).removeClass('with-save-pref-btn');
      $(mobileSaveToMySizesBtn).addClass('d-none');
      $(mobileSaveToMyDesignersBtn).addClass('d-none');
    }
  });
};

function appendModalHtmlElement(data) {
  if ($('#signInModal').length !== 0) {
    $('#signInModal').empty();
  }
  var htmlString =
    '<!-- Modal -->' +
    '<div class="modal-dialog">' +
    '<div class="modal-content">' +
    '<div class="modal-body">' +
    '<div class="cancel-icon">' +
    '<button type="button" class="close svg-svg-22-cross svg-svg-22-cross-dims" data-dismiss="modal" aria-label="Close Modal Box"></button>' +
    '</div>' +
    '<div class="no-gutters modal-row align-items-start modal-contents">' +
    '<div class="modal-column">' +
    data.template +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';
  if ($(document).find('#signInModal').length) {
    $(document).find('#signInModal').append(htmlString);
  }
}

function openLoginPopup(url) {
  var cookieUtil = require('core/components/utilhelper');
  $('body').spinner().start();
  $.ajax({
    url: url,
    dataType: 'json',
    success: function (data) {
      $('body').spinner().stop();
      if (data.redirect) {
        window.location.href = data.redirect;
      } else {
        appendModalHtmlElement(data);
        login.login();
        cookieUtil.setCookie('paurl', '', 1);
        cookieUtil.setCookie('paurl', window.location, 1);
        $('#signInModal').modal();
        login.showPassword();
        setTimeout(function () {
          formField.adjustForAutofill();
        }, 300);
      }
    },
    error: function () {
      $('body').spinner().stop();
    }
  });
}

base.prefCenterInitSignIn = function () {
  var cookieUtil = require('core/components/utilhelper');
  $('body').on('click', '.tooltip-pc-plp-sign-in, .pc-info-cta-save.sign-in, .mobile-pc-info-cta-save.sign-in', function (event) {
    var prefsToSave = [];
    var preferenceType =
      $(event.currentTarget).hasClass('mobile-pc-designers') || $(event.currentTarget).hasClass('pc-info-cta-save-designers')
        ? 'DesignerPreferences'
        : 'SizePreferences';

    var pcSection = preferenceType === 'DesignerPreferences' ? $('li.pc-designers') : $('li.pc-sizes');
    var allRefinements = $(pcSection).siblings('li');
    var selectedRefinements = $(allRefinements).find('span.selected');

    selectedRefinements.each(function (i, el) {
      if (preferenceType === 'DesignerPreferences' || (preferenceType === 'SizePreferences' && !~el.dataset.pcId.toLowerCase().indexOf('intimate'))) {
        prefsToSave.push(el.dataset.pcId);
      }
    });
    var prefsToSaveString = btoa(JSON.stringify(prefsToSave));
    cookieUtil.setCookie('prefstosave', '', 1);
    cookieUtil.setCookie('prefstosave', prefsToSaveString, 1);
    if (window.FABuild) {
      window.location.href = $(this).data('appUrl');
    } else {
      openLoginPopup($(this).data('url'));
    }
  });
};

function getItemsWithMultipleImages() {
  return document.querySelectorAll('.js-product-model-img');
}

// Image data is retrieved from the data-attribute "additional-images" located on the first image element.
function getAdditionalImageData($firstImage) {
  const additionalImageData = $firstImage.data('additional-images');
  return additionalImageData;
}

function getElementImagesHTML(imageArray) {
  const elementImagesHTML = imageArray.reduce((acc, image) => {
    const el = document.createElement('img');
    Object.keys(image).forEach(att => el.setAttribute(att, image[att]));
    return acc + el.outerHTML;
  }, '');

  return elementImagesHTML;
}

function initiateSlickOntoItemsWithMultipleImages(itemsWithMultipleImages) {
  for (let i = 0; i < itemsWithMultipleImages.length; i++) {
    const item = itemsWithMultipleImages[i];
    const $item = $(item);

    if ($item.closest('.image-container').hasClass('prevent-slick')) {
      return false;
    }

    const $firstImage = $item.find('.tile-image:first');
    const firstImageEl = $firstImage.get(0);
    const additionalImageData = getAdditionalImageData($firstImage);

    // We are only creating two image nodes because we want to render two images only for performance. When the user interacts, it will load the rest of the images.
    const twoImageArray = additionalImageData.slice(0, 1);
    const twoImagesHTML = getElementImagesHTML(twoImageArray);
    firstImageEl.insertAdjacentHTML('afterend', twoImagesHTML);

    $item.find('img').each(function () {
      $item.attr('src', $item.attr('data-src'));
    });

    if (!/msie\s|trident\/|Edge\//i.test(navigator.userAgent) && $firstImage.attr('data-additional-images')) {
      $item.slick({
        infinite: true,
        swipeToSlide: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: false,
        autoplaySpeed: 4500,
        speed: 1000,
        touchThreshold: 3
      });

      let xCoordStart;
      let yCoordStart;
      const xSlideTrigger = 10;
      const slickElements = $item.closest('.product-tile').find('.image-container a');
      const slickElement = slickElements[0];

      $(slickElement).on('touchstart', function (e) {
        xCoordStart = e.originalEvent.touches[0].clientX;
        yCoordStart = e.originalEvent.touches[0].clientY;
      });

      $(slickElement).on('touchend', function (e) {
        const xCoordEnd = e.originalEvent.changedTouches[0].clientX;
        const yCoordEnd = e.originalEvent.changedTouches[0].clientY;
        const deltaX = Math.abs(xCoordEnd - xCoordStart);
        const deltaY = Math.abs(yCoordEnd - yCoordStart);

        if (deltaX > deltaY) {
          $(slickElement).slick('slickNext');
        } else if (xCoordStart < xCoordEnd + xSlideTrigger) {
          $(slickElement).slick('slickPrev');
        }
      });
    }
  }
}

/* gucci left nav in desktop to be not collapsed on page load */
$(document).ready( function () {
  if (window.FABuild) {
    const itemsWithMultipleImages = getItemsWithMultipleImages();
    initiateSlickOntoItemsWithMultipleImages(itemsWithMultipleImages);
  }

  if($(window).width() > 1023) {
    $('.gucci-left-nav .nav-item .collapse').addClass("show");
    $('.gucci-left-nav .nav-item .cat-link').removeClass("collapsed");
  }
});
$(window).on('resize', function(){
  if($(window).width() > 1023) {
    $('.gucci-left-nav .nav-item .collapse').addClass("show");
    $('.gucci-left-nav .nav-item .cat-link').removeClass("collapsed");
  } else {
    $('.gucci-left-nav .nav-item .collapse').removeClass("show");
    $('.gucci-left-nav .nav-item .cat-link').addClass("collapsed");
  }
});

base.prefCenterInitSignIn = function () {
  $(document).ready(function () {
    var $noresultRecommendationsWrapper = $('.noresult-recommendations-wrapper');

    if ($noresultRecommendationsWrapper.length) {
      $.ajax({
        url: $noresultRecommendationsWrapper.data('url'),
        type: 'get',
        success: function (data) {
          $noresultRecommendationsWrapper.html(data);
        }
      });
    }
  });
};

module.exports = base;
