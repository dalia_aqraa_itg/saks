'use strict';

// code for anchoring

$(document).on('click', '.designer-alphas a[href]', function (e) {
  e.preventDefault();
  var activeLetter = $('.designer-alphas .active a').text().trim() || 0;
  var currentOffcet = activeLetter != 0 ? parseInt($('#' + activeLetter).offset().top) : 0;

  var href = $(this).attr('href');
  var scrollPosition = parseInt($('#' + href).offset().top);

  var height = parseInt($('#' + href + ' .alpha-bet').height());
  var margin = parseInt(
    $('#' + href)
      .css('margin-top')
      .substring(0, 2)
  );
  var space = margin + height;

  $('.designer-alphas').find('li').removeClass('active');
  $(this).closest('li').addClass('active');
  $('html, body').animate(
    {
      /* eslint-disable */
      scrollTop: scrollPosition < currentOffcet ? scrollPosition - $('.navigation-section').height() - space : scrollPosition - space
      /* eslint-enable */
    },
    500
  );
});

/**
 * Designer Index Slider
 *
 */
function stickyDesignerAlphas() {
  $('.designer-alphas').removeClass('fixed-alphas');
  if ($('.main-menu').hasClass('fixed') || $('#header-container').hasClass('fixed')) {
    $('.designer-alphas').addClass('fixed-alphas');
  }
}
/**
 * Designer Index Slider
 *
 */
function designerSlider() {
  if ($(window).width() <= 1023.99) {
    $('.designer-main')
      .find('.designer-alphas')
      .not('.slick-initialized')
      .slick({
        responsive: [
          {
            breakpoint: 1025,
            settings: 'unslick'
          },
          {
            breakpoint: 769,
            settings: {
              slidesToShow: 16,
              slidesToScroll: 16,
              infinite: false,
              arrows: false
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 8,
              slidesToScroll: 8,
              infinite: false,
              arrows: false
            }
          }
        ]
      });
  }
}

designerSlider();
$(window).resize(function () {
  designerSlider();
  stickyDesignerAlphas();
});

$(window).scroll(function () {
  stickyDesignerAlphas();
});

var stickyHeight = $('.navigation-section').outerHeight();
$(document)
  .on('page:scrollDown', function () {
    $('.designer-alphas').css('top', '0');
  })
  .on('page:scrollUp', function () {
    $('.designer-alphas').css('top', stickyHeight);
  });
