/**
 * Store cookies with commission vendor data
 * @param request
 * @returns
 */
function manageCommissionCookies(sfData) {
  var site_refer = sfData.site_refer;
  // do not store cookies if site_refer parameter does not exist or empty
  if (!validateSiteRefer(site_refer)) {
    return;
  }
  var commissionCookieNames = ['site_refer', 'sf_storeid', 'sf_associd'];
  var age = 14 * 24 * 60 * 60 * 1000;
  commissionCookieNames.forEach(function (name) {
    var value = sfData[name];
    var expires = new Date();
    expires.setTime(expires.getTime() + age);
    document.cookie = name + '=' + value + ';path=/;Secure;SameSite=None;expires=' + expires.toUTCString();
  });
}

function getUrlParameter(name, queryString) {
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  var results = regex.exec(queryString);
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function getCookie(name) {
  var value = '; ' + document.cookie;
  var parts = value.split('; ' + name + '=');
  if (parts.length >= 2) return parts.pop().split(';').shift();
}

/**
 * Attribution logic for cookies
 * @param value
 * @returns
 */
function validateSiteRefer(value) {
  if (!value) {
    return false;
  }
  // If Site Refer contains EML, TRP or TR, Do not set the Site Refere if existing site refer is CNCT and COMX
  if (value.indexOf('EML') > 0 || value.indexOf('TRP') > 0 || value.indexOf('TR') > 0) {
    var getSite_Refer = getCookie('site_refer');
    if (getSite_Refer && (getSite_Refer.indexOf('CNCT') > 0 || getSite_Refer.indexOf('COMX') > 0)) {
      return false;
    }
  }

  var site_ref_search = 'SEM';
  var site_ref_email = 'EML';
  var site_ref_email_SF = 'EMLHB_SF';
  var emlRegexp = new RegExp('^(' + site_ref_email + '|' + site_ref_search + ')');
  if (emlRegexp.test(value)) {
    if (value !== site_ref_email_SF) {
      return false;
    }
  }
  return true;
}
function prepareSFdate(data) {
  var sfData = {};
  var queryString = window.location.search;
  var site_refer = getUrlParameter('site_refer', queryString);

  if (site_refer) {
    sfData.site_refer = site_refer;
  } else if (data.site_refer) {
    sfData.site_refer = data.site_refer;
  } else {
    sfData.site_refer = 'salesfloor';
  }

  if (sfData.site_refer == 'salesfloor') {
    if (data.store_id) sfData.sf_storeid = data.store_id;
    if (data.employee_id) sfData.sf_associd = data.employee_id;
  } else {
    sfData.sf_associd = getUrlParameter('sf_associd', queryString);
    sfData.sf_storeid = getUrlParameter('sf_storeid', queryString);
  }

  return sfData;
}

module.exports = function () {
  $(window).on('load', function () {
    if (window.sf_widget && window.sf_widget.utils) {
      try {
        window.sf_widget.utils.isWithinGeo().then(function (data) {
          if (data === true) {
            $('.stylist-chat-btn').prop('disabled', false);
            sessionStorage.setItem('sf_within_geo', true);
          } else {
            $('.stylist-chat-btn').prop('disabled', true);
          }
        });
      } catch (err) {
        console.log(err); //eslint-disable-line
      }
    }
  });
  $(window).on('load', function () {
    try {
      sf_widget.utils.getTrackingEmployeeObjectAsPromise().then(function (data) {
        if (data) {
          var sfData = prepareSFdate(data);
          manageCommissionCookies(sfData);
        }
      });
    } catch (e) {
      console.log('There was an error with the SF_widget : ' + e.message);
    }
  });
  if (window.location.search.indexOf('site_refer')) {
    const queryString = window.location.search;
    var site_refer = getUrlParameter('site_refer', queryString);
    if (site_refer !== '') {
      var data = {};
      data.site_refer = site_refer;
      data.sf_associd = getUrlParameter('sf_associd', queryString);
      data.sf_storeid = getUrlParameter('sf_storeid', queryString);
      manageCommissionCookies(data);
    }
  }
  $(document).ready(function () {
    try {
      // Initialize the COMX Session
      var comxURL = $('.comx-init').attr('data-url');
      $.ajax({
        url: comxURL,
        type: 'get',
        dataType: 'json',
        success: function (data) {}
      });
    } catch (e) {
      console.log('There was an error with the COMX Session Init : ' + e.message);
    }
  });
};
