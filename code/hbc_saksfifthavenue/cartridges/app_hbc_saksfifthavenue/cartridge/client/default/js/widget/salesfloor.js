'use strict';

const openpgp = require('openpgp');

function sfTracking(orderData) {
  sf_widget_events = {
    events: [],
    tracker: function (eventName, data) {
      sf_widget_events.events.push({
        eventName: eventName,
        data: data
      });
    }
  };

  sf_widget_events.tracker('ecommerce:addTransaction', {
    customer_info: orderData.customer_info,
    trx_id: orderData.trx_id,
    trx_total: orderData.trx_total,
    currency: orderData.currency
  });

  if (orderData.items) {
    var items = orderData.items;
    items.forEach(function (item) {
      sf_widget_events.tracker('ecommerce:addItem', {
        trx_id: orderData.trx_id,
        product_id: item.masterProductID ? item.masterProductID : item.id,
        trx_detail_total: item.priceTotal.unFormattedpriceValue / item.quantity,
        quantity: item.quantity
      });
    });
  }

  try {
    window.sf_widget && sf_widget.widgets && sf_widget.widgets.eventstracker && sf_widget.widgets.eventstracker.load();
  } catch (err) {
    Logger.error(err);
  }
}

function trackSFOrderTransaction() {
  var publicKeyArmored;
  if (window.sfPublicKey !== undefined) {
    publicKeyArmored = window.sfPublicKey;
  } else {
    return null;
  }

  var sfOrder = $('.widget-salesfloor-transaction');

  var customerInfo = {};
  customerInfo.customer_name = sfOrder.data('customer_name');
  customerInfo.customer_email = sfOrder.data('customer_email');

  openpgp.key.readArmored(publicKeyArmored).then(function (publicKey) {
    var options = {
      message: openpgp.message.fromText(JSON.stringify(customerInfo)),
      publicKeys: publicKey.keys[0]
    };
    openpgp.encrypt(options).then(function (ciphertext) {
      var encrypted = ciphertext.data;
      var orderData = {};
      orderData.customer_info = btoa(encrypted);
      orderData.trx_id = sfOrder.data('trx_id');
      orderData.trx_total = sfOrder.data('trx_total');
      orderData.currency = sfOrder.data('currency');
      orderData.items = sfOrder.data('items');
      sfTracking(orderData);
    });
  });
}

module.exports = function () {
  $(window).on('load', function () {
    trackSFOrderTransaction();
  });
};
