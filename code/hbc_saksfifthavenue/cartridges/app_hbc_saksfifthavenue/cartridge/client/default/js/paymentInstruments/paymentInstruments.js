'use strict';

var base = require('core/paymentInstruments/paymentInstruments');
var formValidation = require('base/components/formValidation');
var cleave = require('core/components/cleave');

base.submitPayment = function () {
  var url;
  $('form.payment-form').submit(function (e) {
    var $form = $(this);
    e.preventDefault();
    url = $form.attr('action');
    $('.js-tokenex-error').removeClass('alert-danger');
    $('.js-tokenex-error p').empty();
    $form.spinner().start();
    $('form.payment-form').trigger('payment:submit', e);

    var tccCardNumber;
    if ($('#tccCardNumber').length > 0) {
      tccCardNumber = $('#tccCardNumber').data('cleave').getRawValue();
      if (tccCardNumber && tccCardNumber.length === 29) {
        // Copy TCC to Regular CC.
        $('#cardNumber').val(tccCardNumber);
      }
    }
    var formData = cleave.serializeData($form);

    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: formData,
      success: function (data) {
        $form.spinner().stop();
        if (!data.success) {
          if (data.tokenExError) {
            $('.js-tokenex-error').addClass('alert-danger');
            $('.js-tokenex-error p').text(data.tokenExError);
          } else {
            formValidation($form, data);
          }
        } else {
          location.href = data.redirectUrl;
        }
      },
      error: function (err) {
        if (err.responseJSON.redirectUrl) {
          window.location.href = err.responseJSON.redirectUrl;
        }
        $form.spinner().stop();
      }
    });
    return false;
  });
};

module.exports = base;
