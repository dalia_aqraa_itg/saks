'use strict';

var debounce = require('lodash/debounce');
var utilhelper = require('./utilhelper');

/**
 * Is the window large-ish?
 * @returns {Boolean} true if large, otherwise false.
 */
function isDesktop() {
  return window.innerWidth >= 1024;
}

function isPrefCenter() {
  return !!document.querySelector('.preference-center-page');
}

function getBannerHeight() {
  //var headerContainer = document.querySelector('#header-container');
  var headerBanner = document.querySelector('.header-banner');
  return headerBanner === null ? 0 : headerBanner.offsetHeight;
}

/**
 * Adjusts the `top` value on the .page-header,
 * Because the header has a position: sticky, this will showing or hiding it.
 *
 * @param {String} action 'show' or 'hide'
 */
function adjustStickyHeaderDisplay(action) {
  const header = document.querySelector('.page-header');
  if (!header) {
    return;
  }
  if (action === 'show') {
    // When revealing the header on scroll up, we don't want to reveal the banner.
    header.style.top = `${getBannerHeight() * -1}px`;
  } else {
    // With position: sticky, this allows the header to normally when you scroll to the top of the page
    // But scroll out of view and stay there until you scroll up.
    header.style.top = `${header.offsetHeight * -1}px`;
  }
}

function showStickyHeader() {
  adjustStickyHeaderDisplay('show');
}

function allowStickyHeaderToHide() {
  adjustStickyHeaderDisplay('hide');
}

module.exports = function () {
  /**
   * Homepage Dots dynamic position
   */
  function homepagedots() {
    setTimeout(function () {
      if ($(window).width() <= 1023.99) {
        var h = $('.hero-banner').find('.content-image').height() - 30;
        $('.hero-banner').find('.slick-dots').css('top', h);
      }
    }, 100);
  }

  /**
   * Terms and condition pop up
   */
  function termclose() {
    $('.terms-overlay').remove();
    $('.terms-overlay').removeClass('show');
    $('.term-condition-section').addClass('hide');
  }

  $('body').on('click touchstart', '.terms-overlay', function () {
    termclose();
  });

  $('body').on('click', '.terms-condition', function (e) {
    e.preventDefault();
    $('body').append('<div class="terms-overlay"></div>');
    $('.terms-overlay').addClass('show');
    if ($(this).hasClass('bannerModal')) {
      var data = $(this).closest('.banner-asset').find('.term-condition-section').clone();
      $('.custom-modal-container.term-condition-section').html($(data).html()).removeClass('hide');
    } else {
      $('.term-condition-section').removeClass('hide');
    }
  });

  $('body').on('click', '#terms-condition-close, .term-condition-section .continue-shop', function (e) {
    e.preventDefault();
    termclose();
  });

  /**
   * View Details section Pop up
   */
  function detailsClose() {
    $('.view-details-overlay').remove();
    $('body').find('div.view-details-popup-main').remove();
  }

  $('body').on('click', '.view-details-home', function (e) {
    e.preventDefault();
    $('body').append('<div class="view-details-popup-main"><div class="view-details-overlay"></div></div>');
    $('body')
      .find('div.view-details-popup-main')
      .append($('.' + $(this).attr('id')).html());
  });

  $('body').on('click', '.view-details-popup-main .view-details-overlay', function () {
    detailsClose();
  });

  $('body').on('click', '.view-details-popup-main #view-detail-close', function () {
    detailsClose();
  });

  /**
   * View Details section Pop up
   */
  function detailsClose() {
    $('.view-details-overlay').remove();
    $('body').find('div.view-details-popup-main').remove();
  }

  $('body').on('click', '.view-details-home', function (e) {
    e.preventDefault();
    $('body').append('<div class="view-details-popup-main"><div class="view-details-overlay"></div></div>');
    $('body')
      .find('div.view-details-popup-main')
      .append($('.' + $(this).attr('id')).html());
  });

  $('body').on('click', '.view-details-popup-main .view-details-overlay', function () {
    detailsClose();
  });

  $('body').on('click', '.view-details-popup-main #view-detail-close', function () {
    detailsClose();
  });

  // Listen to the page:scroll events to show and hide the header.

  if (!isPrefCenter()) {
    $(document).on('page:scrollDown', allowStickyHeaderToHide).on('page:scrollUp', showStickyHeader);
  } else {
    $(document).on('page:scrollDown', allowStickyHeaderToHide);
  }

  // Make sure to show the header when a item is added to the cart.
  $('body').on('product:afterAddToCart', showStickyHeader);

  // Set up the header display before a user scrolls.
  allowStickyHeaderToHide();

  // Watch scrolling
  $(window).scroll(function () {
    const footerUtilitySection = $('.footer-top-utility-section');

    if (footerUtilitySection.length) {
      if (window.innerHeight + window.scrollY > footerUtilitySection.offset().top) {
        if (!$('#promo-drawer').hasClass('promo-drawer-open')) {
          $('#drawer-tab').removeClass('ready');
        }
      } else {
        $('#drawer-tab').addClass('ready');
      }
    }
  });
  $(window).resize(function () {
    homepagedots();
  });
  if ($('#banner-content').find('.banner-asset').length > 1) {
    $('#banner-content, #banner-contentmenu').slick({
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000
    });
    var totalSlides = $('#banner-content.slick-initialized .slick-list').find('.slick-slide').length;
    $('#banner-content.slick-initialized').find('.slick-list').attr('aria-live', 'polite');
    $('#banner-content.slick-initialized .slick-list')
      .find('.slick-slide')
      .each(function (i) {
        $(this).attr('aria-label', i + 1 + ' of ' + totalSlides);
      });
  }
  homepagedots();

  var flagLoadInterval = setInterval(pullFlagLoad, 500);

  function pullFlagLoad() {
    if ($('#bfx-cc-wrapper').length > 0) {
      $('.bf-promo-loginHolder').prepend($('#bfx-cc-wrapper'));
      clearInterval(flagLoadInterval);
    }
  }
  // If Couldn't find the flag after 10 Sec, remove interval
  setTimeout(function () {
    clearInterval(flagLoadInterval);
  }, 10000);

  // Add Flex Break for Hero Banner CTAs
  $('.hero-banner .content-text').each(function () {
    var ctaLen = $(this).find('.cta-link').length;
    if (ctaLen == 6) {
      $(this).find('.cta-link:nth-child(3)').after("<div class='break'></div>");
    }
    if (ctaLen == 9) {
      $(this).find('.cta-link:nth-child(3)').after("<div class='break'></div>");
      $(this).find('.cta-link:nth-child(6)').after("<div class='break'></div>");
    }
  });
  // copy the product count from the search API to the tile template
  if ($('.category-slider .product-count-in-cat').length > 0) {
    $('.category-product-count').html($('.category-slider .product-count-in-cat').attr('attr-product-count'));
  }

  function navigateAccPg(link) {
    if ($(link).next('.popover').hasClass('logged-out-user')) {
      window.location = $(link).next('.popover.logged-out-user').find('.signin-link').attr('href');
    } else if ($(link).next('.popover').hasClass('logged-in-user')) {
      window.location = $(link).next('.popover.logged-in-user').find('.my-account').attr('href');
    }
  }

  const wlcsignin = document.querySelector('.navbar-header .user-links .header-account-drawer-toggle');

  if (wlcsignin) {
    $(wlcsignin).on('click', function () {
      navigateAccPg(wlcsignin);
    });
  }

  //Add BFX event handler for adding the currency
  if ($('.bfx-cc-menu').length > 0) {
    $('.bfx-cc-menu').append("<span class='bfx-cc-code'>" + utilhelper.getCookie('bfx.currency') + '</span>');
  }
  //The call back is to handle the country change using bfx
  utilhelper.eventListener('bfx-contextChooser-loadEnd', function () {
    if ($('.bfx-cc-menu').length > 0) {
      $('.bfx-cc-menu .bfx-cc-code').remove();
      $('.bfx-cc-menu').append("<span class='bfx-cc-code'>" + utilhelper.getCookie('bfx.currency') + '</span>');
    }
  });
};
