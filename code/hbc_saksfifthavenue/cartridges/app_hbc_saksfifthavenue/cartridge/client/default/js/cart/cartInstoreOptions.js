'use strict';
var base = require('core/cart/cartInstoreOptions');
var pdpInstoreInventory = require('../product/pdpInstoreInventory');

/**
 * Update tax totals
 *
 * @param {Object} data - Tax totals data
 */
function updateTaxTotal(data) {
  if (data.totals.canadaTaxation && data.totals.canadaTaxation.PST) {
    $('.tax-PST-total').empty().append(data.totals.canadaTaxation.PST);
    $('.tax-pst').removeClass('d-none');
  } else if (!$('.tax-pst').hasClass('d-none')) {
    $('.tax-pst').addClass('d-none');
  }

  if (data.totals.canadaTaxation && data.totals.canadaTaxation['GST/HST']) {
    $('.tax-GST-total').empty().append(data.totals.canadaTaxation['GST/HST']);
    $('.tax-gst').removeClass('d-none');
  } else if (!$('.tax-gst').hasClass('d-none')) {
    $('.tax-gst').addClass('d-none');
  }

  if (data.totals.canadaTaxation && data.totals.canadaTaxation.QST) {
    $('.tax-QST-total').empty().append(data.totals.canadaTaxation.QST);
    $('.tax-qst').removeClass('d-none');
  } else if (!$('.tax-qst').hasClass('d-none')) {
    $('.tax-qst').addClass('d-none');
  }

  if (data.totals.canadaTaxation && data.totals.canadaTaxation.RST) {
    $('.tax-RST-total').empty().append(data.totals.canadaTaxation.RST);
    $('.tax-rst').removeClass('d-none');
  } else if (!$('.tax-rst').hasClass('d-none')) {
    $('.tax-rst').addClass('d-none');
  }

  if (data.totals.canadaTaxation && data.totals.canadaTaxation.ECO) {
    $('.tax-ECO-total').empty().append(data.totals.canadaTaxation.ECO);
    $('.tax-eco').removeClass('d-none');
  } else if (!$('.tax-eco').hasClass('d-none')) {
    $('.tax-eco').addClass('d-none');
  }

  if (!data.totals.canadaTaxation) {
    $('.tax-total').empty().append(data.totals.totalTax);
    $('.tax-normal').removeClass('d-none');
  } else if (!$('.tax-normal').hasClass('d-none')) {
    $('.tax-normal').addClass('d-none');
  }
}

/**
 * re-renders the order totals and the number of items in the cart
 * @param {Object} data - AJAX response from the server
 */
function updateCartTotals(data) {
  $('.shipping-method-price')
    .empty()
    .append('- ' + data.totals.totalShippingCost);

  updateTaxTotal(data);

  $('.grand-total-sum').empty().append(data.totals.grandTotal);
  $('.grand-total-value').empty().append(data.totals.grandTotalValue);
  $('.sub-total').empty().append(data.totals.subTotal);
  if (data.totals.orderLevelDiscountTotal.value > 0) {
    $('.order-discount').removeClass('hide-order-discount');
    $('.order-discount-total')
      .empty()
      .append('- ' + data.totals.orderLevelDiscountTotal.formatted);
  } else {
    $('.order-discount').addClass('hide-order-discount');
    $('.order-discount-total')
      .empty()
      .append('- ' + data.totals.orderLevelDiscountTotal.formatted);
  }
  if (data.totals.totalShippingCostUnformatted == 0 || data.totals.totalShippingCostUnformatted == data.totals.shippingLevelDiscountTotal.value) {
    $('.shipping-total-cost').empty().append(data.totals.freeShippingText);
    $('.shipping-method-price')
      .empty()
      .append('- ' + data.totals.freeShippingText);
  } else {
    $('.shipping-total-cost').empty().append(data.totals.totalShippingCost);
  }
  if (data.totals.shippingLevelDiscountTotal.value > 0 && data.totals.totalShippingCostUnformatted != data.totals.shippingLevelDiscountTotal.value) {
    $('.shipping-discount').removeClass('hide-shipping-discount');
    $('.shipping-discount-total')
      .empty()
      .append('- ' + data.totals.shippingLevelDiscountTotal.formatted);
  } else {
    $('.shipping-discount').addClass('hide-shipping-discount');
    $('.shipping-discount-total')
      .empty()
      .append('- ' + data.totals.shippingLevelDiscountTotal.formatted);
  }
  // append hidden values for bfx-order discount. This tag excludes the product promo price unlike the above orderLevetotalDiscount
  if (data.totals.orderLevelDiscTotalExc.value > 0) {
    $('.order-discount-exlc-total')
      .empty()
      .append('- ' + data.totals.orderLevelDiscTotalExc.formatted);
  }

  if (data.totals.totalSavings.value > 0) {
    $('.grand-total-saving-container').removeClass('d-none');
    $('.checkout-total-savings').empty().append(data.totals.totalSavings.formatted);
  } else {
    $('.grand-total-saving-container').addClass('d-none');
  }
}

/**
 * triggers change store option for a product matching the UUID parameter
 * @param {sting} pliUUID - item uuid to fetch the precise container
 */
function triggerChangeStore(pliUUID, noInventory) {
  var $container = $('.cart-options[data-product-uuid="' + pliUUID + '"]');
  var shipToContainer = $container
    .find('input[value=instore]')
    .siblings('label[for=instore_' + pliUUID + ']')
    .find('a');
  if (noInventory) {
    shipToContainer.addClass('no-inventory');
  } else {
    shipToContainer.removeClass('no-inventory');
  }
  if (shipToContainer.length) {
    shipToContainer.trigger('click');
  }
}

/**
 * remove the store selector modal when a store is selected out of results
 */
function removeSelectStoreModal() {
  if ($('#inStoreInventoryModal').length > 0) {
    $('#inStoreInventoryModal').modal('hide');
    $('#inStoreInventoryModal').remove();
    $('#inStoreInventoryModal').attr('aria-modal', 'false');
    $('.modal-backdrop').remove();
  }
}

/**
 * Updates the store name in the radio label with the new store selected from modal.
 * @param {Object} data - Contains the store info
 * @param {sting} pliUUID - item uuid to fetch the precise container
 */
function updateStoreContent(data, pliUUID) {
  var $container = $('.cart-options[data-product-uuid="' + pliUUID + '"]');
  var shipToContainer = $container
    .find('input[value=instore]')
    .siblings('label[for=instore_' + pliUUID + ']')
    .find('a');
  shipToContainer.removeClass('no-inventory');
  // content update if store is found
  if (data.storeName && data.storeName !== undefined && data.storeId && data.storeId !== undefined && shipToContainer.length > 0) {
    $container.find('input[value=instore]').attr('data-store-id', data.storeId);
    $container.find('input[value=instore]').removeClass('change-store');
    shipToContainer.html(data.storeName.toLowerCase());
  } else if (data.changeStoreHtml) {
    $container
      .find('input[value=instore]')
      .siblings('label[for=instore_' + pliUUID + ']')
      .html(data.changeStoreHtml);
  }
  if (data.storeName && data.storeName !== undefined && data.storeId && data.storeId !== undefined) {
    $container.find('input[value=instore]').removeClass('change-store');
  }
}

base.changeStore = pdpInstoreInventory.changeStore;
base.searchSDDWithPostal = pdpInstoreInventory.searchSDDWithPostal; // sdd zip search modal
base.selectZipWithInventory = pdpInstoreInventory.selectZipWithInventory; //set inventory
base.toggleShippingOption = function () {
  $('body').on('store:cart', function (e, reqdata) {
    var pliUUID = reqdata.pliUUID;
    var storeId = reqdata.storeId;
    var prodid = reqdata.prodid;
    $.spinner().start();
    var changeShiptoUrl = reqdata.url;
    // form data
    var form = {
      storeId: storeId,
      uuid: pliUUID,
      prodid: prodid
    };
    if (changeShiptoUrl) {
      $.ajax({
        url: changeShiptoUrl,
        data: form,
        success: function (data) {
          var $cartLimitedMsgCont = $('.product-info.uuid-' + pliUUID).find('.limited-inventory.cond-3');
          if (reqdata.savefromModal) {
            if (reqdata.selected === 'instore' && !data.storeId) {
              removeSelectStoreModal();
              triggerChangeStore(pliUUID);
            } else {
              updateStoreContent(data, pliUUID); // if event invoke is from modal, updates store html with radio
              removeSelectStoreModal(); // close modal on successful update
            }
          }
          if (data.limitedInventory && $cartLimitedMsgCont.length > 0) {
            $cartLimitedMsgCont.removeClass('d-none');
          } else {
            $cartLimitedMsgCont.addClass('d-none');
          }
          /*
                    if (data.basketModel.totals.totalShippingCostUnformatted == 0) {
                        $(".shipping-total-cost").empty().append(data.basketModel.totals.freeShippingText);
                    } else {
                        $(".shipping-total-cost").empty().append(data.basketModel.totals.totalShippingCost);
                    } */
          updateCartTotals(data.basketModel);

          setTimeout(function () {
            $.spinner().stop();
          }, 2000);
        },
        error: function () {
          $.spinner().stop();
        }
      });
    }
  });
};
base.updateStoreCart = function () {
  $('body').on('store:changeStore', function (e, data) {
    var pliUUID = data.pliUUID;
    $.spinner().start();
    updateStoreContent(data, pliUUID); // if event invoke is from modal, updates store html with radio
    removeSelectStoreModal(); // close modal on successful update
    $.spinner().stop();
  });
};
module.exports = base;
