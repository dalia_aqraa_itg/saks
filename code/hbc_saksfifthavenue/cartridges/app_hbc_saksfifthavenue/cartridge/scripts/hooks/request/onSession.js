/* eslint-disable no-undef */
'use strict';

/**
 * The onSession hook is called for every new session in a site. This hook can be used for initializations,
 * like to prepare promotions or pricebooks based on source codes or affiliate information in
 * the initial URL. For performance reasons the hook function should be kept short.
 *
 */

var Status = require('dw/system/Status');
var Locale = require('dw/util/Locale');

/**
 * Gets the device type of the current user.
 * @return {string} the device type (desktop, mobile or tablet)
 */
function getDeviceType() {
  var deviceType = 'desktop';
  var iPhoneDevice = 'iPhone';
  var iPadDevice = 'iPad';
  var androidDevice = 'Android'; // Mozilla/5.0 (Linux; U; Android 2.3.4; en-us; ADR6300 Build/GRJ22) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1

  var httpUserAgent = request.httpUserAgent;

  if (!httpUserAgent) {
    return;
  }

  if (httpUserAgent.indexOf(iPhoneDevice) > -1) {
    deviceType = 'mobile';
  } else if (httpUserAgent.indexOf(androidDevice) > -1) {
    if (httpUserAgent.toLowerCase().indexOf('mobile') > -1) {
      deviceType = 'mobile';
    }
  } else if (httpUserAgent.indexOf(iPadDevice) > -1) {
    deviceType = 'tablet';
  }

  // eslint-disable-next-line consistent-return
  return deviceType;
}

/**
 * The onSession hook function
 *
 * @returns {dw/system/Status} status - return status
 */
exports.onSession = function () {
    session.custom.device = getDeviceType();
    var locale = !empty(request.locale) ? request.locale : 'default';
    session.custom.currentLang = !empty(locale) && locale !== 'default' ? Locale.getLocale(locale).language : 'en';
    var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
    var shopPreference = cookiesHelper.read('shopPreference');
    if (shopPreference) {
        session.custom.shopPreference = shopPreference;
    } else {
        var isMensHomeUrl = request.httpPath.indexOf("Home-ShowMens") > -1;
        shopPreference = isMensHomeUrl ? 'men' : 'women';

        session.custom.shopPreference = shopPreference;
        cookiesHelper.createWithoutHttpOnly('shopPreference', shopPreference);
    }

    var homeStore = cookiesHelper.read('homeStore');
    if (homeStore && !session.custom.homeStore) {
    	session.custom.homeStore = homeStore;
    }
    if (session.custom.homeStore && !session.custom.currentStore) {
    	session.custom.currentStore = session.custom.homeStore;
    }
    return new Status(Status.OK);
};
