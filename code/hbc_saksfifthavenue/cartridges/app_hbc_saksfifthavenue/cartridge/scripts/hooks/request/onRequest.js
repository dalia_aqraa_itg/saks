/* eslint-disable no-undef */
'use strict';

var Status = require('dw/system/Status');
var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');

/**
 * The onRequest hook function
 *
 * @returns {dw/system/Status} status - return status
 */
exports.onRequest = function () {
  //Changes for SFSX-1504 - https://hbcdigital.atlassian.net/browse/SFSX-1504 - Start
  //logic to handle XSS
  //Check if XSS Check in enabled - Merchant Tools --> Site Preferences --> Custom Site Preference Groups --> XSS Input Validation Config -- Enable XSS Input Validation
  var enableXSS =
    Site.current.allowedLocales.length && !empty(Site.current.getCustomPreferenceValue('EnableXSSInputValidation'))
      ? Site.current.getCustomPreferenceValue('EnableXSSInputValidation')
      : false;
  if (enableXSS) {
    //Trigger the xssHelpers check method if XSS Check is enabled.
    var checkCrossSiteScript = require('*/cartridge/scripts/helpers/xssHelpers').check();
    if (checkCrossSiteScript) {
      //If XSS material found in query string forward to Home-ErrorNotFound page.
      response.redirect(URLUtils.url('Home-ErrorNotFound'));
      return new Status(Status.ERROR);
    }
  }
  //Changes for SFSX-1504 - https://hbcdigital.atlassian.net/browse/SFSX-1504 - end
  if (request.httpParameterMap.site_refer) {
    var CommissionCookiesHelper = require("../../helpers/commissionCookies");
    CommissionCookiesHelper.addCommissionCookies(request);
  }

  return new Status(Status.OK);
};
