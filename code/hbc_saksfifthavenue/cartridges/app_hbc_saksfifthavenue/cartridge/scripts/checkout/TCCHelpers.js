'use strict';

var HBCCards = require('*/cartridge/client/default/js/checkout/HBCCards');
var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');

/**
 *  Returns the 3-digit security code calculated based on the first 26-digits of a TCC for validation.
 * @param {String} inputString - 29-digit TCC number
 * @returns {Boolean} 3-digit security code is a match or not
 */
const HBCTSPTCC_INVALID_INPUT_LENGTH = -1;
const HBCTSPTCC_INPUT_LEN = 26;
const iv = [
  0x36,
  0x35,
  0x37,
  0x34,
  0x38,
  0x33,
  0x39,
  0x32,
  0x30,
  0x31,
  0x31,
  0x33,
  0x35,
  0x37,
  0x39,
  0x30,
  0x38,
  0x36,
  0x34,
  0x32,
  0x33,
  0x39,
  0x32,
  0x30,
  0x31,
  0x36,
  0x35,
  0x37,
  0x34,
  0x38,
  0x30,
  0x38,
  0x36,
  0x34,
  0x39,
  0x33,
  0x35,
  0x39,
  0x31,
  0x36
];

function FoldInput(input, maxValue) {
  var v = 0;
  var input = input.toString();
  var len = input.length;

  for (var i = 0; i < len; i++) {
    v += parseInt(input.substr(i, 1));
  }

  if (v > maxValue) {
    v = FoldInput(v, maxValue);
  }
  return v;
}

function CalculateChunks(input, chunkSize) {
  var v = 0;
  var len = input.length;
  for (var i = 0; i < len; i += chunkSize) {
    if (i + chunkSize > len) {
      v += parseInt(input.substr(i));
    } else {
      var tmp = input.substr(i, chunkSize);
      v += parseInt(tmp);
    }
  }
  return v % 1000;
}

function array_values(array) {
  return array.filter(Boolean);
}

function MaskString(input) {
  var maskedResult = '';
  for (var i = 0; i < HBCTSPTCC_INPUT_LEN; i++) {
    var offset = parseInt(input.substr(i, 1)) + i;
    maskedResult = maskedResult + String.fromCharCode(array_values(iv)[offset]);
  }
  return maskedResult;
}

function ShiftString(input, shift) {
  var returnString = input.substr(shift);

  if (shift === 0) {
    return returnString;
  }
  returnString = returnString + input.substr(0, shift);
  return returnString;
}

function Calc(input) {
  var len = input.length;
  if (len < HBCTSPTCC_INPUT_LEN) {
    return HBCTSPTCC_INVALID_INPUT_LENGTH;
  }

  var shift = FoldInput(input, HBCTSPTCC_INPUT_LEN - 1);
  var masked = MaskString(input);
  var smsString = ShiftString(masked, shift);
  var secCode = CalculateChunks(smsString, 3);
  return secCode;
}
/**
 * Returns true if the card number is expired
 * @param {String} cardNumber a 29 digit string containing the TCC expiration date
 */
function isTCCDateExpired(cardNumber) {
  var expDate = getExpirationDate(cardNumber);
  var today = new Date();

  return Date.parse(today) > Date.parse(expDate) ? true : false;
}

function getExpirationDate(cardNumber) {
  // grab the TCC Exp Date from the 29-digit TCC
  var cardNumberExpDate = cardNumber.substr(20, 6); // example: 190301

  // Parsing of date strings with the Date constructor (and Date.parse, they are
  // equivalent) is strongly discouraged due to browser differences and inconsistencies.
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date
  var year = parseInt(cardNumberExpDate.substr(0, 2)) + 2000;
  var month = parseInt(cardNumberExpDate.substr(2, 2)) - 1; // the month is 0-indexed
  var day = parseInt(cardNumberExpDate.substr(4, 2));

  return new Date(year, month, day);
}

var validateSecurityCode = function (inputString) {
  if (!tsysHelper.isTsysMode()) {
    if (isTCCDateExpired(inputString) === true) {
      return false;
    }
  }
  // if the TCC expiration date is NOT expired, proceed to check the security code

  // grab 3-digit security code from the 29-digit TCC
  var inputStringSecCode = inputString.substr(26, 29);

  // grab first 26-digits from the 29-digit TCC
  var inputString26 = inputString.substr(0, 26);

  var secCode = Calc(inputString26);

  // if inputString26 is less than 26 digits return false
  if (secCode < 0) {
    return false;
  }

  // if there is a match it is valid so return true
  if (parseInt(secCode) === parseInt(inputStringSecCode)) {
    return true;
  }

  // else return false, invalid
  return false;
};

/**
 * Returns the card type of the TCC token (first 16 digits)
 * @param {String} cardNumber a 29 digit string
 */
function getTCCTokenType(cardNumber) {
  var tccType = HBCCards.cardType(cardNumber.substr(0, 16));
  return tccType.name || false;
}

/**
 * Returns the card label of the TCC
 * @param {String} cardNumber a 29 digit string
 */
function getTCCTokenLabel(cardNumber) {
  var tccType = HBCCards.cardType(cardNumber);
  return tccType.label || false;
}

module.exports = {
  validateSecurityCode: validateSecurityCode,
  isTCCDateExpired: isTCCDateExpired,
  getExpirationDate: getExpirationDate,
  getTCCTokenType: getTCCTokenType,
  getTCCTokenLabel: getTCCTokenLabel,
  Calc: Calc
};
