/* eslint-disable guard-for-in */
'use strict';

var base = module.superModule;

var server = require('server');

var HashMap = require('dw/util/HashMap');
var Logger = require('dw/system/Logger');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var collections = require('*/cartridge/scripts/util/collections');
var Money = require('dw/value/Money');
var priceHelper = require('*/cartridge/scripts/helpers/pricing');
var Resource = require('dw/web/Resource');
var preorderHelper = require('*/cartridge/scripts/helpers/preorderHelper');
var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');

var taxLogging = Logger.getLogger('TaxLogging', 'TaxLogging');

/**
 * sets the gift message on a shipment
 * @param {dw.order.Shipment} shipment - Any shipment for the current basket
 * @param {boolean} isGift - is the shipment a gift
 * @param {string} giftMessage - The gift message the user wants to attach to the shipment
 * @param {string} giftRecipientName - The gift giftRecipientName the user wants to attach to the shipment
 * @param {string} deliveryInstructions - for SDD text provided
 * @returns {Object} object containing error information
 */
base.setGift = function (shipment, isGift, giftMessage, giftRecipientName, giftWrapType, deliveryInstructions) {
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();
  var shipments = currentBasket.shipments;
  var result = {
    error: false,
    errorMessage: null
  };
  try {
    Transaction.wrap(function () {
      // set the properties for all shipments for SFSX-2784
      for (var i = 0; i < shipments.length; i++) {
        var indShipment = shipments[i];
        indShipment.setGift(isGift);

        if (isGift && giftRecipientName) {
          indShipment.setGiftMessage(giftMessage);
          indShipment.custom.giftRecipientName = giftRecipientName; // eslint-disable-line
          indShipment.custom.giftWrapType = giftWrapType;
        } else {
          indShipment.setGiftMessage(null);
          indShipment.custom.giftRecipientName = null; // eslint-disable-line
          indShipment.custom.giftWrapType = null;
        }
        if (deliveryInstructions) {
          indShipment.custom.deliveryInstructions = deliveryInstructions.replace(/(!+)|(\?+)/g, '.');
        } else {
          indShipment.custom.deliveryInstructions = null;
        }
      }
    });
  } catch (e) {
    result.error = true;
    result.errorMessage = Resource.msg('error.message.could.not.be.attached', 'checkout', null);
  }

  return result;
};

/**
 * sets the shipping and billing address to basket with only SDD postal
 * @param {dw.order.Shipment} shipment - Any shipment for the current basket
 * @param {boolean} isGift - is the shipment a gift
 * @param {dw.order.Basket} object - current basket
 * @param {Boolean} noValidateAddress - to decide on the address has to applied or not
 * @returns {Boolean} returns if address is applied
 */
base.setSDDPostalToBasket = function (currentBasket, noValidateAddress) {
  var preferences = require('*/cartridge/config/preferences');
  var Logger = require('dw/system/Logger');
  var sddApplied = false;
  var defaultShipment = currentBasket && currentBasket.defaultShipment ? currentBasket.defaultShipment : null;
  try {
    var address = {},
      countryCode = {};
    if (
      preferences.isEnabledforSameDayDelivery &&
      session.custom.sddpostal &&
      defaultShipment &&
      (noValidateAddress || !defaultShipment.shippingAddress || (!defaultShipment.shippingAddress.address1 && !defaultShipment.shippingAddress.city))
    ) {
      address.firstName = '';
      address.lastName = '';
      address.address1 = '';
      address.address2 = '';
      address.city = '';
      address.postalCode = session.custom.sddpostal;
      address.stateCode = '';
      address.phone = '';
      countryCode.value = 'US';
      address.countryCode = countryCode;
      base.copyCustomerAddressToShipment(address);
      base.copyCustomerAddressToBilling(address);
      sddApplied = true;
    }
  } catch (e) {
    Logger.error('Error executiong setSDDPostalToBasket.js ' + e.msg);
  }
  return sddApplied;
};

//Method to fetch the List Price from Reg pricebook
// eslint-disable-next-line require-jsdoc
function getItemListPrice(priceModel) {
  var price = Money.NOT_AVAILABLE;
  var priceBook;
  var priceBookPrice;

  if (priceModel.price.valueOrNull === null && priceModel.minPrice) {
    return priceModel.minPrice;
  }

  priceBook = priceHelper.getRootPriceBook(priceModel.priceInfo.priceBook);
  priceBookPrice = priceModel.getPriceBookPrice(priceBook.ID);

  if (priceBookPrice.available) {
    return priceBookPrice;
  }

  price = priceModel.price.available ? priceModel.price : priceModel.minPrice;

  return price;
}

/** Promotion attached to each of this price adjustment. Any promotion except Associate and FDD will be added to the custom attribute "productpromotiondiscount"
 *  productpromotiondiscount = Pricebook promotion discount + All product promotion Discounts + All order promotion discount except Associate and FDD promotion
 *  associatediscount = Only associate promotion discount
 *  fdddiscount = only FDD promotion discount
 *  If the promotion is of type Associate, save it to "associatediscount" custom attribute
 *  If the promotion is of type FDD, save it to "fdddiscount" custom attribute
 *  @param {dw.order.Basket} currentBasket - Basket dw object
 *  @param {Customer} customer  - customer dw object
 */
// eslint-disable-next-line no-unused-vars
base.prorateLineItemDiscounts = function (currentBasket, _customer) {
  var productLineItems = currentBasket.getProductLineItems();
  var promotionMap = new HashMap();  
  var promoMapObj = {};  
  var isGoodDivisible = true;
  collections.forEach(productLineItems, function (lineItem) {
    Transaction.wrap(function () {
      var pricebookDiscount = new Money(0, currentBasket.getCurrencyCode());
      var productPromotionDiscountWOPricebook = new Money(0, currentBasket.getCurrencyCode());
      var associateDiscount = new Money(0, currentBasket.getCurrencyCode());
      var specialVendorDiscount = new Money(0, currentBasket.getCurrencyCode());
      var fddDiscount = new Money(0, currentBasket.getCurrencyCode());
      // eslint-disable-next-line no-param-reassign
      delete lineItem.custom.proratedPriceAdjustment;
      // eslint-disable-next-line no-param-reassign
      delete lineItem.custom.productPromotionDiscount;
      // eslint-disable-next-line no-param-reassign
      delete lineItem.custom.productPromotionAssociateDiscount;
      // eslint-disable-next-line no-param-reassign
      delete lineItem.custom.productPromotionSpecialVendorDiscount;
      // eslint-disable-next-line no-param-reassign
      delete lineItem.custom.productPromotionFirstDayDiscount;
      delete lineItem.custom.productPromotionDiscountWOPricebook;
      var proratePricesMap = lineItem.getProratedPriceAdjustmentPrices();
      var priceModel = lineItem.product.priceModel;
      var priceBook = priceModel.priceInfo.priceBook;
      var listPrice = new Money(getItemListPrice(priceModel) * lineItem.quantity.value, currentBasket.getCurrencyCode());

      if (priceBook && 'isPromotionPriceBook' in priceBook.custom && priceBook.custom.isPromotionPriceBook) {
        pricebookDiscount = listPrice.subtract(lineItem.getPrice());
        lineItem.custom.isPromotionPriceBook = true;
      } else {
        lineItem.custom.isPromotionPriceBook = false;
      }
      lineItem.custom.retailprice = listPrice.value;

      if (proratePricesMap.length > 0) {
        var priceAdjKeys = proratePricesMap.keySet();
        var proratedPrice;
        collections.forEach(priceAdjKeys, function (priceAdjKey) {
          var promotion = priceAdjKey.promotion;
          var promoItemsObj = {};
          if (priceAdjKey.promotionID !== 'GiftOptions' && proratePricesMap.get(priceAdjKey) && proratePricesMap.get(priceAdjKey).value) {
            proratedPrice = new Money(Math.abs(proratePricesMap.get(priceAdjKey).value), currentBasket.getCurrencyCode());

			/* If proratedPrice is not empty then we will add the item for promotion into promotionMap as an object of data needed 
			 * later to go through and distribute pennies across each item so prorated price is divisible by quantity.
	 		 */
            try {
	            if (!empty(proratedPrice)) {
                var isGood = isEvenlyDivisible(proratedPrice, lineItem.quantity.value);
	            	if (!isGood) {
	            		taxLogging.warn("checkoutHelpers.js: promo ID - {0} | promotion Class - {1} | product ID - {2} | qty - {3} | proratedPrice - {4}", priceAdjKey.promotionID, promotion.promotionClass, lineItem.product.ID, lineItem.quantity.value, proratedPrice);
	            	}
	            	
	            	if (promotion.promotionClass.toLowerCase() == 'order') {
	            		if (promotionMap.containsKey(priceAdjKey.promotionID)) {
		            		var currentPromo = promotionMap.get(priceAdjKey.promotionID);
		            		promoItemsObj = currentPromo.items;            		
		            	} 
		            	
		            	promoItemsObj[lineItem.product.ID] = {
		            		good: isGood,
		            		quantity: lineItem.quantity.value,
		            		origProratedPrice: proratedPrice,
		            		proratedPrice: proratedPrice,
		            		price: getItemListPrice(priceModel),
		            		adjustment: new Money(0, currentBasket.getCurrencyCode())          		
						};
		            	
		            	if (isGood == false) {
		            		isGoodDivisible = false;       		
		            	}
		            	
		            	promoMapObj = {
		          			good: isGoodDivisible,
		          			items: promoItemsObj
		          		};      
		                    
		            	promotionMap.put(priceAdjKey.promotionID, promoMapObj);
	            	}
	            }	            
            } catch(e) {
            	taxLogging.error('checkoutHelpers.js: There was an error while building the promotionMap - {0}', e.message);
            }

            if (!empty(promotion) && promotion.custom.promotionType.value === 'Associate') {
              associateDiscount = associateDiscount.add(proratedPrice);
            } else if (!empty(promotion) && promotion.custom.promotionType.value === 'SpecialVendorDisc') {
              specialVendorDiscount = specialVendorDiscount.add(proratedPrice);
            } else if (!empty(promotion) && promotion.custom.promotionType.value === 'FDD') {
              fddDiscount = fddDiscount.add(proratedPrice);
            } else {
              pricebookDiscount = pricebookDiscount.add(proratedPrice);
              productPromotionDiscountWOPricebook = productPromotionDiscountWOPricebook.add(proratedPrice);
            }
                        
          }
        });       
      }
      // eslint-disable-next-line no-param-reassign
      if (productPromotionDiscountWOPricebook.value > 0) lineItem.custom.productPromotionDiscountWOPricebook = productPromotionDiscountWOPricebook.value;
      // eslint-disable-next-line no-param-reassign
      if (pricebookDiscount.value > 0) lineItem.custom.productPromotionDiscount = pricebookDiscount.value;
      // eslint-disable-next-line no-param-reassign
      if (associateDiscount.value > 0) lineItem.custom.productPromotionAssociateDiscount = associateDiscount.value;
      // eslint-disable-next-line no-param-reassign
      if (specialVendorDiscount.value > 0) lineItem.custom.productPromotionSpecialVendorDiscount = specialVendorDiscount.value;
      // eslint-disable-next-line no-param-reassign
      if (fddDiscount.value > 0) lineItem.custom.productPromotionFirstDayDiscount = fddDiscount.value;
      // eslint-disable-next-line no-param-reassign
      lineItem.custom.proratedPriceAdjustment = 0;      
    });
  });
   
  calculatePennyDistribution(promotionMap, currentBasket);

};

function isEvenlyDivisible(moneyVal, quantity) {
  return (((moneyVal/quantity).toFixed(2))*quantity == moneyVal);
}

function updatePromotionalDiscountAttributes(currentBasket, liCustomAttributes, pennyAdjustment) {
	try {
		var promoAdjustment = new Money(0, currentBasket.getCurrencyCode());
		
		//Adding newly adjusted penny amount to custom attribute.	
		if (!empty(liCustomAttributes.proratedPriceAdjustment)) {
			promoAdjustment = new Money(liCustomAttributes.proratedPriceAdjustment, currentBasket.getCurrencyCode()).add(pennyAdjustment);
			liCustomAttributes.proratedPriceAdjustment = promoAdjustment.value;
		}		
		
		//Updating any other custom attributes with pennyAdjustment for promo prices that may have been added to lineItem.
		if (!empty(liCustomAttributes.productPromotionAssociateDiscount)) {
			promoAdjustment = new Money(liCustomAttributes.productPromotionAssociateDiscount, currentBasket.getCurrencyCode()).add(pennyAdjustment);
			liCustomAttributes.productPromotionAssociateDiscount = promoAdjustment.value;
		}
		if (!empty(liCustomAttributes.productPromotionSpecialVendorDiscount)) {
			promoAdjustment = new Money(liCustomAttributes.productPromotionSpecialVendorDiscount, currentBasket.getCurrencyCode()).add(pennyAdjustment);
			liCustomAttributes.productPromotionSpecialVendorDiscount = promoAdjustment.value;						  
		}
		if (!empty(liCustomAttributes.productPromotionFirstDayDiscount)) {
			promoAdjustment = new Money(liCustomAttributes.productPromotionFirstDayDiscount, currentBasket.getCurrencyCode()).add(pennyAdjustment);
			liCustomAttributes.productPromotionFirstDayDiscount = promoAdjustment.value;						  
		} 
		if (!empty(liCustomAttributes.productPromotionDiscount)) {
			promoAdjustment = new Money(liCustomAttributes.productPromotionDiscount, currentBasket.getCurrencyCode()).add(pennyAdjustment);
			liCustomAttributes.productPromotionDiscount = promoAdjustment.value; 
		}
		if (!empty(liCustomAttributes.productPromotionDiscountWOPricebook)) {
			promoAdjustment = new Money(liCustomAttributes.productPromotionDiscountWOPricebook, currentBasket.getCurrencyCode()).add(pennyAdjustment);
			liCustomAttributes.productPromotionDiscountWOPricebook = promoAdjustment.value;
		}	
	} catch(e) {
		taxLogging.error('checkoutHelpers.js: There was an error in updatePromotionalDiscountAttributes - {0}', e.message);
	}
}

/* Description:
 * Pass in promoMap that has been created and distribute any pennies across lineitems for each promotion to make proratedPrice evenly divisible by the quantity. 
 * */
function calculatePennyDistribution(promoMap, currentBasket) {
	try {
		var MultipleQtyMap = new HashMap();
		var SingleQtyMap = new HashMap();
		var multiQtyMapObj = {};
		var singleQtyMapObj = {};
		var promotionMapKeys = promoMap.keySet(); //Get keys from promoMap
		var pennyBucket = new Money(0, currentBasket.getCurrencyCode());
		
		/* Loop through the keys from promoMap and either remove promos that are marked as good = true or place items from promotion into MultipleQtyMap or 
		*  SingleQtyMap depending on the quantity size of each product lineitem.
		*/
		Object.keys(promotionMapKeys).forEach(function (key) {
			Transaction.wrap(function () {
		  		var currentMappedPromo = promoMap.get(promotionMapKeys[key]);
		  		var isPromoGood = currentMappedPromo.good;
		  		if (isPromoGood == true) {
			  		promoMap.remove(promotionMapKeys[key]);
		  		} else {
			  		var itemKeys = Object.keys(currentMappedPromo.items);
			  		var multiQtyItemsObj = {};
			  		var singleQtyItemsObj = {};
			  		
			  		//Loop through each item in promotion and place it into either singleQtyMapObj or multiQtyMapObj depending on the quantity.
			  		itemKeys.forEach(function(itemKey) {			  
				  		var currentItem = currentMappedPromo.items[itemKey];
				  		if (currentMappedPromo.items[itemKey].quantity == 1) {
				  
					  		singleQtyItemsObj[itemKey] = currentMappedPromo.items[itemKey];
		
					  		singleQtyMapObj = {
					  			items: singleQtyItemsObj
					  		};				  
					  				
						} else if (currentMappedPromo.items[itemKey].quantity > 1) {
					
							multiQtyItemsObj[itemKey] = currentMappedPromo.items[itemKey];
							
							multiQtyMapObj = {
								items: multiQtyItemsObj
							};
						}
					});
			  
			  		var multiQtyItems = {};
			  		var pennyAdjustment = new Money(0, currentBasket.getCurrencyCode());		  
			  		
			  		//If we have an item in multiQtyMapObj or singleQtyMapObj then put it into the correct Map.
			  		if (Object.keys(multiQtyMapObj).length > 0) {
			  			MultipleQtyMap.put(promotionMapKeys[key], multiQtyMapObj);
			  			multiQtyItems = MultipleQtyMap[promotionMapKeys[key]].items;
			  		}
			  		if (Object.keys(singleQtyMapObj).length > 0) {
			  			SingleQtyMap.put(promotionMapKeys[key], singleQtyMapObj);
			  		}
			  		
			  		/* Loop through MultipleQtyMap and if we have items in SingleQtyMap too then begin adding/subtracting pennies through all items not evenly 
			  		*  divisible by quantity. pennyBucket will be adjusted as adding/subtracting pennies.
			  		*/
			  		if (MultipleQtyMap.size() > 0 && !(SingleQtyMap.size() > 0)) {			  			
			  			taxLogging.warn("MultiQtyMap has items but SingleQtyMap is empty!  No penny distribution is currently being done. Promotion ID: {0}", promotionMapKeys[key]);
			  			/*
			  			Object.keys(multiQtyItems).forEach(function(itemKey) {
			  				var pli = currentBasket.getProductLineItems(itemKey);
			  				var originalProratedPrice = multiQtyItems[itemKey].proratedPrice;
			  					
			  				taxLogging.warn("itemNo: {0}, originalProratedPrice: {1}, quantity: {2}", itemKey, originalProratedPrice.value, multiQtyItems[itemKey].quantity);
						});
						*/
		  			} else if (MultipleQtyMap.size() > 0) {
						Object.keys(multiQtyItems).forEach(function(itemKey) {
							var pli = currentBasket.getProductLineItems(itemKey);
							var originalProratedPrice = multiQtyItems[itemKey].proratedPrice;
							
							if (!multiQtyItems[itemKey].good) {		  			 
								var currentQty = multiQtyItems[itemKey].quantity;
								var newProratedPrice = new Money(0, currentBasket.getCurrencyCode());
								
								if (pennyBucket.value <= 0) {
									newProratedPrice = Money(Math.ceil(originalProratedPrice.multiply(100).divide(currentQty).decimalValue),currentBasket.getCurrencyCode()).multiply(currentQty).divide(100);									
								} else {
									newProratedPrice = Money(Math.floor(originalProratedPrice.multiply(100).divide(currentQty).decimalValue),currentBasket.getCurrencyCode()).multiply(currentQty).divide(100);									
								}
								
								pennyAdjustment = newProratedPrice.subtract(originalProratedPrice);
								pennyBucket = pennyBucket.add(pennyAdjustment);
								
								taxLogging.warn("ADJUSTING price for: {0}, original prorated Price: {1}, new prorated Price: {2}, adjusted amount: {3}", itemKey, originalProratedPrice.value, newProratedPrice.value, pennyAdjustment);
								multiQtyItems[itemKey].proratedPrice = newProratedPrice;
								multiQtyItems[itemKey].adjustment = pennyAdjustment;								
							} else {
								multiQtyItems[itemKey].proratedPrice = originalProratedPrice;
								multiQtyItems[itemKey].adjustment = Money(0, currentBasket.getCurrencyCode());	
							}
							
							updatePromotionalDiscountAttributes(currentBasket, pli[0].custom, multiQtyItems[itemKey].adjustment);
						});
					}					
					//taxLogging.warn("CURRENT PENNY BUCKET BEFORE SINGLE QTY DISTRIBUTION: {0}", pennyBucket.value);
					if (SingleQtyMap.size() > 0 && pennyBucket.value != 0) {
						var singleQtyItems = SingleQtyMap[promotionMapKeys[key]].items;
						var singleQtyKeys = Object.keys(singleQtyItems);
			  			var itemCnt = singleQtyKeys.length;
			  			var cnt = 0;			  
			  
			  			if (itemCnt == 1) {
				  			var originalProratedPrice = singleQtyItems[singleQtyKeys[0]].proratedPrice;
				  			pennyAdjustment = Money(0, currentBasket.getCurrencyCode());
				  
				  			if(pennyBucket.value < 0) {
					  			pennyAdjustment = Money(Math.abs(pennyBucket.value), currentBasket.getCurrencyCode());
				  			} else if (pennyBucket.value > 0) {
					  			pennyAdjustment =  Money(-Math.abs(pennyBucket.value), currentBasket.getCurrencyCode());
				  			}

				  			pennyBucket = Money(0, currentBasket.getCurrencyCode());
				  
				  			newProratedPrice = originalProratedPrice.subtract(pennyAdjustment);				  
				  
				  			taxLogging.warn("ADJUSTING price for: {0}, original prorated Price: {1}, new prorated Price: {2}, adjusted amount: {3}", singleQtyKeys[0], originalProratedPrice.value, newProratedPrice.value, pennyAdjustment.value);
				  
				  			singleQtyItems[singleQtyKeys[0]].proratedPrice = newProratedPrice;
				  			singleQtyItems[singleQtyKeys[0]].adjustment = pennyAdjustment;
				  
				  			var pli = currentBasket.getProductLineItems(singleQtyKeys[0]);
				  
				  			updatePromotionalDiscountAttributes(currentBasket, pli[0].custom, singleQtyItems[singleQtyKeys[0]].adjustment);
					  					  					 
			  			} else if (itemCnt > 1 && pennyBucket.value > 0) {
				  			while (pennyBucket.value > 0) {				  	  
					  			singleQtyItems[singleQtyKeys[cnt]].proratedPrice = singleQtyItems[singleQtyKeys[cnt]].proratedPrice.subtract(new Money(parseFloat(0.01), currentBasket.getCurrencyCode()));
					  			singleQtyItems[singleQtyKeys[cnt]].adjustment = singleQtyItems[singleQtyKeys[cnt]].adjustment.subtract(new Money(parseFloat(0.01), currentBasket.getCurrencyCode()));
					  
					  			var pli = currentBasket.getProductLineItems(singleQtyKeys[cnt]);					  					  			
					  			
					  			updatePromotionalDiscountAttributes(currentBasket, pli[0].custom, new Money(parseFloat(-0.01), currentBasket.getCurrencyCode()));
					  
					  			taxLogging.warn("SUBTRACTING penny from item: {0}, original prorated Price: {1}, new prorated Price: {2}, adjusted amount: {3}", singleQtyKeys[cnt], singleQtyItems[singleQtyKeys[cnt]].origProratedPrice.value, singleQtyItems[singleQtyKeys[cnt]].proratedPrice.value, singleQtyItems[singleQtyKeys[cnt]].adjustment.value);
					  			
					  			if (cnt >= itemCnt-1) {
						  			cnt = 0;
					  			} else {
						  			cnt++;
					  			}					  
					  			
					  			pennyBucket = pennyBucket.subtract(new Money(parseFloat(0.01), currentBasket.getCurrencyCode()));
				  			}
			  			} else if (itemCnt > 1 && pennyBucket.value < 0) {
			  				while (pennyBucket.value < 0) {
			  					singleQtyItems[singleQtyKeys[cnt]].proratedPrice = singleQtyItems[singleQtyKeys[cnt]].proratedPrice.add(new Money(parseFloat(0.01), currentBasket.getCurrencyCode()));
			  					singleQtyItems[singleQtyKeys[cnt]].adjustment = singleQtyItems[singleQtyKeys[cnt]].adjustment.add(new Money(parseFloat(0.01), currentBasket.getCurrencyCode()));
			  					
			  					var pli = currentBasket.getProductLineItems(singleQtyKeys[cnt]);
			  			
					  			updatePromotionalDiscountAttributes(currentBasket, pli[0].custom, new Money(parseFloat(0.01), currentBasket.getCurrencyCode()));
					  			
					  			taxLogging.warn("ADDING penny from item: {0}, original prorated Price: {1}, new prorated Price: {2}, adjusted amount: {3}", singleQtyKeys[cnt], singleQtyItems[singleQtyKeys[cnt]].origProratedPrice.value, singleQtyItems[singleQtyKeys[cnt]].proratedPrice.value, singleQtyItems[singleQtyKeys[cnt]].adjustment.value);
					  			
					  			if (cnt >= itemCnt-1) {
					  				cnt = 0;
					  			} else {
					  				cnt++;
					  			}
					  			
					  			pennyBucket = pennyBucket.add(new Money(parseFloat(0.01), currentBasket.getCurrencyCode()));
					  		}
			  			}
			  			
			  			taxLogging.warn("PENNY BUCKET AFTER SINGLE QTY DISTRIBUTION: {0}", pennyBucket.value);
			  		}
				}
			});
		});
	} catch(e) {
		taxLogging.error('checkoutHelpers.js: There was an error in calculatePennyDistribution - {0}', e.message);
	}
}


/* Applicable for SAKS banner only.
 * Description:
 * Identify if the order is complete preOrder and use the result to update the Authorization amount to 0
 * */
base.getAuthAmountExcludingPreOrderTotal = function (LineItemCtnr) {
  var allLineItems = LineItemCtnr.getProductLineItems();
  var paymentInstruments = LineItemCtnr.getPaymentInstruments();
  var obj = {};
  obj.hasOnePreOrderItem = false;
  obj.completePreOrder = true;
  obj.instockTotal = 0;
  obj.instockTotalWithTax = 0;
  obj.instockOrderTotal = 0;
  collections.forEach(allLineItems, function (item) {
    if ('inventoryStatus' in item.custom && item.custom.inventoryStatus == 'PREORDER') {
      obj.hasOnePreOrderItem = true;
      obj.preOrderTotal += item.getAdjustedNetPrice().value;
    } else {
      obj.completePreOrder = false;
      obj.instockOrderTotal += item.custom.proratedPriceTotal;
    }
  });

  if (obj && obj.hasOnePreOrderItem) {
    var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
    var instockOrderTotal = new Money(obj.instockOrderTotal, LineItemCtnr.getCurrencyCode());
    instockOrderTotal = instockOrderTotal.value;

    Transaction.wrap(function () {
      // eslint-disable-next-line no-param-reassign
      LineItemCtnr.custom.instockOrderTotal = instockOrderTotal;
    });

    collections.forEach(paymentInstruments, function (pi) {
      var paymentAmount = pi.paymentTransaction.amount.value; //  use number and not money
      // changing the above to money would result in NaN error
      if (pi.paymentMethod === ipaConstants.GIFT_CARD) {
        // check to see if the full gift card value is utilized.
        if (instockOrderTotal > paymentAmount) {
          instockOrderTotal = instockOrderTotal - paymentAmount;
        } else if (instockOrderTotal <= paymentAmount) {
          paymentAmount = instockOrderTotal;
          instockOrderTotal = 0;
        }
        // if the new total is negative then set it to zero
        if (paymentAmount <= 0) {
          paymentAmount = 0;
        }

        /**
         *  If the paymentAmount is zero, we remove the GiftCard payment instrument
         *  We should not be doing a authorization for a $0 on GC
         *  If the payment is a non-negative and non-zero then we update the custom attribute to indicate the
         *  new authorization amount.
         *
         *  The custom attribute authAmountExcludingPreOrder is utilized to determine the new auth amount
         *
         *  Reason for not updating the paymentTransaction total is - We have an oms attr
         *   @ExtnTenderMaxLimit' - this attribute is suppose to have the original transaction amount for the pi
         *
         */
        Transaction.wrap(function () {
          // eslint-disable-next-line no-param-reassign
          pi.custom.authAmountExcludingPreOrder = paymentAmount;
        });
      }
    });
    collections.forEach(paymentInstruments, function (pi) {
      if (pi.paymentMethod !== ipaConstants.GIFT_CARD) {
        Transaction.wrap(function () {
          // eslint-disable-next-line no-param-reassign
          pi.custom.authAmountExcludingPreOrder = instockOrderTotal;
        });
      }
    });
  }
  return obj;
};

/**
 * Checks if the order default shipment is eligible for sameday
 * and Order has same day delivery eligible items. It is important to have postal code and store id
 * in the session.
 * All of these creteria determine an order being SDD order
 * @param {*} order : order
 */
base.isSameDayDeliveryOrder = function (order) {
  var isSDD = false;
  if (order) {
    var defaultShipment = order.defaultShipment;
    if (
      defaultShipment &&
      defaultShipment.shippingMethod &&
      defaultShipment.shippingMethod.custom.sddEnabled &&
      'sddShoprunnerToken' in defaultShipment.custom
    ) {
      // check for the session attributes value
      // eslint-disable-next-line no-undef
      if (session.custom.sddpostal && session.custom.sddstoreid) {
        isSDD = true;
      }
    }
  }
  return isSDD;
};

function getCarrierCode(type) {
  try {
    var Site = require('dw/system/Site');
    var pdBasedCarrierCode = 'pdBasedCarrierCode' in Site.current.preferences.custom ? Site.current.preferences.custom.pdBasedCarrierCode : '';
    var ccType = JSON.parse(pdBasedCarrierCode);
    var cardType = ccType[type];
  } catch (e) {
    cardType = 'Overnight';
  }
  return cardType;
}

base.getRegularShipment = function (order) {
  var regularShipment = order.defaultShipment;
  var shipments = order.shipments;
  collections.forEach(shipments, function (shipment) {
    // get STH shipment
    if (!shipment.custom.shipmentType || shipment.custom.shipmentType !== 'instore') {
      regularShipment = shipment;
    }
  });
  return regularShipment;
};

/**
 * Lookup shipping method configured in BM and return the shipping configs for oms
 */
function lookupShippingMethods(pli) {
  var ShippingMgr = require('dw/order/ShippingMgr');
  var methods = ShippingMgr.getAllShippingMethods();
  var omsConfigs = {};
  omsConfigs.carrierServiceCode = '';
  omsConfigs.scac = '';
  omsConfigs.shippingMethod = '';
  var shippingMethod = pli.getShipment().getShippingMethod();
  var product = pli.product;
  var masterProduct = 'pdRestrictedShipTypeText' in product.custom ? product : product.masterProduct;
  var pdRestrictedText = masterProduct && 'pdRestrictedShipTypeText' in masterProduct.custom ? masterProduct.custom.pdRestrictedShipTypeText : null;
  var srEligible = 'shopRunnerEligible' in product.custom && product.custom.shopRunnerEligible == 'true' ? true : false;
  var srloggedCustomer = cookiesHelper.read('sr_token');
  var address = pli.getShipment().shippingAddress;
  var stateCode = address ? address.stateCode : null;
  var shippingMethodDisplayName = shippingMethod.displayName;
  var poBoxUSTerritory = address.address1.toLowerCase().indexOf('po box') > -1 || address.address1.toLowerCase().indexOf('us territory') > -1;
  for (var i = 0; i < methods.length; i++) {
    var method = methods[i];

    // if the shipment was BOPIS 11
    // shippingMethod.custom.storePickupEnabled
    if (shippingMethod && shippingMethod.custom.storePickupEnabled) {
      if (method && method.custom.storePickupEnabled) {
        omsConfigs.carrierServiceCode = 'omsCarrierServiceCode' in method.custom ? method.custom.omsCarrierServiceCode : '';
        omsConfigs.scac = 'omsSCAC' in method.custom ? method.custom.omsSCAC : '';
        omsConfigs.shippingMethod = 'ipaShippingMethod' in method.custom ? method.custom.ipaShippingMethod : '';
        break;
      }
    }

    // if the shipment was SDD 6,7
    else if (shippingMethod && shippingMethod.custom.sddEnabled && pli.custom.sddLineItem) {
      if (
        method &&
        method.custom.sddEnabled &&
        ((shippingMethod.ID.indexOf('shoprunner') > -1 && method.ID.indexOf('shoprunner') > -1) ||
          (shippingMethod.ID.indexOf('shoprunner') == -1 && method.ID.indexOf('shoprunner') == -1))
      ) {
        omsConfigs.carrierServiceCode = 'omsCarrierServiceCode' in method.custom ? method.custom.omsCarrierServiceCode : '';
        omsConfigs.scac = 'omsSCAC' in method.custom ? method.custom.omsSCAC : '';
        omsConfigs.shippingMethod = 'ipaShippingMethod' in method.custom ? method.custom.ipaShippingMethod : '';
        break;
      }
    }

    // If the PD restricted Ship text
    else if (pdRestrictedText) {
      if (method && 'pdRestrictedShipTypeText' in method.custom && method.custom.pdRestrictedShipTypeText == pdRestrictedText) {
        omsConfigs.carrierServiceCode = 'omsCarrierServiceCode' in method.custom ? method.custom.omsCarrierServiceCode : '';
        omsConfigs.scac = 'omsSCAC' in method.custom ? method.custom.omsSCAC : '';
        omsConfigs.shippingMethod = 'ipaShippingMethod' in method.custom ? method.custom.ipaShippingMethod : '';
        break;
      }
    }

    // Alaska and Huwai validated 3
    else if (stateCode && (stateCode == 'HI' || stateCode == 'AK') && shippingMethodDisplayName == 'Standard') {
      if (method && method.ID == 'GROUND_HOME_DELIVERY_AK_HI') {
        omsConfigs.carrierServiceCode = 'omsCarrierServiceCode' in method.custom ? method.custom.omsCarrierServiceCode : '';
        omsConfigs.scac = 'omsSCAC' in method.custom ? method.custom.omsSCAC : '';
        omsConfigs.shippingMethod = 'ipaShippingMethod' in method.custom ? method.custom.ipaShippingMethod : '';
        break;
      }
    }

    // PO Box and us Territory validated
    else if (poBoxUSTerritory) {
      if (method && method.ID == 'USPS_GROUND') {
        omsConfigs.carrierServiceCode = 'omsCarrierServiceCode' in method.custom ? method.custom.omsCarrierServiceCode : '';
        omsConfigs.scac = 'omsSCAC' in method.custom ? method.custom.omsSCAC : '';
        omsConfigs.shippingMethod = 'ipaShippingMethod' in method.custom ? method.custom.ipaShippingMethod : '';
        break;
      }
    }

    // if the shipment was Shoprunner 1
    else if (shippingMethod && shippingMethod.ID == 'shoprunner' && srEligible) {
      if (method && method.ID == 'shoprunner') {
        omsConfigs.carrierServiceCode = 'omsCarrierServiceCode' in method.custom ? method.custom.omsCarrierServiceCode : '';
        omsConfigs.scac = 'omsSCAC' in method.custom ? method.custom.omsSCAC : '';
        omsConfigs.shippingMethod = 'ipaShippingMethod' in method.custom ? method.custom.ipaShippingMethod : '';
        break;
      }
    }

    // If the shipment was Saturday  10
    else if (shippingMethod && shippingMethod.ID == 'SATURDAY_DELIVERY' && pdRestrictedText == null) {
      if (method && method.ID == 'SATURDAY_DELIVERY') {
        omsConfigs.carrierServiceCode = 'omsCarrierServiceCode' in method.custom ? method.custom.omsCarrierServiceCode : '';
        omsConfigs.scac = 'omsSCAC' in method.custom ? method.custom.omsSCAC : '';
        omsConfigs.shippingMethod = 'ipaShippingMethod' in method.custom ? method.custom.ipaShippingMethod : '';
        break;
      }
    }

    // IF the SRlogin member usecase 8
    else if (shippingMethod && shippingMethod.custom.sddEnabled && pdRestrictedText == null && srloggedCustomer) {
      if (method && method.ID == 'FEDEX_2_DAY') {
        omsConfigs.carrierServiceCode = 'omsCarrierServiceCode' in method.custom ? method.custom.omsCarrierServiceCode : '';
        omsConfigs.scac = 'omsSCAC' in method.custom ? method.custom.omsSCAC : '';
        omsConfigs.shippingMethod = 'ipaShippingMethod' in method.custom ? method.custom.ipaShippingMethod : '';
        break;
      }
    }

    // If the non-sr login, non-sdd, non-bopis 9
    else if (shippingMethod && shippingMethod.custom.sddEnabled && pdRestrictedText == null && !srloggedCustomer) {
      if (method && method.ID == 'STANDARD_OVERNIGHT') {
        omsConfigs.carrierServiceCode = 'omsCarrierServiceCode' in method.custom ? method.custom.omsCarrierServiceCode : '';
        omsConfigs.scac = 'omsSCAC' in method.custom ? method.custom.omsSCAC : '';
        omsConfigs.shippingMethod = 'ipaShippingMethod' in method.custom ? method.custom.ipaShippingMethod : '';
        break;
      }
    }

    // default 2,4,5
    else if (shippingMethod && method && method.ID == shippingMethod.ID) {
      omsConfigs.carrierServiceCode = 'omsCarrierServiceCode' in method.custom ? method.custom.omsCarrierServiceCode : '';
      omsConfigs.scac = 'omsSCAC' in method.custom ? method.custom.omsSCAC : '';
      omsConfigs.shippingMethod = 'ipaShippingMethod' in method.custom ? method.custom.ipaShippingMethod : '';
      break;
    }
  }

  if (omsConfigs.shippingMethod == '') {
    omsConfigs.carrierServiceCode = 'omsCarrierServiceCode' in shippingMethod.custom ? shippingMethod.custom.omsCarrierServiceCode : '';
    omsConfigs.scac = 'omsSCAC' in shippingMethod.custom ? shippingMethod.custom.omsSCAC : '';
    omsConfigs.shippingMethod = 'ipaShippingMethod' in shippingMethod.custom ? shippingMethod.custom.ipaShippingMethod : '';
  }
  return omsConfigs;
}

base.updateCarrierCodes = function (order) {
  var plis = order.getProductLineItems();
  collections.forEach(plis, function (pli) {
    Transaction.wrap(function () {
      var omsConfigs = lookupShippingMethods(pli);
      if (omsConfigs) {
        pli.custom.CarrierServiceCode = omsConfigs.carrierServiceCode;
        pli.custom.scac = omsConfigs.scac;
        pli.custom.shippingMethod = omsConfigs.shippingMethod;
      }
    });
  });
};

/**
 * Sets the payment transaction amount
 * @param {dw.order.Basket} currentBasket - The current basket
 * @param {number} cardBalance - Card Balance of the Gift Card
 * @returns {Object} an error object
 */
base.getNonGiftCardAmount = function (currentBasket, cardBalance) {
  var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
  var currencyCode = currentBasket.getCurrencyCode();
  var giftCardsTotal = new Money(0.0, currencyCode);
  var result = {};
  result.error = false;
  try {
    var paymentInstruments = currentBasket.getPaymentInstruments(ipaConstants.GIFT_CARD);
    // add all the gift card applied totals
    collections.forEach(paymentInstruments, function (paymentInstrument) {
      giftCardsTotal = giftCardsTotal.add(paymentInstrument.getPaymentTransaction().getAmount());
    });
    // Gets the order total.
    var orderTotal = currentBasket.totalGrossPrice;

    // Also Check for Saks First GC
    var saksGCpaymentInstruments = currentBasket.getPaymentInstruments(ipaConstants.SAKS_GIFT_CARD);
    // add all the gift card applied totals
    if (saksGCpaymentInstruments) {
      collections.forEach(saksGCpaymentInstruments, function (paymentInstrument) {
        giftCardsTotal = giftCardsTotal.add(paymentInstrument.getPaymentTransaction().getAmount());
      });
    }

    // Calculates the amount to charge for the payment instrument.
    // This is the remaining open order total that must be paid.
    var amountOpen = orderTotal.subtract(giftCardsTotal);

    // return GC balance if basket applicable amount is greater than balance, else return the basket applicable amount
    if (cardBalance) {
      if (amountOpen.value >= cardBalance) {
        result.giftCardAmountToApply = new Money(cardBalance, currencyCode);
        result.amountLeft = false;
      } else {
        result.giftCardAmountToApply = amountOpen;
        result.amountLeft = true;
      }
    } else {
      result.remainingAmount = amountOpen;
      result.amountLeft = false;
      result.orderTotal = currentBasket.totalGrossPrice;
    }
  } catch (e) {
    result.error = true;
  }
  return result;
};

/**
 * recalculates and adjusts payment instruments amounts
 *
 * @param {dw.order.Basket} basket - current basket of customer
 * @returns {Object} result - result
 */
base.calculatePaymentTransaction = function (basket) {
  var paymentInstrs = basket.getPaymentInstruments();
  var currencyCode = basket.getCurrencyCode();
  var paymentInstTotals = new Money(0.0, currencyCode);
  var LinkedHashMap = require('dw/util/LinkedHashMap');
  var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
  var result = {
    error: false
  };
  collections.forEach(paymentInstrs, function (paymentInstrument) {
    paymentInstTotals = paymentInstTotals.add(paymentInstrument.getPaymentTransaction().getAmount());
  });

  try {
    var nonGCPaymentInstrument;
    collections.forEach(paymentInstrs, function (paymentInstrument) {
      if (paymentInstrument.getPaymentMethod() !== ipaConstants.GIFT_CARD && paymentInstrument.getPaymentMethod() !== ipaConstants.SAKS_GIFT_CARD) {
        nonGCPaymentInstrument = paymentInstrument;
      }
    });

    var gcPaymentInstrs = basket.getPaymentInstruments(ipaConstants.GIFT_CARD);
    var giftCardsTotal = new Money(0.0, currencyCode);
    var orderTotal = basket.totalGrossPrice;
    if (!gcPaymentInstrs.empty) {
      /**
       * Adjusts gift cards payment instruments after applying coupon(s), because this changes the order total.
       * Removes and then adds currently added gift cards to reflect order total changes.
       */
      var gcPIMap = new LinkedHashMap();
      var i = 0;

      collections.forEach(gcPaymentInstrs, function (gcPaymentInstrument) {
        gcPIMap.put('gcNum' + i, gcPaymentInstrument.custom.giftCardNumber);
        gcPIMap.put('gcPin' + i, gcPaymentInstrument.custom.giftCardPin);
        gcPIMap.put('gcType' + i, gcPaymentInstrument.custom.giftCardType);
        gcPIMap.put('gcBal' + i, gcPaymentInstrument.paymentTransaction.getAmount().value);
        gcPIMap.put('gcCheck' + i, gcPaymentInstrument.custom.giftBalanceLeft);
        i++;
      });
      // remove Gift Card Payment Instrument to make sure we have right GC with the right amount
      collections.forEach(gcPaymentInstrs, function (item) {
        basket.removePaymentInstrument(item);
      });

      // Map is our reference for the GC to be recreated. We need to create new GC Payment to avoid difference in the amount applied vs order total
      // the previous applied amount would be the threshold amount that can be applied and hence treated as the balance.
      // if the giftCardAmountToApply is zero then we skip the GC to be applied
      var gcPaymentToCheck;
      var total;
      for (i = 0; i < gcPaymentInstrs.length; i++) {
        total = base.getNonGiftCardAmount(basket, gcPIMap.get('gcBal' + i));
        if (!gcPIMap.get('gcCheck' + i) || total.amountLeft) {
          if (total.giftCardAmountToApply.value > 0) {
            Transaction.wrap(function () {
              let paymentInstrument = basket.createPaymentInstrument(ipaConstants.GIFT_CARD, total.giftCardAmountToApply);
              paymentInstrument.custom.giftCardNumber = gcPIMap.get('gcNum' + i);
              paymentInstrument.custom.giftCardPin = gcPIMap.get('gcPin' + i);
              paymentInstrument.custom.giftCardType = gcPIMap.get('gcType' + i);
              giftCardsTotal = giftCardsTotal.add(paymentInstrument.getPaymentTransaction().getAmount());
              paymentInstrument.custom.giftBalanceLeft = total.amountLeft;
            });
          }
        } else {
          gcPaymentToCheck = {
            gcNum: gcPIMap.get('gcNum' + i),
            gcPin: gcPIMap.get('gcPin' + i),
            gcType: gcPIMap.get('gcType' + i)
          };
        }
      }
      if (gcPaymentToCheck) {
        var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
        let balanceCheckResult = hooksHelper(
          'app.payment.giftcard.balance.check',
          'gcBalanceCheck',
          [gcPaymentToCheck.gcNum, gcPaymentToCheck.gcPin],
          require('*/cartridge/scripts/util/ipaGCUtils').gcBalanceCheck
        );
        if (
          balanceCheckResult != null &&
          Number(balanceCheckResult.response_code) === 1 &&
          balanceCheckResult.card &&
          balanceCheckResult.card.funds_available &&
          Number(balanceCheckResult.card.funds_available) > 0
        ) {
          total = base.getNonGiftCardAmount(basket, Number(balanceCheckResult.card.funds_available));
          if (total.giftCardAmountToApply.value > 0) {
            Transaction.wrap(function () {
              let paymentInstrument = basket.createPaymentInstrument(ipaConstants.GIFT_CARD, total.giftCardAmountToApply);
              paymentInstrument.custom.giftCardNumber = gcPaymentToCheck.gcNum;
              paymentInstrument.custom.giftCardPin = gcPaymentToCheck.gcPin;
              paymentInstrument.custom.giftCardType = gcPaymentToCheck.gcType;
              paymentInstrument.custom.giftBalanceLeft = total.amountLeft;
              giftCardsTotal = giftCardsTotal.add(paymentInstrument.getPaymentTransaction().getAmount());
            });
          }
        }
      }
    }

    var saksgcPaymentInstrs = basket.getPaymentInstruments(ipaConstants.SAKS_GIFT_CARD);
    var saksgiftCardsTotal = new Money(0.0, currencyCode);
    var orderTotal = basket.totalGrossPrice;
    if (!saksgcPaymentInstrs.empty) {
      // remove Saks Gift Card Payment Instrument to make sure we have right GC with the right amount
      collections.forEach(saksgcPaymentInstrs, function (item) {
        basket.removePaymentInstrument(item);
      });

      // Map is our reference for the GC to be recreated. We need to create new GC Payment to avoid difference in the amount applied vs order total
      // the previous applied amount would be the threshold amount that can be applied and hence treated as the balance.
      // if the giftCardAmountToApply is zero then we skip the GC to be applied
      var gcPaymentToCheck;
      var total;
      // Check the total available GC in the SaksFirst account and if amount is sufficient to redeem.
      if (saksgcPaymentInstrs.length > 0) {
        var saksFirstHelpers = require('*/cartridge/scripts/helpers/saksFirstHelpers');
        var saksFirstInfo = saksFirstHelpers.getSaksFirstMemberInfo(customer.profile);
        if (saksFirstInfo && saksFirstInfo.giftcardDollerAmount && saksFirstInfo.giftcardDollerAmount > 0) {
          for (i = 0; i < saksgcPaymentInstrs.length; i++) {
            total = base.getNonGiftCardAmount(basket);
            if (total.remainingAmount.value > 0) {
              Transaction.wrap(function () {
                var amountToRedeem = saksFirstInfo.giftcardDollerAmount;
                if (total.remainingAmount.value <= saksFirstInfo.giftcardDollerAmount) {
                  amountToRedeem = total.remainingAmount.value;
                }
                let paymentInstrument = basket.createPaymentInstrument(ipaConstants.SAKS_GIFT_CARD, new Money(amountToRedeem, currencyCode));
                giftCardsTotal = giftCardsTotal.add(paymentInstrument.getPaymentTransaction().getAmount());
                paymentInstrument.custom.giftBalanceLeft = total.amountLeft;
              });
            }
          }
        }
      }
    }

    // NonGC PI is - CC, ShopRunner, Masterpass and PayPal
    // NonGC payment instrument will always be one PI among CC, SR, MP and PayPal
    // If the nonGC is not defined or initialed then the order should be fully paid via GC's or promotions
    if (nonGCPaymentInstrument) {
      Transaction.wrap(function () {
        var nonGCOrderTotal = orderTotal.subtract(giftCardsTotal);
        nonGCPaymentInstrument.paymentTransaction.setAmount(nonGCOrderTotal);
      });
    }
    var finalPIs = basket.getPaymentInstruments();
    var amountInPaymentInst = new Money(0.0, currencyCode);
    collections.forEach(finalPIs, function (paymentInstrument) {
      amountInPaymentInst = amountInPaymentInst.add(paymentInstrument.getPaymentTransaction().getAmount());
    });
    if (orderTotal.available && amountInPaymentInst.value !== orderTotal.value) {
      result.error = true;
      if (gcPaymentInstrs.length > 0) {
        result.maxGCLimitReached = gcPaymentInstrs.length == preferences.maxGCLimit;
      } else {
        result.maxGCLimitReached = false;
      }
      // the payment instrument total does not match with the order total, return error.
      // customer should choose additional payment or modify their current payment information
    }
  } catch (e) {
    result.error = true;
  }
  return result;
};

/**
 * Added for SFSX-466
 * This method requires custom steps for Saks Fifth Avenue Banner
 * Hence copied it from app_hbc_core cartridge to the app_hbc_saksfifthavenue cartridge.
 *
 * Copy information from address object and save it in the system
 * @param {Object} req - http request object
 * @param {dw.customer.CustomerAddress} newAddress - newAddress to save information into
 * @param {*} address - Address to copy from
 * @returns {dw.customer.CustomerAddress} newAddress - copied address
 */
base.updateAddressFields = function (req, newAddress, address) {
  var Locale = require('dw/util/Locale');
  var currentLocale = Locale.getLocale(req.locale.id);
  newAddress.setAddress1(address.address1.value || '');
  newAddress.setAddress2(address.address2.value || '');
  newAddress.setCity(address.city.value || '');
  newAddress.setFirstName(address.firstName.value || '');
  newAddress.setLastName(address.lastName.value || '');
  newAddress.setPhone(address.phone.value || '');
  newAddress.setPostalCode(address.postalCode.value || '');
  if (address.states && address.states.stateCode && address.states.stateCode.value) {
    newAddress.setStateCode(address.states.stateCode.value);
  }
  newAddress.setCountryCode(currentLocale.country);
  //Changes for SFSX-466 - start
  var date = new Date();
  newAddress.custom.lastAddressModified = date.toISOString();
  //Changes for SFSX-466 - end
  return newAddress;
};

base.hasDropShipItems = function (LineItemCtnr) {
  if (!LineItemCtnr) {
    return;
  }
  var allItems = LineItemCtnr.getAllProductLineItems();
  var hasOneDropShipProd = false;
  collections.forEach(allItems, function (pli) {
    var product = pli.product;
    if (product && 'pdRestrictedShipTypeText' in product.custom && product.custom.pdRestrictedShipTypeText != null) {
      hasOneDropShipProd = true;
    }
  });
  return hasOneDropShipProd;
};

base.setSignatureRequired = function (order, currentBasket, shipment, signatureRequired) {
  var preferences = require('*/cartridge/config/preferences');
  var skipShipping = false;
  var hasInStoreItems = null;
  var signatureOrderThreshold = preferences.signatureOrderThreshold;
  var result = {
    error: false,
    errorMessage: null
  };
  try {
    var giftWrapEnabledforBopis = preferences.giftWrapEnabledforBopis;
    if (order && order.items && order.items.hasInStoreItems) {
      hasInStoreItems = order.items.hasInStoreItems;
      if (
        hasInStoreItems &&
        hasInStoreItems.length > 0 &&
        hasInStoreItems.length === currentBasket.productLineItems.size() &&
        giftWrapEnabledforBopis == true
      ) {
        skipShipping = true;
      }
    }

    Transaction.wrap(function () {
      if (signatureOrderThreshold && order.totals.grandTotalValue <= signatureOrderThreshold) {
        if (signatureRequired) {
          shipment.custom.signatureRequired = true; // eslint-disable-line
        } else {
          shipment.custom.signatureRequired = false; // eslint-disable-line
        }
      } else if (!skipShipping) {
        if (signatureOrderThreshold && order.totals.grandTotalValue > signatureOrderThreshold) {
          shipment.custom.signatureRequired = true; // eslint-disable-line
        }
      } else {
        // eslint-disable-next-line no-param-reassign
        shipment.custom.signatureRequired = false;
      }
    });
  } catch (e) {
    result.error = true;
  }
  return result;
};

/**
 * Attempts to create an order from the current basket
 * @param {dw.order.Basket} currentBasket - The current basket
 * @returns {dw.order.Order} The order object created from the current basket
 */
base.createOrder = function (currentBasket) {
  var OrderMgr = require('dw/order/OrderMgr');
  var order;

  try {
    order = Transaction.wrap(function () {
      var order = OrderMgr.createOrder(currentBasket);
      order.paymentTransaction.custom.shoprunnerExpressOrder = currentBasket.custom.shoprunnerExpressOrder;
      return order;
    });
  } catch (error) {
    require('dw/system/Logger')
      .getRootLogger()
      .error('Error in creating order: ' + error.message);
    return null;
  }
  return order;
};

base.clearDataWhenRefresh = function () {
  var BasketMgr = require('dw/order/BasketMgr');
  var preferences = require('*/cartridge/config/preferences');
  var collections = require('*/cartridge/scripts/util/collections');
  var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
  var currentBasket = BasketMgr.getCurrentBasket();
  var shipments = currentBasket ? currentBasket.getShipments() : null;
  var bfxCountryCode = cookiesHelper.read('bfx.country');
  var geolocation = request.geolocation;
  var countryCode = !empty(bfxCountryCode) ? bfxCountryCode : geolocation && geolocation.countryCode ? geolocation.countryCode : preferences.countryCode;
  if (shipments) {
    collections.forEach(shipments, function (shipment) {
      if (shipment.shippingMethodID != 'instore') {
        var shippingAddress = {
          address: {
            firstName: '',
            lastName: '',
            address1: '',
            address2: '',
            city: '',
            postalCode: '',
            stateCode: '',
            countryCode: countryCode,
            phone: ''
          }
        }
        // clear shipping address and billing address
        base.copyShippingAddressToShipment(shippingAddress, shipment);
        base.copyBillingAddressToBasket(shippingAddress.address, currentBasket);
        // clear giftCard and Payment
        var paymentInstruments = currentBasket.getPaymentInstruments();
        collections.forEach(paymentInstruments, function (item) {
          currentBasket.removePaymentInstrument(item);
        });
      }
    });
  }
}

module.exports = base;
