/* eslint-disable guard-for-in */
'use strict';

var base = module.superModule;
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var Logger = require('dw/system/Logger');
var arrayHelper = require('*/cartridge/scripts/util/array');
var ShippingMgr = require('dw/order/ShippingMgr');
var ShippingMethodModel = require('*/cartridge/models/shipping/shippingMethod');
const BF_SHIPPINGMETHOD = 'Borderfree';

function getEDDFormattedDate(calendar) {
  return StringUtils.formatCalendar(calendar, 'yyyy-MM-dd');
}

function getEDDDateIfNoEDD(shippingMethod) {
  if (shippingMethod && shippingMethod.custom && shippingMethod.custom.estimatedDateNoEDD) {
    var currentDate = new Calendar();
    currentDate.add(Calendar.DATE, shippingMethod.custom.estimatedDateNoEDD);
    return getEDDFormattedDate(currentDate);
  }
  return null;
}

/**
 * Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * @param {dw.order.Shipment} shipment - the target Shipment
 * @param {Object} [address] - optional address object
 * @returns {dw.util.Collection} an array of ShippingModels
 */
base.getApplicableShippingMethods = function (shipment, address) {
  var Transaction = require('dw/system/Transaction');
  var collections = require('*/cartridge/scripts/util/collections');
  var BasketMgr = require('dw/order/BasketMgr');
  var preferences = require('*/cartridge/config/preferences');
  var dropShippingMethodsValueMap = preferences.dropShippingMethodsValueMap;
  var dropShipItemDetails = null;
  var dropShipDefaultMethod = null;
  if (!shipment) return null;
  var srMethod = null;
  var dropShipItemDetailsKeys = [];
  var shipmentShippingModel = ShippingMgr.getShipmentShippingModel(shipment);
  var shippingMethods;
  if (address) {
    shippingMethods = shipmentShippingModel.getApplicableShippingMethods(address);
  } else {
    shippingMethods = shipmentShippingModel.getApplicableShippingMethods();
  }
  try {
    var currentBasket = BasketMgr.getCurrentBasket();
    if (currentBasket.custom.hasOwnProperty('dropShipItemsDetails') && !empty(currentBasket.custom.dropShipItemsDetails) && dropShippingMethodsValueMap) {
      var dropShipItemDetails = currentBasket.custom.dropShipItemsDetails;
      var dropShipItemCount = 0;
      dropShipItemDetails = JSON.parse(dropShipItemDetails);
      dropShipItemDetailsKeys = Object.keys(dropShipItemDetails);
      var HashSet = require('dw/util/HashSet');
      var shipTypeTextsInBasket = new HashSet();
      var shipTypeTextKeysJSON = JSON.parse(dropShippingMethodsValueMap);
      var shipTypeTextKeys = Object.keys(shipTypeTextKeysJSON);
      // push all ship type text's of product in basket in to an array
      for (var j = 0; j < dropShipItemDetailsKeys.length; j++) {
        var dropShipItem = dropShipItemDetailsKeys[j];
        let count = dropShipItemDetails[dropShipItem];
        if (isNaN(count)) {
          count = Number(count);
        }
        dropShipItemCount = dropShipItemCount + count;
        if (dropShipItem) {
          shipTypeTextsInBasket.add(dropShipItem);
        }
      }
      // filter out the shipping methods
      if (shipTypeTextsInBasket.length > 0 && shipTypeTextKeys.length > 0) {
        var value = dropShipItemDetails[shipTypeTextsInBasket[0]];
        if (isNaN(value)) {
          value = Number(value);
        }
        if (value === currentBasket.productLineItems.length || dropShipItemCount === currentBasket.productLineItems.length) {
          for (var i = 0; i < shipTypeTextKeys.length; i++) {
            var indShipText = shipTypeTextKeys[i];
            var key = shipTypeTextsInBasket.contains(indShipText);
            if (value === currentBasket.productLineItems.length && key) {
              dropShipDefaultMethod = shipTypeTextKeysJSON[indShipText];
              break;
            } else if (key && dropShipItemCount === currentBasket.productLineItems.length) {
              dropShipDefaultMethod = dropShipDefaultMethod + shipTypeTextKeysJSON[indShipText];
            }
          }
        }
      }
    }
    Logger.debug('Default Shipping method ' + dropShipDefaultMethod);
  } catch (e) {
    Logger.error('Error in applyDropShipDefaultMethod: getApplicableShippingMethod ' + e.message);
  }

  let shopRunnerEnabled = require('*/cartridge/scripts/helpers/shopRunnerHelpers').checkSRExpressCheckoutEligibility(currentBasket);

  // Filter out whatever the method associated with in store pickup
  var filteredMethods = [];
  collections.forEach(shippingMethods, function (shippingMethod) {
    if (
      !shippingMethod.custom.storePickupEnabled &&
      (empty(dropShipDefaultMethod) || dropShipDefaultMethod === shippingMethod.ID || dropShipDefaultMethod.indexOf(shippingMethod.ID) > -1)
    ) {
      if (shippingMethod.ID === 'shoprunner') {
        srMethod = shippingMethod;
      }
      if (shippingMethod.ID != BF_SHIPPINGMETHOD) {
        if (shopRunnerEnabled) {
          filteredMethods.push(new ShippingMethodModel(shippingMethod, shipment));
        } else {
          if (shippingMethod.ID != 'shoprunner') {
            filteredMethods.push(new ShippingMethodModel(shippingMethod, shipment));
          }
        }
      }
    }
  });

  /*    if (filteredMethods.length == 1 && dropShipDefaultMethod && (filteredMethods[0].ID === dropShipDefaultMethod || dropShipDefaultMethod.indexOf(filteredMethods[0].ID) > -1) && shipment.shippingMethodID !== 'shoprunner' && shipment.shippingMethodID !== 'instore') {
            if (dropShipItemDetailsKeys && dropShipItemDetailsKeys.length > 1) {
                session.custom.showshippingmsg = filteredMethods[0].displayName;
            }
            Transaction.wrap(function () {
                // set shipping method
                base.selectShippingMethod(shipment, filteredMethods[0].ID);
            });
        } else if (srMethod && dropShipItemDetailsKeys.length > 0) {
            session.custom.showshippingmsg = srMethod.displayName;
            Transaction.wrap(function () {
                // set shipping method
                base.selectShippingMethod(shipment, 'shoprunner');
            });
        }*/
  return filteredMethods;
};

base.getEDDdate = function (shippingMethod, EDD) {
  if (shippingMethod) {
    if (EDD && EDD.containsKey(shippingMethod.ID)) {
      if (EDD.get(shippingMethod.ID)) {
        var eddDate = EDD.get(shippingMethod.ID).split('-');
        var dareStr = '' + eddDate[1] + '/' + eddDate[2] + '/' + eddDate[0];
        var date = new Date(dareStr);
        var formattedDate = StringUtils.formatCalendar(new Calendar(date), 'MM/dd/yy');
        return {
          EstimatedDate: formattedDate, // This will be used on the Storefront
          OrdercreationEstimatedDate: getEDDFormattedDate(new Calendar(date)) // This will be store for order creation Call
        };
      }
    } else {
      return {
        EstimatedDate: shippingMethod && shippingMethod.custom ? shippingMethod.custom.estimatedArrivalTime : null,
        OrdercreationEstimatedDate: getEDDDateIfNoEDD(shippingMethod)
      };
    }
  }
  return null;
};

/**
 * Apply NeXt day shipping method if there is a saksplus product or saksplus eligible customer
 * @param {Object} shipment - shipment
 * @returns {Object} the drop ship default shipping method
 */
base.applySAKSPlusDefaultShipMethod = function (shipment) {
  var Transaction = require('dw/system/Transaction');
  var appliedShipMethodID = null;
  try {
    // drop ship items and not equal to shoprunner
    if (shipment.shippingMethodID !== 'shoprunner') {
      var applicableShippingMethods = base.getApplicableShippingMethods(shipment);
      var matchingMethod = arrayHelper.find(applicableShippingMethods, function (method) {
        return method.saksplusShippingMethod;
      });
      if (matchingMethod) {
        Transaction.wrap(function () {
          // set shipping method
          base.selectShippingMethod(shipment, matchingMethod.ID);
        });
        appliedShipMethodID = matchingMethod.ID;
      }
    }
  } catch (e) {
    Logger.error('Error in applyDropShipDefaultMethod ' + e.message);
    return appliedShipMethodID;
  }
  return appliedShipMethodID;
};

base.getSortedShippingModels = function (currentBasket, customer, containerView) {
  var shipments = currentBasket ? currentBasket.getShipments() : null;
  if (!shipments) return [];
  // since sorting may disturb position of lineitems according to store number just here
  // we are adding home shipments next to bopis shipment
  var ArrayList = require('dw/util/ArrayList');
  var bopisShopipments = new ArrayList();
  var homeShopipments = new ArrayList();
  var collections = require('*/cartridge/scripts/util/collections');
  collections.forEach(shipments, function (shipment) {
    if (!('fromStoreId' in shipment.custom) || !shipment.custom.fromStoreId) {
      homeShopipments.add(shipment);
    } else {
      bopisShopipments.add(shipment);
    }
  });
  bopisShopipments.addAll(homeShopipments);
  // create shipments model for sorted shipping
  var ShippingModel = require('*/cartridge/models/shipping');
  var collections = require('*/cartridge/scripts/util/collections');
  return collections.map(bopisShopipments, function (shipment) {
    return new ShippingModel(shipment, null, customer, containerView);
  });
};

base.isMixedCart = function (lineItemContainer) {
  if (lineItemContainer && lineItemContainer.shipments && lineItemContainer.shipments.length > 1) {
    // Find ship to home shipments since multi shipments exists for only bopis and mixed cart
    var collections = require('*/cartridge/scripts/util/collections');
    var homeShipment = collections.find(lineItemContainer.shipments, function (shipment) {
      return !('fromStoreId' in shipment.custom) || !shipment.custom.fromStoreId;
    });
    return !!homeShipment;
  }
  return false;
};
/**
 * Gets the drops ship item shipping method id
 * @returns {Object} the drop ship default shipping method
 */
base.getDefaultDropShippingMethod = function (shippingMethods) {
  var collections = require('*/cartridge/scripts/util/collections');
  var ArrayList = require('dw/util/ArrayList');
  var BasketMgr = require('dw/order/BasketMgr');
  var array = require('*/cartridge/scripts/util/array');
  var preferences = require('*/cartridge/config/preferences');
  var dropShipDefaultMethod = null;

  var srMethod = null;

  try {
    var dropShippingMethodsValueMap = preferences.dropShippingMethodsValueMap;
    var currentBasket = BasketMgr.getCurrentBasket();
    var shippinfMethodIDs = new ArrayList();
    if (
      currentBasket.custom.hasOwnProperty('dropShipItemsDetails') &&
      !empty(currentBasket.custom.dropShipItemsDetails) &&
      dropShippingMethodsValueMap &&
      shippingMethods.length > 0
    ) {
      var dropShipItemDetails = currentBasket.custom.dropShipItemsDetails;
      var dropShipItemCount = 0;
      for (var j = 0; j < shippingMethods.length; j++) {
        let shippingMethod = shippingMethods[j];
        shippinfMethodIDs.add(shippingMethod.ID);
      }
      dropShipItemDetails = JSON.parse(dropShipItemDetails);
      dropShipItemDetailsKeys = Object.keys(dropShipItemDetails);
      var HashSet = require('dw/util/HashSet');
      var shipTypeTextsInBasket = new HashSet();
      var shipTypeTextKeysJSON = JSON.parse(dropShippingMethodsValueMap);
      var shipTypeTextKeys = Object.keys(shipTypeTextKeysJSON);
      for (var i = 0; i < shipTypeTextKeys.length; i++) {
        var indShipText = shipTypeTextKeys[i];
        var key = array.find(dropShipItemDetailsKeys, function (dropShipItemDetailsKey) {
          return dropShipItemDetailsKey === indShipText;
        });
        if (key && shippinfMethodIDs.contains(shipTypeTextKeysJSON[indShipText])) {
          dropShipDefaultMethod = shipTypeTextKeysJSON[indShipText];
          break;
        }
      }
    }
  } catch (e) {
    Logger.error('Error in getDefaultDropShippingMethod: getDefaultDropShippingMethod ' + e.message);
  }
  Logger.debug('Drop Ship Default method' + dropShipDefaultMethod);
  return dropShipDefaultMethod;
};

/**
 * Apply drop ship items if there are any items
 * @param {Object} shipment - shipment
 * @returns {Object} the drop ship default shipping method
 */
base.applyDropShipDefaultMethod = function (shipment, srTokenCookieValue, shopRunnerEnabled) {
  var Transaction = require('dw/system/Transaction');
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();
  var appliedDropShipMethodID = null;
  var dropShipItemDetailsKeys = [];
  var srMethod = null;
  try {
    var shippingMethods = shipment.applicableShippingMethods;
    delete session.custom.showshippingmsg;
    if (currentBasket.custom.hasOwnProperty('dropShipItemsDetails') && !empty(currentBasket.custom.dropShipItemsDetails)) {
      var dropShipItemDetails = currentBasket.custom.dropShipItemsDetails;
      dropShipItemDetails = JSON.parse(dropShipItemDetails);
      dropShipItemDetailsKeys = Object.keys(dropShipItemDetails);
    }
    // drop ship items and not equal to shoprunner
    if (shippingMethods && shippingMethods.length > 0 && shipment.defaultDropShippingMethod && shipment.defaultDropShippingMethod.length > 0) {
      appliedDropShipMethodID = shipment.defaultDropShippingMethod;
      for (var k = 0; k < shippingMethods.length; k++) {
        var appMethod = shippingMethods[k];
        if (appMethod.ID === 'shoprunner') {
          srMethod = appMethod;
          break;
        }
      }
      if (appliedDropShipMethodID && appliedDropShipMethodID.length > 0) {
        Transaction.wrap(function () {
          // set shipping method
          if (srMethod && srTokenCookieValue && shopRunnerEnabled === true) {
            base.selectShippingMethod(shipment.raw, srMethod.ID);
          } else {
            base.selectShippingMethod(shipment.raw, appliedDropShipMethodID);
          }
        });
        if (dropShipItemDetailsKeys.length > 1 || shipment.raw.shippingMethod.ID === 'shoprunner') {
          session.custom.showshippingmsg = shipment.raw.shippingMethod.displayName;
        }
      }
    }
  } catch (e) {
    Logger.error('Error in applyDropShipDefaultMethod ' + e.message);
    return appliedDropShipMethodID;
  }
  return appliedDropShipMethodID;
};

/**
 * Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * @param {dw.order.Shipment} shipment - the target Shipment
 * @param {dw.util.HashMap} EDDMap - EDD Map for Estimated Delivery date
 * @returns {Object|null} a ShippingMethodModel object
 */
base.getSelectedShippingMethod = function (shipment, EDDMap) {
    if (!shipment) return null;
  
    var method = shipment.shippingMethod;
  
    return method ? new ShippingMethodModel(method, shipment, EDDMap) : null;
  }

/**
 * @returns {dw.util.ArrayList} an array of ShippingModels
 */

/**
 * Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * @param {dw.order.Basket} currentBasket - the target Basket object
 * @param {string} address - optional address object
 * @returns {Array} a array of shipping models
 */
base.getReducedShippingModels = function(currentBasket, address) {
    var shipments = currentBasket ? currentBasket.getShipments() : null;

    if (!shipments) return [];
    var collections = require('*/cartridge/scripts/util/collections');

    return collections.map(shipments, function (shipment) {
        var shippingModel = {};
        var EDDMap;
        if ('EDDResponse' in shipment.custom && !empty(shipment.custom.EDDResponse)) {
            EDDMap = base.getEDDDateMap(JSON.parse(shipment.custom.EDDResponse));
        }
        shippingModel.applicableShippingMethods = base.getApplicableShippingMethods(shipment, address);
        shippingModel.selectedShippingMethod = base.getSelectedShippingMethod(shipment, EDDMap);
        return shippingModel;
    });
}

module.exports = base;
