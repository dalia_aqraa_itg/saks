'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var base = module.superModule;

var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
var StoreMgr = require('dw/catalog/StoreMgr');
var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
/** OVERRIDEN: To change the custom attribute reference and new method addition **/

function removeFromWishlist(productID, req) {
  var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
  var list = productListHelper.getList(req.currentCustomer.raw, {
    type: 10
  });
  var config = {
    qty: 1,
    optionId: null,
    optionValue: null,
    req: req,
    type: 10
  };
  productListHelper.addItem(list, productID, config);
}

/**
 * Gets a session specific to notification
 */
function getNotificationSession() {
  return session.custom.notifiedProducts;
}

/**
 * Create a session specific to notification
 * @param {Object} item product line item
 */
function createNotifyInSession(item) {
  var obj = {};
  obj[item.productID] = true;
  session.custom.notifiedProducts = JSON.stringify(obj);
  return session.custom.notifiedProducts;
}

/**
 * Update notifications in session
 * @param {Object} item product line item
 */
function updateNotifyInSession(item) {
  var jsonObj = getNotificationSession();
  jsonObj[item.productID] = true;
  session.custom.notifiedProducts = jsonObj;
  return false;
}

/**
 * Delete notification sepecific to session
 * @param {Objecy} item Product line item
 */
function deleteNotifyInSession(item) {
  var jsonObj = getNotificationSession();
  var obj = JSON.parse(jsonObj);
  delete obj[item.productID];
  session.custom.notifiedProducts = JSON.stringify(obj);
  return true;
}

/**
 * Is notification for product exists
 * @param {Obejct} item Product line item
 */
function isNotifyProductExists(item) {
  var notificationSession = getNotificationSession();
  return !!(notificationSession && item && JSON.parse(notificationSession)[item.productID]);
}

/**
 * Validates whether OOS product is already notified
 * @param {Object} item Product Line Item
 */
function isAlreadyNotified(item) {
  var notificationSession = getNotificationSession();
  var isNotified = false;
  if (!notificationSession) {
    createNotifyInSession(item);
  } else {
    isNotified = isNotifyProductExists(item);
    isNotified = isNotified ? deleteNotifyInSession(notificationSession, item) : updateNotifyInSession(notificationSession, item);
  }
  return isNotified;
}

/**
 * validates that the product line items exist, are online, and have available inventory.
 * @param {dw.order.Basket} item - The current user's basket
 * @param {boolean} hasInventory - hasInventory
 * @returns {Object} an error object
 */
function validateProductLineItem(item, hasInventory, enableSFCCNotifyOOS) {
  var validatePLI = {
    isInStock: false,
    isNotifyProductExists: false
  };
  if (item.product === null || !item.product.online) {
    return validatePLI;
  }
  //changes for SFDEV-6215
  var hasInventoryInOMS = true;
  if (session.custom.omsInventory && session.custom.omsInventory.length > 0) {
    var omsInventoryArray = JSON.parse(session.custom.omsInventory);
    if (omsInventoryArray && omsInventoryArray.length > 0) {
      for (var i = 0; i < omsInventoryArray.length; i++) {
        if (omsInventoryArray[i].itemID === item.productID && omsInventoryArray[i].quantity < item.quantityValue) {
          hasInventoryInOMS = false;
          break;
        }
      }
    }
  }
  if (!hasInventoryInOMS) {
    return validatePLI;
  }
  var isInPurchaseLimit = productHelper.isInPurchaselimit(item.product.custom.purchaseLimit, item.quantityValue);
  /* eslint-disable */
  if (Object.hasOwnProperty.call(item.custom, 'fromStoreId') && item.custom.fromStoreId) {
    var store = StoreMgr.getStore(item.custom.fromStoreId);
    var storeInventory = ProductInventoryMgr.getInventoryList(store.inventoryListID);
    validatePLI.isInStock =
      hasInventory &&
      storeInventory &&
      storeInventory.getRecord(item.productID) &&
      storeInventory.getRecord(item.productID).ATS.value >= item.quantityValue &&
      isInPurchaseLimit;
  } else {
    var availabilityLevels = item.product.availabilityModel.getAvailabilityLevels(item.quantityValue);
    validatePLI.isInStock = hasInventory && availabilityLevels.notAvailable.value === 0 && isInPurchaseLimit;
  }
  // validates SFCC inventory is notified previously, if not redirect to cart page
  if (!validatePLI.isInStock && typeof enableSFCCNotifyOOS != 'undefined' && enableSFCCNotifyOOS) {
    validatePLI.isInStock = !isAlreadyNotified(item);
    validatePLI.isNotifyProductExists = validatePLI.isInStock;
  }
  return validatePLI;
  /* eslint-enable */
}

/**
 * validates that the product line items exist, are online, and have available inventory.
 * @param {dw.order.Basket} basket - The current user's basket
 * @returns {Object} an error object
 */
function validateCheckoutProducts(basket, req, enableSFCCNotifyOOS) {
  var Logger = require('dw/system/Logger');
  var result = {
    error: true,
    hasInventory: true
  };

  //changes for SFDEV-6215
  var inventoryList = null;
  // eslint-disable-next-line no-undef
  if (session.custom.omsInventory && session.custom.omsInventory.length > 0) {
    // eslint-disable-next-line no-undef
    var omsInventoryObj = JSON.parse(session.custom.omsInventory);
    inventoryList = new dw.util.ArrayList(omsInventoryObj);
  }
  var itemRemoved = false;
  var productLineItems = basket.productLineItems;
  var Transaction = require('dw/system/Transaction');
  var notifyError = false;
  collections.forEach(productLineItems, function (item) {
    var validatePLI = validateProductLineItem(item, true, enableSFCCNotifyOOS);
    if (!validatePLI.isInStock) {
      Transaction.wrap(function () {
        if (item.bonusProductLineItem) {
          // saving attribute in session, since this attribute should be independent of login and cart merge we are saving in custom against privacy
          session.custom.isBonusProRemoved = true;
          // Saving bonus line item
          var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
          cartHelper.saveBonusLineItemInfo(basket, item);
        }
        var pliProduct = item.product;
        basket.removeProductLineItem(item);
        try {
          var dropShipItemDetails = {};
          if (pliProduct.custom.hasOwnProperty('pdRestrictedShipTypeText') && pliProduct.custom.pdRestrictedShipTypeText) {
            let productDropShipAttrValue = pliProduct.custom.pdRestrictedShipTypeText;
            if (currentBasket.custom.hasOwnProperty('dropShipItemsDetails') && !empty(currentBasket.custom.dropShipItemsDetails)) {
              dropShipItemDetails = currentBasket.custom.dropShipItemsDetails;
              dropShipItemDetails = JSON.parse(dropShipItemDetails);
              if (dropShipItemDetails[productDropShipAttrValue]) {
                let value = dropShipItemDetails[productDropShipAttrValue];
                if (isNaN(value)) {
                  value = Number(value);
                }
                if (value === 1) {
                  delete dropShipItemDetails[productDropShipAttrValue];
                } else if (value > 1) {
                  value = value - 1;
                  dropShipItemDetails[productDropShipAttrValue] = value;
                }
                currentBasket.custom.dropShipItemsDetails = JSON.stringify(dropShipItemDetails);
              }
            }
          }
        } catch (e) {
          Logger.error('Error adding drop ship details to basket' + e.message);
        }
        if (!item.bonusProductLineItem) {
          removeFromWishlist(item.product.ID, req);
        }
      });
      if (inventoryList && inventoryList.length > 0) {
        for (var i = 0; i < inventoryList.length; i++) {
          if (inventoryList.get(i).itemID === item.productID) {
            inventoryList.remove(inventoryList.get(i));
            itemRemoved = true;
            break;
          }
        }
      }
    }
    notifyError = notifyError || (typeof enableSFCCNotifyOOS !== 'undefined' && validatePLI.isInStock && validatePLI.isNotifyProductExists);
  });
  //update OMS inventory list in session -  changes for SFDEV-6215
  if (itemRemoved) {
    var currentInventory = inventoryList.toArray();
    // eslint-disable-next-line no-undef
    session.custom.omsInventory = JSON.stringify(currentInventory);
    result.itemRemoved = true;
  }
  result.notifyError = notifyError;
  result.error = !basket.productLineItems || basket.productLineItems.length === 0;
  return result;
}

module.exports = {
  validateProducts: base.validateProducts,
  validateCoupons: base.validateCoupons,
  validateShipments: base.validateShipments,
  validateInventory: base.validateInventory,
  validateCheckoutProducts: validateCheckoutProducts,
  removeInvalidCoupon: base.removeInvalidCoupon
};
