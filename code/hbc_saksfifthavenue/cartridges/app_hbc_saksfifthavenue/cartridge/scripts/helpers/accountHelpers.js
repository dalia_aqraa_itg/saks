'use strict';
var base = module.superModule;
var URLUtils = require('dw/web/URLUtils');
var endpoints = require('*/cartridge/config/oAuthRenentryRedirectEndpoints');

/**
 * Updates the Account Last Modified Date custom attribute, as the system attribute updates the date as the user logins to the system.
 * It doesn't become true last Modified Date. The system attribute of lastModified is updated for customer object being modified in backend.
 *
 * @param {dw.customer.Customer} customer - customer object
 */
base.updateAccLastModifiedDate = function (customer, comingFrom) {
  var date = new Date();
  var Transaction = require('dw/system/Transaction');
  if (customer && customer.authenticated) {
    var profile = customer.profile;
    Transaction.wrap(function () {
      profile.custom.accountModifiedDate = date.toISOString();
    });
  } else if (customer && !empty(comingFrom) && comingFrom === 'forgotPassword') {
    var profile = customer.profile;
    Transaction.wrap(function () {
      profile.custom.accountModifiedDate = date.toISOString();
    });
  }
  var saksplusHelper = require('*/cartridge/scripts/helpers/saksplusHelper');
  saksplusHelper.checkEligibilty(customer);
};

/**
 * Added for SFSX-465
 * Updates the Customer Last Password Modified Date custom attribute.
 *
 *  @param {dw.customer.Profile} profile - profile object
 */
base.updateLastPasswordModified = function (profile) {
  var date = new Date();
  if (profile) {
    profile.custom.lastPasswordModified = date.toISOString();
  }
};

/**
 * Creates an account model for the current customer
 * @param {string} redirectUrl - rurl of the req.querystring
 * @param {string} privacyCache - req.session.privacyCache
 * @param {boolean} newlyRegisteredUser - req.session.privacyCache
 * @returns {string} a redirect url
 */
base.getLoginRedirectURL = function (redirectUrl, privacyCache, newlyRegisteredUser, cartMerged) {
  var endpoint = 'Account-Show';
  var targetEndPoint = redirectUrl ? parseInt(redirectUrl, 10) : 1;
  if (targetEndPoint && targetEndPoint == 2 && cartMerged) {
    targetEndPoint = 4;
  }

  var registered = newlyRegisteredUser ? 'submitted' : 'false';

  var argsForQueryString = privacyCache.get('args');

  var tokenForQueryString = privacyCache.get('Token');

  if (targetEndPoint && endpoints[targetEndPoint]) {
    endpoint = endpoints[targetEndPoint];
  }

  return URLUtils.url(
    endpoint,
    'registration',
    registered,
    argsForQueryString ? 'args' : '',
    argsForQueryString ? argsForQueryString : '',
    tokenForQueryString ? 'Token' : '',
    tokenForQueryString ? tokenForQueryString : ''
  )
    .relative()
    .toString();
};

/**
 * Save TCC to my account
 * @param {String} tccCardNumber - tcc card number 29 digits
 */
base.saveTCCToAccount = function (tccCardNumber) {
  var PaymentInstrument = require('dw/order/PaymentInstrument');
  var Transaction = require('dw/system/Transaction');
  var StringUtils = require('dw/util/StringUtils');
  var TCCHelper = require('*/cartridge/scripts/checkout/TCCHelpers');
  var saved = false;

  if (customer && customer.authenticated) {
    var profile = customer.profile;

    if (profile) {
      var wallet = customer.getProfile().getWallet();
      var expirationYear = parseInt(tccCardNumber.substr(16, 2)) + 2000;
      var expirationMonth = parseInt(tccCardNumber.substr(18, 2));
      var tccExpirationDate = TCCHelper.getExpirationDate(tccCardNumber);

      Transaction.wrap(function () {
        var paymentInstrument = wallet.createPaymentInstrument(PaymentInstrument.METHOD_CREDIT_CARD);
        var cardHolder = StringUtils.format('{0} {1}', profile.firstName, profile.lastName);
        paymentInstrument.setCreditCardHolder(cardHolder);
        paymentInstrument.setCreditCardNumber(tccCardNumber);
        paymentInstrument.setCreditCardType('TCC');
        paymentInstrument.setCreditCardExpirationMonth(expirationMonth);
        paymentInstrument.setCreditCardExpirationYear(expirationYear);
        paymentInstrument.setCreditCardToken(tccCardNumber);
        paymentInstrument.custom.tccExpirationDate = '' + tccExpirationDate;
        profile.custom.lastPaymentModified = new Date().toISOString();
      });
      saved = true;
    }
  }

  return saved;
};

module.exports = base;
