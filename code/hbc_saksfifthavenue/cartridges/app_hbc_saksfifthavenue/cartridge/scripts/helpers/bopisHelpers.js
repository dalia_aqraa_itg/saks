'use strict';
var base = module.superModule;
var URLUtils = require('dw/web/URLUtils');
var StoreMgr = require('dw/catalog/StoreMgr');
var StoreModel = require('*/cartridge/models/store');
var preferences = require('*/cartridge/config/preferences');
var Resource = require('dw/web/Resource');
var instorePUstoreHelpers = require('*/cartridge/scripts/helpers/instorePickupStoreHelpers');
var Logger = require('dw/system/Logger');

/**
 * Sets store id and distance at session
 * @param {string} storeid - Store ID
 * @param {string} storedistance - Store distance
 */
function setStoreFromSession(storeid, storedistance) {
  var store = {};
  if (storeid && storedistance) {
    store.id = storeid;
    store.distance = storedistance;
    session.custom.currentStore = storeid;
    session.custom.store = JSON.stringify(store);
  }
}

/**
 * Set quantity messaging based on the selected and available units
 * @param {Object} storeObj - store model
 * @param {Object} product - product model
 * @returns {string} quantity messaging
 */
function setAvailabilityMessage(storeObj, product) {
  var msg = Resource.msg('store.unitsavailable.unavailable', 'storeLocator', null);
  // site preference for limited quantity message
  var limit = preferences.limitedQtyThreshold;
  if (storeObj && storeObj.unitsAtStores) {
    if (storeObj.unitsAtStores === 0 || storeObj.unitsAtStores < product.selectedQuantity) {
      msg = Resource.msg('store.unitsavailable.unavailable', 'storeLocator', null);
    } else if (storeObj.unitsAtStores > 0 && storeObj.unitsAtStores < limit) {
      msg = Resource.msg('store.unitsavailable.limited', 'storeLocator', null);
    } else {
      msg = Resource.msg('store.unitsavailable.available', 'storeLocator', null);
    }
  }
  return msg;
}

/**
 * Add or remove the store id in the url
 * @param {dw.web.URLUtils} refinementUrl - request url
 * @param {Object} store - store object
 * @param {boolean} isFilterApplied - if request has store id associated
 * @returns {dw.web.URLUtils} refinementUrl - url with store id added
 */
function toggleStoreRefinement(refinementUrl, store, isFilterApplied) {
  refinementUrl.remove('storeid');
  refinementUrl.remove('srchsrc');
  if (refinementUrl && store) {
    refinementUrl.append('storeid', store.ID);
  }
  return refinementUrl;
}

/**
 * Returns the store if available from session or with a store search on the Geolocation
 * @param {Object} product - product model
 * @returns {Object} defaultStore - store model
 */
function getDefaultStore(product, storeId) {
  var defaultStore = null;
  var stores = null;
  var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');
  var sessionStoreId = null;
  var defaultSearchRadius = 100;
  /*    if (storeId && storeId !== '') {
    	sessionStoreId = storeId;
    } else if (customer.authenticated && customer.profile && customer.profile.custom.hasOwnProperty('myDefaultStore') && customer.profile.custom.myDefaultStore) {
    	sessionStoreId = customer.profile.custom.myDefaultStore;
    }*/
  // get store from session
  var sessionCurrentStore = session.custom.currentStore ? session.custom.currentStore : null;
  var sessionStore = session.custom.store ? JSON.parse(session.custom.store) : '';
  /*   if (!sessionStoreId) {
    	sessionStoreId = sessionStore && sessionStore.id && sessionStore.distance ? sessionStore.id : null;
    }*/
  if (sessionCurrentStore) {
    defaultStore = StoreMgr.getStore(sessionCurrentStore);
    defaultStore = new StoreModel(defaultStore);
    defaultStore.distanceInUnits = sessionStore && sessionStore.distance ? sessionStore.distance : null;
  }
  // fall back, search store
  if (!defaultStore) {
    stores = storeHelpers.getStores(defaultSearchRadius, null, null, null, request.geolocation);
    if (stores && stores.stores && stores.stores.length > 0) {
      defaultStore = stores.stores[0];
    }
  }
  // set inventory
  if (defaultStore && product) {
    // property is defined without a prototype since no qualifiers are required
    defaultStore.unitsAtStores = instorePUstoreHelpers.getStoreInventory(defaultStore.ID, product.id);
  }
  return defaultStore;
}

/**
 * Sets view data required to render shipping options
 * @param {httpRequest} req - current request
 * @param {httpResponse} res - current response
 */
function setBopisViewData(req, res) {
  var viewData = res.getViewData();
  var product = viewData.product;
  var defaultStore = null;
  viewData.isBopisEnabled = preferences.isBopisEnabled && product && product.isAvailableForInstore;
  viewData.storeModalUrl = URLUtils.url('Stores-InitSearch').toString();
  viewData.setStoreUrl = URLUtils.url('Stores-SetStore').toString();
  viewData.availableMsg = Resource.msg('shippingoption.product.pickupinstore.available', 'storeLocator', null);
  viewData.notavailableMsg = Resource.msg('shippingoption.product.pickupinstore.notavailable', 'storeLocator', null);
  // bopis check at site and product level
  if (preferences.isBopisEnabled && product && product.isAvailableForInstore) {
    defaultStore = getDefaultStore(product);
    if (defaultStore) {
      viewData.storeInfo = defaultStore;
      // set quantity message
      viewData.availabilityMessage = Resource.msgf(
        'shippingoption.product.availableat',
        'storeLocator',
        null,
        setAvailabilityMessage(defaultStore, product),
        defaultStore.name
      );
      if (product && product.productType === 'variant') {
        // set available units
        viewData.unitsAtStores = instorePUstoreHelpers.getStoreInventory(defaultStore.ID, req.querystring.productid);
      }
    }
  }

  res.setViewData(viewData);
}

module.exports = {
  getDefaultStore: getDefaultStore,
  setBopisViewData: setBopisViewData,
  convertCases: base.convertCases,
  setStoreFromSession: setStoreFromSession,
  setAvailabilityMessage: base.setAvailabilityMessage,
  setShippingOptionData: base.setShippingOptionData,
  isStoreFilterApplied: base.isStoreFilterApplied,
  toggleStoreRefinement: toggleStoreRefinement
};

