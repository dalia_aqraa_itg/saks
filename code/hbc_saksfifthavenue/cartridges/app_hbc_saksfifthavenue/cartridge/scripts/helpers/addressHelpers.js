'use strict';
var Transaction = require('dw/system/Transaction');
var base = module.superModule;

/**
 * Added for SFSX-1826
 * This method requires custom steps for Saks Fifth Avenue Banner
 * Hence copied it from app_hbc_core cartridge to the app_hbc_saksfifthavenue cartridge.
 * This is done to resolve potential bidirectional sync issues during A/B Testing period.
 *
 * Generate address name based on the full address object
 * @param {dw.order.OrderAddress} address - Object that contains shipping address
 * @returns {string} - String with the generated address name
 */
base.generateAddressName = function (address) {
  return address.address1;
};

/**
 * Added for SFSX-466
 * This method requires custom steps for Saks Fifth Avenue Banner
 * Hence copied it from app_hbc_core cartridge to the app_hbc_saksfifthavenue cartridge.
 *
 * Stores a new address for a given customer
 * @param {Object} address - New address to be saved
 * @param {Object} customer - Current customer
 * @param {string} addressId - Id of a new address to be created
 * @returns {void}
 */
base.saveAddress = function (address, customer, addressId) {
  var addressBook = customer.raw.getProfile().getAddressBook();
  Transaction.wrap(function () {
    var newAddress = addressBook.createAddress(addressId);
    if (newAddress) {
      base.updateAddressFields(newAddress, address);
    }
  });

  base.resetAuthorizedCards(customer.raw.getProfile());
};

/**
 * Added for SFSX-466
 * This method requires custom steps for Saks Fifth Avenue Banner
 * Hence copied it from app_hbc_core cartridge to the app_hbc_saksfifthavenue cartridge.
 *
 * Copy information from address object and save it in the system
 * @param {dw.customer.CustomerAddress} newAddress - newAddress to save information into
 * @param {*} address - Address to copy from
 */
base.updateAddressFields = function (newAddress, address) {
  newAddress.setAddress1(address.address1 || '');
  newAddress.setAddress2(address.address2 || '');
  newAddress.setCity(address.city || '');
  newAddress.setFirstName(address.firstName || '');
  newAddress.setLastName(address.lastName || '');
  newAddress.setPhone(address.phone || '');
  newAddress.setPostalCode(address.postalCode || '');

  if (address.country === 'UK') {
    newAddress.setStateCode(address.UKState);
  } else if (address.country !== 'UK' && address.states) {
    if (address.states.stateCode) {
      newAddress.setStateCode(address.states.stateCode);
    } else if (address.states.stateUS) {
      newAddress.setStateCode(address.states.stateUS);
    } else if (address.states.stateCA) {
      newAddress.setStateCode(address.states.stateCA);
    }
  }

  if (address.country) {
    newAddress.setCountryCode(address.country);
  }
  // Vertax Specific Code - Update Tax Number
  newAddress.custom.taxnumber = address.taxnumber; // eslint-disable-line

  newAddress.setJobTitle(address.jobTitle || '');
  newAddress.setPostBox(address.postBox || '');
  newAddress.setSalutation(address.salutation || '');
  newAddress.setSecondName(address.secondName || '');
  newAddress.setCompanyName(address.companyName || '');
  newAddress.setSuffix(address.suffix || '');
  newAddress.setSuite(address.suite || '');
  newAddress.setJobTitle(address.title || '');
  //Changes for SFSX-466 - start
  var date = new Date();
  newAddress.custom.lastAddressModified = date.toISOString();
  //Changes for SFSX-466 - end
};

/**
 * Added for SFSX-466
 * This method requires custom steps for Saks Fifth Avenue Banner
 * Hence copied it from app_hbc_core cartridge to the app_hbc_saksfifthavenue cartridge.
 *
 * Stores a new address for a given customer
 * @param {Object} address - New address to be saved
 * @param {Object} customer - Current customer
 * @param {string} addressId - Id of a new address to be created
 * @returns {void}
 */
base.saveBillingAddress = function (address, customer, addressId) {
  var Transaction = require('dw/system/Transaction');

  var addressBook = customer.raw.getProfile().getAddressBook();
  var profile = customer.raw.getProfile();
  Transaction.wrap(function () {
    var newAddress = addressBook.createAddress(addressId);
    if (newAddress) {
      newAddress.setAddress1(address.address1 || '');
      newAddress.setAddress2(address.address2 || '');
      newAddress.setCity(address.city || '');
      newAddress.setFirstName(address.firstName || '');
      newAddress.setLastName(address.lastName || '');
      newAddress.setPhone(address.phone || '');
      newAddress.setPostalCode(address.postalCode || '');
      newAddress.setStateCode(address.stateCode);
      newAddress.setCountryCode(address.countryCode.value);
      newAddress.setJobTitle(address.jobTitle || '');
      newAddress.setPostBox(address.postBox || '');
      newAddress.setSalutation(address.salutation || '');
      newAddress.setSecondName(address.secondName || '');
      newAddress.setCompanyName(address.companyName || '');
      newAddress.setSuffix(address.suffix || '');
      newAddress.setSuite(address.suite || '');
      newAddress.setJobTitle(address.title || '');
      profile.custom.accountModifiedDate = new Date().toISOString();
      //Changes for SFSX-466 - start
      var date = new Date();
      newAddress.custom.lastAddressModified = date.toISOString();
      //Changes for SFSX-466 - end
    }
  });

  base.resetAuthorizedCards(customer.raw.getProfile());
};

/**
 * Reset Authorized cards if shipping or billing address was chenged
 * @param {Object} customer - Current customer
 * @returns {void}
 */
base.resetAuthorizedCards = function (customer) {
  if (customer) {
    var paymentInstruments = customer.wallet.paymentInstruments;
    for (var i = 0; i < paymentInstruments.length; i++) {
      var paymentInstrument = paymentInstruments[i];
      Transaction.wrap(function () {
        paymentInstrument.custom.authorizedCard = false;
      });
    }
  }
};

module.exports = base;
