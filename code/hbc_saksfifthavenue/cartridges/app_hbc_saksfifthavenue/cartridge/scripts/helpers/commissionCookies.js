'use strict';

var Site = require('dw/system/Site');
var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
var Logger = require('dw/system/Logger');
var Transaction = require('dw/system/Transaction');

/**
 * Store cookies with commission vendor data
 * @param request
 * @returns
 */

function addCommissionCookies(request) {
  var paramMap = request.httpParameterMap;
  var site_referCookieName = 'site_refer';
  var site_refer = paramMap.get(site_referCookieName) ? paramMap.get(site_referCookieName).value : '';
  //do not store cookies if site_refer parameter does not exist or empty
  if (!validateSiteRefer(site_refer)) {
    return;
  }
  var sf_storeidCookieName = 'sf_storeid';
  var sf_associdCookieName = 'sf_associd';
  var commissionCookieNames = [site_referCookieName, sf_storeidCookieName, sf_associdCookieName];
  var age = 14 * 24 * 60 * 60;
  commissionCookieNames.forEach(function (name) {
    var value = paramMap.get(name).value ? paramMap.get(name).value : ' ';
      var cookie = new dw.web.Cookie(name, value);
      cookie.setMaxAge(age);
      cookie.setSecure(true);
      cookie.setHttpOnly(true);
      cookie.setPath('/');
      response.addHttpCookie(cookie);
  });
}

/**
 * Attribution logic for cookies
 * @param value
 * @returns
 */
function validateSiteRefer(value) {
  if (!value) {
    return false;
  }

  // If Site Refer contains EML, TRP or TR, Do not set the Site Refere if existing site refer is CNCT and COMX
  if (value && (value.indexOf('EML') > 0 || value.indexOf('TRP') > 0 || value.indexOf('TR') > 0)) {
    var getSite_Refer = cookiesHelper.read('site_refer');
    if (getSite_Refer.indexOf('CNCT') > 0 || getSite_Refer.indexOf('COMX') > 0) {
      return false;
    }
  }

  var site_ref_search = 'SEM';
  var site_ref_email = 'EML';
  var site_ref_email_SF = 'EMLHB_SF';
  var emlRegexp = new RegExp('^(' + site_ref_email + '|' + site_ref_search + ')');
  if (emlRegexp.test(value)) {
    if (!value.equals(site_ref_email_SF)) {
      return false;
    }
  }
  return true;
}

/**
 * query retrieve commission cookies
 * @param request
 * @param cookieNames
 * @returns
 */
function getCommissionCookies(request, cookieNames) {
  var vendorCookies = new dw.util.HashMap();
  var cookies = request.getHttpCookies();
  var cookieCount = cookies.getCookieCount();
  cookieNames.forEach(function (name) {
    for (var i = 0; i < cookieCount; i++) {
      if (name === cookies[i].getName()) {
        var cookieValue = cookies[i].getValue();
        if (cookieValue && cookieValue !== '') {
          vendorCookies.put(name, {
            value: cookieValue
          });
        }
      }
    }
  });
  return vendorCookies;
}

function getAllStoreData() {
  try {
    var SystemObjectMgr = require('dw/object/SystemObjectMgr');
    var storeData = [];
    var queryStoresItr = SystemObjectMgr.querySystemObjects('Store', 'countryCode ILIKE {0}', 'city asc', 'US');
    if (queryStoresItr.count > 0) {
      while (queryStoresItr.hasNext()) {
        var store = queryStoresItr.next();
        var storeObj = {};
        storeObj.ID = store.ID;
        storeObj.name = store.name;
        storeData.push(storeObj);
      }
    }
    queryStoresItr.close();
    return storeData;
  } catch (e) {
    Logger.error('There was an error while fetching all store data for COMX: ' + e.message);
  }
}

//Changes for SFSX-1833 - https://hbcdigital.atlassian.net/browse/SFSX-1833 - start
//Updated the below method to remove certain validations so store1, associate1, store2 and associate2 values get stored in COMXData properly.
/**
 * This method is used to persist the COMX Data to basket, so when an order is created the same is pushed
 * in the corresponding order.
 *
 * @param req
 * @param basket
 *
 * @returns
 */
function handleComxData(req, basket) {
  try {
    var siteReferer = cookiesHelper.read('site_refer');
    if (siteReferer && siteReferer == 'COMX') {
      var storeData = {};
      if (req.form.comxStore1) {
        storeData.comxStore1 = req.form.comxStore1.toString();
        storeData.comxAssociate1 = empty(req.form.comxAssociate1) ? '' : req.form.comxAssociate1.toString();
      }

      if (req.form.comxStore2 && req.form.comxAssociate2) {
        storeData.comxStore2 = req.form.comxStore2.toString();
        storeData.comxAssociate2 = empty(req.form.comxAssociate2) ? '' : req.form.comxAssociate2.toString();
      }

      if (req.form.comxNote && req.form.comxNote.length > 0) {
        storeData.comxNote = req.form.comxNote.toString();
      }

      if (storeData && !empty(storeData)) {
        Transaction.wrap(function () {
          basket.custom.COMXData = JSON.stringify(storeData);
        });
      } else {
        delete basket.custom.COMXData;
      }
    } else {
      delete basket.custom.COMXData;
    }
  } catch (e) {
    delete basket.custom.COMXData;
    Logger.error('There was an error while fetching all store data for COMX: ' + e.message);
  }
}
//Changes for SFSX-1833 - https://hbcdigital.atlassian.net/browse/SFSX-1833 - end

module.exports = {
  getCommissionCookies: getCommissionCookies,
  addCommissionCookies: addCommissionCookies,
  getAllStoreData: getAllStoreData,
  handleComxData: handleComxData
};
