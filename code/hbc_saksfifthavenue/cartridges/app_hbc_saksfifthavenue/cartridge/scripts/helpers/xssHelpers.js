/** 
 * Added for SFSX-1504 - https://hbcdigital.atlassian.net/browse/SFSX-1504
 * 
 * This Script is used to finding XSS in query parameters.
 *
 */

var Site = require('dw/system/Site');
var Logger = require('dw/system/Logger');

/**
 * This method returns true if XSS params are found in query parameters and false otherwise.
 * 
 * @returns xSSParamFound : true/false 
 */
function check() {

    var xSSParamFound = false;
    var currentRequest = request;
    var queryString = currentRequest.httpQueryString;
    var finalqueryString = '';

    // Getting the list of XSS Cross Site Scripting strings from Site Preferences for verification
    // Merchant Tools --> Site Preferences --> Custom Site Preference Groups --> XSS Input Validation Config -- CrossSiteScript(XSS) Patterns
    var listOfXSSStrings = JSON.parse(Site.current.getCustomPreferenceValue('listOfXSSStrings'));

    // List of XSS Patterns
    var xssListOfPatterns = listOfXSSStrings.XSSListOfPatterns;

    try {
        if (!empty(queryString)) {//If Query string is not empty

            Logger.debug('\n Query String for XSS Cross site scripting check: before decoding ' + queryString);
            queryString = decodeURIComponent(queryString);
            Logger.debug('\n Query String for XSS Cross site scripting check: after decoding ' + queryString);            

            //Check if Regex patterns are found in the query string
            if (!empty(xssListOfPatterns)) {
                finalqueryString = checkForXSSPattern(queryString, xssListOfPatterns);
            }
            //if XSS found in query param redirect to 404 page, setting xSSParamFound variable to true.
            if (finalqueryString) {
                xSSParamFound = true;                
            }
        }
    } catch (e) {
        Logger.error('Error in executing xssHelper.js \ ' + e);
        xSSParamFound = false;        
    }
    
    return xSSParamFound;
}

/**
 * This is to find if configured Regex patterns are found in the query string.
 * 
 * @param queryStr
 * @param listOfPattern
 * 
 * @returns finalqueryString : String
 */
function checkForXSSPattern(queryStr, listOfPattern) {
    if (!empty(queryStr) && !empty(listOfPattern)) {
        var regexp;
        var finalqueryString = false;
        //Changes added for SFSX-2614 - start
        // Merchant Tools --> Site Preferences --> Custom Site Preference Groups --> XSS Input Validation Config -- Exception List
        var exceptionList = Site.current.getCustomPreferenceValue('ExceptionList').split('|');
		var excFound = false;
		var queryStrList = queryStr.split('&');
		for each (var q in queryStrList){
			for (var i = 0; i < listOfPattern.length; i++) {
	            regexp = new RegExp(listOfPattern[i]);
	            if (q.toLowerCase().search(regexp) != -1) {            	
	                //We are going to skip if the queryString has one of the preconfigured list of exceptions.            	
	                for each ( exc in exceptionList){
		                if(q.indexOf(exc) > -1){
		                	excFound=true;break;
	        	        }else{
		                	excFound=false;
	        	        }
	                }                
	                if(!excFound){
	                	finalqueryString = true;
	                    //finalqueryString = queryStr.toLowerCase().replace(regexp, '');
	                    Logger.debug('\n Query String has XSS Cross site scripting regex: ' + regexp);
	                    return finalqueryString;                	
	                }//Changes added for SFSX-2614 - end
	            }
	        }	
		}
    }
    return finalqueryString;
}


module.exports = {
	check : check,
	checkForXSSPattern : checkForXSSPattern
};
