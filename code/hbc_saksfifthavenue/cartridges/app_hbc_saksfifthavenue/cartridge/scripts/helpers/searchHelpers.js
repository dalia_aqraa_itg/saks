'use strict';

/**
 * Set search configuration values
 *
 * @param {dw.catalog.ProductSearchModel} apiProductSearch - API search instance
 * @param {Object} params - Provided HTTP query parameters
 * @return {dw.catalog.ProductSearchModel} - API search instance
 */
function setupSearch(apiProductSearch, params) {
  var CatalogMgr = require('dw/catalog/CatalogMgr');
  var searchModelHelper = require('*/cartridge/scripts/search/search');
  var preferences = require('*/cartridge/config/preferences');
  var designerCategoryID = 'brand';
  if (preferences.designerCategoryID) {
    designerCategoryID = preferences.designerCategoryID;
  }
  var sortingRule = params.srule ? CatalogMgr.getSortingRule(params.srule) : null;
  if (params.cgid === designerCategoryID || params.cgid === 'brand') {
    params.cgid = 'root';
  }
  if (!params.cgid && !params.q && !params.pid && !params.pmid) {
    return apiProductSearch;
  }
  var selectedCategory = CatalogMgr.getCategory(params.cgid);
  selectedCategory = selectedCategory && selectedCategory.online ? selectedCategory : null;

  searchModelHelper.setProductProperties(apiProductSearch, params, selectedCategory, sortingRule);

  if (params.preferences) {
    searchModelHelper.addRefinementValues(apiProductSearch, params.preferences);
  }

  return apiProductSearch;
}

/**
 * Retrieve a category's template filepath if available
 *
 * @param {dw.catalog.ProductSearchModel} apiProductSearch - API search instance
 * @return {string} - Category's template filepath
 */
function getCategoryTemplate(apiProductSearch) {
  return apiProductSearch.category ? apiProductSearch.category.template || 'rendering/category/producthits' : '';
}

/**
 * Set content search configuration values
 *
 * @param {Object} params - Provided HTTP query parameters
 * @return {Object} - content search instance
 */
function setupContentSearch(params) {
  var ContentSearchModel = require('dw/content/ContentSearchModel');
  var ContentSearch = require('*/cartridge/models/search/contentSearch');
  var apiContentSearchModel = new ContentSearchModel();

  apiContentSearchModel.setRecursiveFolderSearch(true);
  apiContentSearchModel.setSearchPhrase(params.q);
  apiContentSearchModel.search();
  var contentSearchResult = apiContentSearchModel.getContent();
  var count = Number(apiContentSearchModel.getCount());
  var contentSearch = new ContentSearch(contentSearchResult, count, params.q, params.startingPage, null);

  return contentSearch;
}

/**
 * Set the cache values
 * eight hours of cache as recommended from Salesforce on the ticket SFSX-4187
 *
 * @param {Object} res - The response object
 */
function applyCache(res) {
  res.cachePeriod = 8; // eslint-disable-line no-param-reassign
  res.cachePeriodUnit = 'hours'; // eslint-disable-line no-param-reassign
  res.personalized = true; // eslint-disable-line no-param-reassign
}

/**
 * performs a search
 *
 * @param {Object} req - Provided HTTP query parameters
 * @param {Object} res - Provided HTTP query parameters
 * @return {Object} - an object with relevant search information
 */
function search(req, res) {
  var CatalogMgr = require('dw/catalog/CatalogMgr');
  var URLUtils = require('dw/web/URLUtils');
  var ProductSearchModel = require('dw/catalog/ProductSearchModel');
  var preferences = require('*/cartridge/config/preferences');
  var ProductSearch = require('*/cartridge/models/search/productSearch');
  var reportingUrlsHelper = require('*/cartridge/scripts/reportingUrls');
  var schemaHelper = require('*/cartridge/scripts/helpers/structuredDataHelper');
  var refineSearch = require('*/cartridge/models/bopis/refineSearch');
  var storeRefineResult = null;
  var categoryTemplate = '';
  var maxSlots = 4;
  var productSearch;
  var reportingURLs;

  var apiProductSearch = res.getViewData().apiProductSearch;
  if (!apiProductSearch) {
    apiProductSearch = new ProductSearchModel();
  }

  // Remove Channel

  var searchRedirect = req.querystring.q ? apiProductSearch.getSearchRedirect(req.querystring.q) : null;

  if (searchRedirect) {
    return {
      searchRedirect: searchRedirect.getLocation()
    };
  }

  // identify if the product ID searched is chanel
  let isChanel = false;
  if (req.querystring.q && /^[0-9]*$/.test(req.querystring.q)) {
    var ProductMgr = require('dw/catalog/ProductMgr');
    var apiProduct = ProductMgr.getProduct(req.querystring.q);
    if (apiProduct && apiProduct.brand && apiProduct.brand.toLowerCase().indexOf('chanel') > -1) {
      isChanel = true;
    }
  }
  // if the search key has chanel, then skip the refinement of non-chanel products.
  if (!isChanel && req.querystring && req.querystring.q && req.querystring.q.toLowerCase().indexOf('chanel') === -1) {
    if (preferences.nonChanelProductTypes) {
      apiProductSearch.addRefinementValues('hbcProductType', preferences.nonChanelProductTypes);
    }
  }

  apiProductSearch = setupSearch(apiProductSearch, req.querystring);

  // PSM search with store refinement
  storeRefineResult = refineSearch.search(apiProductSearch, req.querystring);
  apiProductSearch = storeRefineResult.apiProductsearch;

  if (!apiProductSearch.personalizedSort) {
    applyCache(res);
  }
  categoryTemplate = getCategoryTemplate(apiProductSearch);
  productSearch = new ProductSearch(
    apiProductSearch,
    req.querystring,
    req.querystring.srule,
    CatalogMgr.getSortingOptions(),
    CatalogMgr.getSiteCatalog().getRoot()
  );

  var canonicalUrl = URLUtils.url('Search-Show', 'cgid', req.querystring.cgid);
  var refineurl = URLUtils.url('Search-Refinebar');
  var whitelistedParams = ['q', 'cgid', 'pmin', 'pmax', 'srule', 'pmid', 'storeid', 'srchsrc'];
  var isRefinedSearch = false;

  Object.keys(req.querystring).forEach(function (element) {
    if (whitelistedParams.indexOf(element) > -1) {
      refineurl.append(element, req.querystring[element]);
    }

    if (['pmin', 'pmax'].indexOf(element) > -1) {
      isRefinedSearch = true;
    }

    if (element === 'preferences') {
      var i = 1;
      isRefinedSearch = true;
      Object.keys(req.querystring[element]).forEach(function (preference) {
        refineurl.append('prefn' + i, preference);
        refineurl.append('prefv' + i, req.querystring[element][preference]);
        i++;
      });
    }

    if (element === 'MyDesignerSelected' || element === 'MySizesSelected' || element === 'addToMyPef') {
      refineurl.append(element, req.querystring[element]);
    }
  });

  if (productSearch.searchKeywords !== null && !isRefinedSearch) {
    reportingURLs = reportingUrlsHelper.getProductSearchReportingURLs(productSearch);
  }

  var result = {
    productSearch: productSearch,
    maxSlots: maxSlots,
    reportingURLs: reportingURLs,
    refineurl: refineurl,
    canonicalUrl: canonicalUrl,
    isRefinedByStore: storeRefineResult.isRefinedByStore
  };
  var isGucci = !empty(productSearch.httpParams) && !empty(productSearch.httpParams.preferences) && !empty(productSearch.httpParams.preferences.brand) ? productSearch.httpParams.preferences.brand.toLowerCase() === 'gucci' : false;
  if (productSearch.isCategorySearch && (!productSearch.isRefinedCategorySearch || (isGucci && req.querystring.cgid !== 'brand' && req.querystring.cgid !== 'root'))) {
  // !productSearch.isRefinedCategorySearch is a necessary condition. For Brand pages, the category context would be as refined category
    result.category = apiProductSearch.category;
    result.categoryTemplate = categoryTemplate;
  }
  // Changes required for SEO url changes for filter - DNE this condition
  if (productSearch.isCategorySearch) {
    result.tempCategory = apiProductSearch.category;
  }

  if (!categoryTemplate || categoryTemplate === 'rendering/category/categoryproducthits') {
    result.schemaData = schemaHelper.getListingPageSchema(productSearch.productIds);
  }

  return result;
}

/**
 * function to create designer object
 *
 * @param {dw.catalog.Category} category - main designer category
 * @returns {Object} - search designer object
 */
function searchDesigner(search) {
  var refinements = search.refinements;
  var refinement;
  var refinementValues;
  var SortedMap = require('dw/util/SortedMap');
  var URLUtils = require('dw/web/URLUtils');
  var brandsMappings = new SortedMap();
  for (var i = 0; i < refinements.length; i++) {
    refinement = refinements[i];
    if (refinement.isAttributeRefinement && refinement.attributeID.toLowerCase() === 'brand') {
      refinementValues = refinement.values;
      break;
    }
  }
  var ArrayList = require('dw/util/ArrayList');
  if (refinementValues) {
    var categoryID = search.category && search.category.id != 'root' && search.category.id != 'brand' ? search.category.id : 'brand';
    refinementValues.forEach(function (refinementValue) {
      var brands = new ArrayList();
      refinementValue.url = URLUtils.https('Search-Show', 'cgid', categoryID, 'prefn1', 'brand', 'prefv1', refinementValue.value);
      var firstChar = refinementValue.displayValue.charAt(0).toUpperCase();
      if (/[^a-zA-Z]+/.test(firstChar)) {
        firstChar = 'non-alpha';
      }
      if (brandsMappings != null && brandsMappings.get(firstChar) != null) {
        brands.addAll(brandsMappings.get(firstChar));
        brands.add(refinementValue);
      } else {
        brands.add(refinementValue);
      }
      brandsMappings.put(firstChar, brands);
    });
    return brandsMappings;
  }
}

/**
 * Identifies the category level
 * @param {dw.catalog.Category} category - A single category
 * @param {number} level - Level of the category at the initial stage
 * @returns {number} level - Level of the category
 */
function getCategoryLevel(category, level) {
  if (!category.root) {
    return getCategoryLevel(category.parent, level + 1);
  }
  return level;
}

/**
 * Get category url
 * @param {dw.catalog.Category} category - Current category
 * @returns {string} - Url of the category
 */
function getCategoryUrl(category) {
  var URLUtils = require('dw/web/URLUtils');
  return category.custom && 'alternativeUrl' in category.custom && category.custom.alternativeUrl
    ? category.custom.alternativeUrl.markup
    : URLUtils.url('Search-Show', 'cgid', category.getID()).toString();
}

/**
 * Converts a given category from dw.catalog.Category to plain object
 * @param {dw.catalog.Category} category - A single category
 * @param {boolean} ignoreOnlineProductsCheck - Ignore chec if there's online products or not
 * @returns {Object} plain object that represents a category
 */
function categoryToObject(category, ignoreOnlineProductsCheck) {
  var collections = require('*/cartridge/scripts/util/collections');
  var result = {
    name:
      'categoryh1title' in category.custom && category.custom.categoryh1title
        ? category.custom.categoryh1title
        : 'categoryNameoverwrite' in category.custom && !!category.custom.categoryNameoverwrite
          ? category.custom.categoryNameoverwrite
          : category.getDisplayName(),
    url: getCategoryUrl(category),
    id: category.ID,
    level: getCategoryLevel(category, 0),
    showInMens: 'showInMens' in category.custom && category.custom.showInMens ? category.custom.showInMens : false,
    showInWomens: 'showInWomens' in category.custom && category.custom.showInWomens ? category.custom.showInWomens : false,
    showInDesktopNav: 'showInDesktopNav' in category.custom && category.custom.showInDesktopNav ? category.custom.showInDesktopNav : false,
    showInMobileNav: 'showInMobileNav' in category.custom && category.custom.showInMobileNav ? category.custom.showInMobileNav : false,
    showInRefinementMenu: 'showInRefinementMenu' in category.custom && category.custom.showInRefinementMenu ? category.custom.showInRefinementMenu : false,
    isClickable: 'isClickable' in category.custom && category.custom.isClickable ? category.custom.isClickable : false,
    OnlineWithoutProduct: 'OnlineWithoutProduct' in category.custom && category.custom.OnlineWithoutProduct ? category.custom.OnlineWithoutProduct : false,
    hexColor: 'hexColor' in category.custom && category.custom.hexColor ? category.custom.hexColor : '',
    imageIconnexttocategory:
      'imageIconnexttocategory' in category.custom && category.custom.imageIconnexttocategory ? category.custom.imageIconnexttocategory : null,
    iconCategorytitlereplacement:
      'iconCategorytitlereplacement' in category.custom && category.custom.iconCategorytitlereplacement ? category.custom.iconCategorytitlereplacement : null,
    columnNumber: 'columnNumber' in category.custom && category.custom.columnNumber ? category.custom.columnNumber : null,
    contentInnav1: 'contentInnav1' in category.custom && category.custom.contentInnav1 ? category.custom.contentInnav1 : null,
    contentInnav2: 'contentInnav2' in category.custom && category.custom.contentInnav2 ? category.custom.contentInnav2 : null,
    contentInnav3: 'contentInnav3' in category.custom && category.custom.contentInnav3 ? category.custom.contentInnav3 : null,
    contentInnav4: 'contentInnav4' in category.custom && category.custom.contentInnav4 ? category.custom.contentInnav4 : null,
    contentInnav5: 'contentInnav5' in category.custom && category.custom.contentInnav5 ? category.custom.contentInnav5 : null,
    contentTemplatesize1: 'contentTemplatesize1' in category.custom && category.custom.contentTemplatesize1 ? category.custom.contentTemplatesize1 : null,
    contentTemplatesize2: 'contentTemplatesize2' in category.custom && category.custom.contentTemplatesize2 ? category.custom.contentTemplatesize2 : null,
    contentTemplatesize3: 'contentTemplatesize3' in category.custom && category.custom.contentTemplatesize3 ? category.custom.contentTemplatesize3 : null,
    contentTemplatesize4: 'contentTemplatesize4' in category.custom && category.custom.contentTemplatesize4 ? category.custom.contentTemplatesize4 : null,
    contentTemplatesize5: 'contentTemplatesize5' in category.custom && category.custom.contentTemplatesize5 ? category.custom.contentTemplatesize5 : null
  };
  var subCategories = category.hasOnlineSubCategories() ? category.getOnlineSubCategories() : null;

  if (subCategories) {
    collections.forEach(subCategories, function (subcategory) {
      var converted = null;
      if (((subcategory.hasOnlineProducts() || subcategory.hasOnlineSubCategories()) && isAnyValidProductExists(subcategory)) || ignoreOnlineProductsCheck) {
        converted = categoryToObject(subcategory, ignoreOnlineProductsCheck);
      }
      if (converted) {
        if (!result.subCategories) {
          result.subCategories = [];
        }
        result.subCategories.push(converted);
      }
    });
    if (result.subCategories) {
      result.complexSubCategories = result.subCategories.some(function (item) {
        return !!item.subCategories;
      });
    }
  }

  return result;
}

/**
 * Validates atleast only one valid should exists in category
 * @param {Object} item dw product line item
 * @returns {boolean} isValid validates at least one product should exits
 */
function isAnyValidProductExists(category) {
  if (!category || !category.ID || !category.online) {
    return false;
  }
  var ProductSearchModel = require('dw/catalog/ProductSearchModel');
  var productSearchModel = new ProductSearchModel();
  productSearchModel.setCategoryID(category.ID);
  productSearchModel.search();
  return productSearchModel.getProductSearchHits().hasNext();
}

/**
 * Represents a single category with all of it's children
 * @param {dw.util.ArrayList<dw.catalog.Category>} items - Top level categories
 * @constructor
 */
function categories(items) {
  var collections = require('*/cartridge/scripts/util/collections');
  var categoriesList = [];
  collections.forEach(items, function (item) {
    if ((item.hasOnlineProducts() || item.hasOnlineSubCategories()) && isAnyValidProductExists(item)) {
      categoriesList.push(categoryToObject(item));
    }
  });
  return categoriesList;
}

/**
 * Identifies and returns L1 category for the input category passed
 * @param {dw/catalog/Category} parentCatLevel - Top level categories
 * @param {string} name - Name of the current category
 * @returns {dw/catalog/Category} category which is an L1
 */
function getRootCat(parentCatLevel, name) {
  if (
    (name && parentCatLevel && parentCatLevel.displayName && name.toLowerCase() === parentCatLevel.displayName.toLowerCase()) ||
    parentCatLevel.getParent().isRoot()
  ) {
    return parentCatLevel;
  }
  return getRootCat(parentCatLevel.getParent(), name); // statement 2
}

/**
 * Identifies and returns L1 category for the input category passed
 * @param {dw/catalog/Category} category - Top level categories
 * @param {string} name - Name of the category
 * @returns {dw/catalog/Category} category which is an L1
 */
function getL1CategoryDetails(category, name) {
  return getRootCat(category, name);
}

/**
 * Check if current category or any of its parent category is Enabled on *enablePreferenceCenterCategoriesSizes* Custom pref
 * @param {string} categoryID - Top level categories
 * @returns {{Object}} showMySizes
 */
function showMySizesOnPLP(categoryID) {
  var Site = require('dw/system/Site');
  var CatalogMgr = require('dw/catalog/CatalogMgr');
  var category = CatalogMgr.getCategory(categoryID);
  var categoryToAddInto = '';
  var customPreferences = Site.current.preferences.custom;
  var showMySizes = false;
  var showMySizesObj = {};
  if ('enablePreferenceCenterCategoriesSizes' in customPreferences) {
    var SizeIDsPreferences = customPreferences.enablePreferenceCenterCategoriesSizes;
    //check if *category.id* exist in *SizeIDsPreferences*
    if (SizeIDsPreferences.indexOf(category.ID) !== -1) {
      showMySizes = true;
      categoryToAddInto = category.ID;
    } else {
      //Loop through parent categories until we reach root (Except ROOT)
      var parentCategoryID;
      if (category.parent) {
        parentCategoryID = category.parent.ID;
      } else {
        parentCategoryID = category.ID;
      }
      while (parentCategoryID !== 'root') {
        if (SizeIDsPreferences.indexOf(parentCategoryID) !== -1) {
          showMySizes = true;
          categoryToAddInto = parentCategoryID;
          break;
        } else {
          var currentParentCategory = CatalogMgr.getCategory(parentCategoryID);
          parentCategoryID = currentParentCategory.parent.ID;
        }
      }
    }
  }
  showMySizesObj['showMySizes'] = showMySizes;
  showMySizesObj['categoryToAddInto'] = categoryToAddInto;

  return showMySizesObj;
}

/**
 * Sort Object by Alphabetically
 * @param {Object} obj - Object to be Sorted
 * @returns {Object} object  - Sorted Object
 */
function sortObjectAlphabetically(obj) {
  var keys = [];
  var sortedObject = {};
  for (var k in obj) {
    if (obj.hasOwnProperty(k)) {
      keys.push(k);
    }
  }
  keys.sort();
  for (var i = 0; i < keys.length; i++) {
    k = keys[i];
    sortedObject[k] = obj[k];
  }
  return sortedObject;
}

function isKnownUser(session) {
  var knownUser = false;

  if (session.raw.custom && 'knownUser' in session.raw.custom && session.raw.custom.knownUser === true) {
    knownUser = true;
  } else {
    var SuggestModel = require('dw/suggest/SuggestModel');
    var suggestions = new SuggestModel();
    suggestions.setSearchPhrase('');
    suggestions.setMaxSuggestions(4);

    var recentSuggestionsAvailable = suggestions.recentSearchPhrases.hasNext();
    if (recentSuggestionsAvailable) {
      knownUser = true;
    } else {
      var BasketMgr = require('dw/order/BasketMgr');
      if (BasketMgr.getCurrentBasket()) {
        knownUser = true;
      }
    }
    session.raw.custom.knownUser = knownUser;
  }
  return knownUser;
}
exports.setupSearch = setupSearch;
exports.getCategoryTemplate = getCategoryTemplate;
exports.setupContentSearch = setupContentSearch;
exports.getCategoryUrl = getCategoryUrl;
exports.search = search;
exports.applyCache = applyCache;
exports.searchDesigner = searchDesigner;
exports.categories = categories;
exports.getL1CategoryDetails = getL1CategoryDetails;
exports.showMySizesOnPLP = showMySizesOnPLP;
exports.sortObjectAlphabetically = sortObjectAlphabetically;
exports.isKnownUser = isKnownUser;