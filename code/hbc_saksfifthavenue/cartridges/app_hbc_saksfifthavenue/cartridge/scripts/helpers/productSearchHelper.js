'use strict';
var collections = require('*/cartridge/scripts/util/collections');
var preferences = require('*/cartridge/config/preferences');
var LOAD_PAGE_SIZE = preferences.defaultPageSize ? preferences.defaultPageSize : 24;

/**
 * funtion to obtain the ingrid slot object
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @returns {Object} Ingrid slot detailed object
 */
function getGridSlot(productSearch) {
  // ----code for ingrid slots
  var gridSlot1;
  var gridSlot2;
  var gridSlot3;
  var gridSlot1Content;
  var gridSlot2Content;
  var gridSlot3Content;
  var gridSlot1Size;
  var gridSlot2Size;
  var gridSlot3Size;
  var gridSlot1Placement;
  var gridSlot2Placement;
  var gridSlot3Placement;
  if (productSearch.category !== null) {
    // in grid content asset names
    gridSlot1Content =
      'ingrid-1-categoryName' in productSearch.category.custom && productSearch.category.custom['ingrid-1-categoryName']
        ? productSearch.category.custom['ingrid-1-categoryName']
        : '';
    gridSlot2Content =
      'ingrid-2-categoryName' in productSearch.category.custom && productSearch.category.custom['ingrid-2-categoryName']
        ? productSearch.category.custom['ingrid-2-categoryName']
        : '';
    gridSlot3Content =
      'ingrid-3-categoryName' in productSearch.category.custom && productSearch.category.custom['ingrid-3-categoryName']
        ? productSearch.category.custom['ingrid-3-categoryName']
        : '';

    // in grid content size
    gridSlot1Size =
      'in-grid-size-1' in productSearch.category.custom && productSearch.category.custom['in-grid-size-1']
        ? productSearch.category.custom['in-grid-size-1'].value
        : 0;
    gridSlot2Size =
      'in-grid-size-2' in productSearch.category.custom && productSearch.category.custom['in-grid-size-2']
        ? productSearch.category.custom['in-grid-size-2'].value
        : 0;
    gridSlot3Size =
      'in-grid-size-3' in productSearch.category.custom && productSearch.category.custom['in-grid-size-3']
        ? productSearch.category.custom['in-grid-size-3'].value
        : 0;

    // in grid content placement
    gridSlot1Placement =
      'ingridplacement1' in productSearch.category.custom && productSearch.category.custom.ingridplacement1
        ? productSearch.category.custom.ingridplacement1
        : '';
    gridSlot2Placement =
      'ingridplacement2' in productSearch.category.custom && productSearch.category.custom.ingridplacement2
        ? productSearch.category.custom.ingridplacement2
        : '';
    gridSlot3Placement =
      'ingridplacement3' in productSearch.category.custom && productSearch.category.custom.ingridplacement3
        ? productSearch.category.custom.ingridplacement3
        : '';

    gridSlot1 = !!gridSlot1Content && !!gridSlot1Placement && !!gridSlot1Size;
    gridSlot2 = !!gridSlot2Content && !!gridSlot2Placement && !!gridSlot2Size;
    gridSlot3 = !!gridSlot3Content && !!gridSlot3Placement && !!gridSlot3Size;
  }
  return {
    gridSlot1Data: {
      gridSlot1: gridSlot1,
      gridSlot1Content: gridSlot1Content,
      gridSlot1Placement: gridSlot1Placement,
      gridSlot1Size: gridSlot1Size
    },
    gridSlot2Data: {
      gridSlot2: gridSlot2,
      gridSlot2Content: gridSlot2Content,
      gridSlot2Placement: gridSlot2Placement,
      gridSlot2Size: gridSlot2Size
    },
    gridSlot3Data: {
      gridSlot3: gridSlot3,
      gridSlot3Content: gridSlot3Content,
      gridSlot3Placement: gridSlot3Placement,
      gridSlot3Size: gridSlot3Size
    }
  };
}

/**
 * Function to obtain product grid with in grid slots on the page
 *
 * @param {Iterator} prodItr - product search results iterator
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {string} startIdx - start index of current page
 * @param {Object} httpParams - http query string parameter
 * @returns {Object} - Product grid to be shown on the page
 */
function getProductGridObject(prodItr, productSearch, startIdx, httpParams, isRefinedSearch, filterSearch) {
  var ContentMgr = require('dw/content/ContentMgr');

  var productGrid = {};

  // full collection of all the products
  var isPreOrderSearch = false;
  var productGridObject = collections.map(prodItr, function (item) {
    // This could impact the search page performance - REMOVE it if not required later
    return {
      productID: item.productID,
      productSearchHit: item,
      frpid: item.firstRepresentedProduct.ID
    };
  });
  if (filterSearch) {
    productGrid.isPreOrderSearch = true;
  } else {
    productGrid.isPreOrderSearch = isPreOrderSearch;
  }
  // return the product grid ony if any filter is applied
  if (httpParams.filter || productSearch.count === 0) {
    productGrid.productGridData = productGridObject;
    productGrid.isPreOrderSearch = true;
    return productGrid;
  }

  // fetch the ingrid data
  var gridSlotData = getGridSlot(productSearch);
  var content;

  var productGridData = [];
  var productNumber = 0;
  var startTileNumber = Math.ceil(Number(startIdx) / LOAD_PAGE_SIZE) * LOAD_PAGE_SIZE;
  for (var i = 0; i < LOAD_PAGE_SIZE; i++) {
    if (
      !isRefinedSearch &&
      !httpParams.filter &&
      gridSlotData !== null &&
      gridSlotData.gridSlot1Data !== null &&
      !!gridSlotData.gridSlot1Data.gridSlot1 &&
      gridSlotData.gridSlot1Data.gridSlot1Placement <= startTileNumber + LOAD_PAGE_SIZE &&
      gridSlotData.gridSlot1Data.gridSlot1Placement === startTileNumber + i + 1
    ) {
      content = ContentMgr.getContent(gridSlotData.gridSlot1Data.gridSlot1Content);
      productGridData.push({
        gridSlotContent: {
          contentInnavscene7urllink: !!content ? content.custom.contentInnavscene7urllink : '',
          navHeader: !!content ? content.custom.navHeader : '',
          navSubHeader: !!content ? content.custom.navSubHeader : '',
          contentInnavurllink: !!content ? content.custom.contentInnavurllink : '',
          navtext: !!content ? content.custom.navtext : ''
        },
        gridSlotSize: gridSlotData.gridSlot1Data.gridSlot1Size
      });
      if (gridSlotData.gridSlot1Data.gridSlot1Size >1){
        i = i + gridSlotData.gridSlot1Data.gridSlot1Size -1;
      }
    } else if (
      !isRefinedSearch &&
      !httpParams.filter &&
      gridSlotData !== null &&
      gridSlotData.gridSlot2Data !== null &&
      !!gridSlotData.gridSlot2Data.gridSlot2 &&
      gridSlotData.gridSlot2Data.gridSlot2Placement <= startTileNumber + LOAD_PAGE_SIZE &&
      gridSlotData.gridSlot2Data.gridSlot2Placement === startTileNumber + i + 1
    ) {
      content = ContentMgr.getContent(gridSlotData.gridSlot2Data.gridSlot2Content);
      productGridData.push({
        gridSlotContent: {
          contentInnavscene7urllink: !!content ? content.custom.contentInnavscene7urllink : '',
          navHeader: !!content ? content.custom.navHeader : '',
          navSubHeader: !!content ? content.custom.navSubHeader : '',
          contentInnavurllink: !!content ? content.custom.contentInnavurllink : '',
          navtext: !!content ? content.custom.navtext : ''
        },
        gridSlotSize: gridSlotData.gridSlot2Data.gridSlot2Size
      });
      if (gridSlotData.gridSlot2Data.gridSlot2Size >1){
        i = i + gridSlotData.gridSlot2Data.gridSlot2Size -1;
      }
    } else if (
      !isRefinedSearch &&
      !httpParams.filter &&
      gridSlotData !== null &&
      gridSlotData.gridSlot3Data !== null &&
      !!gridSlotData.gridSlot3Data.gridSlot3 &&
      gridSlotData.gridSlot3Data.gridSlot3Placement <= startTileNumber + LOAD_PAGE_SIZE &&
      gridSlotData.gridSlot3Data.gridSlot3Placement === startTileNumber + i + 1
    ) {
      content = ContentMgr.getContent(gridSlotData.gridSlot3Data.gridSlot3Content);
      productGridData.push({
        gridSlotContent: {
          contentInnavscene7urllink: !!content ? content.custom.contentInnavscene7urllink : '',
          navHeader: !!content ? content.custom.navHeader : '',
          navSubHeader: !!content ? content.custom.navSubHeader : '',
          contentInnavurllink: !!content ? content.custom.contentInnavurllink : '',
          navtext: !!content ? content.custom.navtext : ''
        },
        gridSlotSize: gridSlotData.gridSlot3Data.gridSlot3Size
      });
      if (gridSlotData.gridSlot3Data.gridSlot3Size >1){
        i = i + gridSlotData.gridSlot3Data.gridSlot3Size -1;
      }
    } else {
      // sometimes productGridObject length and productNumber get out of sync
      let productGridObjectData = productGridObject[productNumber];
      if (!empty(productGridObjectData)) {
        productGridData.push({
          // if ingrid data is not to be displayed at that tile then display the product tile
          productID: productGridObjectData.productID,
          productSearchHit: productGridObjectData.productSearchHit,
          frpid: productGridObjectData.frpid
        });
      }
      productNumber++;
      if (productNumber === productGridObject.length) {
        break;
      }
    }
  }
  productGrid.productGridData = productGridData;
  return productGrid;
}

/**
 * Function to get the current product size on the page based on the in grids
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - product search object
 * @param {Object} httpParams - http query string parameter
 * @param {string} startIdx - start index of the current page
 * @returns {number} - total number of product tiles excluding the ingrid slots to be shown on the page
 */
function getPageSize(productSearch, httpParams, startIdx) {
  // commenting out this code as it seems redundant; leave here for the time being; will remove after testing
  // fetch the gridSlotData
  // var gridSlotData = getGridSlot(productSearch);

  // check id the particular gridSlot is available
  // var gridSlot1 = gridSlotData !== null && gridSlotData.gridSlot1Data !== null && !!gridSlotData.gridSlot1Data.gridSlot1;
  // var gridSlot2 = gridSlotData !== null && gridSlotData.gridSlot2Data !== null && !!gridSlotData.gridSlot2Data.gridSlot2;
  // var gridSlot3 = gridSlotData !== null && gridSlotData.gridSlot3Data !== null && !!gridSlotData.gridSlot3Data.gridSlot3;

  // calculate initial pageSize without in grids
  var pageSize = parseInt(httpParams.sz, 10) || LOAD_PAGE_SIZE;

  // var startTileNumber = Math.ceil(Number(startIdx) / LOAD_PAGE_SIZE) * LOAD_PAGE_SIZE;
  // based on the ingrid placement and size, calculate the current pageSize for product grid
  // if (!!gridSlot1 || !!gridSlot2 || !!gridSlot3) {
  //     pageSize = pageSize - (!!gridSlot1 && (gridSlotData.gridSlot1Data.gridSlot1Placement <= startTileNumber + LOAD_PAGE_SIZE && gridSlotData.gridSlot1Data.gridSlot1Placement >= startTileNumber) ? gridSlotData.gridSlot1Data.gridSlot1Size : 0) - (!!gridSlot2 && (gridSlotData.gridSlot2Data.gridSlot2Placement <= startTileNumber + LOAD_PAGE_SIZE && gridSlotData.gridSlot2Data.gridSlot2Placement >= startTileNumber) ? gridSlotData.gridSlot2Data.gridSlot2Size : 0) - (!!gridSlot3 && (gridSlotData.gridSlot3Data.gridSlot3Placement <= startTileNumber + LOAD_PAGE_SIZE && gridSlotData.gridSlot3Data.gridSlot3Placement >= startTileNumber) ? gridSlotData.gridSlot3Data.gridSlot3Size : 0);
  // }
  return pageSize;
}

module.exports = {
  getGridSlot: getGridSlot,
  getProductGridObject: getProductGridObject,
  getPageSize: getPageSize
};
