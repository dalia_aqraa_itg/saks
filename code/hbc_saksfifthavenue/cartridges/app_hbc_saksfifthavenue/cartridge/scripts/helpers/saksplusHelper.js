'use strict';

var Logger = require('dw/system/Logger');
var saksplusLogger = Logger.getLogger('SAKSPLUS', 'SAKSPLUS');

/**
 * returns the saksplus product id
 * @returns {string} saksPlusProductID -saksPlusProductID
 */
function getSaksPlusProductID() {
  var Site = require('dw/system/Site');
  var saksPlusProductID = 'saksPlusProductID' in Site.current.preferences.custom ? Site.current.preferences.custom.saksPlusProductID : '';
  return saksPlusProductID;
}

/**
 * validates that the product line items exist, and has saks plus product in basket.
 * @param {dw.order.Basket} basket - The current user's basket
 * @returns {boolean} hasSaksPlusInBasket - hasSaksPlusInBasket
 */
function hasSaksPlusInBasket(basket) {
  var hasSaksPlus = false;
  var collections = require('*/cartridge/scripts/util/collections');
  if (basket) {
    var plis = basket.getAllProductLineItems();
    var saksPlusProductID = getSaksPlusProductID();
    collections.forEach(plis, function (pli) {
      if (pli.product && pli.product.ID === saksPlusProductID) {
        hasSaksPlus = true;
      }
    });
  }

  // set the session atttribute when customer is a guest user to get the free shipping and promotion benefits during saksplus purchase also.
  session.custom.saksplusProduct = hasSaksPlus;

  return hasSaksPlus;
}

/**
 * validates that the product line items exist, and has saks plus product in basket.
 * @param {dw.customer.Customer} customer - Customer
 */
function updateSaksPlusAttrs(customer) {
  var Transaction = require('dw/system/Transaction');
  var profile = customer ? customer.profile : null;
  var Site = require('dw/system/Site');
  var Calendar = require('dw/util/Calendar');
  var saksEligibilityInDays = Number('saksEligibilityInDays' in Site.current.preferences.custom ? Site.current.preferences.custom.saksEligibilityInDays : '7');
  var days = 60 * 60 * 1000 * 24 * saksEligibilityInDays;
  var today = new Date();
  var saksEligibilityInDaysDate = new Date(today.setTime(today.getTime() + days));
  try {
    if (profile) {
      var curMembershipExpirationDateStr = 'saks+ExpirationDate' in profile.custom ? profile.custom['saks+ExpirationDate'] : null;
      Transaction.wrap(function () {
        if (curMembershipExpirationDateStr) {
          var curMembershipExpirationDate = new Date(curMembershipExpirationDateStr);
          curMembershipExpirationDate = new Date(curMembershipExpirationDate.setTime(curMembershipExpirationDate.getTime() - days));
          var curExpirationCal = new Calendar(curMembershipExpirationDate);
          if (curExpirationCal.compareTo(new Calendar()) !== 1) {
            profile.custom['saks+ExpirationDate'] = saksEligibilityInDaysDate.toString();
          }
        } else {
          profile.custom['saks+ExpirationDate'] = saksEligibilityInDaysDate.toString();
        }
        profile.custom['saks+Member'] = true;
      });
    }
  } catch (e) {
    var errorMsg = e.fileName + ' | line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack; // eslint-disable-line
    saksplusLogger.error(errorMsg);
  }
}

/**
 * Check the eligibility of the profile during Login and on Checkout
 * @param {dw.customer.Customer} customer - Customer
 */
function checkEligibilty(customer) {
  var Transaction = require('dw/system/Transaction');
  var profile = customer ? customer.profile : null;
  var Calendar = require('dw/util/Calendar');
  try {
    if (profile) {
      var curMembershipExpirationDateStr = 'saks+ExpirationDate' in profile.custom ? profile.custom['saks+ExpirationDate'] : null;
      Transaction.wrap(function () {
        if (curMembershipExpirationDateStr) {
          var regex = /^(\d\d-)[a-zA-Z]{3,}(-\d\d)$/;
          if (regex.test(curMembershipExpirationDateStr)) {
            //looking for value to be 31-Jan-21
            var arrDateParts = curMembershipExpirationDateStr.split('-');
            var currentDate = new Date();
            var curMembershipExpirationDate = new Date(
              arrDateParts[1] + ' ' + arrDateParts[0] + ' ' + currentDate.getFullYear().toString().substring(0, 2) + arrDateParts[2]
            );
          } else {
            var curMembershipExpirationDate = new Date(curMembershipExpirationDateStr);
          }
          var curExpirationCal = new Calendar(curMembershipExpirationDate);

          if (curExpirationCal.compareTo(new Calendar()) !== 1) {
            profile.custom['saks+Member'] = false;
          }
        } else {
          profile.custom['saks+Member'] = false;
        }
      });
    }
  } catch (e) {
    var errorMsg = e.fileName + ' | line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack; // eslint-disable-line
    saksplusLogger.error(errorMsg);
  }
}

/**
 * Check the profile if saks + eligible
 * @param {dw.customer.Customer} customer - Customer
 */
function isSaksPlusMember(customer) {
  var profile = customer ? customer.profile : null;
  var saksMember = false;
  try {
    if (('saksplusProduct' in session.custom && session.custom.saksplusProduct) || (profile && profile.custom['saks+Member'])) {
      saksMember = true;
    }
  } catch (e) {
    var errorMsg = e.fileName + ' | line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack; // eslint-disable-line
    saksplusLogger.error(errorMsg);
  }
  return saksMember;
}

module.exports = {
  getSaksPlusProductID: getSaksPlusProductID,
  hasSaksPlusInBasket: hasSaksPlusInBasket,
  updateSaksPlusAttrs: updateSaksPlusAttrs,
  checkEligibilty: checkEligibilty,
  isSaksPlusMember: isSaksPlusMember
};
