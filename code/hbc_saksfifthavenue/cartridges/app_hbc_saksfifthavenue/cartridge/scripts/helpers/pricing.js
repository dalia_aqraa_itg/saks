'use strict';
var base = module.superModule;

// this override is to remove the MSRP percentage display on saks.com
base.calculatePercentOff = function (minPrice, maxPrice) {
  return {};
};

/**
 * Set Applicable Price Books for the current product
 * @param {dw.catalog.ProductPriceModel} priceModel Product price model
 */
base.setPriceBooksPrice = function (priceModel) {
  var PriceBookMgr = require('dw/catalog/PriceBookMgr');
  var collections = require('*/cartridge/scripts/util/collections');
  var priceBookList = [];

  var httpPath = request.httpPath;
  var applyPrivatePricebook =
    httpPath.indexOf('Product-Show') > -1 ||
    httpPath.indexOf('Product-AvailabilityAjax') > -1 ||
    httpPath.indexOf('Product-Variation') > -1 ||
    httpPath.indexOf('Product-ShowQuickView') > -1 ||
    httpPath.indexOf('Search-Show') > -1 ||
    httpPath.indexOf('Wishlist-Show') > -1 ||
    httpPath.indexOf('Wishlist-TileShow') > -1 ||
    httpPath.indexOf('Wishlist-TileShow') > -1 ||
    httpPath.indexOf('Tile-Show') > -1;
  if (!session.privacy.skipPricebookCal && applyPrivatePricebook) {
    var privateSalePromo = base.getPrivateSalePromo();
    if (privateSalePromo) {
      var privateSalePB = 's5a-private-sale';
      var privateSalePriceBook = PriceBookMgr.getPriceBook(privateSalePB);
      priceBookList.push(privateSalePriceBook);
    }

    var allPriceBooks = PriceBookMgr.getSitePriceBooks();
    var bookPrice;
    collections.forEach(allPriceBooks, function (priceBook) {
      var priceBookPrice = priceModel.getPriceBookPrice(priceBook.ID);
      if (priceBookPrice && priceBookPrice.available && priceBook.parentPriceBook) {
      if (!bookPrice) {
          bookPrice = priceBookPrice;
        }
        priceBookList.push(PriceBookMgr.getPriceBook(priceBook.ID));
      }
    });

    PriceBookMgr.setApplicablePriceBooks(priceBookList);
  }
};

module.exports = base;
