'use strict';

var base = module.superModule;
var HashMap = require('dw/util/HashMap');
var bopisHelper = require('*/cartridge/scripts/helpers/bopisHelpers');
var preferences = require('*/cartridge/config/preferences');
var Resource = require('dw/web/Resource');
var Logger = require('dw/system/Logger');

/**
 * create the stores results html
 * @param {Array} storesInfo - an array of objects that contains store information
 * @returns {string} The rendered HTML
 */
function createStoresResultsHtml(storesInfo, postalCode) {
  var HashMap = require('dw/util/HashMap');
  var Template = require('dw/util/Template');
  var URLUtils = require('dw/web/URLUtils');
  var myStore = null;
  var currentCustomerProfile = customer.profile;
  var context = new HashMap();
  var object = { stores: { stores: storesInfo, setMyStoreUrl: URLUtils.url('Stores-SetMyHomeStore').toString() } };

  if (currentCustomerProfile && currentCustomerProfile.custom.hasOwnProperty('myDefaultStore') && currentCustomerProfile.custom.myDefaultStore) {
    myStore = currentCustomerProfile.custom.myDefaultStore;
  } else {
    var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
    var homeStore = cookiesHelper.read('homeStore');
    if (homeStore) {
      myStore = homeStore;
    }
  }

  Object.keys(object).forEach(function (key) {
    context.put(key, object[key]);
  });
  context.myStore = myStore;
  context.distance = preferences.defaultStoreLookUpRadius;
  context.bopisPostal = postalCode ? postalCode : request.geolocation.postalCode;
  var template = new Template('storeLocator/storeLocatorResults');
  return template.render(context).text;
}

/**
 * Searches for stores and creates a plain object of the stores returned by the search
 * @param {string} radius - selected radius
 * @param {string} postalCode - postal code for search
 * @param {string} lat - latitude for search by latitude
 * @param {string} long - longitude for search by longitude
 * @param {Object} geolocation - geloaction object with latitude and longitude
 * @param {boolean} showMap - boolean to show map
 * @param {dw.web.URL} url - a relative url
 * @param {[Object]} products - an array of product ids to quantities that needs to be filtered by.
 * @returns {Object} a plain object containing the results of the search
 */
function getStores(radius, postalCode, lat, long, geolocation, showMap, url, products) {
  var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
  var StoresModel = require('*/cartridge/models/stores');
  var getProduct = require ('dw/catalog/ProductMgr');
  var StoreMgr = require('dw/catalog/StoreMgr');
  var Site = require('dw/system/Site');
  var URLUtils = require('dw/web/URLUtils');

  var countryCode = require('*/cartridge/config/preferences').countryCode.value; // DEBUG: var countryCode = geolocation.countryCode;
  var queryString = '';
  var distanceUnit = countryCode === 'US' ? 'mi' : 'km';
  var defaultRadius = preferences.defaultStoreLookUpRadius;
  var resolvedRadius = radius ? parseInt(radius, 10) : defaultRadius;
  var searchKey = {};
  var formattedCityEntry = {};
  var storeMgrResult = null;
  var location = {};
  var distanceCollection = new HashMap();

  // find by coordinates (detect location)
  location.lat = lat && long ? parseFloat(lat) : geolocation.latitude;
  location.long = long && lat ? parseFloat(long) : geolocation.longitude;

  if (postalCode && postalCode !== '') {
    // find by postal code
    searchKey = postalCode.toString().trim().toUpperCase();
    storeMgrResult = StoreMgr.searchStoresByPostalCode(countryCode, searchKey, distanceUnit, resolvedRadius);
    searchKey = {
      postalCode: searchKey
    };
    // search with city if not found with postal
    if (storeMgrResult.size() === 0) {
      // convert user entered city to all cases and execute search for better results
      formattedCityEntry = bopisHelper.convertCases(postalCode);

      // To make Store search by City similar to look-up by Postal Code, the City-postal code mapping is maintained in a properties file. SFDEV-7932
      var lowercaseCity = formattedCityEntry.lowercase;
      var finalCity = lowercaseCity;
      if (lowercaseCity.indexOf(' ') > 0) {
        //logic to remove any space between the city name before looking up in the properties file
        finalCity = '';
        var splitCity = lowercaseCity.split(' ');
        for (i = 0; i < splitCity.length; i++) {
          finalCity = finalCity + splitCity[i];
        }
        finalCity = finalCity.trim();
      }

      var mappedPostalCode = Resource.msg(finalCity, 'cities', null); // City-postal code mapping is maintained in cities.properties file

      if (mappedPostalCode) {
        storeMgrResult = StoreMgr.searchStoresByPostalCode(countryCode, mappedPostalCode, distanceUnit, resolvedRadius);
      }
    }
  } else {
    storeMgrResult = StoreMgr.searchStoresByCoordinates(location.lat, location.long, distanceUnit, resolvedRadius);
    searchKey = {
      lat: location.lat,
      long: location.long
    };
  }

  var actionUrl = url || URLUtils.url('Stores-FindStores', 'showMap', showMap).toString();
  var apiKey = Site.getCurrent().getCustomPreferenceValue('mapAPI');
  var apiStores = storeMgrResult.keySet();
  // prepare a store distance map
  Object.keys(apiStores).forEach(function (key) {
    var apiStore = apiStores[key];
    return distanceCollection.put(apiStore.ID, storeMgrResult.get(apiStore));
  });
  var storesModel = new StoresModel(storeMgrResult.keySet(), searchKey, resolvedRadius, actionUrl, apiKey);
  storesModel.stores = storesModel.stores.filter(function (store) {
    if (store.storeClosed === 'Yes') {
      return false;
    }
    if (distanceCollection) {
      var distance = distanceCollection.get(store.ID);
      // property is defined without a prototype since no qualifiers are required
      distance = distance ? Number(distance).toFixed(2) : distance;
      store.distanceInUnits = distance + ' ' + distanceUnit; // eslint-disable-line
    }
    if (products) {
      var storeInventoryListId = store.inventoryListId;
      if (storeInventoryListId) {
        var storeInventory = ProductInventoryMgr.getInventoryList(storeInventoryListId);
        if (storeInventory) {
          return products.every(function (product) {
            var inventoryRecord = storeInventory.getRecord(product.id);
            // properties are defined without a prototype since no qualifiers are required
            if (inventoryRecord && inventoryRecord.ATS.value) {
              // set available units
              store.unitsAtStores = inventoryRecord.ATS.value; // eslint-disable-line
            }
            // set quantity messaging
            var product2 = getProduct.getProduct(product.id);
            var isMaster = product2.master;
            if (product2 && isMaster) { 
              store.availabilityMessage = '';
              return true;
            }
            else {  
              store.availabilityMessage = bopisHelper.setAvailabilityMessage(store, product); // eslint-disable-line
              return true;
            }
          });
        }
      } // Returning true to show all the Stores - fix for ticket SFDEV-7688

      for (var i = 0 ; i < products.length ; i++) {
        var product = getProduct.getProduct(products[i].id);
        var isMaster = product.master;
        if (product && isMaster) { 
          store.availabilityMessage = '';
          return true;
        }
        else {
          store.availabilityMessage = Resource.msg('store.unitsavailable.unavailable', 'storeLocator', null);
          return true;
        }
      }
    }
    return true;
  });
  // re-assign html to include post created store model properties in to view
  storesModel.storesResultsHtml = createStoresResultsHtml(storesModel.stores, postalCode);

  return storesModel;
}

/**
 * search for the eligible store
 * @param {string} zipCode - Store ID
 * @param {Object} product - Store distance
 */
function getEligibleStoreForSDD(zipCode, product) {
  var MappingMgr = require('dw/util/MappingMgr');
  var MappingKey = require('dw/util/MappingKey');
  var StoreMgr = require('dw/catalog/StoreMgr');
  var instorePUstoreHelpers = require('*/cartridge/scripts/helpers/instorePickupStoreHelpers');
  var isStoreFound = false;
  var zipCodeSDD = zipCode;
  if (zipCodeSDD && zipCodeSDD !== '' && zipCodeSDD.length > 0 && zipCodeSDD.split('-').length > 0) {
    zipCodeSDD = zipCodeSDD.split('-')[0];
  }
  var storeMap = null,
    sddStore = null,
    store = {};
  var desiredQuantity = product && product.desiredQuantity ? product.desiredQuantity : 1;
  store.pdpSDDAvailabilityMessage = Resource.msg('shippingoption.product.noavailablesdd', 'product', null);
  store.cartSDDAvailabilityMessage = '';
  try {
    if (zipCodeSDD && zipCodeSDD !== '' && preferences.zipStoreIdMappingName) {
      storeMap = MappingMgr.get(preferences.zipStoreIdMappingName, new MappingKey(zipCodeSDD));
      if (storeMap && storeMap.storeID) {
        sddStore = StoreMgr.getStore(storeMap.storeID);
        if (sddStore) {
          store.storeid = sddStore.custom.hasOwnProperty('isEnabledforSameDayDelivery') && sddStore.custom.isEnabledforSameDayDelivery ? sddStore.ID : null;
          session.custom.sddstoreid = store.storeid;
          if (product && product.isAvailableForSDD) {
            // property is defined without a prototype since no qualifiers are required
            store.unitsAvailable = instorePUstoreHelpers.getStoreInventory(sddStore.ID, product.id);
            if (store.unitsAvailable >= desiredQuantity) {
              store.pdpSDDAvailabilityMessage = Resource.msg('shippingoption.product.availablesdd', 'product', null);
            } else {
              store.pdpSDDAvailabilityMessage = Resource.msg('shippingoption.product.noavailablesdd', 'product', null);
            }
            if (store.unitsAvailable > 0) {
              store.cartSDDAvailabilityMessage = Resource.msg('shippingoption.cart.availablesdd', 'product', null);
            }
          }
        } else {
          session.custom.sddstoreid = null;
        }
      } else {
        session.custom.sddstoreid = null;
      }
    }
  } catch (e) {
    Logger.error('Error in storeHelpers.js: getEligibleStoreForSDD ' + e.msg);
    return store;
  }

  return store;
}

/**
 * Add or remove the store id in the url
 * @param {dw.web.URLUtils} refinementUrl - request url
 * @returns {dw.web.URLUtils} refinementUrl - url with store id added
 */
function getSDDRefinementUrl(refinementUrl) {
  var sddUrl = refinementUrl;
  if (sddUrl) {
    sddUrl.remove('storeid');
    if (session.custom.sddstoreid && session.custom.sddstoreid !== '') {
      sddUrl.append('storeid', session.custom.sddstoreid);
      sddUrl.append('srchsrc', 'sdd');
    }
  }
  return sddUrl;
}

/**
 * remove the store id in the url
 * @param {dw.web.URLUtils} refinementUrl - request url
 * @returns {dw.web.URLUtils} refinementUrl - url with store id added
 */
function getRefinementUrl(refinementUrl) {
  var resetSDDUrl = refinementUrl;
  if (resetSDDUrl) {
    resetSDDUrl.remove('storeid');
  }
  return resetSDDUrl;
}

/**
 * Returns the store if available from session or with a store search on the Geolocation
 * @param {Object} product - product model
 * @returns {Object} defaultStore - store model
 */
function getDefaultSDDStore(product) {
  var defaultStore = null;
  var defaultSearchRadius = preferences.defaultStoreLookUpRadius;
  // get store from session
  var sessionPostal =
    session.custom.sddpostal && session.custom.sddpostal !== ''
      ? session.custom.sddpostal
      : session.custom.sddpostalCheckout && session.custom.sddpostalCheckout !== ''
      ? session.custom.sddpostalCheckout
      : null;
  // fall back, search store
  if (!sessionPostal) {
    sessionPostal = request.geolocation.postalCode;
  }
  if (sessionPostal) {
    defaultStore = getEligibleStoreForSDD(sessionPostal, product);
  }
  return defaultStore;
}

/**
 * Check if request has search source associated
 * @param {Object} req - request object
 * @returns {boolean} filterApplied - return fileter applied
 */
function isSDDFilterApplied(req) {
  var filterApplied = false;
  Object.keys(req.querystring).forEach(function (element) {
    if (element.indexOf('srchsrc') > -1) {
      filterApplied = true;
    }
  });
  return filterApplied;
}

module.exports = exports = {
  createStoresResultsHtml: createStoresResultsHtml,
  getStores: getStores,
  createChangeStoreHtml: base.createChangeStoreHtml,
  getEligibleStoreForSDD: getEligibleStoreForSDD,
  getSDDRefinementUrl: getSDDRefinementUrl,
  getRefinementUrl: getRefinementUrl,
  getDefaultSDDStore: getDefaultSDDStore,
  isSDDFilterApplied: isSDDFilterApplied
};
