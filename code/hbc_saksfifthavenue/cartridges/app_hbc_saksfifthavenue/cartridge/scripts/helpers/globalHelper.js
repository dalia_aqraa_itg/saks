'use strict';

/**
 * Get sanitzed value and remove special charaters including emojis
 * Usage from OrderAPIUtils.js - To sanitzed the data before passing the XML to OMS
 * Chars to be removed `, <, >, (, ),* , $, @, #,  %, ^,  & , +, ~ ,` \ ,| , /, ?, !'
 * @param {string} value - Value to be sanitized
 * @returns {string} sanitzedValue - Value after sanitzed
 */
function replaceSpecialCharsAndEmojis(value) {
  var sanitzedValue = value.replace(/[^\x20-\x7F]/g, '').replace(/[`<>()*$@#?!%^&+~`\\|/]/g, '');
  return sanitzedValue;
}

module.exports = {
  replaceSpecialCharsAndEmojis: replaceSpecialCharsAndEmojis
};
