'use strict';
var base = module.superModule;
var Resource = require('dw/web/Resource');
var preferences = require('*/cartridge/config/preferences');
var collections = require('*/cartridge/scripts/util/collections');

/**
 * Generates a map of string resources for the template
 *
 * @returns {ProductDetailPageResourceMap} - String resource map
 */
function getResources() {
  return {
    info_selectforstock: '',
    assistiveSelectedText: Resource.msg('msg.assistive.selected.text', 'common', null),
    soldout: Resource.msg('product.tile.soldout', 'product', 'sold out'),
    addtocart: Resource.msg('product.tile.addtocart', 'product', 'add to bag'),
    preordertocart: Resource.msg('product.tile.preordertobag', 'product', null),
    limitedInventory: Resource.msg('label.limitedstock', 'product', null),
    movetobag: Resource.msg('wishlist.prod.movetocart', 'wishlist', null),
    addtobag: Resource.msg('wishlist.prod.addtocart', 'wishlist', null)
  };
}

/**
 * returns true to enable add to cart on selected templates
 *
 * @returns {enabled} - boolean enabled
 */
function isenabledForClick2OrderCsr(isagent) {
  var siteCustomerGrp = null;
  var enabled = false;
  if (isagent === true && preferences.click2CallCustomerGroupOverride && preferences.click2CallCustomerGroupOverride.length > 0) {
    var click2CallCustomerGroupOverride = preferences.click2CallCustomerGroupOverride;
    siteCustomerGrp = customer.customerGroups;
    if (siteCustomerGrp.length > 0) {
      for (var i = 0; i < siteCustomerGrp.length; i++) {
        var eachGroup = siteCustomerGrp[i];
        if (click2CallCustomerGroupOverride.indexOf(eachGroup.ID) > -1) {
          enabled = true;
          break;
        }
      }
    }
  }
  return enabled;
}
/**
 * Creates the breadcrumbs object
 * @param {string} cgid - category ID from navigation and search
 * @param {string} pid - product ID
 * @param {Array} breadcrumbs - array of breadcrumbs object
 * @returns {Array} an array of breadcrumb objects
 */
function getAllBreadcrumbs(cgid, pid, breadcrumbs) {
  var URLUtils = require('dw/web/URLUtils');
  var CatalogMgr = require('dw/catalog/CatalogMgr');
  var ProductMgr = require('dw/catalog/ProductMgr');

  var category;
  var product;
  if (pid) {
    product = ProductMgr.getProduct(pid);
    category = product.variant ? product.masterProduct.primaryCategory : product.primaryCategory;
  } else if (cgid) {
    category = CatalogMgr.getCategory(cgid);
  }

  if (category) {
    if (!category.custom.hideFromBreadcrumb) {
      breadcrumbs.push({
        htmlValue:
          'categoryh1title' in category.custom && category.custom.categoryh1title
            ? category.custom.categoryh1title
            : 'categoryNameoverwrite' in category.custom && !!category.custom.categoryNameoverwrite
            ? category.custom.categoryNameoverwrite
            : category.displayName,
        url: URLUtils.url('Search-Show', 'cgid', category.ID),
        isClickable: 'isClickable' in category.custom && category.custom.isClickable
      });
    }
    if (category.parent && category.parent.ID !== 'root') {
      return getAllBreadcrumbs(category.parent.ID, null, breadcrumbs);
    }
  }

  return breadcrumbs;
}

base.showProductPage = function (querystring, reqPageMetaData) {
  var URLUtils = require('dw/web/URLUtils');
  var ProductFactory = require('*/cartridge/scripts/factories/product');
  var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');

  var params = querystring;
  var product = ProductFactory.get(params);
  var addToCartUrl = URLUtils.url('Cart-AddProduct');
  var canonicalUrl = URLUtils.url('Product-Show', 'pid', product.id);
  var breadcrumbs = getAllBreadcrumbs(null, product.id, []);
  if (breadcrumbs.length > 0) {
    breadcrumbs.push({
      htmlValue: Resource.msg('label.search.home', 'search', null),
      url: URLUtils.url('Home-Show')
    });
    breadcrumbs = breadcrumbs.reverse();
  }
  var template = 'product/productDetails';

  if (product.template) {
    template = product.template;
  } else if (product.hbcProductType === 'bridal') {
    template = 'product/productDetails_bridal';
  } else if (product.hbcProductType === 'home') {
    template = 'product/productDetails_homeappliances';
  } else if (product.hbcProductType === 'gwp') {
    template = 'product/productDetails_gwp';
  } else if (product.hbcProductType === 'giftcard') {
    template = 'product/productDetails_giftcard';
  } else if (product.hbcProductType === 'chanel') {
    template = 'product/productDetails_chanel';
  } else if (product.hbcProductType === 'CSRonly') {
    template = 'product/productDetails_click2Order';
  } else if (product.hbcProductType === 'GucciCTC') {
    template = 'product/productDetails_click2Order';
  } else if (product.hbcProductType === 'CSRstores') {
    template = 'product/productDetails_RTWChanel';
  } else if (product.hbcProductType === 'RTWChanel') {
    template = 'product/productDetails_RTWChanel';
  } else if (product.productType === 'bundle' && !product.template) {
    template = 'product/bundleDetails';
  } else if (product.productType === 'set' && !product.template) {
    template = 'product/setDetails';
  }

  pageMetaHelper.setPageMetaData(reqPageMetaData, product);
  pageMetaHelper.setPageMetaTags(reqPageMetaData, product);
  var schemaData = require('*/cartridge/scripts/helpers/structuredDataHelper').getProductSchema(product);

  return {
    template: template,
    product: product,
    addToCartUrl: addToCartUrl,
    resources: getResources(),
    breadcrumbs: breadcrumbs,
    canonicalUrl: canonicalUrl,
    schemaData: schemaData
  };
};

/**
 * SFSX-3698 Get PDP special rendering options based on hbcProductType
 * @param {string} productType Provided Product Type
 */
base.getDesignerOptions = function (productType) {
  var result = {
    showShippingInformation: true,
    clickToCallAssetId: 'clickToCallCSROnly',
    clickToCallPhoneAssetId: 'clickToCallCSROnlyPhoneLink'
  };
  if (productType === 'GucciCTC') {
    result.clickToCallAssetId = 'clickToCallGucci';
    result.showShippingInformation = false;
    result.clickToCallPhoneAssetId = 'clickToCallGucciPhoneLink';
  }

  return result;
}

//Utility method to calculate the SaksFirst Points for PDP and Cart pages
base.getHudsonPoints = function (basketOrProduct, isBasket, apiProdPrice) {
  var profile = customer.profile;
  if (
    preferences.saksFirstEnabled &&
    preferences.saksFirstPointsMultiplier !== -1 &&
    profile &&
    'saksFirstLinked' in profile.custom &&
    profile.custom.saksFirstLinked
  ) {
    var saksFirstPointsMultiplier = preferences.saksFirstPointsMultiplier;
    var saksFirstHelpers = require('*/cartridge/scripts/helpers/saksFirstHelpers');
    var saksFirstInfo = saksFirstHelpers.getSaksFirstMemberInfo(profile);
    // If the Saks First Tier is unenrolled, then don't show the banner on Cart.
    if (saksFirstInfo && saksFirstInfo.unenrolledTier) {
      return 0;
    }
    if (saksFirstInfo && saksFirstInfo.pointsMultiplier) {
      saksFirstPointsMultiplier = saksFirstInfo.pointsMultiplier;
    }
    if (isBasket) {
      var finalRewardPoints = 0;
      var includedProductPrice = 0;
      // Add prices of all the products which are eligible for reward points.
      collections.forEach(basketOrProduct.productLineItems, function (pli) {
        var masterProduct = pli.product && pli.product.variationModel ? pli.product.variationModel.master : pli.product;
        if (
          masterProduct &&
          !empty(masterProduct.custom.hbcProductType) &&
          (masterProduct.custom.hbcProductType !== 'chanel' || masterProduct.custom.hbcProductType !== 'giftcard')
        ) {
          includedProductPrice += pli.proratedPrice.value; // Get the adjusted price of the item
        } else if (masterProduct && empty(masterProduct.custom.hbcProductType)) {
          includedProductPrice += pli.proratedPrice.value;
        }
      });

      if (includedProductPrice > 0) {
        finalRewardPoints = Math.floor(includedProductPrice * saksFirstPointsMultiplier);
      }
      return finalRewardPoints;
    }
    var masterProduct = basketOrProduct && basketOrProduct.variationModel ? basketOrProduct.variationModel.master : pli.product;
    if (
      masterProduct &&
      !empty(masterProduct.custom.hbcProductType) &&
      (masterProduct.custom.hbcProductType !== 'chanel' || masterProduct.custom.hbcProductType !== 'giftcard')
    ) {
      var estPrice = apiProdPrice.type && apiProdPrice.type === 'range' ? apiProdPrice.max.sales.value : apiProdPrice.sales.value;
      return Math.floor(estPrice * saksFirstPointsMultiplier);
    } else if (masterProduct && empty(masterProduct.custom.hbcProductType)) {
      var estPrice = apiProdPrice.type && apiProdPrice.type === 'range' ? apiProdPrice.max.sales.value : apiProdPrice.sales.value;
      return Math.floor(estPrice * saksFirstPointsMultiplier);
    }
    return 0;
  }
  return 0;
};

/**
 * SFSX-3542 Check if google ads restricted for the provided brand
 * @param {string} brandName Product Brand Name
 * @returns {boolean} - true if brandName is restricted on restrictedGoogleAdsBrands
 */
base.checkGoogleAdsRestrictedBrands = function (brandName) {
  var restrictGoogleAds = false;
  for (var key = 0; key < preferences.restrictedGoogleAdsBrands.length; key++) {
    if (preferences.restrictedGoogleAdsBrands[key].toUpperCase() === brandName.toUpperCase()) {
      restrictGoogleAds = true;
      break;
    }
  }
  return restrictGoogleAds;
};

/**
 * If a product is master and only have one variant for a given attribute - auto select it
 * @param {dw.catalog.Product} apiProduct - Product from the API
 * @param {Object} params - Parameters passed by querystring
 *
 * @returns {Object} - Object with selected parameters
 */
function normalizeSelectedAttributes(apiProduct, params) {
  if (!apiProduct.master) {
      return params.variables;
  }

  var variables = params.variables || {};
  if (apiProduct.variationModel) {
      collections.forEach(apiProduct.variationModel.productVariationAttributes, function (attribute) {
          var allValues = apiProduct.variationModel.getAllValues(attribute);
          if (allValues.length === 1) {
              variables[attribute.ID] = {
                  id: apiProduct.ID,
                  value: allValues.get(0).ID
              };
          }
      });
  }

  return Object.keys(variables) ? variables : null;
}

/**
 * Get information for model creation
 * @param {dw.catalog.Product} apiProduct - Product from the API
 * @param {Object} params - Parameters passed by querystring
 *
 * @returns {Object} - Config object
 */
 base.getConfig = function (apiProduct, params) {
  var variables = normalizeSelectedAttributes(apiProduct, params);
  var variationModel = base.getVariationModel(apiProduct, variables);
  if (variationModel) {
      var selectedVariant = variationModel.selectedVariant;
      if (selectedVariant) {
          var availabilityModel = selectedVariant.availabilityModel;
          if (availabilityModel.availabilityStatus === 'NOT_AVAILABLE') {
              collections.forEach(variationModel.selectedVariants, function (variant) {
                  if (variant && variant.availabilityModel && variant.availabilityModel.availabilityStatus !== 'NOT_AVAILABLE') {
                      selectedVariant = variant;
                  }
              });
          }
      }
      apiProduct = selectedVariant || apiProduct; // eslint-disable-line
  }
  var PromotionMgr = require('dw/campaign/PromotionMgr');
  var promotions = PromotionMgr.activeCustomerPromotions.getProductPromotions(apiProduct);
  var optionsModel = base.getCurrentOptionModel(apiProduct.optionModel, params.options);
  var options = {
      variationModel: variationModel,
      options: params.options,
      optionModel: optionsModel,
      promotions: promotions,
      quantity: params.quantity,
      variables: variables,
      apiProduct: apiProduct,
      productType: base.getProductType(apiProduct)
  };

  return options;
}

base.getResources = getResources;

base.getAllBreadcrumbs = getAllBreadcrumbs;

base.isEnabledForClick2OrderCSR = isenabledForClick2OrderCsr;

module.exports = base;
