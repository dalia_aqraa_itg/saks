'use strict';

var base = module.superModule;
var sddProduct = require('*/cartridge/models/product/sddProduct');
var ProductMgr = require('dw/catalog/ProductMgr');

base.getSDD = function (params) {
  return sddProduct({}, params.apiProduct);
};

base.baseProduct = function (params) {
  var baseDecorator = require('*/cartridge/models/product/decorators/base');
  var productHelper = require('*/cartridge/scripts/helpers/productHelpers');

  var product = {};
  var apiProduct = ProductMgr.getProduct(params.pid);
  baseDecorator(product, apiProduct, productHelper.getProductType(apiProduct));
  return product;
}

base.getVariations = function (params) {
  var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
  var productVariations = require('*/cartridge/models/product/productVariations');
  var apiProduct = ProductMgr.getProduct(params.pid);
  var options = productHelper.getConfig(apiProduct, params);
  return productVariations({}, options.apiProduct, options);
};

base.getSimpleLineItem = function (params) {
  var baseDecorator = require('*/cartridge/models/product/decorators/base');
  var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
  var fromStoreId = require('*/cartridge/models/productLineItem/decorators/fromStoreId');
  var price = require('*/cartridge/models/product/decorators/price');

  var lineItem = {};
  var apiProduct = ProductMgr.getProduct(params.item.productID);
  if (apiProduct == null) {
    return apiProduct;
  }
  var options = productHelper.getConfig(apiProduct, { pid: params.id });

  fromStoreId(lineItem, params.item);
  baseDecorator(lineItem, apiProduct, productHelper.getProductType(apiProduct));
  price(lineItem, apiProduct, options.promotions, false, options.optionModel);

  return lineItem;
};

module.exports = base;
