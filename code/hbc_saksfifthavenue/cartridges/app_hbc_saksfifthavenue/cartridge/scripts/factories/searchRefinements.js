'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var ACTION_ENDPOINT = 'Search-Show';
/**
 * Retrieves attribute refinement value model
 *
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition - Refinement
 *     definition for which we wish to retrieve refinement values for
 * @return {Object} - Attribute refinement value model module
 */
function getAttributeRefinementValueModel(refinementDefinition) {
  if (refinementDefinition.priceRefinement) {
    return require('*/cartridge/models/search/attributeRefinementValue/price');
  } else if (refinementDefinition.attributeID === 'colorRefinement') {
    return require('*/cartridge/models/search/attributeRefinementValue/color');
  } else if (refinementDefinition.attributeID === 'size') {
    return require('*/cartridge/models/search/attributeRefinementValue/size');
  } else if (refinementDefinition.categoryRefinement) {
    return require('*/cartridge/models/search/attributeRefinementValue/category');
  } else if (refinementDefinition.promotionRefinement) {
    return require('*/cartridge/models/search/attributeRefinementValue/promotion');
  }

  return require('*/cartridge/models/search/attributeRefinementValue/boolean');
}

/**
 * Retrieves the refinement value url and updates with store id if found
 *
 * @param {Object} httpParams - Query params
 * @param {dw.catalog.ProductSearchModel} productSearch - product search model
 * @param {Object} refinementModel - refinement model
 * @param {Object} refinementDefinition - refinement definition
 * @param {Object} value - refinement value
 * @return{sting} - refinement url
 */
function appendStoreRefinement(httpParams, productSearch, refinementModel, refinementDefinition, value) {
  var url = '';
  var storeRefinement = null;
  var volatileModel = refinementModel;
  volatileModel.actionEndpoint = ACTION_ENDPOINT;
  // prepare key value
  if (httpParams) {
    Object.keys(httpParams).forEach(function (element) {
      if (element.indexOf('storeid') > -1) {
        if (storeRefinement) {
          storeRefinement.storeid = httpParams[element];
        } else {
          storeRefinement = {
            storeid: httpParams[element]
          };
        }
      }
      if (element.indexOf('srchsrc') > -1) {
        if (storeRefinement) {
          storeRefinement.srchsrc = httpParams[element];
        } else {
          storeRefinement = {
            srchsrc: httpParams[element]
          };
        }
      }
    });
  }
  // set store id
  if (refinementDefinition.priceRefinement) {
    if (refinementModel.selected) {
      url = productSearch.urlRelaxPrice(refinementModel.actionEndpoint);
      if (storeRefinement) {
        url = url.append('storeid', storeRefinement.storeid);
      }
      if (storeRefinement && storeRefinement.srchsrc) {
        url = url.append('srchsrc', storeRefinement.srchsrc);
      }
    } else {
      url = productSearch.urlRefinePrice(refinementModel.actionEndpoint, value.valueFrom, value.valueTo);
      if (storeRefinement) {
        url = url.append('storeid', storeRefinement.storeid);
      }
      if (storeRefinement && storeRefinement.srchsrc) {
        url = url.append('srchsrc', storeRefinement.srchsrc);
      }
    }
  } else if (refinementDefinition.promotionRefinement) {
    if (refinementModel.selected) {
      url = productSearch.urlRelaxPromotion(refinementModel.actionEndpoint);
      if (storeRefinement) {
        url = url.append('storeid', storeRefinement.storeid);
      }
      if (storeRefinement && storeRefinement.srchsrc) {
        url = url.append('srchsrc', storeRefinement.srchsrc);
      }
    } else {
      url = productSearch.urlRefinePromotion(refinementModel.actionEndpoint, value.value);
      if (storeRefinement) {
        url = url.append('storeid', storeRefinement.storeid);
      }
      if (storeRefinement && storeRefinement.srchsrc) {
        url = url.append('srchsrc', storeRefinement.srchsrc);
      }
    }
  } else if (refinementModel.selected) {
    url = productSearch.urlRelaxAttributeValue(refinementModel.actionEndpoint, refinementModel.id, value.value);
    if (storeRefinement) {
      url = url.append('storeid', storeRefinement.storeid);
    }
    if (storeRefinement && storeRefinement.srchsrc) {
      url = url.append('srchsrc', storeRefinement.srchsrc);
    }
  } else if (!refinementModel.selectable) {
    url = '#';
    return url;
  } else {
    url = productSearch.urlRefineAttributeValue(refinementModel.actionEndpoint, refinementModel.id, value.value);
    if (storeRefinement) {
      url = url.append('storeid', storeRefinement.storeid);
    }
    if (storeRefinement && storeRefinement.srchsrc) {
      url = url.append('srchsrc', storeRefinement.srchsrc);
    }
  }
  // SFSX-4016 Fix Left Nav issues
  if (productSearch.effectiveSortingRule) {
    url.append('srule', productSearch.effectiveSortingRule.ID);
  }

  return url.relative().toString();
}

/**
 * Retrieves the refinement value hit count
 *
 * @param {Object} value - refinement value
 * @return{Object} - Refinement hit count
 */
function getRefinementValueHitCount(value) {
  try {
    if (value && value.hitCount) {
      return value.hitCount;
    }
  } catch (e) {
    return null;
  }
  return null;
}

/**
 * Valdiates wheather all categories are showInRefinementMenu as false
 * @param {Object} catRefinementValues dw Search Refinement value
 * @returns {boolean} status returns whether all the refinement values are non showInRefinementMenu
 */
function isAllCategoriesNonRefinement(catRefinementValues) {
  var CatalogMgr = require('dw/catalog/CatalogMgr');
  var categoriesList = catRefinementValues.iterator();
  var allCatNonRefine = true;
  while (categoriesList.hasNext()) {
    var refinmentValue = categoriesList.next();
    var categoryObject = CatalogMgr.getCategory(refinmentValue.value);
    if (
      categoryObject && 'showInRefinementMenu' in categoryObject.custom && categoryObject.custom.showInRefinementMenu
        ? categoryObject.custom.showInRefinementMenu
        : false
    ) {
      allCatNonRefine = false;
      break;
    }
  }
  return allCatNonRefine;
}

/**
 * Creates an array of category refinements for category search
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition - Refinement
 *     definition for which we wish to retrieve refinement values for
 * @param {CategoryAttributeValue} Model - model of the category class
 * @param {dw/catalog/SearchRefinementValue} refinementValues - Applicable refinement values
 * @return {Array} - List of categories
 */
function createCategorySearchRefinement(productSearch, refinementDefinition, Model, refinementValues) {
  var CatalogMgr = require('dw/catalog/CatalogMgr');
  var childCategory = null;
  var currentCategory = productSearch.category;
  // let bar = CatalogMgr.getCategory(refinmentValue.value);
  var topCategory = null;
  var insertPoint = null;
  if (refinementValues.length && currentCategory.root) {
    topCategory = new Model(productSearch, refinementDefinition, currentCategory, false);
    insertPoint = topCategory.subCategories;
  } else if (refinementValues.length && currentCategory.parent.root) {
    topCategory = new Model(productSearch, refinementDefinition, currentCategory, true);
    insertPoint = topCategory.subCategories;
  } else if (!refinementValues.length || isAllCategoriesNonRefinement(refinementValues)) {
    if (currentCategory.parent.parent && !currentCategory.parent.parent.root) {
      topCategory = new Model(productSearch, refinementDefinition, currentCategory.parent.parent);
      childCategory = new Model(productSearch, refinementDefinition, currentCategory.parent);
      topCategory.subCategories.push(childCategory);
      insertPoint = topCategory.subCategories[0].subCategories;
    } else {
      topCategory = new Model(productSearch, refinementDefinition, currentCategory.parent);
      insertPoint = topCategory.subCategories;
    }
    // leaf values have no refinementValues and a new instance of PSM needs to be instantiated for search
    var ProductSearchModel = require('dw/catalog/ProductSearchModel');
    var searchModel = new ProductSearchModel();
    searchModel.setCategoryID(currentCategory.parent.ID);
    searchModel.search();
    // reassign refinement values to use later on when building tree
    refinementValues = searchModel.refinements.getNextLevelCategoryRefinementValues(currentCategory.parent);
  } else {
    topCategory = new Model(productSearch, refinementDefinition, currentCategory.parent);
    childCategory = new Model(productSearch, refinementDefinition, currentCategory, true);
    topCategory.subCategories.push(childCategory);
    insertPoint = topCategory.subCategories[0].subCategories;
  }
  collections.forEach(refinementValues, function (refinmentValue) {
    let category = CatalogMgr.getCategory(refinmentValue.value);
    if (category) {
      var iscurrentcategory = category.ID == currentCategory.ID;
      insertPoint.push(new Model(productSearch, refinementDefinition, category, iscurrentcategory));
    }
  });
  if (topCategory.id === 'root') {
    return topCategory.subCategories;
  }
  return [topCategory];
}

/**
 * Validates whether category contains atleast one valid product
 * @param {String} categoryId category id
 * @param {String} queryString query string
 * @returns {boolean} return status
 */
function isValidRefineCategory(categoryId, queryString) {
  var ProductSearchModel = require('dw/catalog/ProductSearchModel');
  var productSearchModel = new ProductSearchModel();
  productSearchModel.setCategoryID(categoryId);
  productSearchModel.setSearchPhrase(queryString);
  productSearchModel.search();
  return productSearchModel.productSearchHits.hasNext();
}

/**
 * Creates an array of category refinements for category search
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition - Refinement
 *     definition for which we wish to retrieve refinement values for
 * @param {dw.util.Collection.<dw.catalog.ProductSearchRefinementValue>} refinementValues -
 *     Collection of refinement values
 * @param {CategoryAttributeValue} Model - model of the category class
 * @return {Array} - List of categories
 */
function createProductSearchRefinement(productSearch, refinementDefinition, refinementValues, Model) {
  var catalogMgr = require('dw/catalog/CatalogMgr');
  var URLUtils = require('dw/web/URLUtils');
  var Resource = require('dw/web/Resource');
  var tree = [];
  var preferences = require('*/cartridge/config/preferences');
  var mappedList = {};
  collections.forEach(refinementValues, function (value) {
    var category = catalogMgr.getCategory(value.value);
    if (
      (preferences.catRefinementLevel === 2 && (category.parent.root || category.parent.parent.root)) ||
      (preferences.catRefinementLevel === 1 &&
        ((category.parent.root && !productSearch.refinedByCategory) ||
          (productSearch.refinedByCategory && (category.ID === productSearch.categoryID || category.parent.ID === productSearch.categoryID))))
    ) {
      mappedList[value.value] = new Model(productSearch, refinementDefinition, category, productSearch.categoryID === value.value);
      mappedList[value.value].parent = category.parent.ID;
    }
  });

  // Add sibling catgeory when we have only one category or no children category
  if (Object.keys(mappedList).length === 1 && productSearch.category) {
    delete mappedList[Object.keys(mappedList)[0]];
    var parentCat = productSearch.category.parent;
    var parentCatItr = parentCat.onlineSubCategories.iterator();
    while (parentCatItr.hasNext()) {
      var currentCat = parentCatItr.next();
      if (!isValidRefineCategory(currentCat.ID, productSearch.searchPhrase)) {
        continue;
      }
      mappedList[currentCat.ID] = new Model(productSearch, refinementDefinition, currentCat, productSearch.categoryID === currentCat.ID);
      mappedList[currentCat.ID].parent = currentCat.parent.ID;
    }
  }

  // SFDEV-8522 | addition of parent category for O5 search when there is not child category
  if (preferences.catRefinementLevel === 1 && productSearch.refinedByCategory && !productSearch.category.parent.root) {
    var cat = productSearch.category.parent;
    while (!cat.root) {
      mappedList[cat.ID] = new Model(productSearch, refinementDefinition, cat, false);
      mappedList[cat.ID].parent = cat.parent.ID;
      cat = cat.parent;
    }
  }

  Object.keys(mappedList).forEach(function (key) {
    var category = mappedList[key];
    // SFDEV-8522 | change of logic to add without parent in case of O5 search category refinement
    if (category.parent !== 'root' && mappedList[category.parent]) {
      mappedList[category.parent].subCategories.push(category);
    } else {
      tree.push(category);
    }
  });

  // SFDEV-8522 | Pushing the allresults section at the top in case of pure search with any keyword
  if (preferences.catRefinementLevel === 1 && !productSearch.refinedByCategory) {
    tree.unshift({
      type: 'category',
      id: 'all',
      value: 'all',
      displayValue: Resource.msg('search.all.results.text', 'search', null),
      hitCount: productSearch.count,
      selectable: false,
      selected: true,
      actionEndpoint: ACTION_ENDPOINT,
      url: URLUtils.url(ACTION_ENDPOINT, 'q', productSearch.searchPhrase),
      title: Resource.msgf('label.refinement', 'search', null, 'Category', Resource.msg('search.all.results.text', 'search', null))
    });
  }
  return tree;
}

/**
 * Retrieve refinement values based on refinement type
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition - Refinement
 *     definition for which we wish to retrieve refinement values for
 * @param {dw.util.Collection.<dw.catalog.ProductSearchRefinementValue>} refinementValues -
 *     Collection of refinement values
 * @param {Object} httpParams http parameter query string object
 * @return {Array} - List of refinement values
 */
function get(productSearch, refinementDefinition, refinementValues, httpParams) {
  var Model = getAttributeRefinementValueModel(refinementDefinition);
  // var collections = require('*/cartridge/scripts/util/collections');
  var priceObj = [];
  if (refinementDefinition.categoryRefinement) {
    if (productSearch.categorySearch) {
      // return only current category, direct children and direct parent
      return createCategorySearchRefinement(productSearch, refinementDefinition, Model, refinementValues);
    }
    return createProductSearchRefinement(productSearch, refinementDefinition, refinementValues, Model);
  }

  var refinementCollection = collections.map(refinementValues, function (value) {
    var refinementModel = new Model(productSearch, refinementDefinition, value);
    // add store id to url
    refinementModel.url = appendStoreRefinement(httpParams, productSearch, refinementModel, refinementDefinition, value);
    refinementModel.hitCount = getRefinementValueHitCount(value);
    return refinementModel;
  });

  var priceFilter = false;
  if (httpParams) {
    if (!(Number(httpParams.pmin) === 0 && !httpParams.pmax)) {
      priceFilter = true;
    }
  }
  if (refinementDefinition.priceRefinement && priceFilter) {
    var storeRefinement = null;
    var url = '';
    var selected = false;
    var Resource = require('dw/web/Resource');
    var StringUtils = require('dw/util/StringUtils');
    var Money = require('dw/value/Money');
    var priceHelper = require('*/cartridge/scripts/helpers/pricing');
    var minPrice = productSearch.priceMin || 0;
    var maxprice = productSearch.priceMax || 0;
    var moneyMinPrice = new Money(minPrice, session.currency.currencyCode);
    var moneyMaxPrice = new Money(maxprice, session.currency.currencyCode);
    var priceValue = maxprice
      ? priceHelper.formatMoney(StringUtils.formatMoney(moneyMinPrice)) + ' - ' + priceHelper.formatMoney(StringUtils.formatMoney(moneyMaxPrice))
      : priceHelper.formatMoney(StringUtils.formatMoney(moneyMinPrice)) + '+';

    // prepare key value and add store id to url
    if (httpParams) {
      Object.keys(httpParams).forEach(function (element) {
        if (element.indexOf('storeid') > -1) {
          if (storeRefinement) {
            storeRefinement.storeid = httpParams[element];
          } else {
            storeRefinement = {
              storeid: httpParams[element]
            };
          }
        }
        if (element.indexOf('srchsrc') > -1) {
          if (storeRefinement) {
            storeRefinement.srchsrc = httpParams[element];
          } else {
            storeRefinement = {
              srchsrc: httpParams[element]
            };
          }
        }
      });
    }
    url = productSearch.urlRelaxPrice('Search-Show');
    if (storeRefinement) {
      url = url.append('storeid', storeRefinement.storeid);
    }
    if (storeRefinement && storeRefinement.srchsrc) {
      url = url.append('srchsrc', storeRefinement.srchsrc);
    }
    priceObj.push({
      type: 'price',
      displayValue: priceValue,
      selected: false,
      title: Resource.msg('search.refinement.price.min.max', 'search', null),
      url: url
    });
    Object.keys(refinementCollection).forEach(function (key) {
      if (refinementCollection[key].selected) {
        selected = true;
      }
      priceObj.push(refinementCollection[key]);
    });
    if (productSearch.refinedByPrice && !selected) {
      priceObj[0].selected = true;
      priceObj[0].min = productSearch.priceMin ? productSearch.priceMin : '';
      priceObj[0].max = productSearch.priceMax ? productSearch.priceMax : '';
    }
    refinementCollection = priceObj;
  }

  return refinementCollection;
}

module.exports = {
  get: get
};
