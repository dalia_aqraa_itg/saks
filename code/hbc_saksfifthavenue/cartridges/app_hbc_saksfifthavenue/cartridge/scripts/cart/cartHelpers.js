'use strict';

var base = module.superModule;
var ProductMgr = require('dw/catalog/ProductMgr');
var Resource = require('dw/web/Resource');
var UUIDUtils = require('dw/util/UUIDUtils');
var Logger = require('dw/system/Logger');
var collections = require('*/cartridge/scripts/util/collections');

function addDropShipDetails(currentBasket) {
  try {
    var productLineItems = currentBasket.productLineItems;
    delete currentBasket.custom.dropShipItemsDetails;
    var dropShipItemsDetails = {};
    for (var i = 0; i < productLineItems.length; i++) {
      var pli = productLineItems[i];
      var pliProduct = pli.product;
      if (pliProduct.custom.hasOwnProperty('pdRestrictedShipTypeText') && pliProduct.custom.pdRestrictedShipTypeText) {
        var productDropShipAttrValue = pliProduct.custom.pdRestrictedShipTypeText;
        var dropShipItemDetailsKeys = Object.keys(dropShipItemsDetails);
        if (dropShipItemDetailsKeys.length > 0 && dropShipItemsDetails[productDropShipAttrValue]) {
          let value = dropShipItemsDetails[productDropShipAttrValue];
          if (isNaN(value)) {
            value = Number(value);
          }
          value = value + 1;
          dropShipItemsDetails[productDropShipAttrValue] = value;
        } else {
          dropShipItemsDetails[productDropShipAttrValue] = 1;
        }
      }
    }
    currentBasket.custom.dropShipItemsDetails = JSON.stringify(dropShipItemsDetails);
  } catch (e) {
    Logger.error('ERROR IN addDropShipDetails' + e.message);
  }
}

/**
 * Add cart item to product list
 *
 * @param {dw/catalog/Product} product - Product
 */
function addCartItemToProductList(product) {
  try {
    var preferences = require('*/cartridge/config/preferences');
    var Transaction = require('dw/system/Transaction');
    if (preferences.isABTestingOn) {
      var ProductListMgr = require('dw/customer/ProductListMgr');
      var ProductList = require('dw/customer/ProductList');
      var cartProductList;
      if (session.custom.cartProductListId == null) {
        Transaction.wrap(function () {
          cartProductList = ProductListMgr.createProductList(session.getCustomer(), ProductList.TYPE_CUSTOM_1);
          session.custom.cartProductListId = cartProductList.ID;
        });
        Logger.debug(' addCartItemToProductList NEW cartProductListId -->' + session.custom.cartProductListId);
      }
      if (session.custom.cartProductListId != null) {
        Logger.debug('addCartItemToProductList cartProductListId NOT EMPTY');
        cartProductList = ProductListMgr.getProductList(session.custom.cartProductListId);
        Logger.debug('addCartItemToProductList cartProductListId NOT EMPTY' + cartProductList);
        var itemIter = cartProductList.getItems().iterator();
        var existingItemInList;
        var existingInList = false;
        while (itemIter.hasNext()) {
          existingItemInList = itemIter.next();
          Logger.debug('itemIter ID' + existingItemInList.productID);
          if (existingItemInList.productID === product.ID) {
            existingInList = true;
            break;
          }
        }
        Logger.debug('addCartItemToProductList existingInList NOT EMPTY' + existingInList);

        if (!existingInList) {
          Logger.debug('addCartItemToProductList existingInList DOESNT HAVE ITEM' + existingInList);

          cartProductList.createProductItem(product);
        } else {
          Logger.debug('addCartItemToProductList existingInList HAS ITEM' + existingInList);
        }
      }
    }
  } catch (e) {
    Logger.debug('ERROR IN CART SYNC');
  }
}

/**
 * Sets a flag to exclude the quantity for a product line item matching the provided UUID.  When
 * updating a quantity for an already existing line item, we want to exclude the line item's
 * quantity and use the updated quantity instead.
 * @param {string} selectedUuid - Line item UUID to exclude
 * @param {string} itemUuid - Line item in-process to consider for exclusion
 * @return {boolean} - Whether to include the line item's quantity
 */
function excludeUuid(selectedUuid, itemUuid) {
  return selectedUuid ? itemUuid !== selectedUuid : true;
}

/**
 * SFSX-3635 Customized to ignore products quantity if the same product added to the cart but with different shipping (instore pickup vs shipto)
 * Calculate the quantities for any existing instance of a product, either as a single line item
 * with the same or different options, as well as inclusion in product bundles.  Providing an
 * optional "uuid" parameter, typically when updating the quantity in the Cart, will exclude the
 * quantity for the matching line item, as the updated quantity will be used instead.  "uuid" is not
 * used when adding a product to the Cart.
 *
 * @param {string} productId - ID of product to be added or updated
 * @param {dw.util.Collection<dw.order.ProductLineItem>} lineItems - Cart product line items
 * @param {string} [uuid] - When provided, excludes the quantity for the matching line item
 * @return {number} - Total quantity of all instances of requested product in the Cart and being
 *     requested
 */
base.getQtyAlreadyInCart = function (productId, lineItems, uuid, storeId) {
  var qtyAlreadyInCart = 0;

  collections.forEach(lineItems, function (item) {
    if (item.bundledProductLineItems.length) {
      collections.forEach(item.bundledProductLineItems, function (bundleItem) {
        if (bundleItem.productID === productId && excludeUuid(uuid, bundleItem.UUID)) {
          qtyAlreadyInCart += bundleItem.quantityValue;
        }
      });
    } else if (item.productID === productId && excludeUuid(uuid, item.UUID)) {
      var shippingMethod = item.shipment && item.shipment.shippingMethod ? item.shipment.shippingMethod : {};
      var existingStoreId =
        item.shipment && item.shipment.custom && 'fromStoreId' in item.shipment.custom && item.shipment.custom.fromStoreId
          ? item.shipment.custom.fromStoreId
          : null;
      var storePickupEnabled = shippingMethod.custom && 'storePickupEnabled' in shippingMethod.custom && shippingMethod.custom.storePickupEnabled;
      if ((storeId && storePickupEnabled && existingStoreId === storeId) || (!storeId && !storePickupEnabled)) {
        qtyAlreadyInCart += item.quantityValue;
      }
    }
  });
  return qtyAlreadyInCart;
};

/**
 * Adds a product to the cart. If the product is already in the cart it increases the quantity of that product.
 * @param {dw.order.Basket} currentBasket - Current users's basket
 * @param {string} productId - the productId of the product being added to the cart
 * @param {number} quantity - the number of products to the cart
 * @param {string[]} childProducts - the products' sub-products
 * @param {SelectedOption[]} options - product options
 * @param {string} storeId - store id to be added to product item
 * @param {Object} req - current request
 * @return {Object} returns an error object
 */
base.addProductToCart = function (currentBasket, productId, quantity, childProducts, options, storeId, req) {
  var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
  var instorePickupStoreHelper = require('*/cartridge/scripts/helpers/instorePickupStoreHelpers');
  var availableToSell;
  var defaultShipment = currentBasket.defaultShipment;
  var perpetual = false;
  var product = ProductMgr.getProduct(productId);
  var productInCart;
  var productLineItems = currentBasket.productLineItems;
  var productQuantityInCart;
  var quantityToSet;
  var preferences = require('*/cartridge/config/preferences');

  var result = {
    error: false,
    message: Resource.msg('text.alert.addedtobasket', 'product', null)
  };

  // SFSX-1837 | Security issue | Check if product is gwp when product price is $0 for adding to cart
  if (product.custom.hbcProductType === 'gwp') {
    result.error = true;
    result.message = Resource.msg('error.alert.selected.quantity.cannot.be.added', 'product', null);
    return result;
  }
  // return if basket limit is reached
  if (preferences.basketItemLimit && productLineItems && productLineItems.length >= preferences.basketItemLimit) {
    result.message = 'LIMIT_EXCEEDED';
    return result;
  }

  if (product === null) {
    result.error = true;
    return result;
  }

  var optionModel = productHelper.getCurrentOptionModel(product.optionModel, options);
  var totalQtyRequested = 0;
  var canBeAdded = false;

  if (product.bundle) {
    canBeAdded = base.checkBundledProductCanBeAdded(childProducts, productLineItems, quantity);
  } else {
    totalQtyRequested = quantity + base.getQtyAlreadyInCart(productId, productLineItems, null, storeId);
    if (storeId) {
      var instorePUstoreHelpers = require('*/cartridge/scripts/helpers/instorePickupStoreHelpers');
      var unitsAtStores = instorePUstoreHelpers.getStoreInventory(storeId, productId);
      canBeAdded = totalQtyRequested <= unitsAtStores;
    } else if (product.availabilityModel.inventoryRecord) {
      perpetual = product.availabilityModel.inventoryRecord.perpetual;
      canBeAdded = perpetual || totalQtyRequested <= product.availabilityModel.inventoryRecord.ATS.value;
    }
  }
  var isPurchaseLimit = 'purchaseLimit' in product.custom ? product.custom.purchaseLimit && totalQtyRequested <= product.custom.purchaseLimit : true;
  if (!isPurchaseLimit) {
    result.error = true;
    result.message = Resource.msgf('label.notinpurchaselimit', 'common', null, product.custom.purchaseLimit);
    return result;
  }
  if ('hbcProductType' in  product.custom && product.custom.hbcProductType === 'GucciCTC') {
    canBeAdded = false;
  }
  if (!canBeAdded) {
    result.error = true;
    if (product.availabilityModel.inventoryRecord != null) {
      result.message = Resource.msg('error.alert.selected.quantity.cannot.be.added.for', 'product', null);
    }
    return result;
  }

  // Get the existing product line item from the basket if the new product item
  // has the same bundled items or options and the same instore pickup store selection
  productInCart = base.getExistingProductLineItemInCartWithTheSameStore(product, productId, productLineItems, childProducts, options, storeId);

  if (productInCart) {
    productQuantityInCart = productInCart.quantity.value;
    quantityToSet = quantity ? quantity + productQuantityInCart : productQuantityInCart + 1;
    availableToSell = productInCart.product.availabilityModel.inventoryRecord.ATS.value;

    if (availableToSell >= quantityToSet || perpetual) {
      productInCart.setQuantityValue(quantityToSet);
      result.uuid = productInCart.UUID;
    } else {
      result.error = true;
      result.message =
        availableToSell === productQuantityInCart
          ? Resource.msg('error.alert.max.quantity.in.cart', 'product', null)
          : Resource.msg('error.alert.selected.quantity.cannot.be.added', 'product', null);
    }
  } else {
    var productLineItem;
    // Create a new instore pickup shipment as default shipment for product line item
    // if the shipment if not exist in the basket
    var inStoreShipment = base.createInStorePickupShipmentForLineItem(currentBasket, storeId, req);
    var shipment = inStoreShipment || defaultShipment;

    if (shipment.shippingMethod && shipment.shippingMethod.custom.storePickupEnabled && !storeId) {
      shipment = currentBasket.createShipment(UUIDUtils.createUUID());
    }
    productLineItem = base.addLineItem(currentBasket, product, quantity, childProducts, optionModel, shipment);
    Logger.debug('addCartItemToProductList!!!!!');
    addCartItemToProductList(product);
    addDropShipDetails(currentBasket);
    // Once the new product line item is added, set the instore pickup fromStoreId for the item
    if (storeId) {
      instorePickupStoreHelper.setAndGetStoreInProductLineItem(storeId, productLineItem);
    }
    if (productLineItem) {
      result.uuid = productLineItem.UUID;
    }
  }
  return result;
};

base.handleMiniCartShow = function handleMiniCartShow(currentBasket, currencyCode, basketModel) {
  var Transaction = require('dw/system/Transaction');
  var reportingUrlsHelper = require('*/cartridge/scripts/reportingUrls');

  // changes for SFDEV-5099
  var updatedLineItems = base.adjustLineItemQuantities(currentBasket);
  var reportingURLs;
  // changes for SFDEV-6215
  var soldOutItems = [];
  if (session.custom.omsInventory && session.custom.omsInventory.length > 0) {
    var omsInventoryArray = JSON.parse(session.custom.omsInventory);
    if (omsInventoryArray && omsInventoryArray.length > 0) {
      omsInventoryArray.forEach(function (item) {
        if (item.quantity === 0) {
          soldOutItems.push(item.itemID);
        }
      });
    }
  }
  var purchaseLimitedSKUMap = base.resetPLISPurchaseLimit(currentBasket);

  if (currentBasket) {
    Transaction.wrap(function () {
      if (currentBasket.currencyCode !== currencyCode) {
        currentBasket.updateCurrency();
      }
    });
  }

  if (currentBasket && currentBasket.allLineItems.length) {
    reportingURLs = reportingUrlsHelper.getBasketOpenReportingURLs(currentBasket);
  }
  var productAvaiblityStatus = false;
  if (currentBasket) {
    productAvaiblityStatus = base.productAvailabilityStatus(basketModel, soldOutItems);
  }

  var viewData = {
    reportingURLs: reportingURLs,
    soldOutItems: soldOutItems,
    updatedLineItems: updatedLineItems,
    productAvaiblityStatus: productAvaiblityStatus,
    purchaseLimitedSKUMap: purchaseLimitedSKUMap,
    cartView: 'miniCart'
  };
  return viewData;
};

/**
 * Calculates Basket Hash
 * @param {dw.order.Basket} basket - Basket
 * @returns {string} - Calculated Basket Snapshot
 */
function getBasketSnapshot(basket, currency) {
  var collections = require('*/cartridge/scripts/util/collections');
  var hashMessage = '';
  var products = [];
  collections.forEach(basket.shipments, function (shipment) {
    collections.forEach(shipment.productLineItems, function (lineItem) {
      products.push({
        productId: lineItem.productID,
        quantity: lineItem.quantityValue,
        price: (lineItem.adjustedPrice.valueOrNull || 0)
      });
    });
  });
  
  var discounts = collections.map(basket.priceAdjustments, function (priceAdjustment) {
    return ('Order Discount: ' + priceAdjustment.promotionID + ', Price: ' + (priceAdjustment.price.valueOrNull || 0));
  });

  hashMessage = products.map(function (product) {
    return ('ProductId: ' + product.productId + ', Quantity: ' + product.quantity + ', Price: ' + product.price);
  }).join(';');
  hashMessage += ('; Currency: ' + currency + '; ');
  hashMessage += discounts.join(';');
  return hashMessage;
}

/**
 * Calculates Basket Hash
 * @param {dw.order.Basket} basket - Basket
 * @returns {string} - Calculated Basket Hash
 */
base.getBasketHash = function getBasketHash(basket, currency) {
  var MessageDigest = require('dw/crypto/MessageDigest');
  var Bytes = require('dw/util/Bytes');
  var Encoding = require('dw/crypto/Encoding');

  var hashMessage = getBasketSnapshot(basket, currency);
  var sha256 = new MessageDigest(MessageDigest.DIGEST_SHA_256);
  var encodedMessage = new Bytes(hashMessage, 'UTF8');
  var hash = Encoding.toBase64(sha256.digestBytes(encodedMessage));
  return hash;
};
module.exports = base;
