<iscomment>

Template Notes:

- This template is intended to be referenced from an <isinclude> tag within an <isloop> in another
  source template.  The <isloop> is expected to have a "status" attribute, named "attributeStatus".
- ${attributeStatus.last} is checked to see whether a particular attribute row should include the
  Quantity drop-down menu

</iscomment>
<div class="attribute js-saks-newdesign" data-swatchlimit="${dw.system.Site.current.getCustomPreferenceValue('colorSwatchDisplayLimit')}">
    <isset name="colorSwatchDisplayLimit" value="${dw.system.Site.current.getCustomPreferenceValue('colorSwatchDisplayLimit')}" scope="page" />

<isset name="actualColorAttr" value="${0}" scope="page" />
<isloop items="${attr.values}" var="attrValue">
	<isif condition="${attrValue.selectable || (!attrValue.selectable && attrValue.waitlist)}">
		<isset name="actualColorAttr" value="${actualColorAttr + 1}" scope="page" />
	</isif>
</isloop>

	 <!-- Enable the swatches for home appliance products even if they are OOS, when the "searchableIfUnavailable" flag is set to true. SFDEV-5925 -->
	<isset name="alwaysEnableSwatches" value="${false}" scope="page" />
	<isif condition="${product.hbcProductType === 'home' && pdict.searchableIfUnavailable}">
	    <isset name="alwaysEnableSwatches" value="${true}" scope="page" />
	</isif>

    <isif condition="${attr.swatchable && actualColorAttr <= colorSwatchDisplayLimit}">
        <!-- Select <Attribute> Label -->
		<isif condition="${attr.attributeSelectedValue}">
			<span class="${attr.id} non-input-label">
				<isif condition="${pdict.isEdit || pdict.wishlistAddToBag}">
					<span class="attr-name"> <span><isprint value="${Resource.msg('label.color.edit', 'common', null)}" /></span> <span class="color-value">${attr.attributeSelectedValue.toLowerCase()}</span> </span>
				<iselse/>
					
					<span class="attr-name adobelaunch__colorlink" data-adobelaunchcolorproductid="${product.masterProductID}" data-adobelaunchproductcolor="${attr.attributeSelectedValue}">
						<span class="text1">${attr.displayName}</span> 
						<span class="text2 color-value attribute-displayValue">${attr.displayValue.toLowerCase()}</span>
					</span>
				</isif>
        	</span>
        <iselse/>
        	<span class="${attr.id} non-input-label">
            	<span class="attr-name">${attr.displayName}</span>
        	</span>
		</isif>

        <!-- Circular Swatch Image Attribute Values -->
				<fieldset>
					<legend class="sr-only">${Resource.msg('label.color.textonly', 'common', null)}</legend>
					<ul class="color-wrapper radio-group-list" role="radiogroup">
						<isloop items="${attr.values}" var="attrValue">
								<isif condition="${attrValue.selected}">
									<isset name="isAttrSelected" value="${true}" scope="page" />
								</isif>
								<isif condition="${!(isBundle && product.productType === 'variant' && !attrValue.selected) && attr.values.length > 1}">
								<li role="radio"
										aria-checked="${attrValue.selected ? 'true' : 'false'}">
										<button class="color-attribute radio-group-trigger adobelaunch__colorlink
														${attrValue.selected ? 'selected' : ''}
														${attrValue.selectable ? 'selectable' : 'unselectable'}
												"
														aria-label="${Resource.msg('label.select', 'common', null)} ${attr.displayName} ${attrValue.displayValue}"
														aria-describedby="${attrValue.id}"
														data-adobelaunchcolorproductid="${product.masterProductID}" data-adobelaunchproductcolor="${attrValue.value}"
														data-url="${attrValue.url}" ${ product.productType === "variant" && isBundle ? "disabled" : "" }
														title="${attrValue.value}"
										>
												<span data-attr-value="${attrValue.value}"
															class="
																	${attr.id}-value
																	swatch-circle
																	swatch-value
																	swatch-box
																	${attrValue.selected ? 'selected' : ''}
																	${attrValue.selectable ? 'selectable' : 'unselectable'}
															"

															<isif condition ="${(attrValue.images['swatch'][0]) && (attrValue.images['swatch'][0].url.indexOf('/') === -1)}">
															style="background-color: ${attrValue.images['swatch'].length > 0 ? attrValue.images['swatch'][0].url : ''};"
												>
															<iselse>
															style="background-image: url(${attrValue.images['swatch'].length > 0 ? attrValue.images['swatch'][0].url : ''})"
												>
															</isif>


												</span>
												<span id="${attrValue.id}" class="sr-only selected-assistive-text">
														${attrValue.selected ? Resource.msg('msg.assistive.selected.text', 'common', null) : ''}
												</span>
										</button>
										</li>
								</isif>
						</isloop>
				</ul>
			</fieldset>

    <iselse>
		<div class="pdp-label-wrapper">
			<!-- Select <Attribute> Label -->
			<isif condition="${product.sizeChartTemplate && attr.id === 'size' && !(pdict.isQuickview)}">
				<div class="size_guide adobelaunch__sizeguide" data-adobelaunchsizeguideproductid="${product.masterProductID}" data-masterid="${product.masterProductID}">
					<button type="button" class="btn btn-link">${Resource.msg('label.sizechart', 'product', null)}</button>
				</div>
			</isif>
			<isif condition="${!empty(attr.selectedAttribute)}">
				<span class="${attr.id} non-input-label ${attr.id == 'color' ? 'drop-down-label' : ''}">
					<isif condition="${pdict.isEdit || pdict.wishlistAddToBag}">
						<span class="attr-name"><span class="attribute-displayName">${attr.displayName}</span> <span class="color-value attribute-displayValue">${attr.displayValue.toLowerCase()}</span></span>
					<iselse/>
						<span class="attr-name">
							<isprint value="${attr.attrDisplay}" encoding="off" />
						</span>
					</isif>
				</span>
			<iselse/>
				<span class="${attr.id} non-input-label ${attr.id == 'color' ? 'drop-down-label' : ''}" for="${attr.id}-${loopState && loopState.count ? loopState.count : '1' }">
					<isif condition="${attr.id === 'color' && attr.attributeSelectedValue}">
						<span class="attr-name"><isprint value="${Resource.msgf('label.color', 'common', null, attr.attributeSelectedValue)}" /></span>
					<iselse>
						<isif condition="${pdict.isEdit || pdict.wishlistAddToBag}">
							<span class="attr-name"><span class="attribute-displayName">${attr.displayName}</span> <span class="color-value attribute-displayValue">${attr.displayValue.toLowerCase()}</span></span>
						<iselse/>
							<span class="attr-name">
								<isprint value="${attr.attrDisplay}" encoding="off" />
							</span>
						</isif>
					</isif>
				</span>
			</isif>

			<!-- Attribute Values Drop Down Menu -->
			<isif condition="${ attr.id == 'color' }">
				<div class="js-color-swatch-wrapper">
					<div class="custom-select custom-color-dropdown">
						<select class="form-control select-${attr.id}" id="${attr.id}-${loopState && loopState.count ? loopState.count : '1' }" ${ product.productType === "variant" && isBundle ? "disabled" : "" }>
							<option>
								${Resource.msg('label.select.color', 'common', null)}
							</option>
							<isloop items="${attr.values}" var="attrValue">
								<isif condition="${!(isBundle && product.productType === 'variant' && !attrValue.selected) && attr.values.length > 1}">
									<option class="adobelaunch__colorlink attribute-option-${attrValue.displayValue}" value="${attrValue.url}" data-attr-value="${attrValue.value}"
										data-adobelaunchcolorproductid="${product.masterProductID}" data-adobelaunchproductcolor="${attrValue.value}"
										${!attrValue.selectable && !alwaysEnableSwatches || (attrValue.variantAvailabilityStatus != 'IN_STOCK' && attrValue.sfccPreorder != 'T') ? 'disabled' : ''}
										${attrValue.selected ? 'selected' : ''}
										<isif condition ="${(attrValue.images['swatch'][0]) && (attrValue.images['swatch'][0].url.indexOf('/') === -1)}">
											data-background-color="${attrValue.images['swatch'].length > 0 ? attrValue.images['swatch'][0].url : ''}"
										<iselse>
											data-background-image="${attrValue.images['swatch'].length > 0 ? attrValue.images['swatch'][0].url : ''}"
										</isif>
									>
										${attrValue.displayValue}
									</option>
								</isif>
							</isloop>
						</select>
					</div>
				</div>
			</isif>
		</div>
        <!-- Attribute Values Drop Down Menu -->
        <isif condition="${ attr.id == 'color' }">
			<div class="js-color-swatch-wrapper">
				<!-- We will display all swatches as well  -->
				<isinclude template="product/components/colorvariationAttribute"/>
			</div>
		<iselseif condition="${attr.values.length > 0}">
		    <isset name="actualSizeAttr" value="${Number('0')}" scope="page" />
			<isloop items="${attr.values}" var="attrValue">
				<isif condition="${attrValue.selectable || (!attrValue.selectable && attrValue.waitlist)}">
					 <isset name="actualSizeAttr" value="${actualSizeAttr + Number('1')}" scope="page" />
				</isif>
			</isloop>
			<isset name="showSizeDropdown" value="${false}" scope="Page" />
			<isif condition="${(attr.selectedSizeClass === 'swatch-display-three' && actualSizeAttr > 12) || (attr.selectedSizeClass === 'swatch-display-four' && actualSizeAttr > 16) || (attr.selectedSizeClass === 'swatch-display-six' && actualSizeAttr > 24) || (attr.selectedSizeClass === 'swatch-display-max' && actualSizeAttr > 16)}">
				<isset name="showSizeDropdown" value="${true}" scope="Page" />
			</isif>
			<fieldset>
				<legend class="sr-only">
					<isprint value="${attr.attrDisplay}" encoding="off" />
				</legend>
				<ul role="radiogroup" class="radio-group-list size-attribute ${attr.selectedSizeClass} ${showSizeDropdown === true ? 'd-none': 'show-size-dropdown'}">
					<isloop items="${attr.values}" var="attrValue">
						<isif condition="${attrValue.selectable || (!attrValue.selectable && attrValue.waitlist)}">
							<li class="${attrValue.value.replace(/[-_ ]/g, '').toLowerCase() === 'nosize'? 'd-none' : ''}"   		    	data-attr-value="${attrValue.value}"
								role="radio"
								data-selectableattribute="${attrValue.selectable || (!attrValue.selectable && attrValue.waitlist)}"
								aria-checked="${attrValue.selected ? 'true' : 'false'}"
								${!attrValue.selectable && !alwaysEnableSwatches || (attrValue.variantAvailabilityStatus != 'IN_STOCK' && attrValue.sfccPreorder != 'T') ? 'disabled' : ''}
								${attrValue.selected ? 'selected' : ''}>
									<a class="adobelaunch__sizelink radio-group-trigger"
										href="${attrValue.pdpUrl}" data-href="${attrValue.url}"
										data-adobelaunchsizeproductid="${product.masterProductID}" data-adobelaunchproductsize="${attrValue.value}">
										${attrValue.displayValue}
									</a>
							</li>
						</isif>	
					</isloop>
				</ul>
			</fieldset>
			<div class="form-group ${showSizeDropdown === true ? 'show-size-dropdown-holder': 'd-none'}">
				<select class="custom-select js-size-dropdown form-control select-${attr.id} ${showSizeDropdown === true ? 'show-size-dropdown': 'd-none'}"
					id="${attr.id}-${loopState && loopState.count ? loopState.count : '1' }">
					<option value="${attr.resetUrl}">
						${Resource.msg('label.select.size', 'common', null)}
					</option>
					<isloop items="${attr.values}" var="attrValue">
						<isif condition="${attrValue.selectable || (!attrValue.selectable && attrValue.waitlist)}">
							<option value="${attrValue.url}" data-attr-value="${attrValue.value}"
								data-selectableattribute="${attrValue.selectable || (!attrValue.selectable && attrValue.waitlist)}"
								${!attrValue.selectable ? 'disabled' : ''}
								${attrValue.selected ? 'selected' : ''}
							>
								${attrValue.displayValue}
							</option>
						</isif>
					</isloop>
				</select>
			</div>
    	</isif>
    </isif>
</div>