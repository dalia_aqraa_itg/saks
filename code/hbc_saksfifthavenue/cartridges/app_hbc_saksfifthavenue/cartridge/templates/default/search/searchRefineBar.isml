<div class="refinement-bar-mob ${pdict.designerPrefSelectedCount || pdict.sizePrefSelectedCount ? 'with-save-pref-btn': ''}" role="dialog" aria-labelledby="opensFilter">
<!--- Close button --->
<div class="filter-header">
  <div class="header-bar d-md-none clearfix">
    <button class="float-right button-close">
      <span></span>
    </button>
  </div>
  <div class="filter-data">
    ${Resource.msg('link.filter', 'search', null)}
  </div>

  <!--- Results count and Reset button --->
  <isif condition="${pdict.productSearch.productIds.length > 0 && pdict.productSearch.selectedFilters.length > 0}">
    <div class="applied-refinement">
      <div class="secondary-bar col-12 offset-sm-4 offset-md-0 col-sm-4 col-md-12">
        <button class="applied btn btn-block btn-outline-primary" data-href="${pdict.productSearch.resetLink}">
          ${Resource.msgf('link.applied', 'search', null, pdict.filterCount.toFixed())}
        </button>
      </div>
    </div>
  </isif>
</div>

<div class="refinement-wrapper">
  <div class="refinements">
    <isloop items="${pdict.productSearch.refinements}" var="refinement">
      <isif condition="${refinement.attributeID == 'sfccPreorder' && refinement.values.length < 2}">
        <iscontinue/>
      </isif>
      <isif condition="${!refinement.hideRefinement}">
        <iscomment> TODO: change refinement-_______ to use attributeID instead of displayName because displayNames will and have changed, temp fix adding new class since logic elsewhere may be based on the class using displayName </iscomment>
        <fieldset class="card collapsible-xl ${refinement.attributeID === 'onlineFlag' ? 'bfx-remove-element' : ''} ${refinement.displayName.toLowerCase().replace(/ /g, '') == 'getitfast' ? 'bfx-remove-element' : ''} refinement adobelaunch__mainnav refinement-id-${refinement.attributeID.toLowerCase().replace(/\//g, '').replace(/\?/g,'').replace(/\&/g,'').replace(/ /g, '-')} refinement-${refinement.displayName.toLowerCase().replace(/\//g, '').replace(/\?/g,'').replace(/\&/g,'').replace(/ /g, '-')} ${refinement.addSearchBar && !(refinement.values && refinement.values.length < pdict.maxRefinementValueLimit) ? 'refinement-search' : ''} ${refinement.colorRefinement ? 'refinement-color' : ''} ${refinement.isPriceRefinement ? 'refinement-price' : ''} ${refinement.isCategoryRefinement ? 'refinement-category' : ''}">
          <legend class="card-header">
            <button class="btn text-left btn-block ${refinement.selectedCount > 0 ? 'clicked' : ''}"
              type="button"
              data-close-siblings="true"
              aria-controls="refinement-${refinement.displayName.toLowerCase().replace(/ /g, '-')}"
              id="refinement-${refinement.attributeID}"
              aria-expanded="false">
              <h2 class="title">${refinement.displayName}</h2>
            </button>
          </legend>
          <div class="card-body content value"
            id="refinement-${refinement.displayName.toLowerCase().replace(/ /g, '-')}">
            <!--- CATEGORY REFINEMENTS --->
            <isif condition="${refinement.isCategoryRefinement && !refinement.allNonShowInRefinementMenu}">
              <isset name="categories" value="${refinement.values}" scope="page" />
              <isset name="categoryCount" value="${0}" scope="page" />
              <isset name="showBrandWord" value="true" scope="page" />
              <isset name="showAllResults" value="true" scope="page" />

              <isinclude template="search/refinements/categories" />
              <isif condition="${categoryCount.toFixed() > pdict.plpRefineViewMoreCatLimit}">
                <button type="button" class="btn btn-link view-more-less" data-viewmore="${Resource.msg('search.view.more', 'search', null)}" data-viewless="${Resource.msg('search.view.less', 'search', null)}">${Resource.msg('search.view.more', 'search', null)}</button>
              </isif>
            </isif>

            <!--- ATTRIBUTE REFINEMENTS --->
            <isif condition="${refinement.isAttributeRefinement && refinement.attributeID !== 'onlineFlag'}">
              <isinclude template="search/refinements/attributes" />
            </isif>
            <!--- BOPIS REFINEMENTS --->
            <isif condition="${pdict.isBopisEnabled || pdict.isEnabledforSameDayDelivery}">
              <isif condition="${refinement.attributeID === 'onlineFlag'}">
                <isinclude url="${pdict.storeRefineUrl}"/>
              </isif>
            </isif>
            <!--- PRICE REFINEMENTS --->
            <isif condition="${refinement.isPriceRefinement}">
              <isinclude template="search/refinements/prices" />
            </isif>

            <!--- PROMOTION REFINEMENTS --->
            <isif condition="${refinement.isPromotionRefinement}">
              <isinclude template="search/refinements/promotions" />
            </isif>
          </div>
        </fieldset>
      </isif>
    </isloop>
  </div>
  <div class="shop-item-btn">
  <button class="shop-item d-block d-lg-none btn btn-block btn-primary">${Resource.msgf('search.filter.shop', 'search', null, pdict.productSearch.count)}</button>
    <button data-pref-selected-count="${pdict.designerPrefSelectedCount}" class="d-lg-none btn btn-block btn-secondary mobile-pc-info-cta-save mobile-pc-designers ${!pdict.customerLogin ? 'sign-in': ''}" data-url="${URLUtils.url('Search-ShowLogin', 'savepreferencetype', 'DesignerPreferences')}" data-app-url="${URLUtils.url('Login-Show')}">${Resource.msg('search.mypreferences.designer.savebtn', 'search', null)}</button>
    <isif condition="${pdict.showMySizes}">
      <button data-pref-selected-count="${pdict.sizePrefSelectedCount}" class="d-lg-none btn btn-block btn-secondary mobile-pc-info-cta-save mobile-pc-sizes ${!pdict.customerLogin ? 'sign-in': ''}" data-url="${URLUtils.url('Search-ShowLogin', 'savepreferencetype', 'SizePreferences')}" data-app-url="${URLUtils.url('Login-Show')}">${Resource.msg('search.mypreferences.size.savebtn', 'search', null)}</button>
      <button class="d-lg-none btn btn-block btn-secondary mobile-pc-info-cta-saved">
        <span class="pc-info-cta-saved-text">${Resource.msg('search.mypreferences.saved', 'search', null)}</span>
        <div class="pc-info-cta-saved-icon">
          <div class="pc-info-cta-saved-icon--check svg-svg-107-check-thin pc-saved-icon--check svg-svg-107-check-thin-dims"></div>
        </div>
      </button>
    </isif>
  </div>
</div>
</div>
<div class="applied-filters-bar d-none">
  <isinclude template="search/appliedFilterSection" />
</div>
<isif condition="${pdict.DesignerPreferences && Object.keys(pdict.DesignerPreferences).length > 0}">
    <div class="my-designers-bar d-none">
      <isinclude template="search/myDesignersSection" />
    </div>
</isif>
<isif condition="${pdict.CurrentCatTempObj && Object.keys(pdict.CurrentCatTempObj).length > 0}">
  <div class="my-sizes-bar d-none">
    <isinclude template="search/mySizesSection" />
  </div>
</isif>
