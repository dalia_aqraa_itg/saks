<div class="card-body shipping-content">
    <div class="form-main-label-container ${pdict.customer.registeredUser || showGiftOptionStep === true ? 'd-none' : ''}">
        <span class="form-main-label"><isprint value="${Resource.msg('subheading.checkout.shipping', 'checkout', null)}" /></span>
        <iscomment>
            <span class="form-sub-label"><isprint value="${Resource.msg('shipping.fields.required', 'checkout', null)}" /></span>
        </iscomment>
    </div>
    <form class="shipping-form" autocomplete="on" novalidate
          action="${showGiftOptionStep === true ? URLUtils.url('CheckoutServices-SubmitGiftOptions') : URLUtils.url('CheckoutShippingServices-SubmitShipping')}"
          data-shipment-submit="${URLUtils.url('CheckoutShippingServices-SubmitShipping')}"
          data-address-submit="${URLUtils.url('CheckoutShippingServices-AddAddress')}"
          <isprint value=${pdict.forms.shippingForm.attributes} encoding="off"/>
    >
        <isif condition="${lineItem}">
            <input name="productLineItemUUID" type="hidden" value="${lineItem.UUID}" />
        </isif>

        <input name="originalShipmentUUID" type="hidden" value="${shippingModel.UUID}" />
        <input name="shipmentUUID" type="hidden" value="${shippingModel.UUID}" />

        <div class="shipping-address ${pdict.order.usingMultiShipping ? 'd-none' : ''}">
	        <div class="coutry-rest-msg hbc-alert-error d-none">
				<isprint value="${Resource.msg('address.country.restriction.msg','address',null)}" />
			</div>
            <fieldset class="shipment-selector-block custom-form-spacing ${showGiftOptionStep === true ? 'd-none' : ''} ${pdict.customer.registeredUser ? '' : 'd-none' }">
            	<isset name="addresses" value="${pdict.customer.addresses}" scope="pdict" />
            	<isset name="preferredAddress" value="${pdict.customer.preferredAddress}" scope="pdict" />
                <div id="customer-addresses" class="customer-addresses saks-only row">
                	<isinclude template="checkout/shipping/checkoutCustomerAddress" />
                </div>
            </fieldset>

            <fieldset class="shipping-address-block custom-form-spacing ${pdict.customer.registeredUser || showGiftOptionStep === true ? 'd-none' : ''} " data-poenabled=${pdict.hasUSPSRestrictedItems} data-restricted-sates="${pdict.restrictedStates}" data-restricted-sates-usps="${pdict.restrictedStatesUSPS}">
                <isinclude template="checkout/shipping/shippingAddress" />
            </fieldset>
		    
            <fieldset class="shipping-method-block ${(pdict.customer.registeredUser && pdict.customer.addresses.length === 0) || showGiftOptionStep === true ? 'd-none' : ''}">
                <div class="shipping-method-heading saks-only ${pdict.customer.registeredUser && pdict.customer.addresses.length > 0 ? '' : 'd-none'}">${Resource.msg('heading.shipping.method', 'checkout', null)}</div>
                <div class="po-check-message hbc-alert-error d-none">${Resource.msg('po.items.message','checkout',null)}</div>
                <div class="no-shipping-method-msg hbc-alert-error d-none"><isprint value="${Resource.msg('no.shipping.methods.message','checkout',null)}" /></div>
                <div class="restricted-state-message hbc-alert-error d-none"><isprint value=" ${Resource.msg('restrictedstate.error.message','checkout',null)}" /></div>
                <isinclude template="checkout/shipping/shippingMethod" />             
            </fieldset>
			<div class="signature-required hbc-alert-info ${showGiftOptionStep === true ? 'd-none' : ''} ${(pdict.signatureRequired || !pdict.signatureRequired) && pdict.signatureThreshold && pdict.order.totals.grandTotalValue > pdict.signatureThreshold ? 'verbiage-shown' : 'd-none verbiage-hidden'}">
				<isprint value="${Resource.msg('signature.required.message', 'checkout', null)}" />
				<span class="custom-tooltip">
				    <button class="tooltip-info saks-tooltip-info" type="button" aria-label="${Resource.msg('tooltip.moreinfo', 'product', null)}">i</button>
		            <div class="tooltip-content" data-layout="small">${Resource.msg('signature.tooltip.message', 'checkout', null)}</div>
	        	</span>
			</div>	
            <fieldset class="signature-checkbox ${showGiftOptionStep === true ? 'd-none' : ''} ${(pdict.signatureRequired || !pdict.signatureRequired) && pdict.signatureThreshold && pdict.order.totals.grandTotalValue <= pdict.signatureThreshold ? 'box-shown' : 'd-none box-hidden'}">
                <isinclude template="checkout/shipping/signatureRequired" />
            </fieldset>
            <iscomment>ShopRunner - Shipping Option Div</iscomment>
            <div class="shop-runner-eligible-section bfx-remove-element">
	            <isif condition="${shoprunnerenabled == true && showGiftOptionStep != true && !pdict.isAgentUser}">
	                <div class="checkout-shop-runner">
	                    <isshoprunner p_divname="sr_headerDiv">
	                </div>
	            </isif>
			</div>
			
            <fieldset class="gift-message-block gift-block ${pdict.giftMessageAction === 'hide' && pdict.giftEligible === '' ? 'hidden-global' : ''} ${(pdict.giftMessageAction === 'hide' && pdict.giftEligible === '') || (pdict.customer.registeredUser && pdict.customer.addresses.length === 0) ? 'd-none hide-giftwrap' : ''}">
                <isinclude template="checkout/shipping/isGift" />
            </fieldset>
        </div>

        <div class="view-address-block ${shippingModel.shippingAddress || showGiftOptionStep === true ? '' : 'd-none' }">
            <isinclude template="checkout/shipping/shippingCard" />
        </div>

        <input type="hidden" name="${pdict.csrf.tokenName}" value="${pdict.csrf.token}"/>
    </form>
    <div class="row shipping-next-step-button-row">
        <div class="col-12 next-step-button-disable next-step-button">
            <div>
                <button class="btn btn-primary btn-block" value="submit-shipping" type="submit" name="submit" >
                    <isprint value="${Resource.msg('button.next.general', 'checkout', null)}" />
                </button>
            </div>
        </div>
    </div>
	<div class="row shipping-save-address-block shipping-address-block d-none">
	    <div class="col-6 shipping-next-cancel">
	        <button type="submit" name="cancel" class="btn btn-save-cancel btn-block btn-secondary"><isprint value="${Resource.msg('button.cancel','account',null)}" /></button>
	    </div>
	    <div class="col-6 shipping-next-step-button-row shipping-save-address">
	        <div class="next-step-button">
	          <div>
	            <button class="btn btn-primary btn-block" type="submit" name="submit">
	                <isprint value="${Resource.msg('button.save', 'checkout', null)}" />
	            </button>
	          </div>
	        </div>
	    </div>
	</div>
</div>
