'use strict';

var formValidation = require('./components/formValidation');

$(document).ready(function () {
    $('form.checkout-registration').submit(function (e) {
        var form = $(this);
        e.preventDefault();
        var url = form.attr('action');
        form.spinner().start();
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: form.serialize(),
            success: function (data) {
                form.spinner().stop();
                if (!data.success) {
                    formValidation(form, data);
                } else {
                    location.href = data.redirectUrl;
                }
            },
            error: function (err) {
                if (err.responseJSON.redirectUrl) {
                    window.location.href = err.responseJSON.redirectUrl;
                }
                form.spinner().stop();
            }
        });
        return false;
    });
    $('.show-pwd').on('click', function () {
        var input = $($(this).attr('toggle'));
        if (input.val() !== '') {
          if (input.attr('type') === 'password') {
            input.attr('type', 'text');
            $(this).text($(this).attr('data-hidepassword'));
          } else {
            input.attr('type', 'password');
            $(this).text($(this).attr('data-showpassword'));
          }
        }
    });
});
