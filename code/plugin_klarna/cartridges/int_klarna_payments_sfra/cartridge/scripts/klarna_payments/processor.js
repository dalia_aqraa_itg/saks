/* globals empty, session, request */

'use strict';

var KlarnaPaymentsConstants = require('*/cartridge/scripts/util/klarnaPaymentsConstants');

var PAYMENT_METHOD = KlarnaPaymentsConstants.PAYMENT_METHOD;
var CREDIT_CARD_PROCESSOR_ID = KlarnaPaymentsConstants.CREDIT_CARD_PROCESSOR_ID;
var NOTIFY_EVENT_TYPES = KlarnaPaymentsConstants.NOTIFY_EVENT_TYPES;
var KLARNA_FRAUD_STATUSES = KlarnaPaymentsConstants.FRAUD_STATUS;

var Transaction = require('dw/system/Transaction');
var PaymentMgr = require('dw/order/PaymentMgr');
var OrderMgr = require('dw/order/OrderMgr');
var Logger = require('dw/system/Logger');
var StringUtils = require('dw/util/StringUtils');
var Site = require('dw/system/Site');
var Status = require('dw/system/Status');
var HookMgr = require('dw/system/HookMgr');
var log = Logger.getLogger('KlarnaPayments');

var KlarnaPaymentsOrderRequestBuilder = require('*/cartridge/scripts/klarna_payments/requestBuilder/order');
var KlarnaPaymentsHttpService = require('*/cartridge/scripts/common/klarnaPaymentsHttpService');
var KlarnaPaymentsApiContext = require('*/cartridge/scripts/common/klarnaPaymentsApiContext');
var KlarnaLocaleMgr = require('*/cartridge/scripts/klarna_payments/locale');
var KlarnaSessionManager = require('*/cartridge/scripts/common/klarnaSessionManager');

var klarnaLocaleMgr = new KlarnaLocaleMgr();
var klarnaSessionManager = new KlarnaSessionManager(session, klarnaLocaleMgr);

/**
 * Creates a Klarna payments order through Klarna API
 * @param {dw.order.Order} 			order 			SCC order object
 * @param {dw.object.CustomObject} 	localeObject 	corresponding to the locale Custom Object from KlarnaCountries
 *
 * @private
 * @return {Object} requestObject Klarna Payments request object
 */
function getOrderRequestBody(order, localeObject) {
    var orderRequestBuilder = new KlarnaPaymentsOrderRequestBuilder();
    orderRequestBuilder.setParams({
        order: order,
        localeObject: localeObject
    });

    return orderRequestBuilder.build();
}

/**
 * Attempts to create a full-amount capture through Klarna API.
 * @param {string} klarnaOrderID KP Order ID
 * @param {dw.object.CustomObject} localeObject corresponding to the locale Custom Object from KlarnaCountries.
 * @param {Object} captureData capture data.
 */
function createCapture(klarnaOrderID, localeObject, captureData) {
    var klarnaPaymentsHttpService = new KlarnaPaymentsHttpService();
    var klarnaApiContext = new KlarnaPaymentsApiContext();
    var requestUrl = StringUtils.format(klarnaApiContext.getFlowApiUrls().get('createCapture'), klarnaOrderID);
    var requestBody = {
        captured_amount: captureData.amount
    };

    klarnaPaymentsHttpService.call(requestUrl, 'POST', localeObject.custom.credentialID, requestBody);
}

/**
 * Calls Klarna Create Order API.
 *
 * @param {dw.order.Order} order SCC order object
 * @param {dw.object.CustomObject} localeObject corresponding to the locale Custom Object from KlarnaCountries
 *
 * @return {Object|null} Klarna Payments create order response data on success, null on failure.
 */
function callKlarnaCreateOrderAPI(order, localeObject) {
    var klarnaPaymentsHttpService = {};
    var klarnaApiContext = {};
    var requestBody = {};
    var requestUrl = '';
    var response = {};
    var klarnaAuthorizationToken = session.privacy.KlarnaPaymentsAuthorizationToken;

    try {
        klarnaPaymentsHttpService = new KlarnaPaymentsHttpService();
        klarnaApiContext = new KlarnaPaymentsApiContext();
        requestBody = getOrderRequestBody(order, localeObject);
        requestUrl = StringUtils.format(klarnaApiContext.getFlowApiUrls().get('createOrder'), klarnaAuthorizationToken);

        response = klarnaPaymentsHttpService.call(requestUrl, 'POST', localeObject.custom.credentialID, requestBody);

        return response;
    } catch (e) {
        log.error('Error in creating Klarna Payments Order: {0}', e.message + e.stack);
        return null;
    }
}

/**
 * Call Klarna Payments API to acknowledge the order
 * @param {string} 					klarnaPaymentsOrderID 	Klarna Payments Order ID
 * @param {dw.object.CustomObject} 	localeObject 			corresponding to the locale Custom Object from KlarnaCountries
 *
 * @private
 * @return {void}
 */
function acknowledgeOrder(klarnaPaymentsOrderID, localeObject) {
    var klarnaHttpService = {};
    var klarnaApiContext = {};
    var klarnaOrderID = klarnaPaymentsOrderID;
    var requestUrl = '';

    try {
        klarnaHttpService = new KlarnaPaymentsHttpService();
        klarnaApiContext = new KlarnaPaymentsApiContext();
        requestUrl = StringUtils.format(klarnaApiContext.getFlowApiUrls().get('acknowledgeOrder'), klarnaOrderID);

        klarnaHttpService.call(requestUrl, 'POST', localeObject.custom.credentialID);
    } catch (e) {
        log.error('Error in acknowlidging order: {0}', e);
    }
}

/**
 * Cancels a Klarna order through Klarna API
 * @param {dw.order.Order} order SCC order object
 * @param {dw.object.CustomObject} localeObject corresponding to the locale Custom Object from KlarnaCountries
 *
 * @return {boolean} true if order has been successfully cancelled, otherwise false
 */
function cancelOrder(order, localeObject) {
    var klarnaPaymentsHttpService = {};
    var klarnaApiContext = {};
    var requestUrl = '';

    try {
        klarnaPaymentsHttpService = new KlarnaPaymentsHttpService();
        klarnaApiContext = new KlarnaPaymentsApiContext();
        requestUrl = StringUtils.format(klarnaApiContext.getFlowApiUrls().get('cancelOrder'), order.custom.kpOrderID);

        klarnaPaymentsHttpService.call(requestUrl, 'POST', localeObject.custom.credentialID, null);
    } catch (e)	{
        log.error('Error in cancelling Klarna Payments Order: {0}', e);
        return false;
    }
    return true;
}

/**
 * Find the first klarna payment transaction within the order (if exists).
 *
 * @param {dw.order.Order} order - The Order currently being placed.
 * @returns {dw.order.PaymentTransaction} Klarna Payment Transaction
 */
function findKlarnaPaymentTransaction(order) {
    var paymentTransaction = null;
    var paymentInstruments = order.getPaymentInstruments(PAYMENT_METHOD);

    if (!empty(paymentInstruments) && paymentInstruments.length) {
        paymentTransaction = paymentInstruments[0].paymentTransaction;
    }

    return paymentTransaction;
}

/**
 * Returns full order amount for a DW order.
 *
 * @param {dw.order.Order} dwOrder DW Order object.
 * @return {dw.value.Money} payment transaction amount.
 */
function getPaymentInstrumentAmount(dwOrder) {
    var kpTransaction = findKlarnaPaymentTransaction(dwOrder);

    var transactionAmount = kpTransaction.getAmount();

    return transactionAmount;
}

/**
 * Handle auto-capture functionality.
 *
 * @param {dw.order.Order} dwOrder DW Order object.
 * @param {string} kpOrderId Klarna Payments Order ID
 * @param {dw.object.CustomObject} localeObject locale object (KlarnaCountries).
 */
function handleAutoCapture(dwOrder, kpOrderId, localeObject) {
    var captureData = {
        amount: Math.round(getPaymentInstrumentAmount(dwOrder).getValue() * 100)
    };

    try {
        createCapture(kpOrderId, localeObject, captureData);

        Transaction.wrap(function () {
            dwOrder.setPaymentStatus(dwOrder.PAYMENT_STATUS_PAID);
        });
    } catch (e) {
        log.error('Error in creating Klarna Payments Order Capture: {0}', e.message + e.stack);

        throw e;
    }
}

/**
 * Place an order using OrderMgr. If order is placed successfully,
 * its status will be set as confirmed, and export status set to ready.
 * Autocapture is handled after placing the order if enabled).
 * At last, the order is acknowledged via Klarna Payments API.
 *
 * @param {dw.order.Order} 			order 					SCC order object
 * @param {string} 					klarnaPaymentsOrderID 	Klarna Payments Order ID
 * @param {dw.object.CustomObject} 	localeObject 			Klarna Payments locale Object
 *
 * @return {void}
 */
function placeOrder(order, klarnaPaymentsOrderID, localeObject) {
    Transaction.wrap(function () {
        var placeOrderStatus = OrderMgr.placeOrder(order);
        if (placeOrderStatus === Status.ERROR) {
            OrderMgr.failOrder(order);
            throw new Error('Failed to place order.');
        }
        order.setConfirmationStatus(order.CONFIRMATION_STATUS_CONFIRMED);
        order.setExportStatus(order.EXPORT_STATUS_READY);
    });

    if (!order.custom.kpIsVCN) {
        try {
            var autoCaptureEnabled = Site.getCurrent().getCustomPreferenceValue('kpAutoCapture');

            if (autoCaptureEnabled) {
                handleAutoCapture(order, klarnaPaymentsOrderID, localeObject);
            }

            acknowledgeOrder(klarnaPaymentsOrderID, localeObject);
        } catch (e) {
            log.error('Order could not be placed: {0}', e.message + e.stack);
        }
    }
}

/**
 * Update existing DW order with information from Klarna VCN settlement
 * @param {dw.order.order} order DW Order to be updated
 * @param {Object} cardInfo VCN CC data from Klarna response
 */
function updateOrderWithVCNCardInfo(order, cardInfo) {
    var orderForUpdate = order;

    Transaction.begin();

    orderForUpdate.custom.kpVCNBrand = cardInfo.brand;
    orderForUpdate.custom.kpVCNHolder = cardInfo.holder;
    orderForUpdate.custom.kpVCNCardID = cardInfo.card_id;
    orderForUpdate.custom.kpVCNPCIData = cardInfo.pci_data;
    orderForUpdate.custom.kpVCNIV = cardInfo.iv;
    orderForUpdate.custom.kpVCNAESKey = cardInfo.aes_key;
    orderForUpdate.custom.kpIsVCN = true;

    Transaction.commit();
}

/**
 * Create VCN settlement
 * @param {dw.order.Order} order SCC order object
 * @param {string} klarnaPaymentsOrderID Klarna Payments order id
 * @param {dw.object.CustomObject} localeObject corresponding to the locale Custom Object from KlarnaCountries
 *
 * @return {boolean} true if VCN settlement is created successfully, otherwise false
 */
function createVCNSettlement(order, klarnaPaymentsOrderID, localeObject) {
    var klarnaPaymentsHttpService = {};
    var klarnaApiContext = {};
    var requestBody = {};
    var requestUrl = '';
    var response = {};

    try {
        klarnaPaymentsHttpService = new KlarnaPaymentsHttpService();
        klarnaApiContext = new KlarnaPaymentsApiContext();
        requestBody = {
            order_id: klarnaPaymentsOrderID,
            key_id: Site.getCurrent().getCustomPreferenceValue('kpVCNkeyId')
        };
        requestUrl = klarnaApiContext.getFlowApiUrls().get('vcnSettlement');

        response = klarnaPaymentsHttpService.call(requestUrl, 'POST', localeObject.custom.credentialID, requestBody);
        if (empty(response.settlement_id) || empty(response.cards)) {
            throw new Error('Could not create a VCN settlement');
        }
        var klarnaOrder = getKlarnaOrder(klarnaPaymentsOrderID);
        if (klarnaOrder) {
            updateOrderBillingAddress(order, klarnaOrder);
            setOrderPaymentPlan(order, klarnaOrder);
        } 

        updateOrderWithVCNCardInfo(order, response.cards[0]);
    } catch (e) {
        log.error('Error in creating Klarna Payments VCN Settlement: {0}', e);
        return false;
    }

    return true;
}

function setOrderPaymentPlan(sfccOrder, klarnaOrder) {
    try {
        if (klarnaOrder.initial_payment_method) {
            Transaction.wrap(function () {
                sfccOrder.custom.klarnaPaymentPlan = JSON.stringify(klarnaOrder.initial_payment_method);
            });
        }
    } catch (error) {
        var e = error;
        log.error('Error while retrieving order: {0}', e);
    }
}
/**
 * Update billing address based on what is returned from Klarna Service.
 */
function updateOrderBillingAddress(sfccOrder, klarnaOrder) {
    try {
        Transaction.wrap(function () {
            sfccOrder.billingAddress.address1 = klarnaOrder.billing_address.street_address;
            sfccOrder.billingAddress.city = klarnaOrder.billing_address.city;
            sfccOrder.billingAddress.firstName = klarnaOrder.billing_address.given_name;
            sfccOrder.billingAddress.postalCode = klarnaOrder.billing_address.postal_code;
            sfccOrder.billingAddress.phone = klarnaOrder.billing_address.phone;
        });
    } catch (e) {
        log.error('Error while copying the billing address from klarna to SFCC order' + e);
    } 
}

/**
 * Call Credit Card Authorization Hook (for VCN settlement)
 * @param {dw.order.order} order DW Order
 * @returns {processorResult} authorization result
 */
function callCreditCardAuthorizationHook(order, context) {
    var processorResult = null;
    var paymentInstrument = order.getPaymentInstruments(PAYMENT_METHOD)[0];
    var paymentProcessor = PaymentMgr
		.getPaymentMethod(paymentInstrument.paymentMethod)
        .paymentProcessor;
    var transactionID = paymentInstrument.getPaymentTransaction().getTransactionID();

    var hook = 'app.payment.processor.' + CREDIT_CARD_PROCESSOR_ID;
    if (!HookMgr.hasHook(hook)) {
        throw new Error('File of app.payment.processor.' + CREDIT_CARD_PROCESSOR_ID + ' hook is missing or the hook is not configured');
    }

    processorResult = HookMgr.callHook('app.payment.processor.' + CREDIT_CARD_PROCESSOR_ID, 'VCNAuthorize', order.orderNo, paymentInstrument, paymentProcessor, context);
    return processorResult;
}

/**
 * Fail DW order
 * @param {dw.order.order} order DW Order
 */
function failOrder(order) {
    Transaction.wrap(function () {
        OrderMgr.failOrder(order);
    });
}

/**
 * Handle the processing of a new Klarna payment transaction
 *
 * @param {dw.order.LineItemCtnr} basket - Current basket
 * @param {boolean} isFromCart - Is checkout started from cart
 * @returns {Object} Processor handling result
 */
function handle(basket) {
    var methodId = PAYMENT_METHOD;

    var amount = basket.totalGrossPrice;

    var paymentInstrument = null;

    Transaction.wrap(function () {
        paymentInstrument = basket.createPaymentInstrument(methodId, amount);
    });

    return {
        success: true,
        paymentInstrument: paymentInstrument
    };
}

/**
 * Attempt to authorize a VCN settlement for a DW order
 * @param {dw.order.order} order DW Order
 */
function attemptAuthorizeVCNSettlement(order) {
    var processorResult = callCreditCardAuthorizationHook(order);

    if (processorResult.error) {
        failOrder(order);
    }
}

/**
 * Handle KP order authorization in case VCN settlement is enabled
 * @param {dw.order.order} order DW order
 * @param {string} kpOrderID KP order ID
 * @param {Object} localeObject locale info
 */
function handleVCNOrder(order, kpOrderID, localeObject) {
    try {
        createVCNSettlement(order, kpOrderID, localeObject);

        attemptAuthorizeVCNSettlement(order);
    } catch (e) {
        cancelOrder(order, localeObject);
        failOrder(order);
    }
}

/**
 * @returns {Object} error auth result
 */
function generateErrorAuthResult() {
    return { error: true };
}

/**
 * @returns {Object} success auth result
 */
function generateSuccessAuthResult() {
    return { authorized: true };
}

/**
 *
 * @param {Object} authResult Authorization result
 * @returns {boolean} true, if the auth result is error
 */
function isErrorAuthResult(authResult) {
    return (!empty(authResult.error) && authResult.error);
}

/**
 *
 * @param {dw.order.order} order DW order
 * @param {dw.order.paymentInstrument} paymentInstrument DW payment instrument
 * @param {string} kpOrderId Klarna Order Id.
 */
function updateOrderWithKlarnaOrderInfo(order, paymentInstrument, kpOrderId) {
    var kpVCNEnabledPreferenceValue = Site.getCurrent().getCustomPreferenceValue('kpVCNEnabled');
    var paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod()).getPaymentProcessor();
    var pInstr = paymentInstrument;
    var dwOrder = order;

    Transaction.wrap(function () {
        pInstr.paymentTransaction.transactionID = kpOrderId;
        pInstr.paymentTransaction.paymentProcessor = paymentProcessor;
        dwOrder.custom.kpOrderID = kpOrderId;
        dwOrder.custom.kpIsVCN = empty(kpVCNEnabledPreferenceValue) ? false : kpVCNEnabledPreferenceValue;
    });
}

/**
 * Authorize order already accepted by Klarna.
 *
 * @param {dw.order.order} order DW Order
 * @param {string} kpOrderID KP Order ID
 * @param {Object} localeObject locale info
 *
 * @return {AuthorizationResult} authorization result
 */
function authorizeAcceptedOrder(order, kpOrderID, localeObject, context) {
    var autoCaptureEnabled = Site.getCurrent().getCustomPreferenceValue('kpAutoCapture');
    var kpVCNEnabledPreferenceValue = Site.getCurrent().getCustomPreferenceValue('kpVCNEnabled');
    var authResult = {};

    if (!kpVCNEnabledPreferenceValue) {
        if (autoCaptureEnabled) {
            try {
                handleAutoCapture(order, kpOrderID, localeObject);

                acknowledgeOrder(kpOrderID, localeObject);
            } catch (e) {
                authResult = generateErrorAuthResult();
            }
        } else {
            acknowledgeOrder(kpOrderID, localeObject);
        }
    } else {
        try {
            createVCNSettlement(order, kpOrderID, localeObject);

            authResult = callCreditCardAuthorizationHook(order, context);
        } catch (e) {
            authResult = generateErrorAuthResult();

            cancelOrder(order, localeObject);
        }
    }

    if (isErrorAuthResult(authResult)) {
        return authResult;
    }

    return generateSuccessAuthResult();
}

/**
 * Handle Klarna Create Order API call response.
 *
 * @param {dw.order.order} order DW Order.
 * @param {dw.order.OrderPaymentInstrument} paymentInstrument DW PaymentInstrument.
 * @param {Object} kpOrderInfo Response data from Klarna Create Order API call.
 * @returns {Object} Authorization result object.
 */
function handleKlarnaOrderCreated(order, paymentInstrument, kpOrderInfo, context) {
    var authorizationResult = {};
    var localeObject = klarnaLocaleMgr.getLocale();
    var kpFraudStatus = kpOrderInfo.fraud_status;
    var kpOrderId = kpOrderInfo.order_id;
    var redirectURL = kpOrderInfo.redirect_url;

    klarnaSessionManager.removeSession();

    Transaction.wrap(function () {
        var pInstr = paymentInstrument;
        pInstr.paymentTransaction.custom.kpFraudStatus = kpFraudStatus;
    });

    updateOrderWithKlarnaOrderInfo(order, paymentInstrument, kpOrderId);

    if (kpFraudStatus === KLARNA_FRAUD_STATUSES.REJECTED) {
        authorizationResult = generateErrorAuthResult();
    } else if (kpFraudStatus === KLARNA_FRAUD_STATUSES.PENDING) {
        authorizationResult = generateSuccessAuthResult();
    } else {
        authorizationResult = authorizeAcceptedOrder(order, kpOrderId, localeObject, context);
    }

    if (!authorizationResult.error) {
        session.privacy.KlarnaPaymentsAuthorizationToken = '';
        session.privacy.KPAuthInfo = null;

        if (redirectURL) {
            session.privacy.KlarnaPaymentsRedirectURL = redirectURL;
        }
    }

    return authorizationResult;
}

/**
 * Update Order Data
 *
 * @param {dw.order.LineItemCtnr} order - Order object
 * @param {string} orderNo - Order Number
 * @param {dw.order.OrderPaymentInstrument} paymentInstrument - current payment instrument
 * @returns {Object} Processor authorizing result
 */
function authorize(order, orderNo, paymentInstrument, context) {
    var localeObject = klarnaLocaleMgr.getLocale();
    var apiResponseData = callKlarnaCreateOrderAPI(order, localeObject);

    if (!apiResponseData) {
        return generateErrorAuthResult();
    }

    return handleKlarnaOrderCreated(order, paymentInstrument, apiResponseData, context);
}

/**
 * Save new fraud status into the the first Klarna Payment Transaction of an order
 *
 * @param {dw.order.order} order Order
 * @param {string} kpFraudStatus Klarna fraud status
 */
function saveFraudStatus(order, kpFraudStatus) {
    var paymentInstrument = order.getPaymentInstruments(PAYMENT_METHOD)[0];

    var paymentTransaction = paymentInstrument.paymentTransaction;

    Transaction.wrap(function () {
        paymentTransaction.custom.kpFraudStatus = kpFraudStatus;
    });
}

/**
 * Handle Klarna notification
 * @param {dw.order.order} order DW Order
 * @param {string} kpOrderID KP Order ID
 * @param {string} kpEventType event type
 */
function notify(order, kpOrderID, kpEventType) {
    var localeObject = klarnaLocaleMgr.getLocale();

    saveFraudStatus(order, kpEventType);

    if (kpEventType === NOTIFY_EVENT_TYPES.FRAUD_RISK_ACCEPTED) {
        if (order.custom.kpIsVCN) {
            handleVCNOrder(order, kpOrderID, localeObject);
        }

        placeOrder(order, kpOrderID, localeObject);
    } else {
        failOrder(order);
    }
}

/**
 * Call Klarna Payments API to get an order
 * @param {string} klarnaPaymentsOrderID Klarna Payments Order ID
 * @param {dw.object.CustomObject} localeObject corresponding to the locale Custom Object from KlarnaCountries
 *
 * @return {Object} Klarna order
 */
function getKlarnaOrder(klarnaPaymentsOrderID) {
    var klarnaHttpService = {};
    var klarnaApiContext = {};
    var klarnaOrderID = klarnaPaymentsOrderID;
    var localeObject = klarnaLocaleMgr.getLocale();
    var requestUrl = '';

    try {
        klarnaHttpService = new KlarnaPaymentsHttpService();
        klarnaApiContext = new KlarnaPaymentsApiContext();
        requestUrl = StringUtils.format(klarnaApiContext.getFlowApiUrls().get('getOrder'), klarnaOrderID);

        return klarnaHttpService.call(requestUrl, 'GET', localeObject.custom.credentialID);
    } catch (e) {
        log.error('Error while retrieving order: {0}', e);
    }

    return null;
}

/**
 * Deletes the previous authorization
 * @param {string} authToken Authorization Token
 * @return {string} Service call result
 */
function cancelAuthorization(authToken) {
    var klarnaAuthorizationToken = authToken || session.privacy.KlarnaPaymentsAuthorizationToken;

    if (klarnaAuthorizationToken) {
        var klarnaPaymentsHttpService = new KlarnaPaymentsHttpService();
        var klarnaApiContext = new KlarnaPaymentsApiContext();
        var requestUrl = StringUtils.format(klarnaApiContext.getFlowApiUrls().get('cancelAuthorization'), klarnaAuthorizationToken);
        var localeObject = klarnaLocaleMgr.getLocale();

        try {
            var response = klarnaPaymentsHttpService.call(requestUrl, 'DELETE', localeObject.custom.credentialID);
            session.privacy.KlarnaPaymentsAuthorizationToken = '';
            session.privacy.KPAuthInfo = null;
            return response;
        } catch (e) {
            log.error('Error in canceling Klarna Payments Authorization: {0}', e.message + e.stack);
        }
    }

    return null;
}

module.exports.handle = handle;
module.exports.authorize = authorize;
module.exports.notify = notify;
module.exports.cancelAuthorization = cancelAuthorization;
module.exports.getKlarnaOrder = getKlarnaOrder;
