var page = module.superModule;
var server = require('server');
var collections = require('*/cartridge/scripts/util/collections');
var KlarnaUtils = require('*/cartridge/scripts/klarna/klarnaUtils');

server.extend(page);

function getApplicablePaymentMethods(req) {
    var Locale = require('dw/util/Locale');
    var BasketMgr = require('dw/order/BasketMgr');
    var PaymentMgr = require('dw/order/PaymentMgr');
    var country = Locale.getLocale(req.locale.id).getCountry();
    var gross = BasketMgr.getCurrentBasket().totalGrossPrice.value;
    var applicablePaymentMethods = PaymentMgr.getApplicablePaymentMethods(
        BasketMgr.getCurrentBasket().getCustomer(),
        country,
        gross
    );
    var methodObjects = [];
    collections.forEach(applicablePaymentMethods, function (method) {
        if (method.ID === 'KLARNA_PAYMENTS' && !KlarnaUtils.isKlarnaApplicable(req)) {
            return; // Don't include Klarna if it's not applicable. (Utils has special cases for gift cards, etc.)
        }
        var methodObj = {
            ID: method.ID,
            name: method.name,
            gross: gross
        };
        methodObjects.push(methodObj);
    });
    return methodObjects;
}

function updateKlarnaSession(req, res, next) {
    var KlarnaSessionManager = require('*/cartridge/scripts/common/klarnaSessionManager');
    var KlarnaLocale = require('*/cartridge/scripts/klarna_payments/locale');
    var userSession = req.session.raw;
    var klarnaSessionManager = new KlarnaSessionManager(userSession, new KlarnaLocale());
    klarnaSessionManager.createOrUpdateSession();
    next();
}

function attachApplicablePM(req, res, next) {
    res.json({
        applicablePaymentMethods: getApplicablePaymentMethods(req)
    });

    return next();
}

server.append('AddCoupon', updateKlarnaSession, attachApplicablePM);
server.append('RemoveCouponLineItem', updateKlarnaSession, attachApplicablePM);
server.append('RemoveProductLineItem', updateKlarnaSession, attachApplicablePM);
server.append('UpdateQuantity', updateKlarnaSession, attachApplicablePM);

module.exports = server.exports();
