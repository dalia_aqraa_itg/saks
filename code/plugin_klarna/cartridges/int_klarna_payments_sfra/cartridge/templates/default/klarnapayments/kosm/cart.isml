<isset name="sitePreference" value="${dw.system.Site.current.preferences.custom}" scope="page" />
<isset name="globalCartFEToggle" value="${'CartFEPerformanceChangesEnable' in sitePreference && sitePreference.CartFEPerformanceChangesEnable === true}" scope="page" />
<isset name="globalDeferCSS" value="${'GlobalDeferNonCriticalCSS' in sitePreference && sitePreference.GlobalDeferNonCriticalCSS === true}" scope="page" />
<isscript>
  var assets = require('*/cartridge/scripts/assets.js');
  if (!globalCartFEToggle || !globalDeferCSS) {
    assets.addCss('css/klarnaPayments.css');
  }

  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();
  var totalGrossPrice = currentBasket.getTotalGrossPrice();

  var KlarnaOSM = require('~/cartridge/scripts/klarna_payments/osm');
  var KlarnaUtils = require('*/cartridge/scripts/klarna/klarnaUtils');
  var klarnaEnabled = KlarnaOSM.isEnabledCartPage(); // Should Klarna EVER show?
  var hasGiftCard = KlarnaUtils.basketContainsGiftCardLineItem(currentBasket);
</isscript>
<isif condition="${globalCartFEToggle && globalDeferCSS}">
  <link rel="stylesheet" href="${URLUtils.staticURL('/css/klarnaPayments.css')}" media="print" onload="this.media='all'"/>
  <noscript><link rel="stylesheet" href="${URLUtils.staticURL('/css/klarnaPayments.css')}" /></noscript>
</isif>

<iscomment>
    Unless Klarna is disabled on the cart page, we ALWAYS want it in the DOM, so that it can be hidden/shown as line
    items are removed, or promo codes are added or removed.

    - Removing line items may bring the cart total within Klarna's valid range.
    - Removing a gift card line item may let Klarna show.
    - Adding or removing a promo code may change the cart total, which will affect whether Klarna is applicable.
    - Simply leaving this element empty does NOT fully remove all layout space taken up by this element
    - Using "display: none" to hide this element keeps this markup in sync with the JS that hides/shows this element
</iscomment>
<isif condition="${klarnaEnabled}">
    <div class="kosm-cart ${pdict.basketHasGiftCard || hasGiftCard ? 'd-none' : ''}">
        <klarna-placement data-key="${KlarnaOSM.getCartPagePlacementTagId()}" data-locale="${KlarnaOSM.getLocale()}" data-purchase-amount="${KlarnaOSM.formatPurchaseAmount(totalGrossPrice)}"></klarna-placement>
        <isset name="klarnaOSMJsAdded" value="${true}" scope="page" />
        <isinclude sf-toolkit="off" template="/klarnapayments/scripts" />
    </div>
</isif>
