<!--- TEMPLATENAME: productDetails_giftcard.isml --->
<isdecorate template="common/layout/page">
    <isinclude template="components/modules" />
	<isscript>
        var assets = require('*/cartridge/scripts/assets');
        assets.addJs('/js/productDetail.js');
        assets.addCss('/css/product/detail.css');
    </isscript>
    <iscomment>Inclusion scene7 script API for loading PDP zoomer and video  </iscomment>
	<isinclude template="product/components/s7ScriptsInclude" />
    <isset name="product" value="${pdict.product}" scope="page" />
    <isset name="isQuickView" value="${false}" scope="page" />
    <isset name="isProductSet" value="${pdict.product.productType === 'set'}" scope="page" />
    <isset name="attrIDValues" value="" scope="page" />    
    <isobject object="${product.raw}" view="detail" />
        <div class="container product-detail product-wrapper giftcard-pdp bfx-disable-product"
            data-pid="${product.id}" data-masterid="${product.masterProductID}">
            <span class="d-none bf-product-id bfx-sku">${product.id}</span>
            <!---Breadcrumbs--->
            <div class="row">
                <div class="product-breadcrumb col hidden-sm-down">
                    <isinclude template="components/breadcrumbs/pdpPageBreadcrumbs"/>
                </div>
            </div>
            
            <div class="row">
                <div class="col-12 breadcrumb-col">
                </div>
            </div>

            <div class="row bfx-disable-element-container">
                <!-- Product Images Carousel -->                
                <isinclude template="product/components/imageCarouselGCChanel" />
				
                <div class="col-12 col-md-5 col-sm-6 pdp-right-section">
	                <div class="product-secondary-section pdp-standard">
						<!-- Product Brand -->
						<isif condition="${!empty(product.brand) && !empty(product.brand.name)}"> 
	                    	<div class="row">
	                        	<div class="col">
	                        		<h1 class="product-brand-name adobelaunch__brand" data-adobelaunchproductid="${product.id}">${product.brand.name}</h1>
	                        	</div>
	                    	</div>    		
	                     </isif>                    
	                    
	                    <!-- Product Name -->
	                    <div class="row">
	                        <div class="col">
	                            <h1 class="product-name h2"><isprint value="${product.productName}" encoding="off" /></h1>
	                        </div>
	                    </div>
		
						<div class="row">
	                    	<div class="col-12">
		                        <!-- Prices -->
		                        <div class="prices">
		                        	<!-- Should not affect during variation -->
	                        		<isset name="price" value="${product.price}" scope="page" />
	                        		<div class="price">
	                        			<isinclude template="product/components/pricing/main" />
	                        		</div>
		                        </div>
	                        </div>
	                  	</div>
	                    <!-- Applicable Promotions -->
	                    <div class="row">
		                   <div class="col-12 promotions promotion-txt ${(product.promotionalPricing != null && product.promotionalPricing.isPromotionalPrice && product.promotionalPricing.promoMessage !== '') ? 'd-none' :''}">
		                   		<isinclude template="product/components/promotions" />
		                   </div>
	                    </div>
	                    <iscomment>Display the price info ('Promo Message') if the price is fetched from the promotion pricebook</iscomment>
	                    <isif condition="${pdict.product.promotionalPricing && pdict.product.promotionalPricing.isPromotionalPrice && pdict.product.promotionalPricing.promoMessage !== ''}">
		                    <div class="row">
			                   <div class="col-12 promotion-pricing promotion-txt">
			                   		<isinclude template="product/components/pricing/promotionPricing" />
			                   </div>
		                    </div>	    
	                    </isif>  
	                    <div class="attributes">
	                    	<span class="svg-svg-69-share-dims svg-svg-69-share social-share d-lg-none" data-social-share="${Resource.msg('wishlist.share.text', 'wishlist', null)}"></span>
	
	                        <isset name="isBundle" value="${false}" scope="page" />
	                        <isset name="loopState" value="{count: 1}" scope="page" />
	                        <isloop items="${product.variationAttributes}" var="attr" status="attributeStatus">
	                            <isset name="attrIDValues" value="${attrIDValues+attr.id+' ,'}" scope="page" />
	                            <div class="row" data-attr="${attr.id}">
	                                <div class="col-12">
	                                    <isinclude template="product/components/variationAttribute_GC" />
	                                </div>
	
	                                <isif condition="${attributeStatus.last && !isBundle && product.options.length === 0}">
	                                    <!-- Quantity Drop Down Menu -->
	                                    <div class="attribute quantity col-12">
	                                        <isinclude template="product/components/quantity" />
	                                        
	                                        <!-- Product Availability -->
	                        				<isinclude template="product/components/productAvailability" />
	                                    </div>
	                                </isif>
	                            </div>
	                        </isloop>
	
	                        <isif condition="${!product.variationAttributes && product.options.length === 0}">
	                            <div class="row">
	                                <div class="col-12 attribute quantity simple-quantity">
	                                    <isinclude template="product/components/quantity" />
	                                </div>
	                            </div>
	                        </isif>
	
	
	                        <isif condition="${product.options && product.options.length > 0}">
								<isinclude template="product/components/options" />
	                        </isif>
	                        <div class="sr-only" id="missedattrSelection-ADA"></div>
							<div class="row select-size-color"></div>						
							
	                        <div class="prices-add-to-cart-actions">
	                            <!-- Cart and [Optionally] Apple Pay -->
	                            <isinclude template="product/components/addToCartProduct" />
	                        </div>

						    <!-- Hudson Bay Rewards -->
							<isinclude template="product/components/bayrewardPDP" />
	
							<isif condition="${product.isNotReturnable.value}"> 
								<isset name="badgeisReturnable" value="${product.isNotReturnable.color ? 'style=color:'+product.isNotReturnable.color : ""}" scope="page" />
								<span ${badgeisReturnable} class="final-sale-message">
									${Resource.msg('labe.notreturnable', 'product', null)}
								</span>
							</isif>
							
	                        <!-- Social Sharing Icons -->
	                        <div class="row d-lg-none">
	                            <isinclude template="product/components/socialIcons" />
	                        </div>
	            	</div>
		            <isset name="loopState" value="{count: 1}" scope="page" />
					<div class="accordian-main-div">
			           	<div class="collapsible-xl adobelaunch__productaccordion" data-toggle="content" data-adobelaunchaccordionproductid="${product.id}" data-adobelaunchaccordiontabname="details">
			           		<button class="title btn text-left btn-block d-flex align-items-center justify-content-between" aria-expanded="false" aria-controls="collapsible-details-${loopState && loopState.count ? loopState.count : '1'}">
			                	${Resource.msg('label.details', 'product', null)}
			                	<span class="svg-svg-59-plus-thin-dims svg-svg-59-plus-thin plus pull-right"></span>
			                	<span class="svg-svg-49-minus-thin-dims svg-svg-49-minus-thin minus pull-right"></span>
			            	</button>
			            	<isinclude template="product/components/details" />
			            </div>            
		            </div>
	             </div>
            </div>
            <div class="col-12 recommendations recom-pdts">
                <isslot id="pdp-recommendations-2" description="Recently Viewed" context="global" />
            </div>
        </div>

    </isobject>
</isdecorate>
