/* eslint-disable */
var base = module.superModule;

let StringUtils = require('dw/util/StringUtils');
let SecureRandom = require('dw/crypto/SecureRandom');
let Resource = require('dw/web/Resource');
let Transaction = require('dw/system/Transaction');

base.Helper = function () {};

base.Helper.prototype = {
  getFormattedDate: function () {
    let date = new Date();
    return StringUtils.format(
      '{0}-{1}-{2}',
      date.getFullYear().toString(),
      this.insertLeadingZero(date.getMonth() + 1),
      this.insertLeadingZero(date.getDate())
    );
  },

  insertLeadingZero: function (number) {
    return number < 10 ? '0' + number : number;
  },

  beautifyAddresses: function (form, addresses) {
    let random = new SecureRandom();
    if (!addresses.length || ('postalAddress' in addresses[0] && empty(addresses[0].postalAddress))) {
      return [];
    }
    let items = [];
    for (let i in addresses) {
      var address = addresses[i];

      try {
        if (address.postalAddress) {
          address = address.postalAddress[0];
        }
      } catch (e) {
        // do nothing
        // address@TaxLookupArea throw an exception in case undefined method instead undefined value
      }

      let newAddress = {
        UUID: random.nextInt(),
        ID: address.city,
        key: address.postalCode + address.mainDivision + address.streetAddress1 + address.streetAddress2 + address.city + address.country,
        countryCode: address.country.toLowerCase().substring(0, address.country.length - 1),
        postalCode: address.postalCode,
        stateCode: address.mainDivision,
        address1: address.streetAddress1 == null ? form.address1.value : address.streetAddress1,
        address2: address.streetAddress2,
        displayValue: address.city,
        city: address.city
      };

      items.push(newAddress);
    }
    return items;
  },

  getCurrentNormalizedAddress: function () {
    let form = null;
    let postal = null;
    let state = null;
    if (request.httpParameterMap.multishipping && request.httpParameterMap.multishipping.value) {
      if (session.forms.multishipping) {
        form = session.forms.multishipping.editAddress.addressFields;
        postal = form.postal.value;
        state = form.states.state.value;
      }
    } else {
      if (session.forms.singleshipping) {
        form = session.forms.singleshipping.shippingAddress.addressFields;
        postal = form.postal.value;
        state = form.states.state.value;
      } else if (session.forms.shipping.shippingAddress) {
        form = session.forms.shipping.shippingAddress.addressFields;
        postal = form.postalCode.value;
        state = form.states.stateCode.value;
      }
    }

    return {
      UUID: form.UUID,
      ID: form.city.value,
      key: Resource.msg('form.label.asis', 'vertex', null),
      address1: form.address1.value,
      address2: form.address2.value,
      city: form.city.value,
      postalCode: postal,
      stateCode: state,
      countryCode: form.country.value,
      displayValue: form.city.value
    };
  },

  /**
   * @description check if selected address fields and address fields in the form are identical.
   * address2 field isn't required, so we skip this field.
   */
  isEqualAddresses: function (normalizedAddresses) {
    let restrictedFields = ['ID', 'key', 'UUID', 'displayValue', 'address2'],
      normalizedForm = this.getCurrentNormalizedAddress();

    if (session.custom.VertexAddressSuggestions) {
      let previousForm = JSON.parse(session.custom.VertexAddressSuggestions)[0];
      let currentForm = normalizedForm;
      let formsIsEqual = true;
      let formKeys = Object.keys(previousForm);

      for (let i in Object.keys(previousForm)) {
        let formFieldValue = previousForm[formKeys[i]];
        if (restrictedFields.indexOf(formKeys[i]) == -1) {
          if (formFieldValue.toUpperCase() != currentForm[formKeys[i]].toUpperCase()) {
            formsIsEqual = false;
          }
        }
      }

      if (formsIsEqual) {
        normalizedAddresses.push(normalizedForm);
      }
    }

    for (let i in normalizedAddresses) {
      let address = normalizedAddresses[i];
      let formIsEqual = true;
      let formKeys = Object.keys(address);

      for (let k in formKeys) {
        let fieldKey = formKeys[k];
        if (restrictedFields.indexOf(fieldKey) == -1) {
          let fieldValue = address[fieldKey];

          if (normalizedForm[fieldKey].toUpperCase() != fieldValue.toUpperCase()) {
            formIsEqual = false;
          }
        }
      }

      if (formIsEqual) {
        return true;
      }
    }

    return false;
  },

  /**
   * @description get category name
   * @param product ProductLineItem
   * @param categoryDepth Integer 0: root, 1: product category
   * @example 'fruits-bananas-yellow_bananas'
   */
  getProductClass: function (productWrap) {
    var product;

    if (!empty(productWrap) && productWrap.product) {
      if (productWrap.product.variant) {
        product = productWrap.product.masterProduct;
      } else {
        product = productWrap.product;
      }

      // if product have no Classification category then get Primary category
      if (!product.classificationCategory) {
        return product.primaryCategory.ID;
      }

      return product.classificationCategory.ID;
    }
  },

  prepareCart: function (cart) {
    var GCs = cart.getGiftCertificateLineItems();
    var Products = cart.getAllProductLineItems();

    if (GCs.length) {
      var GCsi = GCs.iterator();
      Transaction.wrap(function () {
        while (GCsi.hasNext()) {
          var GC = GCsi.next();
          GC.updateTax(0.0);
        }
      });
    }

    // if we have only GC in the cart we should set zero tax to the default shipment
    if (!Products.length) {
      var lineItems = cart.getAllLineItems().iterator();

      while (lineItems.hasNext()) {
        let item = lineItems.next();
        let itemClassName = item.constructor.name;

        if (itemClassName == 'dw.order.ShippingLineItem') {
          Transaction.wrap(function () {
            item.updateTax(0.0);
          });
        }
      }
    }

    // Update Tax Zero on the first cart view when we Basket won't have Shipping and Billing Address
    if (Products.length > 0) {
      var lineItems = cart.getAllLineItems().iterator();
      while (lineItems.hasNext()) {
        let item = lineItems.next();
        Transaction.wrap(function () {
          item.updateTax(0.0);
        });
      }
    }
  },

  createWaitlistDetailsObject: function (email, productID, mobile) {
    var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Transaction = require('dw/system/Transaction');
    var Resource = require('dw/web/Resource');

    var StringUtils = require('dw/util/StringUtils');
    var Calendar = require('dw/util/Calendar');

    // Query Custom Object and update it if Email and Mobile is same.
    var query = 'custom.emailId = {0} AND custom.productId = {1}';
    var waitListObj = CustomObjectMgr.queryCustomObject('WaitlistDetails', query, email, productID);
    if (waitListObj) {
      waitListObj.custom.phoneNumber = !empty(mobile) ? mobile : '';
      waitListObj.custom.date = StringUtils.formatCalendar(new Calendar(), 'MM/dd/yyyy HH:mm:ss');
    } else {
      waitListObj = CustomObjectMgr.createCustomObject('WaitlistDetails', productHelper.generateUUID());

      if (waitListObj) {
        waitListObj.custom.productId = productID;
        waitListObj.custom.emailId = email;
        waitListObj.custom.phoneNumber = !empty(mobile) ? mobile : '';
        waitListObj.custom.date = StringUtils.formatCalendar(new Calendar(), 'MM/dd/yyyy HH:mm:ss');
        waitListObj.custom.exported = false;
      }
    }
  },

  clearBonusProduct: function (cart) {
    var isProdDeleted = 'isBonusProRemoved' in session.custom && session.custom.isBonusProRemoved;
    if (isProdDeleted && cart && cart.productLineItems && cart.productLineItems.length > 0) {
      var collections = require('*/cartridge/scripts/util/collections');
      collections.forEach(cart.productLineItems, function (item) {
        if (item.bonusProductLineItem && (!item.bonusDiscountLineItem || !item.bonusDiscountLineItem.bonusProductLineItems.length)) {
          if (
            (item.getQualifyingProductLineItemForBonusProduct() &&
              item.getQualifyingProductLineItemForBonusProduct().custom.dBonus.indexOf(item.productID) != -1) ||
            cart.custom.dBonus.indexOf(item.productID) != -1
          ) {
            Transaction.wrap(function () {
              cart.removeProductLineItem(item);
            });
          }
        }
      });
    }
  },
  getJsonFromMap: function (map) {
    var jsonDataArray = [];
    var keySet = map.keySet();
    for (var i = 0; i < keySet.length; i++) {
      var key = keySet[i];
      var value = map.get(key);
      var jsonObj = {};
      jsonObj[key] = value.value;
      jsonDataArray.push(jsonObj);
    }
    return jsonDataArray;
  }
};

module.exports = new base.Helper();
