'use strict';

var Logger = require('dw/system/Logger');
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');
var WeakMessageDigest = require('dw/crypto/WeakMessageDigest');
var MessageDigest = require('dw/crypto/MessageDigest');
var Bytes = require('dw/util/Bytes');
var Encoding = require('dw/crypto/Encoding');
var ArrayList = require('dw/util/ArrayList');

/**
 * @function CartStateHash
 * Check if we need to skip tax calculation
 * @param {dw.order.Basket} Basket The basket containing the elements for which taxes need to be updated
 * @param {Boolean} ForceRecalculation Boolean used to force tax recalculation (e.g.: place order)
 * @returns {Boolean} Boolean flag indicating if real time tax call can be skipped
 */
function CartStateHash(basket) {
  try {
    var cartStateHash = '';
    var skipTax = false;

    if (basket == null) {
      return skipTax;
    }

    // process gift cards
    var gifts = basket.getGiftCertificateLineItems().iterator();
    while (gifts.hasNext()) {
      var giftLineItem = gifts.next();
      cartStateHash += giftLineItem.giftCertificateID + ';' + giftLineItem.priceValue + '|';
    }

    /**
     * Append shipping totals and basket totals to string (adjustedMerchandizeTotalPrice includes order level price adjustments).
     * Basket Net total checked as catch all for both taxation policies not including taxes.
     */
    cartStateHash +=
      basket.adjustedShippingTotalPrice.valueOrNull + '|' + basket.adjustedMerchandizeTotalPrice.valueOrNull + '|' + basket.totalNetPrice.valueOrNull;

    // append multi-shipments with products
    var shipments = basket.getShipments();
    for (var i = 0; i < shipments.length; i++) {
      var shipment = shipments[i];
      cartStateHash += '|' + (!empty(shipment.getShippingMethodID()) ? shipment.getShippingMethodID() : '');
      var shipAddress = !empty(shipment.getShippingAddress()) ? shipment.getShippingAddress() : null;
      if (!empty(shipAddress)) {
        cartStateHash += '|' + (!empty(shipAddress.stateCode) ? shipAddress.stateCode : '');
        cartStateHash += '|' + (!empty(shipAddress.city) ? shipAddress.city : '');
        cartStateHash += '|' + (!empty(shipAddress.countryCode) ? shipAddress.countryCode.displayValue : '');
        cartStateHash += '|' + (!empty(shipAddress.postalCode) ? shipAddress.postalCode : '') + '|';
      }
      // process line items
      var shipmentLineItems = shipment.getAllLineItems().iterator();
      while (shipmentLineItems.hasNext()) {
        var lineItem = shipmentLineItems.next();
        if (lineItem instanceof dw.order.ProductLineItem) {
          cartStateHash += lineItem.productID + ';' + lineItem.quantityValue + ';' + lineItem.adjustedPrice + '|';
        } else if (lineItem instanceof dw.order.ShippingLineItem) {
          cartStateHash += lineItem.getID() + ';1;' + lineItem.adjustedPrice + '|';
        }
      }
    }

    var giftOptionsPA = basket.getPriceAdjustmentByPromotionID('GiftOptions');
    if (giftOptionsPA) {
      cartStateHash += giftOptionsPA.promotionID + ';1;' + giftOptionsPA.grossPrice + '|';
    }

    // coupon codes
    var couponLineItems = basket.getCouponLineItems();
    if (couponLineItems != null && couponLineItems.length > 0) {
      for (var couponIndex = 0; couponIndex < couponLineItems.length; couponIndex += 1) {
        cartStateHash += '|' + couponLineItems[couponIndex].getCouponCode();
      }
    }

    // adds current date and hour into the cart tax string
    var nowCalendar = new Calendar();
    cartStateHash += '|' + StringUtils.formatCalendar(nowCalendar, request.locale, Calendar.INPUT_DATE_PATTERN) + ';' + nowCalendar.get(Calendar.HOUR_OF_DAY);

    // due to SFCC quota limit on string length which can be stored in session we should used MD5 hash instead raw string
    var hash = new MessageDigest(MessageDigest.DIGEST_SHA_256).digestBytes(new Bytes(cartStateHash.replace(/\s/g, ''), 'UTF-8'));
    cartStateHash = Encoding.toBase64(hash);

    // make sure we're not going to violate the api.session.maxStringLength - warnings start at 1,200
    var maxLength = 1999;
    if (cartStateHash.length > maxLength) {
      cartStateHash = cartStateHash.substr(0, 1998);
    }

    // previously saved cart state is equal current basket state - so we would skip tax call

    if (!empty(session.privacy.cartStateHash) && !empty(cartStateHash)) {
      if (cartStateHash == session.privacy.cartStateHash) {
        skipTax = true;
      }
    }

    session.privacy.skipRealTimeTaxes = skipTax;
    session.privacy.cartStateHash = cartStateHash;
  } catch (ex) {
    Logger.error('TaxUtils.js: ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
  }

  return skipTax;
}

/**
 * @function updateTaxLineItems
 * Updates tax line items in the case that the real time tax call was skipped
 * @param {dw.order.Basket} basket The basket containing the elements for which taxes need to be updated
 */
function updateTaxLineItems(basket) {
  if (basket == null) {
    return;
  }

  try {
    var shipments = basket.getShipments().iterator();
    while (shipments.hasNext()) {
      var shipment = shipments.next();

      var shipmentLineItems = shipment.getAllLineItems().iterator();
      while (shipmentLineItems.hasNext()) {
        var lineItem = shipmentLineItems.next();

        if (lineItem instanceof dw.order.ProductLineItem) {
          if (lineItem.bonusProductLineItem) {
            // tax is not getting calculated for bonus product which is updating bonus line item's tax as /NA. it has the direct impact on basket totals
            // Resolution - update line item tax with 0 which will resolve the tax calculation N/A for bonus line items.
            lineItem.updateTax(0);
          } else {
            updateLineItemTax(lineItem);
          }
        } else if (lineItem instanceof dw.order.ShippingLineItem) {
          updateLineItemTax(lineItem);
        }

        if (lineItem instanceof dw.order.ProductLineItem || lineItem instanceof dw.order.ProductShippingLineItem) {
          for (let i = 0; i < lineItem.priceAdjustments.length; i++) {
            let pa = lineItem.priceAdjustments[i];
            pa.updateTax(0);
          }
        } else if (lineItem instanceof dw.order.ShippingLineItem) {
          for (let i = 0; i < lineItem.shippingPriceAdjustments.length; i++) {
            let pa = lineItem.shippingPriceAdjustments[i];
            pa.updateTax(0);
          }
        }
      }
    }

    // make sure all price adjustments are set to zero tax
    var basketPriceAdjustments = new ArrayList(basket.priceAdjustments);
    basketPriceAdjustments.addAll(basket.shippingPriceAdjustments);
    for (let i = 0; i < basketPriceAdjustments.length; i++) {
      let basketPriceAdjustment = basketPriceAdjustments[i];
      basketPriceAdjustment.updateTax(0);
    }
  } catch (ex) {
    Logger.error(ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
  }

  return;
}

/**
 * @function updateLineItemTax
 * Updates tax amount for the line items passed with its tax amount.
 * @param {dw.order.LineItem} lineItem The LineItem for which taxes need to be updated
 */
function updateLineItemTax(lineItem) {
  if (!empty(lineItem.getTax()) && lineItem.getTax().available) {
    lineItem.updateTaxAmount(lineItem.getTax());
  }
}

module.exports = {
  CartStateHash: CartStateHash,
  updateTaxLineItems: updateTaxLineItems
};
