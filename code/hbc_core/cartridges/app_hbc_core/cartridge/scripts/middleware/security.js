'use strict';

var OMSConstants = require('*/cartridge/scripts/config/OMSServiceConfig');

/**
 * Middleware validating Customer on order
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function validateCustomerOnOrder(req, res, next) {
  var orderNo = req.querystring.orderNumber;
  var cust = req.currentCustomer.raw;

  if (!cust || !cust.profile) {
    res.json({
      error: true
    });
    this.emit('route:Complete', req, res);
    return;
  }
  if (!orderNo) {
    res.json({
      error: true
    });
    this.emit('route:Complete', req, res);
    return;
  }
  var OrderMgr = require('dw/order/OrderMgr');
  var order = OrderMgr.getOrder(orderNo);

  if (cust && order && cust.profile) {
    var customerNo = cust.profile.customerNo;
    var customerNoOnOrder = order.getCustomerNo();

    if (customerNo != customerNoOnOrder) {
      res.json({
        error: true
      });
      this.emit('route:Complete', req, res);
      return;
    }
  }
  next();
}

/**
 * Middleware validating Customer on order
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function validateLoggedInCustomerOnOrder(req, res, next) {
  var orderNo = req.querystring.trackOrderNumber;
  if (!orderNo) {
    orderNo = req.querystring.orderNumber;
  }
  if (OMSConstants.OMS_RemovedZeroPrefix) {
    orderNo = ('0' + orderNo).slice(-9);
  }
  var cust = req.currentCustomer.raw;
  var detailType = req.querystring.detailType;
  if (detailType && detailType == 'STATUS') {
    return next();
  }

  // skip guest user flow and validate customer only when authenticated
  // reason to skip guest user flow- as we are already blocking in displaying the pii info on order status

  if ((!cust && !cust.profile && !cust.profile.customerNo) || (cust && !cust.isAuthenticated())) {
    if (req.querystring.ajax == true) {
      res.json({
        error: true
      });
      this.emit('route:Complete', req, res);
      return;
    } else {
      var URLUtils = require('dw/web/URLUtils');
      res.redirect(URLUtils.url('Login-Show'));
      return next();
    }
  }
  if (!orderNo) {
    res.json({
      error: true
    });
    this.emit('route:Complete', req, res);
    return;
  }

  var OrderMgr = require('dw/order/OrderMgr');
  var order = OrderMgr.getOrder(orderNo);

  //Changes for SFSX-2572 - https://hbcdigital.atlassian.net/browse/SFSX-2572 - start
  //During AB Testing period it is possible that valid existing orders are only available in BlueMartini and are not present in SFCC
  //In this use case Orders will be missing in SFCC and validateLoggedInCustomerOnOrder will fail for that reason.
  //To accommodate this scenario, we are pulling orders from OMS to confirm it exists, this is an expensive call and will
  //only happen when orders are not present in SFCC.
  var orderDetails = '';
  if (!order) {
    var OrderDetailsHelper = require('*/cartridge/scripts/helpers/OrderDetailsHelper');
    try {
      var orderDetailsRequest = {};
      orderDetailsRequest.BillToID = cust.profile.customerNo;
      orderDetailsRequest.OrderNumber = orderNo;
      orderDetailsRequest.InvokedFrom = OMSConstants.OMS_ORDER_DETAILS_INVOKEDFROM;
      orderDetailsRequest.BusinessUnit = OMSConstants.BANNER_CODE;
      orderDetails = OrderDetailsHelper.getOrderDetails(orderDetailsRequest);
      if (empty(orderDetails)) {
        res.json({
          error: true
        });
        this.emit('route:Complete', req, res);
        return;
      }
    } catch (ex) {
      //Let the request pass through, this is being done so timeout issues don't lead to security failures..
    }
  } else if (cust && order && cust.profile) {
    //Changes for SFSX-2572 - https://hbcdigital.atlassian.net/browse/SFSX-2572 - end
    var customerNo = cust.profile.customerNo;
    var customerNoOnOrder = order.getCustomerNo();

    if (customerNo != customerNoOnOrder) {
      res.json({
        error: true
      });
      this.emit('route:Complete', req, res);
      return;
    }
  }
  next();
}

/**
 * Added for SFSX-1552
 *
 * Middleware validating Guest Customer on order
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function validateGuestCustomerOnOrder(req, res, next) {
  var orderNo = req.querystring.ID;
  var guestEmail = req.querystring.orderEmail;

  if (!guestEmail) {
    res.json({
      error: true
    });
    this.emit('route:Complete', req, res);
    return;
  }
  if (!orderNo) {
    res.json({
      error: true
    });
    this.emit('route:Complete', req, res);
    return;
  }
  var OrderMgr = require('dw/order/OrderMgr');
  var order = OrderMgr.getOrder(orderNo);

  if (order) {
    var customerEmail = order.customerEmail;

    if (customerEmail != guestEmail) {
      res.json({
        error: true
      });
      this.emit('route:Complete', req, res);
      return;
    }
  }
  next();
}

/**
 * Added for SFSX-1424
 *
 * Middleware validating Guest Customer on order
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function validateRemorseOnOrder(req, res, next) {
  var orderNo = req.querystring.orderNumber;
  var cust = req.currentCustomer.raw;

  if (!orderNo) {
    res.json({
      error: true
    });
    this.emit('route:Complete', req, res);
    return;
  }

  if (!cust) {
    res.json({
      error: true
    });
    this.emit('route:Complete', req, res);
    return;
  }

  var OrderMgr = require('dw/order/OrderMgr');
  var order = OrderMgr.getOrder(orderNo);

  if (!order) {
    res.json({
      error: true
    });
    this.emit('route:Complete', req, res);
    return;
  }

  if (order) {
    //Guest Customer --
    if (!cust.profile || !cust.profile.customerNo) {
      var orderCustomerId = order.customer.ID;
      var custCustomerId = cust.ID;

      if (orderCustomerId != custCustomerId) {
        res.json({
          error: true
        });
        this.emit('route:Complete', req, res);
        return;
      }
    } else {
      //Logged in customer
      var customerNo = cust.profile.customerNo;
      var customerNoOnOrder = order.getCustomerNo();

      if (customerNo != customerNoOnOrder) {
        res.json({
          error: true
        });
        this.emit('route:Complete', req, res);
        return;
      }
    }
  }
  next();
}

//Change for SFSX-1552 - Updated to include the new validateGuestCustomerOnOrder method
//Added validateRemorseOnOrder for SFSX-1424
module.exports = {
  validateCustomerOnOrder: validateCustomerOnOrder,
  validateLoggedInCustomerOnOrder: validateLoggedInCustomerOnOrder,
  validateGuestCustomerOnOrder: validateGuestCustomerOnOrder,
  validateRemorseOnOrder: validateRemorseOnOrder
};
