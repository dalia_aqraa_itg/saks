'use strict';
var base = module.superModule;

/**
 * Applies the two hours price promotion page cache.
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
base.applyTwoHoursPromotionSensitiveCache = function applyTwoHoursPromotionSensitiveCache(req, res, next) {
  res.cachePeriod = 2; // eslint-disable-line no-param-reassign
  res.cachePeriodUnit = 'hours'; // eslint-disable-line no-param-reassign
  res.personalized = true; // eslint-disable-line no-param-reassign
  next();
};

/**
 * Applies the four hours price promotion page cache.
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
base.applyFourHoursPromotionSensitiveCache = function applyFourHoursPromotionSensitiveCache(req, res, next) {
  res.cachePeriod = 4; // eslint-disable-line no-param-reassign
  res.cachePeriodUnit = 'hours'; // eslint-disable-line no-param-reassign
  res.personalized = true; // eslint-disable-line no-param-reassign
  next();
};

/**
 * Applies the eight hours price promotion page cache.
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
base.applyEightHoursPromotionSensitiveCache = function applyEightHoursPromotionSensitiveCache(req, res, next) {
  res.cachePeriod = 8; // eslint-disable-line no-param-reassign
  res.cachePeriodUnit = 'hours'; // eslint-disable-line no-param-reassign
  res.personalized = true; // eslint-disable-line no-param-reassign
  next();
};

/**
 * Applies the twelve hours price promotion page cache.
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
base.applyTwelveHoursPromotionSensitiveCache = function applyTwelveHoursPromotionSensitiveCache(req, res, next) {
  res.cachePeriod = 12; // eslint-disable-line no-param-reassign
  res.cachePeriodUnit = 'hours'; // eslint-disable-line no-param-reassign
  res.personalized = true; // eslint-disable-line no-param-reassign
  next();
};

/**
 * Applies the four hours Cache on Filters.
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call
 * @returns {void}
 */
base.applyPrefernceCenterCache = function applyPrefernceCenterCache(req, res, next) {
  var Site = require('dw/system/Site');
  var customPreferences = Site.current.preferences.custom;
  var enablePreferenceCenterOnPlp = true;
  if ('enablePreferenceCenterOnPlp' in customPreferences) {
    enablePreferenceCenterOnPlp = customPreferences.enablePreferenceCenterOnPlp;
  }

  if (enablePreferenceCenterOnPlp) {
    res.cachePeriod = 0; // eslint-disable-line no-param-reassign
  } else {
    res.cachePeriod = 4; // eslint-disable-line no-param-reassign
  }
  res.cachePeriodUnit = 'hours'; // eslint-disable-line no-param-reassign
  res.personalized = true; // eslint-disable-line no-param-reassign
  next();
};

module.exports = base;
