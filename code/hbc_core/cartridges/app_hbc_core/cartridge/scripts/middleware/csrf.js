'use strict';

var base = module.superModule;

var csrfProtection = require('dw/web/CSRFProtection');
var CustomerMgr = require('dw/customer/CustomerMgr');
var URLUtils = require('dw/web/URLUtils');
var Resource = require('dw/web/Resource');


/**
 * Middleware validating CSRF token from App request for seamless login
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
base.validateAppRequest = function (req, res, next) {
  if (!csrfProtection.validateRequest()) {
    res.setViewData({
      csrfFail: true
    });
  }
  next();
};

/**
 * Middleware validating CSRF token from App request for seamless login
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
base.validateAppUser = function (req, res, next) {
  if ('mobileAppUser' in session.custom && !session.custom.mobileAppUser) {
    res.setStatusCode(500);
    res.json({
      error: true,
      errorMessage: Resource.msg('error.message.app.fatal.error', 'login', null)
    });
    this.emit('route:Complete', req, res);
    return;
  }
  next();
};

/**
 * Middleware validating CSRF token from ajax request for login page
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
 base.validateAjaxAccountRequest = function (req, res, next) {
  if ('mobileAppUser' in session.custom && session.custom.mobileAppUser) {
    base.validateAppRequest();
  } else if (!csrfProtection.validateRequest() || !req.currentCustomer.profile) {
    CustomerMgr.logoutCustomer(true);
    res.redirect(URLUtils.url('CSRF-AjaxAccountFail'));
  }
  next();
};


module.exports = base;
