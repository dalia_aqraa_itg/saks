var log = dw.system.Logger.getLogger('KlarnaUtils');

/**
 * look in paymentInstruments for anything that matches giftcard
 * @param {dw.order} order - order model
 * @returns {boolean}
 */
function hasGiftCardPI(basket) {
  if (!basket.paymentInstruments || !basket.paymentInstruments.length) return false;

  var pis = basket.paymentInstruments.toArray().some(function(pi) {
    return pi.paymentMethod.toLowerCase() === 'giftcard';
  });

  return pis;
}

function isPaymentMethodActive(method) {
    var pMethod = dw.order.PaymentMgr.getPaymentMethod(method);
    return pMethod && pMethod.isActive();
}

function isKlarnaEnabled() {
    return isPaymentMethodActive('KLARNA_PAYMENTS');
}

function getCountry(req) {
    var Locale = require('dw/util/Locale');
    var localeId = req.locale.id || req.locale; // mysterious differences in requests.
    return Locale.getLocale(localeId).getCountry();
}

/**
 *
 * @param req - HTTP request
 * @param price - Money object indicating current basket total.
 * @returns {boolean|*}
 */
function isKlarnaApplicableToPrice(req, price) {
    try {
        var BasketMgr = require('dw/order/BasketMgr');
        var PaymentMgr = require('dw/order/PaymentMgr');
        var country = getCountry(req);
        var applicablePaymentMethods = PaymentMgr.getApplicablePaymentMethods(
            BasketMgr.getCurrentBasket().getCustomer(),
            country,
            (price.value || price)
        );
        var klarnaApplicable = applicablePaymentMethods.toArray().some(function (m) {
            return m.ID === 'KLARNA_PAYMENTS';
        });
        return klarnaApplicable;
    } catch (e) {
        log.warn('Exception determining if Klarna is applicable: {0}', e.toString());
        return false;
    }
}

function isGiftCard(line) {
    return line.product && 'hbcProductType' in line.product.custom
        && line.product.custom.hbcProductType === 'giftcard'
}

function basketContainsGiftCardLineItem(basket) {
    return basket && (basket.getGiftCertificateLineItems().size() > 0 ||
        basket.getAllProductLineItems().toArray().some(isGiftCard)
    );
}

/**
 * Checks:
 * - Klarna cannot be applied if the basket contains a gift card as a line item.
 * - Checks the configured min/max cart total for Klarna.
 * @param req
 * @returns {boolean}
 */
function isKlarnaApplicable(req) {
    try {
        var BasketMgr = require('dw/order/BasketMgr');
        var basket = BasketMgr.getCurrentBasket();
        if (basketContainsGiftCardLineItem(basket) || hasGiftCardPI(basket)) {
            return false;
        }
        if (isKlarnaApplicableToPrice(req, basket.totalGrossPrice)) {
            return true;
        }
    } catch (e) {
        log.warn('Exception determining if Klarna is applicable: {0}', e.toString());
    }
    return false;
}

/**
 * Check if the customer is international customer or not.
 * @params {Request} - request object to get the location.
 * @returns {boolean} - true if is an international customer,false otherwise.
 */
function isInternationalCustomer(req) {
    var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
    // Logic to not show PDP messaging for Klarna if Internal Customer
    var bfxCountryCode = cookiesHelper.read('bfx.country');

    var isInternationalCustomer = false;
    // no cookie set yet, first access to the site.
    if (!bfxCountryCode) {
        var geolocation = req.geolocation;
        // no cookie set, so check location to see if its on US or not.
        if (geolocation && geolocation.countryCode === 'US') {
            isInternationalCustomer = false;
        } else {
            isInternationalCustomer = true;
        }
    } else {
        if (bfxCountryCode !== 'US') {
            isInternationalCustomer = true;
        }
    }

    return isInternationalCustomer;
}

function hasKlarnaPaymentMethod(orderModel) {
    try {
        if (isKlarnaEnabled()) {
            var KlarnaPaymentConstants = require('*/cartridge/scripts/util/klarnaPaymentsConstants');
            var hasKlarnaPi = orderModel.paymentInfo.paymentMethods.some(function (pi) {
                return pi.paymentMethod === KlarnaPaymentConstants.PAYMENT_METHOD || (pi.paymentType && pi.paymentType === 'KLARNA');
            });
            return hasKlarnaPi;
        }
    } catch (e) {
        log.warn('hasKlarnaPaymentMethod: Exception determining if has klarna on order object: {0}', e.toString());
        return false;
    }
}

function getPaymentMethod() {
  var pMethod = dw.order.PaymentMgr.getPaymentMethod('KLARNA_PAYMENTS');
  return pMethod;
}


module.exports = {
  basketContainsGiftCardLineItem: basketContainsGiftCardLineItem,
  hasKlarnaPaymentMethod: hasKlarnaPaymentMethod,
  isKlarnaEnabled: isKlarnaEnabled,
  isKlarnaApplicable: isKlarnaApplicable,
  isKlarnaApplicableToPrice: isKlarnaApplicableToPrice,
  isInternationalCustomer: isInternationalCustomer,
  getPaymentMethod: getPaymentMethod
};
