var Site = require('dw/system/Site');
var CurrentSite = Site.current.preferences;
var preferences = CurrentSite ? CurrentSite.custom : {};

var Constants = {
  APIGW: preferences.capOnePrefillApigwApiId,
  API_KEY: preferences.capOnePrefillApikey,
  REDIRECT_URL: preferences.capOnePrefillRedirectLink,
  SAKSFIFTH_STORENO: preferences.capOneUniquePidStoreNo,
  prefillService: preferences.capOnePrefillService,
  partnerIdLookUpService: 'capOnePartnerIdLookUpService',
  test29Pid: '9999900000000000020911217198001521145124'
};

module.exports = Constants;
