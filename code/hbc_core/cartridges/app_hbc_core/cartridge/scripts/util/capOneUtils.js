'use strict';

var Logger = require('dw/system/Logger');
var capOneLogger = Logger.getLogger('CapOne-Service-Log', 'CapOne-Service-Utils');
var capOneConstants = require('*/cartridge/scripts/util/capOneConstants');
var uniquePid = require('*/cartridge/scripts/util/uniquePartnerId');

var Result = require('dw/svc/Result');

function capOneNewCustomer(registrationForm, newCustomerProfile, test29) {
  if (test29) {
    var pid = capOneConstants.test29Pid;
  } else {
    var pid = uniquePid.generateId();
  }
  var newCustomerProfileObj = {};
  newCustomerProfileObj.pid = pid;
  newCustomerProfileObj.firstName = registrationForm.firstName;
  newCustomerProfileObj.lastName = registrationForm.lastName;
  newCustomerProfileObj.phoneMobile = registrationForm.phone ? registrationForm.phone : '';
  newCustomerProfileObj.email = registrationForm.email;
  newCustomerProfileObj.zipCode = newCustomerProfile.custom.zipCode;
  newCustomerProfileObj.customerNo = newCustomerProfile.customerNo;
  newCustomerProfileObj.errorCode = 'cpfError';

  return newCustomerProfileObj;
}

function loginCapOnePrefill(req, customer) {
  var pid = req.querystring.test29 ? capOneConstants.test29Pid : uniquePid.generateId();

  var args = {};
  args.pid = pid;
  args.firstName = customer.profile.firstName;
  args.lastName = customer.profile.lastName;
  args.phoneMobile = customer.profile.phoneMobile ? customer.profile.phoneMobile : customer.profile.phoneHome;
  args.email = customer.profile.email;
  args.zipCode =
    customer && customer.addressBook && customer.addressBook.preferredAddress && customer.addressBook.preferredAddress.postalCode
      ? customer.addressBook.preferredAddress.postalCode
      : '';
  args.customerNo = customer.profile.customerNo;
  args.errorCode = 'cpfError';

  return prefillCapOne(req, args);
}

function prefillCapOne(req, args) {
  var capOnePrefillRequest = require('*/cartridge/models/capitalOne/prefillRequest');
  var capOneService = new (require('*/cartridge/scripts/init/capOnePrefillService'))();
  var URLUtils = require('dw/web/URLUtils');
  var prefillRequest = capOnePrefillRequest.getRequestJSON(args);
  var requestJsonString = JSON.stringify(prefillRequest);
  capOneLogger.info(requestJsonString);

  session.privacy.partnerId = prefillRequest.partnerAppRequestId; //Test. Remove before rollout.
  req.session.privacyCache.set('partnerId', prefillRequest.partnerAppRequestId);
  req.session.privacyCache.set('customerNo', session.getCustomer().profile.customerNo);

  var result = capOneService.getPrefillCacheKey(prefillRequest);
  var response = {};
  response.error = false;

  // Check for service availability.
  if (result.status === Result.SERVICE_UNAVAILABLE) {
    capOneLogger.error('[capOneUtil.js prefillCapOne] Error : SERVICE_UNAVAILABLE response');
    response.error = true;
    response.errorMessage = result.errorMessage;
    capOneLogger.error('Prefill Service unavailable.');
    capOneLogger.error(result.errorMessage);

    result = {};
    result.object = {};
    result.object.redirectUrl = URLUtils.url('Account-Show', args.errorCode, 'true').toString();

    return result;
  }
  capOneLogger.info(JSON.stringify(result.object));

  if (result.status != 'OK' || !result.object) {
    response.error = true;
    response.errorMessage = result.errorMessage;
    capOneLogger.error('Failed to get cachkey.');
    capOneLogger.error(result.errorMessage);

    result = {};
    result.object = {};
    result.object.redirectUrl = URLUtils.url('Account-Show', args.errorCode, 'true').toString();
  } else if (result.object && result.object.cacheKey !== undefined && result.object.cacheKey !== null && result.status == 'OK') {
    var test29 = req.querystring.test29; // Test 29 digit
    var cacheKey = result.object.cacheKey;
    var rLink = URLUtils.abs('Account-Show', 'partnerid', prefillRequest.partnerAppRequestId);
    var rdUrl = capOneConstants.REDIRECT_URL.replace('<MOCK_CACHE>', cacheKey).replace('<RLINK>', rLink);
    result.object.redirectUrl = rdUrl;
    if (test29) {
      // result.object.redirectUrl = URLUtils.url('Account-Show').toString();
    }
  } else {
    response.error = true;
    response.errorMessage = result.object.response_message;
    capOneLogger.info('Prefill failure: {0}', result.object.response_message);

    result = {};
    result.object = {};
    result.object.redirectUrl = URLUtils.url('Account-Show', args.errorCode, 'true').toString();
  }
  return result;
}

function getQueryVariable(variable, query) {
  if (query.trim() !== null && query.trim() !== '') {
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split('=');
      if (decodeURIComponent(pair[0].toUpperCase) == variable.toUpperCase) {
        return decodeURIComponent(pair[1]);
      }
    }
    capOneLogger.error('Query variable %s not found', variable);
  }
}

module.exports = {
  prefillCapOne: prefillCapOne,
  loginCapOnePrefill: loginCapOnePrefill,
  capOneNewCustomer: capOneNewCustomer,
  getQueryVariable: getQueryVariable
};
