/* globals session */
'use strict';

var Logger = require('dw/system/Logger');
var uniquePidLogger = Logger.getLogger('CapOne-Service-Log', 'CapOne-Service');

/*
 * Takes 5 digit store number
 * Returns 29 digit string
 */
function generateId() {
  var capOneConstants = require('*/cartridge/scripts/util/capOneConstants');
  var clientIpFull = '';
  var clientIp = request.getHttpRemoteAddress();
  var storeNumber = capOneConstants.SAKSFIFTH_STORENO;

  requestDate = formatDate(new Date());

  if (!storeNumber) {
    return null;
  }

  if (storeNumber.length < 5) {
    return null;
  }

  var clientIpStrip = clientIp.split('.').join('');

  if (clientIpStrip.length < 12) {
    clientIpFull = '000000000000' + clientIpStrip.slice(-12);
  } else {
    clientIpFull = clientIpStrip;
  }

  var uniqueId = storeNumber + clientIpFull + requestDate;
  uniquePidLogger.debug('Unique Partner ID: ' + uniqueId);
  return uniqueId;
}

function formatDate(date) {
  var d = new Date(date);
  var month = '' + ('00' + d.getMonth()).slice(-2);
  var day = ('00' + d.getDate()).slice(-2);
  var year = d.getFullYear().toString().substr(2, 4);
  var hour = ('00' + d.getHours()).slice(-2);
  var minute = ('00' + d.getMinutes()).slice(-2);
  var seconds = ('00' + d.getSeconds()).slice(-2);

  return month + day + year + hour + minute + seconds;
}

module.exports = {
  generateId: generateId
};
