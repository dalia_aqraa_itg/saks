'use strict';

var server = require('server');

var capOneConstants = require('*/cartridge/scripts/util/capOneConstants');
var Logger = require('*/dw/system/Logger');
var prefillUtil = require('*/cartridges/cartridge/scripts/util/capOneUtils.js');

function prepRequest(req) {
  var cust = req.currentCustomer.raw;

  if (!cust) {
    res.json({
      error: true
    });
    this.emit('route:Complete', req, res);
    return;
  }

  var profile = cust.profile;
  if (!profile) {
    res.json({
      error: true
    });
    this.emit('route:Complete', req, res);
    return;
  }

  var args = {};
  args.profile = profile;
  args.address = req.currentCustomer.address;

  var results = prefillUtil.prefillCapOne(args);

  return prefilRequest;
}
