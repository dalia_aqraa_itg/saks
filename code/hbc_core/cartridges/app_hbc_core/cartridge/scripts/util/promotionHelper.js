/* globals session */
'use strict';

var PromotionMgr = require('dw/campaign/PromotionMgr');

/**
 *  Sets the session promotionCache for global promos and PrivateSale promotion.
 *
 *  It has to update the session variable on every run of this function, so the promoCache has the latest reference of promos
 *  which are later leveraged on the tile model.
 */
function setPromoCache() {
  var collections = require('*/cartridge/scripts/util/collections');
  var activePromotions = PromotionMgr.activeCustomerPromotions.getProductPromotions();
  var promoIds = collections.map(activePromotions, function (promo) {
    return promo.ID;
  });
  delete session.privacy.privateSalePromoID;
  delete session.privacy.promoCache;
  var privateSalePriceBookIDs = [];
  collections.find(activePromotions, function (promo) {
    if (promo && promo.custom && promo.custom.hasOwnProperty('privateSalePriceBookID') && promo.custom.privateSalePriceBookID) {
      privateSalePriceBookIDs.push(promo.custom.privateSalePriceBookID);
    }
  });

  var promoIDstring = JSON.stringify(promoIds);
  if (promoIDstring.length <= 2000) {
    session.privacy.promoCache = promoIDstring;
  }
  if (privateSalePriceBookIDs && privateSalePriceBookIDs.length > 0) {
    session.privacy.privateSalePromoID = privateSalePriceBookIDs.toString();
  }
}

/**
 * Get first Promotion related to the coupon line Item.
 * @param {dw.promotion.CouponLineItem} couponLineItem 
 * @return Promotion 
 */
function getCouponLineItemPromotion(couponLineItem) {
  var CouponMgr = require('dw/campaign/CouponMgr');
  var collections = require('*/cartridge/scripts/util/collections');
  var coupon = CouponMgr.getCouponByCode(couponLineItem.getCouponCode());
  var PromotionPlan = PromotionMgr.getActiveCustomerPromotions();
  var activePromotions = PromotionPlan.getPromotions().iterator();
  var promotion;
  var firstCoupon;
  while (activePromotions.hasNext() && !firstCoupon) {
    promotion = activePromotions.next();
    firstCoupon = collections.find(promotion.getCoupons(), function (promoCoupon) {
      return coupon.getID() === promoCoupon.getID();
    });
  }

  return promotion;
}

module.exports = {
  setPromoCache: setPromoCache,
  getCouponLineItemPromotion: getCouponLineItemPromotion
};
