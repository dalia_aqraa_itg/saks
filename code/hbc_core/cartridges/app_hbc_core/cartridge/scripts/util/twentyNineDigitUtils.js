'use strict';

var validateTCCSecurityCode = require('*/cartridge/scripts/checkout/validateTCCSecurityCode');
var StringUtils = require('dw/util/StringUtils');
var capOneLogger = require('dw/system/Logger');
var Result = require('dw/svc/Result');
var Transaction = require('dw/system/Transaction');

function generate(tokenObj) {
  var twentySixDigitNumber = '0';
  var splitDate = tokenObj.expiryDate.toString().split('/');
  var expiryDate = splitDate[1].toString() + splitDate[0].toString();

  tokenObj.passExpDate = add14Days(expiryDate);
  twentySixDigitNumber = tokenObj.tokenizedCard + expiryDate + tokenObj.passExpDate;
  tokenObj.twentyNineDigitNumber = twentySixDigitNumber + ('000' + validateTCCSecurityCode.Calc(twentySixDigitNumber)).substr(-3);

  return tokenObj;
}

function saveCardToProfile(numberObj) {
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
  var saved = false;

  saved = accountHelpers.saveTCCToAccount(numberObj.twentyNineDigitNumber);

  return saved;
}

function add14Days(expDate) {
  var issueExpiryDate = new Date();
  issueExpiryDate.setDate(issueExpiryDate.getDate() + 14);
  var issueExpiryDateFormatted = StringUtils.formatDate(issueExpiryDate, 'yyMMdd');
  return issueExpiryDateFormatted;
}

function partnerIdLookUp(partnerId) {
  var partnerIdRequest = require('*/cartridge/models/capitalOne/partnerIdRequest');
  var partnerIdService = new (require('*/cartridge/scripts/init/capOnePartnerIdService'))();
  var pidRequest = partnerIdRequest.getRequestParamString(partnerId);
  capOneLogger.info(pidRequest);

  var result = partnerIdService.getCardToken(pidRequest);
  var response = {};
  response.error = false;

  // Check for service availability.
  if (result.status === Result.SERVICE_UNAVAILABLE) {
    capOneLogger.getRootLogger().fatal('[twentyNineDigitUtil.js prefillCapOne] Error : SERVICE_UNAVAILABLE response');
    return result;
  }
  capOneLogger.info(JSON.stringify(result.object));

  if (result.status != 'OK' || !result.object) {
    response.error = true;
    response.errorMessage = result.errorMessage;
    capOneLogger.error('Failed to get user from partner id.');
    capOneLogger.error(result.errorMessage);
  } else if (result.object && result.object.success == false) {
    response.error = true;
    response.errorMessage = result.errorMessage;
    capOneLogger.error('Failed to get user from partner id.');
    capOneLogger.error(result.errorMessage);
  } else if (result.object && result.object.success == false) {
    response.error = true;
    response.errorMessage = result.errorMessage;
    capOneLogger.error('Failed to get user from partner id.');
    capOneLogger.error(result.errorMessage);
  } else if (result.object && result.status == 'OK') {
    capOneLogger.debug('Partner ID: ' + result.object.partnerIdRequest + ' Expiry Date: ' + result.object.expiryDate);
  } else {
    response.error = true;
    response.errorMessage = result.object.response_message;
    capOneLogger.info('Partner ID look up failure: {0}', result.object.response_message);
  }
  return result;
}

function generate29DigitNumber(req) {
  var partnerId = req.querystring.partnerid;
  var savedNumber = false;
  var partnerIdValidated = false;
  var sessionPid = req.session.privacyCache.get('partnerId');
  var sessionCustNo = req.session.privacyCache.get('customerNo');
  var currentCustNo = req.currentCustomer.raw.profile.customerNo;
  var success = false;

  //Verify partner id belongs to user
  if (sessionPid == partnerId && sessionCustNo == currentCustNo) {
    partnerIdValidated = true;
  }

  // save saks first apply date - Cardholder is allowed to have FDD for a single day
  var profile = customer.profile;
  if (profile) {
    Transaction.wrap(function () {
      profile.custom.saksFirstApplyDate = new Date();
    });
  }

  if (partnerId && partnerIdValidated) {
    var result = partnerIdLookUp(partnerId);
    if (result.object) {
      if ('success' in result.object && result.object.success == false) {
        success = false;
        capOneLogger.error(result.object.errorMessage);

        //Catches dup card lookup and fowards to success screen
        if (result.object.errorCode === 'ERR_DUP_LOOKUP') {
          success = true;
          capOneLogger.error(result.object.errorMessage);
        }
      } else {
        var tokenObj = result.object;
        var numberObj = generate(tokenObj);

        if (numberObj) {
          savedNumber = saveCardToProfile(numberObj);
        }
        if (numberObj && savedNumber) {
          success = true;
          capOneLogger.info('Number generated successfully stored');
        } else if (numberObj && !savedNumber) {
          success = false;
          capOneLogger.error('Number generated but failed to store number');
        } else {
          success = false;
          capOneLogger.error('Failed to generate and store number');
        }
      }
    } else {
      success = false;
      capOneLogger.error('Non-matched Partner ID. ');
    }
  } else {
    success = false;
    capOneLogger.error('User failed authenication.');
  }
  return success;
}

module.exports = {
  generate: generate,
  saveCardToProfile: saveCardToProfile,
  partnerIdLookUp: partnerIdLookUp,
  add14Days: add14Days,
  generate29DigitNumber: generate29DigitNumber
};
