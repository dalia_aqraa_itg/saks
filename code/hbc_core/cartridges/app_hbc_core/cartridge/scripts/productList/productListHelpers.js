'use strict';
var base = module.superModule;
/**
 * @typedef config
 * @type Object
 * @property {number} type - a number for what type of product list is being created
 */
/**
 * Retrieve the list of the customer
 * @param {dw.customer.Customer} customer - current customer
 * @param {Object} config - configuration object
 * @return {dw.customer.ProductList} list - target productList
 */
function getList(customer, config) {
  var productListMgr = require('dw/customer/ProductListMgr');
  var type = config.type;
  var list;
  if (config.type === 10) {
    if (!empty(customer)) {
      var getProductLists = productListMgr.getProductLists(customer, type);
      list = getProductLists.length > 0 ? getProductLists[0]: base.createList(customer, { type: type });
    } else {
      list = null;
    }
  } else if (config.type === 11) {
    list = productListMgr.getProductList(config.id);
  } else {
    list = null;
  }
  if (list && !list.public) {// Set list.public = true only if its False 
    var Transaction = require('dw/system/Transaction');
    Transaction.wrap(function () {
      list.public = true;
    });
  }
  return list;
}
base.getList = getList;
module.exports = base;
