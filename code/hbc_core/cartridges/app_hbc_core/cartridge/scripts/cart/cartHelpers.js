'use strict';

var base = module.superModule;
var ProductMgr = require('dw/catalog/ProductMgr');
var Resource = require('dw/web/Resource');
var UUIDUtils = require('dw/util/UUIDUtils');
var collections = require('*/cartridge/scripts/util/collections');
var Transaction = require('dw/system/Transaction');
var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
var bopisHelper = require('*/cartridge/scripts/helpers/bopisHelpers');
var instorePickupStoreHelper = require('*/cartridge/scripts/helpers/instorePickupStoreHelpers');
var Money = require('dw/value/Money');
var priceHelper = require('*/cartridge/scripts/helpers/pricing');
var Logger = require('dw/system/Logger');

/**
 * Helper Method written to display the Prorated discount values of the basket items
 *
 * @param {dw/catalog/ProductPriceModel} priceModel - price model
 * @returns {dw/value/Money} price - price
 */
function getListPrice(priceModel) {
  var price = Money.NOT_AVAILABLE;
  var priceBook;
  var priceBookPrice;

  if (priceModel.price.valueOrNull === null && priceModel.minPrice) {
    return priceModel.minPrice;
  }

  priceBook = priceHelper.getRootPriceBook(priceModel.priceInfo.priceBook);
  priceBookPrice = priceModel.getPriceBookPrice(priceBook.ID);

  if (priceBookPrice.available) {
    return priceBookPrice;
  }

  price = priceModel.price.available ? priceModel.price : priceModel.minPrice;

  return price;
}

/**
 * Adjust linte item quantities in basket as per OMS inventory
 * @param {dw.order.Basket} currentBasket - Current users's basket
 * @param {string[]} items with quantity returned by OMS inventory lookup
 */
base.adjustBasketQuantities = function adjustBasketQuantities(basket, omsInventory) {
  collections.forEach(basket.productLineItems, function (lineItem) {
    omsInventory.forEach(function (item) {
      if (item.quantity !== 0) {
        if (item.itemID === lineItem.productID && item.quantity > 0) {
          Transaction.wrap(function () {
            lineItem.setQuantityValue(Number(item.quantity));
          });
        }
      }
    });
  });
};

/**
 * Check the requested quantity of the line item against inventory from OMS (stored in session)
 * @param {dw.order.LineItem} lineItem  currentBasket - Current users's basket
 * @return {boolean} returns if quanity is checked or not
 */
function checkOMSInventory(lineItem) {
  var checkedInOMS = false;
  var omsInventoryArray = JSON.parse(session.custom.omsInventory);
  if (omsInventoryArray && omsInventoryArray.length > 0) {
    for (var i = 0; i < omsInventoryArray.length; i++) {
      if (omsInventoryArray[i].itemID === lineItem.productID) {
        checkedInOMS = true;
        break;
      }
    }
  }
  return checkedInOMS;
}

/**
 * Return OMS inventory data in a map of items and quantities
 * @param {string} lineItem  OMS inventory data in a string representing a JSON array
 * @param {Number} quantity to update
 * @return {dw.unti.HashMap} returns map of items and quantities
 */
base.getOMSInventoryMap = function getOMSInventoryMap(omsInventory) {
  var omsInventoryMap = new dw.util.HashMap();
  var omsInventoryArray = JSON.parse(omsInventory);
  if (omsInventoryArray && omsInventoryArray.length > 0) {
    omsInventoryArray.forEach(function (item) {
      omsInventoryMap.put(item.itemID, item.quantity);
    });
  }
  return omsInventoryMap;
};

/**
 * Adjust line item quantities in the basket based on purchase limit and availability
 * @param {dw.order.Basket} current basket to adjust quantities in line items
 * @returns Array of product ids of updated  line items
 */
base.adjustLineItemQuantities = function adjustLineItemQuantities(basket) {
  var updatedLineItems = [];
  if (basket) {
    var productLineItems = basket.productLineItems;
    for (var i = 0; i < productLineItems.length; i++) {
      if (!checkOMSInventory(productLineItems[i])) {
        var quantityToUpdate = getUpdateQuantity(productLineItems[i]);
        if (quantityToUpdate.updateQty && quantityToUpdate.updateQty > 0) {
          Transaction.wrap(function () {
            productLineItems[i].setQuantityValue(quantityToUpdate.quantity);
          });
          updatedLineItems.push(productLineItems[i].productID);
        }
      }
    }
  }
  return updatedLineItems;
};

/**
 * get Bundled products
 * @param apiProduct
 * @returns
 */
function getBundledProductIds(apiProduct) {
  var productIds = [];
  collections.forEach(apiProduct, function (bundledProduct) {
    productIds.push(bundledProduct.ID);
  });

  return productIds;
}
/**
 * Determine the quantity to be adjusted in a product line item
 * @param lineItem
 * @returns
 */
function getUpdateQuantity(lineItem) {
  var result = {};
  if (lineItem.product === null || !lineItem.product.online) {
    result.updateQty = false;
    return result;
  }
  if (Object.hasOwnProperty.call(lineItem.custom, 'fromStoreId') && lineItem.custom.fromStoreId) {
    var instorePUstoreHelpers = require('*/cartridge/scripts/helpers/instorePickupStoreHelpers');
    var unitsAtStores = instorePUstoreHelpers.getStoreInventory(lineItem.custom.fromStoreId, lineItem.productID);
    if (!unitsAtStores || unitsAtStores <= 0 || unitsAtStores >= lineItem.quantityValue) {
      result.updateQty = false;
      return result;
    }
    var purchaselimit = 'purchaseLimit' in lineItem.product.custom && lineItem.product.custom.purchaseLimit ? lineItem.product.custom.purchaseLimit : null;
    if (purchaselimit && purchaselimit > 0 && unitsAtStores > purchaselimit) {
      result.updateQty = true;
      result.quantity = purchaselimit;
      return result;
    }
    if (lineItem.minOrderQuantityValue && lineItem.minOrderQuantityValue > 0 && unitsAtStores < lineItem.minOrderQuantityValue) {
      result.updateQty = true;
      result.quantity = lineItem.minOrderQuantityValue;
      return result;
    }
    result.updateQty = true;
    result.quantity = unitsAtStores;
    return result;
  } else {
    var perpetual =
      lineItem.product.availabilityModel && lineItem.product.availabilityModel.inventoryRecord
        ? lineItem.product.availabilityModel.inventoryRecord.perpetual
        : false;
    if (perpetual) {
      result.updateQty = false;
      return result;
    }
    var ats =
      lineItem.product.availabilityModel && lineItem.product.availabilityModel.inventoryRecord && lineItem.product.availabilityModel.inventoryRecord.ATS
        ? lineItem.product.availabilityModel.inventoryRecord.ATS.value
        : 0;
    if (!ats || ats <= 0 || ats >= lineItem.quantityValue) {
      result.updateQty = false;
      return result;
    }
    var purchaselimit = 'purchaseLimit' in lineItem.product.custom && lineItem.product.custom.purchaseLimit ? lineItem.product.custom.purchaseLimit : null;
    if (purchaselimit && purchaselimit > 0 && ats > purchaselimit) {
      result.updateQty = true;
      result.quantity = purchaselimit;
      return result;
    }
    if (lineItem.minOrderQuantityValue && lineItem.minOrderQuantityValue > 0 && ats < lineItem.minOrderQuantityValue) {
      result.updateQty = true;
      result.quantity = lineItem.minOrderQuantityValue;
      return result;
    }
    result.updateQty = true;
    result.quantity = ats;
    return result;
  }
}

/**
 * Adds a product to the cart. If the product is already in the cart it increases the quantity of that product.
 * @param {dw.order.Basket} currentBasket - Current users's basket
 * @param {string} productId - the productId of the product being added to the cart
 * @param {number} quantity - the number of products to the cart
 * @param {string[]} childProducts - the products' sub-products
 * @param {SelectedOption[]} options - product options
 * @param {string} storeId - store id to be added to product item
 * @param {Object} req - current request
 * @return {Object} returns an error object
 */
base.addProductToCart = function (currentBasket, productId, quantity, childProducts, options, storeId, req) {
  var availableToSell;
  var defaultShipment = currentBasket.defaultShipment;
  var perpetual = false;
  var product = ProductMgr.getProduct(productId);
  var productInCart;
  var productLineItems = currentBasket.productLineItems;
  var productQuantityInCart;
  var quantityToSet;
  var preferences = require('*/cartridge/config/preferences');

  var result = {
    error: false,
    message: Resource.msg('text.alert.addedtobasket', 'product', null)
  };

  // return if basket limit is reached
  if (preferences.basketItemLimit && productLineItems && productLineItems.length >= preferences.basketItemLimit) {
    result.message = 'LIMIT_EXCEEDED';
    return result;
  }

  if (product === null) {
    result.error = true;
    return result;
  }

  var optionModel = productHelper.getCurrentOptionModel(product.optionModel, options);
  var totalQtyRequested = 0;
  var canBeAdded = false;

  if (product.bundle) {
    canBeAdded = base.checkBundledProductCanBeAdded(childProducts, productLineItems, quantity);
  } else {
    totalQtyRequested = quantity + base.getQtyAlreadyInCart(productId, productLineItems);
    if (storeId) {
      var instorePUstoreHelpers = require('*/cartridge/scripts/helpers/instorePickupStoreHelpers');
      var unitsAtStores = instorePUstoreHelpers.getStoreInventory(storeId, productId);
      canBeAdded = totalQtyRequested <= unitsAtStores;
    } else {
      if (!empty(product.availabilityModel.inventoryRecord)) {
        perpetual = product.availabilityModel.inventoryRecord.perpetual;
        canBeAdded = perpetual || totalQtyRequested <= product.availabilityModel.inventoryRecord.ATS.value;
      }
    }
  }
  var isPurchaseLimit = 'purchaseLimit' in product.custom ? product.custom.purchaseLimit && totalQtyRequested <= product.custom.purchaseLimit : true;
  if (!isPurchaseLimit) {
    result.error = true;
    result.message = Resource.msgf('label.notinpurchaselimit', 'common', null, product.custom.purchaseLimit);
    return result;
  }
  if (!canBeAdded) {
    result.error = true;
    if (product.availabilityModel.inventoryRecord != null) {
      result.message = Resource.msg('error.alert.selected.quantity.cannot.be.added.for', 'product', null);
    }
    return result;
  }

  // Get the existing product line item from the basket if the new product item
  // has the same bundled items or options and the same instore pickup store selection
  productInCart = base.getExistingProductLineItemInCartWithTheSameStore(product, productId, productLineItems, childProducts, options, storeId);

  if (productInCart) {
    productQuantityInCart = productInCart.quantity.value;
    quantityToSet = quantity ? quantity + productQuantityInCart : productQuantityInCart + 1;
    availableToSell = productInCart.product.availabilityModel.inventoryRecord.ATS.value;

    if (availableToSell >= quantityToSet || perpetual) {
      productInCart.setQuantityValue(quantityToSet);
      result.uuid = productInCart.UUID;
    } else {
      result.error = true;
      result.message =
        availableToSell === productQuantityInCart
          ? Resource.msg('error.alert.max.quantity.in.cart', 'product', null)
          : Resource.msg('error.alert.selected.quantity.cannot.be.added', 'product', null);
    }
  } else {
    var productLineItem;
    // Create a new instore pickup shipment as default shipment for product line item
    // if the shipment if not exist in the basket
    var inStoreShipment = base.createInStorePickupShipmentForLineItem(currentBasket, storeId, req);
    var shipment = inStoreShipment || defaultShipment;

    if (shipment.shippingMethod && shipment.shippingMethod.custom.storePickupEnabled && !storeId) {
      shipment = currentBasket.createShipment(UUIDUtils.createUUID());
    }
    productLineItem = base.addLineItem(currentBasket, product, quantity, childProducts, optionModel, shipment);
    Logger.debug('addCartItemToProductList!!!!!');
    addCartItemToProductList(product);

    // Once the new product line item is added, set the instore pickup fromStoreId for the item
    if (storeId) {
      instorePickupStoreHelper.setAndGetStoreInProductLineItem(storeId, productLineItem);
    }
    result.uuid = productLineItem.UUID;
  }
  return result;
};

/**
 * Get the existing non instore shipment
 * @param {dw.order.Basket} basket - the target Basket object
 * @return {dw.order.Shipment} returns Shipment
 */
function getShipToHomeShipment(basket) {
  var existingShipment = null;
  if (basket) {
    var shipments = basket.getShipments();
    if (shipments.length && shipments.length > 1) {
      for (var i = 0; i < shipments.length; i++) {
        var shipment = shipments[i];
        if (!shipment.custom.shipmentType) {
          existingShipment = shipment;
          break;
        }
      }
    } else if (shipments.length && shipments.length === 1) {
      existingShipment = basket.defaultShipment;
      existingShipment.custom.fromStoreId = null;
      existingShipment.custom.shipmentType = null;
    }
  }
  return existingShipment;
}

/**
 * Updates ProductlineItem and Shipment on toggle of ship to options in cart
 * @param {dw.order.Basket} currentBasket - PLI to be updated
 * @param {Object} apiProductLineItem - PLI to be updated
 * @param {string} storeId - storeid to set to product line item
 * @param {Object} req - request oblect
 * @return {Object} store - stroe updated to PLI with custom data
 */
base.changeShippingOption = function (currentBasket, apiProductLineItem, storeId, req) {
  var shipment = currentBasket.defaultShipment;
  var lineItem = apiProductLineItem;
  var store = null;
  // get shipment or create shipment in basket
  if (storeId) {
    // instore shipment with store id
    var inStoreShipment = base.createInStorePickupShipmentForLineItem(currentBasket, storeId, req);
    shipment = inStoreShipment || shipment;
  } else {
    // ship to  home shipment
    shipment = getShipToHomeShipment(currentBasket);
  }
  // set shipment
  if (shipment) {
    if (storeId) {
      store = instorePickupStoreHelper.setAndGetStoreInProductLineItem(storeId, lineItem);
    } else {
      instorePickupStoreHelper.resetInstoreProductLineItem(lineItem);
    }
    lineItem.setShipment(shipment);
  }

  return bopisHelper.setShippingOptionData(store, apiProductLineItem);
};

/**
 * Helper Method written to display the Prorated discount values of the basket items
 *
 * @param {dw/order/ProductLineItem} lineItem - current line item
 * @param {dw/order/Basket} currentBasket - current basket
 * @param {Object} prorationObj - Proration object
 * @returns {Object} prorationObj - proration object
 */
function getProrationCustomAttributes(lineItem, currentBasket, prorationObj) {
  var pricebookDiscount = new Money(0, currentBasket.getCurrencyCode());
  var associateDiscount = new Money(0, currentBasket.getCurrencyCode());
  var specialVendorDiscount = new Money(0, currentBasket.getCurrencyCode());
  var fddDiscount = new Money(0, currentBasket.getCurrencyCode());
  let prorationObject = prorationObj;
  var proratePricesMap = lineItem.getProratedPriceAdjustmentPrices();
  var priceModel = lineItem.product.priceModel;
  var priceBook = priceModel.priceInfo.priceBook;
  var listPrice = new Money(getListPrice(priceModel) * lineItem.quantity.value, currentBasket.getCurrencyCode());
  if (priceBook && 'isPromotionPriceBook' in priceBook.custom && priceBook.custom.isPromotionPriceBook) {
    pricebookDiscount = listPrice.subtract(lineItem.getPrice());
  }
  if (proratePricesMap) {
    var priceAdjKeys = proratePricesMap.keySet().toArray();
    var proratedPrice;
    for (var i = 0; i < priceAdjKeys.length; i++) {
      var promotion = priceAdjKeys[i].promotion;
      proratedPrice = new Money(Math.abs(proratePricesMap.get(priceAdjKeys[i]).value), currentBasket.getCurrencyCode());

      if (!empty(promotion) && promotion.custom.promotionType.value === 'Associate') {
        associateDiscount = associateDiscount.add(proratedPrice);
      } else if (!empty(promotion) && promotion.custom.promotionType.value === 'SpecialVendorDisc') {
        specialVendorDiscount = specialVendorDiscount.add(proratedPrice);
      } else if (!empty(promotion) && promotion.custom.promotionType.value === 'FDD') {
        fddDiscount = fddDiscount.add(proratedPrice);
      } else {
        pricebookDiscount = pricebookDiscount.add(proratedPrice);
      }
    }

    if (pricebookDiscount.value > 0) {
      prorationObject.productPromotionDiscount = pricebookDiscount.value;
    } else {
      prorationObject.productPromotionDiscount = '0';
    }

    if (associateDiscount.value > 0) {
      prorationObject.productPromotionAssociateDiscount = associateDiscount.value;
    } else {
      prorationObject.productPromotionAssociateDiscount = '0';
    }

    if (specialVendorDiscount.value > 0) {
      prorationObject.productPromotionSpecialVendorDiscount = specialVendorDiscount.value;
    } else {
      prorationObject.productPromotionSpecialVendorDiscount = '0';
    }

    if (fddDiscount.value > 0) {
      prorationObject.productPromotionFirstDayDiscount = fddDiscount.value;
    } else {
      prorationObject.productPromotionFirstDayDiscount = '0';
    }
  }
  return prorationObject;
}

/**
 * Helper Method written to display the Prorated discount values of the basket items
 *
 * @param {dw/order/Basket} currentBasket - current basket
 * @returns {string} - final promotion object
 */
base.getProrationData = function (currentBasket) {
  var productLineItems = currentBasket.getProductLineItems();
  // eslint-disable-next-line no-array-constructor
  var prorationObjArray = new Array();
  // eslint-disable-next-line no-new-object
  var finalPromotionObj = new Object();
  var shippingPriceAdjs = currentBasket.getAllShippingPriceAdjustments();
  var shipPromoId = '';
  var shippingPA;
  if (!empty(shippingPriceAdjs)) {
    shippingPA = shippingPriceAdjs.toArray()[0];
    if (!empty(shippingPA)) {
      shipPromoId = shippingPA.promotionID;
    }
  }

  collections.forEach(productLineItems, function (lineItem) {
    // eslint-disable-next-line no-new-object
    var prorationObj = new Object();
    prorationObj.Id = lineItem.productID;
    prorationObj.name = lineItem.productName;
    prorationObj = getProrationCustomAttributes(lineItem, currentBasket, prorationObj);
    var proratePricesMap = lineItem.getProratedPriceAdjustmentPrices();
    if (proratePricesMap) {
      // eslint-disable-next-line no-array-constructor
      var promotionArray = new Array();
      var priceAdjKeys = proratePricesMap.keySet();
      collections.forEach(priceAdjKeys, function (priceAdjKey) {
        var promotionObj = {};
        var promotion = priceAdjKey.promotion;
        var proratedPrice = new Money(Math.abs(proratePricesMap.get(priceAdjKey).value), currentBasket.getCurrencyCode());
        if (!empty(promotion)) {
          promotionObj.promotionId = promotion.ID;
          promotionObj.type = promotion.promotionClass;
          promotionObj.proratedDiscount = proratedPrice.value;
          promotionArray.push(promotionObj);
        }
      });
      prorationObj.prorationData = promotionArray;
    }
    prorationObjArray.push(prorationObj);
  });

  finalPromotionObj.discountDetails = prorationObjArray;
  finalPromotionObj.shippingPromotionID = shipPromoId;
  finalPromotionObj.freeShippingOrderTotal = !empty(session.custom.freeShippingOrderTotal) ? session.custom.freeShippingOrderTotal : '';
  return JSON.stringify(finalPromotionObj, null, 4);
};

/**
 * Returns the product line item of the cart with a specific UUID.
 * @param {Object} basket - Demandware Basket object
 * @param {string} lineItemUUID - The UUID of the line item.
 * @returns {dw.order.ProductLineItem} The product line item or null if not found.
 */
base.getProductLineItemByUUID = function (basket, lineItemUUID) {
  return collections.find(basket.productLineItems, function (pli) {
    return pli.UUID === lineItemUUID;
  });
};

base.mergeCart = function (req) {
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentOrNewBasket();
  var storedBasket = BasketMgr.getStoredBasket();
  var isCartMerged = false;

  if (currentBasket && storedBasket) {
    var preferences = require('*/cartridge/config/preferences');
    var productLineItemItr = storedBasket.getAllProductLineItems().iterator();
    var couponLineItemItr = storedBasket.getCouponLineItems().iterator();
    var currentBasketItemCount = currentBasket.productLineItems ? currentBasket.productLineItems.length : 0;

    while (productLineItemItr.hasNext()) {
      var pli = productLineItemItr.next();
      if (!pli.bundledProductLineItem && !pli.optionProductLineItem) {
        // return if basket limit is reached
        if (preferences.basketItemLimit && preferences.basketItemLimit <= currentBasketItemCount) {
          req.session.privacyCache.set('basketLimitReached', true);
          break;
        }
        // Get the Child Product and option items
        var childProducts = [];
        var optionProducts = [];
        if (pli.bundledProductLineItem.length > 0) {
          for (var i = 0; i < pli.bundledProductLineItem.length; i++) {
            var bundleObj = {
              pid: pli.bundledProductLineItem[i].productID,
              quantity: null
            };
            childProducts.push(bundleObj);
          }
        }

        if (pli.optionProductLineItems.length > 0) {
          for (var j = 0; j < pli.optionProductLineItems.length; j++) {
            var optionObj = {
              optionId: pli.optionProductLineItems[j].optionID,
              selectedValueId: pli.optionProductLineItems[j].optionValueID
            };
            optionProducts.push(optionObj);
          }
        }

        if (!pli.bonusProductLineItem) {
          var result = base.addProductToCart(currentBasket, pli.productID, pli.quantityValue, childProducts, optionProducts);
          if (!result.error) {
            isCartMerged = true;
          }
        }
        currentBasketItemCount += 1;
      }

      // Also Add Coupons If Available in the Stored Basket, but merge status will depends only on Line Items Merge.
      Transaction.wrap(function () {
        while (couponLineItemItr.hasNext()) {
          var couponItem = couponLineItemItr.next();
          if (couponItem) {
            try {
              currentBasket.createCouponLineItem(couponItem, true);
            } catch (e) {
              // Do Nothing if Couldn't added.
            }
          }
        }
      });
    }
  }

  var preferences = require('*/cartridge/config/preferences');
  if (preferences.isABTestingOn) {
    try {
      cartAndProductListSyncOnLogin();
    } catch (e) {
      Logger.debug('ITEM NOT FOUND IN SFCC');
    }
  }

  return isCartMerged;
};

function cartAndProductListSyncOnLogin() {
  var BasketMgr = require('dw/order/BasketMgr');
  var ProductListMgr = require('dw/customer/ProductListMgr');
  var ProductList = require('dw/customer/ProductList');
  var HashSet = require('dw/util/HashSet');
  Logger.debug('mergeLegacyCart FOUND');
  var tempListToAddToCart = new HashSet();
  var tempListToAddToList = new HashSet();
  var finalListToAddToCart = new HashSet();
  var finalListToAddToList = new HashSet();

  var currentBasket = BasketMgr.getCurrentOrNewBasket();

  if (currentBasket) {
    var productLineItemItr = currentBasket.getProductLineItems().iterator();
    Logger.debug('current basket FOUND');
    while (productLineItemItr.hasNext()) {
      var item = productLineItemItr.next();
      if (!item.bonusProductLineItem) {
        tempListToAddToList.add(item.productID);
      }
    }
    Logger.debug('tempListToAddToList -->' + tempListToAddToList.getLength());
  }

  if (session.custom.cartProductListId != null) {
    var productList = ProductListMgr.getProductList(session.custom.cartProductListId);
    Logger.debug('cartProductListId NOT EMPTY');
    var productLineItemItr = productList.getItems().iterator();
    Logger.debug('productList FOUND');
    while (productLineItemItr.hasNext()) {
      var item = productLineItemItr.next();
      //Fix for SFSX-949 //:TODO quantity is not being accounted for in this entire logic, the whole method needs to be rewritten.
      //Commenting the below if-block. This is an instance of dw.customer.ProductListItem not dw.order.ProductLineItem
      //dw.order.ProductLineItem has a property : bonusProductLineItem to check whether a line item is a bonus line item, the
      //dw.customer.ProductListItem has no such property, this was breaking the cart merge flow.
      //if (!item.bonusProductLineItem) {
      if (item != null && !empty(item)) {
        tempListToAddToCart.add(item.productID);
      }
    }
    Logger.debug('tempListToAddToCart -->' + tempListToAddToCart.getLength());
  }

  finalListToAddToCart.addAll(tempListToAddToCart);
  finalListToAddToCart.removeAll(tempListToAddToList);
  Logger.debug('finalListToAddToCart -->' + finalListToAddToCart.getLength());

  finalListToAddToList.addAll(tempListToAddToList);
  finalListToAddToList.removeAll(tempListToAddToCart);
  Logger.debug('finalListToAddToList -->' + finalListToAddToList.getLength());

  if (tempListToAddToCart.getLength() > 0) {
    var childProds = [];
    var optionProds = [];
    var productLineItemItr = tempListToAddToCart.iterator();
    while (productLineItemItr.hasNext()) {
      var item = productLineItemItr.next();
      var quantity = parseInt('1', 10);
      var result = base.addProductToCart(currentBasket, item.valueOf(), quantity, childProds, optionProds);
    }
  }

  if (finalListToAddToList.getLength() > 0) {
    var productLineItemItr = finalListToAddToList.iterator();
    var ProductMgr = require('dw/catalog/ProductMgr');

    while (productLineItemItr.hasNext()) {
      var item = productLineItemItr.next();
      if (!item.bonusProductLineItem) {
        productList.createProductItem(ProductMgr.getProduct(item.valueOf()));
      }
    }
  }
}

function addCartItemToProductList(product) {
  try {
    var preferences = require('*/cartridge/config/preferences');
    if (preferences.isABTestingOn) {
      var ProductListMgr = require('dw/customer/ProductListMgr');
      var ProductList = require('dw/customer/ProductList');
      var cartProductList;
      if (session.custom.cartProductListId == null) {
        Transaction.wrap(function () {
          cartProductList = ProductListMgr.createProductList(session.getCustomer(), ProductList.TYPE_CUSTOM_1);
          session.custom.cartProductListId = cartProductList.ID;
        });
        Logger.debug(' addCartItemToProductList NEW cartProductListId -->' + session.custom.cartProductListId);
      }
      if (session.custom.cartProductListId != null) {
        Logger.debug('addCartItemToProductList cartProductListId NOT EMPTY');
        cartProductList = ProductListMgr.getProductList(session.custom.cartProductListId);
        Logger.debug('addCartItemToProductList cartProductListId NOT EMPTY' + cartProductList);
        var itemIter = cartProductList.getItems().iterator();
        var existingItemInList;
        var existingInList = false;
        while (itemIter.hasNext()) {
          existingItemInList = itemIter.next();
          Logger.debug('itemIter ID' + existingItemInList.productID);
          if (existingItemInList.productID == product.ID) {
            existingInList = true;
            break;
          }
        }
        Logger.debug('addCartItemToProductList existingInList NOT EMPTY' + existingInList);

        if (!existingInList) {
          Logger.debug('addCartItemToProductList existingInList DOESNT HAVE ITEM' + existingInList);

          cartProductList.createProductItem(product);
        } else {
          Logger.debug('addCartItemToProductList existingInList HAS ITEM' + existingInList);
        }
      }
    }
  } catch (e) {
    Logger.debug('ERROR IN CART SYNC');
  }
}

base.removeCartItemFromProductList = function (product) {
  try {
    var preferences = require('*/cartridge/config/preferences');
    if (preferences.isABTestingOn) {
      var ProductListMgr = require('dw/customer/ProductListMgr');
      var ProductList = require('dw/customer/ProductList');
      var cartProductList;
      if (session.custom.cartProductListId == null) {
        Transaction.wrap(function () {
          cartProductList = ProductListMgr.createProductList(session.getCustomer(), ProductList.TYPE_CUSTOM_1);
          session.custom.cartProductListId = cartProductList.ID;
        });
        Logger.debug(' removeCartItemFromProductList NEW cartProductListId -->' + session.custom.cartProductListId);
      }
      if (session.custom.cartProductListId != null) {
        Logger.debug('removeCartItemFromProductList cartProductListId NOT EMPTY' + product);
        cartProductList = ProductListMgr.getProductList(session.custom.cartProductListId);
        Logger.debug('removeCartItemFromProductList cartProductListId NOT EMPTY' + cartProductList);
        var itemIter = cartProductList.getItems().iterator();
        var existingItemInList;
        var existingInList = false;
        while (itemIter.hasNext()) {
          existingItemInList = itemIter.next();
          Logger.debug('itemIter ID' + existingItemInList.productID);
          if (existingItemInList.productID == product.ID) {
            existingInList = true;
            break;
          }
        }
        Logger.debug('removeCartItemFromProductList existingInList NOT EMPTY' + existingInList);

        if (!existingInList) {
          Logger.debug('removeCartItemFromProductList existingInList DOESNT HAVE ITEM' + existingInList);
        } else {
          Logger.debug('removeCartItemFromProductList existingInList HAS ITEM' + existingInList);
          cartProductList.removeItem(existingItemInList);
        }
      }
    }
  } catch (e) {
    Logger.debug('ERROR IN CART SYNC ');
  }
};
base.removeAllItemsFromProductList = function () {
  try {
    var preferences = require('*/cartridge/config/preferences');
    if (preferences.isABTestingOn) {
      var ProductListMgr = require('dw/customer/ProductListMgr');
      var ProductList = require('dw/customer/ProductList');
      var cartProductList;
      /* if (session.custom.cartProductListId == null) {
                Transaction.wrap(function () {
                    cartProductList = ProductListMgr.createProductList(session.getCustomer(), ProductList.TYPE_CUSTOM_1)
                    session.custom.cartProductListId = cartProductList.ID;
                });
                Logger.debug(" removeAllItemsFromProductList NEW cartProductListId -->" + session.custom.cartProductListId);
            } */
      if (session.custom.cartProductListId !== null) {
        Logger.debug('removeAllItemsFromProductList cartProductListId NOT EMPTY');
        cartProductList = ProductListMgr.getProductList(session.custom.cartProductListId);
        Logger.debug('removeAllItemsFromProductList cartProductListId NOT EMPTY' + cartProductList);
        Logger.debug('removeAllItemsFromProductList existingInList HAS ITEM' + existingInList);

        var itemIter = cartProductList.getItems().iterator();
        var existingItemInList;
        var existingInList = false;
        while (itemIter.hasNext()) {
          existingItemInList = itemIter.next();
          cartProductList.removeItem(existingItemInList);
          Logger.debug('removeAllItemsFromProductList Item removed');
        }
      }
    }
  } catch (e) {
    Logger.debug('ERROR IN CART SYNC ');
  }
};

base.addPromotrayCouponToBasket = function (req, currentBasket, couponCode, campaignBased) {
  var addCouponToBasketResult;
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var CouponStatusCodes = require('dw/campaign/CouponStatusCodes');

  try {
    addCouponToBasketResult = Transaction.wrap(function () {
      return currentBasket.createCouponLineItem(couponCode, campaignBased);
    });
  } catch (e) {
    // for now, fix just existing coupon error status
    let coupons = currentBasket.couponLineItems;
    if (empty(req.session.privacyCache.get('appliedCoupons'))) {
      req.session.privacyCache.set('appliedCoupons', couponCode);
    } else {
      req.session.privacyCache.set('appliedCoupons', req.session.privacyCache.get('appliedCoupons') + '|' + couponCode);
    }
    collections.forEach(coupons, function (coupon) {
      if (couponCode.toUpperCase() === coupon.couponCode.toUpperCase()) {
        return {
          CouponStatus: CouponStatusCodes.COUPON_CODE_ALREADY_IN_BASKET
        };
      }
      return null;
    });
    return {
      CouponStatus: e.errorCode
    };
  }

  Transaction.wrap(function () {
    basketCalculationHelpers.calculateTotals(currentBasket);
  });

  if (empty(req.session.privacyCache.get('appliedCoupons'))) {
    req.session.privacyCache.set('appliedCoupons', couponCode);
  } else {
    req.session.privacyCache.set('appliedCoupons', req.session.privacyCache.get('appliedCoupons') + '|' + couponCode);
  }
  return addCouponToBasketResult.statusCode;
};

base.sortLineItems = function (items) {
  /* if (items) {
    	var sortItems = items.sort(function (item1, item2) {
    		return (item1.isBonusProductLineItem) ? 0 : (item2.position - item1.position);
    	});
    	return sortItems;
    }*/
  return items.reverse();
};

base.saveBonusLineItemInfo = function (cart, item) {
  Transaction.wrap(function () {
    // eslint-disable-next-line no-array-constructor
    var array = new Array();
    var dBonus = item.getQualifyingProductLineItemForBonusProduct() ? item.getQualifyingProductLineItemForBonusProduct().custom.dBonus : cart.custom.dBonus;
    if (dBonus) {
      dBonus.forEach(function (ite) {
        array.push(ite);
      });
    }
    array.push(item.productID);
    if (item.getQualifyingProductLineItemForBonusProduct()) {
      // eslint-disable-next-line no-param-reassign
      item.getQualifyingProductLineItemForBonusProduct().custom.dBonus = array;
    } else {
      // eslint-disable-next-line no-param-reassign
      cart.custom.dBonus = array;
    }
  });
};

base.selectBonusProduct = function (cart) {
  var bonusLineItems = cart ? cart.getBonusDiscountLineItems() : null;
  if (!bonusLineItems) {
    return null;
  }
  var URLUtils = require('dw/web/URLUtils');
  var selectBonusModel = {};
  selectBonusModel.title = {};
  selectBonusModel.bonusLineItems = [];
  collections.forEach(bonusLineItems, function (bonusLineItem) {
    var bLineItem = {};
    bLineItem.promotion = {};
    bLineItem.promotion.title = bonusLineItem.promotion && 'gwpTitle' in bonusLineItem.promotion.custom ? bonusLineItem.promotion.custom.gwpTitle : null;
    var promoMsg = bonusLineItem.promotion ? bonusLineItem.promotion.calloutMsg : null;
    if (bonusLineItem.bonusProductLineItems.length) {
      promoMsg =
        bonusLineItem.maxBonusItems === 1
          ? Resource.msg('info.bonus.freegift', 'cart', null)
          : Resource.msgf('info.bonus.choice.gift', 'cart', null, bonusLineItem.bonusProductLineItems.length, bonusLineItem.maxBonusItems);
    }
    bLineItem.promotion.promoMsg = promoMsg;
    bLineItem.uuid = bonusLineItem.UUID;
    bLineItem.pliuuid = bonusLineItem.custom.bonusProductLineItemUUID;
    bLineItem.url = URLUtils.url('Cart-EditBonusProduct', 'duuid', bonusLineItem.UUID).toString();
    bLineItem.maxpids = bonusLineItem.maxBonusItems;
    selectBonusModel.bonusLineItems.push(bLineItem);
  });
  selectBonusModel.title =
    bonusLineItems.length > 1 ? Resource.msg('button.bonus.select.giftoffers', 'cart', null) : Resource.msg('button.bonus.select.giftoffer', 'cart', null);
  return selectBonusModel;
};

base.isMultiChoiceBonusProduct = function (cart) {
  var bonusLineItems = cart ? cart.getBonusDiscountLineItems() : null;
  if (!bonusLineItems || bonusLineItems.length === 0) {
    return false;
  }
  return bonusLineItems.length > 1 || bonusLineItems[0].maxBonusItems > 1;
};

base.provideBonusDiscountLineItemtile = function (bonusDiscountLineItem, products) {
  return bonusDiscountLineItem.maxBonusItems === 1 || products.length === 1
    ? bonusDiscountLineItem.promotion.custom.gwpTitle
    : Resource.msgf('info.bonus.choice.title', 'cart', null, bonusDiscountLineItem.promotion.custom.gwpTitle);
};

base.provideBonusDiscountLineItemHeader = function (bonusDiscountLineItem, products) {
  var bonusPromotionCalloutMsg =
    bonusDiscountLineItem.promotion && bonusDiscountLineItem.promotion.calloutMsg ? bonusDiscountLineItem.promotion.calloutMsg.markup : '';
  return bonusDiscountLineItem.maxBonusItems === 1 || products.length === 1
    ? bonusPromotionCalloutMsg
    : Resource.msgf(
        'info.bonus.choice.header',
        'cart',
        null,
        products.length < bonusDiscountLineItem.maxBonusItems ? products.length : bonusDiscountLineItem.maxBonusItems
      );
};

base.provideButtonName = function (cart) {
  var bonusLineItems = cart ? cart.getBonusDiscountLineItems() : null;
  if (!bonusLineItems || !bonusLineItems.length) {
    return null;
  }
  var isMultiple = this.isMultiChoiceBonusProduct(cart);
  var button = {
    content1: isMultiple ? Resource.msg('button.bonus.select.multiple', 'cart', null) : Resource.msg('button.bonus.select', 'cart', null),
    content2: Resource.msg('button.bonus.select.select', 'cart', null)
  };
  var selectedProduct = 0;
  collections.forEach(bonusLineItems, function (bonusLineItem) {
    if (bonusLineItem.bonusProductLineItems && bonusLineItem.bonusProductLineItems.length) {
      selectedProduct += bonusLineItem.bonusProductLineItems.length;
    }
  });
  if (selectedProduct) {
    var giftMsg = Resource.msgf('info.bonus.selected.content11', 'cart', null, selectedProduct);
    if (selectedProduct > 1) {
      giftMsg = Resource.msgf('info.bonus.selected.content12', 'cart', null, selectedProduct);
    }
    button.content1 = Resource.msg('info.bonus.selected.content1', 'cart', null).concat(' ').concat(giftMsg);
    button.content2 = Resource.msg('info.bonus.selected.content2', 'cart', null);
  }
  return button;
};

base.clearCanadaTax = function (cart) {
  if (empty(cart)) {
    return;
  }
  try {
    var shipments = cart.shipments;
    collections.forEach(shipments, function (shipment) {
      if (!shipment.shippingAddress) {
        Transaction.wrap(function () {
          if ('vertex_taxation_imposition' in cart.custom && cart.custom.vertex_taxation_imposition) {
            // eslint-disable-next-line no-param-reassign
            delete cart.custom.vertex_taxation_imposition;
          }
        });
      }
    });
  } catch (err) {
    var Logger = require('dw/system/Logger');
    Logger.error('Error While deleting the Canada tax from custom attribute ' + err);
  }
};

base.getShipToHomeShipment = function (lineItemCtnr) {
  var shipments = lineItemCtnr.getShipments();
  var shipToHomeShipment = null;
  collections.forEach(shipments, function (shipment) {
    if (!('shipmentType' in shipment.custom && shipment.custom.shipmentType === 'instore')) {
      shipToHomeShipment = shipment;
    }
  });
  if (!shipToHomeShipment && !('shipmentType' in lineItemCtnr.defaultShipment.custom && lineItemCtnr.defaultShipment.custom.shipmentType === 'instore')) {
    shipToHomeShipment = lineItemCtnr.defaultShipment;
  }
  return shipToHomeShipment;
};

base.hasStoreProductsInBasket = function (lineItemCtnr) {
  var productLineItems = lineItemCtnr.getProductLineItems();
  var result = {};
  result.hasBOPISItems = false;

  for (var i = 0; i < productLineItems.length; i++) {
    var pli = productLineItems[i];
    if ('fromStoreId' in pli.custom && pli.custom.fromStoreId) {
      result.hasBOPISItems = true;
    } else {
      result.shipment = pli.getShipment();
      // to fetch only ship to home shipment based on the product
    }
  }
  return result;
};

base.productAvailabilityStatus = function (lineItemCtnr, soldOutItems) {
  var productLineItems = lineItemCtnr.items;
  var hasOOSProducts = false;
  for (var i = 0; i < productLineItems.length; i++) {
    var pli = productLineItems[i];
    if (!pli.available || (soldOutItems && soldOutItems.length > 0 && soldOutItems.indexOf(pli.id) != -1)) {
      hasOOSProducts = true;
      break;
    }
  }
  return hasOOSProducts;
};

base.resetPLISPurchaseLimit = function (basket) {
  var HashMap = require('dw/util/HashMap');
  var purchaseLimitedSkuMap = new HashMap();
  if (basket) {
    var productLineItems = basket.getAllProductLineItems();
    collections.forEach(productLineItems, function (pli) {
      if (pli && pli.product && pli.product.custom && 'purchaseLimit' in pli.product.custom && pli.product.custom.purchaseLimit) {
        var isInPurchaseLimit = productHelper.isInPurchaselimit(pli.product.custom.purchaseLimit, pli.getQuantityValue());
        if (!isInPurchaseLimit) {
          let errorMsg = Resource.msgf('label.notinpurchaselimit', 'common', null, pli.product.custom.purchaseLimit);
          purchaseLimitedSkuMap.put(pli.product.ID, errorMsg);
          Transaction.wrap(function () {
            pli.setQuantityValue(Number(pli.product.custom.purchaseLimit));
          });
        }
      }
    });
  }
  return purchaseLimitedSkuMap;
};

module.exports = base;
//exports.removeCartItemFromProductList = removeCartItemFromProductList;
