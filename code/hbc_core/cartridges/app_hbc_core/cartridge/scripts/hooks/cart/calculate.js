/* eslint-disable */
'use strict';

/**
 * @module calculate.js
 *
 * This javascript file implements methods (via Common.js exports) that are needed by
 * the new (smaller) CalculateCart.ds script file.  This allows OCAPI calls to reference
 * these tools via the OCAPI 'hook' mechanism
 *
 */
var HashMap = require('dw/util/HashMap');
var PromotionMgr = require('dw/campaign/PromotionMgr');
var ShippingMgr = require('dw/order/ShippingMgr');
var ShippingLocation = require('dw/order/ShippingLocation');
var TaxMgr = require('dw/order/TaxMgr');
var Logger = require('dw/system/Logger');
var Status = require('dw/system/Status');
var HookMgr = require('dw/system/HookMgr');
var Transaction = require('dw/system/Transaction');
var Money = require('dw/value/Money');
var collections = require('*/cartridge/scripts/util/collections');
var preorderHelper = require('*/cartridge/scripts/helpers/preorderHelper');

var Vertex = require('*/cartridge/scripts/Vertex');
var Helper = require('*/cartridge/scripts/helper/Helper');
var preferences = require('*/cartridge/config/preferences');

/**
 * @function calculate
 *
 * calculate is the arching logic for computing the value of a basket.  It makes
 * calls into cart/calculate.js and enables both SG and OCAPI applications to share
 * the same cart calculation logic.
 *
 * @param {object} basket The basket to be calculated
 */
exports.calculate = function (basket) {
  /*
        ShopRunner
    */
  let shopRunnerEnabled = require('*/cartridge/scripts/helpers/shopRunnerHelpers').checkSRExpressCheckoutEligibility(basket);

  if (shopRunnerEnabled) {
    var SRHelper = require('*/cartridge/scripts/SRHelper');
    SRHelper.CheckGroundFreePromo();
  }
  // remove giftoptions custom adjustments before
  // all other adjustements are assigned
  manageGiftOptionsPriceAdjustment(basket, 'remove');

  // ===================================================
  // =====   CALCULATE PRODUCT LINE ITEM PRICES    =====
  // ===================================================

  calculateProductPrices(basket);

  // ===================================================
  // =====    CALCULATE GIFT CERTIFICATE PRICES    =====
  // ===================================================

  calculateGiftCertificatePrices(basket);

  // ===================================================
  // =====   Note: Promotions must be applied      =====
  // =====   after the tax calculation for         =====
  // =====   storefronts based on GROSS prices     =====
  // ===================================================

  // ===================================================
  // =====   APPLY PROMOTION DISCOUNTS			 =====
  // =====   Apply product and order promotions.   =====
  // =====   Must be done before shipping 		 =====
  // =====   calculation. 					     =====
  // ===================================================

  PromotionMgr.applyDiscounts(basket);

  // ===================================================
  // =====        CALCULATE SHIPPING COSTS         =====
  // ===================================================

  // apply product specific shipping costs
  // and calculate total shipping costs
  HookMgr.callHook('dw.order.calculateShipping', 'calculateShipping', basket);

  // ===================================================
  // =====   APPLY PROMOTION DISCOUNTS			 =====
  // =====   Apply product and order and 			 =====
  // =====   shipping promotions.                  =====
  // ===================================================

  // Setting up a session variable to activate a customer group, which will control the Shipping Promotion (based on Order total and not Shipment Value)

  var cartTotalIncludingDisc = basket.getAdjustedMerchandizeTotalPrice(true);
  session.custom.freeShippingOrderTotal = cartTotalIncludingDisc.valueOrNull;

  PromotionMgr.applyDiscounts(basket);

  // Remove already deleted bonus product from cart
  Helper.clearBonusProduct(basket);

  // since we might have bonus product line items, we need to
  // reset product prices
  calculateProductPrices(basket);

  // ===================================================
  // =====        CALCULATE SHIPPING COSTS         =====
  // ===================================================
  // apply product specific shipping costs
  // and calculate total shipping costs
  HookMgr.callHook('dw.order.calculateShipping', 'calculateShipping', basket);

  // Update CSR Price Override adjustment
  //csrPriceAdjustmentUpdate(basket);

  // add back the giftoptions adjustment if applicable
  manageGiftOptionsPriceAdjustment(basket, 'add');
  // ===================================================
  // =====         CALCULATE TAX                   =====
  // ===================================================
  var TaxUtils = require('*/cartridge/scripts/helper/TaxUtils');
  if (preferences.enableVertexHasing) {
    // Calculate Basket Hash only if Hashing is enabled.
    TaxUtils.CartStateHash(basket);
  }

  if (
    !empty(session.privacy.skipRealTimeTaxes) &&
    session.privacy.skipRealTimeTaxes == true &&
    !empty(basket.getTotalGrossPrice()) &&
    basket.getTotalGrossPrice().available
  ) {
    Logger.debug('Skip real time tax call, basket did not change');
    TaxUtils.updateTaxLineItems(basket);
  } else {
    //Call the service
    if (Vertex.isEnabled()) {
      // Reset the existing tax
      resetTax(basket);
      if (!!basket.defaultShipment && !!basket.defaultShipment.shippingAddress) {
        Vertex.CalculateTax('Quotation', basket);
        // Update the Cart Hash state only if Hashing is enabled.
        if (preferences.enableVertexHasing) {
          TaxUtils.CartStateHash(basket);
        }
        taxProrationAndUpdate(basket);
      } else {
        Helper.prepareCart(basket);
      }
      //HookMgr.callHook('dw.order.calculateTax', 'calculateTax', basket);
    } else {
      HookMgr.callHook('dw.order.calculateTax', 'calculateTax', basket);
    }
  }

  // ===================================================
  // =====        UPDATE INVENTORY STATUS          =====
  // ===================================================
  try {
    preorderHelper.updateInventoryStatus(basket);
  } catch (e) {
    // blank on purpose
  }

  // ===================================================
  // =====         CALCULATE BASKET TOTALS         =====
  // ===================================================

  basket.updateTotals();

  // Call basket update hook for ocapi requests
  HookMgr.callHook('app.ocapiext.shop.basket.updateBasketforOCAPI', 'updateBasketforOCAPI', basket);

  // ===================================================
  // =====            DONE                         =====
  // ===================================================

  return new Status(Status.OK);
};

/**
 * @function calculateProductPrices
 *
 * Calculates product prices based on line item quantities. Set calculates prices
 * on the product line items.  This updates the basket and returns nothing
 *
 * @param {object} basket The basket containing the elements to be computed
 */
function calculateProductPrices(basket) {
  // get total quantities for all products contained in the basket
  var productQuantities = basket.getProductQuantities();
  var productQuantitiesIt = productQuantities.keySet().iterator();

  // get product prices for the accumulated product quantities
  var productPrices = new HashMap();

  while (productQuantitiesIt.hasNext()) {
    var prod = productQuantitiesIt.next();
    var quantity = productQuantities.get(prod);
    productPrices.put(prod, prod.priceModel.getPrice(quantity));
  }

  // iterate all product line items of the basket and set prices
  var productLineItems = basket.getAllProductLineItems().iterator();
  while (productLineItems.hasNext()) {
    var productLineItem = productLineItems.next();

    // handle non-catalog products
    if (!productLineItem.catalogProduct) {
      productLineItem.setPriceValue(productLineItem.basePrice.valueOrNull);
      continue;
    }

    var product = productLineItem.product;

    // handle option line items
    if (productLineItem.optionProductLineItem) {
      // for bonus option line items, we do not update the price
      // the price is set to 0.0 by the promotion engine
      if (!productLineItem.bonusProductLineItem) {
        productLineItem.updateOptionPrice();
      }
      // handle bundle line items, but only if they're not a bonus
    } else if (productLineItem.bundledProductLineItem) {
      // no price is set for bundled product line items
      // handle bonus line items
      // the promotion engine set the price of a bonus product to 0.0
      // we update this price here to the actual product price just to
      // provide the total customer savings in the storefront
      // we have to update the product price as well as the bonus adjustment
    } else if (productLineItem.bonusProductLineItem && product !== null) {
      var price = product.priceModel.price;
      var adjustedPrice = productLineItem.adjustedPrice;
      // TO handle bonus product without price is defined we are setting price to 0 to avoid subtotoal price NA
      productLineItem.setPriceValue(price.available ? price.valueOrNull : 0);
      // get the product quantity
      var quantity2 = productLineItem.quantity;
      // we assume that a bonus line item has only one price adjustment
      var adjustments = productLineItem.priceAdjustments;
      if (!adjustments.isEmpty()) {
        var adjustment = adjustments.iterator().next();
        var adjustmentPrice = price.multiply(quantity2.value).multiply(-1.0).add(adjustedPrice);
        adjustment.setPriceValue(adjustmentPrice.valueOrNull);
      }

      // set the product price. Updates the 'basePrice' of the product line item,
      // and either the 'netPrice' or the 'grossPrice' based on the current taxation
      // policy

      // handle product line items unrelated to product
    } else if (product === null) {
      productLineItem.setPriceValue(null);
      // handle normal product line items
    } else {
      productLineItem.setPriceValue(productPrices.get(product).valueOrNull);
    }
  }
}

function createShippingLineItemImposition(shipment) {
  Transaction.wrap(function () {
    try {
      var shippingImpositionMap = new HashMap();
      var shippingLineItem = shipment.shippingLineItems[0];
      var shippingPriceforProration = shippingLineItem.getAdjustedPrice().getDecimalValue();
      var shipmentTotal = shipment.getAdjustedMerchandizeTotalPrice().getDecimalValue();
      var productLineItems = shipment.getProductLineItems();

      if (productLineItems.length > 0) {
        var productLineItemsItr = productLineItems.iterator();
        while (productLineItemsItr.hasNext()) {
          var lineItem = productLineItemsItr.next();
          if ('vertex_taxation_details' in lineItem.custom && lineItem.custom.vertex_taxation_details) {
            var impositionJSON = JSON.parse(lineItem.custom.vertex_taxation_details);
            if (impositionJSON && impositionJSON.length > 0) {
              for (var i = 0; i < impositionJSON.length; i++) {
                var taxRate = impositionJSON[i].effectiveRate;
                var impositionType = impositionJSON[i].imposition;
                var lineItemPrice = lineItem.getAdjustedPrice().getDecimalValue();
                var proratedShipping = (lineItemPrice / shipmentTotal) * shippingPriceforProration;
                var taxOnProratedShipping = proratedShipping * taxRate;
                if (isNaN(taxOnProratedShipping)) {
                  taxOnProratedShipping = 0.0;
                }
                if (shippingImpositionMap.containsKey(impositionType)) {
                  var calTax = shippingImpositionMap.get(impositionType);
                  shippingImpositionMap.put(impositionType, calTax + taxOnProratedShipping);
                } else {
                  shippingImpositionMap.put(impositionType, taxOnProratedShipping);
                }
              }
            }
          }
        }
      }

      if (shippingImpositionMap.length > 0) {
        var keySet = shippingImpositionMap.keySet();

        shippingLineItem.custom.vertex_taxation_details = {};

        var txDetails = [];
        var totalTax = new Number(0);
        var totalImpositionTax = new Number(0);
        for (var i = 0; i < keySet.length; i++) {
          var impJSON = {};
          var key = keySet[i];
          var value = shippingImpositionMap.get(key);
          impJSON.taxType = 'regular';
          impJSON.imposition = key;
          impJSON.calculatedTax = new Money(new Number(value), session.getCurrency().currencyCode).value;
          impJSON.effectiveRate = '0.00';
          txDetails.push(impJSON);
          totalTax += value;
          totalImpositionTax = totalImpositionTax + impJSON.calculatedTax;
        }
        // penny correction on the last impostion

        if (totalImpositionTax > 0 && totalTax > 0 && totalTax !== totalImpositionTax && totalImpositionTax > totalTax && keySet.length === txDetails.length) {
          let diff = (totalImpositionTax - totalTax) * 1;
          var taxOnLastImp = txDetails[keySet.length - 1].calculatedTax;
          txDetails[keySet.length - 1].calculatedTax = new Money(taxOnLastImp - diff, session.getCurrency().currencyCode).value;
          // the difference will always be in penny and will nevert be greater than a imposition. As the difference between the tax rounded with actual tax is in penny
        }
        shippingLineItem.custom.vertex_taxation_details = JSON.stringify(txDetails);
        shippingLineItem.updateTaxAmount(new Money(totalTax, session.getCurrency().currencyCode));
      }
    } catch (e) {
      Logger.error('Error While Prorating-->' + e);
    }
  });
}

function taxProrationAndUpdate(basket) {
  // Iterate all the Shipment and their product Line items to check if the Line item and shipments is taxable or not.
  var shipmentsItr = basket.shipments.iterator();
  while (shipmentsItr.hasNext()) {
    var shipment = shipmentsItr.next();
    var banner = preferences.hbcBanner;
    if (banner && banner == 'BAY') {
      createShippingLineItemImposition(shipment);
    }
  }

  try {
    // Iterate all line item to re-calculate
    var impositionMap = new HashMap();
    var ecoFee = new Money(0, session.getCurrency().currencyCode);
    var allLineItemItr = basket.allLineItems.iterator();
    while (allLineItemItr.hasNext()) {
      var lineItem = allLineItemItr.next();
      if ('vertex_taxation_details' in lineItem.custom && lineItem.custom.vertex_taxation_details) {
        var parseImposition = JSON.parse(lineItem.custom.vertex_taxation_details);
        if (parseImposition.length > 0) {
          for (var i = 0; i < parseImposition.length; i++) {
            var taxImposition = parseImposition[i];
            var calculatedTax = new Money(taxImposition.calculatedTax, session.getCurrency().currencyCode);
            if (impositionMap.containsKey(taxImposition.imposition)) {
              var oldTaxImposition = impositionMap.get(taxImposition.imposition);
              var newCalculatedTax = oldTaxImposition.add(calculatedTax);
              impositionMap.put(taxImposition.imposition, newCalculatedTax);
            } else {
              impositionMap.put(taxImposition.imposition, calculatedTax);
            }
          }
        }
      }
      // Also check if ECO fee is their for PLI.
      if ('ecoFee' in lineItem.custom && !empty(lineItem.custom.ecoFee)) {
        ecoFee = ecoFee.add(new Money(lineItem.custom.ecoFee, session.getCurrency().currencyCode));
      }
    }
  } catch (e) {
    Logger.error('Error during Tax Proration-->' + e);
  }

  // Update ECO Fee
  if (ecoFee > 0) {
    impositionMap.put('ECO', ecoFee);
  }

  if (impositionMap.length > 0) {
    var taxationImpositionTaxObj = Helper.getJsonFromMap(impositionMap);
    Transaction.wrap(function () {
      basket.custom.vertex_taxation_imposition = JSON.stringify(taxationImpositionTaxObj);
    });
  }
}

/**
 * @function resetTax
 *
 * Reset the tax for all the basket line items
 * and basket, shipping price adjustments.
 *
 * @param {dw.order.Basket} basket The basket containing the elements to be computed
 */
function resetTax(basket) {
  var allLineItems = basket.allLineItems;

  collections.forEach(allLineItems, function (li) {
    li.updateTax(0);
    if ('vertex_taxation_details' in li.custom && li.custom.vertex_taxation_details) {
      delete li.custom.vertex_taxation_details;
    }
  });

  // Reset line item level price adjustments tax.
  if (!basket.getPriceAdjustments().empty || !basket.getShippingPriceAdjustments().empty) {
    var basketPriceAdjustments = basket.getPriceAdjustments();
    collections.forEach(basketPriceAdjustments, function (basketPriceAdjustment) {
      basketPriceAdjustment.updateTax(0);
    });

    var basketShippingPriceAdjustments = basket.getShippingPriceAdjustments();
    collections.forEach(basketShippingPriceAdjustments, function (basketShippingPriceAdjustment) {
      basketShippingPriceAdjustment.updateTax(0);
    });
  }

  if ('vertex_taxation_imposition' in basket.custom && basket.custom.vertex_taxation_imposition) {
    delete basket.custom.vertex_taxation_imposition;
  }
}

/**
 * @function calculateGiftCertificates
 *
 * Function sets either the net or gross price attribute of all gift certificate
 * line items of the basket by using the gift certificate base price. It updates the basket in place.
 *
 * @param {object} basket The basket containing the gift certificates
 */
function calculateGiftCertificatePrices(basket) {
  var giftCertificates = basket.getGiftCertificateLineItems().iterator();
  while (giftCertificates.hasNext()) {
    var giftCertificate = giftCertificates.next();
    giftCertificate.setPriceValue(giftCertificate.basePrice.valueOrNull);
  }
}

exports.calculateShipping = function (basket) {
  ShippingMgr.applyShippingCost(basket);
  var shipments = basket.shipments;

  // Logic to provide free shipping to a GWP bonus product, if it lands in a seperate shipment (Other items in the cart are Pick-Up in Store only)
  if (!empty(shipments) && shipments.length > 1) {
    collections.forEach(shipments, function (shipment) {
      if (shipment.adjustedMerchandizeTotalPrice.value == 0) {
        var bonusShipmentLineItems = shipment.shippingLineItems;
        collections.forEach(bonusShipmentLineItems, function (bonusShipmentLineItem) {
          bonusShipmentLineItem.setPriceValue(0);
        });
      }
    });
  }

  if (basket.adjustedMerchandizeTotalPrice.value == 0) {
    collections.forEach(shipments, function (shipment) {
      var shipmentLineItems = shipment.shippingLineItems;
      collections.forEach(shipmentLineItems, function (shipmentLineItem) {
        shipmentLineItem.setPriceValue(0);
      });
    });
  }
  return new Status(Status.OK);
};

/**
 * @function calculateTax <p>
 *
 * Determines tax rates for all line items of the basket. Uses the shipping addresses
 * associated with the basket shipments to determine the appropriate tax jurisdiction.
 * Uses the tax class assigned to products and shipping methods to lookup tax rates. <p>
 *
 * Sets the tax-related fields of the line items. <p>
 *
 * Handles gift certificates, which aren't taxable. <p>
 *
 * Note that the function implements a fallback to the default tax jurisdiction
 * if no other jurisdiction matches the specified shipping location/shipping address.<p>
 *
 * Note that the function implements a fallback to the default tax class if a
 * product or a shipping method does explicitly define a tax class.
 *
 * @param {dw.order.Basket} basket The basket containing the elements for which taxes need to be calculated
 */
exports.calculateTax = function (basket) {
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

  var taxes = basketCalculationHelpers.calculateTaxes(basket);

  // convert taxes into hashmap for performance.
  var taxesMap = {};

  taxes.taxes.forEach(function (item) {
    taxesMap[item.uuid] = { value: item.value, amount: item.amount };
  });

  var lineItems = basket.getAllLineItems();

  var totalShippingGrossPrice = 0;
  var totalShippingNetPrice = 0;

  var containsGlobalPriceAdjustments = false;

  // update taxes for all line items
  collections.forEach(lineItems, function (lineItem) {
    var tax = taxesMap[lineItem.UUID];

    if (tax) {
      if (tax.amount) {
        lineItem.updateTaxAmoun(tax.value);
        if (lineItem instanceof dw.order.ShippingLineItem) {
          totalShippingGrossPrice += lineItem.getAdjustedGrossPrice();
          totalShippingNetPrice += lineItem.getAdjustedNetPrice();
        }
      } else {
        lineItem.updateTax(tax.value);
      }
    } else {
      if (lineItem.taxClassID === TaxMgr.customRateTaxClassID) {
        // do not touch tax rate for fix rate items
        lineItem.updateTax(lineItem.taxRate);
      } else {
        // otherwise reset taxes to null
        lineItem.updateTax(null);
      }
    }
  });

  // besides shipment line items, we need to calculate tax for possible order-level price adjustments
  // this includes order-level shipping price adjustments
  if (!basket.getPriceAdjustments().empty || !basket.getShippingPriceAdjustments().empty) {
    if (
      collections.first(basket.getPriceAdjustments(), function (priceAdjustment) {
        return taxesMap[priceAdjusmtnet.UUID] === null;
      }) ||
      collections.first(basket.getShippingPriceAdjustments(), function (shippingPriceAdjustment) {
        return taxesMap[shippingPriceAdjustment.UUID] === null;
      })
    ) {
      // tax hook didn't provide taxes for global price adjustment, we need to calculate them ourselves.
      // calculate a mix tax rate from
      var basketPriceAdjustmentsTaxRate =
        (basket.getMerchandizeTotalGrossPrice().value + basket.getShippingTotalGrossPrice().value) /
          (basket.getMerchandizeTotalNetPrice().value + basket.getShippingTotalNetPrice()) -
        1;

      var basketPriceAdjustments = basket.getPriceAdjustments();
      collections.forEach(basketPriceAdjustments, function (basketPriceAdjustment) {
        basketPriceAdjustment.updateTax(basketPriceAdjustmentsTaxRate);
      });

      var basketShippingPriceAdjustments = basket.getShippingPriceAdjustments();
      collections.forEach(basketShippingPriceAdjustments, function (basketShippingPriceAdjustment) {
        basketShippingPriceAdjustment.updateTax(totalShippingGrossPrice / totalShippingNetPrice - 1);
      });
    }
  }

  // if hook returned custom properties attach them to the order model
  if (taxes.custom) {
    Object.keys(taxes.custom).forEach(function (key) {
      basket.custom[key] = taxes.custom[key];
    });
  }

  return new Status(Status.OK);
};

function csrPriceAdjustmentUpdate(basket) {
  if (session.isUserAuthenticated()) {
    // iterate all product line items of the basket and set prices
    var productLineItems = basket.getAllProductLineItems().iterator();
    while (productLineItems.hasNext()) {
      var productLineItem = productLineItems.next();

      var adjustments = productLineItem.priceAdjustments.iterator();
      while (adjustments.hasNext()) {
        var adjustment = adjustments.next();
        if ('isCSRAdjustment' in adjustment.custom && adjustment.custom.isCSRAdjustment) {
          // Multiply the Actual adjustement with the Line Item Qty
          var newAdjustedPricePrice = adjustment.getPriceValue() * productLineItem.quantityValue;
          adjustment.setPriceValue(newAdjustedPricePrice);
        }
      }
    }
  }
}

/**
 * @function manageGiftOptionsPriceAdjustment
 *
 * Function creates a custom LineIteCntr adjustment for the GiftOptions
 *
 * @param {object} basket The basket
 * @param {string} action remove or add
 */
function manageGiftOptionsPriceAdjustment(basket, action) {
  var AmountDiscount = require('dw/campaign/AmountDiscount');
  var defaultShipment = basket ? basket.defaultShipment : null;
  var priceAdjusted = false;
  var basketSubtotal = basket.getMerchandizeTotalNetPrice();
  var giftOptionsPA = basket.getPriceAdjustmentByPromotionID('GiftOptions');
  if (action === 'remove') {
    if (giftOptionsPA) {
      basket.removePriceAdjustment(giftOptionsPA);
      priceAdjusted = true;
    }
  } else if (
    action === 'add' &&
    defaultShipment &&
    defaultShipment.custom.hasOwnProperty('giftWrapType') &&
    defaultShipment.custom.giftWrapType === 'giftpack' &&
    !isNaN(preferences.giftWrapAmount) &&
    !isNaN(preferences.freeGiftWrapThreshold) &&
    basketSubtotal.available &&
    basketSubtotal.value < preferences.freeGiftWrapThreshold &&
    !giftOptionsPA &&
    !session.custom.csrGiftWrap
  ) {
    giftOptionsPA = basket.createPriceAdjustment('GiftOptions');
    giftOptionsPA.setPriceValue(preferences.giftWrapAmount);
    giftOptionsPA.updateTax(0);
    priceAdjusted = true;
  }
  return priceAdjusted;
}
