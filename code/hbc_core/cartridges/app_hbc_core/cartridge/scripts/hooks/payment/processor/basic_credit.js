'use strict';
var base = module.superModule;

var collections = require('*/cartridge/scripts/util/collections');

var PaymentInstrument = require('dw/order/PaymentInstrument');
var PaymentMgr = require('dw/order/PaymentMgr');
var PaymentStatusCodes = require('dw/order/PaymentStatusCodes');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var Site = require('dw/system/Site');
var TokenExFacade = require('*/cartridge/scripts/services/TokenExFacade');
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');

/**
 * Creates a token. This should be replaced by utilizing a tokenization provider
 * @param {Object} encryptedCard - creating encrypted card
 * @returns {string} a token
 */
base.createToken = function (encryptedCard) {
  var token;
  if ('enableTokenEx' in Site.current.preferences.custom && Site.current.preferences.custom.enableTokenEx === true) {
    token = TokenExFacade.tokenizeEncryptedCard(encryptedCard);
  } else {
    token = {
      error: false,
      success: true,
      token: Math.random().toString(36).substr(2),
      tokenRetry: false,
      tokenEx: false
    };
  }
  return token;
};

/**
 * Verifies that entered credit card information is a valid card. If the information is valid a
 * credit card payment instrument is created
 * @param {dw.order.Basket} basket Current users's basket
 * @param {Object} paymentInformation - the payment information
 * @return {Object} returns an error object
 */
base.Handle = function (basket, paymentInformation) {
  var currentBasket = basket;
  var cardErrors = {};
  var cardNumber = paymentInformation.cardNumber.value;
  var cardSecurityCode = paymentInformation.securityCode.value;
  var expirationMonth = paymentInformation.expirationMonth.value;
  var expirationYear = paymentInformation.expirationYear.value;
  var creditCardToken = paymentInformation.creditCardToken;
  var serverErrors = [];
  var creditCardStatus;
  var tokenObj;
  tokenObj = base.createToken(cardNumber);
  var cardType = paymentInformation.cardType.value;
  var cardOwnerName = paymentInformation.cardOwner.value;
  var paymentCard = PaymentMgr.getPaymentCard(cardType);

  if (!paymentInformation.creditCardToken) {
    // Generate the Card Token Based on Encrypted data and use that token for LUHN validations.
    // tokenObj = base.createToken(cardNumber);
    if (paymentCard && tokenObj.success === true) {
      if (!tokenObj.tokenRetry && tokenObj.token) {
        creditCardStatus = paymentCard.verify(expirationMonth, expirationYear, !tokenObj.tokenEx ? cardNumber : tokenObj.token, cardSecurityCode);
      } else if (tokenObj.tokenRetry) {
        creditCardStatus = {};
        creditCardStatus.error = false;
      } else if (tokenObj.success === true && !tokenObj.token) {
        cardErrors[paymentInformation.cardNumber.htmlName] = Resource.msg('error.invalid.card.number', 'creditCard', null);

        return {
          fieldErrors: [cardErrors],
          serverErrors: serverErrors,
          error: true
        };
      } else {
        // Stop Customer if Retry not enabled.
        serverErrors.push(Resource.msg('error.technical', 'checkout', null));
        return {
          fieldErrors: [cardErrors],
          serverErrors: serverErrors,
          error: true
        };
      }
    } else if (tokenObj && tokenObj.success === false) {
      // Stop Customer if Retry not enabled.
      serverErrors.push(Resource.msg('error.technical', 'checkout', null));
      return {
        fieldErrors: [cardErrors],
        serverErrors: serverErrors,
        error: true
      };
    } else {
      cardErrors[paymentInformation.cardNumber.htmlName] = Resource.msg('error.invalid.card.number', 'creditCard', null);

      return {
        fieldErrors: [cardErrors],
        serverErrors: serverErrors,
        error: true
      };
    }

    if (creditCardStatus.error) {
      collections.forEach(creditCardStatus.items, function (item) {
        switch (item.code) {
          case PaymentStatusCodes.CREDITCARD_INVALID_CARD_NUMBER:
            cardErrors[paymentInformation.cardNumber.htmlName] = Resource.msg('error.invalid.card.number', 'creditCard', null);
            break;

          case PaymentStatusCodes.CREDITCARD_INVALID_EXPIRATION_DATE:
            cardErrors[paymentInformation.expirationMonth.htmlName] = Resource.msg('error.expired.credit.card', 'creditCard', null);
            cardErrors[paymentInformation.expirationYear.htmlName] = Resource.msg('error.expired.credit.card', 'creditCard', null);
            break;

          case PaymentStatusCodes.CREDITCARD_INVALID_SECURITY_CODE:
            cardErrors[paymentInformation.securityCode.htmlName] = Resource.msg('error.invalid.security.code', 'creditCard', null);
            break;
          default:
            serverErrors.push(Resource.msg('error.card.information.error', 'creditCard', null));
        }
      });

      return {
        fieldErrors: [cardErrors],
        serverErrors: serverErrors,
        error: true
      };
    }
  }

  Transaction.wrap(function () {
    var paymentInstruments = currentBasket.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);

    var paypalPI = currentBasket.getPaymentInstruments('PayPal');

    if (paypalPI) {
      collections.forEach(paypalPI, function (item) {
        currentBasket.removePaymentInstrument(item);
      });
    }

    collections.forEach(paymentInstruments, function (item) {
      currentBasket.removePaymentInstrument(item);
    });

    var paymentInstrument = currentBasket.createPaymentInstrument(PaymentInstrument.METHOD_CREDIT_CARD, currentBasket.totalGrossPrice);

    paymentInstrument.setCreditCardHolder(cardOwnerName);
    paymentInstrument.setCreditCardType(cardType);
    if (!tokenObj.tokenEx) {
      paymentInstrument.setCreditCardNumber(cardNumber);
    } else if (paymentInformation.creditCardToken) {
      paymentInstrument.setCreditCardNumber(paymentInformation.creditCardToken);
    } else if (tokenObj.token) {
      paymentInstrument.setCreditCardNumber(tokenObj.token);
    }
    paymentInstrument.setCreditCardExpirationMonth(expirationMonth);
    paymentInstrument.setCreditCardExpirationYear(expirationYear);
    if (paymentInformation.creditCardToken) {
      paymentInstrument.setCreditCardToken(paymentInformation.creditCardToken);
    } else if (tokenObj && tokenObj.token) {
      paymentInstrument.setCreditCardToken(tokenObj.token);
    }

    if (tokenObj && tokenObj.tokenRetry && !paymentInstrument.creditCardToken) {
      currentBasket.custom.tokenExRetry = true;
      currentBasket.custom.tokenExData = cardNumber; // This is encrypted data.
    }
  });

  return {
    fieldErrors: cardErrors,
    serverErrors: serverErrors,
    error: false,
    token: creditCardToken ? creditCardToken : tokenObj.token // eslint-disable-line
  };
};

/**
 * Authorizes a payment using a credit card. Customizations may use other processors and custom
 *      logic to authorize credit card payment.
 * @param {number} orderNumber - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current
 *      payment method
 * @param {Object} req - the request object
 * @return {Object} returns an error object
 */
base.Authorize = function (orderNumber, paymentInstrument, paymentProcessor, req) {
  var serverErrors = [];
  var fieldErrors = {};
  var error = false;

  var OrderMgr = require('dw/order/OrderMgr');
  var order = OrderMgr.getOrder(orderNumber);

  try {
    Transaction.wrap(function () {
      paymentInstrument.paymentTransaction.setTransactionID(orderNumber);
      paymentInstrument.paymentTransaction.setPaymentProcessor(paymentProcessor);
    });
    COHelpers.updateCustomerPI(req.currentCustomer.raw, order);
  } catch (e) {
    error = true;
    serverErrors.push(Resource.msg('error.technical', 'checkout', null));
  }

  return {
    fieldErrors: fieldErrors,
    serverErrors: serverErrors,
    error: error
  };
};

exports.modules = base;
