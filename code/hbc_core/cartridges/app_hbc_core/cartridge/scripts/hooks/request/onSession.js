'use strict';

/**
 * The onSession hook is called for every new session in a site. This hook can be used for initializations,
 * like to prepare promotions or pricebooks based on source codes or affiliate information in
 * the initial URL. For performance reasons the hook function should be kept short.
 *
 */

var Status = require('dw/system/Status');
var Locale = require('dw/util/Locale');
var Site = require('dw/system/Site');
var Logger = require('dw/system/Logger');

/**
 * Gets the device type of the current user.
 * @return {string} the device type (desktop, mobile or tablet)
 */
function getDeviceType() {
  var deviceType = 'desktop';
  var iPhoneDevice = 'iPhone';
  var iPadDevice = 'iPad';
  var androidDevice = 'Android'; // Mozilla/5.0 (Linux; U; Android 2.3.4; en-us; ADR6300 Build/GRJ22) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1

  var httpUserAgent = request.httpUserAgent;

  if (!httpUserAgent) {
    return;
  }

  if (httpUserAgent.indexOf(iPhoneDevice) > -1) {
    deviceType = 'mobile';
  } else if (httpUserAgent.indexOf(androidDevice) > -1) {
    if (httpUserAgent.toLowerCase().indexOf('mobile') > -1) {
      deviceType = 'mobile';
    }
  } else if (httpUserAgent.indexOf(iPadDevice) > -1) {
    deviceType = 'tablet';
  }

  // eslint-disable-next-line consistent-return
  return deviceType;
}

function checkTestMode() {
  var customPreferences = Site.current.preferences.custom;

  if (customPreferences.masterTestMode === true) {
    if (customPreferences.orderCreateBatchTestMode === true && request.httpParameterMap.orderCreateBatch.booleanValue === true) {
      session.custom.orderCreateBatchTest = true;
    } else {
      session.custom.orderCreateBatchTest = false;
    }

    Logger.info('TSYS Testing: session.custom.tsysPreview: ' + (session && session.custom ? session.custom.tsysPreview : 'No session'));
    if (customPreferences.tsysPreviewTestMode === true && request.httpParameterMap.tsysPreview.booleanValue === true) {
      Logger.info('**** TSYS Testing Enabled ****');
      session.custom.tsysPreview = true;
    } else {
      session.custom.tsysPreview = false;
    }
  }
}

/**
 * Saks mobile app provider injects FABUILD/#.#.# in their UserAgent string.
 * @return {boolean}
 */
function identifyMobileAppUser() {
  var customPreferences = Site.current.preferences.custom;
  var isMobileAppUser = false;
  var appIdentifier = 'FABUILD';

  var httpUserAgent = request.httpUserAgent;

  if (!httpUserAgent) {
    return isMobileAppUser;
  }

  if (httpUserAgent.indexOf(appIdentifier) > -1) {
    isMobileAppUser = true;
  }

  // eslint-disable-next-line consistent-return
  return isMobileAppUser;
}

/**
 * EnteredBy value for OMS Create Order XML
 *   Web  (desktop website users)
 *   MWeb (mobile website users)
 *   MApp (mobile app users)
 *
 * @todo relocate EnteredBy commission tracking COMX/Salesfloor logic here?
 *
 * @return {boolean}
 */
function getEnteredBy(deviceType, isMobileAppUser) {
  var enteredBy = 'Web';

  if (deviceType == 'mobile') {
    enteredBy = 'MWeb';
  }

  if (isMobileAppUser) {
    enteredBy = 'MApp';
  }

  return enteredBy;
}

/**
 * The onSession hook function
 *
 * @returns {dw/system/Status} status - return status
 */
exports.onSession = function () {
  session.custom.device = getDeviceType();
  session.custom.mobileAppUser = identifyMobileAppUser();
  session.custom.enteredBy = getEnteredBy(session.custom.device, session.custom.mobileAppUser);
  var locale = !empty(request.locale) ? request.locale : 'default';
  session.custom.currentLang = !empty(locale) && locale !== 'default' ? Locale.getLocale(locale).language : 'en';
  checkTestMode();
  return new Status(Status.OK);
};
