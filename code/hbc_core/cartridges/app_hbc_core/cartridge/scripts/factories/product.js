'use strict';

var ProductMgr = require('dw/catalog/ProductMgr');
var PromotionMgr = require('dw/campaign/PromotionMgr');
var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
var productTile = require('*/cartridge/models/product/productTile');
var productAddRemoveWishlist = require('*/cartridge/models/product/productAddRemoveWishlist');
var wishlistProductTile = require('*/cartridge/models/product/wishlistProductTile');
var bonusProduct = require('*/cartridge/models/product/bonusProduct');
var fullProduct = require('*/cartridge/models/product/fullProduct');
var productSet = require('*/cartridge/models/product/productSet');
var productBundle = require('*/cartridge/models/product/productBundle');
var productLineItem = require('*/cartridge/models/productLineItem/productLineItem');
var bonusProductLineItem = require('*/cartridge/models/productLineItem/bonusProductLineItem');
var bundleProductLineItem = require('*/cartridge/models/productLineItem/bundleLineItem');
var orderLineItem = require('*/cartridge/models/productLineItem/orderLineItem');
var bonusOrderLineItem = require('*/cartridge/models/productLineItem/bonusOrderLineItem');
var bundleOrderLineItem = require('*/cartridge/models/productLineItem/bundleOrderLineItem');
var Logger = require('dw/system/Logger');

function removeItemsFromCart(pid) {
  var BasketMgr = require('dw/order/BasketMgr');
  var Transaction = require('dw/system/Transaction');
  var URLUtils = require('dw/web/URLUtils');
  var CartModel = require('*/cartridge/models/cart');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

  var currentBasket = BasketMgr.getCurrentBasket();

  if (!currentBasket) {
    return;
  }

  var isProductLineItemFound = false;
  var bonusProductsUUIDs = [];

  Transaction.wrap(function () {
    if (pid) {
      var productLineItems = currentBasket.getAllProductLineItems(pid);
      var mainProdItem;
      for (var i = 0; i < productLineItems.length; i++) {
        var item = productLineItems[i];
        if (item.UUID) {
          var shipmentToRemove = item.shipment;
          var fromStoreId = require('*/cartridge/models/productLineItem/decorators/fromStoreId');
          var product = {
            pid: pid
          };
          currentBasket.removeProductLineItem(item);
          var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
          cartHelper.removeCartItemFromProductList(item.product);

          if (shipmentToRemove.productLineItems.empty && !shipmentToRemove.default) {
            currentBasket.removeShipment(shipmentToRemove);
          }
          isProductLineItemFound = true;
          break;
        }
      }
    }
    if (isProductLineItemFound && currentBasket.getAllProductQuantities().size() === 0) {
      currentBasket.removeAllPaymentInstruments();
    }
    basketCalculationHelpers.calculateTotals(currentBasket);
  });
}

module.exports = {
  get: function (params) {
    var productId = params.pid;
    var product = Object.create(null);
    try {
      var apiProduct = ProductMgr.getProduct(productId);
      if (apiProduct === null) {
        removeItemsFromCart(productId);
        return;
      }
      var productType = productHelper.getProductType(apiProduct);
      var options = null;
      var promotions;

      switch (params.pview) {
        case 'addRemoveFromWishList':
          product = productAddRemoveWishlist(product, apiProduct, productType);
          break;
        case 'wishlistTile':
          product = wishlistProductTile(product, apiProduct, productType);
          break;
        case 'tile':
          options = productHelper.getConfig(apiProduct, params);
          product = productTile(product, apiProduct, options.productType, params.frpid, options);
          break;
        case 'bonusProductLineItem':
          promotions = PromotionMgr.activeCustomerPromotions.getProductPromotions(apiProduct);
          options = {
            promotions: promotions,
            quantity: params.quantity,
            variables: params.variables,
            lineItem: params.lineItem,
            productType: productType
          };

          switch (productType) {
            case 'bundle':
              // product = bundleProductLineItem(product, apiProduct, options, this);
              break;
            default:
              var variationsBundle = productHelper.getVariationModel(apiProduct, params.variables);
              if (variationsBundle) {
                apiProduct = variationsBundle.getSelectedVariant() || apiProduct; // eslint-disable-line
              }

              var optionModelBundle = apiProduct.optionModel;
              var optionLineItemsBundle = params.lineItem.optionProductLineItems;
              var currentOptionModelBundle = productHelper.getCurrentOptionModel(
                optionModelBundle,
                productHelper.getLineItemOptions(optionLineItemsBundle, productId)
              );
              var lineItemOptionsBundle = optionLineItemsBundle.length
                ? productHelper.getLineItemOptionNames(optionLineItemsBundle)
                : productHelper.getDefaultOptions(optionModelBundle, optionModelBundle.options);

              options.variationModel = variationsBundle;
              options.lineItemOptions = lineItemOptionsBundle;
              options.currentOptionModel = currentOptionModelBundle;

              if (params.containerView === 'order') {
                product = bonusOrderLineItem(product, apiProduct, options);
              } else {
                product = bonusProductLineItem(product, apiProduct, options);
              }

              break;
          }

          break;
        case 'productLineItem':
          promotions = PromotionMgr.activeCustomerPromotions.getProductPromotions(apiProduct);
          options = {
            promotions: promotions,
            quantity: params.quantity,
            variables: params.variables,
            lineItem: params.lineItem,
            productType: productType
          };

          switch (productType) {
            case 'bundle':
              if (params.containerView === 'order') {
                product = bundleOrderLineItem(product, apiProduct, options, this);
              } else {
                product = bundleProductLineItem(product, apiProduct, options, this);
              }
              break;
            default:
              var variationsPLI = productHelper.getVariationModel(apiProduct, params.variables);
              if (variationsPLI) {
                apiProduct = variationsPLI.getSelectedVariant() || apiProduct; // eslint-disable-line
              }

              var optionModelPLI = apiProduct.optionModel;
              var optionLineItemsPLI = params.lineItem.optionProductLineItems;
              var currentOptionModelPLI = productHelper.getCurrentOptionModel(optionModelPLI, productHelper.getLineItemOptions(optionLineItemsPLI, productId));
              var lineItemOptionsPLI = optionLineItemsPLI.length
                ? productHelper.getLineItemOptionNames(optionLineItemsPLI)
                : productHelper.getDefaultOptions(optionModelPLI, optionModelPLI.options);

              options.variationModel = variationsPLI;
              options.lineItemOptions = lineItemOptionsPLI;
              options.currentOptionModel = currentOptionModelPLI;

              if (params.containerView === 'order') {
                product = orderLineItem(product, apiProduct, options);
              } else {
                product = productLineItem(product, apiProduct, options);
              }

              break;
          }

          break;
        case 'bonus':
          options = productHelper.getConfig(apiProduct, params);

          switch (productType) {
            case 'set':
              break;
            case 'bundle':
              break;
            default:
              product = bonusProduct(product, options.apiProduct, options, params.duuid);
              break;
          }

          break;
        default:
          // PDP
          options = productHelper.getConfig(apiProduct, params);

          switch (productType) {
            case 'set':
              product = productSet(product, options.apiProduct, options, this);
              break;
            case 'bundle':
              product = productBundle(product, options.apiProduct, options, this);
              break;
            default:
              product = fullProduct(product, options.apiProduct, options);
              break;
          }
      }

      var Site = require('dw/system/Site');
      var customPreferences = Site.current.preferences.custom;
      var masterProduct = apiProduct;
      var ismaster = apiProduct.master;
      var displayPIPOnMaster = customPreferences.displayPIPOnMaster; // master control preference
      var displayPIPText = true;
      if (apiProduct && !ismaster && !apiProduct.productSet) {
        masterProduct = apiProduct.masterProduct;
      }
      if (masterProduct && displayPIPOnMaster) {
        if ('sfccPreorder' in masterProduct.custom && masterProduct.custom.sfccPreorder === 'T') {
          displayPIPText = false;
        }
      }
      product.displayPIPText = displayPIPText;

      return product;
    } catch (e) {
      var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;

      Logger.error('Error Details {0} \n {1}', e.message, e.stack);
      return product;
    }
  }
};
