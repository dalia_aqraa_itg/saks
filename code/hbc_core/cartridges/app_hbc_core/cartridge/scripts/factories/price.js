'use strict';

var RangePrice = require('*/cartridge/models/price/range');
var DefaultPrice = require('*/cartridge/models/price/default');
var formatMoney = require('dw/util/StringUtils').formatMoney;
var money = require('dw/value/Money');
var priceHelper = require('*/cartridge/scripts/helpers/pricing');
var TieredPrice = require('*/cartridge/models/price/tiered');
var pricingHelper = require('*/cartridge/scripts/helpers/pricing');

/**
 * Get list price for a product
 *
 * @param {dw.catalog.ProductPriceModel} priceModel - Product price model
 * @return {dw.value.Money} - List price
 */
function getListPrice(priceModel) {
  var price = money.NOT_AVAILABLE;
  var priceBook;
  var priceBookPrice;

  if (priceModel.price.valueOrNull === null && priceModel.minPrice) {
    return priceModel.minPrice;
  }

  priceBook = priceHelper.getRootPriceBook(priceModel.priceInfo.priceBook);
  priceBookPrice = priceModel.getPriceBookPrice(priceBook.ID);

  if (priceBookPrice.available) {
    return priceBookPrice;
  }

  price = priceModel.price.available ? priceModel.price : priceModel.minPrice;

  return price;
}
function getPriceModel(product) {
  var priceModel = product.priceModel;
  if (product.master && ((!product.priceModel.maxPrice.available && !product.priceModel.minPrice.available) || !product.priceModel.isPriceRange())) {
    priceModel = product.variants[0].priceModel;
  } else {
    priceModel = product.priceModel;
  }
  return priceModel;
}
/**
 * Retrieves Price instance
 *
 * @param {dw.catalog.Product|dw.catalog.productSearchHit} inputProduct - API object for a product
 * @param {boolean} useSimplePrice - Flag as to whether a simple price should be used, used for
 *     product tiles and cart line items.
 * @param {dw.util.Collection<dw.campaign.Promotion>} promotions - Promotions that apply to this
 *                                                                 product
 * @param {dw.catalog.ProductOptionModel} currentOptionModel - The product's option model
 * @param {dw.catalog.Product} firstRepresentedProduct - The product's option model
 * @return {TieredPrice|RangePrice|DefaultPrice} - The product's price
 */
function getDefaultPrice(inputProduct, currency, useSimplePrice, promotions, currentOptionModel, firstRepresentedProduct) {
  var rangePrice;
  var salesPrice;
  var listPrice;
  var product = inputProduct;
  var promotionPrice = money.NOT_AVAILABLE;
  var priceModel = currentOptionModel ? product.getPriceModel(currentOptionModel) : product.getPriceModel();
  var priceTable = priceModel.getPriceTable();

  // TIERED
  if (priceTable.quantities.length > 1) {
    return new TieredPrice(priceTable, useSimplePrice);
  }

  // RANGE
  if ((product.master || product.variationGroup) && priceModel.priceRange) {
    rangePrice = new RangePrice(priceModel.minPrice, priceModel.maxPrice);

    if (rangePrice && rangePrice.min.sales.value !== rangePrice.max.sales.value) {
      return rangePrice;
    }
  }

  // DEFAULT
  if ((product.master || product.variationGroup) && product.variationModel.variants.length > 0) {
    product = firstRepresentedProduct;
    priceModel =  getPriceModel(product);
  }
  if ((product.master || product.variationGroup) && product.variationModel.variants.length > 0) {
    product = firstRepresentedProduct;
    priceModel =  getPriceModel(product);
  }
  promotionPrice = priceHelper.getPromotionPrice(product, promotions, currentOptionModel);
  listPrice = getListPrice(priceModel);
  salesPrice = priceModel.price;
  if (promotionPrice && promotionPrice.available && salesPrice.compareTo(promotionPrice)) {
    salesPrice = promotionPrice;
  }

  if (salesPrice && listPrice && salesPrice.value === listPrice.value) {
    listPrice = null;
  }

  if (salesPrice.valueOrNull === null && listPrice && listPrice.valueOrNull !== null) {
    salesPrice = listPrice;
    listPrice = {};
  }
  return new DefaultPrice(salesPrice, listPrice);
}

/**
 * Retrieves Price instance
 * Overridden to add RangePrice for Product Set
 *
 * @param {dw.catalog.Product|dw.catalog.productSearchHit} inputProduct - API object for a product
 * @param {string} currency - Current session currencyCode
 * @param {boolean} useSimplePrice - Flag as to whether a simple price should be used, used for
 *     product tiles and cart line items.
 * @param {dw.util.Collection<dw.campaign.Promotion>} promotions - Promotions that apply to this
 *                                                                 product
 * @param {dw.catalog.ProductOptionModel} currentOptionModel - The product's option model
 * @return {TieredPrice|RangePrice|DefaultPrice|SetRangePrice} - The product's price
 */
function getPrice(inputProduct, currency, useSimplePrice, promotions, currentOptionModel) {
  var rangePrice = null;
  var searchHit = getProdSearchHit(inputProduct);
  let availableProd = searchHit ? searchHit.firstRepresentedProduct : inputProduct;
  var priceModel = currentOptionModel ? availableProd.getPriceModel(currentOptionModel) : availableProd.getPriceModel();

  // Code changes to show the strike through price range for the list price in different scenarios listed under ticket SFDEV-5974
  // Get the ProductSearchHit instance of the input product first and then get the list price min and max values
  // The similar changes for each items on the productile page is handled in searchPrice.js file
  var listPrice;
  if (searchHit) {
    listPrice = getListPrices(searchHit);
  }
  pricingHelper.setPriceBooksPrice(priceModel);
  var defaultPrice = getDefaultPrice(inputProduct, currency, useSimplePrice, promotions, currentOptionModel, availableProd);

  if (inputProduct.isProductSet() && !priceModel.getMinPrice().isAvailable() && !priceModel.getMaxPrice().isAvailable()) {
    var collections = require('*/cartridge/scripts/util/collections');
    var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
    var productSetProducts = inputProduct.getProductSetProducts();
    var maxValue = Number.MIN_VALUE;
    var minValue = Number.MAX_VALUE;
    var minMoney, maxMoney;

    collections.forEach(productSetProducts, function (product) {
      var prod = product.master && product.variants && product.variants.length ? product.variants[0] : product;
      var pid = prod.getID();
      var options = productHelper.getConfig(prod, {pid: pid});
      var pM = prod.getPriceModel(options.optionModel);
      if (pM.getMinPrice().isAvailable() && minValue >= pM.getMinPrice().getValue()) {
        minMoney = pM.getMinPrice();
        minValue = pM.getMinPrice().getValue();
      }

      if (pM.getMaxPrice().isAvailable() && maxValue <= pM.getMaxPrice().getValue()) {
        maxMoney = pM.getMaxPrice();
        maxValue = pM.getMaxPrice().getValue();
      }
    });
    var min = {
      list: convertPriceModel(minMoney)
    };
    var max = {
      list: convertPriceModel(maxMoney)
    };
    return {
      min: min,
      max: max
    }
  }

  if (searchHit === null) {
    return defaultPrice;
  }
  searchHit = getProdSearchHit(inputProduct);
  var salePrice = {
    minPrice: searchHit.minPrice,
    maxPrice: searchHit.maxPrice
  };
  var listMinPrice = '';
  var listMaxPrice = '';

  if (listPrice.minPrice && listPrice.minPrice.valueOrNull !== null) {
    if (listPrice.minPrice.value !== listPrice.maxPrice.value) {
      listMinPrice = convertPriceModel(listPrice.minPrice);
      listMaxPrice = convertPriceModel(listPrice.maxPrice);
    } else {
      listMinPrice = convertPriceModel(listPrice.minPrice);
    }
  }

  if (priceModel.priceRange) {
    rangePrice = new RangePrice(priceModel.minPrice, priceModel.maxPrice);
    defaultPrice.min = rangePrice.min;
    defaultPrice.max = rangePrice.max;

    if (listMinPrice.value == salePrice.minPrice.value && listMaxPrice.value == salePrice.maxPrice.value) {
      // defaultPrice.listMinPrice = listMaxPrice;
      return defaultPrice;
    }

    defaultPrice.listMinPrice = listMinPrice;
    defaultPrice.listMaxPrice = listMaxPrice;

    //Changes to show the percentage OFF for the Price range - SFDEV-10177
    var lowerPriceSavings = priceHelper.calculatePercentOff(rangePrice.min.sales, listMinPrice);

    if (!empty(listMaxPrice)) {
      var higherPriceSavings = priceHelper.calculatePercentOff(rangePrice.max.sales, listMaxPrice);
    } else {
      var higherPriceSavings = priceHelper.calculatePercentOff(rangePrice.max.sales, listMinPrice);
    }

    if (isNaN(higherPriceSavings.savePercentage)) {
      defaultPrice.lowerPriceSavings = lowerPriceSavings;
      defaultPrice.higherPriceSavings = '';
    } else if (lowerPriceSavings.savePercentage === higherPriceSavings.savePercentage) {
      defaultPrice.lowerPriceSavings = lowerPriceSavings;
      defaultPrice.higherPriceSavings = '';
    } else if (lowerPriceSavings.savePercentage < higherPriceSavings.savePercentage) {
      defaultPrice.lowerPriceSavings = lowerPriceSavings;
      defaultPrice.higherPriceSavings = higherPriceSavings;
    } else {
      defaultPrice.lowerPriceSavings = higherPriceSavings;
      defaultPrice.higherPriceSavings = lowerPriceSavings;
    }

    return defaultPrice;
  } else if (salePrice.minPrice.value === salePrice.maxPrice.value) {
    if (listPrice.minPrice && listPrice.minPrice.valueOrNull !== null) {
      if (listPrice.minPrice.value !== salePrice.minPrice.value && listPrice.minPrice.value !== listPrice.maxPrice.value) {
        if (inputProduct.master) {
          var rangeDefaultPrice = new DefaultPrice(salePrice.minPrice, listPrice.minPrice);
          rangeDefaultPrice.listMinPrice = listMinPrice;
          rangeDefaultPrice.listMaxPrice = listMaxPrice;
          rangeDefaultPrice.type = 'rangeDefault';

          //Changes to show the percentage OFF for the Price range - SFDEV-10177
          var lowerPriceSavings = priceHelper.calculatePercentOff(salePrice.minPrice, listMinPrice);
          var higherPriceSavings = priceHelper.calculatePercentOff(salePrice.maxPrice, listMaxPrice);
          if (lowerPriceSavings.savePercentage === higherPriceSavings.savePercentage) {
            rangeDefaultPrice.lowerPriceSavings = lowerPriceSavings;
            rangeDefaultPrice.higherPriceSavings = '';
          } else if (lowerPriceSavings.savePercentage < higherPriceSavings.savePercentage) {
            rangeDefaultPrice.lowerPriceSavings = lowerPriceSavings;
            rangeDefaultPrice.higherPriceSavings = higherPriceSavings;
          } else {
            rangeDefaultPrice.lowerPriceSavings = higherPriceSavings;
            rangeDefaultPrice.higherPriceSavings = lowerPriceSavings;
          }

          return rangeDefaultPrice;
        }
        return defaultPrice;
      }
    }
  } else if (salePrice.minPrice.value !== salePrice.maxPrice.value) {
    // Changes done for SFDEV-8617 - Price range issue
    var rangePrice = new RangePrice(salePrice.minPrice, salePrice.maxPrice);

    if (!empty(listMinPrice)) {
      if (listMinPrice.value == salePrice.minPrice.value && listMaxPrice.value == salePrice.maxPrice.value) {
        return rangePrice;
      }
      rangePrice.listMinPrice = listMinPrice;
      rangePrice.listMaxPrice = listMaxPrice;

      //Changes to show the percentage OFF for the Price range - SFDEV-10177
      var lowerPriceSavings = priceHelper.calculatePercentOff(salePrice.minPrice, listMinPrice);
      var higherPriceSavings = priceHelper.calculatePercentOff(salePrice.maxPrice, listMaxPrice);
      if (lowerPriceSavings.savePercentage === higherPriceSavings.savePercentage) {
        rangePrice.lowerPriceSavings = lowerPriceSavings;
        rangePrice.higherPriceSavings = '';
      } else if (lowerPriceSavings.savePercentage < higherPriceSavings.savePercentage) {
        rangePrice.lowerPriceSavings = lowerPriceSavings;
        rangePrice.higherPriceSavings = higherPriceSavings;
      } else {
        rangePrice.lowerPriceSavings = higherPriceSavings;
        rangePrice.higherPriceSavings = lowerPriceSavings;
      }
    }
    return rangePrice;
  }
  return defaultPrice;
}

/**
 * Get product search hit for a given product
 * @param {dw.catalog.Product} apiProduct - Product instance returned from the API
 * @returns {dw.catalog.ProductSearchHit} - product search hit for a given product
 */
function getProdSearchHit(apiProduct) {
  var ProductSearchModel = require('dw/catalog/ProductSearchModel');
  var searchModel = new ProductSearchModel();
  searchModel.setSearchPhrase(apiProduct.ID);
  searchModel.search();

  if (searchModel.count === 0) {
    searchModel.setSearchPhrase(apiProduct.ID.replace(/-/g, ' '));
    searchModel.search();
    if (searchModel.count === 0) {
      return null;
    }
  }

  var hit = searchModel.getProductSearchHit(apiProduct);
  if (!hit) {
    var tempHit = searchModel.getProductSearchHits().next();
    if (tempHit.firstRepresentedProductID === apiProduct.ID) {
      hit = tempHit;
    }
  }
  return hit;
}

/**
 * Get list price for a given product
 * @param {dw.catalog.ProductSearchHit} hit - current product returned by Search API.
 * @returns {Object} - price for a product
 */
function getListPrices(hit) {
  var PriceBookMgr = require('dw/catalog/PriceBookMgr');
  var priceModel = hit.firstRepresentedProduct.getPriceModel();
  if (!priceModel.priceInfo) {
    return {};
  }
  var rootPriceBook = pricingHelper.getRootPriceBook(priceModel.priceInfo.priceBook);
  PriceBookMgr.setApplicablePriceBooks(rootPriceBook);
  var searchHit = getProdSearchHit(hit.product) || hit;

  if (searchHit) {
    if (searchHit.minPrice.available && searchHit.maxPrice.available) {
      return {
        minPrice: searchHit.minPrice,
        maxPrice: searchHit.maxPrice
      };
    }

    return {
      minPrice: hit.minPrice,
      maxPrice: hit.maxPrice
    };
  }

  return {};
}

function convertPriceModel(price) {
  var value = price.available ? price.getDecimalValue().get() : null;
  var formattedPrice = price.available ? formatMoney(price) : null;
  var decimalPrice;

  if (formattedPrice) {
    decimalPrice = price.getDecimalValue().toString();
  }

  return {
    value: value,
    decimalPrice: decimalPrice,
    formatAmount: priceHelper.formatMoney(formattedPrice)
  };
}

function getFormattedAmount(formattedPrice) {
  var formatAmount = formattedPrice;
  if (request.session.currency.defaultFractionDigits && formattedPrice) {
    // eslint-disable-line
    var decimalValue = formattedPrice.substr(formattedPrice.length - (request.session.currency.defaultFractionDigits - 1) - 1, formattedPrice.length); // eslint-disable-line
    if (parseInt(decimalValue) === 0) {
      // eslint-disable-line
      formatAmount = formattedPrice.substr(0, formattedPrice.length - request.session.currency.defaultFractionDigits - 1); // eslint-disable-line
    }
  }
  return formatAmount;
}

module.exports = {
  getPrice: getPrice
};
