'use strict';

var BasketMgr = require('dw/order/BasketMgr');
var Transaction = require('dw/system/Transaction');
var Resource = require('dw/web/Resource');

/**
 * Generate address name based on the full address object
 * @param {dw.order.OrderAddress} address - Object that contains shipping address
 * @returns {string} - String with the generated address name
 */
function generateAddressName(address) {
  return [address.address1 || '', address.city || '', address.postalCode || ''].join(' - ');
}

/**
 * Verify if the address already exists as a stored user address
 * @param {dw.order.OrderAddress} address - Object that contains shipping address
 * @param {Object[]} storedAddresses - List of stored user addresses
 * @returns {boolean} - Boolean indicating if the address already exists
 */
function checkIfAddressStored(address, storedAddresses) {
  for (var i = 0, l = storedAddresses.length; i < l; i++) {
    if (
      storedAddresses[i].address1 === address.address1 &&
      storedAddresses[i].postalCode === address.postalCode &&
      storedAddresses[i].firstName === address.firstName &&
      storedAddresses[i].lastName === address.lastName
    ) {
      return true;
    }
  }
  return false;
}

/**
 * Copy information from address object and save it in the system
 * @param {dw.customer.CustomerAddress} newAddress - newAddress to save information into
 * @param {*} address - Address to copy from
 */
function updateAddressFields(newAddress, address) {
  newAddress.setAddress1(address.address1 || '');
  newAddress.setAddress2(address.address2 || '');
  newAddress.setCity(address.city || '');
  newAddress.setFirstName(address.firstName || '');
  newAddress.setLastName(address.lastName || '');
  newAddress.setPhone(address.phone || '');
  newAddress.setPostalCode(address.postalCode || '');

  if (address.country === 'UK') {
    newAddress.setStateCode(address.UKState);
  } else if (address.country !== 'UK' && address.states) {
    if (address.states.stateCode) {
      newAddress.setStateCode(address.states.stateCode);
    } else if (address.states.stateUS) {
      newAddress.setStateCode(address.states.stateUS);
    } else if (address.states.stateCA) {
      newAddress.setStateCode(address.states.stateCA);
    }
  }

  if (address.country) {
    newAddress.setCountryCode(address.country);
  }
  // Vertax Specific Code - Update Tax Number
  newAddress.custom.taxnumber = address.taxnumber; // eslint-disable-line

  newAddress.setJobTitle(address.jobTitle || '');
  newAddress.setPostBox(address.postBox || '');
  newAddress.setSalutation(address.salutation || '');
  newAddress.setSecondName(address.secondName || '');
  newAddress.setCompanyName(address.companyName || '');
  newAddress.setSuffix(address.suffix || '');
  newAddress.setSuite(address.suite || '');
  newAddress.setJobTitle(address.title || '');
}

/**
 * Stores a new address for a given customer
 * @param {Object} address - New address to be saved
 * @param {Object} customer - Current customer
 * @param {string} addressId - Id of a new address to be created
 * @returns {void}
 */
function saveAddress(address, customer, addressId) {
  var addressBook = customer.raw.getProfile().getAddressBook();
  Transaction.wrap(function () {
    var newAddress = addressBook.createAddress(addressId);
    if (newAddress) {
      updateAddressFields(newAddress, address);
    }
  });
}

/**
 * Copy dwscript address object into JavaScript object
 * @param {dw.order.OrderAddress} address - Address to be copied
 * @returns {Object} - Plain object that represents an address
 */
function copyShippingAddress(address) {
  return {
    address1: address.address1,
    address2: address.address2,
    city: address.city,
    firstName: address.firstName,
    lastName: address.lastName,
    phone: address.phone,
    postalCode: address.postalCode,
    states: {
      stateCode: address.stateCode
    },
    country: address.countryCode,
    jobTitle: address.jobTitle,
    postBox: address.postBox,
    salutation: address.salutation,
    secondName: address.secondName,
    companyName: address.companyName,
    suffix: address.suffix,
    suite: address.suite,
    title: address.title
  };
}

/**
 * Return address object
 * @param {dw.order.OrderAddress} address - address
 * @returns {Object} address
 */
function getAddressObject(address) {
  if (address) {
    return {
      address1: address.address1,
      address2: address.address2,
      city: address.city,
      countryCode: {
        displayValue: address.countryCode.displayValue,
        value: address.countryCode.value
      },
      firstName: address.firstName,
      lastName: address.lastName,
      ID: address.ID,
      phone: address.phone,
      postalCode: address.postalCode,
      stateCode: address.stateCode,
      taxnumber: address.custom.taxnumber
    };
  }
  return null;
}

/**
 * Creates a list of payment instruments for the current user
 * @param {Array} rawPaymentInstruments - current customer's payment instruments
 * @returns {Array} an array of payment instruments
 */
function getPaymentInstruments(rawPaymentInstruments) {
  var paymentInstruments = [];

  if (rawPaymentInstruments.getLength() > 0) {
    var iterator = rawPaymentInstruments.iterator();
    while (iterator.hasNext()) {
      var item = iterator.next();
      paymentInstruments.push({
        creditCardHolder: item.creditCardHolder,
        maskedCreditCardNumber: item.maskedCreditCardNumber,
        creditCardType: item.creditCardType,
        creditCardExpirationMonth: item.creditCardExpirationMonth,
        creditCardExpirationYear: item.creditCardExpirationYear,
        UUID: item.UUID,
        creditCardNumber: Object.hasOwnProperty.call(item, 'creditCardNumber') ? item.creditCardNumber : null,
        raw: item
      });
    }
  }

  return paymentInstruments;
}

/**
 * Gather all addresses from shipments and return as an array
 * @param {dw.order.Basket} order - current order
 * @returns {Array} - Array of shipping addresses
 */
function gatherShippingAddresses(order) {
  var collections = require('*/cartridge/scripts/util/collections');
  var allAddresses = [];

  if (order.shipments) {
    collections.forEach(order.shipments, function (shipment) {
      if (shipment.shippingAddress) {
        allAddresses.push(copyShippingAddress(shipment.shippingAddress));
      }
    });
  } else {
    allAddresses.push(order.defaultShipment.shippingAddress);
  }
  return allAddresses;
}

/**
 * Translates global customer object into local object
 * @param {dw.customer.Customer} customer - Global customer object
 * @returns {Object} local instance of customer object
 */
function getCustomerObject(customer) {
  if (!customer || !customer.profile) {
    return {
      raw: customer
    };
  }
  if (!customer.authenticated) {
    return {
      raw: customer,
      credentials: {
        username: customer.profile.credentials.login
      }
    };
  }
  var preferredAddress = customer.addressBook.preferredAddress;
  var result;
  result = {
    raw: customer,
    profile: {
      lastName: customer.profile.lastName,
      firstName: customer.profile.firstName,
      email: customer.profile.email,
      phone: customer.profile.phoneHome,
      customerNo: customer.profile.customerNo
    },
    addressBook: {
      preferredAddress: getAddressObject(preferredAddress),
      addresses: []
    },
    wallet: {
      paymentInstruments: getPaymentInstruments(customer.profile.wallet.paymentInstruments)
    }
  };
  if (customer.addressBook.addresses && customer.addressBook.addresses.length > 0) {
    for (var i = 0, ii = customer.addressBook.addresses.length; i < ii; i++) {
      result.addressBook.addresses.push(getAddressObject(customer.addressBook.addresses[i]));
    }
  }
  return result;
}

/**
 * Copies a CustomerAddress to a Shipment as its Shipping Address
 * @param {dw.customer.CustomerAddress} address - The customer address
 * @param {dw.order.Shipment} [shipmentOrNull] - The target shipment
 */
function copyCustomerAddressToShipment(address, shipmentOrNull) {
  var currentBasket = BasketMgr.getCurrentBasket();
  var shipment = shipmentOrNull || currentBasket.defaultShipment;
  var shippingAddress = shipment.shippingAddress;

  Transaction.wrap(function () {
    if (shippingAddress === null) {
      shippingAddress = shipment.createShippingAddress();
    }

    shippingAddress.setFirstName(address.firstName);
    shippingAddress.setLastName(address.lastName);
    shippingAddress.setAddress1(address.address1);
    shippingAddress.setAddress2(address.address2);
    shippingAddress.setCity(address.city);
    shippingAddress.setPostalCode(address.postalCode);
    shippingAddress.setStateCode(address.stateCode);
    var countryCode = address.countryCode;
    shippingAddress.setCountryCode(countryCode.value);
    shippingAddress.setPhone(address.phone);
    shippingAddress.custom.taxnumber = address.taxnumber;
  });
}

/**
 * Add tax number to the Shipping Address
 * @param {dw.customer.CustomerAddress} address - The customer address
 * @param {dw.order.Shipment} shipment - The target shipment
 */
function addTaxNumber(address, shipment) {
  var shippingAddress = shipment.shippingAddress;
  Transaction.wrap(function () {
    shippingAddress.custom.taxnumber = address.taxnumber;
  });
}

/**
 * Add tax number to the Shipping Address
 * @param {FormField} formField - formField
 * @param {string} resourceFile - The resource File
 * @return {FormFields} fields - fields
 */
function getFieldOptions(formField, resourceFile) {
  if (empty(formField.options)) {
    return {};
  }
  var fields = {};

  var opts = formField.options;
  /* eslint-disable */
  for (o in opts) {
    try {
      if (opts[o] && opts[o].value && opts[o].value.length > 0) {
        var option = opts[o];
        fields[option.value] = Resource.msg(option.label, resourceFile, option.label);
      }
    } catch (error) {
      if (!fields.error) {
        fields.error = [];
      }
      fields.error.push('Error: ' + error);
    }
  }
  /* eslint-enable */

  return fields;
}

/**
 * Get the countries and regions
 * @param {FormField} addressForm - formField
 * @param {string} resource -resource File
 * @return {FormFields} fields - fields
 */
function getCountriesAndRegions(addressForm, resource) {
  //eslint-disable-line
  var list = {};
  var countryField = addressForm.country;
  var stateForm = addressForm.states;
  var resourceName = 'forms';

  if (empty(countryField.options)) {
    return list;
  }

  var countryOptions = countryField.options;
  /* eslint-disable */
  for (var o in countryOptions) {
    try {
      if (countryOptions[o] && countryOptions[o].value && countryOptions[o].value.length > 0) {
        var option = countryOptions[o];
        var stateField = !empty(stateForm['state' + option.value]) ? stateForm['state' + option.value] : stateForm.nostates;
        list[option.value] = {
          regionLabel: Resource.msg(stateField.label, resourceName, stateField.label),
          regions: getFieldOptions(stateField, resourceName)
        };
      }
    } catch (error) {
      if (!list.error) {
        list.error = [];
      }
      list.error.push('Error: ' + error);
    }
  }
  list['postal_label'] = {
    US: Resource.msg('label.postal.code.US', 'address', null),
    CA: Resource.msg('label.postal.code.CA', 'address', null),
    UK: Resource.msg('label.postal.code.CA', 'address', null)
  };
  list['postal_req_error'] = {
    US: Resource.msg('error.messagePatternmismatchPostal.us.required', 'error', null),
    CA: Resource.msg('error.messagePatternmismatchPostal.ca.required', 'error', null),
    UK: Resource.msg('error.messagePatternmismatchPostal.ca.required', 'error', null)
  };
  /* eslint-enable */
  return list;
}

/**
 * Stores a new address for a given customer
 * @param {Object} address - New address to be saved
 * @param {Object} customer - Current customer
 * @param {string} addressId - Id of a new address to be created
 * @returns {void}
 */
function saveBillingAddress(address, customer, addressId) {
  var Transaction = require('dw/system/Transaction');

  var addressBook = customer.raw.getProfile().getAddressBook();
  var profile = customer.raw.getProfile();
  Transaction.wrap(function () {
    var newAddress = addressBook.createAddress(addressId);
    if (newAddress) {
      newAddress.setAddress1(address.address1 || '');
      newAddress.setAddress2(address.address2 || '');
      newAddress.setCity(address.city || '');
      newAddress.setFirstName(address.firstName || '');
      newAddress.setLastName(address.lastName || '');
      newAddress.setPhone(address.phone || '');
      newAddress.setPostalCode(address.postalCode || '');
      newAddress.setStateCode(address.stateCode);
      newAddress.setCountryCode(address.countryCode.value);
      newAddress.setJobTitle(address.jobTitle || '');
      newAddress.setPostBox(address.postBox || '');
      newAddress.setSalutation(address.salutation || '');
      newAddress.setSecondName(address.secondName || '');
      newAddress.setCompanyName(address.companyName || '');
      newAddress.setSuffix(address.suffix || '');
      newAddress.setSuite(address.suite || '');
      newAddress.setJobTitle(address.title || '');
      profile.custom.accountModifiedDate = new Date().toISOString();
    }
  });
}

/**
 * Validates the zip versus state
 * @param {Object} address - address to be validated
 * @returns {object} - object with the errored fields
 */
function validateStateVsZip(address) {
  var ShippingLocation = require('dw/order/ShippingLocation');
  var TaxMgr = require('dw/order/TaxMgr');
  var results = {};
  // validate only US states
  if (
    address.addressFields &&
    address.addressFields.postalCode &&
    address.addressFields.postalCode.htmlValue &&
    address.addressFields.states &&
    address.addressFields.states.stateCode &&
    address.addressFields.states.stateCode.htmlValue &&
    address.addressFields.country &&
    (address.addressFields.country.htmlValue === 'US' || address.addressFields.country.htmlValue === '')
  ) {
    var shippingLocation = new ShippingLocation();
    shippingLocation.setStateCode(address.addressFields.states.stateCode.value);
    shippingLocation.setPostalCode(address.addressFields.postalCode.value);
    if (!TaxMgr.getTaxJurisdictionID(shippingLocation)) {
      results[address.addressFields.postalCode.htmlName] = Resource.msg('error.messagePatternmismatchPostal.required', 'error', null);
    }
  }
  return results;
}

module.exports = {
  generateAddressName: generateAddressName,
  checkIfAddressStored: checkIfAddressStored,
  saveAddress: saveAddress,
  copyShippingAddress: copyShippingAddress,
  updateAddressFields: updateAddressFields,
  gatherShippingAddresses: gatherShippingAddresses,
  getCustomerObject: getCustomerObject,
  copyCustomerAddressToShipment: copyCustomerAddressToShipment,
  addTaxNumber: addTaxNumber,
  getCountriesAndRegions: getCountriesAndRegions,
  saveBillingAddress: saveBillingAddress,
  validateStateVsZip: validateStateVsZip
};
