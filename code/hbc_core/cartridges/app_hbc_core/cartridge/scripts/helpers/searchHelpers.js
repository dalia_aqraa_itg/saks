'use strict';

/**
 * Set search configuration values
 *
 * @param {dw.catalog.ProductSearchModel} apiProductSearch - API search instance
 * @param {Object} params - Provided HTTP query parameters
 * @return {dw.catalog.ProductSearchModel} - API search instance
 */
function setupSearch(apiProductSearch, params) {
  var CatalogMgr = require('dw/catalog/CatalogMgr');
  var searchModelHelper = require('*/cartridge/scripts/search/search');
  var preferences = require('*/cartridge/config/preferences');
  var designerCategoryID = 'brand';
  if (preferences.designerCategoryID) {
    designerCategoryID = preferences.designerCategoryID;
  }
  var sortingRule = params.srule ? CatalogMgr.getSortingRule(params.srule) : null;
  if (params.cgid === designerCategoryID || params.cgid === 'brand') {
    params.cgid = 'root';
  }
  if (!params.cgid && !params.q && !params.pid && !params.pmid) {
    return apiProductSearch;
  }
  var selectedCategory = CatalogMgr.getCategory(params.cgid);
  selectedCategory = selectedCategory && selectedCategory.online ? selectedCategory : null;

  searchModelHelper.setProductProperties(apiProductSearch, params, selectedCategory, sortingRule);

  if (params.preferences) {
    searchModelHelper.addRefinementValues(apiProductSearch, params.preferences);
  }

  return apiProductSearch;
}

/**
 * Retrieve a category's template filepath if available
 *
 * @param {dw.catalog.ProductSearchModel} apiProductSearch - API search instance
 * @return {string} - Category's template filepath
 */
function getCategoryTemplate(apiProductSearch) {
  return apiProductSearch.category ? apiProductSearch.category.template || 'rendering/category/producthits' : '';
}

/**
 * Set content search configuration values
 *
 * @param {Object} params - Provided HTTP query parameters
 * @return {Object} - content search instance
 */
function setupContentSearch(params) {
  var ContentSearchModel = require('dw/content/ContentSearchModel');
  var ContentSearch = require('*/cartridge/models/search/contentSearch');
  var apiContentSearchModel = new ContentSearchModel();

  apiContentSearchModel.setRecursiveFolderSearch(true);
  apiContentSearchModel.setSearchPhrase(params.q);
  apiContentSearchModel.search();
  var contentSearchResult = apiContentSearchModel.getContent();
  var count = Number(apiContentSearchModel.getCount());
  var contentSearch = new ContentSearch(contentSearchResult, count, params.q, params.startingPage, null);

  return contentSearch;
}

/**
 * Set the cache values
 *
 * @param {Object} res - The response object
 */
function applyCache(res) {
  res.cachePeriod = 1; // eslint-disable-line no-param-reassign
  res.cachePeriodUnit = 'hours'; // eslint-disable-line no-param-reassign
  res.personalized = true; // eslint-disable-line no-param-reassign
}

/**
 * performs a search
 *
 * @param {Object} req - Provided HTTP query parameters
 * @param {Object} res - Provided HTTP query parameters
 * @return {Object} - an object with relevant search information
 */
function search(req, res) {
  var CatalogMgr = require('dw/catalog/CatalogMgr');
  var URLUtils = require('dw/web/URLUtils');
  var ProductSearchModel = require('dw/catalog/ProductSearchModel');
  var preferences = require('*/cartridge/config/preferences');
  var ProductSearch = require('*/cartridge/models/search/productSearch');
  var reportingUrlsHelper = require('*/cartridge/scripts/reportingUrls');
  var schemaHelper = require('*/cartridge/scripts/helpers/structuredDataHelper');
  var refineSearch = require('*/cartridge/models/bopis/refineSearch');
  var storeRefineResult = null;
  var categoryTemplate = '';
  var maxSlots = 4;
  var productSearch;
  var reportingURLs;

  var apiProductSearch = res.getViewData().apiProductSearch;
  if (!apiProductSearch) {
    apiProductSearch = new ProductSearchModel();
  }

  // Remove Channel

  var searchRedirect = req.querystring.q ? apiProductSearch.getSearchRedirect(req.querystring.q) : null;

  if (searchRedirect) {
    return {
      searchRedirect: searchRedirect.getLocation()
    };
  }

  // identify if the product ID searched is chanel
  let isChanel = false;
  if (req.querystring.q && /^[0-9]*$/.test(req.querystring.q)) {
    var ProductMgr = require('dw/catalog/ProductMgr');
    var apiProduct = ProductMgr.getProduct(req.querystring.q);
    if (apiProduct && apiProduct.brand && apiProduct.brand.toLowerCase().indexOf('chanel') > -1) {
      isChanel = true;
    }
  }
  // if the search key has chanel, then skip the refinement of non-chanel products.
  if (!isChanel && req.querystring && req.querystring.q && req.querystring.q.toLowerCase().indexOf('chanel') === -1) {
    if (preferences.nonChanelProductTypes) {
      apiProductSearch.addRefinementValues('hbcProductType', preferences.nonChanelProductTypes);
    }
  }

  apiProductSearch = setupSearch(apiProductSearch, req.querystring);

  // PSM search with store refinement
  storeRefineResult = refineSearch.search(apiProductSearch, req.querystring);
  apiProductSearch = storeRefineResult.apiProductsearch;

  if (!apiProductSearch.personalizedSort) {
    applyCache(res);
  }
  categoryTemplate = getCategoryTemplate(apiProductSearch);
  productSearch = new ProductSearch(
    apiProductSearch,
    req.querystring,
    req.querystring.srule,
    CatalogMgr.getSortingOptions(),
    CatalogMgr.getSiteCatalog().getRoot()
  );

  var canonicalUrl = URLUtils.url('Search-Show', 'cgid', req.querystring.cgid);
  var refineurl = URLUtils.url('Search-Refinebar');
  var whitelistedParams = ['q', 'cgid', 'pmin', 'pmax', 'srule', 'pmid', 'storeid'];
  var isRefinedSearch = false;

  Object.keys(req.querystring).forEach(function (element) {
    if (whitelistedParams.indexOf(element) > -1) {
      refineurl.append(element, req.querystring[element]);
    }

    if (['pmin', 'pmax'].indexOf(element) > -1) {
      isRefinedSearch = true;
    }

    if (element === 'preferences') {
      var i = 1;
      isRefinedSearch = true;
      Object.keys(req.querystring[element]).forEach(function (preference) {
        refineurl.append('prefn' + i, preference);
        refineurl.append('prefv' + i, req.querystring[element][preference]);
        i++;
      });
    }
  });

  if (productSearch.searchKeywords !== null && !isRefinedSearch) {
    reportingURLs = reportingUrlsHelper.getProductSearchReportingURLs(productSearch);
  }

  var result = {
    productSearch: productSearch,
    maxSlots: maxSlots,
    reportingURLs: reportingURLs,
    refineurl: refineurl,
    canonicalUrl: canonicalUrl,
    isRefinedByStore: storeRefineResult.isRefinedByStore
  };

  if (productSearch.isCategorySearch && !productSearch.isRefinedCategorySearch) {
    // !productSearch.isRefinedCategorySearch is a necessary condition. For Brand pages, the category context would be as refined category
    result.category = apiProductSearch.category;
    result.categoryTemplate = categoryTemplate;
  }
  // Changes required for SEO url changes for filter - DNE this condition
  if (productSearch.isCategorySearch) {
    result.tempCategory = apiProductSearch.category;
  }

  if (!categoryTemplate || categoryTemplate === 'rendering/category/categoryproducthits') {
    result.schemaData = schemaHelper.getListingPageSchema(productSearch.productIds);
  }

  return result;
}

/**
 * function to create designer object
 *
 * @param {dw.catalog.Category} category - main designer category
 * @returns {Object} - search designer object
 */
function searchDesigner(search) {
  var refinements = search.refinements;
  var refinement;
  var refinementValues;
  var SortedMap = require('dw/util/SortedMap');
  var URLUtils = require('dw/web/URLUtils');
  var brandsMappings = new SortedMap();
  for (var i = 0; i < refinements.length; i++) {
    refinement = refinements[i];
    if (refinement.isAttributeRefinement && refinement.attributeID.toLowerCase() === 'brand') {
      refinementValues = refinement.values;
      break;
    }
  }
  var ArrayList = require('dw/util/ArrayList');
  if (refinementValues) {
    refinementValues.forEach(function (refinementValue) {
      var brands = new ArrayList();
      refinementValue.url = URLUtils.https('Search-Show', 'cgid', 'brand', 'prefn1', 'brand', 'prefv1', refinementValue.value);
      var firstChar = refinementValue.displayValue.charAt(0).toUpperCase();
      if (/[^a-zA-Z]+/.test(firstChar)) {
        firstChar = 'non-alpha';
      }
      if (brandsMappings != null && brandsMappings.get(firstChar) != null) {
        brands.addAll(brandsMappings.get(firstChar));
        brands.add(refinementValue);
      } else {
        brands.add(refinementValue);
      }
      brandsMappings.put(firstChar, brands);
    });
    return brandsMappings;
  }
}

/**
 * Get category url
 * @param {dw.catalog.Category} category - Current category
 * @returns {string} - Url of the category
 */
function getCategoryUrl(category) {
  var URLUtils = require('dw/web/URLUtils');
  return category.custom && 'alternativeUrl' in category.custom && category.custom.alternativeUrl
    ? category.custom.alternativeUrl.markup
    : URLUtils.url('Search-Show', 'cgid', category.getID()).toString();
}

/**
 * Converts a given category from dw.catalog.Category to plain object
 * @param {dw.catalog.Category} category - A single category
 * @returns {Object} plain object that represents a category
 */
function categoryToObject(category) {
  var collections = require('*/cartridge/scripts/util/collections');
  var result = {
    name:
      'categoryNameoverwrite' in category.custom && !!category.custom.categoryNameoverwrite ? category.custom.categoryNameoverwrite : category.getDisplayName(),
    url: getCategoryUrl(category),
    id: category.ID
  };
  var subCategories = category.hasOnlineSubCategories() ? category.getOnlineSubCategories() : null;

  if (subCategories) {
    collections.forEach(subCategories, function (subcategory) {
      var converted = null;
      if (subcategory.hasOnlineProducts() || subcategory.hasOnlineSubCategories()) {
        converted = categoryToObject(subcategory);
      }
      if (converted) {
        if (!result.subCategories) {
          result.subCategories = [];
        }
        result.subCategories.push(converted);
      }
    });
    if (result.subCategories) {
      result.complexSubCategories = result.subCategories.some(function (item) {
        return !!item.subCategories;
      });
    }
  }

  return result;
}

/**
 * Represents a single category with all of it's children
 * @param {dw.util.ArrayList<dw.catalog.Category>} items - Top level categories
 * @constructor
 */
function categories(items) {
  var collections = require('*/cartridge/scripts/util/collections');
  var categoriesList = [];
  collections.forEach(items, function (item) {
    if (item.hasOnlineProducts() || item.hasOnlineSubCategories()) {
      categoriesList.push(categoryToObject(item));
    }
  });
  return categoriesList;
}

/**
 * Identifies and returns L1 category for the input category passed
 * @param {dw/catalog/Category} parentCatLevel - Top level categories
 * @returns {dw/catalog/Category} category which is an L1
 */
function getRootCat(parentCatLevel) {
  if (parentCatLevel.getParent().isRoot()) {
    return parentCatLevel;
  }
  return getRootCat(parentCatLevel.getParent()); // statement 2
}

/**
 * Identifies and returns L1 category for the input category passed
 * @param {dw/catalog/Category} category - Top level categories
 * @returns {dw/catalog/Category} category which is an L1
 */
function getL1CategoryDetails(category) {
  return getRootCat(category);
}
exports.setupSearch = setupSearch;
exports.getCategoryTemplate = getCategoryTemplate;
exports.setupContentSearch = setupContentSearch;
exports.getCategoryUrl = getCategoryUrl;
exports.search = search;
exports.applyCache = applyCache;
exports.searchDesigner = searchDesigner;
exports.categories = categories;
exports.getL1CategoryDetails = getL1CategoryDetails;
