'use strict';

var base = module.superModule;
var HashMap = require('dw/util/HashMap');
var bopisHelper = require('*/cartridge/scripts/helpers/bopisHelpers');
var preferences = require('*/cartridge/config/preferences');
var Resource = require('dw/web/Resource');
var Logger = require('dw/system/Logger');

/**
 * Searches for stores and creates a plain object of the stores returned by the search
 * @param {string} radius - selected radius
 * @param {string} postalCode - postal code for search
 * @param {string} lat - latitude for search by latitude
 * @param {string} long - longitude for search by longitude
 * @param {Object} geolocation - geloaction object with latitude and longitude
 * @param {boolean} showMap - boolean to show map
 * @param {dw.web.URL} url - a relative url
 * @param {[Object]} products - an array of product ids to quantities that needs to be filtered by.
 * @returns {Object} a plain object containing the results of the search
 */
function getStores(radius, postalCode, lat, long, geolocation, showMap, url, products) {
  var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
  var StoresModel = require('*/cartridge/models/stores');
  var StoreMgr = require('dw/catalog/StoreMgr');
  var Site = require('dw/system/Site');
  var URLUtils = require('dw/web/URLUtils');

  var countryCode = require('*/cartridge/config/preferences').countryCode.value; // DEBUG: var countryCode = geolocation.countryCode;
  var queryString = '';
  var distanceUnit = countryCode === 'US' ? 'mi' : 'km';
  var resolvedRadius = radius ? parseInt(radius, 10) : 15;
  var searchKey = {};
  var formattedCityEntry = {};
  var storeMgrResult = null;
  var location = {};
  var distanceCollection = new HashMap();

  // find by coordinates (detect location)
  location.lat = lat && long ? parseFloat(lat) : geolocation.latitude;
  location.long = long && lat ? parseFloat(long) : geolocation.longitude;

  if (postalCode && postalCode !== '') {
    // find by postal code
    searchKey = postalCode.toString().trim().toUpperCase();
    storeMgrResult = StoreMgr.searchStoresByPostalCode(countryCode, searchKey, distanceUnit, resolvedRadius);
    searchKey = {
      postalCode: searchKey
    };
    // search with city if not found with postal
    if (storeMgrResult.size() === 0) {
      // convert user entered city to all cases and execute search for better results
      formattedCityEntry = bopisHelper.convertCases(postalCode);

      // To make Store search by City similar to look-up by Postal Code, the City-postal code mapping is maintained in a properties file. SFDEV-7932
      var lowercaseCity = formattedCityEntry.lowercase;
      var finalCity = lowercaseCity;
      if (lowercaseCity.indexOf(' ') > 0) {
        //logic to remove any space between the city name before looking up in the properties file
        finalCity = '';
        var splitCity = lowercaseCity.split(' ');
        for (i = 0; i < splitCity.length; i++) {
          finalCity = finalCity + splitCity[i];
        }
        finalCity = finalCity.trim();
      }

      var mappedPostalCode = Resource.msg(finalCity, 'cities', null); // City-postal code mapping is maintained in cities.properties file

      if (mappedPostalCode) {
        storeMgrResult = StoreMgr.searchStoresByPostalCode(countryCode, mappedPostalCode, distanceUnit, resolvedRadius);
      }
    }
  } else {
    storeMgrResult = StoreMgr.searchStoresByCoordinates(location.lat, location.long, distanceUnit, resolvedRadius);
    searchKey = {
      lat: location.lat,
      long: location.long
    };
  }

  var actionUrl = url || URLUtils.url('Stores-FindStores', 'showMap', showMap).toString();
  var apiKey = Site.getCurrent().getCustomPreferenceValue('mapAPI');
  var apiStores = storeMgrResult.keySet();
  // prepare a store distance map
  Object.keys(apiStores).forEach(function (key) {
    var apiStore = apiStores[key];
    return distanceCollection.put(apiStore.ID, storeMgrResult.get(apiStore));
  });
  var storesModel = new StoresModel(storeMgrResult.keySet(), searchKey, resolvedRadius, actionUrl, apiKey);
  storesModel.stores = storesModel.stores.filter(function (store) {
    if (store.storeClosed === 'Yes') {
      return false;
    }
    if (distanceCollection) {
      var distance = distanceCollection.get(store.ID);
      // property is defined without a prototype since no qualifiers are required
      distance = distance ? Number(distance).toFixed(2) : distance;
      store.distanceInUnits = distance + ' ' + distanceUnit; // eslint-disable-line
    }
    if (products) {
      var storeInventoryListId = store.inventoryListId;
      if (storeInventoryListId) {
        var storeInventory = ProductInventoryMgr.getInventoryList(storeInventoryListId);
        if (storeInventory) {
          return products.every(function (product) {
            var inventoryRecord = storeInventory.getRecord(product.id);
            // properties are defined without a prototype since no qualifiers are required
            if (inventoryRecord && inventoryRecord.ATS.value) {
              // set available units
              store.unitsAtStores = inventoryRecord.ATS.value; // eslint-disable-line
            }
            // set quantity messaging
            store.availabilityMessage = bopisHelper.setAvailabilityMessage(store, product); // eslint-disable-line
            return true;
          });
        }
      } // Returning true to show all the Stores - fix for ticket SFDEV-7688
      store.availabilityMessage = Resource.msg('store.unitsavailable.unavailable', 'storeLocator', null);
      return true;
    }
    return true;
  });
  // re-assign html to include post created store model properties in to view
  storesModel.storesResultsHtml = base.createStoresResultsHtml(storesModel.stores);

  return storesModel;
}

/**
 * create Change Store Html results html
 * @param {string} productId - product it to fetch properties from
 * @param {string} storeid - storeid to fetch store name
 * @returns {string} template - render html
 */
function createChangeStoreHtml(productId, storeid) {
  var Template = require('dw/util/Template');
  var URLUtils = require('dw/web/URLUtils');
  var productFactory = require('*/cartridge/scripts/factories/product');
  var StoreMgr = require('dw/catalog/StoreMgr');
  var context = new HashMap();
  var prodObj = {};
  var brand = {};
  if (productId && storeid) {
    var product = productFactory.get({ pid: productId });
    var store = StoreMgr.getStore(storeid);
    prodObj.productName = product.productName;
    brand.name = product.brand && product.brand.name ? product.brand.name : null;
    prodObj.productType = product.productType;
    var object = {
      setStoreUrl: URLUtils.url('Stores-SetStore').toString(),
      storeModalUrl: URLUtils.url('Stores-InitSearch').toString(),
      product: prodObj,
      storeName: store.name,
      selectedStoreId: storeid
    };

    Object.keys(object).forEach(function (key) {
      context.put(key, object[key]);
    });
  }

  var template = new Template('product/components/bopis/changestoreHtml');
  return template.render(context).text;
}

module.exports = exports = {
  createStoresResultsHtml: base.createStoresResultsHtml,
  getStores: getStores,
  createChangeStoreHtml: createChangeStoreHtml
};
