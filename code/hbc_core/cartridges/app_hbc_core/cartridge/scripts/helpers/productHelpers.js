'use strict';
var base = module.superModule;
var PRODUCT_STANDARD = 'standard';
var Resource = require('dw/web/Resource');

/**
 *@Description - Generating Random Number
 *@return {Object} UUID- Unique ID
 */

base.generateUUID = function () {
  var UUIDUtils = require('dw/util/UUIDUtils');
  var UUID = UUIDUtils.createUUID();
  return UUID;
};

base.discountAppliedInCheckout = function (product) {
  var isDiscountAppliedInCheckout = false;
  if (product && product.promotions && product.promotions.length > 0) {
    product.promotions.every(function (promotion) {
      if (promotion.discountAppliedInCheckout) {
        isDiscountAppliedInCheckout = true;
      }
      return true;
    });
  }
  return isDiscountAppliedInCheckout;
};

base.getPDRestrictedShipTypeText = function (apiProduct) {
  var preferences = require('*/cartridge/config/preferences');
  if (preferences.product.PD_RESTRICTED_SHIP_MAP && apiProduct.custom.pdRestrictedShipTypeText) {
    var pdRestrictedShipTypeTextMap = preferences.product.PD_RESTRICTED_SHIP_MAP;
    var pdRestrictedShipTypeTextMapObj = JSON.parse(pdRestrictedShipTypeTextMap);
    if (pdRestrictedShipTypeTextMapObj) {
      let locale = request.locale; // eslint-disable-line
      var pdRestrictedShipTypeText = apiProduct.custom.pdRestrictedShipTypeText.trim().replace(/\s/g, '');
      if (pdRestrictedShipTypeTextMapObj[pdRestrictedShipTypeText] && pdRestrictedShipTypeTextMapObj[pdRestrictedShipTypeText][locale]) {
        return pdRestrictedShipTypeTextMapObj[pdRestrictedShipTypeText][locale];
      }
    }
  }
  return null;
};

base.getUSPSShipOK = function (apiProduct) {
  if ('uspsShipOk' in apiProduct.custom && apiProduct.custom.uspsShipOk === 'false') {
    return Resource.msg('label.USPS_ShipOK', 'product', null);
  }
  return null;
};

/**
 * Generates a map of string resources for the template
 *
 * @returns {ProductDetailPageResourceMap} - String resource map
 */
function getResources() {
  return {
    info_selectforstock: '',
    assistiveSelectedText: Resource.msg('msg.assistive.selected.text', 'common', null),
    soldout: Resource.msg('product.tile.soldout', 'product', 'sold out'),
    addtocart: Resource.msg('product.tile.addtocart', 'product', 'add to bag'),
    preordertocart: Resource.msg('product.tile.preordertobag', 'product', null),
    limitedInventory: Resource.msg('label.limitedstock', 'product', null),
    movetobag: Resource.msg('wishlist.prod.movetocart', 'wishlist', null),
    addtobag: Resource.msg('wishlist.prod.addtocart', 'wishlist', null)
  };
}

/**
 * Creates the breadcrumbs object
 * @param {string} cgid - category ID from navigation and search
 * @param {string} pid - product ID
 * @param {Array} breadcrumbs - array of breadcrumbs object
 * @returns {Array} an array of breadcrumb objects
 */
function getAllBreadcrumbs(cgid, pid, breadcrumbs) {
  var URLUtils = require('dw/web/URLUtils');
  var CatalogMgr = require('dw/catalog/CatalogMgr');
  var ProductMgr = require('dw/catalog/ProductMgr');

  var category;
  var product;
  if (pid) {
    product = ProductMgr.getProduct(pid);
    category = product.variant ? product.masterProduct.primaryCategory : product.primaryCategory;
  } else if (cgid) {
    category = CatalogMgr.getCategory(cgid);
  }

  if (category) {
    if (!category.custom.hideFromBreadcrumb) {
      breadcrumbs.push({
        htmlValue:
          'categoryNameoverwrite' in category.custom && !!category.custom.categoryNameoverwrite ? category.custom.categoryNameoverwrite : category.displayName,
        url: URLUtils.url('Search-Show', 'cgid', category.ID)
      });
    }
    if (category.parent && category.parent.ID !== 'root') {
      return getAllBreadcrumbs(category.parent.ID, null, breadcrumbs);
    }
  }

  return breadcrumbs;
}

base.showProductPage = function (querystring, reqPageMetaData) {
  var URLUtils = require('dw/web/URLUtils');
  var ProductFactory = require('*/cartridge/scripts/factories/product');
  var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');

  var params = querystring;
  var product = ProductFactory.get(params) || {};
  var addToCartUrl = URLUtils.url('Cart-AddProduct');
  var canonicalUrl = URLUtils.url('Product-Show', 'pid', product.id);
  var breadcrumbs = getAllBreadcrumbs(null, product.id, []);
  if (breadcrumbs.length > 0) {
    breadcrumbs.push({
      htmlValue: Resource.msg('label.search.home', 'search', null),
      url: URLUtils.url('Home-Show')
    });
    breadcrumbs = breadcrumbs.reverse();
  }
  var template = 'product/productDetails';

  if (product.template) {
    template = product.template;
  } else if (product.hbcProductType === 'bridal') {
    template = 'product/productDetails_bridal';
  } else if (product.hbcProductType === 'home') {
    template = 'product/productDetails_homeappliances';
  } else if (product.hbcProductType === 'gwp') {
    template = 'product/productDetails_gwp';
  } else if (product.hbcProductType === 'giftcard') {
    template = 'product/productDetails_giftcard';
  } else if (product.hbcProductType === 'chanel') {
    template = 'product/productDetails_chanel';
  } else if (product.productType === 'bundle' && !product.template) {
    template = 'product/bundleDetails';
  } else if (product.productType === 'set' && !product.template) {
    template = 'product/setDetails';
  }

  pageMetaHelper.setPageMetaData(reqPageMetaData, product);
  pageMetaHelper.setPageMetaTags(reqPageMetaData, product);
  var schemaData = require('*/cartridge/scripts/helpers/structuredDataHelper').getProductSchema(product);

  return {
    template: template,
    product: product,
    addToCartUrl: addToCartUrl,
    resources: getResources(),
    breadcrumbs: breadcrumbs,
    canonicalUrl: canonicalUrl,
    schemaData: schemaData
  };
};

base.getSearchableIfUnavailableFlag = function (apiProduct) {
  var searchableIfUnavailable = false;
  if (apiProduct && apiProduct.searchableIfUnavailableFlag) {
    return true;
  }
  return searchableIfUnavailable;
};

base.getHBCProductType = function (apiProduct) {
  return apiProduct.custom.hbcProductType ? apiProduct.custom.hbcProductType : PRODUCT_STANDARD;
};

base.alwaysEnableSwatches = function (apiProduct) {
  var alwaysEnableSwatches = false;
  if (apiProduct && apiProduct.searchableIfUnavailableFlag && 'hbcProductType' in apiProduct.custom && apiProduct.custom.hbcProductType === 'home') {
    return true;
  }
  return alwaysEnableSwatches;
};

base.isInPurchaselimit = function (purchaselimit, quantity) {
  // Validates for not null and 0, if null default will be true
  if (quantity && (purchaselimit || purchaselimit === 0)) {
    return quantity <= purchaselimit;
  }
  return true;
};

// Utility method to calculate the Hudson Reward Points for PDP and Cart pages
base.getHudsonPoints = function (basketOrProduct, isBasket, apiProdPrice) {
  var preferences = require('*/cartridge/config/preferences');
  // First check the custom preference. Hudson points are only applied to the Bay Site and not O5th.
  if (preferences.HUDSON_REWARD_POINT !== -1) {
    if (isBasket) {
      // If the points are calculated for the cart, only "rewardEligible" products should be counted
      var finalRewardPoints = 0;
      var includedProductPrice = 0;
      var collections = require('*/cartridge/scripts/util/collections');
      // Add prices of all the products which are eligible for reward points.
      collections.forEach(basketOrProduct.productLineItems, function (pli) {
        var masterProduct = pli.product && pli.product.variationModel ? pli.product.variationModel.master : '';
        if (masterProduct && !empty(masterProduct.custom.rewardEligible) && masterProduct.custom.rewardEligible === 'true') {
          includedProductPrice += pli.proratedPrice.value; // Get the adjusted price of the item
        }
      });

      if (includedProductPrice > 0) {
        finalRewardPoints = Math.floor(includedProductPrice * preferences.HUDSON_REWARD_POINT);
      }
      return finalRewardPoints;
    } // Points calculated for a "rewardEligible" product on the PDP page.
    var masterProduct = basketOrProduct && basketOrProduct.variationModel ? basketOrProduct.variationModel.master : '';
    if (masterProduct && !empty(masterProduct.custom.rewardEligible) && masterProduct.custom.rewardEligible === 'true') {
      var estPrice = apiProdPrice.type && apiProdPrice.type === 'range' ? apiProdPrice.max.sales.value : apiProdPrice.sales.value;
      return Math.floor(estPrice * preferences.HUDSON_REWARD_POINT);
    }
    return 0;
  }
  return 0;
};

base.getResources = getResources;

base.getAllBreadcrumbs = getAllBreadcrumbs;

/**
 * Return type of the current product
 * @param  {dw.catalog.ProductVariationModel} product - Current product
 * @return {string} type of the current product
 */
base.getProductType = function (product) {
  var result;
  if (product) {
    if (product.master) {
      result = 'master';
    } else if (product.variant) {
      result = 'variant';
    } else if (product.variationGroup) {
      result = 'variationGroup';
    } else if (product.productSet) {
      result = 'set';
    } else if (product.bundle) {
      result = 'bundle';
    } else if (product.optionProduct) {
      result = 'optionProduct';
    } else {
      result = 'standard';
    }
  }
  return result;
};

// This method is overided since setSelectedAttributeValue method should be called only for master/variation group not on variant
base.getVariationModel = function (product, productVariables) {
  var collections = require('*/cartridge/scripts/util/collections');
  var variationModel = product.variationModel;
  if (!variationModel.master && !variationModel.selectedVariant) {
    variationModel = null;
  } else if (productVariables && !product.variant) {
    var variationAttrs = variationModel.productVariationAttributes;
    Object.keys(productVariables).forEach(function (attr) {
      if (attr && productVariables[attr].value) {
        var dwAttr = collections.find(variationAttrs, function (item) {
          return item.ID === attr;
        });
        var dwAttrValue = collections.find(variationModel.getAllValues(dwAttr), function (item) {
          return item.value === productVariables[attr].value;
        });
        if (dwAttr && dwAttrValue) {
          variationModel.setSelectedAttributeValue(dwAttr.ID, dwAttrValue.ID);
        }
      }
    });
  }
  return variationModel;
};

module.exports = base;
