'use strict';
var URLUtils = require('dw/web/URLUtils');
var StoreMgr = require('dw/catalog/StoreMgr');
var StoreModel = require('*/cartridge/models/store');
var preferences = require('*/cartridge/config/preferences');
var Resource = require('dw/web/Resource');
var instorePUstoreHelpers = require('*/cartridge/scripts/helpers/instorePickupStoreHelpers');
var Logger = require('dw/system/Logger');

/**
 * Returns the store if available from session or with a store search on the Geolocation
 * @param {Object} product - product model
 * @returns {Object} defaultStore - store model
 */
function getDefaultStore(product) {
  var defaultStore = null;
  var stores = null;
  var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');
  var defaultSearchRadius = 15;
  // get store from session
  var sessionStore = session.custom.store ? JSON.parse(session.custom.store) : '';
  var sessionStoreId = sessionStore && sessionStore.id && sessionStore.distance ? sessionStore.id : null;
  if (sessionStoreId) {
    defaultStore = StoreMgr.getStore(sessionStoreId);
    defaultStore = new StoreModel(defaultStore);
    defaultStore.distanceInUnits = sessionStore.distance;
  }
  // fall back, search store
  if (!defaultStore) {
    stores = storeHelpers.getStores(defaultSearchRadius, null, null, null, request.geolocation);
    if (stores && stores.stores && stores.stores.length > 0) {
      defaultStore = stores.stores[0];
    }
  }
  // set inventory
  if (defaultStore && product) {
    // property is defined without a prototype since no qualifiers are required
    defaultStore.unitsAtStores = instorePUstoreHelpers.getStoreInventory(defaultStore.ID, product.id);
  }
  return defaultStore;
}

/**
 * Check if request has store id associated
 * @param {Object} req - request object
 * @returns {boolean} filterApplied - return fileter applied
 */
function isStoreFilterApplied(req) {
  var filterApplied = false;
  Object.keys(req.querystring).forEach(function (element) {
    if (element.indexOf('storeid') > -1) {
      filterApplied = true;
    }
  });
  return filterApplied;
}

/**
 * Add or remove the store id in the url
 * @param {dw.web.URLUtils} refinementUrl - request url
 * @param {Object} store - store object
 * @param {boolean} isFilterApplied - if request has store id associated
 * @returns {dw.web.URLUtils} refinementUrl - url with store id added
 */
function toggleStoreRefinement(refinementUrl, store, isFilterApplied) {
  if (refinementUrl && store && !isFilterApplied) {
    refinementUrl.append('storeid', store.ID);
  } else {
    refinementUrl.remove('storeid');
  }
  return refinementUrl;
}

/**
 * Set quantity messaging based on the selected and available units
 * @param {Object} storeObj - store model
 * @param {Object} product - product model
 * @returns {string} quantity messaging
 */
function setAvailabilityMessage(storeObj, product) {
  var msg = Resource.msg('store.unitsavailable.unavailable', 'storeLocator', null);
  // site preference for limited quantity message
  var limit = preferences.limitedQtyThreshold;
  if (storeObj && storeObj.unitsAtStores) {
    if (storeObj.unitsAtStores === 0 || storeObj.unitsAtStores < product.selectedQuantity) {
      msg = Resource.msg('store.unitsavailable.unavailable', 'storeLocator', null);
    } else if (storeObj.unitsAtStores > 0 && storeObj.unitsAtStores < limit) {
      msg = Resource.msg('store.unitsavailable.limited', 'storeLocator', null);
    } else {
      msg = Resource.msg('store.unitsavailable.available', 'storeLocator', null);
    }
  }
  return msg;
}

/**
 * Converts string in to lowercase, UPPERCASE and camelCase
 * @param {Sting} str - string to be converted
 * @returns {Object} containg all the version of a string
 */
function convertCases(str) {
  var formattedString = {};
  if (str) {
    formattedString.lowercase = str.toString().trim().toLowerCase();
    formattedString.uppercase = str.toString().trim().toUpperCase();
    formattedString.camelcase = formattedString.lowercase.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
      return index === 0 ? word.toUpperCase() : word.toUpperCase();
    });
  }
  return formattedString;
}

/**
 * Sets view data required to render shipping options
 * @param {httpRequest} req - current request
 * @param {httpResponse} res - current response
 */
function setBopisViewData(req, res) {
  var viewData = res.getViewData();
  var product = viewData.product;
  var defaultStore = null;
  viewData.isBopisEnabled = preferences.isBopisEnabled && product.isAvailableForInstore;
  viewData.storeModalUrl = URLUtils.url('Stores-InitSearch').toString();
  viewData.setStoreUrl = URLUtils.url('Stores-SetStore').toString();
  // bopis check at site and product level
  if (preferences.isBopisEnabled && product.isAvailableForInstore) {
    defaultStore = getDefaultStore(product);
    if (defaultStore) {
      viewData.storeInfo = defaultStore;
      // set quantity message
      viewData.availabilityMessage = Resource.msgf(
        'shippingoption.product.availableat',
        'storeLocator',
        null,
        setAvailabilityMessage(defaultStore, product),
        defaultStore.name
      );
      if (product && product.productType === 'variant') {
        // set available units
        viewData.unitsAtStores = instorePUstoreHelpers.getStoreInventory(defaultStore.ID, req.querystring.productid);
      }
    }
  }

  res.setViewData(viewData);
}

/**
 * Sets store id and distance at session
 * @param {string} storeid - Store ID
 * @param {string} storedistance - Store distance
 */
function setStoreFromSession(storeid, storedistance) {
  var store = {};
  if (storeid && storedistance) {
    store.id = storeid;
    store.distance = storedistance;
    if (Logger.isWarnEnabled()) {
      // Extra logging for SFDEV-10229
      Logger.warn('SFDEV-10229 - bopisHelpers.js/setStoreFromSession: \nSelected storeId={0}', storeid ? store.id : '');
    }
    session.custom.store = JSON.stringify(store);
  }
}

/**
 * Returns object to set
 * @param {dw.catalog.Store} store - store set to PLI
 * @param {Object} data - custom object
 * @returns {Object} data - custom object
 */
function setShippingOptionData(store, lineItem) {
  var data = {};
  var inStoreFieldLabel = Resource.msg('shippingoption.product.pickupinstore', 'storeLocator', null);
  // assign new element to display limited inventory messaging when the option is instore toggled
  var inventoryList = lineItem.productInventoryList;
  var inventoryRecord = inventoryList ? inventoryList.getRecord(lineItem.productID) : lineItem.product.availabilityModel.inventoryRecord;
  var thresholdValue = preferences.product.INVENTORY_THRESHOLD_VALUE;
  if (thresholdValue && inventoryRecord && inventoryRecord.ATS.value <= thresholdValue) {
    data.limitedInventory = true;
  } else {
    data.limitedInventory = false;
  }
  if (store) {
    data.storeId = store.ID;
    data.storeName = store.name;
    inStoreFieldLabel = Resource.msgf('shippingoption.product.instore', 'storeLocator', null, store.name);
  }
  data.inStoreFieldLabel = inStoreFieldLabel;

  if (Logger.isWarnEnabled()) {
    // Extra logging for SFDEV-10229
    Logger.warn(
      'SFDEV-10229 - bopisHelpers.js/setShippingOptionData: \nPLI with UUID={0},\n Chosen storeID={1},\n Limited inventory flag={2},\n InStore field label={3}',
      lineItem.getUUID(),
      store ? store.ID : '',
      data.limitedInventory,
      data.inStoreFieldLabel
    );
  }

  return data;
}

module.exports = {
  getDefaultStore: getDefaultStore,
  setBopisViewData: setBopisViewData,
  convertCases: convertCases,
  setStoreFromSession: setStoreFromSession,
  setAvailabilityMessage: setAvailabilityMessage,
  setShippingOptionData: setShippingOptionData,
  isStoreFilterApplied: isStoreFilterApplied,
  toggleStoreRefinement: toggleStoreRefinement
};
