'use strict';

/**
 * Function to build alternate URLs for using with HREFLANG
 *
 *
 * @param {string} action - req action
 * @param {Object} request - current request
 * @returns {dw/util/HashMap} - alternate url map
 */
function getAlternateURLs(action, request) {
  var HashMap = require('dw/util/HashMap');
  var Site = require('dw/system/Site');
  var URLAction = require('dw/web/URLAction');
  var preferences = require('*/cartridge/config/preferences');
  var Logger = require('dw/system/Logger');
  var alternateURLs = new HashMap();
  if (action && request) {
    var currentSite = Site.getCurrent();
    var parameterMap = request.httpParameterMap;
    var collections = require('*/cartridge/scripts/util/collections');
    var URLParameter = require('dw/web/URLParameter');
    var URLUtils = require('dw/web/URLUtils');
    collections.forEach(currentSite.getAllowedLocales(), function (locale) {
      // get hostname from preference
      var hostName = preferences.hostRedirectMap[locale];
      var args = [new URLAction(action, currentSite.name, locale)];
      try {
        if (hostName && hostName.length > 0) {
          args = [new URLAction(action, currentSite.name, locale, hostName)];
        }
      } catch (e) {
        Logger.error('Error execution SEOHelper.js : ' + e);
      }

      if (request.httpMethod === 'GET') {
        // eslint-disable-next-line no-restricted-syntax
        for (var p in parameterMap) {
          if (parameterMap.isParameterSubmitted(p)) {
            // ignore the lang parameter
            if (p !== 'lang' && p !== 'locale') {
              args.push(new URLParameter(p, parameterMap.get(p)));
            }
          }
        }

        let alternateURL = URLUtils.abs.apply(null, args);
        alternateURLs.put(locale, alternateURL);
      }
    });
  }

  return alternateURLs;
}

module.exports = {
  getAlternateURLs: getAlternateURLs
};
