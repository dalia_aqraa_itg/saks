'use strict';

function isPreorder(product) {
  var flag = false;
  if (product && 'sfccPreorder' in product.custom && product.custom.sfccPreorder == 'T') {
    var availabilityModel = product.availabilityModel;
    // null check for availability
    if (availabilityModel) {
      var inventoryRecord = availabilityModel.inventoryRecord;
      if (inventoryRecord) {
        // the inventory record exists, we should evaluate it to HBC-preorder
        // SFCC backorder is treated as HBC-preorder
        if (inventoryRecord.backorderable || inventoryRecord.preorderable) {
          // the inventory record is backorderable
          var allocation = inventoryRecord.preorderBackorderAllocation.value;
          if (allocation > 0) {
            flag = true;
          }
          // end of backorderable if
        }
        if (
          inventoryRecord.ATS &&
          inventoryRecord.preorderBackorderAllocation &&
          inventoryRecord.ATS.value - inventoryRecord.preorderBackorderAllocation.value > 0
        ) {
          flag = false;
        }
      }
    }
  }
  return flag;
}

function updateInventoryStatus(LineItemCtnr) {
  if (LineItemCtnr) {
    var plis = LineItemCtnr.getProductLineItems();
    var Transaction = require('dw/system/Transaction');
    var collections = require('*/cartridge/scripts/util/collections');
    collections.forEach(plis, function (pli) {
      Transaction.wrap(function () {
        if (pli.product && isPreorder(pli.product)) {
          pli.custom.inventoryStatus = 'PREORDER';
        } else {
          pli.custom.inventoryStatus = 'REGULAR';
        }
      });
    });
  }
}

module.exports = {
  isPreorder: isPreorder,
  updateInventoryStatus: updateInventoryStatus
};
