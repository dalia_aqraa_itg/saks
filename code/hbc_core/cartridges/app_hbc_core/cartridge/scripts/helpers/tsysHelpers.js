'use strict';

var Site = require('dw/system/Site');
var customPreferences = Site.current.preferences.custom;

/**
 * Checks TSYS Mode
 * @returns {boolean} return true if TSYS enabled
 */
function isTsysMode() {
  return session.custom.tsysPreview == true || customPreferences.tsysPhase2 == true;
}

/**
 * Check Site Banner
 * @returns {boolean} returns true if the current site is The Bay
 */
function isTsysBay() {
  return Site.getCurrent().ID.toLowerCase() === 'thebay';
}

/* Checks Dual Acceptance Mode Saks
 * @returns {boolean} return true if TSYS enabled
 */
function isDualAcceptanceSaks() {
  var isProduction = dw.system.System.instanceType === dw.system.System.PRODUCTION_SYSTEM;
  return !isProduction && (session.custom.tsysPreview == true || customPreferences.dualAcceptanceEndSaks == true);
}

module.exports = {
  isTsysMode: isTsysMode,
  isTsysBay: isTsysBay,
  isDualAcceptanceSaks: isDualAcceptanceSaks
};
