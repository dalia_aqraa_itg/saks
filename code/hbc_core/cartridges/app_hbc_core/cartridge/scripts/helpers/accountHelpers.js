'use strict';
var URLUtils = require('dw/web/URLUtils');
var endpoints = require('*/cartridge/config/oAuthRenentryRedirectEndpoints');
var preferences = require('*/cartridge/config/preferences');
var cnsUtil = require('*/cartridge/scripts/email/cnsUtil');
var cnsConstants = preferences.cnsSubtypes ? JSON.parse(preferences.cnsSubtypes) : {};

/**
 * Fetch the country code for email preferred language selected
 *
 * @param {string} lang - Selected language preference
 * @returns {string} - locale country code
 */
function fetchLanguage(lang) {
  var Locale = require('dw/util/Locale');
  let country = '';
  let langPref = preferences.emailLangPrefLocaleMapping;
  if (lang && Object.prototype.hasOwnProperty.call(langPref, lang)) {
    country = Locale.getLocale(langPref[lang]).getLanguage();
  }
  return country;
}
/**
 * Creates an account model for the current customer
 * @param {string} redirectUrl - rurl of the req.querystring
 * @param {string} privacyCache - req.session.privacyCache
 * @param {boolean} newlyRegisteredUser - req.session.privacyCache
 * @returns {string} a redirect url
 */
function getLoginRedirectURL(redirectUrl, privacyCache, newlyRegisteredUser) {
  var endpoint = 'Account-Show';
  var targetEndPoint = redirectUrl ? parseInt(redirectUrl, 10) : 1;

  var registered = newlyRegisteredUser ? 'submitted' : 'false';

  var argsForQueryString = privacyCache.get('args');

  var tokenForQueryString = privacyCache.get('Token');

  if (targetEndPoint && endpoints[targetEndPoint]) {
    endpoint = endpoints[targetEndPoint];
  }

  return URLUtils.url(
    endpoint,
    'registration',
    registered,
    argsForQueryString ? 'args' : '',
    argsForQueryString ? argsForQueryString : '',
    tokenForQueryString ? 'Token' : '',
    tokenForQueryString ? tokenForQueryString : ''
  )
    .relative()
    .toString();
}

/**
 * Send an email that would notify the user that account was created
 * @param {dw/customer/Profile} registeredUser - object that contains user's email address and name information.
 * @param {string} language - language of the user.
 */
function sendCreateAccountEmail(registeredUser, language) {
  if (preferences.enableCNS) {
    var customerProfile = {
      email: registeredUser.email,
      firstName: registeredUser.firstName,
      lastName: registeredUser.lastName
    };
    var parameters = {
      current_email: registeredUser.email
    };

    let prefLang = fetchLanguage(registeredUser.custom.preferredLanguage) || language;
    cnsUtil.sendEmail(customerProfile, parameters, cnsConstants.accountCreation, prefLang);
  }
}

/**
 * Gets the password reset token of a customer
 * @param {dw/customer/Customer} customer - the customer requesting password reset token
 * @returns {string} password reset token string
 */
function getPasswordResetToken(customer) {
  var Transaction = require('dw/system/Transaction');

  var passwordResetToken;
  Transaction.wrap(function () {
    passwordResetToken = customer.profile.credentials.createResetPasswordToken();
  });
  return passwordResetToken;
}

/**
 * Sends the email with password reset instructions
 * @param {dw/customer/Customer} resettingCustomer - the customer requesting password reset
 * @param {string} language - language of the user.
 */
function sendPasswordResetEmail(resettingCustomer, language) {
  if (preferences.enableCNS) {
    var customerProfile = {
      email: resettingCustomer.profile.email,
      firstName: resettingCustomer.profile.firstName,
      lastName: resettingCustomer.profile.lastName
    };
    var passwordResetToken = getPasswordResetToken(resettingCustomer);
    var parameters = {
      reset_password_link: URLUtils.https('Account-SetNewPassword', 'Token', passwordResetToken).toString()
    };
    let prefLang = fetchLanguage(resettingCustomer.profile.custom.preferredLanguage) || language;
    cnsUtil.sendEmail(customerProfile, parameters, cnsConstants.forgotPassword, prefLang);
  }
}

/**
 * Sends the email with password reset instructions
 * @param {dw/customer/Customer} resettingCustomer - the customer requesting password reset
 * @param {string} passwordResetToken - the customers password reset token
 * @param {string} language - language of the user.
 * @returns {Object} status - CNS sent email status
 */
function sendPasswordResetEmailForOCAPI(resettingCustomer, passwordResetToken, language) {
  var status;
  if (preferences.enableCNS) {
    var customerProfile = {
      email: resettingCustomer.profile.email,
      firstName: resettingCustomer.profile.firstName,
      lastName: resettingCustomer.profile.lastName
    };
    var parameters = {
      reset_password_link: URLUtils.https('Account-SetNewPassword', 'Token', passwordResetToken).toString()
    };
    let prefLang = fetchLanguage(resettingCustomer.profile.custom.preferredLanguage) || language;
    status = cnsUtil.sendEmail(customerProfile, parameters, cnsConstants.forgotPassword, prefLang);
  }
  return status;
}

/**
 * Send an email that would notify the user that account email was edited
 * @param {dw/customer/Profile} profile - object that contains user's profile information.
 * @param {string} previousEmail - object that contains user's previous email.
 * @param {string} language - language of the user.
 */
function sendAccountEditedEmail(profile, previousEmail, language) {
  if (preferences.enableCNS) {
    var customerProfile = {
      email: profile.email,
      firstName: profile.firstName,
      lastName: profile.lastName
    };
    var parameters = {
      current_email: profile.email,
      old_email: previousEmail
    };
    let prefLang = fetchLanguage(profile.custom.preferredLanguage) || language;
    cnsUtil.sendEmail(customerProfile, parameters, cnsConstants.changeOfEmail, prefLang);
  }
}

/**
 * Send an email that would notify the user that account address was edited
 * @param {dw/customer/Profile} profile - object that contains user's profile information.
 * @param {string} language - language of the user.
 */
function sendEditAddressEmail(profile, language) {
  if (preferences.enableCNS) {
    var customerProfile = {
      email: profile.email,
      firstName: profile.firstName,
      lastName: profile.lastName
    };
    var parameters = {};
    let prefLang = fetchLanguage(profile.custom.preferredLanguage) || language;
    cnsUtil.sendEmail(customerProfile, parameters, cnsConstants.updateShipping, prefLang);
  }
}

/**
 * Send an email that would notify the user that account password was updated
 * @param {Object} profile - object that contains user's profile information.
 * @param {string} language - language of the user.
 */
function sendUpdatePasswordEmail(profile, language) {
  if (preferences.enableCNS) {
    var customerProfile = {
      email: profile.email,
      firstName: profile.firstName,
      lastName: profile.lastName
    };
    var parameters = {
      current_email: profile.email
    };
    let prefLang = fetchLanguage(profile.custom.preferredLanguage) || language;
    cnsUtil.sendEmail(customerProfile, parameters, cnsConstants.updatePassword, prefLang);
  }
}

/**
 * Send an email that would notify the user that account was locaked due to numerous invalid attempts
 * @param {dw/customer/Profile} profile - object that contains user's profile information.
 * @param {string} language - language of the user.
 */
function sendAccountLockedEmail(profile, language) {
  if (preferences.enableCNS) {
    var customerProfile = {
      email: profile.email,
      firstName: profile.firstName,
      lastName: profile.lastName
    };
    var parameters = {};
    let prefLang = fetchLanguage(profile.custom.preferredLanguage) || language;
    cnsUtil.sendEmail(customerProfile, parameters, cnsConstants.lockout, prefLang);
  }
}

/**
 * @param {Object} currentCustomer - the customer get address
 * @param {Object} addressBook - the customer addressBook
 * @returns {string} addressHtml returning address
 */
function getAddressHtml(currentCustomer, addressBook) {
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var actionUrls = {
    deleteActionUrl: URLUtils.url('Address-DeleteAddress').toString(),
    listActionUrl: URLUtils.url('Address-List').toString()
  };

  var addressBookAjaxTemplate = 'account/addressBookAjax';

  var addressContext = {
    addressBook: addressBook,
    actionUrls: actionUrls
  };

  var addressHtml = renderTemplateHelper.getRenderedHtml(addressContext, addressBookAjaxTemplate);
  return addressHtml;
}

/**
 * update customer preferences based on the setting selected by customer
 *
 * @param {Object} req - request object
 * @param {string} email - customer email
 * @param {dw.customer.Profile} profile - Customer profile
 * @param {Object} preferencesForm - preferences form object set by customer
 */
function updateCustomerPreferences(req, email, profile, preferencesForm) {
  var Transaction = require('dw/system/Transaction');
  var CustomObjectMgr = require('dw/object/CustomObjectMgr');
  try {
    // Create/Update  Subscription Object
    Transaction.wrap(function () {
      var subscribeCO = CustomObjectMgr.getCustomObject('Subscriptions', email);
      if (!subscribeCO) {
        subscribeCO = CustomObjectMgr.createCustomObject('Subscriptions', email);
      }
      subscribeCO.custom.sourceId = 'userAccount';
      subscribeCO.custom.exported = false;
      subscribeCO.custom.firstName = profile.firstName;
      subscribeCO.custom.lastName = profile.lastName;

      if (profile.addressBook.preferredAddress) {
        var preferredAddr = profile.addressBook.preferredAddress;
        subscribeCO.custom.address = preferredAddr.address1;
        if (preferredAddr.address2) {
          subscribeCO.custom.address = preferredAddr.address2;
        }
        subscribeCO.custom.city = preferredAddr.city;
        subscribeCO.custom.state = preferredAddr.stateCode;
        subscribeCO.custom.zipFull = preferredAddr.postalCode;
        subscribeCO.custom.country = preferredAddr.countryCode.value;
      }
      subscribeCO.custom.phone = profile.phoneHome ? profile.phoneHome : '';
      subscribeCO.custom.subscribedOrUnsubscribed = new Date();
      subscribeCO.custom.language = req.locale.id;
      subscribeCO.custom.banner = preferences.dataSubscription.BANNER;
      if (preferences.dataSubscription.canadaOptAllowed) {
        subscribeCO.custom.canadaFlag = preferencesForm.isCanadianCustomer === 'T' ? 'Y' : 'N';
      }
      if ('hudsonReward' in profile.custom && !empty(profile.custom.hudsonReward)) {
        subscribeCO.custom.hbcRewardsNumber = profile.custom.hudsonReward;
      }
      if ('moreNumber' in profile.custom && !empty(profile.custom.moreNumber)) {
        subscribeCO.custom.moreNumber = profile.custom.moreNumber;
      }

      // Opt In/out Logic
      /**
             *  If customer opts for email subscription, we need to send it as Y.
                If customer doesn’t opt for email subscription, send blank.
                If customer tries to change from opt-in to opt-out, send N
                If Customer tries to change from Opt-out to opt in, send Y
                If customer retains the opt-out status as is, then send blank.
             */

      if (preferences.dataSubscription.siteType === 'SAKS') {
        if ((empty(profile.custom.saksOptIn) || !profile.custom.saksOptIn) && preferencesForm.saksOptIn) {
          // Customer wants to opt-in
          subscribeCO.custom.offFiveThOptStatus = 'Y';
          // eslint-disable-next-line no-param-reassign
          profile.custom.saksOptIn = true;
        } else if (!empty(profile.custom.saksOptIn) && profile.custom.saksOptIn && empty(preferencesForm.saksOptIn)) {
          // Customer want to opt-out
          subscribeCO.custom.offFiveThOptStatus = 'N';
          // eslint-disable-next-line no-param-reassign
          profile.custom.saksOptIn = false;
        } else if (empty(preferencesForm.saksOptIn)) {
          // Customer want to opt-out
          subscribeCO.custom.offFiveThOptStatus = 'N';
        }
        if ((empty(profile.custom.saksOptInCA) || !profile.custom.saksOptInCA) && preferencesForm.saksOptInCA) {
          // Customer wants to opt-in
          subscribeCO.custom.offFiveThCanadaOptStatus = 'Y';
          // eslint-disable-next-line no-param-reassign
          profile.custom.saksOptInCA = true;
        } else if (!empty(profile.custom.saksOptInCA) && profile.custom.saksOptInCA && empty(preferencesForm.saksOptInCA)) {
          // Customer want to opt-out
          subscribeCO.custom.offFiveThCanadaOptStatus = 'N';
          // eslint-disable-next-line no-param-reassign
          profile.custom.saksOptInCA = false;
        }

        if ((empty(profile.custom.saksAvenueOptIn) || !profile.custom.saksAvenueOptIn) && preferencesForm.saksAvenueOptIn) {
          // Customer wants to opt-in
          subscribeCO.custom.saksOptStatus = 'Y';
          // eslint-disable-next-line no-param-reassign
          profile.custom.saksAvenueOptIn = true;
        } else if (!empty(profile.custom.saksAvenueOptIn) && profile.custom.saksAvenueOptIn && empty(preferencesForm.saksAvenueOptIn)) {
          // Customer want to opt-out
          subscribeCO.custom.saksOptStatus = 'N';
          // eslint-disable-next-line no-param-reassign
          profile.custom.saksAvenueOptIn = false;
        }

        if ((empty(profile.custom.saksAvenueOptInCA) || !profile.custom.saksAvenueOptInCA) && preferencesForm.saksAvenueOptInCA) {
          // Customer wants to opt-in
          subscribeCO.custom.saksCanadaOptStatus = 'Y';
          // eslint-disable-next-line no-param-reassign
          profile.custom.saksAvenueOptInCA = true;
        } else if (!empty(profile.custom.saksAvenueOptInCA) && profile.custom.saksAvenueOptInCA && empty(preferencesForm.saksAvenueOptInCA)) {
          // Customer want to opt-out
          subscribeCO.custom.saksCanadaOptStatus = 'N';
          // eslint-disable-next-line no-param-reassign
          profile.custom.saksAvenueOptInCA = false;
        }
        // If Customer opted for all Saks sites
        if (
          !empty(profile.custom.saksOptIn) &&
          profile.custom.saksOptIn &&
          !empty(profile.custom.saksOptInCA) &&
          profile.custom.saksOptInCA &&
          !empty(profile.custom.saksAvenueOptIn) &&
          profile.custom.saksAvenueOptIn &&
          !empty(profile.custom.saksAvenueOptInCA) &&
          profile.custom.saksAvenueOptInCA
        ) {
          subscribeCO.custom.saksFamilyOptStatus = 'N';
        }
      }
      if ((empty(profile.custom.emailOptIn) || !profile.custom.emailOptIn) && preferencesForm.addtoemaillist) {
        // Customer wants to opt-in
        if (preferences.dataSubscription.siteType === 'SAKS') {
          subscribeCO.custom.theBayOptStatus = 'N';
          subscribeCO.custom.offFiveThOptStatus = 'Y';
        } else {
          subscribeCO.custom.theBayOptStatus = 'Y';
        }
        // eslint-disable-next-line no-param-reassign
        profile.custom.emailOptIn = true;
      } else if (!empty(profile.custom.emailOptIn) && profile.custom.emailOptIn && empty(preferencesForm.addtoemaillist)) {
        // Customer want to opt-out
        subscribeCO.custom.theBayOptStatus = 'N';
        // eslint-disable-next-line no-param-reassign
        profile.custom.emailOptIn = false;
      } else if (!empty(profile.custom.emailOptIn)) {
        // Customer want to opt-out
        subscribeCO.custom.theBayOptStatus = profile.custom.emailOptIn ? 'Y' : 'N';
      } else {
        // Customer want to opt-out
        subscribeCO.custom.theBayOptStatus = 'N';
      }
      // While Registration, If Customer is opted for Canadian sites, set for CA preference.
      if (
        !empty(preferencesForm.addtoemaillist) &&
        preferencesForm.addtoemaillist &&
        !empty(preferencesForm.isCanadianCustomer) &&
        preferencesForm.isCanadianCustomer === 'T'
      ) {
        subscribeCO.custom.offFiveThCanadaOptStatus = 'Y';
        profile.custom.saksOptInCA = true;
        subscribeCO.custom.saksCanadaOptStatus = 'Y';
        profile.custom.saksAvenueOptInCA = true;
      }
    });
  } catch (e) {
    var Logger = require('dw/system/Logger');
    Logger.error('Error: Error while updating Opt-in/out for Customer', e);
  }
}

/**
 * Updates the Account Last Modified Date custom attribute, as the system attribute updates the date as the user logins to the system.
 * It doesn't become true last Modified Date. The system attribute of lastModified is updated for customer object being modified in backend.
 *
 * @param {dw.customer.Customer} customer - customer object
 */
function updateAccLastModifiedDate(customer, comingFrom) {
  var date = new Date();
  var Transaction = require('dw/system/Transaction');
  if (customer && customer.authenticated) {
    var profile = customer.profile;
    Transaction.wrap(function () {
      profile.custom.accountModifiedDate = date.toISOString();
    });
  } else if (customer && !empty(comingFrom) && comingFrom === 'forgotPassword') {
    var profile = customer.profile;
    Transaction.wrap(function () {
      profile.custom.accountModifiedDate = date.toISOString();
    });
  }
}

/**
 *
 * @param {dw.customer.Profile} profile - customer profile
 * @returns {Object} customers's preferences object based on its setting
 */
function getCustomerPreferences(profile) {
  var customerPreferences = {};

  if (profile && 'preferredLanguage' in profile.custom && profile.custom.preferredLanguage) {
    customerPreferences.preferredLanguage = profile.custom.preferredLanguage.value;
  }

  if (profile && 'saksOptIn' in profile.custom && profile.custom.saksOptIn) {
    customerPreferences.saksOptIn = profile.custom.saksOptIn;
  }

  if (profile && 'saksOptInCA' in profile.custom && profile.custom.saksOptInCA) {
    customerPreferences.saksOptInCA = profile.custom.saksOptInCA;
  }
  if (profile && 'saksAvenueOptIn' in profile.custom && profile.custom.saksAvenueOptIn) {
    customerPreferences.saksAvenueOptIn = profile.custom.saksAvenueOptIn;
  }
  if (profile && 'saksAvenueOptInCA' in profile.custom && profile.custom.saksAvenueOptInCA) {
    customerPreferences.saksAvenueOptInCA = profile.custom.saksAvenueOptInCA;
  }

  if (profile && 'emailOptIn' in profile.custom && profile.custom.emailOptIn) {
    customerPreferences.emailOptIn = profile.custom.emailOptIn;
  }

  return customerPreferences;
}

/**
 * Function to check if any of preference center data saved on customer profile and return TRUE ? FALSE
 * Custom attributes to search SizePreferences , CategoryPreferences , DesignerPreferences
 * @param {dw.customer.Customer} customer - customer object
 * @returns {boolean}
 */
function preferenceDataExist(customer) {
  if (customer.custom.DesignerPreferences && customer.custom.DesignerPreferences !== '{}') {
    // Adding a check if data = {} - This means all the added prefernces are deleted
    return true;
  } else if (customer.custom.CategoryPreferences && customer.custom.CategoryPreferences !== '{}') {
    return true;
  } else if (customer.custom.SizePreferences && customer.custom.SizePreferences !== '{}') {
    return true;
  } else {
    return false;
  }
}

module.exports = {
  getLoginRedirectURL: getLoginRedirectURL,
  sendCreateAccountEmail: sendCreateAccountEmail,
  sendPasswordResetEmail: sendPasswordResetEmail,
  sendPasswordResetEmailForOCAPI: sendPasswordResetEmailForOCAPI,
  sendAccountEditedEmail: sendAccountEditedEmail,
  sendEditAddressEmail: sendEditAddressEmail,
  sendUpdatePasswordEmail: sendUpdatePasswordEmail,
  sendAccountLockedEmail: sendAccountLockedEmail,
  getAddressHtml: getAddressHtml,
  updateCustomerPreferences: updateCustomerPreferences,
  getCustomerPreferences: getCustomerPreferences,
  updateAccLastModifiedDate: updateAccLastModifiedDate,
  preferenceDataExist: preferenceDataExist
};
