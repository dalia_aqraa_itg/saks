'use strict';

/**
 * The first-time login process for a legacy customer will be seamless to the user.
 * User will not be required to reset their passwords after migration to the new Site.
 * All the legacy customers will be migrated with the same default password and "isLegacy" flag attribute.
 * The actual password will be converted into a hash using an algorithm and saved to another custom attribute in the profile.
 *
 * @module cartridge/scripts/helpers/legacyCustomerLogin
 */

var WeakMessageDigest = require('dw/crypto/WeakMessageDigest');
var Bytes = require('dw/util/Bytes');
var CustomerMgr = require('dw/customer/CustomerMgr');
var Transaction = require('dw/system/Transaction');
var Logger = require('dw/system/Logger');
var Resource = require('dw/web/Resource');

/**
 * This method hashes the input password and compares it with the
 * legacyHash value stored in the customer profile.
 * @param {string} hashedLegacyPwd - hashedLegacyPwd
 * @param {string} unhashedPwd - unhashedPwd
 * @returns {boolean} match result of the hashed pwd and input password
 */
function isLegacyPassworMatched(hashedLegacyPwd, unhashedPwd) {
  var getInt = function getInt(b) {
    if (b < 0) b = 127 - b; // eslint-disable-line
    return b;
  };

  var encodeChars = function (bytes) {
    var buf = new String(); // eslint-disable-line
    for (var i = 0; i < bytes.length; i++) {
      var num = getInt(bytes.byteAt(i));
      buf += ((num & 0xf0) >> 4).toString(16);
      buf += (num & 0x0f).toString(16);
    }
    return buf;
  };

  var hashedStr = encodeChars(new WeakMessageDigest(WeakMessageDigest.DIGEST_SHA).digestBytes(new Bytes(unhashedPwd)));

  return hashedStr.equalsIgnoreCase(hashedLegacyPwd);
}

/**
 * This method hashes the new input password for reset password flow.
 * @param {string} unhashedPwd - unhashedPwd
 * @returns {string} hashedStr
 */
function getPasswordLegacyHashed(unhashedPwd) {
  var getInt = function getInt(b) {
    if (b < 0) b = 127 - b; // eslint-disable-line
    return b;
  };

  var encodeChars = function (bytes) {
    var buf = new String(); // eslint-disable-line
    for (var i = 0; i < bytes.length; i++) {
      var num = getInt(bytes.byteAt(i));
      buf += ((num & 0xf0) >> 4).toString(16);
      buf += (num & 0x0f).toString(16);
    }
    return buf;
  };

  var hashedStr = encodeChars(new WeakMessageDigest(WeakMessageDigest.DIGEST_SHA).digestBytes(new Bytes(unhashedPwd)));

  return hashedStr;
}

/**
 * Update the actual password in the legacy customer’s profile
 * @param {dw.customer.Customer} customer - Class Customer Object dw.customer.Customer Represents a customer
 * @param {string} password - password of the customer
 * @returns {Status} status
 */
function setLegacyPwd(customer, password) {
  var status;
  Transaction.wrap(function () {
    var credentials = customer.profile.credentials;
    var token = credentials.createResetPasswordToken();
    status = credentials.setPasswordWithToken(token, password);
  });
  return status;
}

/**
 * Remove the legacy hash and set isLegacy flag to false after password update
 * @param {dw.customer.Customer} customer - Class Customer Object dw.customer.Customer Represents a customer
 */
function removeLegacyFlag(customer) {
  Transaction.wrap(function () {
    delete customer.profile.custom.legacyPasswordHash; // eslint-disable-line
  });
}

/**
 * Update the actual password in the legacy customer’s profile
 * @param {string} username - username i.e. email of the customer
 * @param {string} password - password of the customer
 * @param {boolean} rememberMe - rememberMe preference selected by customer
 * @returns {dw.customer.Customer} customer
 */
function loginCustomer(username, password, rememberMe) {
  var result;
  Transaction.wrap(function () {
    var authStatus = CustomerMgr.authenticateCustomer(username, password);
    result = CustomerMgr.loginCustomer(authStatus, rememberMe);
  });

  return result;
}

/**
 * Match the legacy hash, login the customer using default password and then update the password.
 * @param {dw.customer.Customer} customer - Class Customer Object dw.customer.Customer Represents a customer
 * @param {string} hashedLegacyPwd - hashedLegacyPwd
 * @param {string} email - email i.e. email of the customer
 * @param {string} password - password of the customer
 * @param {boolean} rememberMe - rememberMe preference selected by customer
 * @returns {Object} match result of the hashed pwd and input password
 */
function loginLegacyCustomer(customer, hashedLegacyPwd, email, password, rememberMe) {
  var result = {};
  var matchLegacyPasswordResult = isLegacyPassworMatched(hashedLegacyPwd, password);
  result.error = !matchLegacyPasswordResult;
  result.authenticatedCustomer = null;
  result.authStatus = null;
  var preferences = require('*/cartridge/config/preferences');

  if (matchLegacyPasswordResult && !preferences.isABTestingOn) {
    setLegacyPwd(customer, password);
    result.authenticatedCustomer = loginCustomer(email, password, rememberMe);
    removeLegacyFlag(customer);
  } else if (matchLegacyPasswordResult && preferences.isABTestingOn) {
    result.authenticatedCustomer = loginCustomer(email, preferences.tempLegacyLogin, rememberMe);
  } else {
    // If customer provided password doesn't match with HASH, Show correct error when wrong password entered.
    var authStatus;
    var islocked = false;
    Transaction.wrap(function () {
      authStatus = CustomerMgr.authenticateCustomer(email, password);
    });

    if (authStatus && authStatus.status !== 'AUTH_OK') {
      var errorCodes = {
        ERROR_CUSTOMER_DISABLED: 'error.message.account.disabled',
        ERROR_CUSTOMER_LOCKED: 'error.message.account.locked',
        ERROR_CUSTOMER_NOT_FOUND: 'error.message.login.form',
        ERROR_PASSWORD_EXPIRED: 'error.message.password.expired',
        ERROR_PASSWORD_MISMATCH: 'error.message.password.mismatch',
        ERROR_UNKNOWN: 'error.message.error.unknown',
        default: 'error.message.login.form'
      };

      var errorMessageKey = errorCodes[authStatus.status] || errorCodes.default;
      var errorMessage = Resource.msg(errorMessageKey, 'login', null);

      if (customer.profile && customer.profile.credentials && customer.profile.credentials.isLocked()) {
        islocked = true;
        errorMessage = Resource.msg('error.message.account.locked', 'login', null);
      }
      result.error = true;
      result.islocked = islocked;
      result.errorMessage = errorMessage;
      result.authStatus = authStatus;
    }
  }
  return result;
}

module.exports = {
  loginLegacyCustomer: loginLegacyCustomer,
  removeLegacyFlag: removeLegacyFlag,
  getPasswordLegacyHashed: getPasswordLegacyHashed,
  isLegacyPassworMatched: isLegacyPassworMatched,
  setLegacyPwd: setLegacyPwd
};
