'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var base = module.superModule;

var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
var StoreMgr = require('dw/catalog/StoreMgr');
var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
/** OVERRIDEN: To change the custom attribute reference and new method addition **/

/**
 * validates that the product line items exist, are online, and have available inventory.
 * @param {dw.order.Basket} item - The current user's basket
 * @param {boolean} hasInventory - hasInventory
 * @returns {Object} an error object
 */
function validateProductLineItem(item, hasInventory) {
  if (item.product === null || !item.product.online) {
    return false;
  }
  //changes for SFDEV-6215
  var hasInventoryInOMS = true;
  if (session.custom.omsInventory && session.custom.omsInventory.length > 0) {
    var omsInventoryArray = JSON.parse(session.custom.omsInventory);
    if (omsInventoryArray && omsInventoryArray.length > 0) {
      for (var i = 0; i < omsInventoryArray.length; i++) {
        if (omsInventoryArray[i].itemID === item.productID && omsInventoryArray[i].quantity < item.quantityValue) {
          hasInventoryInOMS = false;
          break;
        }
      }
    }
  }
  if (!hasInventoryInOMS) {
    return false;
  }
  var isInPurchaseLimit = productHelper.isInPurchaselimit(item.product.custom.purchaseLimit, item.quantityValue);
  /* eslint-disable */
  if (Object.hasOwnProperty.call(item.custom, 'fromStoreId') && item.custom.fromStoreId) {
    var store = StoreMgr.getStore(item.custom.fromStoreId);
    var storeInventory = ProductInventoryMgr.getInventoryList(store.inventoryListID);
    return (
      hasInventory &&
      storeInventory &&
      storeInventory.getRecord(item.productID) &&
      storeInventory.getRecord(item.productID).ATS.value >= item.quantityValue &&
      isInPurchaseLimit
    );
  } else {
    var availabilityLevels = item.product.availabilityModel.getAvailabilityLevels(item.quantityValue);
    return hasInventory && availabilityLevels.notAvailable.value === 0 && isInPurchaseLimit;
  }
  /* eslint-enable */
}

/**
 * validates that the product line items exist, are online, and have available inventory.
 * @param {dw.order.Basket} basket - The current user's basket
 * @returns {Object} an error object
 */
function validateProducts(basket) {
  var result = {
    error: true,
    hasInventory: true
  };
  var productLineItems = basket.productLineItems;

  collections.forEach(productLineItems, function (item) {
    result.hasInventory = validateProductLineItem(item, result.hasInventory);
    result.error = !result.hasInventory;
  });

  return result;
}

/**
 * validates that the product line items exist, are online, and have available inventory.
 * @param {dw.order.Basket} basket - The current user's basket
 * @returns {Object} an error object
 */
function validateCheckoutProducts(basket) {
  var result = {
    error: true,
    hasInventory: true
  };

  //changes for SFDEV-6215
  var inventoryList = null;
  if (session.custom.omsInventory && session.custom.omsInventory.length > 0) {
    var omsInventoryObj = JSON.parse(session.custom.omsInventory);
    inventoryList = new dw.util.ArrayList(omsInventoryObj);
  }
  var itemRemoved = false;
  var productLineItems = basket.productLineItems;
  var Transaction = require('dw/system/Transaction');
  collections.forEach(productLineItems, function (item) {
    if (!validateProductLineItem(item, true)) {
      Transaction.wrap(function () {
        if (item.bonusProductLineItem) {
          // saving attribute in session, since this attribute should be independent of login and cart merge we are saving in custom against privacy
          session.custom.isBonusProRemoved = true;
          // Saving bonus line item
          var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
          cartHelper.saveBonusLineItemInfo(basket, item);
        }
        basket.removeProductLineItem(item);
      });
      if (inventoryList && inventoryList.length > 0) {
        for (var i = 0; i < inventoryList.length; i++) {
          if (inventoryList.get(i).itemID === item.productID) {
            inventoryList.remove(inventoryList.get(i));
            itemRemoved = true;
            break;
          }
        }
      }
    }
  });
  //update OMS inventory list in session -  changes for SFDEV-6215
  if (itemRemoved) {
    var currentInventory = inventoryList.toArray();
    session.custom.omsInventory = JSON.stringify(currentInventory);
    result.itemRemoved = true;
  }

  result.error = !basket.productLineItems || basket.productLineItems.length === 0;
  return result;
}

/**
 * validates that the product line items exist, are online, and have available inventory.
 * @param {dw.order.Basket} basket - The current user's basket
 * @returns {Object} an availability
 */
function validateInventory(basket) {
  var InventoryService = require('*/cartridge/scripts/services/InventoryService');
  var ProductLineItemsModel = require('*/cartridge/models/productLineItems');
  var productLineItems = new ProductLineItemsModel(basket.productLineItems, 'basket');
  return InventoryService.getAvailability(productLineItems, 'lookup');
}

function removeInvalidCoupon(basket) {
  var Transaction = require('dw/system/Transaction');
  var PromotionMgr = require('dw/campaign/PromotionMgr');
  delete session.custom.expiredPromo;

  if (!empty(basket.couponLineItems)) {
    Transaction.wrap(function () {
      PromotionMgr.applyDiscounts(basket);
    });
  }

  collections.forEach(basket.couponLineItems, function (couponLineItem) {
    if (!couponLineItem.valid) {
      Transaction.wrap(function () {
        basket.removeCouponLineItem(couponLineItem);
      });
      session.custom.expiredPromo = true;
    }
  });
}

module.exports = {
  validateProducts: validateProducts,
  validateCoupons: base.validateCoupons,
  validateShipments: base.validateShipments,
  validateInventory: validateInventory,
  validateCheckoutProducts: validateCheckoutProducts,
  removeInvalidCoupon: removeInvalidCoupon
};
