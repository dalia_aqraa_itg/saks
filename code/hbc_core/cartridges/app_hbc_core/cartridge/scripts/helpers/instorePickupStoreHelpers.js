'use strict';

var base = module.superModule;
var StoreMgr = require('dw/catalog/StoreMgr');
var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
var Transaction = require('dw/system/Transaction');
var Logger = require('dw/system/Logger');

/**
 * Sets the store and its inventory list for the given product line item.
 * @param {string} storeId - The store id
 * @param {dw.order.ProductLineItem} productLineItem - The ProductLineItem object
 * @returns {dw.catalog.Store} updatedStore - updates store to line item
 */
function setAndGetStoreInProductLineItem(storeId, productLineItem) {
  var store = null;
  var updatedStore = null;
  try {
    Transaction.wrap(function () {
      if (storeId) {
        store = StoreMgr.getStore(storeId);
        var inventoryListId = store.inventoryListID;
        if (store && inventoryListId) {
          var storeinventory = ProductInventoryMgr.getInventoryList(inventoryListId);
          if (storeinventory) {
            if (
              storeinventory.getRecord(productLineItem.productID) &&
              storeinventory.getRecord(productLineItem.productID).ATS.value >= productLineItem.quantityValue
            ) {
              productLineItem.custom.fromStoreId = store.ID; // eslint-disable-line
              productLineItem.setProductInventoryList(storeinventory);
              updatedStore = store;
            }
          }
        }
      }
    });
  } catch (e) {
    Logger.error('Error in instorePickupStoreHelpers.js: setAndGetStoreInProductLineItem ' + e);
  }
  return updatedStore;
}

/**
 * Resets the product line item with "ship to home"
 * @param {dw.order.ProductLineItem} productLineItem - The ProductLineItem object
 */
function resetInstoreProductLineItem(productLineItem) {
  try {
    Transaction.wrap(function () {
      productLineItem.custom.fromStoreId = null; // eslint-disable-line
      productLineItem.setProductInventoryList(null);
    });
  } catch (e) {
    Logger.error('Error in instorePickupStoreHelpers.js: resetInstoreProductLineItem ' + e);
  }
}

module.exports = {
  setAndGetStoreInProductLineItem: setAndGetStoreInProductLineItem,
  getStoreInventory: base.getStoreInventory,
  resetInstoreProductLineItem: resetInstoreProductLineItem
};
