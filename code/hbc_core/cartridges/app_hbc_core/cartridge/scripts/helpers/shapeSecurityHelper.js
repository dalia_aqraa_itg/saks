'use strict';

/**
 * Function to determine if shape security tag needs to be included in a page
 *
 *
 * @param {Object} PipelineDictionary
 * @returns {Object} - Information to include shape security tag(s)
 */
exports.getShapeSecurityTagInfo = function (pdict) {
  var shapeTagInfo = {};
  shapeTagInfo.includeShape1 = false;
  shapeTagInfo.includeShape2 = false;
  var preferences = require('*/cartridge/config/preferences');
  var shapeSecurityConfig = preferences.shapeSecurity;
  if (shapeSecurityConfig && shapeSecurityConfig.tag1Pages && shapeSecurityConfig.tag1Script && shapeSecurityConfig.tag1Pages.indexOf(pdict.action) !== -1) {
    if (pdict.action === 'Search-Show') {
      if (pdict.category && pdict.category.ID && shapeSecurityConfig.categories.indexOf(pdict.category.ID) !== -1) {
        shapeTagInfo.includeShape1 = true;
        shapeTagInfo.tag1Script = shapeSecurityConfig.tag1Script;
      }
    } else {
      shapeTagInfo.includeShape1 = true;
      shapeTagInfo.tag1Script = shapeSecurityConfig.tag1Script;
    }
  }
  if (shapeSecurityConfig && shapeSecurityConfig.tag2Pages && shapeSecurityConfig.tag2Script && shapeSecurityConfig.tag2Pages.indexOf(pdict.action) !== -1) {
    if (pdict.action === 'Search-Show') {
      if (pdict.category && pdict.category.ID && shapeSecurityConfig.categories.indexOf(pdict.category.ID) !== -1) {
        shapeTagInfo.includeShape2 = true;
        shapeTagInfo.tag2Script = shapeSecurityConfig.tag2Script;
      }
    } else {
      shapeTagInfo.tag2Script = shapeSecurityConfig.tag2Script;
      shapeTagInfo.includeShape2 = true;
    }
  }
  return shapeTagInfo;
};
