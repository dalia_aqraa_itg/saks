'use strict';
var preferences = require('*/cartridge/config/preferences');
var LOAD_PAGE_SIZE = preferences.defaultPageSize ? preferences.defaultPageSize : 24;
var TOTAL_PAGE_SIZE = preferences.totalPageSize ? preferences.totalPageSize : 96;
var HashMap = require('dw/util/HashMap');
var URLUtils = require('dw/web/URLUtils');

/**
 * Generates URLs for Pagination
 *
 * @param {number} productSearchCount - Product search Count
 * @param {number} pageNumber - current page number
 * @return {string} - More button URL
 */
function getPaginationUrls(productSearchCount, pageNumber) {
  var pageSize = TOTAL_PAGE_SIZE;
  var pagingUrlsNew = new HashMap();
  var startPage;
  var endPage;
  // eslint-disable-next-line no-param-reassign
  var currentPageNumber = Math.ceil(pageNumber / Math.ceil(TOTAL_PAGE_SIZE / LOAD_PAGE_SIZE));
  var pageCount = Math.ceil(productSearchCount / pageSize);

  for (var i = 1; i <= pageCount; i++) {
    pagingUrlsNew.put(i, URLUtils.url('Wishlist-Show', 'pageNumber', i));
  }

  if (pageCount <= 3) {
    startPage = 1;
    endPage = pageCount;
  } else if (currentPageNumber <= 2) {
    startPage = 1;
    endPage = 4;
  } else if (currentPageNumber > 2) {
    if (currentPageNumber + 1 < pageCount) {
      startPage = currentPageNumber - 1;
      endPage = currentPageNumber + 1;
    } else {
      startPage = pageCount - 3;
      endPage = pageCount - 1;
    }
  }

  pagingUrlsNew.put('totalPages', pageCount);
  pagingUrlsNew.put('startPage', startPage);
  pagingUrlsNew.put('endPage', endPage);
  pagingUrlsNew.put('currentPage', currentPageNumber);
  if (currentPageNumber === 1) {
    pagingUrlsNew.put('disablePreviousButton', 'disabled');
  }
  if (currentPageNumber === pageCount) {
    pagingUrlsNew.put('disableNextButton', 'disabled');
  }

  pagingUrlsNew.put('firstPage', pagingUrlsNew.get(1));
  pagingUrlsNew.put('previous', pagingUrlsNew.get(currentPageNumber - 1));
  pagingUrlsNew.put('next', pagingUrlsNew.get(currentPageNumber + 1));
  pagingUrlsNew.put('lastPage', pagingUrlsNew.get(pageCount));

  return pagingUrlsNew;
}

/**
 * Generates URLs for Pagination for the Shared Wishlist
 *
 * @param {number} productSearchCount - Product search Count
 * @param {number} pageNumber - current page number
 * @return {string} - More button URL
 */
function getShareWLPaginationUrls(productSearchCount, pageNumber, listID) {
  var pageSize = TOTAL_PAGE_SIZE;
  var pagingUrlsNew = new HashMap();
  var startPage;
  var endPage;
  // eslint-disable-next-line no-param-reassign
  var currentPageNumber = Math.ceil(pageNumber / Math.ceil(TOTAL_PAGE_SIZE / LOAD_PAGE_SIZE));
  var pageCount = Math.ceil(productSearchCount / pageSize);

  for (var i = 1; i <= pageCount; i++) {
    pagingUrlsNew.put(i, URLUtils.url('Wishlist-ShowOthers', 'id', listID, 'pageNumber', i));
  }

  if (pageCount <= 3) {
    startPage = 1;
    endPage = pageCount;
  } else if (currentPageNumber <= 2) {
    startPage = 1;
    endPage = 4;
  } else if (currentPageNumber > 2) {
    if (currentPageNumber + 1 < pageCount) {
      startPage = currentPageNumber - 1;
      endPage = currentPageNumber + 1;
    } else {
      startPage = pageCount - 3;
      endPage = pageCount - 1;
    }
  }

  pagingUrlsNew.put('totalPages', pageCount);
  pagingUrlsNew.put('startPage', startPage);
  pagingUrlsNew.put('endPage', endPage);
  pagingUrlsNew.put('currentPage', currentPageNumber);
  if (currentPageNumber === 1) {
    pagingUrlsNew.put('disablePreviousButton', 'disabled');
  }
  if (currentPageNumber === pageCount) {
    pagingUrlsNew.put('disableNextButton', 'disabled');
  }

  pagingUrlsNew.put('firstPage', pagingUrlsNew.get(1));
  pagingUrlsNew.put('previous', pagingUrlsNew.get(currentPageNumber - 1));
  pagingUrlsNew.put('next', pagingUrlsNew.get(currentPageNumber + 1));
  pagingUrlsNew.put('lastPage', pagingUrlsNew.get(pageCount));

  return pagingUrlsNew;
}

module.exports = {
  getPaginationUrls: getPaginationUrls,
  getShareWLPaginationUrls: getShareWLPaginationUrls
};
