'use strict';

var Encoding = require('dw/crypto/Encoding');
var MessageDigest = require('dw/crypto/MessageDigest');
var Bytes = require('dw/util/Bytes');
var PaymentInstrument = require('dw/order/PaymentInstrument');
var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
var collections = require('*/cartridge/scripts/util/collections');
var preferences = require('*/cartridge/config/preferences');

/**
 * Generate Hash for the basket address and the payment card used.
 * Check the Express checkout before modifying the hash parameters
 * @param {dw.order.Basket} basket - customer basket
 * @returns {string} hash- if success then return true
 */
function generateBasketHash(basket) {
  var messageDigest = new MessageDigest(MessageDigest.DIGEST_SHA_256);
  var hashString = '';
  var billingAddress = basket.billingAddress;
  var shipToHomeShipment = cartHelper.getShipToHomeShipment(basket);

  if (billingAddress) {
    hashString += billingAddress.getFirstName();
    hashString += billingAddress.getLastName();
    hashString += billingAddress.getAddress1();
    hashString += billingAddress.getAddress2();
    hashString += billingAddress.getCity();
    hashString += billingAddress.getPostalCode();
    hashString += billingAddress.getPhone();
    hashString += billingAddress.getCountryCode();
    hashString += billingAddress.getStateCode();
  }

  if (shipToHomeShipment) {
    var shippingAddress = shipToHomeShipment.getShippingAddress();
    if (shippingAddress) {
      hashString += shippingAddress.getFirstName();
      hashString += shippingAddress.getLastName();
      hashString += shippingAddress.getAddress1();
      hashString += shippingAddress.getAddress2();
      hashString += shippingAddress.getCity();
      hashString += shippingAddress.getPostalCode();
      hashString += shippingAddress.getPhone();
      hashString += shippingAddress.getCountryCode();
      hashString += shippingAddress.getStateCode();
    }
    hashString += shipToHomeShipment.shippingMethod ? shipToHomeShipment.shippingMethod.ID : '';
  }

  // check if the payment instrument lengths
  var paymentInstruments = basket.getPaymentInstruments();

  // Add the length of the payments. To identify if a GC was combined with the CC payment.
  hashString += paymentInstruments.getLength();

  hashString += basket.getCustomerEmail();

  var paymentInstrument;
  collections.forEach(paymentInstruments, function (pi) {
    if (pi.getPaymentMethod() === PaymentInstrument.METHOD_CREDIT_CARD) {
      paymentInstrument = pi;
    }
  });

  if (paymentInstrument) {
    hashString += paymentInstrument.getCreditCardHolder();
    hashString += paymentInstrument.getCreditCardType();
    hashString += paymentInstrument.getCreditCardExpirationMonth();
    hashString += paymentInstrument.getCreditCardExpirationYear();
    hashString += paymentInstrument.getCreditCardNumberLastDigits(); // get last four digits of the card
  }

  var hash = Encoding.toHex(messageDigest.digestBytes(new Bytes(hashString)));
  return hash;
}

function validateBasketHashChange(currentBasket, req) {
  var basketUpdated = false;
  if (preferences.enableExpressCheckout && req.currentCustomer.raw.authenticated) {
    var currentBasketHash = generateBasketHash(currentBasket);
    var basketHashAfter = req.session.privacyCache.get('basketHashAfterCustAddressUpdate');
    if (basketHashAfter && basketHashAfter !== currentBasketHash) {
      basketUpdated = true;
    }
    return {
      Express: true,
      basketUpdated: basketUpdated
    };
  }
  return {
    Express: false,
    BasketUpdated: basketUpdated
  };
}
module.exports = {
  generateBasketHash: generateBasketHash,
  validateBasketHashChange: validateBasketHashChange
};
