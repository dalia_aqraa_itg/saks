'use strict';

var Site = require('dw/system/Site');

/**
 * Store cookies with commission vendor data
 * @param request
 * @returns
 */

function addCommissionCookies(request) {
  var paramMap = request.httpParameterMap;
  var site_referCookieName = 'site_refer';
  var site_refer = paramMap.get(site_referCookieName) ? paramMap.get(site_referCookieName).value : '';
  //do not store cookies if site_refer parameter does not exist or empty
  if (!validateSiteRefer(site_refer)) {
    return;
  }
  var sf_storeidCookieName = 'sf_storeid';
  var sf_associdCookieName = 'sf_associd';
  var commissionCookieNames = [site_referCookieName, sf_storeidCookieName, sf_associdCookieName];
  var age = 7 * 24 * 60 * 60;
  commissionCookieNames.forEach(function (name) {
    var value = paramMap.get(name).value;
    if (value) {
      var cookie = new dw.web.Cookie(name, value);
      cookie.setDomain(request.getHttpHost());
      cookie.setMaxAge(age);
      cookie.setSecure(true);
      cookie.setHttpOnly(true);
      cookie.setPath('/');
      response.addHttpCookie(cookie);
    }
  });
}

/**
 * Attribution logic for cookies
 * @param value
 * @returns
 */
function validateSiteRefer(value) {
  if (!value) {
    return false;
  } else {
    var site_ref_search = 'SEM';
    var site_ref_email = 'EML';
    var site_ref_email_SF = 'EMLHB_SF';
    var emlRegexp = new RegExp('^(' + site_ref_email + '|' + site_ref_search + ')');
    if (emlRegexp.test(value)) {
      if (!value.equals(site_ref_email_SF)) {
        return false;
      }
    }
  }
  return true;
}

/**
 * query retrieve commission cookies
 * @param request
 * @param cookieNames
 * @returns
 */
function getCommissionCookies(request, cookieNames) {
  var vendorCookies = new dw.util.HashMap();
  var cookies = request.getHttpCookies();
  var cookieCount = cookies.getCookieCount();
  cookieNames.forEach(function (name) {
    for (var i = 0; i < cookieCount; i++) {
      if (name === cookies[i].getName()) {
        vendorCookies.put(name, cookies[i]);
      }
    }
  });
  return vendorCookies;
}

module.exports = {
  getCommissionCookies: getCommissionCookies,
  addCommissionCookies: addCommissionCookies
};
