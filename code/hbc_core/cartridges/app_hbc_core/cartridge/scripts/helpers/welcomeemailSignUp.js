'use strict';

/**
 * Middleware to use welcome email-modal signup check
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @returns {void}
 */
function emailsignup(req, res, next) {
  var cookies = request.getHttpCookies();
  var emailSignUpCookieExists = false;
  for (var i = 0; i < cookies.cookieCount; i++) {
    var cookie = cookies[i];
    // reset sr_token cookie
    if (cookie.name === 'emailsignup') {
      emailSignUpCookieExists = true;
      break;
    }
  }

  res.setViewData({
    welcomeemail_signup: emailSignUpCookieExists
  });
  next();
}

module.exports = {
  emailsignup: emailsignup
};
