'use strict';
var preferences = require('*/cartridge/config/preferences');
var productSearchHelper = require('*/cartridge/scripts/helpers/productSearchHelper');
var LOAD_PAGE_SIZE = preferences.defaultPageSize ? preferences.defaultPageSize : 24;
var TOTAL_PAGE_SIZE = preferences.totalPageSize ? preferences.totalPageSize : 96;
var HashMap = require('dw/util/HashMap');

/**
 *
 *
 * @param {string} param - param
 * @param {string} newval - newval
 * @param {string} search - search string
 * @returns {string} queryString
 */
function replaceQueryParam(param, newval, search) {
  var searchParams = search.split('&');
  var queryString = '';
  Object.keys(searchParams).forEach(function (key) {
    var paramValue = searchParams[key].split('=');
    if (paramValue[0] === param) {
      paramValue[1] = newval;
    }
    queryString = queryString ? queryString + '&' + paramValue[0] + '=' + paramValue[1] : paramValue[0] + '=' + paramValue[1];
  });

  return queryString;
}

/**
 * function to create price url for refinements
 *
 * @param {string} url - Price refinement URL
 * @returns {string} url
 */
function getPriceUrl(url) {
  var newURL = url;
  if (url.indexOf('pmin') > -1) {
    var params = {};
    var paramStr = url.slice(url.indexOf('?') + 1);
    var urlPath = url.slice(0, url.indexOf('?') + 1);
    var definitions = paramStr.split('&');

    definitions.forEach(function (val) {
      var parts = val.split('=', 2);
      params[parts[0]] = parts[1];
    });

    Object.keys(params).forEach(function (key) {
      var val = params[key];
      if (key === 'pmin' || key === 'pmax') {
        if (val.indexOf('%2c') > -1) {
          val = val.replace('%2c', '');
          paramStr = replaceQueryParam(key, val, paramStr);
        } else if (val.indexOf('%2C') > -1) {
          val = val.replace('%2C', '');
          paramStr = replaceQueryParam(key, val, paramStr);
        }
      }
    });
    newURL = urlPath + paramStr;
  }
  return newURL;
}

/**
 *
 * gets paging model
 * @param {Iterator} productHits - product hits iterator
 * @param {number} count - count of product hits
 * @param {number} pageSize - total page size
 * @param {number} startIndex - starting page index
 * @returns {dw.web.PagingModel} - paging model
 */
function getPagingModel(productHits, count, pageSize, startIndex) {
  var PagingModel = require('dw/web/PagingModel');
  var paging = new PagingModel(productHits, count);

  paging.setStart(startIndex || 0);
  paging.setPageSize(pageSize || LOAD_PAGE_SIZE);

  return paging;
}

/**
 * Generates URLs for paginations
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product Search model
 * @param {number} pageNumber - current page number of pagination
 * @param {category} category - category
 * @param {Object} httpParams - Http query paramteres
 * @returns {dw/util/HashMap} - Map of all the pagination and other URLs
 */
function getPaginationUrls(productSearch, pageNumber, category, httpParams) {
  var paginationURL = 'Search-Show';
  var pageSize = TOTAL_PAGE_SIZE;
  var totalPageSize = TOTAL_PAGE_SIZE;
  var hitsCount = productSearch.count;
  var pagingUrls = new HashMap();
  // var nextStart;
  var startPage;
  var endPage;

  // grid slot data
  var gridSlotData = productSearchHelper.getGridSlot(productSearch);

  // count the total product count based on the gridslot
  var totalGridCount =
    hitsCount +
    (gridSlotData !== null && !!gridSlotData.gridSlot1Data.gridSlot1Size ? gridSlotData.gridSlot1Data.gridSlot1Size : 0) +
    (gridSlotData !== null && !!gridSlotData.gridSlot2Data.gridSlot2Size ? gridSlotData.gridSlot2Data.gridSlot2Size : 0) +
    (gridSlotData !== null && !!gridSlotData.gridSlot3Data.gridSlot3Size ? gridSlotData.gridSlot3Data.gridSlot3Size : 0);

  var pageCount = Math.ceil((!httpParams.filter ? totalGridCount : hitsCount) / pageSize);
  if (pageCount <= 3) {
    startPage = 0;
    endPage = pageCount - 1;
  } else if (pageNumber <= 1) {
    startPage = 0;
    endPage = 3;
  } else if (pageNumber > 1) {
    if (pageNumber + 2 < pageCount) {
      startPage = pageNumber - 1;
      endPage = pageNumber + 1;
    } else {
      startPage = pageCount - 4;
      endPage = pageCount - 1;
    }
  }

  pagingUrls.put('totalPages', pageCount);
  pagingUrls.put('startPage', startPage + 1);
  pagingUrls.put('endPage', endPage + 1);
  pagingUrls.put('currentPage', pageNumber + 1);
  if (pageNumber === 0) {
    pagingUrls.put('disablePreviousButton', 'disabled');
  }
  if (pageNumber === pageCount - 1) {
    pagingUrls.put('disableNextButton', 'disabled');
  }

  for (var i = 0; i < pageCount; i++) {
    var tempcurrentStart = (i - 1) * totalPageSize;
    var temppaging = getPagingModel(productSearch.productSearchHits, hitsCount, LOAD_PAGE_SIZE, tempcurrentStart);

    // original estimated page size without grids
    var originalPageSize = i * Number(totalPageSize);

    // calculated page size till this page with grids
    var currentPageSize =
      originalPageSize -
      (gridSlotData !== null && gridSlotData.gridSlot1Data !== null && gridSlotData.gridSlot1Data.gridSlot1Placement <= originalPageSize
        ? gridSlotData.gridSlot1Data.gridSlot1Size
        : 0) -
      (gridSlotData !== null && gridSlotData.gridSlot2Data !== null && gridSlotData.gridSlot2Data.gridSlot2Placement <= originalPageSize
        ? gridSlotData.gridSlot2Data.gridSlot2Size
        : 0) -
      (gridSlotData !== null && gridSlotData.gridSlot3Data !== null && gridSlotData.gridSlot3Data.gridSlot3Placement <= originalPageSize
        ? gridSlotData.gridSlot3Data.gridSlot3Size
        : 0);

    temppaging.setStart(!httpParams.filter ? currentPageSize : originalPageSize);

    var baseUrl = productSearch.url(paginationURL); // Search-Show

    // Append In Store and SDD Filters
    if (httpParams.srchsrc && httpParams.storeid) {
      baseUrl.append('srchsrc', httpParams.srchsrc);
    }
    if (httpParams.storeid) {
      baseUrl.append('storeid', httpParams.storeid);
    }

    var finalUrl = temppaging.appendPaging(baseUrl);
    if (finalUrl.toString().indexOf('%2c') > -1 || finalUrl.toString().indexOf('%2C') > -1) {
      finalUrl = getPriceUrl(finalUrl.toString());
    }

    if (i === 0) {
      pagingUrls.put('firstPage', finalUrl);
    }
    if (i === pageNumber - 1) {
      pagingUrls.put('previous', finalUrl);
    }
    if (i === pageNumber + 1) {
      pagingUrls.put('next', finalUrl);
    }
    if (i === pageCount - 1) {
      pagingUrls.put('lastPage', finalUrl);
    }
    pagingUrls.put(Math.floor(i + 1), finalUrl);
  }

  return pagingUrls;
}

module.exports = {
  getPaginationUrls: getPaginationUrls
};
