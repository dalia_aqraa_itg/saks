'use strict';
var CustomObject = require('*/cartridge/models/customobject');
var customObjectModel = new CustomObject('Subscriptions');
var HashMap = require('dw/util/HashMap');

/**
 * Build a custom object
 * @param {Object} hashMap fields to be saved in custom object
 * @param {Object} customer raw customer to get profile details
 * @return {Object} returns updated list
 */
function buildSubscription(hashMap, customer) {
  if (customer && customer.registered && customer.profile) {
    var profile = customer.profile;
    hashMap.put('firstName', profile.firstName);
    hashMap.put('middleName', profile.secondName);
    hashMap.put('lastName', profile.lastName);
    hashMap.put('gender', profile.gender.displayValue ? profile.gender.displayValue : '');
    hashMap.put('birthday', profile.birthday);
    if (profile.addressBook && profile.addressBook.preferredAddress) {
      var customerAddress = profile.addressBook.preferredAddress;
      hashMap.put('address', customerAddress.address1);
      hashMap.put('addressTwo', customerAddress.address2);
      hashMap.put('city', customerAddress.city);
      hashMap.put('state', customerAddress.stateCode);
      hashMap.put('zipFull', customerAddress.postalCode);
      hashMap.put('country', customerAddress.countryCode);
      hashMap.put('phone', customerAddress.phone);
    }
  }
  return hashMap;
}

/**
 * Build a custom object
 * @param {Object} hashMap fields to be saved in custom object
 * @param {Object} form to get the form details details
 * @param {Object} String which specifies the gender
 * @return {Object} returns updated list
 */
function buildSubscriptionEmailSignUp(hashMap, form, gender) {
  if (form) {
    hashMap.put('firstName', form.customer.firstname.value);
    hashMap.put('lastName', form.customer.lastname.value);
    if (!empty(gender)) {
      hashMap.put('gender', gender);
    }
    var birthDay;
    if (!empty(form.customer.birthYear.value) && !empty(form.customer.birthMonth.value) && !empty(form.customer.birthDate.value)) {
      birthDay = new Date(form.customer.birthYear.value, form.customer.birthMonth.value - 1, form.customer.birthDate.value);
      hashMap.put('birthday', birthDay);
    }
    hashMap.put('zipFull', form.customer.zipcode.value);
    if (!empty(form.customer.hudsonbayrewards)) {
      hashMap.put('hbcRewardsNumber', form.customer.hudsonbayrewards.value);
    }
  }
  return hashMap;
}

/**
 * function to fetch the initial email data
 *
 * @param {string} loc - locale id
 * @returns {dw/util/HashMap} hashMap - hashmap
 */
function getInitialData(loc) {
  var preferences = require('*/cartridge/config/preferences');
  var hashMap = new HashMap();
  hashMap.put('subscribedOrUnsubscribed', new Date());
  hashMap.put('language', loc);
  hashMap.put('banner', preferences.dataSubscription.BANNER);
  hashMap.put('exported', false);
  return hashMap;
}

var subscribeDetails = {
  createSubscription: function (email, hashMap, customer) {
    return customObjectModel.save(email, buildSubscription(hashMap, customer));
  },

  fetchAllSubscription: function (days, callBack) {
    var records = customObjectModel.queryCustom('custom.EXPORTED = NULL OR custom.EXPORTED = false', null);
    if (callBack && records && records.count > 0) {
      var collections = require('*/cartridge/scripts/util/collections');
      collections.forEach(records.asList(), callBack);
    }
    return records;
  },

  updateSubscription: function (map, co) {
    return customObjectModel.updateCustom(map, co);
  },

  isRequestFromMobile: function () {
    return request.httpUserAgent.indexOf('Mobile') > 0;
  },

  formatDateField: function (dateField) {
    if (!dateField) {
      return dateField;
    }
    var Calendar = require('dw/util/Calendar');
    var StringUtils = require('dw/util/StringUtils');
    var cal = new Calendar(dateField);
    return StringUtils.formatCalendar(cal, 'yyyy-MM-dd').concat('Z');
  },
  createSubscriptionFromWelcomeModal: function (email, hashMap, customer) {
    return customObjectModel.save(email, buildSubscription(hashMap, customer));
  },

  createSubscriptionFromEmailSignUp: function (email, hashMap, form, gender) {
    return customObjectModel.save(email, buildSubscriptionEmailSignUp(hashMap, form, gender));
  },

  getInitialData: getInitialData
};
module.exports = subscribeDetails;
