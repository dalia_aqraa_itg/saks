'use strict';

var preferences = require('*/cartridge/config/preferences');

/**
 * Returns the boolean response based product attribute value in the basket.
 * @param {dw.order.Basket} cart - Object that contains LineItemCtr
 * @returns {boolean} Boolean indicating if the cart qualifies for shop runner checkout.
 */
function checkSRExpressCheckoutEligibility(cart) {
  let shopRunnerEnabled = preferences.shopRunnerEnabled;
  if (shopRunnerEnabled) {
    if (cart) {
      var CheckCartEligibility = require('*/cartridge/scripts/checkout/CheckCartEligibility');
      var shopRunnerEligibleBasket = CheckCartEligibility.checkEligibility(cart);
      if (shopRunnerEligibleBasket && shopRunnerEligibleBasket === 'NO_SR') {
        shopRunnerEnabled = false;
      }
    } else {
      shopRunnerEnabled = false;
    }
  }
  return shopRunnerEnabled;
}

/**
 * Returns the boolean response based product attribute value in the basket.
 * @param {dw.order.Basket} cart - Object that contains LineItemCtr
 * @returns {boolean} Boolean indicating if the cart qualifies for shop runner checkout.
 */
function checkSRCheckoutEligibilityForCart(cart) {
  var sREnabled = preferences.shopRunnerEnabled;
  if (sREnabled) {
    if (cart) {
      var CheckCartEligibility = require('*/cartridge/scripts/checkout/CheckCartEligibility');
      var shopRunnerEligibleBasket = CheckCartEligibility.checkEligibilityForSRExpess(cart);
      if (shopRunnerEligibleBasket && (shopRunnerEligibleBasket === 'NO_SR' || shopRunnerEligibleBasket === 'MIXED' || shopRunnerEligibleBasket === '')) {
        sREnabled = false;
      }
    } else {
      sREnabled = false;
    }
  }
  return sREnabled;
}
module.exports = {
  checkSRExpressCheckoutEligibility: checkSRExpressCheckoutEligibility,
  checkSRCheckoutEligibilityForCart: checkSRCheckoutEligibilityForCart
};
