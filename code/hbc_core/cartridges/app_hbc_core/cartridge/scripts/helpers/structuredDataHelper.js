'use strict';

/**
 * Get product schema information
 * @param {Object} product - Product Object
 *
 * @returns {Object} - Product Schema object
 */
function getProductSchema(product) {
  var factoryProduct = product;
  var ProductMgr = require('dw/catalog/ProductMgr');
  var apiProduct = ProductMgr.getProduct(product.id);
  var collections = require('*/cartridge/scripts/util/collections');
  var schema = '';
  var productVariant = apiProduct.getVariants();

  if (apiProduct.isMaster() && productVariant) {
    collections.forEach(productVariant, function (variant) {
      schema = schema.concat(JSON.stringify(loopSchema(variant, factoryProduct)));
    });
    return schema;
  } else {
    schema = loopSchema(apiProduct, factoryProduct);
  }

  return schema;
}

function loopSchema(product, factoryProduct) {
  var ProductMgr = require('dw/catalog/ProductMgr');
  var apiProduct = ProductMgr.getProduct(product.getID());

  var description = '';
  if (factoryProduct.shortDescription) {
    description = factoryProduct.shortDescription;
  } else if (factoryProduct.longDescription) {
    description = factoryProduct.longDescription;
  }

  var schema = {
    '@context': 'http://schema.org/',
    '@type': 'Product',
    name: factoryProduct.productName,
    description: description,
    mpn: apiProduct.manufacturerSKU ? apiProduct.manufacturerSKU : '',
    sku: apiProduct.getID(),
    gtin13: apiProduct.UPC ? apiProduct.UPC : ''
  };
  if (factoryProduct.brand) {
    schema.brand = {
      '@type': 'Thing',
      name: factoryProduct.brand.name
    };
  }

  if (factoryProduct.images && factoryProduct.images.large) {
    schema.image = [];
    factoryProduct.images.large.forEach(function (image) {
      schema.image.push(image.url);
    });
  }

  if (factoryProduct.price) {
    schema.offers = {
      url: require('dw/web/URLUtils').abs('Product-Show', 'pid', factoryProduct.id).toString()
    };
    if (factoryProduct.price.type === 'range') {
      schema.offers['@type'] = 'AggregateOffer';
      schema.offers.priceCurrency = factoryProduct.price.min.sales.currency;
      schema.offers.lowprice = factoryProduct.price.min.sales.value;
      schema.offers.highprice = factoryProduct.price.max.sales.value;
    } else {
      schema.offers['@type'] = 'Offer';
      if (factoryProduct.price.sales) {
        schema.offers.priceCurrency = factoryProduct.price.sales.currency;
        schema.offers.price = factoryProduct.price.sales.decimalPrice;
      } else if (factoryProduct.price.list) {
        schema.offers.priceCurrency = factoryProduct.price.list.currency;
        schema.offers.price = factoryProduct.price.list.decimalPrice;
      }
    }
    schema.offers.availability = 'http://schema.org/InStock';
    if (factoryProduct.available) {
      if (factoryProduct.availability && factoryProduct.availability.messages[0] === require('dw/web/Resource').msg('label.preorder', 'common', null)) {
        schema.offers.availability = 'http://schema.org/PreOrder';
      }
    } else {
      schema.offers.availability = 'http://schema.org/OutOfStock';
    }
  }

  // get master product for review counts
  var masterProduct = product.master ? product : product.variant ? product.masterProduct : product;
  if ('turntoAverageRating' in masterProduct.custom && 'turntoReviewCount' in masterProduct.custom) {
    schema.aggregateRating = {
      '@type': 'AggregateRating',
      ratingValue: masterProduct.custom.turntoAverageRating ? masterProduct.custom.turntoAverageRating : '',
      // eslint-disable-next-line radix
      reviewCount: masterProduct.custom.turntoReviewCount ? parseInt(masterProduct.custom.turntoReviewCount).toString() : ''
    };
  }
  return schema;
}

/**
 * Get product listing page schema information
 * @param {List} productIds - Product Ids
 *
 * @returns {Object} - Listing Schema object
 */
function getListingPageSchema(productIds) {
  var schema = {
    '@context': 'http://schema.org/',
    '@type': 'ItemList',
    itemListElement: []
  };
  if (productIds && productIds.length > 0) {
    Object.keys(productIds).forEach(function (item) {
      var productID = productIds[item].productID;
      schema.itemListElement.push({
        '@type': 'ListItem',
        position: Number(item) + 1,
        url: require('dw/web/URLUtils').abs('Product-Show', 'pid', productID).toString()
      });
    });
  }

  return schema;
}

/**
 * function to get breadcrumbs schema
 *
 * @param {dw/util/Collection} breadCrumbs - breadcrumb collection
 * @returns {Object} schema - breadcrumb schema object
 */
function getBreadCrumbsSchema(breadCrumbs) {
  var schema = {
    '@context': 'http://schema.org/',
    '@type': 'BreadcrumbList',
    itemListElement: []
  };
  var URL = require('dw/web/URL');
  if (breadCrumbs) {
    var collections = require('*/cartridge/scripts/util/collections');
    collections.forEach(breadCrumbs, function (item) {
      var itemURL = item.url instanceof URL ? item.url.abs().toString() : item.url;
      schema.itemListElement.push({
        '@type': 'ListItem',
        position: breadCrumbs.indexOf(item) + 1,
        name: item.htmlValue,
        item: itemURL
      });
    });
  }

  return schema;
}

module.exports = {
  getProductSchema: getProductSchema,
  getListingPageSchema: getListingPageSchema,
  getBreadCrumbsSchema: getBreadCrumbsSchema
};
