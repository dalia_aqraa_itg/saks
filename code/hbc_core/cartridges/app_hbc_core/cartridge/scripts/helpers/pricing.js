'use strict';
var base = module.superModule;

/**
 * Get Currency format
 * @param {string} money formatted money
 * @returns {string} money returns formatted currency format.
 */
function getCurrencyFromMoney(money) {
  var regExp = new RegExp('([^0-9^,^.])', 'g');
  return money.match(regExp).join('');
}
/**
 * Remove Decimals in money -- Since parseInt method might throws exception so handling in try catch block
 * @param {string} money Money to be converted
 * @returns {string} formatAmount converted money
 */
function removeDecimalFromMoney(money) {
  var formatAmount = money;
  try {
    if (request.session.currency.defaultFractionDigits && money) {
      // eslint-disable-line
      var currencyCode = getCurrencyFromMoney(money);
      var tMoney = money.replace(currencyCode, '');
      if (tMoney) {
        var decimalValue = tMoney.substr(tMoney.length - (request.session.currency.defaultFractionDigits - 1) - 1, tMoney.length); // eslint-disable-line
        if (parseInt(decimalValue) === 0) {
          // eslint-disable-line
          formatAmount = tMoney.substr(0, tMoney.length - request.session.currency.defaultFractionDigits - 1); // eslint-disable-line
          formatAmount = money.indexOf(currencyCode) === 0 ? currencyCode.concat(formatAmount) : formatAmount.concat(currencyCode);
        }
      }
    }
  } catch (e) {
    formatAmount = money;
  }
  return formatAmount;
}

base.calculatePercentOff = function (minPrice, maxPrice) {
  var savings = {};
  if (minPrice != null && maxPrice != null && maxPrice !== minPrice) {
    savings.savings = maxPrice.value - minPrice.value;
    savings.savePercentage = Math.floor((savings.savings / maxPrice.value) * 100).toFixed();
  }
  return savings;
};

base.formatMoney = function (money) {
  return removeDecimalFromMoney(money);
};

base.getPrivateSalePromo = function () {
  if (session.privacy.privateSalePromoID) {
    return session.privacy.privateSalePromoID;
  }
  return null;
};

module.exports = base;
