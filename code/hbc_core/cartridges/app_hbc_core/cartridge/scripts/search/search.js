'use strict';
var base = module.superModule;

/**
 * Remove special character String and parse to X.XXX,XX even for fr_CA which was X,XXX.XX
 * @param {string} price string needs to parsed
 * @returns {string} formatted string
 */
function removeExtraChars(price) {
  var tPrice = price;
  var isAnySpecialChar = tPrice && (tPrice.indexOf(',') >= 0 || tPrice.indexOf('.') >= 0);
  if (isAnySpecialChar && request.session.currency.defaultFractionDigits && tPrice) {
    // eslint-disable-line
    var currencySeperator = tPrice.charAt(price.length - request.session.currency.defaultFractionDigits - 1);
    var priceSplit = tPrice.split(currencySeperator);
    if (priceSplit && priceSplit.length && priceSplit.length === 2) {
      var beforeSpecialChar = priceSplit[0];
      var afterSpecialChar = priceSplit[1];
      if (beforeSpecialChar.indexOf(',') > 0) {
        beforeSpecialChar = beforeSpecialChar.replace(/,/g, '');
      }
      if (beforeSpecialChar.indexOf('.') > 0) {
        beforeSpecialChar = beforeSpecialChar.replace(/\./g, '');
      }
      currencySeperator = '.';
      tPrice = beforeSpecialChar.concat(currencySeperator).concat(afterSpecialChar);
    }
  }
  return tPrice;
}

/**
 * Sets the relevant product search model properties, depending on the parameters provided
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {Object} httpParams - Query params
 * @param {dw.catalog.Category} selectedCategory - Selected category
 * @param {dw.catalog.SortingRule} sortingRule - Product grid sort rule
 */
base.setProductProperties = function (productSearch, httpParams, selectedCategory, sortingRule) {
  var searchPhrase;

  if (httpParams.q) {
    searchPhrase = httpParams.q;
    productSearch.setSearchPhrase(searchPhrase);
  }
  if (selectedCategory) {
    productSearch.setCategoryID(selectedCategory.ID);
  }
  if (httpParams.pid) {
    productSearch.setProductID(httpParams.pid);
  }
  if (httpParams.pmin) {
    productSearch.setPriceMin(Number(removeExtraChars(httpParams.pmin)));
    // productSearch.setPriceMin(Number(httpParams.pmin.replace(/,/g, '')));
  }
  if (httpParams.pmax) {
    productSearch.setPriceMax(Number(removeExtraChars(httpParams.pmax)));
    // productSearch.setPriceMax(Number(httpParams.pmax.replace(/,/g, '')));
  }
  if (httpParams.pmid) {
    productSearch.setPromotionID(httpParams.pmid);
  }
  if (sortingRule) {
    productSearch.setSortingRule(sortingRule);
  }

  productSearch.setRecursiveCategorySearch(true);
};

module.exports = base;
