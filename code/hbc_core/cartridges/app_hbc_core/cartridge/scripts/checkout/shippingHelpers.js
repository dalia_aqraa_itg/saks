'use strict';
var base = module.superModule;
var collections = require('*/cartridge/scripts/util/collections');
var ArrayList = require('dw/util/ArrayList');
var ShippingMgr = require('dw/order/ShippingMgr');
var Logger = require('dw/system/Logger');
var HashMap = require('dw/util/HashMap');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var ShippingMethodModel = require('*/cartridge/models/shipping/shippingMethod');
var EDDFacade = require('*/cartridge/scripts/services/EDDFacade');
var preferences = require('*/cartridge/config/preferences');
var arrayHelper = require('*/cartridge/scripts/util/array');

/**
 * Returns the first shipping method (and maybe prevent in store pickup)
 * @param {dw.util.Collection} methods - Applicable methods from ShippingShipmentModel
 * @param {boolean} filterPickupInStore - whether to exclude PUIS method
 * @returns {dw.order.ShippingMethod} - the first shipping method (maybe non-PUIS)
 */
function getFirstApplicableShippingMethod(methods, filterPickupInStore) {
  var method;
  var iterator = methods.iterator();
  while (iterator.hasNext()) {
    method = iterator.next();
    if (!filterPickupInStore || (filterPickupInStore && !method.custom.storePickupEnabled)) {
      break;
    }
  }
  return method;
}

base.getEDDDateMap = function (EDDResponse) {
  var ShippingMethodMap = new HashMap();
  try {
    var parseRes = JSON.parse(EDDResponse);
    Object.keys(parseRes).forEach(function (key) {
      var value = parseRes[key];
      if (value) {
        ShippingMethodMap.put(key, value);
      }
    });
    return ShippingMethodMap;
  } catch (e) {
    Logger.error('ERROR while parseing the EDD response', e);
  }
  return ShippingMethodMap;
};

function getEDDFormattedDate(calendar) {
  return StringUtils.formatCalendar(calendar, 'yyyy-MM-dd');
}

function getEDDDateIfNoEDD(shippingMethod) {
  if (shippingMethod && shippingMethod.custom && shippingMethod.custom.estimatedDateNoEDD) {
    var currentDate = new Calendar();
    currentDate.add(Calendar.DATE, shippingMethod.custom.estimatedDateNoEDD);
    return getEDDFormattedDate(currentDate);
  }
  return null;
}

base.getEDDdate = function (shippingMethod, EDD) {
  if (shippingMethod) {
    if (EDD && EDD.containsKey(shippingMethod.ID)) {
      if (EDD.get(shippingMethod.ID)) {
        var eddDate = EDD.get(shippingMethod.ID).split('-');
        var dareStr = '' + eddDate[1] + '/' + eddDate[2] + '/' + eddDate[0];
        var date = new Date(dareStr);
        var formattedDate = StringUtils.formatCalendar(new Calendar(date), 'MMM dd, yyyy');
        return {
          EstimatedDate: formattedDate, // This will be used on the Storefront
          OrdercreationEstimatedDate: getEDDFormattedDate(new Calendar(date)) // This will be store for order creation Call
        };
      }
    } else {
      return {
        EstimatedDate: shippingMethod && shippingMethod.custom ? shippingMethod.custom.estimatedArrivalTime : null,
        OrdercreationEstimatedDate: getEDDDateIfNoEDD(shippingMethod)
      };
    }
  }
  return null;
};

/**
 * Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * @param {dw.order.Shipment} shipment - the target Shipment
 * @param {Object} [address] - optional address object
 * @returns {dw.util.Collection} an array of ShippingModels
 */
base.getApplicableShippingMethods = function (shipment, address) {
  var shippingCostsMap = new HashMap();
  var BasketMgr = require('dw/order/BasketMgr');
  var HookMgr = require('dw/system/HookMgr');
  var Transaction = require('dw/system/Transaction');
  var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');

  if (!shipment) {
    return null;
  }
  var shipmentShippingModel = ShippingMgr.getShipmentShippingModel(shipment);
  var shippingMethods = null;
  if (address) {
    shippingMethods = shipmentShippingModel.getApplicableShippingMethods(address);
  } else {
    shippingMethods = shipmentShippingModel.getApplicableShippingMethods();
  }

  var EDDMap;
  if ('EDDResponse' in shipment.custom && !empty(shipment.custom.EDDResponse)) {
    EDDMap = base.getEDDDateMap(JSON.parse(shipment.custom.EDDResponse));
  }

  // Move Pickup in store method to the end of the list
  var pickupInstoreMethod = collections.find(shippingMethods, function (method) {
    return method.custom.storePickupEnabled;
  });
  if (pickupInstoreMethod) {
    shippingMethods.remove(pickupInstoreMethod);
    // shippingMethods.add(pickupInstoreMethod);
  }
  var isBOPISShipment = false;
  var shippingMethod = shipment.shippingMethod;
  if (!empty(shippingMethod) && shippingMethod.custom.storePickupEnabled) {
    isBOPISShipment = true;
  }

  // Changes done to identifiy the promotional Shipping Cost for each shipping method (SFDEV-4668)
  if (shipment.getShippingAddress() && !isBOPISShipment) {
    session.privacy.callVertax = false; // Set a session variable to not make the TAX call in Cart calculate, while we are trying to find out the promotional Shipping Cost
    var currentBasket = BasketMgr.getCurrentBasket();
    if (currentBasket) {
      var currentShippingMethod = currentBasket.getDefaultShipment().getShippingMethod() || ShippingMgr.getDefaultShippingMethod();

      // Transaction controls are for fine tuning the performance of the data base interactions when calculating shipping methods
      Transaction.begin();
      collections.forEach(shippingMethods, function (shippingMethod) {
        updateShipmentShippingMethod(shipment.getID(), shippingMethod.getID(), shippingMethod, shippingMethods);
        HookMgr.callHook('dw.order.calculate', 'calculate', currentBasket);
        shippingCostsMap.put(shippingMethod.getID(), shipment.adjustedShippingTotalPrice);
      });
      Transaction.rollback();

      // finally Clear up the session variable to allow vartex call.
      delete session.privacy.callVertax;

      Transaction.wrap(function () {
        updateShipmentShippingMethod(currentBasket.getDefaultShipment().getID(), currentShippingMethod.getID(), currentShippingMethod, shippingMethods);
        HookMgr.callHook('dw.order.calculate', 'calculate', currentBasket);
      });
    }
  }

  // Filter out whatever the method associated with in store pickup
  var filteredMethods = [];
  collections.forEach(shippingMethods, function (shippingMethod) {
    filteredMethods.push(new ShippingMethodModel(shippingMethod, shipment, EDDMap, shippingCostsMap));
  });

  // If we don't have both a country and a state, use defaults in their place to ensure
  // ShopRunner is listed as a shipping method
  if (shipment && shipment.getShippingAddress()) {
    var stateCode = shipment.getShippingAddress().getStateCode();
    var countryCode = shipment.getShippingAddress().getCountryCode();

    if (!stateCode || countryCode.getValue() === '') {
      shipmentShippingModel = ShippingMgr.getShipmentShippingModel(shipment);

      // eslint-disable-next-line no-new-object
      var addressObj = new Object();
      addressObj.countryCode = countryCode.getValue() !== '' ? countryCode.getValue() : 'US';
      addressObj.stateCode = stateCode ? stateCode : 'NY'; // eslint-disable-line
      addressObj.postalCode = shipment.getShippingAddress().getPostalCode();
      addressObj.city = shipment.getShippingAddress().getCity();

      shippingMethods = shipmentShippingModel.getApplicableShippingMethods(addressObj);

      var iterator = shippingMethods.iterator();
      while (iterator.hasNext()) {
        var shippingMethod = iterator.next();
        if (shippingMethod.ID === 'shoprunner') {
          filteredMethods.unshift(new ShippingMethodModel(shippingMethod, shipment));
          break;
        }
      }
    }
  }
  return filteredMethods;
};

/**
 * Gets the drops ship item shipping method id
 * @returns {Object} the drop ship default shipping method
 */
base.getDropShippingMethod = function () {
  var dropShipDefaultMethod = null;
  var Locale = require('dw/util/Locale');
  var currentLocale = Locale.getLocale(request.locale);
  var BasketMgr = require('dw/order/BasketMgr');
  var OrderModel = require('*/cartridge/models/order');
  var shipTypeTextsInBasket = new ArrayList();
  var currentBasket = BasketMgr.getCurrentBasket();
  // preference holds the key value for product RestrictedShipTet vs shipping method
  var dropShippingMethodsValueMap = preferences.dropShippingMethodsValueMap;
  var basketModel = new OrderModel(currentBasket, { usingMultiShipping: false, countryCode: currentLocale.country, containerView: 'basket' });
  var basketItems = basketModel.items;
  // in regular item with drip ship items
  if (basketModel.items.dropShipItems.length > 0 && basketModel.items.dropShipItems.length < basketItems.items.length && dropShippingMethodsValueMap) {
    var shipTypeTextKeysJSON = JSON.parse(dropShippingMethodsValueMap);
    var shipTypeTextKeys = Object.keys(shipTypeTextKeysJSON);
    // push all ship type text's of product in basket in to an array
    for (var j = 0; j < basketModel.items.dropShipItems.length; j++) {
      var dropShipItem = basketModel.items.dropShipItems[j];
      if (dropShipItem.shipTypeText) {
        shipTypeTextsInBasket.add(dropShipItem.shipTypeText);
      }
    }
    // filter out the shipping methods
    if (shipTypeTextsInBasket.length > 0 && shipTypeTextKeys.length > 0) {
      for (var i = 0; i < shipTypeTextKeys.length; i++) {
        var indShipText = shipTypeTextKeys[i];
        var key = shipTypeTextsInBasket.contains(indShipText);
        if (key) {
          dropShipDefaultMethod = shipTypeTextKeysJSON[indShipText];
          break;
        }
      }
    }
  }
  return dropShipDefaultMethod;
};

/**
 * Apply drop ship items if there are any items
 * @param {Object} shipment - shipment
 * @returns {Object} the drop ship default shipping method
 */
base.applyDropShipDefaultMethod = function (shipment) {
  var Transaction = require('dw/system/Transaction');
  var appliedDropShipMethodID = null;
  try {
    var dropShipDefaultMethodID = base.getDropShippingMethod();
    // drop ship items and not equal to shoprunner
    if (dropShipDefaultMethodID && shipment.shippingMethodID !== 'shoprunner') {
      var applicableShippingMethods = base.getApplicableShippingMethods(shipment);
      var matchingMethod = arrayHelper.find(applicableShippingMethods, function (method) {
        return method.ID === dropShipDefaultMethodID;
      });
      if (matchingMethod) {
        Transaction.wrap(function () {
          // set shipping method
          base.selectShippingMethod(shipment, matchingMethod.ID);
        });
        appliedDropShipMethodID = matchingMethod.ID;
      }
    }
  } catch (e) {
    Logger.error('Error in applyDropShipDefaultMethod ' + e.message);
    return appliedDropShipMethodID;
  }
  return appliedDropShipMethodID;
};

var baseSelectShippingMethod = base.selectShippingMethod.bind(base);

// eslint-disable-next-line consistent-return
base.selectShippingMethod = function (shipment, shippingMethodID, shippingMethods, address) {
  if (shippingMethodID === 'shoprunner' && !address) {
    // eslint-disable-next-line no-redeclare
    var shippingMethods = ShippingMgr.getAllShippingMethods();

    var isShipmentSet = false;

    var iterator = shippingMethods.iterator();
    while (iterator.hasNext()) {
      var shippingMethod = iterator.next();
      if (shippingMethod.ID === shippingMethodID) {
        shipment.setShippingMethod(shippingMethod);
        isShipmentSet = true;
        break;
      }
    }

    if (!isShipmentSet) {
      return baseSelectShippingMethod(shipment, shippingMethodID, shippingMethods, address);
    }
  } else {
    // eslint-disable-next-line block-scoped-var
    return baseSelectShippingMethod(shipment, shippingMethodID, shippingMethods, address);
  }
};

/**
 * Mark a shipment to be picked up instore
 * @param {dw.order.Shipment} shipment - line item container to be marked for pickup instore
 * @param {string} storeId - Id of the store for shipment to be picked up from.
 */
base.markShipmentForPickup = function (shipment, storeId) {
  var StoreMgr = require('dw/catalog/StoreMgr');
  var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
  var Transaction = require('dw/system/Transaction');

  var store = StoreMgr.getStore(storeId);
  var storeInventory = ProductInventoryMgr.getInventoryList(store.custom.inventoryListId);
  Transaction.wrap(function () {
    collections.forEach(shipment.productLineItems, function (lineItem) {
      lineItem.custom.fromStoreId = storeId; // eslint-disable-line no-param-reassign
      lineItem.setProductInventoryList(storeInventory);
    });
    shipment.custom.fromStoreId = storeId; // eslint-disable-line no-param-reassign
  });
};

/**
 * Remove pickup instore indicators from the shipment
 * @param {dw.order.Shipment} shipment - Shipment to be marked
 */
base.markShipmentForShipping = function (shipment) {
  var Transaction = require('dw/system/Transaction');

  Transaction.wrap(function () {
    collections.forEach(shipment.productLineItems, function (lineItem) {
      lineItem.custom.fromStoreId = null; // eslint-disable-line no-param-reassign
      lineItem.setProductInventoryList(null);
    });
    shipment.custom.fromStoreId = null; // eslint-disable-line no-param-reassign
  });
};
/**
 * create the customer address results html
 * @param {Array} customerAddressBook - an array of objects that contains address information
 * @param {string} email - email to set
 * @param {Object} preferredAddress - Preferred Address object
 * @param {Object} selectedAddress - selectedAddress Address object
 * @returns {string} The rendered HTML
 */
base.createCustomerAddressHtml = function (customerAddressBook, email, preferredAddress, selectedAddress) {
  var Template = require('dw/util/Template');

  var context = new HashMap();
  var object = { addresses: customerAddressBook, preferredAddress: preferredAddress, selectedAddress: selectedAddress };

  Object.keys(object).forEach(function (key) {
    var address = object[key];
    for (var i = 0; i < address.length; i++) {
      var item = address[i];
      item.email = email;
    }
    context.put(key, address);
  });

  var template = new Template('checkout/shipping/checkoutCustomerAddress');
  return template.render(context).text;
};

/**
 * Sets the default ShippingMethod for a Shipment, if absent
 * @param {dw.order.Shipment} shipment - the target Shipment object
 */
base.ensureShipmentHasMethod = function (shipment) {
  var shippingMethod = shipment.shippingMethod;
  if (!shippingMethod) {
    var methods = ShippingMgr.getShipmentShippingModel(shipment).applicableShippingMethods;
    var defaultMethod = ShippingMgr.getDefaultShippingMethod();

    if (!defaultMethod) {
      // If no defaultMethod set, just use the first one
      shippingMethod = getFirstApplicableShippingMethod(methods, true);
    } else {
      // Look for defaultMethod in applicableMethods
      shippingMethod = collections.find(methods, function (method) {
        return method.ID === defaultMethod.ID;
      });
    }

    // If found, use it.  Otherwise return the first one
    if (!shippingMethod && methods && methods.length > 0) {
      shippingMethod = getFirstApplicableShippingMethod(methods, true);
    }

    if (shippingMethod) {
      shipment.setShippingMethod(shippingMethod);
    }
  } else {
    // BOPIS + GWP Order: The shipment with only bonus items should be a Ship-To home shipment, if the bonus items are not selected for Store Pick-Up
    if (shippingMethod.custom.storePickupEnabled && shipment.adjustedMerchandizeTotalPrice == 0) {
      var markBonusShipmentForShipping = false;
      collections.forEach(shipment.productLineItems, function (lineItem) {
        if (empty(lineItem.custom.fromStoreId)) {
          markBonusShipmentForShipping = true;
        }
      });

      if (markBonusShipmentForShipping) {
        var Transaction = require('dw/system/Transaction');
        Transaction.wrap(function () {
          shipment.custom.fromStoreId = null;
          shipment.custom.shipmentType = null;
          shipment.setShippingMethod(ShippingMgr.getDefaultShippingMethod());
        });
      }
    }
  }
};

function updateShipmentShippingMethod(shipmentID, shippingMethodID, shippingMethod, shippingMethods) {
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();
  if (currentBasket) {
    var shipment = currentBasket.getShipment(shipmentID);

    // Tries to set the shipment shipping method to the passed one.
    for (var i = 0; i < shippingMethods.length; i++) {
      var method = shippingMethods[i];

      if (!shippingMethod) {
        if (!method.ID.equals(shippingMethodID)) {
          continue;
        }
      } else if (method !== shippingMethod) {
        continue;
      }

      // Sets this shipping method.
      shipment.setShippingMethod(method);
      return;
    }

    var defaultShippingMethod = ShippingMgr.getDefaultShippingMethod();
    if (shippingMethods.contains(defaultShippingMethod)) {
      // Sets the default shipping method if it is applicable.
      shipment.setShippingMethod(defaultShippingMethod);
    } else if (shippingMethods.length > 0) {
      // Sets the first shipping method in the applicable list.
      shipment.setShippingMethod(shippingMethods.iterator().next());
    } else {
      // Invalidates the current shipping method selection.
      shipment.setShippingMethod(null);
    }
  }
  return;
}

module.exports = base;
