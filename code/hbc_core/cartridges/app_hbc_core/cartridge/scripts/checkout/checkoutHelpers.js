/* eslint-disable guard-for-in */
'use strict';

var base = module.superModule;

var server = require('server');

var collections = require('*/cartridge/scripts/util/collections');
var Money = require('dw/value/Money');
var priceHelper = require('*/cartridge/scripts/helpers/pricing');
var Resource = require('dw/web/Resource');

var PaymentInstrument = require('dw/order/PaymentInstrument');
var Transaction = require('dw/system/Transaction');
var HookMgr = require('dw/system/HookMgr');
var OrderMgr = require('dw/order/OrderMgr');
var PaymentMgr = require('dw/order/PaymentMgr');
var preferences = require('*/cartridge/config/preferences');
var basketHashHelper = require('*/cartridge/scripts/helpers/basketHashHelpers');
var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');

/**
 * renders the user's stored payment Instruments
 * @param {Object} req - The request object
 * @param {Object} accountModel - The account model for the current customer
 * @param {Object} orderModel - The order model for the current customer
 * @returns {string|null} newly stored payment Instrument
 */
base.getRenderedPaymentInstruments = function (req, accountModel, orderModel) {
  var result;
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

  if (req.currentCustomer.raw.authenticated && req.currentCustomer.raw.registered && req.currentCustomer.raw.profile.wallet.paymentInstruments.getLength()) {
    var context;
    var template = 'checkout/billing/paymentOptions/registeredPaymentInstrument';

    context = {
      customer: accountModel,
      order: orderModel
    };
    result = renderTemplateHelper.getRenderedHtml(context, template);
  }

  return result || null;
};

/**
 * saves payment instruemnt to customers wallet
 * @param {Object} billingData - billing information entered by the user
 * @param {dw.order.Basket} currentBasket - The current basket
 * @param {dw.customer.Customer} customer - The current customer
 * @returns {dw.customer.CustomerPaymentInstrument} newly stored payment Instrument
 */
base.savePaymentInstrumentToWallet = function (billingData, currentBasket, customer) {
  // eslint-disable-line
  if (empty(currentBasket.custom.tokenExRetry)) {
    var wallet = customer.getProfile().getWallet();
    var paymentInstruments = wallet.paymentInstruments;
    if (billingData.saveCardDefault && !empty(paymentInstruments)) {
      // Make False to other Cards.
      for (var i = 0; i < paymentInstruments.length; i++) {
        var pi = paymentInstruments[i];
        Transaction.wrap(function () {
          //eslint-disable-line
          pi.custom.defaultCreditCard = false;
        });
      }
    }

    return Transaction.wrap(function () {
      var storedPaymentInstrument = wallet.createPaymentInstrument(PaymentInstrument.METHOD_CREDIT_CARD);
      var date = new Date();
      var profile = customer.getProfile();
      var paymentInfornation = billingData.paymentInformation;

      storedPaymentInstrument.setCreditCardHolder(billingData.paymentInformation.cardOwner.value);
      storedPaymentInstrument.setCreditCardNumber(billingData.paymentInformation.token);
      storedPaymentInstrument.setCreditCardType(billingData.paymentInformation.cardType.value);

      if (
        !empty(billingData.paymentInformation.expirationMonth.value) &&
        billingData.paymentInformation.expirationMonth &&
        !isNaN(billingData.paymentInformation.expirationMonth.value)
      ) {
        storedPaymentInstrument.setCreditCardExpirationMonth(billingData.paymentInformation.expirationMonth.value);
      }
      if (
        !empty(billingData.paymentInformation.expirationYear.value) &&
        billingData.paymentInformation.expirationYear &&
        !isNaN(billingData.paymentInformation.expirationYear.value)
      ) {
        storedPaymentInstrument.setCreditCardExpirationYear(billingData.paymentInformation.expirationYear.value);
      }

      storedPaymentInstrument.custom.defaultCreditCard = !!billingData.saveCardDefault;

      if (tsysHelper.isTsysMode()) {
        if (billingData.paymentInformation.cardType && billingData.paymentInformation.cardType.value === 'TCC') {
          storedPaymentInstrument.setCreditCardToken(billingData.paymentInformation.cardNumber.value);
          storedPaymentInstrument.custom.tccExpirationDate = paymentInfornation.tccExpirationDate;
        } else {
          storedPaymentInstrument.setCreditCardToken(billingData.paymentInformation.token);
        }
      } else {
        storedPaymentInstrument.setCreditCardToken(billingData.paymentInformation.token);
      }

      profile.custom.lastPaymentModified = date.toISOString();

      profile.custom.accountModifiedDate = date.toISOString();

      return storedPaymentInstrument;
    });
  }
};

/**
 * handles the payment authorization for each payment instrument
 * @param {dw.order.Order} order - the order object
 * @param {string} orderNumber - The order number for the order
 * @param {Object} req - request object
 * @returns {Object} an error object
 */
base.handlePayments = function (order, orderNumber, req) {
  var result = {};
  var paymentInstruments = order.paymentInstruments;
  if (order.totalNetPrice.value !== 0.0 || (order.totalNetPrice.value === 0 && paymentInstruments.length > 0)) {
    if (paymentInstruments.length === 0) {
      Transaction.wrap(function () {
        order.trackOrderChange('Order Failed: No payment instrument');
        OrderMgr.failOrder(order);
      });
      result.error = true;
    }
    var args = {};
    args.session = req.session;
    args.Order = order;
    args.remoteAddress = req.remoteAddress;
    args.customer = req.currentCustomer.raw;
    args.deviceFingerPrintID = req.session.privacyCache.get('transactionUUID');
    // Capture the no of failure attempts executed by the customer in the same session
    var attempts = req.session.privacyCache.get('failAttempts') || 0;
    attempts += 1;
    req.session.privacyCache.set('failAttempts', attempts);

    args.failAttempts = attempts;

    if (!result.error) {
      // we are not required to do auth call for every payment instrument.
      // The current SPG/IPA does a composite Auth call which loops through the payment instruments.
      // Currently, executing any payment instrument Auth hook should take care of everything
      var paymentInstrument = paymentInstruments[0];
      var paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.paymentMethod).paymentProcessor;
      var authorizationResult;
      if (paymentProcessor === null) {
        Transaction.begin();
        paymentInstrument.paymentTransaction.setTransactionID(orderNumber);
        Transaction.commit();
      } else {
        if (HookMgr.hasHook('app.payment.processor.' + paymentProcessor.ID.toLowerCase())) {
          authorizationResult = HookMgr.callHook(
            'app.payment.processor.' + paymentProcessor.ID.toLowerCase(),
            'Authorize',
            orderNumber,
            paymentInstrument,
            paymentProcessor,
            args
          );
        } else {
          authorizationResult = HookMgr.callHook('app.payment.processor.default', 'Authorize');
        }

        if (authorizationResult.error) {
          Transaction.wrap(function () {
            order.trackOrderChange('Order Failed: payment auth error ');
            OrderMgr.failOrder(order);
          });
          result.error = true;
        }
      }
    }
  }

  return result;
};

/**
 * Get list price for a product
 *
 * @param {dw.catalog.ProductPriceModel} priceModel - Product price model
 * @return {dw.value.Money} - List price
 */
function getListPrice(priceModel) {
  var price = Money.NOT_AVAILABLE;
  var priceBook;
  var priceBookPrice;

  if (priceModel.price.valueOrNull === null && priceModel.minPrice) {
    return priceModel.minPrice;
  }

  priceBook = priceHelper.getRootPriceBook(priceModel.priceInfo.priceBook);
  priceBookPrice = priceModel.getPriceBookPrice(priceBook.ID);

  if (priceBookPrice.available) {
    return priceBookPrice;
  }

  price = priceModel.price.available ? priceModel.price : priceModel.minPrice;

  return price;
}

// Method to fetch the List Price from Reg pricebook and not MSRP pricebook for the O5th Site - Ticket SFDEV-6849
function getItemListPrice(priceModel) {
  var price = Money.NOT_AVAILABLE;
  var priceBook;
  var priceBookPrice;

  if (priceModel.price.valueOrNull === null && priceModel.minPrice) {
    return priceModel.minPrice;
  }
  var o5RegPricebookId = 'o5a-reg-price'; // The pricebook Id's will not change. So not adding it to a custom preference
  var o5MSRPPricebookId = 'o5a-msrp-price';

  priceBook = priceHelper.getRootPriceBook(priceModel.priceInfo.priceBook);
  if (priceBook.ID === o5MSRPPricebookId) {
    priceBookPrice = priceModel.getPriceBookPrice(o5RegPricebookId);
  } else {
    priceBookPrice = priceModel.getPriceBookPrice(priceBook.ID);
  }

  if (priceBookPrice.available) {
    return priceBookPrice;
  }

  price = priceModel.price.available ? priceModel.price : priceModel.minPrice;

  return price;
}

/** Promotion attached to each of this price adjustment. Any promotion except Associate and FDD will be added to the custom attribute "productpromotiondiscount"
 *  productpromotiondiscount = Pricebook promotion discount + All product promotion Discounts + All order promotion discount except Associate and FDD promotion
 *  associatediscount = Only associate promotion discount
 *  fdddiscount = only FDD promotion discount
 *  If the promotion is of type Associate, save it to "associatediscount" custom attribute
 *  If the promotion is of type FDD, save it to "fdddiscount" custom attribute
 *  @param {dw.order.Basket} currentBasket - Basket dw object
 *  @param {Customer} customer  - customer dw object
 */
// eslint-disable-next-line no-unused-vars
base.prorateLineItemDiscounts = function (currentBasket, _customer) {
  var productLineItems = currentBasket.getProductLineItems();
  collections.forEach(productLineItems, function (lineItem) {
    Transaction.wrap(function () {
      var pricebookDiscount = new Money(0, currentBasket.getCurrencyCode());
      var productPromotionDiscountWOPricebook = new Money(0, currentBasket.getCurrencyCode());
      var associateDiscount = new Money(0, currentBasket.getCurrencyCode());
      var specialVendorDiscount = new Money(0, currentBasket.getCurrencyCode());
      var fddDiscount = new Money(0, currentBasket.getCurrencyCode());
      // eslint-disable-next-line no-param-reassign
      delete lineItem.custom.productPromotionDiscount;
      // eslint-disable-next-line no-param-reassign
      delete lineItem.custom.productPromotionAssociateDiscount;
      // eslint-disable-next-line no-param-reassign
      delete lineItem.custom.productPromotionSpecialVendorDiscount;
      // eslint-disable-next-line no-param-reassign
      delete lineItem.custom.productPromotionFirstDayDiscount;
      delete lineItem.custom.productPromotionDiscountWOPricebook;
      var proratePricesMap = lineItem.getProratedPriceAdjustmentPrices();
      var priceModel = lineItem.product.priceModel;
      var priceBook = priceModel.priceInfo.priceBook;
      var isPromotionPriceBook;
      var listPrice = new Money(getItemListPrice(priceModel) * lineItem.quantity.value, currentBasket.getCurrencyCode());

      if (priceBook && 'isPromotionPriceBook' in priceBook.custom && priceBook.custom.isPromotionPriceBook) {
        pricebookDiscount = listPrice.subtract(lineItem.getPrice());
        lineItem.custom.isPromotionPriceBook = true;
      } else {
        lineItem.custom.isPromotionPriceBook = false;
      }
      lineItem.custom.retailprice = listPrice.value;

      if (proratePricesMap) {
        var priceAdjKeys = proratePricesMap.keySet();
        var proratedPrice;

        collections.forEach(priceAdjKeys, function (priceAdjKey) {
          var promotion = priceAdjKey.promotion;
          if (proratePricesMap.get(priceAdjKey) && proratePricesMap.get(priceAdjKey).value) {
            proratedPrice = new Money(Math.abs(proratePricesMap.get(priceAdjKey).value), currentBasket.getCurrencyCode());
            if (!empty(promotion) && promotion.custom.promotionType.value === 'Associate') {
              associateDiscount = associateDiscount.add(proratedPrice);
            } else if (!empty(promotion) && promotion.custom.promotionType.value === 'SpecialVendorDisc') {
              specialVendorDiscount = specialVendorDiscount.add(proratedPrice);
            } else if (!empty(promotion) && promotion.custom.promotionType.value === 'FDD') {
              fddDiscount = fddDiscount.add(proratedPrice);
            } else {
              pricebookDiscount = pricebookDiscount.add(proratedPrice);
              productPromotionDiscountWOPricebook = productPromotionDiscountWOPricebook.add(proratedPrice);
            }
          }
        });
      }
      // eslint-disable-next-line no-param-reassign
      if (productPromotionDiscountWOPricebook.value > 0) lineItem.custom.productPromotionDiscountWOPricebook = productPromotionDiscountWOPricebook.value;
      // eslint-disable-next-line no-param-reassign
      if (pricebookDiscount.value > 0) lineItem.custom.productPromotionDiscount = pricebookDiscount.value;
      // eslint-disable-next-line no-param-reassign
      if (associateDiscount.value > 0) lineItem.custom.productPromotionAssociateDiscount = associateDiscount.value;
      // eslint-disable-next-line no-param-reassign
      if (specialVendorDiscount.value > 0) lineItem.custom.productPromotionSpecialVendorDiscount = specialVendorDiscount.value;
      // eslint-disable-next-line no-param-reassign
      if (fddDiscount.value > 0) lineItem.custom.productPromotionFirstDayDiscount = fddDiscount.value;
    });
  });
};

/**
 * Prepares the InstorePickUp form
 * @returns {Object} processed InstorePickUp form object
 */
base.prepareInstorePickupForm = function () {
  var instorePickupForm = server.forms.getForm('instorepickup');
  //instorePickupForm.clear();
  return instorePickupForm;
};

/**
 * Validate shipping form fields
 * @param {Object} form - the form object with pre-validated form fields
 * @returns {Object} the names of the invalid form fields
 */
base.validateInstorePickUpForm = function (form) {
  return base.validateFields(form);
};

/**
 * compare shipping address from addresses in customer address book and returns the precise address
 * @param {dw.customer.CustomerAddress} shippingAddress - shipping address
 * @param {Object} addressBook - customer address book
 * @returns {dw.customer.CustomerAddress} selectedAddress - selected address
 */
base.getSelectedCustomerAddress = function (shippingAddress, addressBook) {
  var customerAddressBook = addressBook;
  var selectedAddress = null;
  if (shippingAddress && customerAddressBook.length > 0) {
    customerAddressBook.forEach(customerAddressBook, function (address) {
      if (
        address.firstName === shippingAddress.firstName &&
        address.lastName === shippingAddress.lastName &&
        address.address1 === shippingAddress.address1 &&
        address.address2 === shippingAddress.address2 &&
        address.city === shippingAddress.city &&
        address.postalCode === shippingAddress.postalCode &&
        address.stateCode === shippingAddress.stateCode
      ) {
        selectedAddress = address;
      }
    });
  }
  if (!selectedAddress && customerAddressBook.length > 0) {
    selectedAddress = customerAddressBook[0];
  }
  return selectedAddress;
};

/**
 * Copy information from address object and save it in the system
 * @param {Object} req - http request object
 * @param {dw.customer.CustomerAddress} newAddress - newAddress to save information into
 * @param {*} address - Address to copy from
 * @returns {dw.customer.CustomerAddress} newAddress - copied address
 */
base.updateAddressFields = function (req, newAddress, address) {
  var Locale = require('dw/util/Locale');
  var currentLocale = Locale.getLocale(req.locale.id);
  newAddress.setAddress1(address.address1.value || '');
  newAddress.setAddress2(address.address2.value || '');
  newAddress.setCity(address.city.value || '');
  newAddress.setFirstName(address.firstName.value || '');
  newAddress.setLastName(address.lastName.value || '');
  newAddress.setPhone(address.phone.value || '');
  newAddress.setPostalCode(address.postalCode.value || '');
  if (address.states && address.states.stateCode && address.states.stateCode.value) {
    newAddress.setStateCode(address.states.stateCode.value);
  }
  newAddress.setCountryCode(currentLocale.country);
  return newAddress;
};

/**
 * sets the gift message on a shipment
 * @param {dw.order.Shipment} shipment - Any shipment for the current basket
 * @param {boolean} isGift - is the shipment a gift
 * @param {string} giftMessage - The gift message the user wants to attach to the shipment
 * @param {string} giftRecipientName - The gift giftRecipientName the user wants to attach to the shipment
 * @returns {Object} object containing error information
 */
base.setGift = function (shipment, isGift, giftMessage, giftRecipientName) {
  var result = {
    error: false,
    errorMessage: null
  };
  try {
    Transaction.wrap(function () {
      shipment.setGift(isGift);

      if (isGift && giftMessage) {
        shipment.setGiftMessage(giftMessage);
        shipment.custom.giftRecipientName = giftRecipientName; // eslint-disable-line
      } else {
        shipment.setGiftMessage(null);
        shipment.custom.giftRecipientName = null; // eslint-disable-line
      }
    });
  } catch (e) {
    result.error = true;
    result.errorMessage = Resource.msg('error.message.could.not.be.attached', 'checkout', null);
  }

  return result;
};

/**
 * sets the setSignatureRequired on a shipment
 * @param {dw.order.Shipment} shipment - Any shipment for the current basket
 * @param {boolean} signatureRequired - is the shipment a setSignatureRequired
 * @returns {Object} object containing error information
 */
base.setSignatureRequired = function (shipment, signatureRequired) {
  var result = {
    error: false,
    errorMessage: null
  };
  try {
    Transaction.wrap(function () {
      if (signatureRequired) {
        shipment.custom.signatureRequired = signatureRequired; // eslint-disable-line
      } else {
        // eslint-disable-next-line no-param-reassign
        shipment.custom.signatureRequired = false;
      }
    });
  } catch (e) {
    result.error = true;
  }
  return result;
};

/**
 * get the shipment map with shippingUUID as key and shipment as value.
 * @param {dw.order.Order} order - The current order
 * @returns {dw.util.HashMap} shipmentMap - newly created map.
 */
function getShipmentMap(order) {
  var HashMap = require('dw/util/HashMap');
  var shipmentMap = new HashMap();
  collections.forEach(order.shipments, function (shipment) {
    shipmentMap.put(shipment.UUID, shipment);
  });
  return shipmentMap;
}

/**
 * updates the shipping address details to item object
 * @param {Object} productLineItems - productLinteItems modal object
 * @param {dw.util.HashMap} shipmentMap - Shipping Map
 */
function updateShippingAddressToItem(productLineItems, shipmentMap) {
  productLineItems.items.forEach(function (item) {
    let shipment = shipmentMap.get(item.shipmentUUID);
    let shippingAddress = shipment.shippingAddress;
    if (shippingAddress != null) {
      if (shippingAddress.countryCode != null) {
        item.shippingCountryCode = shippingAddress.countryCode.value; // eslint-disable-line
      }
      if (shippingAddress.stateCode != null) {
        item.shippingStateCode = shippingAddress.stateCode; // eslint-disable-line
      }
      if (shippingAddress.postalCode != null) {
        if (shippingAddress.countryCode != null && shippingAddress.countryCode.value === 'US') {
          item.shippingPostalCode = shippingAddress.postalCode.substr(0, 5); // eslint-disable-line
        } else {
          item.shippingPostalCode = shippingAddress.postalCode; // eslint-disable-line
        }
      }
    }
  });
}

/**
 * Reserves the inventory of the products in SFCC OMS Order Management.
 * @param {dw.order.Order} order - The current user's order
 * @returns {Object} an availability
 */
base.reserveInventory = function (order) {
  var InventoryService = require('*/cartridge/scripts/services/InventoryService');
  var ProductLineItemsModel = require('*/cartridge/models/productLineItems');
  var productLineItems = new ProductLineItemsModel(order.productLineItems, 'basket');
  var shipmentMap = getShipmentMap(order);
  updateShippingAddressToItem(productLineItems, shipmentMap);
  return InventoryService.reserveInventory(productLineItems, 'reserve', order);
};

/**
 * Cancels the reserved inventory of the products in SFCC OMS Order Management.
 * @param {dw.order.Order} order - The current user's order
 * @returns {Object} an availability
 */
base.cancelInventoryReservation = function (order) {
  if (order.custom && order.custom.reservationID) {
    var InventoryService = require('*/cartridge/scripts/services/InventoryService');
    return InventoryService.cancelReservation(order.custom.reservationID);
  }
  return {};
};

/**
 * Adds address to basket
 *
 * @param {dw/customer/Customer} customer - customer for which the profile is set
 * @param {Object} preferredAddress - preferred address to be set in customer
 * @param {dw/order/PaymentInstrument} defaultPI - default Payment instrument to be set in customer
 * @param {dw/order/Basket} basket - current basket
 */
function addAddressToBasket(customer, preferredAddress, defaultPI, basket) {
  var billingAddress = basket.billingAddress;
  Transaction.wrap(function () {
    if (!billingAddress) {
      billingAddress = basket.createBillingAddress();
    }
    billingAddress.setFirstName(preferredAddress.firstName || '');
    billingAddress.setLastName(preferredAddress.lastName || '');
    billingAddress.setAddress1(preferredAddress.address1 || '');
    billingAddress.setAddress2(preferredAddress.address2 || '');
    billingAddress.setCity(preferredAddress.city || '');
    billingAddress.setPostalCode(preferredAddress.postalCode || '');
    billingAddress.setPhone(preferredAddress.phone || '');
    billingAddress.setCountryCode(preferredAddress.countryCode || '');
    billingAddress.setStateCode(preferredAddress.stateCode || '');
    basket.setCustomerEmail(customer.profile.email);
  });

  var shippingAddress = basket.defaultShipment.shippingAddress;
  Transaction.wrap(function () {
    if (!shippingAddress) {
      shippingAddress = basket.defaultShipment.createShippingAddress();
    }
    shippingAddress.setFirstName(preferredAddress.firstName || '');
    shippingAddress.setLastName(preferredAddress.lastName || '');
    shippingAddress.setAddress1(preferredAddress.address1 || '');
    shippingAddress.setAddress2(preferredAddress.address2 || '');
    shippingAddress.setCity(preferredAddress.city || '');
    shippingAddress.setPostalCode(preferredAddress.postalCode || '');
    shippingAddress.setPhone(preferredAddress.phone || '');
    shippingAddress.setCountryCode(preferredAddress.countryCode || '');
    shippingAddress.setStateCode(preferredAddress.stateCode || '');
  });

  Transaction.wrap(function () {
    var paymentInstruments = basket.getPaymentInstruments();

    collections.forEach(paymentInstruments, function (item) {
      basket.removePaymentInstrument(item);
    });

    var paymentInstrument = basket.createPaymentInstrument(PaymentInstrument.METHOD_CREDIT_CARD, basket.totalGrossPrice);

    paymentInstrument.setCreditCardHolder(defaultPI.getCreditCardHolder());
    paymentInstrument.setCreditCardType(defaultPI.getCreditCardType());
    paymentInstrument.setCreditCardNumber(defaultPI.getCreditCardToken());
    paymentInstrument.setCreditCardExpirationMonth(defaultPI.getCreditCardExpirationMonth());
    paymentInstrument.setCreditCardExpirationYear(defaultPI.getCreditCardExpirationYear());
    paymentInstrument.setCreditCardToken(defaultPI.getCreditCardToken());
  });
}

base.eligibleForExpressCheckout = function (customer, basket, req) {
  var Calendar = require('dw/util/Calendar');
  if (!customer.authenticated) {
    return false;
  }

  if (!customer.profile) {
    return false;
  }

  var preferredAddress = customer.addressBook ? customer.addressBook.preferredAddress : null;
  if (!preferredAddress) {
    return false;
  }
  var allowedCountries = preferences.allowedCountriesExpCheckout;
  var countryCode = preferredAddress.getCountryCode().value;
  if (countryCode.indexOf(allowedCountries) === -1) {
    return false;
  }
  var paymentInstruments = customer.profile && customer.profile.wallet ? customer.profile.wallet.paymentInstruments : null;
  if (!paymentInstruments || paymentInstruments.getLength() === 0) {
    return false;
  }
  var iterator = paymentInstruments.iterator();
  var defaultPI;
  while (iterator.hasNext()) {
    var item = iterator.next();
    if ('defaultCreditCard' in item.custom && item.custom.defaultCreditCard) {
      defaultPI = item;
    }
    var paymentInstruments = customer.profile && customer.profile.wallet ? customer.profile.wallet.paymentInstruments : null;
    if (!paymentInstruments || paymentInstruments.getLength() === 0) {
      return false;
    }
    var iterator = paymentInstruments.iterator();
    var defaultPI;
    while (iterator.hasNext()) {
      var item = iterator.next();
      if ('defaultCreditCard' in item.custom && item.custom.defaultCreditCard) {
        defaultPI = item;
      }
    }
    if (!defaultPI) {
      return false;
    }

    // check if the card was authorized with the CVV for the first time
    if (!(defaultPI && 'authorizedCard' in defaultPI.custom && defaultPI.custom.authorizedCard)) {
      return false;
    }

    var lastModifiedDate =
      'accountModifiedDate' in customer.profile.custom ? new Date(customer.profile.custom.accountModifiedDate) : customer.profile.getLastModified();
    var creationDateCal = new Calendar(defaultPI.lastModified);
    var lastModifiedDateCal = new Calendar(lastModifiedDate);
    var allowExpress = lastModifiedDateCal.compareTo(creationDateCal);
    if (allowExpress !== -1) {
      return false;
    }

    var expirationMonth = defaultPI.getCreditCardExpirationMonth();
    var expirationYear = defaultPI.getCreditCardExpirationYear();
    var currentDate = new Date();
    if (!empty(expirationYear) && !empty(expirationMonth)) {
      if (expirationYear < currentDate.getFullYear()) {
        return false;
      }
      if (expirationYear === currentDate.getFullYear() && expirationMonth <= currentDate.getMonth()) {
        return false;
      }
    }
  }
  addAddressToBasket(customer, preferredAddress, defaultPI, basket);
  var basketHashAfterCustAddressUpdate = basketHashHelper.generateBasketHash(basket);
  req.session.privacyCache.set('basketHashAfterCustAddressUpdate', basketHashAfterCustAddressUpdate);
  return true;
};

/**
 * Sets the payment transaction amount
 * @param {dw.order.Basket} currentBasket - The current basket
 * @param {number} cardBalance - Card Balance of the Gift Card
 * @returns {Object} an error object
 */
base.getNonGiftCardAmount = function (currentBasket, cardBalance) {
  var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
  var currencyCode = currentBasket.getCurrencyCode();
  var giftCardsTotal = new Money(0.0, currencyCode);
  var result = {};
  result.error = false;
  try {
    var paymentInstruments = currentBasket.getPaymentInstruments(ipaConstants.GIFT_CARD);
    // add all the gift card applied totals
    collections.forEach(paymentInstruments, function (paymentInstrument) {
      giftCardsTotal = giftCardsTotal.add(paymentInstrument.getPaymentTransaction().getAmount());
    });
    // Gets the order total.
    var orderTotal = currentBasket.totalGrossPrice;

    // Calculates the amount to charge for the payment instrument.
    // This is the remaining open order total that must be paid.
    var amountOpen = orderTotal.subtract(giftCardsTotal);

    // return GC balance if basket applicable amount is greater than balance, else return the basket applicable amount
    if (cardBalance) {
      if (amountOpen.value >= cardBalance) {
        result.giftCardAmountToApply = new Money(cardBalance, currencyCode);
        result.amountLeft = false;
      } else {
        result.giftCardAmountToApply = amountOpen;
        result.amountLeft = true;
      }
    } else {
      result.remainingAmount = amountOpen;
      result.amountLeft = false;
    }
  } catch (e) {
    result.error = true;
  }
  return result;
};

/**
 * recalculates and adjusts payment instruments amounts
 *
 * @param {dw.order.Basket} basket - current basket of customer
 * @returns {Object} result - result
 */
base.calculatePaymentTransaction = function (basket) {
  var paymentInstrs = basket.getPaymentInstruments();
  var currencyCode = basket.getCurrencyCode();
  var paymentInstTotals = new Money(0.0, currencyCode);
  var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
  var result = {
    error: false
  };
  collections.forEach(paymentInstrs, function (paymentInstrument) {
    paymentInstTotals = paymentInstTotals.add(paymentInstrument.getPaymentTransaction().getAmount());
  });

  try {
    var nonGCPaymentInstrument;
    collections.forEach(paymentInstrs, function (paymentInstrument) {
      if (paymentInstrument.getPaymentMethod() !== ipaConstants.GIFT_CARD) {
        nonGCPaymentInstrument = paymentInstrument;
      }
    });

    var gcPaymentInstrs = basket.getPaymentInstruments(ipaConstants.GIFT_CARD);
    var giftCardsTotal = new Money(0.0, currencyCode);
    var orderTotal = basket.totalGrossPrice;
    if (!gcPaymentInstrs.empty) {
      /**
       * Adjusts gift cards payment instruments after applying coupon(s), because this changes the order total.
       * Removes and then adds currently added gift cards to reflect order total changes.
       */
      var LinkedHashMap = require('dw/util/LinkedHashMap');
      var gcPIMap = new LinkedHashMap();
      var i = 0;

      collections.forEach(gcPaymentInstrs, function (gcPaymentInstrument) {
        gcPIMap.put('gcNum' + i, gcPaymentInstrument.custom.giftCardNumber);
        gcPIMap.put('gcPin' + i, gcPaymentInstrument.custom.giftCardPin);
        gcPIMap.put('gcBal' + i, gcPaymentInstrument.paymentTransaction.getAmount().value);
        gcPIMap.put('gcCheck' + i, gcPaymentInstrument.custom.giftBalanceLeft);
        i++;
      });
      // remove Gift Card Payment Instrument to make sure we have right GC with the right amount
      collections.forEach(gcPaymentInstrs, function (item) {
        basket.removePaymentInstrument(item);
      });

      // Map is our reference for the GC to be recreated. We need to create new GC Payment to avoid difference in the amount applied vs order total
      // the previous applied amount would be the threshold amount that can be applied and hence treated as the balance.
      // if the giftCardAmountToApply is zero then we skip the GC to be applied
      var gcPaymentToCheck;
      var total;
      for (i = 0; i < gcPaymentInstrs.length; i++) {
        total = base.getNonGiftCardAmount(basket, gcPIMap.get('gcBal' + i));
        if (!gcPIMap.get('gcCheck' + i) || total.amountLeft) {
          if (total.giftCardAmountToApply.value > 0) {
            Transaction.wrap(function () {
              let paymentInstrument = basket.createPaymentInstrument(ipaConstants.GIFT_CARD, total.giftCardAmountToApply);
              paymentInstrument.custom.giftCardNumber = gcPIMap.get('gcNum' + i);
              paymentInstrument.custom.giftCardPin = gcPIMap.get('gcPin' + i);
              giftCardsTotal = giftCardsTotal.add(paymentInstrument.getPaymentTransaction().getAmount());
              paymentInstrument.custom.giftBalanceLeft = total.amountLeft;
            });
          }
        } else {
          gcPaymentToCheck = {
            gcNum: gcPIMap.get('gcNum' + i),
            gcPin: gcPIMap.get('gcPin' + i)
          };
        }
      }
      if (gcPaymentToCheck) {
        var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
        let balanceCheckResult = hooksHelper(
          'app.payment.giftcard.balance.check',
          'gcBalanceCheck',
          [gcPaymentToCheck.gcNum, gcPaymentToCheck.gcPin],
          require('*/cartridge/scripts/util/ipaGCUtils').gcBalanceCheck
        );
        if (
          balanceCheckResult != null &&
          Number(balanceCheckResult.response_code) === 1 &&
          balanceCheckResult.card &&
          balanceCheckResult.card.funds_available &&
          Number(balanceCheckResult.card.funds_available) > 0
        ) {
          total = base.getNonGiftCardAmount(basket, Number(balanceCheckResult.card.funds_available));
          if (total.giftCardAmountToApply.value > 0) {
            Transaction.wrap(function () {
              let paymentInstrument = basket.createPaymentInstrument(ipaConstants.GIFT_CARD, total.giftCardAmountToApply);
              paymentInstrument.custom.giftCardNumber = gcPaymentToCheck.gcNum;
              paymentInstrument.custom.giftCardPin = gcPaymentToCheck.gcPin;
              paymentInstrument.custom.giftBalanceLeft = total.amountLeft;
              giftCardsTotal = giftCardsTotal.add(paymentInstrument.getPaymentTransaction().getAmount());
            });
          }
        }
      }
    }
    // NonGC PI is - CC, ShopRunner, Masterpass and PayPal
    // NonGC payment instrument will always be one PI among CC, SR, MP and PayPal
    // If the nonGC is not defined or initialed then the order should be fully paid via GC's or promotions
    if (nonGCPaymentInstrument) {
      Transaction.wrap(function () {
        var nonGCOrderTotal = orderTotal.subtract(giftCardsTotal);
        nonGCPaymentInstrument.paymentTransaction.setAmount(nonGCOrderTotal);
      });
    }
    var finalPIs = basket.getPaymentInstruments();
    var amountInPaymentInst = new Money(0.0, currencyCode);
    collections.forEach(finalPIs, function (paymentInstrument) {
      amountInPaymentInst = amountInPaymentInst.add(paymentInstrument.getPaymentTransaction().getAmount());
    });
    if (orderTotal.available && amountInPaymentInst.value !== orderTotal.value) {
      result.error = true;
      if (gcPaymentInstrs.length > 0) {
        result.maxGCLimitReached = gcPaymentInstrs.length == preferences.maxGCLimit;
      } else {
        result.maxGCLimitReached = false;
      }
      // the payment instrument total does not match with the order total, return error.
      // customer should choose additional payment or modify their current payment information
    }
  } catch (e) {
    result.error = true;
  }
  return result;
};

/**
 * Validates payment - overriding in order to return valid statement when giftcard payment method is also used
 *
 * @param {Object} req - The local instance of the request object
 * @param {dw.order.Basket} currentBasket - The current basket
 * @returns {Object} an object that has error information
 */
base.validatePayment = function (req, currentBasket) {
  var applicablePaymentCards;
  var applicablePaymentMethods;
  var creditCardPaymentMethod = PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_CREDIT_CARD);
  var paymentAmount = currentBasket.totalGrossPrice.value;
  var countryCode = req.geolocation.countryCode;
  var currentCustomer = req.currentCustomer.raw;
  var paymentInstruments = currentBasket.paymentInstruments;
  var result = {};
  var errorMessage = Resource.msg('error.payment.not.valid', 'checkout', null);

  applicablePaymentMethods = PaymentMgr.getApplicablePaymentMethods(currentCustomer, countryCode, paymentAmount);
  applicablePaymentCards = creditCardPaymentMethod.getApplicablePaymentCards(currentCustomer, countryCode, paymentAmount);

  var invalid = true;

  if (paymentInstruments.length === 0 && paymentAmount === 0) {
    invalid = false;
  }
  if (paymentInstruments.length > 0) {
    for (var i = 0; i < paymentInstruments.length; i++) {
      var paymentInstrument = paymentInstruments[i];

      if (PaymentInstrument.METHOD_GIFT_CERTIFICATE.equals(paymentInstrument.paymentMethod) || paymentInstrument.paymentMethod.equals('GiftCard')) {
        invalid = false;
      }

      var paymentMethod = PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod());

      if (paymentMethod && applicablePaymentMethods.contains(paymentMethod)) {
        if (PaymentInstrument.METHOD_CREDIT_CARD.equals(paymentInstrument.paymentMethod)) {
          var card = PaymentMgr.getPaymentCard(paymentInstrument.creditCardType);

          // Checks whether payment card is still applicable.
          if (card && applicablePaymentCards.contains(card)) {
            invalid = false;
          }

          if (paymentInstrument.isCreditCardExpired()) {
            invalid = true;
            errorMessage = Resource.msg('error.payment.expired', 'checkout', null);
          }

        } else {
          invalid = false;
        }
      }

      if (invalid) {
        break; // there is an invalid payment instrument
      }
    }
  }

  result.error = invalid;
  result.errorMessage = errorMessage;
  return result;
};

/**
 * Update the customer Payment instrument with auth response
 *
 * @param {Object} customer - Current customer
 * @param {order} order - The current order
 */
base.updateCustomerPI = function (customer, order) {
  var paymentInstruments = order.getPaymentInstruments();

  if (!customer) {
    return;
  }

  if (!customer.authenticated) {
    return;
  }

  var paymentInstrument;
  collections.forEach(paymentInstruments, function (pi) {
    if (pi.getPaymentMethod() === PaymentInstrument.METHOD_CREDIT_CARD) {
      paymentInstrument = pi;
    }
  });

  var customerPIs = customer.profile.wallet.paymentInstruments;
  if (paymentInstrument) {
    collections.forEach(customerPIs, function (custPI) {
      if (paymentInstrument.paymentMethod === custPI.paymentMethod && paymentInstrument.creditCardNumberLastDigits === custPI.creditCardNumberLastDigits) {
        Transaction.wrap(function () {
          // eslint-disable-next-line no-param-reassign
          // We are deliberately updating the values false and true. As it would update the lastModifiedDate at the PI level
          // If an account is udpated and the PI is already marked true, setting it true again would not udpate the lastModifiedDate
          // If the LastModifiedDate of the PI is not updated then the express checkout will not be enabled.
          custPI.custom.authorizedCard = false;
          custPI.custom.authorizedCard = true;
        });
      }
    });
  }
  return;
};

/**
 * renders the user's stored payment Instruments
 * @param {Object} req - The request object
 * @param {Object} accountModel - The account model for the current customer
 * @param {Object} orderModel - The order model for the current customer
 * @param {Object} country - The country code
 * @returns {string|null} newly stored payment Instrument
 */
base.getCustomerSavedAddresses = function (req, accountModel, orderModel, country) {
  var result;
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

  if (req.currentCustomer.raw.authenticated && req.currentCustomer.raw.registered) {
    var context;
    var template = 'checkout/billing/addressSelector';

    context = {
      customer: accountModel,
      order: orderModel,
      country2Code: country
    };
    result = renderTemplateHelper.getRenderedHtml(context, template);
  }

  return result || null;
};

base.getOrderCreditCardPaymentDetails = function (order) {
  if (!order.custom.tokenExRetry) {
    var paymentInstruments = order.getPaymentInstruments();
    var paymentInstrument;
    collections.forEach(paymentInstruments, function (pi) {
      if (pi.getPaymentMethod() === PaymentInstrument.METHOD_CREDIT_CARD) {
        paymentInstrument = pi;
      }
    });

    if (paymentInstrument) {
      var orderPaymentInstrument = {
        name: paymentInstrument.creditCardHolder,
        token: paymentInstrument.creditCardToken,
        expYear: paymentInstrument.creditCardExpirationYear,
        expMonth: paymentInstrument.creditCardExpirationMonth,
        type: paymentInstrument.creditCardType
      };
      return orderPaymentInstrument;
    }
    return null;
  }
  return null;
};

base.saveOrderPaymentToProfile = function (profile, payment) {
  var wallet = profile.getWallet();
  var paymentInstrument = wallet.createPaymentInstrument(PaymentInstrument.METHOD_CREDIT_CARD);
  paymentInstrument.setCreditCardHolder(payment.name);
  paymentInstrument.setCreditCardNumber(payment.token);
  paymentInstrument.setCreditCardType(payment.type);
  paymentInstrument.setCreditCardExpirationMonth(payment.expMonth);
  paymentInstrument.setCreditCardExpirationYear(payment.expYear);
  paymentInstrument.setCreditCardToken(payment.token);
  profile.custom.lastPaymentModified = new Date().toISOString();
  // This will be the only card saved in the profile, so marked it as default.
  paymentInstrument.custom.defaultCreditCard = true;
};

/**
 * Copies a raw address object to the baasket billing address
 * @param {Object} address - an address-similar Object (firstName, ...)
 * @param {Object} currentBasket - the current shopping basket
 */
base.copyBillingAddressToBasket = function (address, currentBasket) {
  var billingAddress = currentBasket.billingAddress;

  Transaction.wrap(function () {
    if (!billingAddress) {
      billingAddress = currentBasket.createBillingAddress();
    }

    billingAddress.setFirstName(address.firstName);
    billingAddress.setLastName(address.lastName);
    billingAddress.setAddress1(address.address1);
    billingAddress.setAddress2(address.address2);
    billingAddress.setCity(address.city);
    billingAddress.setPostalCode(address.postalCode);
    billingAddress.setCountryCode(address.countryCode.value);
    billingAddress.setStateCode(address.stateCode);
    if (!billingAddress.phone) {
      billingAddress.setPhone(address.phone);
    }
  });
};

/**
 * returns the giftcard type
 * @returns {string} giftcard -giftcard
 */
base.getGiftCardType = function (gcNumber) {
  var Site = require('dw/system/Site');
  var giftCardType = 'giftCardType' in Site.current.preferences.custom ? Site.current.preferences.custom.giftCardType : '';
  var firstfourNum = gcNumber.substr(0, 4);
  var cardType;
  try {
    var gcType = JSON.parse(giftCardType);
    cardType = gcType[firstfourNum];
    if (cardType == undefined) {
      cardType = gcType['default'];
    }
  } catch (e) {
    cardType = gcType['default'];
  }
  return cardType;
};

module.exports = base;
