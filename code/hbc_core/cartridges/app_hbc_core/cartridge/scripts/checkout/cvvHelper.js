'use strict';

var cardTypePLCC = ['HBC', 'SAKS', 'MPA'];
var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');

function isCVVRequired(paymentInstrument, profile) {
  var required = false;
  var token = paymentInstrument.creditCardToken;

  // rule #1 - hide cvv for TCC
  if (paymentInstrument.creditCardType === 'TCC') {
    return false;
  }

  // Rules #2 - PLCC cards
  // TSYS mode off - hide cvv for all PLCC cards
  // TSYS mode on - hide cvv for old PLCC cards (8 and 10 length) */
  if (cardTypePLCC.indexOf(paymentInstrument.creditCardType) !== -1) {
    if (!tsysHelper.isTsysMode()) {
      return false;
    } else {
      if (token && (token.length === 8 || token.length === 10)) {
        return false;
      }
    }
  }

  // rule #3 - show cvv if card is not authorized or if shipping address was changed
  if (!paymentInstrument.authorizedCard) {
    required = true;
  }

  return required;
}

module.exports.isCVVRequired = isCVVRequired;
