/* eslint-disable guard-for-in */
'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var preferences = require('*/cartridge/config/preferences');
var Money = require('dw/value/Money');
var Decimal = require('dw/util/Decimal');
var HashMap = require('dw/util/HashMap');
var Transaction = require('dw/system/Transaction');
var pliCount = new HashMap();
var shippingCharge = new HashMap();
var shippingTax = new HashMap();
var firstTaxableLineShipTax;
var proratedShippingTaxTotal = 0;
var fristLineItem;
var Logger = require('dw/system/Logger');
var taxLogging = Logger.getLogger('TaxLogging', 'TaxLogging');
function getTwoDecimalMoneyFormat(value, currencyCode) {
  var numVal = new Number(value);
  if (numVal < 0) {
    return new Money(new Number(0), currencyCode);
  } else {
    return new Money(numVal, currencyCode);
  }
}

function getProratedPriceCustom(lineItem, order) {
  var proratedPriceDiscount = new Money(0, order.getCurrencyCode());
  var discount;
  if (lineItem.custom.productPromotionDiscountWOPricebook != null) {
    discount = new Money(lineItem.custom.productPromotionDiscountWOPricebook, order.getCurrencyCode());
    proratedPriceDiscount = proratedPriceDiscount.add(discount);
  }
  if (lineItem.custom.productPromotionAssociateDiscount != null) {
    discount = new Money(lineItem.custom.productPromotionAssociateDiscount, order.getCurrencyCode());
    proratedPriceDiscount = proratedPriceDiscount.add(discount);
  }
  if (lineItem.custom.productPromotionSpecialVendorDiscount != null) {
    discount = new Money(lineItem.custom.productPromotionSpecialVendorDiscount, order.getCurrencyCode());
    proratedPriceDiscount = proratedPriceDiscount.add(discount);
  }
  if (lineItem.custom.productPromotionFirstDayDiscount != null) {
    discount = new Money(lineItem.custom.productPromotionFirstDayDiscount, order.getCurrencyCode());
    proratedPriceDiscount = proratedPriceDiscount.add(discount);
  }
  var proratedPrice = lineItem.getNetPrice().subtract(proratedPriceDiscount);
  return proratedPrice;
}

function appendLineLevelShipPrice(order, pli) {
  var surcharge = 0;
  if (pli.shippingLineItem && pli.shippingLineItem.adjustedPrice) {
    surcharge = pli.shippingLineItem.adjustedPrice;
  }
  var shipmentID = pli.getShipment().ID;

  if (shippingCharge.get(shipmentID) == null) {
    shippingCharge.put(shipmentID, 0);
  }
  var shippingPrice = 0;

  var giftOptionsPA = order.getPriceAdjustmentByPromotionID('GiftOptions');
  var finalTotal = new Money(0, order.getCurrencyCode());

  if (pli.getShipment().productLineItems.length == pliCount.get(shipmentID)) {
    shippingPrice = Money(pli.getShipment().adjustedShippingTotalPrice - shippingCharge.get(shipmentID), order.getCurrencyCode());
  } else {
    var shipmntPrice = new Number(pli.getShipment().getAdjustedMerchandizeTotalPrice(true));
    if (shipmntPrice !== 0) {
      var adjustedShippingPriceTotal = pli.getShipment().adjustedShippingTotalPrice;
      var customProratedPrice = getProratedPriceCustom(pli, order);
      var adjustedMerchandizeTotalPrice = pli.getShipment().getAdjustedMerchandizeTotalPrice(true);
      if (adjustedShippingPriceTotal.value > 0 && customProratedPrice.value > 0 && adjustedMerchandizeTotalPrice.value > 0) {
        shippingPrice = new Decimal(adjustedShippingPriceTotal)
          .abs()
          .multiply(new Number(customProratedPrice))
          .divide(new Number(adjustedMerchandizeTotalPrice));
      }
    }
    shippingPrice = Money(shippingPrice, order.getCurrencyCode());
    shippingCharge.put(shipmentID, shippingCharge.get(shipmentID) + shippingPrice);
  }

  Transaction.wrap(function () {
    if (pli.adjustedPrice.value !== 0) {
      var total = new Money(shippingPrice - surcharge, order.getCurrencyCode());
      var prodprice = new Money(getProratedPriceCustom(pli, order) / pli.quantity.value, order.getCurrencyCode());
      prodprice = new Money(prodprice.value * pli.quantity.value, order.getCurrencyCode()); // this step is to avoid roundiing off issues. OMS does this based on the list price which is a unit price
      finalTotal = finalTotal.add(prodprice);
      finalTotal = finalTotal.add(pli.getAdjustedTax());
      finalTotal = finalTotal.add(total);
      pli.custom.proratedPriceTotal = finalTotal.value;
    } else {
      pli.custom.proratedPriceTotal = 0;
    }
  });
}

function appendLineLevelShipTax(order, pli) {
  var shippingPriceTax = 0;
  var shipmentID = pli.getShipment().ID;
  if (shippingTax.get(shipmentID) == null) {
    shippingTax.put(shipmentID, 0);
  }

  if (pli.getShipment().productLineItems.length == pliCount.get(shipmentID)) {
    if (('isNonTaxableProduct' in pli.custom && pli.custom.isNonTaxableProduct) || pli.adjustedPrice.value == 0) {
      shippingPriceTax = 0;
    } else {
      shippingPriceTax = Money(pli.getShipment().adjustedShippingTotalTax - shippingTax.get(shipmentID), order.getCurrencyCode());
    }

    // If last item was non taxable and we need to adjust the penny difference
    if (shippingPriceTax === 0 && firstTaxableLineShipTax) {
      var pennyDifferenceTax = Money(pli.getShipment().adjustedShippingTotalTax - shippingTax.get(shipmentID), order.getCurrencyCode());
      var updatedFirstTaxableItemShipTax = firstTaxableLineShipTax.add(pennyDifferenceTax);
      Transaction.wrap(function () {
        fristLineItem.custom.proratedPriceTotal += updatedFirstTaxableItemShipTax;
      });
    }
  } else {
    if (('isNonTaxableProduct' in pli.custom && pli.custom.isNonTaxableProduct) || pli.adjustedPrice.value == 0) {
      shippingPriceTax = 0;
    } else {
      var shipmntPrice = new Number(pli.getShipment().getAdjustedMerchandizeTotalPrice(true));
      if (shipmntPrice !== 0) {
        var adjustedShippingTotalTax = pli.getShipment().adjustedShippingTotalTax;
        var proratedPriceCustom = getProratedPriceCustom(pli, order);
        var adjustedMerchandizeTotalPrice = pli.getShipment().getAdjustedMerchandizeTotalPrice(true);
        if (adjustedShippingTotalTax.value > 0 && proratedPriceCustom.value > 0 && adjustedMerchandizeTotalPrice.value > 0) {
          shippingPriceTax = new Decimal(adjustedShippingTotalTax)
            .abs()
            .multiply(new Number(proratedPriceCustom))
            .divide(new Number(adjustedMerchandizeTotalPrice));
        }
      }
      // Store the First Taxable item shipping tax which we can use for penny difference adjustment.
      if (!firstTaxableLineShipTax) {
        firstTaxableLineShipTax = Money(shippingPriceTax, order.getCurrencyCode());
        fristLineItem = pli;
      }
    }

    shippingPriceTax = Money(shippingPriceTax, order.getCurrencyCode());
    shippingTax.put(shipmentID, shippingTax.get(shipmentID) + shippingPriceTax);
  }

  if (proratedShippingTaxTotal >= pli.getShipment().adjustedShippingTotalTax) {
    shippingPriceTax = Money(0, order.getCurrencyCode());
  }
  var shippingTaxNumber = shippingPriceTax.class && shippingPriceTax instanceof dw.value.Money ? shippingPriceTax.value : shippingPriceTax;
  var shippingTaxMoney = new Money(shippingTaxNumber, order.getCurrencyCode());
  proratedShippingTaxTotal = proratedShippingTaxTotal + shippingTaxMoney.value;

  Transaction.wrap(function () {
    if (pli.adjustedPrice.value !== 0) {
      var proratedPriceTotal = new Money(Number(pli.custom.proratedPriceTotal), order.getCurrencyCode());
      proratedPriceTotal = proratedPriceTotal.add(shippingTaxMoney);
      pli.custom.proratedPriceTotal = proratedPriceTotal.value;
    } else {
      pli.custom.proratedPriceTotal = 0;
    }
  });
}

// get difference between actual and prorated values
function getDelta(proratedValue, baseValue, order) {
  var delta = new Money(0, order.getCurrencyCode());
  if (proratedValue > baseValue) {
    // Over charged
    delta = new Money(-0.01, order.getCurrencyCode());
  } else {
    // Under charged
    delta = new Money(0.01, order.getCurrencyCode());
  }
  return delta;
}

function calculateOrderGiftChargesAndTaxes(order, lineitems) {
  // Calculate totalAdjustedMerchandisePrice and build list of giftwrap eligible items
  var giftItems = new Array();
  var totalAdjustedMerchandisePrice = new Money(0, order.getCurrencyCode());

  try {
    var giftOptionsPA = order.getPriceAdjustmentByPromotionID('GiftOptions');

    Transaction.wrap(function () {
      collections.forEach(lineitems, function (pli) {
        var lineitemPrice = pli.adjustedPrice;

        if (
          giftOptionsPA &&
          pli.shipment.custom.hasOwnProperty('giftWrapType') &&
          pli.shipment.custom.giftWrapType === 'giftpack' &&
          pli.product.custom.hasOwnProperty('giftWrapEligible') && 
          pli.product.custom.giftWrapEligible === 'true' && 
          lineitemPrice.value > 0) 
        {
          totalAdjustedMerchandisePrice = totalAdjustedMerchandisePrice.add(lineitemPrice);
          giftItems.push(pli);
        }

        // Clear out any previously calculated amounts for all items
        pli.custom.proratedGiftCharge = 0;
        pli.custom.proratedGiftTax = 0;
      });
    });

    if (empty(giftOptionsPA) || giftItems.length == 0){
      return giftItems;
    }

    var proratedGiftCharges = new Money(0, order.getCurrencyCode());
    var chargeAmount = giftOptionsPA.getPriceValue();  // double check this returns $5, then delete this comment

    var proratedGiftTaxes = new Money(0, order.getCurrencyCode());
    var taxAmount = giftOptionsPA.getTax();
    
    // Iterate over all gift wrap eligible items, and set the intial prorated charge and tax
    Transaction.wrap(function () {
      giftItems.forEach(function (giftPli) {         
        // Do the stright math for gift wrap charge
        var proratedPrice = new Money(
          (giftPli.adjustedNetPrice.value / totalAdjustedMerchandisePrice.value) * chargeAmount, order.getCurrencyCode()
        );
        proratedGiftCharges = proratedGiftCharges.add(proratedPrice);
        giftPli.custom.proratedGiftCharge = proratedPrice.value;
        
        // Do the stright math for gift wrap tax 
        var proratedTax = getTwoDecimalMoneyFormat(
          (giftPli.adjustedNetPrice.value / totalAdjustedMerchandisePrice.value) * taxAmount.value, order.getCurrencyCode()
        );
        proratedGiftTaxes = proratedGiftTaxes.add(proratedTax);
        giftPli.custom.proratedGiftTax = proratedTax.value;
        });
    });

    // Round robin any remaining pennies for gift charges
    if (proratedGiftCharges.value != chargeAmount)
    {
      var delta = getDelta(proratedGiftCharges.value, chargeAmount, order);
    
      Transaction.wrap(function () {
        while (proratedGiftCharges.value != chargeAmount) {
          
          giftItems.forEach(function (giftPli) {
            // if charges are equal, break out of the loop
            if (proratedGiftCharges.value == chargeAmount) {
              return false;
            } 
            var proratedGiftCharge = new Money(giftPli.custom.proratedGiftCharge, order.getCurrencyCode());
            proratedGiftCharge = proratedGiftCharge.add(delta);
            giftPli.custom.proratedGiftCharge = proratedGiftCharge.value;
            proratedGiftCharges = proratedGiftCharges.add(delta);
          });
        }
      });
    }
    // Round robin any remaining pennies for gift taxes
    if (proratedGiftTaxes.value != taxAmount.value)
    {
      var delta = getDelta(proratedGiftTaxes.value, taxAmount.value, order);
     
      Transaction.wrap(function () {
        while (proratedGiftTaxes.value != taxAmount.value) {
 
          giftItems.forEach(function (giftPli) {
            // if charges are equal, break out of the loop
            if (proratedGiftTaxes.value == taxAmount.value) {
              return false;
            } 
            var proratedGiftTax = new Money(giftPli.custom.proratedGiftTax, order.getCurrencyCode());
            proratedGiftTax = proratedGiftTax.add(delta);
            giftPli.custom.proratedGiftTax = proratedGiftTax.value;
            proratedGiftTaxes = proratedGiftTaxes.add(delta);
          });
        }
      });
    }
  } catch (e) {
    taxLogging.error('Error in prorotationHelper.js function calculateOrderGiftChargesAndTaxes: ' + e);
  }
  return giftItems;
}


function appendLineLevelGiftChargesAndTax(order, giftItems) {
  try {
  // Iterate over all gift wrap eligible items, and add the gift charge and gift tax to the custom prorated amount
    Transaction.wrap(function () {
      giftItems.forEach(function (giftPli) {  
        var proratedTotal = new Money(empty(giftPli.custom.proratedPriceTotal) ? 0 : giftPli.custom.proratedPriceTotal, order.getCurrencyCode());
        proratedTotal = proratedTotal.add(Money(giftPli.custom.proratedGiftCharge, order.getCurrencyCode()));
        proratedTotal = proratedTotal.add(Money(giftPli.custom.proratedGiftTax, order.getCurrencyCode()));
        giftPli.custom.proratedPriceTotal = proratedTotal.value;
      });
    });
  } catch (e) { 
    taxLogging.error('ERROR  in prorotationHelper.js function appendLineLevelGiftChargesAndTax: ' + e);
  }
}

function doLineItemCalculations(order) {
  var lineitems = order.getProductLineItems();

  var giftItems = calculateOrderGiftChargesAndTaxes(order, lineitems);

  collections.forEach(lineitems, function (pli) {
    var shipment = pli.shipment;
    var shipmentID = pli.getShipment().ID;

    if (pliCount.get(shipmentID) == null) {
      pliCount.put(shipmentID, 1);
    } else {
      pliCount.put(shipmentID, pliCount.get(shipmentID) + 1);
    }
    appendLineLevelShipPrice(order, pli);
    appendLineLevelShipTax(order, pli);
  });

  if (giftItems && giftItems.length > 0) {
    appendLineLevelGiftChargesAndTax(order, giftItems);
  }
}

module.exports = {
  doLineItemCalculations: doLineItemCalculations
};