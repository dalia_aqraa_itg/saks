'use strict';

/* API Includes */
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

var capOneConstants = require('*/cartridge/scripts/util/capOneConstants');
var ServiceHelper = require('*/cartridge/scripts/services/helpers/ServiceHelper');
var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');

var Logger = require('dw/system/Logger');

var CapOnePrefillUtil = function () {
  /**
   * Helper method for sending prefill requests
   *
   * @param {Object} data The JSON data to send in the body of the request
   *
   */
  this.getPrefillCacheKey = function (data) {
    var capOneService = LocalServiceRegistry.createService(capOneConstants.prefillService, {
      createRequest: function (service, params) {
        service.setRequestMethod('POST');
        service.addHeader('Accept', 'application/json');
        service = ServiceHelper.addServiceHeaders(service);

        service.addHeader('x-apigw-api-id', capOneConstants.APIGW);
        service.addHeader('x-api-key', capOneConstants.API_KEY);
        return params;
      },
      parseResponse: function (service, httpClient) {
        return JSON.parse(httpClient.text);
      },
      mockCall: function (service, httpClient) {
        return {
          statusCode: 200,
          statusMessage: 'Success',
          text:
            '{"success": true,"cacheKey": "MOCK-CACHE-KEY:1df23ww34:0001","akamaiToken": "exp=1602687643~hmac=e85c1ef77956043d9b9f9049993a9162a4f71d800d28d0617117bd2ecdb3e6cb"}'
        };
      }
    });

    // Will be mocked for now
    // capOneService.setMock();

    var result = capOneService.call(JSON.stringify(data));
    if (result.status != 'OK' || result.error || !result.object) {
      Logger.error('Capital One Prefill Failed: ', result.errorMessage);
      return result;
    }

    return result;
  };
};

module.exports = CapOnePrefillUtil;
