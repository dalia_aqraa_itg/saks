'use strict';

/* API Includes */
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var capOneConstants = require('*/cartridge/scripts/util/capOneConstants');
var Logger = require('dw/system/Logger');

var CapOnePartnerIdUtil = function () {
  /**
   * Helper method for sending partner id look up requests
   * @param {Object} data The JSON data to send in the body of the request
   */
  this.getCardToken = function (data) {
    var pidService = LocalServiceRegistry.createService(capOneConstants.partnerIdLookUpService, {
      createRequest: function (service, params) {
        service.setRequestMethod('GET');
        service.setURL(service.URL.toString() + '?' + params);
        service.addHeader('Content-Type', 'application/json');
        service.addHeader('Accept', 'application/json');
        service.addHeader('x-api-key', capOneConstants.API_KEY);
        service.addHeader('x-apigw-api-id', capOneConstants.APIGW);

        return params;
      },
      parseResponse: function (service, httpClient) {
        return JSON.parse(httpClient.text);
      },
      mockCall: function (service, httpClient) {
        return {
          statusCode: 200,
          statusMessage: 'Success',
          text: '{"success": true,"partnerAppRequestId": "1918839278839292773","tokenizedCard": "6003046819908780","expiryDate": "11/23"}'
        };
      }
    });

    // Will be mocked for now
    // pidService.setMock();

    var result = pidService.call(data);
    if (result.status != 'OK' || result.error || !result.object) {
      Logger.error('Partner ID lookup Failed: ', result.errorMessage);
      return result;
    }

    return result;
  };
};

module.exports = CapOnePartnerIdUtil;
