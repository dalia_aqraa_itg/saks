// lazyloading for non-Safari
function hydrateLazyLoadedImages() {
  if ('loading' in HTMLImageElement.prototype) {
    const images = document.querySelectorAll('.lazyload');
    Array.prototype.forEach.call(images, img => {
      img.src = img.dataset.src;
      img.srcset = img.dataset.srcset;
    });
  }
}

module.exports = { hydrateLazyLoadedImages: hydrateLazyLoadedImages };
