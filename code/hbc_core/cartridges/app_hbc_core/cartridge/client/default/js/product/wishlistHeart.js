'use strict';

/**
 * appends params to a url
 * @param {string} data - data returned from the server's ajax call
 * @param {Object} icon - icon that was clicked to add a product to the wishlist
 * @param {Object} imageContainer <div> - image container div to append the data once the product is wishlisted
 */

function displayMessageAndChangeIcon(data, icon, imageContainer) {
  $.spinner().stop();
  var customerStatus = $('.customer-details').data('customer') && $('.customer-details').data('customer').firstName;
  if (data.success) {
    if (!customerStatus) {
      if (imageContainer.find('.customer-content').hasClass('d-none')) {
        if (typeof Storage !== 'undefined') {
          imageContainer.find('.wishlistTile').find('span').hide();
          if (!sessionStorage.wishListClicked) {
            sessionStorage.wishListClicked = 1;
            imageContainer.addClass('wishlist_checked');
            imageContainer.find('.customer-content').removeClass('d-none');
          } else {
            imageContainer.find('.wishlistTile').find('span').show();
          }
        }
      }
    }
  }
}

function addToWishlist() {
  $('body').on('click', '.wishlistTile.select-wishlist', function (e) {
    e.preventDefault();
    e.stopPropagation();
    var icon = $(this).find($('i'));
    var url = $(this).attr('href');
    var pid = $(this).closest('.product').data('pid');
    var optionId = $(this).closest('.product-detail').find('.product-option').attr('data-option-id');
    var optionVal = $(this).closest('.product-detail').find('.options-select option:selected').attr('data-value-id');
    var imageContainer = $(this).closest('.product-tile').find('.image-container');
    var customerStatusLoad = $('.customer-details').data('customer') && $('.customer-details').data('customer').firstName;
    if (!customerStatusLoad) {
      $('.wishlistTile span').removeAttr('style');
      $('.bagde-img').removeAttr('style');
      $('.tile-image').removeClass('guest-user');
    }
    optionId = optionId || null;
    optionVal = optionVal || null;
    if (!url || !pid) {
      return;
    }

    $.spinner().start();
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: {
        pid: pid,
        optionId: optionId,
        optionVal: optionVal
      },
      success: function (data) {
        if (!data.product) {
          $.spinner().stop();
          return;
        }
        $('body').trigger('adobeTagManager:addToFav', data.product);
        $('.customer-content').addClass('d-none');
        var productObj = data.product;
        var productTile = $('.product-tile').closest('div[data-pid="' + productObj.id + '"]').length
          ? $('.product-tile').closest('div[data-pid="' + productObj.id + '"]')
          : $('.product-tile').closest('div[data-pid="' + productObj.masterProductID + '"]');
        var $addToCartBtn = productTile.find('.ajax-update-atc-button');
        $addToCartBtn.addClass('wishlist-add-to-bag');
        if ((productObj.productObjType == 'standard' || productObj.hasOnlyColor || productObj.hasOneSize || productObj.hasOnlyOneVariant || (productObj.readyToOrder && productObj.available))) {
          $addToCartBtn.addClass('add-to-cart');
        } else {
          $addToCartBtn.addClass('item-edit');
        }
        imageContainer = productTile.find('.image-container');
        displayMessageAndChangeIcon(data, icon, imageContainer);
        if ($('.pg-name').length > 0 && $('.pg-name').val() === 'wishlist') {
          window.location.reload();
        }
      },
      error: function (err) {
        displayMessageAndChangeIcon(err, icon, imageContainer);
      },
      complete: function (data) {
        var customerStatus = $('.customer-details').data('customer') && $('.customer-details').data('customer').firstName;
        if (!customerStatus) {
          if (data.status === 200 && data.responseText) {
            var productObj = JSON.parse(data.responseText).product;
            if (!productObj) {
              $.spinner().stop();
              return;
            }
            var productTile = $('.product-tile').closest('div[data-pid="' + productObj.id + '"]').length
              ? $('.product-tile').closest('div[data-pid="' + productObj.id + '"]')
              : $('.product-tile').closest('div[data-pid="' + productObj.masterProductID + '"]');
            var $this = productTile.find('.wishlistTile');
            // $this.find('span').hide();
            //$this.parentsUntil('.product-tile').find('.bagde-img').hide();
            $this.closest('.image-container').find('.tile-image').addClass('guest-user');
            $this.find('span').addClass('svg-svg-96-heart-blue svg-svg-96-heart-blue-dims');
            $this.removeClass('select-wishlist').addClass('deselect-wishlist');
            $this.find('.customer-content').addClass('d-none');
          }
        } else if (data.status === 200 && data.responseText) {
          var productObj = JSON.parse(data.responseText).product;
          if (!productObj) {
            return;
          }
          var productTile = $('.product-tile').closest('div[data-pid="' + productObj.id + '"]').length
            ? $('.product-tile').closest('div[data-pid="' + productObj.id + '"]')
            : $('.product-tile').closest('div[data-pid="' + productObj.masterProductID + '"]');
          var $this = productTile.find('.wishlistTile');
          $this.find('span').addClass('svg-svg-96-heart-blue svg-svg-96-heart-blue-dims');
          $this.removeClass('select-wishlist').addClass('deselect-wishlist');
        }
      }
    });
  });
}

(function () {
  var elWishlist = $('.wishlist-page').length;

  // By default changing the heart icon Selected in wishlist page.
  if (elWishlist > 0) {
    $('.wishlistItemCards .product-tile').find('.image-container').addClass('wishlist_checked');
    $('.wishlistItemCards .wishlistTile').find('span').addClass('svg-svg-96-heart-blue svg-svg-96-heart-blue-dims');
    $('.wishlistItemCards .wishlistTile').removeClass('select-wishlist').addClass('deselect-wishlist');
  }

  $('body').on('click', '.account-merge .close', function () {
    $('.account-merge').addClass('d-none');
  });

  $(document).ready(function () {
    $('.TTteaser__rating').each(function () {
      var currentdata = $(this).parents('.product').find('.product-tile .hover-bg-content .tile-body-footer');
      $(this).appendTo(currentdata);
    });
  });
})();

module.exports = {
  addToWishlist: addToWishlist,
  removeFromWishlist: function () {
    $('body').on('click', '.plpremove-whishlist, .deselect-wishlist', function (e) {
      e.preventDefault();
      e.stopPropagation();
      var deselectNode = $(this);
      var pid = deselectNode.closest('.product').data('pid');
      var url = deselectNode.closest('.product-tile').find('.removeFromWlTileUrl').val();

      $(this).removeClass('deselect-wishlist').addClass('select-wishlist');
      $.spinner().start();
      $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
          pid: pid
        },
        success: function (data) {
          var productObj = data.product;
          if (!productObj) {
            return;
          }
          var productTile = $('.product-tile').closest('div[data-id="' + productObj.id + '"]').length
            ? $('.product-tile').closest('div[data-id="' + productObj.id + '"]')
            : $('.product-tile').closest('div[data-id="' + productObj.masterProductID + '"]');
          var imageContainer = productTile.find('.image-container');
          var currentReference = productTile.find('.plpremove-whishlist');
          var customerStatus = $('.customer-details').data('customer') && $('.customer-details').data('customer').firstName;
          if (!customerStatus) {
            imageContainer.find('.wishlistTile span').removeAttr('style');
            imageContainer.find('.bagde-img').removeAttr('style');
            imageContainer.find('.customer-content').addClass('d-none');
            imageContainer.find('.tile-image').removeClass('guest-user');
            imageContainer.find('.wishlistTile').removeClass('deselect-wishlist').addClass('select-wishlist');
          }
          imageContainer.find('.wishlistTile span').removeClass('svg-svg-96-heart-blue svg-svg-96-heart-blue-dims');
          currentReference.find('span').removeClass('svg-svg-96-heart-blue svg-svg-96-heart-blue-dims');
          // replacing the content on wishlist page
          if ($('.pg-name').length > 0 && $('.pg-name').val() === 'wishlist') {
            deselectNode.closest('.wishlist-prod-tile').remove();
            $('.wl-count').text((_, count) => Number(count) - 1);
          }
          $.spinner().stop();
          $('body').trigger('adobeTagManager:removeFromFav', data.product);
          // Showing the wishlist empty message and hiding other wishlist content If wishlist is empty
          if (data.listIsEmpty === true) {
            $('.wishlist-wrapper .wishlist-owner.text-center.wishlist-empty-heading').removeClass('d-none');
            $('.wishlist-wrapper .wishlist-header-wrap').addClass('d-none');
            $('.wishlist-wrapper .guest-signin-info.guest-signin-info-lp').addClass('d-none');
          }
        },
        error: function () {
          $.spinner().stop();
        }
      });
    });
  }
};
