/* eslint-disable no-param-reassign */
if (!('loading' in HTMLImageElement.prototype)) {
  const script = document.createElement('script');
  script.src = 'https://cdnjs.cloudflare.com/ajax/libs/lazysizes/5.1.2/lazysizes.min.js';
  script.async = true;
  document.body.appendChild(script);
} else {
  const images = document.querySelectorAll('.lazyload');
  Array.prototype.forEach.call(images, img => {
    img.src = img.dataset.src;
    img.srcset = img.dataset.srcset;
  });
}
