'use strict';

var hbcTooltip = require('../tooltip');
var teaser = require('./../teaserOrderDetails/teasersOD');
var orderCancel = require('./orderCancel');

module.exports = {
  orderStatus: function () {
    $('form.trackorder').submit(function (e) {
      e.preventDefault();
      var form = $(this);
      $('.track-order-error').addClass('d-none').empty();
      // eslint-disable-next-line no-undef
      grecaptcha.ready(function () {
        // eslint-disable-next-line no-undef
        grecaptcha.execute($('.google-recaptcha-key').html(), { action: 'orderdetails' }).then(function (token) {
          $('.g-recaptcha-token').val(token);

          var url = form.attr('action');
          form.spinner().start();
          // insert space after 3 characters for canadian postal code
          var postalCode = $('#trackorder-form-zip').val();
          if (postalCode.length === 6 && postalCode.indexOf(' ') === -1 && /[a-zA-Z][0-9][a-zA-Z]/.test(postalCode.substring(0, 3))) {
            var formattedString = postalCode.substring(0, 3) + ' ' + postalCode.substring(3, 6);
            $('#trackorder-form-zip').val(formattedString);
          }
          $.ajax({
            url: url,
            method: 'GET',
            data: form.serialize(),
            success: function (data) {
              if (data.error) {
                // $('.track-order-error').removeClass('d-none');
                $('.track-order-error').removeClass('d-none').html(data.message);
                if ($('.track-order-error').attr('data-borderfreemsg')) {
                  var bfMessage = $('.track-order-error').attr('data-borderfreemsg');
                  $('.track-order-error').append(' ').append(bfMessage);
                }
                //errrorMsg
              } else {
                $('.order-status-page').empty().html(data);
                teaser.createReview();
                hbcTooltip.tooltipInit();
              }
              if (data.botError) {
                $('form.trackorder button.btn-primary').attr('disabled', 'disabled');
                $('.track-order-error').removeClass('d-none').html(data.error);
              }

              $.spinner().stop();
            },
            error: function (data) {
              //$('.track-order-error').removeClass('d-none');
              $('.track-order-error').removeClass('d-none').html(data.error);
              if (data.botError) {
                $('form.trackorder button.btn-primary').attr('disabled', 'disabled');
                $('.track-order-error').removeClass('d-none').html(data.error);
              }

              $.spinner().stop();
            }
          });
        });
      });
    });
  },
  orderCancelHandler: function () {
    orderCancel();
  }
};
