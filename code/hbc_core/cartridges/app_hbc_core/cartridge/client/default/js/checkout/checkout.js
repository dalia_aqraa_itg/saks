'use strict';

var addressHelpers = require('base/checkout/address');
var shippingHelpers = require('./shipping');
var billingHelpers = require('./billing');
var storePickPersonHelpers = require('./instore');
var summaryHelpers = require('./summary');
var formHelpers = require('base/checkout/formErrors');
var scrollAnimate = require('base/components/scrollAnimate');
var floatLabel = require('../floatLabel');
var clientSideValidation = require('../components/clientSideValidation');
var giftCard = require('./giftcard');
var hbcTooltip = require('../tooltip');
var cookiesUtil = require('../components/utilhelper');

/**
 * Create the jQuery Checkout Plugin.
 *
 * This jQuery plugin will be registered on the dom element in checkout.isml with the
 * id of "checkout-main".
 *
 * The checkout plugin will handle the different state the user interface is in as the user
 * progresses through the varying forms such as shipping and payment.
 *
 * Billing info and payment info are used a bit synonymously in this code.
 *
 */
(function ($) {
  $.fn.checkout = function () {
    // eslint-disable-line
    var plugin = this;

    //
    // Collect form data from user input
    //
    var formData = {
      // Shipping Address
      shipping: {},

      // Billing Address
      billing: {},

      // Payment
      payment: {},

      // Gift Codes
      giftCode: {}
    };

    //
    // The different states/stages of checkout
    //
    var checkoutStages = ['shipping', 'payment', 'placeOrder', 'submitted'];
    // add new stage if basket has instore items
    if ($(plugin).find('.instore-section').length > 0 && !$(plugin).find('.instore-section').hasClass('d-none')) {
      checkoutStages.unshift('pickupperson');
    }
    /**
     * Updates the URL to determine stage
     * @param {number} currentStage - The current stage the user is currently on in the checkout
     */
    function updateUrl(currentStage) {
      history.pushState(
        checkoutStages[currentStage],
        document.title,
        location.pathname + '?stage=' + checkoutStages[currentStage] + '#' + checkoutStages[currentStage]
      );
      var title = '';
      if (checkoutStages[currentStage] && checkoutStages[currentStage] === 'shipping') {
        title = 'Shipping | Checkout';
      } else if (checkoutStages[currentStage] && checkoutStages[currentStage] === 'payment') {
        title = 'Billing | Checkout';
      } else if (checkoutStages[currentStage] && checkoutStages[currentStage] === 'placeOrder') {
        title = 'Order Review | Checkout';
      }
      if (title !== '') {
        $('title').text(title);
      }
    }

    //
    // Local member methods of the Checkout plugin
    //
    var members = {
      // initialize the currentStage variable for the first time
      currentStage: 0,

      /**
       * Set or update the checkout stage (AKA the shipping, billing, payment, etc... steps)
       * @returns {Object} a promise
       */
      updateStage: function () {
        var stage = checkoutStages[members.currentStage];
        var defer = $.Deferred(); // eslint-disable-line

        if ($('.invalid-feedback').not('.hudson-error, .hudson-error-max').is(':visible')) {
          scrollAnimate($('.invalid-feedback:visible'));
          return defer.reject();
        }

        if (stage === 'shipping') {
          //
          // Clear Previous Errors
          //
          formHelpers.clearPreviousErrors('.shipping-form');

          //
          // Submit the Shipping Address Form
          //
          var isMultiShip = $('#checkout-main').hasClass('multi-ship');
          var formSelector = isMultiShip ? '.multi-shipping .active form' : '.single-shipping .shipping-form';
          var form = $(formSelector);

          if ($('.hbc-alert-error').is(':visible')) {
            scrollAnimate($('.hbc-alert-error:visible'));
            return defer.reject();
          }
          $('.expired-promo').addClass('d-none');

          // Reset the TCC Form
          if ($('.regular-card-number').hasClass('d-none')) {
            $('body').trigger('checkout:resetTCCForm');
          }
          if (isMultiShip && form.length === 0) {
            // disable the next:Payment button here
            // $('body').trigger('checkout:disableButton', '.next-step-button button');
            // in case the multi ship form is already submitted
            var url = $('#checkout-main').attr('data-checkout-get-url');
            $.ajax({
              url: url,
              method: 'GET',
              success: function (data) {
                // enable the next:Payment button here
                $('body').trigger('checkout:enableButton', '.next-step-button button');
                if (!data.error) {
                  $('body').trigger('checkout:updateCheckoutView', {
                    order: data.order,
                    customer: data.customer
                  });
                  // SFDEV-4849 | Enable GC and CC fields in case order total changes from previous one.
                  if (data.order && data.order.giftCard && data.order.giftCard.amountReached) {
                    $('.credit-card-form input').attr('disabled', 'disabled');
                    $('.gift-card-form-group input').attr('disabled', 'disabled');
                    $('.gift-card-form-group .giftcard-apply-submit button').attr('disabled', 'disabled');
                    $('.cancel-new-payment').attr('disabled', 'disabled');
                    if ($('.saved-payment-instrument-section').length) {
                      $('.saved-payment-security-code').attr('disabled', 'disabled');
                      $('.credit-card-selection-new').addClass('disabled');
                    }
                  } else if (data.order && data.order.giftCard && !data.order.giftCard.amountReached) {
                    $('.credit-card-form input').removeAttr('disabled');
                    if ($('.saved-payment-instrument-section').length) {
                      $('.saved-payment-security-code').removeAttr('disabled');
                      $('.credit-card-selection-new').removeClass('disabled');
                    }
                    $('.cancel-new-payment').removeAttr('disabled');
                    if (!data.order.giftCard.maxLimitReached) {
                      $('.gift-card-form-group input').removeAttr('disabled', 'disabled');
                      $('.gift-card-form input, .gift-card-form button').removeAttr('disabled');
                    } else {
                      $('.gift-card-form-group input').removeAttr('disabled', 'disabled');
                      $('.gift-card-form input, .gift-card-form button').attr('disabled', 'disabled');
                    }
                  }

                  if (data.order && data.order.giftCard && data.order.giftCard.giftCardHtml) {
                    $('.gift-card-applied').empty().html(data.order.giftCard.giftCardHtml);
                  }
                  if (data.order && data.order.orderSummaryPaymentHtml) {
                    $('.order-summary-payment-applied').empty().html(data.order.orderSummaryPaymentHtml);
                  }
                  if (data.order && data.order.giftCard && data.order.giftCard.hasGiftCard) {
                    if (!$('li.nav-item.paypal').hasClass('paypal-disable')) {
                      $('li.nav-item.paypal').addClass('paypal-disable');
                      $('#payPalRadioButton').attr('disabled', true);
                    }
                  }
                  defer.resolve();
                } else if ($('.shipping-error .alert-danger').length < 1) {
                  var errorMsg = data.message;
                  var errorHtml =
                    '<div class="alert alert-danger alert-dismissible valid-cart-error ' +
                    'fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    errorMsg +
                    '</div>';
                  $('.shipping-error').append(errorHtml);
                  scrollAnimate($('.shipping-error'));
                  defer.reject();
                }
              },
              error: function () {
                // enable the next:Payment button here
                $('body').trigger('checkout:enableButton', '.next-step-button button');
                // Server error submitting form
                defer.reject();
              }
            });
          } else {
            var isemailDisabled = false;
            // disabled feilds are not serialized, eanble before serializing
            if (form.find('input[name$=_shippingAddress_email]').length > 0 && form.find('input[name$=_shippingAddress_email]').prop('disabled')) {
              form.find('input[name$=_shippingAddress_email]').removeAttr('disabled');
              isemailDisabled = true;
            }
            var shippingFormData = form.serialize();
            if (form.find('input[name$=_shippingAddress_email]').length > 0 && isemailDisabled) {
              form.find('input[name$=_shippingAddress_email]').attr('disabled', 'disabled');
            }
            $('body').trigger('checkout:serializeShipping', {
              form: form,
              data: shippingFormData,
              callback: function (data) {
                shippingFormData = data;
              }
            });
            $.spinner().start();
            // disable the next:Payment button here
            // $('body').trigger('checkout:disableButton', '.next-step-button button');
            $.ajax({
              url: form.attr('action'),
              type: 'post',
              data: shippingFormData,
              success: function (data) {
                // Clear the CVV field value for the registered user
                if ($('.saved-payment-security-code.cvvNumField').length > 0) {
                  $('.saved-payment-security-code.cvvNumField').each(function () {
                    $(this).val('');
                    $(this).next('.valid').remove();
                  });
                }

                if (data.renderedPaymentInstruments) {
                  $('.stored-payments').empty().html(data.renderedPaymentInstruments);
                  // Set a flag to recognize if saved card is added to wallet.
                  $('.stored-payments').attr('data-savedcard', true);
                }

                if ($('.orderBillState').length && $('.orderBillState').val() === 'null' && data.order && data.order.billing.billingAddress.address) {
                  $('.orderBillState').val(data.order.billing.billingAddress.address.stateCode);
                }

                if ($('.card.payment-form .payment-next-step-button-row').hasClass('card-open')) {
                  $('.card.payment-form .billing-cancel').removeClass('d-none');
                } else {
                  $('.card.payment-form .billing-cancel').addClass('d-none');
                }
                // SFDEV-4849 | Enable GC and CC fields in case order total changes from previous one.
                if (data.order && data.order.giftCard && data.order.giftCard.amountReached) {
                  $('.credit-card-form input').attr('disabled', 'disabled');
                  $('.gift-card-form-group input').attr('disabled', 'disabled');
                  $('.gift-card-form-group .giftcard-apply-submit button').attr('disabled', 'disabled');
                  $('.cancel-new-payment').attr('disabled', 'disabled');
                  if ($('.saved-payment-instrument-section').length) {
                    $('.saved-payment-security-code').attr('disabled', 'disabled');
                    $('.credit-card-selection-new').addClass('disabled');
                  }
                } else if (data.order && data.order.giftCard && !data.order.giftCard.amountReached) {
                  $('.credit-card-form input').removeAttr('disabled');
                  $('.cancel-new-payment').removeAttr('disabled');
                  if ($('.saved-payment-instrument-section').length) {
                    $('.saved-payment-security-code').removeAttr('disabled');
                    $('.credit-card-selection-new').removeClass('disabled');
                  }
                  if (!data.order.giftCard.maxLimitReached) {
                    $('.gift-card-form-group input').removeAttr('disabled', 'disabled');
                    $('.gift-card-form input, .gift-card-form button').removeAttr('disabled');
                  } else {
                    $('.gift-card-form-group input').removeAttr('disabled', 'disabled');
                    $('.gift-card-form input, .gift-card-form button').attr('disabled', 'disabled');
                  }
                }
                if (data.order && data.order.giftCard && data.order.giftCard.giftCardHtml) {
                  $('.gift-card-applied').empty().html(data.order.giftCard.giftCardHtml);
                }
                if (data.order && data.order.orderSummaryPaymentHtml) {
                  $('.order-summary-payment-applied').empty().html(data.order.orderSummaryPaymentHtml);
                }
                if (data.order && data.order.giftCard && data.order.giftCard.hasGiftCard) {
                  if (!$('li.nav-item.paypal').hasClass('paypal-disable')) {
                    $('li.nav-item.paypal').addClass('paypal-disable');
                    $('#payPalRadioButton').attr('disabled', true);
                  }
                }
                // enable the next:Payment button here
                /* if ($('.enableBillingButton').length > 0 && $('.enableBillingButton').val() === 'false') {
                                    $('.card.payment-form').find('.submit-payment').attr('disabled', 'disabled');
                                } else {
                                    $('body').trigger('checkout:enableButton', '.next-step-button button');
                                }*/

                $('body').trigger('checkout:enableButton', '.next-step-button button');

                setTimeout(function () {
                  floatLabel.resetFloatLabel();
                  clientSideValidation.checkValidationOnAjax($('.billing-form'), true, true);
                  clientSideValidation.checkValidationOnAjax($('.credit-card-form'), true, true);
                  $('.saved-payment-security-code.cvvNumField').each(function () {
                    $(this).next('.valid').remove();
                  });
                  $('.gift-card-form-group input').next('.valid').remove();
                }, 200);

                // Uncheck the checkbox different as shipping address checkbox
                if ($('.billing-checkbox').length && $('.billing-checkbox').prop('checked') == true) {
                  $('.billing-checkbox').prop('checked', false);
                  if ($('.billing-form').find('.billing-addr-saved').hasClass('registered-user')) {
                    $('.billing-form').find('.billing-addr-saved').addClass('d-none');
                  }
                  $('.billing-form').find('.billing-addr-form').addClass('d-none');
                }
                // Updating the customer saved addresses
                if (data.customerSavedAddressesHtml) {
                  $('.stored-customer-addresses').empty().html(data.customerSavedAddressesHtml);
                }
                var steps = {};
                steps.step = 'payment';
                steps.items = data.order && data.order.items && data.order.items.items && data.order.items.items.length ? data.order.items.items : [];
                steps.optIn = data.order ? data.order.emailOptIn : false;
                shippingHelpers.methods.shippingFormResponse(defer, data);
                $('body').trigger('adobeTagManager:checkoutStepChange', steps);
                $.spinner().stop();
              },
              error: function (err) {
                // enable the next:Payment button here
                $('body').trigger('checkout:enableButton', '.next-step-button button');
                if (err.responseJSON && err.responseJSON.redirectUrl) {
                  window.location.href = err.responseJSON.redirectUrl;
                }
                // Server error submitting form
                defer.reject(err.responseJSON);
                $.spinner().stop();
              }
            });
          }
          return defer;
        } else if (stage === 'payment') {
          //
          // Submit the Billing Address Form
          //

          formHelpers.clearPreviousErrors('.payment-form');

          var billingAddressForm = $('#dwfrm_billing .billing-address-block :input').serialize();
          $('body').trigger('checkout:serializeBilling', {
            form: $('#dwfrm_billing .billing-address-block'),
            data: billingAddressForm,
            callback: function (data) {
              if (data) {
                billingAddressForm = data;
              }
            }
          });

          var contactInfoForm = $('#dwfrm_billing .contact-info-block :input').serialize();

          $('body').trigger('checkout:serializeBilling', {
            form: $('#dwfrm_billing .contact-info-block'),
            data: contactInfoForm,
            callback: function (data) {
              if (data) {
                contactInfoForm = data;
              }
            }
          });

          var activeTabId = $('.tab-pane.active').attr('id');
          var paymentInfoSelector = '#dwfrm_billing .' + activeTabId + ' .payment-form-fields :input';
          if ($('#tccCardNumber').length > 0) {
            var tccCardNumber = $('#tccCardNumber').data('cleave').getRawValue();
            if (tccCardNumber && tccCardNumber.length === 29) {
              // Copy this to Regular CC.
              $('#cardNumber').val(tccCardNumber);
            }
          }
          var paymentInfoForm = $(paymentInfoSelector).serialize();

          $('body').trigger('checkout:serializeBilling', {
            form: $(paymentInfoSelector),
            data: paymentInfoForm,
            callback: function (data) {
              if (data) {
                paymentInfoForm = data;
              }
            }
          });

          var paymentForm = billingAddressForm + '&' + contactInfoForm + '&' + paymentInfoForm;

          if ($('.data-checkout-stage').data('customer-type') === 'registered') {
            // if payment method is credit card
            if ($('.payment-information').data('payment-method-id') === 'CREDIT_CARD') {
              if (!$('.payment-information').data('is-new-payment')) {
                var cvvCode = $('.saved-payment-instrument.' + 'selected-payment .saved-payment-security-code').val();

                if (cvvCode === '') {
                  var cvvElement = $('.saved-payment-instrument.' + 'selected-payment ' + '.form-control');
                  cvvElement.addClass('is-invalid');
                  cvvElement.parent().find('.invalid-feedback').text(cvvElement.data('missing-error'));
                  cvvElement.prev('.form-control-label').addClass('is-invalid');
                  if (cvvElement.next('span').length === 0) {
                    $('<span></span>').insertAfter(cvvElement);
                  }
                  cvvElement.next('span').addClass('invalid');
                  if (cvvElement.next('span').hasClass('valid')) {
                    cvvElement.next('span').removeClass('valid').addClass('invalid');
                  }
                  scrollAnimate(cvvElement);
                  defer.reject();
                  return defer;
                }

                var $savedPaymentInstrument = $('.saved-payment-instrument' + '.selected-payment');

                paymentForm += '&storedPaymentUUID=' + $savedPaymentInstrument.data('uuid');

                paymentForm += '&securityCode=' + cvvCode;

                if (tccCardNumber && tccCardNumber.length === 29) {
                  paymentForm += '&tccCard=' + true;
                }
              }
            }
          }

          var isPLCCcard = $('.card-number-wrapper').attr('data-plcccard');
          if (isPLCCcard && isPLCCcard === 'true') {
            paymentForm += '&plcccard=true';
          }

          var errorIndicator = false;
          var formKeys = ['.credit-card-form', '.billing-address-block'];
          formKeys.forEach(function (formKeysVal, formKeysInd) {
            $(formKeysVal)
              .find('input:visible:not([disabled]), select:visible:not([disabled])')
              .each(function () {
                if (
                  typeof $(this).attr('required') !== 'undefined' &&
                  (($(this).is('input') && $(this).val().length === 0) || ($(this).is('select') && $(this).prop('selectedIndex') === -1))
                ) {
                  if (!errorIndicator) {
                    errorIndicator = true;
                  }
                  $(this).blur();
                }
              });
          });

          if (errorIndicator) {
            scrollAnimate($('.invalid-feedback:visible'));
            return;
          }

          // disable the next:Place Order button here
          // $('body').trigger('checkout:disableButton', '.next-step-button button');
          $.spinner().start();
          $.ajax({
            url: $('#dwfrm_billing').attr('action'),
            method: 'POST',
            data: paymentForm,
            success: function (data) {
              $('.checkout-secondary-section .order-product-summary').html($(data.orderProductSummary).children());
              // enable the next:Place Order button here
              $('body').trigger('checkout:enableButton', '.next-step-button button');

              var billAddrAvailable = !!(
                data.order &&
                data.order.billing &&
                data.order.billing.billingAddress &&
                data.order.billing.billingAddress.address &&
                data.order.billing.billingAddress.address.stateCode
              );
              if ($('.orderBillState').length && billAddrAvailable) {
                $('.orderBillState').val(data.order.billing.billingAddress.address.stateCode);
              }

              // Reset the TCC Form
              if ($('.regular-card-number').hasClass('d-none')) {
                $('body').trigger('checkout:resetTCCForm');
              }

              // update Estimated Earns when a Tender is entered
              var estimatedEarnDisplay = $('.hudson-point-value');
              if (estimatedEarnDisplay.length && data.order.totals) {
                estimatedEarnDisplay.text(data.order.totals.hudsonpoint);
              }

              // look for field validation errors
              if (data.error) {
                if (data.fieldErrors.length) {
                  data.fieldErrors.forEach(function (error) {
                    if (Object.keys(error).length) {
                      formHelpers.loadFormErrors('.payment-form', error);
                    }
                  });
                }

                if (data.serverErrors.length) {
                  data.serverErrors.forEach(function (error) {
                    $('.error-message').show();
                    $('.error-message-text').text(error);
                    scrollAnimate($('.error-message'));
                  });
                  if (data.orderTotalMismatch) {
                    $('.credit-card-form input').removeAttr('disabled');
                    if ($('.saved-payment-instrument-section').length) {
                      $('.saved-payment-security-code').removeAttr('disabled');
                      $('.credit-card-selection-new').removeClass('disabled');
                    }
                    if (!data.maxGCLimitReached) {
                      $('.gift-card-form-group input').removeAttr('disabled', 'disabled');
                      $('.gift-card-form input, .gift-card-form button').removeAttr('disabled');
                    }
                  }
                }

                if (data.cartError) {
                  window.location.href = data.redirectUrl;
                }

                // Toggle the Cancel button to the right state after an error.
                if ($('.card.payment-form .payment-next-step-button-row').hasClass('card-open')) {
                  $('.card.payment-form .billing-cancel').removeClass('d-none');
                } else {
                  $('.card.payment-form .billing-cancel').addClass('d-none');
                }

                defer.reject();

                $.spinner().stop();
              } else {
                // adobe tag manager
                var steps = {};
                steps.step = 'review and submit';
                steps.items = data.order && data.order.items && data.order.items.items && data.order.items.items.length ? data.order.items.items : [];
                steps.optIn = data.order ? data.order.emailOptIn : false;
                $('body').trigger('adobeTagManager:checkoutStepChange', steps);

                //
                // Populate the Address Summary
                //
                $('body').trigger('checkout:updateCheckoutView', {
                  order: data.order,
                  customer: data.customer
                });

                if (data.renderedPaymentInstruments) {
                  $('.stored-payments').empty().html(data.renderedPaymentInstruments);
                  // Set a flag to recognize if saved card is added to wallet.
                  $('.stored-payments').attr('data-savedcard', true);
                  // Enable credit card default check box
                  $('.save-card-section').find('.save-card-section-default').removeClass('d-none');
                }
                // Updating the customer saved addresses
                if (data.customerSavedAddressesHtml) {
                  $('.stored-customer-addresses').empty().html(data.customerSavedAddressesHtml);
                }

                // SFDEV-4849 | Enable GC and CC fields in case order total changes from previous one.
                if (data.order && data.order.giftCard && data.order.giftCard.amountReached) {
                  $('.credit-card-form input').attr('disabled', 'disabled');
                  $('.gift-card-form-group input').attr('disabled', 'disabled');
                  $('.gift-card-form-group .giftcard-apply-submit button').attr('disabled', 'disabled');
                  $('.cancel-new-payment').attr('disabled', 'disabled');
                  if ($('.saved-payment-instrument-section').length) {
                    $('.saved-payment-security-code').attr('disabled', 'disabled');
                    $('.credit-card-selection-new').addClass('disabled');
                  }
                } else if (data.order && data.order.giftCard && !data.order.giftCard.amountReached) {
                  $('.credit-card-form input').removeAttr('disabled');
                  $('.cancel-new-payment').removeAttr('disabled');
                  if ($('.saved-payment-instrument-section').length) {
                    $('.saved-payment-security-code').removeAttr('disabled');
                    $('.credit-card-selection-new').removeClass('disabled');
                  }
                  if (!data.order.giftCard.maxLimitReached) {
                    $('.gift-card-form-group input').removeAttr('disabled', 'disabled');
                    $('.gift-card-form input, .gift-card-form button').removeAttr('disabled');
                  } else {
                    $('.gift-card-form-group input').removeAttr('disabled', 'disabled');
                    $('.gift-card-form input, .gift-card-form button').attr('disabled', 'disabled');
                  }
                }
                if (data.order && data.order.giftCard && data.order.giftCard.giftCardHtml) {
                  $('.gift-card-applied').empty().html(data.order.giftCard.giftCardHtml);
                }
                if (data.order && data.order.orderSummaryPaymentHtml) {
                  $('.order-summary-payment-applied').empty().html(data.order.orderSummaryPaymentHtml);
                }
                if (data.order && data.order.giftCard && data.order.giftCard.hasGiftCard) {
                  if (!$('li.nav-item.paypal').hasClass('paypal-disable')) {
                    $('li.nav-item.paypal').addClass('paypal-disable');
                    $('#payPalRadioButton').attr('disabled', true);
                  }
                }

                if (data.customer.registeredUser && data.customer.customerPaymentInstruments.length) {
                  $('.cancel-new-payment').removeClass('checkout-hidden');
                }
                $.spinner().stop();
                scrollAnimate();
                defer.resolve(data);
              }
            },
            error: function (err) {
              // enable the next:Place Order button here
              $('body').trigger('checkout:enableButton', '.next-step-button button');
              if (err.responseJSON && err.responseJSON.redirectUrl) {
                window.location.href = err.responseJSON.redirectUrl;
              }
              $.spinner().stop();
            }
          });

          return defer;
        } else if (stage === 'placeOrder') {
          // disable the placeOrder button here
          // $('body').trigger('checkout:disableButton', '.next-step-button button');
          $.spinner().start();
          // if Adjusted Rewards Points are available, forward them through to Order-Confirm controller via CS-PlaceOrder
          const updatedEarns = document.querySelector('.hudson-point-value');
          const adjustedEarns = updatedEarns ? updatedEarns.innerHTML : '';

          $.ajax({
            url: $('.place-order').data('action') + '?adjustedEarns=' + adjustedEarns,
            data: { inAuthTID: $('#inAuthTID').val() },
            method: 'POST',
            success: function (data) {
              // enable the placeOrder button here
              $('body').trigger('checkout:enableButton', '.next-step-button button');
              if (data.error) {
                if (data.cartError) {
                  window.location.href = data.redirectUrl;
                  defer.reject();
                } else {
                  // go to appropriate stage and display error message
                  defer.reject(data);
                }
              } else {
                var continueUrl = data.continueUrl;
                var urlParams = {
                  ID: data.orderID,
                  token: data.orderToken
                };

                continueUrl +=
                  (continueUrl.indexOf('?') !== -1 ? '&' : '?') +
                  Object.keys(urlParams)
                    .map(function (key) {
                      return key + '=' + encodeURIComponent(urlParams[key]);
                    })
                    .join('&');

                window.location.href = continueUrl;
                defer.resolve(data);
              }
              $.spinner().stop();
            },
            error: function () {
              // enable the placeOrder button here
              $('body').trigger('checkout:enableButton', $('.next-step-button button'));
              $.spinner().stop();
            }
          });

          return defer;
        } else if (stage === 'pickupperson') {
          // instore pickup init
          return storePickPersonHelpers.submitInstorePickup(defer);
        }
        var p = $('<div>').promise(); // eslint-disable-line
        setTimeout(function () {
          p.done(); // eslint-disable-line
        }, 500);
        return p; // eslint-disable-line
      },

      /**
       * Initialize the checkout stage.
       *
       * TODO: update this to allow stage to be set from server?
       */
      initialize: function () {
        // set the initial state of checkout
        members.currentStage = checkoutStages.indexOf($('.data-checkout-stage').data('checkout-stage'));
        $(plugin).attr('data-checkout-stage', checkoutStages[members.currentStage]);

        //
        // Handle Payment option selection
        //
        $('input[name$="paymentMethod"]', plugin).on('change', function () {
          $('.credit-card-form').toggle($(this).val() === 'CREDIT_CARD');
        });

        //
        // Handle address selection on page load
        //
        if ($('.data-checkout-stage').data('customer-type') === 'registered' && $('input[name="address"]:checked').length) {
          var selectedAdress = $('input[name="address"]:checked').data('address-info');
          var address = {};
          var shipmentUUID = $('[name=shipmentUUID]').val();
          if (selectedAdress && shipmentUUID) {
            address.UUID = shipmentUUID;
            address.shippingAddress = selectedAdress;
            shippingHelpers.methods.updateShippingAddressFormValues(address);
            $('input[name$=_shippingAddress_addressFields_address2]', 'form.shipping-form').val(selectedAdress.address2);
            if (selectedAdress.email) {
              $('input[name$=_shippingAddress_email]', 'form.shipping-form').val(selectedAdress.email);
            } else if ($('input[name="address"]:checked').data('customer-email')) {
              $('input[name$=_shippingAddress_email]', 'form.shipping-form').val($('input[name="address"]:checked').data('customer-email'));
            }
          }
        }
        //
        // Handle Next State button click
        //
        $(plugin).on('click', '.next-step-button button', function () {
          members.nextStage();
        });

        //
        // Handle Edit buttons on shipping and payment summary cards
        //
        $('.shipping-summary .edit-button', plugin).on('click', function () {
          if (!$('#checkout-main').hasClass('multi-ship')) {
            $('body').trigger('shipping:selectSingleShipping');
            var steps = {};
            steps.step = 'shipping address';
            $('body').trigger('adobeTagManager:checkoutStepChange', steps);
          }

          members.gotoStage('shipping');

          // trigger select on the chosen address to run validation and enabled checkout button
          if (
            $('.data-checkout-stage').data('customer-type') === 'registered' &&
            $('input[name="address"]:checked').length &&
            $('input[name="address"]:checked').closest('.shipping-content .form-check.customer-addresses-section').length > 0
          ) {
            $('input[name="address"]:checked').closest('.shipping-content .form-check.customer-addresses-section').trigger('click');
          }
          setTimeout(function () {
            floatLabel.resetFloatLabel();
            clientSideValidation.checkValidationOnAjax($('.shipping-address-block'), true);
          }, 200);
          if ($('.cardExpiration').attr('required') === 'required' && $('.cardExpiration').val().length == 0) {
            $('.cardExpiration').removeAttr('placeholder');
          }
        });

        $('.payment-summary .edit-button', plugin).on('click', function () {
          // Uncheck the checkbox different as shipping address checkbox
          if ($('.billing-checkbox').length && $('.billing-checkbox').prop('checked') == true) {
            $('.billing-checkbox').prop('checked', false);
            if ($('.billing-form').find('.billing-addr-saved').hasClass('registered-user')) {
              $('.billing-form').find('.billing-addr-saved').addClass('d-none');
            }
            $('.billing-form').find('.billing-addr-form').addClass('d-none');
          }

          // Check if Any address save in the wallet.
          var cardSaved = $('.stored-payments').attr('data-savedcard');

          if (cardSaved !== undefined && $('.billing-form').find('.billing-addr-saved').hasClass('registered-user')) {
            $('.credit-card-form').addClass('checkout-hidden');
            $('.user-payment-instruments').removeClass('checkout-hidden');
            $('.payment-next-step-button-row').removeClass('cancel-button-enable card-open');
          }

          if ($('.payment-information').data('is-new-payment')) {
            $('.payment-information').data('is-new-payment', false);
          }

          if (cardSaved !== undefined && $('.billing-form').find('.billing-addr-saved').hasClass('registered-user')) {
            $('.credit-card-form').addClass('checkout-hidden');
            $('.user-payment-instruments').removeClass('checkout-hidden');
            $('.payment-next-step-button-row').removeClass('cancel-button-enable card-open');
          }

          if ($('.payment-information').data('is-new-payment')) {
            $('.payment-information').data('is-new-payment', false);
          }

          /* if ($('.card.payment-form .payment-next-step-button-row').hasClass('card-open')) {
                        $('.card.payment-form .billing-cancel').removeClass('d-none');
                    } else {
                        $('.card.payment-form .billing-cancel').addClass('d-none');
                    }*/

          $('.card.payment-form .billing-cancel').addClass('d-none');

          // changes done to udpate the state drop down
          if ($('select[name $= "billing_addressFields_country"]').val() !== null) {
            var country = $('select[name $= "billing_addressFields_country"]').val();
            var billingState;
            var stateField = $('select[name $= "billing_addressFields_states_stateCode"]');
            var countryRegion = $('.countryRegion').data('countryregion');
            if (!countryRegion) {
              return;
            }
            var regions = countryRegion[country].regions;
            var regionsLabel = countryRegion[country].regionLabel;
            stateField.closest('.form-group').find('label.form-control-label').text(regionsLabel);
            var postalField = $('.billing-form').find('.billingZipCode');
            // if Country is UK, display the text field for state.
            if (country === 'UK') {
              var optionArr = [];
              for (var stateCode in regions) {
                // eslint-disable-line
                optionArr.push('<option id="' + stateCode + '" value="' + stateCode + '">' + regions[stateCode] + '</option>');
              }
              stateField.html(optionArr.join(''));

              if ($('.site-locale').val() === 'fr_CA') {
                postalField.closest('.form-group').find('label.form-control-label').text(postalField.data('zipcode-fr'));
              } else {
                postalField.closest('.form-group').find('label.form-control-label').text(postalField.data('zipcode-label'));
              }

              if ($('.orderBillState').length && $('.orderBillState').val() !== '') {
                billingState = $('.orderBillState').val();
              } else {
                billingState = $('.js-state-code-input').val();
              }
              $('.state-drop-down').addClass('d-none');
              $('.state-input').removeClass('d-none');
              $('form[name=dwfrm_billing]').find('.js-state-code-input').val(billingState);
              $('.state-drop-down').find('.form-group').removeClass('required');
              $('.state-drop-down').find('.billingState').prop('required', false);
              $('.state-drop-down').find('.billingState').removeClass('is-invalid');
            } else {
              // Generate the State Options
              var optionArr = [];
              for (var stateCode in regions) {
                // eslint-disable-line
                optionArr.push('<option id="' + stateCode + '" value="' + stateCode + '">' + regions[stateCode] + '</option>');
              }
              // Update the State Field
              stateField.html(optionArr.join(''));

              if ($('.site-locale').val() === 'fr_CA') {
                postalField.closest('.form-group').find('label.form-control-label').text(postalField.data('zipcode-fr'));
              } else if (country === 'CA') {
                postalField.closest('.form-group').find('label.form-control-label').text(postalField.data('zipcode-ca'));
              } else {
                postalField.closest('.form-group').find('label.form-control-label').text(postalField.data('zipcode-label'));
              }

              if ($('.orderBillState').length && $('.orderBillState').val() !== '') {
                billingState = $('.orderBillState').val();
              } else {
                billingState = $('.billingState').val();
              }
              $('.state-drop-down').removeClass('d-none');
              $('.state-input').addClass('d-none');
              $('form[name=dwfrm_billing]').find('select[name$=_stateCode]').val(billingState);
              $('.state-drop-down').find('.form-group').addClass('required');
              $('.state-drop-down').find('.billingState').prop('required', true);
            }
            clientSideValidation.updatePoPatterWithCountry($('.billing-addr-form'));
          }
          setTimeout(function () {
            floatLabel.resetFloatLabel();
            clientSideValidation.checkValidationOnAjax($('.billing-form'), true, true);
            clientSideValidation.checkValidationOnAjax($('.credit-card-form'), true, true);
            hbcTooltip.tooltipInit();
          }, 200);
          members.gotoStage('payment');
          var steps = {};
          steps.step = 'payment';
          $('body').trigger('adobeTagManager:checkoutStepChange', steps);
        });

        $('.pickup-summary .edit-button', plugin).on('click', function () {
          members.gotoStage('pickupperson');
          var steps = {};
          steps.step = 'pickup options';
          $('body').trigger('adobeTagManager:checkoutStepChange', steps);
        });
        //
        // remember stage (e.g. shipping)
        //
        updateUrl(members.currentStage);

        //
        // Listen for foward/back button press and move to correct checkout-stage
        //
        $(window).on('popstate', function (e) {
          //
          // Back button when event state less than current state in ordered
          // checkoutStages array.
          //
          if (e.state === null || checkoutStages.indexOf(e.state) < members.currentStage) {
            members.handlePrevStage(false);
          } else if (checkoutStages.indexOf(e.state) > members.currentStage) {
            // Forward button  pressed
            members.handleNextStage(false);
          }
        });

        //
        // Set the form data
        //
        plugin.data('formData', formData);
      },

      /**
       * The next checkout state step updates the css for showing correct buttons etc...
       */
      nextStage: function () {
        var promise = members.updateStage();

        promise.done(function (data) {
          // Update UI with new stage
          if (data.skipShipping) {
            members.gotoStage('payment');
            return;
          }
          if (data.isBasketUpdated && data.isBasketUpdated.Express === true && data.isBasketUpdated.basketUpdated === true) {
            // Once Profile is updated, make it normal flow.
            $('#checkout-main').attr('data-express-checkout', 'false');
          }
          if (!data.createCustomerAddressHtml) {
            // skip next stage navigation for address add view
            members.handleNextStage(true);
          }
        });

        promise.fail(function (data) {
          // show errors
          if (data) {
            if (data.errorStage) {
              members.gotoStage(data.errorStage.stage);

              if (data.errorStage.step === 'billingAddress') {
                var $billingAddressSameAsShipping = $('input[name$="_shippingAddressUseAsBillingAddress"]');
                if ($billingAddressSameAsShipping.is(':checked')) {
                  $billingAddressSameAsShipping.prop('checked', false);
                }
              }
            }

            if (data.errorMessage) {
              $('.error-message').show();
              $('.error-message-text').text(data.errorMessage);
            }
          }
        });
      },

      /**
       * The next checkout state step updates the css for showing correct buttons etc...
       *
       * @param {boolean} bPushState - boolean when true pushes state using the history api.
       */
      handleNextStage: function (bPushState) {
        if (members.currentStage < checkoutStages.length - 1) {
          // move stage forward
          members.currentStage++;

          if ($('#checkout-main').attr('data-express-checkout') == 'true') {
            members.currentStage = 2;
            // $('#checkout-main').attr('data-express-checkout', 'false');
          }

          //
          // show new stage in url (e.g.payment)
          //
          if (bPushState) {
            updateUrl(members.currentStage);
          }
        }

        // Set the next stage on the DOM
        $(plugin).attr('data-checkout-stage', checkoutStages[members.currentStage]);
      },

      /**
       * Previous State
       */
      handlePrevStage: function () {
        if (members.currentStage > 0) {
          // move state back
          members.currentStage--;
          updateUrl(members.currentStage);
        }

        $(plugin).attr('data-checkout-stage', checkoutStages[members.currentStage]);
      },

      /**
       * Use window history to go to a checkout stage
       * @param {string} stageName - the checkout state to goto
       */
      gotoStage: function (stageName) {
        members.currentStage = checkoutStages.indexOf(stageName);
        updateUrl(members.currentStage);
        $(plugin).attr('data-checkout-stage', checkoutStages[members.currentStage]);
      }
    };

    //
    // Initialize the checkout
    //
    members.initialize();

    return this;
  };
})(jQuery);

var exports = {
  initialize: function () {
    $('#checkout-main').checkout();
  },

  updateCheckoutView: function () {
    $('body').on('checkout:updateCheckoutView', function (e, data) {
      if (data.order && data.instorepickup) {
        storePickPersonHelpers.updateStorePickUpPersonInfo(data.order, data.instorepickup); // update instore details on every step
      }
      shippingHelpers.methods.updateMultiShipInformation(data.order);
      summaryHelpers.updateTotals(data.order.totals, data.order);
      data.order.shipping.forEach(function (shipping) {
        shippingHelpers.methods.updateShippingInformation(shipping, data.order, data.customer, data.options);
      });
      data.order.shipping.forEach(function (shipping) {
        try {
          if (shipping) {
            var shippingMethods = shipping.applicableShippingMethods;
            var srToken = cookiesUtil.getCookie('sr_token');
            $.each(shippingMethods, function (methodIndex, shippingMethod) {
              if (shippingMethod.ID === 'shoprunner' && $('.checkout-shop-runner').length > 0) {
                var shippingLabel = $('label.shipping-method-option');
                $.each(shippingLabel, function (labelIndex, shipMethodLabel) {
                  if (
                    $(shipMethodLabel).attr('for').indexOf('shippingMethod-shoprunner') > -1 &&
                    (srToken === null || srToken === undefined || srToken === '')
                  ) {
                    $(shipMethodLabel).closest('.col-12').remove();
                  }
                });
              }
            });
          }
        } catch (e) {
          // eslint-disable-line
          // do not error out
        }
      });
      billingHelpers.methods.updateBillingInformation(data.order, data.customer, data.options);
      billingHelpers.methods.updatePaymentInformation(data.order, data.options);
      billingHelpers.methods.resetPLCCBanner();
      summaryHelpers.updateOrderProductSummaryInformation(data.order, data.options);
      summaryHelpers.updateProductPromotionsSummary(data.order.items);
      summaryHelpers.updateAppliedCouponCount(data.order);
    });
  },

  disableButton: function () {
    $('body').on('checkout:disableButton', function (e, button) {
      $(button).prop('disabled', true);
    });
  },

  enableButton: function () {
    $('body').on('checkout:enableButton', function (e, button) {
      $(button).prop('disabled', false);
    });
  },

  resetTCCCardForm: function () {
    $('body').on('checkout:resetTCCForm', function (e) {
      // Hide all regular card component
      $('.billing-tcc-cancel').addClass('d-none');
      var mainForm = $('.payment-form-fields .credit-card-form');

      if (!$('.tcc-card-number').hasClass('d-none')) {
        mainForm.find('.tcc-card-number').addClass('d-none');
      }

      mainForm.find('.cardExpiryDate').removeClass('d-none');
      mainForm.find('.newCreditCvvHolder').removeClass('d-none');
      mainForm.find('.cardExpiryDate').addClass('required');
      mainForm.find('.cardExpiryMonth').addClass('required');
      mainForm.find('.cardExpiryYear').addClass('required');
      mainForm.find('.newCreditCvvHolder').addClass('required');
      mainForm.find('.cardExpiration').prop('required', true);
      mainForm.find('.expirationMonth').prop('required', true);
      mainForm.find('.expirationYear').prop('required', true);
      mainForm.find('.securityCode').prop('required', true);

      // Remove all invalid classes
      mainForm.find('.cardNumber').removeClass('is-invalid').removeClass('focus-visible');
      mainForm.find('.cardNumber').next('label').removeClass('is-invalid').removeClass('input-focus');

      mainForm.find('.cardExpiration').removeClass('is-invalid').removeClass('focus-visible');
      mainForm.find('.cardExpiration').next('label').removeClass('is-invalid').removeClass('input-focus');

      mainForm.find('.securityCode').removeClass('is-invalid').removeClass('focus-visible');
      mainForm.find('.securityCode').next('label').removeClass('is-invalid').removeClass('input-focus');

      mainForm.find('#tccCardNumber').removeClass('is-invalid').removeClass('focus-visible');
      mainForm.find('#tccCardNumber').next('label').removeClass('is-invalid').removeClass('input-focus');

      mainForm.find('.regular-card-number').removeClass('d-none');

      mainForm.find('.regular-expiry').removeClass('d-none');

      mainForm.find('.regular-cvv').removeClass('d-none');

      // Copy Current entered data and populate in TCC field
      mainForm.find('.tccCardNumber').val('');
      mainForm.find('#tccCardNumber').val('');
      mainForm.find('#tccCardNumber').removeClass('is-invalid');
      mainForm.find('#tccCardNumber').next('span').remove();
      mainForm.find('#tccCardNumber').next('label').removeClass('is-invalid').removeClass('input-focus');

      $('.card-number-wrapper').removeAttr('data-type');
      $('.card-number-wrapper').removeAttr('data-plcccard');

      if (!$('.tcc-link').hasClass('d-none')) {
        $('.tcc-link').addClass('d-none');
      }
    });
  },

  checkoutPromoRewards: function () {
    $('body').on('click', '.rewards-click', function () {
      $(this).toggleClass('open-state');
      $(this).closest('.form-group').find('.rewards-detail-section').toggleClass('expand');
    });
    $('body').on('click', '.promotab-click', function () {
      $(this).toggleClass('open-state');
      $(this).closest('.form-group').find('.promo-detail-section').toggleClass('expand');
      $('input.coupon-code-field').each(function () {
        var $label = $(this).closest('.form-group').find('label');
        if ($(this).val() !== null) {
          if (!$(this).val().length && $label.hasClass('input-focus')) {
            $label.removeClass('input-focus');
          }
          if ($(this).val().length) {
            $label.addClass('input-focus');
          }
        } else if ($label.hasClass('input-focus')) {
          $label.removeClass('input-focus');
        }
      });
    });
  },

  floatLabel: floatLabel.resetFloatLabel,
  giftCardBalanceCheck: giftCard.giftCardBalanceCheck,
  removeGiftCardAction: giftCard.removeGiftCardAction
};

[billingHelpers, shippingHelpers, addressHelpers].forEach(function (library) {
  Object.keys(library).forEach(function (item) {
    if (typeof library[item] === 'object') {
      exports[item] = $.extend({}, exports[item], library[item]);
    } else {
      exports[item] = library[item];
    }
  });
});

module.exports = exports;
