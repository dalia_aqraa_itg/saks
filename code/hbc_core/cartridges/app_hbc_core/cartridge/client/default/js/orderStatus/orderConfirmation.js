'use strict';

var hbcTooltip = require('../tooltip');
var orderCancel = require('./orderCancel');

function ConfirmpwdValidation() {
  var emailval = $('.order-confirm-create-account #newPassword').val();
  var confirmemailval = $('.order-confirm-create-account #newPasswordConfirm').val();
  var $passField = $('.order-confirm-create-account #newPassword');
  var $this = $('.order-confirm-create-account #newPasswordConfirm');
  if (emailval !== '' && confirmemailval !== '') {
    if (emailval !== confirmemailval) {
      $this.addClass('is-invalid');
      $this.prev('label').addClass('is-invalid');
      if ($this.next('span').length === 0) {
        $('<span></span>').insertAfter($this);
      }
      $this.next('span').addClass('invalid');
      if ($this.next('span').hasClass('valid')) {
        $this.next('span').removeClass('valid').addClass('invalid');
      }
      $this.parents('.form-group').find('.invalid-feedback').text($this.data('pattern-pwd-mismatch'));
    } else if (emailval == confirmemailval && !$passField.hasClass('is-invalid')) {
      if ($this.hasClass('is-invalid')) {
        $this.removeClass('is-invalid');
        $this.prev('label').removeClass('is-invalid');
        if ($this.next('span').hasClass('invalid')) {
          $this.next('span').removeClass('invalid').addClass('valid');
        }
        $this.parents('.form-group').find('.invalid-feedback').empty();
      }
    }
    return false;
  }
}

module.exports = {
  orderConfirmation: function () {
    $('.order-confirmation').on('click', '.order-status', function (e) {
      e.preventDefault();
      $.ajax({
        url: $(this).attr('href'),
        method: 'GET',
        data: {
          ajax: true
        },
        success: function (data) {
          if (!data.error) {
            $('.order-status-page').removeClass('row no-gutters order-confirmation');
            $('.order-status-page').empty().html(data);
            hbcTooltip.tooltipInit();
          }
        }
      });
    });
  },
  passwordValidation: function () {
    $('.order-confirm-create-account .passCheck').on('blur', function () {
      ConfirmpwdValidation();
    });
  },
  orderCancelHandler: function () {
    orderCancel();
  }
};
