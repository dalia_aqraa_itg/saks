'use strict';
var base = require('base/checkout/shipping');
var addressHelpers = require('base/checkout/address');
var formHelpers = require('base/checkout/formErrors');
var scrollAnimate = require('base/components/scrollAnimate');
var floatLabel = require('../floatLabel');
var hbcTooltip = require('../tooltip');
var clientSideValidation = require('../components/clientSideValidation');
var cookiesUtil = require('../components/utilhelper');

/**
 * Does Ajax call to select shipping method
 * @param {string} url - string representation of endpoint URL
 * @param {Object} urlParams - url params
 * @param {Object} el - element that triggered this call
 */
function selectShippingMethodAjax(url, urlParams, el) {
  $.spinner().start();
  $.ajax({
    url: url,
    type: 'post',
    dataType: 'json',
    data: urlParams
  })
    .done(function (data) {
      var dropshipDefaultMethod = el.data('dropshipdefault-id');
      if (data.error) {
        window.location.href = data.redirectUrl;
      } else {
        $('body').trigger('checkout:updateCheckoutView', {
          order: data.order,
          customer: data.customer,
          options: { keepOpen: true },
          urlParams: urlParams
        });
        $('body').trigger('checkout:postUpdateCheckoutView', {
          el: el
        });
        // update drop ship restriction message
        data.order.shipping.forEach(function (shipping) {
          if (
            dropshipDefaultMethod &&
            dropshipDefaultMethod !== 'null' &&
            shipping.selectedShippingMethod &&
            shipping.selectedShippingMethod.ID !== dropshipDefaultMethod
          ) {
            let id1 = el.siblings('.dropship-msg').attr('data-shipping-id1');
            let id2 = el.siblings('.dropship-msg').attr('data-shipping-id2');
            let dropShipMsg = el.siblings('.dropship-msg').text().replace('-', shipping.selectedShippingMethod.displayName);
            if (id1 !== undefined && id1 !== '' && id2 !== undefined && id2 !== '') {
              dropShipMsg = dropShipMsg.replace(id1, id2);
            }
            el.siblings('.hbc-alert-info').removeClass('d-none');
            el.siblings('.hbc-alert-info').html(dropShipMsg);
          } else {
            el.siblings('.hbc-alert-info').addClass('d-none');
            el.siblings('.hbc-alert-info').html('');
          }
          if (shipping.selectedShippingMethod) {
            el.siblings('.hbc-shipping-method-description').removeClass('d-none');
            el.siblings('.hbc-shipping-method-description').html(shipping.selectedShippingMethod.description);
          }
        });
        // update signature required
        if (data.signatureRequired && data.signatureThreshold) {
          if (data.order.totals.grandTotalValue > data.signatureThreshold) {
            $('.signature-required').removeClass('d-none');
            $('.signature-checkbox').addClass('d-none');
            $('.signature-is-required').removeClass('d-none');
          } else {
            $('.signature-required').addClass('d-none');
            $('.signature-checkbox').removeClass('d-none');
            $('.signature-is-required').addClass('d-none');
          }
        }
      }
      // show error message on non-canadian address entry on shipping form
      var addressInfo = $('input[name="address"]:checked').data('address-info');
      if (addressInfo) {
        // eslint-disable-next-line no-undef
        if (defaultShippingCountry && addressInfo.countryCode && addressInfo.countryCode.value !== defaultShippingCountry && $('.coutry-rest-msg').length > 0) {
          $('.coutry-rest-msg').removeClass('d-none');
          // $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
          // disable address edit for non candaian address
          if ($('input[name="address"]:checked').closest('.customer-addresses-section').find('.edit-customer-address').length > 0) {
            $('input[name="address"]:checked').closest('.customer-addresses-section').find('.edit-customer-address').addClass('no-mouse-events');
          }
        } else {
          $('.coutry-rest-msg').addClass('d-none');
        }
      }
      $.spinner().stop();
    })
    .fail(function () {
      $.spinner().stop();
    });
}

/**
 * Update list of available shipping methods whenever user modifies shipping address details.
 * @param {jQuery} $shippingForm - current shipping form
 */
function updateShippingMethodList($shippingForm) {
  // delay for autocomplete!
  setTimeout(function () {
    var $shippingMethodList = $shippingForm.find('.shipping-method-list');
    var urlParams = addressHelpers.methods.getAddressFieldsFromUI($shippingForm);
    var shipmentUUID = $shippingForm.find('[name=shipmentUUID]').val();
    var url = $shippingMethodList.data('actionUrl');
    urlParams.shipmentUUID = shipmentUUID;

    $.spinner().start();
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: urlParams,
      success: function (data) {
        if (data.error) {
          window.location.href = data.redirectUrl;
        } else {
          $('body').trigger('checkout:updateCheckoutView', {
            order: data.order,
            customer: data.customer,
            options: { keepOpen: true }
          });
          // toggle no shipping method message based on shipping form validation
          var result = clientSideValidation.checkFormvalidCheckout.call($('form.shipping-form'));
          if (result && $('form.shipping-form').closest('.checkout-primary-section').length !== 0) {
            if ($('.no-shipping-method-msg').attr('data-has-methods') != undefined && $('.no-shipping-method-msg').attr('data-has-methods') === 'true') {
              $('.no-shipping-method-msg').addClass('d-none');
              $('.next-step-button:visible').find('button[type="submit"]').removeAttr('disabled');
            } else if (
              $('.no-shipping-method-msg').attr('data-has-methods') != undefined &&
              $('.no-shipping-method-msg').attr('data-has-methods') === 'false'
            ) {
              $('.no-shipping-method-msg').removeClass('d-none');
              $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
            }
          } else {
            $('.no-shipping-method-msg').addClass('d-none');
            $('.next-step-button:visible').find('button[type="submit"]').removeAttr('disabled');
          }
          $.spinner().stop();
        }
      }
    });
  }, 300);
}

/**
 * Check restricted state and display the error message
 * @param {string} data - string to be validated
 */
function checkRestrictedState(data) {
  var restrictedStates = $('.shipping-address-block').data('restricted-sates');
  if (restrictedStates && restrictedStates !== null && restrictedStates.length > 0) {
    if (data && data != '' && (restrictedStates.indexOf(data.toLowerCase()) >= 0 || restrictedStates.indexOf(data.toUpperCase()) >= 0)) {
      $('.restricted-state-message').removeClass('d-none');
      // $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
    } else {
      $('.restricted-state-message').addClass('d-none');
    }
  }
}

/**
 * Check PO address and display the error message
 * @param {string} data - string to be validated
 */
function poCheck(data) {
  // var data = ($(this).val()).toLowerCase();
  if (data && data.length > 0 && $('.shipping-address-block').data('poenabled')) {
    var reGroups = [
      '^ *((#\\d+))',
      '((box|bin)[-. \\/\\\\]?\\d+)',
      '(.*p[ \\.]? ?(o|0)[-. \\/\\\\]? *-?((box|bin)|b|(#|num)?\\d+))',
      '(p(ost)? *(o(ff(ice)?)?)? *((box|bin)|b)? *\\d+)',
      '(p *-?\\/?(o)? *-?box)',
      'post office box',
      '(((box|bin)|b) *(number|num|#)? *\\d+)$',
      '(\\d+ *(box|post box|post office box)$)',
      '(((num|number|#) *\\d+)$)',
      '^[afd][ .]?p[ .]?o'
    ];
    for (var i = 0; i < reGroups.length; i++) {
      var pattern = new RegExp(reGroups[i], 'i');
      var poResult = pattern.test(data);
      if (poResult) {
        $('.po-check-message').removeClass('d-none');
        // $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
        break;
      } else {
        $('.po-check-message').addClass('d-none');
      }
    }
  }
}

/**
 * Check FPO,APO, DPO address and display the error message
 * @param {string} data - string to be validated
 */
function fpoCheck(data) {
  if (data && data.length > 0 && $('.shipping-address-block').data('poenabled')) {
    var reGroups = ['^[afd][ .]?p[ .]?o'];
    for (var i = 0; i < reGroups.length; i++) {
      var pattern = new RegExp(reGroups[i], 'i');
      var poResult = pattern.test(data);
      if (poResult) {
        $('.po-check-message').removeClass('d-none');
        break;
      } else {
        $('.po-check-message').addClass('d-none');
      }
    }
  }
}

/**
 * Create an alert to display the error message
 * @param {Object} message - Error message to display
 */
function createErrorNotification(message) {
  var errorHtml =
    '<div class="alert alert-danger alert-dismissible valid-cart-error hbc-alert-error ' +
    'fade show" role="alert">' +
    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
    '<span aria-hidden="true">&times;</span>' +
    '</button>' +
    message +
    '</div>';

  $('.shipping-error').append(errorHtml);
  scrollAnimate($('.shipping-error'));
}

/**
 * Toggels address to address and addresses diaplay view
 * @param {string} view - the view to toggle
 */
function toggleShippingItems(view) {
  var shippingContent = $('.shipping-content');
  var shippingForm = $('form.shipping-form');
  var shippingSubmit = shippingForm.data('shipment-submit');
  var addressSubmit = shippingForm.data('address-submit');
  if (shippingContent.length) {
    if (view === 'address') {
      shippingContent.find('#customer-addresses').removeClass('d-none');
      shippingContent.find('.shipping-address-block').addClass('d-none');
      shippingContent.find('.address-add-registered').removeClass('d-none');
      shippingContent.find('.shipping-method-block').removeClass('d-none');
      if (!shippingContent.find('.gift-message-block').hasClass('hide-giftwrap')) {
        shippingContent.find('.gift-message-block').removeClass('d-none');
      }
      shippingContent.find('.shipping-next-step-button-row').removeClass('d-none');
      shippingContent.find('div.shipping-next-step-button-row.shipping-save-address').addClass('d-none');
      if (shippingSubmit) {
        shippingForm.attr('action', shippingSubmit);
      }
      var addressInfo = $('input[name="address"]:checked').data('address-info');
      if (addressInfo) {
        // eslint-disable-next-line no-undef
        if (defaultShippingCountry && addressInfo.countryCode && addressInfo.countryCode.value !== defaultShippingCountry && $('.coutry-rest-msg').length > 0) {
          $('.coutry-rest-msg').removeClass('d-none');
          // disable address edit for non candaian address
          if ($('input[name="address"]:checked').closest('.customer-addresses-section').find('.edit-customer-address').length > 0) {
            $('input[name="address"]:checked').closest('.customer-addresses-section').find('.edit-customer-address').addClass('no-mouse-events');
          }
          // $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
        } else {
          $('.coutry-rest-msg').addClass('d-none');
          $('.next-step-button:visible').find('button[type="submit"]').removeAttr('disabled');
        }
      }
      if (shippingContent.find('.signature-required').length && shippingContent.find('.signature-required').hasClass('verbiage-shown')) {
        shippingContent.find('.signature-required').removeClass('d-none');
      }
      if (shippingContent.find('.signature-checkbox').length && shippingContent.find('.signature-checkbox').hasClass('box-shown')) {
        shippingContent.find('.signature-checkbox').removeClass('d-none');
      }
      if ($('.shipping-method-list').length > 0 && !$('.shipping-method-list').hasClass('no-address')) {
        $('.shipping-method-list').removeClass('d-none');
      }
    } else {
      shippingContent.find('#customer-addresses').addClass('d-none');
      shippingContent.find('.shipping-address-block').removeClass('d-none');
      shippingContent.find('.address-add-registered').addClass('d-none');
      shippingContent.find('.shipping-method-block').addClass('d-none');
      shippingContent.find('.gift-message-block').addClass('d-none');
      shippingContent.find('.shipping-next-step-button-row').addClass('d-none');
      shippingContent.find('div.shipping-next-step-button-row.shipping-save-address').removeClass('d-none');
      shippingContent.find('.signature-required').addClass('d-none');
      shippingContent.find('.signature-checkbox').addClass('d-none');
      if (addressSubmit) {
        shippingForm.attr('action', addressSubmit);
      }
    }
    if ($('.no-shipping-method-msg').attr('data-has-methods') != undefined && $('.no-shipping-method-msg').attr('data-has-methods') === 'true') {
      $('.no-shipping-method-msg').addClass('d-none');
      $('.next-step-button:visible').find('button[type="submit"]').removeAttr('disabled');
    } else if ($('.no-shipping-method-msg').attr('data-has-methods') != undefined && $('.no-shipping-method-msg').attr('data-has-methods') === 'false') {
      $('.no-shipping-method-msg').removeClass('d-none');
      $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
    }
  }
}

/**
 *
 * Update customer addresses content
 *
 * @param {string} customerAddressHtml - HTML dom of customer address
 */
function updateShippingAddressInformation(customerAddressHtml) {
  $('#customer-addresses').html(customerAddressHtml);
}

/**
 * updates the shipping address form values within shipping forms
 * @param {Object} shipping - the shipping (shipment model) model
 */
base.methods.updateShippingAddressFormValues = function (shipping) {
  var addressObject = $.extend({}, shipping.shippingAddress);

  if (addressObject) {
    addressObject.isGift = shipping.isGift;
    addressObject.giftMessage = shipping.giftMessage;

    $('input[value=' + shipping.UUID + ']').each(function (formIndex, el) {
      var form = el.form;
      if (!form) return;
      var countryCode = addressObject.countryCode;

      if (addressObject.firstName !== '' && addressObject.firstName !== null) $('input[name$=_firstName]', form).val(addressObject.firstName);
      if (addressObject.lastName !== '' && addressObject.lastName !== null) $('input[name$=_lastName]', form).val(addressObject.lastName);
      if (addressObject.address1 !== '' && addressObject.address1 !== null) $('input[name$=_address1]', form).val(addressObject.address1);
      if (addressObject.address2 !== '' && addressObject.address2 !== null) $('input[name$=_address2]', form).val(addressObject.address2);
      if (addressObject.city !== '' && addressObject.city !== null) $('input[name$=_city]', form).val(addressObject.city);
      if (addressObject.postalCode !== '' && addressObject.postalCode !== null) $('input[name$=_postalCode]', form).val(addressObject.postalCode);
      if (addressObject.stateCode !== '' && addressObject.stateCode !== null && addressObject.stateCode != undefined)
        $('select[name$=_stateCode],input[name$=_stateCode]', form).val(addressObject.stateCode);

      // eslint-disable-next-line no-undef
      if (
        !defaultShippingCountry ||
        (defaultShippingCountry && !!addressObject.countryCode && addressObject.countryCode === defaultShippingCountry) ||
        (defaultShippingCountry &&
          !!addressObject.countryCode &&
          !!addressObject.countryCode.value &&
          addressObject.countryCode.value === defaultShippingCountry)
      ) {
        if (countryCode && typeof countryCode === 'object') {
          $('select[name$=_country]', form).val(addressObject.countryCode.value);
        } else {
          $('select[name$=_country]', form).val(addressObject.countryCode);
        }
      }
      if (addressObject.phone !== '' && addressObject.phone !== null) {
        $('input[name$=_phone]', form).val(addressObject.phone);
      }

      $('input[name$=_isGift]', form).prop('checked', addressObject.isGift);
      $('textarea[name$=_giftMessage]', form).val(addressObject.isGift && addressObject.giftMessage ? addressObject.giftMessage : '');
    });
  }
  $('body').trigger('shipping:updateShippingAddressFormValues', { shipping: shipping });
};

base.methods.clearShippingForms = function (order) {
  order.shipping.forEach(function (shipping) {
    $('input[value=' + shipping.UUID + ']').each(function (formIndex, el) {
        var form = el.form;
        if (!form) return;

        $('input[name$=_firstName]', form).val('');
        $('input[name$=_lastName]', form).val('');
        $('input[name$=_address1]', form).val('');
        $('input[name$=_address2]', form).val('');
        $('input[name$=_city]', form).val('');
        $('input[name$=_postalCode]', form).val('');
        $('select[name$=_stateCode],input[name$=_stateCode]', form).val('');
        $('select[name$=_country]', form).val('');

        $('input[name$=_phone]', form).val('');

        $('input[name$=_isGift]', form).prop('checked', false);
        $('textarea[name$=_giftMessage]', form).val('');

        $(form).attr('data-address-mode', 'new');
        var addressSelectorDropDown = $('.addressSelector option[value=new]', form);
        $(addressSelectorDropDown).prop('selected', true);
    });
  });

  $('body').trigger('shipping:clearShippingForms', { order: order });
}

function appendCommaAfterCity($addressContainer, city) {
  if (
    $addressContainer.length > 0 &&
    $($addressContainer).find('.city').length > 0 &&
    $($addressContainer).find('.city').text() !== '' &&
    city &&
    city.length > 0 &&
    city !== ''
  ) {
    var updatedCity = city + ',';
    $($addressContainer).find('.city').text(updatedCity);
  }
}

/**
 * updates the order shipping summary for an order shipment model
 * @param {Object} shipping - the shipping (shipment model) model
 * @param {Object} order - the order model
 */
function updateShippingSummaryInformation(shipping, order) {
  $('[data-shipment-summary=' + shipping.UUID + ']').each(function (i, el) {
    var $container = $(el);
    var $shippingAddressLabel = $container.find('.shipping-addr-label');
    var $addressContainer = $container.find('.address-summary');
    var $shippingPhone = $container.find('.shipping-phone');
    var $methodTitle = $container.find('.shipping-method-title');
    var $methodArrivalTime = $container.find('.shipping-method-arrival-time');
    var $methodPrice = $container.find('.shipping-method-price');
    var $shippingSummaryLabel = $container.find('.shipping-method-label');
    var $summaryDetails = $container.find('.row.summary-details');
    var giftMessageSummary = $container.find('.gift-summary');
    var signatureReq = $container.find('.signature-is-required');
    var $shippingEmail = $container.find('.shipping-email');

    var address = shipping.shippingAddress;
    var selectedShippingMethod = shipping.selectedShippingMethod;
    var isGift = shipping.isGift;
    var signatureRequired = shipping.signatureRequired;
    var shipSignatureRequired = shipping.shipSignatureRequired;
    var shipSignatureThreshold = shipping.shipSignatureThreshold;

    addressHelpers.methods.populateAddressSummary($addressContainer, address);

    if (address && address.city) {
      appendCommaAfterCity($addressContainer, address.city);
    }

    if (address && address.phone) {
      $shippingPhone.text(address.phone);
    } else {
      $shippingPhone.empty();
    }
    if (address && address.email) {
      $shippingEmail.text(address.email);
    } else {
      $shippingEmail.empty();
    }

    if (selectedShippingMethod) {
      $('body').trigger('shipping:updateAddressLabelText', {
        selectedShippingMethod: selectedShippingMethod,
        resources: order.resources,
        shippingAddressLabel: $shippingAddressLabel
      });
      $shippingSummaryLabel.show();
      $summaryDetails.show();
      $methodTitle.text(selectedShippingMethod.displayName);
      if (selectedShippingMethod.estimatedArrivalTime) {
        $methodArrivalTime.text('( ' + selectedShippingMethod.estimatedArrivalTime + ' )');
      } else {
        $methodArrivalTime.empty();
      }
      $methodPrice.text(' - ' + selectedShippingMethod.finalShippingCost);
    }

    if (isGift) {
      //saks only change
      $('.saks-only.no-giftoptions').addClass('d-none');
      $container.find('.gift-message-recipientname.saks-only').closest('div').removeClass('d-none');

      giftMessageSummary.find('.gift-message-recipientname').text(shipping.giftRecipientName);
      giftMessageSummary.find('.gift-message-summary').text(shipping.giftMessage);
      if (shipping.giftWrapType) {
        if (shipping.giftWrapType === 'giftnote') {
          giftMessageSummary.find('.gift-note-only').removeClass('d-none');
          giftMessageSummary.find('.gift-wrap-only').addClass('d-none');
        } else if (shipping.giftWrapType === 'giftpack') {
          giftMessageSummary.find('.gift-note-only').addClass('d-none');
          giftMessageSummary.find('.gift-wrap-only').removeClass('d-none');
        }
      }
      giftMessageSummary.removeClass('d-none');
    } else {
      giftMessageSummary.addClass('d-none');
      //saks only change
      if ($('.saks-only.no-giftoptions').hasClass('show-go-step')) {
        $('.saks-only.no-giftoptions').removeClass('d-none');
      }
    }

    if (signatureRequired) {
      signatureReq.removeClass('d-none');
    } else {
      if (shipSignatureRequired && shipSignatureThreshold && order.totals.grandTotalValue > shipSignatureThreshold) {
        signatureReq.removeClass('d-none');
      } else {
        signatureReq.addClass('d-none');
      }
    }

    if (shipping.applicableShippingMethods && shipping.applicableShippingMethods.length > 0) {
      $('.no-shipping-method-msg').attr('data-has-methods', true);
    } else {
      $('.no-shipping-method-msg').attr('data-has-methods', false);
    }
  });

  $('body').trigger('shipping:updateShippingSummaryInformation', { shipping: shipping, order: order });
}

/**
 * Update the shipping UI for a single shipping info (shipment model)
 * @param {Object} shipping - the shipping (shipment model) model
 * @param {Object} order - the order/basket model
 * @param {Object} customer - the customer model
 * @param {Object} [options] - options for updating PLI summary info
 * @param {Object} [options.keepOpen] - if true, prevent changing PLI view mode to 'view'
 */
base.methods.updateShippingInformation = function (shipping, order, customer, options) {
  // First copy over shipmentUUIDs from response, to each PLI form
  order.shipping.forEach(function (aShipping) {
    aShipping.productLineItems.items.forEach(function (productLineItem) {
      base.methods.updateProductLineItemShipmentUUIDs(productLineItem, aShipping);
    });
  });

  // Now update shipping information, based on those associations
  base.methods.updateShippingMethods(shipping);

  base.methods.updateShippingAddressFormValues(shipping);
  updateShippingSummaryInformation(shipping, order);

  // And update the PLI-based summary information as well
  shipping.productLineItems.items.forEach(function (productLineItem) {
    base.methods.updateShippingAddressSelector(productLineItem, shipping, order, customer);
    base.methods.updatePLIShippingSummaryInformation(productLineItem, shipping, order, options);
  });

  $('body').trigger('shipping:updateShippingInformation', {
    order: order,
    shipping: shipping,
    customer: customer,
    options: options
  });
};
/**
 * Handle response from the server for valid or invalid form fields.
 * @param {Object} defer - the deferred object which will resolve on success or reject.
 * @param {Object} data - the response data with the invalid form fields or
 *  valid model data.
 */
base.methods.shippingFormResponse = function (defer, data) {
  var isMultiShip = $('#checkout-main').hasClass('multi-ship');
  var formSelector = isMultiShip ? '.multi-shipping .active form' : '.single-shipping form';

  // highlight fields with errors
  if (data.error) {
    if (data.fieldErrors && data.fieldErrors.length) {
      data.fieldErrors.forEach(function (error) {
        if (Object.keys(error).length) {
          formHelpers.loadFormErrors(formSelector, error);
        }
      });
      if (
        $('.data-checkout-stage').data('customer-type') === 'registered' &&
        $('form.shipping-form .customer-addresses-section:visible').length > 1 &&
        $('input[name="address"]:checked').closest('.customer-addresses-section').find('.edit-customer-address').length > 0
      ) {
        $('input[name="address"]:checked').closest('.customer-addresses-section').find('.edit-customer-address').trigger('click');
      }
      defer.reject(data);
    }

    if (data.serverErrors && data.serverErrors.length) {
      $.each(data.serverErrors, function (index, element) {
        createErrorNotification(element);
      });

      defer.reject(data);
    }

    if (data.cartError) {
      window.location.href = data.redirectUrl;
      defer.reject();
    } else if (data.vertexError) {
      $('.vertexError').removeClass('d-none');
      $('#vertex-suggestion').html('');
      data.vertexAddressSuggestions.forEach(function (item) {
        var aid = !item.key ? item.ID : item.key;
        $('#vertex-suggestion').append(
          $('<option></option>')
            .attr('value', aid)
            .text((item.displayValue ? item.displayValue : item) + ' ' + item.address1 + ' ' + item.city + ' ' + item.stateCode + ' ' + item.postalCode)
            .attr('data-address', JSON.stringify(item))
        );
      });
    }
  } else {
    // Populate the Address Summary
    // for addresses display view
    if ($('.data-checkout-stage').data('customer-type') === 'registered' && data.selectedAddress && data.createCustomerAddressHtml) {
      var selectedAddress = {};
      selectedAddress.shippingAddress = data.selectedAddress;
      $('body').trigger('checkout:updateAddressView', {
        customerAddressHtml: data.createCustomerAddressHtml,
        selectedAddress: data.selectedAddress
      });
    } else {
      $('body').trigger('checkout:updateCheckoutView', {
        order: data.order,
        customer: data.customer
      });
      scrollAnimate($('.payment-form'));
    }
    defer.resolve(data);
  }
};

/**
 * Handle Address suggestion for Vertax
 * @param {Object} address - address object
 * @param {Object} form - form data having address
 */
base.fillAddressSuggestion = function (address, form) {
  $('.btn-show-details').trigger('click');

  var url = $('#vertex-suggestions').data('suggest');
  $.ajax({
    url: url,
    type: 'post',
    dataType: 'json'
  });
  $(form).find('input[name$=_address1]').val(address.address1);
  if (address.address2) {
    $(form).find('input[name$=_address2]').val(address.address2);
  }
  $(form).find('input[name$=_city]').val(address.city);
  $(form).find('input[name$=_postalCode]').val(address.postalCode);
  $(form).find('select[name$=_stateCode],input[name$=_stateCode]').val(address.stateCode);
  $(form).find('select[name$=_country]').val(address.countryCode.toUpperCase());
};

/**
 * Clear out vertax Error
 */
base.clearVertexErrors = function () {
  $('.vertexError').addClass('d-none');
  $('.vertexSuggestions').addClass('d-none');
};

module.exports = {
  methods: base.methods,
  shippingTooltip: hbcTooltip.tooltipInit,

  selectShippingMethod: function () {
    $('.shipping-method-list').change(function () {
      var $shippingForm = $(this).parents('form');
      var methodID = $(':checked', this).val();
      var shipmentUUID = $shippingForm.find('[name=shipmentUUID]').val();
      var urlParams = addressHelpers.methods.getAddressFieldsFromUI($shippingForm);
      urlParams.shipmentUUID = shipmentUUID;
      urlParams.methodID = methodID;
      urlParams.isGift = $shippingForm.find('.gift').prop('checked');
      urlParams.giftMessage = $shippingForm.find('textarea[name$=_giftMessage]').val();
      urlParams.giftRecipientName = $shippingForm.find('input[name$=_giftRecipientName]').val();

      var url = $(this).data('select-shipping-method-url');
      selectShippingMethodAjax(url, urlParams, $(this));
    });
  },

  toggleMultiship: function () {
    var baseObj = this;

    $('input[name="usingMultiShipping"]').on('change', function () {
      var url = $('.multi-shipping-checkbox-block form').attr('action');
      var usingMultiShip = this.checked;

      $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: {
          usingMultiShip: usingMultiShip
        },
        success: function (response) {
          if (response.error) {
            window.location.href = response.redirectUrl;
          } else {
            $('body').trigger('checkout:updateCheckoutView', {
              order: response.order,
              customer: response.customer
            });

            if ($('#checkout-main').data('customer-type') === 'guest') {
              if (baseObj.methods && baseObj.methods.clearShippingForms) {
                baseObj.methods.clearShippingForms(response.order);
              }
              /** else {
                                                           clearShippingForms(response.order);
                                                       }*/
            } else {
              response.order.shipping.forEach(function (shipping) {
                $('input[value=' + shipping.UUID + ']').each(function (formIndex, el) {
                  var form = el.form;
                  if (!form) return;

                  $(form).attr('data-address-mode', 'edit');
                  var addressSelectorDropDown = $(form).find('.addressSelector option[value="ab_' + shipping.matchingAddressId + '"]');
                  $(addressSelectorDropDown).prop('selected', true);
                  $('input[name$=_isGift]', form).prop('checked', false);
                  $('textarea[name$=_giftMessage]', form).val('');
                  $('input[name$=_giftRecipientName]', form).val('');
                  $(form).find('.gift-message').addClass('d-none');
                  $(form).find('.gift-message-recipient').addClass('d-none');
                });
              });
            }

            if (usingMultiShip) {
              $('body').trigger('shipping:selectMultiShipping', {
                data: response
              });
            } else {
              $('body').trigger('shipping:selectSingleShipping', {
                data: response
              });
            }
          }

          $.spinner().stop();
        },
        error: function () {
          $.spinner().stop();
        }
      });
    });
  },

  selectSingleShipping: function () {
    $('body').on('shipping:selectSingleShipping', function () {
      $('.single-shipping .shipping-address').removeClass('d-none');
    });
  },

  selectMultiShipping: function () {
    var baseObj = this;

    $('body').on('shipping:selectMultiShipping', function (e, data) {
      $('.multi-shipping .shipping-address').addClass('d-none');

      data.data.order.shipping.forEach(function (shipping) {
        var element = $('.multi-shipping .card[data-shipment-uuid="' + shipping.UUID + '"]');

        if (shipping.shippingAddress) {
          if (baseObj.methods && baseObj.methods.viewMultishipAddress) {
            baseObj.methods.viewMultishipAddress($(element));
          }
          /** else {
                          viewMultishipAddress($(element));
                    } */
        } else {
          /* eslint-disable no-lonely-if */
          if (baseObj.methods && baseObj.methods.enterMultishipView) {
            baseObj.methods.enterMultishipView($(element));
          }
          /** else {
                        enterMultishipView($(element));
                    }*/
          /* eslint-enable no-lonely-if */
        }
      });
    });
  },

  selectSingleShipAddress: function () {
    $('.single-shipping .addressSelector').on('change', function () {
      var form = $(this).parents('form')[0];
      var selectedOption = $('option:selected', this);
      var attrs = selectedOption.data();
      var shipmentUUID = selectedOption[0].value;
      var originalUUID = $('input[name=shipmentUUID]', form).val();
      var element;

      Object.keys(attrs).forEach(function (attr) {
        element = attr === 'countryCode' ? 'country' : attr;
        $('[name$=' + element + ']', form).val(attrs[attr]);
      });

      $('[name$=stateCode]', form).trigger('change');

      if (shipmentUUID === 'new') {
        $(form).attr('data-address-mode', 'new');
      } else if (shipmentUUID === originalUUID) {
        $(form).attr('data-address-mode', 'shipment');
      } else if (shipmentUUID.indexOf('ab_') === 0) {
        $(form).attr('data-address-mode', 'customer');
      } else {
        $(form).attr('data-address-mode', 'edit');
      }
    });
  },

  selectMultiShipAddress: function () {
    var baseObj = this;

    $('.multi-shipping .addressSelector').on('change', function () {
      var form = $(this).closest('form');
      var selectedOption = $('option:selected', this);
      var attrs = selectedOption.data();
      var shipmentUUID = selectedOption[0].value;
      var originalUUID = $('input[name=shipmentUUID]', form).val();
      var pliUUID = $('input[name=productLineItemUUID]', form).val();
      var createNewShipmentScoped = baseObj.methods.createNewShipment;

      var element;
      Object.keys(attrs).forEach(function (attr) {
        if (attr === 'isGift') {
          $('[name$=' + attr + ']', form).prop('checked', attrs[attr]);
          $('[name$=' + attr + ']', form).trigger('change');
        } else {
          element = attr === 'countryCode' ? 'country' : attr;
          $('[name$=' + element + ']', form).val(attrs[attr]);
        }
      });

      if (shipmentUUID === 'new' && pliUUID) {
        var createShipmentUrl = $(this).attr('data-create-shipment-url');
        createNewShipmentScoped(createShipmentUrl, {
          productLineItemUUID: pliUUID
        })
          .done(function (response) {
            $.spinner().stop();
            if (response.error) {
              if (response.redirectUrl) {
                window.location.href = response.redirectUrl;
              }
              return;
            }

            $('body').trigger('checkout:updateCheckoutView', {
              order: response.order,
              customer: response.customer,
              options: {
                keepOpen: true
              }
            });

            $(form).attr('data-address-mode', 'new');
          })
          .fail(function () {
            $.spinner().stop();
          });
      } else if (shipmentUUID === originalUUID) {
        $('select[name$=stateCode]', form).trigger('change');
        $(form).attr('data-address-mode', 'shipment');
      } else if (shipmentUUID.indexOf('ab_') === 0) {
        var url = $(form).attr('action');
        var serializedData = $(form).serialize();
        createNewShipmentScoped(url, serializedData)
          .done(function (response) {
            $.spinner().stop();
            if (response.error) {
              if (response.redirectUrl) {
                window.location.href = response.redirectUrl;
              }
              return;
            }

            $('body').trigger('checkout:updateCheckoutView', {
              order: response.order,
              customer: response.customer,
              options: {
                keepOpen: true
              }
            });

            $(form).attr('data-address-mode', 'customer');
            var $rootEl = $(form).closest('.shipping-content');
            baseObj.methods.editMultiShipAddress($rootEl);
          })
          .fail(function () {
            $.spinner().stop();
          });
      } else {
        var updatePLIShipmentUrl = $(form).attr('action');
        var serializedAddress = $(form).serialize();
        createNewShipmentScoped(updatePLIShipmentUrl, serializedAddress)
          .done(function (response) {
            $.spinner().stop();
            if (response.error) {
              if (response.redirectUrl) {
                window.location.href = response.redirectUrl;
              }
              return;
            }

            $('body').trigger('checkout:updateCheckoutView', {
              order: response.order,
              customer: response.customer,
              options: {
                keepOpen: true
              }
            });

            $(form).attr('data-address-mode', 'edit');
          })
          .fail(function () {
            $.spinner().stop();
          });
      }
    });
  },

  updateShippingList: function () {
    var baseObj = this;

    /** $('select[name$="shippingAddress_addressFields_states_stateCode"]')
            .on('change', function (e) {
                if (baseObj.methods && baseObj.methods.updateShippingMethodList) {
                    baseObj.methods.updateShippingMethodList($(e.currentTarget.form));
                }
        }); */

    $('input[name$="shippingAddress_addressFields_postalCode"]').on('blur', function (e) {
      if ($(this).val() !== '') {
        updateShippingMethodList($(e.currentTarget.form));
      }
    });
  },

  updateDataAddressMode: function () {
    $('body').on('shipping:updateDataAddressMode', function (e, data) {
      $(data.form).attr('data-address-mode', data.mode);
    });
  },

  enterMultiShipInfo: function () {
    var baseObj = this;

    $('.btn-enter-multi-ship').on('click', function (e) {
      e.preventDefault();

      if (baseObj.methods && baseObj.methods.editOrEnterMultiShipInfo) {
        baseObj.methods.editOrEnterMultiShipInfo($(this), 'new');
      }
      /** else {
                editOrEnterMultiShipInfo($(this), 'new');
            }*/
    });
  },

  editMultiShipInfo: function () {
    var baseObj = this;

    $('.btn-edit-multi-ship').on('click', function (e) {
      e.preventDefault();

      if (baseObj.methods && baseObj.methods.editOrEnterMultiShipInfo) {
        baseObj.methods.editOrEnterMultiShipInfo($(this), 'edit');
      }
      /** else {
                 editOrEnterMultiShipInfo($(this), 'edit');
           }*/
    });
  },

  saveMultiShipInfo: function () {
    var baseObj = this;

    $('.btn-save-multi-ship').on('click', function (e) {
      e.preventDefault();

      // Save address to checkoutAddressBook
      var form = $(this).closest('form');
      var $rootEl = $(this).closest('.shipping-content');
      var data = $(form).serialize();
      var url = $(form).attr('action');

      $rootEl.spinner().start();
      $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: data
      })
        .done(function (response) {
          formHelpers.clearPreviousErrors(form);
          if (response.error) {
            if (response.fieldErrors && response.fieldErrors.length) {
              response.fieldErrors.forEach(function (error) {
                if (Object.keys(error).length) {
                  formHelpers.loadFormErrors(form, error);
                }
              });
            } else if (response.serverErrors && response.serverErrors.length) {
              $.each(response.serverErrors, function (index, element) {
                createErrorNotification(element);
              });
            }
          } else {
            // Update UI from response
            $('body').trigger('checkout:updateCheckoutView', {
              order: response.order,
              customer: response.customer
            });

            if (baseObj.methods && baseObj.methods.viewMultishipAddress) {
              baseObj.methods.viewMultishipAddress($rootEl);
            }
            /** else {
                          viewMultishipAddress($rootEl);
                        }*/
          }

          if (response.order && response.order.shippable) {
            $('button.submit-shipping').attr('disabled', null);
          }

          $rootEl.spinner().stop();
        })
        .fail(function (err) {
          if (err.responseJSON.redirectUrl) {
            window.location.href = err.responseJSON.redirectUrl;
          }

          $rootEl.spinner().stop();
        });

      return false;
    });
  },

  cancelMultiShipAddress: function () {
    var baseObj = this;

    $('.btn-cancel-multi-ship-address').on('click', function (e) {
      e.preventDefault();

      var form = $(this).closest('form');
      var $rootEl = $(this).closest('.shipping-content');
      var restoreState = $rootEl.data('saved-state');

      // Should clear out changes / restore previous state
      if (restoreState) {
        var restoreStateObj = JSON.parse(restoreState);
        var originalStateCode = restoreStateObj.shippingAddress.stateCode;
        var stateCode = $('[name$=_stateCode]', form).val();

        if (baseObj.methods && baseObj.methods.updateShippingAddressFormValues) {
          baseObj.methods.updateShippingAddressFormValues(restoreStateObj);
        }
        /** else {
                  updateShippingAddressFormValues(restoreStateObj);
               }*/

        if (stateCode !== originalStateCode) {
          $('[data-action=save]', form).trigger('click');
        } else {
          $(form).attr('data-address-mode', 'edit');
          if (baseObj.methods && baseObj.methods.editMultiShipAddress) {
            baseObj.methods.editMultiShipAddress($rootEl);
          }
          /** else {
                        editMultiShipAddress($rootEl);
                    }*/
        }
      }

      return false;
    });
  },

  isGift: function () {
    $('.gift').on('change', function (e) {
      e.preventDefault();
      var form = $(this).closest('form');

      if (this.checked) {
        $(form).find('.gift-message').removeClass('d-none');
        $(form).find('.gift-option-container').removeClass('d-none');
        $(form).find('.gift-message-recipient').removeClass('d-none');
        $(form).find('input[name$=_shippingAddress_isGift]').attr('checked', true);
        $(form).find('input[name*=_giftRecipientName]').parents('div .form-group').addClass('required');
        $(form).find('input[name*=_giftRecipientName]').prop('required', true);
        clientSideValidation.checkValidationOnAjax($('.shipping-form'), true, true);
      } else {
        $(form).find('input[name$=_shippingAddress_isGift]').removeAttr('checked');
        $(form).find('.gift-option-container').addClass('d-none');
        $(form).find('.gift-message').addClass('d-none');
        $(form).find('.gift-message-recipient').addClass('d-none');
        $(form).find('.gift-message').val('');
        $(form).find('.gift-message-recipient').val('');
        $(form).find('input[name*=_gift-message]').parents('div .form-group').removeClass('required');
        $(form).find('input[name*=_gift-message]').prop('required', false);
        clientSideValidation.checkValidationOnAjax($('.shipping-form'), true, true);
      }
    });
  },

  vertexSuggestions: function () {
    $('select[name="vertex-suggestion"]').change(function () {
      var form = $(this).closest('.card').find('.shipping-form');
      base.fillAddressSuggestion($(this).find('option:selected').data('address'), form);
      // need to remember as is address2 so we will write it
      var address2AsIs = '';
      if (form.find('#shippingAddressTwo').val() !== '') {
        address2AsIs = form.find('#shippingAddressTwo').val();
      }
      var selectedAddress = $('select[name="vertex-suggestion"]').val();
      // if address was picked from service we blank address 2 line
      if (selectedAddress === 'Use Address As-Is') {
        $('#shippingAddressTwo').val(address2AsIs);
      } else {
        $('#shippingAddressTwo').val('');
      }
    });
  },
  // updates address view in shipping
  updateAddressView: function () {
    $('body').on('checkout:updateAddressView', function (e, data) {
      updateShippingAddressInformation(data.customerAddressHtml);
      base.methods.updateShippingAddressFormValues(data.selectedAddress);
      toggleShippingItems('address');
      var addressInfo = $('input[name="address"]:checked').data('address-info');
      if (addressInfo) {
        $('input[name$=_shippingAddress_addressFields_address2]', 'form.shipping-form').val(addressInfo.address2);
        $('input[name$=_shippingAddress_email]', 'form.shipping-form').val(addressInfo.email);
        // eslint-disable-next-line no-undef
        if (defaultShippingCountry && addressInfo.countryCode && addressInfo.countryCode.value !== defaultShippingCountry && $('.coutry-rest-msg').length > 0) {
          $('.coutry-rest-msg').removeClass('d-none');
          // disable address edit for non candaian address
          if ($('input[name="address"]:checked').closest('.customer-addresses-section').find('.edit-customer-address').length > 0) {
            $('input[name="address"]:checked').closest('.customer-addresses-section').find('.edit-customer-address').addClass('no-mouse-events');
          }
          // $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
        } else {
          $('.coutry-rest-msg').addClass('d-none');
        }
        if ($('.gift-message-block.gift-block').length > 0 && !$('.gift-message-block.gift-block').hasClass('hidden-global')) {
          $('.gift-message-block.gift-block').removeClass('d-none');
        }
      }
    });
  },
  // toggle new address view
  addNewAddressRegistered: function () {
    var baseObj = this;

    $(document).on('click', 'form.shipping-form .address-add-registered .customer-addresses-section', function () {
      var shipmentUUID = $('input[name="shipmentUUID"]').val();
      var form = $('form.shipping-form');
      var addressSubmit = form.data('address-submit');
      var shippingArray = [];
      var shipping = {};
      var order = {};
      if (shipmentUUID) {
        shipping.UUID = shipmentUUID;
        shippingArray.push(shipping);
        order.shipping = shippingArray;
        baseObj.methods.clearShippingForms(order);
        // set default address unchecked
        // eslint-disable-next-line no-unused-vars
        $('input[value=' + shipping.UUID + ']').each(function (formIndex, el) {
          $('input[name$=_setAsDefault]', form).prop('checked', false);
        });
      }
      if (addressSubmit) {
        form.attr('action', addressSubmit);
      }
      $('.shipping-method-block').addClass('d-none');
      $('.gift-message-block').addClass('d-none');
      $(this).closest('.address-add-registered').addClass('d-none');
      $('.shipping-content').find('div.shipping-next-step-button-row').removeClass('d-none');
      $('.shipping-content').find('div.shipping-next-step-button-row:not(.shipping-save-address)').addClass('d-none');
      $('.shipping-address-block').removeClass('d-none');
      $('#customer-addresses').addClass('d-none');
      $('.coutry-rest-msg').addClass('d-none');
      $('.signature-required').addClass('d-none');
      $('.signature-checkbox').addClass('d-none');
      floatLabel.resetFloatLabel();
      clientSideValidation.checkValidationOnAjax($('.shipping-form'), true);
    });
  },
  // edit address on the addresses in shipping
  edditAddress: function () {
    $(document).on('click', '.edit-customer-address', function (e) {
      e.preventDefault();
      var shippingAddress = {};
      var shipmentUUID = $('[name=shipmentUUID]').val();
      var address = {};
      var addressUrl = $(this).closest('.address-result').data('edit-url');
      var addressId = $(this).closest('.address-result').data('addressid');
      if (addressId && addressUrl) {
        var addressInfo = $('input[id="input-' + addressId + '"]').data('address-info');
        if (addressInfo && shipmentUUID) {
          address.UUID = shipmentUUID;
          shippingAddress = addressInfo;
          address.shippingAddress = shippingAddress;
          base.methods.updateShippingAddressFormValues(address);
          $('input[name$=_shippingAddress_addressFields_address2]', 'form.shipping-form').val(addressInfo.address2);
          if (addressInfo.email) {
            $('input[name$=_shippingAddress_email]', 'form.shipping-form').val(addressInfo.email);
          } else if (
            $(this)
              .closest('input[id="input-' + addressId + '"]')
              .data('customer-email')
          ) {
            $('input[name$=_shippingAddress_email]', 'form.shipping-form').val(
              $(this)
                .closest('input[id="input-' + addressId + '"]')
                .data('customer-email')
            );
          }
          if ($('input[id="input-' + addressId + '"]').attr('data-default-address') === 'true') {
            $('input[name$=_shippingAddress_addressFields_setAsDefault]', 'form.shipping-form').prop('checked', true);
          } else {
            $('input[name$=_shippingAddress_addressFields_setAsDefault]', 'form.shipping-form').prop('checked', false);
          }
        }
        $('.shipping-method-block').addClass('d-none');
        toggleShippingItems();
        $('form.shipping-form').attr('action', addressUrl);
        setTimeout(function () {
          floatLabel.resetFloatLabel();
          clientSideValidation.checkValidationOnAjax($('.shipping-form'));
        }, 200);
      }
    });
  },
  // slelect addresses form the list with radio
  selectAddress: function () {
    var baseObj = this;
    $(document).on('click', '.shipping-content .form-check.customer-addresses-section', function (e) {
      e.preventDefault();
      var shippingAddress = {};
      var shipmentUUID = $('[name=shipmentUUID]').val();
      var address = {};
      var addressInfo = $(this).find('input[name=address]').data('address-info');

      // set invalidShipping Country if unable to ship there
      var invalidShippingCountry = false;
      if (defaultShippingCountry && addressInfo.countryCode && addressInfo.countryCode.value && addressInfo.countryCode.value !== defaultShippingCountry) {
        invalidShippingCountry = true;
      }

      $('.shipping-content .form-check.customer-addresses-section').removeClass('selected');
      $(this).addClass('selected');
      $(this).find('input[name="address"]').prop('checked', true);
      if (addressInfo && shipmentUUID) {
        address.UUID = shipmentUUID;
        shippingAddress = addressInfo;
        address.shippingAddress = shippingAddress;
        base.methods.updateShippingAddressFormValues(address);

        if (!invalidShippingCountry) {
          updateShippingMethodList($('form.shipping-form'));
        }

        $('input[name$=_shippingAddress_addressFields_address2]', 'form.shipping-form').val(addressInfo.address2);
        if (addressInfo.email) {
          $('input[name$=_shippingAddress_email]', 'form.shipping-form').val(addressInfo.email);
        } else if ($(this).find('input[name=address]').data('customer-email')) {
          $('input[name$=_shippingAddress_email]', 'form.shipping-form').val($(this).find('input[name=address]').data('customer-email'));
        }
        // restricted state message
        checkRestrictedState(addressInfo.stateCode);
        // po message
        if (addressInfo.address1 && addressInfo.address1.length > 0) {
          poCheck(addressInfo.address1.toLowerCase());
        }
        // fpo message
        if (addressInfo.city && addressInfo.city.length > 0) {
          fpoCheck(addressInfo.city.toLowerCase());
        }
        var $shippingMethods = $('.shipping-content .shipping-method-list').find('input');
        // eslint-disable-next-line no-undef
        if (invalidShippingCountry && $('.coutry-rest-msg').length > 0) {
          $('.coutry-rest-msg').removeClass('d-none');
          // disable address edit for non canadian address
          if ($(this).find('input[name=address]').closest('.customer-addresses-section').find('.edit-customer-address').length > 0) {
            $(this).find('input[name=address]').closest('.customer-addresses-section').find('.edit-customer-address').addClass('no-mouse-events');
          }
          $shippingMethods.each(function () {
            $(this).attr('disabled', 'disabled');
          });
          $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
        } else {
          $('.coutry-rest-msg').addClass('d-none');
          $shippingMethods.each(function () {
            $(this).removeAttr('disabled');
          });
          $('.next-step-button:visible').find('button[type="submit"]').removeAttr('disabled');
        }
        if ($('.no-shipping-method-msg').attr('data-has-methods') != undefined && $('.no-shipping-method-msg').attr('data-has-methods') === 'true') {
          $('.no-shipping-method-msg').addClass('d-none');
          $('.next-step-button:visible').find('button[type="submit"]').removeAttr('disabled');
        } else if ($('.no-shipping-method-msg').attr('data-has-methods') != undefined && $('.no-shipping-method-msg').attr('data-has-methods') === 'false') {
          $('.no-shipping-method-msg').removeClass('d-none');
          $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
        }
      }
    });
  },
  // cancle addres save in add address modal
  cancelAddressSave: function () {
    $(document).on('click', '.shipping-next-cancel', function (e) {
      e.preventDefault();
      toggleShippingItems('address');
      if ($('input[name="address"]:checked').length > 0) {
        $('input[name="address"]:checked').trigger('click');
      }
    });
  },
  poAddressCheck: function () {
    $('.shippingAddressOne').on('keyup input change', function () {
      var data = $(this).val().toLowerCase();
      poCheck(data);
      /*            if ($('.shipping-address-block').data('poenabled')) {
                var reGroups = ['^ *((#\\d+))',
                    '((box|bin)[-. \\/\\\\]?\\d+)',
                    '(.*p[ \\.]? ?(o|0)[-. \\/\\\\]? *-?((box|bin)|b|(#|num)?\\d+))',
                    '(p(ost)? *(o(ff(ice)?)?)? *((box|bin)|b)? *\\d+)',
                    '(p *-?\\/?(o)? *-?box)',
                    'post office box',
                    '(((box|bin)|b) *(number|num|#)? *\\d+)$',
                    '(\\d+ *(box|post box|post office box)$)',
                    '(((num|number|#) *\\d+)$)'];
                for (var i = 0; i < reGroups.length; i++) {
                    var pattern = new RegExp(reGroups[i]);
                    var poResult = pattern.test(data);
                    if (poResult) {
                        $('.po-check-message').removeClass('d-none');
                        //$('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
                        break;
                    } else {
                        $('.po-check-message').addClass('d-none');
                    }
                }
            }*/
    });
  },

  fpoAddressCheck: function () {
    $('.shippingAddressCity').on('keyup input change blur', function () {
      var data = $(this).val().toLowerCase();
      fpoCheck(data);
    });
  },

  restrictedStateCheck: function () {
    $('.shippingState').on('keyup input change', function () {
      var data = $(this).val();
      // restricted state message
      checkRestrictedState(data);
      /*            var restrictedStates = $('.shipping-address-block').data('restricted-sates');
            if (restrictedStates && restrictedStates !== null && restrictedStates.length > 0) {
                if (restrictedStates.indexOf(data.toLowerCase()) >= 0 || restrictedStates.indexOf(data.toUpperCase()) >= 0) {
                    $('.restricted-state-message').removeClass('d-none');
                    //$('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
                } else {
                    $('.restricted-state-message').addClass('d-none');
                }
            }*/
    });
  },
  validateBopisForm: function () {
    if ($('.pickup-form:visible').length > 0) {
      clientSideValidation.checkValidationOnAjax($('.pickup-form'), true, true);
    }
  },
  // update shipping method if has at least one saved address
  updateShippingMethodsOnLoad: function () {
    var baseObj = this;
    // set invalidShipping Country if unable to ship there
    var $addressSection = $('.shipping-content .form-check.customer-addresses-section.selected');
    var $addressInfo = $addressSection.find('input[name=address]').data('address-info');
    var $shippingMethods = $('.shipping-content .shipping-method-list').find('input');

    var invalidShippingCountry = false;
    if (
      defaultShippingCountry &&
      !!$addressInfo &&
      $addressInfo.countryCode &&
      $addressInfo.countryCode.value &&
      $addressInfo.countryCode.value !== defaultShippingCountry
    ) {
      invalidShippingCountry = true;
    }
    if (
      !invalidShippingCountry &&
      $('.data-checkout-stage').data('customer-type') === 'registered' &&
      $('form.shipping-form .customer-addresses-section:visible').length > 1
    ) {
      updateShippingMethodList($('form.shipping-form'));
    }
    if (invalidShippingCountry && $('.coutry-rest-msg').length > 0) {
      $('.coutry-rest-msg').removeClass('d-none');
      // disable address edit for non canadian address
      if ($addressSection.find('input[name=address]').closest('.customer-addresses-section').find('.edit-customer-address').length > 0) {
        $addressSection.find('input[name=address]').closest('.customer-addresses-section').find('.edit-customer-address').addClass('no-mouse-events');
      }
      $shippingMethods.each(function () {
        $(this).attr('disabled', 'disabled');
      });
      $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
    } else {
      $('.coutry-rest-msg').addClass('d-none');
      $shippingMethods.each(function () {
        $(this).removeAttr('disabled');
      });
      $('.next-step-button:visible').find('button[type="submit"]').removeAttr('disabled');
    }
    if ($('.no-shipping-method-msg').attr('data-has-methods') != undefined && $('.no-shipping-method-msg').attr('data-has-methods') === 'true') {
      $('.no-shipping-method-msg').addClass('d-none');
      $('.next-step-button:visible').find('button[type="submit"]').removeAttr('disabled');
    } else if ($('.no-shipping-method-msg').attr('data-has-methods') != undefined && $('.no-shipping-method-msg').attr('data-has-methods') === 'false') {
      $('.no-shipping-method-msg').removeClass('d-none');
      $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
    }
  },
  validateShippingForm: function () {
    if ($('.shipping-form:visible').length > 0) {
      clientSideValidation.checkValidationOnAjax($('.shipping-form'), true);
      // show error message on non-canadian address entry on shipping form
      var addressInfo = $('input[name="address"]:checked').data('address-info');
      if (addressInfo) {
        // eslint-disable-next-line no-undef
        if (defaultShippingCountry && addressInfo.countryCode && addressInfo.countryCode.value !== defaultShippingCountry && $('.coutry-rest-msg').length > 0) {
          $('.coutry-rest-msg').removeClass('d-none');
          // $('.next-step-button:visible').find('button[type="submit"]').attr('disabled', 'disabled');
          // disable address edit for non candaian address
          if ($('input[name="address"]:checked').closest('.customer-addresses-section').find('.edit-customer-address').length > 0) {
            $('input[name="address"]:checked').closest('.customer-addresses-section').find('.edit-customer-address').addClass('no-mouse-events');
          }
        } else {
          $('.coutry-rest-msg').addClass('d-none');
        }
      }
    }
  },
  openAddressEntry: function () {
    if ($('#customer-addresses:visible').length > 0) {
      // open address entry modal if user has one and only us address in address book
      if (
        $('#address-auto-open').length > 0 &&
        ($('#address-auto-open').attr('data-open-adressentry') === 'true' ||
          ($('#address-auto-open').attr('data-firstadrress-country') !== undefined &&
            $('#address-auto-open').attr('data-firstadrress-country') !== '' &&
            $('#address-auto-open').attr('data-firstadrress-country') !== 'US')) &&
        $('form.shipping-form').find('.address-add-registered .customer-addresses-section').length > 0
      ) {
        $('form.shipping-form').find('.address-add-registered .customer-addresses-section').trigger('click');
      }
    }
  },

  updateShippingInformation: function () {
    $('body').on('shipping:updateShippingInformation', function (e, data) {
      var shipping = data.shipping;
      var srToken = cookiesUtil.getCookie('sr_token');
      if (shipping) {
        var shippingMethods = shipping.applicableShippingMethods;
        $.each(shippingMethods, function (methodIndex, shippingMethod) {
          if (shippingMethod.ID === 'shoprunner' && $('.checkout-shop-runner').length > 0) {
            var shippingLabel = $('label.shipping-method-option');
            $.each(shippingLabel, function (labelIndex, shipMethodLabel) {
              if ($(shipMethodLabel).attr('for').indexOf('shippingMethod-shoprunner') > -1 && (srToken === null || srToken === undefined || srToken === '')) {
                $(shipMethodLabel).closest('.col-12').remove();
              }
            });
          }
        });
      }
    });
  }
};
