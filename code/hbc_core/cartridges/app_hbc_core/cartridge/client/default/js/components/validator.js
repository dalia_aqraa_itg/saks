'use strict';

module.exports = {
  updatePostalCodePattern: function (form, countryCode) {
    var postal = {
      US: '(^\\d{5}(-\\d{4})?$)',
      CA: '(^[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY]{1}\\d{1}[A-Za-z]{1} *\\d{1}[A-Za-z]{1}\\d{1}$)'
    };

    if ($(form) !== undefined && $(form).find('.validateZipCode').length > 0) {
      $(form).find('.validateZipCode').val('').removeAttr('pattern');
    }

    if (postal[countryCode]) {
      if ($(form) !== undefined && $(form).find('.validateZipCode').length > 0) {
        $(form).find('.validateZipCode').val('').attr('pattern', postal[countryCode]);
      }
    }
  }
};
