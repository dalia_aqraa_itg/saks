'use strict';

function isEmpty(obj) {
  for (var i in obj) {
    return false;
  }
  return true;
}
/**
 * Updates tax totals
 *
 * @param {Object} totals - Tax totals Object
 */
function updateTaxTotal(totals) {
  if (totals.canadaTaxation && totals.canadaTaxation.PST) {
    $('.tax-PST-total').empty().append(totals.canadaTaxation.PST);
    $('.tax-pst').removeClass('d-none');
  } else if (!$('.tax-pst').hasClass('d-none')) {
    $('.tax-pst').addClass('d-none');
  }

  if (totals.canadaTaxation && totals.canadaTaxation['GST/HST']) {
    $('.tax-GST-total').empty().append(totals.canadaTaxation['GST/HST']);
    $('.tax-gst').removeClass('d-none');
  } else if (!$('.tax-gst').hasClass('d-none')) {
    $('.tax-gst').addClass('d-none');
  }

  if (totals.canadaTaxation && totals.canadaTaxation.QST) {
    $('.tax-QST-total').empty().append(totals.canadaTaxation.QST);
    $('.tax-qst').removeClass('d-none');
  } else if (!$('.tax-qst').hasClass('d-none')) {
    $('.tax-qst').addClass('d-none');
  }

  if (totals.canadaTaxation && totals.canadaTaxation.RST) {
    $('.tax-RST-total').empty().append(totals.canadaTaxation.RST);
    $('.tax-rst').removeClass('d-none');
  } else if (!$('.tax-rst').hasClass('d-none')) {
    $('.tax-rst').addClass('d-none');
  }

  if (totals.canadaTaxation && totals.canadaTaxation.ECO) {
    $('.tax-ECO-total').empty().append(totals.canadaTaxation.ECO);
    $('.tax-eco').removeClass('d-none');
  } else if (!$('.tax-eco').hasClass('d-none')) {
    $('.tax-eco').addClass('d-none');
  }

  if (isEmpty(totals.canadaTaxation)) {
    $('.tax-total').empty().append(totals.totalTax);
    $('.tax-normal').removeClass('d-none');
  } else if (!$('.tax-normal').hasClass('d-none')) {
    $('.tax-normal').addClass('d-none');
  }
}
/**
 * updates the totals summary
 * @param {Array} totals - the totals data
 */
function updateTotals(totals, order) {
  $('.shipping-total-cost').text(totals.totalShippingCost);
  updateTaxTotal(totals);
  // $('.tax-total').text(totals.totalTax);
  $('.sub-total').text(totals.subTotal);
  $('.grand-total-sum').text(totals.grandTotal);

  if (order.giftWrap && order.giftWrap.applied) {
    $('.gift-wrap-item').removeClass('d-none');
    $('.gift-wrap-cost').text(order.giftWrap.charge);
  } else {
    $('.gift-wrap-item').addClass('d-none');
  }

  if (totals.orderLevelDiscountTotal.value > 0) {
    $('.order-discount').removeClass('hide-order-discount');
    $('.order-discount-total').text('- ' + totals.orderLevelDiscountTotal.formatted);
  } else {
    $('.order-discount').addClass('hide-order-discount');
  }

  if (totals.shippingLevelDiscountTotal.value > 0) {
    $('.shipping-total-cost').empty().append(totals.freeShippingText);
  }
  if (totals.totalSavings.value > 0) {
    $('.grand-total-saving-container').removeClass('d-none');
    $('.checkout-total-savings').empty().append(totals.totalSavings.formatted);
  } else {
    $('.grand-total-saving-container').addClass('d-none');
  }

  $('.coupons-and-promos').empty().append(totals.discountsHtml);

  /** Toggle the Associate, First Day or SaksFirst member discount message. */
  if (totals.associateOrFDDMsg !== '' || totals.freeShippingMsg !== '') {
    $('.associate-fdd-promo').removeClass('d-none');
    $('.associate-promo-msg')
      .empty()
      .append(totals.associateOrFDDMsg ? totals.associateOrFDDMsg : totals.freeShippingMsg);
  } else {
    $('.associate-fdd-promo').addClass('d-none');
  }
}

/**
 * updates the order product shipping summary for an order model
 * @param {Object} order - the order model
 */
function updateOrderProductSummaryInformation(order) {
  var $productSummary = $('<div />');
  order.shipping.forEach(function (shipping) {
    shipping.productLineItems.items.forEach(function (lineItem) {
      var pli = $($('[data-product-line-item=' + lineItem.UUID + ']')[0]);
      $productSummary.append(pli);
    });

    var address = shipping.shippingAddress || {};
    var selectedMethod = shipping.selectedShippingMethod;

    var nameLine = address.firstName ? address.firstName + ' ' : '';
    if (address.lastName) nameLine += address.lastName;

    var address1Line = address.address1;
    var address2Line = address.address2;

    var phoneLine = address.phone;

    var shippingCost = selectedMethod ? selectedMethod.shippingCost : '';
    var methodNameLine = selectedMethod ? selectedMethod.displayName : '';
    var methodArrivalTime = selectedMethod && selectedMethod.estimatedArrivalTime ? '( ' + selectedMethod.estimatedArrivalTime + ' )' : '';

    var tmpl = $('#pli-shipping-summary-template').clone();

    if (shipping.productLineItems.items && shipping.productLineItems.items.length > 1) {
      $('h5 > span').text(' - ' + shipping.productLineItems.items.length + ' ' + order.resources.items);
    } else {
      $('h5 > span').text('');
    }

    var stateRequiredAttr = $('#shippingState').attr('required');
    var isRequired = stateRequiredAttr !== undefined && stateRequiredAttr !== false;
    var stateExists = shipping.shippingAddress && shipping.shippingAddress.stateCode ? shipping.shippingAddress.stateCode : false;
    var stateBoolean = false;
    if ((isRequired && stateExists) || !isRequired) {
      stateBoolean = true;
    }

    var shippingForm = $('.multi-shipping input[name="shipmentUUID"][value="' + shipping.UUID + '"]').parent();

    if (
      shipping.shippingAddress &&
      shipping.shippingAddress.firstName &&
      shipping.shippingAddress.address1 &&
      shipping.shippingAddress.city &&
      stateBoolean &&
      shipping.shippingAddress.countryCode &&
      (shipping.shippingAddress.phone || shipping.productLineItems.items[0].fromStoreId)
    ) {
      $('.ship-to-name', tmpl).text(nameLine);
      $('.ship-to-address1', tmpl).text(address1Line);
      $('.ship-to-address2', tmpl).text(address2Line);
      $('.ship-to-city', tmpl).text(address.city);
      if (address.stateCode) {
        $('.ship-to-st', tmpl).text(address.stateCode);
      }
      $('.ship-to-zip', tmpl).text(address.postalCode);
      $('.ship-to-phone', tmpl).text(phoneLine);

      if (!address2Line) {
        $('.ship-to-address2', tmpl).hide();
      }

      if (!phoneLine) {
        $('.ship-to-phone', tmpl).hide();
      }

      shippingForm.find('.ship-to-message').text('');
    } else {
      shippingForm.find('.ship-to-message').text(order.resources.addressIncomplete);
    }

    if (shipping.isGift) {
      $('.gift-message-recipientname', tmpl).text(shipping.giftRecipientName);
      $('.gift-message-summary', tmpl).text(shipping.giftMessage);
    } else {
      $('.gift-summary', tmpl).addClass('d-none');
    }

    // checking h5 title shipping to or pickup
    var $shippingAddressLabel = $('.shipping-header-text', tmpl);
    $('body').trigger('shipping:updateAddressLabelText', {
      selectedShippingMethod: selectedMethod,
      resources: order.resources,
      shippingAddressLabel: $shippingAddressLabel
    });

    if (shipping.selectedShippingMethod) {
      $('.display-name', tmpl).text(methodNameLine);
      $('.arrival-time', tmpl).text(methodArrivalTime);
      $('.price', tmpl).text(shippingCost);
    }

    var $shippingSummary = $('<div class="multi-shipping" data-shipment-summary="' + shipping.UUID + '" />');
    $shippingSummary.html(tmpl.html());
    $productSummary.append($shippingSummary);
  });

  $('.product-summary-block').html($productSummary.html());
}

/**
 * updates the discount information for each line item in the order summary section
 * @param {Object} order - the order model
 */
function updateProductPromotionsSummary(order) {
  order.items.forEach(function (item) {
    if (item.checkoutDiscountTotalHtml) {
      $('.checkout_discount-' + item.UUID).html(item.checkoutDiscountTotalHtml);
    }
  });
}

/**
 * Updates the promo code count in the header of the Coupon Section
 * @param {Object} data - updates the promo code count in the header of the Coupon Section
 */
function updateAppliedCouponCount(data) {
  var html = '';
  $('.promo-heading.promo-label').empty();
  // eslint-disable-next-line radix
  if (parseInt(data.totalAppliedCoupons) === 0) {
    html = data.noCouponMsg;
    // eslint-disable-next-line radix
  } else if (parseInt(data.totalAppliedCoupons) === 1) {
    html = data.totalAppliedCoupons + ' ' + data.singleCouponMsg;
  } else {
    html = data.totalAppliedCoupons + ' ' + data.multipleCouponMsg;
  }
  $('.promo-heading.promo-label').append(html);
}

module.exports = {
  updateTotals: updateTotals,
  updateOrderProductSummaryInformation: updateOrderProductSummaryInformation,
  updateProductPromotionsSummary: updateProductPromotionsSummary,
  updateAppliedCouponCount: updateAppliedCouponCount
};
