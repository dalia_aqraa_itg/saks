'use strict';

var Cleave = require('cleave.js').default;
var tokenEx = require('tokenex/tokenEx');
var HBCCards = require('../checkout/HBCCards');

module.exports = {
  handleCreditCardNumber: function (cardFieldSelector, cardTypeSelector) {
    var cleave = new Cleave(cardFieldSelector, {
      creditCard: true,
      onCreditCardTypeChanged: function (type) {
        var creditCardTypes = {
          visa: 'Visa',
          mastercard: 'Master Card',
          amex: 'Amex',
          discover: 'Discover',
          jcb: 'JCB',
          diners: 'DinersClub',
          unionPay: 'ChinaUnionPay',
          unknown: 'Unknown',
          uatp: 'Unknown'
        };

        var cardType = creditCardTypes[Object.keys(creditCardTypes).indexOf(type) > -1 ? type : 'unknown'];
        if (type !== 'uatp') {
          $(cardTypeSelector).val(cardType);
          $('.card-number-wrapper').attr('data-type', type);
        }
        // Get Raw Value - If Card length is 15 and type is not recognized, Set the type as unknown.
        if ($(cardFieldSelector).val() !== '' && $(cardFieldSelector).val().indexOf('*') === -1) {
          var rawCardNumber = $(cardFieldSelector).data('cleave').getRawValue();
          if (rawCardNumber.length >= 15 && (type === 'uatp' || creditCardTypes[type] === undefined)) {
            $(cardTypeSelector).val('Unknown');
            $('.card-number-wrapper').attr('data-type', 'unknown');
            $('.card-number-wrapper').attr('data-plcccard', 'false');
          }
        }

        if (type === 'amex') {
          $('#securityCode').attr('maxlength', 4);
        } else {
          $('#securityCode').attr('maxlength', 3);
        }
      }
    });

    $(cardFieldSelector).data('cleave', cleave);
  },
  handleTemporaryCreditCardNumber: function (cardFieldSelector, cardTypeSelector) {
    var cleave = new Cleave(cardFieldSelector, {
      blocks: [4, 4, 4, 4, 4, 6, 3],
      delimiter: ' ',
      onValueChanged: function (type) {
        if ($(cardFieldSelector).val() !== '' && $(cardFieldSelector).val().indexOf('*') === -1) {
          var rawTCCCardNumber = $(cardFieldSelector).data('cleave').getRawValue();
          if (rawTCCCardNumber.length === 29) {
            var tokenNumberType = HBCCards.cardType(rawTCCCardNumber.substr(0, 16));
            var tokenNumberTypeName = tokenNumberType ? tokenNumberType.name : false;
            $(cardTypeSelector).val('TCC');
            $('.card-number-wrapper').attr('data-type', 'tcc');
            if (tokenNumberTypeName) {
              $('.card-number-wrapper').attr('data-tcc-type', 'tcc-' + tokenNumberTypeName.toLowerCase());
              $('.tcc-card-number-wrapper').attr('data-tcc-type', 'tcc-' + tokenNumberTypeName.toLowerCase());
            }
            $('.card-number-wrapper').attr('data-plcccard', 'true');
            $('.tcc-card-number-wrapper').attr('data-type', 'tcc');
            $('.tcc-card-number-wrapper').attr('data-plcccard', 'true');
          } else {
            $(cardTypeSelector).val('');
            $('.card-number-wrapper').removeAttr('data-type');
            $('.card-number-wrapper').removeAttr('data-tcc-type');
            $('.card-number-wrapper').removeAttr('data-plcccard');
            $('.tcc-card-number-wrapper').removeAttr('data-type');
            $('.tcc-card-number-wrapper').removeAttr('data-tcc-type');
            $('.tcc-card-number-wrapper').removeAttr('data-plcccard');
          }
        }

        $('#securityCode').attr('maxlength', 3);
      }
    });

    $(cardFieldSelector).data('cleave', cleave);
  },
  handleSaksFirstAccountNumber: function (cardFieldSelector, cardTypeSelector) {
    var cleave = new Cleave(cardFieldSelector, {
      blocks: [4, 4, 4, 4],
      delimiter: ' ',
      onValueChanged: function (type) {
        if ($(cardFieldSelector).val() !== '' && $(cardFieldSelector).val().indexOf('*') === -1) {
          var rawTCCCardNumber = $(cardFieldSelector).data('cleave').getRawValue();
          if (rawTCCCardNumber.length === 16) {
            $(cardTypeSelector).val('SAKS');
            $('.saks-card-wrapper').attr('data-type', 'saksfirst');
          } else {
            $(cardTypeSelector).val('');
            $('.saks-card-wrapper').removeAttr('data-type');
          }
        }

        $('#securityCode').attr('maxlength', 3);
      }
    });

    $(cardFieldSelector).data('cleave', cleave);
  },
  handleCOMXNumber: function (cardFieldSelector) {
    var cleave = new Cleave(cardFieldSelector, {
      blocks: [3, 3],
      numericOnly: true,
      delimiter: ''
    });
  },

  serializeData: function (form) {
    var serializedArray = form.serializeArray();

    serializedArray.forEach(function (item) {
      if (item.name.indexOf('cardNumber') > -1) {
        var isTokenEnabled = 'true';
        var cardType = $('.card-number-wrapper').attr('data-type');
        if (cardType && cardType === 'tcc') {
          item.value = $('#cardNumber').data('cleave').getRawValue(); // eslint-disable-line
        } else if (isTokenEnabled !== undefined && isTokenEnabled === 'true') {
          var cardNumber = $('#cardNumber').data('cleave').getRawValue();
          var publicKey = $('input.tokenExPubKey').val();

          if (cardNumber && publicKey) {
            if ((cardType && (cardType === 'HBC' || cardType === 'SAKS' || cardType === 'MPA')) || tokenEx.luhnCheck(cardNumber)) {
              var token = tokenEx.encryptCC(publicKey, cardNumber);
              item.value = token; // eslint-disable-line
            } else {
              item.value = ''; // eslint-disable-line
            }
          } else {
            item.value = ''; // eslint-disable-line
          }
        } else {
          item.value = $('#cardNumber').data('cleave').getRawValue(); // eslint-disable-line
        }
      }
    });

    return $.param(serializedArray);
  }
};
