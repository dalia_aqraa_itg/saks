'use strict';

var base = require('base/profile/profile');
var formValidation = require('../components/formValidation');

/**
 * Create an alert to display the error message
 * @param {Object} message - Error message to display
 */
function createErrorNotification(message) {
  var errorHtml =
    '<div class="alert alert-danger alert-dismissible valid-cart-error ' +
    'fade show" role="alert">' +
    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
    '<span aria-hidden="true">&times;</span>' +
    '</button>' +
    message +
    '</div>';

  $('.error-messaging').append(errorHtml);
}

base.submitAddAccount = function () {
  $('form.add-account-form').submit(function (e) {
    var $form = $(this);
    e.preventDefault();
    var url = $form.attr('action');
    $form.spinner().start();
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: $form.serialize(),
      success: function (data) {
        $form.spinner().stop();
        if (!data.success) {
          formValidation($form, data);
        } else {
          location.href = data.redirectUrl;
        }
      },
      error: function (err) {
        if (err.responseJSON.redirectUrl) {
          window.location.href = err.responseJSON.redirectUrl;
        }
        $form.spinner().stop();
      }
    });
    return false;
  });
};

base.validatePassword = function () {
  $('body').on('keyup', '#newPassword', function () {
    var regex = new RegExp(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#_?!@$%^()+=><~`}{|&*-])^[^'<>/]{8,}$/);
    if ($(this).val() !== '') {
      $('.password-condition').removeClass('d-none');
      if (regex.test($(this).val())) {
        $('.password-condition').addClass('d-none');
        $('#newPassword').removeClass('is-invalid');
      }
    } else {
      $('.password-condition').addClass('d-none');
    }
  });
};

base.showPassword = function () {
  $('.show-pwd').click(function () {
    var input = $($(this).attr('toggle'));
    if (input.val() !== '') {
      if (input.attr('type') === 'password') {
        input.attr('type', 'text');
        $(this).text($(this).attr('data-hidepassword'));
      } else {
        input.attr('type', 'password');
        $(this).text($(this).attr('data-showpassword'));
      }
    }
  });
};

base.submitPassword = function () {
  $('form.change-password-form').submit(function (e) {
    var $form = $(this);
    e.preventDefault();
    var regex = new RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/);
    var password = $('#newPassword').val();
    if (password !== '') {
      if (!regex.test(password)) {
        $('#newPassword').addClass('is-invalid');
        return false;
      }
    } else {
      $('.password-condition').addClass('d-none');
      return false;
    }
    var url = $form.attr('action');
    $form.spinner().start();
    $('form.change-password-form').trigger('password:edit', e);
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: $form.serialize(),
      success: function (data) {
        $form.spinner().stop();
        if (!data.success) {
          formValidation($form, data);
        } else {
          location.href = data.redirectUrl;
        }
      },
      error: function (err) {
        if (err.responseJSON.redirectUrl) {
          window.location.href = err.responseJSON.redirectUrl;
        }
        $form.spinner().stop();
      }
    });
    return false;
  });
};

base.setPreferences = function () {
  $('form.preference-form').submit(function (e) {
    var form = $(this);
    e.preventDefault();
    var url = form.attr('action');
    form.spinner().start();
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: form.serialize(),
      success: function (data) {
        form.spinner().stop();
        if (!data.success) {
          formValidation(form, data);
        } else {
          location.href = data.redirectUrl;
        }
      },
      error: function (err) {
        if (err.responseJSON.redirectUrl) {
          window.location.href = err.responseJSON.redirectUrl;
        } else {
          createErrorNotification($('.error-messaging'), err.responseJSON.errorMessage);
        }

        form.spinner().stop();
      }
    });
    return false;
  });
};

base.submitProfile = function () {
  $('form.edit-profile-form').submit(function (e) {
    e.preventDefault();
    var $form = $(this);
    // eslint-disable-next-line no-undef
    grecaptcha.ready(function () {
      // eslint-disable-next-line no-undef
      grecaptcha.execute($('.google-recaptcha-key').html(), { action: 'updateprofile' }).then(function (token) {
        $('.g-recaptcha-token').val(token);
        var url = $form.attr('action');
        $form.spinner().start();
        $('form.edit-profile-form').trigger('profile:edit', e);
        var data = $form.serialize();
        // If HBC Reward Number is disabled, we need to append it the POST request.
        if ($('#hudsonbayrewards').prop('disabled')) {
          data += '&dwfrm_profile_customer_hudsonbayrewards=';
        }
        $.ajax({
          url: url,
          type: 'post',
          dataType: 'json',
          data: data,
          success: function (resp) {
            $form.spinner().stop();
            if (!resp.success) {
              formValidation($form, resp);
              if (resp.rewardCounterReached) {
                $form.find('[name="dwfrm_profile_customer_hudsonbayrewards"]').attr('disabled', 'disabled');
                $form.find('[name="dwfrm_profile_customer_hudsonbayrewards"]').val('');
              } else {
                $form.find('[name="dwfrm_profile_customer_hudsonbayrewards"]').removeAttr('disabled');
              }
              if (resp.rewardError) {
                $form.find('[name="dwfrm_profile_customer_hudsonbayrewards"]').val('');
              }
            } else {
              location.href = resp.redirectUrl;
            }
            if (resp.botError) {
              $('form.edit-profile-form button.btn-primary').attr('disabled', 'disabled');
            }
          },
          error: function (err) {
            if (err.responseJSON.redirectUrl) {
              window.location.href = err.responseJSON.redirectUrl;
            }
            if (err.botError) {
              $('form.edit-profile-form button.btn-primary').attr('disabled', 'disabled');
            }

            $form.spinner().stop();
          }
        });
        return false;
      });
    });
  });
};
module.exports = base;
