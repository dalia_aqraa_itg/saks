window.jQuery = window.$ = require('jquery');
var processInclude = require('base/util');

$(document).ready(function () {
  processInclude(require('./formFields/formFields'));
  processInclude(require('base/components/countrySelector'));
  processInclude(require('./components/clientSideValidation'));
  processInclude(require('./caLanding/caLanding'));
});

require('base/thirdParty/bootstrap');
require('base/components/spinner');
