'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
  processInclude(require('./product/detail'));
  processInclude(require('./product/detailPrimaryImages'));
  processInclude(require('./product/detailStickyElements'));
  processInclude(require('./product/pdpInstoreInventory'));
});
