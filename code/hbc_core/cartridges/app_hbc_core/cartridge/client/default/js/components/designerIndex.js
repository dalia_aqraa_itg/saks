'use strict';

// code for anchoring

$(document).on('click', '.designer-alphas a[href]', function (e) {
  e.preventDefault();
  var href = $(this).attr('href');
  var scrollPosition = $('#' + href).offset().top;
  $('.designer-alphas').find('li').removeClass('active');
  $(this).closest('li').addClass('active');
  var scroll;
  if ($('.main-menu').hasClass('fixed') || $('#header-container').hasClass('fixed')) {
    scroll = $('.designer-alphas').outerHeight() + $('.fixed').outerHeight();
  } else if ($(window).width() < 1023.99 && !$('#header-container').hasClass('fixed')) {
    scroll = $('.designer-alphas').outerHeight() + 200;
  } else {
    scroll = $('.designer-alphas').outerHeight() + 80;
  }
  $('html, body').animate(
    {
      /* eslint-disable */
      scrollTop: scrollPosition - scroll
      /* eslint-enable */
    },
    500
  );
});

/**
 * Designer Index Slider
 *
 */
function stickyDesignerAlphas() {
  $('.designer-alphas').removeClass('fixed-alphas');
  if ($('.main-menu').hasClass('fixed') || $('#header-container').hasClass('fixed')) {
    $('.designer-alphas').addClass('fixed-alphas');
  }
}
/**
 * Designer Index Slider
 *
 */
function designerSlider() {
  if ($(window).width() <= 1023.99) {
    $('.designer-main')
      .find('.designer-alphas')
      .not('.slick-initialized')
      .slick({
        responsive: [
          {
            breakpoint: 1025,
            settings: 'unslick'
          },
          {
            breakpoint: 769,
            settings: {
              slidesToShow: 16,
              slidesToScroll: 16,
              infinite: false,
              arrows: false
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 8,
              slidesToScroll: 8,
              infinite: false,
              arrows: false
            }
          }
        ]
      });
  }
}

designerSlider();
$(window).resize(function () {
  designerSlider();
  stickyDesignerAlphas();
});

$(window).scroll(function () {
  stickyDesignerAlphas();
});
