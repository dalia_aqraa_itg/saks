'use strict';

var base = require('base/search/search');
var formField = require('./../formFields/formFields');
// to invoke store change modal in search
var pdpInstoreInventory = require('../product/pdpInstoreInventory');
var productGridTeasers = require('../teasersproductgrid');
var isLoading = 0;
var isPreviousLoading = 0;
var currentPos = $(window).scrollTop();
var persistentWishlist = require('../product/persistentWishlist');
var lazyLoad = require('../util/lazyLoadImages');
var utils = require('../components/utilhelper');

/**
 * Routes the handling of attribute processing depending on whether the attribute has image
 *     swatches or not
 *
 * @param {Object} attrs - Attribute
 * @param {string} attr.id - Attribute ID
 * @param {jQuery} $productContainer - DOM element for a given product
 */
function updateAttrs(attrs, $productContainer) {
  attrs.forEach(function (attr) {
    attr.values.forEach(function (attrValue) {
      var $attrValue = $productContainer.find('a[data-attr-value="' + attrValue.value + '"]');
      if (attrValue.url) {
        $attrValue.data('valueurl', attrValue.url);
      }
      $attrValue.removeClass('selectable swatch-not-available size-not-available');
      var notAvailableClass = $attrValue.hasClass('colorswatch') ? 'swatch-not-available' : 'size-not-available';
      $attrValue.addClass(attrValue.selectable ? '' : notAvailableClass);
    });
  });
  // append model images if it is enabled
  $('body').trigger('category:set:model:view');
}

/**
 * remove the store selector modal when a store is selected out of results
 */
function removeSelectStoreModal() {
  if ($('#inStoreInventoryModal').length > 0) {
    $('#inStoreInventoryModal').modal('hide');
    $('#inStoreInventoryModal').remove();
    $('#inStoreInventoryModal').attr('aria-modal', 'false');
  }
}

/**
 * Removes the specified parameter from url
 *
 * @param {string} url - the url
 * @param {string} parameter - parameter to be removed
 * @returns {string} url - url after removing the parameter
 */
function removeURLParameter(url, parameter) {
  // prefer to use l.search if you have a location/link object
  var urlparts = url.split('?');
  if (urlparts.length >= 2) {
    var prefix = encodeURIComponent(parameter) + '=';
    var pars = urlparts[1].split(/[&;]/g);
    // reverse iteration as may be destructive
    for (var i = pars.length; i-- > 0; ) {
      // idiom for string.startsWith
      if (pars[i].lastIndexOf(prefix, 0) !== -1) {
        pars.splice(i, 1);
      }
    }

    // eslint-disable-next-line no-param-reassign
    url = urlparts[0] + '?' + pars.join('&');
    return url;
  }
  return url;
}

/**
 * Updates the store name
 * @param {Object} data - Contains the store info
 */
function updateStoreContent(data) {
  var storeId = data.storeID;
  var storeDistance = data.storeDistance;
  var storeName = data.name;
  $.spinner().start();
  var setStoreUrl = data.url;
  // form data
  var form = {
    storeId: storeId,
    storeDistance: storeDistance,
    ajax: true,
    savetosession: true,
    noviewdata: true
  };
  if (setStoreUrl) {
    $.ajax({
      url: setStoreUrl,
      data: form,
      success: function (data) {
        //eslint-disable-line
        var storeRefine = $('.change-store').parent().siblings('.store-refine');
        $('.change-store').html(storeName);
        var dataUrl = storeRefine.find('input[id="bopisCheck"]').data('href');
        // remove the store filter in the refinement and execute the search
        var storeUrl = removeURLParameter(dataUrl, 'storeid');
        $(document).find('.store-refine').attr('data-search-source', 'store');
        storeRefine.find('input[id="bopisCheck"]').attr('data-href', storeUrl);
        $('input[id="bopisCheck"]').removeAttr('disabled');
        $('.no-products-store').addClass('d-none');
        $('input[id="bopisCheck"]').trigger('click');
      },
      error: function () {
        $.spinner().stop();
      }
    });
  }
}

/**
 * Update sort option URLs from Ajax response
 *
 * @param {string} response - Ajax response HTML code
 * @return {undefined}
 */
function updateSortOptions(response) {
  var $tempDom = $('<div>').append($(response));
  var sortOptions = $tempDom.find('.grid-footer').data('sort-options').options;
  sortOptions.forEach(function (option) {
    $('option.' + option.id).val(option.url);
  });
}

/**
 * updates the product view when a product attribute is selected or deselected or when
 *         changing quantity
 * @param {string} selectedValueUrl - the Url for the selected variation value
 * @param {jQuery} $productContainer - DOM element for current product
 * @param {Array} oldImgs - Old images array
 */
function attributeSelect(selectedValueUrl, $productContainer, oldImgs) {
  if (selectedValueUrl) {
    $productContainer.spinner().start();
    $.ajax({
      url: selectedValueUrl,
      method: 'GET',
      success: function (data) {
        updateAttrs(data.product.variationAttributes, $productContainer);
        persistentWishlist.makrSingleProductWishlisted(data.product.id, $productContainer);
        $productContainer.closest('.product').data('pid', data.product.id);
        $productContainer.closest('.product').attr('data-pid', data.product.id);
        $productContainer.find('.bf-product-id').empty().text(data.product.id);
        $productContainer.closest('.product').data('producttype', data.product.productType);
        $productContainer
          .closest('.product')
          .find('.prdt_tile_btn')
          .data('readytoorder', data.product.readyToOrder && data.product.available);
        if (data.product.readyToOrder && data.product.available) {
          $productContainer.closest('.product').find('.prdt_tile_btn').data('view', 'selected');
        }

        // update the price
        /* var $priceSelector = $('.price', $productContainer);
                var $priceRange = $('.range', $productContainer);
                if (data.product.price.type != null && data.product.price.type == 'range') {
                    $priceRange.empty().html(data.product.price.html);
                } else {
                    $priceSelector.empty().html(data.product.price.html);
                }*/
        var $priceSelector = $('.prod-price', $productContainer);
        $priceSelector.empty().html(data.product.price.html);

        // Update promotions
        if (data.product.promotionalPricing && data.product.promotionalPricing.isPromotionalPrice && data.product.promotionalPricing.promoMessage !== '') {
          $('.promotion-pricing', $productContainer).empty().html(data.product.promotionalPricing.priceHtml);
          $('.promotion-pricing', $productContainer).removeClass('d-none');
          $('.promotions', $productContainer).addClass('d-none');
        } else {
          $('.promotion-pricing', $productContainer).addClass('d-none');
          $('.promotions', $productContainer).removeClass('d-none');
          $('.promotions', $productContainer).empty().html(data.product.promotionsHtml);
        }

        // Update limited inventory message
        if (data.product.availability.isAboveThresholdLevel) {
          $('.limited-inventory', $productContainer).empty().text(data.resources.limitedInventory);
        } else {
          $('.limited-inventory', $productContainer).empty();
        }

        if ((!data.product.available || !data.product.readyToOrder) && data.product.waitlistable && data.product.productType !== 'master') {
          $productContainer.find('button.prdt_tile_btn').addClass('d-none');
          $productContainer.find('a.prdt_tile_btn').removeClass('d-none');
          $productContainer.find('a.prdt_tile_btn').attr('href', data.product.pdpURL + '#waitlistenabled');
        } else if ((!data.product.available || !data.product.readyToOrder) && data.product.productType !== 'master') {
          $productContainer.find('button.prdt_tile_btn').addClass('soldout');
          $productContainer.find('button.prdt_tile_btn').removeClass('add-to-cart');
          $productContainer.find('button.prdt_tile_btn').removeClass('d-none');
          $productContainer.find('button.prdt_tile_btn').text(data.resources.soldout);
          $productContainer.find('button.prdt_tile_btn').attr('disabled', true);
          $productContainer.find('a.prdt_tile_btn').addClass('d-none');
        } else {
          $productContainer.find('button.prdt_tile_btn').addClass('add-to-cart');
          $productContainer.find('button.prdt_tile_btn').removeClass('soldout');
          $productContainer.find('button.prdt_tile_btn').removeClass('d-none');
          $productContainer.find('a.prdt_tile_btn').addClass('d-none');
          $productContainer.find('button.prdt_tile_btn').removeAttr('disabled');
          if (data.product.preOrder && data.product.preOrder.applicable && data.product.preOrder.applicable === true) {
            $productContainer.find('button.prdt_tile_btn').text(data.resources.preordertocart);
          } else if ($('body').hasClass('cart-page')) {
            $productContainer.find('button.prdt_tile_btn').text(data.resources.movetobag);
          } else {
            $productContainer.find('button.prdt_tile_btn').text(data.resources.addtocart);
          }
        }
        base.plpSwatchesEvents();
        // append model images if it is enabled
        $('body').trigger('category:set:model:view');
        $productContainer.spinner().stop();
        setTimeout(function () {
          if (typeof oldImgs !== 'undefined') {
            for (let oi in oldImgs) {
              //eslint-disable-line
              oldImgs[oi].remove();
            }
          }
        }, 1000);
      },
      error: function () {
        $productContainer.spinner().stop();
      }
    });
  }
}

base.plpSwatchesEvents = function () {
  $('body').on('click', '.sizeSwatch', function (e) {
    e.preventDefault();
    if ($(this).hasClass('selected')) {
      return;
    }
    var $selectedSize = $(this);
    var $prevSelectedSize = $selectedSize.closest('.size').find('.sizeSwatch.selected');
    $prevSelectedSize.removeClass('selected');
    $selectedSize.addClass('selected');
    $(this).closest('.product-tile').find('.prdt_tile_btn').data('view', 'selected');
    attributeSelect($(this).data('valueurl'), $(this).closest('.product-tile'));
  });
  $('body').on('click', '.color-swatches .swatches .colorswatch', function (e) {
    e.preventDefault();
    if ($(this).hasClass('selected')) {
      return;
    }

    var $selectedSwatch = $(this);
    var $tile = $selectedSwatch.closest('.product-tile');
    $tile.find('.color-swatches .swatches .colorswatch.selected').each(function () {
      $(this).removeClass('selected');
    });
    $selectedSwatch.addClass('selected');
    var color = $selectedSwatch.data('attr-value');
    $tile.find(".color-swatches .swatches .colorswatch[data-attr-value='" + color + "']").addClass('selected');
    $tile.find('.image-container').removeClass('prevent-slick');
    $tile.find('.thumb-link').attr('href', $selectedSwatch.attr('href'));
    $tile.find('.link').attr('href', $selectedSwatch.attr('href'));
    var data = $(this).find('.swatch-circle').data('imgsource');
    var oldImgs = [];
    $tile.find('.image-container .thumb-link img').each(function () {
      oldImgs.push($(this));
    });
    if ($selectedSwatch.data('qvurl')) {
      $tile.find('.image-container .quickview').attr('href', $selectedSwatch.data('qvurl'));
    }
    for (var i = 0; i < data.length; i++) {
      var $thumblink = $tile.find('.image-container .thumb-link');
      var setImg = data[i];
      $('<img class="tile-image" itemprop="image"/>')
        .attr('src', '' + setImg.url + '') // ADD IMAGE PROPERTIES.
        .attr('title', setImg.title)
        .attr('alt', setImg.alt)
        .appendTo($thumblink); // ADD THE IMAGE TO DIV
    }
    attributeSelect($(this).data('valueurl'), $(this).closest('.product-tile'), oldImgs);
    // $thumb.data('current', currentAttrs);
  });
  /* eslint-disable */
  if ($(window).width() > 1023.99) {
    $('body')
      .on('mouseenter', '.thumb-link', function () {
        var initialIndex = 0;
        if ($(this).closest('.product-tile').find('.firstSelectableIndex').length > 0) {
          var initialIndex = $(this).closest('.product-tile').find('.firstSelectableIndex').eq(0).attr('data-firstSelectableIndex');
        }
        if (!$(this).closest('.product-tile').find('.size').hasClass('slick-initialized')) {
          $(this).closest('.product-tile').find('.size').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: true,
            autoplay: false
          });

          if ($(this).closest('.product-tile').find('.size').eq(0).find('.slick-slide').length - 1 == Number(initialIndex)) {
            $(this)
              .closest('.product-tile')
              .find('.size')
              .slick('slickGoTo', Number(initialIndex) - 1, true);
          } else {
            $(this).closest('.product-tile').find('.size').slick('slickGoTo', Number(initialIndex), true);
          }
        }
        if ($(this).closest('.image-container').hasClass('prevent-slick')) {
          return false;
        }
        if ($(this).hasClass('slick-initialized')) {
          $(this).slick('unslick');
        }
        $(this)
          .find('img')
          .each(function () {
            $(this).attr('src', $(this).attr('data-src'));
          });
        if (!/msie\s|trident\/|Edge\//i.test(navigator.userAgent)) {
          $(this).slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 500
          });
        }
        if ($(this).closest('.image-container').find('.tile-image').hasClass('guest-user')) {
          $(this).slick('unslick');
        }
      })
      .on('mouseleave', '.thumb-link', function () {
        if ($(this).hasClass('slick-initialized')) {
          $(this).slick('unslick');
        }
      });
    /* eslint-enable */

    $('body').on('mouseenter', '.product-tile', function () {
      var initialIndex = 0;
      if ($(this).find('.firstSelectableIndex').length > 0) {
        initialIndex = $(this).find('.firstSelectableIndex').eq(0).attr('data-firstSelectableIndex');
      }
      if (!$(this).find('.size').hasClass('slick-initialized')) {
        $(this).find('.size').slick({
          dots: false,
          infinite: false,
          speed: 300,
          slidesToShow: 2,
          slidesToScroll: 1,
          arrows: true,
          autoplay: false
        });

        if ($(this).find('.size').eq(0).find('.slick-slide').length - 1 == Number(initialIndex)) {
          $(this)
            .find('.size')
            .slick('slickGoTo', Number(initialIndex) - 1, true);
        } else {
          $(this).find('.size').slick('slickGoTo', Number(initialIndex), true);
        }
      }
    });
  }
};

/**
 * Update DOM elements with Ajax results
 *
 * @param {Object} $results - jQuery DOM element
 * @param {string} selector - DOM element to look up in the $results
 * @return {undefined}
 */
function updateDom($results, selector) {
  var $updates = $results.find(selector);
  $(selector).empty().html($updates.html());
  // add reviews on update
  productGridTeasers.addReviews();
}

/**
 * Keep refinement panes expanded/collapsed after Ajax refresh
 *
 * @param {Object} $results - jQuery DOM element
 * @return {undefined}
 */
function handleRefinements($results) {
  $('.refinement.active').each(function () {
    $(this).removeClass('active');
    var activeDiv = $results.find('.' + $(this)[0].className.replace('/', '').replace('?', '').replace('&', '').replace(/ /g, '.'));
    activeDiv.addClass('active');
    activeDiv.find('button.title').attr('aria-expanded', 'true');
  });

  updateDom($results, '.refinements');
}

/**
 * Parse Ajax results and updated select DOM elements
 *
 * @param {string} response - Ajax response HTML code
 * @param {event} event - function event
 * @returns {boolean}- return state if there were any search results
 */
function parseResults(response, event) {
  var $results = $(response);
  if ($results.find('.search-results-total-count').html() === '0') {
    if (
      event &&
      event.currentTarget &&
      event.currentTarget &&
      $(event.currentTarget).closest('input[id="bopisCheck"]').length > 0 &&
      $('.no-products-store').length &&
      $('.no-products-store').hasClass('d-none')
    ) {
      $('.no-products-store').removeClass('d-none');
      $('input[id="bopisCheck"]').attr('disabled', true);
    }
    // for saks only
    if (
      event &&
      event.currentTarget &&
      event.currentTarget &&
      $(event.currentTarget).closest('input[id="bopisCheck"]').length > 0 &&
      $(event.currentTarget).closest('input[id="bopisCheck"]').hasClass('saks-only') &&
      $(event.currentTarget).closest('input[id="bopisCheck"]').attr('data-nostores-msg') !== undefined
    ) {
      $('input[id="bopisCheck"]').attr('disabled', 'disabled');
      $('input[id="bopisCheck"]').siblings('label').html($(event.currentTarget).closest('input[id="bopisCheck"]').attr('data-nostores-msg'));
    }
    // change label of SDD if no product are not
    if (event && event.currentTarget && event.currentTarget && $(event.currentTarget).closest('input[id="sddCheck"]').length > 0) {
      $('input[id="sddCheck"]').attr('disabled', true);
      $('input[id="sddCheck"]').prop('checked', true);
      if (
        $results.find('.search-results').attr('data-noresults-message') !== '' &&
        $results.find('.search-results').attr('data-noresults-message') !== 'null' &&
        $('input[id="sddCheck"]').siblings('label[for=sddCheck]').length > 0
      ) {
        $('input[id="sddCheck"]').siblings('label[for=sddCheck]').html($results.find('.search-results').attr('data-noresults-message'));
      }
    }
    return false;
  }
  var searchSource = $(document).find('.store-refine').attr('data-search-source');
  $('.refinement-price .no-results').addClass('d-none');
  var specialHandlers = {
    '.refinements': handleRefinements
  };
  var domElements = [
    '.grid-header',
    '.header-bar',
    '.header.page-title',
    '.product-grid',
    '.show-more',
    '.filter-bar',
    '.search-count',
    '.sort-results-container',
    '.sort-selection-block',
    '.shop-item',
    '.search-results-count',
    '.store-bar',
    '.product-search-results .breadcrumb',
    '.product-search-results .search-results-header',
    '.filter-header',
    '.filter-results-container'
  ];

  // for store refinement, the .filter-bar is not neccessary to be refreshed
  if (event && event.currentTarget && event.currentTarget && $(event.currentTarget).closest('input[id="bopisCheck"]').length > 0) {
    domElements.splice(5, 1);
  }
  // Update DOM elements that do not require special handling
  domElements.forEach(function (selector) {
    updateDom($results, selector);
  });
  $('body').trigger('category:category:refinement:clone');

  // dup header elements
  var dupHeaders = $results.find('.product-search-results .search-results-header');
  Object.keys(dupHeaders).forEach(function (elm) {
    var headerElement = dupHeaders.eq(elm);
    if (headerElement.text().trim() !== '') {
      $('.product-search-results .search-results-header').empty().html(headerElement.html());
    }
  });

  if ($('.product-search-results').find('.search-results-header').length > 1) {
    $('.product-search-results').find('.search-results-header').eq(1).remove();
  }

  // append model images if it is enabled
  $('body').trigger('category:set:model:view');

  // toggle pick up check box
  if (
    event &&
    event.currentTarget &&
    event.currentTarget &&
    $(event.currentTarget).closest('input[id="sddCheck"]').length === 0 &&
    $results.find('.search-results').data('isrefinedby-store')
  ) {
    $('input[id="bopisCheck"]').attr('checked', true);
  } else {
    $('input[id="bopisCheck"]').removeAttr('checked');
    // don not diable the button and message when the search is from store select
    if (
      event &&
      event.currentTarget &&
      event.currentTarget &&
      $(event.currentTarget).closest('input[id="bopisCheck"]').length > 0 &&
      $('.no-products-store').length &&
      $('.no-products-store').hasClass('d-none') &&
      searchSource === 'search'
    ) {
      $('.no-products-store').removeClass('d-none');
      $('input[id="bopisCheck"]').attr('disabled', true);
    } else {
      $(document).find('.store-refine').attr('data-search-source', 'search');
    }
  }
  // update url if the trigger is form the store select from the store change modal
  if ($results.siblings('.search-results').data('storecheck-url') && $(event.currentTarget).closest('input[id="bopisCheck"]').length === 0) {
    $('input[id="bopisCheck"]').attr('data-href', $results.siblings('.search-results').data('storecheck-url'));
  }
  Object.keys(specialHandlers).forEach(function (selector) {
    specialHandlers[selector]($results);
  });
  // update result count
  if (event && event.currentTarget && event.currentTarget && $(event.currentTarget).closest('input[id="sddCheck"]').length > 0) {
    $('input[id="sddCheck"]').prop('checked', true);
    $('input[id="sddCheck"]').removeAttr('disabled');
    if (
      $results.find('.search-results').attr('data-results-message') !== '' &&
      $results.find('.search-results').attr('data-results-message') !== 'null' &&
      $('input[id="sddCheck"]').siblings('label[for=sddCheck]').length > 0
    ) {
      $('input[id="sddCheck"]').siblings('label[for=sddCheck]').html($results.find('.search-results').attr('data-results-message'));
    }
  }
  formField.updateSelect();
  if ($(window).width() >= 1024) {
    if (event !== undefined && !$(event.currentTarget).hasClass('bopis-refinement')) {
      $(window).scrollTop($('.search-result-wrapper').offset().top - $('.navigation-section .main-menu').outerHeight() - 18);
    }
  } else {
    $(window).scrollTop($('.product-tile-section').offset().top - document.querySelector('header').offsetHeight);
  }
  return true;
}

var viewmoreshow = function () {
  if ($(window).width() > 1023.99) {
    $('.catgories-values').each(function () {
      if ($(this).hasClass('category-hide')) {
        $(this).addClass('d-none');
      }
    });
  } else {
    $('.catgories-values').each(function () {
      if ($(this).hasClass('category-hide')) {
        $(this).removeClass('d-none');
      }
    });
  }
};

viewmoreshow();

var thumbWidth = function () {
  if ($(window).width() > 1023.99) {
    if ($('.product-tile.saks-avenue-product-tile').length === 0) {
      $('.product-tile .thumb-link').each(function () {
        $(this).attr('style', '');
        $(this).css('width', Math.floor($(this).outerWidth()));
      });
    }
  }
};

thumbWidth();

function prepActiveRefinementTrigger() {
  var activeRefinement = document.querySelector('.refinement.card.active');
  var trigger;
  if (!activeRefinement) {
    return;
  }
  trigger = activeRefinement.querySelector('.card-header button[aria-controls]');
  trigger.setAttribute('aria-expanded', 'true');
}

/**
 * update grid of search results/category page
 *
 * @param {string} url - url to be used for fetching desired results
 * @param {string} type - type of filter
 * @param {Object} e - event object
 * @param {Object} $this - current element
 */
function updateGrid(url, type, e, $this) {
  const refinementBar = document.querySelector('.refinement-bar');
  const refinementWrapperScrollTop = refinementBar ? refinementBar.scrollTop : 0;
  $.ajax({
    url: url,
    data: {
      page: $('.grid-footer').data('page-number'),
      selectedUrl: $(this).data('href'),
      type: type,
      filter: true,
      ajax: true
    },
    method: 'GET',
    success: function (response) {
      let result = parseResults(response, e);
      if (result) {
        // remove the brand parameter appended on the refinement url, this will impact SEO url
        // also impacts the clearAll link being set wrongly on the page load. This param is intended to be used only on ajax
        if (url && url.indexOf('doNotReset=true') > -1) {
          url = url.replace('&doNotReset=true', '').replace('doNotReset=true&', '').replace('?doNotReset=true', '').replace('doNotReset=true', ''); // eslint-disable-line
        }
        history.pushState({}, '', url);
        persistentWishlist.markProductArrWishlisted();
        $('.card-header button').removeClass('clicked');
        // eslint-disable-next-line newline-per-chained-call
        $('.card-body').find('.selected').parents('.refinement').find('.card-header button').addClass('clicked');
        $('.view-more-less').text('View More');
        $('.refinement-category').removeClass('viewmore');
        // customscrollbar();
        viewmoreshow();
        prepActiveRefinementTrigger();
        // append model images if it is enabled
        $('body').trigger('category:category:refinement:clone');
        $('body').trigger('category:set:model:view');
        $('body').trigger('adobeTagManager:productArrayUpdate', $this);
      }
      $.spinner().stop();
      if (refinementWrapperScrollTop) {
        window.setTimeout(() => {
          refinementBar.scrollTop = refinementWrapperScrollTop;
        }, 10);
      }
      if (refinementBar && !refinementBar.contains(document.activeElement)) {
        $('.refinement-bar').find(':visible:focusable').first().focus();
      }
    },
    error: function () {
      $.spinner().stop();
    }
  });
}

base.applyFilter = function () {
  // Handle refinement value selection and reset click
  $('.container').on(
    'click',
    '.refinements li button, .refinement-bar button.reset, .filter-value button, .swatch-filter button, .clear-refinement button.reset, .store-refine input, .refinements li a, .sdd-refine input, .all-refine input',
    function (e) {
      e.preventDefault();
      e.stopPropagation();
      $.spinner().start();

      var url = $(this).attr('data-href') || $(this).attr('href');
      /* identify if the page has 'hide-designer'
              If the page has hide-designer class, then we are passing an addition query parameter to avoid
              setting the clear All link to brand filter.
            */
      if (!$(this).closest('.search-results').hasClass('hide-designer')) {
        url += url && url.indexOf('?') > -1 ? '&doNotReset=true' : '?doNotReset=true';
      }

      if (window.matchMedia('(max-width: 1023px)').matches) {
        window.lastFilter = $(this).data('href') || $(this).attr('href');
      }
      if (!$(this).hasClass('bopis-refinement')) {
        window.params = url.split('?')[1];
      }

      // logic added for clearing of selected category from refinement
      var type = $(this).data('type') || '';

      if (type === 'category' && $(window).width() >= 1024) {
        // remove the brand parameter appended on the refinement url, this will impact SEO url
        // also impacts the clearAll link being set wrongly on the page load. This param is intended to be used only on ajax
        if (url && url.indexOf('doNotReset=true') > -1) {
          url = url.replace('&doNotReset=true', '').replace('doNotReset=true&', '').replace('?doNotReset=true', '').replace('doNotReset=true', ''); // eslint-disable-line
        }
        window.location = url;
      } else {
        if (type === 'category') {
          window.filterType = true;
        }
        $(this).trigger('search:filter', e);
        var $this = $(this);
        updateGrid(url, type, e, $this);
      }
    }
  );
};

$('.search-results')
  .off('click')
  .on('click', '.view-more-less', function (e) {
    e.preventDefault();
    var $button = $(this);
    var labelViewMore = $button.data('viewmore');
    var labelViewLess = $button.data('viewless');
    $button.text(function (i, textupdate) {
      return textupdate === labelViewMore ? labelViewLess : labelViewMore;
    });
    $button.blur();
    $('.refinement-category').toggleClass('viewmore');

    if ($('.refinement-category').hasClass('viewmore')) {
      $('.catgories-values').removeClass('d-none');
    } else {
      viewmoreshow();
    }
  });

/**
 * Clears the designer search
 *
 * @param {Object} element - current element
 */
function clearSearch(element) {
  var allRefValues = element.parent().siblings('ul').find('li');
  allRefValues.removeClass('d-none');
  $('.refinement-search-bar').siblings('.no-results').addClass('d-none');
}

/**
 * Sorting brand
 */
base.filterBrands = function () {
  $('body').on('keyup', '.refinement-search .refinement-search-bar input', function () {
    let $this = $(this);
    var searchKeyWord = $this.val();
    if (searchKeyWord) {
      $this.siblings('button.search-button').addClass('d-none');
      $this.siblings('button.close-button').removeClass('d-none');
      var allRefValues = $this.parent().siblings('ul').find('li');
      var searchFound = false;
      $.each(allRefValues, function () {
        var html = $(this).find('span.value').html();
        if (html.trim().toLowerCase().indexOf(searchKeyWord.toLowerCase()) === 0) {
          searchFound = true;
          $(this).removeClass('d-none');
        } else {
          $(this).addClass('d-none');
        }
      });
      $this.parent().siblings('.no-results').toggleClass('d-none', searchFound);
    } else {
      $this.siblings('button.search-button').removeClass('d-none');
      $this.siblings('button.close-button').addClass('d-none');
      clearSearch($this);
    }
  });
};

// Remove the no results bar for location
$(document).on('click', '.no-products-store .close', function () {
  let $this = $('.no-products-store');
  $this.addClass('d-none');
});

// Remove the search selection in Refinement
$('body').on('click', '.refinement-search-bar button.close-button', function () {
  let $this = $(this);
  $this.addClass('d-none');
  $this.siblings('button.search-button').removeClass('d-none');
  $this.siblings('input').val('');
  clearSearch($this);
});

$('body').on('click', '.price-select button.go-price', function (e) {
  e.preventDefault();
  e.stopPropagation();
  var formElement = this.form;
  // eslint-disable-next-line radix
  var min = formElement.minPrice.value ? parseInt(formElement.minPrice.value) : '';
  // eslint-disable-next-line radix
  var max = formElement.maxPrice.value ? parseInt(formElement.maxPrice.value) : '';
  var search = false;
  var regexTester = new RegExp(/^(\s*|\d+)$/);
  if (!!min && !!max) {
    if (!regexTester.test(min) || !regexTester.test(max)) {
      search = false;
    } else if (min < max) {
      search = true;
    } else {
      search = false;
    }
  } else if (!min && !!max) {
    search = regexTester.test(max);
  } else if (!max && !!min) {
    search = regexTester.test(min);
  } else {
    search = true;
  }
  if (search) {
    var searchURL = $(this).data('href');
    if (min) {
      searchURL = searchURL + (searchURL.indexOf('?') > -1 ? '&pmin=' : '?pmin=') + min;
    }
    if (max) {
      searchURL = searchURL + (searchURL.indexOf('?') > -1 ? '&pmax=' : '?pmax=') + max;
    }
    $.spinner().start();
    $(this).trigger('search:filter', e);
    var $this = $(this);
    $.ajax({
      url: searchURL,
      data: {
        page: $('.grid-footer').data('page-number'),
        selectedUrl: searchURL,
        filter: true,
        ajax: true
      },
      method: 'GET',
      success: function (response) {
        const result = parseResults(response, e);
        if (result) {
          lazyLoad.hydrateLazyLoadedImages();
          if (!$(response).find('.search-results-total-count').html() === '0') {
            updateSortOptions(response);
          }
          history.pushState({}, '', searchURL);
          $('.card-header button').removeClass('clicked');
          $('.card-body').find('.selected').parents('.refinement').find('.card-header button').addClass('clicked');
          $('.view-more-less').text('View Less');
          $('.refinement-category').toggleClass('viewmore');
          // customscrollbar();
          $('.price-error').addClass('d-none');
          $('.price-select input').removeClass('error');
          $('#min-input').removeClass('error');
          $('#max-input').removeClass('error');
          $('.refinement-price .no-results').addClass('d-none');
          $('body').trigger('adobeTagManager:productArrayUpdate', $this);
          $('.search-results .price-no-results').addClass('d-none');
        } else {
          $('.refinement-price .no-results').removeClass('d-none');
          $('.refinement-price .price-error').addClass('d-none');
          $('.search-results .price-no-results').removeClass('d-none');
        }
        $.spinner().stop();
      },
      error: function () {
        $('.price-error').removeClass('d-none');
        if (regexTester.test(min) === false) {
          $('#min-input').addClass('error');
        } else {
          $('#min-input').removeClass('error');
        }
        if (regexTester.test(max) === false) {
          $('#max-input').addClass('error');
        } else {
          $('#max-input').removeClass('error');
        }
        $.spinner().stop();
      }
    });
  } else {
    $('.refinement-price .price-error').removeClass('d-none');
    $('.refinement-price .no-results').addClass('d-none');
    if (regexTester.test(min) === false) {
      $('#min-input').addClass('error');
    } else {
      $('#min-input').removeClass('error');
    }
    if (regexTester.test(max) === false) {
      $('#max-input').addClass('error');
    } else {
      $('#max-input').removeClass('error');
    }
  }
});

// eslint-disable-next-line consistent-return
$(document).on('keydown', '#min-input, #max-input', function (e) {
  if (e.ctrlKey === true && (e.which === 118 || e.which === 86)) {
    e.preventDefault();
  }

  var keyVal = e.which ? e.which : e.keyCode;
  if (keyVal === 46 || keyVal === 190 || keyVal === 8 || keyVal === 9) {
    return true;
  } else if (!(keyVal >= 48 && keyVal <= 57)) {
    return false;
  }
});

$(window).scroll(function () {
  setTimeout(function () {
    var elementTop;
    var elementBottom;
    var viewportTop;
    var viewportBottom;
    if ($('.show-more button').length) {
      elementTop = $('.show-more').offset().top;
      elementBottom = elementTop + $('.show-more').outerHeight();
      viewportTop = $(window).scrollTop();
      viewportBottom = viewportTop + $(window).height();
      if ((elementBottom > viewportTop && elementTop < viewportBottom) || window.innerHeight + $(window).scrollTop() === $(document).height()) {
        // eslint-disable-next-line no-undef
        if (isLoading === 0) {
          isLoading = 1;
          $('body .show-more button').click();
        }
      }
    }
    if ($('.show-previous button').length) {
      elementTop = $('.show-previous').offset().top;
      elementBottom = elementTop + $('.show-previous').outerHeight();
      viewportTop = $(window).scrollTop();
      viewportBottom = viewportTop + $(window).height();
      if ($(this).scrollTop() < currentPos && elementBottom > viewportTop && elementTop < viewportBottom) {
        if (isPreviousLoading === 0) {
          isPreviousLoading = 1;
          $('body .show-previous button').click();
        }
      }
      currentPos = $(this).scrollTop();
    }
  }, 1500);
});

$('body').on('click', '.product-grid div.product', function () {
  var pid = $(this).attr('data-pid');
  var startPageSize = $(this).parent().attr('data-tile-start');
  var state = {
    pid: pid
  };
  var locationParts = window.location.href.split('?');
  var newLocation = locationParts[0].split('#')[0];
  // eslint-disable-next-line no-nested-ternary
  var newLocParams = window.params ? window.params : locationParts[1] ? locationParts[1].split('#')[0] : '';
  var queryParams = newLocParams ? newLocParams.split('&') : '';
  if (!queryParams && !!startPageSize) {
    newLocation = newLocation + '?start=' + startPageSize;
  } else if (!!queryParams && newLocParams.indexOf('start=') < 0 && !!startPageSize) {
    newLocation = newLocation.concat('?start=' + startPageSize, '&' + newLocParams);
  } else {
    for (var i = 0; i < queryParams.length; i++) {
      if (i !== 0) {
        newLocation += '&';
      } else {
        newLocation += '?';
      }
      if (queryParams[i].indexOf('start') > -1) {
        newLocation = newLocation + 'start=' + startPageSize;
      } else {
        newLocation += queryParams[i];
      }
    }
  }
  utils.setCookie('itemID', pid, 2);
  history.replaceState(state, null, newLocation);
});

$('body').on('change keyup', '.price-select input', function () {
  if ($(this).val().length > 0) {
    $(this).addClass('validdata');
  } else {
    $(this).removeClass('validdata success-change-icon');
  }
});

$('body').on('input', '.price-select input', function () {
  $(this).val(
    $(this)
      .val()
      .replace(/[^0-9a-zA-Z]/g, '')
  );
});

/**
 * function to close the refinement button
 *
 */
function closeButton() {
  $('.refinement-bar').removeClass('d-block');
  $('.refinement-bar').siblings().attr('aria-hidden', false);
  $('.refinement-bar').closest('.row').siblings().attr('aria-hidden', false);
  $('.refinement-bar').closest('.tab-pane.active').siblings().attr('aria-hidden', false);
  $('.refinement-bar').closest('.container.search-results').siblings().attr('aria-hidden', false);
  $('.btn.filter-results').focus();
  $('body').removeClass('fixed filters-visible modal-open');
}

$('.refinement-wrapper').on('click', '.shop-item', function () {
  if ($(window).width() < 1024 && !!window.filterType) {
    window.location = window.lastFilter;
  } else {
    closeButton();
  }
  $('.product-refinement-no-search').data('href', window.lastFilter);
});

$('.my-designers-bar').on('click', '.shop-item', function () {
  if ($(window).width() < 1024 && !!window.filterType) {
    window.location = window.lastFilter;
  } else {
    closeButton();
  }
  $('.product-refinement-no-search').data('href', window.lastFilter);
});

$('.my-sizes-bar').on('click', '.shop-item', function () {
  if ($(window).width() < 1024 && !!window.filterType) {
    window.location = window.lastFilter;
  } else {
    closeButton();
  }
  $('.product-refinement-no-search').data('href', window.lastFilter);
});


$('.refinement-bar').on('click', '.header-bar button.button-close', function (e) {
  var url = $('.product-refinement-no-search').data('href');
  if (!url) {
    closeButton();
    return;
  }
  window.params = url.split('?')[1];
  if (window.matchMedia('(max-width: 1023px)').matches) {
    window.filterType = '';
    e.preventDefault();
    if (!!window.lastFilter && url !== window.lastFilter) {
      $.spinner().start();
      $.ajax({
        url: url,
        data: {
          page: $('.grid-footer').data('page-number'),
          selectedUrl: url
        },
        method: 'GET',
        success: function (response) {
          let result = parseResults(response, e);
          history.pushState({}, '', url);
          if (result) {
            $('.card-header button').removeClass('clicked');
            $('.card-body').find('.selected').parents('.refinement').find('.card-header button').addClass('clicked');
            $('.view-more-less').text('View More');
            $('.refinement-category').removeClass('viewmore');
            // customscrollbar();
            viewmoreshow();
            if ($(window).width() < 1023 && $('.refinement-wrapper .refinements').height() > $('.refinement-wrapper').height()) {
              $('.collapsible-xl.refinement .card-body').height($('.refinement-wrapper .refinements').height());
            }
            closeButton();
          }
          $.spinner().stop();
        },
        error: function () {
          $.spinner().stop();
          closeButton();
        }
      });
    } else {
      closeButton();
    }
  } else {
    closeButton();
  }
});

$('.container').on('click', '.show-previous button', function (e) {
  isPreviousLoading = 1;
  e.stopPropagation();
  var showPreviousURL = $(this).data('url');
  var data = {
    selectedUrl: showPreviousURL,
    previous: true
  };
  e.preventDefault();
  $('.grid-previous').eq(0).removeClass('invisible');
  $('.grid-previous').eq(0).find('.show-previous').addClass('invisible');
  $('.grid-previous').eq(0).spinner().start();
  $.ajax({
    url: showPreviousURL,
    data: data,
    method: 'GET',
    success: function (response) {
      $('.grid-previous').eq(0).replaceWith(response);
      // eslint-disable-next-line newline-per-chained-call

      updateSortOptions(response);
      lazyLoad.hydrateLazyLoadedImages();
      // update review teasers
      productGridTeasers.addReviews();
      $('.grid-previous').eq(0).spinner().stop();
      isPreviousLoading = 0;
      $('body').css('overflow', 'auto');
    },
    error: function () {
      $('.grid-previous').eq(0).spinner().stop();
      $('body').css('overflow', 'auto');
    }
  });
});

// Hide the price error in case close button is clicked
$('body').on('click', '.search-results .price-no-results button.close-button', function () {
  $('.search-results .price-no-results').addClass('d-none');
});
/**
 * Show a spinner inside a given element
 */
function addGridSpinner() {
  var $placement = $('.grid-footer').eq($('.grid-footer').length - 1);
  $placement.append('<div class="spinner"><div class="dot1"></div><div class="dot2"></div></div>');
}

/**
 *
 * Remove existing spinner
 *
 */
function removeGridSpinner() {
  var $spinner = $('.grid-footer')
    .eq($('.grid-footer').length - 1)
    .find('.spinner');
  $spinner.remove();
}

base.showMore = function () {
  // Show more products
  $('.container').on('click', '.show-more button', function (e) {
    isLoading = 1;
    e.stopPropagation();
    var showMoreUrl = $(this).data('url');
    var data = {
      selectedUrl: showMoreUrl,
      showMore: true
    };
    e.preventDefault();
    // $.spinner().start();
    addGridSpinner();
    $('body').css('overflow', 'auto');
    $(this).trigger('search:showMore', e);
    var $this = $(this);
    $.ajax({
      url: showMoreUrl,
      data: data,
      method: 'GET',
      success: function (response) {
        $('.grid-footer')
          .eq($('.grid-footer').length - 1)
          .replaceWith(response);
        // eslint-disable-next-line newline-per-chained-call
        updateSortOptions(response);
        lazyLoad.hydrateLazyLoadedImages();
        // update review teasers
        productGridTeasers.addReviews();
        // $.spinner().stop();
        removeGridSpinner();
        persistentWishlist.markProductArrWishlisted();
        isLoading = 0;
        $('body').css('overflow', 'auto');
        $('body').trigger('category:set:model:view');
        $('body').trigger('adobeTagManager:productArrayUpdate', $this);
      },
      error: function () {
        // $.spinner().stop();
        removeGridSpinner();
        $('body').css('overflow', 'auto');
      }
    });
  });
};

base.filter = function () {
  // Display refinements bar when Menu icon clicked
  $('.container').on('click', 'button.filter-results', function () {
    $('.refinement-bar').addClass('d-block');
    $('.refinement-bar').siblings().attr('aria-hidden', true);
    $('.refinement-bar').closest('.row').siblings().attr('aria-hidden', true);
    $('.refinement-bar').closest('.tab-pane.active').siblings().attr('aria-hidden', true);
    $('.refinement-bar').closest('.container.search-results').siblings().attr('aria-hidden', true);
    $('.refinement-bar .close').focus();
    $('body').addClass('fixed filters-visible modal-open');
    $('.refinement-bar').find(':focusable').first().focus();
    if ($(window).width() < 1023 && $('.refinement-wrapper .refinements').height() > $('.refinement-wrapper').height()) {
      $('.collapsible-xl.refinement .card-body').height($('.refinement-wrapper .refinements').height());
    }
    formField.findInsiders($('.refinement-bar'));
  });
};

base.closeRefinements = function () {
  // Refinements close button
  $('.container').on('click', '.refinement-bar button.close', function () {
    closeButton();
  });
};

base.resize = function () {
  // removed the Close refinement bar and hide modal background if user resizes browser to fix android mobile input field issue.
  $(window).resize(function () {
    if (typeof stickyrefinement === 'function') {
      stickyrefinement();
    }
    // customscrollbar();
    viewmoreshow();
    thumbWidth();
    $('body').trigger('category:category:refinement:clone');
  });
};

base.sort = function () {
  // code changes for adding sort parameter to url when moving out of the page for scroll persistence
  $('.container').on('change', '[name=sort-order]', function (e) {
    e.preventDefault();
    e.stopPropagation();
    window.params = this.value.split('?')[1];
    $.spinner().start();
    $(this).trigger('search:sort', this.value);
    var $this = $(this);
    $.ajax({
      url: this.value,
      data: {
        selectedUrl: this.value
      },
      method: 'GET',
      success: function (response) {
        parseResults(response, e);
        $('.card-header button').removeClass('clicked');
        $('.card-body').find('.selected').parents('.refinement').find('.card-header button').addClass('clicked');
        $('.view-more-less').text('View More');
        $('.refinement-category').removeClass('viewmore');
        // customscrollbar();
        viewmoreshow();
        if ($(window).width() < 1023 && $('.refinement-wrapper .refinements').height() > $('.refinement-wrapper').height()) {
          $('.collapsible-xl.refinement .card-body').height($('.refinement-wrapper .refinements').height());
        }
        $('body').trigger('adobeTagManager:productArrayUpdate', $this);
        // update review teasers
        productGridTeasers.addReviews();
        $.spinner().stop();
      },
      error: function () {
        $.spinner().stop();
      }
    });
  });
};
base.changeStore = pdpInstoreInventory.changeStore; // change store modal
base.selectStoreWithInventory = pdpInstoreInventory.selectStoreWithInventory; // select store from modal
base.triggerClickOnEnter = pdpInstoreInventory.triggerClickOnEnter; // trigger click with enter key
/**
 * updates the store info on select of store from the modal.
 */
base.selectStoreFromSearch = function () {
  $('body').on('store:selectedSearch', function (e, data) {
    $.spinner().start();
    updateStoreContent(data); // if event invoke is from modal, updates store html
    removeSelectStoreModal(); // close modal on successful update
    $.spinner().stop();
  });
};

// if we go back to a "next pagination" of products, scroll the product into view
function scrollToLastViewed() {
  const url = new URL(document.location);
  const isNPage = Number(url.searchParams.get('start')) > 0;
  const itemID = utils.getCookie('itemID');
  const element = $('div[data-tile-pid=' + itemID + ']')[0];
  let intersectionObserver = null;

  if (!isNPage) return Promise.resolve();

  if ('scrollRestoration' in history) {
    history.scrollRestoration = 'manual';
  }

  return new Promise(function(res) {
    if (!element) res();


    intersectionObserver = new IntersectionObserver((entries) => {
      let [entry] = entries;
      if (entry.isIntersecting) {
        // mitigate scroll 'ease-in'
        setTimeout(() => {
          res();
        }, 200);
      }
      // fallback resolve in weird situations, like no-scroll
      setTimeout(() => {
        res();
      }, 500);
    });

    // start observing to capture scroll 'end'
    intersectionObserver.observe(element);

    element.scrollIntoView({ behavior: 'smooth' });
  }).then(() => {
    if (element) {
      intersectionObserver.unobserve(element);
    }
  });
}

/* Javascript to load on page load*/
$(document).ready(function () {
  scrollToLastViewed()
    .then(() => {
      $.spinner().stop();
    });

  // make sure product grid doesn't add slick to search results on desktop to avoid image zoom
  if ($(window).width() > 1023.99) {
    var productGridTile = document.querySelector('#product-search-results .image-container');
    if (productGridTile) {
      productGridTile.classList.add('prevent-slick');
    }
  }
  // add review
  productGridTeasers.addReviews();
  if (document.querySelector('.refinement-bar')) {
    window.onpopstate = function () {
      window.location = location.href;
    };
  }
});

module.exports = base;
