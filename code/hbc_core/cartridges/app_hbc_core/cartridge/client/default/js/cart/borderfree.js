'use strict';
var utilhelper = require('../components/utilhelper');

var borderfree = {
  /**
   * @function
   * @description Clear minicart after successful bf checkout
   */
  handleEmptyCart: function () {
    function bfResponse(e) {
      if (typeof e.data === 'string' && e.data.indexOf('OrderCreation-') >= 0) {
        // clear cart contents
        $('.minicart-quantity').html('0');
        $('.mini-cart-content').remove();
        var countTitle = $('a.minicart-count').attr('data-defaultcount');
        $('a.minicart-count').attr('title', countTitle);
        $('a.minicart-count').attr('aria-label', countTitle);
        // Set Tracing Data
        var url = $('.bfx-tracking-url').attr('data-bfx-trackingurl');
        $.ajax({
          url: url,
          type: 'get',
          dataType: 'json',
          success: function (data) {
            if (data.success || data.pending) {
              $('body').trigger('adobe:borderFreeOrderConfirm', data.trackingData);
            }
          }
        });
      }
    }
    if (window.addEventListener) {
      // For standards-compliant web browsers
      window.addEventListener('message', bfResponse, false);
    } else {
      window.attachEvent('onmessage', bfResponse);
    }
  },
  handleEnvoyOrderID: function () {
    var checkForOrderSuccessTimer = setInterval(function () {
      // If the success or pending URL is triggered
      // and the tracking frame's src contains "orderTracking" means
      // the transaction is complete and we can parse the E4X ID
      if (
        $('#__frame').length > 0 &&
        $('#__frame').attr('src') &&
        $('#__frame')
          .attr('src')
          .search(/Success|Pending/) != -1 &&
        $('#__trackingframe').length > 0 &&
        $('#__trackingframe').attr('src') &&
        $('#__trackingframe')
          .attr('src')
          .search(/orderTracking/) != -1
      ) {
        // Clear the timer
        clearInterval(checkForOrderSuccessTimer);
        // Extract orderID from the iframes src attribute
        var idRegex = new RegExp('orderId=(.*)');
        var orderID = idRegex.exec($('#__trackingframe').attr('src'))[1];
        console.debug('E4X ID IS: ' + orderID);

        var url = $('.set-e4x').attr('data-set-e4x');
        //'OrderCreation-BFXOrderTrackingData
        $.ajax({
          url: url,
          type: 'get',
          data: { E4XNUM: orderID },
          success: function () {
            // do nothing;
          }
        });
      }
    }, 2000);
  }
};

module.exports = borderfree;
