'use strict';

$('.chanel-categories')
  .find('.cat-link')
  .on('click', function (e) {
    e.preventDefault();
    $(this).closest('.dropdown-item').siblings('li').removeClass('show').find('li').removeClass('show');
    $(this).closest('.dropdown-item').toggleClass('show');
  });
