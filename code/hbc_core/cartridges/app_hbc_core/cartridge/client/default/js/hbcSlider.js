var debounce = require('lodash/debounce');

const PREFER_NATIVE_SCROLL_ON_TOUCHSCREENS = ['category-slider', 'recommendation-products', 'primary-images', 'primary-images-set'];

const isObject = obj => obj && typeof obj === 'object';
const isArray = arr => arr && arr instanceof Array;

/**
 * Returns all unique values of an array, based on a provided comparator function.
 * @param {Array} arr An array of objects
 * @param {Function} fn A comparison function
 * @returns {Array} An array of unique elements, as determined by the comparison function
 */
const uniqueElementsBy = (arr, fn) => {
  return arr.reduce((acc, v) => {
    if (!acc.some(x => fn(v, x))) acc.push(v);
    return acc;
  }, []);
};

/**
 * Merges two Slick 'responsive' config objects together
 * @param {Array} target A responsive object in the slick config
 * @param {Array} source A responsive object in the slick config
 */
const mergeBreakpoints = (target, source) => {
  const mergedSources = uniqueElementsBy(source.concat(target), (a, b) => a.breakpoint === b.breakpoint);
  return mergedSources.map(sourceElement => {
    let targetElement = target.find(el => sourceElement.breakpoint === el.breakpoint);
    if (targetElement) {
      return {
        breakpoint: sourceElement.breakpoint,
        settings: Object.assign({}, sourceElement.settings, targetElement.settings)
      };
    }
    return sourceElement;
  });
};

/**
 * Returns a configuration object, based on the defaultSliderConfig, any property of which can be overridden
 * @param {Object} overrides Any overrides to the default slick carousel configuration
 * @return {Object} A slick carousel config object
 */
const getSliderConfig = overrides => {
  const defaultConfigCopy = Object.assign({}, defaultSliderConfig);
  if (!isObject(overrides)) {
    return defaultConfigCopy;
  }
  const mergedConfig = Object.assign(defaultConfigCopy, overrides);
  if (!isArray(overrides.responsive)) {
    return mergedConfig;
  }
  // If we're here, we need to merge the responsive config
  mergedConfig.responsive = mergeBreakpoints(defaultConfigCopy.responsive, mergedConfig.responsive);
  return mergedConfig;
};

/**
 * The default Slick carousel config object.
 */
const defaultSliderConfig = {
  slidesToShow: 1,
  slidesToScroll: 1,
  speed: 150,
  swipe: true,
  draggable: true,
  swipeToSlide: false,
  touchThreshold: 10,
  dots: true,
  arrows: true,
  centerMode: false,
  infinite: false,
  focusOnSelect: false,
  variableWidth: false,
  accessibility: true,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        swipeToSlide: true
      }
    }
  ]
};

/**
 * Configurations for individual slick carousels.
 */
const sliderData = {
  'recommendation-products': getSliderConfig({
    slidesToShow: 5,
    slidesToScroll: 5,
    responsive: [
      { breakpoint: 1260, settings: { slidesToShow: 4, slidesToScroll: 4 } },
      { breakpoint: 1024, settings: { dots: false, arrows: false, slidesToShow: 3, slidesToScroll: 3 } },
      { breakpoint: 544, settings: { dots: false, arrows: false, slidesToShow: 1, slidesToScroll: 1 } }
    ]
  }),
  'primary-images': getSliderConfig(),
  'shop-the-look': getSliderConfig(),
  'primary-images-set': getSliderConfig(),
  'quick-view-images': getSliderConfig(),
  'edit-product': getSliderConfig(),
  'category-slider': getSliderConfig({
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      { breakpoint: 1024, settings: { dots: false, arrows: false, slidesToShow: 3, slidesToScroll: 3 } },
      { breakpoint: 544, settings: { dots: false, arrows: false, slidesToShow: 1, slidesToScroll: 1 } }
    ]
  }),
  'homepage-slider': getSliderConfig({
    responsive: [{ breakpoint: 1024, settings: { arrows: false } }]
  }),
  'homepage-cat-slider': getSliderConfig({
    slidesToShow: 5,
    slidesToScroll: 5,
    responsive: [
      { breakpoint: 1024, settings: { dots: false, arrows: false, slidesToShow: 3, slidesToScroll: 1 } },
      { breakpoint: 544, settings: { dots: false, arrows: false, slidesToShow: 1, slidesToScroll: 1 } }
    ]
  })
};

var cartReccommendationSliderInit = function () {
  let hbcRecSliders = document.querySelectorAll('.cart-page-content .hbc-slider');
  for (let i = 0; i < hbcRecSliders.length; i++) {
    let recSlider = hbcRecSliders[i];
    if (
      (recSlider.classList.contains('recommendation-products') || recSlider.classList.contains('homepage-cat-slider')) &&
      recSlider.classList.contains('slick-initialized')
    ) {
      $(recSlider).slick('unslick');
    }
    if (recSlider.classList.contains('recommendation-products')) {
      $(recSlider).slick(
        Object.assign(
          {},
          {
            slidesToShow: 5,
            slidesToScroll: 5,
            responsive: [
              { breakpoint: 1260, settings: { slidesToShow: 4, slidesToScroll: 4 } },
              { breakpoint: 1024, settings: { dots: false, arrows: false, slidesToShow: 3, slidesToScroll: 3 } },
              { breakpoint: 544, settings: { dots: false, arrows: false, slidesToShow: 1, slidesToScroll: 1 } }
            ]
          }
        )
      );
    } else if (recSlider.classList.contains('homepage-cat-slider')) {
      $(recSlider).slick(
        Object.assign(
          {},
          {
            slidesToShow: 5,
            slidesToScroll: 5,
            responsive: [
              { breakpoint: 1024, settings: { dots: false, arrows: false, slidesToShow: 3, slidesToScroll: 1 } },
              { breakpoint: 544, settings: { dots: false, arrows: false, slidesToShow: 1, slidesToScroll: 1 } }
            ]
          }
        )
      );
    }
  }
};

/**
 * Largescreen?
 * @returns {boolean} True for largescreen, otherwise false.
 */
function isDesktop() {
  return window.innerWidth >= 1024;
}

/**
 * Generates a unique ID.
 * @return {String} a unique ID.
 */
const generateUUID = () =>
  ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c => (c ^ (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))).toString(16));

/**
 * Handles clicks on the dots on touchscreens.
 * @param {Event} evt The event object
 */
const handleTouchscreenDotClick = evt => {
  const targetSlide = document.querySelector(`[data-dot-id="${evt.target.parentElement.id}"]`);
  if (!targetSlide) {
    return;
  }
  const targetSlideContainer = targetSlide.parentElement;

  targetSlideContainer.scrollTo({
    left: targetSlide.offsetLeft - targetSlideContainer.offsetWidth / 2, // Try to center the clicked up slide.
    behavior: 'smooth'
  });
};

/**
 * Builds dot indiators for touchscreen sliders.
 * @param {Element} slider The slider wrapper element
 */
const buildNativeScrollSliderDots = slider => {
  const list = document.createElement('ul');
  ['touchscreen-slider-dots', 'slick-dots'].forEach(str => list.classList.add(str));
  const slides = Array.from(slider.children);
  slides.forEach(slide => (slide.dataset.dotId = `touchscreen-slider-dot-${generateUUID()}`));
  const dots = slides.map((slide, index) => `<li id="${slide.dataset.dotId}"><button type="button">${index + 1}</button></li>`).join('');
  list.innerHTML = dots;
  list.addEventListener('click', handleTouchscreenDotClick);
  slider.insertAdjacentElement('afterend', list);
};

/**
 * Sets up an intersection observer to update the active dot state on touchscreens.
 * @param {Element} slider A slider container element
 */
const observeNativeScrollSlides = slider => {
  const handleIntersect = entries => {
    entries.forEach(entry => {
      const dot = document.getElementById(entry.target.dataset.dotId);
      if (entry.isIntersecting) {
        dot.classList.add('slick-active');
      } else {
        dot.classList.remove('slick-active');
      }
    });
  };
  const observer = new IntersectionObserver(handleIntersect, {
    root: slider,
    rootMargin: '0px',
    threshold: [0.5, 0.99]
  });
  const slides = Array.from(slider.children);
  slides.forEach(slide => observer.observe(slide));
};

/**
 * Sets up touchscreen slider functionality.
 * @param {String} sliderType The key for the type of slider.
 */
const decorateNativeScrollSlider = sliderType => {
  let sliders;
  if (sliderType) {
    sliders = document.querySelectorAll(`.${sliderType}`);
  }
  if (!sliders.length) {
    return;
  }
  sliders.forEach(slider => {
    if (slider.classList.contains('touchscreen-slider-initialized')) {
      return;
    }
    if (slider.dataset.touchscreenSliderDots !== 'false') {
      buildNativeScrollSliderDots(slider);
      observeNativeScrollSlides(slider);
    }
    slider.classList.add('touchscreen-slider-initialized');
  });
};

const initResponsiveSlider = (slider, sliderType) => {
  const $slider = $(slider);
  const determineResponsiveSliderStatus = function () {
    if (isDesktop()) {
      if ($slider.hasClass('slick-initialized')) {
        $slider.slick('unslick');
      }
      return;
    }
    if (!$slider.hasClass('slick-initialized')) {
      $slider.slick(sliderData[sliderType]);
    }
  };

  // Make sure no other call try to incorrectly intialize slick
  const desktopInitializationObserver = new MutationObserver(function () {
    if ($slider.hasClass('slick-initialized') && (isDesktop() || (PREFER_NATIVE_SCROLL_ON_TOUCHSCREENS.includes(sliderType) && window.isTouchscreen()))) {
      $slider.slick('unslick');
    }
  });
  desktopInitializationObserver.observe($slider.get(0), { attributes: true });

  // Update on resize and orientation change
  const debounceDetermineResponsiveSliderStatus = debounce(determineResponsiveSliderStatus, 50);
  window.addEventListener('resize', debounceDetermineResponsiveSliderStatus);
  window.addEventListener('orientationchange', debounceDetermineResponsiveSliderStatus);

  // Set it up
  determineResponsiveSliderStatus();
};

var hbcSliderInit = function (sliderType, slider) {
  if (sliderType && PREFER_NATIVE_SCROLL_ON_TOUCHSCREENS.includes(sliderType) && window.isTouchscreen()) {
    decorateNativeScrollSlider(sliderType);
    return;
  }
  let $sliders;
  if (typeof sliderType === 'undefined') {
    $.each(sliderData, function (sliderTypeVal, sliderDataVal) {
      if (PREFER_NATIVE_SCROLL_ON_TOUCHSCREENS.includes(sliderTypeVal) && window.isTouchscreen()) {
        decorateNativeScrollSlider(sliderTypeVal);
        return;
      }
      $sliders = $('.' + sliderTypeVal + ':not(.slick-initialized)');

      if (!$sliders.is('.async-slider')) {
        $sliders.slick(sliderDataVal);
      }
    });
  } else {
    if (typeof slider === 'undefined') {
      $sliders = $('.' + sliderType + ':not(.slick-initialized)');
    } else {
      $sliders = slider;
    }

    $sliders.each(function () {
      if (this.classList.contains('responsive-slider')) {
        initResponsiveSlider(this, sliderType);
      } else {
        $(this).slick(sliderData[sliderType]);
      }
    });
  }
};

module.exports = {
  hbcSliderInit: hbcSliderInit,
  cartReccommendationSliderInit: cartReccommendationSliderInit
};
