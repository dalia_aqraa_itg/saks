'use strict';
var formHelpers = require('base/checkout/formErrors');
var clientSideValidation = require('../components/clientSideValidation');
var floatLabel = require('../floatLabel');

/**
 * updates the instore pick up person form
 * @param {Object} order - the order model
 * @param {Object} instorepickup - the form data
 */
function updatePickUpPersonFormValues(order, instorepickup) {
  var instorepickupForm = instorepickup;
  if (!instorepickupForm) return;

  var form = $('form.pickup-form');
  if (!form) return;
  if (instorepickupForm.fullName.htmlValue) {
    $('input[name$=_fullName]', form).val(instorepickupForm.fullName.htmlValue);
  }
  if (instorepickupForm.phone.htmlValue) {
    $('input[name$=_phone]', form).val(instorepickupForm.phone.htmlValue);
  }
  if (instorepickupForm.email.htmlValue) {
    $('input[name$=_email]', form).val(instorepickupForm.email.htmlValue);
  }
}

/**
 * updates the html with the pick up person details
 * @param {Object} order - the order model
 */
function updatePickUpPersonSummary(order) {
  var pickUpPerson = null;
  var $container = $('#instorepick-details');
  if (order && order.personInfoMarkFor) {
    pickUpPerson = JSON.parse(order.personInfoMarkFor);
    var $fullName = $container.find('.fullname');
    var $phone = $container.find('.phone');
    var $email = $container.find('.email');
    $fullName.text(pickUpPerson.fullName);
    $phone.text(pickUpPerson.phone);
    $email.text(pickUpPerson.email);
  }
}

/**
 * updates the html the url response
 * @param {Object} defer - defer object
 * @param {Object} data - data from response
 */
function updateFormResponse(defer, data) {
  var formSelector = '.pickup-form';
  // highlight fields with errors
  if (data.error) {
    if (data.fieldErrors.length) {
      data.fieldErrors.forEach(function (error) {
        if (Object.keys(error).length) {
          formHelpers.loadFormErrors(formSelector, error);
        }
      });
      defer.reject(data);
    }

    if (data.serverErrors && data.serverErrors.length) {
      $.each(data.serverErrors, function (index, element) {
        base.createErrorNotification(element); //eslint-disable-line
      });
      defer.reject(data);
    }
  } else {
    $('body').trigger('checkout:updateCheckoutView', { order: data.order, customer: data.customer, instorepickup: data.instorepickup });

    defer.resolve(data);
  }
}
module.exports = {
  // event on submit of instore pcik up form
  submitInstorePickup: function (defer) {
    //
    // Clear Previous Errors
    //
    formHelpers.clearPreviousErrors('.pickup-form');

    //
    // Submit the Shipping Address Form
    //
    var formSelector = '.pickup-form';
    var form = $(formSelector);
    var instoreFormData = form.serialize();

    $('body').trigger('checkout:serializeInstore', {
      form: form,
      data: instoreFormData,
      callback: function (data) {
        if (data) {
          instoreFormData = data;
        }
      }
    });

    // disable the next:Payment button here
    //$('body').trigger('checkout:disableButton', '.next-step-button button');
    $.ajax({
      url: form.attr('action'),
      type: 'post',
      data: instoreFormData,
      success: function (data) {
        // enable the next:Payment button here
        $('body').trigger('checkout:enableButton', '.next-step-button button');
        updateFormResponse(defer, data);
        if ($('#customer-addresses:visible').length > 0 && $('#customer-addresses:visible').hasClass('saks-only')) {
          // open address entry modal if user has one and only us address in address book
          if (
            $('#address-auto-open').length > 0 &&
            ($('#address-auto-open').attr('data-open-adressentry') === 'true' ||
              ($('#address-auto-open').attr('data-firstadrress-country') !== undefined &&
                $('#address-auto-open').attr('data-firstadrress-country') !== '' &&
                $('#address-auto-open').attr('data-firstadrress-country') !== 'US')) &&
            $('form.shipping-form').find('.address-add-registered .customer-addresses-section').length > 0
          ) {
            $('form.shipping-form').find('.address-add-registered .customer-addresses-section').trigger('click');
          }
        }
        //Clear the CVV field value for the registered user
        if ($('.saved-payment-security-code.cvvNumField').length > 0) {
          $('.saved-payment-security-code.cvvNumField').each(function () {
            $(this).val('');
          });
        }
        if ($('.billing-addr-form').length > 0) {
          clientSideValidation.updatePoPatterWithCountry($('.billing-addr-form'));
          clientSideValidation.checkValidationOnAjax($('.billing-form'), true, true);
          clientSideValidation.checkValidationOnAjax($('.credit-card-form'), true, true);
          // changes done to udpate the state drop down
          if ($('select[name $= "billing_addressFields_country"]').val() !== null) {
            var country = $('select[name $= "billing_addressFields_country"]').val();
            var billingState;
            var stateField = $('select[name $= "billing_addressFields_states_stateCode"]');
            var countryRegion = $('.countryRegion').data('countryregion');
            if (!countryRegion) {
              return;
            }
            var regions = countryRegion[country].regions;
            var regionsLabel = countryRegion[country].regionLabel;
            stateField.prev('label').text(regionsLabel);
            var postalField = $('.billing-form').find('.billingZipCode');
            // if Country is UK, display the text field for state.
            if (country === 'UK') {
              var optionArr = [];
              for (var stateCode in regions) {
                // eslint-disable-line
                optionArr.push('<option id="' + stateCode + '" value="' + stateCode + '">' + regions[stateCode] + '</option>');
              }
              // Update the State Field
              stateField.html(optionArr.join(''));
              if ($('.site-locale').val() === 'fr_CA') {
                postalField.prev('label').text(postalField.data('zipcode-fr'));
              } else {
                postalField.prev('label').text(postalField.data('zipcode-label'));
              }

              if ($('.orderBillState').length && $('.orderBillState').val() !== 'null') {
                billingState = $('.orderBillState').val();
              } else {
                billingState = $('.js-state-code-input').val();
              }
              $('.state-drop-down').addClass('d-none');
              $('.state-input').removeClass('d-none');
              $('form[name=dwfrm_billing]').find('.js-state-code-input').val(billingState);
              $('.state-drop-down').find('.form-group').removeClass('required');
              $('.state-drop-down').find('.billingState').prop('required', false);
              $('.state-drop-down').find('.billingState').removeClass('is-invalid');
            } else {
              // Generate the State Options
              var optionArr = [];
              for (var stateCode in regions) {
                // eslint-disable-line
                optionArr.push('<option id="' + stateCode + '" value="' + stateCode + '">' + regions[stateCode] + '</option>');
              }
              // Update the State Field
              stateField.html(optionArr.join(''));
              if ($('.site-locale').val() === 'fr_CA') {
                postalField.prev('label').text(postalField.data('zipcode-fr'));
              } else if (country === 'CA') {
                postalField.prev('label').text(postalField.data('zipcode-ca'));
              } else {
                postalField.prev('label').text(postalField.data('zipcode-label'));
              }

              if ($('.orderBillState').length && $('.orderBillState').val() !== 'null') {
                billingState = $('.orderBillState').val();
              } else {
                billingState = $('.billingState').val();
              }

              $('.state-drop-down').removeClass('d-none');
              $('.state-input').addClass('d-none');
              $('form[name=dwfrm_billing]').find('select[name$=_stateCode]').val(billingState);
              $('.state-drop-down').find('.form-group').addClass('required');
              $('.state-drop-down').find('.billingState').prop('required', true);
            }
            clientSideValidation.updatePoPatterWithCountry(form);
          }
          floatLabel.resetFloatLabel();
          // to validate the country field and state
          $('select[name$=_country]', $('form.billing-form')).trigger('focusout');
          $('select[name$=_stateCode]', $('form.billing-form')).trigger('focusout');
        }
        // adobe tag manager
        var steps = {};
        steps.step = data.skipShipping ? 'payment' : 'shipping address';
        steps.items = data.order && data.order.items && data.order.items.items && data.order.items.items.length ? data.order.items.items : [];
        steps.optIn = false;
        /*if ($('.enableBillingButton').length > 0 && $('.enableBillingButton').val() === 'false') {
                    $('.card.payment-form').find('.submit-payment').attr('disabled', 'disabled');
                } else {
                    $('body').trigger('checkout:enableButton', '.next-step-button button');
                }*/
        $('body').trigger('checkout:enableButton', '.next-step-button button');
        $('body').trigger('adobeTagManager:checkoutStepChange', steps);
      },
      error: function (err) {
        // enable the next:Payment button here
        $('body').trigger('checkout:enableButton', '.next-step-button button');
        if (err.responseJSON && err.responseJSON.redirectUrl) {
          window.location.href = err.responseJSON.redirectUrl;
        }
        // Server error submitting form
        defer.reject(err.responseJSON);
      }
    });
    return defer;
  },
  // update content
  updateStorePickUpPersonInfo: function (order, instorepickup) {
    updatePickUpPersonFormValues(order, instorepickup);
    updatePickUpPersonSummary(order);
  }
};
