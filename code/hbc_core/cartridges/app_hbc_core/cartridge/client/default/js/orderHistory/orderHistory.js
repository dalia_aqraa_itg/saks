'use strict';

var hbcTooltip = require('../tooltip');
var orderCancel = require('../orderStatus/orderCancel');

module.exports = function () {
  $('body').on('change', '.order-history-selectbox', function (e) {
    var $ordersContainer = $('.order-list-container');
    $ordersContainer.empty();
    $.spinner().start();
    $('.order-history-select').trigger('orderHistory:sort', e);
    $.ajax({
      url: e.currentTarget.value,
      method: 'GET',
      success: function (data) {
        $ordersContainer.html(data);
        $.spinner().stop();
      },
      error: function (err) {
        if (err.responseJSON.redirectUrl) {
          window.location.href = err.responseJSON.redirectUrl;
        }
        $.spinner().stop();
      }
    });
  });

  $('body').on('change', '.pending-orders', function (e) {
    if ($(this).is(':checked')) {
      var url = $(this).attr('data-url') + '&pendingOrders=true';
    } else {
      var url = $(this).attr('data-url') + '&pendingOrders=false';
    }
    var $ordersContainer = $('.order-list-container');
    $ordersContainer.empty();
    $.spinner().start();
    $('.order-history-select').trigger('orderHistory:sort', e);
    $.ajax({
      url: url,
      method: 'GET',
      success: function (data) {
        $ordersContainer.html(data);
        $.spinner().stop();
      },
      error: function (err) {
        if (err.responseJSON.redirectUrl) {
          window.location.href = err.responseJSON.redirectUrl;
        }
        $.spinner().stop();
      }
    });
  });

  $('body').on('click', '.view-more-orders button', function (e) {
    e.preventDefault();
    $.spinner().start();
    $.ajax({
      url: $(this).data('action'),
      data: { prevOrder: $(this).data('prevorder') },
      method: 'GET',
      success: function (data) {
        $('.view-more-orders').replaceWith(data);
        $.spinner().stop();
      },
      error: function () {
        $.spinner().stop();
      }
    });
  });

  $('body').on('click', '.order-detail-link', function (e) {
    $('.order-cannot-cancel').addClass('d-none');
    $('.order-cancel-oms-fail').addClass('d-none');
    e.preventDefault();
    $.spinner().start();
    var orderNo = $(this).data('orderno');
    var $detailsContainer = $('.order-details-section-' + orderNo);
    if ($.trim($detailsContainer.html()).length) {
      $detailsContainer.empty();
      $(this).removeClass('active');
      $(this).removeClass('svg-svg-49-minus-thin');
      $.spinner().stop();
    } else {
      $detailsContainer.empty();
      $.spinner().start();
      $(this).addClass('active');
      $(this).addClass('svg-svg-49-minus-thin');
      $.ajax({
        url: $(this).attr('href'),
        method: 'GET',
        data: {
          ajax: true
        },
        success: function (data) {
          if (!data.error) {
            $detailsContainer.html(data);
            hbcTooltip.tooltipInit();
          }
          $.spinner().stop();
        },
        error: function () {
          $.spinner().stop();
        }
      });
    }
  });

  orderCancel();
};
