'use strict';

var scrollAnimate = require('base/components/scrollAnimate');

/**
 * appends params to a url
 * @param {string} data - data returned from the server's ajax call
 * @param {Object} button - button that was clicked for email sign-up
 */
function displayMessage(data, button) {
  $.spinner().stop();
  var status;
  $('.home-email-signup').find('.error-container .error').remove();
  $('.email-signup-input').removeClass('error');
  if (data.success) {
    status = 'alert-success';
  } else {
    status = 'alert-danger';
  }

  if ($('.email-signup-message-footer').length === 0) {
    $('div.email-signup-input').append('<div class="email-signup-message-footer"></div>');
  }
  if (data.success) {
    $('.email-signup-message-footer').append(
      '<div class="d-flex align-items-center ' +
        status +
        '"><span class="svg-svg-95-check-green-thick-dims svg-svg-95-check-green-thick success-img"></span>' +
        data.msg +
        '</div>'
    );
    $('.email-signup-input').addClass('success');
    setTimeout(function () {
      $('.email-signup-message-footer').remove();
      $('.email-signup-input').removeClass('success');
      $('.email-signup-input').find('input').val(''); // remove the email prefill as he has already submitted
      button.removeAttr('disabled');
    }, 3000);
  } else {
    $('.home-email-signup')
      .find('.error-container')
      .append('<span class="error">' + data.msg + '</span>');
    $('.email-signup-input').addClass('error');
    button.removeAttr('disabled');
  }
}

module.exports = function () {
  $(window).scroll(function () {
    if ($(window).scrollTop() > 100) {
      $('.back-to-top').addClass('active');
    } else $('.back-to-top').removeClass('active');
  });

  $('.back-to-top').click(function () {
    scrollAnimate();
  });

  $('.subscribe-email').on('click', function (e) {
    e.preventDefault();
    var url = $(this).data('href');
    var button = $(this);
    var subscribeForm = $(this).closest('form');
    var emailId = $('input[name=hpEmailSignUp]').val();
    $.spinner().start();
    $(this).attr('disabled', true);
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: {
        emailId: emailId
      },
      success: function (data) {
        displayMessage(data, button);
        if (data.success) {
          $('body').trigger('adobeTagManager:emailsignupmodal', 'footer');
        }
      },
      error: function (err) {
        displayMessage(err, button);
      }
    });
  });

  $('.liveChat-btn').on('click', function () {
    if (window.sf_widget && window.sf_widget.utils) {
      try {
        if (!sessionStorage.sf_within_geo) {
          window.sf_widget.utils.isWithinGeo().then(function (data) {
            if (data === true) {
              $('.stylist-chat-btn').prop('disabled', false);
              sessionStorage.setItem('sf_within_geo', true);
            } else {
              $('.stylist-chat-btn').prop('disabled', true);
            }
          });
        }
      } catch (err) {
        console.log(err); //eslint-disable-line
      }
    }

    $('body').toggleClass('chat-overlay');
    $('.live-chat-content').toggleClass('d-none');
    $('.live-chat-view-2').toggleClass('d-none');
    $('.chat-img').toggleClass('d-none');
    $('.chat-img-cross').toggleClass('d-none');
    $('.page').addClass('non-scrollable');

    if (!$('.chat-img-cross').hasClass('d-none')) {
      $('.live-chat-view-2').removeClass('d-none');
      $('.live-chat-view-3').addClass('d-none');
      $('.live-chat-view-4').addClass('d-none');
    }

    if ($('.embeddedServiceHelpButton .helpButton .uiButton.helpButtonDisabled').length > 0) {
      $('.chat-service-links .customer-service-chat-link').addClass('live-chat-offline');
    } else if ($('.embeddedServiceHelpButton .helpButton .uiButton.helpButtonEnabled').length > 0) {
      $('.chat-service-links .customer-service-chat-link').addClass('live-chat-online');
    }
  });

  $('.stylist-chat-btn').on('click', function () {
    $('.live-chat-content').toggleClass('d-none');
    $('.live-chat-view-2').toggleClass('d-none');
    $('.chat-img').toggleClass('d-none');
    $('.chat-img-cross').toggleClass('d-none');
    $('.page').removeClass('non-scrollable');
  });

  $('.customer-service-button').on('click', function () {
    $('.live-chat-view-2').toggleClass('d-none');
    $('.live-chat-view-3').toggleClass('d-none');
  });

  $('.customer-service').on('click', function () {
    $('.live-chat-view-2').toggleClass('d-none');
    $('.live-chat-view-3').toggleClass('d-none');
  });

  $('.customer-service-call-link').on('click', function () {
    $('.live-chat-view-3').toggleClass('d-none');
    $('.live-chat-view-4').toggleClass('d-none');
  });

  $('.customer-service-call-link-saks').on('click', function () {
    $('.live-chat-view-2').toggleClass('d-none');
    $('.live-chat-view-3').toggleClass('d-none');
  });

  $('.call-customer-service').on('click', function () {
    $('.live-chat-view-3').toggleClass('d-none');
    $('.live-chat-view-4').toggleClass('d-none');
  });

  $('.call-customer-service-saks').on('click', function () {
    $('.live-chat-view-2').toggleClass('d-none');
    $('.live-chat-view-3').toggleClass('d-none');
  });

  $('.chat-img-cross-black').on('click', function () {
    $('body').toggleClass('chat-overlay');
    $('.chat-img-cross').addClass('d-none');
    $('.chat-img').removeClass('d-none');
    $('.live-chat-content').addClass('d-none');
    $('.page').removeClass('non-scrollable');
  });

  $('body').on('click', '.customer-service-chat-link', function () {
    if ($('.embeddedServiceHelpButton .helpButton .uiButton.helpButtonEnabled').length > 0) {
      $('.live-chat-content').toggleClass('d-none');
      $('.live-chat-view-3').toggleClass('d-none');
      $('.chat-img').toggleClass('d-none');
      $('.chat-img-cross').toggleClass('d-none');
      $('.embeddedServiceHelpButton .helpButton .uiButton.helpButtonEnabled').trigger('click');
    }
  });

  $('body').on('click', '#bfx-cc-btn', function (e) {
    e.preventDefault();
    var bfxCountryCode = $('.bfx-cc-countries').val();
    var bfxURL = $('.bfxcountrycodeURL').attr('data-bfxcountrycode');
    $.ajax({
      url: bfxURL,
      type: 'post',
      dataType: 'json',
      data: {
        bfxCountryCode: bfxCountryCode
      },
      success: function () {
        // Just Redirect
        return true;
      }
    });
  });
};
