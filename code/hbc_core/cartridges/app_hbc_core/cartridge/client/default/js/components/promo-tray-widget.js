'use strict';

var drawer;
var items;
var overlay;
var scrollholder;
var arrowB;
var arrowF;
var promoWidth;
var promoCount;
var cartFlag;
var transitionEvent = 'transitionend';
var MAX_TITLE_LEN = 25 - 1;
var MAX_DISCLAIMER_LEN = 34 - 1;
var MAX_DESC_LEN = 88 - 1;
// iphone x
var devicesDisabledSwipeUp = ['Mobile/15A372'];

var passiveSupported = false;
var formFields = require('../formFields/formFields');

// test to determine whether browser supports passive event listeners for scroll events
try {
  var options = Object.defineProperty({}, 'passive', {
    get: function () {
      passiveSupported = true;
    }
  });

  window.addEventListener('test', options, options);
  window.removeEventListener('test', options, options);
} catch (err) {
  passiveSupported = false;
}

/**
 * disable buttons via property, mouse and tab behavior. returns original set to maintain chainability
 * @param {Object} buttons disabled if url not available
 * @returns {Object} styled element.
 */
function disableButtons(buttons) {
  var $btns = $(buttons);

  if ($btns.length === 0) {
    return $btns;
  }

  $btns.each(function (i, el) {
    var $el = $(el);
    var canDisable = $el.is('button');
    var origIndex = $el.attr('tabindex');

    if (undefined !== origIndex) {
      $el.data('origIndex', origIndex);
    }

    // disabled class adds pointer-events: none;
    // -1 makes the button untabble
    if (canDisable) {
      $el.prop('disabled', true);
    } else {
      $el.addClass('disabled').attr('tabindex', -1);
    }
  });

  return $btns;
}

/**
 * coupon code is updated at session level
 * @param {string} code disabled if url not available
 */
function updateSessionCoupons(code) {
  if (typeof code !== 'string' || code === '' || !window.sessionStorage.hasOwnProperty.hasOwnProperty('APPLIED_COUPONS')) {
    return;
  } // eslint-disable-line

  if (!Array.isArray(window.sessionStorage.hasOwnProperty.APPLIED_COUPONS)) {
    window.sessionStorage.hasOwnProperty.APPLIED_COUPONS = [code];
  } else if (window.sessionStorage.hasOwnProperty.APPLIED_COUPONS.indexOf(code) === -1) {
    window.sessionStorage.hasOwnProperty.APPLIED_COUPONS.push(code);
  } // eslint-disable-line
}
/**
 * @param {Object} el element to be styled
 * @returns {Object} styled element.
 */
function outerWidth(el) {
  var style = getComputedStyle(el);

  return parseInt(style.width, 10) + parseInt(style.marginLeft, 10) + parseInt(style.marginRight, 10);
}

/**
 * @param {Object} el to be faded on page load
 */
function fadeIn(el) {
  var last = +new Date();
  el.style.opacity = 0; // eslint-disable-line
  el.style.display = 'block'; // eslint-disable-line

  (function tick() {
    el.style.opacity = +el.style.opacity + (new Date() - last) / 400; // eslint-disable-line

    if (+el.style.opacity < 0.5) {
      requestAnimationFrame(tick);
    } else {
      el.style.opacity = ''; // eslint-disable-line
    }
  })();
}
/**
 * @param {Object} el to be faded out on page load
 */
function fadeOut(el) {
  var last = +new Date();
  el.style.opacity = 0.5; // eslint-disable-line

  (function tick() {
    el.style.opacity = +el.style.opacity - (new Date() - last) / 400; // eslint-disable-line

    if (+el.style.opacity > 0) {
      requestAnimationFrame(tick);
    } else {
      el.style.opacity = 0; // eslint-disable-line
      el.style.display = 'none'; // eslint-disable-line
    }
  })();
}
/**
 * Sets the message to the button
 * @param {Object} btn to be styled
 */
function addCouponSuccessMessage(btn, appliedText) {
  btn.classList.add('success');
  var couponapplied = $('.promo-tray').data('couponapplied');
  btn.innerHTML = '<i class="icon-success"></i>' + couponapplied; // eslint-disable-line
}
/**
 * Sets the width of the promotional modal
 * @param {Object} data used to design promo tray modal
 * @returns {Object} style being set for the element promo tray modal
 */
var sanitizeData = function (data) {
  var nodes = document.createElement('div');
  var offers;
  var offerLen;
  var i;
  nodes.innerHTML = data;

  offers = nodes.getElementsByClassName('pd-offer');
  if (offers !== null) {
    for (i = 0, offerLen = offers.length; i < offerLen; i++) {
      var node = offers[i];
      var title = node.querySelector('.title');
      var details = node.querySelector('.details');
      var disclaimer = node.querySelector('.disclaimer');
      var coupon = node.querySelector('.coupon');
      var code;
      var url;
      var titleVal;
      var detailsVal;
      var appliedText;
      var btn = node.querySelector('button');

      if (btn === null) {
        btn = document.createElement('button');
      }
      if (title == null || details == null) break;

      titleVal = title.textContent;
      detailsVal = details.textContent;
      // enforce maximum lengths
      title.textContent = titleVal.slice(0, MAX_TITLE_LEN);
      details.textContent = detailsVal.slice(0, MAX_DESC_LEN);
      if (disclaimer !== null) {
        disclaimer.textContent = disclaimer.textContent.slice(0, MAX_DISCLAIMER_LEN);
      }

      if (coupon !== null) {
        code = coupon.getAttribute('data-couponcode');
        appliedText = coupon.getAttribute('data-appliedtext');

        if (code === null) {
          url = '#';
        } else {
          // enforce a correct coupon code URL
          url = $('.promo-tray').data('promodrawer-addcoupon');
        }

        btn.setAttribute('class', 'coupon');
        btn.setAttribute('href', url);
        //btn.textContent = 'Apply'; // CTA per reqs

        if (
          window.sessionStorage.hasOwnProperty('APPLIED_COUPONS') && // eslint-disable-line
          Array.isArray(window.sessionStorage.APPLIED_COUPONS) &&
          window.sessionStorage.APPLIED_COUPONS.indexOf(code) !== -1
        ) {
          addCouponSuccessMessage(btn, appliedText);
        }
      } else {
        btn.setAttribute('class', '');
        var icon = document.createElement('i');
        icon.setAttribute('class', 'icon-arrow-right');
        btn.appendChild(icon); // Append CTA icon to button
      }

      // disable coupon addition if a valid code isn't present
      if (url === '#') {
        disableButtons(btn);
      }
    }
  } else nodes.innerHTML = '';

  return nodes;
};
/**
 * function for slick arrow in carousel
 */
/**
 * function setting width of promo tray
 */
function showTransitionEnd() {
  fadeIn(overlay);
  drawer.removeEventListener(transitionEvent, showTransitionEnd);
}
/**
 * hiding of promo tray on page load
 */
function hideTransitionEnd() {
  fadeOut(overlay);
  drawer.querySelector('ul.toggleVis').style.display = 'none';
  drawer.removeEventListener(transitionEvent, hideTransitionEnd);
}

/**
 * Promo Tray Slider
 */
function promotraySlider() {
  var $promotray = $('.toggleVis');

  if ($promotray.hasClass('slick-initialized')) {
    $promotray.slick('unslick');
  }

  $promotray.slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: true,
    arrows: true,
    infinite: false,
    responsive: [
      {
        breakpoint: 1023,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false,
          arrows: false,
          centerMode: true,
          accessibility: true
        }
      }
    ]
  });

  $('.slick-dots li').find('button').attr('tabindex', 0);
  $promotray.on('afterChange', function () {
    $promotray.find('.slick-list').addClass('center-content');
    if ($promotray.find('.slick-slide').first().hasClass('slick-active')) {
      $promotray.find('.slick-list').removeClass('center-content');
    }
  });
}
/**
 * function showing promo tray
 */
function showPromos() {
  if (drawer !== null) {
    drawer.className = 'promo-drawer-open';
    fadeIn(overlay);
    drawer.querySelector('ul.toggleVis').style.display = 'block';
    if (transitionEvent && drawer) {
      drawer.addEventListener(transitionEvent, showTransitionEnd);
    }

    promotraySlider();
    $('body').addClass('modal-open');
    $('a.bay-shop-details').on('click', function (e) {
      e.preventDefault();
      var contentURL = $(this).attr('href');
      showDetailsDialog(contentURL);
    });

    // Hide the chat widget if it's open
    if ($('body').hasClass('chat-overlay')) {
      $('.liveChat-btn').trigger('click');
    }
    formFields.findInsiders($('#promo-drawer'));

    // Adding description to links for ADA
    var totalOffers = $('.scroll-holder ul').find('li.pd-offer').length;
    $('.scroll-holder ul')
      .find('li.pd-offer')
      .each(function (i) {
        $(this)
          .find('.offer-main .text-bg')
          .attr('id', i + 1 + '-of-' + totalOffers + '-offers');
        $(this)
          .find('.offer-main .bottom-section a')
          .attr('aria-describedby', i + 1 + '-of-' + totalOffers + '-offers');
      });

    // Focus on the first focusable item in the drawer
    $('#promo-drawer .scroll-holder .slick-current *:focusable:first').focus();
  }
}
/**
 * function hiding promo tray
 */
function hidePromos() {
  if (drawer !== null) {
    if (transitionEvent && drawer) {
      drawer.addEventListener(transitionEvent, hideTransitionEnd);
    }

    drawer.className = '';
    fadeOut(overlay);
    $('body').removeClass('modal-open');
    if (cartFlag === true && document.getElementById('wrapper').classList.contains('pt_cart')) {
      location.reload(true);
    }
    drawer.querySelector('.drawer-trigger').focus();
  }
}
/**
 * updates the currently 'active' offer for mobile snap-to-scroll / arrows
 * @param {Object} direction to be styled
 */
function updateActive(direction) {
  var startPos = scrollholder.scrollLeft;
  var newLeft = startPos + (direction === 'right' ? -promoWidth : promoWidth);

  // animate scrollLeft property
  /* eslint-disable */
  (function animate() {
    var last = +new Date();
    var maxPos = scrollholder.scrollWidth - scrollholder.offsetWidth;
    var diff = Math.abs(startPos - newLeft);

    if (newLeft > maxPos) {
      newLeft = maxPos;
    }
    (function tick() {
      var step = (diff * (new Date() - last)) / 400;
      scrollholder.scrollLeft += startPos < newLeft ? step : -step;

      if (
        (startPos > newLeft && scrollholder.scrollLeft > newLeft && scrollholder.scrollLeft > 0) ||
        (startPos < newLeft && scrollholder.scrollLeft < newLeft && scrollholder.scrollLeft <= maxPos)
      ) {
        requestAnimationFrame(tick);
      } else {
        scrollholder.scrollLeft = newLeft;
      }
    })();
  })();
  /* eslint-enable */
}
/**
 * function for initializing events for displaying promo tray
 */
function initializeEvents() {
  var trigger;
  var startX;
  var startY;
  var startTime;
  var elapsedTime;
  var allowedTime = 300;
  var restraint = 100;
  var i = 0;
  var len;
  var touchStart = function (e) {
    var touchobj = e.changedTouches[0];
    startX = touchobj.pageX;
    startY = touchobj.pageY;
    startTime = new Date().getTime();
  };
  /* eslint-disable */
  var touchEnd = function (e) {
    var touchobj = e.changedTouches[0];
    var distX = touchobj.pageX - startX;
    var distY = touchobj.pageY - startY;
    var swipeDir = 'none';
    var i = 0;
    var len;

    elapsedTime = new Date().getTime() - startTime;

    if (elapsedTime <= allowedTime) {
      if (Math.abs(distY) >= 40 && Math.abs(distX) <= restraint) {
        swipeDir = distY < 0 ? 'up' : 'down';
      }
    }

    if (swipeDir !== 'none') {
      e.preventDefault();
    }

    if (swipeDir === 'up') {
      for (len = devicesDisabledSwipeUp.length; i < len; i++) {
        if (navigator.userAgent.indexOf(devicesDisabledSwipeUp[i]) !== -1) {
          e.preventDefault();
          return;
        }
      }
      showPromos();
    } else if (swipeDir === 'down') {
      hidePromos();
    }
  };
  /* eslint-enable */
  var toggleFn = function (e) {
    e.preventDefault();

    if (drawer.classList.contains('promo-drawer-open')) {
      hidePromos();
    } else {
      showPromos();
    }
  };

  // toggle drawer state
  trigger = document.getElementsByClassName('drawer-trigger');
  if (trigger !== null) {
    trigger[0].addEventListener('touchstart', touchStart, false);
    trigger[0].addEventListener('touchend', touchEnd, false);
    trigger[0].addEventListener('click', toggleFn, false);
  }
  /**
   * Button on click of which coupon to be added to cart
   * @param {Object} e direction to be styled
   */
  function btnClick(e) {
    var that = this;
    var href = this.getAttribute('href');
    /**
     * Append the data passed in the function to the coupon apply button
     * @param {Object} data direction to be styled
     */
    function processJSON(data) {
      var parent;
      var code;
      var appliedText;
      /* eslint-disable */
      data = typeof data === 'string' ? JSON.parse(data) : data;

      if (data && data.success) {
        parent = that.parentNode.parentNode;
        code = parent.getElementsByClassName('coupon')[0].getAttribute('data-couponcode');
        appliedText = parent.getElementsByClassName('coupon')[0].getAttribute('data-appliedtext');

        addCouponSuccessMessage(that, appliedText);
        updateSessionCoupons(code);
        cartFlag = true;
      }
      /* eslint-enable */
      var promoCoupon = {};
      promoCoupon.coupon = code;
      promoCoupon.status = data.success ? data.success : false;
      $('body').trigger('adobeTagManager:promoCodeEntered', promoCoupon);
    }

    if (this.classList.contains('coupon')) {
      e.preventDefault();

      if (this.classList.contains('success')) return;
      var form = {
        couponCode: this.getAttribute('data-couponcode')
      };
      $.ajax({
        url: href,
        method: 'POST',
        data: form,
        success: function (data) {
          return processJSON(data);
        },
        error: function () {
          $.spinner().stop();
        }
      });
    } else {
      window.location = href;
    }
  }

  // nav arrows
  var btns = drawer.querySelectorAll('.arrow-back, .arrow-forward');
  if (btns) {
    for (i = 0, len = btns.length; i < len; i++) {
      btns[i].addEventListener('click', function () {
        updateActive(this.classList.contains('arrow-back') ? 'right' : 'left');
      });
    }
  }
  // add coupon / shop now
  btns = drawer.querySelectorAll('.pd-offer button');
  var appliedCouponCodes = $('span.applied-couponcodes').data('couponcodes');
  var couponCodes;
  var couponapplied = $('.promo-tray').data('couponapplied');
  if (appliedCouponCodes !== null && appliedCouponCodes !== '') {
    couponCodes = appliedCouponCodes.split('|');
  }
  if (btns) {
    for (i = 0, len = btns.length; i < len; i++) {
      btns[i].addEventListener('click', btnClick);
      if (couponCodes !== undefined && couponCodes.length >= 1) {
        if (couponCodes.indexOf(btns[i].dataset.couponcode) !== -1) {
          btns[i].classList.add('success');
          btns[i].innerHTML = '<i class="icon-success"></i>' + couponapplied;
        }
      }
    }
  }
}
/**
 * @function
 * @description appends the parameters to the given url and returns the changed url
 * @param {string} url the url to which the parameters will be added
 * @param {Object} name of the anchor tag
 * @param {string} value the url to which the parameters will be added
 * @returns {Object} url element.
 */
function appendParamToURL(url, name, value) {
  // quit if the param already exists
  /* eslint-disable */
  if (url.indexOf(name + '=') !== -1) {
    return url;
  }
  value = decodeURIComponent(value);
  var separator = url.indexOf('?') !== -1 ? '&' : '?';
  return url + separator + name + '=' + encodeURIComponent(value);
  /* eslint-disable */
}
/**
 * @function
 * @description appends the parameters to the given url and returns the changed url
 * @param {string} url the url to which the parameters will be added
 * @param {Object} params to be appended to url
 * @returns {string} url element.
 */
function appendParamsToUrl(url, params) {
  var _url = url;
  $.each(params, function (name, value) {
    _url = appendParamToURL(_url, name, value);
  });
  return _url;
}
/**
 * @function
 * @description appends the parameters to the given url and returns the changed url
 * @param {string} url the url to which the parameters will be added
 * @returns response for the url passed
 */
function fetch(url) {
  /* eslint-disable */
  return new TPromise(function (resolve, reject) {
    try {
      // tag vendor is partially patching fetch into the window object, breaking IE11
      fetch(new Request(url), {
        credentials: 'include'
      }).then(function (response) {
        if (response.ok) {
          resolve(response.text());
        } else {
          reject(new Error(response.statusText));
        }
      });
    } catch (e) {
      $.ajax({
        url: url
      })
        .done(function (response) {
          resolve(response);
        })
        .fail(function (jxhr, status, err) {
          reject(new Error(err));
        });
    }
  });
  /* eslint-enable */
}

function showDetailsDialog(contentUrl) {
  var htmlString =
    '<!-- Modal -->' +
    '<div class="modal show" id="bay-promotray-dialog" class="bay-promotray-dialog" role="dialog" style="display: block;">' +
    '<div class="modal-dialog">' +
    '<!-- Modal content-->' +
    '<div class="modal-content"><button type="button" class="promo-tray-close close svg-svg-22-cross-dims svg-svg-22-cross" data-dismiss="modal" aria-label="Close"></button>' +
    '<div class="modal-body"></div>' +
    '</div>' +
    '</div>' +
    '</div>';
  $('body').append(htmlString);

  $.ajax({
    url: contentUrl,
    type: 'get',
    dataType: 'html',
    success: function (response) {
      $('#bay-promotray-dialog').find('.modal-body').html(response);
      $('#bay-promotray-dialog').modal('show');
      $('.promo-tray-close,.modal-backdrop').on('click', function () {
        $('#bay-promotray-dialog').modal('hide');
        $('#bay-promotray-dialog').remove();
      });
      $('#bay-promotray-dialog').on('hidden.bs.modal', function () {
        $('#bay-promotray-dialog').modal('hide');
        $('#bay-promotray-dialog').remove();
      });
    },
    error: function (err) {
      console.log('error : ' + err);
      $('#bay-promotray-dialog').remove();
    }
  });
}
exports.init = function () {
  var wrapper = document.getElementById('wrapper');
  var url;

  // don't display on sign-in / checkout
  if ($('.promoTrayEnabled').length > 0 && $('.promoTrayEnabled').val() == 'true') {
  } else {
    return;
  }

  overlay = document.createElement('div');
  overlay.id = 'promo-drawer-overlay';
  document.body.appendChild(overlay);
  // hide drawer on click anywhere outside
  overlay.addEventListener('click', hidePromos);

  drawer = document.createElement('div');
  drawer.id = 'promo-drawer';
  var offertrayheader = $('.promo-tray').data('offertraymsg');
  drawer.innerHTML = `
    <button type="button" id="drawer-tab" class="drawer-trigger">
        ${offertrayheader}
    </button>
    <div class="scroll-holder">
        <ul class="toggleVis custom-scrollbar"></ul>
    </div>`;
  document.body.appendChild(drawer);
  arrowB = drawer.getElementsByClassName('arrow-back')[0];
  arrowF = drawer.getElementsByClassName('arrow-forward')[0];
  items = drawer.getElementsByTagName('ul')[0];
  /**
   * @function
   * @Places the data into promo tray
   * @param {Object} data which needs to be appended in promo tray
   */
  function buildDrawer(data) {
    var safeData = sanitizeData(data);
    var offers;

    items.innerHTML = safeData.innerHTML || '';
    offers = document.getElementsByClassName('pd-offer');
    if (offers) {
      promoCount = offers.length;
      if (promoCount) {
        promoWidth = outerWidth(offers[0]);
      }
    }

    if (promoCount > 0) {
      // slide into view via CSS transition
      if (!$('.search-result-options').hasClass('is-fixed')) {
        document.getElementById('drawer-tab').classList.add('ready');
      }

      document.documentElement.classList.add('promo-drawer');
      initializeEvents();
    } else {
      document.getElementById('promo-drawer').classList.add('hide');
    }
  }

  // Check if product listings options is on page
  if ($('.product-listings-options').length > 0) {
    // set variables
    var headerOffset = -50;
    var plo = $('.product-listings-options').offset().top + headerOffset;
    var resultHits = $('.results-hits').offset().top;
    var $window = $(window);
    var lastScrollTop = $window.scrollTop();

    /**
     * @function
     * @description hide/show product listing options and promo drawer based on scroll position
     */
    function updatePLO() {
      // Bind scroll event on small screens
      if (window.matchMedia('(max-width: 767px)').matches) {
        // Fix the Product Listings Options to top of Page and Hide Promo Drawer
        var scrollTop = $window.scrollTop();

        if (scrollTop >= plo && !(scrollTop >= resultHits)) {
          $('.search-result-options').addClass('is-fixed');
          if (scrollTop > lastScrollTop) $('#drawer-tab').removeClass('ready');
          // scrolling down
          else $('#drawer-tab').addClass('ready'); // scrolling up
        } else {
          $('.search-result-options').removeClass('is-fixed');
          $('#drawer-tab').addClass('ready');
        }
      } else {
        $('#drawer-tab').addClass('ready');
      }
      // update lastScrollTop value
      lastScrollTop = scrollTop;
    }
    // Bind scroll
    $window.on('scroll', updatePLO);
    // Bind on page load
    if (window.matchMedia('(max-width: 767px)').matches) {
      // immediately run function on small screen sizes
      updatePLO();
    }
  }
  url = appendParamsToUrl($('.promo-tray').data('promodrawer-url'), { format: 'ajax' });
  $.ajax({
    url: url,
    type: 'get',
    dataType: 'html',
    success: function (response) {
      // $('.modal-body').html(response);
      buildDrawer(response);
    },
    error: function () {
      $('#consent-tracking').remove();
    }
  });
  /* customscrollbar(true);*/
};
