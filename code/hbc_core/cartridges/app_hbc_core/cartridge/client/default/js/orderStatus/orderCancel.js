'use strict';

module.exports = function () {
  /**
   * ORDER EXPIRY CHECK
   * event to check expiry of order
   */
  function checkForOrderExpiry(orderTimerEle, hideCTA) {
    var countDownDate = new Date(orderTimerEle.getAttribute('order-place-time')).getTime();
    var timeLeft = orderTimerEle.getAttribute('order-cancel-buffer');
    var now = new Date().getTime();
    var timeDiff = now - countDownDate;
    if (timeDiff >= parseInt(timeLeft) * 60 * 1000) {
      if (typeof hideCTA !== 'undefined' && hideCTA) {
        orderTimerEle.remove();
      }
      return true;
    }
    return false;
  }

  /**
   * CLICK EVENT
   * event handler triggered with the click of cancel order in order history
   */
  $('body').on('click', '.cancel-order-link', function (e) {
    e.preventDefault();
    var orderTimerEle = e.target.closest('.timer');
    var expiryCheck = false;
    var orderNo = orderTimerEle.getAttribute('order-no');
    var url = $(this).attr('href');
    if (url && url.length > 0 && orderNo && orderNo.length > 0) {
      url = url + '?orderNumber=' + orderNo;
    }
    if (expiryCheck) {
      $('.order-cannot-cancel').removeClass('d-none');
      $('html, body').animate(
        {
          scrollTop: 0
        },
        500
      );
    } else {
      $('.cancel-order-section-timer').removeClass('order-to-cancel');
      $(this).closest('.cancel-order-section-timer').addClass('order-to-cancel');
      $('.order-cannot-cancel').addClass('d-none');
      $('.order-cancel-oms-fail').addClass('d-none');
      $('#confirmCancelOrderModal').removeClass('cancelled');
      $('#confirmCancelOrderModal').modal('show');
      $('#confirmCancelOrderModal').attr('data-cancel-url', url);
      $('#confirmCancelOrderModal').addClass('timer');
      $('#confirmCancelOrderModal').attr('order-place-time', orderTimerEle.getAttribute('order-place-time'));
      $('#confirmCancelOrderModal').attr('order-cancel-buffer', orderTimerEle.getAttribute('order-cancel-buffer'));
    }
  });

  /**
   * CLICK EVENT
   * event handler triggered with the click of yes order cancel modal
   */
  $('body').on('click', 'button.cancel-order-modal-yes', function (e) {
    e.preventDefault();
    var orderTimerEle = document.querySelector('.order-to-cancel');
    var expiryCheck = false;
    if (expiryCheck) {
      $('#confirmCancelOrderModal').modal('hide');
      $('.order-cannot-cancel').removeClass('d-none');
      $('html, body').animate(
        {
          scrollTop: 0
        },
        500
      );
      return;
    }
    $('#confirmCancelOrderModal').spinner().start();
    var $orderCancelUrl = $('#confirmCancelOrderModal').attr('data-cancel-url');
    if ($orderCancelUrl && $orderCancelUrl.length > 0) {
      $.ajax({
        url: $orderCancelUrl,
        method: 'GET',
        data: {
          ajax: true
        },
        success: function (data) {
          var orderTimerEle = document.querySelector('.order-to-cancel');
          if (!data.error) {
            if ($(orderTimerEle).parents('.order-history-section').length > 0) {
              $(orderTimerEle).parents('.order-history-section').find('.order-status-to-change').html('Canceled');
            } else {
              $('.order-status-to-change').html('Canceled');
            }
            orderTimerEle.remove();
            $('#confirmCancelOrderModal').addClass('cancelled');
          } else {
            $('#confirmCancelOrderModal').modal('hide');
            //show error message order_cannot_cancel
            $('.order-cannot-cancel').removeClass('d-none');
            orderTimerEle.remove();
            $('html, body').animate(
              {
                scrollTop: 0
              },
              500
            );
          }
          $.spinner().stop();
        },
        error: function () {
          $.spinner().stop();
        }
      });
    } else {
      $.spinner().stop();
      $('#confirmCancelOrderModal').modal('hide');
      //show error message order_cannot_cancel
      $('.order-cannot-cancel').removeClass('d-none');
      $('html, body').animate(
        {
          scrollTop: 0
        },
        500
      );
    }
  });
};
