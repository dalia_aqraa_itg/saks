'use strict';

/**
 * This calculates the top position of an element so that
 * it essentially sticks to the bottom of its container
 * if it is too tall to be fully viewable in the viewport.
 * Otherwise, it sticks to the top, under the main nav.
 */
function calculateTop(el) {
  const mainNav = document.querySelector('.main-menu');
  const padding = 10;
  if (!el || window.innerWidth < 1024) {
    return;
  }

  let top = `calc(100vh - ${el.offsetHeight + padding}px)`;

  if (mainNav) {
    if (el.offsetHeight + mainNav.offsetHeight - padding < window.innerHeight) {
      top = `${mainNav.offsetHeight + padding}px`;
    }
  }
  el.style.top = top;
}

/**
 * Calculates the top value to apply to the thumbnail images.
 */
function calculateThumbnailsTop() {
  calculateTop(document.querySelector('.primary-thumbnails'));
}

/**
 * Calculates the top value to apply to the secondary column
 */
function calculatePDPSecondaryColumnTop() {
  calculateTop(document.querySelector('.product-secondary-section.pdp-standard'));
}

/**
 * Calculates the appropriate top values of all the sticky elements on the page.
 */
function calculateAllStickyElementTops() {
  calculatePDPSecondaryColumnTop();
  calculateThumbnailsTop();
}

/**
 * PDP Standard product sticky functionality.
 *
 **/
var pdpStdSticky = function () {
  const stickyColumn = document.querySelector('.product-secondary-section.pdp-standard');
  const mainNav = document.querySelector('.main-menu');
  const primaryThumbnails = document.querySelector('.primary-thumbnails');

  // Watch the main nav for state changes.
  if (mainNav) {
    const mainNavObserver = new MutationObserver(function (mutationsList) {
      for (let mutation of mutationsList) {
        if (mutation.type === 'attributes') {
          calculatePDPSecondaryColumnTop();
          calculateThumbnailsTop();
        }
      }
    });
    mainNavObserver.observe(mainNav, { attributes: true });
  }

  // Watch for the PDP details collapsible sections to be toggled or the HTML to be otherwise changed.
  if (stickyColumn) {
    const stickyColumnObserver = new MutationObserver(function (mutationsList) {
      for (let mutation of mutationsList) {
        if (mutation.type === 'attributes' && mutation.target.matches('button[aria-controls]')) {
          calculatePDPSecondaryColumnTop();
        } else if (mutation.type === 'childList') {
          calculatePDPSecondaryColumnTop();
        }
      }
    });
    stickyColumnObserver.observe(stickyColumn, {
      attributes: true,
      childList: true,
      subtree: true
    });
    // Do an intial calculation.
    calculatePDPSecondaryColumnTop();
  }

  if (primaryThumbnails) {
    const primaryThumbnailsObserver = new MutationObserver(calculateThumbnailsTop);
    primaryThumbnailsObserver.observe(primaryThumbnails, {
      attributes: true,
      childList: true,
      subtree: true
    });
    // Do an intial calculation.
    calculateThumbnailsTop();
  }

  // Bind window change events.
  if (primaryThumbnails || stickyColumn) {
    window.addEventListener('resize', calculateAllStickyElementTops);
    window.addEventListener('orientationchange', calculateAllStickyElementTops);
  }
};

module.exports = pdpStdSticky;
