'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
  processInclude(require('./wishlist/wishlist'));
  processInclude(require('./search/search'));
  processInclude(require('./product/wishlistHeart'));
  processInclude(require('./product/quickView'));
});
