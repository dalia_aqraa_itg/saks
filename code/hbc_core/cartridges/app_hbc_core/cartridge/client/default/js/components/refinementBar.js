'use strict';

var base = require('base/search/search');

/**
 * Checks to see if the screen is above a certian width.
 * @return {boolean} True if large screen, otherwise false
 */
function isDesktop() {
  return window.innerWidth > 1023;
}

var stickyrefinement = function () {
  const $tileDescriptions = $('.tile-descriptions');
  const refinementBar = document.querySelector('.refinement-bar');
  if (!$tileDescriptions.length || !refinementBar) {
    return;
  }
  window.addEventListener('scroll', () => {
    if (!isDesktop()) {
      return;
    }
    var stickytop = document.querySelector('.main-menu').offsetHeight + 20;
    var refinementtop = window.scrollY + stickytop;
    if (refinementtop >= $tileDescriptions.offset().top) {
      refinementBar.classList.add('stickyrefinement');
    } else {
      refinementBar.classList.remove('stickyrefinement');
    }
  });
};
/**
 * Sets the top and max-height values on the refinement bar so it displays properly beneath the header
 */
function setRefinementTop() {
  if (!isDesktop()) {
    return;
  }

  const mainMenu = document.querySelector('.main-menu');
  const pageHeader = document.querySelector('header');
  const refinementBar = document.querySelector('.refinement-bar');
  let visibleHeaderHeight;

  if (mainMenu && mainMenu.classList.contains('fixed')) {
    visibleHeaderHeight = mainMenu.offsetHeight;
  } else {
    visibleHeaderHeight = Math.max(pageHeader.offsetHeight - window.scrollY, 0);
  }

  refinementBar.style.maxHeight = `calc(100vh - ${visibleHeaderHeight}px)`;
  refinementBar.style.top = `${visibleHeaderHeight}px`;
}

if (document.querySelector('.refinement-bar') !== null) {
  const refinementBarObserver = new MutationObserver(mutations => {
    mutations.forEach(mutation => {
      if (mutation.target.classList.contains('stickyrefinement')) {
        setRefinementTop();
      } else {
        mutation.target.removeAttribute('style');
      }
    });
  });
  refinementBarObserver.observe(document.querySelector('.refinement-bar'), { attributes: true });
  refinementBarObserver.observe(document.querySelector('.main-menu'), { attributes: true });
}

// Scrolls the refinement bar to the top of the open category if necessary.
var $refinementBar = $('.refinement-bar');
if (!$refinementBar.data('scrollRefinementIntialized')) {
  $refinementBar.on('click', '.card-header button[aria-controls]', function (evt) {
    // Largescreen only.
    if (window.innerWidth < 1024) {
      return;
    }
    // Jank city here. Using a timeout to ensure the accordion functionality happens first
    window.setTimeout(function () {
      var card = evt.currentTarget.closest('.card');
      var bar = $refinementBar.get(0);

      if (bar.scrollTop > card.offsetTop) {
        bar.scrollTop = card.offsetTop;
      }
    }, 10);
  });
  $refinementBar.data('scrollRefinementIntialized', true);
}

base.stickyrefinement = stickyrefinement;
