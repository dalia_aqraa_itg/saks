'use strict';

var keyboardAccessibility = require('base/components/keyboardAccessibility');
var formFields = require('../formFields/formFields');

const DROPDOWN_NAV_ITEMS_SELECTOR = '.navbar-nav > .nav-item.dropdown:not(.disabled)';
const FOCUSABLE_MENU_ITEM_SELECTOR = '.main-menu .nav-link:focusable, .main-menu .dropdown-link:focusable, .close-menu .close-button';
var activeNavItem;
var hoverDelay = 250; // ms
var enterTimeout = null;
var exitTimeout = null;

/**
 * Opens the small screen menu
 * @param {*} e The Event object
 */
function openMobileMenu(e) {
  if (e) {
    e.preventDefault();
  }
  $(this).attr('aria-expanded', 'true');
  $('.main-menu').toggleClass('in');

  $('body').addClass('mobile-menu-open');
  $('.modal-background').on('click', closeMobileMenu);

  $('.main-menu').removeClass('d-none');
  $('.main-menu').attr('aria-hidden', 'false');
  $('.main-menu').attr('aria-modal', 'true');
  $('.main-menu').siblings().attr('aria-hidden', 'true');
  $('header').siblings().attr('aria-hidden', 'true');

  $('.main-menu').find('.menu-group').append($('#bfx-cc-wrapper'));
  $('.livechat-wrapper').hide();
  $('.cart-and-ipay').hide();
  $('.navbar-toggler').blur();
  window.setTimeout(function () {
    const visibleNavElements = getFocusableNavElements();
    if (visibleNavElements.length) {
      visibleNavElements[0].focus();
    }
  }, 100);

  addWindowChangeListeners();
  formFields.findInsiders($('#sg-navbar-collapse'));
}

/**
 * Closes the small screen menu
 * @param {*} e The Event object
 */
function closeMobileMenu(e) {
  if (e) {
    e.preventDefault();
  }
  $('.menu-toggleable-left').removeClass('in');

  $('body').removeClass('mobile-menu-open');
  $('.modal-background').off('click', closeMobileMenu);

  $('.navbar-toggler').attr('aria-expanded', 'false').focus();

  $('.main-menu').attr('aria-hidden', 'true');
  $('.main-menu').siblings().attr('aria-hidden', 'false');
  $('header').siblings().attr('aria-hidden', 'false');
  $('.dropdown').removeClass(' show');
  removeWindowChangeListeners();
  $('.cart-and-ipay').show();
}

/**
 * Binds keyboard events for the user popover menu
 */
function userPopoverKeyboardEvents() {
  var userLinkSelector = '.navbar-header .user .user-links';
  keyboardAccessibility(
    userLinkSelector,
    {
      40: function ($wrapper) {
        // down
        var $children = $wrapper.find('.popover').children('a, button');
        if (!$children.filter(':focus').length || $children.last().is(':focus')) {
          $children.first().focus();
        } else {
          $children.filter(':focus').next().focus();
        }
      },
      38: function ($wrapper) {
        // up
        var $children = $wrapper.find('.popover').children('a, button');
        if (!$children.filter(':focus').length || $children.first().is(':focus')) {
          $children.last().focus();
        } else {
          $children.filter(':focus').prev().focus();
        }
      },
      27: function () {
        // Escape
        hideUserPopover();
      }
    },
    function () {
      return $(userLinkSelector);
    }
  );
}

/**
 * Shows the user popover
 */
function showUserPopover() {
  var popover = document.querySelector('.navbar-header .user .popover');
  var toggler = document.querySelector('.navbar-header .user .header-account-drawer-toggle');
  if (!popover) {
    return;
  }
  popover.classList.add('show');
  toggler.setAttribute('aria-expanded', 'true');

  // Close the minicart popover
  $('.minicart .popover').removeClass('show');

  // Close Country Selector if Open
  if ($('.bfx-cc-expanded:visible') && $(window).width() >= 1023.99) {
    $('.bfx-cc-expanded').slideUp();
  }
}

/**
 * Hides the user popover
 */
function hideUserPopover() {
  var popover = document.querySelector('.navbar-header .user .popover');
  var toggler = document.querySelector('.navbar-header .user .header-account-drawer-toggle');
  if (!popover) {
    return;
  }
  popover.classList.remove('show');
  toggler.setAttribute('aria-expanded', 'false');
}

/**
 * Checks to see if the screen is above a certian width.
 * @return {boolean} True if large screen, otherwise false
 */
function isDesktop() {
  return window.innerWidth > 1023;
}

/**
 * Checks to see if the mouse moved outside of the currently active nav item.
 * Waits a beat to check user intent, and if we determine they've moved away from the menu, close it.
 * Bound to the mouseover event on the body, and only applicable to large screen treatment
 * @param {*} evt The Event Object
 */
function listenForNavExit(evt) {
  if (!activeNavItem || $.contains(activeNavItem, evt.target)) {
    clearTimeout(exitTimeout);
    return;
  }
  exitTimeout = window.setTimeout(function () {
    if (!activeNavItem || $.contains(activeNavItem, evt.target)) {
      return;
    }
    closeLargescreenMenu({ currentTarget: activeNavItem });
  }, hoverDelay);
}

/**
 * Checks the mouse position to see if it's close to the bottom edge of the window. If it is,
 * it triggers the active menu item to close.
 * @param {*} evt The event object. Bound to the mousemove event on the body when a menu is open
 */
function windowBottomEdgeCheck(evt) {
  var threshold = 12; // Pixels from the bottom edge that should trigger the menu to close
  if (!activeNavItem || window.innerHeight - evt.clientY > threshold) {
    return;
  }
  closeLargescreenMenu({ currentTarget: activeNavItem });
}

/**
 * Sizes the white underlay element that goes full width under a mega menu. Creates it if ncessary.
 * @param {jQuery} $navListElement The list element triggering the mega menu behind which to add the underlay.
 */
function setMenuUnderlay($navListElement) {
  var $underlay = $navListElement.find('> .dropdown-underlay');
  var $dropdownMenu = $navListElement.find('> .dropdown-menu');

  if (!$underlay.length) {
    $underlay = $('<div class="dropdown-underlay"></div>');
    $navListElement.append($underlay);
  }

  $underlay.height($dropdownMenu.get(0).scrollHeight + 'px');
}

/**
 * Sets the max-height value on the active mega menu so the user can always scroll
 * to the bottom of the menu, regardless of the state of the header it's attached to.
 */
function setMenuMaxHeight() {
  if (!activeNavItem || !isDesktop()) {
    return;
  }

  var dropdownMenu = activeNavItem.querySelector('.dropdown-menu');
  var underlay = activeNavItem.querySelector('.dropdown-underlay');
  var mainMenu = document.querySelector('.main-menu');
  var pageHeader = document.querySelector('header');
  var visibleHeaderHeight;

  if (mainMenu && mainMenu.classList.contains('fixed')) {
    visibleHeaderHeight = mainMenu.offsetHeight;
  } else {
    visibleHeaderHeight = Math.max(pageHeader.offsetHeight - window.scrollY, 0);
  }

  dropdownMenu.style.maxHeight = 'calc(100vh - ' + visibleHeaderHeight + 'px)';
  underlay.style.maxHeight = 'calc(100vh - ' + visibleHeaderHeight + 'px)';
}

/**
 * Swaps menu systems when a user goes from small screen to large screen
 */
function desktopMobileTransitionCheck() {
  if (isDesktop()) {
    closeMobileMenu();
    openLargescreenMenu();
  } else {
    closeLargescreenMenu({ currentTarget: activeNavItem });
    openMobileMenu();
  }
}

/**
 * Binds resize and orientation change listeners
 */
function addWindowChangeListeners() {
  window.addEventListener('resize', desktopMobileTransitionCheck);
  window.addEventListener('orientationchange', desktopMobileTransitionCheck);
}

/**
 * Releases resize and orientation change listeners
 */
function removeWindowChangeListeners() {
  window.removeEventListener('resize', desktopMobileTransitionCheck);
  window.removeEventListener('orientationchange', desktopMobileTransitionCheck);
}

/**
 * Opens a large screen mega menu treatment.
 * @param {*} evt The event object. The currentTarget property should be the list element in the main navigation
 */
function openLargescreenMenu(evt) {
  if (!isDesktop()) {
    return;
  }
  var $navListElement = $(evt.currentTarget);
  var $navLink = $navListElement.find('[data-toggle="dropdown"]');
  var $dropdownMenu = $navListElement.find('> .dropdown-menu');

  $navListElement.addClass('show');
  $dropdownMenu.addClass('show');
  $dropdownMenu.get(0).scrollTop = 0;
  formFields.addMenuOverlay();
  $navLink.attr('aria-expanded', 'true');
  document.body.classList.add('horizontal-scroll');
  activeNavItem = $navListElement.get(0);

  setMenuUnderlay($navListElement);
  setMenuMaxHeight();

  window.clearTimeout(enterTimeout);
  document.body.addEventListener('mouseover', listenForNavExit);
  document.body.addEventListener('mousemove', windowBottomEdgeCheck);
  window.addEventListener('scroll', setMenuMaxHeight);
  addWindowChangeListeners();
}

/**
 * Closes a large screen mega menu treatment.
 * @param {*} evt The event object. The currentTarget property should be the list element in the main navigation
 */
function closeLargescreenMenu(evt) {
  if (!isDesktop()) {
    return;
  }
  var $navListElement = $(evt.currentTarget);
  var $navLink = $navListElement.find('[data-toggle="dropdown"]');
  var $dropdownMenu = $navListElement.find('> .dropdown-menu');

  $navListElement.removeClass('show');
  $dropdownMenu.removeClass('show');
  $navLink.attr('aria-expanded', 'false');
  formFields.removeMenuOverlay();
  activeNavItem = null;
  document.body.classList.remove('horizontal-scroll');
  document.body.removeEventListener('mouseover', listenForNavExit);
  document.body.removeEventListener('mousemove', windowBottomEdgeCheck);
  window.removeEventListener('scroll', setMenuMaxHeight);
  removeWindowChangeListeners();
}

/**
 * Closes all open largescreen screen mega menu treatments, except the one determined to be currently active.
 * @param {*} evt The event object. The currentTarget property should be a list element in the main navigation
 */
function closeAllLargescreenMenus(evt) {
  if (!isDesktop() || !activeNavItem) {
    return;
  }
  $('.navbar-nav > li.dropdown.show').each(function () {
    // Skip over closing the current menu
    if (evt.currentTarget === this) {
      return;
    }
    closeLargescreenMenu({ currentTarget: this });
  });
}

/**
 * Handles tap events on the navbar on touchscreen capable devices.
 */
function bindNavTouchEvents() {
  if (!window.isTouchscreen()) {
    return;
  }

  let touchStarted = false;
  let cachedX = 0;
  let cachedY = 0;

  $('body')
    .on('touchstart', DROPDOWN_NAV_ITEMS_SELECTOR + ' > a, ' + DROPDOWN_NAV_ITEMS_SELECTOR + ' > span', function (e) {
      e.preventDefault();
      cachedX = e.pageX;
      cachedY = e.pageY;
      touchStarted = true;
      setTimeout(function () {
        if (!touchStarted && Math.abs(cachedX - e.pageX) < 3 && Math.abs(cachedY - e.pageY) < 3) {
          // If we get here, we can consider this a tap event.
          if (!isDesktop()) {
            return;
          }
          evt.preventDefault();
          var listItem = evt.currentTarget.parentNode;
          if (activeNavItem && activeNavItem === listItem) {
            closeLargescreenMenu({ currentTarget: listItem });
            evt.currentTarget.blur();
          } else {
            closeAllLargescreenMenus({ currentTarget: listItem });
            openLargescreenMenu({ currentTarget: listItem });
          }
        }
      }, 200);
    })
    .on('touchend touchcancel', function (e) {
      touchStarted = false;
    });
}

/**
 * Handles mouse events on the main nav.
 */
function bindNavMouseEvents() {
  $('body')
    .on('mouseenter', DROPDOWN_NAV_ITEMS_SELECTOR, function (evt) {
      enterTimeout = setTimeout(
        function () {
          closeAllLargescreenMenus(evt);
          openLargescreenMenu(evt);
        },
        activeNavItem ? 0 : hoverDelay
      );
    })
    .on('mouseleave', DROPDOWN_NAV_ITEMS_SELECTOR, function (evt) {
      if (!evt.currentTarget.classList.contains('show')) {
        clearTimeout(enterTimeout);
      }
    });
}

/**
 * Gets an array of focusable elements in the main nav
 * @returns {Array} array of focusable elements
 */
function getFocusableNavElements() {
  return $(FOCUSABLE_MENU_ITEM_SELECTOR).filter(':visible').toArray();
}

/**
 * Finds the next focusable element in the modal overlay and focuses that
 * @param {Element} currentItem DOM element representing the currenly focussed item.
 * @param {boolean} forward determines the direction of the next item to find. true is forward, next is back. Defaults to true.
 * @returns {Element} The next focusable element.
 */
function getNextFocusableItem(currentItem, forward) {
  var focusableItems = getFocusableNavElements();
  var currentItemIndex = Array.prototype.indexOf.call(focusableItems, currentItem);
  var nextItem;
  forward = typeof forward === 'boolean' ? forward : true;
  if (!focusableItems.length) {
    return currentItem;
  }
  if (currentItemIndex === -1) {
    return focusableItems[forward ? 0 : focusableItems.length - 1];
  }
  switch (currentItemIndex) {
    case 0:
      nextItem = forward ? focusableItems[1] : focusableItems[focusableItems.length - 1];
      break;
    case focusableItems.length - 1:
      nextItem = forward ? focusableItems[0] : focusableItems[currentItemIndex - 1];
      break;
    default:
      nextItem = forward ? focusableItems[currentItemIndex + 1] : focusableItems[currentItemIndex - 1];
      break;
  }
  return nextItem;
}

const keystrokeHandlers = {
  largescreen: {
    down: function (menuLink) {
      const menuItem = menuLink.parent();
      if (menuItem.hasClass('nav-item')) {
        // top level
        openLargescreenMenu({ currentTarget: menuItem });
        menuItem.find('ul > li > a').first().focus();
      } else {
        menuItem.removeClass('show').children('.dropdown-menu').removeClass('show');
        if (!(menuItem.next().length > 0)) {
          // if this is the last menuItem
          menuItem
            .parent()
            .parent()
            .find('li > a') // set focus to the first menuitem
            .first()
            .focus();
        } else {
          menuItem.next().children().first().focus();
        }
      }
    },
    up: function (menuLink) {
      const menuItem = menuLink.parent();
      if (menuItem.hasClass('nav-item')) {
        // top level
        closeLargescreenMenu({ currentTarget: menuItem });
      } else if (menuItem.prev().length === 0) {
        // first menuItem
        menuItem.parent().parent().removeClass('show').children('.nav-link').attr('aria-expanded', 'false');
        menuItem
          .parent()
          .children()
          .last()
          .children() // set the focus to the last menuItem
          .first()
          .focus();
      } else {
        menuItem.prev().children().first().focus();
      }
    },
    left: function (menuLink) {
      const menuItem = menuLink.parent();
      if (menuItem.hasClass('nav-item')) {
        // top level
        closeAllLargescreenMenus({ currentTarget: menuItem });
        const next = getNextFocusableItem(menuLink.get(0), false);
        next.focus();
      } else {
        menuItem.closest('.show').removeClass('show').closest('li.show').removeClass('show').children().first().focus().attr('aria-expanded', 'false');
      }
    },
    right: function (menuLink) {
      const menuItem = menuLink.parent();
      if (menuItem.hasClass('nav-item')) {
        // top level
        closeAllLargescreenMenus({ currentTarget: menuItem });
        const next = getNextFocusableItem(menuLink.get(0));
        next.focus();
      } else if (menuItem.hasClass('dropdown')) {
        menuItem.addClass('show').children('.dropdown-menu').addClass('show');
        $(this).attr('aria-expanded', 'true');
        menuItem.find('ul > li > a').first().focus();
      }
    },
    escape: function (menuItem) {
      var parentMenu = menuItem.hasClass('show') ? menuItem : menuItem.closest('li.show');
      parentMenu.children().first().focus();
      closeLargescreenMenu({ currentTarget: parentMenu });
    }
  },
  smallscreen: {
    down: function (menuLink) {
      const next = getNextFocusableItem(menuLink.get(0));
      next.focus();
    },
    up: function (menuLink) {
      const next = getNextFocusableItem(menuLink.get(0), false);
      next.focus();
    },
    right: function (menuLink) {
      const next = getNextFocusableItem(menuLink.get(0));
      next.focus();
    },
    left: function (menuLink) {
      const next = getNextFocusableItem(menuLink.get(0), false);
      next.focus();
    },
    escape: function () {
      closeMobileMenu();
    }
  }
};

/**
 * Finds the appropriate handler function in the keystrokeHandlers object and fires that function
 * @param {string} key The keystroke name that maps to a property name in the keystrokeHandlers object (e.g. 'down', 'up', 'escape')
 * @param {jQuery} menuItem The menu item which the keystroke was bound to.
 * @param {Element} scope The scope from which the function should be called.
 */
function keystrokeHandlerForLayout(key, menuItem, scope) {
  const handler = keystrokeHandlers[isDesktop() ? 'largescreen' : 'smallscreen'];
  if (typeof handler[key] === 'function') {
    handler[key].call(scope, menuItem);
  }
}

module.exports = function () {
  var headerBannerStatus = window.sessionStorage.getItem('hide_header_banner');
  $('.header-banner .close').on('click', function () {
    $('.header-banner').addClass('d-none');
    window.sessionStorage.setItem('hide_header_banner', '1');
  });

  if (!headerBannerStatus || headerBannerStatus < 0) {
    $('.header-banner').removeClass('d-none');
  }

  keyboardAccessibility(
    FOCUSABLE_MENU_ITEM_SELECTOR,
    {
      40: function (menuItem) {
        // down
        keystrokeHandlerForLayout('down', menuItem, this);
      },
      39: function (menuItem) {
        // right
        keystrokeHandlerForLayout('right', menuItem, this);
      },
      38: function (menuItem) {
        // up
        keystrokeHandlerForLayout('up', menuItem, this);
      },
      37: function (menuItem) {
        // left
        keystrokeHandlerForLayout('left', menuItem, this);
      },
      27: function (menuItem) {
        // escape
        keystrokeHandlerForLayout('escape', menuItem, this);
      }
    },
    function () {
      return $(this);
    }
  );

  $('.navbar-nav > .nav-item.dropdown:not(.disabled) [data-toggle="dropdown"]').on('click', function (e) {
    if (!isDesktop()) {
      $('.modal-background').show();
      if ($('.modal-background').length) {
        $('.modal-background').addClass('search-backdrop');
      }
      // copy parent element into current UL
      // copy navigation menu into view
      if ($(this).parent().hasClass('nav-item')) {
        $(this).parent().toggleClass('show');
        $(this).parent().find('li:first').focus();
        $(this).parent().siblings('li').removeClass('show').find('li').removeClass('show');
        $(this).parent().siblings('li').children('.nav-link').attr('aria-expanded', 'false');
        if ($(this).parent().hasClass('show')) {
          $(this).attr('aria-expanded', 'true');
        } else {
          $(this).attr('aria-expanded', 'false');
        }
      } else {
        $(this).siblings('ul').not($(this).next('ul')).removeClass('show');
        $(this).next('ul').toggleClass('show');
        $(this).parent().find('.dropdown-toggle').not($(this)).removeClass('opened');
        $(this).toggleClass('opened');
        $(this).next('ul').focus();
        if ($(this).siblings('ul').hasClass('show')) {
          $(this).attr('aria-expanded', 'true');
        } else {
          $(this).attr('aria-expanded', 'false');
        }
      }
      e.preventDefault();
    }
  });

  bindNavTouchEvents();
  bindNavMouseEvents();

  /* Changed traversing of the close button click event as per the structure change */

  $('.close-menu>.close-button').on('click', closeMobileMenu);

  $('body').on('click', '.close-button', function (e) {
    e.preventDefault();
    $('.navbar-nav').find('.top-category').detach();
    $('.navbar-nav').find('.nav-menu').detach();
    $('.navbar-nav').find('.show').removeClass('show');
    $('.menu-toggleable-left').removeClass('in');

    $('.main-menu').siblings().attr('aria-hidden', 'false');
    $('header').siblings().attr('aria-hidden', 'false');

    if ($('.modal-background').length) {
      $('.modal-background').removeClass('search-backdrop');
    }
    $('.modal-background').hide();
    $('.header-login').prepend($('#bfx-cc-wrapper'));
    $('.livechat-wrapper').show();
    $('.navbar').find('.borderfree-modal').remove();
    $('.cart-and-ipay').show();
  });

  $('.navbar-toggler').click(openMobileMenu);

  $('body').on('click', '#bfx-cc-wrapper', function (e) {
    e.stopPropagation();
    if ($(window).width() <= 1023.99) {
      $('.navbar').append('<span class="borderfree-modal"></span>');
      $('.main-menu').scrollTop(0);
      $('.bfx-cc-expanded').append('<span class="borderfree-close svg-svg-22-cross svg-svg-22-cross-dims"></span>');
      $('.main-menu').addClass('overflow-control');
      $('.navigation-section').find('.close-button').hide();
    }
  });

  $('body').on('click', '.bfx-cc-expanded', function (e) {
    e.stopPropagation();
  });

  $('body').on('click touchstart', '.borderfree-close', function (e) {
    e.stopPropagation();
    if ($(window).width() <= 1023.99) {
      $('.navbar').find('.borderfree-modal').remove();
      $('#bfx-cc-wrapper').find('.borderfree-close').remove();
      $('.bfx-cc-expanded').hide();
      $('.main-menu').removeClass('overflow-control');
      $('.navigation-section').find('.close-button').show();
    }
  });

  $('body').on('click', '#bfx-wm-switch-country a', function () {
    if ($(window).width() <= 1023.99) {
      $('.navbar-toggler').trigger('click');
      $('.navbar').append('<span class="borderfree-modal"></span>');
      $('.main-menu').addClass('overflow-control');
      $('.navigation-section').find('.close-button').hide();
    }
  });

  keyboardAccessibility(
    '.navbar-header .user',
    {
      40: function ($popover) {
        // down
        if ($popover.children('a').first().is(':focus')) {
          $popover.next().children().first().focus();
        } else {
          $popover.children('a').first().focus();
        }
      },
      38: function ($popover) {
        // up
        if ($popover.children('a').first().is(':focus')) {
          $(this).focus();
          $popover.removeClass('show');
        } else {
          $popover.children('a').first().focus();
        }
      },
      27: function () {
        // escape
        $('.navbar-header .user .popover').removeClass('show');
        $('.user').attr('aria-expanded', 'false');
      },
      9: function () {
        // tab
        $('.navbar-header .user .popover').removeClass('show');
        $('.user').attr('aria-expanded', 'false');
      }
    },
    function () {
      var $popover = $('.user .popover li.nav-item');
      return $popover;
    }
  );

  const userLinks = document.querySelector('.navbar-header .user-links');

  if (userLinks) {
    userLinks.addEventListener('mouseenter', showUserPopover);
    userLinks.addEventListener('mouseleave', hideUserPopover);
    userLinks.addEventListener('focusin', showUserPopover);
  }

  userPopoverKeyboardEvents();

  $('body').on('click', '#myaccount', function () {
    event.preventDefault();
  });

  if ($(window).width() >= 1023.99) {
    $('.dropdown-item.d-lg-none').remove();
  }

  if (isDesktop()) {
    const visibleNavItems = document.querySelectorAll('.navbar-nav > li.dropdown:not(.d-lg-none)');
    const last = visibleNavItems[visibleNavItems.length - 1];
    if (typeof last !== 'undefined') {
      last.classList.add('d-last-visible');
    }
  }

  if (!isDesktop()) {
    $('.dropdown-link.dropdown-toggle').each(function () {
      if ($(this).next('ul').children().length === $(this).next('ul').find('li.d-lg-block').length) {
        $(this).removeClass('dropdown-toggle').unbind('click');
      }
    });
  }
};
