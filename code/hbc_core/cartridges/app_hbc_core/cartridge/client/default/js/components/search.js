'use strict';

var debounce = require('lodash/debounce');
var keyboardAccessibility = require('base/components/keyboardAccessibility');
var endpoint = $('.suggestions-wrapper').data('url');
var minChars = 3;
var UP_KEY = 38;
var DOWN_KEY = 40;
var ESC_KEY = 27;
var DIRECTION_DOWN = 1;
var DIRECTION_UP = -1;
var formFields = require('../formFields/formFields');
var mobileSearchActive = false;

/**
 * Retrieves Suggestions element relative to scope
 *
 * @param {Object} scope - Search input field DOM element
 * @return {JQuery} - .suggestions-wrapper element
 */
function getSuggestionsWrapper(scope) {
  return $(scope).siblings('.suggestions-wrapper');
}

/**
 * Determines whether DOM element is inside the .search-mobile class
 *
 * @param {Object} scope - DOM element, usually the input.search-field element
 * @return {boolean} - Whether DOM element is inside  div.search-mobile
 */
function isMobileSearch(scope) {
  return !!scope.closest('.search-mobile');
}

/**
 * Captures touchmove events on the body. Used to prevent scrolling on iOS.
 * @param {Boolean} stop Whether to stop or start touchscreen scrolling.
 */
function toggleBodyScrolling(stop) {
  var freezeViewport = function (e) {
    e.preventDefault();
  };

  if (stop) {
    document.body.addEventListener('touchmove', freezeViewport, false);
  } else {
    document.body.removeEventListener('touchmove', freezeViewport, false);
  }
}

/**
 * Remove modal classes needed for mobile suggestions
 *
 */
function clearModals() {
  document.body.classList.remove('search-modal-open');
  $('header').siblings().attr('aria-hidden', 'false');
  $('.suggestions').empty();
  toggleBodyScrolling(false);
}

/**
 * Apply modal classes needed for mobile suggestions
 *
 * @param {Object} scope - Search input field DOM element
 */
function applyModals(scope) {
  if (!isMobileSearch(scope)) {
    return;
  }
  document.body.classList.add('search-modal-open');
  $('header').siblings().attr('aria-hidden', 'true');
  toggleBodyScrolling(true);
  setUpSuggestions(scope);
}

/**
 * Set up Suggestions panel
 *
 * @param {Object} scope - Search input field DOM element
 */
function setUpSuggestions(scope) {
  var $wrapper = getSuggestionsWrapper(scope);
  if (!$wrapper.html()) {
    $wrapper.html('<div class="suggestions"></div>');
  }
  $wrapper.find('.suggestions').addClass('modal').show();
  positionSuggestions(scope);
}

/**
 * Tear down Suggestions panel
 */
function tearDownSuggestions() {
  $('input.search-field').val('');
  clearModals();
  $('.search-mobile .suggestions').unbind('scroll');
  $('.suggestions-wrapper').empty();
}

/**
 * Toggle search field icon from search to close and vice-versa
 *
 * @param {string} action - Action to toggle to
 */
function toggleSuggestionsIcon(action) {
  var mobileSearchIcon = '.search-mobile button.';
  var iconSearch = 'fa-search';
  var iconSearchClose = 'fa-close';

  if (action === 'close') {
    $(mobileSearchIcon + iconSearch)
      .removeClass(iconSearch)
      .addClass(iconSearchClose)
      .attr('type', 'button');
  } else {
    $(mobileSearchIcon + iconSearchClose)
      .removeClass(iconSearchClose)
      .addClass(iconSearch)
      .attr('type', 'submit');
  }
}

/**
 * Determines whether the "More Content Below" icon should be displayed
 *
 * @param {Object} scope - DOM element, usually the input.search-field element
 */
function handleMoreContentBelowIcon(scope) {
  if ($(scope).scrollTop() + $(scope).innerHeight() >= $(scope)[0].scrollHeight) {
    $('.more-below').fadeOut();
  } else {
    $('.more-below').fadeIn();
  }
}

/**
 * Positions Suggestions panel on page
 *
 * @param {Object} scope - DOM element, usually the input.search-field element
 */
function positionSuggestions(scope) {
  var $suggestions;

  if (isMobileSearch(scope)) {
    $suggestions = getSuggestionsWrapper(scope).find('.suggestions');
    handleMoreContentBelowIcon(scope);

    // Unfortunately, we have to bind this dynamically, as the live scroll event was not
    // properly detecting dynamic suggestions element's scroll event
    $suggestions.scroll(function () {
      handleMoreContentBelowIcon(this);
    });
  }
}

let inactiveFormElements;

/**
 * Sets up the search view for small screens only.
 * @param {*} scope The search input element. Either DOM or jQuery.
 */
function activateMobileSearch(scope) {
  if (!isMobileSearch(scope) || mobileSearchActive) {
    return;
  }
  $('.search-mobile .cancel-button').addClass('active');
  $('body').addClass('overlay');
  toggleSuggestionsIcon('close');
  applyModals(scope);
  scope.focus();
  mobileSearchActive = true;
  // Disable other inputs on the page. This prevents mobile safari's form assistant from jumping out of the search from.
  if (inactiveFormElements && inactiveFormElements.length) {
    return;
  }
  inactiveFormElements = Array.prototype.filter.call(
    document.querySelectorAll('input:not(:disabled), button:not(:disabled), textarea:not(:disabled), select:not(:disabled)'),
    function (element) {
      return !element.closest('.search-form');
    }
  );
  inactiveFormElements.forEach(function (element) {
    element.disabled = true;
  });
}

/**
 * Sets up the search view for small screens only.
 */
function deactivateMobileSearch() {
  $('.search-mobile').removeClass('fixed');
  $('.mobile-rightsec .sticky-search').removeClass('search-icon-show');
  $('.search-mobile .cancel-button').removeClass('active');
  $('.search-mobile .search-field').val('');
  $('.search-mobile .reset-button').removeClass('d-sm-block');
  $('.suggestions').removeClass('modal');
  $('body').removeClass('overlay');
  clearModals();
  formFields.removeMenuOverlay();
  // Reenable form elements we deactivated when opening the mobile search.
  do {
    let element = inactiveFormElements.shift();
    if (element && element.disabled) {
      element.disabled = false;
    }
  } while (inactiveFormElements.length);
  mobileSearchActive = false;
}

function activateFloatingSearch() {
  if ($('.header').hasClass('fixed')) {
    $('.search-mobile').addClass('fixed');
    var headerHeight = $('#header-container').outerHeight();
    $('.search-mobile').css('top', headerHeight);
    $('.mobile-rightsec .sticky-search').addClass('search-icon-show');
    $('.search-mobile .search-field').focus();
  } else if ($('.main-menu').hasClass('fixed')) {
    $('.site-search').addClass('fixed');
    var headerHeight1 = $('.main-menu').outerHeight() + 10;
    $('.site-search').css('top', headerHeight1);
    $('.search .site-search.fixed').find('.search-field').focus();
  }
  $('body').addClass('overlay');
}

function dismissLargescreenSearch(e) {
  if (
    mobileSearchActive ||
    $('.suggestions').has(e.target).length ||
    e.target.classList.contains('search-field') ||
    e.target.classList.contains('sticky-search') ||
    e.target.classList.contains('reset-button')
  ) {
    return;
  }
  $('.suggestions').hide();
  $('.site-search').removeClass('search-border');
  $('body').removeClass('overlay');
  formFields.removeMenuOverlay();
  $('.site-search').removeClass('fixed');
  $('.site-search').css('top', 0);

  $('body').off('click touchend', dismissLargescreenSearch);
}

/**
 * Process Ajax response for SearchServices-GetSuggestions
 *
 * @param {Object|string} response - Empty object literal if null response or string with rendered
 *                                   suggestions template contents
 */
function processResponse(response) {
  var $suggestionsWrapper = getSuggestionsWrapper(this);

  $.spinner().stop();

  if (!(typeof response === 'object')) {
    // Hide the chat widget if it's open
    if ($('body').hasClass('chat-overlay')) {
      $('.liveChat-btn').trigger('click');
    }

    $suggestionsWrapper.html(response).show();
    $('.site-search').addClass('search-border');
    $('body').addClass('overlay');
    if (!mobileSearchActive) {
      formFields.addMenuOverlay();
    }
    positionSuggestions(this);

    applyModals(this);

    // Bind search modal dismissal on larger screens
    if (!isMobileSearch($suggestionsWrapper)) {
      $('body').on('click touchend', dismissLargescreenSearch);
    }

    // Trigger screen reader by setting aria-describedby with the new suggestion message.
    var suggestionsList = $('.suggestions .item');
    if ($(suggestionsList).length) {
      $('input.search-field').attr({
        'aria-describedby': 'search-result-count',
        'aria-controls': 'search-results'
      });
    }
  } else {
    // $suggestionsWrapper.hide();
    $('input.search-field').removeAttr('aria-describedby aria-controls');
  }
}

/**
 * Retrieve suggestions
 *
 * @param {Object} scope - Search field DOM element
 */
function getSuggestions(scope) {
  if ($(scope).val().length >= minChars) {
    $.spinner().start();
    $.ajax({
      context: scope,
      url: endpoint + encodeURIComponent($(scope).val()),
      method: 'GET',
      success: processResponse,
      error: function () {
        $.spinner().stop();
      }
    });
    // Bind search modal dismissal on larger screens
    if (!isMobileSearch(scope)) {
      $('body').on('click touchend', dismissLargescreenSearch);
    }
  } else {
    toggleSuggestionsIcon('search');
    $('.site-search').removeClass('search-border');
    $('body').on('click touchend', dismissLargescreenSearch);
  }
}

/**
 * Handle Search Suggestion Keyboard Arrow Keys
 *
 * @param {Integer} direction takes positive or negative number constant, DIRECTION_UP (-1) or DIRECTION_DOWN (+1)
 */
function handleArrow(direction) {
  // get all li elements in the suggestions list
  var suggestionsList = $('.suggestions .item');
  if (suggestionsList.filter('.selected').length === 0) {
    suggestionsList.first().addClass('selected');
    $('input.search-field').each(function () {
      $(this).attr('aria-activedescendant', suggestionsList.first()[0].id);
    });
  } else {
    suggestionsList.each(function (index) {
      var idx = index + direction;
      if ($(this).hasClass('selected')) {
        $(this).removeClass('selected');
        $(this).removeAttr('aria-selected');
        if (suggestionsList.eq(idx).length !== 0) {
          suggestionsList.eq(idx).addClass('selected');
          suggestionsList.eq(idx).attr('aria-selected', true);
          $(this).removeProp('aria-selected');
          $('input.search-field').each(function () {
            $(this).attr('aria-activedescendant', suggestionsList.eq(idx)[0].id);
          });
        } else {
          suggestionsList.first().addClass('selected');
          suggestionsList.first().attr('aria-selected', true);
          $('input.search-field').each(function () {
            $(this).attr('aria-activedescendant', suggestionsList.first()[0].id);
          });
        }
        return false;
      }
      return true;
    });
  }
  if (suggestionsList.filter('.selected').length > 0) {
    suggestionsList.filter('.selected').find('a').focus();
  }
}

module.exports = function () {
  $('form[name="simpleSearch"]').submit(function (e) {
    var suggestionsList = $('.suggestions .item');
    if (suggestionsList.filter('.selected').length !== 0) {
      e.preventDefault();
      suggestionsList.filter('.selected').find('a')[0].click();
    }
  });

  $('input.search-field').each(function () {
    /**
     * Use debounce to avoid making an Ajax call on every single key press by waiting a few
     * hundred milliseconds before making the request. Without debounce, the user sees the
     * browser blink with every key press.
     */
    var debounceSuggestions = debounce(getSuggestions, 300);
    var $input = $(this);
    var $reset = $input.siblings('.reset-button');

    // handle keyboard events.
    $input.on('keyup focus', function (e) {
      // Capture Down/Up Arrow Key Events
      switch (e.which) {
        case DOWN_KEY:
          handleArrow(DIRECTION_DOWN);
          e.preventDefault(); // prevent moving the cursor
          break;
        case UP_KEY:
          handleArrow(DIRECTION_UP);
          e.preventDefault(); // prevent moving the cursor
          break;
        case ESC_KEY:
          e.preventDefault();
          $('.site-search .reset-button').trigger('click');
          break;
        default:
          debounceSuggestions(this, e);
      }
      //To hide the user popover if open based on ADA focus only
      var popover = document.querySelector('.navbar-header .user .popover');
      if (popover && popover.classList.contains('show')) {
        var toggler = document.querySelector('.navbar-header .user .header-account-drawer-toggle');
        popover.classList.remove('show');
        toggler.setAttribute('aria-expanded', 'false');
      }
    });

    // Display/Hide the reset button appropriately. Any value in the field should display it.
    $input.on('input', function (e) {
      if (e.target.value === '') {
        $reset.removeClass('d-sm-block');
      } else {
        $reset.addClass('d-sm-block');
      }
    });

    // Reset focus
    $input.get(0).form.addEventListener('reset', function () {
      $input.focus();
    });
  });

  keyboardAccessibility(
    '.suggestions-wrapper',
    {
      40: function ($popover) {
        // down
        handleArrow(DIRECTION_DOWN);
      },
      38: function ($popover) {
        // up
        handleArrow(DIRECTION_UP);
      },
      27: function () {
        // escape
        $('.site-search .reset-button').trigger('click');
      },
      9: function () {
        // tab
        $('.site-search .reset-button').trigger('click');
      }
    },
    function () {
      var $popover = $('.suggestions-wrapper .item');
      return $popover;
    }
  );

  // Handle mobile search focus
  $('.search-mobile input.search-field').on('focus', function () {
    activateMobileSearch(this);
  });

  // Sticky search icons.
  $('.mobile-rightsec .sticky-search').on('click', function () {
    activateMobileSearch(document.querySelector('.search-mobile .search-field'));
  });

  $('.main-menu .sticky-search').on('click', activateFloatingSearch);

  $('body').on('click touchend', '.search-mobile button.fa-close', function (e) {
    e.preventDefault();
    $('.site-search').removeClass('search-border');
    $('body').removeClass('overlay');
    formFields.removeMenuOverlay();
    toggleSuggestionsIcon('search');
    tearDownSuggestions();
  });

  $('.site-search .reset-button').on('click', function () {
    if (isMobileSearch(this)) {
      $('.suggestions').empty();
      return;
    }
    $(this).removeClass('d-sm-block');
    $('.site-search').removeClass('search-border');
    if (!$('.main-menu').hasClass('fixed')) {
      $('body').removeClass('overlay');
    }
    clearModals();
    formFields.removeMenuOverlay();
  });

  $('body').on('click', '.search-mobile .cancel-button', deactivateMobileSearch);
};
