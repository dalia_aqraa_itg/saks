'use strict';

var formFields = require('../formFields/formFields');
var utilhelper = require('../components/utilhelper');

/**
 * appends params to a url
 * @param {string} data - data returned from the server's ajax call
 * @param {Object} button - button that was clicked for email sign-up
 */
function displayMessage(data, button) {
  $.spinner().stop();
  var status;
  $('.welcome-email-input-box').find('.error-container .error').remove();
  $('.welcome-email-signup-input').removeClass('error');
  if (data.success) {
    status = 'alert-success';
  } else {
    status = 'alert-danger';
  }

  if ($('.welcome-email-signup-message-footer').length === 0) {
    $('div.welcome-email-signup-input').append('<div class="welcome-email-signup-message-footer"></div>');
  }
  if (data.success) {
    $('.welcome-email-signup-message-footer').append(
      '<div class="d-flex align-items-center ' +
        status +
        '"><span class="svg-svg-95-check-green-thick-dims svg-svg-95-check-green-thick success-img"></span>' +
        data.msg +
        '</div>'
    );
    $('.welcome-email-signup-input').addClass('success');
    setTimeout(function () {
      $('.welcome-email-signup-message-footer').remove();
      $('.welcome-email-signup-input').removeClass('success');
      $('.welcome-email-signup-input').find('input').val(''); // remove the email prefill as he has already submitted
      button.removeAttr('disabled');
    }, 3000);
  } else {
    $('.welcome-email-input-box')
      .find('.error-container')
      .append('<span class="error">' + data.msg + '</span>');
    $('.welcome-email-signup-input').addClass('error');
    button.removeAttr('disabled');
  }
}
/**
 * function to create/delete cookie
 *
 */
function welcomeModalCloseFunctionality(redirecttoNewPage) {
  var emailId = 'welcomeemailsignupdecline';
  var form = {
    emailId: emailId
  };
  var declineurl = $('#welcome-email-modal .button-wrapper button.subscribe-email').data('href-unsubcribe');
  $.ajax({
    url: declineurl,
    type: 'post',
    dataType: 'json',
    data: form,
    success: function (data) {
      if (data.success) {
        // emailsunbsciptiondeclinecookie is set and closing the welcome email signup modal
        $('#welcome-email-modal').attr('aria-modal', 'false');
        $('#welcome-email-modal').modal('hide');
        if (redirecttoNewPage) {
          window.open($('#welcome-email-modal').find('a').attr('href'));
        }
      }
    }
  });
}

/**
 * validate email and confirm email on blur
 *
 */
$('.email-signup .user-confirm-email').on('blur', function () {
  var emailval = $('.email-signup .user-email').val();
  var confirmemailval = $(this).val();
  if (emailval !== confirmemailval) {
    $(this).addClass('is-invalid');
    $(this).prev('label').addClass('is-invalid');
    if ($(this).next('span').length === 0) {
      $('<span></span>').insertAfter(this);
    }
    $(this).next('span').addClass('invalid');
    if ($(this).next('span').hasClass('valid')) {
      $(this).next('span').removeClass('valid').addClass('invalid');
    }
    $(this).parents('.form-group').find('.invalid-feedback').text($(this).data('message-doesnt-match'));
  }
});

/**
 * validate postal code on blur
 *
 */

$('#registration-form-zipcode').on('blur', function () {
  if ($(this).val() !== '') {
    var validate;
    if ($(this).hasClass('us-pattern')) {
      validate = /(^\d{5}(-\d{4})?$)|(^[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Za-z]{1} *\d{1}[A-Za-z]{1}\d{1}$)/.test($(this).val()) === false;
    } else if ($(this).hasClass('ca-pattern')) {
      validate = /^(?!.*[DFIOQUdfioqu])[A-VXYa-vxy][0-9][A-Za-z] ?[0-9][A-Za-z][0-9]$/.test($(this).val()) === false;
    }
    if (validate) {
      $(this).addClass('is-invalid');
      $(this).prev('label').addClass('is-invalid');
      $(this).parents('.form-group').find('.invalid-feedback').text($(this).data('pattern-mismatch'));
      if ($(this).next('span').length === 0) {
        $('<span></span>').insertAfter(this);
      }
      $(this).next('span').addClass('invalid');
      if ($(this).next('span').hasClass('valid')) {
        $(this).next('span').removeClass('valid').addClass('invalid');
      }
    } else {
      $(this).removeClass('is-invalid');
      $(this).prev('label').removeClass('is-invalid');
      $(this).parents('.form-group').find('.invalid-feedback').text();
      if ($(this).next('span').length === 0) {
        $('<span></span>').insertAfter(this);
      }
      $(this).next('span').addClass('valid');

      if ($(this).next('span').hasClass('invalid')) {
        $(this).next('span').removeClass('invalid').addClass('valid');
      }
      if ($(this).hasClass('ca-pattern')) {
        // eslint-disable-next-line newline-per-chained-call
        var zipVal = $(this).val().trim().replace(/\s/g, '').toUpperCase();
        var formatedVal = zipVal.substr(0, 3) + ' ' + zipVal.substr(3, zipVal.length);
        $(this).val(formatedVal);
      }
    }
  }
});

// Hudson bay number check on email sign up page
$('body').on('blur', '.email-signup-form .hudson-reward', function () {
  var $this = $(this);
  var rewardNo = $this.val();
  var url = $this.data('check-balance-url');
  var regex = new RegExp('^[0-9]+$');
  if (rewardNo && url && regex.test(rewardNo)) {
    $.spinner().start();
    $.ajax({
      url: url,
      type: 'GET',
      data: {
        reward: rewardNo,
        check: true
      },
      success: function (data) {
        if (!data.success) {
          $('.hudson-error').removeClass('d-none');
          $('.hudson-reward').val('');
          $('.hudson-reward').addClass('is-invalid');
          $('label.hudsonbay-rewards').addClass('is-invalid');
          $('<span class="invalid"></span>').insertAfter('.hbc-loyalty-number-prefix');
          if (data.counter >= 5) {
            $('.hudson-reward').attr('disabled', true);
            $('.hudson-error').addClass('d-none');
            $('.hudson-error-max').removeClass('d-none');
          }
        } else {
          $('.hudson-error').addClass('d-none');
          $('.hudson-reward').removeClass('is-invalid');
          $('label.hudsonbay-rewards').removeClass('is-invalid');
          $('.hbc-loyalty-number-prefix').nextAll('.invalid').remove();
        }
        $.spinner().stop();
      },
      error: function () {
        $('.hudson-error').removeClass('d-none');
        $('.hudson-reward').val('');
        $('.hudson-reward').addClass('is-invalid');
        $('label.hudsonbay-rewards').addClass('is-invalid');
        $('<span class="invalid"></span>').insertAfter('.hudson-reward');
        $.spinner().stop();
      }
    });
  } else {
    $('.hudson-error').removeClass('d-none');
    $('label.hudsonbay-rewards').addClass('is-invalid');
    $('.hudson-reward').addClass('is-invalid');
    $('.hudson-reward').val('');
  }
});

// Handle the enable and disable of the button
$('body').on('blur', 'form input, form select', function () {
  var emailval = $('.email-signup .user-email').val();
  var confirmemailval = $('.email-signup .user-confirm-email').val();
  var validate = true;
  var fname = $('.email-signup .user-fname').val();
  var lname = $('.email-signup .user-lname').val();
  var zipcode = $('.email-signup-form #registration-form-zipcode').val();
  if (zipcode !== '' && zipcode !== null && $('.email-signup-form #registration-form-zipcode').hasClass('us-pattern')) {
    validate = /(^\d{5}(-\d{4})?$)|(^[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Za-z]{1} *\d{1}[A-Za-z]{1}\d{1}$)/.test(zipcode) === false;
  } else if (zipcode !== '' && zipcode !== null && $('.email-signup-form #registration-form-zipcode').hasClass('ca-pattern')) {
    validate = /^(?!.*[DFIOQUdfioqu])[A-VXYa-vxy][0-9][A-Za-z] ?[0-9][A-Za-z][0-9]$/.test(zipcode) === false;
  }
  if (
    fname === null ||
    fname === '' ||
    lname === null ||
    lname === '' ||
    zipcode === null ||
    zipcode === '' ||
    validate ||
    emailval === null ||
    emailval === '' ||
    confirmemailval === null ||
    confirmemailval === '' ||
    emailval !== confirmemailval
  ) {
    $('.email-signup-form').find('.email-signup-submit').attr('disabled', 'disabled');
  } else {
    $('.email-signup-form').find('.email-signup-submit').removeAttr('disabled');
  }
});

/**
 * Renders a modal window that will track the users consenting to accepting site tracking policy
 */
function showWelcomeEmailSignUpModal() {
  if ($('.tracking-consent.welcome').hasClass('api-true')) {
    return;
  }

  var urlContent = $('.tracking-consent.welcome').data('url');
  var urlContentForResponse = $('.tracking-consent.welcome').data('responseurl');

  var htmlString =
    '<!-- Modal -->' +
    '<div class="modal show" id="welcome-email-modal" class="consent-tracking-modal" role="dialog" aria-modal="true">' +
    '<div class="modal-dialog email-signup-modal">' +
    '<!-- Modal content-->' +
    '<div class="modal-content">' +
    '<div class="welcome-modal-body"></div>' +
    '</div>' +
    '</div>' +
    '</div>';
  $.spinner().start();
  $('body').append(htmlString);

  $.ajax({
    url: urlContent,
    type: 'get',
    dataType: 'html',
    success: function (response) {
      $('.welcome-modal-body').html(response);
      $('#welcome-email-modal').modal('show');
      formFields.adjustForAutofill();
      $.spinner().stop();
      $('.consent-tracking-close,.modal-backdrop').on('click', function () {
        welcomeModalCloseFunctionality(false);
      });
      $('#welcome-email-modal')
        .find('a')
        .on('click', function (e) {
          e.preventDefault();
          welcomeModalCloseFunctionality(true);
        });
      $('#welcome-email-modal').on('hidden.bs.modal', function () {
        welcomeModalCloseFunctionality(false);
      });
      $('#welcome-email-modal .button-wrapper button.subscribe-email').click(function (e) {
        e.preventDefault();
        var button = $(this);
        $(this).attr('disabled', true);
        var emailId = $(this).parent().parent('.welcome-email-signup-input').find('input[name=welcomeEmailSignUp]').val();
        var form = {
          emailId: emailId
        };
        var url = $(this).data('href');
        $.ajax({
          url: url,
          type: 'post',
          dataType: 'json',
          data: form,
          success: function (data) {
            if (data.success) {
              $('body').trigger('adobeTagManager:emailsignupmodal', 'welcome overlay');
              $.ajax({
                url: urlContentForResponse,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                  // eslint-disable-line
                  $('.welcome-modal-body').html(response);
                  $('#thank-email-close,.modal-backdrop').on('click', function () {
                    $('#welcome-email-modal').attr('aria-modal', 'false');
                    $('#welcome-email-modal').modal('hide');
                  });
                  $.spinner().stop();
                },
                error: function () {
                  $('#welcome-email-modal').attr('aria-modal', 'false');
                  $('#welcome-email-modal').remove();
                  $.spinner().stop();
                }
              });
            } else {
              $('#email-signup').focus().blur();
              displayMessage(data, button);
              $.spinner().stop();
            }
            // $('#consent-tracking').remove();
            // $.spinner().stop();
          },
          error: function (err) {
            $('#email-signup').focus().blur();
            displayMessage(err, button);
            $.spinner().stop();
          }
        });
      });
      $('#email-signup').on('blur', function () {
        if ($('#form-email-error').text().length > 0) {
          $(this).attr('aria-invalid', 'true');
        } else {
          $(this).attr('aria-invalid', 'false');
        }
      });
    },
    error: function () {
      $('#welcome-email-modal').attr('aria-modal', 'false');
      $('#welcome-email-modal').remove();
    }
  });
}

function hbcEmailSignup() {
  $('.email-signup-form form.email-signup').submit(function (e) {
    var errors = $(this).find('span.invalid').length;
    if (errors === 0) {
      $('body').trigger('adobeTagManager:emailsignupmodal', 'email signup');
    }
  });
}

module.exports = function () {
  if ($('.tracking-consent.welcome').hasClass('api-false')) {
    showWelcomeEmailSignUpModal();
  }

  if ($('.tracking-consent.welcome').hasClass('api-false')) {
    $('.tracking-consent').click(function () {
      showWelcomeEmailSignUpModal();
    });
  }
  hbcEmailSignup();
};
