'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
  processInclude(require('./product/wishlistHeart'));
  processInclude(require('./product/quickView'));
});
