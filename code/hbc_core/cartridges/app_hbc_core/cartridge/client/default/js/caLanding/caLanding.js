var clientSideValidation = require('../components/clientSideValidation');

/**
 * Prepare html modal for appointment
 * @param {Object} obj - object containing the data
 *
 **/
function getAppointmentModalHtmlElement(obj) {
  if ($('#signupModal').length !== 0) {
    $('#signupModal').remove();
  }
  var htmlString =
    '<!-- Modal -->' +
    '<div class="modal signup-Modal fade CA-Landing-modal" id="signupModal" role="dialog">' +
    '<div class="modal-dialog">' +
    '<!-- Modal content-->' +
    '<div class="modal-content row no-gutters align-items-center justify-content-center text-center">' +
    '<div class="modal-header">' +
    '    <button type="button" class="close pull-right" data-dismiss="modal">' +
    '        <span aria-hidden="true" class="cancel-icon svg-svg-22-cross svg-svg-22-cross-dims"></span>' +
    '    </button>' +
    '</div>' +
    '<div class="modal-header-content">' +
    '<div class="header-text">' +
    obj.header +
    '</div>' +
    '</div><div class="header-info">' +
    obj.headerInfo +
    '</div>' +
    '<div id="signupForm" class="modal-body">' +
    '<form class="email-signup-form" action="' +
    obj.url +
    '" method="post">' +
    '<div class="row"><div class="col-lg-6 col-sm-12"><div class="form-group form-input floating-label required"><label class="form-control-label">' +
    obj.firstName +
    '</label>' +
    '<input type="text" autofocus name="firstName" required max-length="50" class="form-control required" id="firstName" data-missing-error="' +
    obj.requiredMsg +
    '" pattern="' +
    obj.pattern +
    '" data-default-error="' +
    obj.defaultError +
    '"/><div class="invalid-feedback text-left"></div></div></div>' +
    '<div class="col-lg-6 col-sm-12"><div class="form-group form-input floating-label required"><label class="form-control-label">' +
    obj.lastName +
    '</label>' +
    '<input type="text" name="lastName" required  max-length="50" data-missing-error="' +
    obj.requiredMsg +
    '" class="form-control" id="lastName" pattern="' +
    obj.pattern +
    '" data-default-error="' +
    obj.defaultError +
    '"/><div class="invalid-feedback text-left"></div></div></div></div>' +
    '<div class="row"><div class="col-lg-6 col-sm-12"><div class="form-group form-input floating-label required"><label class="form-control-label">' +
    obj.email +
    '</label>' +
    '<input type="email" name="email" required max-length="50" aria-describedby="form-email-error" aria-required="true" data-missing-error="' +
    obj.requiredMsg +
    '" class="form-control" pattern="^[\\w.%+-]+@[\\w.-]+\\.[\\w]{2,6}$" data-pattern-mismatch="' +
    obj.emailInvalidMsg +
    '" id="email" /><div class="invalid-feedback text-left"></div></div></div>' +
    '<div class="col-lg-6 col-sm-12"><div class="form-group form-input floating-label required"><label class="form-control-label">' +
    obj.postalCode +
    '</label>' +
    '<input type="text" name="postalCode" data-missing-error="' +
    obj.requiredMsg +
    '" data-pattern-mismatch="' +
    obj.patternmismatch +
    '" required maxlength="7" pattern="^(?!.*[DFIOQUdfioqu])[A-VXYa-vxy][0-9][A-Za-z] ?[0-9][A-Za-z][0-9]$" class="form-control ca-Landing-Po" id="postalCode" /><div class="invalid-feedback text-left"></div></div></div></div>' +
    '<div class="row"><div class="col-lg-6 col-sm-12"><div class="form-group form-input floating-label required"><label class="form-control-label">' +
    obj.phone +
    '</label>' +
    '<input type="text" name="phone" data-missing-error="' +
    obj.requiredMsg +
    '" pattern="^\\(?([2-9][0-8][0-9])\\)?[\\-\\. ]?([2-9][0-9]{2})[\\-\\. ]?([0-9]{4})(\\s*x[0-9]+)?$" class="form-control" id="phoneNumber" data-default-error="' +
    obj.defaultError +
    '"/><div class="invalid-feedback text-left"></div></div></div></div>' +
    '<div class="row appointment-form-btns m-auto"><span class="note text-left mb-3">' +
    obj.note +
    '</span><div class="clearfix rmber-frgt"><div class="form-group custom-control custom-checkbox pull-left"><input type="checkbox" class="custom-control-input" id="companyone" name="companyone" ><label class="custom-control-label text-left" for="companyone">' +
    '' +
    obj.optionone +
    '</label></div><div class="form-group custom-control custom-checkbox pull-left"><input type="checkbox" class="custom-control-input" id="companytwo" name="companytwo" ><label class="custom-control-label text-left" for="companytwo">' +
    obj.optiontwo +
    '</label></div>' +
    '</div></div>' +
    '<div class="row appointment-form-btns justify-content-center mt-4"><div class="col-12 col-sm-6 appointment-form-btn-right"><button type="submit" id="submitAppointment" class="btn btn-primary btn-block">' +
    obj.continueLabel +
    '</button></div></div>' +
    '<div class="withdraw-notify"><span class="note text-left">' +
    obj.notetwo +
    '</span></div>' +
    '</form>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';
  $('body').append(htmlString);
  $('#signupModal').on('shown.bs.modal', function () {
    setTimeout(function () {
      $('#signupModal').find('input:visible:enabled:first').focus();
      poBlurSpaceAdjust();
      clientSideValidation.invalid();
    }, 1000);
  });

  $('form.email-signup-form').submit(function (e) {
    e.preventDefault();
    var params = {};
    params.firstName = $('#firstName').val();
    params.lastName = $('#lastName').val();
    params.email = $('#email').val();
    params.postalCode = $('#postalCode').val();
    params.phoneNumber = $('#phoneNumber').val();
    params.companyOne = $('#companyone').prop('checked');
    params.companyTwo = $('#companytwo').prop('checked');
    $.ajax({
      url: $(this).attr('action'),
      type: 'post',
      data: params,
      success: function (data) {
        $('#signupModal').find('.header-text').empty();
        $('#signupModal').find('.header-info').empty().remove();
        $('#signupModal').find('.modal-dialog').addClass('ca-response');
        $('#signupModal').find('.modal-content').addClass('response');
        $('#signupModal').find('.modal-body').empty().html(data);
      }
    });
  });
}

function showDialogExit(contentUrl) {
  var htmlString =
    '<!-- Modal -->' +
    '<div class="modal show shop-saksoff5th" id="shop-saksoff5th" role="dialog">' +
    '<div class="modal-dialog">' +
    '<!-- Modal content-->' +
    '<div class="modal-content">' +
    '<div class="modal-header">' +
    '    <button type="button" class="close pull-right" data-dismiss="modal">' +
    '        <span aria-hidden="true" class="cancel-icon svg-svg-22-cross svg-svg-22-cross-dims"></span>' +
    '    </button>' +
    '</div>' +
    '<div class="modal-body"></div>' +
    '</div>' +
    '</div>' +
    '</div>';
  $('body').append(htmlString);

  $.ajax({
    url: contentUrl,
    type: 'get',
    dataType: 'html',
    success: function (response) {
      $('#shop-saksoff5th').find('.modal-body').html(response);
    },
    error: function () {
      $('#shop-saksoff5th').remove();
    }
  });
}

function poBlurSpaceAdjust() {
  $('.ca-Landing-Po').on('blur', function () {
    var zipVal = $(this).val().trim().replace(/\s/g, '').toUpperCase();
    var formatedVal = zipVal.substr(0, 3) + ' ' + zipVal.substr(3, zipVal.length);
    $(this).val(formatedVal);
  });
}

module.exports = {
  emailSignUp: function () {
    $('#home-ca').on('click', function (e) {
      e.preventDefault();
      var obj = JSON.parse($('#ca-resource').val());
      getAppointmentModalHtmlElement(obj);
      $('#signupModal').modal('show');
    });
    $('.home-ca-close,.modal-backdrop').on('click', function () {
      $('#signupModal').modal('hide');
    });
    $('#signupModal').on('hidden.bs.modal', function () {
      $('#signupModal').modal('hide');
    });
  },
  leaveCAsite: function () {
    $('a.shop-saksoff5th').on('click', function (e) {
      e.preventDefault();
      var resources = JSON.parse($('#ca-resource').val());
      showDialogExit(resources.shopContentUrl);
      $('#shop-saksoff5th').modal('show');
    });
  }
};
