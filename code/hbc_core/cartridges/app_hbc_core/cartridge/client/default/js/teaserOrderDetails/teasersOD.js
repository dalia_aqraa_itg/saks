'use strict';

/**
 * @function
 * @name createTeaserElement
 * @description This is a helper function used by other functions to create an element with a specific tag name and class name. Optionally pass “text” to populate the element with the provided text.
 * @param {string} tag type of DOM element
 * @param {string} className name of the element class
 * @param {string} text value to be added to the element
 * @return {Object} el DOM object
 */
function createTeaserElement(tag, className, text, sku) {
  var el = document.createElement(tag);
  el.setAttribute('class', className);
  el.setAttribute('data-pid', sku);
  if (text) {
    el.innerText = text;
  }
  return el;
}

/**
 * @function
 * @name generateWriteReview
 * @description This function is called by the populateTeaser function to simply create a ‘Write a Review’ link. The click event handler for this element is added in the populateTeaser() function after the teaser has been rendered on the page.
 * @param {string} text value to be added to the element
 * @return {Object} DOM object
 */
function generateWriteReview(text, sku) {
  return createTeaserElement('button', 'TTteaser__write-review', text, sku);
}

/**
 * @function
 * @name populateTeaser
 * @description This function calls other functions to build specific parts of your CGC Teaser. It should be modified to account for what content types you want to promote and/or the messaging you want based on the count returned. It ends with the writeReview() function that spawns the TurnTo review form from your CGC Teaser. Note: If the reviews list and/or comments are initially hidden under a tab, see the linking to your Content Underneath a tab section for an example of how to handle this situation.
 * @param {string} sku the product id to append the reviews
 */
function populateTeaser(sku) {
  var fragment = document.createDocumentFragment();
  fragment.appendChild(generateWriteReview('Write a Review', sku));
  var teaserElem = document.getElementById('tt-teaser-' + sku);
  if (!teaserElem) {
    return;
  }
  teaserElem.appendChild(fragment);

  if ($('.TTteaser__write-review').length) {
    $('body').on('click', '.TTteaser__write-review', function () {
      TurnToCmd('set', { sku: $(this).data('pid') }); //eslint-disable-line
      TurnToCmd('reviewsList.writeReview'); //eslint-disable-line
    });
  }
}
/**
 * @function
 * @description The loadTeaserCounts function first loads the UGC counts maintained by TurnTo and then calls populateTeaser() function.
 * @name loadTeaserCounts
 * @param {string} sku the product ID
 */
function loadTeaserCounts(sku) {
  var xhr = new XMLHttpRequest();
  var turntoUrl = window.turntoUrl;
  var siteKey = window.siteKey;

  if (!turntoUrl || !siteKey) {
    return;
  }

  var ugcCountsUrl = 'https://cdn-ws.' + turntoUrl + '/v5/sitedata/' + siteKey + '/' + sku + '/d/ugc/counts/' + turnToConfig.locale; //eslint-disable-line
  xhr.open('GET', ugcCountsUrl, true);
  xhr.addEventListener('load', function () {
    if (xhr.responseText) {
      populateTeaser(sku);
    }
  });
  xhr.send();
}

module.exports = {
  createReview: function () {
    $('.product-line-items .review-ratings').each(function () {
      loadTeaserCounts($(this).attr('data-pid'));
    });
  }
};
