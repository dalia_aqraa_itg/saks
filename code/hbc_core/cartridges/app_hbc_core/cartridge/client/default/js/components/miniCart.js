'use strict';

var cart = require('../cart/cart');
var formFields = require('../formFields/formFields');
var keyboardAccessibility = require('base/components/keyboardAccessibility');

var updateMiniCart = true;

/**
 appending html content to the div based on shoprunner enabled condition
 */
function checkShopRunnerEnabled() {
  if ($('.enableShopRunner').length > 0) {
    if (typeof window.parent.sr_updateMessages === 'function') {
      window.parent.sr_updateMessages();
    }
  }
}

/**
  Mini cart height
*/
function getMiniCartHeight() {
  setTimeout(function () {
    if ($('.main-menu').is('.fixed')) {
      $($('.minicart .product-summary')[1]).css(
        'height',
        $($('.minicart .popover')[1]).outerHeight(true) - ($($('.minicart-footer')[1]).outerHeight(true) + $('.main-menu').outerHeight(true))
      );
    } else {
      $($('.minicart .product-summary')[0]).css(
        'height',
        $($('.minicart .popover')[0]).outerHeight(true) -
          ($($('.minicart-footer')[0]).outerHeight(true) + $('.header').outerHeight(true) - $(window).scrollTop() + 33)
      );
    }
  }, 200);
}

// Expose hoverintent in/out functions outside of hoverintent function
// so that they can be accessed by keyboard events
var focusIn = function focusIn(element) {
  if ($('.search:visible').length === 0) {
    return;
  }
  var _element;
  var url = $('.minicart').data('action-url');
  var count = parseInt($('.minicart .minicart-quantity').text(), 10);
  var bannerHeightoverlay = parseInt($('.header-banner').outerHeight()) + parseInt($('#header-container').outerHeight()) - $(window).scrollTop(); // eslint-disable-line
  var bannerHeight = parseInt($('.navbar').offset().top) - parseInt($('.minicart-link').offset().top) + parseInt($('.minicart-link').outerHeight() - 7); // eslint-disable-line
  var stkyHeight = parseInt($('.menu-wrapper').height()) - 11; // eslint-disable-line
  var hideOverflowY = function hideOverflowY() {
    $('body').css({ 'overflow-y': 'hidden' });
  };

  if (count !== 0 && $('.minicart .popover.show').length === 0) {
    $(element.relatedTarget).focus();
    hideOverflowY();
    _element = element || $('.minicart');

    // Swap Minicart Content for Duplicate content
    if ($('body').find('.main-menu').hasClass('fixed') && $('.minicart-popup-container .popover').text().length > 0) {
      $('.minicart-popup-container-sticky .popover').empty().append($('.minicart-popup-container .popover').html());
      $('.minicart-popup-container .popover').empty();
    } else {
      if (!$('body').find('.main-menu').hasClass('fixed') && $('.minicart-popup-container-sticky .popover').text().length > 0) {
        $('.minicart-popup-container .popover').empty().append($('.minicart-popup-container-sticky .popover').html());
        $('.minicart-popup-container-sticky .popover').empty();
      }
    }

    if (!updateMiniCart) {
      $('.minicart .popover')
        .addClass('show')
        .css('top', $('.minicart').outerHeight() + 3);
      if ($('body').find('.main-menu').hasClass('fixed')) {
        $('.minicart .popover').addClass('show').css('top', stkyHeight);
      }
      setMiniCartScrollHeight();
      $('body').trigger('adobeTagManager:openMiniCart', _element);
      return;
    }

    $('.minicart .popover')
      .addClass('show')
      .css('top', $('.minicart').outerHeight() + 3);
    if ($('body').find('.main-menu').hasClass('fixed')) {
      $('.minicart .popover').addClass('show').css('top', stkyHeight);
    }
    $('.minicart .popover').spinner().start();
    $.get(url, function (data) {
      // Line Item is 0, don't popup the minicart.
      var miniCartContainer = $(data).filter('.mini-cart-container');
      var noOFLineItem = parseInt(miniCartContainer.attr('data-numItems'));
      var minicartTitle = miniCartContainer.attr('data-title');
      var minicartLabel = miniCartContainer.attr('data-aria-label');
      $('.minicart .minicart-quantity').text(noOFLineItem);
      $('.minicart .minicart-link').attr({
        'aria-label': minicartLabel,
        title: minicartTitle
      });
      $('.minicart .popover').empty();
      if (noOFLineItem > 0) {
        $('.minicart .popover').append(data);
        if ($('body').find('.main-menu').hasClass('fixed')) {
          $('.minicart-popup-container .popover').empty();
        } else {
          $('.minicart-popup-container-sticky .popover').empty();
        }
        updateMiniCart = false;
        $.spinner().stop();
        $('.minicart .product-summary').each(function () {
          $(this).scrollTop(1);
        });
        $('body').trigger('adobeTagManager:openMiniCart', data);
        formFields.quantityHoverBorder();
        setMiniCartScrollHeight();
      } else {
        // Update the quantity and don't open up the mini cart.
        $.spinner().stop();
        $('.minicart .popover').removeClass('show');
      }
    });
  }

  // Hide the chat widget if it's open
  if ($('body').hasClass('chat-overlay')) {
    $('.liveChat-btn').trigger('click');
  }

  // Hide Confirmation Modal if open
  $('.product-added-to-cart-modal.modal:visible').hide();
};

var focusOut = function focusOut() {
  // Code for hover intent out
  $('.minicart .popover').removeClass('show');
  $('body').css({
    'overflow-y': 'auto',
    'padding-right': ''
  });
  checkShopRunnerEnabled();
};

function minicartKeyboardEvents() {
  var minicartSelector = '.minicart';
  keyboardAccessibility(
    minicartSelector,
    {
      40: function ($wrapper) {
        // down
        var $children = $wrapper.find('.popover a, .popover button, .popover input');
        if (!$children.length) {
          return;
        }
        var focusIndex = $children.index(document.activeElement);
        if (focusIndex === -1 || focusIndex === $children.length - 1) {
          $children.eq(0).focus();
        } else {
          $children.eq(focusIndex + 1).focus();
        }
      },
      38: function ($wrapper) {
        // up
        var $children = $wrapper.find('.popover a, .popover button, .popover input');
        if (!$children.length) {
          return;
        }
        var focusIndex = $children.index(document.activeElement);
        if (focusIndex === 0 || focusIndex === -1) {
          $children.eq($children.length - 1).focus();
        } else {
          $children.eq(focusIndex - 1).focus();
        }
      },
      27: function () {
        // Escape
        document.querySelector('.minicart-link').focus();
        focusOut();
      }
    },
    function () {
      return $(minicartSelector);
    }
  );
}

/**
 * Mini cart Scroll Height
 */
function setMiniCartScrollHeight() {
  var winHt = $(window).outerHeight();
  var mcFooterHt = $('.minicart-footer').outerHeight();
  setTimeout(function () {
    if ($('.main-menu').is('.fixed')) {
      $('.minicart .product-summary').scrollTop(0);
      mcFooterHt = $($('.minicart-footer')[1]).outerHeight();
      if (winHt < 786) {
        $('.minicart .product-summary').css({
          'max-height': 410 - mcFooterHt
        });
      } else {
        $('.minicart .product-summary').css({
          'max-height': 610 - mcFooterHt
        });
      }
    } else {
      $('.minicart .product-summary').scrollTop(0);
      mcFooterHt = $($('.minicart-footer')[0]).outerHeight();
      if (winHt < 786) {
        $('.minicart .product-summary').css({
          'max-height': 410 - mcFooterHt
        });
      } else {
        $('.minicart .product-summary').css({
          'max-height': 610 - mcFooterHt
        });
      }
    }
  }, 200);
}

module.exports = function () {
  cart();
  checkShopRunnerEnabled();
  $('.minicart').on('count:update', function (event, count) {
    if (count && $.isNumeric(count.quantityTotal)) {
      $('.minicart .minicart-quantity').text(count.quantityTotal);
      $('.minicart .minicart-link').attr({
        'aria-label': count.cart.resources.minicartCountOfItems,
        title: count.cart.resources.minicartCountOfItems
      });
    }
  });

  /**
   * Hover intent functionality for minicart.
   * @param {element} element minicart bag.
   */
  function hoverIntentFunctionality(element) {
    var hoverListener = hoverintent(
      element, // eslint-disable-line
      focusIn,
      focusOut
    ).options(opts); // eslint-disable-line
  }

  var opts = {
    timeout: 500,
    interval: 0
  };
  var el = $('.minicart');
  if (el.length > 0) {
    hoverIntentFunctionality(el[0]);
    hoverIntentFunctionality(el[1]);
  }

  // Open the cart from the keyboard
  $('.minicart-link').on('focusin', function (evt) {
    focusIn(evt.target);
  });

  // Close minicart when tabbing away from:
  // 1. cart container
  // 2. the cart icon itself
  $('body').on('keyup', function (e) {
    // Halt if minicart popover is shown
    if (!$('.minicart .popover').hasClass('show')) {
      return;
    }

    let target;
    let code;
    let cartContainer = '.mini-cart-container';
    const TAB_KEY = 9;

    code = e.keyCode || e.which;

    if (code === TAB_KEY) {
      target = $(e.target);

      if (!target.parents(cartContainer).length && !target.hasClass('minicart-link')) {
        focusOut();
      }
    }
  });

  minicartKeyboardEvents();

  $('body').on('touchstart click', function (e) {
    if ($('.minicart').has(e.target).length <= 0 || $('.popover').has(e.target).length <= 0) {
      $('.minicart .popover').removeClass('show');
      $('body').css({ 'overflow-y': 'auto' });
    }
  });

  /* $('.minicart').on('mouseleave', function (event) {
        if ((event.type === 'focusout' && $('.minicart').has(event.target).length > 0)
            || (event.type === 'mouseleave' && $(event.target).is('.minicart .quantity'))
            || $('body').hasClass('modal-open')) {
            event.stopPropagation();
            return;
        }
    });*/

  $('body').on('change', '.minicart .quantity', function () {
    if ($(this).parents('.bonus-product-line-item').length && $('.cart-page').length) {
      location.reload();
    }
  });
  $('body').on('product:afterAddToCart', function () {
    updateMiniCart = true;
  });
  $('body').on('cart:update', function () {
    updateMiniCart = true;
  });
};
