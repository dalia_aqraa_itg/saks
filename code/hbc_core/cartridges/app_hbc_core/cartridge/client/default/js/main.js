window.jQuery = window.$ = require('jquery');
window.slick = window.Slick = require('slick-carousel/slick/slick');
window.perfectScrollbar = require('perfect-scrollbar/dist/perfect-scrollbar');
window.hoverintent = require('hoverintent/dist/hoverintent.min');
window.maskFun = require('jquery-mask-plugin/dist/jquery.mask.min');
require('focus-visible/dist/focus-visible.min');
var smoothscroll = require('smoothscroll-polyfill');
var processInclude = require('base/util');
window.isTouchscreen = require('./util/isTouchscreen');
window.hbcSlider = require('./hbcSlider');

$(document).ready(function () {
  processInclude(require('./util/loginPopup'));
  processInclude(require('./product/persistentWishlist'));
  processInclude(require('./components/menu'));
  processInclude(require('./components/header'));
  processInclude(require('./components/search'));
  processInclude(require('base/components/cookie'));
  processInclude(require('./components/consentTracking'));
  processInclude(require('./components/commissionCookies'));
  processInclude(require('./components/footer'));
  processInclude(require('./components/miniCart'));
  processInclude(require('./components/collapsibleItem'));
  processInclude(require('./components/refinementBar'));
  processInclude(require('./components/clientSideValidation'));
  processInclude(require('base/components/countrySelector'));
  processInclude(require('base/components/toolTip'));
  processInclude(require('./formFields/formFields'));
  processInclude(require('./hbcSlider'));
  processInclude(require('./components/slot'));
  processInclude(require('./components/welcomeemailSignUp'));
  processInclude(require('./components/promo-tray-widget'));
  processInclude(require('./components/designerIndex'));
  processInclude(require('./product/wishlist'));
  processInclude(require('./product/wishlistHeart'));
  processInclude(require('./product/tileFocusManager'));
  processInclude(require('./components/chanel'));
  if ($('input[name="isAtmEnabled"]').val() === 'true') {
    var atmHelper = require('./atm/atmHelper');
    var adobeTagManger = require('./atm/atm');
    adobeTagManger(atmHelper);
  }
  smoothscroll.polyfill();
});

require('base/thirdParty/bootstrap');
require('base/components/spinner');

/**
 * Extend jQuery selectors to allow for :focusable
 */
jQuery.extend(jQuery.expr[':'], {
  focusable: function (el) {
    return $(el).is('a, button, :input, [tabindex]');
  }
});
