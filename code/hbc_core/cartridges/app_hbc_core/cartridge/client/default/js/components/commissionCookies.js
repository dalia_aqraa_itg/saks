/**
 * Store cookies with commission vendor data
 * @param request
 * @returns
 */
function manageCommissionCookies() {
  const queryString = window.location.search;
  var site_refer = getUrlParameter('site_refer', queryString);
  // do not store cookies if site_refer parameter does not exist or empty
  if (!validateSiteRefer(site_refer)) {
    return;
  }
  var commissionCookieNames = ['site_refer', 'sf_storeid', 'sf_associd'];
  var age = 7 * 24 * 60 * 60 * 1000;
  commissionCookieNames.forEach(function (name) {
    var value = getUrlParameter(name, queryString);
    var expires = new Date();
    expires.setTime(expires.getTime() + age);
    document.cookie = name + '=' + value + ';path=/;Secure;SameSite=None;expires=' + expires.toUTCString();
  });
}

function getUrlParameter(name, queryString) {
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  var results = regex.exec(queryString);
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

/**
 * Attribution logic for cookies
 * @param value
 * @returns
 */
function validateSiteRefer(value) {
  if (!value) {
    return false;
  }
  var site_ref_search = 'SEM';
  var site_ref_email = 'EML';
  var site_ref_email_SF = 'EMLHB_SF';
  var emlRegexp = new RegExp('^(' + site_ref_email + '|' + site_ref_search + ')');
  if (emlRegexp.test(value)) {
    if (value !== site_ref_email_SF) {
      return false;
    }
  }

  return true;
}

module.exports = function () {
  if (window.location.search.indexOf('site_refer')) {
    manageCommissionCookies();
  }
};
