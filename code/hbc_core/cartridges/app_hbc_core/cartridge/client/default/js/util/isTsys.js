/**
 * Checkes if TSYS mode enabled
 *  @returns {boolean} - tsys mode
 */

function isTsysMode() {
  return !!document.querySelector('.tsys-mode[data-mode=true]');
}

module.exports = isTsysMode;
