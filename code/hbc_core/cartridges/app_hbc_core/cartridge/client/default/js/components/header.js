'use strict';

var debounce = require('lodash/debounce');

/**
 * Is the window large-ish?
 * @returns {Boolean} true if large, otherwise false.
 */
function isDesktop() {
  return window.innerWidth >= 1024;
}

function getBannerHeight() {
  var headerContainer = document.querySelector('#header-container');
  var headerBanner = document.querySelector('.header-banner');
  return headerBanner === null ? headerContainer.offsetHeight : headerBanner.offsetHeight + headerContainer.offsetHeight;
}

let initialMainMenuHeight;
let currentWindowWidth = window.innerWidth;

/**
 * Gets the height of the nav menu in it's initial state, scrolled to the top of the page.
 * @returns {Deferred} A jQuery Deferred object. The menu height is passed as the only argument to the `.then` and `.done` methods.
 */
function getInitialMainMenuHeight() {
  const deferred = $.Deferred();
  if (typeof initialMainMenuHeight === 'number') {
    return deferred.resolve(initialMainMenuHeight);
  }
  const scrollPos = window.scrollY;
  window.scrollTo({ top: 0 });
  window.setTimeout(() => {
    initialMainMenuHeight = document.querySelector('.main-menu').offsetHeight;
    window.scrollTo({ top: scrollPos });
    deferred.resolve(initialMainMenuHeight);
  }, 2);
  return deferred;
}

const resetIntialMainMenuHeight = debounce(() => {
  // We might get here on a resize event, which gets triggered on iOS
  // by Mobile Safari minimizing the browser chrome as you scroll.
  // If our window resize was vertical only, no need to re-measure header height,
  // as we haven't transitioned between smallscreen and largescreen layouts.
  if (currentWindowWidth === window.innerWidth) {
    return;
  }

  initialMainMenuHeight = null;
  currentWindowWidth = window.innerWidth;
  // Wait to ensure the window isn't scrolling before checking the menu height
  // Because if we do this check while scrolling, at least on an iOS device,
  // the scrolling intertia will stop dead.
  let prevScrollY = window.scrollY;
  const scrollCheck = setInterval(() => {
    if (prevScrollY === window.scrollY) {
      getInitialMainMenuHeight();
      clearInterval(scrollCheck);
      return;
    }
    prevScrollY = window.scrollY;
  }, 200);
}, 50);

window.addEventListener('resize', resetIntialMainMenuHeight);
window.addEventListener('orientationchange', resetIntialMainMenuHeight);

function fixLargescreenHeader() {
  const headerBanner = document.querySelector('.header-banner');
  getInitialMainMenuHeight().then(menuHeight => {
    const off5thMenuWrapper = document.querySelector('.main-menu .off5htMenuWrapper');
    const mainContent = document.querySelector('#maincontent');
    $('.main-menu').addClass('fixed');
    $('.header-banner').addClass('fixed');
    $('#header-container').addClass('hidden');
    $('.minicart-overlay').css('top', menuHeight);
    $('.minicart .popover').css('top', '42px');
    if (off5thMenuWrapper) {
      off5thMenuWrapper.style.paddingTop = `${headerBanner.offsetHeight}px`;
      mainContent.style.paddingTop = `${menuHeight + headerBanner.offsetHeight}px`;
    } else {
      mainContent.style.paddingTop = `${menuHeight}px`;
    }
  });
}

function unfixLargescreenHeader() {
  getInitialMainMenuHeight().then(menuHeight => {
    $('.main-menu.fixed .off5htMenuWrapper').removeAttr('style');
    $('.main-menu').removeClass('fixed');
    $('.header-banner').removeClass('fixed');
    $('.main-menu .off5htMenuWrapper').css('padding-top', 0);
    $('#header-container').removeClass('hidden');
    $('.site-search').removeClass('fixed');
    $('.site-search').css('top', 0);
    $('.minicart-overlay').css('top', menuHeight);
    $('.minicart .popover').css('top', '77px');
    $('#maincontent').css('padding-top', 0);
    if (!$('.suggestions').length || $('.suggestions').is(':hidden')) {
      $('body').removeClass('overlay');
      $('.site-search').removeClass('search-border');
    }
  });
}

function fixSmallscreenHeader() {
  const headerContainer = document.getElementById('header-container');
  headerContainer.classList.add('fixed');
  $('header').addClass('fixed');
  $('#maincontent').css('padding-top', headerContainer.offsetHeight);
}

function unfixSmallscreenHeader() {
  $('#header-container').removeClass('fixed');
  $('.search-mobile').removeClass('fixed');
  $('header').removeClass('fixed');
  $('.mobile-rightsec .sticky-search').removeClass('search-icon-show');
  $('#maincontent').css('padding-top', 0);
}

function stickyHeader() {
  const headerBanner = document.querySelector('.header-banner');
  if (isDesktop()) {
    if (window.scrollY >= getBannerHeight()) {
      //eslint-disable-line
      fixLargescreenHeader();
    } else {
      unfixLargescreenHeader();
    }
  } else {
    if (window.scrollY >= headerBanner.offsetHeight) {
      fixSmallscreenHeader();
    } else {
      unfixSmallscreenHeader();
    }
  }
}

module.exports = function () {
  /**
   * Creates a plain object that contains profile information
   * @param {Object} profile - current customer's profile
   */

  function addMarginPerBannerHeight() {
    if ($('#banner-content').find('.banner-asset').length > 0) {
      var bannerHtot = $('.header-banner').height();
      $('header .off5th-Nav-section').css('margin-top', bannerHtot);
    }
  }
  /**
   * Homepage Dots dynamic position
   */
  function homepagedots() {
    setTimeout(function () {
      if ($(window).width() <= 1023.99) {
        var h = $('.hero-banner').find('.content-image').height() - 30;
        $('.hero-banner').find('.slick-dots').css('top', h);
      }
    }, 100);
  }

  /**
   * Terms and condition pop up
   */
  function termclose() {
    $('.terms-overlay').remove();
    $('.terms-overlay').removeClass('show');
    $('.term-condition-section').addClass('hide');
  }

  $('body').on('click touchstart', '.terms-overlay', function () {
    termclose();
  });

  $('body').on('click', '.terms-condition', function (e) {
    e.preventDefault();
    $('body').append('<div class="terms-overlay"></div>');
    $('.terms-overlay').addClass('show');
    $('.term-condition-section').removeClass('hide');
  });

  $('#terms-condition-close').on('click', function () {
    termclose();
  });

  /**
   * View Details section Pop up
   */
  function detailsClose() {
    $('.view-details-overlay').remove();
    $('body').find('div.view-details-popup-main').remove();
  }

  $('body').on('click', '.view-details-home', function (e) {
    e.preventDefault();
    $('body').append('<div class="view-details-popup-main"><div class="view-details-overlay"></div></div>');
    $('body')
      .find('div.view-details-popup-main')
      .append($('.' + $(this).attr('id')).html());
  });

  $('body').on('click', '.view-details-popup-main .view-details-overlay', function () {
    detailsClose();
  });

  $('body').on('click', '.view-details-popup-main #view-detail-close', function () {
    detailsClose();
  });

  /**
   * View Details section Pop up
   */
  function detailsClose() {
    $('.view-details-overlay').remove();
    $('body').find('div.view-details-popup-main').remove();
  }

  $('body').on('click', '.view-details-home', function (e) {
    e.preventDefault();
    $('body').append('<div class="view-details-popup-main"><div class="view-details-overlay"></div></div>');
    $('body')
      .find('div.view-details-popup-main')
      .append($('.' + $(this).attr('id')).html());
  });

  $('body').on('click', '.view-details-popup-main .view-details-overlay', function () {
    detailsClose();
  });

  $('body').on('click', '.view-details-popup-main #view-detail-close', function () {
    detailsClose();
  });

  $(window).scroll(function () {
    if ($('.sign-in-page').length > 0) {
      if ($(window).width() >= 768 && $(window).width() <= 1023.99 && $(window).scrollTop() > 100) {
        stickyHeader();
      } else if ($(window).width() >= 1024 || $(window).width() <= 767) {
        stickyHeader();
      }
    } else {
      stickyHeader();
    }

    const footerUtilitySection = $('.footer-top-utility-section');

    if (footerUtilitySection.length) {
      if (window.innerHeight + window.scrollY > footerUtilitySection.offset().top) {
        if (!$('#promo-drawer').hasClass('promo-drawer-open')) {
          $('#drawer-tab').removeClass('ready');
        }
      } else {
        $('#drawer-tab').addClass('ready');
      }
    }
  });
  $(window).resize(function () {
    stickyHeader();
    homepagedots();
  });
  if ($('#banner-content').find('.banner-asset').length > 1) {
    $('#banner-content, #banner-contentmenu').slick({
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000
    });
    var totalSlides = $('#banner-content.slick-initialized .slick-list').find('.slick-slide').length;
    $('#banner-content.slick-initialized').find('.slick-list').attr('aria-live', 'polite');
    $('#banner-content.slick-initialized .slick-list')
      .find('.slick-slide')
      .each(function (i) {
        $(this).attr('aria-label', i + 1 + ' of ' + totalSlides);
      });
  }
  homepagedots();
  addMarginPerBannerHeight();
  resetIntialMainMenuHeight();

  var flagLoadInterval = setInterval(pullFlagLoad, 500);

  function pullFlagLoad() {
    if ($('#bfx-cc-wrapper').length > 0) {
      $('.header-login').prepend($('#bfx-cc-wrapper'));
      clearInterval(flagLoadInterval);
    }
  }
  // If Couldn't find the flag after 10 Sec, remove interval
  setTimeout(function () {
    clearInterval(flagLoadInterval);
  }, 10000);

  // Add Flex Break for Hero Banner CTAs
  $('.hero-banner .content-text').each(function () {
    var ctaLen = $(this).find('.cta-link').length;
    if (ctaLen == 6) {
      $(this).find('.cta-link:nth-child(3)').after("<div class='break'></div>");
    }
    if (ctaLen == 9) {
      $(this).find('.cta-link:nth-child(3)').after("<div class='break'></div>");
      $(this).find('.cta-link:nth-child(6)').after("<div class='break'></div>");
    }
  });
};
