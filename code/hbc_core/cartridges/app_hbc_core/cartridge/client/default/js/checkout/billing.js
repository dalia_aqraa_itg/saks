'use strict';
var base = require('base/checkout/billing');
var addressHelpers = require('base/checkout/address');
var hbcTooltip = require('../tooltip');

function appendCommaAfterCity($addressContainer, city) {
  if (
    $addressContainer.length > 0 &&
    $($addressContainer).find('.city').length > 0 &&
    $($addressContainer).find('.city').text() !== '' &&
    city &&
    city.length > 0 &&
    city !== ''
  ) {
    var updatedCity = city + ',';
    $($addressContainer).find('.city').text(updatedCity);
  }
}

function isSelectedPaymentMethodKlarna(order) {
  try {
    return order.billing.payment.selectedPaymentInstruments.some(
      pi => pi.paymentMethod === 'KLARNA_PAYMENTS'
    );
  } catch (e) {
    return false;
  }
}

/**
 * Updates the payment information in checkout, based on the supplied order model
 * @param {Object} order - checkout model to use as basis of new truth
 */
base.methods.updatePaymentInformation = function (order) {
  // update payment details
  var $paymentSummary = $('.payment-details:not(.payment-details--klarna-updated)');

  $paymentSummary.empty();
  var hasMPACards = false;
  if (order.billing.payment && order.billing.payment.selectedPaymentInstruments && order.billing.payment.selectedPaymentInstruments.length > 0) {
    $.each(order.billing.payment.selectedPaymentInstruments, function (index, value) {
      let htmlToAppend = '';
      if (value.name) {
        htmlToAppend += '<span>' + value.name + '</span>';
        if (value.paymentMethod === 'CREDIT_CARD' && value.type === 'SAKS' && value.maskedCreditCardNumber && value.maskedCreditCardNumber.length <= 10) {
          /**
           * This is potentially an [8,10] digit SAKS card. Therefore, no masked number is shown
           * since the last four digits will not match the actual card number. Consider revising
           * if https://hbcdigital.atlassian.net/browse/SFDEV-11183 is addressed.
           */
        } else if (value.shortMaskedNumber) {
          htmlToAppend += '<div class="order-card-number">' + value.shortMaskedNumber + '</div>';
        }
      }
      if (value.paymentMethod.toUpperCase() !== 'KLARNA' && $paymentSummary.length === 0) {
        $paymentSummary = $('.payment-details');
        $paymentSummary.empty();
      }
      $paymentSummary.append(htmlToAppend);
    });
  }
  if (hasMPACards) {
    $('.review-mpaMessage').attr('data-mpaenabled', 'true');
  } else {
    $('.review-mpaMessage').attr('data-mpaenabled', 'false');
  }
  hbcTooltip.tooltipInit();
};

base.methods.updateBillingInformation = function (order, customer) {
  base.methods.updateBillingAddressSelector(order, customer);

  // update billing address form
  base.methods.updateBillingAddressFormValues(order);

  // update billing address summary
  addressHelpers.methods.populateAddressSummary('.billing .address-summary', order.billing.billingAddress.address);

  if (order.billing && order.billing.billingAddress && order.billing.billingAddress.address && order.billing.billingAddress.address.city) {
    appendCommaAfterCity('.billing .address-summary', order.billing.billingAddress.address.city);
  }

  // update billing parts of order summary
  $('.order-summary-email').text(order.orderEmail);

  if (order.billing.billingAddress.address) {
    $('.order-summary-phone').text(order.billing.billingAddress.address.phone);
  }

  $('.billing-summary-section').toggle(!isSelectedPaymentMethodKlarna(order));

  // Also reset the TCC card number
  if ($('input[name$=tccCardNumber]').length > 0) {
    $('input[name$=tccCardNumber]').data('cleave').setRawValue('');
  }
};

base.methods.resetPLCCBanner = function () {
  var cardNumber = $('input[name$=cardNumber]').data('cleave').getRawValue('');
  if (cardNumber === '') {
    $('.card-number-wrapper').removeAttr('data-plcccard');
    $('.card-number-wrapper').attr('data-type', 'unknown');
  }
};


module.exports = {
  methods: base.methods
//   tooltip: hbcTooltip.tooltipInit,
//
//   showBillingDetails: function () {
//     $('.btn-show-billing-details').on('click', function () {
//       $(this).parents('[data-address-mode]').attr('data-address-mode', 'new');
//     });
//   },
//
//   hideBillingDetails: function () {
//     $('.btn-hide-billing-details').on('click', function () {
//       $(this).parents('[data-address-mode]').attr('data-address-mode', 'shipment');
//     });
//   },
//
//   updateStateOptions: function () {
//     $('body').on('change', 'select[name $= "billing_addressFields_country"]', function () {
//       var country = $(this).val();
//       if (country) {
//         // calling this function defined in EDQUtils.js which is globally included in header directly
//         // eslint-disable-next-line no-undef
//         window.EdqConfig.GLOBAL_INTUITIVE_ISO3_COUNTRY = countryAlpha3(country);
//       }
//       var form = $(this).closest('form');
//       var address1 = $('input[name $= "billing_addressFields_address1"]');
//       var address2 = $('input[name $= "billing_addressFields_address2"]');
//       var city = $('input[name $= "billing_addressFields_city"]');
//       var postalField = $(form).find('.validateZipCode');
//       postalField.val('');
//       postalField.next('span').remove();
//       postalField.removeClass('is-invalid');
//       postalField.prev('label').removeClass('is-invalid').removeClass('input-focus');
//       // clear address1
//       address1.val('');
//       address1.next('span').remove();
//       address1.removeClass('is-invalid');
//       address1.prev('label').removeClass('is-invalid').removeClass('input-focus');
//       // clear address2
//       address2.val('');
//       address2.next('span').remove();
//       address2.removeClass('is-invalid');
//       address2.prev('label').removeClass('is-invalid').removeClass('input-focus');
//       // clear city
//       city.val('');
//       city.next('span').remove();
//       city.removeClass('is-invalid');
//       city.prev('label').removeClass('is-invalid').removeClass('input-focus');
//       clientSideValidation.updatePoPatterWithCountry(form);
//       var stateField = $('select[name $= "billing_addressFields_states_stateCode"]');
//       var countryRegion = $('.countryRegion').data('countryregion');
//       if (!countryRegion) {
//         return;
//       }
//       var regions = countryRegion[country].regions;
//       var regionsLabel = countryRegion[country].regionLabel;
//       stateField.closest('.form-group').find('label.form-control-label').text(regionsLabel);
//       clientSideValidation.checkValidationOnAjax($('.billing-form'), true, true);
//       clientSideValidation.checkValidationOnAjax($('.credit-card-form'), true, true);
//       // if Country is UK, display the text field for state.
//       $('.js-state-code-input').val('');
//       if (country === 'UK') {
//         var optionArr = [];
//         for (var stateCode in regions) {
//           // eslint-disable-line
//           optionArr.push('<option id="' + stateCode + '" value="' + stateCode + '">' + regions[stateCode] + '</option>');
//         }
//         // Update the State Field
//         stateField.html(optionArr.join(''));
//
//         if ($('.site-locale').val() === 'fr_CA') {
//           postalField.closest('.form-group').find('label.form-control-label').text(postalField.data('zipcode-fr'));
//         } else {
//           postalField.closest('.form-group').find('label.form-control-label').text(postalField.data('zipcode-label'));
//         }
//
//         $('.state-drop-down').addClass('d-none');
//         $('.state-input').removeClass('d-none');
//         // validator.updatePostalCodePattern(form, country);
//         $('.state-drop-down').find('.form-group').removeClass('required');
//         $('.state-drop-down').find('.billingState').prop('required', false);
//         $('.state-drop-down').find('.billingState').removeClass('is-invalid');
//       } else {
//         // Generate the State Options
//         var optionArr = [];
//         for (var stateCode in regions) {
//           // eslint-disable-line
//           optionArr.push('<option value="' + stateCode + '">' + regions[stateCode] + '</option>');
//         }
//         // Update the State Field
//         stateField.html(optionArr.join(''));
//
//         if ($('.site-locale').val() === 'fr_CA') {
//           postalField.closest('.form-group').find('label.form-control-label').text(postalField.data('zipcode-fr'));
//         } else if (country === 'CA') {
//           postalField.closest('.form-group').find('label.form-control-label').text(postalField.data('zipcode-ca'));
//         } else {
//           postalField.closest('.form-group').find('label.form-control-label').text(postalField.data('zipcode-label'));
//         }
//         // validator.updatePostalCodePattern(form, country);
//         $('.state-drop-down').removeClass('d-none');
//         $('.state-input').addClass('d-none');
//         $('.state-drop-down').find('.form-group').addClass('required');
//         $('.state-drop-down').find('.billingState').prop('required', true);
//       }
//     });
//   },
//
//   selectBillingAddress: function () {
//     $('.payment-form .form-check.customer-addresses-section').on('click', function () {
//       $('.payment-form .form-check.customer-addresses-section').removeClass('selected');
//       $(this).addClass('selected');
//       $(this).find('input[type="radio"]').prop('checked', true);
//       var form = $(this).parents('form')[0];
//       var selectedOption = $(':checked', '.billing-addresses');
//       var optionID = selectedOption.val();
//
//       if (optionID === 'new') {
//         // Show Address
//         $(form).attr('data-address-mode', 'new');
//       } else {
//         // Hide Address
//         $(form).attr('data-address-mode', 'shipment');
//       }
//
//       // Copy fields
//       var attrs = selectedOption.data();
//       var element;
//
//       Object.keys(attrs).forEach(function (attr) {
//         element = attr === 'countryCode' ? 'country' : attr;
//         if (element === 'cardNumber') {
//           $('.cardNumber').data('cleave').setRawValue(attrs[attr]);
//         } else {
//           $('[name$=' + element + ']', form).val(attrs[attr]);
//         }
//       });
//     });
//   },
//
//   handleCreditCardNumber: function () {
//     cleave.handleCreditCardNumber('.cardNumber', '#cardType');
//   },
//
//   handleTemporaryCreditCardNumber: function () {
//     if ($('.tccCardNumber').length > 0) {
//       cleave.handleTemporaryCreditCardNumber('.tccCardNumber', '#cardType');
//     }
//   },
//
//   handleCOMXNumber: function () {
//     if ($('#comxAssociate1').length > 0 || $('#comxAssociate2').length > 0) {
//       cleave.handleCOMXNumber('#comxAssociate1');
//       cleave.handleCOMXNumber('#comxAssociate2');
//     }
//   },
//
//   santitizeForm: function () {
//     $('body').on('checkout:serializeBilling', function (e, data) {
//       var serializedForm = cleave.serializeData(data.form);
//       data.callback(serializedForm);
//     });
//   },
//
//   selectSavedPaymentInstrument: function () {
//     $(document).on('click', '.saved-payment-instrument', function (e) {
//       if (!$(this).is('.selected-payment')) {
//         $('.saved-payment-security-code').val('');
//         $('.saved-payment-instrument').removeClass('selected-payment');
//         $(this).addClass('selected-payment');
//         $('.saved-payment-instrument .card-image').removeClass('checkout-hidden');
//         $('.saved-payment-instrument .security-code-input').addClass('checkout-hidden');
//         $('.saved-payment-instrument.selected-payment' + ' .card-image').addClass('checkout-hidden');
//         $('.saved-payment-instrument.selected-payment ' + '.security-code-input').removeClass('checkout-hidden');
//         if ($(this).is('.cvvNumField')) {
//           if ($(this).find('.cvvNumField').val().length === 0) {
//             $(this).find('.cvvNumField').next('span').removeClass('valid');
//           }
//           if ($(this).find('.cvvNumField').hasClass('is-invalid')) {
//             $(this).find('.cvvNumField').next('span').addClass('invalid');
//           }
//         }
//         if (!$(e.target).is('.saved-payment-security-code')) {
//           floatLabel.resetFloatLabel();
//         }
//         clientSideValidation.checkValidationOnAjax($('.billing-form'), true, true);
//         $('.error-payment-message').addClass('d-none');
//       }
//     });
//   },
//
//   addNewPaymentInstrument: function () {
//     $('body').on('click', '.add-payment-section-wrapper', function (e) {
//       e.preventDefault();
//       $('.payment-information').data('is-new-payment', true);
//       clearCreditCardForm();
//       $('.credit-card-form').removeClass('checkout-hidden');
//       $('.user-payment-instruments').addClass('checkout-hidden');
//       $('.card.payment-form .billing-cancel').removeClass('d-none');
//       $('.card.payment-form .payment-next-step-button-row').addClass('cancel-button-enable');
//       $('.card.payment-form .payment-next-step-button-row').addClass('card-open');
//       $('.save-card-section').removeClass('d-none');
//       $('.save-card-section').find('#saveCreditCard').prop('checked', true);
//       setTimeout(function () {
//         floatLabel.resetFloatLabel();
//         clientSideValidation.checkValidationOnAjax($('.credit-card-form'), true, true);
//       }, 200);
//       $('.error-payment-message').addClass('d-none');
//     });
//
//     $('.error-payment-message').addClass('d-none');
//   },
//
//   cancelNewPayment: function () {
//     $('.cancel-new-payment').on('click', function (e) {
//       e.preventDefault();
//       $('.payment-information').data('is-new-payment', false);
//       clearCreditCardForm();
//       $('.user-payment-instruments').removeClass('checkout-hidden');
//       $('.credit-card-form').addClass('checkout-hidden');
//       /* if ($('.enableBillingButton').length > 0 && $('.enableBillingButton').val() === 'false') {
//                     $('.card.payment-form').find('.submit-payment').attr('disabled', 'disabled');
//                 } else {
//                     $('.card.payment-form').find('.submit-payment').removeAttr('disabled');
//                 }*/
//       setTimeout(function () {
//         floatLabel.resetFloatLabel();
//         clientSideValidation.checkValidationOnAjax($('.billing-form'), true, true);
//       }, 200);
//     });
//   },
//
//   clearBillingForm: function () {
//     $('body').on('checkout:clearBillingForm', function () {
//       base.methods.clearBillingAddressFormValues();
//     });
//   },
//
//   paymentTabs: function () {
//     $('.payment-options .nav-item').on('click', function (e) {
//       e.preventDefault();
//       if ($(this).find('input').prop('disabled')) { return false };
//       base.methods.selectPaymentMethod($(this));
//     });
//   },
//   tokenizeCard: function () {
//     $('body').on('checkout:tokenizeCard', function (e, data) {
//       var token = tokenEx.encryptCC(data.publicKey, data.cardNumber);
//       data.callback(token);
//     });
//   },
//   validatePLCCcard: function () {
//     $('#cardNumber').keyup(function () {
//       if (!$('#credit-card-content').find('#mpaMessaging').hasClass('d-none')) {
//         $('#credit-card-content').find('#mpaMessaging').addClass('d-none');
//         $('#credit-card-content').closest('.checkout-primary-section').removeClass('mpa-card');
//       }
//       var cardNumber = $('#cardNumber').data('cleave').getRawValue();
//       if (cardNumber !== '') {
//         var cardNumberInt = cardNumber; // eslint-disable-line
//         var cardType = HBCCards.cardType(cardNumberInt);
//         //check for Saks 8 digit cards
//         var saksCard = HBCCards.saksCard(cardNumber);
//         var mainForm = $('.payment-form-fields .credit-card-form');
//
//         if (cardType.name || (saksCard && saksCard.name)) {
//           var cardName;
//           if (cardType.name) {
//             cardName = cardType.name;
//           } else {
//             cardName = saksCard.name;
//           }
//           $('.card-number-wrapper').attr('data-type', cardName);
//           $('.card-number-wrapper').attr('data-plcccard', 'true');
//           $('#cardType').val(cardName);
//
//           // Make the CVV  and Expiration fields as not mandatory fields if user enters a PLCC card
//           if (cardType.name && (cardType.name === 'HBCMC' || cardType.name === 'SAKSMC')) {
//             mainForm.find('.cardExpiryDate').removeClass('d-none');
//             mainForm.find('.newCreditCvvHolder').removeClass('d-none');
//             mainForm.find('.cardExpiryDate').addClass('required');
//             mainForm.find('.cardExpiryMonth').addClass('required');
//             mainForm.find('.cardExpiryYear').addClass('required');
//             mainForm.find('.newCreditCvvHolder').addClass('required');
//             mainForm.find('.cardExpiration').prop('required', true);
//             mainForm.find('.expirationMonth').prop('required', true);
//             mainForm.find('.expirationYear').prop('required', true);
//             mainForm.find('.securityCode').prop('required', true);
//           } else if (cardType.name && cardType.name === 'SAKS' && cardType.length[0] === 16) {
//             mainForm.find('.cardExpiryDate').removeClass('d-none');
//             if (isTsysMode()) {
//               mainForm.find('.newCreditCvvHolder').removeClass('d-none');
//             } else {
//               mainForm.find('.newCreditCvvHolder').addClass('d-none');
//             }
//             mainForm.find('.cardExpiryDate').addClass('required');
//             mainForm.find('.cardExpiryMonth').addClass('required');
//             mainForm.find('.cardExpiryYear').addClass('required');
//             if (isTsysMode()) {
//               mainForm.find('.newCreditCvvHolder').addClass('required');
//             } else {
//               mainForm.find('.newCreditCvvHolder').removeClass('required');
//             }
//             mainForm.find('.cardExpiration').prop('required', true);
//             mainForm.find('.expirationMonth').prop('required', true);
//             mainForm.find('.expirationYear').prop('required', true);
//             if (isTsysMode()) {
//               mainForm.find('.securityCode').prop('required', true);
//             } else {
//               mainForm.find('.securityCode').prop('required', false);
//             }
//           } else {
//             mainForm.find('.cardExpiryDate').addClass('d-none');
//             mainForm.find('.newCreditCvvHolder').addClass('d-none');
//             mainForm.find('.cardExpiration').val('');
//             mainForm.find('#expirationMonth').val('');
//             mainForm.find('#expirationYear').val('');
//             mainForm.find('.cardExpiration').removeClass('is-invalid');
//             mainForm.find('.cardExpiration').next('span').remove();
//             mainForm.find('.cardExpiration').removeAttr('placeholder');
//             mainForm.find('.cardExpiration').prev('label').removeClass('is-invalid').removeClass('input-focus');
//
//             mainForm.find('.securityCode').val('');
//             mainForm.find('.securityCode').removeClass('is-invalid');
//             mainForm.find('.securityCode').next('span').remove();
//             mainForm.find('.securityCode').prev('label').removeClass('is-invalid').removeClass('input-focus');
//           }
//           // show MPA messaging
//           if (cardType.name === 'MPA') {
//             $('#credit-card-content').closest('.checkout-primary-section').addClass('mpa-card');
//             $('#credit-card-content').find('#mpaMessaging').removeClass('d-none');
//           }
//         } else {
//           mainForm.find('.cardExpiryDate').removeClass('d-none');
//           mainForm.find('.newCreditCvvHolder').removeClass('d-none');
//           mainForm.find('.cardExpiryDate').addClass('required');
//           mainForm.find('.cardExpiryMonth').addClass('required');
//           mainForm.find('.cardExpiryYear').addClass('required');
//           mainForm.find('.newCreditCvvHolder').addClass('required');
//           mainForm.find('.cardExpiration').prop('required', true);
//           mainForm.find('.expirationMonth').prop('required', true);
//           mainForm.find('.expirationYear').prop('required', true);
//           mainForm.find('.securityCode').prop('required', true);
//
//           cleave.handleCreditCardNumber('.cardNumber', '#cardType');
//         }
//         showTCCLink(cardNumber, $('.card-number-wrapper').attr('data-type'));
//       } else {
//         $('.card-number-wrapper').attr('data-type', 'unknown');
//         $('.card-number-wrapper').attr('data-plcccard', 'false');
//         $('#cardType').val('Unknown');
//       }
//     });
//   },
//   handleCardExpirationPlaceholder: function () {
//     $('.cardExpiration').on('focusout', function () {
//       if (typeof $(this).attr('required') === 'undefined') {
//         $(this).removeAttr('placeholder');
//       }
//     });
//   },
//   openTCCCardHolder: function () {
//     $('.use-tcc-card').on('click', function () {
//       var enteredCardNumber = $('#cardNumber').data('cleave').getRawValue();
//       // Hide all regular card component
//       $(this).closest('.tcc-link').addClass('d-none');
//       var mainForm = $('.payment-form-fields .credit-card-form');
//
//       mainForm.attr('data-tccopen', 'true');
//
//       mainForm.find('#cardNumber').val('');
//       mainForm.find('#cardNumber').removeClass('is-invalid');
//       mainForm.find('#cardNumber').next('span').remove();
//       mainForm.find('#cardNumber').prev('label').removeClass('is-invalid').removeClass('input-focus');
//       mainForm.find('.regular-card-number').addClass('d-none');
//
//       mainForm.find('.cardExpiration').val('');
//       mainForm.find('#expirationMonth').val('');
//       mainForm.find('#expirationYear').val('');
//       mainForm.find('.cardExpiration').removeClass('is-invalid');
//       mainForm.find('.cardExpiration').next('span').remove();
//       mainForm.find('.cardExpiration').removeAttr('placeholder');
//       mainForm.find('.cardExpiration').prev('label').removeClass('is-invalid').removeClass('input-focus');
//       mainForm.find('.regular-expiry').addClass('d-none');
//
//       mainForm.find('.securityCode').val('');
//       mainForm.find('.securityCode').removeClass('is-invalid');
//       mainForm.find('.securityCode').next('span').remove();
//       mainForm.find('.securityCode').prev('label').removeClass('is-invalid').removeClass('input-focus');
//       mainForm.find('.regular-cvv').addClass('d-none');
//
//       // Copy Current entered data and populate in TCC field
//       mainForm.find('.tccCardNumber').val(enteredCardNumber);
//       mainForm.find('.tccCardNumber').prop('required', true);
//       cleave.handleTemporaryCreditCardNumber('.tccCardNumber', '#cardType');
//
//       if (!isTsysMode()) {
//         mainForm.find('.save-card-section').addClass('d-none');
//         mainForm.find('.save-card-section').find('#saveCreditCard').prop('checked', false);
//       }
//
//       $('.tcc-card-number-wrapper').removeAttr('data-type');
//       $('.tcc-card-number-wrapper').removeAttr('data-plcccard');
//
//       if ($('.tcc-card-number').hasClass('d-none')) {
//         $('.tcc-card-number').removeClass('d-none');
//       }
//
//       mainForm.find('.tccCardNumber').next('label').addClass('input-focus');
//       $('.billing-tcc-cancel').removeClass('d-none');
//       $('.billing-tcc-cancel').closest('.payment-form').find('.payment-next-step-button-row').addClass('plcc-card-visibility');
//       if (!$('.card.payment-form .billing-cancel').hasClass('d-none')) {
//         $('.card.payment-form .billing-cancel').addClass('d-none');
//       }
//     });
//   },
//
//   cancelTCCCardHolder: function () {
//     $('.billing-tcc-cancel').on('click', function () {
//       // Hide all regular card component
//       $(this).closest('.billing-tcc-cancel').addClass('d-none');
//       $(this).closest('.payment-form').find('.payment-next-step-button-row').removeClass('plcc-card-visibility');
//       var mainForm = $('.payment-form-fields .credit-card-form');
//
//       mainForm.removeAttr('data-tccopen');
//
//       if (!$('.tcc-card-number').hasClass('d-none')) {
//         mainForm.find('.tcc-card-number').addClass('d-none');
//       }
//
//       mainForm.find('.cardExpiryDate').removeClass('d-none');
//       mainForm.find('.newCreditCvvHolder').removeClass('d-none');
//       mainForm.find('.cardExpiryDate').addClass('required');
//       mainForm.find('.cardExpiryMonth').addClass('required');
//       mainForm.find('.cardExpiryYear').addClass('required');
//       mainForm.find('.newCreditCvvHolder').addClass('required');
//       mainForm.find('.cardExpiration').prop('required', true);
//       mainForm.find('.expirationMonth').prop('required', true);
//       mainForm.find('.expirationYear').prop('required', true);
//       mainForm.find('.securityCode').prop('required', true);
//
//       // Remove all invalid classes
//       mainForm.find('.cardNumber').removeClass('is-invalid').removeClass('focus-visible');
//       mainForm.find('.cardNumber').next('label').removeClass('is-invalid').removeClass('input-focus');
//
//       mainForm.find('.cardExpiration').removeClass('is-invalid').removeClass('focus-visible');
//       mainForm.find('.cardExpiration').next('label').removeClass('is-invalid').removeClass('input-focus');
//
//       mainForm.find('.securityCode').removeClass('is-invalid').removeClass('focus-visible');
//       mainForm.find('.securityCode').next('label').removeClass('is-invalid').removeClass('input-focus');
//
//       mainForm.find('#tccCardNumber').prop('required', true);
//       mainForm.find('#tccCardNumber').removeClass('is-invalid').removeClass('focus-visible');
//       mainForm.find('#tccCardNumber').next('label').removeClass('is-invalid').removeClass('input-focus');
//
//       mainForm.find('.regular-card-number').removeClass('d-none');
//
//       mainForm.find('.regular-expiry').removeClass('d-none');
//
//       mainForm.find('.regular-cvv').removeClass('d-none');
//
//       // Clean TCC field, regular card
//       mainForm.find('.tccCardNumber').val('');
//       mainForm.find('#tccCardNumber').val('');
//       mainForm.find('.cardNumber').val('');
//       mainForm.find('.cardOwner').val('');
//       mainForm.find('#tccCardNumber').removeClass('is-invalid');
//       mainForm.find('#tccCardNumber').next('span').remove();
//       mainForm.find('#tccCardNumber').next('label').removeClass('is-invalid').removeClass('input-focus');
//
//       $('.card-number-wrapper').removeAttr('data-type');
//       $('.card-number-wrapper').removeAttr('data-plcccard');
//
//       mainForm.find('.save-card-section').removeClass('d-none');
//       mainForm.find('.save-card-section').find('#saveCreditCard').prop('checked', true);
//
//       cleave.handleCreditCardNumber('.cardNumber', '#cardType');
//
//       if ($('.card.payment-form .payment-next-step-button-row').hasClass('card-open')) {
//         $('.card.payment-form .billing-cancel').removeClass('d-none');
//       } else {
//         $('.card.payment-form .billing-cancel').addClass('d-none');
//       }
//
//       if (!$('.tcc-link').hasClass('d-none')) {
//         $('.tcc-link').addClass('d-none');
//       }
//     });
//   },
//
//   showBillingAddr: function () {
//     $(document).on('change', '.billing-checkbox', function () {
//       var form = $(this).closest('form');
//
//       if (this.checked) {
//         if ($(form).find('.billing-addr-saved').hasClass('registered-user')) {
//           $(form).find('.billing-addr-saved').removeClass('d-none');
//           $(form).find('.billing-addresses').removeClass('d-none');
//           $(form).find('.customer-addresses-section').removeClass('d-none');
//           // changes done to update the address once edited and saved in shipping accordion
//           if ($(':checked', '.billing-addresses').length) {
//             var billform = $('form[name=dwfrm_billing]');
//             var selectedOption = $(':checked', '.billing-addresses');
//
//             $('input[name$=_firstName]', billform).val(selectedOption.data('first-name'));
//             $('input[name$=_lastName]', billform).val(selectedOption.data('last-name'));
//             $('input[name$=_address1]', billform).val(selectedOption.data('address1'));
//             $('input[name$=_address2]', billform).val(selectedOption.data('address2'));
//             $('input[name$=_city]', billform).val(selectedOption.data('city'));
//             $('input[name$=_postalCode]', billform).val(selectedOption.data('postal-code'));
//             $('select[name$=_stateCode],input[name$=_stateCode]', billform).val(selectedOption.data('state-code'));
//             $('select[name$=_country]', billform).val(selectedOption.data('country-code'));
//           }
//         } else {
//           clientSideValidation.checkValidationOnAjax($('.billing-address-block'), true, true);
//           $(form).find('.billing-addr-saved').addClass('d-none');
//           clientSideValidation.updatePoPatterWithCountry(form);
//           $(form).find('.billing-addr-form').removeClass('d-none');
//           clearBillingAddrFormValues();
//         }
//         floatLabel.resetFloatLabel();
//         // changes done to udpate the state drop down
//         if ($('select[name $= "billing_addressFields_country"]').val() !== null) {
//           var country = $('select[name $= "billing_addressFields_country"]').val();
//           var billingState;
//           var stateField = $('select[name $= "billing_addressFields_states_stateCode"]');
//           var countryRegion = $('.countryRegion').data('countryregion');
//           if (!countryRegion) {
//             return;
//           }
//           var regions = countryRegion[country].regions;
//           var regionsLabel = countryRegion[country].regionLabel;
//           stateField.closest('.form-group').find('label.form-control-label').text(regionsLabel);
//           var postalField = $('.billing-form').find('.billingZipCode');
//           // if Country is UK, display the text field for state.
//           if (country === 'UK') {
//             var optionArr = [];
//             for (var stateCode in regions) {
//               // eslint-disable-line
//               optionArr.push('<option id="' + stateCode + '" value="' + stateCode + '">' + regions[stateCode] + '</option>');
//             }
//             // Update the State Field
//             stateField.html(optionArr.join(''));
//             if ($('.site-locale').val() === 'fr_CA') {
//               postalField.closest('.form-group').find('label.form-control-label').text(postalField.data('zipcode-fr'));
//               // postalField.prev('label').text(postalField.data('zipcode-fr'));
//             } else {
//               postalField.closest('.form-group').find('label.form-control-label').text(postalField.data('zipcode-label'));
//               // postalField.prev('label').text(postalField.data('zipcode-label'));
//             }
//
//             if ($('.orderBillState').length && $('.orderBillState').val() !== 'null') {
//               billingState = $('.orderBillState').val();
//             } else {
//               billingState = $('.js-state-code-input').val();
//             }
//             $('.state-drop-down').addClass('d-none');
//             $('.state-input').removeClass('d-none');
//             // $('form[name=dwfrm_billing]').find('.js-state-code-input').val(billingState);
//             $('.state-drop-down').find('.form-group').removeClass('required');
//             $('.state-drop-down').find('.billingState').prop('required', false);
//             $('.state-drop-down').find('.billingState').removeClass('is-invalid');
//           } else {
//             // Generate the State Options
//             var optionArr = [];
//             for (var stateCode in regions) {
//               // eslint-disable-line
//               optionArr.push('<option value="' + stateCode + '">' + regions[stateCode] + '</option>');
//             }
//             // Update the State Field
//             stateField.html(optionArr.join(''));
//             if ($('.site-locale').val() === 'fr_CA') {
//               postalField.closest('.form-group').find('label.form-control-label').text(postalField.data('zipcode-fr'));
//               // postalField.prev('label').text(postalField.data('zipcode-fr'));
//             } else if (country === 'CA') {
//               postalField.closest('.form-group').find('label.form-control-label').text(postalField.data('zipcode-ca'));
//               // postalField.prev('label').text(postalField.data('zipcode-ca'));
//             } else {
//               postalField.closest('.form-group').find('label.form-control-label').text(postalField.data('zipcode-label'));
//               // postalField.prev('label').text(postalField.data('zipcode-label'));
//             }
//
//             if ($('.orderBillState').length && $('.orderBillState').val() !== 'null') {
//               billingState = $('.orderBillState').val();
//             } else {
//               billingState = $('.billingState').val();
//             }
//
//             $('.state-drop-down').removeClass('d-none');
//             $('.state-input').addClass('d-none');
//             // $('form[name=dwfrm_billing]').find('select[name$=_stateCode]').val(billingState);
//             $('.state-drop-down').find('.form-group').addClass('required');
//             $('.state-drop-down').find('.billingState').prop('required', true);
//           }
//           clientSideValidation.updatePoPatterWithCountry(form);
//         }
//         setTimeout(function () {
//           clientSideValidation.checkValidationOnAjax($('.billing-address-block'), true, true);
//         }, 200);
//       } else {
//         // copy shipping in to billing when check box is unchecked
//         var shippingForm = $('form[name=dwfrm_shipping]');
//         var billform = $('form[name=dwfrm_billing]');
//         if (shippingForm.length > 0) {
//           $('input[name$=_billing_addressFields_firstName]', billform).val(shippingForm.find('input[name$=_firstName]').val());
//           $('input[name$=_billing_addressFields_lastName]', billform).val(shippingForm.find('input[name$=_lastName]').val());
//           $('input[name$=_billing_addressFields_address1]', billform).val(shippingForm.find('input[name$=_address1]').val());
//           $('input[name$=_billing_addressFields_address2]', billform).val(shippingForm.find('input[name$=_address2]').val());
//           $('input[name$=_billing_addressFields_city]', billform).val(shippingForm.find('input[name$=_city]').val());
//           $('input[name$=_billing_addressFields_postalCode]', billform).val(shippingForm.find('input[name$=_postalCode]').val());
//           $('select[name$=_billing_addressFields_country]', billform).val(shippingForm.find('select[name$=_country]').val());
//           $('select[name$=_stateCode],input[name$=_stateCode]', billform).val(shippingForm.find('select[name$=_stateCode]').val());
//           $('input[name$=_billing_addressFields_stateCodeUK]', billform).val('');
//         }
//         clientSideValidation.checkValidationOnAjax($('.billing-form'), true, true);
//         clientSideValidation.checkValidationOnAjax($('.credit-card-form'), true, true);
//         if ($(form).find('.billing-addr-saved').hasClass('registered-user')) {
//           $(form).find('.billing-addr-saved').addClass('d-none');
//         }
//         $(form).find('.billing-addr-form').addClass('d-none');
//         if ($('.card.payment-form .payment-next-step-button-row').hasClass('card-open')) {
//           $('.card.payment-form .billing-cancel').removeClass('d-none');
//         } else {
//           $('.card.payment-form .billing-cancel').addClass('d-none');
//         }
//         if (!$('.card.payment-form .billing-tcc-cancel').hasClass('d-none')) {
//           $('.card.payment-form .billing-cancel').addClass('d-none');
//         }
//       }
//       setTimeout(function () {
//         floatLabel.resetFloatLabel();
//       }, 200);
//     });
//   },
//   billingAddressFocusHandler: function () {
//     $('.billingFirstName').on('focus', function () {
//       setTimeout(function () {
//         var $label = $('.billingFirstName').closest('.form-group').find('label.form-control-label');
//         if (!$label.hasClass('input-focus')) {
//           $label.addClass('input-focus');
//         }
//       }, 500);
//     });
//   },
//
//   billingCountryChangeHandler: function () {
//     $('.billingCountry').on('change', function () {
//       setTimeout(function () {
//         floatLabel.resetFloatLabel();
//       }, 500);
//     });
//   },
//
//   cardExpiry: function () {
//     $('.cardExpiration').attr('type', 'tel');
//     $('.cardExpiration').on('focus', function () {
//       $(this).prop('placeholder', 'MM/YY');
//     });
//     // date mask
//     if ($('.cardExpiration').length > 0) {
//       $('.cardExpiration').mask('00/00');
//     }
//   },
//   editBillingAddress: function () {
//     $(document).on('click', '.edit-customerbill-address', function (e) {
//       e.preventDefault();
//       var billingAddress = {};
//       var address = {};
//       // eslint-disable-next-line no-unused-vars
//       var updatedForm;
//       var addressUrl = $(this).closest('.address-result').data('edit-url');
//       var addressId = $(this).closest('.address-result').data('addressid');
//       if (addressId && addressUrl) {
//         var addressInfo = $(this)
//           .closest('.address-result')
//           .find('input[id="' + addressId + '"]')
//           .data('address-info');
//         if (addressInfo) {
//           billingAddress = addressInfo;
//           address.billingAddress = billingAddress;
//           updatedForm = updateBillingAddressFromCustAddr(address);
//         }
//         toggleBillingItems('edit');
//         setTimeout(function () {
//           clientSideValidation.checkValidationOnAjax($('.billing-addr-form'), true, true);
//           clientSideValidation.updatePoPatterWithCountry($('.billing-addr-form'));
//         }, 200);
//       }
//     });
//   },
//   cancelBillingAddr: function () {
//     $('.billing-save-cancel').on('click', function (e) {
//       e.preventDefault();
//       toggleBillingItems('address');
//     });
//   },
//   addNewAddressRegisteredBilling: function () {
//     $('body').on('click', 'form.billing-form .customer-addresses-section.btn-add-new-bill', function () {
//       var $el = $(this);
//       clearBillingAddrFormValues();
//       var $option = $($el.parents('form').find('.addressSelector')[0]);
//       $option.attr('value', 'new');
//       var $newTitle = $('#dwfrm_billing input[name=localizedNewAddressTitle]').val();
//       $option.text($newTitle);
//       $option.prop('checked', true);
//       $el.parents('[data-address-mode]').attr('data-address-mode', 'new');
//
//       setTimeout(function () {
//         $('#billingCountry').trigger('change');
//         floatLabel.resetFloatLabel();
//         clientSideValidation.checkValidationOnAjax($('.billing-addr-form'), true, true);
//         clientSideValidation.updatePoPatterWithCountry($('.billing-addr-form'));
//       }, 200);
//       $('form.billing-form .billing-addr-saved').addClass('d-none');
//       $('.card.payment-form .billing-cancel').removeClass('d-none');
//       $('div.billing-tcc-cancel').addClass('d-none');
//       $('.card.payment-form .payment-next-step-button-row').addClass('cancel-button-enable');
//       $('.card.payment-form .payment-next-step-button-row').addClass('addr-open');
//       toggleBillingItems('add');
//     });
//   },
//   giftCardToggle: function () {
//     $('.gift-card-click').on('click', function () {
//       if ($(this).hasClass('active')) {
//         $(this).removeClass('active');
//         $('.gift-card-form-group').addClass('d-none');
//         floatLabel.resetFloatLabel();
//         clientSideValidation.checkValidationOnAjax($('.gift-card-form-group'), true);
//       } else {
//         $(this).addClass('active');
//         // eslint-disable-next-line no-undef
//         $('.gift-card-form-group').removeClass('d-none');
//         floatLabel.resetFloatLabel();
//         clientSideValidation.checkValidationOnAjax($('.gift-card-form-group'), true);
//       }
//       $('.gift-card-form-group .common-error .generic-error').empty();
//     });
//   },
//   // update the form data on change of expiration
//   updateExpirationDate: function () {
//     $('body').on('focusout', '#cardExpiration', function () {
//       var creditCard = $(this).closest('form');
//       if ($('#cardExpiration').val() !== null && $('#cardExpiration').val() !== '') {
//         $('#expirationMonth').val('');
//         $('#expirationYear').val('');
//         $(this).find('.form-group').find('.invalid-feedback').empty();
//         var invalidExp = expirationValidation($(this));
//         if (!invalidExp) {
//           var cardExp = $(this).val();
//           var month = cardExp.split('/')[0];
//           var currDate = new Date().getFullYear().toString().substr(0, 2);
//           var year = currDate + cardExp.split('/')[1];
//           creditCard.find('#expirationMonth').val(month);
//           creditCard.find('#expirationYear').val(year);
//           $(this).removeClass('is-invalid');
//           $(this).prev('.form-control-label').removeClass('is-invalid');
//           $(this).closest('div').find('.invalid-feedback').text('');
//           if ($(this).next('span').length === 0) {
//             $('<span></span>').insertAfter(this);
//             $(this).next('span').addClass('valid');
//           }
//           if ($(this).next('span').length !== 0 && $(this).next('span').hasClass('invalid')) {
//             $(this).next('span').removeClass('invalid').addClass('valid');
//           }
//         } else {
//           $('#expirationMonth').val('');
//           $('#expirationYear').val('');
//           var validationMessage = $(this).data('pattern-mismatch');
//           $(this).parents('.form-group').find('.invalid-feedback').text(validationMessage);
//           $(this).prev('.form-control-label').addClass('is-invalid');
//           $(this).addClass('is-invalid');
//           if ($(this).next('span').length === 0) {
//             $('<span></span>').insertAfter(this);
//             $(this).next('span').addClass('invalid');
//           }
//           if ($(this).next('span').hasClass('valid')) {
//             $(this).next('span').removeClass('valid').addClass('invalid');
//           }
//           var form = {};
//           form.formName = 'payment';
//           form.errorFields = ['cardExpiration'];
//           // $('body').trigger('adobeTagManager:formError', form);
//         }
//       }
//     });
//   },
//   saveSelectedAddr: function () {
//     $(document).on('click', 'form.billing-form .billing-addr-saved .customer-addresses-section.billing-cust-addr-sec', function (e) {
//       e.preventDefault();
//       $('.payment-form .form-check.customer-addresses-section').removeClass('selected');
//       $(this).addClass('selected');
//       $(this).find('input[type="radio"]').prop('checked', true);
//       var form = $('form[name=dwfrm_billing]');
//       var selectedOption = $(':checked', '.billing-addresses');
//
//       $('input[name$=_firstName]', form).val(selectedOption.data('first-name'));
//       $('input[name$=_lastName]', form).val(selectedOption.data('last-name'));
//       $('input[name$=_address1]', form).val(selectedOption.data('address1'));
//       $('input[name$=_address2]', form).val(selectedOption.data('address2'));
//       $('input[name$=_city]', form).val(selectedOption.data('city'));
//       $('input[name$=_postalCode]', form).val(selectedOption.data('postal-code'));
//       $('select[name$=_stateCode],input[name$=_stateCode]', form).val(selectedOption.data('state-code'));
//       $('select[name$=_country]', form).val(selectedOption.data('country-code'));
//     });
//   },
//
//   cvvNumValidation: function () {
//     $(document).on('keydown', '.cvvNumField', function (e) {
//       if (e.which === 38 || e.which === 40 || e.which === 69) {
//         e.preventDefault();
//       }
//       if (e.ctrlKey === true && (e.which === 118 || e.which === 86)) {
//         e.preventDefault();
//       }
//       if ($(this).parents('.savedCreditCvvHolder').length > 0) {
//         var savedCardType = $(this).data('type');
//         if (savedCardType === 'Amex') {
//           $(this).attr('maxlength', 4);
//         } else {
//           $(this).attr('maxlength', 3);
//         }
//       }
//     });
//
//     $(document).on('keypress', '.cvvNumField', function (event) {
//       return event.charCode === 0 || /\d/.test(String.fromCharCode(event.charCode));
//     });
//
//     $(document).on('blur', '.cvvNumField', function () {
//       var cvvData = $(this).val();
//       var cvvLen = cvvData.length;
//       var maxVal = parseInt($(this).attr('maxlength'), 10);
//
//       if (cvvLen !== maxVal) {
//         $(this).prev('.form-control-label').addClass('is-invalid');
//         $(this).addClass('is-invalid');
//         if (!cvvData) {
//           $(this).parent().find('.invalid-feedback').text($(this).data('missing-error'));
//         } else {
//           $(this).parent().find('.invalid-feedback').text($(this).data('parsing-error'));
//         }
//         if ($(this).next('span').length === 0) {
//           $('<span></span>').insertAfter(this);
//         }
//         $(this).next('span').addClass('invalid');
//         if ($(this).next('span').hasClass('valid')) {
//           $(this).next('span').removeClass('valid').addClass('invalid');
//         }
//         // $('.card.payment-form').find('.submit-payment').attr('disabled', 'disabled');
//         var form = {};
//         form.formName = 'payment';
//         form.errorFields = ['cvvNumber'];
//         // $('body').trigger('adobeTagManager:formError', form);
//       } else if ($(this).parents('.savedCreditCvvHolder').length) {
//         $(this).prev('.form-control-label').removeClass('is-invalid');
//         $(this).removeClass('is-invalid');
//         $(this).parents('.form-group').find('.invalid-feedback').empty();
//         if ($(this).next('span').length === 0) {
//           $('<span></span>').insertAfter(this);
//         }
//         $(this).next('span').removeClass('invalid').addClass('valid');
//       }
//     });
//   },
//   /* enableBillingButton: function () {
//         $(document).ready(function () {
//             if ($('.enableBillingButton').length > 0 && $('.enableBillingButton').val() === 'false') {
//                 $('.card.payment-form').find('.submit-payment').attr('disabled', 'disabled');
//             }
//             clientSideValidation.checkValidationOnAjax($('.credit-card-form'), false, true);
//         });
//     },*/
//
//   // Shows authorization message to save credit card which is configured in asset.
//   showAuthorizemesgCC: function () {
//     $(document).on('click', '.saveCC', function () {
//       if ($(this).is(':checked')) {
//         $(this).closest('.save-card-section').find('.authorization-msg-cc').removeClass('d-none');
//       } else {
//         $(this).closest('.save-card-section').find('.authorization-msg-cc').addClass('d-none');
//       }
//     });
//   },
//
//   // Changes done for SFDEV-5333, for hide/show the forms on payment section in checkout
//   hideBillingForms: function () {
//     $(document).on('click', '.billing-cancel .billing-allforms-cancel', function () {
//       if ($('.card.payment-form .billing-address-block').find('.billing-addr-form').is(':visible')) {
//         if ($(':checked', '.billing-addresses').length) {
//           var billform = $('form[name=dwfrm_billing]');
//           var selectedOption = $(':checked', '.billing-addresses');
//
//           $('input[name$=_firstName]', billform).val(selectedOption.data('first-name'));
//           $('input[name$=_lastName]', billform).val(selectedOption.data('last-name'));
//           $('input[name$=_address1]', billform).val(selectedOption.data('address1'));
//           $('input[name$=_address2]', billform).val(selectedOption.data('address2'));
//           $('input[name$=_city]', billform).val(selectedOption.data('city'));
//           $('input[name$=_postalCode]', billform).val(selectedOption.data('postal-code'));
//           $('select[name$=_stateCode],input[name$=_stateCode]', billform).val(selectedOption.data('state-code'));
//           $('select[name$=_country]', billform).val(selectedOption.data('country-code'));
//         }
//         $('.card.payment-form').find('.billing-save-cancel').trigger('click');
//       }
//
//       if ($('.card.payment-form').find('.credit-card-form').is(':visible')) {
//         $('.card.payment-form').find('.cancel-new-payment').trigger('click');
//       }
//
//       if (
//         $('.card.payment-form .billing-address-block').find('.billing-addr-form').is(':visible') ||
//         $('.card.payment-form').find('.credit-card-form').is(':visible')
//       ) {
//         $('.card.payment-form .billing-cancel').removeClass('d-none');
//         $('.card.payment-form .payment-next-step-button-row').addClass('cancel-button-enable');
//       } else {
//         $('.card.payment-form .billing-cancel').addClass('d-none');
//         $('.card.payment-form .payment-next-step-button-row').removeClass('cancel-button-enable');
//         $('.card.payment-form .payment-next-step-button-row').removeClass('card-open');
//         $('.card.payment-form .payment-next-step-button-row').removeClass('addr-open');
//       }
//
//       // Reset the TCC Form
//       if ($('.regular-card-number').hasClass('d-none')) {
//         $('body').trigger('checkout:resetTCCForm');
//       }
//       clientSideValidation.checkValidationOnAjax($('.billing-form'), true, true);
//       clientSideValidation.checkValidationOnAjax($('.credit-card-form'), true, true);
//     });
//   },
//
//   listenForTotalsUpdates: function () {
//     $('body').on('checkout:updateApplicablePaymentMethods', function (e, methods) {
//         base.methods.updateAvailablePaymentMethods(methods);
//     });
//   }
};
