'use strict';

var base = require('base/paymentInstruments/paymentInstruments');

var formValidation = require('base/components/formValidation');
var cleave = require('../components/cleave');
var HBCCards = require('../checkout/HBCCards');
var isTsysMode = require('../util/isTsys');
var url;

/**
 * validate expiration
 *
 * @param {dom} expiration - expiration dom
 * @returns {boolean} - validity for expiration check
 */
function expirationValidation(expiration) {
  var invalidExp = false;
  var cardExp = $(expiration).val();
  if (cardExp.indexOf('/') > -1) {
    var month = cardExp.split('/')[0];
    var currDate = new Date().getFullYear().toString().substr(0, 2);
    var year = currDate + cardExp.split('/')[1];
    // Validate if Year is valid or Not.
    var lastPossibleYear = parseInt($('#year option:last-child').val(), 10);
    var selectedYear = parseInt(year, 10);
    var selectedMonth = parseInt(month, 10) - 1;
    var getCurrentYear = new Date().getFullYear();
    var getCurrentMonth = new Date().getMonth();
    if (selectedYear === getCurrentYear) {
      if (selectedMonth < 12 && selectedMonth >= getCurrentMonth) {
        invalidExp = false;
      } else {
        invalidExp = true;
      }
    } else if (selectedYear > getCurrentYear && selectedYear <= lastPossibleYear && selectedMonth >= 0 && selectedMonth <= 11) {
      invalidExp = false;
    } else {
      invalidExp = true;
    }
  } else {
    invalidExp = true;
  }

  return invalidExp;
}

/**
 * toggle tcc-link
 *
 * @param {string} cardNumber - card number
 * @param {string} cardTypeName - card type name
 */
function showTCCLink(cardNumber, cardTypeName) {
  // On the 15th Card digit, display TCC Link. Because the TCC number
  // starts with 1, it will be considered as UATP number with 15 length.
  if (
    cardNumber.length >= 15 &&
    (cardTypeName === 'unknown' ||
      cardTypeName === 'SAKS' || // A 16 digit SAKS card could also be the beginning of a 29 digit TCC card.
      cardTypeName === 'SAKSMC') // A 16 digit SAKSMC card could also be the beginning of a 29 digit TCC card.
  ) {
    $('.tcc-link').removeClass('d-none');
  } else {
    $('.tcc-link').addClass('d-none');
  }
}
module.exports = {
  base: base,
  removePayment: function () {
    $('.js-container').on('click', '.remove-payment', function (e) {
      e.preventDefault();
      url = $(this).data('url') + '?UUID=' + $(this).data('id');
      $('.payment-to-remove').empty().append($(this).data('card'));

      $('body')
        .off('click')
        .on('click', '.delete-confirmation-btn', function (f) {
          f.preventDefault();
          $('.remove-payment').trigger('payment:remove', f);
          $.ajax({
            url: url,
            type: 'get',
            dataType: 'json',
            success: function (data) {
              $('body').find('.js-container').empty().html(data.paymentHtml);
            },
            error: function (err) {
              if (err.responseJSON.redirectUrl) {
                window.location.href = err.responseJSON.redirectUrl;
              }
              $.spinner().stop();
            }
          });
        });
    });
  },

  submitPayment: function () {
    $('form.payment-form').submit(function (e) {
      var $form = $(this);
      e.preventDefault();
      url = $form.attr('action');
      $('.js-tokenex-error p').empty();
      $form.spinner().start();
      $('form.payment-form').trigger('payment:submit', e);

      var tccCardNumber;
      if ($('#tccCardNumber').length > 0) {
        tccCardNumber = $('#tccCardNumber').data('cleave').getRawValue();
        if (tccCardNumber && tccCardNumber.length === 29) {
          // Copy this to Regular CC.
          $('#cardNumber').val(tccCardNumber);
        }
      }

      var formData = cleave.serializeData($form);

      $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: formData,
        success: function (data) {
          $form.spinner().stop();
          if (!data.success) {
            if (data.tokenExError) {
              $('.js-tokenex-error p').text(data.tokenExError);
            } else {
              formValidation($form, data);
            }
          } else {
            location.href = data.redirectUrl;
          }
        },
        error: function (err) {
          if (err.responseJSON.redirectUrl) {
            window.location.href = err.responseJSON.redirectUrl;
          }
          $form.spinner().stop();
        }
      });
      return false;
    });
  },

  handleTemporaryCreditCardNumber: function () {
    if ($('.tccCardNumber').length > 0) {
      cleave.handleTemporaryCreditCardNumber('.tccCardNumber', '#cardType');
    }
  },

  handleCreditCardNumber: function () {
    if ($('#cardNumber').length && $('#cardType').length) {
      cleave.handleCreditCardNumber('#cardNumber', '#cardType');
    }
  },
  validatePLCCcard: function () {
    $('#cardNumber').keyup(function () {
      var cardNumber = $('#cardNumber').data('cleave').getRawValue();
      var mainForm = $('.add-payment-form');
      if (cardNumber !== '') {
        var cardNumberInt = cardNumber; // eslint-disable-line
        var cardType = HBCCards.cardType(cardNumberInt);
        //check for Saks 8 digit cards
        var saksCard = HBCCards.saksCard(cardNumber);

        if (cardType.name || (saksCard && saksCard.name)) {
          var cardName;
          if (cardType.name) {
            cardName = cardType.name;
          } else {
            cardName = saksCard.name;
          }

          $('.card-number-wrapper').attr('data-type', cardName);
          $('.card-number-wrapper').attr('data-plcccard', 'true');
          $('#cardType').val(cardName);

          //Make the CVV  and Expiration fields as not mandatory fields if user enters a PLCC card
          if (cardType.name && (cardType.name === 'HBCMC' || cardType.name === 'SAKSMC')) {
            mainForm.find('.cardExpiryDate').removeClass('d-none');
            mainForm.find('.cardExpiryDate').addClass('required');
            mainForm.find('.cardExpiration').prop('required', true);
          } else if (cardType.name && cardType.name === 'SAKS' && cardType.length[0] === 16) {
            mainForm.find('.cardExpiryDate').removeClass('d-none');
            mainForm.find('.cardExpiryDate').addClass('required');
            mainForm.find('.cardExpiration').prop('required', true);
          } else {
            mainForm.find('.cardExpiryDate').addClass('d-none');
            mainForm.find('.cardExpiryDate').removeClass('required');
            mainForm.find('.cardExpiration').prop('required', false);
            mainForm.find('.cardExpiration').removeClass('is-invalid');
            mainForm.find('.cardExpiration').next('span').remove();
            mainForm.find('.cardExpiration').prev('label').removeClass('is-invalid').removeClass('input-focus');
          }
        } else {
          mainForm.find('.cardExpiryDate').removeClass('d-none');
          mainForm.find('.cardExpiryDate').addClass('required');
          mainForm.find('.cardExpiration').prop('required', true);

          cleave.handleCreditCardNumber('#cardNumber', '#cardType');
        }
        if (isTsysMode()) {
          showTCCLink(cardNumber, $('.card-number-wrapper').attr('data-type'));
        }
      } else {
        $('.card-number-wrapper').attr('data-type', 'unknown');
        $('.card-number-wrapper').attr('data-plcccard', 'false');
        $('#cardType').val('Unknown');
      }
    });
  },

  openTCCCardHolder: function () {
    $('.use-tcc-card').on('click', function () {
      var enteredCardNumber = $('#cardNumber').data('cleave').getRawValue();
      // Hide all regular card component
      $(this).closest('.tcc-link').addClass('d-none');
      var mainForm = $('.payment-form');

      mainForm.attr('data-tccopen', 'true');

      mainForm.find('#cardNumber').val('');
      mainForm.find('#cardNumber').prop('required', false);
      mainForm.find('#cardNumber').removeClass('is-invalid');
      mainForm.find('#cardNumber').next('span').remove();
      mainForm.find('#cardNumber').removeAttr('aria-required');
      mainForm.find('#cardNumber').prev('label').removeClass('is-invalid').removeClass('input-focus');
      mainForm.find('.regular-card-number').addClass('d-none');
      mainForm.find('.regular-card-number').removeClass('is-invalid').removeClass('required');

      mainForm.find('.cardExpiration').val('');
      mainForm.find('.cardExpiration').prop('required', false);
      mainForm.find('#expirationMonth').val('');
      mainForm.find('#expirationYear').val('');
      mainForm.find('.cardExpiration').removeClass('is-invalid');
      mainForm.find('.cardExpiration').next('span').remove();
      mainForm.find('.cardExpiration').removeAttr('placeholder');
      mainForm.find('.cardExpiration').prev('label').removeClass('is-invalid').removeClass('input-focus');
      mainForm.find('.cardExpiryDate').addClass('d-none');

      // Copy Current entered data and populate in TCC field
      mainForm.find('.tccCardNumber').val(enteredCardNumber);
      mainForm.find('.tccCardNumber').prop('required', true);

      cleave.handleTemporaryCreditCardNumber('.tccCardNumber', '#cardType');

      if (!isTsysMode()) {
        mainForm.find('.save-card-section').addClass('d-none');
        mainForm.find('.save-card-section').find('#saveCreditCard').prop('checked', false);
      }

      $('.tcc-card-number-wrapper').removeAttr('data-type');
      $('.tcc-card-number-wrapper').removeAttr('data-plcccard');

      if ($('.tcc-card-number').hasClass('d-none')) {
        $('.tcc-card-number').removeClass('d-none');
      }

      mainForm.find('.tccCardNumber').next('label').addClass('input-focus');
      $('.billing-tcc-cancel').removeClass('d-none');
      $('.billing-tcc-cancel').closest('.payment-form').find('.payment-next-step-button-row').addClass('plcc-card-visibility');
      if (!$('.card.payment-form .billing-cancel').hasClass('d-none')) {
        $('.card.payment-form .billing-cancel').addClass('d-none');
      }

      // makes form wider
      $('.add-payment-form').addClass('tcc-add-payment-form');
    });
  },

  cancelTCCCardHolder: function () {
    $('.billing-tcc-cancel').on('click', function () {
      // Hide all regular card component
      $(this).closest('.billing-tcc-cancel').addClass('d-none');
      $(this).closest('.payment-form').find('.payment-next-step-button-row').removeClass('plcc-card-visibility');
      var mainForm = $('.payment-form-fields .credit-card-form');

      mainForm.removeAttr('data-tccopen');

      if (!$('.tcc-card-number').hasClass('d-none')) {
        mainForm.find('.tcc-card-number').addClass('d-none');
      }

      mainForm.find('.cardExpiryDate').removeClass('d-none');
      mainForm.find('.newCreditCvvHolder').removeClass('d-none');
      mainForm.find('.cardExpiryDate').addClass('required');
      mainForm.find('.cardExpiryMonth').addClass('required');
      mainForm.find('.cardExpiryYear').addClass('required');
      mainForm.find('.newCreditCvvHolder').addClass('required');
      mainForm.find('.cardExpiration').prop('required', true);
      mainForm.find('.expirationMonth').prop('required', true);
      mainForm.find('.expirationYear').prop('required', true);
      mainForm.find('.securityCode').prop('required', true);

      // Remove all invalid classes
      mainForm.find('.cardNumber').removeClass('is-invalid').removeClass('focus-visible');
      mainForm.find('.cardNumber').next('label').removeClass('is-invalid').removeClass('input-focus');

      mainForm.find('.cardExpiration').removeClass('is-invalid').removeClass('focus-visible');
      mainForm.find('.cardExpiration').next('label').removeClass('is-invalid').removeClass('input-focus');

      mainForm.find('.securityCode').removeClass('is-invalid').removeClass('focus-visible');
      mainForm.find('.securityCode').next('label').removeClass('is-invalid').removeClass('input-focus');

      mainForm.find('#tccCardNumber').prop('required', true);
      mainForm.find('#tccCardNumber').removeClass('is-invalid').removeClass('focus-visible');
      mainForm.find('#tccCardNumber').next('label').removeClass('is-invalid').removeClass('input-focus');

      mainForm.find('.regular-card-number').removeClass('d-none');

      mainForm.find('.regular-expiry').removeClass('d-none');

      mainForm.find('.regular-cvv').removeClass('d-none');

      // Copy Current entered data and populate in TCC field
      mainForm.find('.tccCardNumber').val('');
      mainForm.find('#tccCardNumber').val('');
      mainForm.find('#tccCardNumber').removeClass('is-invalid');
      mainForm.find('#tccCardNumber').next('span').remove();
      mainForm.find('#tccCardNumber').next('label').removeClass('is-invalid').removeClass('input-focus');

      $('.card-number-wrapper').removeAttr('data-type');
      $('.card-number-wrapper').removeAttr('data-plcccard');

      mainForm.find('.save-card-section').removeClass('d-none');
      mainForm.find('.save-card-section').find('#saveCreditCard').prop('checked', true);

      cleave.handleCreditCardNumber('.cardNumber', '#cardType');

      if ($('.card.payment-form .payment-next-step-button-row').hasClass('card-open')) {
        $('.card.payment-form .billing-cancel').removeClass('d-none');
      } else {
        $('.card.payment-form .billing-cancel').addClass('d-none');
      }

      if (!$('.tcc-link').hasClass('d-none')) {
        $('.tcc-link').addClass('d-none');
      }
    });
  },

  handleCreditCardExpiration: function () {
    $('.cardExpiration').on('focus', function () {
      $(this).prop('placeholder', 'MM/YY');
    });
    // date mask
    if ($('.cardExpiration').length > 0) {
      $('.cardExpiration').mask('00/00');
    }
  },

  updateExpirationDate: function () {
    $('body').on('focusout', '#cardExpiration', function () {
      var creditCard = $(this).closest('form');
      if ($('#cardExpiration').val() !== null && $('#cardExpiration').val() !== '') {
        $('#month').val('');
        $('#year').val('');
        $(this).find('.form-group').find('.invalid-feedback').empty();
        var invalidExp = expirationValidation($(this));
        if (!invalidExp) {
          var cardExp = $(this).val();
          var month = cardExp.split('/')[0];
          var currDate = new Date().getFullYear().toString().substr(0, 2);
          var year = currDate + cardExp.split('/')[1];
          creditCard.find('#month').val(month);
          creditCard.find('#year').val(year);
          $(this).removeClass('is-invalid');
          $(this).prev('.form-control-label').removeClass('is-invalid');
          $(this).closest('div').find('.invalid-feedback').text('');
          if ($(this).next('span').length === 0) {
            $('<span></span>').insertAfter(this);
            $(this).next('span').addClass('valid');
          }
          if ($(this).next('span').length !== 0 && $(this).next('span').hasClass('invalid')) {
            $(this).next('span').removeClass('invalid').addClass('valid');
          }
        } else {
          $('#month').val('');
          $('#year').val('');
          var validationMessage = $(this).data('pattern-mismatch');
          $(this).parents('.form-group').find('.invalid-feedback').text(validationMessage);
          $(this).addClass('is-invalid');
          if ($(this).next('span').length === 0) {
            $('<span></span>').insertAfter(this);
            $(this).next('span').addClass('invalid');
          }
          if ($(this).next('span').hasClass('valid')) {
            $(this).next('span').removeClass('valid').addClass('invalid');
          }
        }
      } else {
        if (typeof $(this).attr('required') === 'undefined') {
          $(this).removeAttr('placeholder');
        }
      }
    });
  }
};
