'use strict';

/**
 * appends params to a url
 * @param {string} data - data returned from the server's ajax call
 * @param {Object} button - button that was clicked to add a product to the wishlist
 */
function displayMessage(data, button) {
  $.spinner().stop();
  var status;
  if (data.success) {
    status = 'alert-success';
  } else {
    status = 'alert-danger';
  }

  if ($('.add-to-wishlist-messages').length === 0) {
    $('body').append('<div class="add-to-wishlist-messages "></div>');
  }
  $('.add-to-wishlist-messages').append('<div class="add-to-wishlist-alert text-center ' + status + '">' + data.msg + '</div>');

  setTimeout(function () {
    $('.add-to-wishlist-messages').remove();
    button.removeAttr('disabled');
  }, 5000);
}

module.exports = {
  addToWishlist: function () {
    $('body').on('click', '.wishlistTile.pdpselect-wishlist', function (e) {
      e.preventDefault();
      var curref = $(this);
      var url = curref.attr('href');
      var pid = curref.closest('.product-detail').data('pid');
      if (!url || !pid) {
        return;
      }
      var optionId = curref.closest('.product-detail').find('.product-option').attr('data-option-id');
      var optionVal = curref.closest('.product-detail').find('.options-select option:selected').attr('data-value-id');
      var customerStatus = $('.customer-details').data('customer') && $('.customer-details').data('customer').firstName;
      optionId = optionId || null;
      optionVal = optionVal || null;

      $.spinner().start();
      $(this).attr('disabled', true);
      $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: {
          pid: pid,
          optionId: optionId,
          optionVal: optionVal
        },
        success: function (data) {
          displayMessage(data, curref);
          curref.attr('aria-label', 'Remove product from Favorite');
          if (data.product) {
            $('body').trigger('adobeTagManager:addToFav', data.product);
          }
          if (!customerStatus) {
            curref.removeClass('pdpselect-wishlist').addClass('pdpdeselect-wishlist');
            const wishlistPopup = curref.next('.primary-images-container .pdp-customer-content');
            wishlistPopup.removeClass('d-none');
            window.setTimeout(() => wishlistPopup.addClass('d-none'), 5000);
          } else {
            curref.removeClass('pdpselect-wishlist').addClass('pdpdeselect-wishlist');
          }
        },
        error: function (err) {
          displayMessage(err, curref);
        }
      });
    });
  },
  removeFromWishlist: function () {
    $('body').on('click', '.pdpdeselect-wishlist', function (e) {
      e.preventDefault();
      var url = $('.primary-images-container').find('.removefromwl').val();
      var customerStatus = $('.customer-details').data('customer') && $('.customer-details').data('customer').firstName;
      var curref = $(this);
      var pid = $(this).closest('.product-detail').data('pid');
      url = url + '?pid=' + pid;
      $.spinner().start();
      $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {},
        success: function (data) {
          if (!customerStatus) {
            curref.addClass('pdpselect-wishlist').removeClass('pdpdeselect-wishlist');
            curref.next('.primary-images-container .pdp-customer-content').addClass('d-none');
            curref.attr('aria-label', 'Add product to wishlist');
          } else {
            curref.addClass('pdpselect-wishlist').removeClass('pdpdeselect-wishlist');
            curref.attr('aria-label', 'Add product to wishlist');
          }
          if (data.product) {
            $('body').trigger('adobeTagManager:removeFromFav', data.product);
          }
          $.spinner().stop();
        },
        error: function () {
          $.spinner().stop();
        }
      });
    });
  },
  removeWishlistOnClose: function () {
    $(document).on('click', '.wishlist-container-close', function (e) {
      var $target = $(e.target);
      $target.closest('.customer-content').addClass('d-none');
      $target.closest('.product-tile .hover-content').addClass('d-none');
      $target.closest('.image-container').removeClass('wishlist_checked');
      $target.closest('.image-container').find('.wishlistTile').find('span').show();
    });
  },
  removeWishlistOnClickOutside: function () {
    $(document).on('click', function (e) {
      var $target = $(e.target);
      if ($target.closest('.customer-content').length && $('.customer-content').is(':visible')) {
        if ($target.closest('.wishlist_container').length) return true;
        return false;
      }
      if (!$target.closest('.pdpdeselect-wishlist').length && !$target.closest('.deselect-wishlist').length) {
        if ($('.customer-content').is(':visible')) {
          $('.customer-content').addClass('d-none');
          $('.image-container').removeClass('wishlist_checked');
          $('.image-container').find('.wishlistTile').find('span').show();
        }
      }
    });
  }
};
