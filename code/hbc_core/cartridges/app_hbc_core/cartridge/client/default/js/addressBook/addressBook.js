'use strict';
var base = require('base/addressBook/addressBook');
var validator = require('../components/validator');
var formValidation = require('base/components/formValidation');
var floatLabel = require('../floatLabel');
var clientSideValidation = require('../components/clientSideValidation');

var url;
var isDefault;

/**
 * Create an alert to display the error message
 * @param {Object} message - Error message to display
 */
function createErrorNotification(message) {
  var errorHtml =
    '<div class="alert alert-danger alert-dismissible valid-cart-error ' +
    'fade show" role="alert">' +
    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
    '<span aria-hidden="true">&times;</span>' +
    '</button>' +
    message +
    '</div>';

  $('.error-messaging').append(errorHtml);
}

base.removeAddress = function () {
  $('.js-container').on('click', '.remove-address', function (e) {
    e.preventDefault();
    isDefault = $(this).data('default');
    if (isDefault) {
      url = $(this).data('url') + '?addressId=' + encodeURIComponent($(this).data('id')) + '&isDefault=' + isDefault;
    } else {
      url = $(this).data('url') + '?addressId=' + encodeURIComponent($(this).data('id'));
    }
    if ($(this).data('text')) {
      $('.product-to-remove').empty().append($(this).data('text'));
    } else {
      $('.product-to-remove').empty().append($(this).data('id'));
    }
  });
};

base.removeAddressConfirmation = function () {
  $('body').on('click', '.delete-confirmation-btn', function (e) {
    e.preventDefault();
    $.spinner().start();
    $.ajax({
      url: url,
      type: 'get',
      data: $('#deleteAddressToken').serialize(),
      dataType: 'json',
      success: function (data) {
        $('body').find('.js-container').empty().html(data.addresstHtml);
        $.spinner().stop();
      },
      error: function (err) {
        if (err.responseJSON.redirectUrl) {
          window.location.href = err.responseJSON.redirectUrl;
        } else {
          createErrorNotification(err.responseJSON.errorMessage);
        }
        $.spinner().stop();
      }
    });
  });
};

base.updateStateOptions = function () {
  $('body').on('change', 'select[name $= "address_country"]', function () {
    var country = $(this).val();
    if (country) {
      // calling this function defined in EDQUtils.js which is globally included in header directly
      // eslint-disable-next-line no-undef
      window.EdqConfig.GLOBAL_INTUITIVE_ISO3_COUNTRY = countryAlpha3(country);
    }
    var form = $(this).closest('form');
    var postalField = $(form).find('.validateZipCode');
    postalField.val('');
    postalField.next('span').remove();
    postalField.removeClass('is-invalid');
    postalField.prev('label').removeClass('is-invalid').removeClass('input-focus');
    clientSideValidation.updatePoPatterWithCountry(form);
    var stateField = $('select.customer-address-state');
    var countryRegion = $('.countryRegion').data('countryregion');
    if (!countryRegion) {
      return;
    }
    // Update Zip Label
    postalField.prev('label').text(countryRegion['postal_label'][country]);
    postalField.data('pattern-mismatch', countryRegion['postal_req_error'][country]);

    // if Country is UK, display the text field for state.
    $('.js-state-code-input').val('');
    if (country === 'UK') {
      $('.state-drop-down').addClass('d-none');
      $('.state-drop-down').find('.form-group').removeClass('required');
      $('.state-drop-down').find('.remove-required-uk-address').prop('required', false);
      $('.state-drop-down').find('.remove-required-uk-address').removeClass('is-invalid');
      $('.state-input').removeClass('d-none');
      //validator.updatePostalCodePattern(form, country);
    } else {
      var regions = countryRegion[country].regions;
      var regionsLabel = countryRegion[country].regionLabel;
      // Generate the State Options
      var optionArr = [];
      // Add First Option with class d-none
      optionArr.push('<option value class="d-none"></option>');
      stateField.prev('label').text(regionsLabel);
      for (var stateCode in regions) {
        // eslint-disable-line
        optionArr.push('<option value="' + stateCode + '">' + regions[stateCode] + '</option>');
      }
      // Update the State Field
      stateField.html(optionArr.join(''));
      floatLabel.resetFloatLabel();
      stateField.removeClass('is-invalid');
      stateField.closest('.form-group').find('label').removeClass('input-focus is-invalid');
      stateField.next('span.invalid').remove();
      stateField.closest('.form-group').find('.invalid-feedback').empty();
      //validator.updatePostalCodePattern(form, country);
      $('.state-drop-down').removeClass('d-none');
      $('.state-input').addClass('d-none');
      $('.state-drop-down').find('.form-group').addClass('required');
      $('.state-drop-down').find('.remove-required-uk-address').prop('required', true);
    }
  });
};

base.submitAddress = function () {
  $('form.address-form').submit(function (e) {
    var $form = $(this);
    var stateField = $form.find('select[name $= "states_stateCode"]');
    var countryCode = $form.find('select[name $= "address_country"]').val();
    if (countryCode === 'UK') {
      var UKState = $form.find('.js-state-code-input').val();
      if (UKState !== undefined && UKState !== '') {
        var option = '<option id="' + UKState + '" value="' + UKState + '">' + UKState + '</option>';
        stateField.html(option);
      }
    }
    e.preventDefault();
    url = $form.attr('action');
    $form.spinner().start();
    $('form.address-form').trigger('address:submit', e);
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: $form.serialize(),
      success: function (data) {
        $form.spinner().stop();
        if (!data.success) {
          formValidation($form, data);
        } else {
          location.href = data.redirectUrl;
        }
      },
      error: function (err) {
        if (err.responseJSON.redirectUrl) {
          window.location.href = err.responseJSON.redirectUrl;
        }
        $form.spinner().stop();
      }
    });
    return false;
  });
};

base.updatePostalPatteronLoad = function () {
  $(window).on('load', function () {
    var form = $('.address-form');
    clientSideValidation.updatePoPatterWithCountry(form);
  });
};

module.exports = base;
