'use strict';

var base = require('base/product/detail');
var baseBase = require('./base');
var hbcTooltip = require('../tooltip');
var formFields = require('../formFields/formFields');
var clientSideValidation = require('../components/clientSideValidation');
var scrollAnimate = require('base/components/scrollAnimate');
var search = require('../search/search');

base.addToCart = baseBase.addToCart;

base.updateAttributesAndDetails = function () {
  $('body').on('product:statusUpdate', function (e, data) {
    var $productContainer = $('.product-detail[data-pid="' + data.id + '"]');

    $productContainer.find('.description-and-detail .product-attributes').empty().html(data.attributesHtml);

    if (data.shortDescription) {
      $productContainer.find('.description-and-detail .description').removeClass('hidden-xl-down');
      $productContainer.find('.description-and-detail .description .content').empty().html(data.shortDescription);
    } else {
      $productContainer.find('.description-and-detail .description').addClass('hidden-xl-down');
    }

    if (data.longDescription) {
      $productContainer.find('.description-and-detail .details').removeClass('hidden-xl-down');
      $productContainer.find('.description-and-detail .details .content').empty().html(data.longDescription);
    } else {
      $productContainer.find('.description-and-detail .details').addClass('hidden-xl-down');
    }
    if ($('.enableTruefit').length > 0 && $('.enableTruefit').val() === 'true') {
      tfcapi('calculate'); // eslint-disable-line
    }
  });
};

base.updateAddToCart = function () {
  $('body').on('product:updateAddToCart', function (e, response) {
    // update local add to cart (for sets)
    $('button.add-to-cart').data('readytoorder', response.product.readyToOrder && response.product.available);
    $('button.add-to-cart').data('readytoordertext', response.product.readyToOrderMsg);
    if (
      response.product.waitlistable &&
      response.product.availability.ats <= 0 &&
      response.product.productType !== 'master' &&
      (!response.product.readyToOrder || !response.product.available)
    ) {
      $('.js-add-to-cart').addClass('d-none');
      $('.js-product-availability-qty').addClass('d-none');
      $('.js-wait-list-form').removeClass('d-none');
      $('.js-wait-list-form').find('.waitlist-product-id').val(response.product.id);
      $('.wait-list-success').empty();
    } else {
      $('.js-add-to-cart').removeClass('d-none');
      $('.js-waitlist-wrapper').addClass('d-none');
      $('.js-wait-list-form').addClass('d-none');
      $('.js-product-availability-qty').removeClass('d-none');
      $('.wait-list-success').empty();
    }
    /* var enable = $('.product-availability').toArray().every(function (item) {
            return $(item).data('available') && $(item).data('ready-to-order');
        }); */
    if ($('.page').data('producttype') === 'set') {
      $('button.add-to-cart', response.$productContainer).text(response.product.availability.buttonName);
      $('button.add-to-cart', response.$productContainer).attr('disabled', !response.product.available && response.product.availability.isInPurchaselimit);
      $('button.add-to-cart-global', response.$productContainer).text(response.product.availability.buttonName);
      $('div [id^="collapsible-details-"] .product-detail-id', response.$productContainer).html(response.product.longDescriptionStyle);
    } else {
      // change button text only if pick up is not selected
      if (
        !(
          $('.shipping-option').length > 0 &&
          $('.shipping-option').find('input:checked').length > 0 &&
          $('.shipping-option').find('input:checked').val().length > 0 &&
          $('.shipping-option').find('input:checked').val() === 'instore'
        )
      ) {
        if (response.product.preOrder && response.product.preOrder.applicable && response.product.preOrder.applicable === true) {
          $('button.add-to-cart').text(response.product.preOrder.preorderButtonName);
          if (response.product.preOrder.shipDate) {
            $('div .preorder-ship-date').text(response.product.preOrder.shipDate);
          }
        } else {
          $('button.add-to-cart').text(response.product.availability.buttonName);
          $('div .preorder-ship-date').empty();
        }
      }
      $('button.add-to-cart').attr('disabled', !response.product.available && response.product.availability.isInPurchaselimit);
      if (response.product.preOrder && response.product.preOrder.applicable && response.product.preOrder.applicable === true) {
        $('button.add-to-cart-global').text(response.product.preOrder.preorderButtonName);
        if (response.product.preOrder.shipDate) {
          $('div .preorder-ship-date').text(response.product.preOrder.shipDate);
        }
      } else {
        $('button.add-to-cart-global').text(response.product.availability.buttonName);
        $('div .preorder-ship-date').empty();
      }
      $('div [id^="collapsible-details-"] .product-detail-id').html(response.product.longDescriptionStyle);
    }
  });
};

base.submitWaitList = function () {
  $('body').on('submit', 'form.waitlistForm', function (e) {
    var form = $(this);
    e.preventDefault();
    var url = form.attr('action');
    form.spinner().start();
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: form.serialize(),
      success: function (data) {
        if (data.success) {
          $('.wait-list-success')
            .empty()
            .html(
              '<div class="success-msg"><span class="svg-svg-12-check-thick-dims svg-svg-12-check-thick d-inline-block check-img"></span><span class="message">' +
                data.msg +
                '</span></div>'
            );
          $('.js-wait-list-form').addClass('d-none');
          $('.waitlistForm')[0].reset();
          $('.waitlistForm').find('.input-focus').removeClass('input-focus');
          $('body').trigger('adobe:waitListComplete');
        } else {
          $('.wait-list-success').empty().text(data.msg);
        }
        form.spinner().stop();
      },
      error: function (data) {
        $('.wait-list-success').empty().text(data.msg);
        form.spinner().stop();
      }
    });
    return false;
  });
};

base.displayWaitListOptMsg = function () {
  $('body').on('focus keyup', '.js-waitlist-mobile', function () {
    if ($(this).val() !== '') {
      $('.js-mobile-opt-msg').removeClass('d-none');
      $(this).attr('required', true);
      $(this).attr('pattern', $(this).attr('data-pattern'));
    } else {
      $('.js-mobile-opt-msg').addClass('d-none');
      $(this).removeAttr('required');
      $(this).removeAttr('pattern');
    }
  });
};

base.updateAttribute = function () {
  $('body').on('product:afterAttributeSelect', function (e, response) {
    if ($('.product-detail>.bundle-items').length) {
      response.container.data('pid', response.data.product.id);
      response.container.find('.product-id').text(response.data.product.id);
      response.container.find('.bf-product-id').text(response.data.product.id);
    } else if ($('.product-set-detail').eq(0)) {
      response.container.data('pid', response.data.product.id);
      response.container.find('.product-id').text(response.data.product.id);
      response.container.find('.bf-product-id').empty().text(response.data.product.id);
    } else {
      $('.product-id').text(response.data.product.id);
      $('.product-detail:not(".bundle-item")').data('pid', response.data.product.id);
      response.container.find('.bf-product-id').empty().text(response.data.product.id);
    }
    TurnToCmd('set', { sku: response.data.product.id }); //eslint-disable-line
  });
};

base.availability = baseBase.availability;

/**
 * PDP scene 7 image zoom window
 *
 * @param {params} params - Params for scene 7
 * @param {product} pid - Product id
 **/
var enableScene7Zoom = function (params, pid) {
  if ($('.enablePDPZoomer').length > 0) {
    var prodID = $('div.s7-viewer').attr('data-productID');
    var zoomViewer = new s7viewers.FlyoutViewer({
      // eslint-disable-line
      containerId: 's7viewer-' + prodID, // eslint-disable-line
      params: params // eslint-disable-line
    }).init();
  }
};

/**
 * PDP zoom window slick
 *
 * @param {params} $primaryImgZoomWindow - Primary zoom window
 **/
var zoomWindowSlider = function ($primaryImgZoomWindow) {
  if ($primaryImgZoomWindow.is('.slick-initialized')) {
    $primaryImgZoomWindow.slick('unslick');
  }

  $primaryImgZoomWindow.slick({
    slidesToShow: 100,
    dots: false,
    arrows: true,
    centerMode: false,
    infinite: true,
    focusOnSelect: true,
    speed: 300,
    variableWidth: false,
    swipe: true,
    swipeToSlide: true,
    vertical: true,
    draggable: true
  });
};

/**
 * PDP activate zoom window, slider on zoom window, call scene 7 zoom functionality
 *
 * @param {element} $element - Slick element
 * @param {product} pid - Product id
 * @param {slide} currentSlide - Current slide
 **/
var activateZoomSlick = function ($element, pid, currentSlide) {
  var $primaryImgZoomWindow = $element.find('.zoom-thumbnails');

  zoomWindowSlider($primaryImgZoomWindow);

  setTimeout(function () {
    $primaryImgZoomWindow.slick('slickGoTo', currentSlide);
  }, 500);

  $primaryImgZoomWindow.on('afterChange', function () {
    $(this).find('.thumb-nail').removeClass('active');
    $(this).find('.slick-current .thumb-nail').addClass('active');
    var serverUrl = $element.find('div.pdp-carousel').data('scene7hosturl');
    var asset;
    var image;
    var params = {};
    image = $(this).find('.slick-current .thumb-nail.active').find('img').attr('src').split('image/'); // eslint-disable-line
    asset = image[image.length - 1];
    params.asset = asset;
    params.serverurl = serverUrl;
    $('.s7-viewer').empty();
    enableScene7Zoom(params, pid);
  });
};

/**
 * PDP activate primary selected image zoom
 *
 * @param {this} $this - current element
 **/
var activatePrimaryImageZoom = function ($this) {
  var $element = $this.closest('.product-detail');
  var pid = $('div.s7-viewer').attr('data-productID');
  $('.s7-viewer').empty();
  $('.s7-modal.' + pid).modal('show');
  setTimeout(function () {
    var currentSlide = $this.parent().data('index') || 0;
    activateZoomSlick($element, pid, currentSlide);
  }, 500);
};

var imgActive = false;

/**
 * PDP primary image actions. Activate the primary image/zoom window
 *
 **/

var pdpImageActions = function () {
  var enterKeyPressed = function enterKeyPressed(e) {
    var code = e.charCode || e.keyCode;
    var ENTER_KEY = 13;
    return code === ENTER_KEY;
  };

  $('.product-detail').on('click keypress', '.primary-image img', function (e) {
    if (e.type === 'keypress') {
      if (enterKeyPressed(e)) {
        $(this).trigger('click');
      }
      return;
    }
    activatePrimaryImageZoom($(this));
  });

  $('.zoom-thumbnails').on('keypress', '.thumb-nail', function (e) {
    if (e.type === 'keypress') {
      if (enterKeyPressed(e)) {
        $(this).trigger('click');
      }
      return;
    }
  });

  $('.primary-thumbnails').on('click keypress', '.thumb-nail', function (e) {
    if (e.type === 'keypress') {
      if (enterKeyPressed(e)) {
        $(this).trigger('click');
      }
      return;
    }

    imgActive = true;
    if ($('.primary-images').find('.slick-current').offset()) {
      $('html, body').animate(
        {
          scrollTop: $('.primary-images').find('.slick-current').offset().top - 200
        },
        500
      );
    }
    var pid = $(this).closest('.product-detail').attr('data-pid');
    if ($(this).is('.video-player')) {
      baseBase.playVideoPlayer(pid);
    }
    setTimeout(function () {
      imgActive = false;
    }, 500);
    $('body').trigger('adobeTagManager:altImageView');
  });

  $('.zoom-thumbnails').on('click keypress', '.thumb-nail', function (e) {
    $('body').trigger('adobeTagManager:altImageView');
  });

  $(window).on('resize load', function () {
    $('.primary-images').off('afterChange');
    if ($(window).width() < 1024) {
      $('.primary-images').on('afterChange', function (event, slick, currentSlide, nextSlide) {
        $('body').trigger('adobeTagManager:altImageView');
      });
    }
  });
};

/**
 * PDP Size chart model actions.
 *
 **/
var sizeChartModel = function () {
  $('.size_guide').on('click', 'button', function (e) {
    e.preventDefault();
    $('.size-modal').modal('show');
  });
};

/**
 * code to enable native share functionality in devices which supports.
 */
var shareThis = function () {
  var $windowMedia = window.matchMedia('(max-width: 1025px)').matches;
  $('.social-share').on('click', function () {
    if ($windowMedia) {
      const url = window.location.href;
      const dataHead = document.title;
      const text = $(this).attr('data-social-share');
      if (navigator.share !== undefined) {
        navigator
          .share({
            title: dataHead,
            text: text,
            url: url
          })
          .then(() => {
            // See if the user copied the link from the share sheet.
            navigator.clipboard.readText().then(clipText => {
              if (clipText.includes(url)) {
                // Pare the copied text just down to the URL.
                navigator.clipboard.writeText(url);
              }
            });
          });
      }
    }
  });
};

/**
 * Prepare html modal for appointment
 * @param {Object} obj - object containing the data
 *
 **/
function getAppointmentModalHtmlElement(obj) {
  if ($('#appointmentModal').length !== 0) {
    $('#appointmentModal').remove();
  }
  // eslint-disable-next-line no-param-reassign
  obj.firstName = $('.customer-details').data('customer').firstName ? $('.customer-details').data('customer').firstName : '';
  // eslint-disable-next-line no-param-reassign
  obj.lastName = $('.customer-details').data('customer').lastName ? $('.customer-details').data('customer').lastName : '';
  // eslint-disable-next-line no-param-reassign
  obj.phone = $('.customer-details').data('customer').phone ? $('.customer-details').data('customer').phone : '';
  // eslint-disable-next-line no-param-reassign
  obj.email = $('.customer-details').data('customer').email ? $('.customer-details').data('customer').email : '';
  var htmlString =
    '<!-- Modal -->' +
    '<div class="modal fade time-trade-dialog" id="appointmentModal" role="dialog">' +
    '<div class="modal-dialog">' +
    '<!-- Modal content-->' +
    '<div class="modal-content row no-gutters align-items-center justify-content-center text-center"><button type="button" class="close svg-svg-22-cross-dims svg-svg-22-cross" data-dismiss="modal" aria-label="Close"></button>' +
    '<div class="modal-header-content">' +
    '    <div class="header-text">' +
    obj.resources.header +
    '</div>' +
    '</div><div class="header-info">' +
    obj.resources.headerInfo +
    '</div>' +
    '<div id="appointmentForm" class="modal-body">' +
    '<form class="appointment-form" action="' +
    obj.url +
    '" method="post" target="_blank">' +
    '<div class="form-group form-input floating-label required"><label class="form-control-label">' +
    obj.resources.firstName +
    '</label>' +
    '<input type="text" name="attendee_person_firstName" required max-length="50" class="form-control required" id="firstName" data-missing-error="' +
    obj.resources.requiredMsg +
    '" value="' +
    obj.firstName +
    '"/><div class="invalid-feedback"></div></div>' +
    '<div class="form-group form-input floating-label required"><label class="form-control-label">' +
    obj.resources.lastName +
    '</label>' +
    '<input type="text" name="attendee_person_lastName" required  max-length="50"data-missing-error="' +
    obj.resources.requiredMsg +
    '" class="form-control" id="lastName" value="' +
    obj.lastName +
    '"/><div class="invalid-feedback"></div></div>' +
    '<div class="form-group form-input floating-label required"><label class="form-control-label">' +
    obj.resources.email +
    '</label>' +
    '<input type="email" name="attendee_email" required max-length="50" aria-describedby="form-email-error" aria-required="true" data-missing-error="' +
    obj.resources.requiredMsg +
    '" class="form-control" pattern="^[\\w.%+-]+@[\\w.-]+\\.[\\w]{2,6}$" data-pattern-mismatch="' +
    obj.resources.emailInvalidMsg +
    '" id="email" value="' +
    obj.email +
    '"/><div class="invalid-feedback"></div></div>' +
    '<div class="form-group form-input floating-label required"><label class="form-control-label">' +
    obj.resources.phone +
    '</label>' +
    '<input type="text" name="attendee_phone_phoneNumber" data-missing-error="' +
    obj.resources.requiredMsg +
    '" required pattern="^\\(?([2-9][0-8][0-9])\\)?[\\-\\. ]?([2-9][0-9]{2})[\\-\\. ]?([0-9]{4})(\\s*x[0-9]+)?$" class="form-control" id="phoneNumber" value="' +
    obj.phone +
    '" /><div class="invalid-feedback"></div></div>' +
    '<input type="hidden" name="locationId" value="' +
    obj.locationID +
    '"/>' +
    '<input type="hidden" name="appointmentTypeId" value="' +
    obj.appointmentTypeID +
    '"/>' +
    '<div class="row appointment-form-btns"><div class="col-12 col-sm-6 appointment-form-btn-left"><button type="button" data-dismiss="modal" class="btn btn-secondary btn-block">' +
    '' +
    obj.resources.cancelLabel +
    '' +
    '</button></div><div class="col-12 col-sm-6 appointment-form-btn-right"><button type="submit" id="submitAppointment" class="btn btn-primary btn-block">' +
    obj.resources.continueLabel +
    '</button></div></div>' +
    '</form>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';
  $('body').append(htmlString);
  if (obj.email !== '') {
    formFields.adjustForAutofill();
  }
  $('form.appointment-form').submit(function () {
    setTimeout(function () {
      $('.appointment-form-btn-left button').trigger('click');
    }, 100);
  });
}

var initiateTimeTradeEvents = function () {
  $('button.schedule-an-appointment.enabled').on('click', function (e) {
    e.preventDefault();

    var obj = $(this).data('timetradejson'); // + 'app/hbc/workflows/HBC001/schedule/';
    getAppointmentModalHtmlElement(obj);
    setTimeout(function () {
      formFields.adjustForAutofill();
    }, 200);
    clientSideValidation.submit();
    clientSideValidation.invalid();
    clientSideValidation.buttonClick();
  });
};

/**
 * PDP set go to first product
 *
 **/
var pdpSetGoTo = function () {
  $('.shop-collection').on('click', function () {
    $('html, body').animate(
      {
        scrollTop: $($('.product-set-detail').find('.set-item')[0]).offset().top - 100
      },
      500
    );
  });
};

var waitlistAnchor = function () {
  $(document).ready(function () {
    var url = window.location.href;
    var query = url.split('#')[1];
    if (query !== '' && query == 'waitlistenabled') {
      scrollAnimate($('#waitlist'));
    }
  });
};

var buttonToggle = function () {
  if ($(window).width() < 544) {
    $(window).on('scroll', function () {
      if ($(window).scrollTop() >= $('.footer-top-utility-section, .footer-email-signup').offset().top - window.innerHeight) {
        $('.prices-add-to-cart-actions').hide();
      } else {
        $('.prices-add-to-cart-actions').show();
      }
    });
  }
};

var showChanelRecommendations = function () {
  if ($('div.product-detail.chanel-pdp') && !$("div[id*='cq_recomm_slot-']").length) {
    var recommProducts = $('div.product-detail.chanel-pdp').find('div.col-12.recommendations').find('.product.bfx-disable-product');
    if (recommProducts !== undefined && recommProducts.length > 0) {
      $('div.product-detail')
        .find('div.col-12.recommendations')
        .find('.product.bfx-disable-product')
        .each(function () {
          if (!$(this).hasClass('chanel')) {
            $(this).closest('div.slick-slide').addClass('d-none');
            $(this).closest('div.slick-slide').remove();
          }
        });
    }
    var recommProductsLength = $('div.product-detail.chanel-pdp').find('div.col-12.recommendations').find('.product.bfx-disable-product');
    if (recommProductsLength.length === 0) {
      $('div.product-detail.chanel-pdp').find('div.col-12.recommendations').addClass('d-none');
    }
  }
};

var exportDetail = $.extend({}, base, {
  pdpImageActions: pdpImageActions,
  activatePrimarySlick: baseBase.activatePrimarySlick,
  activateVideoPlayer: baseBase.activateVideoPlayer,
  tooltipInit: hbcTooltip.tooltipInit,
  sizeChartModel: sizeChartModel,
  shareThis: shareThis,
  pdpSetGoTo: pdpSetGoTo,
  waitlistAnchor: waitlistAnchor,
  initiateTimeTradeEvents: initiateTimeTradeEvents,
  plpSwatchesEvents: search.plpSwatchesEvents,
  buttonToggle: buttonToggle,
  showChanelRecommendations: showChanelRecommendations
});

module.exports = exportDetail;
