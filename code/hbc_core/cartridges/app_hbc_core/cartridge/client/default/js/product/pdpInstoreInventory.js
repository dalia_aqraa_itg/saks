'use strict';

var storeLocator = require('../storeLocator/storeLocator');
var base = require('instorepickup/product/pdpInstoreInventory');
var formFields = require('../formFields/formFields');

/**
 * Updates the product shipping option DOM elements post Ajax call
 * @param {UpdatedQuantity[]} data - response of an Ajax
 * @param {jQuery} $productContainer - DOM container for a given product
 */
var updateShippingOptions = function (data, $productContainer) {
  var $storeDataElement = $productContainer.find('.shipping-option');
  var product = data.product;
  if (data && data.isBopisEnabled && $storeDataElement.length > 0) {
    // show elements if bopis is enabled
    $storeDataElement.find('.product-pis').removeClass('change-store');
    $storeDataElement.show();
    // update store id in DOM
    if (data.storeInfo) {
      $storeDataElement.find('input.product-pis').attr('data-store-id', data.storeInfo.ID);
    }
    // update store change link
    if ($('a.change-store').length > 0) {
      if (data.storeInfo && $storeDataElement.attr('data-changestore-text')) {
        $('a.change-store').text($storeDataElement.attr('data-changestore-text'));
      } else if ($storeDataElement.attr('data-choosestore-text')) {
        $('a.change-store').text($storeDataElement.attr('data-choosestore-text'));
      }
    }
    // set add to bag button text
    var selectedValue = $storeDataElement.find('input[name=shipto]:checked').val();
    var addToStoreText = $storeDataElement.data('addtostore-text');
    var addToCartButton = $storeDataElement.closest('.product-detail').find('.add-to-cart');
    if (addToStoreText !== undefined && addToStoreText.length && addToStoreText !== '') {
      if (selectedValue === 'instore') {
        addToCartButton.html(addToStoreText);
      }
    }
    if (product.productType !== 'variant') {
      // product is a master
      $storeDataElement.find('input[value=instore]').attr('disabled', true);
      $storeDataElement.find('.store-change-link').hide();
    } else if (data.storeInfo && (data.storeInfo.unitsAtStores === 0 || data.storeInfo.unitsAtStores < product.selectedQuantity)) {
      // product is variant and units are unavailable
      if (data.availabilityMessage) {
        $storeDataElement.find('.store-info').text(data.availabilityMessage);
      }
      if (data.storeInfo.distanceInUnits && data.storeInfo.distanceInUnits !== '0') {
        $storeDataElement.find('.distance-info').text(data.storeInfo.distanceInUnits);
      }
      $storeDataElement.find('input[value=instore]').attr('disabled', true);
      $storeDataElement.find('.store-change-link').show();
      if ($storeDataElement.find('input[name=shipto]:checked').val() === 'instore') {
        $storeDataElement.find('input[value=shipto]').trigger('click');
      }
    } else {
      // product is variant and units are available
      $storeDataElement.find('.product-pis').removeClass('change-store');
      if (data.storeInfo && data.storeInfo.distanceInUnits && data.storeInfo.distanceInUnits !== '0') {
        $storeDataElement.find('.distance-info').text(data.storeInfo.distanceInUnits);
      }

      if (data.storeInfo && data.availabilityMessage) {
        $storeDataElement.find('.store-info').text(data.availabilityMessage);
      } else {
        $storeDataElement.find('.store-info').text(data.availabilityPromptText);
        $storeDataElement.find('.product-pis').addClass('change-store');
      }
      $storeDataElement.find('input[value=instore]').removeAttr('disabled');
      $storeDataElement.find('.store-change-link').show();
    }
  } else {
    // hide elements if bopis is disabled
    $storeDataElement.find('.product-pis').removeClass('change-store');
    $storeDataElement.hide();
  }
};
/**
 * Sets the data attribute of Quantity Selector to save its original state.
 * @param {HTMLElement} $quantitySelect - The Quantity Select Element
 */
function setOriginalQuantitySelect($quantitySelect) {
  if (!$quantitySelect.data('originalHTML')) {
    $quantitySelect.data('originalHTML', $quantitySelect.html());
  } // If it's already there, don't re-set it
}

/**
 * Postal code formatting for in store
 */
function bopisPostalCodeValidation() {
  $('#store-postal-code').on('keyup', function () {
    var postalCode = $(this).val();
    if (/[a-zA-Z][0-9][a-zA-Z]/.test(postalCode.substring(0, 3))) {
      if (postalCode.length === 4 && postalCode.indexOf(' ') === 3) {
        return;
      }
      if (postalCode.length === 5 && postalCode.lastIndexOf(' ') === 4) {
        $(this).val(postalCode.substring(0, postalCode.length - 1));
        return;
      }
      var formattedString = postalCode
        .replace(/ /g, '')
        .replace(/(\w{3})/, '$1 ')
        .replace(/(^\s+|\s+$)/, ' ')
        .toUpperCase()
        .trim();
      $(this).val(formattedString);
    }
  });
}

/**
 * Generates the modal window on the first call. Modal head and
 * main text is appended through parameter
 * @param {string} modalBodyText - main text in the modal
 * @param {string} modalCloseText - close title
 * @param {string} modalHeadText - modal header text
 */
function getModalHtmlElement(modalBodyText, modalCloseText, modalHeadText) {
  if ($('#inStoreInventoryModal').length !== 0) {
    $('#inStoreInventoryModal').modal('hide');
    $('#inStoreInventoryModal').remove();
  }
  var modalHead = modalHeadText !== undefined ? modalHeadText : $('.store-change-link').data('modal-header-text'); // fallback to get content from DOM, PDP
  var modalClose = modalCloseText !== undefined ? modalCloseText : $('.store-change-link').data('modal-header-text'); // fallback to get content from DOM, PDP
  var modalBody = modalBodyText !== undefined ? modalBodyText : $('.store-change-link').data('modal-body-text'); // fallback to get content from DOM, PDP
  // set empty to avoid display of null
  modalHead = modalHead === null ? '' : modalHead;
  modalClose = modalClose === null ? '' : modalClose;
  modalBody = modalBody === null ? '' : modalBody;
  var htmlString =
    '<!-- Modal -->' +
    '<div class="modal inStoreInventoryModal" id="inStoreInventoryModal" role="dialog" aria-modal="true">' +
    '<div class="modal-dialog in-store-inventory-dialog">' +
    '<!-- Modal content-->' +
    '<div class="modal-content">' +
    '<div class="modal-header">' +
    '<span>' +
    modalHead +
    '</span>' +
    '    <button type="button" class="close svg-svg-22-cross svg-svg-22-cross-dims" data-dismiss="modal" aria-label="Close inventory modal" title="' +
    modalClose +
    '">' + // eslint-disable-line
    '    </button>' +
    '</div>' +
    '<div class="change-a-store">' +
    modalBody +
    '</div>' +
    '<div class="modal-body"></div>' +
    '<div class="modal-footer"></div>' +
    '</div>' +
    '</div>' +
    '</div>';
  $('body').append(htmlString);
  $('#inStoreInventoryModal').modal('show');
  $('.change-store[aria-haspopup=true]').attr('aria-controls', 'inStoreInventoryModal');
  formFields.inputfocusEvent();
}

/**
 * Replaces the content in the modal window with find stores components and
 * the result store list.
 * @param {string} pid - The product ID to search for
 * @param {number} quantity - Number of products to search inventory for
 * @param {number} selectedPostalCode - The postal code to search for inventory
 * @param {number} selectedRadius - The radius to search for inventory
 * @param {string} pliUUID - unid id of an item
 * @param {string} selectedOption - selected option between shipto and instore
 * @param {string} instoreFieldLabel - default lable for instore
 * @param {string} source - source of invoke
 */
function fillModalElement(pid, quantity, selectedPostalCode, selectedRadius, pliUUID, selectedOption, instoreFieldLabel, source) {
  var requestData = {
    products: pid + ':' + quantity
  };

  if (selectedRadius) {
    requestData.radius = selectedRadius;
  }

  if (selectedPostalCode) {
    requestData.postalCode = selectedPostalCode;
  }

  $('#inStoreInventoryModal').spinner().start();
  $.ajax({
    url: $('.change-store').data('open-action'),
    data: requestData,
    method: 'GET',
    success: function (response) {
      $('.inStoreInventoryModal .modal-body').empty();
      $('.inStoreInventoryModal .modal-body').html(response.storesResultsHtml);
      storeLocator.search();
      storeLocator.changeRadius();
      storeLocator.selectStore();
      storeLocator.updateSelectStoreButton();
      formFields.updateSelect();
      formFields.adjustForAutofill();
      $('.btn-storelocator-search').attr('data-search-pid', pid);
      $('.btn-storelocator-search').attr('data-product-pliuuid', pliUUID);
      $('.btn-storelocator-search').attr('data-shipto-selected', selectedOption);
      $('.btn-storelocator-search').attr('data-instorefieldlabel', instoreFieldLabel);
      $('.btn-storelocator-search').attr('data-source', source);
      if (selectedRadius) {
        $('#radius').val(selectedRadius);
      }

      if (selectedPostalCode) {
        $('#store-postal-code').val(selectedPostalCode);
      }

      if (!$('.results').data('has-results')) {
        $('.store-locator-no-results').show();
      }
      // do not show error message with the modal open
      if (!$('.results').data('searchexecuted')) {
        $('.store-locator-no-results').hide();
      }
      $('#inStoreInventoryModal').modal('show');
      $('#inStoreInventoryModal').spinner().stop();
      $('#inStoreInventoryModal').find('#store-postal-code').focus();
      bopisPostalCodeValidation();
    },
    error: function () {
      $('#inStoreInventoryModal').spinner().stop();
    }
  });
}

/**
 * Update quantity options. Only display quantity options that are available for the store.
 * @param {sring} searchPID - The product ID of the selected product.
 * @param {number} storeId - The store ID selected for in store pickup.
 * @param {number} storeDistance - The store distance of the store selected for in store pickup.
 * @param {string} searchPlid - unique id of an item
 */
function updateQuantityOptions(searchPID, storeId, storeDistance, searchPlid) {
  var selectorPrefix = '.product-detail[data-pid="' + searchPID + '"]';
  var quantitySelector = selectorPrefix + ' .quantity-select';

  setOriginalQuantitySelect($(quantitySelector));

  var requestData = {
    pid: searchPID,
    quantitySelected: $(quantitySelector).val(),
    storeId: storeId,
    storeDistance: storeDistance,
    searchPlid: searchPlid,
    savetosession: true
  };

  $.ajax({
    url: $('.store-change-link').data('setstore'),
    data: requestData,
    method: 'GET',
    success: function (response) {
      var $productContainer = $('.product-detail[data-pid="' + searchPID + '"]');
      $('button.add-to-cart, button.add-to-cart-global, button.update-cart-product-global').trigger('product:updateAddToCart', {
        product: response.product,
        $productContainer: $productContainer
      });
      // update shipping option elements
      updateShippingOptions(response, $productContainer);
    }
  });
}

/**
 * CUSTOM EVENT
 * event handler triggered with the store selection on modal
 */
base.selectStoreWithInventory = function () {
  $('body').on('store:selected', function (e, data) {
    var searchPID = $('.btn-storelocator-search').attr('data-search-pid');
    var searchPlid = $('.btn-storelocator-search').attr('data-product-pliuuid');
    var storeElement =
      $('.product-detail[data-pid="' + searchPID + '"]') === undefined
        ? $('.cart-options[data-pid="' + searchPID + '"]')
        : $('.product-detail[data-pid="' + searchPID + '"]'); // fall back from cart, pick product id

    var $changeStoreButton = $(storeElement).find('.change-store');
    $($changeStoreButton).data('postal', data.searchPostalCode);
    $($changeStoreButton).data('radius', data.searchRadius);
    updateQuantityOptions(searchPID, data.storeID, data.storeDistance, searchPlid);
    $('#inStoreInventoryModal').attr('data-event-trigger', 'store');
    $('#inStoreInventoryModal').modal('hide');
    $('#inStoreInventoryModal').remove();
    $('#inStoreInventoryModal').attr('aria-modal', 'false');
  });
};

/**
 * CLICK EVENT
 * event handler triggered with the click of change store in PDP
 */
base.changeStore = function () {
  $('body').on('click', '.change-store', function () {
    var pid =
      $(this).closest('.product-detail').attr('data-pid') === undefined
        ? $(this).closest('.cart-options').data('product-id')
        : $(this).closest('.product-detail').attr('data-pid');
    var quantity =
      $(this).closest('.product-detail').find('.quantity-select').val() === undefined
        ? $(this).closest('.cart-options').data('product-qty')
        : $(this).closest('.product-detail').find('.quantity-select').val();
    var pliUUID = $(this).closest('.cart-options').data('product-uuid');
    var modalBodyText = $(this).closest('.cart-options').data('modal-body-text');
    var modalCloseText = $(this).closest('.cart-options').data('modal-close-text');
    var modalHeadText = $(this).closest('.cart-options').data('modal-header-text');
    var instoreFieldLabel = $(this).closest('.cart-options').data('instorefieldlabel');
    var source = $('.change-store').parent().siblings('.store-refine').length > 0 ? 'search' : null;
    var selectedOption = $(this).closest('.cart-options').find('input[name*="shipto"]:checked').val();
    getModalHtmlElement(modalBodyText, modalCloseText, modalHeadText);
    fillModalElement(pid, quantity, $(this).data('postal'), $(this).data('radius'), pliUUID, selectedOption, instoreFieldLabel, source);
  });
};

/**
 * CLICK EVENT
 * event handler triggered with the click of change store in PDP
 */
base.updateAddToCartFormData = function () {
  $('body').on('updateAddToCartFormData', function (e, form) {
    var storeID = null; // eslint-disable-line
    var instoreStoreID;
    if (form.pidsObj) {
      var pidsObj = JSON.parse(form.pidsObj);
      pidsObj.forEach(function (product) {
        var storeElement = $('.product-detail[data-pid="' + product.pid + '"]').find('.shipping-option');
        $(':checked', '.shipping-option').val();
        instoreStoreID =
          $(storeElement).length && $(storeElement).find('input[name*="shipto"]').length
            ? $('input[name*="shipto"]:checked', '.shipping-option').data('store-id')
            : null;
        product.storeId = instoreStoreID && instoreStoreID !== undefined && instoreStoreID !== '' ? instoreStoreID : null; // eslint-disable-line
        storeID = product.storeId;
      });

      form.pidsObj = JSON.stringify(pidsObj); // eslint-disable-line no-param-reassign
    }

    var storeElement = $('.product-detail[data-pid="' + form.pid + '"]').find('.shipping-option');

    instoreStoreID =
      $(storeElement).length && $(storeElement).find('input[name*="shipto"]').length ? $('input[name*="shipto"]:checked', storeElement).data('store-id') : null;
    instoreStoreID = instoreStoreID && instoreStoreID !== undefined && instoreStoreID !== '' ? instoreStoreID : null;

    if (instoreStoreID) {
      form.storeId = instoreStoreID; // eslint-disable-line
    }
  });
};
/**
 * Changes add to bag text dynamically on toggle of shipping options
 */
base.changeButtonText = function () {
  $('.shipping-option').on('click', 'input:not(.change-store)', function () {
    var selectedValue = $(this).val();
    var addToBagText = $('.shipping-option').data('addtobag-text');
    var addToStoreText = $('.shipping-option').data('addtostore-text');
    var addToCartButton = $(this).closest('.product-detail').find('.add-to-cart');
    if (addToStoreText !== undefined && addToStoreText.length && addToStoreText !== '') {
      if (selectedValue === 'instore') {
        addToCartButton.html(addToStoreText);
        $('body').trigger('adobe:bopusStart');
      } else {
        addToCartButton.html(addToBagText);
      }
    }
  });
};

/**
 * Assign aria-checked boolean values to radio group items based on user input
 */
base.selectRadioFromGroupList = function () {
  $('.radio-group-list[role=radiogroup]').on('click', '.radio-group-trigger', function () {
    var $this = $(this);
    $this.closest('.radio-group-list').find('[role=radio]').attr('aria-checked', 'false');
    $this.closest('[role=radio]').attr('aria-checked', 'true');
  });
};

/**
 * Trigger bopus start when radio is toggled to pick up i store
 */
base.triggerBopusEvent = function () {
  $('.shipping-option').on('click', 'input.change-store', function () {
    var selectedValue = $(this).val();
    if (selectedValue === 'instore') {
      $('body').trigger('adobe:bopusStart');
    }
  });
  $('.shipping-option').on('click', 'a.change-store', function () {
    $('body').trigger('adobe:bopusStart');
  });
};

/**
 * update the radio button to 'Ship to' if store selector modal is closed
 */
base.switchShiptoOnModalClosePDP = function () {
  $('body').on('hidden.bs.modal', '#inStoreInventoryModal', function () {
    var $container = $('.shipping-option');
    if (
      $container.length > 0 &&
      $container.find('input[name=shipto]:checked').length > 0 &&
      $container.find('input[name=shipto]:checked').val() === 'instore'
    ) {
      if ($container.find('input[value=instore]').length > 0) {
        // only if Pick up in store has no store selected
        if (
          ($container.find('input[value=instore]').attr('data-store-id') === undefined ||
            $container.find('input[value=instore]').attr('data-store-id') === '') &&
          !$('#inStoreInventoryModal').attr('data-event-trigger')
        ) {
          $container.find('input[value=shipto]').trigger('click');
        }
      }
    }
  });
};
/**
 * trigger click on enter key with store search
 */
base.triggerClickOnEnter = function () {
  $('body').on('keydown', '#store-postal-code', function (event) {
    var keyCode = event.keyCode ? event.keyCode : event.which;
    if (keyCode == 13) {
      $('.btn-storelocator-search').trigger('click');
    }
  });
};

module.exports = base;
