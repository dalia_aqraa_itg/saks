'use strict';

module.exports = function () {
  /**
   * Loads the js again in the page
   *
   * @param {string} src Script to be reloaded on the page
   */
  function loadJS(src) {
    $('script[src="' + src + '"]').remove();
    $('<script>').attr('src', src).appendTo('head');
    $('.edq-global-intuitive-address-suggestions ').remove();
  }

  $('.shippingAddressOne, .billingAddressOne').on('click', function () {
    window.EdqConfig.GLOBAL_INTUITIVE_ELEMENT = document.getElementById(this.id);
    var addressBlock = $(this).parents("fieldset[class*='address-block']");
    window.EdqConfig.GLOBAL_INTUITIVE_MAPPING = [
      {
        field: addressBlock.find("input[class*='AddressOne']")[0],
        elements: ['address.addressLine1']
      },
      {
        field: addressBlock.find("input[class*='AddressTwo']")[0],
        elements: ['address.addressLine2']
      },
      {
        field: addressBlock.find("input[class*='AddressCity']")[0],
        elements: ['address.locality']
      },
      {
        field: addressBlock.find("select[class*='State']")[0],
        elements: ['address.province']
      },
      {
        field: addressBlock.find("input[class*='ZipCode']")[0],
        elements: ['address.postalCode']
      }
    ];
    loadJS(window.edq.src);
  });

  $('#billing-addr-checkbox').on('change', function() {
    if (this.checked) {
      var countrySelector = $('select[name $= "billing_addressFields_country"]');
      var country = $(countrySelector).val();
      if (country) {
        // calling this function defined in EDQUtils.js which is globally included in header directly
        // eslint-disable-next-line no-undef
        window.EdqConfig.GLOBAL_INTUITIVE_ISO3_COUNTRY = countryAlpha3(country);
      }
    }
  });
};
