'use strict';

var base = require('wishlist/wishlist/wishlist');
var lazyLoad = require('../util/lazyLoadImages');
var isLoading = 0;
var isPreviousLoading = 0;
var currentPos = $(window).scrollTop();
var elWishlist = $('.wishlist-page').length;

// By default changing the heart icon Selected in wishlist page.
var defaultWishlistSelect = function () {
  if (elWishlist > 0) {
    $('.wishlistItemCards .product-tile').find('.image-container').addClass('wishlist_checked');
    $('.wishlistItemCards .wishlistTile').find('span').addClass('svg-svg-96-heart-blue svg-svg-96-heart-blue-dims');
    $('.wishlistItemCards .wishlistTile').removeClass('select-wishlist').addClass('deselect-wishlist');
  }
};

/**
 * renders the list up to a given page number
 * @param {number} pageNumber - current page number
 * @param {boolean} spinner - if the spinner has already started
 * @param {string} action - defines if the action in next or previous in the scroll persistence
 * @param {string} focusElementSelector - selector of the element to focus on
 */
function renderNewPageOfItems(pageNumber, spinner, action, focusElementSelector) {
  var publicView = $('.wishlistItemCardsData').data('public-view');
  var listUUID = $('.wishlistItemCardsData').data('uuid');
  var url = $('.wishlistItemCardsData').data('href');
  var isShared = $('.wishlistItemCardsData').data('isshared');
  if (spinner) {
    $.spinner().start();
  }
  var scrollPosition = document.documentElement.scrollTop;
  var newPageNumber = pageNumber;

  $.ajax({
    url: url,
    method: 'get',
    data: {
      pageNumber: ++newPageNumber,
      publicView: publicView,
      id: listUUID,
      actionTrigger: action,
      isShared: isShared
    }
  })
    .done(function (data) {
      $('.wishlist.my-account').replaceWith(data);
      lazyLoad.hydrateLazyLoadedImages();
      if (focusElementSelector) {
        $(focusElementSelector).focus();
      } else {
        document.documentElement.scrollTop = scrollPosition;
      }
      isLoading = 0;
      $('body').css('overflow', 'auto');
      defaultWishlistSelect();
      $.spinner().stop();
    })
    .fail(function () {
      $('.more-wl-items').remove();
      isLoading = 0;
      $.spinner().stop();
    });
}

/**
 * renders the list up to a given page number
 * @param {number} pageNumber - current page number
 * @param {boolean} spinner - if the spinner has already started
 * @param {string} action - defines if the action in next or previous in the scroll persistence
 * @param {string} focusElementSelector - selector of the element to focus on
 */
function renderNewPageOfItemsPrevious(pageNumber, spinner, action, focusElementSelector) {
  var publicView = $('.wishlistItemCardsData').data('public-view');
  var listUUID = $('.wishlistItemCardsData').data('uuid');
  var url = $('.wishlistItemCardsData').data('href');
  var isShared = $('.wishlistItemCardsData').data('isshared');
  if (spinner) {
    $.spinner().start();
  }
  var scrollPosition = document.documentElement.scrollTop;
  var newPageNumber = pageNumber;

  $.ajax({
    url: url,
    method: 'get',
    data: {
      pageNumber: ++newPageNumber,
      publicView: publicView,
      id: listUUID,
      actionTrigger: action,
      isShared: isShared
    }
  })
    .done(function (data) {
      $('.wl-show-previous').replaceWith(data);
      lazyLoad.hydrateLazyLoadedImages();
      if (focusElementSelector) {
        $(focusElementSelector).focus();
      } else {
        document.documentElement.scrollTop = scrollPosition;
      }
      isPreviousLoading = 0;
      $('body').css('overflow', 'auto');
      defaultWishlistSelect();
      $.spinner().stop();
    })
    .fail(function () {
      $('.prev-wl-items').remove();
      isPreviousLoading = 0;
      $.spinner().stop();
    });
}

base.moreWLItems = function () {
  $('body').on('click', '.more-wl-items', function () {
    var pageNumber = $('.wishlistItemCardsData.more-items').data('page-number');
    isLoading = 1;
    renderNewPageOfItems(pageNumber, true, 'next');
  });
};

$('body').on('click', '.prev-wl-items', function () {
  var pageNumber = $('.wishlistItemCardsData.less-items').data('page-number');
  isPreviousLoading = 1;
  renderNewPageOfItemsPrevious(pageNumber - 2, true, 'previous');
});

base.copyWishlistLink = function () {
  $('body').on('click', '.fa-link', function () {
    var $temp = $('<input>');
    $('body').append($temp);
    $temp.val($('#shareUrl').val()).select();
    document.execCommand('copy');
    $temp.remove();
    $('.copy-link-message').removeClass('d-none');
    setTimeout(function () {
      $('.copy-link-message').addClass('d-none');
    }, 3000);
  });

  $('body').on('click', '.wishlist-page .copy-button', function () {
    var $temp = $('<input>');
    $('body').append($temp);
    $temp.val($('#shareUrl').val()).select();
    document.execCommand('copy');
    $temp.remove();
  });

  if ($(window).width() < 1025) {
    $('body').on('click', '.wishlist-page .wl-social-sharing', function () {
      const url = $('#shareUrl').val();
      const dataHead = document.title;
      const text = $('#shareText').val();
      if (typeof navigator.share !== 'undefined') {
        navigator.share({
          title: dataHead,
          text: text,
          url: url
        });
      }
      $('body').trigger('adobeTagManager:wishlistShare');
      $('.copy-button').trigger('click');
    });
  }

  if ($(window).width() > 1025) {
    $('body').on('click', '.wishlist-page .wl-social-sharing', function () {
      $('.copy-link-message').removeClass('d-none');
      $('body').trigger('adobeTagManager:wishlistShare');
      $('.copy').val(function () {
        return $('#shareUrl').val();
      });
    });
  }

  $('body').on('click', '.wishlist-page .close-icon', function () {
    $('.copy-link-message').addClass('d-none');
  });

  $('body').on('click', '.wishlist-merge .close', function () {
    $('.wishlist-merge').addClass('d-none');
  });
};

base.addToCartFromWishlist = function () {
  $('body').on('click', '.add-to-cart:not(".wishlist-add-to-bag")', function () {
    var pid;
    var addToCartUrl;
    var pidsQty;

    $('body').trigger('product:beforeAddToCart', this);

    pid = $(this).data('pid');
    addToCartUrl = $(this).data('url');
    pidsQty = parseInt(
      $(this).closest('.product-info').find('.quantity').val() !== undefined ? $(this).closest('.product-info').find('.quantity').val() : 1,
      10
    );

    var form = {
      pid: pid,
      quantity: pidsQty
    };

    if ($(this).data('option')) {
      form.options = JSON.stringify($(this).data('option'));
    }

    $(this).trigger('updateAddToCartFormData', form);
    if (addToCartUrl) {
      $.ajax({
        url: addToCartUrl,
        method: 'POST',
        data: form,
        success: function (data) {
          if (data.message && data.message === 'LIMIT_EXCEEDED') {
            $('body').trigger('triggerBasketLimitMsgModal');
          } else {
            $('body').trigger('product:afterAddToCart', data);
          }
          $.spinner().stop();
        },
        error: function () {
          $.spinner().stop();
        }
      });
    }
  });
};

// Lazy loading
if (elWishlist > 0) {
  $(window).scroll(function () {
    var elementTop;
    var elementBottom;
    var viewportTop;
    var viewportBottom;
    if ($('.wishlist.my-account button').length) {
      elementTop = $('.wishlist.my-account').offset().top;
      elementBottom = elementTop + $('.wishlist.my-account').outerHeight();
      viewportTop = $(window).scrollTop();
      viewportBottom = viewportTop + $(window).height();
      if ((elementBottom > viewportTop && elementTop < viewportBottom) || window.innerHeight + $(window).scrollTop() === $(document).height()) {
        // eslint-disable-next-line no-undef
        if (isLoading === 0) {
          $('body').css('overflow', 'hidden');
          $('body .wishlist.my-account button').click();
        }
      }
    }
    if ($('.wl-show-previous button').length) {
      elementTop = $('.wl-show-previous').offset().top;
      elementBottom = elementTop + $('.wl-show-previous').outerHeight();
      viewportTop = $(window).scrollTop();
      viewportBottom = viewportTop + $(window).height();
      if ($(this).scrollTop() < currentPos && elementBottom > viewportTop && elementTop < viewportBottom) {
        if (isPreviousLoading === 0) {
          $('body').css('overflow', 'hidden');
          $('body .wl-show-previous button').click();
        }
      }
      currentPos = $(this).scrollTop();
    }
  });
}

// scroll persistence for wishlist
$('body').on('click', '.wishlist-prod-tile', function () {
  var pid = $(this).attr('data-id');
  var startPageSize = $(this).data('start-size');
  var state = {
    pid: pid
  };
  var locationParts = window.location.href.split('?');
  var newLocation = locationParts[0];
  var queryParams = locationParts[1] ? locationParts[1].split('&') : '';
  if (!queryParams && !!startPageSize) {
    newLocation = newLocation + '?pageNumber=' + startPageSize;
  } else if (!!queryParams && locationParts[1].indexOf('pageNumber=') < 0 && !!startPageSize) {
    newLocation = window.location.href + '&pageNumber=' + startPageSize;
  } else {
    for (var i = 0; i < queryParams.length; i++) {
      if (i !== 0) {
        newLocation += '&';
      } else {
        newLocation += '?';
      }
      if (queryParams[i].indexOf('pageNumber') > -1) {
        newLocation = newLocation + 'pageNumber=' + startPageSize;
      } else {
        newLocation += queryParams[i];
      }
    }
  }
  if (newLocation.split('?')[1] != null) {
    newLocation += '&scrollpersist=true';
  } else {
    newLocation += '?scrollpersist=true';
  }
  newLocation = newLocation.split('#')[0];
  newLocation += '#' + pid;
  history.replaceState(state, null, newLocation);
});

module.exports = base;
