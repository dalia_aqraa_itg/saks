'use strict';

var pdpInstoreInventory = require('../product/pdpInstoreInventory');

/**
 * Updates the store name in the radio label with the new store selected from modal.
 * @param {Object} data - Contains the store info
 * @param {sting} pliUUID - item uuid to fetch the precise container
 */
function updateStoreContent(data, pliUUID) {
  var $container = $('.cart-options[data-product-uuid="' + pliUUID + '"]');
  var shipToContainer = $container
    .find('input[value=instore]')
    .siblings('label[for=instore_' + pliUUID + ']')
    .find('a');
  shipToContainer.removeClass('no-inventory');
  // content update if store is found
  if (data.storeName && data.storeName !== undefined && data.storeId && data.storeId !== undefined && shipToContainer.length > 0) {
    $container.find('input[value=instore]').attr('data-store-id', data.storeId);
    $container.find('input[value=instore]').removeClass('change-store');
    shipToContainer.html(data.storeName);
  } else if (data.changeStoreHtml) {
    $container
      .find('input[value=instore]')
      .siblings('label[for=instore_' + pliUUID + ']')
      .html(data.changeStoreHtml);
  }
}

/**
 * remove the store selector modal when a store is selected out of results
 */
function removeSelectStoreModal() {
  if ($('#inStoreInventoryModal').length > 0) {
    $('#inStoreInventoryModal').modal('hide');
    $('#inStoreInventoryModal').remove();
    $('#inStoreInventoryModal').attr('aria-modal', 'false');
    $('.modal-backdrop').remove();
  }
}

/**
 * triggers change store option for a product matching the UUID parameter
 * @param {sting} pliUUID - item uuid to fetch the precise container
 */
function triggerChangeStore(pliUUID, noInventory) {
  var $container = $('.cart-options[data-product-uuid="' + pliUUID + '"]');
  var shipToContainer = $container
    .find('input[value=instore]')
    .siblings('label[for=instore_' + pliUUID + ']')
    .find('a');
  if (noInventory) {
    shipToContainer.addClass('no-inventory');
  } else {
    shipToContainer.removeClass('no-inventory');
  }
  if (shipToContainer.length) {
    shipToContainer.trigger('click');
  }
}

module.exports = {
  /** *initiate change store event***/
  changeStore: pdpInstoreInventory.changeStore,
  /** *initiate select store from modal event***/
  selectStoreWithInventory: pdpInstoreInventory.selectStoreWithInventory,
  /** *trigger click with enter on store search***/
  triggerClickOnEnter: pdpInstoreInventory.triggerClickOnEnter,
  /** *********CUSTOM EVENT**************
   * updates ship to home and ship to store option on toggle of radio
   * updates item with the store info
   * */
  toggleShippingOption: function () {
    $('body').on('store:cart', function (e, reqdata) {
      var pliUUID = reqdata.pliUUID;
      var storeId = reqdata.storeId;
      var prodid = reqdata.prodid;
      $.spinner().start();
      var changeShiptoUrl = reqdata.url;
      // form data
      var form = {
        storeId: storeId,
        uuid: pliUUID,
        prodid: prodid
      };
      if (changeShiptoUrl) {
        $.ajax({
          url: changeShiptoUrl,
          data: form,
          success: function (data) {
            var $cartLimitedMsgCont = $('.product-info.uuid-' + pliUUID).find('.limited-inventory.cond-3');
            if (reqdata.savefromModal) {
              if (reqdata.selected === 'instore' && !data.storeId) {
                removeSelectStoreModal();
                triggerChangeStore(pliUUID);
              } else {
                updateStoreContent(data, pliUUID); // if event invoke is from modal, updates store html with radio
                removeSelectStoreModal(); // close modal on successful update
              }
            } else if (!data.storeId && reqdata.selected === 'instore') {
              triggerChangeStore(pliUUID, true); // store id is returned on successful update. Trigger change store modal on failure
            } // update of store to item fails when inventory is insufficient or on exception
            if (data.limitedInventory && $cartLimitedMsgCont.length > 0) {
              $cartLimitedMsgCont.removeClass('d-none');
            } else {
              $cartLimitedMsgCont.addClass('d-none');
            }
            $.spinner().stop();
          },
          error: function () {
            $.spinner().stop();
          }
        });
      }
    });
  },
  /**
   * CUSTOM EVENT TRIGGER
   * on toggle of radio button in cart
   */
  selectStoreCart: function () {
    $('.cart-options').on('click', 'input:not(.change-store)', function (e) {
      // in input field has to change store event trigger required
      if (window.sessionStorage.getItem('change-store-click') === 'true') {
        e.preventDefault();
        window.sessionStorage.removeItem('change-store-click');
        return;
      }

      // prepare data
      var data = {
        url: $('.cart-options').data('toggle-url'),
        pliUUID: $(this).closest('.cart-options').data('product-uuid') !== undefined ? $(this).closest('.cart-options').data('product-uuid') : null,
        storeId: $(this).attr('data-store-id') !== undefined && $(this).attr('data-store-id') !== 'null' ? $(this).attr('data-store-id') : null,
        selected: $(this).val(),
        prodid: $(this).closest('.cart-options').data('pid') !== undefined ? $(this).closest('.cart-options').data('pid') : null,
        event: e
      };
      // Trigger bopus start when radio is toggled to pick up in store
      if ($(this).val() === 'instore') {
        var quantity =
          $(this).closest('.cart-options').data('product-qty') && $(this).closest('.cart-options').data('product-qty') != 'null'
            ? $(this).closest('.cart-options').data('product-qty')
            : '';
        $('body').trigger('adobe:bopusStart', { pid: data.prodid, page: 'cart', quantity: quantity });
      }
      /** CUSTOM EVENT TRIGGER**/
      $('body').trigger('store:cart', data);
    });
  },
  /**
   * Trigger bopus start when radio is toggled to pick up in store
   */
  triggerBopusEventCart: function () {
    $('.cart-options').on('click', 'input.change-store', function () {
      var selectedValue = $(this).val();
      if (selectedValue === 'instore') {
        var quantity =
          $(this).closest('.cart-options').data('product-qty') && $(this).closest('.cart-options').data('product-qty') != 'null'
            ? $(this).closest('.cart-options').data('product-qty')
            : '';
        var prodid = $(this).closest('.cart-options').data('pid') !== undefined ? $(this).closest('.cart-options').data('pid') : null;
        $('body').trigger('adobe:bopusStart', { pid: prodid, page: 'cart', quantity: quantity });
      }
    });
  },
  /**
   * updates the store info on select of store from the modal.
   * non-transactional and updates html only.
   * USE CASE : change store with 'ship to' option selected
   * on toggle of radio button in cart
   */
  updateStoreCart: function () {
    $('body').on('store:changeStore', function (e, data) {
      var pliUUID = data.pliUUID;
      $.spinner().start();
      updateStoreContent(data, pliUUID); // if event invoke is from modal, updates store html with radio
      removeSelectStoreModal(); // close modal on successful update
      $.spinner().stop();
    });
  },
  /**
   * update the radio button to 'Ship to' if store selector modal is closed
   */
  switchShiptoOnModalClose: function () {
    function maybeSwitchShipTo() {
      var $container, $changeStoreLink;
      if ($('.btn-storelocator-search').data('shipto-selected') === 'instore' && $('.btn-storelocator-search').data('product-pliuuid') !== undefined) {
        $container = $('.cart-options[data-product-uuid="' + $('.btn-storelocator-search').data('product-pliuuid') + '"]');
        if ($container.length > 0 && $container.find('input[value=instore]').length > 0) {
          $changeStoreLink = $container
            .find('input[value=instore]')
            .siblings('label[for=instore_' + $('.btn-storelocator-search').data('product-pliuuid') + ']')
            .find('a');
          // only if Pick up in store has no store selected
          if (
            ($changeStoreLink.length === 0 && $changeStoreLink.data('store-set') === undefined) ||
            ($changeStoreLink.length > 0 && $changeStoreLink.hasClass('no-inventory'))
          ) {
            $changeStoreLink.removeClass('no-inventory');
            window.sessionStorage.removeItem('change-store-click');
            $container.find('input[value=shipto]').trigger('click');
          }
        }
      }
    }

    $('body').on('hidden.bs.modal', '#inStoreInventoryModal', function () {
      $('.modal-backdrop').remove();
      maybeSwitchShipTo();
    });

    //handle back navigation
    $(window).on('beforeunload', function () {
      if ($('#inStoreInventoryModal').hasClass('show')) {
        maybeSwitchShipTo();
      }
    });
  }
};
