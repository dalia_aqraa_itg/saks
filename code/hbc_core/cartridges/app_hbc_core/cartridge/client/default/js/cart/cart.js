'use strict';

var base = require('../product/base');
var focusHelper = require('base/components/focus');
var login = require('../login/login');
var hbcSlider = require('../hbcSlider');
var formField = require('./../formFields/formFields');
var hbcTooltip = require('../tooltip');

/**
 * replace content of modal
 * @param {Object} data - discount updation
 */
function updateDiscountsHtml(data) {
  var productLineItems = data.items;
  productLineItems.forEach(function (productLineItem) {
    if (!productLineItem || !productLineItem.isBonusProductLineItem) {
      $('.price_discount-' + productLineItem.UUID).html(productLineItem.discountTotalHtml);
    }
  });
}

/**
 * appends params to a url
 * @param {string} url - Original url
 * @param {Object} params - Parameters to append
 * @returns {string} result url with appended parameters
 */
function appendToUrl(url, params) {
  var newUrl = url;
  newUrl +=
    (newUrl.indexOf('?') !== -1 ? '&' : '?') +
    Object.keys(params)
      .map(function (key) {
        return key + '=' + encodeURIComponent(params[key]);
      })
      .join('&');

  return newUrl;
}

/**
 * Checks whether the basket is valid. if invalid displays error message and disables
 * checkout button
 * @param {Object} data - AJAX response from the server
 */
function validateBasket(data) {
  if (data.valid.error) {
    if (data.valid.message) {
      var errorHtml =
        '<div class="alert alert-danger alert-dismissible valid-cart-error ' +
        'fade show" role="alert">' +
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
        '<span aria-hidden="true">&times;</span>' +
        '</button>' +
        data.valid.message +
        '</div>';
      $('.cart-error').html('');
      $('.cart-error').append(errorHtml);
    } else {
      $('.cart')
        .empty()
        .append('<div class="row"> ' + '<div class="col-12 text-center"> ' + '<h1>' + data.resources.emptyCartMsg + '</h1> ' + '</div> ' + '</div>');
      $('.number-of-items').empty().append(data.resources.numberOfItems);
      $('.minicart-quantity').empty().append(data.numItems);
      $('.minicart-link').attr({
        'aria-label': data.resources.minicartCountOfItems,
        title: data.resources.minicartCountOfItems
      });
      $('.minicart .popover').empty();
      $('.minicart .popover').removeClass('show');
    }

    $('.checkout-btn, .proxy-checkout-btn').addClass('disabled');
  } else {
    $('.checkout-btn, .proxy-checkout-btn').removeClass('disabled');
  }
}

/**
 * Update tax totals
 *
 * @param {Object} data - Tax totals data
 */
function updateTaxTotal(data) {
  if (data.totals.canadaTaxation && data.totals.canadaTaxation.PST) {
    $('.tax-PST-total').empty().append(data.totals.canadaTaxation.PST);
    $('.tax-pst').removeClass('d-none');
  } else if (!$('.tax-pst').hasClass('d-none')) {
    $('.tax-pst').addClass('d-none');
  }

  if (data.totals.canadaTaxation && data.totals.canadaTaxation['GST/HST']) {
    $('.tax-GST-total').empty().append(data.totals.canadaTaxation['GST/HST']);
    $('.tax-gst').removeClass('d-none');
  } else if (!$('.tax-gst').hasClass('d-none')) {
    $('.tax-gst').addClass('d-none');
  }

  if (data.totals.canadaTaxation && data.totals.canadaTaxation.QST) {
    $('.tax-QST-total').empty().append(data.totals.canadaTaxation.QST);
    $('.tax-qst').removeClass('d-none');
  } else if (!$('.tax-qst').hasClass('d-none')) {
    $('.tax-qst').addClass('d-none');
  }

  if (data.totals.canadaTaxation && data.totals.canadaTaxation.RST) {
    $('.tax-RST-total').empty().append(data.totals.canadaTaxation.RST);
    $('.tax-rst').removeClass('d-none');
  } else if (!$('.tax-rst').hasClass('d-none')) {
    $('.tax-rst').addClass('d-none');
  }

  if (data.totals.canadaTaxation && data.totals.canadaTaxation.ECO) {
    $('.tax-ECO-total').empty().append(data.totals.canadaTaxation.ECO);
    $('.tax-eco').removeClass('d-none');
  } else if (!$('.tax-eco').hasClass('d-none')) {
    $('.tax-eco').addClass('d-none');
  }

  if (!data.totals.canadaTaxation) {
    $('.tax-total').empty().append(data.totals.totalTax);
    $('.tax-normal').removeClass('d-none');
  } else if (!$('.tax-normal').hasClass('d-none')) {
    $('.tax-normal').addClass('d-none');
  }
}

/**
 * re-renders the order totals and the number of items in the cart
 * @param {Object} data - AJAX response from the server
 */
function updateCartTotals(data) {
  $('.number-of-items').empty().append(data.resources.numberOfItems);
  $('.shipping-total-cost').empty().append(data.totals.totalShippingCost);

  $('.shipping-method-price')
    .empty()
    .append('- ' + data.totals.totalShippingCost);

  updateTaxTotal(data);

  $('.grand-total-sum').empty().append(data.totals.grandTotal);
  $('.grand-total-value').empty().append(data.totals.grandTotalValue);
  $('.sub-total').empty().append(data.totals.subTotal);
  $('.mini-sub-total').empty().append(data.totals.miniCartEstimatedTotal);
  $('.minicart-quantity').empty().append(data.numItems);
  $('.minicart-link').attr({
    'aria-label': data.resources.minicartCountOfItems,
    title: data.resources.minicartCountOfItems
  });
  if (data.totals.orderLevelDiscountTotal.value > 0) {
    $('.order-discount').removeClass('hide-order-discount');
    $('.order-discount-total')
      .empty()
      .append('- ' + data.totals.orderLevelDiscountTotal.formatted);
  } else {
    $('.order-discount').addClass('hide-order-discount');
    $('.order-discount-total')
      .empty()
      .append('- ' + data.totals.orderLevelDiscountTotal.formatted);
  }

  if (data.totals.shippingLevelDiscountTotal.value > 0) {
    $('.shipping-total-cost').empty().append(data.totals.freeShippingText);

    $('.shipping-method-price')
      .empty()
      .append('- ' + data.totals.freeShippingText);
  }

  if (data.totals.totalSavings.value > 0) {
    $('.grand-total-saving-container').removeClass('d-none');
    $('.checkout-total-savings').empty().append(data.totals.totalSavings.formatted);
  } else {
    $('.grand-total-saving-container').addClass('d-none');
  }

  data.items.forEach(function (item) {
    if (item.checkoutDiscountTotalHtml) {
      $('.checkout_discount-' + item.UUID).html(item.checkoutDiscountTotalHtml);
    }
  });

  /** Toggle the Associate, First Day or SaksFirst member discount message. */
  if (data.totals.associateOrFDDMsg !== '' || data.totals.freeShippingMsg !== '') {
    $('.associate-fdd-promo').removeClass('d-none');
    $('.associate-promo-msg')
      .empty()
      .append(data.totals.associateOrFDDMsg ? data.totals.associateOrFDDMsg : data.totals.freeShippingMsg);
  } else {
    $('.associate-fdd-promo').addClass('d-none');
  }

  if (data.totals.hudsonpoint > 0) {
    $('.hudson-reward-points .hudson-point').html(data.totals.hudsonpoint);
  } else {
    $('.hudson-reward-points').addClass('d-none');
  }
}

/**
 * re-renders the order totals and the number of items in the cart
 * @param {Object} message - Error message to display
 */
function createErrorNotification(message) {
  var errorHtml =
    '<div class="alert alert-danger alert-dismissible valid-cart-error ' +
    'fade show" role="alert">' +
    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
    '<span aria-hidden="true">&times;</span>' +
    '</button>' +
    message +
    '</div>';
  $('.cart-error').html('');
  $('.cart-error').html(errorHtml);
}

/**
 * re-renders the order totals and the number of items in the cart
 * @param {Object} message - Error message to display
 */
function createErrorNotificationOnLineItem(message, lineItem) {
  var errorHtml = '<div class="alert alert-dismissible valid-cart-error line-item-error' + 'fade show" role="alert">' + message + '</div>';
  $(lineItem).find('.qty-error-message').html('');
  $(lineItem).find('.qty-error-message').html(errorHtml);

  setTimeout(function () {
    $(lineItem).find('.qty-error-message').html('');
  }, 3000);
}

/**
 * re-renders the order totals and the number of items in the cart
 * @param {Object} message - Error message to display
 */
function createPurchaseErrorNotificationAtLineItem(message, element) {
  element.closest('.product-info').find('.cannot-shipping-message').remove();
  var errorHtml = '<div class="col12 value content cannot-shipping-message">' + message + '</div>';
  element.closest('.product-info').prepend(errorHtml);
}

/**
 * re-renders the order totals and the number of items in the cart
 * @param {Object} message - Error message to display
 */
function createErrorNotificationQuickView(message) {
  var errorHtml =
    '<div class="alert alert-danger alert-dismissible valid-cart-error ' +
    'fade show" role="alert">' +
    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
    '<span aria-hidden="true">&times;</span>' +
    '</button>' +
    message +
    '</div>';

  $('div.error-msg').append(errorHtml);
}

/**
 * re-renders the approaching discount messages
 * @param {Object} approachingDiscounts - updated approaching discounts for the cart
 */
function updateApproachingDiscounts(approachingDiscounts) {
  var html = '';
  $('.approaching-discounts').empty();
  if (approachingDiscounts.length > 0) {
    approachingDiscounts.forEach(function (item) {
      html += '<div class="single-approaching-discount text-center">' + item.discountMsg + '</div>';
    });
  }
  $('.approaching-discounts').append(html);
}

/**
 * Updates the promo code count in the header of the Coupon Section
 * @param {Object} data - updates the promo code count in the header of the Coupon Section
 */
function updateAppliedCouponCount(data) {
  var html = '';
  $('.promo-heading.promo-label').empty();

  $('.promo-code-form .form-control').removeClass('is-invalid');
  $('.coupon-error-message').empty();

  // eslint-disable-next-line radix
  if (parseInt(data.totalAppliedCoupons) === 0) {
    html = data.noCouponMsg;
    // eslint-disable-next-line radix
  } else if (parseInt(data.totalAppliedCoupons) === 1) {
    html = data.totalAppliedCoupons + ' ' + data.singleCouponMsg;
  } else {
    html = data.totalAppliedCoupons + ' ' + data.multipleCouponMsg;
  }
  $('.promo-heading.promo-label').append(html);
}

/**
 * Updates the Total Savings value on Cart update.
 * @param {Object} data - Updates the Total Savings value on Cart update.
 */
function updateTotalSavings(data) {
  if (data.totals.totalSavings.value > 0) {
    $('.estm-saved').removeClass('d-none');
    $('.total-savings').empty().append(data.totals.totalSavings.formatted);
  } else {
    $('.estm-saved').addClass('d-none');
  }
}

/**
 * Updates the availability of a product line item
 * @param {Object} data - AJAX response from the server
 * @param {string} uuid - The uuid of the product line item to update
 */
function updateAvailability(data, uuid) {
  var lineItem;
  var messages = '';

  for (var i = 0; i < data.items.length; i++) {
    if (data.items[i].UUID === uuid) {
      lineItem = data.items[i];
      break;
    }
  }

  $('.availability-' + lineItem.UUID).empty();

  if (lineItem.availability) {
    if (lineItem.availability.messages) {
      lineItem.availability.messages.forEach(function (message) {
        messages += '<p class="line-item-attributes">' + message + '</p>';
      });
    }

    if (lineItem.availability.inStockDate) {
      messages += '<p class="line-item-attributes line-item-instock-date">' + lineItem.availability.inStockDate + '</p>';
    }
    $('.uuid-' + uuid)
      .find('.cannot-shipping-message')
      .empty();
  }

  $('.availability-' + lineItem.UUID).html(messages);
}

/**
 * Finds an element in the array that matches search parameter
 * @param {array} array - array of items to search
 * @param {function} match - function that takes an element and returns a boolean indicating if the match is made
 * @returns {Object|null} - returns an element of the array that matched the query.
 */
function findItem(array, match) {
  for (var i = 0, l = array.length; i < l; i++) {
    if (match.call(this, array[i])) {
      return array[i];
    }
  }
  return null;
}

/**
 * Updates details of a product line item
 * @param {Object} data - AJAX response from the server
 * @param {string} uuid - The uuid of the product line item to update
 */
function updateProductDetails(data, uuid) {
  updateItemsHTML(data.cartModel);
}

/**
 * Updates details of a product line item in the Cart Section
 * @param {Object} data - AJAX response from the server
 */
function updateItemsHTML(data) {
  if ($('.cart-page-content').length && $('.prdt-cart-details').length === 0 && data && data.items.length > 0) {
    window.location.reload();
  } else {
    $('.container.cart-page-content .cart-plis.product-line-item').empty().html($(data.itemsHTML).children().html());
    $('.container.cart-page-content .cart-wl-content').empty();
    $('.container.cart-page-content').find('.cart-wl-content').html(data.wlItemsHTML);
    formField.updateSelect();
    hbcTooltip.tooltipInit();
  }
}

/**
 * Updates details of a product line items in the Mini-Cart Section
 * @param {Object} data - AJAX response from the server
 */
function updateMiniCartItemsHTML(data) {
  $('.mini-cart-container').empty();
  $('.mini-cart-container').html(data.minicart);
  formField.updateSelect();
}

/**
 * Get Product options
 * @param {Object} $productContainer product container
 * @return {string} returns json value
 */
function getOptions($productContainer) {
  var options = $productContainer
    .find('.product-option')
    .map(function () {
      var $elOption = $(this).find('.options-select');
      var urlValue = $elOption.val();
      var selectedValueId = $elOption.find('option[value="' + urlValue + '"]').data('value-id');
      return {
        optionId: $(this).data('option-id'),
        selectedValueId: selectedValueId
      };
    })
    .toArray();

  return JSON.stringify(options);
}

/**
 * Get all bundled products
 * @return {string} returns json value
 */
function getChildProducts() {
  var childProducts = [];
  $('.bundle-item').each(function () {
    childProducts.push({
      pid: $(this).find('.product-id').text(),
      quantity: parseInt($(this).find('label.quantity').data('quantity'), 10)
    });
  });

  return childProducts.length ? JSON.stringify(childProducts) : [];
}

/**
 * Generates the modal window on the first call.
 *
 */
function getOptionalBonusModalHtmlElement() {
  if ($('#editProductModal').length !== 0) {
    $('#editProductModal').remove();
  }
  var htmlString =
    '<!-- Modal -->' +
    '<div class="modal fade gwp" id="editProductModal" tabindex="-1" role="dialog">' +
    '<span class="enter-message sr-only" ></span>' +
    '<div class="modal-dialog gift-offers quick-view-dialog ">' +
    '<!-- Modal content-->' +
    '<div class="modal-content">' +
    '<div class="modal-header">' +
    '    <button type="button" class="close pull-right" data-dismiss="modal">' +
    '        <span aria-hidden="true" class="cancel-icon svg-svg-22-cross svg-svg-22-cross-dims"></span>' +
    '    </button>' +
    '    <span aria-hidden="true" class="choice-bonus-header"></span>' +
    '    <span class="sr-only"> </span>' +
    '</div>' +
    '<div class="modal-body"></div>' +
    '<div class="modal-footer"></div>' +
    '</div>' +
    '</div>' +
    '</div>';
  $('body').append(htmlString);
}

/**
 * Generates the modal window on the first call.
 *
 */
function getModalHtmlElement() {
  if ($('#editProductModal').length !== 0) {
    $('#editProductModal').remove();
  }
  var htmlString =
    '<!-- Modal -->' +
    '<div class="modal fade" id="editProductModal" tabindex="-1" role="dialog">' +
    '<span class="enter-message sr-only" ></span>' +
    '<div class="modal-dialog quick-view-dialog">' +
    '<!-- Modal content-->' +
    '<div class="modal-content">' +
    '<div class="modal-header">' +
    '    <button type="button" class="close pull-right" data-dismiss="modal">' +
    '        <span aria-hidden="true" class="cancel-icon svg-svg-22-cross svg-svg-22-cross-dims"></span>' +
    '    </button>' +
    '    <span aria-hidden="true" class="pdt-name-edit"></span>' +
    '    <span class="sr-only"> </span>' +
    '</div>' +
    '<div class="modal-body"></div>' +
    '<div class="modal-footer"></div>' +
    '</div>' +
    '</div>' +
    '</div>';
  $('body').append(htmlString);
}

/**
 * Parses the html for a modal window
 * @param {string} html - representing the body and footer of the modal window
 *
 * @return {Object} - Object with properties body and footer.
 */
function parseHtml(html) {
  var $html = $('<div>').append($.parseHTML(html));

  var body = $html.find('.product-quickview');
  var footer = $html.find('.modal-footer').children();

  return {
    body: body,
    footer: footer
  };
}

function displayWaitListOptMsg() {
  $('body').on('focus keyup', '.js-waitlist-mobile', function () {
    if ($(this).val() !== '') {
      $('.js-mobile-opt-msg').removeClass('d-none');
      $(this).attr('required', true);
      $(this).attr('pattern', $(this).attr('data-pattern'));
    } else {
      $('.js-mobile-opt-msg').addClass('d-none');
      $(this).removeAttr('required');
      $(this).removeAttr('pattern');
    }
  });
}

function submitWaitList() {
  $('body').on('submit', 'form.waitlistForm', function (e) {
    var form = $(this);
    e.preventDefault();
    var url = form.attr('action');
    form.spinner().start();
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: form.serialize(),
      success: function (data) {
        if (data.success) {
          $('.wait-list-success')
            .empty()
            .html(
              '<div class="success-msg"><span class="svg-svg-12-check-thick-dims svg-svg-12-check-thick d-inline-block check-img"></span><span class="message">' +
                data.msg +
                '</span></div>'
            );
          $('.js-wait-list-form').addClass('d-none');
          $('.waitlistForm')[0].reset();
          $('.waitlistForm').find('.input-focus').removeClass('input-focus');
          $('body').trigger('adobe:waitListComplete');
        } else {
          $('.wait-list-success').empty().text(data.msg);
        }
        form.spinner().stop();
      },
      error: function (data) {
        $('.wait-list-success').empty().text(data.msg);
        form.spinner().stop();
      }
    });
    return false;
  });
}
/**
 * replaces the content in the modal window for product variation to be edited.
 * @param {string} editProductUrl - url to be used to retrieve a new product model
 */
function fillModalElement(editProductUrl) {
  $('#editProductModal').modal('show');
  $('.quick-view-dialog .modal-content').spinner().start();
  $.ajax({
    url: editProductUrl,
    method: 'GET',
    dataType: 'json',
    success: function (data) {
      var parsedHtml = parseHtml(data.renderedTemplate);

      $('#editProductModal .modal-body').empty();
      $('#editProductModal .modal-body').html(parsedHtml.body);
      $('#editProductModal .modal-footer').html(parsedHtml.footer);
      $('#editProductModal .modal-header .close .sr-only').text(data.closeButtonText);
      $('#editProductModal .enter-message').text(data.enterDialogMessage);
      $('#editProductModal .pdt-name-edit').empty();
      if (data.product.brand.name) {
        $('#editProductModal .pdt-name-edit').text(data.product.brand.name);
      }
      // Remove sizechart on cart edit
      $('#editProductModal').find('.size_guide').empty();
      hbcSlider.hbcSliderInit('edit-product');
      formField.updateSelect();
      $('#editProductModal .modal-body').find('.js-mobile-opt-msg').empty().html($('.js-waitlist-opt-message').html());
      displayWaitListOptMsg();
      submitWaitList();
      hbcTooltip.tooltipInit();
      $.spinner().stop();
    },
    error: function () {
      $.spinner().stop();
    }
  });
}

/**
 * replaces the content in the modal window for product variation to be edited.
 * @param {string} data - render data html string
 */
function fillChoiceOfBonusModalElement(data) {
  var parsedHtml = data.renderedTemplate;
  $('#editProductModal .modal-body').empty();
  $('#editProductModal .modal-body').html(parsedHtml);
  $('#editProductModal .choice-bonus-header').html(data.bonusModel.title);
  $('#editProductModal').modal('show');
}

/**
 * replace content of modal
 * @param {string} actionUrl - url to be used to remove product
 * @param {string} productID - pid
 * @param {string} productName - product name
 * @param {string} uuid - uuid
 */
function confirmDelete(actionUrl, productID, productName, uuid) {
  var $deleteConfirmBtn = $('.cart-delete-confirmation-btn');
  var $productToRemoveSpan = $('.product-to-remove');

  $deleteConfirmBtn.data('pid', productID);
  $deleteConfirmBtn.data('action', actionUrl);
  $deleteConfirmBtn.data('uuid', uuid);

  $productToRemoveSpan.empty().append(productName);
}

/**
 * Changes done for wishlist
 * @param {Object} attrs product varition attributes
 * @param {Object} $productContainer product container
 */
function updateAttrs(attrs, $productContainer) {
  attrs.forEach(function (attr) {
    var $attr = '[data-attr="' + attr.id + '"]';
    var $defaultOption = $productContainer.find($attr + ' .select-' + attr.id + ' option:first');
    $defaultOption.attr('value', attr.resetUrl);
    attr.values.forEach(function (attrValue) {
      var $attrValue = $productContainer.find($attr + ' [data-attr-value="' + attrValue.value + '"]');
      $attrValue.attr('value', attrValue.url).removeAttr('disabled');

      if (!attrValue.selectable) {
        $attrValue.attr('disabled', true);
      }
    });
  });
}

/**
 * Changes done for wishlist
 * @param {Object} selectedValueUrl product selected variant
 * @param {Object} $productContainer product container
 */
function attributeSelect(selectedValueUrl, $productContainer) {
  if ($productContainer.length && selectedValueUrl) {
    $productContainer.spinner().start();
    $.ajax({
      url: selectedValueUrl,
      method: 'GET',
      success: function (data) {
        updateAttrs(data.product.variationAttributes, $productContainer);
        $productContainer.closest('.product-detail-wlcart').data('pid', data.product.id);
        $productContainer.closest('.product-detail-wlcart').data('producttype', data.product.productType);
        $productContainer
          .closest('.product-detail-wlcart')
          .find('.prod_wl_bttn')
          .data('readytoorder', data.product.readyToOrder && data.product.available);

        var $priceSelector = $('.prod-price', $productContainer);
        $priceSelector.empty().html(data.product.price.html);
        if (data.product.price.sales !== undefined && data.product.price.sales != null) {
          $('.product-price-selected', $productContainer).empty().text(data.product.price.sales.formatted);
        }

        // Update promotions
        if (data.product.promotionalPricing && data.product.promotionalPricing.isPromotionalPrice && data.product.promotionalPricing.promoMessage !== '') {
          $('.promotion-pricing', $productContainer).empty().html(data.product.promotionalPricing.priceHtml);
          $('.promotion-pricing', $productContainer).removeClass('d-none');
          $('.promotions', $productContainer).addClass('d-none');
        } else {
          $('.promotion-pricing', $productContainer).addClass('d-none');
          $('.promotions', $productContainer).removeClass('d-none');
          $('.promotions', $productContainer).empty().html(data.product.promotionsHtml);
        }

        // Update limited inventory message
        if (data.product.availability.isAboveThresholdLevel) {
          $('.limited-inventory', $productContainer).empty().text(data.resources.limitedInventory);
        } else {
          $('.limited-inventory', $productContainer).empty();
        }

        if (data.product.productType === 'master') {
          $productContainer.find('.alert-msg').show();
        } else if ((!data.product.available || !data.product.readyToOrder) && data.product.productType !== 'master') {
          $productContainer.find('button.prod_wl_bttn').addClass('soldout');
          $productContainer.find('button.prod_wl_bttn').removeClass('add-to-cart');
          $productContainer.find('button.prod_wl_bttn').text(data.resources.soldout);
          $productContainer.find('button.prod_wl_bttn').attr('disabled', true);
        } else {
          $productContainer.find('button.prod_wl_bttn').addClass('add-to-cart');
          $productContainer.find('button.prod_wl_bttn').removeClass('soldout');
          $productContainer.find('button.prod_wl_bttn').text(data.resources.movetobag);
          $productContainer.find('button.prod_wl_bttn').removeAttr('disabled');
        }
        $productContainer.spinner().stop();
        var isSelectReady = $productContainer.find('button.add-to-cart').data('readytoorder');
        if (isSelectReady) {
          $productContainer.find('.alert-msg').addClass('d-none');
          $productContainer.find('.selected-option').removeClass('error');
        }
      },
      error: function () {
        $productContainer.spinner().stop();
      }
    });
  }
}

/**
 * Changes done for wishlist
 * @param {Object} $elementAppendTo element
 * @param {string} msg message to display
 */
function displayErrorMessage($elementAppendTo, msg) {
  if ($('.remove-from-wishlist-messages').length === 0) {
    $elementAppendTo.append('<div class="remove-from-wishlist-messages "></div>');
  }
  $('.remove-from-wishlist-messages').append('<div class="remove-from-wishlist-alert text-center alert-danger">' + msg + '</div>');

  setTimeout(function () {
    $('.remove-from-wishlist-messages').remove();
  }, 3000);
}

/**
 * Generates the modal window on the first call.
 *
 * @param {Object} data - render data
 */
function appendModalHtmlElement(data) {
  if ($('#signInModal').length !== 0) {
    $('#signInModal').empty();
  }
  var htmlString =
    '<!-- Modal -->' +
    '<div class="modal-dialog">' +
    '<div class="modal-content">' +
    '<div class="modal-body">' +
    '<div class="cancel-icon">' +
    '<button type="button" class="close svg-svg-22-cross svg-svg-22-cross-dims" data-dismiss="modal" aria-label="Close Modal Box"></button>' +
    '</div>' +
    '<div class="no-gutters modal-row align-items-start modal-contents">' +
    '<div class="modal-column">' +
    data.template +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';
  if ($(document).find('#signInModal').length) {
    $(document).find('#signInModal').append(htmlString);
  }
}

/**
 * open login model
 * @param {string} url url to be open with
 */
function openLoginPopup(url) {
  $('body').spinner().start();
  $.ajax({
    url: url,
    dataType: 'json',
    success: function (data) {
      $('body').spinner().stop();
      if (data.redirect) {
        window.location.href = data.redirect;
      } else {
        appendModalHtmlElement(data);
        login.login();
        $('#signInModal').modal();
        setTimeout(function () {
          formField.adjustForAutofill();
        }, 300);
      }
    },
    error: function () {
      $('body').spinner().stop();
    }
  });
}

/**
 * init for SignIn Model
 */
function initSignIn() {
  $('body').on('click', '.cart-empty .signin', function () {
    openLoginPopup($(this).data('url'));
  });
}

/**
 * Init for validate bonus product
 */
function initValidateOptionalBonusSelect() {
  $('body').on('validateBonusChoiceProduct', function (e, $this) {
    var maxPids = $('.choose-bonus-product-dialog').data('total-qty');
    var totalQty = $('input[name="chooseBonusproductIds"]:checked').length;
    if (totalQty <= maxPids) {
      $('.choice-of-bonus-product .error-div').empty();
      $('.pre-cart-products').html($('input[name="chooseBonusproductIds"]:checked').length);
      $('.selected-bonus-products .bonus-summary').removeClass('alert-danger');
    } else {
      $($this).prop('checked', false);
      // $('.selected-bonus-products .bonus-summary').addClass('alert-danger');
    }
  });
}

/**
 * Init for tax calculation
 */
function initOptionalBonusSelect() {
  $('body').on('click', '.bonus-product-item input[name="chooseBonusproductIds"]', function () {
    $('body').trigger('validateBonusChoiceProduct', this);
  });
}

/**
 * Init for product detail bonus product
 */
function initChooseBonusProductDetails() {
  $('body').on('click', '.gwp-pdt-details .product-details', function () {
    $(this).closest('div').find('.productDetailModal').addClass('show');
  });
}

// wishlist Floating labelwithout selecting the value.
var floatinglabelWishlist = function () {
  $('.wishlistItemCards .add-to-cart').each(function () {
    var isReadyToOrder = $(this).data('readytoorder');
    if (!isReadyToOrder) {
      $(this).closest('.card').find('.non-input-label').removeClass('input-focus');
      $(this).closest('.card').find('.selected-option span').addClass('d-none');
    }
  });
};

// wishlist product Tool tip.
var tooltipWishlist = function () {
  $('body').on('mouseover', '.wl-tooltip', function () {
    $(this).parents('.custom-tooltip').find('.tooltip-wlsale').attr('aria-hidden', 'false');
  });

  $('body').on('mouseleave', '.wl-tooltip', function () {
    $(this).parents('.custom-tooltip').find('.tooltip-wlsale').attr('aria-hidden', 'true');
  });

  $('body').on('focus', '.wl-tooltip', function () {
    $(this).parents('.custom-tooltip').find('.tooltip-wlsale').attr('aria-hidden', 'false');
  });

  $('body').on('keydown', '.wl-tooltip', function (e) {
    if (e.which === 27) {
      $(this).parents('.custom-tooltip').find('.tooltip-wlsale').attr('aria-hidden', 'true');
      e.preventDefault();
      return false;
    }
    return true;
  });

  $(document).ready(function () {
    floatinglabelWishlist();
  });
};

tooltipWishlist();

/**
Mini cart height
*/
function getMiniCartHeight() {
  if ($('.main-menu').is('.fixed')) {
    $($('.minicart .product-summary')[1]).css(
      'height',
      $($('.minicart .popover')[1]).outerHeight(true) -
        ($($('.minicart-footer')[1]).outerHeight(true) + $($('.bag-heading')[1]).outerHeight(true) + $('.main-menu').outerHeight(true))
    );
  } else {
    $($('.minicart .product-summary')[0]).css(
      'height',
      $($('.minicart .popover')[0]).outerHeight(true) -
        ($($('.minicart-footer')[0]).outerHeight(true) +
          $($('.bag-heading')[0]).outerHeight(true) +
          $('.header').outerHeight(true) -
          $(window).scrollTop() +
          33)
    );
  }
}

/**
 * Mini cart Scroll Height
 */
function setMiniCartScrollHeight() {
  var winHt = $(window).outerHeight();
  var mcFooterHt = $('.minicart-footer').outerHeight();
  $('.minicart .product-summary').scrollTop(0);
  if ($('.main-menu').is('.fixed')) {
    mcFooterHt = $($('.minicart-footer')[1]).outerHeight();
  } else {
    mcFooterHt = $($('.minicart-footer')[0]).outerHeight();
  }
  if (winHt < 786) {
    $('.minicart .cart').css('height', 410);
    $('.minicart .product-summary').css({
      'max-height': 410 - mcFooterHt
    });
  } else {
    $('.minicart .cart').css('height', 610);
    $('.minicart .product-summary').css({
      'max-height': 610 - mcFooterHt
    });
  }
}

module.exports = function () {
  $('body').on('afterRemoveFromCart', function (e, data) {
    e.preventDefault();
    confirmDelete(data.actionUrl, data.productID, data.productName, data.uuid);
  });

  $('.optional-promo').click(function (e) {
    e.preventDefault();
    $('.promo-code-form').toggle();
  });

  // wishlist changes
  $('body').on('click', '.remove-prod-from-wishlist', function (e) {
    e.preventDefault();
    var url = $(this).data('url');

    $.spinner().start();
    $.ajax({
      url: url,
      type: 'get',
      dataType: 'json',
      success: function (data) {
        if (data.totals && data.totals.discountsHtml) {
          $('.coupons-and-promos').empty().append(data.totals.discountsHtml);
        }

        // Since deleting product in wishlist will not affect anything I am deleting this
        updateItemsHTML(data);
        formField.adjustForAutofill();
        floatinglabelWishlist();
        if (data.deletedProduct) {
          $('body').trigger('adobeTagManager:removeFromFav', data.deletedProduct);
        }
        $.spinner().stop();
      },
      error: function () {
        $.spinner().stop();
        var $elToAppendWL = $('.wishlistItemCards');
        var msg = $elToAppendWL.data('error-msg');
        displayErrorMessage($elToAppendWL, msg);
      }
    });
  });

  $('body').on('click', '.remove-product, .remove-prod-minicart', function (e) {
    e.preventDefault();
    e.stopPropagation();

    var productID = $(this).data('pid');
    var url = $(this).data('action');
    var uuid = $(this).data('uuid');
    var updatedLineItems = $(this).closest('.prdt-cart-details').attr('updatedLineItems');
    if (updatedLineItems) {
      var urlParams = {
        pid: productID,
        uuid: uuid,
        updatedLineItems: updatedLineItems
      };
    } else {
      var urlParams = {
        pid: productID,
        uuid: uuid
      };
    }
    url = appendToUrl(url, urlParams);
    var isMiniCart = $(this).hasClass('remove-prod-minicart');
    var $currElement;
    if (isMiniCart) {
      $currElement = $('.uuid-' + uuid)
        .closest('.prdt-section')
        .prev('.prdt-section');
    }
    $('body > .modal-backdrop').remove();
    $('.cart-error').html('');

    if (isMiniCart) {
      $('.minicart .popover').spinner().start();
    } else {
      $.spinner().start();
    }
    $.ajax({
      url: url,
      type: 'get',
      dataType: 'json',
      success: function (data) {
        if (isMiniCart) {
          $('body').trigger('adobeTagManager:quantityUpdateFromMiniCart', data);
        } else {
          $('body').trigger('adobeTagManager:udpateCartQuantity', data);
        }
        if (data.basket.items.length === 0) {
          $('.pdt-cntr').empty().append(data.basket.emptyCartHtml);
          $('.number-of-items').empty().append(data.basket.resources.numberOfItems);
          $('.cart-summary-section').remove();
          $('.minicart-quantity').empty().append(data.basket.numItems);
          $('.minicart-link').attr({
            'aria-label': data.basket.resources.minicartCountOfItems,
            title: data.basket.resources.minicartCountOfItems
          });
          $('.minicart .popover').empty();
          $('.minicart .popover').removeClass('show');
          $('body').removeClass('modal-open');
          $('html').removeClass('veiled');
          $('.minicart-overlay').removeClass('background').removeAttr('style');
          $('body').css({
            'overflow-y': 'auto'
          });
          if ($('.cart-recommendation-umal').hasClass('d-none')) {
            $('.cart-recommendation-umal').removeClass('d-none');
            hbcSlider.cartReccommendationSliderInit();
          }
        } else {
          if (data.toBeDeletedUUIDs && data.toBeDeletedUUIDs.length > 0) {
            for (var i = 0; i < data.toBeDeletedUUIDs.length; i++) {
              $('.uuid-' + data.toBeDeletedUUIDs[i]).remove();
            }
          }
          $('.uuid-' + uuid).remove();
          if (!data.basket.hasBonusProduct) {
            $('.bonus-product').remove();
          }
          $('.minicart .popover .minicart-item-count').empty().append(data.basket.items.length);
          $('.coupons-and-promos').empty().append(data.basket.totals.discountsHtml);
          updateCartTotals(data.basket);
          updateItemsHTML(data.basket);
          updateApproachingDiscounts(data.basket.approachingDiscounts);
          updateAppliedCouponCount(data.basket);
          updateTotalSavings(data.basket);
          updateDiscountsHtml(data.basket);
          $('body').trigger('setShippingMethodSelection', data.basket);
          validateBasket(data.basket);
          if (!$('.cart-recommendation-umal').hasClass('d-none')) {
            $('.cart-recommendation-umal').addClass('d-none');
          }
        }

        $('body').trigger('cart:update');
        if (isMiniCart) {
          setMiniCartScrollHeight();
          if ($currElement.length > 0) {
            $('.minicart .product-summary').scrollTop($('.minicart .product-summary').scrollTop() - $currElement.offset().top);
          } else {
            $('.minicart .product-summary').scrollTop(0);
          }
        }

        $.spinner().stop();
      },
      error: function (err) {
        if (err.responseJSON.redirectUrl) {
          window.location.href = err.responseJSON.redirectUrl;
        } else {
          createErrorNotification(err.responseJSON.errorMessage);
          $.spinner().stop();
        }
      }
    });
  });

  $('body').on('afterRemoveFromCart', function (e, data) {
    e.preventDefault();
    confirmDelete(data.actionUrl, data.productID, data.productName, data.uuid);
  });

  $('.optional-promo').click(function (e) {
    e.preventDefault();
    $('.promo-code-form').toggle();
  });
  $('body').on('click', '.cart-delete-confirmation-btn', function (e) {
    e.preventDefault();

    var productID = $(this).data('pid');
    // eslint-disable-next-line no-unused-vars
    var url = $(this).data('action');
    var uuid = $(this).data('uuid');
    var urlParams = {
      pid: productID,
      uuid: uuid
    };

    url = appendToUrl(url, urlParams);

    $('body > .modal-backdrop').remove();

    var couponCode = $(this).data('code');
    // eslint-disable-next-line no-redeclare
    var uuid = $(this).data('uuid');
    var $deleteConfirmBtn = $('.delete-coupon-confirmation-btn');
    var $productToRemoveSpan = $('.coupon-to-remove');

    $deleteConfirmBtn.data('uuid', uuid);
    $deleteConfirmBtn.data('code', couponCode);
    $productToRemoveSpan.empty().append(couponCode);
  });

  $('body').on('click', '.delete-coupon-confirmation-btn', function (e) {
    e.preventDefault();

    var url = $(this).data('action');
    var uuid = $(this).data('uuid');
    var couponCode = $(this).data('code');
    var urlParams = {
      code: couponCode,
      uuid: uuid
    };

    url = appendToUrl(url, urlParams);

    $('body > .modal-backdrop').remove();

    $.spinner().start();
    $.ajax({
      url: url,
      type: 'get',
      dataType: 'json',
      success: function (data) {
        $('.coupon-uuid-' + uuid).remove();
        updateCartTotals(data);
        updateApproachingDiscounts(data.approachingDiscounts);
        updateDiscountsHtml(data);
        updateAppliedCouponCount(data);
        validateBasket(data);
        $.spinner().stop();
      },
      error: function (err) {
        if (err.responseJSON.redirectUrl) {
          window.location.href = err.responseJSON.redirectUrl;
        } else {
          createErrorNotification(err.responseJSON.errorMessage);
          $.spinner().stop();
        }
      }
    });
  });

  $('body').on('showOptionalProduct', function (e, data) {
    getOptionalBonusModalHtmlElement();
    $.spinner().start();
    $.ajax({
      url: $(data).data('url'),
      method: 'GET',
      dataType: 'json',
      success: function (resp) {
        fillChoiceOfBonusModalElement(resp);
        $.spinner().stop();
      },
      error: function () {
        $.spinner().stop();
      }
    });
    return;
  });

  $('body').on('click', '.bonus-product-button', function () {
    $(this).addClass('launched-modal');
    if ($(this).data('intermediate')) {
      $('body').trigger('showOptionalProduct', this);
      return;
    }
    $.spinner().start();
    $.ajax({
      url: $(this).data('url'),
      method: 'GET',
      dataType: 'json',
      success: function (data) {
        base.methods.editBonusProducts(data);
        $.spinner().stop();
      },
      error: function () {
        $.spinner().stop();
      }
    });
  });

  $('body').on('hidden.bs.modal', '#chooseBonusProductModal', function () {
    $('#chooseBonusProductModal').remove();
    $('#chooseBonusProductModal').next().remove();
    if ($('#editProductModal').hasClass('show')) {
      $('body').addClass('modal-open');
      if ($(window).width() > 1024) {
        $('body').css('padding-right', '17px');
      }
    }

    if ($('.cart-page').length) {
      $('.launched-modal .btn-outline-primary').trigger('focus');
      $('.launched-modal').removeClass('launched-modal');
    } else {
      $('.product-detail .add-to-cart').focus();
    }
  });

  $('body').on('click', '.cart-page .item-attributes .edit .brand-name', function (e) {
    e.preventDefault();
    // handle events after ajax, since to avaoid multiple time registration of events
    var editProductUrl = $(this).parent().attr('href');
    getModalHtmlElement();
    fillModalElement(editProductUrl);
  });

  $('body').on('click', '.cart-page .item-attributes .edit .line-item-name', function (e) {
    e.preventDefault();
    // handle events after ajax, since to avaoid multiple time registration of events
    var editProductUrl = $(this).parent().parent().attr('href');
    getModalHtmlElement();
    fillModalElement(editProductUrl);
  });

  $('body').on('click', '.cart-page .product-edit .edit, .cart-page .bundle-edit .edit, .cart-page .item-image .edit', function (e) {
    e.preventDefault();
    // handle events after ajax, since to avaoid multiple time registration of events
    var editProductUrl = $(this).attr('href');
    getModalHtmlElement();
    fillModalElement(editProductUrl);
  });

  // wishlist changes done to add to cart from wishlist
  if ($('.page').data('action') === 'Cart-Show') {
    $(document).on('change', 'select[class*="select-"]', function (e) {
      e.preventDefault();

      var $productContainer = $(this).closest('.set-item');
      if (!$productContainer.length) {
        $productContainer = $(this).closest('.card.product-info');
      }
      attributeSelect(e.currentTarget.value, $productContainer);
    });
    // Changes made to cart - add to cart
    $(document).on('click', 'button.add-to-cart, button.add-to-cart-global', function () {
      var addToCartUrl;
      var pid;
      var pidsObj;
      var setPids;

      var isReadyToOrder = $(this).data('readytoorder');
      if (!isReadyToOrder) {
        $(this).closest('.card').find('.alert-msg').removeClass('d-none');
        $(this).closest('.card').find('.selected-option').addClass('error');
        return;
      }

      var wlRemoveUrl = $(this).closest('.card').find('.removefromwlcart').val();

      $('body').trigger('product:beforeAddToCart', this);

      if ($('.set-items').length && $(this).hasClass('add-to-cart-global')) {
        setPids = [];

        $('.product-detail').each(function () {
          if (!$(this).hasClass('product-set-detail')) {
            setPids.push({
              pid: $(this).find('.product-id').text(),
              qty: $(this).find('.quantity-select').val(),
              options: getOptions($(this))
            });
          }
        });
        pidsObj = JSON.stringify(setPids);
      }
      pid = $(this).closest('.product-detail-wlcart').data('pid');

      var $productContainer = $(this).closest('.product-detail');
      if (!$productContainer.length) {
        $productContainer = $(this).closest('.quick-view-dialog').find('.product-detail');
      }

      addToCartUrl = $('.add-to-cart-url').val();

      var form = {
        pid: pid,
        pidsObj: pidsObj,
        childProducts: getChildProducts(),
        quantity: 1
      };

      if (!$('.bundle-item').length) {
        form.options = getOptions($productContainer);
      }

      $(this).trigger('updateAddToCartFormData', form);
      if (addToCartUrl) {
        $.spinner().start();
        var $this = $(this);
        $.ajax({
          url: addToCartUrl,
          method: 'POST',
          data: form,
          success: function (data) {
            // return and trigger a message product addition exceeded the basket preference limit
            if (data.message && data.message === 'LIMIT_EXCEEDED') {
              $('body').trigger('triggerBasketLimitMsgModal');
              $.spinner().stop();
            } else {
              if ($($this).hasClass('prod_wl_bttn')) {
                // Commented bagAdd event as per ticket SFDEV-3970
                // $('body').trigger('adobeTagManager:addToBag', data);
              } else {
                $('body').trigger('adobeTagManager:addToBag', data);
              }
              base.showAddToCartConfirmation(data.addToCartConfirmationModal);
              $('body').trigger('product:afterAddToCart', data);
              // changes made to remove the product from wishlist of the same is being added from WL - Start
              $.ajax({
                url: wlRemoveUrl,
                type: 'get',
                dataType: 'json',
                success: function (response) {
                  if ($($this).hasClass('prod_wl_bttn')) {
                    $('body').trigger('adobeTagManager:moveFavToBag', data);
                  }
                  $('.coupons-and-promos').empty().append(response.totals.discountsHtml);
                  updateItemsHTML(response);
                  updateCartTotals(response);
                  updateAppliedCouponCount(response);
                  updateTotalSavings(response);
                  formField.adjustForAutofill();
                  floatinglabelWishlist();
                  if (response.items.length === 0 && $('.cart-recommendation-umal').hasClass('d-none')) {
                    $('.cart-recommendation-umal').removeClass('d-none');
                    hbcSlider.cartReccommendationSliderInit();
                  } else if (response.items.length > 0 && !$('.cart-recommendation-umal').hasClass('d-none')) {
                    $('.cart-recommendation-umal').addClass('d-none');
                  }
                  $.spinner().stop();
                },
                error: function () {
                  $.spinner().stop();
                  var $elToAppendWL = $('.wishlistItemCards');
                  var msg = $elToAppendWL.data('error-msg');
                  displayErrorMessage($elToAppendWL, msg);
                }
              });
              // changes made to remove the product from wishlist of the same is being added from WL - End
            }
          },
          error: function () {
            $.spinner().stop();
          }
        });
      }
    });
  }

  // Changes done for cart - add to wishlist - Start
  $('body').on('click', '.cart-page .product-move .move', function (e) {
    e.preventDefault();
    var uuid = $(this).data('uuid');
    var pid = $(this).data('pid');
    var actionurl = $(this).data('action');
    var url = $(this).attr('href');

    var urlParams = {
      pid: pid,
      uuid: uuid
    };

    actionurl = appendToUrl(actionurl, urlParams);

    $.spinner().start();
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: {
        pid: pid
      },
      success: function (response) {
        $('body').trigger('adobeTagManager:cartMoveToFav', response);
        $.ajax({
          url: actionurl,
          type: 'get',
          dataType: 'json',
          success: function (data) {
            if (data.basket.items.length === 0) {
              $('.cart')
                .empty()
                .append(
                  '<div class="row"> ' + '<div class="col-12 text-center"> ' + '<h1>' + data.basket.resources.emptyCartMsg + '</h1> ' + '</div> ' + '</div>'
                );
              $('.cart-plis').empty().append(data.basket.emptyCartHtml);
              $('.number-of-items').empty().append(data.basket.resources.numberOfItems);
              $('.cart-summary-section').remove();
              $('.minicart-quantity').empty().append(data.basket.numItems);
              $('.minicart-link').attr({
                'aria-label': data.basket.resources.minicartCountOfItems,
                title: data.basket.resources.minicartCountOfItems
              });
              $('.minicart .popover').empty();
              $('.minicart .popover').removeClass('show');
              $('body').removeClass('modal-open');
              $('html').removeClass('veiled');
              if ($('.cart-recommendation-umal').hasClass('d-none')) {
                $('.cart-recommendation-umal').removeClass('d-none');
                hbcSlider.cartReccommendationSliderInit();
              }
            } else {
              if (data.toBeDeletedUUIDs && data.toBeDeletedUUIDs.length > 0) {
                for (var i = 0; i < data.toBeDeletedUUIDs.length; i++) {
                  $('.uuid-' + data.toBeDeletedUUIDs[i]).remove();
                }
              }
              $('.uuid-' + uuid).remove();
              if (!data.basket.hasBonusProduct) {
                $('.bonus-product').remove();
              }
              $('.coupons-and-promos').empty().append(data.basket.totals.discountsHtml);
              updateCartTotals(data.basket);
              updateApproachingDiscounts(data.basket.approachingDiscounts);
              updateAppliedCouponCount(data.basket);
              updateTotalSavings(data.basket);
              $('body').trigger('setShippingMethodSelection', data.basket);
              validateBasket(data.basket);
              if (!$('.cart-recommendation-umal').hasClass('d-none')) {
                $('.cart-recommendation-umal').addClass('d-none');
              }
            }

            $('body').trigger('cart:update');
            updateItemsHTML(data.basket);
            formField.adjustForAutofill();
            floatinglabelWishlist();
            $.spinner().stop();
          },
          error: function (err) {
            if (err.responseJSON.redirectUrl) {
              window.location.href = err.responseJSON.redirectUrl;
            } else {
              createErrorNotification(err.responseJSON.errorMessage);
              $.spinner().stop();
            }
          }
        });
      }
    });
  });
  // Changes done for cart - add to wishlist - End

  $('body').on('shown.bs.modal', '#editProductModal', function () {
    $('#editProductModal').siblings().attr('aria-hidden', 'true');
    $('#editProductModal .close').focus();
  });

  $('body').on('click', '.promo-click', function (e) {
    e.stopImmediatePropagation();
    $(this).toggleClass('open-state');
    $(this).closest('.form-group').find('.promo-detail-section').toggleClass('expand');
    $(this).closest('.loyalty-pwp__main-form-group').find('.promo-detail-section').toggleClass('expand');
  });

  $('body').on('hidden.bs.modal', '#editProductModal', function () {
    $('#editProductModal').siblings().attr('aria-hidden', 'false');
  });

  $('body').on('keydown', '#editProductModal', function (e) {
    var focusParams = {
      event: e,
      containerSelector: '#editProductModal',
      firstElementSelector: '.close',
      lastElementSelector: '.update-cart-product-global',
      nextToLastElementSelector: '.modal-footer .quantity-select'
    };
    focusHelper.setTabNextFocus(focusParams);
  });

  $('body').on('product:updateAddToCart', function (e, response) {
    // update global add to cart (single products, bundles)
    /* var dialog = $(response.$productContainer)
            .closest('.quick-view-dialog'); */

    $('button.add-to-cart-global').text(response.product.availability.buttonName);
    $('.update-cart-product-global').attr('disabled', !response.product.available);
    if (response.product.waitlistable && response.product.productType !== 'master' && !response.product.available) {
      var action = response.product.pdpURL + '#waitlistenabled';
      $('.add-to-waitlist-link').attr('href', action);
      $('.add-to-cart-global').addClass('d-none');
      $('.js-add-to-cart ').addClass('d-none');
      $('.update-cart-btn').addClass('d-none');
      $('.js-wait-list-form').removeClass('d-none');
      $('.js-add-to-waitlist-link').removeClass('d-none');
      $('.js-wait-list-form').find('.waitlist-product-id').val(response.product.id);
      $('.wait-list-success').empty();
      $('.js-wait-list-form')
        .find('input.js-waitlist-email')
        .on('focus', function () {
          $('body').trigger('adobe:waitListStart');
        });
      $('.js-wait-list-form')
        .find('input.js-waitlist-email')
        .on('blur', function () {
          $('.js-wait-list-form').find('input.js-waitlist-email').off('focus');
        });
    } else {
      $('.add-to-cart-global').removeClass('d-none');
      $('.update-cart-btn').removeClass('d-none');
      $('.js-add-to-cart ').removeClass('d-none');
      $('.js-add-to-waitlist-link').addClass('d-none');
      $('.js-wait-list-form').addClass('d-none');
      $('.wait-list-success').empty();
    }
  });

  $('body').on('product:updateAvailability', function (e, response) {
    // bundle individual products
    $('.product-availability', response.$productContainer)
      .data('ready-to-order', response.product.readyToOrder)
      .data('available', response.product.available)
      .find('.availability-msg')
      .empty()
      .html(response.message);
    $('div.error-msg').empty();
    if (!response.product.available && !response.product.waitlistable) {
      if (!response.product.availability.isInPurchaselimit) {
        createErrorNotificationQuickView(response.product.availability.isInPurchaselimitMessage);
      } else if (response.product.availability.messages && response.product.availability.messages.length > 0) {
        createErrorNotificationQuickView(response.product.availability.messages[0]);
      }
    }

    var dialog = $(response.$productContainer).closest('.quick-view-dialog');

    if ($('.product-availability', dialog).length) {
      // bundle all products
      var allAvailable = $('.product-availability', dialog)
        .toArray()
        .every(function (item) {
          return $(item).data('available');
        });

      var allReady = $('.product-availability', dialog)
        .toArray()
        .every(function (item) {
          return $(item).data('ready-to-order');
        });

      $('.global-availability', dialog).data('ready-to-order', allReady).data('available', allAvailable);

      $('.global-availability .availability-msg', dialog)
        .empty()
        .html(allReady ? response.message : response.resources.info_selectforstock);
    } else {
      // single product
      $('.global-availability', dialog)
        .data('ready-to-order', response.product.readyToOrder)
        .data('available', response.product.available)
        .find('.availability-msg')
        .empty()
        .html(response.message);
    }
  });

  $('body').on('product:afterAttributeSelect', function (e, response) {
    if ($('.modal.show .product-quickview .bundle-items').length) {
      $('.modal.show').find(response.container).data('pid', response.data.product.id);
      $('.modal.show').find(response.container).find('.product-id').text(response.data.product.id);
    } else {
      $('.modal.show .product-quickview').data('pid', response.data.product.id);
    }
    TurnToCmd('set', {
      //eslint-disable-line
      sku: response.data.product.id //eslint-disable-line
    }); //eslint-disable-line
  });

  $('body').on('change', '.quantity-select', function () {
    var selectedQuantity = $(this).val();
    $('.modal.show .update-cart-url').data('selected-quantity', selectedQuantity);
  });

  $('body').on('change', '.options-select', function () {
    var selectedOptionValueId = $(this).children('option:selected').data('value-id');
    $('.modal.show .update-cart-url').data('selected-option', selectedOptionValueId);
  });

  $('body').on('change', '.quantity-form select', function () {
    $('.qty-error-message').empty();
    var preSelectQty = $(this).data('pre-select-qty');
    var quantity = $(this).val();
    if (!quantity && $(this).parent().find('.quantity-val')) {
      quantity = $(this).parent().find('.quantity-val').attr('max');
    }
    var productID = $(this).data('pid');
    var url = $(this).data('action');
    var uuid = $(this).data('uuid');
    var urlParams = {
      pid: productID,
      quantity: quantity,
      uuid: uuid
    };
    var isFromMiniCart = $(this).closest('.mini-cart-container').length > 0;
    var currElementPos = 0;
    if (isFromMiniCart) {
      currElementPos = $(this).closest('.product-summary').scrollTop();
    }
    url = appendToUrl(url, urlParams);

    $(this).parents('.card').spinner().start();

    $.ajax({
      url: url,
      type: 'get',
      context: this,
      dataType: 'json',
      success: function (data) {
        $('.quantity[data-uuid="' + uuid + '"]').val(quantity);
        $('.coupons-and-promos').empty().append(data.totals.discountsHtml);
        updateItemsHTML(data);
        updateMiniCartItemsHTML(data);
        updateCartTotals(data);
        updateApproachingDiscounts(data.approachingDiscounts);
        updateAvailability(data, uuid);
        validateBasket(data);
        updateDiscountsHtml(data);
        updateAppliedCouponCount(data);
        updateTotalSavings(data);
        if ($('.enableShopRunner').length > 0) {
          if (typeof window.parent.sr_updateMessages === 'function') {
            window.parent.sr_updateMessages();
          }
        }
        if (isFromMiniCart) {
          $('body').trigger('adobeTagManager:quantityUpdateFromMiniCart', data);
          setMiniCartScrollHeight();
          $('.minicart .product-summary').each(function () {
            $(this).scrollTop(currElementPos);
          });
        } else {
          $('body').trigger('adobeTagManager:udpateCartQuantity', data);
        }
        $(this).data('pre-select-qty', quantity);

        $('body').trigger('cart:update');

        $.spinner().stop();
      },
      error: function (err) {
        if (err.responseJSON.redirectUrl) {
          window.location.href = err.responseJSON.redirectUrl;
        } else {
          /* if (err.responseJSON.isInStock === false) {
                        } else if (err.responseJSON.isInPurchaseLimit === false && $('body').hasClass('cart-page')) {
                            createPurchaseErrorNotificationAtLineItem(err.responseJSON.errorMessage, $(this));
                        } else {
                            createErrorNotification(err.responseJSON.errorMessage);
                        }*/

          createErrorNotificationOnLineItem(err.responseJSON.errorMessage, $(this).closest('.product-card-item'));

          // SFDEV-1933
          var $this = $(this).closest('div').find('input[class*="quantity-val"]');
          var currentVal = $this.val();
          var previousVal = parseInt(preSelectQty, 10);

          if (currentVal > previousVal) {
            $this.val(parseInt(preSelectQty, 10));
          }

          var minValue = parseInt($this.attr('min'), 10);
          var maxValue = parseInt($this.attr('max'), 10);
          var valueCurrent = parseInt($this.val(), 10);
          var name = $this.attr('name');

          if (valueCurrent >= minValue) {
            $this
              .closest('div')
              .find(".btn-number[data-type='minus'][data-field='" + name + "']")
              .removeAttr('disabled'); // eslint-disable-line
          } else {
            $this.val($(this).data('oldValue'));
          }

          if (valueCurrent <= maxValue) {
            $this
              .closest('div')
              .find(".btn-number[data-type='plus'][data-field='" + name + "']")
              .removeAttr('disabled'); // eslint-disable-line
          }

          if (valueCurrent === 1) {
            $this
              .closest('div')
              .find(".btn-number[data-type='minus'][data-field='" + name + "']")
              .attr('disabled', true); // eslint-disable-line
          } else if (valueCurrent === maxValue) {
            $this
              .closest('div')
              .find(".btn-number[data-type='plus'][data-field='" + name + "']")
              .attr('disabled', true); // eslint-disable-line
          } // update Drop Down

          $.spinner().stop();
        }
      }
    });
  });

  $('.shippingMethods').change(function () {
    var url = $(this).attr('data-actionUrl');
    var urlParams = {
      methodID: $(this).find(':selected').attr('data-shipping-id')
    };
    // url = appendToUrl(url, urlParams);

    $('.totals').spinner().start();
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: urlParams,
      success: function (data) {
        if (data.error) {
          window.location.href = data.redirectUrl;
        } else {
          $('.coupons-and-promos').empty().append(data.totals.discountsHtml);
          updateCartTotals(data);
          updateApproachingDiscounts(data.approachingDiscounts);
          updateAppliedCouponCount(data);
          updateTotalSavings(data);
          validateBasket(data);
        }
        $.spinner().stop();
      },
      error: function (err) {
        if (err.redirectUrl) {
          window.location.href = err.redirectUrl;
        } else {
          createErrorNotification(err.responseJSON.errorMessage);
          $.spinner().stop();
        }
      }
    });
  });

  $('body').on('submit', '.promo-code-form', function (e) {
    e.preventDefault();
    $.spinner().start();
    $('.coupon-missing-error').hide();
    $('.coupon-error-message').empty();
    if (!$('.coupon-code-field').val()) {
      $('.promo-code-form .form-control').addClass('is-invalid');
      $('.promo-input label').addClass('is-invalid');
      $(this).find('.coupon-code-field').next('span.invalid').remove();
      $('<span class="invalid"></span>').insertAfter('.coupon-code-field');
      $('.promo-code-form .form-control').attr('aria-describedby', 'missingCouponCode');
      $('.coupon-missing-error').show();
      $.spinner().stop();
      var formData = {};
      formData.errorFields = ['coupon code'];
      formData.formName = 'cart';
      $('body').trigger('adobeTagManager:formError', formData);
      return false;
    }
    var $form = $('.promo-code-form');
    $('.promo-code-form .form-control').removeClass('is-invalid');
    $('.promo-input label').removeClass('is-invalid');
    $('.coupon-code-field').next('.invalid').remove();
    $('.coupon-error-message').empty();

    $.ajax({
      url: $form.attr('action'),
      type: 'GET',
      dataType: 'json',
      data: $form.serialize(),
      success: function (data) {
        var promoCoupon = {};
        promoCoupon.status = data && !data.error;
        promoCoupon.coupon = $($form).find('.coupon-code-field').val();
        $('body').trigger('adobeTagManager:promoCodeEntered', promoCoupon);
        if (data.error) {
          $('.promo-code-form .form-control').addClass('is-invalid');
          $('.promo-input label').addClass('is-invalid');
          $('<span class="invalid"></span>').insertAfter('.coupon-code-field');
          $('.promo-code-form .form-control').attr('aria-describedby', 'invalidCouponCode');
          $('.coupon-error-message').empty().append(data.errorMessage);
          $('.coupon-code-field').val(promoCoupon.coupon);
        } else {
          $('.coupons-and-promos').empty().append(data.totals.discountsHtml);
          updateItemsHTML(data);
          updateCartTotals(data);
          updateApproachingDiscounts(data.approachingDiscounts);
          updateDiscountsHtml(data);
          updateAppliedCouponCount(data);
          updateTotalSavings(data);
          validateBasket(data);
          $('.coupon-code-field').val('');
        }
        hbcTooltip.tooltipInit();
        $.spinner().stop();
      },
      error: function (err) {
        if (err.responseJSON.redirectUrl) {
          window.location.href = err.responseJSON.redirectUrl;
        } else {
          createErrorNotification(err.errorMessage);
          $.spinner().stop();
        }
      }
    });
    return false;
  });
  $('body').on('focusout', '.coupon-code-field', function (e) {
    e.preventDefault();
    var form = $('.promo-code-form');
    var couponCode = $('.coupon-code-field').val();
    if (!couponCode) {
      $('.coupon-missing-error').hide();
      $(form).find('.coupon-code-field').removeClass('is-invalid');
      $(form).find('.coupon-code-field').next('span').remove();
      $(form).find('span.coupon-error-message').empty();
      $(form).find('.coupon-code-field').next('label').removeClass('is-invalid').removeClass('input-focus');
    }
  });

  $('body').on('click', '.remove-coupon', function (e) {
    e.preventDefault();

    var url = $(this).data('action');
    var uuid = $(this).data('uuid');
    var couponCode = $(this).data('code');
    var urlParams = {
      code: couponCode,
      uuid: uuid
    };

    url = appendToUrl(url, urlParams);

    $('body > .modal-backdrop').remove();

    $.spinner().start();
    $.ajax({
      url: url,
      type: 'get',
      dataType: 'json',
      success: function (data) {
        $('.coupon-uuid-' + uuid).remove();
        $('.coupons-and-promos').empty().append(data.totals.discountsHtml);
        updateItemsHTML(data);
        updateCartTotals(data);
        updateApproachingDiscounts(data.approachingDiscounts);
        updateAppliedCouponCount(data);
        updateDiscountsHtml(data);
        updateTotalSavings(data);
        validateBasket(data);
        $.spinner().stop();
      },
      error: function (err) {
        if (err.responseJSON.redirectUrl) {
          window.location.href = err.responseJSON.redirectUrl;
        } else {
          createErrorNotification(err.responseJSON.errorMessage);
          $.spinner().stop();
        }
      }
    });
  });

  $('body').on('hidden.bs.modal', '#chooseBonusProductModal', function () {
    $('#chooseBonusProductModal').remove();
    $('#chooseBonusProductModal').next().remove();
    if ($('#editProductModal').hasClass('show')) {
      $('body').addClass('modal-open');
      if ($(window).width() > 1024) {
        $('body').css('padding-right', '17px');
      }
    }

    if ($('.cart-page').length) {
      $('.launched-modal .btn-outline-primary').trigger('focus');
      $('.launched-modal').removeClass('launched-modal');
    } else {
      $('.product-detail .add-to-cart').focus();
    }
  });

  $('body').on('shown.bs.modal', '#editProductModal', function () {
    $('#editProductModal').siblings().attr('aria-hidden', 'true');
    $('#editProductModal .close').focus();
  });

  /* $('body')
        .on('click', '.form-group', '.promo-heading', function () {
            $('.form-group')
                .toggleClass('open-state');
            $('.form-group')
                .find('.promo-apply')
                .toggleClass('expand');
        });*/

  $('body').on('click', '.productDetailModal .back-icon, .productDetailModal .close', function () {
    $('.productDetailModal').removeClass('show').attr('aria-modal style');
  });

  $('body').on('hidden.bs.modal', '#editProductModal', function () {
    $('#editProductModal').siblings().attr('aria-hidden', 'false');
  });

  $('body').on('keydown', '#editProductModal', function (e) {
    var focusParams = {
      event: e,
      containerSelector: '#editProductModal',
      firstElementSelector: '.close',
      lastElementSelector: '.update-cart-product-global',
      nextToLastElementSelector: '.modal-footer .quantity-select'
    };
    focusHelper.setTabNextFocus(focusParams);
  });

  $('body').on('product:afterAttributeSelect', function (e, response) {
    if ($('.modal.show .product-quickview .bundle-items').length) {
      $('.modal.show').find(response.container).data('pid', response.data.product.id);
      $('.modal.show').find(response.container).find('.product-id').text(response.data.product.id);
      $('.modal.show').find(response.container).find('.bf-product-id').text(response.data.product.id);
    } else {
      $('.modal.show .product-quickview').data('pid', response.data.product.id);
      $('.modal.show .bf-product-id').text(response.data.product.id);
    }
  });

  $('body').on('change', '.quantity-select', function () {
    var selectedQuantity = $(this).val();
    $('.modal.show .update-cart-url').data('selected-quantity', selectedQuantity);
  });

  $('body').on('change', '.options-select', function () {
    var selectedOptionValueId = $(this).children('option:selected').data('value-id');
    $('.modal.show .update-cart-url').data('selected-option', selectedOptionValueId);
  });

  $('body').on('click', '.update-cart-product-global', function (e) {
    e.preventDefault();

    if (!$(this).data('readytoorder')) {
      $('.select-size-color').html($(this).data('readytoordertext'));
      return;
    }

    var updateProductUrl = $(this).closest('.cart-and-ipay').find('.update-cart-url').val();
    var selectedQuantity = $(this).closest('.cart-and-ipay').find('.update-cart-url').data('selected-quantity');
    var selectedOptionValueId = $(this).closest('.cart-and-ipay').find('.update-cart-url').data('selected-option');
    var uuid = $(this).closest('.cart-and-ipay').find('.update-cart-url').data('uuid');

    var form = {
      uuid: uuid,
      pid: base.getPidValue($(this)),
      quantity: selectedQuantity,
      selectedOptionValueId: selectedOptionValueId
    };

    $(this).parents('.card').spinner().start();
    if (updateProductUrl) {
      $.ajax({
        url: updateProductUrl,
        type: 'post',
        context: this,
        data: form,
        dataType: 'json',
        success: function (data) {
          $('#editProductModal').modal('hide');

          $('.coupons-and-promos').empty().append(data.cartModel.totals.discountsHtml);
          updateCartTotals(data.cartModel);
          updateApproachingDiscounts(data.cartModel.approachingDiscounts);
          updateAvailability(data.cartModel, uuid);
          updateProductDetails(data);
          updateAppliedCouponCount(data.cartModel);
          updateTotalSavings(data.cartModel);

          if (data.uuidToBeDeleted) {
            $('.uuid-' + data.uuidToBeDeleted).remove();
          }

          validateBasket(data.cartModel);

          $('body').trigger('cart:update');

          $.spinner().stop();
        },
        error: function (err) {
          if (err.responseJSON.redirectUrl) {
            window.location.href = err.responseJSON.redirectUrl;
          } else {
            createErrorNotificationQuickView(err.responseJSON.errorMessage);
            $.spinner().stop();
          }
        }
      });
    }
  });

  /**
   * Initialize proxy checkout
   *
   */
  function initProxyCheckout() {
    $('body').on('click', '.proxy-checkout-btn', function (e) {
      e.preventDefault();
      // Get the BFX Country Code sleected.
      $('.bfx-error-message').empty();
      if ($('.bfx-cc-countries').length > 0) {
        var bfxCountryCode = $('.bfx-cc-countries').val();
        if (bfxCountryCode !== 'US') {
          var bfxMerchatNumber = $('.bordefree-merchat-reference-number').text().trim();
          $('body').spinner().start();
          $.ajax({
            url: $(this).attr('href'),
            type: 'post',
            data: {
              bfxCountryCode: bfxCountryCode,
              bfxMerchatNumber: encodeURIComponent(bfxMerchatNumber)
            },
            dataType: 'json',
            success: function (data) {
              if (data && data.success) {
                $('.trigger-checkout-btn')[0].click();
                setTimeout(function () {
                  $('body').spinner().stop();
                }, 3000);
              } else if (data.error && data.errorMessage) {
                var errorHtml =
                  '<div class="alert alert-danger alert-dismissible valid-cart-error ' +
                  'fade show" role="alert">' +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                  '<span aria-hidden="true">&times;</span>' +
                  '</button>' +
                  data.errorMessage +
                  '</div>';
                $('.cart-error').html('');
                $('.cart-error').append(errorHtml);
                $('body').spinner().stop();
              } else if (data.error && data.redirectURL) {
                window.location.href = data.redirectURL;
                $('body').spinner().stop();
              }
            },
            error: function () {
              $('body').spinner().stop();
              window.location.reload();
            }
          });
        } else {
          $('.trigger-checkout-btn')[0].click();
        }
      } else {
        $('.trigger-checkout-btn')[0].click();
      }
    });
  }

  /**
   * Initialize checkout
   *
   */
  function initCheckout() {
    $('body').on('click', '.cart-summary-section .checkout-btn', function (e) {
      e.preventDefault();
      if (this.getAttribute('href').charAt(0) !== '#') {
        openLoginPopup($(this).attr('href'));
      }
    });
  }

  /**
   * initializing calculate tax
   *
   */
  function initCalculateTax() {
    $('body').on('submit', '.cart-tax-calculation-form', function (e) {
      e.preventDefault();
      $('body').spinner().start();
      $.ajax({
        url: $(this).attr('action'),
        type: 'post',
        data: $(this).serialize(),
        dataType: 'json',
        success: function (data) {
          $('body').spinner().stop();
          $('.cart-summary-details .invalid-feedback-error').empty();
          if (!data.error) {
            updateCartTotals(data.data);
            $('.shipping-postal-code').text(data.postalCode);
            $('.postal-container').toggleClass('active');
            $('.tax_calculate').parents('.form-group').find('label, input').removeClass('is-invalid');
          } else {
            $('.cart-summary-details .invalid-feedback-error').html(data.error);
            $('.tax_calculate').parents('.form-group').find('label, input').addClass('is-invalid');
          }
        },
        error: function () {
          $('body').spinner().stop();
        }
      });
    });

    $('body').on('click', '.cart-summary-details .shipping-postal-code', function (e) {
      e.preventDefault();
      $('.postal-container').toggleClass('active');
    });

    $('body').on('click', '.cart-summary-details .tax_cancel', function (e) {
      e.preventDefault();
      $('.postal-container').removeClass('active');
    });
  }

  function openCSCMOdel() {
    $('body').on('click', '.js-open-csc-price-model', function (e) {
      e.preventDefault();
      $('body').spinner().start();
      var itemPrice = $(this).closest('.cart-pdt-details').find('.formatted_price').last().text();
      var itemUUID = $(this).data('pliuuid');
      $('#editCSCPrice .csc-current-price').val(itemPrice);
      $('#editCSCPrice #newPrice').val('');
      $('.price-override-reason-options').val('');
      $('#editCSCPrice .csc-pli-uuid').val(itemUUID);
      $('#editCSCPrice .csr_error_message').removeClass('alert alert-danger alert-dismissible');
      $('#editCSCPrice .csr_error_message').empty();
      $('#editCSCPrice').modal('show');
      $('body').spinner().stop();
    });
  }

  $(document).on('keydown', '#newPrice', function (e) {
    if (e.ctrlKey === true && (e.which === 118 || e.which === 86)) {
      e.preventDefault();
    }

    var keyVal = e.which ? e.which : e.keyCode;
    if (keyVal === 46 || keyVal === 190 || keyVal === 8) {
      return true;
    } else if (!(keyVal >= 48 && keyVal <= 57)) {
      return false;
    }
  });

  function submitCSCPrice() {
    $('body').on('submit', '.price-override-form', function (e) {
      e.preventDefault();
      var $form = $(this);
      var pliuuid = $('#editCSCPrice .csc-pli-uuid').val();
      $('.csr_error_message').empty();
      $('.csr_error_message').removeClass('alert alert-danger alert-dismissible');
      $('body').spinner().start();
      $.ajax({
        url: $form.attr('action'),
        type: 'post',
        data: $(this).closest('form').serialize(),
        dataType: 'json',
        success: function (data) {
          $('body').spinner().stop();
          $('.coupons-and-promos').empty().append(data.totals.discountsHtml);
          updateItemsHTML(data);
          updateMiniCartItemsHTML(data);
          updateCartTotals(data);
          updateApproachingDiscounts(data.approachingDiscounts);
          updateAvailability(data, pliuuid);
          validateBasket(data);
          updateDiscountsHtml(data);
          updateAppliedCouponCount(data);
          updateTotalSavings(data);
          $('body').trigger('cart:update');
          if (!data.CSRSuccess && data.CSRerrorMessage) {
            $('.csr_error_message').empty().text(data.CSRerrorMessage);
            $('.csr_error_message').addClass('alert alert-danger alert-dismissible');
          } else {
            $('#editCSCPrice').modal('hide');
          }
          $('body').spinner().stop();
        },
        error: function () {
          $('body').spinner().stop();
        }
      });
    });
  }

  /**
   * Init on click of alternate image
   */
  function initAlternateImage() {
    $('body').on('click', '.product-quickview .slick-next, .product-quickview .slick-prev', function () {
      $('body').trigger('adobeTagManager:quickViewAltImageView');
    });
  }

  /**
   * Init on click of alternate image
   */
  function initAlternateBubble() {
    $('body').on('click', '.product-quickview .slick-dots button', function () {
      $('body').trigger('adobeTagManager:quickViewAltImageView');
    });
  }

  /**
   * Init Adobe Analytices paymentServiceExit event
   */
  function initPaymentServiceExit() {
    /*
     *   paypal cart button click event handled in LINK_PayPal/cartridges/int_paypal_sfra/cartridge/static/default/js/paypalUtils.js
     *   paypal billing button click event handled in LINK_PayPal/cartridges/int_paypal_sfra/cartridge/static/default/js/paypalMFRA.js
     */
    $('.pass-cart-buttons-container')
      .find('img#masterpass-payment')
      .on('click', function () {
        $('body').trigger('adobeTagManager:exitToPaymentService', 'masterpass');
      });
    $('body, html').on('click', '.chkout-btn-shoprunner button', function () {
      $('body').trigger('adobeTagManager:exitToPaymentService', 'shoprunner_express');
    });
  }

  /**
   * function to enable button if data/input available in promo and hbc rewards field
   */
  function promoRewardsButtonUpdate() {
    $('.promoRewHolder').on('input', 'input.emptyCheck', function () {
      $(this).val().length > 0
        ? $(this).closest('.promoRewHolder').find('.prmoRewBtnHolder button').removeAttr('disabled')
        : $(this).closest('.promoRewHolder').find('.prmoRewBtnHolder button').attr('disabled', true);
    });
  }

  /**
   * Trigger basket limit messaging modal
   */
  function triggerBasketMimitModal() {
    if (
      ($('.cart-page-content').length > 0 &&
        $('.cart-page-content').attr('data-basketlimit-reached') != undefined &&
        $('.cart-page-content').attr('data-basketlimit-reached') === 'true') ||
      ($('.welcome-account-header').length > 0 &&
        $('.welcome-account-header').attr('data-basketlimit-reached') != undefined &&
        $('.welcome-account-header').attr('data-basketlimit-reached') === 'true')
    ) {
      $('body').trigger('triggerBasketLimitMsgModal');
    }
  }

  function loadCartWishlistAsync() {
    if ($('.page').data('action') === 'Cart-Show') {
      $.ajax({
        url: $('.cart-page-content').data('cart-wishlist'),
        success: function (response) {
          $('body').find('.cart-wl-content').html(response);
          hbcSlider.hbcSliderInit('wishlist-cart-slider');
          hbcSlider.cartReccommendationSliderInit();
        }
      });
    }
  }

  hbcTooltip.tooltipInit();
  base.initQuantity();
  base.availability();
  base.selectAttribute();
  base.colorAttribute();
  base.removeBonusProduct();
  base.selectBonusProduct();
  base.enableBonusProductSelection();
  base.showMoreBonusProducts();
  base.addBonusProductsToCart();
  base.focusChooseBonusProductModal();
  base.trapChooseBonusProductModalFocus();
  base.onClosingChooseBonusProductModal();
  base.checkProductAvailability();
  initSignIn();
  initCheckout();
  initCalculateTax();
  initValidateOptionalBonusSelect();
  initOptionalBonusSelect();
  initChooseBonusProductDetails();
  initProxyCheckout();
  openCSCMOdel();
  submitCSCPrice();
  initAlternateImage();
  initAlternateBubble();
  initPaymentServiceExit();
  promoRewardsButtonUpdate();
  triggerBasketMimitModal();
  loadCartWishlistAsync();
};
