'use strict';

const debounce = require('lodash/debounce');
/**
 * Finds the associated full size image referenced by the anchor link on a thumbnail image.
 * @param {element} thumbnailLink The thumbnail link element
 */
const getAssociatedImage = function (thumbnailLink) {
  return document.querySelector(thumbnailLink.href.replace(/(.*)#(.*)/, '#$2'));
};

/**
 * Sets up scrolling to a target image when a user clicks on a thumbnail. Bound to the click event on the thumbnails.
 * @param {Event} evt The event object.
 */
const handleThumbnailClick = function (evt) {
  evt.preventDefault();
  let thumbnailLink = evt.target;
  if (!thumbnailLink.matches('a')) {
    thumbnailLink = evt.target.closest('a');
  }
  const targetImage = getAssociatedImage(thumbnailLink);
  const mainMenu = document.querySelector('.main-menu');
  const top = $(targetImage).offset().top - (mainMenu ? mainMenu.offsetHeight : 0);
  if ('scroll' in window) {
    window.scroll({ top: top, behavior: 'smooth' });
  } else {
    window.scrollTop = top;
  }
};

var primaryImageObservers = [];

/**
 * Sets up an intersection observer to update the active thumbnail when scrolling.
 * Updates the observer on resize and orientation change.
 * @param {Element} thumbnailLink A thumbnail image link
 */
const initScrollObserver = function (thumbnailLink) {
  const targetImage = getAssociatedImage(thumbnailLink);
  if (!targetImage || !('IntersectionObserver' in window)) {
    return;
  }
  const handleIntersect = function (entries, observer) {
    entries.forEach(function (entry) {
      if (entry.isIntersecting) {
        const image = entry.target;
        const thumbnail = document.querySelector('.primary-thumbnails a[href="#' + image.id + '"]');
        updateActiveThumbnail(thumbnail);
      }
    });
  };

  let observer;

  const observeImageScrollPosition = function () {
    if (observer) {
      observer.disconnect();
    }
    // Ignore references to targetImage references which have been removed from the DOM
    // This happens when you select a size or color and the images get re-loaded.
    if (!document.body.contains(targetImage)) {
      return;
    }
    const pageHeader = document.querySelector('.page header'); // It's the first one. It has no class name
    const headerHeight = pageHeader ? pageHeader.offsetHeight : 0;
    observer = new IntersectionObserver(handleIntersect, {
      threshold: 10 / targetImage.offsetHeight,
      rootMargin: `${headerHeight * -1}px 0px ${(window.innerHeight - headerHeight - 10) * -1}px 0px`
    });
    observer.observe(targetImage);
    primaryImageObservers.push({
      observer: observer,
      element: targetImage
    });
  };

  observeImageScrollPosition();

  const debouncedObserveImageScrollPosition = debounce(observeImageScrollPosition, 300);

  window.addEventListener('resize', debouncedObserveImageScrollPosition);
  window.addEventListener('orientationchange', debouncedObserveImageScrollPosition);

  // Watch changes on the fixed header
  const mainMenu = document.querySelector('.main-menu');
  if (mainMenu) {
    const menuObserver = new MutationObserver(() => observeImageScrollPosition());
    menuObserver.observe(mainMenu, { attributes: true });
  }
};

/**
 * Initializes thumbnail image links
 */
const thumbnailImageLinks = function () {
  const thumbnailLinks = document.querySelectorAll('.primary-thumbnails a[href^="#"]');
  if (!thumbnailLinks.length) {
    return;
  }
  [].forEach.call(thumbnailLinks, function (link) {
    link.addEventListener('click', handleThumbnailClick);
    initScrollObserver(link);
  });
};

const initPrimaryImages = function () {
  const primaryImages = document.querySelector('.primary-images');
  if (!primaryImages) {
    return;
  }

  // Look for changes to the primary images (e.g. color / variant selected)
  const observerOptions = { childList: true };
  const observer = new MutationObserver(function (mutationList) {
    mutationList.forEach(function (mutation) {
      if (mutation.type === 'childList') {
        if (mutation.removedNodes.length) {
          let node;
          do {
            node = primaryImageObservers.shift();
            if (node) {
              node.observer.unobserve(node.element);
            }
          } while (primaryImageObservers.length);
        }

        if (mutation.addedNodes.length) {
          // Initialize new thumbnails
          thumbnailImageLinks();
        }
      }
    });
  });
  observer.observe(primaryImages, observerOptions);

  // Set up thumbnails.
  thumbnailImageLinks();
};

/**
 * Sets a thumbnail as active, deactivating other thumbnails in the process.
 * @param {Element} el The thumbnail link to make active
 */
var updateActiveThumbnail = function (el) {
  var active = document.querySelectorAll('.primary-thumbnails a.active');
  [].forEach.call(active, function (link) {
    link.classList.remove('active');
  });
  if (el) {
    el.classList.add('active');
  }
};

module.exports = initPrimaryImages;
