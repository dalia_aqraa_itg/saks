'use strict';

var base = module.superModule;
var Site = require('dw/system/Site');
var customPreferences = Site.current.preferences.custom;
var URLUtils = require('dw/web/URLUtils');
var isTsysMode = require('*/cartridge/scripts/helpers/tsysHelpers').isTsysMode;

base.maxOrderQty = 'maxOrderQty' in customPreferences ? customPreferences.maxOrderQty : 10;

// PLP and wishlist config
base.defaultPageSize = 'defaultPageSize' in customPreferences ? customPreferences.defaultPageSize : 24;
base.refinementValueMaxLimit = customPreferences.refinementValueMaxLimit;
base.designerFilters = customPreferences.designerFilters;
base.totalPageSize = 'totalPageSize' in customPreferences ? customPreferences.totalPageSize : 96;

if(request.httpUserAgent && ~request.httpUserAgent.indexOf('FABUILD')) {
  base.totalPageSize = 24000;
}

base.product = {
  INVENTORY_THRESHOLD_VALUE: 'InventoryThreshold' in customPreferences ? customPreferences.InventoryThreshold : null,
  IS_FINAL_SALE_TOOGLE: 'isFinalSaleToogle' in customPreferences ? customPreferences.isFinalSaleToogle : null,
  PD_RESTRICTED_SHIP_MAP: 'PD_RestrictedShipType_Text_Map' in customPreferences ? customPreferences.PD_RestrictedShipType_Text_Map : null,
  BADGE_HEX_COLOR: 'badgeColorHex' in customPreferences ? customPreferences.badgeColorHex : null,
  SIZE_TREATMENT: 'sizeTreatmentMap' in customPreferences ? customPreferences.sizeTreatmentMap : null,
  edvp: 'EDVP' in customPreferences && !!customPreferences.EDVP ? customPreferences.EDVP : ''
};

base.price = {
  PRICE_BANDS_FOR_BRIDAL_PRODUCT: 'priceBandsForBridalProduct' in customPreferences ? customPreferences.priceBandsForBridalProduct : null
};
base.dataSubscription = {
  BANNER: 'bannerSubscription' in customPreferences ? customPreferences.bannerSubscription : null,
  canadaOptAllowed: 'optAllowedForCanadaCustomer' in customPreferences ? customPreferences.optAllowedForCanadaCustomer : false,
  siteType: 'siteType' in customPreferences ? customPreferences.siteType.value : null
};
base.displayQuickLook = customPreferences.DisplayQuicklook;

base.vertexShippingProductClass = customPreferences.hasOwnProperty('vertexShippingProductClass') ? customPreferences.vertexShippingProductClass : null; // eslint-disable-line

base.plpRefineViewMoreCatLimit = 'plpRefineViewMoreCatLimit' in customPreferences ? customPreferences.plpRefineViewMoreCatLimit : 9;
base.isBopisEnabled = customPreferences.hasOwnProperty('isBopisEnabled') ? customPreferences.isBopisEnabled : false; // eslint-disable-line
base.limitedQtyThreshold =
  customPreferences.hasOwnProperty('limitedQtyThreshold') && customPreferences.limitedQtyThreshold ? customPreferences.limitedQtyThreshold : 5; // eslint-disable-line

// scene7 preferences
base.s7APIForPDPZoomViewer = 's7APIForPDPZoomViewer' in customPreferences ? customPreferences.s7APIForPDPZoomViewer : '';
base.s7APIForPDPVideoPlayer = 's7APIForPDPVideoPlayer' in customPreferences ? customPreferences.s7APIForPDPVideoPlayer : '';
base.Zoom = 'Zoom' in customPreferences ? customPreferences.Zoom : '';
base.s7VideoServerURL = 's7VideoServerURL' in customPreferences ? customPreferences.s7VideoServerURL : '';
base.s7ImageHostURL = 's7ImageHostURL' in customPreferences ? customPreferences.s7ImageHostURL : '';
base.imageRelativePath = 'imageRelativePath' in customPreferences ? customPreferences.imageRelativePath : '';
base.videoRelativePath = 'videoRelativePath' in customPreferences ? customPreferences.videoRelativePath : '';
base.imageSharpeningParams = 'imageSharpeningParams' in customPreferences ? customPreferences.imageSharpeningParams : '';

base.enableCNS = 'enableCNS' in customPreferences ? customPreferences.enableCNS : false;
base.enableNarvar = 'enableNarvar' in customPreferences ? customPreferences.enableNarvar : false;
base.returnURL = 'returnURL' in customPreferences ? customPreferences.returnURL : '';
base.shopRunnerEnabled = 'sr_enabled' in customPreferences ? customPreferences.sr_enabled : false;

base.hudsonReward = 'hbcRewardPrefix' in customPreferences ? customPreferences.hbcRewardPrefix : '';
base.HUDSON_REWARD_POINT = 'hudsonBayPriceMap' in customPreferences ? customPreferences.hudsonBayPriceMap : -1;

// Payment Config
base.enableTokenEx = 'enableTokenEx' in customPreferences && customPreferences.enableTokenEx ? customPreferences.enableTokenEx : false;
base.tokenExPublicKey = 'tokenExPublicKey' in customPreferences && customPreferences.enableTokenEx ? customPreferences.tokenExPublicKey : '';

base.promoTrayEnabled = 'PROMO_TRAY_ENABLED' in customPreferences ? customPreferences.PROMO_TRAY_ENABLED : false;

base.locationID = 'locationID' in customPreferences ? customPreferences.locationID : 'hbc';
base.appointmentTypeID = 'appointmentTypeID' in customPreferences ? customPreferences.appointmentTypeID : 'dressselection';
base.timeTradeURL = 'timeTradeURL' in customPreferences ? customPreferences.timeTradeURL : '';

// HBC MW configs
base.hbcAPIKey = 'hbcAPIKey' in customPreferences ? customPreferences.hbcAPIKey : '';
base.hbcAPIID = 'hbcGWAPIKey' in customPreferences ? customPreferences.hbcGWAPIKey : '';
base.hbcBanner = 'hbcBanner' in customPreferences ? customPreferences.hbcBanner : '';
base.hbcStoreNumber = 'hbcStoreNumber' in customPreferences ? customPreferences.hbcStoreNumber : '';
base.hbcLanguageCode = 'hbcLanguageCode' in customPreferences ? customPreferences.hbcLanguageCode : '';
base.hbcCountryCode = 'hbcCountryCode' in customPreferences ? customPreferences.hbcCountryCode : '';
base.countryCode = 'countryCode' in customPreferences ? customPreferences.countryCode : ''; // Find Store Country Code
base.hbcChannel = 'hbcChannel' in customPreferences ? customPreferences.hbcChannel : '';
base.hbcSubChannel = 'hbcSubChannel' in customPreferences ? customPreferences.hbcSubChannel : '';
base.hbcGcTypeOnRequest = 'hbcGcTypeOnRequest' in customPreferences ? customPreferences.hbcGcTypeOnRequest : '';
base.hbcEntryMode = 'hbcEntryMode' in customPreferences ? customPreferences.hbcEntryMode : '';
base.hbcCurrencyCode = 'hbcCurrencyCode' in customPreferences ? customPreferences.hbcCurrencyCode : '';
base.giftCardBalanceCheckService = 'ipa.gcbalancecheck.' + Site.getCurrent().ID.toLowerCase();
base.dropShippingMethodsValueMap = 'dropShippingMethodsValueMap' in customPreferences ? customPreferences.dropShippingMethodsValueMap : null;
base.giftWrapDisabled = 'giftWrapDisabled' in customPreferences ? customPreferences.giftWrapDisabled : true;
base.signatureRequired = 'signatureRequired' in customPreferences ? customPreferences.signatureRequired : false;
base.signatureOrderThreshold = 'signatureOrderThreshold' in customPreferences ? customPreferences.signatureOrderThreshold : null;

base.createOrderService = 'oms.createorderservice.' + Site.getCurrent().ID.toLowerCase();

// CNS Configs
base.cnsBanner = 'cnsBanner' in customPreferences ? customPreferences.cnsBanner : '';
base.cnsSubtypes = 'cnsSubtypes' in customPreferences ? customPreferences.cnsSubtypes : '';
base.cnsEventType = 'cnsEventType' in customPreferences ? customPreferences.cnsEventType : '';
base.emailLangPrefLocaleMapping = 'emailLangPrefLocaleMapping' in customPreferences ? JSON.parse(customPreferences.emailLangPrefLocaleMapping) : {};

// Loyalty configs
base.loyaltyBanner = 'loyaltyBanner' in customPreferences ? customPreferences.loyaltyBanner : '';

// Gooogle Recaptcha configs
base.googleRecaptchaAPIURL = 'googleRecaptchaAPIURL' in customPreferences ? customPreferences.googleRecaptchaAPIURL : '';
base.googleRecaptchaAPIKey = 'googleRecaptchaAPIKey' in customPreferences ? customPreferences.googleRecaptchaAPIKey : '';
base.googleRecaptchaAPISecret = 'googleRecaptchaAPISecret' in customPreferences ? customPreferences.googleRecaptchaAPISecret : '';
base.reCaptchaBotScoreThreshold = 'reCaptchaBotScoreThreshold' in customPreferences ? customPreferences.reCaptchaBotScoreThreshold : '.5';

// HBC Configs
base.omsEnterpriseCode = 'omsEnterpriseCode' in customPreferences ? customPreferences.omsEnterpriseCode : '';
base.omsEntryType = 'omsEntryType' in customPreferences ? customPreferences.omsEntryType : '';
base.omsDraftOrderFlag = 'omsDraftOrderFlag' in customPreferences ? customPreferences.omsDraftOrderFlag : '';
base.omsAllocationRuleID = 'omsAllocationRuleID' in customPreferences ? customPreferences.omsAllocationRuleID : '';
base.omsDeliveryMethod = 'omsDeliveryMethod' in customPreferences ? customPreferences.omsDeliveryMethod : '';
base.omsTokenizationCode = 'omsTokenizationCode' in customPreferences ? customPreferences.omsTokenizationCode : '';
base.omsHoldType = 'omsHoldType' in customPreferences ? customPreferences.omsHoldType : '';
base.omsHoldTypeStatus = 'omsHoldTypeStatus' in customPreferences ? customPreferences.omsHoldTypeStatus : '';
base.omsUnitOfMeasure = 'omsUnitOfMeasure' in customPreferences ? customPreferences.omsUnitOfMeasure : '';
base.omsEnteredBy = 'omsEnteredBy' in customPreferences ? customPreferences.omsEnteredBy : '';
base.omsDocumentType = 'omsDocumentType' in customPreferences ? customPreferences.omsDocumentType : '';
base.omsGenericGiftMessage = 'omsGenericGiftMessage' in customPreferences ? customPreferences.omsGenericGiftMessage : '';
base.omsCSREntryType = 'Call Center';

base.API_KEY = customPreferences.hbcAPIKey;
base.API_KEY_GW = customPreferences.hbcGWAPIKey;
base.tokenExSchema = customPreferences.tokenExSchema;

// master pass site preference configurations
base.masterpass = {
  enableMasterPass: customPreferences.enableMasterPass || false,
  masterPassJSLibrary: customPreferences.masterPassJSLibrary || '',
  masterPassButtonImagePath: customPreferences.masterPassButtonImagePath || '',
  allowedCardTypes: customPreferences.masterPassAllowedCards || '',
  suppress3Ds: customPreferences.suppress3Ds || '',
  checkoutId: JSON.parse(customPreferences.checkoutID) || {},
  consumerKey: customPreferences.consumerKey || '',
  certAlias: customPreferences.certName || '',
  allowedShippingLocations: customPreferences.allowedShippingLocations || '',
  callbackUrl: URLUtils.https('MasterPass-Checkout').toString()
};
base.allowedCountriesExpCheckout = customPreferences.allowedShippingLocations || '';
base.maxGCLimit = 'maxGCLimit' in customPreferences ? customPreferences.maxGCLimit : 2;
base.bfxInventoryReservationTime = 'bfxInventoryReservationTime' in customPreferences ? customPreferences.bfxInventoryReservationTime : 30;

base.designerCategoryID = customPreferences.designerCategoryID;

base.enableExpressCheckout = customPreferences.enableExpressCheckout || false;
base.promptShippingEnabled = customPreferences.promptShippingEnabled || false;
base.nonChanelProductTypes = customPreferences.nonChanelProductType;
base.ipaReviewAuthStatus = customPreferences.ipaReviewAuthStatus;
base.csrBrandExcluded = customPreferences.csrBrandExcluded;
base.paymentChargeType = customPreferences.paymentChargeType;
base.cscPriceoverrideReasonCode = 'cscPriceoverrideReasonCode' in customPreferences ? customPreferences.cscPriceoverrideReasonCode : '';
base.isABTestingOn = 'isABTestingOn' in customPreferences ? customPreferences.isABTestingOn : false;
base.tempLegacyLogin = 'tempLegacyLogin' in customPreferences ? customPreferences.tempLegacyLogin : 'Salesforce@2019';

base.isABTestingOn = 'isABTestingOn' in customPreferences ? customPreferences.isABTestingOn : false;
base.instructionTypeName_SITE_REFER =
  'InstructionTypeName_SITE_REFER' in customPreferences && customPreferences.InstructionTypeName_SITE_REFER
    ? customPreferences.InstructionTypeName_SITE_REFER
    : 'SITE_REFER';

base.site_referCookieName =
  'SITE_REFER_CookieName' in customPreferences && customPreferences.SITE_REFER_CookieName ? customPreferences.SITE_REFER_CookieName : 'site_refer';
base.sf_storeidCookieName =
  'SF_STOREIDCookieName' in customPreferences && customPreferences.SF_STOREIDCookieName ? customPreferences.SF_STOREIDCookieName : 'sf_storeid';
base.sf_associdCookieName =
  'SF_ASSSOCIDCookieName' in customPreferences && customPreferences.SF_ASSSOCIDCookieName ? customPreferences.SF_ASSSOCIDCookieName : 'sf_associd';
base.createUserIdPrefix = 'CreateUserIdPrefix' in customPreferences && customPreferences.CreateUserIdPrefix ? customPreferences.CreateUserIdPrefix : '000000';
base.site_refer_Salesfloor_Values =
  'SITE_REFER_Salesfloor_Values' in customPreferences && customPreferences.SITE_REFER_Salesfloor_Values ? customPreferences.SITE_REFER_Salesfloor_Values : '';
base.instructionUsage_Salesfloor_OMS =
  'InstructionUsage_Salesfloor_OMS' in customPreferences && customPreferences.InstructionUsage_Salesfloor_OMS
    ? customPreferences.InstructionUsage_Salesfloor_OMS
    : 'SalesFloor';

base.borderFreeEnabled = 'borderFreeEnabled' in customPreferences && customPreferences.borderFreeEnabled ? customPreferences.borderFreeEnabled : false;
base.bfxOrderStatusUrl = 'bfxOrderStatusUrl' in customPreferences && customPreferences.bfxOrderStatusUrl ? customPreferences.bfxOrderStatusUrl : '';
base.vertaxCanadaTaxationEnabled =
  'vertaxCanadaTaxationEnabled' in customPreferences && customPreferences.vertaxCanadaTaxationEnabled ? customPreferences.vertaxCanadaTaxationEnabled : false;

base.searchByCityRadius = 'searchByCityRadius' in customPreferences && customPreferences.searchByCityRadius ? customPreferences.searchByCityRadius : 20000;

base.hostRedirectMap = 'hostRedirectMap' in customPreferences ? JSON.parse(customPreferences.hostRedirectMap) : {};
base.hostRedirectEnabled = 'hostRedirectEnabled' in customPreferences && customPreferences.hostRedirectEnabled ? customPreferences.hostRedirectEnabled : false;

// PLP/SRP color swatch
base.swatchColorMap = 'swatchHexColorPLP' in customPreferences ? JSON.parse(customPreferences.swatchHexColorPLP) : {};

// Vetex Hashing.
base.enableVertexHasing = 'enableVertexHasing' in customPreferences && customPreferences.enableVertexHasing ? customPreferences.enableVertexHasing : false;

// eslint-disable-next-line no-prototype-builtins
base.saleCategoryID = customPreferences.hasOwnProperty('saleCategoryID') ? customPreferences.saleCategoryID : '';
// eslint-disable-next-line no-prototype-builtins
base.clearanceCategoryID = customPreferences.hasOwnProperty('clearanceCategoryID') ? customPreferences.clearanceCategoryID : '';

// SRP cat refinement level config
base.catRefinementLevel = 'catRefinementLevel' in customPreferences ? customPreferences.catRefinementLevel : '';

// Search suggestions type
base.noProductSearchSuggestion = 'noProductSearchSuggestion' in customPreferences ? customPreferences.noProductSearchSuggestion : false;

// MPA preferences
base.mpaEnabled = 'mpaEnabled' in customPreferences ? customPreferences.mpaEnabled : false;
base.mpaStartDate = 'mpaStartDate' in customPreferences ? customPreferences.mpaStartDate : null;
base.mpaEndDate = 'mpaEndDate' in customPreferences ? customPreferences.mpaEndDate : null;
base.mpaMessaging = !isTsysMode() && 'mpaMessaging' in customPreferences ? customPreferences.mpaMessaging : null;

//Shape security configuration
base.shapeSecurity = {
  tag1Script: 'shape_security_tag1' in customPreferences && customPreferences.shape_security_tag1 ? customPreferences.shape_security_tag1 : '',
  tag2Script: 'shape_security_tag2' in customPreferences && customPreferences.shape_security_tag2 ? customPreferences.shape_security_tag2 : '',
  tag1Pages: 'shape_security_tag1_pages' in customPreferences && customPreferences.shape_security_tag1_pages ? customPreferences.shape_security_tag1_pages : '',
  tag2Pages: 'shape_security_tag2_pages' in customPreferences && customPreferences.shape_security_tag2_pages ? customPreferences.shape_security_tag2_pages : '',
  categories: 'shape_security_categories' in customPreferences && customPreferences.shape_security_categories ? customPreferences.shape_security_categories : ''
};

base.basketItemLimit = 'basketItemLimit' in customPreferences ? customPreferences.basketItemLimit : 50;

//mapping of the chargeable amount versus the threshold above which the charge is applied
base.giftWrapAmount = 'giftWrapAmount' in customPreferences ? customPreferences.giftWrapAmount : 5;
base.freeGiftWrapThreshold = 'freeGiftWrapThreshold' in customPreferences ? customPreferences.freeGiftWrapThreshold : 500;

module.exports = base;
