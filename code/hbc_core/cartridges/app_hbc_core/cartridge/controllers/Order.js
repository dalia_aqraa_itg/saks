'use strict';

var server = require('server');

var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var security = require('*/cartridge/scripts/middleware/security');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var preferences = require('*/cartridge/config/preferences');
var OMSConstants = require('*/cartridge/scripts/config/OMSServiceConfig');
var Logger = require('dw/system/Logger');
var OMSLogger = Logger.getLogger('OMS', 'OMSService');
var TurnToHelper = require('*/cartridge/scripts/util/HelperUtil');
var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
var HookMgr = require('dw/system/HookMgr');
var security = require('*/cartridge/scripts/middleware/security');

server.extend(module.superModule);

server.replace('Details', consentTracking.consent, server.middleware.https, security.validateLoggedInCustomerOnOrder, function (req, res, next) {
  if (session.custom.isbot != null && session.custom.isbot) {
    res.json({
      error: [Resource.msg('error.message.login.bot', 'login', null)],
      botError: true
    });

    return next();
  }
  // Logger.debug('start OrderDetails !!!' + req.querystring.token);
  hooksHelper('app.google.recaptcha.verify', 'validateToken', req.querystring.token, require('*/cartridge/scripts/googleRecaptchaUtil').validateToken);
  // Logger.debug('end OrderDetails !!!' + result.score);

  if (session.custom.isbot) {
    res.json({
      error: [Resource.msg('error.message.login.bot', 'login', null)],
      botError: true
    });

    return next();
  }

  if (!req.querystring.detailsInclude && !(req.querystring.trackOrderPostal && req.querystring.trackOrderNumber)) {
    res.json({
      error: true
    });
  } else {
    var OrderDetailsHelper = require('*/cartridge/scripts/helpers/OrderDetailsHelper');
    var orderDetails = '';
    var errrorMsg = '';
    try {
      var orderDetailsRequest = {};
      orderDetailsRequest.BillToID = req.currentCustomer && req.currentCustomer.profile ? req.currentCustomer.profile.customerNo : '';
      orderDetailsRequest.OrderNumber = req.querystring.trackOrderNumber ? req.querystring.trackOrderNumber.trim() : '';
      orderDetailsRequest.ZipCode = req.querystring.trackOrderPostal ? req.querystring.trackOrderPostal.trim() : '';
      orderDetailsRequest.InvokedFrom = OMSConstants.OMS_ORDER_DETAILS_INVOKEDFROM;
      orderDetailsRequest.BusinessUnit = OMSConstants.BANNER_CODE;
      orderDetails = OrderDetailsHelper.getOrderDetails(orderDetailsRequest);
      if (orderDetails) {
        var breadcrumbs = [
          {
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
          },
          {
            htmlValue: Resource.msg('page.title.myaccount', 'account', null),
            url: URLUtils.url('Account-Show').toString()
          },
          {
            htmlValue: Resource.msg('label.orderhistory', 'account', null),
            url: URLUtils.url('Order-History').toString()
          }
        ];

        var exitLinkText = Resource.msg('link.orderdetails.orderhistory', 'account', null);
        var exitLinkUrl = URLUtils.https('Order-History', 'orderFilter', req.querystring.orderFilter);

        res.render('account/order/accountOrderDetails', {
          order: orderDetails,
          exitLinkText: exitLinkText,
          exitLinkUrl: exitLinkUrl,
          breadcrumbs: breadcrumbs,
          enableNarvar: preferences.enableNarvar,
          narvarReturnURL: preferences.returnURL
            ? preferences.returnURL + 'order=' + orderDetails.orderNo + '&bzip=' + orderDetails.billingAddress.zipCode + '&locale=' + req.locale.id
            : '',
          source: !req.querystring.detailsInclude ? 'track' : 'history',
          page: req.querystring.page
        });
      } else {
        if (!empty(session.custom.orderDetailsStatus)) {
          if (session.custom.orderDetailsStatus === 'SERVICE_DOWN') {
            errrorMsg = errrorMsg = Resource.msg('order.service.down.error', 'order', null);
          } else if (session.custom.orderDetailsStatus === 'NO_ORDER') {
            errrorMsg = Resource.msg('order.not.found.error', 'order', null);
          }
        }
        res.json({
          error: true,
          message: errrorMsg
        });
      }
    } catch (ex) {
      OMSLogger.error('[OMS] Error : {0} {1}', ex, ex.stack);

      if (!empty(session.custom.orderDetailsStatus)) {
        if (session.custom.orderDetailsStatus === 'SERVICE_DOWN') {
          errrorMsg = Resource.msg('order.service.down.error', 'order', null);
        } else if (session.custom.orderDetailsStatus === 'NO_ORDER') {
          errrorMsg = Resource.msg('order.not.found.error', 'order', null);
        }
      }

      res.json({
        error: true,
        message: errrorMsg
      });
    }
  }
  return next();
});

/**
 * Fetches order history from SFCC, OMS and merges them and renders response as
 * JSON.
 *
 */
server.replace('History', consentTracking.consent, server.middleware.https, function (req, res, next) {
  var customer = req.currentCustomer;
  var orderHistoryResponse = '';
  if (!customer || !customer.profile) {
    if (req.querystring.args) {
      req.session.privacyCache.set('args', req.querystring.args);
    }

    var target = req.querystring.rurl || 1;

    res.redirect(URLUtils.url('Login-Show', 'rurl', target, 'orderhistory', true));
  } else {
    var filterValues = [
      {
        displayValue: Resource.msg('orderhistory.threemonths.option', 'order', null),
        optionValue: URLUtils.url('Order-History', 'orderFilter', '3', 'filter', true).abs().toString()
      },
      {
        displayValue: Resource.msg('orderhistory.sixmonths.option', 'order', null),
        optionValue: URLUtils.url('Order-History', 'orderFilter', '6', 'filter', true).abs().toString()
      },
      {
        displayValue: Resource.msg('orderhistory.twelvemonths.option', 'order', null),
        optionValue: URLUtils.url('Order-History', 'orderFilter', '12', 'filter', true).abs().toString()
      },
      {
        displayValue: Resource.msg('orderhistory.all.option', 'order', null),
        optionValue: URLUtils.url('Order-History', 'orderFilter', 'none', 'filter', true).abs().toString()
      }
    ];
    var breadcrumbs = [
      {
        htmlValue: Resource.msg('global.home', 'common', null),
        url: URLUtils.home().toString()
      },
      {
        htmlValue: Resource.msg('page.title.myaccount', 'account', null),
        url: URLUtils.url('Account-Show').toString()
      }
    ];

    // logic to add date from and to for order based on last few month drop down selection
    var orderHistoryRequest = {};
    var currentDate = new Date();
    var currentMonth = currentDate.getMonth();
    var startMonth;
    var adjustYear = true; // a little confusing but: If we are in June, going 6 months back, we want to know about orders in December, if we are Feb going 3 months back, we want to know about orders in November

    if (req.querystring.orderFilter === '12') {
      startMonth = currentMonth;
    } else if (req.querystring.orderFilter === '6') {
      startMonth = (currentMonth + 6) % 12;
      adjustYear = currentMonth - 6 < 0;
    } else if (req.querystring.orderFilter === '3' || req.querystring.orderFilter === 'none' || !req.querystring.orderFilter) {
      startMonth = (currentMonth + 9) % 12;
      adjustYear = currentMonth - 3 < 0;
    }
    var adjustYearValue = adjustYear ? 1 : 0;

    var startDate = currentDate.getYear() - adjustYearValue + '-' + (startMonth + 1).toLocaleString() + '-' + currentDate.getDate().toLocaleString();
    // code for pagination and order by month
    orderHistoryRequest.PageInput = {
      PageNumber: Number(req.querystring.pageNumber) || 1,
      OrderHeaderKey: req.querystring.prevOrder || ''
    };

    orderHistoryRequest.FromOrderDate = startDate;

    var OrderHistoryHelper = require('*/cartridge/scripts/helpers/OrderHistoryHelper');

    // Filter by Pendings Order
    var pendingOrders = req.querystring.pendingOrders && req.querystring.pendingOrders == 'true' ? true : false;
    var pendingOrderURL = URLUtils.url('Order-History', 'filter', true).abs().toString();

    try {
      orderHistoryResponse = OrderHistoryHelper.getOrderHistory(customer, orderHistoryRequest, req.locale.id);
      if (pendingOrders) {
        orderHistoryResponse.orders = orderHistoryResponse.orders.filter(function (order) {
          return order.OrderComplete === 'N';
        });
      }
    } catch (ex) {
      OMSLogger.error('[OMS] Error : {0} {1}', ex, ex.stack);
      res.json({
        error: 'Error in processing response from Order Management'
      });
    }
    let template = 'account/order/history';
    let siteKey;
    let turntoUrl;
    if (req.querystring.filter) {
      template = 'account/order/orderList';
    } else {
      siteKey = TurnToHelper.getLocalizedSitePreferenceFromRequestLocale().turntoSiteKey;
      turntoUrl = TurnToHelper.getLocalizedSitePreferenceFromRequestLocale().domain;
    }
    res.render(template, {
      includeRecaptchaJS: true,
      orders: orderHistoryResponse.orders,
      siteKey: siteKey,
      turntoUrl: turntoUrl,
      isLastPage: orderHistoryResponse.IsLastPage,
      nextPageNumber: (req.querystring.pageNumber ? Number(req.querystring.pageNumber) : 1) + 1,
      lastOrderHeaderKey: orderHistoryResponse.lastOrderHeaderKey || '',
      filterValues: filterValues,
      pendingOrderURL: pendingOrderURL,
      orderFilter: req.querystring.orderFilter || '3',
      accountlanding: false,
      breadcrumbs: breadcrumbs,
      pageType: 'order-history',
      historyStatus: orderHistoryResponse.historyStatus,
      borderFreeEnabled: preferences.borderFreeEnabled,
      bfxOrderStatusUrl: preferences.bfxOrderStatusUrl,
      isABTestingOn: preferences.isABTestingOn
    });
  }
  next();
});

server.replace('Confirm', consentTracking.consent, server.middleware.https, csrfProtection.generateToken, function (req, res, next) {
  var reportingUrlsHelper = require('*/cartridge/scripts/reportingUrls');
  var OrderMgr = require('dw/order/OrderMgr');
  var order = OrderMgr.getOrder(req.querystring.ID);
  var OrderModel = require('*/cartridge/models/order');
  var Locale = require('dw/util/Locale');

  var order = OrderMgr.getOrder(req.querystring.ID);
  var token = req.querystring.token ? req.querystring.token : null;
  var softLoggedIn = !!(req.currentCustomer.raw.profile && !req.currentCustomer.raw.authenticated);
  // disable buyer's remorse in case of SDD
  var defaultShipmentMethod = order.defaultShipment.shippingMethod;
  var isSDDShippingApplied = defaultShipmentMethod.custom.hasOwnProperty('sddEnabled') && defaultShipmentMethod.custom.sddEnabled ? true : false;
  if (!order || !token || token !== order.orderToken || (!softLoggedIn && order.customer.ID !== req.currentCustomer.raw.ID)) {
    res.redirect(URLUtils.url('Home-Show'));

    return next();
  }
  var lastOrderID = Object.prototype.hasOwnProperty.call(req.session.raw.custom, 'orderID') ? req.session.raw.custom.orderID : null;
  if (lastOrderID === req.querystring.ID) {
    res.redirect(URLUtils.url('Home-Show'));
    return next();
  }

  var config = {
    numberOfLineItems: '*'
  };

  var currentLocale = Locale.getLocale(req.locale.id);

  var orderModel = new OrderModel(order, { config: config, countryCode: currentLocale.country, containerView: 'order' });

  res.setViewData({
    orderUUID: order.getUUID(),
    customerName: order.customerName,
    customerEmail: order.customerEmail,
    currency: order.currencyCode
  });

  var passwordForm;

  var reportingURLs = reportingUrlsHelper.getOrderReportingURLs(order);

  // Appending the details for shoprunner and Clear all the promotion session attributes
  // eslint-disable-next-line no-param-reassign
  delete req.session.raw.custom.associateTier;
  delete req.session.raw.custom.specialVendorDiscountTier;
  req.session.privacyCache.set('inAuthTransactionUUID', null);
  // eslint-disable-next-line no-param-reassign
  delete req.session.raw.custom.isHBCTenderType;
  let shopRunnerEnabled = require('*/cartridge/scripts/helpers/shopRunnerHelpers').checkSRExpressCheckoutEligibility(order);

  req.session.privacyCache.set('EDDZipCode', null);
  var ContentMgr = require('dw/content/ContentMgr');
  // content asset's content
  var apiContent = ContentMgr.getContent('order-confirm-next-steps');
  var orderNextStepContent = null;
  if (apiContent) {
    var ContentModel = require('*/cartridge/models/content');
    orderNextStepContent = new ContentModel(apiContent, 'components/content/contentAssetInc');
    if (orderNextStepContent.body) {
      // replace delimiters in content's body with dynamic values out of order. Email and order number
      var contentBody = orderNextStepContent.body.markup;
      if (contentBody && contentBody.indexOf('xxxxx') > -1) {
        contentBody = contentBody.replace('xxxxxxxx', orderModel.orderNumber);
      }
      if (contentBody && contentBody.indexOf('xxxxxx@xxxx.com') > -1) {
        contentBody = contentBody.replace('xxxxxx@xxxx.com', orderModel.orderEmail);
      }
      orderNextStepContent = contentBody;
    }
  }

  if (!req.currentCustomer.profile && !softLoggedIn) {
    passwordForm = server.forms.getForm('newPasswords');
    passwordForm.clear();
    res.render('checkout/confirmation/confirmation', {
      order: orderModel,
      returningCustomer: false,
      passwordForm: passwordForm,
      reportingURLs: reportingURLs,
      orderUUID: order.getUUID(),
      shopRunnerEnabled: shopRunnerEnabled,
      orderNextStepContent: orderNextStepContent,
      passwordRegex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#_?!@$%^()+=~`}{|&*-])^[^'<>/]{8,}$",
      customer: req.currentCustomer.raw,
      orderCustID: order.customerNo,
      isSDDShippingApplied: isSDDShippingApplied
    });
  } else {
    res.render('checkout/confirmation/confirmation', {
      order: orderModel,
      returningCustomer: true,
      reportingURLs: reportingURLs,
      orderUUID: order.getUUID(),
      shopRunnerEnabled: shopRunnerEnabled,
      orderNextStepContent: orderNextStepContent,
      passwordRegex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#_?!@$%^()+=~`}{|&*-])^[^'<>/]{8,}$",
      customer: req.currentCustomer.raw,
      orderCustID: order.customerNo,
      isSDDShippingApplied: isSDDShippingApplied
    });
  }
  req.session.raw.custom.orderID = req.querystring.ID; // eslint-disable-line no-param-reassign
  return next();
});

server.get('Status', consentTracking.consent, server.middleware.https, csrfProtection.generateToken, function (req, res, next) {
  var ContentMgr = require('dw/content/ContentMgr');
  var target = req.querystring.rurl || 1;

  if (req.currentCustomer.profile) {
    res.redirect(URLUtils.url('Order-History'));
  }
  var rememberMe = false;
  var userName = '';
  var actionUrl = URLUtils.url('Account-Login', 'rurl', target);
  var createAccountUrl = URLUtils.url('Account-SubmitRegistration', 'rurl', target).relative().toString();
  var navTabValue = req.querystring.action;

  if (req.currentCustomer.credentials) {
    rememberMe = true;
    userName = req.currentCustomer.credentials.username;
  }

  var breadcrumbs = [
    {
      htmlValue: Resource.msg('global.home', 'common', null),
      url: URLUtils.home().toString()
    }
  ];

  let siteKey = TurnToHelper.getLocalizedSitePreferenceFromRequestLocale().turntoSiteKey;
  let turntoUrl = TurnToHelper.getLocalizedSitePreferenceFromRequestLocale().domain;
  var borderFreeStatusMsg = ContentMgr.getContent('checkorder-international');
  var internationalMessage = ContentMgr.getContent('checkorder-international-message');
  var profileForm = server.forms.getForm('profile');
  profileForm.clear();
  res.render('account/order/orderStatus', {
    navTabValue: navTabValue || 'login',
    siteKey: siteKey,
    turntoUrl: turntoUrl,
    rememberMe: rememberMe,
    userName: userName,
    actionUrl: actionUrl,
    profileForm: profileForm,
    breadcrumbs: breadcrumbs,
    oAuthReentryEndpoint: 1,
    createAccountUrl: createAccountUrl,
    includeRecaptchaJS: true,
    borderFreeStatusMsg: borderFreeStatusMsg ? borderFreeStatusMsg.custom.body : null,
    internationalMessage: internationalMessage && internationalMessage.custom.body ? internationalMessage.custom.body.markup : null
  });
  return next();
});

server.replace(
  'CreateAccount',
  server.middleware.https,
  csrfProtection.validateAjaxRequest,
  security.validateLoggedInCustomerOnOrder,
  function (req, res, next) {
    var OrderMgr = require('dw/order/OrderMgr');
    var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');

    var formErrors = require('*/cartridge/scripts/formErrors');

    var passwordForm = server.forms.getForm('newPasswords');
    var newPassword = passwordForm.newpassword.htmlValue;
    var confirmPassword = passwordForm.newpasswordconfirm.htmlValue;
    if (newPassword !== confirmPassword) {
      passwordForm.valid = false;
      passwordForm.newpasswordconfirm.valid = false;
      passwordForm.newpasswordconfirm.error = Resource.msg('error.message.mismatch.newpassword', 'forms', null);
    }

    // changes from prepend method of plugin_wishlist cartridge
    var list = productListHelper.getList(req.currentCustomer.raw, { type: 10 });
    res.setViewData({
      list: list
    });

    var order = OrderMgr.getOrder(req.querystring.ID);
    /**
     * Security patch. Check order UUID and bail out if mismatch
     */
    if (!order || order.customer.ID !== req.currentCustomer.raw.ID || order.getUUID() !== req.querystring.UUID) {
      res.json({ error: [Resource.msg('error.message.unable.to.create.account', 'login', null)] });
      return next();
    }
    res.setViewData({ orderID: req.querystring.ID });

    var registrationObj = {
      firstName: order.billingAddress.firstName,
      lastName: order.billingAddress.lastName,
      phone: order.billingAddress.phone,
      email: order.customerEmail,
      password: newPassword
    };

    var creditCardPaymentDetails = COHelpers.getOrderCreditCardPaymentDetails(order);
    if (creditCardPaymentDetails) {
      registrationObj.payment = creditCardPaymentDetails;
    }

    if (passwordForm.valid) {
      res.setViewData(registrationObj);

      this.on('route:BeforeComplete', function (req, res) {
        // eslint-disable-line no-shadow
        var CustomerMgr = require('dw/customer/CustomerMgr');
        var Transaction = require('dw/system/Transaction');
        var addressHelpers = require('*/cartridge/scripts/helpers/addressHelpers');

        var registrationData = res.getViewData();

        var login = registrationData.email;
        var password = registrationData.password;
        var newCustomer;
        var authenticatedCustomer;
        var newCustomerProfile;
        var errorObj = {};

        delete registrationData.email;
        delete registrationData.password;

        // attempt to create a new user and log that user in.
        try {
          Transaction.wrap(function () {
            var error = {};
            var profilePassword;

            if (preferences.isABTestingOn) {
              profilePassword = preferences.tempLegacyLogin;
            } else {
              profilePassword = password;
            }
            newCustomer = CustomerMgr.createCustomer(login, profilePassword);

            var authenticateCustomerResult = CustomerMgr.authenticateCustomer(login, profilePassword);
            if (authenticateCustomerResult.status !== 'AUTH_OK') {
              error = { authError: true, status: authenticateCustomerResult.status };
              throw error;
            }

            authenticatedCustomer = CustomerMgr.loginCustomer(authenticateCustomerResult, false);

            if (!authenticatedCustomer) {
              error = { authError: true, status: authenticateCustomerResult.status };
              throw error;
            } else {
              // assign values to the profile
              newCustomerProfile = newCustomer.getProfile();

              newCustomerProfile.firstName = registrationData.firstName;
              newCustomerProfile.lastName = registrationData.lastName;
              newCustomerProfile.phoneHome = registrationData.phone;
              newCustomerProfile.email = login;
              if (preferences.isABTestingOn) {
                var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
                var newPswd = legacyCustomerLogin.getPasswordLegacyHashed(password);
                newCustomerProfile.custom.legacyPasswordHash = newPswd;
              }

              order.setCustomer(newCustomer);

              // save all used shipping addresses to address book of the logged in customer
              var allAddresses = addressHelpers.gatherShippingAddresses(order);
              allAddresses.forEach(function (address) {
                addressHelpers.saveAddress(address, { raw: newCustomer }, addressHelpers.generateAddressName(address));
              });
              // Save the Credit Card used to place the order.
              if (registrationData.payment) {
                COHelpers.saveOrderPaymentToProfile(newCustomerProfile, registrationData.payment);
              }
              // Maybe enroll in loyalty here
              if (HookMgr.hasHook('app.customer.hbc.loyalty.enroll')) {
                var loyaltyEnrollForm = server.forms.getForm('loyaltyEnrollCheckoutConfirm');

                if (loyaltyEnrollForm && loyaltyEnrollForm.enroll.htmlValue) {
                  // Call Enroll API if Enroll checkbox is true
                  var loyaltyResponse = hooksHelper('app.customer.hbc.loyalty.enroll', 'expressEnroll', [newCustomerProfile, order.billingAddress.postalCode]);

                  // Assign loyalty_id to the customer profile
                  if (loyaltyResponse && loyaltyResponse.success === true) {
                    newCustomerProfile.custom.hudsonReward = String(loyaltyResponse.loyalty_id); // IMPORTANT - MUST CAST TO STRING
                  }
                  if (loyaltyResponse.errorMessage) {
                    Logger.error('Error registering customer for Loyalty: \n{0}', loyaltyResponse.errorMessage);
                  }
                }
              }
            }
          });
        } catch (e) {
          errorObj.error = true;
          errorObj.errorMessage = e.authError
            ? Resource.msg('error.message.unable.to.create.account', 'login', null)
            : Resource.msg('error.message.username.invalid', 'forms', null);
        }
        // Security Patch
        delete registrationData.firstName;
        delete registrationData.lastName;
        delete registrationData.phone;

        if (errorObj.error) {
          res.json({ error: [errorObj.errorMessage] });

          return;
        }

        // send a registration email via CNS hook code change from accounthelpers
        // account creation email via CNS
        var Locale = require('dw/util/Locale');
        var currentLanguage = Locale.getLocale(req.locale.id).getLanguage();
        hooksHelper(
          'app.customer.email.account.created',
          'sendCreateAccountEmail',
          authenticatedCustomer.profile,
          currentLanguage,
          require('*/cartridge/scripts/helpers/accountHelpers').sendCreateAccountEmail
        );

        // UCID Call to Sync
        hooksHelper('ucid.middleware.service', 'createUCIDCustomer', [authenticatedCustomer.profile, authenticatedCustomer.profile.customerNo, null]);

        // changes added from append code in plugin_wishlists
        var listLoggedIn = productListHelper.getList(newCustomer, { type: 10 });
        productListHelper.mergelists(listLoggedIn, list, req, { type: 10 });
        res.json({
          success: true,
          redirectUrl: URLUtils.url('Account-Show', 'registration', 'submitted').toString()
        });
      });
    } else {
      res.json({
        fields: formErrors.getFormErrors(passwordForm)
      });
    }

    return next();
  }
);

server.get(
  'RemorseCancel',
  consentTracking.consent,
  server.middleware.https,
  security.validateRemorseOnOrder, // /* Fix for SFSX-1424 - https://hbcdigital.atlassian.net/browse/SFSX-1424 */
  function (req, res, next) {
    if (!req.querystring.orderNumber) {
      res.json({
        error: true
      });
    } else {
      var OrderDetailsHelper = require('*/cartridge/scripts/helpers/OrderDetailsHelper');
      var orderDetails = {};
      var orderCancelRequest = {};
      orderCancelRequest.OrderNumber = req.querystring.orderNumber ? req.querystring.orderNumber.trim() : '';
      orderCancelRequest.userFullName =
        req.currentCustomer && req.currentCustomer.profile
          ? req.currentCustomer.profile.firstName + ' ' + req.currentCustomer.profile.lastName
          : OMSConstants.OMS_CANCEL_CANTACT_USER_GUEST;
      orderDetails = OrderDetailsHelper.getOrderCancelStatus(orderCancelRequest);
      if (orderDetails) {
        if (orderDetails.OrderCancelled && !orderDetails.OmsError && !orderDetails.DwError) {
          res.json({
            error: false
          });
        } else {
          res.json({
            error: true,
            type: 'oms'
          });
        }
      } else {
        res.json({
          error: true,
          type: 'sfcc'
        });
      }
    }
    next();
  }
);
module.exports = server.exports();
