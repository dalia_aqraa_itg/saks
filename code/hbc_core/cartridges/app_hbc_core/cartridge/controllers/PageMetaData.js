/**
 * Controller to generate page meta tags.
 *
 * @module  controllers/PageMetaData
 */
'use strict';

var server = require('server');

var ContentMgr = require('dw/content/ContentMgr');
var CatalogMgr = require('dw/catalog/CatalogMgr');
var ProductMgr = require('dw/catalog/ProductMgr');
var Site = require('dw/system/Site');

var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');
var cache = require('*/cartridge/scripts/middleware/cache');

server.get(
  'IncludeMetaTags',
  cache.applyDefaultCache,
  function (req, res, next) {
    var action = req.querystring.action;

    if (typeof action == 'object') {
      if (action.length > 1) {
        action = action[1].toString();
      }
    }

    var apiContent = {};
    switch (action) {
      case 'Home-Show':
        apiContent = Site.getCurrent();
        break;

      case 'Search-Show':
        if (req.querystring.cgid) {
          apiContent = CatalogMgr.getCategory(req.querystring.cgid);
        }
        break;

      case 'Product-Show':
        if (req.querystring.pid) {
          apiContent = ProductMgr.getProduct(req.querystring.pid);
        }
        break;

      case 'Page-Show':
        if (req.querystring.cid) {
          apiContent = ContentMgr.getContent(req.querystring.cid);
        }
        break;

      default:
        apiContent = ContentMgr.getContent('seo-' + action.toLowerCase()); // Account-Show seo-account-show
    }

    if (apiContent) {
      var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');
      pageMetaHelper.setPageMetaData(req.pageMetaData, apiContent);
      pageMetaHelper.setPageMetaTags(req.pageMetaData, apiContent);
    }
    res.render('seo/pageMetaData', {});
    next();
  },
  pageMetaData.computedPageMetaData
);

module.exports = server.exports();
