'use strict';

var server = require('server');
server.extend(module.superModule);

server.get('AjaxAccountFail', function (req, res, next) {
  var URLUtils = require('dw/web/URLUtils');
  res.setStatusCode(500);
  res.json({ csrfError: true, redirectUrl: URLUtils.url('Account-Show').toString() });
  next();
});

server.append('AjaxFail', function (req, res, next) {
  var URLUtils = require('dw/web/URLUtils');
  var Resource = require('dw/web/Resource');
  var viewData = res.getViewData();
  viewData.failLoginRedirectURL = URLUtils.url('Login-Show','errorMessage',Resource.msg('error.message.session.time.out', 'login', null)).toString();
  res.setViewData(viewData);
  next();
});

module.exports = server.exports();
