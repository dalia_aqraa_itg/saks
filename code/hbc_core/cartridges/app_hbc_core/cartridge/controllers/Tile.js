'use strict';

var server = require('server');

server.extend(module.superModule);

var cache = require('*/cartridge/scripts/middleware/cache');
var preferences = require('*/cartridge/config/preferences');

server.append('Show', cache.applyFourHoursPromotionSensitiveCache, function (req, res, next) {
  var URLUtils = require('dw/web/URLUtils');
  var ProductMgr = require('dw/catalog/ProductMgr');
  var ImageModel = require('*/cartridge/models/product/productImages');
  var viewData = res.getViewData();
  var product = viewData.product;

  var addToCartUrl = URLUtils.url('Cart-AddProduct');
  var showPromoMessage = false;
  if (req.querystring.isGift && req.querystring.isGift === 'true') {
    res.render('product/gridGiftTile.isml');
  }
  // Attribute to hide the Promo Message for the produc Recommendations on the pdp page
  if (!empty(req.querystring.hidePromoMsg) && req.querystring.hidePromoMsg === 'false') {
    showPromoMessage = true;
  }
  // tile image is fetched from first set product if ProductSet has no image configured
  try {
    var setProducts;
    if (product && product.productType === 'set' && (!product.images || !product.images.medium)) {
      setProducts = ProductMgr.getProduct(product.id);
      product.images = new ImageModel(setProducts, {
        types: ['medium'],
        quantity: 'all'
      });
      if (
        product.images.medium &&
        product.images.medium.length &&
        product.images.medium[0].url &&
        product.images.medium[0].url.indexOf('ImageNotAvailable') > -1 &&
        setProducts.productSetProducts.length > 0
      ) {
        product.images = new ImageModel(setProducts.productSetProducts[0], {
          types: ['medium'],
          quantity: 'all'
        });
      }
    } else if (
      product &&
      product.productType === 'set' &&
      product.images.medium &&
      product.images.medium.length &&
      product.images.medium[0].url &&
      product.images.medium[0].url.indexOf('ImageNotAvailable') > -1
    ) {
      setProducts = ProductMgr.getProduct(product.id);
      if (setProducts.productSetProducts.length > 0) {
        product.images = new ImageModel(setProducts.productSetProducts[0], {
          types: ['medium'],
          quantity: 'all'
        });
      }
    }
  } catch (e) {
    var Logger = require('dw/system/Logger');
    Logger.error('Error fetching tile image for productSet' + e);
  }

  var showInventoryMessage = true;
  if (!empty(req.querystring.hideInventory) && req.querystring.hideInventory) {
    showInventoryMessage = false;
  }
  var adobelaunchrecommendationid = '';
  if (req.querystring.adobelaunchrecommendationid || (req.querystring.pid && req.querystring.adobelaunchrecommendationposition)) {
    var recProduct = ProductMgr.getProduct(req.querystring.pid);
    if (recProduct) {
      adobelaunchrecommendationid = recProduct.master ? recProduct.ID : recProduct.variant ? recProduct.masterProduct.ID : recProduct.ID; // eslint-disable-line
    }
  }

  var isPrefSlotTile = !!(req.querystring.slotTile == 'true');

  res.setViewData({
    addToCartUrl: addToCartUrl,
    displayQuickLook: preferences.displayQuickLook ? preferences.displayQuickLook : '',
    isChanelCategory: !!req.querystring.isChanel,
    adobelaunchrecommendationposition: req.querystring.adobelaunchrecommendationposition,
    adobelaunchrecommendationid: adobelaunchrecommendationid,
    showPromoMessage: showPromoMessage,
    showInventoryMessage: showInventoryMessage,
    product: product,
    isPrefSlotTile: isPrefSlotTile
  });
  next();
});

module.exports = server.exports();
