'use strict';

var server = require('server');
server.extend(module.superModule);

var URLUtils = require('dw/web/URLUtils');
var Resource = require('dw/web/Resource');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');

/**
 * Creates a list of address model for the logged in user
 * @param {string} customerNo - customer number of the current customer
 * @returns {List} a plain list of objects of the current customer's addresses
 */
function getList(customerNo) {
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var AddressModel = require('*/cartridge/models/address');
  var collections = require('*/cartridge/scripts/util/collections');

  var customer = CustomerMgr.getCustomerByCustomerNumber(customerNo);
  var rawAddressBook = customer.addressBook.getAddresses();
  var addressBook = collections.map(rawAddressBook, function (rawAddress) {
    var addressModel = new AddressModel(rawAddress);
    addressModel.address.UUID = rawAddress.UUID;
    return addressModel;
  });
  return addressBook;
}

server.replace('List', userLoggedIn.validateLoggedIn, csrfProtection.generateToken, consentTracking.consent, function (req, res, next) {
  var actionUrls = {
    deleteActionUrl: URLUtils.url('Address-DeleteAddress').toString(),
    listActionUrl: URLUtils.url('Address-List').toString()
  };
  res.render('account/addressBook', {
    includeRecaptchaJS: true,
    addressBook: getList(req.currentCustomer.profile.customerNo),
    actionUrls: actionUrls,
    pageType: 'address'
  });
  next();
});

server.replace('AddAddress', csrfProtection.generateToken, consentTracking.consent, userLoggedIn.validateLoggedIn, function (req, res, next) {
  var Locale = require('dw/util/Locale');
  var addressHelpers = require('*/cartridge/scripts/helpers/addressHelpers');
  var addressForm = server.forms.getForm('address');
  addressForm.clear();
  // get Country And Region
  var countryRegion = addressHelpers.getCountriesAndRegions(addressForm.base);

  res.render('account/editAddAddress', {
    includeRecaptchaJS: true,
    countryCode: Locale.getLocale(req.locale.id).ISO3Country,
    country2Code: Locale.getLocale(req.locale.id).country,
    addressForm: addressForm,
    countryRegion: JSON.stringify(countryRegion),
    addressCountryCode: Locale.getLocale(req.locale.id).country,
    pageType: 'address',
    postalLabel: countryRegion.postal_label[Locale.getLocale(req.locale.id).country]
  });
  next();
});

server.replace('EditAddress', csrfProtection.generateToken, userLoggedIn.validateLoggedIn, consentTracking.consent, function (req, res, next) {
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var AddressModel = require('*/cartridge/models/address');
  var addressHelpers = require('*/cartridge/scripts/helpers/addressHelpers');
  var Locale = require('dw/util/Locale');

  var addressId = req.querystring.addressId;
  var customer = CustomerMgr.getCustomerByCustomerNumber(req.currentCustomer.profile.customerNo);
  var addressBook = customer.getProfile().getAddressBook();
  var getPreferredAddress = addressBook.getPreferredAddress();
  var rawAddress = addressBook.getAddress(addressId);
  var isDefaultAddress = false;
  if (getPreferredAddress) {
    isDefaultAddress = getPreferredAddress.ID === rawAddress.ID;
  }
  var addressModel = new AddressModel(rawAddress);
  var addressForm = server.forms.getForm('address');
  addressForm.clear();

  addressForm.copyFrom(addressModel.address);
  // get Country And Region
  var countryRegion = addressHelpers.getCountriesAndRegions(addressForm.base);

  res.render('account/editAddAddress', {
    includeRecaptchaJS: true,
    country2Code: Locale.getLocale(req.locale.id).country,
    countryCode: Locale.getLocale(req.locale.id).ISO3Country,
    addressForm: addressForm,
    addressId: addressId,
    countryRegion: JSON.stringify(countryRegion),
    addressCountryCode: addressModel.address.countryCode.value,
    stateCode: addressModel.address.stateCode,
    pageType: 'address',
    isDefaultAddress: isDefaultAddress,
    postalLabel: countryRegion.postal_label[addressModel.address.countryCode.value]
  });

  next();
});

server.replace('SaveAddress', csrfProtection.validateAjaxRequest, function (req, res, next) {
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var Transaction = require('dw/system/Transaction');
  var formErrors = require('*/cartridge/scripts/formErrors');
  var addressHelpers = require('*/cartridge/scripts/helpers/addressHelpers');
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');

  var addressForm = server.forms.getForm('address');
  var addressFormObj = addressForm.toObject();
  addressFormObj.addressForm = addressForm;
  if (req.form.UKState && req.form.UKState !== '') {
    addressFormObj.UKState = req.form.UKState;
  } else {
    addressFormObj.UKState = '';
  }
  var customer = CustomerMgr.getCustomerByCustomerNumber(req.currentCustomer.profile.customerNo);
  var addressBook = customer.getProfile().getAddressBook();
  var isDefaultAddress = false;
  var addressType = 'U';
  var isAddAddress = true;

  if (addressForm.valid) {
    res.setViewData(addressFormObj);
    var formInfo = res.getViewData();
    var isUpdateAddress = false;
    formInfo.addressFields = addressForm;
    var stateZipErrors = addressHelpers.validateStateVsZip(formInfo);
    if (Object.keys(stateZipErrors).length > 0) {
      formInfo.postalCode.valid = false;
      formInfo.addressForm.postalCode.valid = false;
      formInfo.addressForm.postalCode.error = Resource.msg('error.messagePatternmismatchPostal.required', 'error', null);
      res.json({
        success: false,
        fields: formErrors.getFormErrors(addressForm)
      });
    } else {
      this.on('route:BeforeComplete', function (req, res) {
        // eslint-disable-line no-shadow
        Transaction.wrap(function () {
          var address = null;
          if (formInfo.addressId.equals(req.querystring.addressId) || !addressBook.getAddress(formInfo.addressId)) {
            address = req.querystring.addressId ? addressBook.getAddress(req.querystring.addressId) : addressBook.createAddress(formInfo.addressId);
            isUpdateAddress = !!req.querystring.addressId;
          }

          if (address) {
            if (req.querystring.addressId) {
              address.setID(formInfo.addressId);
            }

            // Save form's address
            addressHelpers.updateAddressFields(address, formInfo);

            // Set As Default
            if (formInfo.setAsDefault) {
              addressBook.setPreferredAddress(address);
              isDefaultAddress = true;
            }

            if (!empty(addressBook.addresses)) {
              if (addressBook.addresses.length == 1) {
                isDefaultAddress = true;
              } else {
                var defaultAddress = addressBook.getPreferredAddress();
                if (defaultAddress.ID === address.ID) {
                  isDefaultAddress = true;
                }
              }
            }
            // UCID Customer Update.
            hooksHelper('ucid.middleware.service', 'updateUCIDCustomer', [
              customer.profile,
              customer.profile.customerNo,
              address,
              addressType,
              isDefaultAddress,
              isAddAddress
            ]);

            // Send account edited email via CNS hooks
            if (!req.querystring.addressId || isUpdateAddress) {
              var Locale = require('dw/util/Locale');
              var currentLanguage = Locale.getLocale(req.locale.id).getLanguage();
              hooksHelper(
                'app.customer.email.address.edited',
                'sendEditAddressEmail',
                customer.profile,
                currentLanguage,
                require('*/cartridge/scripts/helpers/accountHelpers').sendEditAddressEmail
              );
            }
            // Update the custom attribute for last modified date as the system attribute lastModified is not true profile update from a customer perspective
            accountHelpers.updateAccLastModifiedDate(req.currentCustomer.raw);

            res.json({
              success: true,
              redirectUrl: URLUtils.url('Address-List').toString()
            });
          } else {
            formInfo.addressForm.valid = false;
            formInfo.addressForm.addressId.valid = false;
            formInfo.addressForm.addressId.error = Resource.msg('error.message.idalreadyexists', 'forms', null);
            res.json({
              success: false,
              fields: formErrors.getFormErrors(addressForm)
            });
          }
        });
      });
    }
  } else {
    res.json({
      success: false,
      fields: formErrors.getFormErrors(addressForm)
    });
  }
  return next();
});

server.replace('DeleteAddress', csrfProtection.validateAjaxAccountRequest, userLoggedIn.validateLoggedInAjax, function (req, res, next) {
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var Transaction = require('dw/system/Transaction');
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  var addressType = 'U';
  var isAddAddress = false;

  var data = res.getViewData();
  if (data && !data.loggedin) {
    res.json();
    return next();
  }

  var addressId = req.querystring.addressId;
  var isDefault = req.querystring.isDefault;
  var customer = CustomerMgr.getCustomerByCustomerNumber(req.currentCustomer.profile.customerNo);
  var addressBook = customer.getProfile().getAddressBook();
  var address = addressBook.getAddress(addressId);
  var UUID = address.getUUID();
  this.on('route:BeforeComplete', function () {
    // eslint-disable-line no-shadow
    var length;
    Transaction.wrap(function () {
      addressBook.removeAddress(address);
      length = addressBook.getAddresses().length;
      if (isDefault && length > 0) {
        var newDefaultAddress = addressBook.getAddresses()[0];
        addressBook.setPreferredAddress(newDefaultAddress);
      }
    });

    hooksHelper('ucid.middleware.service', 'updateUCIDCustomer', [
      customer.profile,
      customer.profile.customerNo,
      address,
      addressType,
      isDefault,
      isAddAddress
    ]);

    var addresstHtml = accountHelpers.getAddressHtml(req.currentCustomer, getList(req.currentCustomer.profile.customerNo));

    // Update the custom attribute for last modified date as the system attribute lastModified is not true profile update from a customer perspective
    accountHelpers.updateAccLastModifiedDate(customer);

    if (length === 0) {
      res.json({
        UUID: UUID,
        defaultMsg: Resource.msg('label.addressbook.defaultaddress', 'account', null),
        message: Resource.msg('msg.no.saved.addresses', 'address', null),
        addresstHtml: addresstHtml
      });
    } else {
      res.json({
        UUID: UUID,
        defaultMsg: Resource.msg('label.addressbook.defaultaddress', 'account', null),
        addresstHtml: addresstHtml
      });
    }
  });
  return next();
});

server.replace('SetDefault', userLoggedIn.validateLoggedIn, function (req, res, next) {
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var Transaction = require('dw/system/Transaction');
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');

  var addressId = req.querystring.addressId;
  var customer = CustomerMgr.getCustomerByCustomerNumber(req.currentCustomer.profile.customerNo);
  var addressBook = customer.getProfile().getAddressBook();
  var address = addressBook.getAddress(addressId);
  var isDefaultAddress = true;
  var addressType = 'U';
  var isAddAddress = true;
  this.on('route:BeforeComplete', function () {
    // eslint-disable-line no-shadow
    Transaction.wrap(function () {
      addressBook.setPreferredAddress(address);
    });

    hooksHelper('ucid.middleware.service', 'updateUCIDCustomer', [
      customer.profile,
      customer.profile.customerNo,
      address,
      addressType,
      isDefaultAddress,
      isAddAddress
    ]);

    // Update the custom attribute for last modified date as the system attribute lastModified is not true profile update from a customer perspective
    accountHelpers.updateAccLastModifiedDate(customer);

    res.redirect(URLUtils.url('Address-List'));
  });
  next();
});

server.replace('Header', server.middleware.include, function (req, res, next) {
  if (!req.currentCustomer.profile) {
    res.render('account/header-anon', {});
  } else {
    res.render('account/header-logged', {
      name: req.currentCustomer.profile.firstName
    });
  }
  next();
});

module.exports = server.exports();
