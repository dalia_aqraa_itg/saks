'use strict';

var server = require('server');
server.extend(module.superModule);

var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var Vertex = require('int_vertex/cartridge/scripts/Vertex');
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var preferences = require('*/cartridge/config/preferences');
var RootLogger = require('dw/system/Logger').getRootLogger();
var Site = require('dw/system/Site');
var customPreferences = Site.current.preferences.custom;

server.replace('Get', server.middleware.https, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var AccountModel = require('*/cartridge/models/account');
  var OrderModel = require('*/cartridge/models/order');
  var Locale = require('dw/util/Locale');
  var Resource = require('dw/web/Resource');

  var currentBasket = BasketMgr.getCurrentBasket();
  var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
  if (usingMultiShipping === true && currentBasket.shipments.length < 2) {
    req.session.privacyCache.set('usingMultiShipping', false);
    usingMultiShipping = false;
  }

  var currentLocale = Locale.getLocale(req.locale.id);
  var allValid = COHelpers.ensureValidShipments(currentBasket);
  var basketModel = new OrderModel(currentBasket, {
    usingMultiShipping: usingMultiShipping,
    countryCode: currentLocale.country,
    containerView: 'basket'
  });

  res.json({
    order: basketModel,
    customer: new AccountModel(req.currentCustomer),
    error: !allValid,
    message: allValid ? '' : Resource.msg('error.message.shipping.addresses', 'checkout', null)
  });

  next();
});

/**
 *  Handle Ajax payment (and billing) form submit
 */
server.replace('SubmitPayment', server.middleware.https, csrfProtection.validateAjaxRequest, function (req, res, next) {
  var PaymentManager = require('dw/order/PaymentMgr');
  var HookManager = require('dw/system/HookMgr');
  var Resource = require('dw/web/Resource');
  var BasketMgr = require('dw/order/BasketMgr');

  var viewData = {};
  var paymentForm = server.forms.getForm('billing');
  // Veratax code
  delete session.custom.VertexAddressSuggestions;
  delete session.custom.VertexAddressSuggestionsError;

  // verify billing form data
  var billingFormErrors = COHelpers.validateBillingForm(paymentForm.addressFields);
  var contactInfoFormErrors = COHelpers.validateFields(paymentForm.contactInfoFields);

  var currentBasket = BasketMgr.getCurrentBasket();

  var formFieldErrors = [];
  if (Object.keys(billingFormErrors).length) {
    formFieldErrors.push(billingFormErrors);
  } else {
    viewData.address = {
      firstName: {
        value: paymentForm.addressFields.firstName.value
      },
      lastName: {
        value: paymentForm.addressFields.lastName.value
      },
      address1: {
        value: paymentForm.addressFields.address1.value
      },
      address2: {
        value: paymentForm.addressFields.address2.value
      },
      city: {
        value: paymentForm.addressFields.city.value
      },
      postalCode: {
        value: paymentForm.addressFields.postalCode.value
      },
      countryCode: {
        value: paymentForm.addressFields.country.value
      }
    };

    if (paymentForm.addressFields.country.value === 'UK') {
      viewData.address.stateCode = {
        value: paymentForm.addressFields.stateCodeUK.value
      };
    } else if (Object.prototype.hasOwnProperty.call(paymentForm.addressFields, 'states')) {
      viewData.address.stateCode = {
        value: paymentForm.addressFields.states.stateCode.value
      };
    }
  }
  // Setting the value in the billing form to update the order email and phone when the customer changes PI
  if (empty(paymentForm.contactInfoFields.email.value) || empty(paymentForm.contactInfoFields.phone.value)) {
    var defaultShipmentAddr = currentBasket.defaultShipment.shippingAddress;
    if (!empty(defaultShipmentAddr.custom.customerEmail) && !empty(defaultShipmentAddr.phone)) {
      paymentForm.contactInfoFields.email.value = defaultShipmentAddr.custom.customerEmail;
      paymentForm.contactInfoFields.phone.value = defaultShipmentAddr.phone;
    } else if (!empty(currentBasket.customerEmail) || !empty(currentBasket.billingAddress.phone)) {
      paymentForm.contactInfoFields.email.value = currentBasket.customerEmail;
      paymentForm.contactInfoFields.phone.value = currentBasket.billingAddress.phone;
    }
  }

  if (Object.keys(contactInfoFormErrors).length) {
    formFieldErrors.push(contactInfoFormErrors);
  } else {
    viewData.email = {
      value: paymentForm.contactInfoFields.email.value
    };

    viewData.phone = {
      value: paymentForm.contactInfoFields.phone.value
    };
  }

  var paymentMethodIdValue = paymentForm.paymentMethod.value;
  if (!PaymentManager.getPaymentMethod(paymentMethodIdValue).paymentProcessor) {
    throw new Error(Resource.msg('error.payment.processor.missing', 'checkout', null));
  }

  var paymentProcessor = PaymentManager.getPaymentMethod(paymentMethodIdValue).getPaymentProcessor();

  var paymentFormResult;
  if (HookManager.hasHook('app.payment.form.processor.' + paymentProcessor.ID.toLowerCase())) {
    paymentFormResult = HookManager.callHook('app.payment.form.processor.' + paymentProcessor.ID.toLowerCase(), 'processForm', req, paymentForm, viewData);
  } else {
    paymentFormResult = HookManager.callHook('app.payment.form.processor.default_form_processor', 'processForm');
  }

  if (paymentFormResult.error && paymentFormResult.fieldErrors) {
    formFieldErrors.push(paymentFormResult.fieldErrors);
  }

  if (formFieldErrors.length || paymentFormResult.serverErrors) {
    // respond with form data and errors
    res.json({
      form: paymentForm,
      fieldErrors: formFieldErrors,
      serverErrors: paymentFormResult.serverErrors ? paymentFormResult.serverErrors : [],
      error: true
    });
    return next();
  }

  res.setViewData(paymentFormResult.viewData);

  this.on('route:BeforeComplete', function (req, res) {
    // eslint-disable-line no-shadow
    var HookMgr = require('dw/system/HookMgr');
    var PaymentMgr = require('dw/order/PaymentMgr');
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var Transaction = require('dw/system/Transaction');
    var AccountModel = require('*/cartridge/models/account');
    var OrderModel = require('*/cartridge/models/order');
    var URLUtils = require('dw/web/URLUtils');
    var Locale = require('dw/util/Locale');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
    var validationHelpers = require('*/cartridge/scripts/helpers/basketValidationHelpers');
    var basketHashHelper = require('*/cartridge/scripts/helpers/basketHashHelpers');
    var isHBCCard = false;
    var addressHelpers = require('*/cartridge/scripts/helpers/addressHelpers');
    var Site = require('dw/system/Site');
    var Calendar = require('dw/util/Calendar');
    var preferences = require('*/cartridge/config/preferences');

    currentBasket = BasketMgr.getCurrentBasket();
    var validatedProducts = validationHelpers.validateProducts(currentBasket);

    var billingData = res.getViewData();

    if (!currentBasket || validatedProducts.error) {
      delete billingData.paymentInformation;

      res.json({
        error: true,
        cartError: true,
        fieldErrors: [],
        serverErrors: [],
        redirectUrl: URLUtils.url('Cart-Show').toString()
      });
      return;
    }

    var billingAddress = currentBasket.billingAddress;
    var billingForm = server.forms.getForm('billing');
    var paymentMethodID = billingData.paymentMethod.value;
    var result;
    var shipments = currentBasket.shipments;

    billingForm.creditCardFields.cardNumber.htmlValue = '';
    billingForm.creditCardFields.securityCode.htmlValue = '';

    Transaction.wrap(function () {
      if (!billingAddress) {
        billingAddress = currentBasket.createBillingAddress();
      }

      billingAddress.setFirstName(billingData.address.firstName.value);
      billingAddress.setLastName(billingData.address.lastName.value);
      billingAddress.setAddress1(billingData.address.address1.value);
      billingAddress.setAddress2(billingData.address.address2.value);
      billingAddress.setCity(billingData.address.city.value);
      billingAddress.setPostalCode(billingData.address.postalCode.value);
      if (Object.prototype.hasOwnProperty.call(billingData.address, 'stateCode')) {
        billingAddress.setStateCode(billingData.address.stateCode.value);
      }
      billingAddress.setCountryCode(billingData.address.countryCode.value);

      billingAddress.setPhone(billingData.phone.value);
      currentBasket.setCustomerEmail(billingData.email.value);

      var bopisValid = false;
      if ('personInfoMarkFor' in currentBasket.custom && currentBasket.custom.personInfoMarkFor !== null) {
        bopisValid = true;
      }

      //flag to check if order contains any non BOPIS items
      var isBopisOnly = true;
      for (shipmentItem in shipments) {
        if (!('shipmentType' in shipmentItem.custom && shipmentItem.custom.shipmentType === 'instore' && shipmentItem.shippingAddress !== null)) {
          isBopisOnly = false;
          break;
        }
      }

      for (let k = 0; k < shipments.length; k++) {
        let shipmentIndividual = shipments[k];
        if (
          'shipmentType' in shipmentIndividual.custom &&
          shipmentIndividual.custom.shipmentType === 'instore' &&
          shipmentIndividual.shippingAddress !== null
        ) {
          if (isBopisOnly && bopisValid) {
            // for BOPIS only(non mixed order), set first name and last name to pick up name
            var pickUpData = JSON.parse(currentBasket.custom.personInfoMarkFor);
            var puName = pickUpData.fullName !== null ? pickUpData.fullName : '';
            var fullNameAry = puName.split(' ');
            shipmentIndividual.shippingAddress.setFirstName(fullNameAry[0]);
            shipmentIndividual.shippingAddress.setLastName(fullNameAry[1]);
          } else {
            // for mixed BOPIS and Ship to home orders, set first name and last name from billing address
            shipmentIndividual.shippingAddress.setFirstName(billingData.address.firstName.value);
            shipmentIndividual.shippingAddress.setLastName(billingData.address.lastName.value);
          }
        }
      }
    });

    // Delete the masterpass basket attributes, if the payment is edited
    if ('masterpassTxnId' in currentBasket.custom && currentBasket.custom.masterpassTxnId) {
      Transaction.wrap(function () {
        currentBasket.custom.masterpassTxnId = null;
        currentBasket.custom.masterpassPostBackAttempted = null;
        currentBasket.custom.masterpassWalletId = null;
      });
    }

    // if there is no selected payment option and balance is greater than zero
    if (!paymentMethodID && currentBasket.totalGrossPrice.value > 0) {
      var noPaymentMethod = {};

      noPaymentMethod[billingData.paymentMethod.htmlName] = Resource.msg('error.no.selected.payment.method', 'payment', null);

      delete billingData.paymentInformation;

      res.json({
        form: billingForm,
        fieldErrors: [noPaymentMethod],
        serverErrors: [],
        error: true
      });
      return;
    }

    // Validate payment instrument if CC is used in basket
    if (paymentMethodIdValue === PaymentInstrument.METHOD_CREDIT_CARD) {
      var creditCardPaymentMethod = PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_CREDIT_CARD);
      var paymentCard = PaymentMgr.getPaymentCard(billingData.paymentInformation.cardType.value);

      var applicablePaymentCards = creditCardPaymentMethod.getApplicablePaymentCards(req.currentCustomer.raw, req.geolocation.countryCode, null);

      if (!applicablePaymentCards.contains(paymentCard)) {
        // Invalid Payment Instrument
        var invalidPaymentMethod = Resource.msg('error.payment.not.valid', 'checkout', null);
        delete billingData.paymentInformation;
        res.json({
          form: billingForm,
          fieldErrors: [],
          serverErrors: [invalidPaymentMethod],
          error: true
        });
        return;
      }

      // check if MPA payment is enabled
      var mpaEnabled = preferences.mpaEnabled;
      var mpaEnablePayment = false;

      if (preferences.mpaStartDate !== null && preferences.mpaEndDate !== null) {
        var mpaStartDate = preferences.mpaStartDate.toLocaleDateString();
        var mpaEndDate = preferences.mpaEndDate.toLocaleDateString();

        var todayDate = new Date();
        todayDate = todayDate.toLocaleDateString();
        todayDate = new Date(todayDate);
        var today = new Calendar(todayDate);

        mpaStartDate = new Date(mpaStartDate);
        var startDate = new Calendar(mpaStartDate);
        startDate.add(Calendar.HOUR, -24);

        mpaEndDate = new Date(mpaEndDate);
        var endDate = new Calendar(mpaEndDate);

        if (mpaEnabled && (today.after(startDate) || today.equals(startDate)) && (today.before(endDate) || today.equals(endDate))) {
          mpaEnablePayment = true;
        }
      }

      if (billingData.paymentInformation.cardType.value === 'MPA' && !mpaEnablePayment) {
        // Invalid MPA Payment Instrument
        var invalidPaymentMethod = Resource.msg('error.mpapayment.not.valid', 'checkout', null);
        delete billingData.paymentInformation;
        res.json({
          form: billingForm,
          fieldErrors: [],
          serverErrors: [invalidPaymentMethod],
          error: true
        });
        return;
      }
    }

    // check to make sure there is a payment processor
    if (!PaymentMgr.getPaymentMethod(paymentMethodID).paymentProcessor) {
      throw new Error(Resource.msg('error.payment.processor.missing', 'checkout', null));
    }

    var processor = PaymentMgr.getPaymentMethod(paymentMethodID).getPaymentProcessor();

    // handle payment processor
    var handlePass = true;
    if (HookMgr.hasHook('app.payment.processor.' + processor.ID.toLowerCase())) {
      result = HookMgr.callHook('app.payment.processor.' + processor.ID.toLowerCase(), 'Handle', currentBasket, billingData.paymentInformation, handlePass);
    } else {
      result = HookMgr.callHook('app.payment.processor.default', 'Handle');
    }

    // need to invalidate credit card fields
    if (result.error) {
      delete billingData.paymentInformation;

      res.json({
        form: billingForm,
        fieldErrors: result.fieldErrors,
        serverErrors: result.serverErrors,
        error: true
      });
      return;
    }

    // Set Token From token
    if (result.token) {
      billingData.paymentInformation.token = result.token;
    }

    // Double make sure that we are not saving TCC in account.
    if (billingData.paymentInformation && billingData.paymentInformation.cardType.value === 'TCC') {
      billingData.saveCard = false;
      billingData.saveCardDefault = false;
    }

    if (HookMgr.hasHook('app.payment.form.processor.' + processor.ID.toLowerCase())) {
      HookMgr.callHook('app.payment.form.processor.' + processor.ID.toLowerCase(), 'savePaymentInformation', req, currentBasket, billingData);
    } else {
      HookMgr.callHook('app.payment.form.processor.default', 'savePaymentInformation');
    }

    // Save customer address in Address book while adding a new address in billing Step
    if (req.currentCustomer.addressBook) {
      if (!addressHelpers.checkIfAddressStored(billingAddress, req.currentCustomer.addressBook.addresses)) {
        addressHelpers.saveBillingAddress(billingAddress, req.currentCustomer, addressHelpers.generateAddressName(billingAddress));
      }
    }

    // Call Associate Discount Service
    if (
      !empty(billingData.paymentInformation) &&
      !empty(billingData.paymentInformation.cardType) &&
      (billingData.paymentInformation.cardType.value === 'SAKS' ||
        billingData.paymentInformation.cardType.value === 'SAKSMC' ||
        billingData.paymentInformation.cardType.value === 'HBC' ||
        billingData.paymentInformation.cardType.value === 'HBCMC' ||
        billingData.paymentInformation.cardType.value === 'MPA' ||
        billingData.paymentInformation.cardType.value === 'TCC')
    ) {
      isHBCCard = true;
    }
    if (isHBCCard) {
      if (billingData.paymentInformation.token) {
        // Call Associate Discount Service and set the associate tier in session
        hooksHelper('ucid.middleware.service', 'associateDiscountCode', [req, billingData.paymentInformation.token]);
      }
      // eslint-disable-next-line no-param-reassign
      req.session.raw.custom.isHBCTenderType = true; // Setting the session attribute for the HBC Card dependednt promotions
    } else {
      delete req.session.raw.custom.associateTier; //eslint-disable-line
      delete req.session.raw.custom.specialVendorDiscountTier; //eslint-disable-line
      // eslint-disable-next-line no-param-reassign
      req.session.raw.custom.isHBCTenderType = false;
    }

    // Calculate the basket
    Transaction.wrap(function () {
      basketCalculationHelpers.calculateTotals(currentBasket);
    });

    // Re-calculate the payments.
    var calculatedPaymentTransaction = COHelpers.calculatePaymentTransaction(currentBasket);

    if (calculatedPaymentTransaction.error) {
      res.json({
        form: paymentForm,
        fieldErrors: [],
        serverErrors: [Resource.msg('error.technical', 'checkout', null)],
        error: true,
        orderTotalMismatch: true,
        maxGCLimitReached: result.maxGCLimitReached
      });
      return;
    }

    var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
    if (usingMultiShipping === true && currentBasket.shipments.length < 2) {
      req.session.privacyCache.set('usingMultiShipping', false);
      usingMultiShipping = false;
    }

    hooksHelper('app.customer.subscription', 'subscribeTo', [paymentForm.subscribe.checked, paymentForm.contactInfoFields.email.htmlValue], function () {});

    var currentLocale = Locale.getLocale(req.locale.id);

    var basketModel = new OrderModel(currentBasket, {
      usingMultiShipping: usingMultiShipping,
      countryCode: currentLocale.country,
      containerView: 'basket'
    });

    var accountModel = new AccountModel(req.currentCustomer);
    var renderedStoredPaymentInstrument = COHelpers.getRenderedPaymentInstruments(req, accountModel, basketModel);

    var customerSavedAddressesHtml = COHelpers.getCustomerSavedAddresses(req, accountModel, basketModel, currentLocale.country);

    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
    var orderProductSummary = renderTemplateHelper.getRenderedHtml(
      {
        order: basketModel
      },
      'checkout/orderProductSummary'
    );

    res.json({
      renderedPaymentInstruments: renderedStoredPaymentInstrument,
      customer: accountModel,
      order: basketModel,
      form: billingForm,
      orderProductSummary: orderProductSummary,
      isBasketUpdated: basketHashHelper.validateBasketHashChange(currentBasket, req),
      error: false,
      customerSavedAddressesHtml: customerSavedAddressesHtml
    });
  });

  return next();
});

server.replace('PlaceOrder', server.middleware.https, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var OrderMgr = require('dw/order/OrderMgr');
  var Resource = require('dw/web/Resource');
  var Transaction = require('dw/system/Transaction');
  var URLUtils = require('dw/web/URLUtils');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  var validationHelpers = require('*/cartridge/scripts/helpers/basketValidationHelpers');
  var addressHelpers = require('*/cartridge/scripts/helpers/addressHelpers');
  var Logger = require('dw/system/Logger');
  var OrderAPIUtils = require('*/cartridge/scripts/util/OrderAPIUtil');
  var Order = require('dw/order/Order');

  var currentBasket = BasketMgr.getCurrentBasket();

  // Accertify InAuth logic
  var inAuthTransactionID = request.httpParameterMap.inAuthTID.stringValue;
  var HBCUtils = require('*/cartridge/scripts/HBCUtils');
  HBCUtils.setTransactionUUID(req, inAuthTransactionID);

  if (!currentBasket) {
    res.json({
      error: true,
      cartError: true,
      fieldErrors: [],
      serverErrors: [],
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }

  var validatedProducts = validationHelpers.validateProducts(currentBasket);

  var Locale = require('dw/util/Locale');
  var currentLocale = Locale.getLocale(req.locale.id);

  let shopRunnerEnabled = require('*/cartridge/scripts/helpers/shopRunnerHelpers').checkSRExpressCheckoutEligibility(currentBasket);

  if (shopRunnerEnabled) {
    var shoprunnerShippingMethodSelection = require('*/cartridge/scripts/ShoprunnerShippingMethodSelection').ShoprunnerShippingMethodSelection;
    shoprunnerShippingMethodSelection(true);

    var ShopRunner = require('*/cartridge/scripts/ShopRunner'); // eslint-disable-line
    ShopRunner.PlaceOrderPrepend(req, res, next); // eslint-disable-line
  }

  if (!currentBasket || validatedProducts.error) {
    res.json({
      error: true,
      cartError: true,
      fieldErrors: [],
      serverErrors: [],
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }

  if (req.session.privacyCache.get('fraudDetectionStatus')) {
    res.json({
      error: true,
      cartError: true,
      redirectUrl: URLUtils.url('Error-ErrorCode', 'err', '01').toString(),
      errorMessage: Resource.msg('error.technical', 'checkout', null)
    });

    return next();
  }

  var validationOrderStatus = hooksHelper(
    'app.validate.order',
    'validateOrder',
    currentBasket,
    require('*/cartridge/scripts/hooks/validateOrder').validateOrder
  );
  if (validationOrderStatus.error) {
    res.json({
      error: true,
      errorMessage: validationOrderStatus.message
    });
    return next();
  }

  // Check to make sure there is a shipping address
  if (currentBasket.defaultShipment.shippingAddress === null) {
    res.json({
      error: true,
      errorStage: {
        stage: 'shipping',
        step: 'address'
      },
      errorMessage: Resource.msg('error.no.shipping.address', 'checkout', null)
    });
    return next();
  }

  // Check to make sure there is a shipping address and the shipping Address Country matches the locale Country code else redirect the user to shipping page
  if (currentLocale.country !== currentBasket.defaultShipment.shippingAddress.countryCode.value) {
    res.json({
      error: true,
      errorStage: {
        stage: 'shipping',
        step: 'address'
      },
      errorMessage: Resource.msg('error.shipping.address.nomatch', 'checkout', null)
    });
    return next();
  }

  // Check to make sure billing address exists
  if (!currentBasket.billingAddress) {
    res.json({
      error: true,
      errorStage: {
        stage: 'payment',
        step: 'billingAddress'
      },
      errorMessage: Resource.msg('error.no.billing.address', 'checkout', null)
    });
    return next();
  }

  // Calculate the basket
  Transaction.wrap(function () {
    basketCalculationHelpers.calculateTotals(currentBasket);
  });

  // Calculate FDD, Associate and discount values for the product line items
  COHelpers.prorateLineItemDiscounts(currentBasket, req.currentCustomer.raw);

  // Re-validates existing payment instruments
  var validPayment = COHelpers.validatePayment(req, currentBasket);
  if (validPayment.error) {
    res.json({
      error: true,
      errorStage: {
        stage: 'payment',
        step: 'paymentInstrument'
      },
      errorMessage: Resource.msg('error.payment.not.valid', 'checkout', null)
    });
    return next();
  }

  // Re-calculate the payments.
  var calculatedPaymentTransactionTotal = COHelpers.calculatePaymentTransaction(currentBasket);
  if (calculatedPaymentTransactionTotal.error) {
    res.json({
      error: true,
      errorStage: {
        stage: 'payment',
        step: 'paymentInstrument'
      },
      errorMessage: Resource.msg('error.payment.not.valid', 'checkout', null)
    });
    return next();
  }

  if (Logger.isWarnEnabled() && currentBasket) {
    // Extra logging for SFDEV-10229
    var collections = require('*/cartridge/scripts/util/collections');
    collections.forEach(currentBasket.getAllProductLineItems(), function (pli) {
    });
  }

  // Creates a new order.
  var order = COHelpers.createOrder(currentBasket);
  if (!order) {
    res.json({
      error: true,
      errorMessage: Resource.msg('error.technical', 'checkout', null)
    });
    return next();
  }
  // retrieve commission vendor cookies and store the values in order.
  // var preferences = dw.system.Site.current.preferences.custom;
  var site_referCookieName = preferences.site_referCookieName;
  var sf_storeidCookieName = preferences.sf_storeidCookieName;
  var sf_associdCookieName = preferences.sf_associdCookieName;
  var CommissionCookiesHelper = require('*/cartridge/scripts/helpers/commissionCookies');
  var commissionCookies = CommissionCookiesHelper.getCommissionCookies(request, [site_referCookieName, sf_storeidCookieName, sf_associdCookieName]);
  if (commissionCookies && commissionCookies.length > 0) {
    if (
      commissionCookies.get(site_referCookieName) &&
      commissionCookies.get(site_referCookieName) !== '' &&
      commissionCookies.get(sf_storeidCookieName) &&
      commissionCookies.get(sf_associdCookieName)
    ) {
      var createUserIdPrefix = preferences.createUserIdPrefix;
      Transaction.wrap(function () {
        order.custom.site_refer = commissionCookies.get(site_referCookieName).value;
        order.custom.createUserid = createUserIdPrefix + commissionCookies.get(sf_storeidCookieName).value + commissionCookies.get(sf_associdCookieName).value;
      });
    }
  }

  // Call inventory reservation service.
  var reserveInventoryResult = hooksHelper('app.inventory.reservation', 'reserveInventory', [order]);
  // Fail the order and throw user to previous checkout step, only if service is up and any of the items doesn't have sufficient inventory in OMS.
  if (!reserveInventoryResult.hasInventory) {
    Transaction.wrap(function () {
      order.trackOrderChange('Failed to reserve inenventory');
      OrderMgr.failOrder(order);
    });
    if (reserveInventoryResult.omsInventory && reserveInventoryResult.omsInventory.length > 0) {
      var omsInventory = JSON.stringify(reserveInventoryResult.omsInventory);
      session.custom.omsInventory = omsInventory;
      var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
      cartHelper.adjustBasketQuantities(currentBasket, reserveInventoryResult.omsInventory);
    }
    res.json({
      error: true,
      errorMessage: Resource.msg('error.technical', 'checkout', null)
    });
    return next();
  }
  // Handles payment authorization
  var handlePaymentResult = COHelpers.handlePayments(order, order.orderNo, req);
  if (handlePaymentResult.error) {
    // Call cancel reservation.
    hooksHelper('app.inventory.reservation', 'cancelInventoryReservation', [order.custom.reservationID]);

    res.json({
      error: true,
      errorMessage: Resource.msg('error.payment.failure', 'checkout', null)
    });
    return next();
  }

  // Vertax Specific code
  Vertex.CalculateTax('Invoice', order); // eslint-disable-line

  var fraudDetectionStatus = hooksHelper(
    'app.fraud.detection',
    'fraudDetection',
    currentBasket,
    require('*/cartridge/scripts/hooks/fraudDetection').fraudDetection
  );
  if (fraudDetectionStatus.status === 'fail') {
    Transaction.wrap(function () {
      order.trackOrderChange('Failed due to fraud');
      OrderMgr.failOrder(order);
    });

    // fraud detection failed
    req.session.privacyCache.set('fraudDetectionStatus', true);

    res.json({
      error: true,
      cartError: true,
      redirectUrl: URLUtils.url('Error-ErrorCode', 'err', fraudDetectionStatus.errorCode).toString(),
      errorMessage: Resource.msg('error.technical', 'checkout', null)
    });

    return next();
  }

  // Places the order
  var placeOrderResult = COHelpers.placeOrder(order, fraudDetectionStatus);
  if (placeOrderResult.error) {
    res.json({
      error: true,
      errorMessage: Resource.msg('error.technical', 'checkout', null)
    });
    var args = {};
    args.session = req.session;
    args.Order = order;
    args.remoteAddress = req.remoteAddress;
    args.customer = req.currentCustomer.raw;
    args.deviceFingerPrintID = req.session.privacyCache.get('transactionUUID');
    // Capture the no of failure attempts executed by the customer in the same session
    var attempts = req.session.privacyCache.get('failAttempts') || 0;
    attempts += 1;
    req.session.privacyCache.set('failAttempts', attempts);

    args.failAttempts = attempts;
    // call Reversal for any technical error
    hooksHelper('app.payment.processor.ipa_credit', 'Reverse', [order.orderNo, args]);
    return next();
  }

  var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
  cartHelper.removeAllItemsFromProductList();
  // save the device finger print and fail attempts to the order object
  Transaction.wrap(function () {
    if (req.session.privacyCache.get('transactionUUID')) {
      order.custom.deviceFingerPrintID = req.session.privacyCache.get('transactionUUID');
    }
    if (req.session.privacyCache.get('failAttempts')) {
      order.custom.failAttempts = req.session.privacyCache.get('failAttempts');
    }
    // mark reversal for Cancelled orders as false
    order.custom.reversalForCancelOrder = false;
  });

  // Update EDD Date
  var shippingHelpers = require('*/cartridge/scripts/checkout/shippingHelpers');
  // Delete the EDD Save Zip code
  delete session.privacy.EDDZipCode;
  var orderShipments = order.shipments;
  Transaction.wrap(function () {
    for (var i = 0; i < orderShipments.length; i++) {
      var thisShipment = orderShipments[i];
      var EDDMap;
      if ('EDDResponse' in thisShipment.custom && !empty(thisShipment.custom.EDDResponse)) {
        EDDMap = shippingHelpers.getEDDDateMap(JSON.parse(thisShipment.custom.EDDResponse));
      }
      var orderEstimateddate = shippingHelpers.getEDDdate(thisShipment.shippingMethod, EDDMap);
      if (orderEstimateddate.EstimatedDate) {
        thisShipment.custom.selectedEstimatedDate = orderEstimateddate.EstimatedDate;
      }
      if (orderEstimateddate.OrdercreationEstimatedDate) {
        thisShipment.custom.selectedEstimatedDateforOMS = orderEstimateddate.OrdercreationEstimatedDate;
      }

      delete thisShipment.custom.EDDResponse;
    }
  });

  //Moving this Shoprunner section up to fix ticket SFDEV-9508 : To pass missing sr_token value in order xml file for SR orders
  //setting the order no. in response as its required by the Shoprunner to set the SRToken at order level at different scenarios
  res.setViewData({
    orderID: order.orderNo
  });

  if (shopRunnerEnabled) {
    var ShopRunner = require('*/cartridge/scripts/ShopRunner'); // eslint-disable-line
    ShopRunner.PlaceOrderAppend(req, res, next); // eslint-disable-line
  }

  Transaction.wrap(function () {
    //SFDEV-11204 | Generate OMS Create Order XML to be used in ExportOrders batch job
    order.custom.omsCreateOrderXML = OrderAPIUtils.buildRequestXML(order);
  });

  if (customPreferences.orderCreateBatchEnabled !== true) {
    try {
      var responseOrderXML = OrderAPIUtils.createOrderInOMS(order);
      // eslint-disable-next-line no-undef
      var resXML = new XML(responseOrderXML);
      Logger.debug('resXML.child(ResponseMessage) -->' + resXML.child('ResponseMessage'));

      if (resXML.child('ResponseMessage').toString() === 'Success') {
        Transaction.wrap(function () {
          order.exportStatus = Order.EXPORT_STATUS_EXPORTED;
          order.trackOrderChange('Order Export Successful');
          order.trackOrderChange(responseOrderXML);
        });
      } else {
        Transaction.wrap(function () {
          order.exportStatus = Order.EXPORT_STATUS_FAILED;
          order.trackOrderChange('Order Exported Failed');
          order.trackOrderChange(responseOrderXML);
        });
      }
    } catch (err) {
      RootLogger.fatal('Error while calling the Order Create Service for Order ' + order.orderNo + ' error message ' + err.message);
    }
  }

  // If the Order has the First Day Discount applied, call the FDDNotify Service
  var collections = require('*/cartridge/scripts/util/collections');
  var issFDDApplied = false;
  var priceAdjs = order.getPriceAdjustments();
  if (!empty(priceAdjs)) {
    collections.forEach(priceAdjs, function (priceAdj) {
      var promo = priceAdj.getPromotion();
      if (!empty(promo) && 'promotionType' in promo.custom && promo.custom.promotionType !== null && promo.custom.promotionType.value.toString() === 'FDD') {
        issFDDApplied = true;
      }
    });
  }
  // If FDD Promotion applied to this Order, call the Service
  if (issFDDApplied) {
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var paymentToken = '';
    var paymentInstruments = order.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);
    collections.forEach(paymentInstruments, function (paymentInstr) {
      paymentToken = paymentInstr.getCreditCardToken();
    });

    if (!empty(paymentToken)) {
      hooksHelper('ucid.middleware.service', 'fddNotifyService', paymentToken);
    }
  }

  // reset the fail attempts after the order is placed successful
  req.session.privacyCache.set('failAttempts', null);
  req.session.privacyCache.set('basketHashAfterCustAddressUpdate', null);

  if (req.currentCustomer.addressBook) {
    // save all used shipping addresses to address book of the logged in customer
    var allAddresses = addressHelpers.gatherShippingAddresses(order);
    allAddresses.forEach(function (address) {
      if (!addressHelpers.checkIfAddressStored(address, req.currentCustomer.addressBook.addresses)) {
        addressHelpers.saveAddress(address, req.currentCustomer, addressHelpers.generateAddressName(address));
      }
    });
  }

  // removed order confirmation email as it would be sent via OMS.

  // Reset usingMultiShip after successful Order placement
  req.session.privacyCache.set('usingMultiShipping', false);

  // This is to clear the payment form after place order. Earlier this was done during submission, beyond which we will not be able to access the security code of the card
  var paymentForm = server.forms.getForm('billing');
  delete paymentForm.creditCardFields;
  req.session.privacyCache.set('failAttempts', 0);

  delete session.privacy.skipRealTimeTaxes;
  delete session.privacy.cartStateHash;

  // TODO: Exposing a direct route to an Order, without at least encoding the orderID
  //  is a serious PII violation.  It enables looking up every customers orders, one at a
  //  time.
  res.json({
    error: false,
    orderID: order.orderNo,
    orderToken: order.orderToken,
    continueUrl: URLUtils.url('Order-Confirm', 'adjustedEarns', req.querystring.adjustedEarns).toString()
  });

  return next();
});

/**
 * Handle Ajax on submit of Instore Pickup person form
 */
server.post('SubmitInstorePickUp', server.middleware.https, csrfProtection.validateAjaxRequest, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();
  var Transaction = require('dw/system/Transaction');

  var form = server.forms.getForm('instorepickup');
  var pickUpPersonData = {};
  var skipShipping = false;

  // verify instorepickup form data
  var instorePickUpFormErrors = COHelpers.validateInstorePickUpForm(form);
  // check for errors
  if (Object.keys(instorePickUpFormErrors).length > 0) {
    res.json({
      form: form,
      fieldErrors: [instorePickUpFormErrors],
      serverErrors: [],
      error: true
    });
  } else {
    pickUpPersonData = {
      fullName: form.fullName.value,
      email: form.email.value,
      phone: form.phone.value
    };

    var billingForm = server.forms.getForm('billing');
    billingForm.contactInfoFields.email.value = form.email.value;
    billingForm.contactInfoFields.phone.value = form.phone.value;

    this.on('route:BeforeComplete', function (req, res) {
      // eslint-disable-line no-shadow
      var AccountModel = require('*/cartridge/models/account');
      var OrderModel = require('*/cartridge/models/order');
      var Locale = require('dw/util/Locale');

      var currentLocale = Locale.getLocale(req.locale.id);
      // save detail as json string
      Transaction.wrap(function () {
        currentBasket.custom.personInfoMarkFor = JSON.stringify(pickUpPersonData);
      });

      var basketModel = new OrderModel(currentBasket, {
        usingMultiShipping: false,
        countryCode: currentLocale.country,
        containerView: 'basket'
      });
      var hasInStoreItems = basketModel.items.hasInStoreItems;
      if (hasInStoreItems.length > 0 && hasInStoreItems.length === currentBasket.productLineItems.size()) {
        skipShipping = true;
      }
      res.json({
        error: false,
        order: basketModel,
        customer: new AccountModel(req.currentCustomer),
        instorepickup: form,
        skipShipping: skipShipping
      });
    });
  }

  return next();
});

/**
 * 29-digit TCC response handling
 *
 * @module controllers/TCCVerify
 */
server.get('TCCVerify', function (req, res, next) {
  var validateTCCSecurityCode = require('*/cartridge/scripts/checkout/validateTCCSecurityCode');

  var tcc = req.querystring.tcc;

  var validateSecurityCode = validateTCCSecurityCode.validateSecurityCode(tcc);

  res.json({
    validTCC: validateSecurityCode
  });

  next();
});

module.exports = server.exports();
