'use strict';

var server = require('server');
server.extend(module.superModule);
var cache = require('*/cartridge/scripts/middleware/cache');

/**
 * Renders the promodrawer.isml template.
 */
server.get('GetPromos', function (req, res, next) {
  var appliedCoupnCodes = 'appliedCoupons' in session.privacy && session.privacy.appliedCoupons ? session.privacy.appliedCoupons : null;
  res.setViewData({ appliedCoupnCodes: appliedCoupnCodes });
  res.render('components/promotray');
  next();
});

server.append('Show', function (req, res, next) {
    var cookiesHelper = require("*/cartridge/scripts/helpers/cookieHelpers");
    if (cookiesHelper.read('shopPreference') === 'men') {
        var URLUtils = require("dw/web/URLUtils");
        res.redirect(URLUtils.url('Home-ShowMens'));
    } else {
        var preferences = require('*/cartridge/config/preferences');
        req.querystring.action = 'Home-Show'; // eslint-disable-line
        res.setViewData({ promoTrayEnabled: preferences.promoTrayEnabled, action: 'Home-Show' });
        // With host only redirects, controller-action is not coming thru
        // set the action explicitly, w/o proper action downstream requests such as
        // adobe tag manager etc. which depend on proper action will not work
    }
    next();
});

server.replace('ErrorNotFound', function (req, res, next) {
  res.setStatusCode(410);
  res.render('error/notFound');
  next();
});

server.get('CSRBanner', function (req, res, next) {
  if (req.session.raw.isUserAuthenticated()) {
    var csrObj = {
      customerNo: req.currentCustomer.profile.customerNo,
      firstName: req.currentCustomer.profile.firstName,
      lastName: req.currentCustomer.profile.lastName,
      email: req.currentCustomer.profile.email
    };
    res.setViewData(csrObj);
    res.setViewData({ agentUser: true });
  }
  res.render('components/header/csrBanner');
  next();
});
/**
 * Retrieve the content asset and render the mark-up
 *
 * */
server.get('GetContent', function (req, res, next) {
  var ContentMgr = require('dw/content/ContentMgr');
  var ContentModel = require('*/cartridge/models/content');

  var apiContent = ContentMgr.getContent(req.querystring.cid);

  if (apiContent) {
    var content = new ContentModel(apiContent, 'components/content/contentAssetInc');
    if (content.template) {
      res.render(content.template, { content: content });
    }
  }
  next();
});

server.get('CA', cache.applyDefaultCache, function (req, res, next) {
  var Resource = require('dw/web/Resource');
  var URLUtils = require('dw/web/URLUtils');
  var preferences = require('*/cartridge/config/preferences');
  var messageCid = 'saksoff5th-ca-leaving';
  if (preferences.dataSubscription.BANNER && preferences.dataSubscription.BANNER === 'Saks') {
    messageCid = 'ca-landing-leaving-message';
  }

  var resource = {
    pattern: "(^[^('<>/)]+$)",
    defaultError: Resource.msg('error.message.invalid', 'error', null),
    header: Resource.msg('header', 'calanding', null),
    headerInfo: Resource.msg('headerInfo', 'calanding', null),
    firstName: Resource.msg('firstName', 'calanding', null),
    lastName: Resource.msg('lastName', 'calanding', null),
    email: Resource.msg('email', 'calanding', null),
    phone: Resource.msg('phone', 'calanding', null),
    cancelLabel: Resource.msg('cancelLabel', 'calanding', null),
    continueLabel: Resource.msg('continueLabel', 'calanding', null),
    requiredMsg: Resource.msg('error.message.required', 'error', null),
    patternmismatch: Resource.msg('error.messagePatternmismatchPostal.required', 'error', null),
    emailInvalidMsg: Resource.msg('msg.email.invalid', 'error', null),
    postalCode: Resource.msg('postalCode', 'calanding', null),
    note: Resource.msg('note', 'calanding', null),
    notetwo: Resource.msg('note.two', 'calanding', null),
    optionone: Resource.msg('option.one', 'calanding', null),
    optiontwo: Resource.msg('option.two', 'calanding', null),
    url: URLUtils.https('EmailSubscribe-CASignPost').toString(),
    shopContentUrl: URLUtils.url('Home-GetContent', 'cid', messageCid).abs().toString()
  };
  res.setViewData({ resourceMsg: resource });
  res.render('/home/caHomePage');
  next();
});

module.exports = server.exports();
