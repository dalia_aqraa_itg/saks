'use strict';
var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
var preferences = require('*/cartridge/config/preferences');
var Logger = require('dw/system/Logger');
var Resource = require('dw/web/Resource');
var server = require('server');

server.get('CheckBalanceForm', function (req, res, next) {
  var context = {
    template: 'giftcard/giftcardForm.isml'
  };

  res.setViewData(context);
  this.on('route:BeforeComplete', function (req, res) {
    // eslint-disable-line no-shadow
    var viewData = res.getViewData();
    var renderedTemplate = renderTemplateHelper.getRenderedHtml(viewData, viewData.template);

    res.json({
      renderedTemplate: renderedTemplate,
      includeRecaptchaJS: true
    });
  });

  return next();
});

server.get('FetchContentData', function (req, res, next) {
  var ContentMgr = require('dw/content/ContentMgr');
  var ContentModel = require('*/cartridge/models/content');

  var apiContent = ContentMgr.getContent(req.querystring.cid);

  if (apiContent) {
    var content = new ContentModel(apiContent, 'components/content/contentAssetInc');
    if (content.template) {
      var contentBody = {
        content: content
      };
      var renderedTemplate = renderTemplateHelper.getRenderedHtml(contentBody, content.template);
      res.json({
        renderedTemplate: renderedTemplate
      });
    }
  }
  next();
});

server.post('CheckBalance', server.middleware.https, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  var currentBasket = BasketMgr.getCurrentBasket();
  var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
  var regex = new RegExp("(^[^(\\'\\<\\>\\/)]+$)");

  if (session.custom.isbot != null && session.custom.isbot) {
    res.json({
      error: [Resource.msg('error.message.login.bot', 'login', null)],
      botError: true
    });

    return next();
  }
  //Logger.debug("start CheckBalance !!!" + req.form.token);
  var result = hooksHelper('app.google.recaptcha.verify', 'validateToken', req.form.token, require('*/cartridge/scripts/googleRecaptchaUtil').validateToken);
  //Logger.debug("start CheckBalance !!!" + result.score);

  if (session.custom.isbot) {
    res.json({
      error: [Resource.msg('error.message.login.bot', 'login', null)],
      botError: true
    });

    return next();
  }

  if (
    !req.form.checkbalance &&
    currentBasket &&
    currentBasket.getPaymentInstruments(ipaConstants.GIFT_CARD) &&
    currentBasket.getPaymentInstruments(ipaConstants.GIFT_CARD).length === preferences.maxGCLimit
  ) {
    res.json({
      success: false,
      limitReached: true,
      message: Resource.msg('error.giftcard.max.in.cart', 'giftcard', null)
    });
    return next();
  }
  var gcData = {
    gcNumber: req.form.gcNumber,
    gcPin: req.form.gcPin
  };
  if (!!gcData.gcNumber && regex.test(gcData.gcNumber) && !!gcData.gcPin && regex.test(gcData.gcPin)) {
    if (
      !req.form.checkbalance &&
      currentBasket &&
      currentBasket.getPaymentInstruments(ipaConstants.GIFT_CARD) &&
      currentBasket.getPaymentInstruments(ipaConstants.GIFT_CARD).length === preferences.maxGCLimit
    ) {
      res.json({
        success: false,
        limitReached: true,
        message: Resource.msg('error.giftcard.max.in.cart', 'giftcard', null)
      });
      return next();
    }
    //var result = hooksHelper('app.google.recaptcha.verify', 'validateToken', req.form.token, require('*/cartridge/scripts/googleRecaptchaUtil').validateToken);
    var result;
    if (!!gcData.gcNumber && !!gcData.gcPin) {
      result = hooksHelper(
        'app.payment.giftcard.balance.check',
        'gcBalanceCheck',
        [gcData.gcNumber, gcData.gcPin, ''],
        require('*/cartridge/scripts/util/ipaGCUtils').gcBalanceCheck
      );
      if (result != null && result.ok != null && !result.ok) {
        res.json({
          success: false,
          message: Resource.msg('error.message.giftcard.service', 'giftcard', null)
        });
      } else if (!result || !result.card || !result.card.number) {
        res.json({
          success: false,
          serviceError: true,
          message: Resource.msg('error.service.unable', 'giftcard', null)
        });
      } else if (result != null && !!result.response_code && Number(result.response_code) == 99) {
        res.json({
          success: false,
          message: Resource.msg('error.message.giftcard.service', 'giftcard', null)
        });
      } else if (result != null && !!result.response_code && Number(result.response_code) !== 1) {
        res.json({
          success: false,
          message: Resource.msg('error.message.giftcard.request', 'giftcard', null)
        });
      } else if (!req.form.checkout) {
        var renderedTemplate = renderTemplateHelper.getRenderedHtml(result, 'giftcard/giftcardBalance');
        res.json({
          success: true,
          renderedTemplate: renderedTemplate
        });
      } else {
        res.json({
          success: true,
          balance: result.card.funds_available
        });
      }
    } else {
      res.json({
        success: false,
        message: Resource.msg('error.message.giftcard.request', 'giftcard', null)
      });
    }
  } else {
    res.json({
      success: false,
      message: Resource.msg('error.message.giftcard.request', 'giftcard', null),
      includeRecaptchaJS: true
    });
  }
  return next();
});

server.post('AddGiftCard', server.middleware.https, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var URLUtils = require('dw/web/URLUtils');
  var Transaction = require('dw/system/Transaction');
  var OrderModel = require('*/cartridge/models/order');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
  var hasPreOrderItems = false;
  var KlarnaUtils = require('*/cartridge/scripts/klarna/klarnaUtils');

  var currentBasket = BasketMgr.getCurrentBasket();
  var gcNumber = req.form.gcNumber.trim();
  var gcPin = req.form.gcPin.trim();

  // error in case basket is not present
  if (!currentBasket) {
    res.setStatusCode(500);
    res.json({
      error: true,
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }

  var error = false;
  var errorMessage = '';
  var addCardResponse;
  var handlePass = false;
  try {
    Transaction.wrap(function () {
      addCardResponse = hooksHelper(
        'app.payment.processor.gift_card',
        'Handle',
        [currentBasket, req.form.balance, handlePass, gcNumber, gcPin],
        require('*/cartridge/scripts/hooks/payment/processor/gift_card').Handle
      );
    });
  } catch (e) {
    Logger.error('Gift Card addition error: {0} \n {1}', e.message, e.stack);
    error = true;
  }

  if (error || addCardResponse.error) {
    var errorCodes = {
      CARD_ALREADY_IN_CART: 'error.giftcard.already.in.cart',
      NO_CARD_REQUIRED: 'error.giftcard.cannot.be.applied',
      default: 'giftcard.default.error'
    };
    var errorMessageKey = addCardResponse != null ? errorCodes[addCardResponse.type] : errorCodes.default;
    errorMessage = Resource.msg(errorMessageKey, 'checkout', null);
    res.json({
      error: addCardResponse ? addCardResponse.error : error,
      errorMessage: errorMessage
    });
    return next();
  }
  Transaction.wrap(function () {
    basketCalculationHelpers.calculateTotals(currentBasket);
  });

  // check if max limit reached to disable the GC input field
  var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
  var limitReached = false;
  if (
    currentBasket &&
    currentBasket.getPaymentInstruments(ipaConstants.GIFT_CARD) &&
    currentBasket.getPaymentInstruments(ipaConstants.GIFT_CARD).length === preferences.maxGCLimit
  ) {
    limitReached = true;
  }

  var total = COHelpers.getNonGiftCardAmount(currentBasket, null);
  var amountFinished;
  if (!total.error) {
    amountFinished = !(total.remainingAmount > 0);
  }
  var basketModel = new OrderModel(currentBasket, {
    containerView: 'basket'
  });
  hasPreOrderItems = basketModel.items.hasPreOrderItems;

  // check whether Klarna should be enabled or not
  var isKlarnaEnabled = KlarnaUtils.isKlarnaApplicable(req);

  res.json({
    error: error,
    order: basketModel,
    limitReached: limitReached,
    amountFinished: amountFinished,
    hasPreOrderItems: hasPreOrderItems,
    isKlarnaEnabled: isKlarnaEnabled
  });

  return next();
});

server.post('RemoveGiftCard', server.middleware.https, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var Transaction = require('dw/system/Transaction');
  var URLUtils = require('dw/web/URLUtils');
  var OrderModel = require('*/cartridge/models/order');
  var arrayHelper = require('*/cartridge/scripts/util/array');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var KlarnaUtils = require('*/cartridge/scripts/klarna/klarnaUtils');

  var currentBasket = BasketMgr.getCurrentBasket();

  if (!currentBasket) {
    res.setStatusCode(500);
    res.json({
      error: true,
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });

    return next();
  }

  var existingPaymentInstrument;
  var paymentInstruments = currentBasket.getPaymentInstruments();

  if (currentBasket && req.form.uuid) {
    existingPaymentInstrument = arrayHelper.find(paymentInstruments, function (paymentInstrument) {
      return paymentInstrument.custom.giftCardNumber === req.form.uuid;
    });

    if (existingPaymentInstrument) {
      try {
        Transaction.wrap(function () {
          currentBasket.removePaymentInstrument(existingPaymentInstrument);
          basketCalculationHelpers.calculateTotals(currentBasket);
        });
      } catch (e) {
        Logger.error('Gift Card removal error: {0} \n {1}', e.message, e.stack);
        res.setStatusCode(500);
        res.json({
          error: true,
          errorMessage: Resource.msg('error.cannot.remove.giftcard', 'giftcard', null)
        });
        return next();
      }

      var basketModel = new OrderModel(currentBasket, {
        containerView: 'basket'
      });

      var isKlarnaEnabled = KlarnaUtils.isKlarnaApplicable(req);
      basketModel.isKlarnaEnabled = isKlarnaEnabled;

      res.json(basketModel);
      return next();
    }
  }

  res.setStatusCode(500);

  res.json({
    error: true,
    errorMessage: Resource.msg('error.cannot.remove.giftcard', 'giftcard', null)
  });

  return next();
});
module.exports = server.exports();
