'use strict';

var server = require('server');
server.extend(module.superModule);

var Resource = require('dw/web/Resource');
var welcomeemailSignUp = require('*/cartridge/scripts/helpers/welcomeemailSignUp');
var HashMap = require('dw/util/HashMap');

// Controller to handle footer signup
// eslint-disable-next-line consistent-return
server.append('Subscribe', function (req, res, next) {
  var EmailSubscribeHelper = require('*/cartridge/scripts/helpers/EmailSubscribeHelper');
  var preferences = require('*/cartridge/config/preferences');
  var hashMap = new HashMap();
  hashMap = EmailSubscribeHelper.getInitialData(req.locale.id);
  hashMap.put('sourceId', EmailSubscribeHelper.isRequestFromMobile() ? 'mobileFooter' : 'desktopFooter');
  if (preferences.dataSubscription.BANNER && preferences.dataSubscription.BANNER === 'Bay') {
    hashMap.put('theBayOptStatus', 'Y');
  } else if (preferences.dataSubscription.BANNER && preferences.dataSubscription.BANNER === 'Off5th') {
    hashMap.put('theBayOptStatus', 'N');
    hashMap.put('offFiveThOptStatus', 'Y');
  } else if (preferences.dataSubscription.BANNER && preferences.dataSubscription.BANNER === 'Saks') {
    hashMap.put('theBayOptStatus', 'N');
    hashMap.put('saksOptStatus', 'Y');
  }
  hashMap.put('canadaFlag', 'N');
  var email = req.form.emailId;
  var customer = req.currentCustomer.raw;
  // SFDEV-6109 : return error response if emailID is empty
  if (!email || res.viewData.error) {
    res.setViewData({
      error: true,
      success: false,
      msg: Resource.msg('subscribe.email.invalid', 'homePage', null)
    });
    return next();
  }
  // do not return error message if the email is already subscribed, so the return value for the below function is not needed
  EmailSubscribeHelper.createSubscription(email, hashMap, customer);
  next();
});

/**
 * Checks if the email value entered is correct format
 * @param {string} email - email string to check if valid
 * @returns {boolean} Whether email is valid
 */
function validateEmail(email) {
  var regex = /^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/;
  return regex.test(email);
}

server.post('SetCookie', function (req, res, next) {
  var preferences = require('*/cartridge/config/preferences');
  var EmailSubscribeHelper = require('*/cartridge/scripts/helpers/EmailSubscribeHelper');
  var email = req.form.emailId;
  var isValidEmailid;
  if (email) {
    isValidEmailid = validateEmail(email);

    if (!isValidEmailid) {
      res.json({
        success: false,
        error: true,
        msg: Resource.msg('subscribe.email.invalid', 'homePage', null)
      });
    } else {
      var customer = req.currentCustomer.raw;
      var hashMap = new HashMap();
      var cookieHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
      hashMap = EmailSubscribeHelper.getInitialData(req.locale.id);
      hashMap.put('sourceId', EmailSubscribeHelper.isRequestFromMobile() ? 'mobileWelcome' : 'desktopWelcome');

      if (preferences.dataSubscription.BANNER && preferences.dataSubscription.BANNER === 'Bay') {
        hashMap.put('theBayOptStatus', 'Y');
      } else if (preferences.dataSubscription.BANNER && preferences.dataSubscription.BANNER === 'Off5th') {
        hashMap.put('theBayOptStatus', 'N');
        hashMap.put('offFiveThOptStatus', 'Y');
      } else if (preferences.dataSubscription.BANNER && preferences.dataSubscription.BANNER === 'Saks') {
        hashMap.put('theBayOptStatus', 'N');
        hashMap.put('saksOptStatus', 'Y');
      }
      hashMap.put('canadaFlag', 'N');

      EmailSubscribeHelper.createSubscriptionFromWelcomeModal(email, hashMap, customer);
      cookieHelper.create('emailsignup', email);
      res.json({
        success: true
      });
    }
  } else {
    res.json({
      success: false,
      error: true,
      msg: Resource.msg('subscribe.email.invalid', 'homePage', null)
    });
  }
  next();
});

server.post('SetDeclineSubscriptionCookie', function (req, res, next) {
  var email = req.form.emailId;
  var cookieHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
  cookieHelper.create('emailsignup', email);
  res.json({
    success: true
  });
  next();
});

server.get('GetContent', function (req, res, next) {
  var ContentMgr = require('dw/content/ContentMgr');
  var ContentModel = require('*/cartridge/models/content');

  var apiContent = ContentMgr.getContent(req.querystring.cid);

  if (apiContent && apiContent.online) {
    var content = new ContentModel(apiContent, 'components/content/contentAssetInc');
    if (content.template) {
      res.render(content.template, {
        content: content
      });
    }
  } else {
    res.render('components/content/contentAssetInc', {
      content: null
    });
  }
  next();
});

server.get('Check', welcomeemailSignUp.emailsignup, function (req, res, next) {
  var ContentMgr = require('dw/content/ContentMgr');
  var content = ContentMgr.getContent('welcome-modal');
  var online = content && content.online;
  res.render('/common/welcomeemailSignUp', {
    caOnline: online
  });
  next();
});

server.post('CASignPost', function (req, res, next) {
  var params = req.form;
  var ContentMgr = require('dw/content/ContentMgr');
  var ContentModel = require('*/cartridge/models/content');
  var preferences = require('*/cartridge/config/preferences');

  var email = params.email;

  var Transaction = require('dw/system/Transaction');
  var CustomObjectMgr = require('dw/object/CustomObjectMgr');

  var apiContent = ContentMgr.getContent('ca-landing-response');
  // Create/Update  Subscription Object
  Transaction.wrap(function () {
    var subscribeCO = CustomObjectMgr.getCustomObject('Subscriptions', email);
    if (!subscribeCO) {
      subscribeCO = CustomObjectMgr.createCustomObject('Subscriptions', email);
    }
    subscribeCO.custom.sourceId = 'caLandingSignup';
    subscribeCO.custom.firstName = params.firstName;
    subscribeCO.custom.lastName = params.lastName;
    subscribeCO.custom.phone = params.phoneNumber;
    subscribeCO.custom.subscribedOrUnsubscribed = new Date();
    subscribeCO.custom.language = req.locale.id;
    subscribeCO.custom.banner = preferences.dataSubscription.BANNER;
    if (preferences.dataSubscription.BANNER && preferences.dataSubscription.BANNER === 'Off5th') {
      if (params.companyOne) {
        subscribeCO.custom.offFiveThOptStatus = 'Y';
        subscribeCO.custom.offFiveThCanadaOptStatus = 'Y';
      }
    } else if (preferences.dataSubscription.BANNER && preferences.dataSubscription.BANNER === 'Saks') {
      if (params.companyOne) {
        subscribeCO.custom.saksOptStatus = 'Y';
        subscribeCO.custom.saksCanadaOptStatus = 'Y';
      }
    }
    if (params.companyTwo) {
      subscribeCO.custom.saksFamilyOptStatus = 'Y';
    }
    subscribeCO.custom.canadaFlag = 'Y';
  });
  if (apiContent) {
    var content = new ContentModel(apiContent, 'components/content/contentAssetInc');
    if (content.template) {
      res.render(content.template, {
        content: content
      });
    }
  }
  next();
});

// Email sign up form

server.get('EmailSignUp', function (req, res, next) {
  var URLUtils = require('dw/web/URLUtils');
  var emailSignUpForm = server.forms.getForm('emailSignUp');
  var emailSignUpAction = URLUtils.url('EmailSubscribe-EmailSignUpAction').relative().toString();
  var preferences = require('*/cartridge/config/preferences');
  emailSignUpForm.clear();
  var currentYear = new Date().getFullYear();
  var birthYears = [];

  for (var j = 0; j <= 100; j++) {
    birthYears.push(currentYear - j);
  }
  res.render('/common/emailSignUp', {
    emailSignUpForm: emailSignUpForm,
    emailSignUpAction: emailSignUpAction,
    birthYears: birthYears,
    hudsonReward: preferences.hudsonReward,
    counter: 'counter' in req.session.raw.custom ? req.session.raw.custom.counter : 0,
    checkNumberURL: URLUtils.https('Checkout-CheckHudsonReward').relative().toString()
  });
  next();
});

server.post('EmailSignUpAction', function (req, res, next) {
  var EmailSubscribeHelper = require('*/cartridge/scripts/helpers/EmailSubscribeHelper');
  var emailSignUpForm = server.forms.getForm('emailSignUp');
  var preferences = require('*/cartridge/config/preferences');
  var email = emailSignUpForm.customer.email.value;
  var isValidEmailid;
  var success;
  if (email) {
    isValidEmailid = validateEmail(email);

    if (!isValidEmailid) {
      success = false;
    } else {
      var hashMap = new HashMap();
      hashMap = EmailSubscribeHelper.getInitialData(req.locale.id);
      hashMap.put('sourceId', EmailSubscribeHelper.isRequestFromMobile() ? 'mobileFooter' : 'desktopFooter');
      if (preferences.dataSubscription.BANNER && preferences.dataSubscription.BANNER === 'Bay') {
        hashMap.put('theBayOptStatus', 'Y');
      } else if (preferences.dataSubscription.BANNER && preferences.dataSubscription.BANNER === 'Off5th') {
        hashMap.put('theBayOptStatus', 'N');
        hashMap.put('offFiveThOptStatus', 'Y');
      }
      hashMap.put('canadaFlag', 'N');
      EmailSubscribeHelper.createSubscriptionFromEmailSignUp(email, hashMap, emailSignUpForm, req.form.emailsignupgender);
      success = true;
    }
  } else {
    success = false;
  }

  res.render('/common/emailSignUpSuccess', {
    success: success
  });
  next();
});

module.exports = server.exports();
