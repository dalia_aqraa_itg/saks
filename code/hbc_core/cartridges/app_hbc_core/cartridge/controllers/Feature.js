'use strict';

var server = require('server');

server.get('TSYS', function (req, res, next) {
  var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');
  res.render('/common/feature/featureFlag', {
    tsysPreview: session.custom.tsysPreview,
    tsysMode: tsysHelper.isTsysMode()
  });
  return next();
});

module.exports = server.exports();
