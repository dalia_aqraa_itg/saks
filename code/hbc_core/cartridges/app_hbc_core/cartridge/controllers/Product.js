'use strict';

var server = require('server');
var preferences = require('*/cartridge/config/preferences');
server.extend(module.superModule);
var bopisHelper = require('*/cartridge/scripts/helpers/bopisHelpers');
var cache = require('*/cartridge/scripts/middleware/cache');
var Logger = require('dw/system/Logger');
var promotionHelper = require('*/cartridge/scripts/util/promotionHelper');

server.append('Show', function (req, res, next) {
  var URLUtils = require('dw/web/URLUtils');
  var Site = require('dw/system/Site');
  var ProductMgr = require('dw/catalog/ProductMgr');
  var storeLocatorURL =
    'storeLocatorURL' in Site.current.preferences.custom && !empty(Site.current.preferences.custom.storeLocatorURL)
      ? Site.current.preferences.custom.storeLocatorURL
      : '';
  var waitListURI = URLUtils.url('Product-SaveWaitList');
  var TimeTradeModel = require('*/cartridge/models/timeTrade');
  var timeTradeJSON = new TimeTradeModel(req.locale.id);
  var quantity = req.querystring.quantity ? req.querystring.quantity : 1;
  var viewData = res.getViewData();
  if (empty(viewData.product)){
    return next();
  }
  var availabilityUrl = URLUtils.url(
    'Product-AvailabilityAjax',
    'pid',
    viewData.product.id,
    'quantity',
    quantity,
    'readyToOrder',
    viewData.product.readyToOrder
  );
  // fetch product api object to pass in the pdict for einstein recommendations
  var apiProduct = ProductMgr.getProduct(viewData.product.id);
  // Bridal page
  var searchableIfUnavailable = false;
  if (viewData.product.masterProductID !== null) {
    searchableIfUnavailable = ProductMgr.getProduct(viewData.product.masterProductID.toString()).searchableIfUnavailableFlag;
  }
  /**
   *  The Search Controller is cached  1hr and the Tile cache is 4hrs currently.
   *  Calling this on the Tile-Show, would make an expense call to PromotionMgr.activeCustomerPromotions.getProductPromotions() for almost
   *  all the requests until the initial Requests.
   *
   *  It would be a lot more performative if the Requests are reduced to 1 per category vs 1 per every product tile on all category and search page.
   *
   * This is called on Search  to reduce the number of calls from Tile-Show.
   */
  promotionHelper.setPromoCache();

  bopisHelper.setBopisViewData(req, res);

   var KlarnaUtils = require('*/cartridge/scripts/klarna/klarnaUtils');
   var isInternationalCustomer = KlarnaUtils.isInternationalCustomer(req);
   

  res.setViewData({
    isBfxInternationalCustomer: isInternationalCustomer,
    waitListURI: waitListURI,
    klarnaPaymentActive: KlarnaUtils.isKlarnaEnabled(),
    apiProduct: apiProduct,
    s7APIForPDPZoomViewer: preferences.s7APIForPDPZoomViewer,
    s7APIForPDPVideoPlayer: preferences.s7APIForPDPVideoPlayer,
    s7VideoServerURL: preferences.s7VideoServerURL,
    s7ImageHostURL: preferences.s7ImageHostURL,
    Zoom: preferences.Zoom,
    storeLocatorURL: storeLocatorURL,
    shopRunnerEnabled: preferences.shopRunnerEnabled,
    promoTrayEnabled: preferences.promoTrayEnabled,
    timeTradeJSON: timeTradeJSON,
    searchTerm: req.querystring.term,
    searchType: req.querystring.type,
    availabilityUrl: availabilityUrl,
    searchableIfUnavailable: searchableIfUnavailable
  });
  next();
});

server.post('SaveWaitList', function (req, res, next) {
  // eslint-disable-line
  var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
  var CustomObjectMgr = require('dw/object/CustomObjectMgr');
  var Transaction = require('dw/system/Transaction');
  var Resource = require('dw/web/Resource');
  var StringUtils = require('dw/util/StringUtils');
  var Calendar = require('dw/util/Calendar');
  var EmailSubscribeHelper = require('*/cartridge/scripts/helpers/EmailSubscribeHelper');
  var preferences = require('*/cartridge/config/preferences');
  var email = req.form.waitlistEmail;
  var productID = req.form.productID;
  if (email && productID) {
    try {
      Transaction.wrap(function () {
        // Query Custom Object and update it if Email and Mobile is same.
        var query = 'custom.emailId = {0} AND custom.productId = {1}';
        var waitListObj = CustomObjectMgr.queryCustomObject('WaitlistDetails', query, email, productID);
        if (waitListObj) {
          waitListObj.custom.phoneNumber = !empty(req.form.waitlistMobile) ? req.form.waitlistMobile : '';
          waitListObj.custom.date = StringUtils.formatCalendar(new Calendar(), 'MM/dd/yyyy HH:mm:ss');
          if (preferences.dataSubscription.BANNER && preferences.dataSubscription.BANNER === 'Saks') {
            waitListObj.custom.sourceId = EmailSubscribeHelper.isRequestFromMobile() ? 'mobileWaitlist' : 'desktopWaitlist';
          }
        } else {
          waitListObj = CustomObjectMgr.createCustomObject('WaitlistDetails', productHelper.generateUUID());
          if (waitListObj) {
            waitListObj.custom.productId = productID;
            waitListObj.custom.emailId = email;
            waitListObj.custom.phoneNumber = !empty(req.form.waitlistMobile) ? req.form.waitlistMobile : '';
            waitListObj.custom.date = StringUtils.formatCalendar(new Calendar(), 'MM/dd/yyyy HH:mm:ss');
            waitListObj.custom.exported = false;
            if (preferences.dataSubscription.BANNER && preferences.dataSubscription.BANNER === 'Saks') {
              waitListObj.custom.sourceId = EmailSubscribeHelper.isRequestFromMobile() ? 'mobileWaitlist' : 'desktopWaitlist';
            }
          }
        }
      });
    } catch (e) {
      res.json({
        success: false,
        msg: Resource.msg('msg.wiatlist.not.submitted', 'product', null)
      });
      return next();
    }
    res.json({
      success: true,
      msg: Resource.msg('msg.wiatlist.submitted', 'product', null)
    });
  }
  next();
});

server.get('Availability', function (req, res, next) {
  var ProductMgr = require('dw/catalog/ProductMgr');
  var decorators = require('*/cartridge/models/product/decorators/index');
  var availability = Object.create(null);

  var productId = req.querystring.pid;
  var quantity = req.querystring.quantity === 'undefined' ? undefined : req.querystring.quantity;
  var readyToOrder = req.querystring.readyToOrder === 'true';

  var apiProduct = ProductMgr.getProduct(productId);
  decorators.availability(availability, quantity, apiProduct.minOrderQuantity.value, apiProduct.availabilityModel, apiProduct);

  res.render('product/components/availability', {
    readyToOrder: readyToOrder,
    availability: availability.availability
  });
  next();
});

server.get('AvailabilityAjax', function (req, res, next) {
  var ProductMgr = require('dw/catalog/ProductMgr');
  var decorators = require('*/cartridge/models/product/decorators/index');
  var priceHelper = require('*/cartridge/scripts/helpers/pricing');
  var ProductFactory = require('*/cartridge/scripts/factories/product');
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
  var availability = Object.create(null);

  var productId = req.querystring.pid;
  var quantity = req.querystring.quantity === 'undefined' ? undefined : req.querystring.quantity;
  var readyToOrder = req.querystring.readyToOrder === 'true';

  var apiProduct = ProductMgr.getProduct(productId);
  var params = req.querystring;
  var product = ProductFactory.get(params);

  var context = {
    price: product.price
  };

  product.price.html = priceHelper.renderHtml(priceHelper.getHtmlContext(context));

  var attributeContext = {
    product: {
      attributes: product.attributes
    }
  };
  var attributeTemplate = 'product/components/attributesPre';
  product.attributesHtml = renderTemplateHelper.getRenderedHtml(attributeContext, attributeTemplate);

  var promotionsContext = {
    product: {
      promotions: product.promotions,
      displayPIPText: product.displayPIPText
    }
  };
  var promotionsTemplate = 'product/components/promotions';

  product.promotionsHtml = renderTemplateHelper.getRenderedHtml(promotionsContext, promotionsTemplate);
  decorators.availability(availability, quantity, apiProduct.minOrderQuantity.value, apiProduct.availabilityModel, apiProduct);
  res.json({
    product: product,
    readyToOrder: readyToOrder,
    availability: availability.availability,
    resources: productHelper.getResources()
  });
  next();
});

server.replace('Variation', function (req, res, next) {
  var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
  var priceHelper = require('*/cartridge/scripts/helpers/pricing');
  var ProductFactory = require('*/cartridge/scripts/factories/product');
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var collections = require('*/cartridge/scripts/util/collections');
  var ProductMgr = require('dw/catalog/ProductMgr');
  var preorderHelper = require('*/cartridge/scripts/helpers/preorderHelper');
  var HashMap = require('dw/util/HashMap');

  var params = req.querystring;
  if (params.pid && params.variables && params.variables.size) {
    /***
     * If both color and size are available, check for the the variant if it is having in-stock     inventory
     * if it is in-stock - do nothing
     * if it is out of stock and wailistable - do nothing
     * if it is out of stock and not-waitlistable - remote size from params
     */
    var tempProduct = ProductMgr.getProduct(params.pid);
    if (tempProduct) {
      if (tempProduct.isVariant()) {
        tempProduct = tempProduct.masterProduct;
      }
      var variationModel = tempProduct.variationModel;
      if (variationModel) {
        var allAttributes = variationModel.productVariationAttributes;
        var attrMap = new HashMap();
        collections.forEach(allAttributes, function (attr) {
          if (params.variables && params.variables[attr.ID] && params.variables[attr.ID].value) {
            var attrValue = params.variables[attr.ID].value;
            if (attrValue) {
              attrMap.put(attr.ID, attrValue);
            }
          }
        });
        if (attrMap && attrMap.length > 0) {
          var waitlistable = false;
          var productVariants = variationModel.getVariants(attrMap);
          if (productVariants && productVariants.length > 0) {
            Object.keys(productVariants).forEach(function (key) {
              if (productVariants[key].custom.waitlist == 'true') {
                waitlistable = true;
              }
            });
            if (!waitlistable) {
              // Calculate the Inventory and pre order status
              var inventoryRecord = productVariants[0].availabilityModel.inventoryRecord;
              if (inventoryRecord && inventoryRecord.stockLevel.value <= 0 && !preorderHelper.isPreorder(productVariants[0])) {
                // Delete Size attribute
                delete params.variables['size'];
              }
            }
          } else {
            // If Color ans Size combination doesn't exist, please remove the size.
            delete params.variables['size'];
          }
        }
      }
    }
  }
  var product = ProductFactory.get(params);

  var context = {
    price: product.price
  };

  product.price.html = priceHelper.renderHtml(priceHelper.getHtmlContext(context));

  var attributeContext = { product: { attributes: product.attributes } };
  var attributeTemplate = 'product/components/attributesPre';
  product.attributesHtml = renderTemplateHelper.getRenderedHtml(attributeContext, attributeTemplate);

  var promotionsContext = { product: { promotions: product.promotions } };
  var promotionsTemplate = 'product/components/promotions';

  product.promotionsHtml = renderTemplateHelper.getRenderedHtml(promotionsContext, promotionsTemplate);

  res.json({
    product: product,
    resources: productHelper.getResources()
  });

  next();
});

server.append('Variation', function (req, res, next) {
  var priceHelper = require('*/cartridge/scripts/helpers/pricing');
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var URLUtils = require('dw/web/URLUtils');
  var product = res.viewData.product;
  if (product) {
    var promotionsContext = {
      product: product
    };
    var promotionsTemplate = 'product/components/promotions';

    product.promotionsHtml = renderTemplateHelper.getRenderedHtml(promotionsContext, promotionsTemplate);

    var context = {
      price: product.price,
      product: product
    };
    product.price.html = priceHelper.renderHtml(priceHelper.getHtmlContext(context));
    if (!empty(product.promotionalPricing)) {
      product.promotionalPricing.priceHtml = renderTemplateHelper.getRenderedHtml(
        {
          product: product,
          locale: req.locale
        },
        'product/components/pricing/promotionPricing'
      );
    }
    bopisHelper.setBopisViewData(req, res);
    var quantityValue = req.querystring.quantity ? req.querystring.quantity : 1;
    var availabilityUrl = URLUtils.url(
      'Product-AvailabilityAjax',
      'pid',
      product.id,
      'quantity',
      quantityValue,
      'readyToOrder',
      product.readyToOrder
    ).toString();

    res.setViewData({
      product: product,
      availabilityUrl: availabilityUrl,
      availabilityPromptText: dw.web.Resource.msg('store.unitsavailable.prompt.text', 'storeLocator', null)
    });
  }

  next();
});

server.append('ShowQuickView', cache.applyPromotionSensitiveCache, function (req, res, next) {
  var URLUtils = require('dw/web/URLUtils');
  var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
  var ProductFactory = require('*/cartridge/scripts/factories/product');
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var Resource = require('dw/web/Resource');
  var ContentMgr = require('dw/content/ContentMgr');
  var priceToolTip = ContentMgr.getContent('pdp-price-tooltip');
  if (priceToolTip) {
    priceToolTip = priceToolTip.custom.body.markup;
  }
  var bayRewardsToolTip = ContentMgr.getContent('hudson-bay-rewards');
  if (bayRewardsToolTip) {
    bayRewardsToolTip = bayRewardsToolTip.custom.body.markup;
  }
  var params = req.querystring;
  var product = ProductFactory.get(params);
  var addToCartUrl = URLUtils.url('Cart-AddProduct');
  var template = product.productType === 'set' ? 'product/setQuickView.isml' : 'product/quickView.isml';
  var quantityValue = req.querystring.quantity ? req.querystring.quantity : 1;
  var availabilityUrl = URLUtils.url('Product-AvailabilityAjax', 'pid', product.id, 'quantity', quantityValue, 'readyToOrder', product.readyToOrder).toString();
  var tfcQvUrl = URLUtils.url('TrueFit-TfcFitrecQuickView', 'pid', product.id).relative().toString();
  var context = {
    availabilityUrl: availabilityUrl,
    product: product,
    addToCartUrl: addToCartUrl,
    resources: productHelper.getResources(),
    quickViewFullDetailMsg: Resource.msg('link.quickview.viewdetails', 'product', null),
    closeButtonText: Resource.msg('link.quickview.close', 'product', null),
    enterDialogMessage: Resource.msg('msg.enter.quickview', 'product', null),
    template: template,
    s7APIForPDPZoomViewer: preferences.s7APIForPDPZoomViewer,
    s7APIForPDPVideoPlayer: preferences.s7APIForPDPVideoPlayer,
    s7VideoServerURL: preferences.s7VideoServerURL,
    s7ImageHostURL: preferences.s7ImageHostURL,
    Zoom: preferences.Zoom,
    priceToolTip: priceToolTip,
    bayRewardsToolTip: bayRewardsToolTip,
    tfcQvUrl: tfcQvUrl
  };

  res.setViewData(context);
  res.setViewData({
    isQuickview: 'true'
  });

  this.on('route:BeforeComplete', function (req, res) {
    // eslint-disable-line no-shadow
    var viewData = res.getViewData();
    var renderedTemplate = renderTemplateHelper.getRenderedHtml(viewData, viewData.template);

    res.json({
      renderedTemplate: renderedTemplate,
      productUrl: URLUtils.url('Product-Show', 'pid', viewData.product.id).relative().toString()
    });
  });

  next();
});

server.append('ShowBonusProducts', function (req, res, next) {
  var duuid = req.querystring.DUUID;
  if (duuid) {
    var BasketMgr = require('dw/order/BasketMgr');
    var currentBasket = BasketMgr.getCurrentOrNewBasket();
    var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
    var collections = require('*/cartridge/scripts/util/collections');
    var bonusDiscountLineItem = collections.find(currentBasket.getBonusDiscountLineItems(), function (item) {
      return item.UUID === duuid;
    });
    var products = res.viewData.products;
    var tProducts = [];
    if (products) {
      products.forEach(function (product) {
        if (product.available && product.readyToOrder) {
          tProducts.push(product);
        }
      });
    }
    res.setViewData({
      products: tProducts
    });
    res.setViewData({
      header: cartHelper.provideBonusDiscountLineItemHeader(bonusDiscountLineItem, tProducts),
      labels: {
        selectprods: cartHelper.provideBonusDiscountLineItemtile(bonusDiscountLineItem, tProducts)
      },
      gwpTitle: bonusDiscountLineItem.promotion ? bonusDiscountLineItem.promotion.custom.gwpTitle : '',
      calloutMsg: bonusDiscountLineItem.promotion && bonusDiscountLineItem.promotion.calloutMsg ? bonusDiscountLineItem.promotion.calloutMsg.markup : null
    });
  }
  next();
});

module.exports = server.exports();
