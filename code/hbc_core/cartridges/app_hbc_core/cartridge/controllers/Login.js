'use strict';

var server = require('server');
server.extend(module.superModule);

var URLUtils = require('dw/web/URLUtils');

var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');

server.append('Show', function (req, res, next) {
  var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
  var preferences = require('*/cartridge/config/preferences');
  // Read the RememberMeForced cookies if true clear the remember me and user name
  var viewData = res.getViewData();
  var cookieValue = cookiesHelper.read('RememberMeForced');
  if (cookieValue) {
    viewData.rememberMe = false;
    viewData.userName = '';
  }
  if (req.querystring.orderhistory) {
    var target = req.querystring.rurl || 1;
    viewData.actionUrl = URLUtils.url('Account-Login', 'rurl', target, 'orderhistory', true);
  }

  if (req.querystring.myprefcenter) {
    if (req.querystring.args) {
      req.session.privacyCache.set('args', req.querystring.args);
    }
    var target = req.querystring.rurl || 1;
    viewData.actionUrl = URLUtils.url('Account-Login', 'rurl', target, 'myprefcenter', true);
  }

  if (tsysHelper.isTsysMode()) {
    if (req.querystring.apply == 'true') {
      viewData.apply = true;
      viewData.actionUrl = URLUtils.url('Account-Login', 'rurl', target, 'apply', true);
      viewData.createAccountUrl = URLUtils.url('Account-SubmitRegistration', 'rurl', target, 'apply', true);
    } else {
      viewData.apply = false;
    }
  }

  //29 Digit test
  if (req.querystring.test29 && !req.querystring.register) {
    var Logger = require('dw/system/Logger');
    Logger.info('******Prefill/Test 29 digit test mode******');

    var target = req.querystring.rurl || 1;
    viewData.actionUrl = URLUtils.url('Account-Login', 'rurl', target, 'test29', true);
  }

  if (req.querystring.register || (req.querystring.action && req.querystring.action === 'register')) {
    var test29 = req.querystring.test29 || '';
    var paUrl = req.querystring.paurl || '';
    var apply = viewData.apply || '';
    var savePreferenceType = req.querystring.savepreferencetype || '';
    viewData.register = true;
    viewData.createAccountUrl = URLUtils.url(
      'Account-SubmitRegistration',
      'test29',
      test29,
      'apply',
      apply,
      'paurl',
      paUrl,
      'savepreferencetype',
      savePreferenceType
    );
  } else {
    viewData.register = false;
  }

  res.setViewData({
    hudsonReward: preferences.hudsonReward,
    includeRecaptchaJS: true,
    errorMessage: req.querystring.errorMessage,
    counter: 'counter' in req.session.raw.custom ? req.session.raw.custom.counter : 0,
    checkNumberURL: URLUtils.https('Checkout-CheckHudsonReward').relative().toString(),
    passwordRegex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#_?!@$%^()+=~`}{|&*-])^[^'<>/]{8,}$"
  });
  next();
});

server.replace('Logout', function (req, res, next) {
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var AgentUserMgr = require('dw/customer/AgentUserMgr');
  try {
    if (req.session.isUserAuthenticated()) {
      AgentUserMgr.logoutAgentUser();
    }
  } catch (e) {
    // error handlin g
  }
  CustomerMgr.logoutCustomer(false);
  res.redirect(URLUtils.url('Home-Show'));
  next();
});

module.exports = server.exports();
