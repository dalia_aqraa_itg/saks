/* eslint-disable radix */
'use strict';

var server = require('server');
var Site = require('dw/system/Site');
var cache = require('*/cartridge/scripts/middleware/cache');

/**
 * Check if live chat is enabled or not
 *
 * @returns {boolean} - result of check for livechat
 */
function checkIfLivechatEnabled() {
  var chatStartTime =
    'liveChatStartTime' in Site.current.preferences.custom && Site.current.preferences.custom.liveChatStartTime
      ? Site.current.preferences.custom.liveChatStartTime
      : '';
  var startTime = chatStartTime.split(':');
  var startHour = parseInt(startTime[0]);
  var startMin = parseInt(startTime[1]);

  var chatEndTime =
    'liveChatEndTime' in Site.current.preferences.custom && Site.current.preferences.custom.liveChatEndTime
      ? Site.current.preferences.custom.liveChatEndTime
      : '';
  var endTime = chatEndTime.split(':');
  var endtHour = parseInt(endTime[0]);
  var endtMin = parseInt(endTime[1]);

  var currentStartTime = new Date();
  var currentEndTime = new Date();
  currentStartTime.setHours(startHour);
  currentStartTime.setMinutes(startMin);

  currentEndTime.setHours(endtHour);
  currentEndTime.setMinutes(endtMin);
  if (new Date() >= currentStartTime && new Date() <= currentEndTime) {
    return true;
  }

  return false;
}

server.get('ChatBtn', function (req, res, next) {
  var enableLiveChatButton =
    'enableLiveChatButton' in Site.current.preferences.custom && Site.current.preferences.custom.enableLiveChatButton
      ? Site.current.preferences.custom.enableLiveChatButton
      : false;
  var livechatAssetID =
    'serviceCloudLiveAgentAssetID' in Site.current.preferences.custom && Site.current.preferences.custom.serviceCloudLiveAgentAssetID
      ? Site.current.preferences.custom.serviceCloudLiveAgentAssetID
      : '';

  // check if Live Chat is allowed for current locale
  var liveChatAllowedLocales = Site.current.preferences.custom.liveChatAllowedLocales;
  var currentLocale = req.locale.id;
  var liveChatLocaleEnabled = false;

  for (var i = 0; i < liveChatAllowedLocales.length; i++) {
    if (liveChatAllowedLocales[i] === currentLocale) {
      liveChatLocaleEnabled = true;
    }
  }

  res.render('liveChat/liveChatButton', {
    enableLiveChatButton: enableLiveChatButton,
    livechatAssetID: livechatAssetID,
    name: req.currentCustomer.raw.profile ? req.currentCustomer.raw.profile.firstName : null,
    chatEnabled: checkIfLivechatEnabled(),
    liveChatLocaleEnabled: liveChatLocaleEnabled,
    isUSCutomer: request && request.geolocation && request.geolocation.countryCode == 'US'
  });
  return next();
});

/**
 * LiveChat-ContentAsset : This end point is triggered when LiveChat content assets are embedded within a rendered page
 * @name Base/LiveChat-ContentAsset
 * @function
 * @memberof Page
 * @param {middleware} - server.middleware.include
 * @param {middleware} - cache.applyDefaultCache
 */
server.get(
    'ContentAsset',
    server.middleware.include,
    cache.applyDefaultCache,
    function (req, res, next) {
        res.render('liveChat/contentAsset', {
          contentCall: getContentAssetTemplate('live-chat-call'),
          contentCallFaq: getContentAssetTemplate('live-chat-call-email-faq'),
          contentAsset: getContentAssetTemplate(req.querystring.livechatAssetID)
        })
        next();
    }
);

/**
 * Get contentAsset template from content id
 * @param {cid} - The id of the content asset to be embeded in a full page
 */
function getContentAssetTemplate(cid) {
  var ContentMgr = require('dw/content/ContentMgr');
  var Logger = require('dw/system/Logger');
  var ContentModel = require('*/cartridge/models/content');
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

  var apiContent = ContentMgr.getContent(cid);
  var template = '/components/content/offlineContent';
  var content;

  if (apiContent) {
      content = new ContentModel(apiContent, 'components/content/contentAssetInc');
      if (content && content.template) {
          var template = content.template;
      } else {
          Logger.warn('Content asset with ID {0} is offline', cid);
      }
  } else {
      Logger.warn('Content asset with ID {0} was included but not found', cid);
  }

  var template = renderTemplateHelper.getRenderedHtml({ content: content }, template);

  return template;
}

module.exports = server.exports();
