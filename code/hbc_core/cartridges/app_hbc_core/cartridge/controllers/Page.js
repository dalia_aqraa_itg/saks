'use strict';

var server = require('server');
server.extend(module.superModule);

var cache = require('*/cartridge/scripts/middleware/cache');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');
var cookiesHelper = require("*/cartridge/scripts/helpers/cookieHelpers");

server.replace('SetLocale', function (req, res, next) {
  var URLUtils = require('dw/web/URLUtils');
  var Currency = require('dw/util/Currency');
  var Site = require('dw/system/Site');
  var BasketMgr = require('dw/order/BasketMgr');
  var Transaction = require('dw/system/Transaction');
  var preferences = require('*/cartridge/config/preferences');

  var currentBasket = BasketMgr.getCurrentBasket();

  var QueryString = server.querystring;
  var currency;
  var currentSite = Site.getCurrent();
  var allowedCurrencies = currentSite.allowedCurrencies;
  var queryStringObj = new QueryString(req.querystring.queryString || '');

  if (Object.hasOwnProperty.call(queryStringObj, 'lang')) {
    delete queryStringObj.lang;
  }

  if (req.setLocale(req.querystring.code)) {
    currency = Currency.getCurrency(req.querystring.CurrencyCode);
    if (allowedCurrencies.indexOf(req.querystring.CurrencyCode) > -1 && req.querystring.CurrencyCode !== req.session.currency.currencyCode) {
      req.session.setCurrency(currency);

      if (currentBasket && currency && currentBasket.currencyCode !== currency.currencyCode) {
        Transaction.wrap(function () {
          currentBasket.updateCurrency();
        });
      }
    }
    var action = req.querystring.action || 'Home-Show';
    var redirectUrl = URLUtils.url(action).toString();
    if (Object.hasOwnProperty.call(queryStringObj, 'cgid')) {
      redirectUrl = URLUtils.url(action, 'cgid', queryStringObj.cgid).toString();
      delete queryStringObj.cgid;
    }
    var qsConnector = redirectUrl.indexOf('?') >= 0 ? '&' : '?';

    redirectUrl =
      Object.keys(queryStringObj).length === 0 ? (redirectUrl += queryStringObj.toString()) : (redirectUrl += qsConnector + queryStringObj.toString());

    var redirecthost = '';

    if (preferences.hostRedirectEnabled) {
      // get the host to redirect to based on locale
      redirecthost = preferences.hostRedirectMap[req.querystring.code];

      if (!empty(redirecthost)) {
        // Create session redirect URL for the new host
        var hostredirectUrl = URLUtils.sessionRedirect(redirecthost, URLUtils.url(redirectUrl));

        res.json({
          success: true,
          redirectUrl: hostredirectUrl.toString().split('?url=')[0] + '?url=' + encodeURIComponent('https://' + redirecthost + redirectUrl)
        });
      } else {
        res.json({
          success: true,
          redirectUrl: redirectUrl
        });
      }
    } else {
      res.json({
        success: true,
        redirectUrl: redirectUrl
      });
    }
  } else {
    res.json({ error: true });
  }
  next();
});

server.get("ForMe",function (req, res, next) {
  var catalogMgr = require("dw/catalog/CatalogMgr");
  var Categories = require("*/cartridge/models/categories");
  var searchHelper = require("*/cartridge/scripts/helpers/searchHelpers");
  var siteRootCategory = catalogMgr.getSiteCatalog().getRoot();
  var topLevelCategories = siteRootCategory.hasOnlineSubCategories()
    ? siteRootCategory.getOnlineSubCategories()
    : null;

  if (req.currentCustomer.raw.profile !== null) {
    var DesignerPreferences =
      req.currentCustomer.raw.profile.custom.DesignerPreferences;
    var CategoryPreferences =
      req.currentCustomer.raw.profile.custom.CategoryPreferences;
    var SizePreferences =
      req.currentCustomer.raw.profile.custom.SizePreferences;
  }
  var softLoggedIn = !!(
    req.currentCustomer.raw.profile && !req.currentCustomer.raw.authenticated
  );
  var noPreferences =
    DesignerPreferences === null &&
    CategoryPreferences === null &&
    SizePreferences === null
      ? true
      : false;
  var onlyDesigners =
    DesignerPreferences !== null &&
    CategoryPreferences === null &&
    SizePreferences === null
      ? true
      : false;
  var onlySizes =
    DesignerPreferences === null &&
    CategoryPreferences === null &&
    SizePreferences !== null
      ? true
      : false;
  var onlySizeAndCat =
    DesignerPreferences === null &&
    CategoryPreferences !== null &&
    SizePreferences !== null
      ? true
      : false;
  var onlyCategories =
    DesignerPreferences === null &&
    CategoryPreferences !== null &&
    SizePreferences === null
      ? true
      : false;
  var categoriesSet = CategoryPreferences !== null ? true : false;



  var selectedCategories = [];
  if (CategoryPreferences) {
    CategoryPreferences = JSON.parse(CategoryPreferences);
    Object.keys(CategoryPreferences).forEach(function eachKey(key) {
      var tempCategory = catalogMgr.getCategory(key);
      var name = tempCategory
        ? tempCategory.custom.PreferenceCenterCategoryName
          ? tempCategory.custom.PreferenceCenterCategoryName
          : tempCategory.displayName
        : "";
      var url = searchHelper.getCategoryUrl(tempCategory);
      var catAttributes = { id: key, name: name, url: url };
      selectedCategories.push(catAttributes);
    });
  }

  var brandRefinementsURL = "";
  if (DesignerPreferences) {
    var selectedDesignersList = [];
    Object.keys(JSON.parse(DesignerPreferences)).forEach(function eachKey(key) {
      selectedDesignersList.push(key);
    });

    brandRefinementsURL = selectedDesignersList[0];
    if (selectedDesignersList.length > 1) {
      for (var i = 1; i < selectedDesignersList.length; i++)
        brandRefinementsURL += "|" + selectedDesignersList[i];
    }
  }
  var cookieValue = cookiesHelper.read("RememberMeForced");
  var loggedIn = !!(req.currentCustomer.raw.profile) ? true : false;
  var site = require("dw/system/Site");
  var softLoggedOut =
    !req.currentCustomer.raw.authenticated && cookieValue ? true : false;
  var isMinTiles =
    !loggedIn || softLoggedIn || noPreferences || onlySizes ? true : false;

  var currentSite = site.getCurrent().getID();
  var addCats = !categoriesSet && !onlySizes && loggedIn && !softLoggedIn ? true : false;
  //  && !onlySizes && loggedIn && !softLoggedIn ? true : false;

  var stopper = 'stop';
  if (currentSite === "SaksFifthAvenue") {
    res.render("/components/header/forMe", {
      Categories: new Categories(topLevelCategories),
      softLoggedOut: softLoggedOut,
      name: req.currentCustomer.raw.profile,
      noPreferences: noPreferences,
      onlyDesigners: onlyDesigners,
      loggedIn: loggedIn,
      categoriesSet: categoriesSet,
      selectedCategories: selectedCategories,
      onlySizes: onlySizes,
      onlyCategories: onlyCategories,
      onlySizeAndCat: onlySizeAndCat,
      softLoggedIn: softLoggedIn,
      isMinTiles: isMinTiles,
      brandRefinementsURL: brandRefinementsURL,
      addCats: addCats
    });
  }
  next();
});

server.replace(
  'IncludeHeaderMenu',
  server.middleware.include,
  cache.applyDefaultCache,
  function (req, res, next) {
    var catalogMgr = require('dw/catalog/CatalogMgr');
    var siteRootCategory = catalogMgr.getSiteCatalog().getRoot();
    var preferences = require('*/cartridge/config/preferences');
    var Categories;

    if (preferences.GlobalBEPerformanceChangesEnable) {
      Categories = require('*/cartridge/models/categoriesNew');
    } else {
      Categories = require('*/cartridge/models/categories');
    }

    var topLevelCategories = siteRootCategory.hasOnlineSubCategories()
      ? siteRootCategory.getOnlineSubCategories()
      : null;

    // res.render('/components/header/menu', new Categories(topLevelCategories));
    if (req.currentCustomer.raw.profile !== null) {
      var DesignerPreferences = req.currentCustomer.raw.profile.custom.DesignerPreferences;
      var CategoryPreferences = req.currentCustomer.raw.profile.custom.CategoryPreferences;
      var SizePreferences = req.currentCustomer.raw.profile.custom.SizePreferences;
    }

    var noPreferences = (DesignerPreferences === null && CategoryPreferences === null && SizePreferences === null) ? true : false;

    if (preferences.GlobalBEPerformanceChangesEnable) {
      if (!req.currentCustomer.raw.authenticated) {
        if (cookiesHelper.read("RememberMeForced")) {
          var softLoggedOut = true;
        }
      }
    } else {
      var cookieValue = cookiesHelper.read("RememberMeForced");

      if (!req.currentCustomer.raw.authenticated && cookieValue) {
        var  softLoggedOut = true;
      }
    }

    res.render('/components/header/menu', {
      categories: new Categories(topLevelCategories).categories,
      softLoggedOut: softLoggedOut,
      name: req.currentCustomer.raw.profile,
      noPreferences: noPreferences
    });

    next();
  }
);

server.append('Locale', function (req, res, next) {
  var preferences = require('*/cartridge/config/preferences');
  res.setViewData({ hostRedirectEnabled: preferences.hostRedirectEnabled });
  next();
});

module.exports = server.exports();
