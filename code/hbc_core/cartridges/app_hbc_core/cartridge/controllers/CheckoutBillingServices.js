'use strict';

var server = require('server');

var csrfProtection = require('*/cartridge/scripts/middleware/csrf');

/**
 * Handle Ajax address addition to address book
 */
server.post('AddAddress', server.middleware.https, csrfProtection.validateAjaxRequest, function (req, res, next) {
  var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
  var Transaction = require('dw/system/Transaction');
  var BasketMgr = require('dw/order/BasketMgr');
  var Locale = require('dw/util/Locale');
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var currentLocale = Locale.getLocale(req.locale.id);
  var AccountModel = require('*/cartridge/models/account');
  var addressFormObj = {};
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var OrderModel = require('*/cartridge/models/order');

  var form = server.forms.getForm('billing');

  addressFormObj = form.addressFields && form.addressFields ? form.addressFields : null;
  var customer = CustomerMgr.getCustomerByCustomerNumber(req.currentCustomer.profile.customerNo);
  var addressBook = customer.getProfile().getAddressBook();
  this.on('route:BeforeComplete', function () {
    // eslint-disable-line no-shadow
    var formInfo = addressFormObj;
    if (formInfo) {
      Transaction.wrap(function () {
        var address = null;
        var currentBasket = BasketMgr.getCurrentBasket();
        if (req.querystring.addressId) {
          address = addressBook.getAddress(req.querystring.addressId);
          if (!address) {
            address = addressBook.createAddress(req.querystring.addressId);
          }
        } else {
          address = addressBook.getAddress(formInfo.address1.value);
          if (!address) {
            address = addressBook.createAddress(formInfo.address1.value);
          }
        }

        var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
        if (usingMultiShipping === true && currentBasket.shipments.length < 2) {
          req.session.privacyCache.set('usingMultiShipping', false);
          usingMultiShipping = false;
        }

        var basketModel = new OrderModel(currentBasket, {
          usingMultiShipping: usingMultiShipping,
          shippable: true,
          countryCode: currentLocale.country,
          containerView: 'basket'
        });

        if (address) {
          // Save form's address
          var selectedAddress = COHelpers.updateAddressFields(req, address, formInfo);
          var customerModel = new AccountModel(req.currentCustomer);
          res.json({
            form: server.forms.getForm('billing'),
            selectedAddress: selectedAddress,
            createCustomerAddressHtml: renderTemplateHelper.getRenderedHtml(
              {
                customer: customerModel,
                order: basketModel
              },
              'checkout/billing/addressSelector'
            )
          });
        } else {
          res.json({
            error: true
          });
        }
      });
    }
  });
  return next();
});

module.exports = server.exports();
