'use strict';

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');
server.extend(module.superModule);
var URLUtils = require('dw/web/URLUtils');

//* EXTENTED*//

/**
 * route: Initiates the store search modal
 * renders: StoreModel {Object} containing array of store models
 */
server.get('InitSearch', cache.applyDefaultCache, function (req, res, next) {
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var showMap = false;
  var horizontalView = true;
  var isForm = true;
  var storesModel = {
    stores: null
  };
  storesModel.radiusOptions = [15, 30, 50, 100, 300];
  storesModel.actionUrl = URLUtils.url('Stores-FindStores', 'showMap', showMap).toString();
  var viewData = {
    stores: storesModel,
    horizontalView: horizontalView,
    isForm: isForm,
    showMap: showMap,
    executeSearch: false
  };
  if (req.querystring.products) {
    viewData.products = req.querystring.products;
  }
  var storesResultsHtml = renderTemplateHelper.getRenderedHtml(viewData, 'storeLocator/storeLocatorNoDecorator');

  storesModel.storesResultsHtml = storesResultsHtml;
  res.json(storesModel);
  next();
});

/**
 * route: Sets the selected store in to session
 * renders: ViewData containing store details
 */
server.get('SetStore', function (req, res, next) {
  var params = req.querystring;
  var bopisHelper = require('*/cartridge/scripts/helpers/bopisHelpers');
  var ProductFactory = require('*/cartridge/scripts/factories/product');
  var product = ProductFactory.get(params);
  res.setViewData({
    product: product,
    storeid: params.storeId ? params.storeId : null
  });
  if (params.storeId && params.storeDistance) {
    // set store to session only in search page
    if (params.savetosession) {
      bopisHelper.setStoreFromSession(params.storeId, params.storeDistance);
    }
    /** ************BOPIS******************
     *****Store Info, Inventory Detail******
     ****Availability Message only in PDP***/

    if (!params.noviewdata) {
      bopisHelper.setBopisViewData(req, res);
    }
  }
  res.json(res.getViewData());
  next();
});

/** Add store refiment bar in the search pages****
 *** This is a url include in the template same****
 *** as refiments***/
server.get('ShowStoreRefinement', server.middleware.include, function (req, res, next) {
  var CatalogMgr = require('dw/catalog/CatalogMgr');
  var bopisHelper = require('*/cartridge/scripts/helpers/bopisHelpers');
  var ProductSearchModel = require('dw/catalog/ProductSearchModel');
  var ProductSearch = require('*/cartridge/models/search/productSearch');
  var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
  var refineSearch = require('*/cartridge/models/bopis/refineSearch');
  var refinementActionUrl = 'Search-Show';
  var isRefinedByStore = false;
  var isStoreFilterApplied = false;
  var storeObj = {};

  var storeRefineResult = null;
  // get defalt store from session or with geo location
  var defaultStore = bopisHelper.getDefaultStore();
  isStoreFilterApplied = bopisHelper.isStoreFilterApplied(req);
  // set store search url
  var storeRefineUrl = refineSearch.getStoreRefinementUrl(req, refinementActionUrl);
  if (defaultStore && req.querystring.ajax) {
    storeObj.storeid = defaultStore.ID;
  }
  if (req.querystring.storeid) {
    storeObj.storeid = req.querystring.storeid;
  }
  // add or remove the store if in search query
  storeRefineUrl = bopisHelper.toggleStoreRefinement(storeRefineUrl, defaultStore, isStoreFilterApplied);

  var apiProductSearch = new ProductSearchModel();
  apiProductSearch = searchHelper.setupSearch(apiProductSearch, req.querystring);
  // PSM search with store refinement
  storeRefineResult = refineSearch.search(apiProductSearch, storeObj);
  isRefinedByStore = storeRefineResult.isRefinedByStore;
  apiProductSearch = storeRefineResult.apiProductsearch;

  var productSearch = new ProductSearch(
    apiProductSearch,
    req.querystring,
    req.querystring.srule,
    CatalogMgr.getSortingOptions(),
    CatalogMgr.getSiteCatalog().getRoot()
  );
  res.render('store/storeRefineBar', {
    defaultStore: defaultStore,
    isRefinedByStore: isRefinedByStore,
    productSearch: productSearch,
    storeRefineUrl: storeRefineUrl,
    isStoreFilterApplied: isStoreFilterApplied,
    storeModalUrl: URLUtils.url('Stores-InitSearch').toString(),
    setStoreUrl: URLUtils.url('Stores-SetStore').toString()
  });
  next();
});

module.exports = server.exports();
