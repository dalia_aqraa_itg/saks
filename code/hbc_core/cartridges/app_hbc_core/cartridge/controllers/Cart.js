'use strict';

var server = require('server');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var preferences = require('*/cartridge/config/preferences');
var Resource = require('dw/web/Resource');
var collections = require('*/cartridge/scripts/util/collections');
var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
var Logger = require('dw/system/Logger');
server.extend(module.superModule);

/**
 * route: On toggle of ship to radio buttons on cart page
 * renders: custom store object data
 */
server.get('ToggleShippingOption', function (req, res, next) {
  /** api modules */
  var BasketMgr = require('dw/order/BasketMgr');
  var Transaction = require('dw/system/Transaction');
  /** script module */
  var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');
  var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');

  var pliUUID = req.querystring.uuid;
  var productId = req.querystring.prodid;
  var storeID = req.querystring.storeId ? req.querystring.storeId : null;
  var currentBasket = BasketMgr.getCurrentBasket();
  var data = {};
  var viewData = null;

  try {
    data.storeId = storeID;
    if (currentBasket && pliUUID) {
      Transaction.wrap(function () {
        // delete empty shipments
        COHelpers.ensureNoEmptyShipments(req);
        // get product line item by UUID passed as parameter
        var pli = cartHelper.getProductLineItemByUUID(currentBasket, pliUUID);
        // update PLI, shipment for 'shiptohome' or 'instore' option
        if (pli) {
          viewData = cartHelper.changeShippingOption(currentBasket, pli, storeID, req, data);
        }
        var basketDefaultShipment = currentBasket.defaultShipment;
        // @FIXTO : retaining shipping address when user toggles back to STH in cart form PICKUP
        if (
          currentBasket.shipments.length === 1 &&
          !('shipmentType' in basketDefaultShipment.custom && basketDefaultShipment.custom.shipmentType === 'instore')
        ) {
          var shippingAddress = basketDefaultShipment.shippingAddress;
          if (
            shippingAddress &&
            shippingAddress.firstName &&
            shippingAddress.firstName != '' &&
            (!shippingAddress.lastName || shippingAddress.lastName === '')
          ) {
            var storeAddress = {
              address: {
                firstName: null,
                lastName: null,
                address1: null,
                address2: null,
                city: null,
                stateCode: null,
                postalCode: null,
                countryCode: {
                  value: null
                },
                phone: null
              }
            };
            // clear shipping address
            COHelpers.copyShippingAddressToShipment(storeAddress, basketDefaultShipment);
          }
        }
        viewData.changeStoreHtml = storeHelpers.createChangeStoreHtml(productId, storeID);

        basketCalculationHelpers.calculateTotals(currentBasket);
      });
    }
  } catch (err) {
    Logger.error('Error in Cart.js: ToggleShippingOption ' + err);
  }
  res.json(viewData);
  next();
});

/**
 * Helper Method written to display the Prorated discount values of the basket items
 */
server.get('ProrationData', server.middleware.https, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();
  var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
  var viewData = null;
  viewData = cartHelper.getProrationData(currentBasket);
  res.render('/cart/cartProratedData', {
    proratedData: viewData
  });
  next();
});

server.replace('Show', server.middleware.https, consentTracking.consent, csrfProtection.generateToken, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var Transaction = require('dw/system/Transaction');
  var CartModel = require('*/cartridge/models/cart');
  var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
  var reportingUrlsHelper = require('*/cartridge/scripts/reportingUrls');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
  var URLUtils = require('dw/web/URLUtils');
  var OrderMgr = require('dw/order/OrderMgr');
  var inventoryError = req.querystring.inventoryerror ? req.querystring.inventoryerror : null;
  var currentBasket = BasketMgr.getCurrentBasket();
  // changes for SFDEV-5099
  var updatedLineItems = cartHelper.adjustLineItemQuantities(currentBasket);
  var reportingURLs;
  var purchaseLimitedSKUMap = cartHelper.resetPLISPurchaseLimit(currentBasket);
  var basketLimitReached = false;

  // Clear up Canada Tax if not Save the shipping address
  cartHelper.clearCanadaTax(currentBasket);

  // Setting up the Payment Type Session attribute so that promotions dependent on Regular Payment Methods (Customer Group RegularTenderUsers) always apply in the cart
  if (req.session.raw.custom.isHBCTenderType === null || req.session.raw.custom.isHBCTenderType === undefined) {
    // eslint-disable-next-line no-param-reassign
    req.session.raw.custom.isHBCTenderType = false;
  }

  req.session.privacyCache.set('EDDZipCode', null);

  if (dw.web.Resource.msg('paypal.version.number', 'paypal_version', null) == '18.3.1') {
    var paypalHelper = require('*/cartridge/scripts/paypal/paypalHelper');
    var customerBillingAgreement;
    var isAddressExistForBillingAgreementCheckout;
    var prefs = paypalHelper.getPrefs();
    if (currentBasket && currentBasket.getCurrencyCode()) {
      customerBillingAgreement = paypalHelper.getCustomerBillingAgreement(currentBasket.getCurrencyCode());
      isAddressExistForBillingAgreementCheckout = !!customerBillingAgreement.getDefaultShippingAddress();
    }

    // In this case no need shipping address
    if (currentBasket && currentBasket.getDefaultShipment().productLineItems.length <= 0) {
      isAddressExistForBillingAgreementCheckout = true;
    }

    var buttonConfig;
    if (customerBillingAgreement && customerBillingAgreement.hasAnyBillingAgreement && prefs.PP_BillingAgreementState !== 'DoNotCreate') {
      buttonConfig = prefs.PP_Cart_Button_Config;
      buttonConfig.env = prefs.environmentType;
      buttonConfig.billingAgreementFlow = {
        startBillingAgreementCheckoutUrl: URLUtils.https('Paypal-StartBillingAgreementCheckout').toString(),
        isShippingAddressExist: isAddressExistForBillingAgreementCheckout
      };
    } else {
      buttonConfig = prefs.PP_Cart_Button_Config;
      buttonConfig.env = prefs.environmentType;
      buttonConfig.createPaymentUrl = URLUtils.https('Paypal-StartCheckoutFromCart', 'isAjax', 'true').toString();
    }

    buttonConfig.locale = req.locale.id;

    res.setViewData({
      paypal: {
        prefs: prefs,
        buttonConfig: buttonConfig
      }
    });
  }

  if (currentBasket) {
    Transaction.wrap(function () {
      if (currentBasket.currencyCode !== req.session.currency.currencyCode) {
        currentBasket.updateCurrency();
      }
      cartHelper.ensureAllShipmentsHaveMethods(currentBasket);

      basketCalculationHelpers.calculateTotals(currentBasket);
    });
  }

  if (currentBasket && currentBasket.allLineItems.length) {
    reportingURLs = reportingUrlsHelper.getBasketOpenReportingURLs(currentBasket);
  }

  /** script module */
  var bopisHelper = require('*/cartridge/scripts/helpers/bopisHelpers');
  var defaltStore = null;
  var viewData = res.getViewData();
  viewData.storeModalUrl = URLUtils.url('Stores-InitSearch').toString();
  viewData.toggleShitoUrl = URLUtils.url('Cart-ToggleShippingOption').toString();
  if (preferences.isBopisEnabled) {
    defaltStore = bopisHelper.getDefaultStore();
    viewData.storeInfo = defaltStore;
  }

  var address = server.forms.getForm('address');
  address.clear();
  viewData.addressForm = address;
  res.setViewData(viewData);
  if (req.session.privacyCache.get('basketLimitReached')) {
    basketLimitReached = true;
    req.session.privacyCache.set('basketLimitReached', null);
  }
  let shopRunnerEnabled = require('*/cartridge/scripts/helpers/shopRunnerHelpers').checkSRExpressCheckoutEligibility(currentBasket);
  var isAllProductsSREligible = require('*/cartridge/scripts/helpers/shopRunnerHelpers').checkSRCheckoutEligibilityForCart(currentBasket);
  var gwp = {
    isMultipleChoiceExists: cartHelper.isMultiChoiceBonusProduct(currentBasket),
    buttonName: cartHelper.provideButtonName(currentBasket)
  };
  res.setViewData({
    addressForm: server.forms.getForm('address'),
    paypalCalculatedCost: currentBasket ? currentBasket.totalGrossPrice : 0,
    reportingURLs: reportingURLs,
    shopRunnerEnabled: shopRunnerEnabled,
    isAllProductsSREligible: isAllProductsSREligible,
    updatedLineItems: updatedLineItems,
    inventoryError: inventoryError,
    invErroMsg: inventoryError ? Resource.msg('error.cart.or.checkout.error', 'cart', null) : null,
    promoTrayEnabled: preferences.promoTrayEnabled,
    mpPreferences: preferences.masterpass,
    mpCartID: currentBasket ? currentBasket.getUUID() : -1,
    purchaseLimitedSKUMap: purchaseLimitedSKUMap,
    gwp: gwp,
    s7APIForPDPZoomViewer: preferences.s7APIForPDPZoomViewer,
    s7APIForPDPVideoPlayer: preferences.s7APIForPDPVideoPlayer,
    basketLimitReached: basketLimitReached,
    BasketLimitMsg: Resource.msg('commom.basketlimit.message.merge', 'common', null)
  });
  // BFX Order Creation Number
  // Release Inventory if someone reserve it already and changing the country/loading the cart page again.
  if (currentBasket != null) {
    currentBasket.releaseInventory();
  }
  res.setViewData({
    BorderFreeEnabled: preferences.borderFreeEnabled
  });
  var bfxCountryCode = cookiesHelper.read('bfx.country');
  if (bfxCountryCode && bfxCountryCode !== 'US') {
    var bfxOrderNumber = req.session.privacyCache.get('bfxOrderNumber');
    if (!bfxOrderNumber) {
      bfxOrderNumber = OrderMgr.createOrderNo();
      req.session.privacyCache.set('bfxOrderNumber', bfxOrderNumber);
    }
    res.setViewData({
      bfxOrderNumber: bfxOrderNumber
    });
  } else {
    req.session.privacyCache.set('bfxOrderNumber', null);
  }

  // Price Overrride/Price adjustment for OOBO
  var isAgentUser = false;
  if (req.session.raw.isUserAuthenticated()) {
    isAgentUser = true;
  }
  res.setViewData({
    isAgentUser: isAgentUser,
    priceOverrideForm: server.forms.getForm('priceOverride'),
    isCartPage: true,
    priceOverrideReasonCode: preferences.cscPriceoverrideReasonCode
  });

  // Setting the CSRF token details in session, so that it can be used in the Add Coupon method (cartPromoCode.isml)
  var tokenName = viewData.csrf.tokenName;
  var tokenValue = viewData.csrf.token;
  req.session.privacyCache.set('csrftokenName', tokenName);
  req.session.privacyCache.set('csrftokenValue', tokenValue);

  // Cart Tax Calculation
  var postalCode;
  if (
    currentBasket &&
    currentBasket.defaultShipment &&
    currentBasket.defaultShipment.shippingAddress &&
    currentBasket.defaultShipment.shippingAddress.postalCode
  ) {
    postalCode = currentBasket.defaultShipment.shippingAddress.postalCode;
  } else if (
    currentBasket &&
    currentBasket.customer.authenticated &&
    currentBasket.customer.addressBook &&
    currentBasket.customer.addressBook.preferredAddress &&
    currentBasket.customer.addressBook.preferredAddress.postalCode
  ) {
    postalCode = currentBasket.customer.addressBook.preferredAddress.postalCode;
  } else {
    postalCode = Resource.msg('label.enter.postal.code', 'cart', null);
  }
  res.setViewData({
    taxPostalCode: postalCode
  });

  var basketModel = new CartModel(currentBasket);
  res.render('cart/cart', basketModel);

  // Handling Empty Cart
  if (!res.viewData.numItems) {
    var emptyCart = {
      Redirecturl: URLUtils.url('Home-Show')
    };
    res.setViewData({
      emptyCart: emptyCart
    });
  }

  var target = req.querystring.rurl || 1;
  var actionUrl = URLUtils.url('Account-Login', 'rurl', target);
  res.setViewData({
    includeRecaptchaJS: true,
    actionUrl: actionUrl
  });
  next();
});

server.post('AdjustLineItemPrice', function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var UUIDUtils = require('dw/util/UUIDUtils');
  var Transaction = require('dw/system/Transaction');
  var CartModel = require('*/cartridge/models/cart');
  var Money = require('dw/value/Money');
  var PercentageDiscount = require('dw/campaign/PercentageDiscount');
  var AmountDiscount = require('dw/campaign/AmountDiscount');
  var Status = require('dw/system/Status');
  var PriceAdjustmentLimitTypes = require('dw/order/PriceAdjustmentLimitTypes');
  var formatMoney = require('dw/util/StringUtils').formatMoney;
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var currentBasket = BasketMgr.getCurrentBasket();
  if (!currentBasket) {
    res.setStatusCode(500);
    res.json({
      error: true,
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }
  var priceOverrideForm = server.forms.getForm('priceOverride');
  var priceOverrideObj = priceOverrideForm.toObject();
  var pliUUID = req.form.pliuuid;
  var priceOverrided = false;
  priceOverrideObj.pliUUID = pliUUID;
  if (priceOverrideForm.valid && pliUUID) {
    res.setViewData(priceOverrideObj);
    var formInfo = res.getViewData();
    Transaction.wrap(function () {
      var uuid = formInfo.pliUUID;
      var updatedPrice = parseFloat(formInfo.newPrice);
      if (updatedPrice >= 0) {
        var productLineItems = currentBasket.allProductLineItems;
        var reqlineItem = collections.find(productLineItems, function (item) {
          return item.UUID === uuid;
        });
        if (reqlineItem) {
          // If the Overriding Price is more than Adjusted Price.
          var currentPrice = reqlineItem.getBasePrice().value;
          if (updatedPrice > currentPrice) {
            var basketModel = new CartModel(currentBasket);
            basketModel.CSRSuccess = false;
            basketModel.CSRerrorMessage = Resource.msgf('label.exceed.base.amount', 'cart', null);
            res.json(basketModel);
            return;
          }
          // Remove the existing CSR Adjustment if already applied.
          var adjustments = reqlineItem.priceAdjustments.iterator();
          while (adjustments.hasNext()) {
            var existingAdjustment = adjustments.next();
            if ('isCSRAdjustment' in existingAdjustment.custom && existingAdjustment.custom.isCSRAdjustment) {
              reqlineItem.removePriceAdjustment(existingAdjustment);
              delete reqlineItem.custom.priceOverrided;
              delete reqlineItem.custom.cscPriceoverrideReason;
              delete reqlineItem.custom.cscPriceoverrideNote;
              delete reqlineItem.custom.cscPriceoverrideNewprice;
              // refresh Basket Calculation
              basketCalculationHelpers.calculateTotals(currentBasket);
            }
          }
          var newPrice = new Money(updatedPrice, currentBasket.getCurrencyCode());
          var oldPrice = reqlineItem.adjustedPrice.divide(reqlineItem.quantityValue);
          var adjustment = reqlineItem.createPriceAdjustment(UUIDUtils.createUUID(), new AmountDiscount(new Number(oldPrice.subtract(newPrice).value)));
          adjustment.setManual(true);
          basketCalculationHelpers.calculateTotals(currentBasket);
          var limitStatus = currentBasket.verifyPriceAdjustmentLimits();
          if (limitStatus.status === Status.ERROR) {
            var errorItems = limitStatus.items;
            for (var i = 0; i < errorItems.length; i++) {
              var statusItem = errorItems[i];
              var statusDetail = statusItem.details.entrySet().iterator().next();
              var maxAllowedLimit = Number(statusDetail.key);
              if (statusItem.code == PriceAdjustmentLimitTypes.TYPE_ITEM) {
                // Remove the adjustment and display the error message to CSR.
                reqlineItem.removePriceAdjustment(adjustment);
                basketCalculationHelpers.calculateTotals(currentBasket);
                var maxLimit = formatMoney(new Money(maxAllowedLimit, currentBasket.getCurrencyCode()));
                var basketModel = new CartModel(currentBasket);
                basketModel.CSRSuccess = false;
                basketModel.CSRerrorMessage = Resource.msgf('label.exceed.allowed.amount', 'cart', null, maxLimit);
                res.json(basketModel);
              }
            }
          } else {
            adjustment.custom.isCSRAdjustment = true;
            reqlineItem.custom.priceOverrided = true;
            reqlineItem.custom.cscPriceoverrideReason = formInfo.priceOverrideReason;
            reqlineItem.custom.cscPriceoverrideNote = formInfo.addNote;
            reqlineItem.custom.cscPriceoverrideNewprice = updatedPrice;
            priceOverrided = true;
          }
        }
      }
    });
  }
  if (priceOverrided) {
    // eslint-disable-next-line
    var basketModel = new CartModel(currentBasket);
    // eslint-disable-next-line block-scoped-var
    res.json(basketModel);
  }
  next();
});

server.replace('MiniCart', server.middleware.https, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');

  var currentBasket = BasketMgr.getCurrentBasket();
  var quantityTotal;

  if (currentBasket) {
    quantityTotal = currentBasket.productQuantityTotal;
  } else {
    quantityTotal = 0;
  }

  res.render('/components/header/miniCart', {
    quantityTotal: quantityTotal,
    shopRunnerEnabled: preferences.shopRunnerEnabled
  });
  next();
});

server.replace('MiniCartShow', function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var Transaction = require('dw/system/Transaction');
  var CartModel = require('*/cartridge/models/cart');
  var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
  var reportingUrlsHelper = require('*/cartridge/scripts/reportingUrls');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

  var currentBasket = BasketMgr.getCurrentBasket();
  // changes for SFDEV-5099
  var updatedLineItems = cartHelper.adjustLineItemQuantities(currentBasket);
  var reportingURLs;
  // changes for SFDEV-6215
  var soldOutItems = [];
  if (session.custom.omsInventory && session.custom.omsInventory.length > 0) {
    var omsInventoryArray = JSON.parse(session.custom.omsInventory);
    if (omsInventoryArray && omsInventoryArray.length > 0) {
      omsInventoryArray.forEach(function (item) {
        if (item.quantity === 0) {
          soldOutItems.push(item.itemID);
        }
      });
    }
  }

  var purchaseLimitedSKUMap = cartHelper.resetPLISPurchaseLimit(currentBasket);

  // Setting up the Payment Type Session attribute so that promotions dependent on Regular Payment Methods (Customer Group RegularTenderUsers) always apply in the cart
  if (req.session.raw.custom.isHBCTenderType === null || req.session.raw.custom.isHBCTenderType === undefined) {
    // eslint-disable-next-line no-param-reassign
    req.session.raw.custom.isHBCTenderType = false;
  }

  if (currentBasket) {
    Transaction.wrap(function () {
      if (currentBasket.currencyCode !== req.session.currency.currencyCode) {
        currentBasket.updateCurrency();
      }
      cartHelper.ensureAllShipmentsHaveMethods(currentBasket);
      basketCalculationHelpers.calculateTotals(currentBasket);
    });
  }

  if (currentBasket && currentBasket.allLineItems.length) {
    reportingURLs = reportingUrlsHelper.getBasketOpenReportingURLs(currentBasket);
  }
  var basketModel = new CartModel(currentBasket);
  var productAvaiblityStatus = false;
  if (currentBasket) {
    productAvaiblityStatus = cartHelper.productAvailabilityStatus(basketModel, soldOutItems);
  }

  res.setViewData({
    reportingURLs: reportingURLs,
    shopRunnerEnabled: preferences.shopRunnerEnabled,
    soldOutItems: soldOutItems,
    updatedLineItems: updatedLineItems,
    productAvaiblityStatus: productAvaiblityStatus,
    purchaseLimitedSKUMap: purchaseLimitedSKUMap,
    cartView: 'miniCart'
  });
  res.render('checkout/cart/miniCart', basketModel);
  next();
});

server.replace('UpdateQuantity', function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var Transaction = require('dw/system/Transaction');
  var URLUtils = require('dw/web/URLUtils');
  var CartModel = require('*/cartridge/models/cart');
  var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

  var currentBasket = BasketMgr.getCurrentBasket();

  if (!currentBasket) {
    res.setStatusCode(500);
    res.json({
      error: true,
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });

    return next();
  }

  var productId = req.querystring.pid;
  var updateQuantity = parseInt(req.querystring.quantity, 10);
  var uuid = req.querystring.uuid;
  var productLineItems = currentBasket.productLineItems;
  var matchingLineItem = collections.find(productLineItems, function (item) {
    return item.productID === productId && item.UUID === uuid;
  });
  var availableToSell = 0;

  var totalQtyRequested = 0;
  var qtyAlreadyInCart = 0;
  var minOrderQuantity = 0;
  var previousQuantity = 0;
  var perpetual = false;
  var canBeUpdated = false;
  var bundleItems;
  var bonusDiscountLineItemCount = currentBasket.bonusDiscountLineItems.length;
  var errorMsg = Resource.msg('error.cannot.update.product.quantity', 'cart', null);
  if (matchingLineItem) {
    if (matchingLineItem.product.bundle) {
      bundleItems = matchingLineItem.bundledProductLineItems;
      canBeUpdated = collections.every(bundleItems, function (item) {
        var quantityToUpdate = updateQuantity * matchingLineItem.product.getBundledProductQuantity(item.product).value;
        qtyAlreadyInCart = cartHelper.getQtyAlreadyInCart(item.productID, productLineItems, item.UUID);
        totalQtyRequested = quantityToUpdate + qtyAlreadyInCart;
        availableToSell = item.product.availabilityModel.inventoryRecord.ATS.value;
        perpetual = item.product.availabilityModel.inventoryRecord.perpetual;
        minOrderQuantity = item.product.minOrderQuantity.value;
        var isInStock = (totalQtyRequested <= availableToSell || perpetual) && quantityToUpdate >= minOrderQuantity;
        var isInPurchaseLimit = productHelper.isInPurchaselimit(item.product.custom.purchaseLimit, updateQuantity);
        if (!isInPurchaseLimit) {
          errorMsg = Resource.msgf('label.notinpurchaselimit', 'common', null, item.product.custom.purchaseLimit);
        } else if (!isInStock) {
          errorMsg = Resource.msg('label.not.available.items', 'common', null);
        }
        canBeUpdated = isInPurchaseLimit && isInStock;
      });
    } else {
      availableToSell = matchingLineItem.product.availabilityModel.inventoryRecord.ATS.value;
      perpetual = matchingLineItem.product.availabilityModel.inventoryRecord.perpetual;
      qtyAlreadyInCart = cartHelper.getQtyAlreadyInCart(productId, productLineItems, matchingLineItem.UUID);
      totalQtyRequested = updateQuantity + qtyAlreadyInCart;
      minOrderQuantity = matchingLineItem.product.minOrderQuantity.value;

      var isInStock = (totalQtyRequested <= availableToSell || perpetual) && updateQuantity >= minOrderQuantity;
      var isInPurchaseLimit = productHelper.isInPurchaselimit(matchingLineItem.product.custom.purchaseLimit, updateQuantity);
      if (!isInPurchaseLimit && matchingLineItem.product.custom.purchaseLimit <= availableToSell) {
        errorMsg = Resource.msgf('label.notinpurchaselimit', 'common', null, matchingLineItem.product.custom.purchaseLimit);
      } else if (!isInStock) {
        errorMsg = Resource.msg('label.not.available.items', 'common', null);
      }
      canBeUpdated = isInPurchaseLimit && isInStock;
    }
  }

  // changes for SFDEV-6215
  if (canBeUpdated) {
    if (session.custom.omsInventory && session.custom.omsInventory.length > 0) {
      var omsInventoryArray = JSON.parse(session.custom.omsInventory);
      if (omsInventoryArray && omsInventoryArray.length > 0) {
        for (var i = 0; i < omsInventoryArray.length; i++) {
          if (omsInventoryArray[i].itemID === matchingLineItem.productID) {
            if (omsInventoryArray[i].quantity < updateQuantity && omsInventoryArray[i].quantity > 0) {
              updateQuantity = omsInventoryArray[i].quantity;
            }
            break;
          }
        }
      }
    }
  }

  if (canBeUpdated) {
    previousQuantity = matchingLineItem && matchingLineItem.quantity.available ? matchingLineItem.quantity.value : 0;
    Transaction.wrap(function () {
      matchingLineItem.setQuantityValue(updateQuantity);

      var previousBounsDiscountLineItems = collections.map(currentBasket.bonusDiscountLineItems, function (bonusDiscountLineItem) {
        return bonusDiscountLineItem.UUID;
      });

      basketCalculationHelpers.calculateTotals(currentBasket);
      if (currentBasket.bonusDiscountLineItems.length > bonusDiscountLineItemCount) {
        var prevItems = JSON.stringify(previousBounsDiscountLineItems);

        collections.forEach(currentBasket.bonusDiscountLineItems, function (bonusDiscountLineItem) {
          if (prevItems.indexOf(bonusDiscountLineItem.UUID) < 0) {
            bonusDiscountLineItem.custom.bonusProductLineItemUUID = matchingLineItem.UUID; // eslint-disable-line no-param-reassign
            matchingLineItem.custom.bonusProductLineItemUUID = 'bonus';
            matchingLineItem.custom.preOrderUUID = matchingLineItem.UUID;
          }
        });
      }
    });
  }

  if (matchingLineItem && canBeUpdated) {
    var productFactory = require('*/cartridge/scripts/factories/product');
    var recentlyUpdatedProduct = {
      product: productFactory.get({
        pid: req.querystring.pid
      })
    };
    res.setViewData({
      previousQuantity: previousQuantity
    });
    res.setViewData(recentlyUpdatedProduct);
    var basketModel = new CartModel(currentBasket);
    // change for SFDEV-6215
    basketModel.valid.error = false;
    res.json(basketModel);
  } else {
    res.setStatusCode(500);
    res.json({
      errorMessage: errorMsg,
      isInStock: isInStock,
      isInPurchaseLimit: isInPurchaseLimit
    });
  }

  return next();
});

server.append('GetProduct', function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var requestUuid = req.querystring.uuid;
  var requestPLI = collections.find(BasketMgr.getCurrentBasket().allProductLineItems, function (item) {
    return item.UUID === requestUuid;
  });
  res.setViewData({
    requestPLI: requestPLI,
    isEdit: true,
    s7APIForPDPZoomViewer: preferences.s7APIForPDPZoomViewer,
    s7APIForPDPVideoPlayer: preferences.s7APIForPDPVideoPlayer,
    s7VideoServerURL: preferences.s7VideoServerURL,
    s7ImageHostURL: preferences.s7ImageHostURL,
    Zoom: preferences.Zoom,
    template: 'cart/productCard/editProduct'
  });
  next();
});

server.replace('EditProductLineItem', function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var ProductMgr = require('dw/catalog/ProductMgr');
  var URLUtils = require('dw/web/URLUtils');
  var Transaction = require('dw/system/Transaction');
  var CartModel = require('*/cartridge/models/cart');
  var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

  var currentBasket = BasketMgr.getCurrentBasket();

  if (!currentBasket) {
    res.setStatusCode(500);
    res.json({
      error: true,
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }

  var uuid = req.form.uuid;
  var productId = req.form.pid;
  var selectedOptionValueId = req.form.selectedOptionValueId;
  var updateQuantity = parseInt(req.form.quantity, 10);

  var productLineItems = currentBasket.allProductLineItems;
  var requestLineItem = collections.find(productLineItems, function (item) {
    return item.UUID === uuid;
  });

  var uuidToBeDeleted = null;
  var pliToBeDeleted;
  var newPidAlreadyExist = collections.find(productLineItems, function (item) {
    if (item.productID === productId && item.UUID !== uuid) {
      uuidToBeDeleted = item.UUID;
      pliToBeDeleted = item;
      updateQuantity += parseInt(item.quantity, 10);
      return true;
    }
    return false;
  });

  var availableToSell = 0;
  var totalQtyRequested = 0;
  var qtyAlreadyInCart = 0;
  var minOrderQuantity = 0;
  var canBeUpdated = false;
  var perpetual = false;
  var bundleItems;

  if (requestLineItem) {
    if (requestLineItem.product.bundle) {
      bundleItems = requestLineItem.bundledProductLineItems;
      canBeUpdated = collections.every(bundleItems, function (item) {
        var quantityToUpdate = updateQuantity * requestLineItem.product.getBundledProductQuantity(item.product).value;
        qtyAlreadyInCart = cartHelper.getQtyAlreadyInCart(item.productID, productLineItems, item.UUID);
        totalQtyRequested = quantityToUpdate + qtyAlreadyInCart;
        availableToSell = item.product.availabilityModel.inventoryRecord.ATS.value;
        perpetual = item.product.availabilityModel.inventoryRecord.perpetual;
        minOrderQuantity = item.product.minOrderQuantity.value;
        return (
          (totalQtyRequested <= availableToSell || perpetual) &&
          quantityToUpdate >= minOrderQuantity &&
          productHelper.isInPurchaselimit(item.product.custom.purchaseLimit, updateQuantity)
        );
      });
    } else {
      availableToSell = requestLineItem.product.availabilityModel.inventoryRecord.ATS.value;
      perpetual = requestLineItem.product.availabilityModel.inventoryRecord.perpetual;
      qtyAlreadyInCart = cartHelper.getQtyAlreadyInCart(productId, productLineItems, requestLineItem.UUID);
      totalQtyRequested = updateQuantity + qtyAlreadyInCart;
      minOrderQuantity = requestLineItem.product.minOrderQuantity.value;
      canBeUpdated =
        (totalQtyRequested <= availableToSell || perpetual) &&
        updateQuantity >= minOrderQuantity &&
        productHelper.isInPurchaselimit(requestLineItem.product.custom.purchaseLimit, updateQuantity);
      // This scenario will come when customer tries to update which is in stock but not the current product.
      if (availableToSell === 0 && !newPidAlreadyExist) {
        canBeUpdated = true;
      }
    }
  }
  var omsSoldOut = false;
  // changes for SFDEV-6215
  if (canBeUpdated) {
    if (session.custom.omsInventory && session.custom.omsInventory.length > 0) {
      var omsInventoryArray = JSON.parse(session.custom.omsInventory);
      if (omsInventoryArray && omsInventoryArray.length > 0) {
        for (var i = 0; i < omsInventoryArray.length; i++) {
          if (omsInventoryArray[i].itemID === productId) {
            if (omsInventoryArray[i].quantity <= 0) {
              omsSoldOut = true;
              break;
            }
          }
        }
      }
    }
  }
  var error = false;
  if (canBeUpdated) {
    var product = ProductMgr.getProduct(productId);
    try {
      Transaction.wrap(function () {
        if (newPidAlreadyExist) {
          var shipmentToRemove = pliToBeDeleted.shipment;
          currentBasket.removeProductLineItem(pliToBeDeleted);
          if (shipmentToRemove.productLineItems.empty && !shipmentToRemove.default) {
            currentBasket.removeShipment(shipmentToRemove);
          }
        }

        if (!requestLineItem.product.bundle) {
          requestLineItem.replaceProduct(product);
        }

        // If the product has options
        var optionModel = product.getOptionModel();
        if (optionModel && optionModel.options && optionModel.options.length) {
          var productOption = optionModel.options.iterator().next();
          var productOptionValue = optionModel.getOptionValue(productOption, selectedOptionValueId);
          var optionProductLineItems = requestLineItem.getOptionProductLineItems();
          var optionProductLineItem = optionProductLineItems.iterator().next();
          optionProductLineItem.updateOptionValue(productOptionValue);
        }

        requestLineItem.setQuantityValue(updateQuantity);
        basketCalculationHelpers.calculateTotals(currentBasket);
      });
    } catch (e) {
      error = true;
    }
  }
  // changes for SFDEV-6215
  if (omsSoldOut) {
    res.setStatusCode(500);
    res.json({
      error: true,
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });

    return next();
  }

  if (!error && requestLineItem && canBeUpdated) {
    var cartModel = new CartModel(currentBasket);
    // change for SFDEV-6215
    if (cartModel.valid) {
      cartModel.valid.error = false;
    }
    var responseObject = {
      cartModel: cartModel,
      newProductId: productId
    };

    if (uuidToBeDeleted) {
      responseObject.uuidToBeDeleted = uuidToBeDeleted;
    }

    var priceHelper = require('*/cartridge/scripts/helpers/pricing');
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var product = ProductFactory.get({
      pid: requestLineItem.product.ID
    });
    var context = {
      price: product.price,
      product: product
    };
    var priceHTML = priceHelper.renderHtml(priceHelper.getHtmlContext(context));
    res.setViewData({
      priceHTML: priceHTML
    });
    res.json(responseObject);
  } else {
    res.setStatusCode(500);
    res.json({
      errorMessage: Resource.msg('error.cannot.update.product', 'cart', null)
    });
  }

  return next();
});

server.post('CalculateTax', csrfProtection.validateAjaxRequest, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var CartModel = require('*/cartridge/models/cart');
  var currentBasket = BasketMgr.getCurrentBasket();
  var postalCodeForm = server.forms.getForm('address').postalCode;
  if (currentBasket && postalCodeForm.value && postalCodeForm.valid) {
    var checkoutHelper = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var Transaction = require('dw/system/Transaction');
    var prefferedAddress =
      currentBasket.customer.authenticated && currentBasket.customer.addressBook && currentBasket.customer.addressBook.preferredAddress
        ? currentBasket.customer.addressBook.preferredAddress
        : null;
    // Zip code should not be updated to saved address
    var address = {};
    address.firstName = '';
    address.lastName = '';
    address.address1 = '';
    address.address2 = '';
    address.city = '';
    address.postalCode = postalCodeForm.value;
    address.stateCode = '';
    var countryCode = {
      value: 'CA'
    };
    address.countryCode = countryCode;
    address.phone = '';
    checkoutHelper.copyCustomerAddressToShipment(address);
    checkoutHelper.copyCustomerAddressToBilling(address);
    Transaction.wrap(function () {
      basketCalculationHelpers.calculateTotals(currentBasket);
    });
    res.setViewData({
      postalCode: postalCodeForm.value
    });
  }
  var cartModel = new CartModel(currentBasket);
  res.json({
    error: postalCodeForm.error,
    data: cartModel
  });
  next();
});

server.append('AddProduct', function (req, res, next) {
  if (!res.viewData.error) {
    var productId = req.form.pid;
    var productFactory = require('*/cartridge/scripts/factories/product');
    var renderHelper = require('*/cartridge/scripts/renderTemplateHelper');
    var recentlyAddedProduct = {
      product: productFactory.get({
        pid: productId
      })
    };
    var addToCartConfirmationModal = renderHelper.getRenderedHtml(recentlyAddedProduct, 'cart/addToCartConfirmation');
    res.setViewData({
      addToCartConfirmationModal: addToCartConfirmationModal,
      product: { id: productId }
    });

  }
  next();
});

/**
 * Adds a coupon to the cart using JSON.
 *
 * Gets the The Baket. Gets the coupon code from the form couponCode parameter.
 * In a transaction, adds the coupon to the cart and renders a JSON object that includes the coupon code
 * and the status of the transaction.
 *
 */
server.post('AddCouponJson', function (req, res, next) {
  var couponCode;
  var couponStatus;
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentOrNewBasket();
  var URLUtils = require('dw/web/URLUtils');
  couponCode = req.form.couponCode;
  var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');

  if (!currentBasket) {
    res.setStatusCode(500);
    res.json({
      error: true,
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }
  if (couponCode) {
    var campaignBased = true;
    couponStatus = cartHelper.addPromotrayCouponToBasket(req, currentBasket, couponCode, campaignBased);
    res.json({
      status: couponStatus,
      message: Resource.msgf('cart.' + couponStatus.code, 'checkout', null, couponCode),
      success: !couponStatus.error,
      baskettotal: currentBasket.adjustedMerchandizeTotalGrossPrice.value,
      CouponCode: couponCode
    });
  }
  return next();
});

server.replace('AddCoupon',
  csrfProtection.validateAjaxRequest,
  server.middleware.https,
  csrfProtection.validateAjaxRequest,
  function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Transaction = require('dw/system/Transaction');
    var URLUtils = require('dw/web/URLUtils');
    var CartModel = require('*/cartridge/models/cart');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

    var currentBasket = BasketMgr.getCurrentBasket();

    if (!currentBasket) {
      res.setStatusCode(500);
      res.json({
        error: true,
        redirectUrl: URLUtils.url('Cart-Show').toString()
      });

      return next();
    }

    if (!currentBasket) {
      res.setStatusCode(500);
      res.json({
        errorMessage: Resource.msg('error.add.coupon', 'cart', null)
      });
      return next();
    }
    // Setting up the Payment Type Session attribute so that promotions dependent on Regular Payment Methods (Customer Group RegularTenderUsers) always apply in the cart
    if (req.session.raw.custom.isHBCTenderType === null || req.session.raw.custom.isHBCTenderType === undefined) {
      // eslint-disable-next-line no-param-reassign
      req.session.raw.custom.isHBCTenderType = false;
    }

    var error = false;
    var errorMessage;
    var cpnCode = req.querystring.couponCode.trim().toUpperCase();

    try {
      Transaction.wrap(function () {
        return currentBasket.createCouponLineItem(cpnCode, true);
      });
    } catch (e) {
      error = true;
      var errorCodes = {
        COUPON_CODE_ALREADY_IN_BASKET: 'error.coupon.already.in.cart',
        COUPON_ALREADY_IN_BASKET: 'error.coupon.cannot.be.combined',
        COUPON_CODE_ALREADY_REDEEMED: 'error.coupon.already.redeemed',
        COUPON_CODE_UNKNOWN: 'error.unable.to.add.coupon',
        COUPON_DISABLED: 'error.unable.to.add.coupon',
        REDEMPTION_LIMIT_EXCEEDED: 'error.limit.exceeded',
        TIMEFRAME_REDEMPTION_LIMIT_EXCEEDED: 'error.limit.exceeded',
        NO_ACTIVE_PROMOTION: 'error.unable.to.add.coupon',
        default: 'error.unable.to.add.coupon'
      };

      var errorMessageKey = errorCodes[e.errorCode] || errorCodes.default;
      errorMessage = Resource.msg(errorMessageKey, 'cart', null);
    }

    if (error) {
      res.json({
        error: error,
        errorMessage: errorMessage
      });
      return next();
    }

    Transaction.wrap(function () {
      basketCalculationHelpers.calculateTotals(currentBasket);
    });

    var basketModel = new CartModel(currentBasket);
    basketModel.valid.error = false;
    res.json(basketModel);
    return next();
  }
);

server.replace('RemoveProductLineItem', function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var Transaction = require('dw/system/Transaction');
  var URLUtils = require('dw/web/URLUtils');
  var CartModel = require('*/cartridge/models/cart');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

  var currentBasket = BasketMgr.getCurrentBasket();

  /**
   * On a private sale, the promo price and the price book are same
   * Platform randomly sometimes considers the private-sale pricebook price and other time as a promotion discount
   * Since the promotionalDiscount was also a Price from pricebook, it is benefial to force promotional price and get the PIP to display
   */
  if (session.privacy.privateSalePromoID) {
    // setting a different session variable to not apply pricebooks to the session on the common model
    session.privacy.skipPricebookCal = true;
  }

  if (!currentBasket) {
    res.setStatusCode(500);
    res.json({
      error: true,
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });

    return next();
  }

  var isProductLineItemFound = false;
  var bonusProductsUUIDs = [];

  Transaction.wrap(function () {
    if (req.querystring.pid && req.querystring.uuid) {
      var ProductFactory = require('*/cartridge/scripts/factories/product');
      var productLineItems = currentBasket.getAllProductLineItems(req.querystring.pid);
      var bonusProductLineItems = currentBasket.bonusLineItems;
      var deletedBonusProductsIDs = [];
      var mainProdItem;
      for (var i = 0; i < productLineItems.length; i++) {
        var item = productLineItems[i];
        if (item.UUID === req.querystring.uuid) {
          if (bonusProductLineItems && bonusProductLineItems.length > 0) {
            for (var j = 0; j < bonusProductLineItems.length; j++) {
              var bonusItem = bonusProductLineItems[j];
              mainProdItem = bonusItem.getQualifyingProductLineItemForBonusProduct();
              if (mainProdItem !== null && mainProdItem.productID === item.productID) {
                bonusProductsUUIDs.push(bonusItem.UUID);
                deletedBonusProductsIDs.push(
                  ProductFactory.get({
                    pid: bonusItem.productID
                  })
                );
              }
            }
          }

          var shipmentToRemove = item.shipment;
          if (item.bonusProductLineItem) {
            // saving attribute in session, since this attribute should be independent of login and cart merge we are saving in custom against privacy
            session.custom.isBonusProRemoved = true;
            // Saving bonus line item
            var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
            cartHelper.saveBonusLineItemInfo(currentBasket, item);
          }

          var fromStoreId = require('*/cartridge/models/productLineItem/decorators/fromStoreId');
          var product = {
            pid: req.querystring.pid
          };
          var deletedItem = ProductFactory.get(product);
          fromStoreId(deletedItem, item);
          var previousQuantity = item.quantity.value;
          res.setViewData({
            deletedItem: deletedItem,
            deletedBonusProductsIDs: deletedBonusProductsIDs,
            previousQuantity: previousQuantity,
            mainProdItem: mainProdItem
          });

          currentBasket.removeProductLineItem(item);
          var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');

          cartHelper.removeCartItemFromProductList(item.product);

          if (shipmentToRemove.productLineItems.empty && !shipmentToRemove.default) {
            currentBasket.removeShipment(shipmentToRemove);
          }
          isProductLineItemFound = true;
          // changes for SFDEV-6215 -update the OMS inventory in session if the product line item from the list is removed.
          if (session.custom.omsInventory && session.custom.omsInventory.length > 0) {
            var itemRemoved = false;
            var omsInventoryArray = JSON.parse(session.custom.omsInventory);
            var inventoryList = new dw.util.ArrayList(omsInventoryArray);
            if (inventoryList && inventoryList.length > 0) {
              for (var i = 0; i < inventoryList.length; i++) {
                if (inventoryList.get(i).itemID === item.productID) {
                  inventoryList.remove(inventoryList.get(i));
                  itemRemoved = true;
                  break;
                }
              }
            }
            // update OMS inventory list in session
            if (itemRemoved) {
              var currentInventory = inventoryList.toArray();
              session.custom.omsInventory = JSON.stringify(currentInventory);
            }
          }
          break;
        }
      }
    }
    if (isProductLineItemFound && currentBasket.getAllProductQuantities().size() === 0) {
      currentBasket.removeAllPaymentInstruments();
    }
    basketCalculationHelpers.calculateTotals(currentBasket);
  });

  if (isProductLineItemFound) {
    var basketModel = new CartModel(currentBasket);

    // change for SFDEV-5099
    var updatedLineItems = 'updatedLineItems' in req.querystring ? req.querystring.updatedLineItems : null;
    if (updatedLineItems) {
      var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
      basketModel.updatedLineItems = updatedLineItems;
      basketModel.itemsHTML = renderTemplateHelper.getRenderedHtml(basketModel, 'cart/cartItems');
    }
    // change for SFDEV-6215
    basketModel.valid.error = false;
    var basketModelPlus = {
      basket: basketModel,
      toBeDeletedUUIDs: bonusProductsUUIDs
    };
    res.json(basketModelPlus);
  } else {
    res.setStatusCode(500);
    res.json({
      errorMessage: Resource.msg('error.cannot.remove.product', 'cart', null)
    });
  }
  if (session.privacy.privateSalePromoID) {
    // setting a different session variable to not apply pricebooks to the session on the common model
    delete session.privacy.skipPricebookCal;
  }
  return next();
});

server.get('SelectBonusProduct', function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();
  if (currentBasket) {
    var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
    var renderHelper = require('*/cartridge/scripts/renderTemplateHelper');
    var bonusModel = cartHelper.selectBonusProduct(currentBasket);
    var renderedTemplate = renderHelper.getRenderedHtml(bonusModel, 'cart/productCard/selectBonusProduct');
    res.json({
      bonusModel: bonusModel,
      renderedTemplate: renderedTemplate
    });
  }
  next();
});

server.replace('AddBonusProducts', function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var ProductMgr = require('dw/catalog/ProductMgr');
  var Transaction = require('dw/system/Transaction');
  var currentBasket = BasketMgr.getCurrentOrNewBasket();
  var data = JSON.parse(req.form.pids);
  var pliUUID = req.form.pliuuid;
  var newBonusDiscountLineItems = currentBasket.getBonusDiscountLineItems();
  var qtyAllowed = data.totalQty;
  var totalQty = 0;

  for (var i = 0; i < data.bonusProducts.length; i++) {
    totalQty += data.bonusProducts[i].qty;
  }

  if (totalQty === 0) {
    res.json({
      errorMessage: Resource.msg('error.alert.choiceofbonus.no.product.selected', 'product', null),
      error: true,
      success: false
    });
  } else if (totalQty > qtyAllowed) {
    res.json({
      errorMessage: Resource.msgf('error.alert.choiceofbonus.max.quantity', 'product', null, qtyAllowed, totalQty),
      error: true,
      success: false
    });
  } else {
    var bonusDiscountLineItem = collections.find(newBonusDiscountLineItems, function (item) {
      return item.UUID === req.form.uuid;
    });

    if (currentBasket) {
      Transaction.wrap(function () {
        collections.forEach(bonusDiscountLineItem.getBonusProductLineItems(), function (dli) {
          if (dli.product) {
            currentBasket.removeProductLineItem(dli);
          }
        });

        var pli;
        data.bonusProducts.forEach(function (bonusProduct) {
          var product = ProductMgr.getProduct(bonusProduct.pid);
          var selectedOptions = bonusProduct.options;
          var optionModel = productHelper.getCurrentOptionModel(product.optionModel, selectedOptions);
          pli = currentBasket.createBonusProductLineItem(bonusDiscountLineItem, product, optionModel, null);
          pli.setQuantityValue(bonusProduct.qty);
          pli.custom.bonusProductLineItemUUID = pliUUID;
        });

        collections.forEach(currentBasket.getAllProductLineItems(), function (productLineItem) {
          if (productLineItem.UUID === pliUUID) {
            productLineItem.custom.bonusProductLineItemUUID = 'bonus'; // eslint-disable-line no-param-reassign
            productLineItem.custom.preOrderUUID = productLineItem.UUID; // eslint-disable-line no-param-reassign
          }
        });
      });
    }

    res.json({
      totalQty: currentBasket.productQuantityTotal,
      msgSuccess: Resource.msg('text.alert.choiceofbonus.addedtobasket', 'product', null),
      success: true,
      error: false
    });
  }
  next();
});

server.replace('RemoveCouponLineItem', function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var Resource = require('dw/web/Resource');
  var Transaction = require('dw/system/Transaction');
  var URLUtils = require('dw/web/URLUtils');
  var CartModel = require('*/cartridge/models/cart');
  var collections = require('*/cartridge/scripts/util/collections');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

  if (session.privacy.privateSalePromoID) {
    // setting a different session variable to not apply pricebooks to the session on the common model
    session.privacy.skipPricebookCal = true;
  }
  var currentBasket = BasketMgr.getCurrentBasket();

  if (!currentBasket) {
    res.setStatusCode(500);
    res.json({
      error: true,
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });

    return next();
  }

  var couponLineItem;

  if (currentBasket && req.querystring.uuid) {
    couponLineItem = collections.find(currentBasket.couponLineItems, function (item) {
      return item.UUID === req.querystring.uuid;
    });

    if (couponLineItem) {
      Transaction.wrap(function () {
        currentBasket.removeCouponLineItem(couponLineItem);
        basketCalculationHelpers.calculateTotals(currentBasket);
      });

      var basketModel = new CartModel(currentBasket);
      basketModel.valid.error = false;

      res.json(basketModel);
      return next();
    }
  }

  if (session.privacy.privateSalePromoID) {
    // setting a different session variable to not apply pricebooks to the session on the common model
    delete session.privacy.skipPricebookCal;
  }

  res.setStatusCode(500);
  res.json({ errorMessage: Resource.msg('error.cannot.remove.coupon', 'cart', null) });
  return next();
});

module.exports = server.exports();
