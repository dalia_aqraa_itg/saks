'use strict';

var server = require('server');
server.extend(module.superModule);

/** script module */
var refineSearch = require('*/cartridge/models/bopis/refineSearch');
var preferences = require('*/cartridge/config/preferences');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var cache = require('*/cartridge/scripts/middleware/cache');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');
var URLUtils = require('dw/web/URLUtils');
var ACTION_HOME = 'Home-Show';
var SEARCH_SHOWAJAX = 'Search-ShowAjax';
var promotionHelper = require('*/cartridge/scripts/util/promotionHelper');
var TOTAL_PAGE_SIZE = preferences.totalPageSize ? preferences.totalPageSize : 96;
var LOAD_PAGE_SIZE = preferences.defaultPageSize ? preferences.defaultPageSize : 24;
var ACTION_REFINEMENT = 'Stores-ShowStoreRefinement';

server.extend(module.superModule);

/**
 * @function
 * @description appends the parameters to the given url and returns the changed url
 * @param {string} url the url to which the parameters will be added
 * @param {Object} name of the anchor tag
 * @param {string} value the url to which the parameters will be added
 * @returns {Object} url element.
 */
function appendParamToURL(url, name, value) {
  // quit if the param already exists
  /* eslint-disable */
  if (url.indexOf(name + '=') !== -1) {
    return url;
  }
  value = decodeURIComponent(value);
  var separator = url.indexOf('?') !== -1 ? '&' : '?';
  return url + separator + name + '=' + encodeURIComponent(value);
  /* eslint-disable */
}

server.replace(
  'ShowAjax',
  cache.applyShortPromotionSensitiveCache,
  consentTracking.consent,
  function (req, res, next) {
    var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
    var result = searchHelper.search(req, res);
    var refinementActionUrl = 'Search-ShowAjax';

    if (result.searchRedirect) {
      res.redirect(result.searchRedirect);
      return next();
    }
    var productSearch = result.productSearch;
    var renderingTemplate = 'search/searchResultsNoDecorator';
    if (productSearch.isCategorySearch && !productSearch.isRefinedCategorySearch && result.categoryTemplate) {
      renderingTemplate = result.categoryTemplate;
    }
    var totalBlocks = Math.ceil(TOTAL_PAGE_SIZE / LOAD_PAGE_SIZE);
    var currentPage = productSearch.pageNumber + 1;
    var showPreviousPage =
      currentPage === totalBlocks * Math.ceil(currentPage / totalBlocks)
        ? true
        : !!(currentPage < totalBlocks * Math.ceil(currentPage / totalBlocks) && currentPage > totalBlocks * Math.floor(currentPage / totalBlocks) + 1);
    // prepare store refinement url loaded as url include in template
    var storeRefineUrl = refineSearch.getStoreRefinementUrl(req, ACTION_REFINEMENT);
    // url to reset and execute the search form the store select
    var storeRefineUrlForCheckBox = refineSearch.getStoreRefinementUrl(req, refinementActionUrl);
    var refineurl = result.refineurl;
    if ('shopPreference' in session.custom && session.custom.shopPreference) {
      refineurl = appendParamToURL(refineurl.toString(), 'shopperPreference', session.custom.shopPreference);
    }

    res.render(renderingTemplate, {
      ajax: true,
      showPreviousPage: showPreviousPage,
      showMore: currentPage <= totalBlocks * Math.ceil(currentPage / totalBlocks),
      startSize: req.querystring.start ? req.querystring.start : '',
      storeRefineUrl: storeRefineUrl,
      productSearch: productSearch,
      maxSlots: result.maxSlots,
      reportingURLs: result.reportingURLs,
      refineurl: refineurl,
      isRefinedByStore: result.isRefinedByStore,
      isBopisEnabled: preferences.isBopisEnabled,
      storeRefineUrlForCheckBox: storeRefineUrlForCheckBox
    });

    return next();
  },
  pageMetaData.computedPageMetaData
);

server.replace('UpdateGrid', cache.applyShortPromotionSensitiveCache, function (req, res, next) {
  var CatalogMgr = require('dw/catalog/CatalogMgr');
  var ProductSearchModel = require('dw/catalog/ProductSearchModel');
  var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
  var ProductSearch = require('*/cartridge/models/search/productSearch');

  var apiProductSearch = new ProductSearchModel();
  apiProductSearch = searchHelper.setupSearch(apiProductSearch, req.querystring);
  var viewData = {
    apiProductSearch: apiProductSearch
  };

  /**
   *  The Search Controller is cached  1hr and the Tile cache is 4hrs currently.
   *  Calling this on the Tile-Show, would make an expense call to PromotionMgr.activeCustomerPromotions.getProductPromotions() for almost
   *  all the requests until the initial Requests.
   *
   *  It would be a lot more performative if the Requests are reduced to 1 per category vs 1 per every product tile on all category and search page.
   *
   * This is called on Search  to reduce the number of calls from Tile-Show.
   */
  promotionHelper.setPromoCache();

  res.setViewData(viewData);

  this.on('route:BeforeComplete', function (req, res) {
    // eslint-disable-line no-shadow
    // execute custom search and return the search model
    var storeRefineResult = refineSearch.search(apiProductSearch, req.querystring);
    apiProductsearch = storeRefineResult.apiProductsearch; // eslint-disable-line

    if (!apiProductSearch.personalizedSort) {
      searchHelper.applyCache(res);
    }
    var productSearch = new ProductSearch(
      apiProductSearch,
      req.querystring,
      req.querystring.srule,
      CatalogMgr.getSortingOptions(),
      CatalogMgr.getSiteCatalog().getRoot()
    );

    var totalBlocks = Math.ceil(TOTAL_PAGE_SIZE / LOAD_PAGE_SIZE);
    var currentPage = productSearch.pageNumber;
    var showPreviousPage = !!(
      currentPage < totalBlocks * Math.ceil(currentPage / totalBlocks) && currentPage > totalBlocks * Math.floor(currentPage / totalBlocks)
    );

    // SFDEV-2503 || Fix for Gift card PLP ajax loading product grid count
    var template = 'search/components/categoryProductGrid';
    if (productSearch.category != null && productSearch.category.template === 'rendering/category/producthitsgiftcards') {
      template = 'search/productGiftGrid';
    }
    res.render(template, {
      productSearch: productSearch,
      startSize: req.querystring.start ? req.querystring.start : '',
      // eslint-disable-next-line no-nested-ternary
      showMore:
        req.querystring.showMore || req.querystring.previous
          ? currentPage === totalBlocks * Math.ceil(currentPage / totalBlocks)
            ? false
            : !req.querystring.previous
          : true,
      showPreviousPage: showPreviousPage && !!req.querystring.previous && !req.querystring.showMore
    });
  });

  next();
});

// eslint-disable-next-line consistent-return
server.replace(
  'Show',
  cache.applyShortPromotionSensitiveCache,
  consentTracking.consent,
  function (req, res, next) {
    var ProductSearchModel = require('dw/catalog/ProductSearchModel');
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var ContentMgr = require('dw/content/ContentMgr');
    var template = 'search/searchResults';

    var apiProductSearch = new ProductSearchModel();
    var viewData = {
      apiProductSearch: apiProductSearch
    };
    res.setViewData(viewData);
    var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
    var Resource = require('dw/web/Resource');

    // set store search url
    var storeRefineUrl = refineSearch.getStoreRefinementUrl(req, ACTION_REFINEMENT);
    var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
    var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');
    var result = searchHelper.search(req, res);
    var Site = require('dw/system/Site');
    var siteName = Site.getCurrent().getName();
    var hasBrand = req.querystring.preferences && req.querystring.preferences.brand;
    var isSale = req.querystring.preferences && req.querystring.preferences.isSale;
    var isClearance = req.querystring.preferences && req.querystring.preferences.isClearance;
    let refinementTitle = '';
    var doNotReset = req.querystring && req.querystring.doNotReset;
    var brandMetaTag = {};
    if (!!hasBrand) {
      // eslint-disable-line
      if (result.tempCategory && result.tempCategory.ID !== 'root') {
        pageMetaHelper.setPageMetaData(req.pageMetaData, result.productSearch);
        pageMetaHelper.setPageMetaTags(req.pageMetaData, result.productSearch);
      } else {
        brandMetaTag.pageTitle = req.querystring.preferences.brand + ' | ' + siteName; // eslint-disable-line
        brandMetaTag.pageDescription = brandMetaTag.pageTitle;
        brandMetaTag.keywords = brandMetaTag.pageTitle;
      }
      pageMetaHelper.setPageMetaData(req.pageMetaData, brandMetaTag);
      pageMetaHelper.setPageMetaTags(req.pageMetaData, brandMetaTag);
    } else if (result.category && result.category.ID == 'root') {
      var designerSEO = ContentMgr.getContent('seo-designer');
      if (designerSEO) {
        pageMetaHelper.setPageMetaData(req.pageMetaData, designerSEO);
        pageMetaHelper.setPageMetaTags(req.pageMetaData, designerSEO);
      }
    } else if (!empty(req.querystring.q)) {
      var searchObj = {};
      searchObj.pageTitle = req.querystring.q + ' | ' + siteName;
      searchObj.pageDescription = req.querystring.q + ' | ' + siteName;
      pageMetaHelper.setPageMetaData(req.pageMetaData, searchObj);
      pageMetaHelper.setPageMetaTags(req.pageMetaData, searchObj);
    } else if (isSale && req.querystring && req.querystring.cgid !== preferences.saleCategoryID) {
      // The conditions are separated at  line #179 for isClearance. It is separated to give preference to isSale
      let titleObj = {};
      // eslint-disable-next-line no-nested-ternary
      refinementTitle = Resource.msg('refinement.sale.label', 'common', null);
      if ('pageMetaTags' in result.productSearch) {
        result.productSearch.pageMetaTags.forEach(function (item) {
          if (item.title) {
            titleObj.pageTitle = refinementTitle + ' ' + item.content;
          } else if (item.name && item.ID === 'description') {
            titleObj.pageDescription = item.content;
          } else if (item.name && item.ID === 'keywords') {
            titleObj.pageKeywords = item.content;
          }
        });
      }
      pageMetaHelper.setPageMetaData(req.pageMetaData, titleObj);
      pageMetaHelper.setPageMetaTags(req.pageMetaData, titleObj);
    } else if (isClearance && req.querystring && req.querystring.cgid !== preferences.clearanceCategoryID) {
      let titleObj = {};
      refinementTitle = Resource.msg('refinement.clearance.label', 'common', null);
      if ('pageMetaTags' in result.productSearch) {
        result.productSearch.pageMetaTags.forEach(function (item) {
          if (item.title) {
            titleObj.pageTitle = refinementTitle + ' ' + item.content;
          } else if (item.name && item.ID === 'description') {
            titleObj.pageDescription = item.content;
          } else if (item.name && item.ID === 'keywords') {
            titleObj.pageKeywords = item.content;
          }
        });
      }
      pageMetaHelper.setPageMetaData(req.pageMetaData, titleObj);
      pageMetaHelper.setPageMetaTags(req.pageMetaData, titleObj);
    } else if (result.productSearch) {
      pageMetaHelper.setPageMetaData(req.pageMetaData, result.productSearch);
      pageMetaHelper.setPageMetaTags(req.pageMetaData, result.productSearch);
    }
    // url to reset and execute the search form the store select
    var storeRefineUrlForCheckBox = refineSearch.getStoreRefinementUrl(req, SEARCH_SHOWAJAX);

    /**
     *  The Search Controller is cached  1hr and the Tile cache is 4hrs currently.
     *  Calling this on the Tile-Show, would make an expense call to PromotionMgr.activeCustomerPromotions.getProductPromotions() for almost
     *  all the requests until the initial Requests.
     *
     *  It would be a lot more performative if the Requests are reduced to 1 per category vs 1 per every product tile on all category and search page.
     *
     * This is called on Search  to reduce the number of calls from Tile-Show.
     */
    promotionHelper.setPromoCache();

    this.on('route:BeforeComplete', function (req, res) {
      // eslint-disable-line
      if (result.searchRedirect) {
        res.redirect(result.searchRedirect);
        return;
      }

      // redirect to error if any refinement is applied without these parameters
      if (!req.querystring.cgid && !req.querystring.q && !req.querystring.pid && !req.querystring.pmid && req.querystring.q != '') {
        res.redirect(URLUtils.https('Home-ErrorNotFound').toString());
        return;
      }
      //  SFDEV-3056 | Issue for redirecting to PDP in case only one product is found
      if (result.productSearch.count === 1 && !req.querystring.ajax && !hasBrand) {
        let product = result.productSearch.productIds[0].productID;
        res.redirect(URLUtils.url('Product-Show', 'pid', product));
        return;
      }
      if (result.category) {
        template = result.categoryTemplate || 'rendering/category/producthits';
      }

      if (result.category && result.category.ID === 'root' && !(isSale || isClearance)) {
        template = 'rendering/category/designerbay';
      }
      // if the category is root, then set the brand as the category
      var categoryOnBrandPage;
      if (result.tempCategory) {
        categoryOnBrandPage = result.tempCategory.ID;
        if (result.tempCategory.ID === 'root') {
          categoryOnBrandPage = 'brand';
        }
      }

      // reset the clear All to fetch the results only with the brand in selection and not wipe brand selection
      // As brand selection does not show up on the filter bar for the brand pages.
      if (!doNotReset && hasBrand && hasBrand.indexOf('|') === -1) {
        result.productSearch.resetLink = URLUtils.https('Search-Show', 'cgid', categoryOnBrandPage, 'prefn1', 'brand', 'prefv1', hasBrand);
      }
      var refineurl = result.refineurl;
      if ('shopPreference' in session.custom && session.custom.shopPreference) {
        refineurl = appendParamToURL(refineurl.toString(), 'shopperPreference', session.custom.shopPreference);
      }
      res.render(template, {
        productSearch: result.productSearch,
        isRootCategory: (result.category && result.category.root) || (result.tempCategory && result.tempCategory.root),
        maxSlots: result.maxSlots,
        isSale: isSale,
        isClearance: isClearance,
        clearanceCategoryID: preferences.clearanceCategoryID,
        saleCategoryID: preferences.saleCategoryID,
        reportingURLs: result.reportingURLs,
        refineurl: refineurl,
        category: result.category ? result.category : result.tempCategory,
        canonicalUrl: result.canonicalUrl,
        schemaData: result.schemaData,
        isRefinedByStore: result.isRefinedByStore,
        storeRefineUrl: storeRefineUrl,
        storeRefineUrlForCheckBox: storeRefineUrlForCheckBox,
        showPriceFilter: req.querystring.pmin || req.querystring.pmax
      });
      var designerObject = {};
      viewData = res.getViewData();
      var productSearch = viewData.productSearch;
      var breadcrumbs;
      var clearanceCatDisplayName;
      var saleCatDisplayName;
      // logic to include recaptcha JS
      var includeRecaptchaJS =
        productSearch.isCategorySearch && productSearch.category !== null && productSearch.category.template === 'rendering/category/catLanding';

      // Search for Keyword banner content asset using the mapping saved in the "KeywordSearchBanner" Custom Object.
      var searchPromo;
      if (!empty(req.querystring.q)) {
        var searchKeyword = req.querystring.q.toLowerCase();
        searchKeyword = '*|' + searchKeyword + '|*';
        var CustomObjectMgr = require('dw/object/CustomObjectMgr');
        var searchBannerCO = CustomObjectMgr.queryCustomObject('KeywordSearchBanner', 'custom.keywords ILIKE {0}', searchKeyword);
        if (searchBannerCO) {
          if (!empty(searchBannerCO.custom.contentAssetId)) {
            searchPromo = ContentMgr.getContent(searchBannerCO.custom.contentAssetId);
          }
        }
      }

      // designer category changes
      if (result.category && result.category.ID === 'root' && !(isSale || isClearance)) {
        var alphaNums = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '#'];
        designerObject.results = searchHelper.searchDesigner(productSearch);
        designerObject.alphaNums = alphaNums;
      }

      if (req.querystring && req.querystring.preferences && req.querystring.preferences['brand'] && req.querystring.cgid) {
        // eslint-disable-line
        // req.querystring.preferences['brand'] does not work in dot notation
        breadcrumbs = [];

        if (req.querystring.cgid !== 'brand' && req.querystring.cgid !== 'root') {
          breadcrumbs = productHelper.getAllBreadcrumbs(req.querystring.cgid, null, breadcrumbs);
        }
        if (req.querystring.preferences['brand'].indexOf('|') === -1) {
          // eslint-disable-line
          breadcrumbs.push({
            htmlValue: req.querystring.preferences['brand'], // eslint-disable-line
            url: URLUtils.https('Search-Show', 'cgid', 'brand', 'prefn1', 'brand', 'prefv1', hasBrand)
          });
        }

        breadcrumbs.push({
          htmlValue: Resource.msg('label.search.home', 'search', null),
          url: URLUtils.url(ACTION_HOME)
        });
        breadcrumbs.reverse();
      } else if (req.querystring && req.querystring.preferences && req.querystring.preferences.isSale && req.querystring.cgid != preferences.saleCategoryID) {
        breadcrumbs = [];

        if (req.querystring.cgid !== 'brand' && req.querystring.cgid !== 'root') {
          breadcrumbs = productHelper.getAllBreadcrumbs(req.querystring.cgid, null, breadcrumbs);
        }
        var saleCategory = CatalogMgr.getCategory(preferences.saleCategoryID);
        if (saleCategory) {
          saleCatDisplayName = saleCategory.displayName;
          breadcrumbs.push({
            htmlValue: saleCategory.displayName, // eslint-disable-line
            url: searchHelper.getCategoryUrl(saleCategory)
          });
        }

        breadcrumbs.push({
          htmlValue: Resource.msg('label.search.home', 'search', null),
          url: URLUtils.url(ACTION_HOME)
        });
        breadcrumbs.reverse();
      } else if (
        req.querystring &&
        req.querystring.preferences &&
        req.querystring.preferences.isClearance &&
        req.querystring.cgid != preferences.clearanceCategoryID
      ) {
        breadcrumbs = [];

        if (req.querystring.cgid !== 'brand' && req.querystring.cgid !== 'root') {
          breadcrumbs = productHelper.getAllBreadcrumbs(req.querystring.cgid, null, breadcrumbs);
        }
        var clearanceCategory = CatalogMgr.getCategory(preferences.clearanceCategoryID);
        if (clearanceCategory) {
          clearanceCatDisplayName = clearanceCategory.displayName;
          breadcrumbs.push({
            htmlValue: clearanceCategory.displayName, // eslint-disable-line
            url: searchHelper.getCategoryUrl(clearanceCategory)
          });
        }

        breadcrumbs.push({
          htmlValue: Resource.msg('label.search.home', 'search', null),
          url: URLUtils.url(ACTION_HOME)
        });
        breadcrumbs.reverse();
      } else if (productSearch.category) {
        var categoryId = productSearch.category.id;
        breadcrumbs = productHelper.getAllBreadcrumbs(categoryId, null, []);
        breadcrumbs.push({
          htmlValue: Resource.msg('label.search.home', 'search', null),
          url: URLUtils.url(ACTION_HOME)
        });
        breadcrumbs.reverse();
      } else if (productSearch.searchKeywords) {
        breadcrumbs = productHelper.getAllBreadcrumbs(null, null, [
          {
            htmlValue: Resource.msg('label.search.home', 'search', null),
            url: URLUtils.url(ACTION_HOME)
          },
          {
            htmlValue: Resource.msg('label.search.all.results', 'search', null),
            url: null
          }
        ]);
      }
      var totalBlocks = Math.ceil(TOTAL_PAGE_SIZE / LOAD_PAGE_SIZE);
      var currentPage = productSearch.pageNumber + 1;
      var showPreviousPage =
        currentPage === totalBlocks * Math.ceil(currentPage / totalBlocks)
          ? true
          : !!(currentPage < totalBlocks * Math.ceil(currentPage / totalBlocks) && currentPage > totalBlocks * Math.floor(currentPage / totalBlocks) + 1);
      // Logic to retrieve category taxonomy for Chanel CLP/listing page
      var chanelCategory;
      var chanelCategories = [];
      if (
        productSearch.isCategorySearch &&
        productSearch.category !== null &&
        (productSearch.category.template === 'rendering/category/categorylandingchanel' ||
          productSearch.category.template === 'rendering/category/producthitschanel')
      ) {
        var categoryFromProductSearch = CatalogMgr.getCategory(productSearch.category.id);
        if (categoryFromProductSearch) {
          chanelCategory = searchHelper.getL1CategoryDetails(categoryFromProductSearch);
        }
        if (chanelCategory) {
          if (chanelCategory.hasOnlineSubCategories()) {
            chanelCategories = searchHelper.categories(chanelCategory.onlineSubCategories);
          }
        }
      }
      viewData = {
        ajax: !!req.querystring.ajax,
        designerObject: designerObject,
        hasBrandRefinement: !!hasBrand,
        brandValue: hasBrand || '',
        clearanceCatDisplayName: clearanceCatDisplayName,
        saleCatDisplayName: saleCatDisplayName,
        showPreviousPage: showPreviousPage,
        showMore: currentPage <= totalBlocks * Math.ceil(currentPage / totalBlocks),
        breadcrumbs: breadcrumbs,
        startSize: req.querystring.start ? req.querystring.start : '',
        includeRecaptchaJS: includeRecaptchaJS,
        chanelCategories: chanelCategories,
        SearchPromo: searchPromo,
        searchTerm: req.querystring.term,
        searchType: req.querystring.type,
        chanelCatUrl: chanelCategory != null ? URLUtils.https('Search-Show', 'cgid', chanelCategory.ID).toString() : '',
        searchPageClass:
          !productSearch.isRefinedCategorySearch && (preferences.catRefinementLevel === 2 || !preferences.catRefinementLevel) ? 'search-results-page' : ''
      };
      res.setViewData(viewData);
    });

    viewData = {
      previousWord: req.querystring.prevQ,
      searchWord: req.querystring.q,
      searchSuggestion: !!req.querystring.ss,
      s7APIForPDPZoomViewer: preferences.s7APIForPDPZoomViewer,
      s7APIForPDPVideoPlayer: preferences.s7APIForPDPVideoPlayer,
      displayQuickLook: preferences.displayQuickLook ? preferences.displayQuickLook : '',
      isBopisEnabled: preferences.isBopisEnabled,
      promoTrayEnabled: preferences.promoTrayEnabled,
      src: req.querystring.src
    };
    viewData.storeRefineUrl = storeRefineUrl;
    res.setViewData(viewData);
    next();
  },
  pageMetaData.computedPageMetaData
);

/** ROUTE REPLACED : to add store refinement in refinement***
 ****custom search with store price book set to session***
 ****if store id is found in the query***/
server.replace(
  'Refinebar',
  cache.applyShortPromotionSensitiveCache,
  function (req, res, next) {
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var ProductSearchModel = require('dw/catalog/ProductSearchModel');
    var ProductSearch = require('*/cartridge/models/search/productSearch');
    var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
    var maxRefinementValueLimit = preferences.refinementValueMaxLimit ? preferences.refinementValueMaxLimit : 8;
    var viewData = res.getViewData();
    var hasBrand = req.querystring.preferences && req.querystring.preferences.brand;

    var apiProductSearch = new ProductSearchModel();
    apiProductSearch = searchHelper.setupSearch(apiProductSearch, req.querystring);

    // if the search key has chanel, then skip the refinement of non-chanel products.
    if (req.querystring && req.querystring.q && req.querystring.q.toLowerCase().indexOf('chanel') === -1) {
      if (preferences.nonChanelProductTypes) {
        apiProductSearch.addRefinementValues('hbcProductType', preferences.nonChanelProductTypes);
      }
    }
    // PSM search with store refinement
    var storeRefineResult = refineSearch.search(apiProductSearch, req.querystring);
    apiProductsearch = storeRefineResult.apiProductsearch; // eslint-disable-line
    var productSearch = new ProductSearch(
      apiProductSearch,
      req.querystring,
      req.querystring.srule,
      CatalogMgr.getSortingOptions(),
      CatalogMgr.getSiteCatalog().getRoot()
    );
    viewData = {
      maxRefinementValueLimit: maxRefinementValueLimit,
      plpRefineViewMoreCatLimit: preferences.plpRefineViewMoreCatLimit
    };
    res.setViewData(viewData);
    res.render('/search/searchRefineBar', {
      productSearch: productSearch,
      querystring: req.querystring,
      isRefinedByStore: storeRefineResult.isRefinedByStore,
      searchSource: storeRefineResult.isRefinedByStore ? 'search' : 'store',
      hasBrandRefinement: !!hasBrand,
      brandValue: hasBrand || ''
    });

    next();
  },
  pageMetaData.computedPageMetaData
);

module.exports = server.exports();
