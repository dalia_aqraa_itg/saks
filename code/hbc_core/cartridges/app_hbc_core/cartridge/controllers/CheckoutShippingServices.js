'use strict';

var server = require('server');
server.extend(module.superModule);

var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var Vertex = require('int_vertex/cartridge/scripts/Vertex');
var EmailSubscribeHelper = require('*/cartridge/scripts/helpers/EmailSubscribeHelper');
var HashMap = require('dw/util/HashMap');
var preferences = require('*/cartridge/config/preferences');

server.replace('ToggleMultiShip', server.middleware.https, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var Transaction = require('dw/system/Transaction');
  var AccountModel = require('*/cartridge/models/account');
  var OrderModel = require('*/cartridge/models/order');
  var URLUtils = require('dw/web/URLUtils');
  var collections = require('*/cartridge/scripts/util/collections');
  var Locale = require('dw/util/Locale');
  var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var shippingHelpers = require('*/cartridge/scripts/checkout/shippingHelpers');

  var currentBasket = BasketMgr.getCurrentBasket();
  if (!currentBasket) {
    res.json({
      error: true,
      cartError: true,
      fieldErrors: [],
      serverErrors: [],
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return;
  }

  var shipments = currentBasket.shipments;
  var defaultShipment = currentBasket.defaultShipment;
  var usingMultiShipping = req.form.usingMultiShip === 'true';

  req.session.privacyCache.set('usingMultiShipping', usingMultiShipping);

  if (usingMultiShipping) {
    var UUIDUtils = require('dw/util/UUIDUtils');
    // split line items into separate shipments
    Transaction.wrap(function () {
      collections.forEach(shipments, function (shipment) {
        if (shipment.productLineItems.length > 1) {
          collections.forEach(shipment.productLineItems, function (lineItem) {
            var uuid = UUIDUtils.createUUID();
            var newShipment = currentBasket.createShipment(uuid);
            // only true if customer is registered
            if (req.currentCustomer.addressBook && req.currentCustomer.addressBook.preferredAddress) {
              var preferredAddress = req.currentCustomer.addressBook.preferredAddress;
              COHelpers.copyCustomerAddressToShipment(preferredAddress, newShipment);
            }

            shippingHelpers.selectShippingMethod(newShipment);
            lineItem.setShipment(newShipment);
          });
        }
      });

      shippingHelpers.selectShippingMethod(defaultShipment);
      defaultShipment.createShippingAddress();

      COHelpers.ensureNoEmptyShipments(req);

      basketCalculationHelpers.calculateTotals(currentBasket);
    });
  } else {
    // combine multiple shipments into a single one
    Transaction.wrap(function () {
      collections.forEach(shipments, function (shipment) {
        if (!shipment.default) {
          collections.forEach(shipment.productLineItems, function (lineItem) {
            lineItem.setShipment(defaultShipment);
          });
          currentBasket.removeShipment(shipment);
        }
      });

      shippingHelpers.selectShippingMethod(defaultShipment);
      defaultShipment.createShippingAddress();

      COHelpers.ensureNoEmptyShipments(req);

      if (req.currentCustomer.addressBook && req.currentCustomer.addressBook.preferredAddress) {
        var preferredAddress = req.currentCustomer.addressBook.preferredAddress;
        COHelpers.copyCustomerAddressToShipment(preferredAddress);
      }

      basketCalculationHelpers.calculateTotals(currentBasket);
    });
  }

  var currentLocale = Locale.getLocale(req.locale.id);

  var basketModel = new OrderModel(currentBasket, { usingMultiShipping: usingMultiShipping, countryCode: currentLocale.country, containerView: 'basket' });

  res.json({
    customer: new AccountModel(req.currentCustomer),
    order: basketModel
  });

  next();
});

server.replace('SelectShippingMethod', server.middleware.https, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var Resource = require('dw/web/Resource');
  var Transaction = require('dw/system/Transaction');
  var AccountModel = require('*/cartridge/models/account');
  var OrderModel = require('*/cartridge/models/order');
  var URLUtils = require('dw/web/URLUtils');
  var ShippingHelper = require('*/cartridge/scripts/checkout/shippingHelpers');
  var Locale = require('dw/util/Locale');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');

  var currentBasket = BasketMgr.getCurrentBasket();

  if (!currentBasket) {
    res.json({
      error: true,
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }

  var shipmentUUID = req.querystring.shipmentUUID || req.form.shipmentUUID;
  var shippingMethodID = req.querystring.methodID || req.form.methodID;
  var shipment;
  if (shipmentUUID) {
    shipment = ShippingHelper.getShipmentByUUID(currentBasket, shipmentUUID);
  } else {
    shipment = currentBasket.defaultShipment;
  }

  var viewData = res.getViewData();
  viewData.address = ShippingHelper.getAddressFromRequest(req);
  viewData.isGift = req.form.isGift === 'true';
  viewData.giftMessage = req.form.isGift ? req.form.giftMessage : null;
  viewData.giftRecipientName = req.form.isGift ? req.form.giftRecipientName : null;
  viewData.giftWrapType = req.form.addgift ? req.form.addgift : null;
  res.setViewData(viewData);

  this.on('route:BeforeComplete', function (req, res) {
    // eslint-disable-line no-shadow
    var shippingData = res.getViewData();
    var address = shippingData.address;
    var signatureRequired = false;

    try {
      Transaction.wrap(function () {
        var shippingAddress = shipment.shippingAddress;

        if (!shippingAddress) {
          shippingAddress = shipment.createShippingAddress();
        }

        shippingAddress.setFirstName(address.firstName || '');
        shippingAddress.setLastName(address.lastName || '');
        shippingAddress.setAddress1(address.address1 || '');
        shippingAddress.setAddress2(address.address2 || '');
        shippingAddress.setCity(address.city || '');
        shippingAddress.setPostalCode(address.postalCode || '');
        shippingAddress.setStateCode(address.stateCode || '');
        shippingAddress.setCountryCode(address.countryCode || '');
        shippingAddress.setPhone(address.phone || '');

        ShippingHelper.selectShippingMethod(shipment, shippingMethodID);

        // Apply Shipping Cost to Avoid Multiple Tax Call.
        var HookMgr = require('dw/system/HookMgr');
        HookMgr.callHook('dw.order.calculateShipping', 'calculateShipping', currentBasket);
        currentBasket.updateTotals();

        // Apply Shipping Cost to Avoid Multiple Tax Call.
        var HookMgr = require('dw/system/HookMgr');
        HookMgr.callHook('dw.order.calculateShipping', 'calculateShipping', currentBasket);
        currentBasket.updateTotals();

        basketCalculationHelpers.calculateTotals(currentBasket);
      });
    } catch (err) {
      res.setStatusCode(500);
      res.json({
        error: true,
        errorMessage: Resource.msg('error.cannot.select.shipping.method', 'cart', null)
      });

      return;
    }

    COHelpers.setGift(shipment, shippingData.isGift, shippingData.giftMessage, shippingData.giftRecipientName, shippingData.giftWrapType);

    // check if the basket has drop ship items
    var hasDropShipItems = COHelpers.hasDropShipItems(currentBasket);

    var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
    var currentLocale = Locale.getLocale(req.locale.id);

    var basketModel = new OrderModel(currentBasket, { usingMultiShipping: usingMultiShipping, countryCode: currentLocale.country, containerView: 'basket' });
    // if signature required
    signatureRequired = preferences.signatureRequired && basketModel.items.signatureRequired;
    res.json({
      customer: new AccountModel(req.currentCustomer),
      order: basketModel,
      signatureRequired: signatureRequired,
      signatureThreshold: preferences.signatureOrderThreshold,
      hasDropShipItems: hasDropShipItems
    });
  });

  return next();
});

server.replace('UpdateShippingMethodsList', server.middleware.https, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var Transaction = require('dw/system/Transaction');
  var AccountModel = require('*/cartridge/models/account');
  var OrderModel = require('*/cartridge/models/order');
  var URLUtils = require('dw/web/URLUtils');
  var ShippingHelper = require('*/cartridge/scripts/checkout/shippingHelpers');
  var Locale = require('dw/util/Locale');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var EDDFacade = require('*/cartridge/scripts/services/EDDFacade');

  var currentBasket = BasketMgr.getCurrentBasket();

  if (!currentBasket) {
    res.json({
      error: true,
      cartError: true,
      fieldErrors: [],
      serverErrors: [],
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }

  let shopRunnerEnabled = require('*/cartridge/scripts/helpers/shopRunnerHelpers').checkSRExpressCheckoutEligibility(currentBasket);

  if (shopRunnerEnabled) {
    var shoprunnerShippingMethodSelection = require('*/cartridge/scripts/ShoprunnerShippingMethodSelection').ShoprunnerShippingMethodSelection;
    shoprunnerShippingMethodSelection();
  }

  var shipmentUUID = req.querystring.shipmentUUID || req.form.shipmentUUID;
  var shipment;
  if (shipmentUUID) {
    shipment = ShippingHelper.getShipmentByUUID(currentBasket, shipmentUUID);
  } else {
    shipment = currentBasket.defaultShipment;
  }

  var address = ShippingHelper.getAddressFromRequest(req);

  var shippingMethodID;

  if (shipment.shippingMethod) {
    shippingMethodID = shipment.shippingMethod.ID;
  }

  Transaction.wrap(function () {
    var shippingAddress = shipment.shippingAddress;

    if (!shippingAddress) {
      shippingAddress = shipment.createShippingAddress();
    }

    Object.keys(address).forEach(function (key) {
      var value = address[key];
      if (value) {
        shippingAddress[key] = value;
      } else {
        shippingAddress[key] = null;
      }
    });

    ShippingHelper.selectShippingMethod(shipment, shippingMethodID);

    basketCalculationHelpers.calculateTotals(currentBasket);
  });

  // Call EDD to get the Estimated date.
  EDDFacade.getEDDDate(shipment);

  var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
  var currentLocale = Locale.getLocale(req.locale.id);

  var basketModel = new OrderModel(currentBasket, { usingMultiShipping: usingMultiShipping, countryCode: currentLocale.country, containerView: 'basket' });

  res.json({
    customer: new AccountModel(req.currentCustomer),
    order: basketModel,
    shippingForm: server.forms.getForm('shipping')
  });

  return next();
});

/**
 * Handle Ajax shipping form submit
 */
server.replace('SubmitShipping', server.middleware.https, csrfProtection.validateAjaxRequest, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var URLUtils = require('dw/web/URLUtils');
  var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
  var validationHelpers = require('*/cartridge/scripts/helpers/basketValidationHelpers');
  var Transaction = require('dw/system/Transaction');
  var Locale = require('dw/util/Locale');
  var currentLocale = Locale.getLocale(req.locale.id);

  var currentBasket = BasketMgr.getCurrentBasket();
  var validatedProducts = validationHelpers.validateProducts(currentBasket);

  delete session.custom.expiredPromo;

  if (!currentBasket || validatedProducts.error) {
    res.json({
      error: true,
      cartError: true,
      fieldErrors: [],
      serverErrors: [],
      redirectUrl: URLUtils.url('Cart-Show').toString()
    });
    return next();
  }

  var form = server.forms.getForm('shipping');
  var result = {};

  let shopRunnerEnabled = require('*/cartridge/scripts/helpers/shopRunnerHelpers').checkSRExpressCheckoutEligibility(currentBasket);

  if (shopRunnerEnabled) {
    var shoprunnerShippingMethodSelection = require('*/cartridge/scripts/ShoprunnerShippingMethodSelection').ShoprunnerShippingMethodSelection;
    shoprunnerShippingMethodSelection();
  }

  // verify shipping form data
  var shippingFormErrors = COHelpers.validateShippingForm(form.shippingAddress);

  // Vertax Specific code
  var resultVertex = Vertex.LookupTaxAreas(form, currentBasket); // eslint-disable-line

  if (Object.keys(shippingFormErrors).length > 0) {
    req.session.privacyCache.set(currentBasket.defaultShipment.UUID, 'invalid');

    res.json({
      form: form,
      fieldErrors: [shippingFormErrors],
      serverErrors: [],
      error: true
    });
  } else if (!resultVertex) {
    res.json({
      form: form,
      vertexError: true,
      vertexAddressSuggestions: session.custom.VertexAddressSuggestions ? JSON.parse(session.custom.VertexAddressSuggestions) : null
    });
  } else {
    req.session.privacyCache.set(currentBasket.defaultShipment.UUID, 'valid');

    result.address = {
      firstName: form.shippingAddress.addressFields.firstName.value,
      lastName: form.shippingAddress.addressFields.lastName.value,
      address1: form.shippingAddress.addressFields.address1.value,
      address2: form.shippingAddress.addressFields.address2.value,
      city: form.shippingAddress.addressFields.city.value,
      postalCode: form.shippingAddress.addressFields.postalCode.value,
      countryCode: currentLocale.country, // country field is absent in shipping form
      phone: form.shippingAddress.addressFields.phone.value
    };
    if (Object.prototype.hasOwnProperty.call(form.shippingAddress.addressFields, 'states')) {
      result.address.stateCode = form.shippingAddress.addressFields.states.stateCode.value;
    }

    result.shippingBillingSame = form.shippingAddress.shippingAddressUseAsBillingAddress.value;

    result.shippingMethod = form.shippingAddress.shippingMethodID.value ? form.shippingAddress.shippingMethodID.value.toString() : null;

    result.isGift = form.shippingAddress.isGift.checked;
    result.signatureRequired = form.shippingAddress.signatureRequired.checked;
    result.giftMessage = result.isGift ? form.shippingAddress.giftMessage.value : null;
    result.giftRecipientName = result.isGift ? form.shippingAddress.giftRecipientName.value : null;

    res.setViewData(result);

    this.on('route:BeforeComplete', function (req, res) {
      // eslint-disable-line no-shadow
      var AccountModel = require('*/cartridge/models/account');
      var OrderModel = require('*/cartridge/models/order');
      var basketHashHelper = require('*/cartridge/scripts/helpers/basketHashHelpers');
      var shippingData = res.getViewData();

      COHelpers.copyShippingAddressToShipment(shippingData, currentBasket.defaultShipment);
      Transaction.wrap(function () {
        currentBasket.setCustomerEmail(server.forms.getForm('shipping').shippingAddress.email.value);
        currentBasket.defaultShipment.shippingAddress.custom.customerEmail = server.forms.getForm('shipping').shippingAddress.email.value;
      });

      var giftResult = COHelpers.setGift(currentBasket.defaultShipment, shippingData.isGift, shippingData.giftMessage, shippingData.giftRecipientName);
      // set signature at shipment
      COHelpers.setSignatureRequired(currentBasket.defaultShipment, shippingData.signatureRequired);

      if (giftResult.error) {
        res.json({
          error: giftResult.error,
          fieldErrors: [],
          serverErrors: [giftResult.errorMessage]
        });
        return;
      }

      /*if (!currentBasket.billingAddress || !currentBasket.billingAddress.address1) {
                    if (req.currentCustomer.addressBook
                        && req.currentCustomer.addressBook.preferredAddress) {
                        // Copy over preferredAddress (use addressUUID for matching)
                        COHelpers.copyBillingAddressToBasket(
                            req.currentCustomer.addressBook.preferredAddress, currentBasket);
                    } else {
                        // Copy over first shipping address (use shipmentUUID for matching)
                        COHelpers.copyBillingAddressToBasket(
                            currentBasket.defaultShipment.shippingAddress, currentBasket);
                    }
                }*/

      // Copy over the shipping address to billing address while submit shipping
      COHelpers.copyBillingAddressToBasket(currentBasket.defaultShipment.shippingAddress, currentBasket);

      var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
      if (usingMultiShipping === true && currentBasket.shipments.length < 2) {
        req.session.privacyCache.set('usingMultiShipping', false);
        usingMultiShipping = false;
      }

      COHelpers.recalculateBasket(currentBasket);
      var addtoemaillist = form.shippingAddress.addtoemaillist.checked;
      if (addtoemaillist) {
        var hashMap = new HashMap();
        var shippingEmail = form.shippingAddress.email.value;
        var customerCheckout = req.currentCustomer.raw;
        hashMap.put('language', req.locale.id);
        hashMap.put('sourceId', 'checkOut');
        hashMap.put('theBayOptStatus', 'Y');
        hashMap.put('canadaFlag', 'N');
        hashMap.put('banner', preferences.dataSubscription.BANNER);
        hashMap.put('exported', false);
        hashMap.put('subscribedOrUnsubscribed', new Date());
        if (shippingData.address) {
          hashMap.put('address', shippingData.address.address1 ? shippingData.address.address1 : '');
          hashMap.put('addressTwo', shippingData.address.address2 ? shippingData.address.address2 : '');
          hashMap.put('city', shippingData.address.city ? shippingData.address.city : '');
          hashMap.put('country', shippingData.address.countryCode ? shippingData.address.countryCode : '');
          hashMap.put('firstName', shippingData.address.firstName ? shippingData.address.firstName : '');
          hashMap.put('lastName', shippingData.address.lastName ? shippingData.address.lastName : '');
          hashMap.put('phone', shippingData.address.phone ? shippingData.address.phone : '');
          hashMap.put('state', shippingData.address.stateCode ? shippingData.address.stateCode : '');
          hashMap.put('zipFull', shippingData.address.postalCode ? shippingData.address.postalCode : '');
        }

        EmailSubscribeHelper.createSubscription(shippingEmail, hashMap, customerCheckout);
        req.session.privacyCache.set('email_opt_in', true);
      }

      // SFDEV-4849 | Enable GC and CC fields in case order total changes from previous one.
      COHelpers.calculatePaymentTransaction(currentBasket);

      var basketModel = new OrderModel(currentBasket, {
        usingMultiShipping: usingMultiShipping,
        shippable: true,
        countryCode: currentLocale.country,
        containerView: 'basket'
      });

      var billingForm = server.forms.getForm('billing');
      var defaultShipmentAddr = currentBasket.defaultShipment.shippingAddress;

      if (!empty(defaultShipmentAddr) && !empty(basketModel.orderEmail)) {
        billingForm.contactInfoFields.email.value = basketModel.orderEmail;
        billingForm.contactInfoFields.phone.value = defaultShipmentAddr.phone;
      }

      var customer = new AccountModel(req.currentCustomer);

      //update cards section in payment
      var renderedStoredPaymentInstrument = COHelpers.getRenderedPaymentInstruments(req, customer, basketModel);
      res.json({
        customer: customer,
        order: basketModel,
        form: server.forms.getForm('shipping'),
        customerSavedAddressesHtml: COHelpers.getCustomerSavedAddresses(req, customer, basketModel, currentLocale.country),
        isBasketUpdated: basketHashHelper.validateBasketHashChange(currentBasket, req),
        renderedPaymentInstruments: renderedStoredPaymentInstrument
      });
    });
  }

  return next();
});

/**
 * Handle Ajax address addition to address book
 */
server.post('AddAddress', server.middleware.https, csrfProtection.validateAjaxRequest, function (req, res, next) {
  var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
  var Transaction = require('dw/system/Transaction');
  var BasketMgr = require('dw/order/BasketMgr');
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var shippingHelpers = require('*/cartridge/scripts/checkout/shippingHelpers');
  var AccountModel = require('*/cartridge/models/account');
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  var AddressModel = require('*/cartridge/models/address');
  var addressFormObj = {};
  var email = null;
  var currentBasket = BasketMgr.getCurrentBasket();
  var Locale = require('dw/util/Locale');
  var currentLocale = Locale.getLocale(req.locale.id);
  var OrderModel = require('*/cartridge/models/order');

  var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
  if (usingMultiShipping === true && currentBasket.shipments.length < 2) {
    req.session.privacyCache.set('usingMultiShipping', false);
    usingMultiShipping = false;
  }
  var basketModel = new OrderModel(currentBasket, {
    usingMultiShipping: usingMultiShipping,
    shippable: true,
    countryCode: currentLocale.country,
    containerView: 'basket'
  });

  var form = server.forms.getForm('shipping');
  // verify shipping form data
  var formErrors = COHelpers.validateShippingForm(form.shippingAddress);
  if (Object.keys(formErrors).length > 0) {
    res.json({
      form: form,
      fieldErrors: [formErrors],
      serverErrors: [],
      error: true
    });
  } else {
    addressFormObj = form.shippingAddress && form.shippingAddress ? form.shippingAddress : null;
    var customer = CustomerMgr.getCustomerByCustomerNumber(req.currentCustomer.profile.customerNo);
    var addressBook = customer.getProfile().getAddressBook();
    var isDefaultAddress = false;
    var addressType = 'S';
    var isAddAddress = true;
    if (form.valid) {
      res.setViewData(addressFormObj);
      this.on('route:BeforeComplete', function () {
        // eslint-disable-line no-shadow
        var shippingFields = res.getViewData();
        var formInfo = shippingFields.addressFields;
        if (formInfo && shippingFields) {
          Transaction.wrap(function () {
            var address = null;
            if (req.querystring.addressId) {
              address = addressBook.getAddress(req.querystring.addressId);
              if (!address) {
                address = addressBook.createAddress(req.querystring.addressId);
              }
            } else {
              address = addressBook.getAddress(formInfo.address1.value);
              if (!address) {
                address = addressBook.createAddress(formInfo.address1.value);
              }
            }

            if (address) {
              // Save form's address
              var selectedAddress = COHelpers.updateAddressFields(req, address, formInfo);
              // set customer email on address addition, override in shipping submit
              if (shippingFields.email.value && currentBasket) {
                currentBasket.setCustomerEmail(shippingFields.email.value);
                email = shippingFields.email.value;
                // selectedAddress.email = shippingFields.email.value;
              }

              accountHelpers.updateAccLastModifiedDate(req.currentCustomer.raw);

              // Set As Default
              if (formInfo.setAsDefault && formInfo.setAsDefault.checked) {
                addressBook.setPreferredAddress(address);
                isDefaultAddress = true;
              }
              // UCID Customer Update.
              hooksHelper('ucid.middleware.service', 'updateUCIDCustomer', [
                customer.profile,
                customer.profile.customerNo,
                address,
                addressType,
                isDefaultAddress,
                isAddAddress
              ]);
              var customerModel = new AccountModel(req.currentCustomer);
              var selectedAddressModel = selectedAddress ? new AddressModel(selectedAddress) : selectedAddress;
              res.json({
                form: server.forms.getForm('shipping'),
                selectedAddress: selectedAddress,
                createCustomerAddressHtml: shippingHelpers.createCustomerAddressHtml(
                  customerModel.addresses,
                  email,
                  customerModel.preferredAddress,
                  selectedAddressModel
                ),
                customer: customerModel,
                order: basketModel,
                customerSavedAddressesHtml: COHelpers.getCustomerSavedAddresses(req, customerModel, basketModel, currentLocale.country)
              });
            } else {
              res.json({
                error: true
              });
            }
          });
        }
      });
    }
  }
  return next();
});

module.exports = server.exports();
