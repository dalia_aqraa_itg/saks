'use strict';

var server = require('server');
server.extend(module.superModule);

var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');

/**
 * Checks if a credit card is valid or not
 * @param {Object} req - request object
 * @param {Object} card - plain object with card details
 * @param {Object} form - form object
 * @returns {boolean} a boolean representing card validation
 */
function verifyCard(req, card, form) {
  var collections = require('*/cartridge/scripts/util/collections');
  var TCCHelper = require('*/cartridge/scripts/checkout/TCCHelpers');
  var Resource = require('dw/web/Resource');
  var PaymentMgr = require('dw/order/PaymentMgr');
  var PaymentStatusCodes = require('dw/order/PaymentStatusCodes');
  var PaymentInstrument = require('dw/order/PaymentInstrument');

  var currentCustomer = req.currentCustomer.raw;
  var countryCode = req.geolocation.countryCode;
  var creditCardPaymentMethod = PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_CREDIT_CARD);
  var paymentCard = PaymentMgr.getPaymentCard(card.cardType);
  var error = false;

  var applicablePaymentCards = creditCardPaymentMethod.getApplicablePaymentCards(currentCustomer, countryCode, null);

  var cardNumber = card.cardNumber;
  var creditCardStatus;
  var formCardNumber = form.cardNumber;
  var isHBCCard = false;

  if (card.cardType === 'TCC') {
    if (TCCHelper.isTCCDateExpired(cardNumber)) {
      form.tccCardNumber.valid = false;
      form.tccCardNumber.error = Resource.msg('error.message.tcc.date', 'checkout', null);
      error = true;
    }

    return error;
  }

  if (paymentCard) {
    if (applicablePaymentCards.contains(paymentCard)) {
      // determine if the card is an HBC card that requires special verification
      if (paymentCard.cardType === 'SAKS' || paymentCard.cardType === 'HBC' || paymentCard.cardType === 'MPA') {
        isHBCCard = true;
      }
      if (isHBCCard && cardNumber.length >= 8 && cardNumber.length <= 10) {
        // allow SAKS 8-10 digit cards (no expiration date)
        creditCardStatus = true;
      } else {
        // verify any other card types
        creditCardStatus = paymentCard.verify(card.expirationMonth, card.expirationYear, cardNumber);
      }
    } else {
      // Invalid Payment Instrument
      formCardNumber.valid = false;
      formCardNumber.error = Resource.msg('error.payment.not.valid', 'checkout', null);
      error = true;
    }
  } else {
    formCardNumber.valid = false;
    formCardNumber.error = Resource.msg('error.message.creditnumber.invalid', 'forms', null);
    error = true;
  }

  /**
   * TODO: Because SAKS cards are defined as 8,10 digits in Business Manager,
   * PLCC cards with 16 digits require custom verification. When [8,10] cards
   * are phased out consider revising custom code that verifies 16 digits here
   * and throughout. (SFDEV-11183)
   */
  if (creditCardStatus && creditCardStatus.error) {
    collections.forEach(creditCardStatus.items, function (item) {
      switch (item.code) {
        case PaymentStatusCodes.CREDITCARD_INVALID_CARD_NUMBER:
          // invalid card number error is true for non HBC cards or HBC cards whose card length is not equal to 16
          if (!isHBCCard || (isHBCCard && cardNumber.length !== 16)) {
            formCardNumber.valid = false;
            formCardNumber.error = Resource.msg('error.message.creditnumber.invalid', 'forms', null);
            error = true;
          }
          break;

        case PaymentStatusCodes.CREDITCARD_INVALID_EXPIRATION_DATE:
          var expirationMonth = form.expirationMonth;
          var expirationYear = form.expirationYear;
          expirationMonth.valid = false;
          expirationMonth.error = Resource.msg('error.message.creditexpiration.expired', 'forms', null);
          expirationYear.valid = false;
          error = true;
          break;
        default:
          error = true;
      }
    });
  }
  return error;
}

/**
 * Creates an object from form values
 * @param {Object} paymentForm - form object
 * @returns {Object} a plain object of payment instrument
 */
function getDetailsObject(paymentForm) {
  return {
    name: paymentForm.cardOwner.value,
    cardNumber: paymentForm.cardNumber.value,
    cardType: paymentForm.cardType.value,
    expirationMonth: paymentForm.expirationMonth.value,
    expirationYear: paymentForm.expirationYear.value,
    paymentForm: paymentForm
  };
}

/**
 * Creates a list of expiration years from the current year
 * @returns {List} a plain list of expiration years from current year
 */
function getExpirationYears() {
  // eslint-disable-line
  var currentYear = new Date().getFullYear();
  var creditCardExpirationYears = [];

  for (var i = 0; i < 10; i++) {
    creditCardExpirationYears.push((currentYear + i).toString());
  }

  return creditCardExpirationYears;
}

/**
 * Creates a token. This should be replaced by utilizing a tokenization provider
 * @param {Object} encryptedCard - creating encryptedCard
 * @returns {string} token - a token
 */
function createToken(encryptedCard) {
  var TokenExFacade = require('*/cartridge/scripts/services/TokenExFacade');
  var token = TokenExFacade.tokenizeEncryptedCard(encryptedCard);
  return token;
}

server.replace('AddPayment', csrfProtection.generateToken, consentTracking.consent, userLoggedIn.validateLoggedIn, function (req, res, next) {
  var preferences = require('*/cartridge/config/preferences');

  var creditCardExpirationYears = getExpirationYears();
  var paymentForm = server.forms.getForm('creditCard');
  paymentForm.clear();

  var months = paymentForm.expirationMonth.options;
  for (var j = 0, k = months.length; j < k; j++) {
    months[j].selected = false;
  }

  // Card Tokenization enabled
  var tokenEnabled = preferences.enableTokenEx;
  var tokenPublicKey = preferences.tokenExPublicKey;
  res.setViewData({
    tokenEnabled: tokenEnabled,
    tokenPublicKey: tokenPublicKey
  });

  res.render('account/payment/addPayment', {
    includeRecaptchaJS: true,
    paymentForm: paymentForm,
    expirationYears: creditCardExpirationYears,
    pageType: 'payment'
  });

  next();
});

server.replace('SavePayment', csrfProtection.validateAjaxRequest, function (req, res, next) {
  var formErrors = require('*/cartridge/scripts/formErrors');
  var Resource = require('dw/web/Resource');
  var dwOrderPaymentInstrument = require('dw/order/PaymentInstrument');
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
  var TCCHelper = require('*/cartridge/scripts/checkout/TCCHelpers');

  var paymentForm = server.forms.getForm('creditCard');
  var result = getDetailsObject(paymentForm);
  var cartType = result.cardType;
  result.makeDefaultPayment = req.form.makeDefaultPayment;
  var tokenObj;

  if (cartType && cartType === 'TCC') {
    tokenObj = {};
    tokenObj.token = result.cardNumber;
  } else {
    // Generate Token
    if (result.cardNumber) {
      tokenObj = createToken(result.cardNumber);
    }
    if (tokenObj && (tokenObj.error === true || tokenObj.token.length === 0)) {
      res.json({
        success: false,
        tokenExError: Resource.msg('label.save.tokenex.error', 'payment', null)
      });
      return next();
    }
    // Save TokenEx Token For Luhn Checksum
    result.cardNumber = tokenObj.token;
  }

  if (paymentForm.valid && !verifyCard(req, result, paymentForm)) {
    res.setViewData(result);
    this.on('route:BeforeComplete', function (req, res) {
      // eslint-disable-line no-shadow
      var URLUtils = require('dw/web/URLUtils');
      var CustomerMgr = require('dw/customer/CustomerMgr');
      var Transaction = require('dw/system/Transaction');

      var formInfo = res.getViewData();
      var customer = CustomerMgr.getCustomerByCustomerNumber(req.currentCustomer.profile.customerNo);
      var wallet = customer.getProfile().getWallet();

      // If we are saving this card as default, false for other cards
      if (formInfo.makeDefaultPayment) {
        // Make False to other Cards.
        var paymentInstruments = req.currentCustomer.wallet.paymentInstruments;
        /* eslint-disable */
        for (var i = 0; i < paymentInstruments.length; i++) {
          var pi = paymentInstruments[i];
          Transaction.wrap(function () {
            pi.raw.custom.defaultCreditCard = false;
          });
        }
        /* eslint-enable */
      }

      Transaction.wrap(function () {
        var paymentInstrument = wallet.createPaymentInstrument(dwOrderPaymentInstrument.METHOD_CREDIT_CARD);
        paymentInstrument.setCreditCardHolder(formInfo.name);
        paymentInstrument.setCreditCardNumber(formInfo.cardNumber);
        paymentInstrument.setCreditCardType(formInfo.cardType);
        if (!empty(formInfo.expirationMonth) && !empty(formInfo.expirationYear)) {
          paymentInstrument.setCreditCardExpirationMonth(parseInt(formInfo.expirationMonth, 10));
          paymentInstrument.setCreditCardExpirationYear(parseInt(formInfo.expirationYear, 10));
        }
        paymentInstrument.setCreditCardToken(tokenObj.token);
        paymentInstrument.custom.defaultCreditCard = !!formInfo.makeDefaultPayment;
        paymentInstrument.custom.ocapi_token = tokenObj.token;
        customer.getProfile().custom.lastPaymentModified = new Date().toISOString();
        if (cartType && cartType === 'TCC') {
          var tccExpirationDate = TCCHelper.getExpirationDate(formInfo.cardNumber);
          paymentInstrument.custom.tccExpirationDate = '' + tccExpirationDate;
        }
      });

      if (wallet.getPaymentInstruments().length === 1) {
        var lastPaymentIns = wallet.getPaymentInstruments()[0];
        Transaction.wrap(function () {
          lastPaymentIns.custom.defaultCreditCard = true;
        });
      }

      // Update the custom attribute for last modified date as the system attribute lastModified is not true profile update from a customer perspective
      accountHelpers.updateAccLastModifiedDate(req.currentCustomer.raw);

      res.json({
        success: true,
        redirectUrl: URLUtils.url('PaymentInstruments-List', 'new-payment', 'true').toString()
      });
    });
  } else {
    res.json({
      success: false,
      fields: formErrors.getFormErrors(paymentForm)
    });
  }
  return next();
});

server.replace('List', userLoggedIn.validateLoggedIn, consentTracking.consent, function (req, res, next) {
  var URLUtils = require('dw/web/URLUtils');
  var PropertyComparator = require('dw/util/PropertyComparator');
  var ArrayList = require('dw/util/ArrayList');
  var AccountModel = require('*/cartridge/models/account');
  var paymentInstruments = AccountModel.getCustomerPaymentInstruments(req.currentCustomer.wallet.paymentInstruments);
  paymentInstruments = new ArrayList(paymentInstruments);
  paymentInstruments.sort(new PropertyComparator('defaultCreditCard', false));

  res.render('account/payment/payment', {
    includeRecaptchaJS: true,
    paymentInstruments: paymentInstruments,
    actionUrl: URLUtils.url('PaymentInstruments-DeletePayment').toString(),
    pageType: 'payment'
  });
  next();
});

server.get('SetDefault', userLoggedIn.validateLoggedIn, function (req, res, next) {
  var Transaction = require('dw/system/Transaction');
  var URLUtils = require('dw/web/URLUtils');
  var array = require('*/cartridge/scripts/util/array');
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');

  var UUID = req.querystring.paymentID;
  var paymentInstruments = req.currentCustomer.wallet.paymentInstruments;
  var paymentToSetDefault = array.find(paymentInstruments, function (item) {
    return UUID === item.UUID;
  });
  res.setViewData(paymentToSetDefault);

  this.on('route:BeforeComplete', function () {
    // eslint-disable-line no-shadow
    var paymentToSetDefault = res.getViewData(); // eslint-disable-line no-shadow
    var paymentInstruments = req.currentCustomer.wallet.paymentInstruments; // eslint-disable-line no-shadow
    /* eslint-disable */
    for (var i = 0; i < paymentInstruments.length; i++) {
      var pi = paymentInstruments[i];
      Transaction.wrap(function () {
        pi.raw.custom.defaultCreditCard = false;
      });
    }
    /* eslint-enable */
    Transaction.wrap(function () {
      paymentToSetDefault.raw.custom.defaultCreditCard = true;
    });

    // Update the custom attribute for last modified date as the system attribute lastModified is not true profile update from a customer perspective
    accountHelpers.updateAccLastModifiedDate(req.currentCustomer.raw);

    res.redirect(URLUtils.url('PaymentInstruments-List'));
  });
  next();
});

server.replace('DeletePayment', userLoggedIn.validateLoggedInAjax, function (req, res, next) {
  var array = require('*/cartridge/scripts/util/array');
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
  var AccountModel = require('*/cartridge/models/account');

  var data = res.getViewData();
  if (data && !data.loggedin) {
    res.json();
    return next();
  }

  var UUID = req.querystring.UUID;
  var paymentInstruments = req.currentCustomer.wallet.paymentInstruments;
  var paymentToDelete = array.find(paymentInstruments, function (item) {
    return UUID === item.UUID;
  });
  paymentToDelete.defaultCreditCard = paymentToDelete.raw.custom.defaultCreditCard;
  res.setViewData(paymentToDelete);
  this.on('route:BeforeComplete', function () {
    // eslint-disable-line no-shadow
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var Transaction = require('dw/system/Transaction');
    var Resource = require('dw/web/Resource');

    var payment = res.getViewData();
    var customer = CustomerMgr.getCustomerByCustomerNumber(req.currentCustomer.profile.customerNo);
    var wallet = customer.getProfile().getWallet();
    Transaction.wrap(function () {
      wallet.removePaymentInstrument(payment.raw);
      customer.getProfile().custom.lastPaymentModified = new Date().toISOString();
    });

    // Update the custom attribute for last modified date as the system attribute lastModified is not true profile update from a customer perspective
    accountHelpers.updateAccLastModifiedDate(req.currentCustomer.raw);

    // Set the Last Payment method as default.
    if (payment.defaultCreditCard && payment.setNextDefaultPayment && payment.setNextDefaultPayment === true) {
      var customerPaymentInstruments = wallet.getPaymentInstruments();
      for (var i = 0; i < customerPaymentInstruments.length; i++) {
        var pi = customerPaymentInstruments[i];
        if (!pi.isCreditCardExpired()) {
          Transaction.wrap(function () {
            pi.custom.defaultCreditCard = true;
          });
          break;
        }
      }
    } else {
      if (wallet.getPaymentInstruments().length === 1) {
        var lastPaymentIns = wallet.getPaymentInstruments()[0];
        Transaction.wrap(function () {
          lastPaymentIns.custom.defaultCreditCard = true;
        });
      }
    }

    var paymentHtml = AccountModel.getCustomerPaymentHtml(req.currentCustomer);

    if (wallet.getPaymentInstruments().length === 0) {
      res.json({
        UUID: UUID,
        message: Resource.msg('msg.no.saved.payments', 'payment', null),
        paymentHtml: paymentHtml
      });
    } else {
      res.json({
        UUID: UUID,
        defaultMsg: Resource.msg('label.payment.defaultheaders', 'account', null),
        paymentHtml: paymentHtml
      });
    }
  });

  return next();
});

module.exports = server.exports();
