'use strict';

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var preferences = require('*/cartridge/config/preferences');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var PAGE_SIZE_ITEMS = preferences.defaultPageSize ? preferences.defaultPageSize : 24;
var TOTAL_PAGE_SIZE = preferences.totalPageSize ? preferences.totalPageSize : 96;
var ProductPagination = require('*/cartridge/scripts/helpers/wishlistPagination');
var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
var Site = require('dw/system/Site');
server.extend(module.superModule);

server.replace('Show', consentTracking.consent, server.middleware.https, csrfProtection.generateToken, function (req, res, next) {
  var list = productListHelper.getList(req.currentCustomer.raw, {
    type: 10
  });
  var WishlistModel = require('*/cartridge/models/productList');
  var userName = '';
  var firstName;
  var rememberMe = false;
  if (req.currentCustomer.credentials) {
    rememberMe = true;
    userName = req.currentCustomer.credentials.username;
  }
  var loggedIn = req.currentCustomer.profile;

  var target = req.querystring.rurl || 1;
  var actionUrl = URLUtils.url('Account-Login');
  var createAccountUrl = URLUtils.url('Account-SubmitRegistration', 'rurl', target).relative().toString();
  var navTabValue = req.querystring.action;
  var breadcrumbs;

  if (loggedIn) {
    breadcrumbs = [
      {
        htmlValue: Resource.msg('global.home', 'common', null),
        url: URLUtils.home().toString()
      },
      {
        htmlValue: Resource.msg('page.title.myaccount', 'account', null),
        url: URLUtils.url('Account-Show').toString()
      },
      {
        htmlValue: Resource.msg('label.wishlist', 'account', null)
      }
    ];
  } else {
    breadcrumbs = [
      {
        htmlValue: Resource.msg('global.home', 'common', null),
        url: URLUtils.home().toString()
      },
      {
        htmlValue: Resource.msg('label.wishlist', 'account', null)
      }
    ];
  }

  var profileForm = server.forms.getForm('profile');
  profileForm.clear();
  var pageSize = PAGE_SIZE_ITEMS;

  var pageNumber = 1;
  if (req.querystring.pageNumber) {
    if (req.querystring.scrollpersist) {
      pageNumber = req.querystring.pageNumber;
    } else {
      pageNumber = (req.querystring.pageNumber - 1) * Math.ceil(TOTAL_PAGE_SIZE / PAGE_SIZE_ITEMS) + 1;
    }
  }
  var wishlistModel = new WishlistModel(list, {
    type: 'wishlist',
    publicView: false,
    pageSize: pageSize,
    pageNumber: pageNumber,
    scrollPersist: !!req.querystring.scrollpersist,
    isShared: false
  }).productList;

  // var pageNumb = req.querystring.pageNumber || 1;
  var paginationUrls = ProductPagination.getPaginationUrls(wishlistModel.length, pageNumber);

  var cookieValue = cookiesHelper.read('RememberMeForced');
  if (cookieValue) {
    rememberMe = false;
    userName = '';
  }

  var isWishlistMerged = false;
  if (req.session.privacyCache.get('mergeWishlist')) {
    isWishlistMerged = true;
    req.session.privacyCache.set('mergeWishlist', null);
  }

  res.render('/wishlist/wishlistLanding', {
    wishlist: wishlistModel,
    navTabValue: navTabValue || 'login',
    rememberMe: rememberMe,
    userName: userName,
    actionUrl: actionUrl,
    actionUrls: {
      updateQuantityUrl: ''
    },
    profileForm: profileForm,
    breadcrumbs: breadcrumbs,
    oAuthReentryEndpoint: 1,
    loggedIn: loggedIn,
    firstName: firstName,
    socialLinks: loggedIn,
    publicOption: loggedIn,
    createAccountUrl: createAccountUrl,
    paginationUrls: paginationUrls,
    startSize: pageNumber,
    isWishlistMerged: isWishlistMerged,
    s7APIForPDPZoomViewer: preferences.s7APIForPDPZoomViewer,
    s7APIForPDPVideoPlayer: preferences.s7APIForPDPVideoPlayer
  });
  next();
});

server.replace('MoreList', function (req, res, next) {
  var publicView = req.querystring.publicView === 'true' || false;
  var list;
  var paginationUrls;
  var isSharedWishList = req.querystring.isShared === 'true' || false;

  if (publicView && req.querystring.id) {
    var productListMgr = require('dw/customer/ProductListMgr');
    list = productListMgr.getProductList(req.querystring.id);
  } else if (req.querystring.id) {
    // To handle the Lazy Load for the Shared Wishlist SFDEV-6952
    var productListMgr = require('dw/customer/ProductListMgr');
    list = productListMgr.getProductList(req.querystring.id);
  } else {
    list = productListHelper.getList(req.currentCustomer.raw, {
      type: 10
    });
  }
  var WishlistModel = require('*/cartridge/models/productList');
  var wishlistModel = new WishlistModel(list, {
    type: 'wishlist',
    publicView: publicView,
    pageSize: PAGE_SIZE_ITEMS,
    pageNumber: req.querystring.pageNumber || 1,
    urlParam: req.querystring.actionTrigger || null,
    isShared: isSharedWishList
  }).productList;
  var pageNumb = Math.ceil((req.querystring.pageNumber || 1) / Math.ceil(TOTAL_PAGE_SIZE / PAGE_SIZE_ITEMS));

  //  Need to build seperate Pagination URL's for regular wishlist and shared wishlist - SFDEV-6952
  if (isSharedWishList) {
    paginationUrls = ProductPagination.getShareWLPaginationUrls(wishlistModel.length, req.querystring.pageNumber, req.querystring.id);
  } else {
    paginationUrls = ProductPagination.getPaginationUrls(wishlistModel.length, req.querystring.pageNumber);
  }

  var publicOption = list.owner ? req.currentCustomer.raw.ID === list.owner.ID : false;

  res.render('/wishlist/components/wishlistedProducts', {
    wishlist: wishlistModel,
    publicOption: publicOption,
    paginationUrls: paginationUrls,
    startSize: req.querystring.pageNumber ? req.querystring.pageNumber : '',
    actionUrls: {
      updateQuantityUrl: ''
    }
  });
  next();
});

server.get('GetCartWLItems', function (req, res, next) {
  if (session.custom.Wishlist && preferences.CartBEPerformanceChangesEnable) {
    var itemsId = session.custom.Wishlist.slice(0, -1);
    itemsId = itemsId.split(',');
    res.render('/wishlist/cartWishlistLanding', {
      itemsId: itemsId
    });
  }else {
  var list = productListHelper.getList(req.currentCustomer.raw, {
    type: 10
  });

  var WishlistModel = require('*/cartridge/models/carProductList');

  var wishlistModel = new WishlistModel(list, {
    type: 'wishlist',
    publicView: false,
    pageSize: 10,
    pageNumber: req.querystring.pageNumber || 1
  }).productList;

  var sessionItemsId = '';
  var itemsId = []; 
  var numberOfItems =  Site.current.preferences.custom.Wishlist_Items_Saved_On_Session;
  numberOfItems = wishlistModel.items.length < numberOfItems ? wishlistModel.items.length : numberOfItems;
  for (var idIndex = 0;  idIndex < numberOfItems ; idIndex++) {
     itemsId.push(wishlistModel.items[idIndex].id);
     sessionItemsId += wishlistModel.items[idIndex].id;
     sessionItemsId += ",";
  }
  session.custom.Wishlist = sessionItemsId;
  res.render('/wishlist/cartWishlistLanding', {
    itemsId: itemsId
  });
}
  next();
});

server.get('RemoveWLProduct', function (req, res, next) {
  var list = productListHelper.removeItem(req.currentCustomer.raw, req.querystring.pid, {
    req: req,
    type: 10
  }); //eslint-disable-line
  var BasketMgr = require('dw/order/BasketMgr');
  var CartModel = require('*/cartridge/models/cart');
  var currentBasket = BasketMgr.getCurrentBasket();
  var basketModel = new CartModel(currentBasket);

  // This is used for analytics
  var ProductFactory = require('*/cartridge/scripts/factories/product');
  var product = {
    pid: req.querystring.pid
  };
  var curProduct = ProductFactory.get(product);
  res.setViewData({
    deletedProduct: curProduct
  });

  res.json(basketModel);
  next();
});

server.replace('GetProduct', function (req, res, next) {
  var ProductFactory = require('*/cartridge/scripts/factories/product');
  var addToCartUrl = URLUtils.url('Cart-AddProduct');
  var requestUuid = req.querystring.uuid;
  var requestPid = req.querystring.pid;

  var product = {
    pid: requestPid
  };

  var curProduct = ProductFactory.get(product);

  res.render('/wishlist/components/wishlistProductCard', {
    product: curProduct,
    UUID: requestUuid,
    addToCartUrl: addToCartUrl
  });

  next();
});

server.post('CartAddToWL', function (req, res, next) {
  var list = productListHelper.getList(req.currentCustomer.raw, {
    type: 10
  });
  var pid = req.form.pid;
  var optionId = req.form.optionId || null;
  var optionVal = req.form.optionVal || null;

  var BasketMgr = require('dw/order/BasketMgr');
  var CartModel = require('*/cartridge/models/cart');
  var currentBasket = BasketMgr.getCurrentBasket();
  var basketModel = new CartModel(currentBasket);

  var config = {
    qty: 1,
    optionId: optionId,
    optionValue: optionVal,
    req: req,
    type: 10
  };
  var errMsg = productListHelper.itemExists(list, pid, config)
    ? Resource.msg('wishlist.addtowishlist.exist.msg', 'wishlist', null)
    : Resource.msg('wishlist.addtowishlist.failure.msg', 'wishlist', null);

  var success = productListHelper.addItem(list, pid, config);
  if (success) {
    // Added this for analytics
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var product = {
      pid: pid
    };
    var movedProduct = ProductFactory.get(product);
    res.setViewData({
      recentlyMovedProduct: movedProduct
    });
    res.json(basketModel);
  } else {
    res.json({
      error: true,
      pid: pid,
      msg: errMsg
    });
  }
  next();
});

server.get('GetWishlistProduct', function (req, res, next) {
  var list = productListHelper.getList(req.currentCustomer.raw, {
    type: 10
  });
  var pid = req.querystring.pid;

  var found = false;
  list.items.toArray().forEach(function (item) {
    if (item.productID === pid) {
      found = true;
    }
  });
  res.json({
    wishlisted: found
  });
  next();
});

server.replace('ShowOthers', consentTracking.consent, function (req, res, next) {
  var id = req.querystring.id;
  var productListMgr = require('dw/customer/ProductListMgr');
  var ProductPagination = require('*/cartridge/scripts/helpers/wishlistPagination');
  var apiList = productListMgr.getProductList(id);
  var breadcrumbs = [
    {
      htmlValue: Resource.msg('global.home', 'common', null),
      url: URLUtils.home().toString()
    }
  ];
  var loggedIn = req.currentCustomer.profile;
  if (loggedIn) {
    breadcrumbs.push({
      htmlValue: Resource.msg('page.title.myaccount', 'account', null),
      url: URLUtils.url('Account-Show').toString()
    });
  }
  if (apiList) {
    if (apiList.owner.ID === req.currentCustomer.raw.ID) {
      res.redirect(URLUtils.url('Wishlist-Show'));
    }
    if (!apiList.public) {
      res.render('/wishlist/viewWishlist', {
        wishlist: null,
        breadcrumbs: breadcrumbs,
        loggedIn: loggedIn,
        privateList: true,
        errorMsg: Resource.msg('wishlist.not.viewable.text', 'wishlist', null)
      });
    } else {
      var pageSize = PAGE_SIZE_ITEMS;
      var pageNumber = 1;
      if (req.querystring.pageNumber) {
        if (req.querystring.scrollpersist) {
          pageNumber = req.querystring.pageNumber;
        } else {
          pageNumber = (req.querystring.pageNumber - 1) * Math.ceil(TOTAL_PAGE_SIZE / PAGE_SIZE_ITEMS) + 1;
        }
      }

      var WishlistModel = require('*/cartridge/models/productList');
      var wishlistModel = new WishlistModel(apiList, {
        type: 'wishlist',
        publicView: false,
        pageSize: pageSize,
        pageNumber: pageNumber,
        scrollPersist: !!req.querystring.scrollpersist,
        isShared: true
      }).productList;

      var paginationUrls = ProductPagination.getShareWLPaginationUrls(wishlistModel.length, pageNumber, id);

      res.render('/wishlist/viewWishlist', {
        wishlist: wishlistModel,
        breadcrumbs: breadcrumbs,
        loggedIn: loggedIn,
        publicOption: false,
        privateList: false,
        errorMsg: '',
        socialLinks: false,
        paginationUrls: paginationUrls,
        startSize: pageNumber
      });
    }
  } else {
    res.render('/wishlist/viewWishlist', {
      wishlist: null,
      breadcrumbs: breadcrumbs,
      loggedIn: loggedIn,
      privateList: true,
      errorMsg: Resource.msg('wishlist.not.viewable.text', 'wishlist', null),
      socialLinks: false
    });
  }
  next();
});

// Adding this for analytics template
server.append('AddProduct', function (req, res, next) {
  if (res.viewData.success && res.viewData.pid) {
    var productFactory = require('*/cartridge/scripts/factories/product');
    var product = productFactory.get({
      pid: res.viewData.pid,
      pview: 'addRemoveFromWishList'
    });

    if (session.custom.Wishlist) {
      var numberOfItems =  Site.current.preferences.custom.Wishlist_Items_Saved_On_Session;
      var itemsID = session.custom.Wishlist;
      var itemsID = itemsID.split(',');
      if (itemsID.length <numberOfItems) {
        session.custom.Wishlist += res.viewData.pid;
        session.custom.Wishlist +=  ",";
        
      }
    }

    res.setViewData({
      product: product
    });
  }
  next();
});

// Adding this for analytics template
server.append('RemoveProduct', function (req, res, next) {
  if (res.viewData.success && req.querystring.pid) {
    var productFactory = require('*/cartridge/scripts/factories/product');
    var product = productFactory.get({
      pid: req.querystring.pid,
      pview: 'addRemoveFromWishList'
    });
    res.setViewData({
      product: product
    });
  }
  next();
});

server.get('GetWishlistProducts', function (req, res, next) {
  var list = productListHelper.getList(req.currentCustomer.raw, {
    type: 10
  });
  var wishListProducts = [];
  
  if (!empty(list) && list.items.length > 0) {
    var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
    list.items.toArray().forEach(function (item) {
      var wishListProduct = {};
      wishListProduct.id = item.productID;
      wishListProduct.productType = productHelper.getProductType(item.product);
      wishListProducts.push(wishListProduct);
    });
  }
  res.json({
    wishlistProducts: wishListProducts
  });
  next();
});

server.get('TileShow', cache.applyEightHoursPromotionSensitiveCache, function (req, res, next) {
  var ProductFactory = require('*/cartridge/scripts/factories/product');
  var ProductMgr = require('dw/catalog/ProductMgr');

  var productTileParams = { pview: 'wishlistTile' };
  Object.keys(req.querystring).forEach(function (key) {
    productTileParams[key] = req.querystring[key];
  });

  var product;
  var productUrl;
  var quickViewUrl;

  try {
    product = ProductFactory.get(productTileParams);
    productUrl = URLUtils.url('Product-Show', 'pid', product.id).relative().toString();
    quickViewUrl = URLUtils.url('Product-ShowQuickView', 'pid', product.id).relative().toString();
  } catch (e) {
    var Logger = require('dw/system/Logger');
    Logger.error('Tile Show error {0}: {1} \n {2}', productTileParams.pid, e.message, e.stack);
    product = false;
    productUrl = URLUtils.url('Home-Show'); // TODO: change to coming soon page
    quickViewUrl = URLUtils.url('Home-Show');
  }

  var adobelaunchrecommendationid = '';
  if (req.querystring.adobelaunchrecommendationid || (req.querystring.pid && req.querystring.adobelaunchrecommendationposition)) {
    var recProduct = ProductMgr.getProduct(req.querystring.pid);
    if (recProduct) {
      adobelaunchrecommendationid = recProduct.master ? recProduct.ID : recProduct.variant ? recProduct.masterProduct.ID : recProduct.ID; // eslint-disable-line
    }
  }

  var context = {
    product: product,
    urls: {
      product: productUrl,
      quickView: quickViewUrl
    },
    display: {},
    addToCartUrl: URLUtils.url('Cart-AddProduct'),
    displayQuickLook: preferences.displayQuickLook ? preferences.displayQuickLook : '',
    isChanelCategory: !!req.querystring.isChanel,
    adobelaunchrecommendationposition: req.querystring.adobelaunchrecommendationposition,
    adobelaunchrecommendationid: adobelaunchrecommendationid,
    showPromoMessage: !req.querystring.hidePromoMsg,
    showInventoryMessage: !req.querystring.hideInventory,
    lazyLoad: req.querystring.lazyLoad === 'true'
  };

  Object.keys(req.querystring).forEach(function (key) {
    if (req.querystring[key] === 'true') {
      context.display[key] = true;
    } else if (req.querystring[key] === 'false') {
      context.display[key] = false;
    }
  });

  res.render('product/gridTile.isml', context);

  next();
});

module.exports = server.exports();
