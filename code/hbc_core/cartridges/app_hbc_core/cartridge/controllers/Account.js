// @ts-check
'use strict';

var server = require('server');
server.extend(module.superModule);

var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');
var capOneUtils = require('*/cartridge/scripts/util/capOneUtils');

/**
 * Checks if the email value entered is correct format
 * @param {string} email - email string to check if valid
 * @returns {boolean} Whether email is valid
 */
function validateEmail(email) {
  var regex = /^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/;
  return regex.test(email);
}

/**
 * Creates an account model for the current customer
 * @param {Object} req - local instance of request object
 * @returns {Object} a plain object of the current customer's account
 */
function getModel(req) {
  var AccountModel = require('*/cartridge/models/account');
  var AddressModel = require('*/cartridge/models/address');

  var orderModel;
  var preferredAddressModel;

  if (!req.currentCustomer.profile) {
    return null;
  }

  if (req.currentCustomer.addressBook.preferredAddress) {
    preferredAddressModel = new AddressModel(req.currentCustomer.addressBook.preferredAddress);
  } else {
    preferredAddressModel = null;
  }

  return new AccountModel(req.currentCustomer, preferredAddressModel, orderModel);
}

/**
 * Reason to override the controller - SFCC Looks up customer by LoginId and identifies the customer as legacy, based on the profile attribute
 * Route 'Login' was prepended and appended on plugin_wishlists cartridge. So the logic written in wishlist is also carried over to this controller
 */
server.replace('Login', server.middleware.https, csrfProtection.validateAjaxRequest, function (req, res, next) {
  var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var Resource = require('dw/web/Resource');
  var Transaction = require('dw/system/Transaction');
  var BasketMgr = require('dw/order/BasketMgr');
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
  var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
  var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
  var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
  var HookMgr = require('dw/system/HookMgr');
  var preferences = require('*/cartridge/config/preferences');
  var Logger = require('dw/system/Logger');

  if (session.custom.isbot != null && session.custom.isbot) {
    res.json({
      error: [Resource.msg('error.message.login.bot', 'login', null)],
      botError: true
    });

    return next();
  }

  // Logger.debug('start Login !!!' + req.form.token);
  var result = hooksHelper('app.google.recaptcha.verify', 'validateToken', req.form.token, require('*/cartridge/scripts/googleRecaptchaUtil').validateToken);
  // Logger.debug('end Login !!!' + result.score);
  // Logger.debug('session.custom.isbot' + session.custom.isbot);

  if (session.custom.isbot) {
    res.json({
      error: [Resource.msg('error.message.login.bot', 'login', null)],
      botError: true
    });

    return next();
  }

  var viewData = res.getViewData();
  var list = productListHelper.getList(req.currentCustomer.raw, {
    type: 10
  });
  viewData.list = list;
  res.setViewData(viewData);
  var email = req.form.loginEmail;
  var password = req.form.loginPassword;
  var rememberMe = req.form.loginRememberMe ? !!req.form.loginRememberMe : false;
  // Cart Merge Logic
  if (!rememberMe) {
    // Enforce remember me by setting cookies
    cookiesHelper.create('RememberMeForced', 'true');
    rememberMe = true;
  } else {
    // Already Remember me is selected.
    cookiesHelper.deleteCookie('RememberMeForced');
  }

  var customer = CustomerMgr.getCustomerByLogin(email);
  var profile = null;
  var customerLoginResult;
  var islocked = false;
  if (customer !== null) {
    profile = customer.getProfile();
  }

  if (tsysHelper.isTsysMode()) {
    /* Check if the user is applying for a SaksFirst Credit Card and is a Canadian customer */
    var customerIsApply = req.querystring.apply;
    var customerIsCanadian = false;
    if (profile !== null && 'isCanadianCustomer' in profile.custom && profile.custom.isCanadianCustomer !== null) {
      customerIsCanadian = profile.custom.isCanadianCustomer;
    }
    /* Display an error if the user is applying for a SaksFirst Credit Card and is a Canadian customer */
    if (customerIsApply && customerIsCanadian) {
      res.json({
        error: [Resource.msg('label.canada.customer.apply.msg', 'registration', null)]
      });
      return next();
    }
  }

  if (profile !== null && 'legacyPasswordHash' in profile.custom && profile.custom.legacyPasswordHash !== null) {
    customerLoginResult = {
      error: false,
      errorMessage: Resource.msg('error.message.password.mismatch', 'login', null),
      status: 'ERROR_PASSWORD_MISMATCH',
      authenticatedCustomer: null
    };
    var legacyLoginResult = legacyCustomerLogin.loginLegacyCustomer(customer, profile.custom.legacyPasswordHash, email, password, rememberMe);
    customerLoginResult.error = legacyLoginResult.error;
    customerLoginResult.authenticatedCustomer = legacyLoginResult.authenticatedCustomer;
    if (customerLoginResult.error) {
      Logger.warn('Login Error for customer =>' + email);
    }
  } else {
    customerLoginResult = Transaction.wrap(function () {
      var authenticateCustomerResult = CustomerMgr.authenticateCustomer(email, password);

      if (authenticateCustomerResult.status !== 'AUTH_OK') {
        var errorCodes = {
          ERROR_CUSTOMER_DISABLED: 'error.message.account.disabled',
          ERROR_CUSTOMER_LOCKED: 'error.message.account.locked',
          ERROR_CUSTOMER_NOT_FOUND: 'error.message.login.form',
          ERROR_PASSWORD_EXPIRED: 'error.message.password.expired',
          ERROR_PASSWORD_MISMATCH: 'error.message.password.mismatch',
          ERROR_UNKNOWN: 'error.message.error.unknown',
          default: 'error.message.login.form'
        };

        Logger.warn('Login Error for customer =>' + email + ' ' + authenticateCustomerResult.status);

        var errorMessageKey = errorCodes[authenticateCustomerResult.status] || errorCodes.default;
        var errorMessage = Resource.msg(errorMessageKey, 'login', null);

        if (profile && profile.credentials && profile.credentials.isLocked()) {
          islocked = true;
          errorMessage = Resource.msg('error.message.account.locked', 'login', null);
        }

        return {
          error: true,
          errorMessage: errorMessage,
          status: authenticateCustomerResult.status,
          authenticatedCustomer: null
        };
      }

      return {
        error: false,
        errorMessage: null,
        status: authenticateCustomerResult.status,
        authenticatedCustomer: CustomerMgr.loginCustomer(authenticateCustomerResult, rememberMe)
      };
    });
  }

  if (customerLoginResult.error) {
    if (islocked) {
      var Locale = require('dw/util/Locale');
      var currentLanguage = Locale.getLocale(req.locale.id).getLanguage();
      hooksHelper(
        'app.customer.email.account.locked',
        'sendAccountLockedEmail',
        customer.profile,
        currentLanguage,
        require('*/cartridge/scripts/helpers/accountHelpers').sendAccountLockedEmail
      );
    }

    res.json({
      error: [customerLoginResult.errorMessage || Resource.msg('error.message.login.form', 'login', null)]
    });

    return next();
  }

  if (customerLoginResult.authenticatedCustomer) {
    res.setViewData({
      authenticatedCustomer: customerLoginResult.authenticatedCustomer
    });

    var preferences = require('*/cartridge/config/preferences');

    if (preferences.hudsonReward && profile.custom.hudsonReward && profile.custom.hudsonReward.indexOf(preferences.hudsonReward) === -1) {
      Transaction.wrap(function () {
        profile.custom.hudsonReward = preferences.hudsonReward + profile.custom.hudsonReward;
      });
    }

    if (preferences.isABTestingOn) {
      var ProductListMgr = require('dw/customer/ProductListMgr');
      var CustomerMgr = require('dw/customer/CustomerMgr');

      var ProductList = require('dw/customer/ProductList');
      var Logger = require('dw/system/Logger');
      var customerLists = session.getCustomer().getProductLists(ProductList.TYPE_CUSTOM_1);
      if (customerLists) {
        var customerListIter = customerLists.iterator();
        Logger.debug('customerListIter FOUND' + customerLists.getLength());
        while (customerListIter.hasNext()) {
          productList = customerListIter.next();
          Logger.debug('productList FOUND');
          session.custom.cartProductListId = productList.ID;
          Logger.debug('req.session.custom.cartProductListId -->' + session.custom.cartProductListId);
        }
      }
      /* if (session.custom.cartProductListId == null) {
                    Transaction.wrap(function () {
                        cartProductList = ProductListMgr.createProductList(session.getCustomer(), ProductList.TYPE_CUSTOM_1);
                        session.custom.cartProductListId = cartProductList.ID;
                    });
                    Logger.debug(' NEW cartProductListId -->' + session.custom.cartProductListId);
                }*/
    }

    // Cart Merge Logic
    Transaction.wrap(function () {
      var isCartMerged = cartHelper.mergeCart(req);
      if (isCartMerged) {
        req.session.privacyCache.set('basketMerged', true);
        var currentBasket = BasketMgr.getCurrentBasket();
        if (currentBasket) {
          Transaction.wrap(function () {
            if (currentBasket.currencyCode !== req.session.currency.currencyCode) {
              currentBasket.updateCurrency();
            }
            cartHelper.ensureAllShipmentsHaveMethods(currentBasket);
            basketCalculationHelpers.calculateTotals(currentBasket);
          });
        }
      }

      // hook logic to send create loyalty profile in case more number was empty
      if (HookMgr.hasHook('app.customer.loyalty.rewards.profile') && !customer.profile.custom.moreCustomer) {
        var loyaltyResponse = hooksHelper(
          'app.customer.loyalty.rewards.profile',
          'callLoyaltyProfile',
          [customer.profile, true],
          require('*/cartridge/scripts/loyalty/loyaltyProfileUtil').callLoyaltyProfile
        );
        if (loyaltyResponse !== null && Number(loyaltyResponse.ResponseCode) === 0) {
          customer.profile.custom.moreCustomer =
            loyaltyResponse.LoyaltyProfile !== null &&
            loyaltyResponse.LoyaltyProfile.CustomerInfo !== null &&
            !!loyaltyResponse.LoyaltyProfile.CustomerInfo.moreNumber
              ? loyaltyResponse.LoyaltyProfile.CustomerInfo.moreNumber
              : '';
        }
      }
    });
    var accRedirectURL;
    var URLUtils = require('dw/web/URLUtils');
    var capOneApply = req.querystring.apply;
    var test29 = req.querystring.test29; // test 29 digit

    if (capOneApply || test29) {
      var result = capOneUtils.loginCapOnePrefill(req, customer);
      accRedirectURL = result.object.redirectUrl;
    } else if (req.querystring.orderhistory) {
      accRedirectURL = URLUtils.url('Order-History').relative().toString();
    } else if (req.querystring.myprefcenter) {
      accRedirectURL = URLUtils.url('PreferenceCenter-Show').relative().toString();
    } else if (req.session.privacyCache.get('basketLimitReached') && req.querystring.rurl && req.querystring.rurl === '2') {
      // redirect to cart in basket limit conflict during intermediate login
      accRedirectURL = URLUtils.url('Cart-Show').relative().toString();
    } else {
      accRedirectURL = accountHelpers.getLoginRedirectURL(req.querystring.rurl, req.session.privacyCache, false);
    }
    res.json({
      success: true,
      redirectUrl: accRedirectURL
    });

    req.session.privacyCache.set('args', null);
    var listGuest = viewData.list;
    if (!empty(listGuest.items)) {
      req.session.privacyCache.set('mergeWishlist', true);
    }
    var listLoggedIn = productListHelper.getList(customerLoginResult.authenticatedCustomer, { type: 10 });
    // We need to do this check when the both lists will be the same in case of session expires.
    if (listLoggedIn.ID !== listGuest.ID) {
      productListHelper.mergelists(listLoggedIn, listGuest, req, {
        type: 10
      });
    }
    // productListHelper.mergelists(listLoggedIn, listGuest, req, { type: 10 });
    productListHelper.updateWishlistPrivacyCache(req.currentCustomer.raw, req, { type: 10 });
  } else {
    res.json({
      error: [Resource.msg('error.message.login.form', 'login', null)]
    });
  }

  return next();
});

server.replace('Show', server.middleware.https, userLoggedIn.validateLoggedIn, consentTracking.consent, function (req, res, next) {
  // code from sfra base cartridge
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var Resource = require('dw/web/Resource');
  var URLUtils = require('dw/web/URLUtils');
  var Site = require('dw/system/Site');
  var reportingUrlsHelper = require('*/cartridge/scripts/reportingUrls');
  var preferences = require('*/cartridge/config/preferences');
  var reportingURLs;
  var basketLimitReached = false;

  // Get reporting event Account Open url
  if (req.querystring.registration && req.querystring.registration === 'submitted') {
    reportingURLs = reportingUrlsHelper.getAccountOpenReportingURLs(CustomerMgr.registeredCustomerCount);
  }
  var accountModel = getModel(req);
  var customPreferences = Site.current.preferences.custom;
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');

  // additional code for hbc
  var isCartMerged = false;
  if (req.session.privacyCache.get('basketMerged')) {
    isCartMerged = true;
    req.session.privacyCache.set('basketMerged', null);
  }
  if (req.session.privacyCache.get('basketLimitReached')) {
    basketLimitReached = true;
    req.session.privacyCache.set('basketLimitReached', null);
  }
  if (req.currentCustomer.raw.profile && req.currentCustomer.raw.authenticated) {
    var missedCustomerData;
    if (req.currentCustomer.addressBook.addresses.length === 0) {
      missedCustomerData = 'ADDRESS';
    } else if (req.currentCustomer.wallet.paymentInstruments.length === 0) {
      missedCustomerData = 'PAYMENT';
    }
    // Check if PreferenceCenter is enabled on SITE/BANNER
    // Check if preferenceDataExist
    else if ('enablePreferenceCenter' in customPreferences && customPreferences.enablePreferenceCenter == true) {
      if (!accountHelpers.preferenceDataExist(req.currentCustomer.raw.profile)) {
        missedCustomerData = 'PREFERENCES';
      }
    }
    if (missedCustomerData) {
      res.setViewData({
        missedCustomerData: missedCustomerData
      });
    }
  }

  var preferencesSiteType = 'siteType' in customPreferences ? customPreferences.siteType.value : null;
  var pid = req.querystring.partnerid;
  var qs = req.querystring;
  var isInstantIssuance =
    capOneUtils.getQueryVariable('isintantissuance', qs.toString()) == 'true' ? capOneUtils.getQueryVariable('isintantissuance', qs.toString()) : false;
  var cpfError = 'cpfError' in req.querystring || false; // CapOne Pre-fill error value
  var cpidError = 'cpidError' in req.querystring || false; // CapOne Partner ID Lookup error value
  var test29 = 'test29' in req.querystring ? req.querystring.test29 : false; // CapOne test 29 digit, partnerId lookup

  if (tsysHelper.isTsysMode() || pid || test29) {
    var saved = false;

    if (test29 && pid) {
      // Test button
      req.session.privacyCache.set('partnerId', pid);
      req.session.privacyCache.set('customerNo', req.currentCustomer.raw.profile.customerNo);
    }
    if (pid && isInstantIssuance === 'true') {
      var numberGen = require('*/cartridge/scripts/util/twentyNineDigitUtils');
      saved = numberGen.generate29DigitNumber(req) || saved;
      if (saved == false) {
        cpidError = true;
      }
    }
    res.setViewData({
      cpfError: cpfError,
      cpidError: cpidError,
      applyUser: saved,
      test29: test29,
      preferencesSiteType: preferencesSiteType
    });
  } else {
    res.setViewData({
      preferencesSiteType: preferencesSiteType
    });
  }

  // changes added from plugin_wishlist cartridge
  var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
  var wishListAccount = require('*/cartridge/models/account/wishListAccount');
  var wishListType = require('dw/customer/ProductList').TYPE_WISH_LIST;
  var apiWishList = productListHelper.getList(req.currentCustomer.raw, {
    type: wishListType
  });

  wishListAccount(accountModel, apiWishList);
  var wishlist = {
    UUID: apiWishList.ID
  };

  res.render('account/accountDashboard', {
    account: accountModel,
    accountlanding: true,
    breadcrumbs: [
      {
        htmlValue: Resource.msg('global.home', 'common', null),
        url: URLUtils.home().toString()
      }
    ],
    reportingURLs: reportingURLs,
    isCartMerged: isCartMerged,
    includeRecaptchaJS: true,
    socialLinks: true,
    wishlist: wishlist,
    basketLimitReached: basketLimitReached,
    BasketLimitMsg: Resource.msg('commom.basketlimit.message.merge', 'common', null),
    pageType: 'account',
    saksFirstEnabled: preferences.saksFirstEnabled
  });
  next();
});

server.replace('SaveProfile', server.middleware.https, csrfProtection.validateAjaxAccountRequest, function (req, res, next) {
  var Transaction = require('dw/system/Transaction');
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var Resource = require('dw/web/Resource');
  var URLUtils = require('dw/web/URLUtils');

  var formErrors = require('*/cartridge/scripts/formErrors');
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
  var preferences = require('*/cartridge/config/preferences');
  var Logger = require('dw/system/Logger');

  if (session.custom.isbot != null && session.custom.isbot) {
    res.json({
      error: [Resource.msg('error.message.login.bot', 'login', null)],
      botError: true
    });

    return next();
  }

  // Logger.debug('start SaveProfile !!!' + req.form.token);
  var result = hooksHelper('app.google.recaptcha.verify', 'validateToken', req.form.token, require('*/cartridge/scripts/googleRecaptchaUtil').validateToken);
  // Logger.debug('end SaveProfile !!!' + result.score);

  if (session.custom.isbot) {
    res.json({
      error: [Resource.msg('error.message.login.bot', 'login', null)],
      botError: true
    });

    return next();
  }

  var profileForm = server.forms.getForm('profile');

  // form validation
  if (profileForm.customer.email.value.toLowerCase() !== profileForm.customer.emailconfirm.value.toLowerCase()) {
    profileForm.valid = false;
    profileForm.customer.email.valid = false;
    profileForm.customer.emailconfirm.valid = false;
    profileForm.customer.emailconfirm.error = Resource.msg('error.message.mismatch.email', 'forms', null);
  }

  var result = {
    firstName: profileForm.customer.firstname.value,
    lastName: profileForm.customer.lastname.value,
    phone: profileForm.customer.phone.value,
    email: profileForm.customer.email.value,
    confirmEmail: profileForm.customer.emailconfirm.value,
    password: profileForm.login.password.value,
    hudsonbayrewards: profileForm.customer.hudsonbayrewards.value,
    profileForm: profileForm,
    isHBCLoyalty: Number(req.form.isHBCLoyalty) || false
  };
  if (profileForm.valid) {
    res.setViewData(result);
    this.on('route:BeforeComplete', function (req, res) {
      // eslint-disable-line no-shadow
      var HookMgr = require('dw/system/HookMgr');
      var formInfo = res.getViewData();
      var customer = CustomerMgr.getCustomerByCustomerNumber(req.currentCustomer.profile.customerNo);
      var profile = customer.getProfile();
      var customerLogin;
      var status;
      var isError = false;
      if (req.session.raw.isUserAuthenticated()) {
        customerLogin = true;
      } else {
        var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
        var isLegacyCustomer = false;
        if (profile !== null && 'legacyPasswordHash' in profile.custom && profile.custom.legacyPasswordHash !== null) {
          isLegacyCustomer = true;
        }
        // When the AB Testing Flag is ON and this is a legacy customer, we will first compare the saved hash password
        // If it matches, we will let the user login with the temp password (from site preference).
        // If the legacy hash donot match, we let the user login with the entered password
        if (preferences.isABTestingOn && isLegacyCustomer) {
          var matchLegacyPasswordResult = legacyCustomerLogin.isLegacyPassworMatched(profile.custom.legacyPasswordHash, formInfo.password);

          if (matchLegacyPasswordResult) {
            Transaction.wrap(function () {
              status = profile.credentials.setPassword(preferences.tempLegacyLogin, preferences.tempLegacyLogin, true);

              if (status.error) {
                formInfo.profileForm.login.password.valid = false;
                formInfo.profileForm.login.password.error = Resource.msg('error.message.currentpasswordnomatch', 'forms', null);
              } else {
                customerLogin = profile.credentials.setLogin(formInfo.email, preferences.tempLegacyLogin);
              }
            });
          } else {
            Transaction.wrap(function () {
              status = profile.credentials.setPassword(formInfo.password, formInfo.password, true);

              if (status.error) {
                formInfo.profileForm.login.password.valid = false;
                formInfo.profileForm.login.password.error = Resource.msg('error.message.currentpasswordnomatch', 'forms', null);
              }
            });
          }
        } else {
          Transaction.wrap(function () {
            status = profile.credentials.setPassword(formInfo.password, formInfo.password, true);

            if (status.error) {
              formInfo.profileForm.login.password.valid = false;
              formInfo.profileForm.login.password.error = Resource.msg('error.message.currentpasswordnomatch', 'forms', null);
            } else {
              customerLogin = profile.credentials.setLogin(formInfo.email, formInfo.password);
            }
          });
        }
      }
      delete formInfo.password;
      delete formInfo.confirmEmail;

      if (customerLogin) {
        var success = true;
        var timeout = false;
        var errorMsg;
        var rewardCounterReached = false;
        if (!!formInfo.hudsonbayrewards && HookMgr.hasHook('app.customer.loyalty.rewards.info') && !formInfo.isHBCLoyalty) {
          if ('counter' in req.session.raw.custom && Number(req.session.raw.custom.counter) === 5) {
            success = false;
            errorMsg = Resource.msg('error.profile.addaccount.hudsonreward.maxattempt', 'account', null);
            rewardCounterReached = true;
            Transaction.wrap(function () {
              profile.custom.hudsonReward = '';
            });
            res.json({
              success: false,
              fields: {
                dwfrm_profile_customer_hudsonbayrewards: errorMsg
              },
              rewardCounterReached: rewardCounterReached
            });
          } else {
            var loyaltyResponse = hooksHelper(
              'app.customer.loyalty.rewards.info',
              'checkLoyaltyRewards',
              formInfo.hudsonbayrewards,
              require('*/cartridge/scripts/loyalty/loyaltyRewardsInfoUtil').checkLoyaltyRewards
            );
            if (!!loyaltyResponse.reason && loyaltyResponse.reason === 'TIMEOUT') {
              success = true;
              timeout = true;
            } else if (!(loyaltyResponse !== null && Number(loyaltyResponse.ResponseCode) === 0)) {
              success = false;
              req.session.raw.custom.counter = 'counter' in req.session.raw.custom ? req.session.raw.custom.counter + 1 : 1; // eslint-disable-line
              errorMsg = Resource.msg('error.profile.addaccount.hudsonreward', 'account', null);
              if (Number(req.session.raw.custom.counter) === 5) {
                errorMsg = Resource.msg('error.profile.addaccount.hudsonreward.maxattempt', 'account', null);
                rewardCounterReached = true;
              }
              res.json({
                success: false,
                fields: {
                  dwfrm_profile_customer_hudsonbayrewards: errorMsg
                },
                rewardError: true,
                rewardCounterReached: rewardCounterReached
              });
            }
          }
        }
        if (success) {
          if (formInfo.hudsonbayrewards) {
            req.session.raw.custom.counter = 0; // eslint-disable-line
          }
          var previousEmail = profile.getEmail();
          Transaction.wrap(function () {
            profile.setFirstName(formInfo.firstName);
            profile.setLastName(formInfo.lastName);
            profile.setEmail(formInfo.email);
            profile.custom.hudsonReward = timeout ? '' : formInfo.hudsonbayrewards;
          });

          // hook logic to send updated loyalty profile in case profile was updated
          if (HookMgr.hasHook('app.customer.loyalty.rewards.profile') && !!profile.custom.moreCustomer) {
            hooksHelper(
              'app.customer.loyalty.rewards.profile',
              'callLoyaltyProfile',
              [profile, false],
              require('*/cartridge/scripts/loyalty/loyaltyProfileUtil').callLoyaltyProfile
            );
          }

          // Send account edited email
          if (previousEmail !== formInfo.email) {
            var Locale = require('dw/util/Locale');
            var currentLanguage = Locale.getLocale(req.locale.id).getLanguage();
            hooksHelper(
              'app.customer.email.account.edited',
              'sendAccountEditedEmail',
              [profile, previousEmail, currentLanguage],
              require('*/cartridge/scripts/helpers/accountHelpers').sendAccountEditedEmail
            );
          }

          // Update the custom attribute for last modified date as the system attribute lastModified is not true profile update from a customer perspective
          accountHelpers.updateAccLastModifiedDate(customer);

          delete formInfo.profileForm;
          delete formInfo.email;
          // UCID Call to Update the Profile
          hooksHelper('ucid.middleware.service', 'updateUCIDCustomer', [customer.profile, customer.profile.customerNo, null, null, null, null]);

          res.json({
            success: true,
            redirectUrl: URLUtils.url('Account-Show').toString()
          });
        }
      } else {
        if (!status.error) {
          formInfo.profileForm.customer.email.valid = false;
          formInfo.profileForm.customer.email.error = Resource.msg('error.message.username.invalid', 'forms', null);
        }

        delete formInfo.profileForm;
        delete formInfo.email;

        res.json({
          success: false,
          fields: formErrors.getFormErrors(profileForm)
        });
      }
    });
  } else {
    res.json({
      success: false,
      fields: formErrors.getFormErrors(profileForm)
    });
  }
  return next();
});

server.replace('SetNewPassword', server.middleware.https, consentTracking.consent, function (req, res, next) {
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var URLUtils = require('dw/web/URLUtils');

  var passwordForm = server.forms.getForm('newPasswords');
  passwordForm.clear();
  var token = req.querystring.Token;
  var resettingCustomer = CustomerMgr.getCustomerByToken(token);
  if (!resettingCustomer) {
    res.redirect(URLUtils.url('Account-ForgotPassword'));
  } else {
    res.render('account/password/newPassword', {
      passwordForm: passwordForm,
      token: token,
      includeRecaptchaJS: true,
      passwordRegex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#_?!@$%^()+=~`}{|&*-])^[^'<>/]{8,}$"
    });
  }
  next();
});

/**
 * Reason to override the controller - To clear the legacyPasswordHash custom attribute value if the first time customer login chooses to reset his password.
 * The hash value is cleared only if the password was successfully reset.
 */
server.replace('SaveNewPassword', server.middleware.https, function (req, res, next) {
  var Transaction = require('dw/system/Transaction');
  var Resource = require('dw/web/Resource');
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var preferences = require('*/cartridge/config/preferences');
  var passwordForm = server.forms.getForm('newPasswords');
  var token = req.querystring.Token;

  if (session.custom.isbot != null && session.custom.isbot) {
    passwordForm.clear();
    res.render('account/password/newPassword', {
      passwordForm: passwordForm,
      token: token,
      includeRecaptchaJS: true,
      error: Resource.msg('error.message.login.bot', 'login', null),
      botError: true,
      passwordRegex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#_?!@$%^()+=~`}{|&*-])^[^'<>/]{8,}$"
    });

    return next();
  }

  // Logger.debug('start SaveNewPassword !!!' + req.form.token);
  var result = hooksHelper('app.google.recaptcha.verify', 'validateToken', req.form.token, require('*/cartridge/scripts/googleRecaptchaUtil').validateToken);

  if (session.custom.isbot) {
    passwordForm.clear();
    res.render('account/password/newPassword', {
      passwordForm: passwordForm,
      token: token,
      includeRecaptchaJS: true,
      error: Resource.msg('error.message.login.bot', 'login', null),
      botError: true,
      passwordRegex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#_?!@$%^()+=~`}{|&*-])^[^'<>/]{8,}$"
    });

    return next();
  }
  var existingCustomer = CustomerMgr.getCustomerByToken(token);
  if (!empty(existingCustomer)) {
    var email = existingCustomer.profile.email;

    if (preferences.isABTestingOn) {
      // While the AB Testing is ON, just compare the input password hash with the saved password hash
      var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
      var newPswdHash = legacyCustomerLogin.getPasswordLegacyHashed(passwordForm.newpassword.value);
      var customer = CustomerMgr.getCustomerByLogin(email);
      if (!empty(customer) && !empty(customer.profile) && customer.profile.custom.legacyPasswordHash === newPswdHash) {
        passwordForm.valid = false;
        passwordForm.newpassword.valid = false;
        passwordForm.newpasswordconfirm.valid = false;
        passwordForm.newpassword.error = Resource.msg('error.message.same.password', 'forms', null);
      }
    } else {
      // If the AB Testing flag is OFF, authenticate the customer by using the new password to find out if the customer is trying to reset to the same password
      var authenticateCustomerResult;
      Transaction.wrap(function () {
        authenticateCustomerResult = CustomerMgr.authenticateCustomer(email, passwordForm.newpassword.value);
      });
      if (authenticateCustomerResult.authenticated) {
        passwordForm.valid = false;
        passwordForm.newpassword.valid = false;
        passwordForm.newpasswordconfirm.valid = false;
        passwordForm.newpassword.error = Resource.msg('error.message.same.password', 'forms', null);
      }
    }
  }

  if (passwordForm.newpassword.value !== passwordForm.newpasswordconfirm.value) {
    passwordForm.valid = false;
    passwordForm.newpassword.valid = false;
    passwordForm.newpasswordconfirm.valid = false;
    passwordForm.newpasswordconfirm.error = Resource.msg('error.message.mismatch.newpassword', 'forms', null);
  }

  res.setViewData({
    passwordRegex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#_?!@$%^()+=~`}{|&*-])^[^'<>/]{8,}$"
  });

  if (passwordForm.valid) {
    var result = {
      newPassword: passwordForm.newpassword.value,
      newPasswordConfirm: passwordForm.newpasswordconfirm.value,
      token: token,
      passwordForm: passwordForm
    };
    res.setViewData(result);
    this.on('route:BeforeComplete', function (req, res) {
      // eslint-disable-line no-shadow
      var URLUtils = require('dw/web/URLUtils');

      var formInfo = res.getViewData();
      var status;
      var resettingCustomer;
      Transaction.wrap(function () {
        resettingCustomer = CustomerMgr.getCustomerByToken(formInfo.token);

        if (!preferences.isABTestingOn) {
          status = resettingCustomer.profile.credentials.setPasswordWithToken(formInfo.token, formInfo.newPassword);
        } else {
          var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
          var newPswd = legacyCustomerLogin.getPasswordLegacyHashed(formInfo.newPassword);
          status = resettingCustomer.profile.credentials.setPasswordWithToken(formInfo.token, preferences.tempLegacyLogin);
          resettingCustomer.profile.custom.legacyPasswordHash = newPswd;
        }
      });

      if (status.error) {
        passwordForm.newpassword.valid = false;
        passwordForm.newpasswordconfirm.valid = false;
        passwordForm.newpasswordconfirm.error = Resource.msg('error.message.resetpassword.invalidformentry', 'forms', null);
        res.render('account/password/newPassword', {
          passwordForm: passwordForm,
          token: token,
          includeRecaptchaJS: true
        });
      } else {
        var comingFrom = 'forgotPassword';
        var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
        accountHelpers.updateAccLastModifiedDate(resettingCustomer, comingFrom);

        if (!preferences.isABTestingOn) {
          var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
          legacyCustomerLogin.removeLegacyFlag(resettingCustomer);
        }
        var Locale = require('dw/util/Locale');
        var currentLanguage = Locale.getLocale(req.locale.id).getLanguage();
        hooksHelper(
          'app.customer.email.update.password',
          'sendUpdatePasswordEmail',
          resettingCustomer.profile,
          currentLanguage,
          require('*/cartridge/scripts/helpers/accountHelpers').sendUpdatePasswordEmail
        );
        res.redirect(URLUtils.url('Login-Show'));
      }
    });
  } else {
    passwordForm.clear();
    res.render('account/password/newPassword', {
      passwordForm: passwordForm,
      token: token,
      includeRecaptchaJS: true
    });
  }
  next();
});

server.get('ForgotPassword', server.middleware.https, csrfProtection.generateToken, function (req, res, next) {
  res.render('account/password/forgotPassword');
  res.setViewData({
    includeRecaptchaJS: true
  });
  next();
});

server.replace('PasswordResetDialogForm', server.middleware.https, function (req, res, next) {
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var Resource = require('dw/web/Resource');
  var URLUtils = require('dw/web/URLUtils');
  var email = req.form.loginEmail;
  var errorMsg;
  var isValid;
  var resettingCustomer;
  var mobile = req.querystring.mobile;
  var receivedMsgHeading = Resource.msg('label.resetpasswordreceived', 'login', null);
  var receivedMsgBody = Resource.msg('msg.requestedpasswordreset', 'login', null);
  var buttonText = Resource.msg('button.text.loginform', 'login', null);
  var returnUrl = URLUtils.url('Login-Show').toString();

  if (session.custom.isbot != null && session.custom.isbot) {
    res.json({
      error: [Resource.msg('error.message.login.bot', 'login', null)],
      botError: true
    });

    return next();
  }

  // var result = hooksHelper('app.google.recaptcha.verify', 'validateToken', req.form.token, require('*/cartridge/scripts/googleRecaptchaUtil').validateToken);

  if (session.custom.isbot) {
    res.json({
      error: [Resource.msg('error.message.login.bot', 'login', null)],
      botError: true
    });

    return next();
  }

  if (email) {
    isValid = validateEmail(email);
    if (isValid) {
      resettingCustomer = CustomerMgr.getCustomerByLogin(email);
      if (resettingCustomer) {
        var Locale = require('dw/util/Locale');
        var currentLanguage = Locale.getLocale(req.locale.id).getLanguage();
        hooksHelper(
          'app.customer.email.password.reset',
          'sendPasswordResetEmail',
          resettingCustomer,
          currentLanguage,
          require('*/cartridge/scripts/helpers/accountHelpers').sendPasswordResetEmail
        );
        res.json({
          success: true,
          receivedMsgHeading: receivedMsgHeading,
          receivedMsgBody: receivedMsgBody,
          buttonText: buttonText,
          mobile: mobile,
          returnUrl: returnUrl,
          resetMessage1: Resource.msg('msg.reset.password.message1', 'login', null),
          resetMessage2: Resource.msgf('msg.reset.password.message2', 'login', null, email)
        });
      } else {
        errorMsg = Resource.msg('msg.error.invalid.email', 'login', null);
        res.json({
          invalid: true,
          invalidMsg: errorMsg
        });
      }
    } else {
      errorMsg = Resource.msg('error.message.passwordreset', 'login', null);
      res.json({
        fields: {
          loginEmail: errorMsg
        }
      });
    }
  } else {
    errorMsg = Resource.msg('error.message.required', 'login', null);
    res.json({
      fields: {
        loginEmail: errorMsg
      }
    });
  }
  next();
});

server.get(
  'AddAccount',
  server.middleware.https,
  csrfProtection.generateToken,
  userLoggedIn.validateLoggedIn,
  consentTracking.consent,
  function (req, res, next) {
    var preferences = require('*/cartridge/config/preferences');

    var accountModel = getModel(req);
    var profileForm = server.forms.getForm('profile');
    profileForm.clear();
    profileForm.customer.firstname.value = accountModel.profile.firstName;
    profileForm.customer.lastname.value = accountModel.profile.lastName;
    profileForm.customer.hudsonbayrewards.value = accountModel.profile.hbcRewardNumber;
    profileForm.customer.email.value = accountModel.profile.email;
    res.setViewData({ hudsonReward: preferences.hudsonReward });
    res.render('account/addAccount', {
      includeRecaptchaJS: true,
      profileForm: profileForm,
      pageType: 'account'
    });
    next();
  }
);

server.post('SaveAccount', server.middleware.https, csrfProtection.validateAjaxAccountRequest, function (req, res, next) {
  var Transaction = require('dw/system/Transaction');
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var URLUtils = require('dw/web/URLUtils');
  var HookMgr = require('dw/system/HookMgr');
  var Resource = require('dw/web/Resource');
  var formErrors = require('*/cartridge/scripts/formErrors');
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');

  var profileForm = server.forms.getForm('profile');

  var result = {
    firstName: profileForm.customer.firstname.value,
    lastName: profileForm.customer.lastname.value,
    email: profileForm.customer.email.value,
    hudsonbayrewards: profileForm.customer.hudsonbayrewards.value,
    profileForm: profileForm
  };
  if (profileForm.valid) {
    res.setViewData(result);
    this.on('route:BeforeComplete', function (req, res) {
      // eslint-disable-line no-shadow
      var formInfo = res.getViewData();
      var customer = CustomerMgr.getCustomerByCustomerNumber(req.currentCustomer.profile.customerNo);
      var profile = customer.getProfile();

      if (profile) {
        var success = true;
        if (!!formInfo.hudsonbayrewards && HookMgr.hasHook('app.customer.loyalty.rewards.info')) {
          var loyaltyResponse = hooksHelper(
            'app.customer.loyalty.rewards.info',
            'checkLoyaltyRewards',
            formInfo.hudsonbayrewards,
            require('*/cartridge/scripts/loyalty/loyaltyRewardsInfoUtil').checkLoyaltyRewards
          );
          if (!(loyaltyResponse !== null && Number(loyaltyResponse.ResponseCode) === 0)) {
            success = false;
            var errorMsg = Resource.msg('error.profile.addaccount.hudsonreward', 'account', null);
            res.json({
              success: false,
              fields: {
                dwfrm_profile_customer_hudsonbayrewards: errorMsg
              }
            });
          }
        }
        if (success) {
          var previousEmail = profile.getEmail();
          Transaction.wrap(function () {
            profile.setFirstName(formInfo.firstName);
            profile.setLastName(formInfo.lastName);
            profile.setEmail(formInfo.email);
            profile.custom.hudsonReward = formInfo.hudsonbayrewards;
          });

          // Update the custom attribute for last modified date as the system attribute lastModified is not true profile update from a customer perspective
          accountHelpers.updateAccLastModifiedDate(req.currentCustomer.raw);

          // UCID Call to Update the Profile
          hooksHelper('ucid.middleware.service', 'updateUCIDCustomer', [profile, profile.customerNo, null, null, null, null]);

          // Send account edited email
          if (previousEmail !== formInfo.email) {
            var Locale = require('dw/util/Locale');
            var currentLanguage = Locale.getLocale(req.locale.id).getLanguage();
            hooksHelper(
              'app.customer.email.account.edited',
              'sendAccountEditedEmail',
              [profile, previousEmail],
              currentLanguage,
              require('*/cartridge/scripts/helpers/accountHelpers').sendAccountEditedEmail
            );
          }

          res.json({
            success: true,
            redirectUrl: URLUtils.url('Account-Show').toString()
          });
        }
      } else {
        res.json({
          success: true,
          redirectUrl: URLUtils.url('Login-Show').toString()
        });
      }
    });
  } else {
    res.json({
      success: false,
      fields: formErrors.getFormErrors(profileForm)
    });
  }
  return next();
});

server.replace('Header', server.middleware.include, function (req, res, next) {
  var Site = require('dw/system/Site');
  var storeLocatorURL =
    'storeLocatorURL' in Site.current.preferences.custom && !empty(Site.current.preferences.custom.storeLocatorURL)
      ? Site.current.preferences.custom.storeLocatorURL
      : '';
  var locale = req.locale.id;
  res.setViewData({
    storeLocatorURL: storeLocatorURL + locale.replace('_', '-').toLowerCase() + '?'
  });

  if (req.currentCustomer.raw.profile && !req.currentCustomer.raw.authenticated) {
    res.setViewData({
      softLoggedIn: true
    });
  }
  var cookieValue = cookiesHelper.read('RememberMeForced');
  if (!req.currentCustomer.raw.authenticated && cookieValue) {
    res.setViewData({
      softLoggedOut: true
    });
  }
  var profile = {};
  if (req.currentCustomer.raw.profile) {
    // These values not available for a soft logged in user
    profile.firstName = req.currentCustomer.raw.profile.firstName;
    profile.lastName = req.currentCustomer.raw.profile.lastName;
    profile.email = req.currentCustomer.raw.profile.email;
    profile.phone = req.currentCustomer.raw.profile.phoneHome;
  }

  var template = req.querystring.mobile ? 'account/mobileHeader' : 'account/header';
  res.render(template, {
    name: req.currentCustomer.raw.profile ? req.currentCustomer.raw.profile.firstName : null,
    profile: profile
  });
  next();
});

server.replace('SavePassword', server.middleware.https, csrfProtection.validateAjaxAccountRequest, function (req, res, next) {
  var Transaction = require('dw/system/Transaction');
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var Resource = require('dw/web/Resource');
  var URLUtils = require('dw/web/URLUtils');
  var preferences = require('*/cartridge/config/preferences');
  var formErrors = require('*/cartridge/scripts/formErrors');
  var profileForm = server.forms.getForm('profile');
  var newPasswords = profileForm.login.newpasswords;

  // form validation
  if (profileForm.login.currentpassword.value === newPasswords.newpassword.value) {
    // Logic to not let customer use the same password again in the Password reset flow SFDEV-5349
    var email = req.currentCustomer.profile.email;

    if (preferences.isABTestingOn) {
      // While the AB Testing is ON, just compare the input password hash with the saved password hash
      var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
      var newPswdHash = legacyCustomerLogin.getPasswordLegacyHashed(newPasswords.newpassword.value);
      var customer = CustomerMgr.getCustomerByLogin(email);
      if (!empty(customer) && !empty(customer.profile) && customer.profile.custom.legacyPasswordHash === newPswdHash) {
        profileForm.valid = false;
        newPasswords.newpassword.valid = false;
        newPasswords.newpasswordconfirm.valid = false;
        newPasswords.newpassword.error = Resource.msg('error.message.same.password', 'forms', null);
      }
    } else {
      // If the AB Testing flag is OFF, authenticate the customer by using the new password to find out if the customer is trying to reset to the same password
      var authenticateCustomerResult;
      Transaction.wrap(function () {
        authenticateCustomerResult = CustomerMgr.authenticateCustomer(email, newPasswords.newpassword.value);
      });
      if (authenticateCustomerResult.authenticated) {
        profileForm.valid = false;
        newPasswords.newpassword.valid = false;
        newPasswords.newpasswordconfirm.valid = false;
        newPasswords.newpassword.error = Resource.msg('error.message.same.password', 'forms', null);
      }
    }
  } else if (newPasswords.newpassword.value !== newPasswords.newpasswordconfirm.value) {
    profileForm.valid = false;
    newPasswords.newpassword.valid = false;
    newPasswords.newpasswordconfirm.valid = false;
    newPasswords.newpasswordconfirm.error = Resource.msg('error.message.mismatch.newpassword', 'forms', null);
  } else if (!CustomerMgr.isAcceptablePassword(newPasswords.newpassword.value) || !CustomerMgr.isAcceptablePassword(newPasswords.newpasswordconfirm.value)) {
    profileForm.valid = false;
    newPasswords.newpassword.valid = false;
    newPasswords.newpasswordconfirm.valid = false;
    newPasswords.newpasswordconfirm.error = Resource.msg('error.message.password.constraints.not.matched', 'forms', null);
  }

  var result = {
    currentPassword: profileForm.login.currentpassword.value,
    newPassword: newPasswords.newpassword.value,
    newPasswordConfirm: newPasswords.newpasswordconfirm.value,
    profileForm: profileForm
  };

  if (profileForm.valid) {
    res.setViewData(result);
    this.on('route:BeforeComplete', function (req, res) {
      // eslint-disable-line no-shadow
      var formInfo = res.getViewData();
      var customer = CustomerMgr.getCustomerByCustomerNumber(req.currentCustomer.profile.customerNo);
      var status;
      var isError = false;

      if (!preferences.isABTestingOn) {
        Transaction.wrap(function () {
          status = customer.profile.credentials.setPassword(formInfo.newPassword, formInfo.currentPassword, true);
        });
        isError = status.error;
      } else {
        var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
        var newPswd = legacyCustomerLogin.getPasswordLegacyHashed(formInfo.newPassword);

        var oldPswdHash = legacyCustomerLogin.getPasswordLegacyHashed(formInfo.currentPassword);
        if (!empty(customer.profile.custom.legacyPasswordHash) && customer.profile.custom.legacyPasswordHash === oldPswdHash) {
          Transaction.wrap(function () {
            status = customer.profile.credentials.setPassword(preferences.tempLegacyLogin, preferences.tempLegacyLogin, true);
            customer.profile.custom.legacyPasswordHash = newPswd;
          });
          isError = status.error;
        } else {
          isError = true;
        }
      }
      if (isError) {
        formInfo.profileForm.login.currentpassword.valid = false;
        formInfo.profileForm.login.currentpassword.error = Resource.msg('error.message.currentpasswordnomatch', 'forms', null);

        delete formInfo.currentPassword;
        delete formInfo.newPassword;
        delete formInfo.newPasswordConfirm;
        delete formInfo.profileForm;

        res.json({
          success: false,
          fields: formErrors.getFormErrors(profileForm)
        });
      } else {
        var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
        // Update the custom attribute for last modified date as the system attribute lastModified is not true profile update from a customer perspective
        accountHelpers.updateAccLastModifiedDate(customer);

        delete formInfo.currentPassword;
        delete formInfo.newPassword;
        delete formInfo.newPasswordConfirm;
        delete formInfo.profileForm;

        res.json({
          success: true,
          redirectUrl: URLUtils.url('Account-Show').toString()
        });

        var Locale = require('dw/util/Locale');
        var currentLanguage = Locale.getLocale(req.locale.id).getLanguage();
        hooksHelper(
          'app.customer.email.update.password',
          'sendUpdatePasswordEmail',
          customer.profile,
          currentLanguage,
          require('*/cartridge/scripts/helpers/accountHelpers').sendUpdatePasswordEmail
        );
      }
    });
  } else {
    res.json({
      success: false,
      fields: formErrors.getFormErrors(profileForm)
    });
  }
  return next();
});

// Replaced the base route in order to send the registration email from CNS hook
server.replace('SubmitRegistration', server.middleware.https, csrfProtection.validateAjaxRequest, function (req, res, next) {
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var Resource = require('dw/web/Resource');
  var HookMgr = require('dw/system/HookMgr');
  var preferences = require('*/cartridge/config/preferences');
  var formErrors = require('*/cartridge/scripts/formErrors');
  var registrationForm = server.forms.getForm('profile');
  var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');

  if (session.custom.isbot != null && session.custom.isbot) {
    res.json({
      error: [Resource.msg('error.message.login.bot', 'login', null)],
      botError: true
    });
    return next();
  }
  var result = hooksHelper('app.google.recaptcha.verify', 'validateToken', req.form.token, require('*/cartridge/scripts/googleRecaptchaUtil').validateToken);
  if (session.custom.isbot) {
    res.json({
      error: [Resource.msg('error.message.login.bot', 'login', null)],
      botError: true
    });
    return next();
  }

  if (registrationForm.login.password.value !== registrationForm.login.passwordconfirm.value) {
    registrationForm.login.password.valid = false;
    registrationForm.login.passwordconfirm.valid = false;
    registrationForm.login.passwordconfirm.error = Resource.msg('error.message.mismatch.password', 'forms', null);
    registrationForm.valid = false;
  }

  if (!CustomerMgr.isAcceptablePassword(registrationForm.login.password.value)) {
    registrationForm.login.password.valid = false;
    registrationForm.login.passwordconfirm.valid = false;
    registrationForm.login.password.error = Resource.msg('error.message.password.constraints.not.matched', 'forms', null);
    // If there is an error for Password Missmatch also, clear that.
    delete registrationForm.login.passwordconfirm.error;
    registrationForm.valid = false;
  }
  // changes done to merge the guest wishlist while the user is creating an acc
  var list = productListHelper.getList(req.currentCustomer.raw, {
    type: 10
  });

  var Locale = require('dw/util/Locale');
  var currentLanguage = Locale.getLocale(req.locale.id).getLanguage();
  var preferredLanguage = 'ENGLISH';
  if (currentLanguage == 'fr') {
    preferredLanguage = 'FRENCH';
  }

  // Check hbc_loyalty hook is available
  if (HookMgr.hasHook('app.customer.hbc.loyalty.enroll')) {
    // Import Loyalty Form Helper Script
    var loyaltyFormValidator = require('*/cartridge/scripts/util/enrollFormHelper');
    // Check which radio button was selected for Hudson's Bay Rewards Program
    // i.e, (Sign Up, Already A Member, No Thanks)
    const selectedRadio = registrationForm.customer.loyaltyEnroll.selectedLoyaltyRegistrationRadio.value;
    registrationForm = loyaltyFormValidator.validateLoyaltyFields(selectedRadio, registrationForm);
  }

  // setting variables for the BeforeComplete function
  var registrationFormObj = {
    firstName: registrationForm.customer.firstname.value,
    lastName: registrationForm.customer.lastname.value,
    phone: registrationForm.customer.phone.value,
    email: registrationForm.customer.email.value,
    emailConfirm: registrationForm.customer.emailconfirm.value,
    password: registrationForm.login.password.value,
    passwordConfirm: registrationForm.login.passwordconfirm.value,
    hudsonbayrewards: req.form.hbcRewardNumber,
    zipCode: registrationForm.customer.zipcode.value,
    addtoemaillist: registrationForm.customer.addtoemaillist.value,
    saksOptIn: req.form.saksOpt,
    saksAvenueOptIn: req.form.saksAvenueOpt,
    isCanadianCustomer: req.form.isCanadianCustomer,
    validForm: registrationForm.valid,
    form: registrationForm,
    list: list
  };

  if (registrationForm.valid) {
    res.setViewData(registrationFormObj);

    this.on('route:BeforeComplete', function (req, res) {
      // eslint-disable-line no-shadow
      var Transaction = require('dw/system/Transaction');
      var URLUtils = require('dw/web/URLUtils');
      var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
      var authenticatedCustomer;
      var newCustomerProfileObj;
      var serverError;

      // getting variables for the BeforeComplete function
      var registrationForm = res.getViewData(); // eslint-disable-line

      if (registrationForm.validForm) {
        var login = registrationForm.email;
        var password = registrationForm.password;

        // attempt to create a new user and log that user in.
        try {
          Transaction.wrap(function () {
            var error = {};
            var profilePassword;
            if (preferences.isABTestingOn) {
              profilePassword = preferences.tempLegacyLogin;
            } else {
              profilePassword = password;
            }
            var newCustomer = CustomerMgr.createCustomer(login, profilePassword);
            var authenticateCustomerResult = CustomerMgr.authenticateCustomer(login, profilePassword);
            if (authenticateCustomerResult.status !== 'AUTH_OK') {
              error = {
                authError: true,
                status: authenticateCustomerResult.status
              };
              throw error;
            }

            authenticatedCustomer = CustomerMgr.loginCustomer(authenticateCustomerResult, false);

            if (!authenticatedCustomer) {
              error = {
                authError: true,
                status: authenticateCustomerResult.status
              };
              throw error;
            } else {
              // assign values to the profile
              var newCustomerProfile = newCustomer.getProfile();

              newCustomerProfile.firstName = registrationForm.firstName;
              newCustomerProfile.lastName = registrationForm.lastName;
              newCustomerProfile.phoneHome = registrationForm.phone ? registrationForm.phone : '';
              newCustomerProfile.email = registrationForm.email;

              var listGuest = registrationForm.list;
              if (!empty(listGuest.items)) {
                req.session.privacyCache.set('mergeWishlist', true);
              }
              if (authenticatedCustomer) {
                var listLoggedIn = productListHelper.getList(authenticatedCustomer, { type: 10 });
                productListHelper.mergelists(listLoggedIn, listGuest, req, {
                  type: 10
                });
                productListHelper.updateWishlistPrivacyCache(req.currentCustomer.raw, req, { type: 10 });
              }

              if (preferences.isABTestingOn) {
                var legacyCustomerLogin = require('*/cartridge/scripts/helpers/legacyCustomerLogin');
                var newPswd = legacyCustomerLogin.getPasswordLegacyHashed(password);
                newCustomerProfile.custom.legacyPasswordHash = newPswd;
              }

              if (registrationForm.hudsonbayrewards) {
                newCustomerProfile.custom.hudsonReward = registrationForm.hudsonbayrewards;
              }
              if (registrationForm.zipCode) {
                newCustomerProfile.custom.zipCode = registrationForm.zipCode;
              }
              accountHelpers.updateCustomerPreferences(req, newCustomerProfile.email, newCustomerProfile, registrationForm);

              // Update the custom attribute for last modified date as the system attribute lastModified is not true profile update from a customer perspective
              accountHelpers.updateAccLastModifiedDate(newCustomer);

              if (registrationForm.isCanadianCustomer) {
                newCustomerProfile.custom.isCanadianCustomer = registrationForm.isCanadianCustomer === 'T';
              }

              newCustomerProfile.custom.preferredLanguage = preferredLanguage;

              // Create new object for capital one apply
              newCustomerProfileObj = capOneUtils.capOneNewCustomer(registrationForm, newCustomerProfile, req.querystring.test29);

              // hook logic to send create loyalty profile
              if (HookMgr.hasHook('app.customer.loyalty.rewards.profile')) {
                var loyaltyResponse = hooksHelper(
                  'app.customer.loyalty.rewards.profile',
                  'callLoyaltyProfile',
                  [newCustomerProfile, true],
                  require('*/cartridge/scripts/loyalty/loyaltyProfileUtil').callLoyaltyProfile
                );
                if (loyaltyResponse !== null && Number(loyaltyResponse.ResponseCode) === 0) {
                  newCustomerProfile.custom.moreCustomer =
                    loyaltyResponse.LoyaltyProfile !== null &&
                    loyaltyResponse.LoyaltyProfile.CustomerInfo !== null &&
                    !!loyaltyResponse.LoyaltyProfile.CustomerInfo.moreNumber
                      ? loyaltyResponse.LoyaltyProfile.CustomerInfo.moreNumber
                      : '';
                }
              }
            }
          });
        } catch (e) {
          if (e.authError) {
            serverError = true;
          } else {
            registrationForm.validForm = false;
            registrationForm.form.customer.email.valid = false;
            registrationForm.form.customer.emailconfirm.valid = false;
            registrationForm.form.customer.email.error = Resource.msg('error.message.username.invalid', 'forms', null);
          }
        }
      }

      delete registrationForm.password;
      delete registrationForm.passwordConfirm;
      formErrors.removeFormValues(registrationForm.form);

      if (serverError) {
        res.setStatusCode(500);
        res.json({
          success: false,
          errorMessage: Resource.msg('error.message.unable.to.create.account', 'login', null)
        });

        return;
      }

      if (registrationForm.validForm) {
        // send a registration email via CNS hook code change from accounthelpers
        hooksHelper(
          'app.customer.email.account.created',
          'sendCreateAccountEmail',
          authenticatedCustomer.profile,
          currentLanguage,
          require('*/cartridge/scripts/helpers/accountHelpers').sendCreateAccountEmail
        );

        // UCID Call to Sync
        hooksHelper('ucid.middleware.service', 'createUCIDCustomer', [authenticatedCustomer.profile, authenticatedCustomer.profile.customerNo, null]);

        var hasLoyaltyHook = HookMgr.hasHook('app.customer.hbc.loyalty.enroll');
        var loyaltyResponse;
        // Call Enroll API if HBC Loyalty Hook is available
        if (hasLoyaltyHook) {
          // Assign selected radio to var from form
          var loyaltyRegistrationOption = registrationForm.form.customer.loyaltyEnroll.selectedLoyaltyRegistrationRadio.value;
          // Check optout option is not selected
          if (loyaltyRegistrationOption !== 'optout') {
            // Get the newly created customer Profile - to save reward number later
            const customer = CustomerMgr.getCustomerByCustomerNumber(authenticatedCustomer.profile.customerNo);
            const profile = customer.getProfile();

            switch (loyaltyRegistrationOption) {
              case 'signup':
                // User clicked Sign Up
                loyaltyResponse = hooksHelper('app.customer.hbc.loyalty.enroll', 'callLoyaltyEnroll', [profile, registrationForm, true]);
                if (loyaltyResponse !== null && Number(loyaltyResponse.response_code) === 1) {
                  Transaction.wrap(function () {
                    // Assign loyalty_id to the customer profile
                    profile.custom.hudsonReward = loyaltyResponse.errorMessage == null ? String(loyaltyResponse.loyalty_id) : '';
                    profile.custom.isLinkedAccount = false; // if false, indicates user was enrolled
                  });

                  // The session data is set up within callLoyaltyEnroll hook but also needs profile.custom.hudsonReward to verify enrollment; profile.custom.hudsonReward is assigned after session data is created - we must clear it.
                  req.session.privacyCache.set('loyalty', null);
                }

                break;
              case 'members':
                // User clicked Already A Member
                loyaltyResponse = hooksHelper('app.customer.hbc.loyalty.enroll', 'linkAccount', [profile, registrationForm]);

                if (loyaltyResponse !== null && Number(loyaltyResponse.response_code) === 1) {
                  Transaction.wrap(function () {
                    // Assign loyalty_id to the customer profile from the customer lookup response
                    profile.custom.hudsonReward = loyaltyResponse.loyaltyId;
                    profile.custom.isLinkedAccount = true;
                  });
                  // The session data is set up within linkAccount hook but also needs profile.custom.hudsonReward to verify enrollment; profile.custom.hudsonReward is assigned after session data is created - we must clear it.
                  req.session.privacyCache.set('loyalty', null);
                }
                break;
              default:
                // nothing to do - selected radio option does not match
                loyaltyResponse = {
                  success: false
                };
            }
          }
        }

        res.setViewData({
          authenticatedCustomer: authenticatedCustomer
        });

        var capOneApply = req.querystring.apply;
        var isCanadianCustomer = registrationForm.isCanadianCustomer && registrationForm.isCanadianCustomer === 'T';
        var test29 = req.querystring.test29; // test 29 digit

        if ((capOneApply || test29) && !isCanadianCustomer) {
          var result = capOneUtils.prefillCapOne(req, newCustomerProfileObj);

          res.json({
            success: true,
            redirectUrl: result.object.redirectUrl
          });
        } else {
          if (hasLoyaltyHook && loyaltyResponse !== undefined && loyaltyResponse.success === false) {
            res.json({
              success: true,
              redirectUrl: URLUtils.url('Loyalty-Fail').relative().toString()
            });
          } else {
            res.json({
              success: true,
              redirectUrl: accountHelpers.getLoginRedirectURL(req.querystring.rurl, req.session.privacyCache, true)
            });
          }
        }

        req.session.privacyCache.set('args', null);
      } else {
        res.json({
          fields: formErrors.getFormErrors(registrationForm)
        });
      }
    });
  } else {
    res.json({
      fields: formErrors.getFormErrors(registrationForm)
    });
  }

  return next();
});

server.get('Profile', server.middleware.https, userLoggedIn.validateLoggedIn, function (req, res, next) {
  var accountModel = getModel(req);
  res.render('account/customerProfile', {
    includeRecaptchaJS: true,
    account: accountModel,
    pageType: 'profile'
  });
  next();
});

server.replace(
  'EditProfile',
  server.middleware.https,
  csrfProtection.generateToken,
  userLoggedIn.validateLoggedIn,
  consentTracking.consent,
  function (req, res, next) {
    var preferences = require('*/cartridge/config/preferences');
    var accountModel = getModel(req);
    var profileForm = server.forms.getForm('profile');
    profileForm.clear();
    profileForm.customer.firstname.value = accountModel.profile.firstName;
    profileForm.customer.lastname.value = accountModel.profile.lastName;
    profileForm.customer.phone.value = accountModel.profile.phone;
    profileForm.customer.email.value = accountModel.profile.email;
    profileForm.customer.hudsonbayrewards.value = accountModel.profile.hbcRewardNumber;
    res.setViewData({ hudsonReward: preferences.hudsonReward });
    // Price Overrride/Price adjustment for OOBO
    if (req.session.raw.isUserAuthenticated()) {
      res.setViewData({ isAgentUser: true });
    }
    res.render('account/profile', {
      profileForm: profileForm,
      includeRecaptchaJS: true,
      pageType: 'profile',
      counter: req.session.raw.custom.counter
    });
    next();
  }
);

server.replace(
  'EditPassword',
  server.middleware.https,
  csrfProtection.generateToken,
  userLoggedIn.validateLoggedIn,
  consentTracking.consent,
  function (req, res, next) {
    var profileForm = server.forms.getForm('profile');
    profileForm.clear();
    res.render('account/password', {
      includeRecaptchaJS: true,
      profileForm: profileForm,
      pageType: 'profile',
      passwordRegex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#_?!@$%^()+=~`}{|&*-])^[^'<>/]{8,}$"
    });
    next();
  }
);

server.get(
  'EmailPreferences',
  server.middleware.https,
  csrfProtection.generateToken,
  userLoggedIn.validateLoggedIn,
  consentTracking.consent,
  function (req, res, next) {
    var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
    var profileForm = server.forms.getForm('profile');
    profileForm.clear();
    var preferences = accountHelpers.getCustomerPreferences(req.currentCustomer.raw.profile);

    res.render('account/emailPreferences', {
      includeRecaptchaJS: true,
      profileForm: profileForm,
      preferences: preferences,
      pageType: 'profile'
    });
    next();
  }
);

server.post('SavePreferences', server.middleware.https, csrfProtection.validateAjaxAccountRequest, function (req, res, next) {
  var Resource = require('dw/web/Resource');
  var URLUtils = require('dw/web/URLUtils');
  var formErrors = require('*/cartridge/scripts/formErrors');
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');

  var preferencesForm = server.forms.getForm('profile');
  // setting variables for the BeforeComplete function
  var preferencesFormObj = {
    saksOptIn: req.form.saksOpt,
    saksAvenueOptIn: req.form.saksAvenueOpt,
    saksOptInCA: req.form.saksOptCA,
    saksAvenueOptInCA: req.form.saksAvenueOptCA,
    validForm: preferencesForm.valid,
    form: preferencesForm
  };

  if (preferencesForm.valid) {
    res.setViewData(preferencesFormObj);

    this.on('route:BeforeComplete', function (req, res) {
      // eslint-disable-line no-shadow
      var Transaction = require('dw/system/Transaction');
      var serverError;

      // getting variables for the BeforeComplete function
      var preferencesFormData = res.getViewData(); // eslint-disable-line

      if (preferencesFormData.validForm) {
        // attempt to create a new user and log that user in.
        try {
          Transaction.wrap(function () {
            var customerProfile = req.currentCustomer.raw.profile;
            // TBD Below code may get updated based on Maureen Update. Need to confirms with Maureen.
            if (customerProfile) {
              accountHelpers.updateCustomerPreferences(req, customerProfile.email, customerProfile, preferencesFormData);
            }
          });
          // Update the custom attribute for last modified date as the system attribute lastModified is not true profile update from a customer perspective
          accountHelpers.updateAccLastModifiedDate(req.currentCustomer.raw);
        } catch (e) {
          serverError = true;
        }
      }

      if (serverError) {
        res.setStatusCode(500);
        res.json({
          success: false,
          errorMessage: Resource.msg('error.message.unable.to.save.preferences', 'registration', null)
        });
        return;
      }

      res.json({
        success: true,
        redirectUrl: URLUtils.url('Account-Profile', 'save', 'true').toString()
      });
    });
  } else {
    res.json({
      fields: formErrors.getFormErrors(preferencesForm)
    });
  }
  return next();
});

server.post('SavePreferredLanguage', server.middleware.https, csrfProtection.validateAjaxAccountRequest, function (req, res, next) {
  var Resource = require('dw/web/Resource');
  var URLUtils = require('dw/web/URLUtils');

  var formErrors = require('*/cartridge/scripts/formErrors');
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');

  var preferencesForm = server.forms.getForm('profile');
  // setting variables for the BeforeComplete function
  var preferencesFormObj = {
    preferredlanguage: preferencesForm.customer.preferences.preferredlanguage.value,
    addtoemaillist: req.form.emailPreferncesOptIn,
    validForm: preferencesForm.customer.preferences.preferredlanguage.valid,
    form: preferencesForm
  };

  if (preferencesFormObj.validForm) {
    res.setViewData(preferencesFormObj);

    // eslint-disable-next-line consistent-return
    this.on('route:BeforeComplete', function (req, res) {
      // eslint-disable-line no-shadow
      var Transaction = require('dw/system/Transaction');
      var CustomObjectMgr = require('dw/object/CustomObjectMgr');
      var serverError;

      // getting variables for the BeforeComplete function
      var preferencesFormData = res.getViewData(); // eslint-disable-line

      if (preferencesFormData.validForm) {
        // attempt to create a new user and log that user in.
        try {
          Transaction.wrap(function () {
            var customerProfile = req.currentCustomer.raw.profile;
            if (customerProfile) {
              if (preferencesFormData.preferredlanguage) {
                customerProfile.custom.preferredLanguage = preferencesFormData.preferredlanguage; // Please update the Custom Attribute ID. Could be ENUM Type
              }
              accountHelpers.updateCustomerPreferences(req, customerProfile.email, customerProfile, preferencesFormData);
            }
          });
          // Update the custom attribute for last modified date as the system attribute lastModified is not true profile update from a customer perspective
          accountHelpers.updateAccLastModifiedDate(req.currentCustomer.raw);
        } catch (e) {
          serverError = true;
        }
      }

      if (serverError) {
        res.setStatusCode(500);
        res.json({
          success: false,
          errorMessage: Resource.msg('error.message.unable.to.save.preferences', 'registration', null)
        });
        return next();
      }

      res.json({
        success: true,
        redirectUrl: URLUtils.url('Account-Profile', 'save', 'true').toString()
      });
    });
  } else {
    res.json({
      fields: formErrors.getFormErrors(preferencesForm)
    });
  }
  return next();
});

module.exports = server.exports();
