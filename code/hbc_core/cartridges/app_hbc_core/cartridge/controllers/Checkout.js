'use strict';

var server = require('server');
server.extend(module.superModule);

var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var addressHelper = require('*/cartridge/scripts/helpers/addressHelper');
var preferences = require('*/cartridge/config/preferences');
var ShippingHelper = require('*/cartridge/scripts/checkout/shippingHelpers');
var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');

server.post('CheckHudsonReward', server.middleware.https, function (req, res, next) {
  var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
  var result;
  var success = true;
  // var regex = new RegExp('(^[\d]*$)');
  var reward = req.form.reward;
  if (reward) {
    result = hooksHelper(
      'app.customer.loyalty.rewards.info',
      'checkLoyaltyRewards',
      reward,
      require('*/cartridge/scripts/loyalty/loyaltyRewardsInfoUtil').checkLoyaltyRewards
    );
    if (result !== null && result.errorMessage) {
      success = false;
      req.session.raw.custom.counter = 'counter' in req.session.raw.custom ? req.session.raw.custom.counter + 1 : 1; // eslint-disable-line
    } else if (result !== null && !!result.ResponseCode && Number(result.ResponseCode) !== 0) {
      success = false;
      req.session.raw.custom.counter = 'counter' in req.session.raw.custom ? req.session.raw.custom.counter + 1 : 1; // eslint-disable-line
    } else {
      req.session.raw.custom.counter = 0; // eslint-disable-line
      if (!req.form.check) {
        var Transaction = require('dw/system/Transaction');
        let BasketMgr = require('dw/order/BasketMgr');
        var basket = BasketMgr.getCurrentBasket();
        Transaction.wrap(function () {
          basket.custom.hudsonRewardNumber = reward;
        });
      }
    }
  } else {
    success = false;
  }
  var hudsonRewardNumber;
  if (reward) {
    hudsonRewardNumber = preferences.hudsonReward + reward.toString().replace(/^([\d]{3})([\d]{3})/, ' $1 $2 ');
  }
  res.json({
    success: success,
    counter: 'counter' in req.session.raw.custom ? req.session.raw.custom.counter : 0,
    hudsonRewardNumber: hudsonRewardNumber
  });
  return next();
});

server.get('RemoveHudsonReward', server.middleware.https, function (req, res, next) {
  var Transaction = require('dw/system/Transaction');
  let BasketMgr = require('dw/order/BasketMgr');
  var basket = BasketMgr.getCurrentBasket();
  let success = false;
  try {
    Transaction.wrap(function () {
      delete basket.custom.hudsonRewardNumber;
    });
    success = true;
  } catch (e) {
    var Logger = require('dw/system/Logger');
    Logger.error('Hudson reward number removal error: {0} \n {1}', e.message, e.stack);
    success = false;
  }
  res.json({
    success: success
  });
  next();
});

server.get('GetTaxJurisdiction', function (req, res, next) {
  var system = require('dw/system/System');
  if (system.getInstanceType() !== system.PRODUCTION_SYSTEM) {
    var zipcode = req.querystring.zip;
    var state = req.querystring.state;
    var ShippingLocation = require('dw/order/ShippingLocation');
    var TaxMgr = require('dw/order/TaxMgr');
    var results = {};
    results.success = true;
    // validate only US states
    var shippingLocation = new ShippingLocation();
    shippingLocation.setStateCode(state);
    shippingLocation.setPostalCode(zipcode);
    if (!TaxMgr.getTaxJurisdictionID(shippingLocation)) {
      results.error = 'No Zip and State Mapping found';
      results.success = false;
    }
    res.json({ result: results });
  }
  next();
});

module.exports = server.exports();
