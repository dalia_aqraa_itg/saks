'use strict';

var server = require('server');
server.extend(module.superModule);

var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');

//* EXTENTED : content asset id change*//

server.replace('Check', consentTracking.consent, function (req, res, next) {
  var ContentMgr = require('dw/content/ContentMgr');
  var content = ContentMgr.getContent('tracking_cookie_hint');
  res.render('/common/consent', {
    consentApi: Object.prototype.hasOwnProperty.call(req.session.raw, 'setTrackingAllowed'),
    caOnline: content.online
  });
  next();
});

module.exports = server.exports();
