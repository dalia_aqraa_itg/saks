'use strict';

var server = require('server');
server.extend(module.superModule);

var cache = require('*/cartridge/scripts/middleware/cache');

var Resource = require('dw/web/Resource');

server.replace('GetSuggestions', cache.applyDefaultCache, function (req, res, next) {
  var SuggestModel = require('dw/suggest/SuggestModel');
  var CategorySuggestions = require('*/cartridge/models/search/suggestions/category');
  var ProductSuggestions = require('*/cartridge/models/search/suggestions/product');
  var preferences = require('*/cartridge/config/preferences');
  var categorySuggestions;
  var productSuggestions;
  var searchTerms = req.querystring.q;
  var suggestions;
  // TODO: Move minChars and maxSuggestions to Site Preferences when ready for refactoring
  var minChars = 3;
  // Unfortunately, by default, max suggestions is set to 10 and is not configurable in Business
  // Manager.
  var maxSuggestions = 4;

  if (searchTerms && searchTerms.length >= minChars) {
    suggestions = new SuggestModel();
    suggestions.setSearchPhrase(searchTerms);
    suggestions.setMaxSuggestions(maxSuggestions);
    categorySuggestions = new CategorySuggestions(suggestions, maxSuggestions, searchTerms);
    if (!preferences.noProductSearchSuggestion) {
      productSuggestions = new ProductSuggestions(suggestions, maxSuggestions, searchTerms);
    }

    if ((!!productSuggestions && productSuggestions.available) || categorySuggestions.available) {
      let total = categorySuggestions.categories.length;
      if (!!productSuggestions && productSuggestions.available) {
        total += productSuggestions.products.length;
      }
      res.render('search/suggestions', {
        suggestions: {
          product: productSuggestions,
          category: categorySuggestions,
          message: Resource.msgf('label.header.search.result.count.msg', 'common', null, ['' + total])
        }
      });
    } else {
      res.json({});
    }
  } else {
    // Return an empty object that can be checked on the client.  By default, rendered
    // templates automatically get a diagnostic string injected into it, making it difficult
    // to check for a null or empty response on the client.
    res.json({});
  }

  next();
});

module.exports = server.exports();
