/* eslint-disable consistent-return */
'use strict';

var ShippingMgr = require('dw/order/ShippingMgr');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var Resource = require('dw/web/Resource');

var formatCurrency = require('*/cartridge/scripts/util/formatting').formatCurrency;

/**
 * Returns shippingCost property for a specific Shipment / ShippingMethod pair
 * @param {dw.order.ShippingMethod} shippingMethod - the default shipment of the current basket
 * @param {dw.order.Shipment} shipment - a shipment of the current basket
 * @returns {string} String representation of Shipping Cost
 */
function getShippingCost(shippingMethod, shipment, shippingCostsMap) {
  if (!empty(shippingCostsMap) && !empty(shippingCostsMap.get(shippingMethod.ID))) {
    var shipCost = shippingCostsMap.get(shippingMethod.ID);
    return formatCurrency(shipCost.value, shipCost.currencyCode);
  } else {
    var shipmentShippingModel = ShippingMgr.getShipmentShippingModel(shipment);
    var shippingCost = shipmentShippingModel.getShippingCost(shippingMethod);
    return formatCurrency(shippingCost.amount.value, shippingCost.amount.currencyCode);
  }
}

/**
 * Returns isSelected property for a specific Shipment / ShippingMethod pair
 * @param {dw.order.ShippingMethod} shippingMethod - the default shipment of the current basket
 * @param {dw.order.Shipment} shipment - a shipment of the current basket
 * @returns {boolean} true is shippingMethod is selected in Shipment
 */
function getIsSelected(shippingMethod, shipment) {
  var selectedShippingMethod = shipment.shippingMethod || ShippingMgr.getDefaultShippingMethod();
  var selectedShippingMethodID = selectedShippingMethod ? selectedShippingMethod.ID : null;

  return selectedShippingMethodID && shippingMethod.ID === selectedShippingMethodID;
}

/**
 * get EDD date
 *
 * @param {dw.order.ShippingMethod} shippingMethod - the default shipment of the current basket
 * @param {Object} EDD - EDD object
 * @returns {Object} - return EDD date formatted
 */
function getEDDdate(shippingMethod, EDD) {
  if (shippingMethod) {
    if (EDD && EDD.containsKey(shippingMethod.ID)) {
      if (EDD.get(shippingMethod.ID)) {
        var eddDate = EDD.get(shippingMethod.ID).split('-');
        var dareStr = '' + eddDate[1] + '/' + eddDate[2] + '/' + eddDate[0];
        var date = new Date(dareStr);
        var formattedDate = Resource.msg('label.estimated.delivery', 'checkout', null) + ' ' + StringUtils.formatCalendar(new Calendar(date), 'MMM dd, yyyy');
        return formattedDate;
      }
    } else {
      return shippingMethod && shippingMethod.custom
        ? Resource.msg('label.estimated.delivery', 'checkout', null) + ' ' + shippingMethod.custom.estimatedArrivalTime
        : null;
    }
  }
}

/**
 * Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * @param {dw.order.ShippingMethod} shippingMethod - the default shipment of the current basket
 * @param {dw.order.Shipment} [shipment] - a Shipment
 * @param {Object} EDD - edd object
 */
function ShippingMethodModel(shippingMethod, shipment, EDD, shippingCostsMap) {
  this.ID = shippingMethod ? shippingMethod.ID : null;
  this.displayName = shippingMethod ? shippingMethod.displayName : null;
  this.description = shippingMethod ? shippingMethod.description : null;
  this.estimatedArrivalTime =
    'selectedEstimatedDate' in shipment.custom && !empty(shipment.custom.selectedEstimatedDate)
      ? Resource.msg('label.estimated.delivery', 'checkout', null) + ' ' + shipment.custom.selectedEstimatedDate
      : getEDDdate(shippingMethod, EDD);
  this.default = shippingMethod ? shippingMethod.defaultMethod : null;

  // Mix in dynamically transformed properties
  if (shipment) {
    // Optional model information available with 'shipment' parameter
    this.shippingCost = getShippingCost(shippingMethod, shipment, shippingCostsMap);
    this.selected = getIsSelected(shippingMethod, shipment);
    if (this.selected) {
      this.finalShippingCost =
        shipment.adjustedShippingTotalPrice.value > 0
          ? formatCurrency(shipment.adjustedShippingTotalPrice.value, shipment.adjustedShippingTotalPrice.currencyCode)
          : Resource.msg('info.cart.free.shipping', 'cart', null);
      this.unFormatFinalShippingCost =
        shipment.adjustedShippingTotalPrice.value > 0 ? shipment.adjustedShippingTotalPrice.value : Resource.msg('info.cart.free.shipping', 'cart', null);
    }
  }

  // Instore Pickup code
  this.storePickupEnabled = shippingMethod.custom && shippingMethod.custom.storePickupEnabled ? shippingMethod.custom.storePickupEnabled : false;
}

module.exports = ShippingMethodModel;
