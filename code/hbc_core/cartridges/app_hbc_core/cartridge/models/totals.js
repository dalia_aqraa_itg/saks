/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
'use strict';

var base = module.superModule;
var formatMoney = require('dw/util/StringUtils').formatMoney;
var collections = require('*/cartridge/scripts/util/collections');
var Money = require('dw/value/Money');
var priceHelper = require('*/cartridge/scripts/helpers/pricing');
var URLUtils = require('dw/web/URLUtils');
var HashMap = require('dw/util/HashMap');
var Template = require('dw/util/Template');
var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
var preferences = require('*/cartridge/config/preferences');
var Resource = require('dw/web/Resource');
var preorderHelper = require('*/cartridge/scripts/helpers/preorderHelper');

/**
 * Accepts a total object and formats the value
 * @param {dw.value.Money} total - Total price of the cart
 * @returns {string} the formatted money value
 */
function getTotals(total) {
  return !total.available ? '-' : formatMoney(total);
}

/**
 * Get list price for a product
 *
 * @param {dw.catalog.ProductPriceModel} priceModel - Product price model
 * @return {dw.value.Money} - List price
 */
function getListPrice(priceModel) {
  var price = Money.NOT_AVAILABLE;
  var priceBook;
  var priceBookPrice;

  if (priceModel.price.valueOrNull === null && priceModel.minPrice) {
    return priceModel.minPrice;
  }

  priceBook = priceHelper.getRootPriceBook(priceModel.priceInfo.priceBook);
  priceBookPrice = priceModel.getPriceBookPrice(priceBook.ID);

  if (priceBookPrice.available) {
    return priceBookPrice;
  }

  price = priceModel.price.available ? priceModel.price : priceModel.minPrice;

  return price;
}

/**
 * Gets the order discount and the lineItem level discount.
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket
 * @returns {Object} an object that contains the value and formatted value of the order discount
 */
function getOrderLevelDiscountTotal(lineItemContainer) {
  var totalExcludingOrderDiscount = lineItemContainer.getAdjustedMerchandizeTotalPrice(false);
  var totalIncludingOrderDiscount = lineItemContainer.getAdjustedMerchandizeTotalPrice(true);
  var orderDiscount = totalExcludingOrderDiscount.subtract(totalIncludingOrderDiscount);
  // exclude giftoptions custom adjustment
  var giftOptionsPA = lineItemContainer.getPriceAdjustmentByPromotionID('GiftOptions');
  if (giftOptionsPA && giftOptionsPA.price.available) {
    orderDiscount = orderDiscount.add(giftOptionsPA.price);
  }
  // Get the product lineitem level priceadjustments
  collections.forEach(lineItemContainer.productLineItems, function (li) {
    collections.forEach(li.priceAdjustments, function (priceAdjustment) {
      orderDiscount = orderDiscount.add(new Money(Math.abs(priceAdjustment.getPrice().value), lineItemContainer.getCurrencyCode()));
    });
  });
  return {
    value: orderDiscount.value,
    formatted: formatMoney(orderDiscount)
  };
}

/**
 * Gets the order discount only.
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket
 * @returns {Object} an object that contains the value and formatted value of the order discount
 */
function getOrderLevelDiscTotalExc(lineItemContainer) {
  var totalExcludingOrderDiscount = lineItemContainer.getAdjustedMerchandizeTotalPrice(false);
  var totalIncludingOrderDiscount = lineItemContainer.getAdjustedMerchandizeTotalPrice(true);
  var orderDiscount = totalExcludingOrderDiscount.subtract(totalIncludingOrderDiscount);
  // exclude giftoptions custom adjustment
  var giftOptionsPA = lineItemContainer.getPriceAdjustmentByPromotionID('GiftOptions');
  if (giftOptionsPA && giftOptionsPA.price.available) {
    orderDiscount = orderDiscount.add(giftOptionsPA.price);
  }
  return {
    value: orderDiscount.value,
    formatted: formatMoney(orderDiscount)
  };
}

/**
 * Gets the total savings, this is to include the promotional pricebook discount.
 * @param {Object} orderAndProductPromoDiscount - order and product promo discount object
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket
 * @returns {Object} an object that contains the value and formatted value of the order discount
 */
function getTotalSavings(orderAndProductPromoDiscount, lineItemContainer) {
  var nonNegativeValue = Math.abs(orderAndProductPromoDiscount.value);
  var orderAndProductPromoDisc = new Money(nonNegativeValue, lineItemContainer.getCurrencyCode()); // The discount applied would return negative value
  // Get the total savings on the order, this includes the promotional pricebook discount product and order level discount.
  collections.forEach(lineItemContainer.productLineItems, function (li) {
    var product = li.product;
    var priceModel = product ? product.getPriceModel() : null;
    if (priceModel) {
      var listPrice = new Money(getListPrice(priceModel) * li.quantity.value, lineItemContainer.getCurrencyCode());
      var pricebookDiscount = listPrice.subtract(li.getPrice());
      orderAndProductPromoDisc = orderAndProductPromoDisc.add(pricebookDiscount);
    }
  });
  return {
    value: orderAndProductPromoDisc.value,
    formatted: formatMoney(orderAndProductPromoDisc)
  };
}

/**
 * Adds discounts to a discounts object
 * @param {dw.util.Collection} collection - a collection of price adjustments
 * @param {Object} discounts - an object of price adjustments
 * @returns {Object} an object of price adjustments
 */
function createDiscountObject(collection, discounts) {
  var result = discounts;
  collections.forEach(collection, function (item) {
    if (!item.basedOnCoupon && item.promotionID !== 'GiftOptions') {
      result[item.UUID] = {
        UUID: item.UUID,
        lineItemText: item.lineItemText,
        price: formatMoney(item.price),
        type: 'promotion',
        callOutMsg: item.promotion && item.promotion.calloutMsg ? item.promotion.calloutMsg.markup : '',
        custom: {
          promotionType:
            item.promotion && item.promotion.custom && item.promotion.custom.promotionType && item.promotion.custom.promotionType.value
              ? item.promotion.custom.promotionType.value
              : ''
        }
      };
    }
  });

  return result;
}

/**
 * creates an array of discounts.
 * @param {dw.order.LineItemCtnr} lineItemContainer - the current line item container
 * @returns {Array} an array of objects containing promotion and coupon information
 */
function getDiscounts(lineItemContainer) {
  var Resource = require('dw/web/Resource');
  var PromotionHelper = require('*/cartridge/scripts/util/promotionHelper');
  var discounts = {};
  collections.forEach(lineItemContainer.couponLineItems, function (couponLineItem) {
    var cpnMessage = '';
    var cpnPromo = PromotionHelper.getCouponLineItemPromotion(couponLineItem);
    var cpnPriceAdjs = couponLineItem.priceAdjustments;
    // Looking up Promotion through Price Adjustment to solve the issue SFDEV-6865 where same coupon is attached to multiple promotions
    collections.forEach(couponLineItem.priceAdjustments, function (priceAdjustment) {
      var couponPromo = priceAdjustment.promotion;
      // If the promotion is dependent on Tender Type and promo code is applied, we need to show the discout description message in the coupon code section
      // If the promotions is dependent on Tender Type and promo code is NOT applied yet, we need to show the message from plccPromoMessage custom attribute
      // If the promotion is NOT dependent on Tender Type  and promo code is applied, NO message will be shown in the coupon code section
      if (!empty(couponPromo)) {
        if (couponPromo.custom.isTenderTypeDependent) {
          if (couponLineItem.applied) {
            cpnMessage = !empty(couponPromo.custom.discountDescription) ? couponPromo.custom.discountDescription : '';
          } else {
            cpnMessage = !empty(couponPromo.custom.plccPromoMessage) ? couponPromo.custom.plccPromoMessage : '';
          }
        } else if (!couponPromo.custom.isTenderTypeDependent && !couponLineItem.applied) {
          // SFDEV-6948 | Coupon is simply Not Applied
          cpnMessage = Resource.msg('error.notapplied.coupon', 'cart', null);
        }
      }
    });

    // As tender dependent dummy promotion will not have price adjustment, we need to call this method below to set the PLCC message SFDEV-7919
    if (!empty(cpnPromo) && cpnPromo.active && cpnPriceAdjs.length === 0) {
      if (cpnPromo.custom.isTenderTypeDependent) {
        if (couponLineItem.applied) {
          cpnMessage = !empty(cpnPromo.custom.discountDescription) ? cpnPromo.custom.discountDescription : '';
        } else {
          cpnMessage = !empty(cpnPromo.custom.plccPromoMessage) ? cpnPromo.custom.plccPromoMessage : '';
        }
      } else if (!cpnPromo.custom.isTenderTypeDependent && !couponLineItem.applied) {
        // SFDEV-6948 | Coupon is simply Not Applied
        cpnMessage = Resource.msg('error.notapplied.coupon', 'cart', null);
      }
    }

    discounts[couponLineItem.UUID] = {
      type: 'coupon',
      UUID: couponLineItem.UUID,
      couponCode: couponLineItem.couponCode,
      applied: couponLineItem.applied,
      valid: couponLineItem.valid,
      cpnMessage: cpnMessage,
      removeCouponURL: URLUtils.url('Cart-RemoveCouponLineItem').toString()
    };
  });

  discounts = createDiscountObject(lineItemContainer.priceAdjustments, discounts);
  discounts = createDiscountObject(lineItemContainer.allShippingPriceAdjustments, discounts);

  return Object.keys(discounts).map(function (key) {
    return discounts[key];
  });
}

/**
 * create the discount results html
 * @param {Array} discounts - an array of objects that contains coupon and priceAdjustment
 * information
 * @returns {string} The rendered HTML
 */
function getDiscountsHtml(discounts) {
  var context = new HashMap();
  var object = { totals: { discounts: discounts } };

  Object.keys(object).forEach(function (key) {
    context.put(key, object[key]);
  });

  var template = new Template('cart/cartCouponDisplay');
  return template.render(context).text;
}

/**
 * Calculate the Mini-cart estimated subtotal
 * @param {Object} subTotalAmt - subTotalAmt
 * @param {Object} discountTotalAmt - discountTotalAmt
 * @param {dw.order.LineItemCtnr} lineItemContainer - the current line item container
 * @returns {Object} The estimated subtotal for Mini-cart
 */

function getMiniCartEstimatedTotal(subTotalAmt, discountTotalAmt, lineItemContainer) {
  if (discountTotalAmt.value > 0) {
    var miniCartEstimatedTotal = subTotalAmt.value - discountTotalAmt.value;
    return formatMoney(new Money(miniCartEstimatedTotal, lineItemContainer.getCurrencyCode()));
  } else {
    return getTotals(subTotalAmt);
  }
}

/**
 * Returns the message for Associate or First Day Discount
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket
 * @returns {string} The Message for the Associate or First Day Discount
 */
function getOrderPromotionMsg(lineItemContainer) {
  var Resource = require('dw/web/Resource');
  var orderPromoMsg = '';
  if (!empty(lineItemContainer) && !empty(lineItemContainer.priceAdjustments) && lineItemContainer.priceAdjustments.length > 0) {
    collections.forEach(lineItemContainer.priceAdjustments, function (priceAdj) {
      var promo = priceAdj.promotion;
      if (!empty(promo) && !empty(promo.custom.promotionType)) {
        if (promo.custom.promotionType.value === 'Associate' || promo.custom.promotionType.value === 'SpecialVendorDisc') {
          orderPromoMsg = Resource.msg('info.associate.discount.msg', 'cart', null);
        } else if (promo.custom.promotionType.value === 'FDD') {
          orderPromoMsg = Resource.msg('info.fdd.discount.msg', 'cart', null);
        }
      }
    });
  }
  return orderPromoMsg;
}

/**
 * Returns a message stating SaksFirst members receive free shipping
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket
 * @returns {string} A message for SaksFirst members if the card type is SAKS or SAKSMC
 */
function getOrderShippingPromotionMsg(lineItemContainer) {
  var orderPromoMsg = '';
  var paymentInstruments = lineItemContainer.getPaymentInstruments();
  collections.forEach(paymentInstruments, function (item) {
    if (item.creditCardType === 'SAKS' || item.creditCardType === 'SAKSMC') {
      orderPromoMsg = Resource.msg('info.cart.free.shipping.msg', 'cart', null);
    }
  });
  return orderPromoMsg;
}

/**
 * Returns the Canada specific taxes based on the State
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket
 * @returns {array} Array contains all possible tax for current basket.
 */
function getCanadaTaxation(lineItemContainer) {
  if (
    preferences.vertaxCanadaTaxationEnabled &&
    'vertex_taxation_imposition' in lineItemContainer.custom &&
    lineItemContainer.custom.vertex_taxation_imposition
  ) {
    var parseCanadaTax = JSON.parse(lineItemContainer.custom.vertex_taxation_imposition);
    if (parseCanadaTax.length > 0) {
      var canadaTaxObj = {};
      let QSTPSTRST = new Money(0, lineItemContainer.getCurrencyCode());
      let GSTHST = new Money(0, lineItemContainer.getCurrencyCode());
      let RST = new Money(0, lineItemContainer.getCurrencyCode());
      let ecoFeeChargeTotal = new Money(0, lineItemContainer.getCurrencyCode());
      for (var i = 0; i < parseCanadaTax.length; i++) {
        var key = Object.keys(parseCanadaTax[i])[0];
        var taxAmount = new Money(Number(parseCanadaTax[i][key]), lineItemContainer.getCurrencyCode());
        if (key.indexOf('GST') > -1 || key.indexOf('HST') > -1) {
          canadaTaxObj['GST/HST'] = formatMoney(taxAmount);
          GSTHST = GSTHST.add(taxAmount);
        } else if (key.indexOf('PST') > -1) {
          canadaTaxObj.PST = formatMoney(taxAmount);
          QSTPSTRST = QSTPSTRST.add(taxAmount);
        } else if (key.indexOf('QST') > -1 || key.indexOf('Quebec') > -1 || key.indexOf('VAT') > -1) {
          QSTPSTRST = QSTPSTRST.add(taxAmount);
          canadaTaxObj.QST = formatMoney(taxAmount);
        } else if (key.indexOf('ECO') > -1) {
          canadaTaxObj.ECO = formatMoney(taxAmount);
          ecoFeeChargeTotal = ecoFeeChargeTotal.add(taxAmount);
        } else if (key.indexOf('RST') > -1) {
          QSTPSTRST = QSTPSTRST.add(taxAmount);
          canadaTaxObj.RST = formatMoney(taxAmount);
        }
      }
      canadaTaxObj.GSTHST = GSTHST.toFormattedString();
      canadaTaxObj.QSTPSTRST = QSTPSTRST.toFormattedString();
      canadaTaxObj.ecoFee = ecoFeeChargeTotal;
      return canadaTaxObj;
    }
  }
  return null;
}

function preOrder(LineItemCtnr) {
  var allLineItems = LineItemCtnr.getProductLineItems();
  var obj = {};
  obj.hasOnePreOrderItem = false;
  obj.completePreOrder = true;
  obj.preOrderTotal = 0;
  obj.allItemsTotal = 0;
  obj.instockTotal = 0;
  obj.instockTotalWithTax = 0;
  obj.instockOrderTotal = 0;
  collections.forEach(allLineItems, function (item) {
    obj.allItemsTotal += item.getAdjustedNetPrice().value;
    if (preorderHelper.isPreorder(item.product)) {
      obj.hasOnePreOrderItem = true;
      obj.preOrderTotal += item.getAdjustedNetPrice().value;
    } else {
      obj.completePreOrder = false;
      obj.instockTotal += item.getAdjustedNetPrice().value;
      var totalLineItemPrice = item.getAdjustedGrossPrice().value; // includues tax as well
      obj.instockTotalWithTax += totalLineItemPrice;
    }
  });

  if (obj && obj.hasOnePreOrderItem) {
    var lineProrationRate = obj.instockTotal / obj.allItemsTotal;

    /**
     * Returns the sum of all shipping line items of the line item container, including tax
     * before shipping adjustments have been applied.
     */
    var shippingPrice = LineItemCtnr.getAdjustedShippingTotalGrossPrice().value;
    var shippingProPrice = shippingPrice * lineProrationRate;
    var instockOrderTotal = obj.instockTotalWithTax + shippingProPrice;
    obj.instockOrderTotal = instockOrderTotal.toFixed(2);
    obj.RequestAmount = obj.instockOrderTotal;
    obj.ExtnTenderMaxLimit = LineItemCtnr.getMerchandizeTotalNetPrice().value;
  }

  return obj;
}

/**
 * @constructor
 * @classdesc totals class that represents the order totals of the current line item container
 *
 * @param {dw.order.lineItemContainer} lineItemContainer - The current user's line item container
 */
function totals(lineItemContainer) {
  base.call(this, lineItemContainer);
  this.subTotal = getTotals(lineItemContainer.getMerchandizeTotalNetPrice());
  this.subTotalUnformatted = lineItemContainer.getMerchandizeTotalNetPrice();
  this.subTotalAdjusted = lineItemContainer.getAdjustedMerchandizeTotalNetPrice();
  this.orderLevelDiscountTotal = getOrderLevelDiscountTotal(lineItemContainer);
  this.orderLevelDiscTotalExc = getOrderLevelDiscTotalExc(lineItemContainer);
  this.totalSavings = getTotalSavings(this.orderLevelDiscountTotal, lineItemContainer);
  this.grandTotalValue = lineItemContainer.totalGrossPrice.value;
  this.currencyCode = lineItemContainer.totalGrossPrice.currencyCode;
  this.discounts = getDiscounts(lineItemContainer);
  this.discountsHtml = getDiscountsHtml(this.discounts);
  this.associateOrFDDMsg = getOrderPromotionMsg(lineItemContainer);
  this.hudsonpoint = productHelper.getHudsonPoints(lineItemContainer, true, null, true);
  this.canadaTaxation = getCanadaTaxation(lineItemContainer);
  this.totalTaxUnformatted = lineItemContainer.totalTax;
  this.totalShippingCostUnformatted = lineItemContainer.shippingTotalPrice.value;
  this.totalShippingAdjusted = lineItemContainer.adjustedShippingTotalPrice;
  this.freeShippingText = Resource.msg('info.cart.free.shipping', 'cart', null);
  this.freeShippingMsg = getOrderShippingPromotionMsg(lineItemContainer);
  this.miniCartEstimatedTotal = getMiniCartEstimatedTotal(this.subTotalUnformatted, this.orderLevelDiscountTotal, lineItemContainer);
  this.preorder = preOrder(lineItemContainer);
}

module.exports = totals;
