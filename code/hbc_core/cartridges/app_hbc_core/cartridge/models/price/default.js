'use strict';

var formatMoney = require('dw/util/StringUtils').formatMoney;
var priceHelper = require('*/cartridge/scripts/helpers/pricing');
var preferences = require('*/cartridge/config/preferences');

/**
 * To remove all zeros followed by decimal, Ignore lint for global object
 * @param {Object} formattedPrice Unformatted price
 * @returns {Object} formattedPrice formatted price
 */
function getFormattedAmount(formattedPrice) {
  var formatAmount = formattedPrice;
  if (request.session.currency.defaultFractionDigits && formattedPrice) {
    // eslint-disable-line
    var decimalValue = formattedPrice.substr(formattedPrice.length - (request.session.currency.defaultFractionDigits - 1) - 1, formattedPrice.length); // eslint-disable-line
    if (parseInt(decimalValue) === 0) {
      // eslint-disable-line
      formatAmount = formattedPrice.substr(0, formattedPrice.length - request.session.currency.defaultFractionDigits - 1); // eslint-disable-line
    }
  }
  return formatAmount;
}

/**
 * Convert API price to an object
 * @param {dw.value.Money} price - Price object returned from the API
 * @returns {Object} price formatted as a simple object
 */
function toPriceModel(price) {
  var value = price.available ? price.getDecimalValue().get() : null;
  var currency = price.available ? price.getCurrencyCode() : null;
  var formattedPrice = price.available ? formatMoney(price) : null;
  var decimalPrice;

  if (formattedPrice) {
    decimalPrice = price.getDecimalValue().toString();
  }

  var priceBandFormatted = '';
  if (preferences.price.PRICE_BANDS_FOR_BRIDAL_PRODUCT) {
    var priceBandsForBridalPDP = preferences.price.PRICE_BANDS_FOR_BRIDAL_PRODUCT;
    for (var i = 0; i < priceBandsForBridalPDP.length; i++) {
      var priceBandsItem = priceBandsForBridalPDP[i];
      var priceBandRangeArray = priceBandsItem.split('-');
      if (value >= Number(priceBandRangeArray[0]) && value <= Number(priceBandRangeArray[1])) {
        priceBandFormatted = priceBandRangeArray[2];
        break;
      }
    }
  }

  return {
    value: value,
    currency: currency,
    formatted: formattedPrice,
    decimalPrice: decimalPrice,
    formatAmount: priceHelper.formatMoney(formattedPrice),
    priceBandFormatted: priceBandFormatted
  };
}

/**
 * @constructor
 * @classdesc Default price class
 * @param {dw.value.Money} salesPrice - Sales price
 * @param {dw.value.Money} listPrice - List price
 */
function DefaultPrice(salesPrice, listPrice) {
  this.sales = toPriceModel(salesPrice);
  this.list = listPrice ? toPriceModel(listPrice) : null;
  var savings = priceHelper.calculatePercentOff(salesPrice, listPrice);
  if (savings.savings && savings.savePercentage) {
    this.savings = savings.savings;
    this.savePercentage = savings.savePercentage;
  }
}

module.exports = DefaultPrice;
