'use strict';

var base = module.superModule;
var priceHelper = require('*/cartridge/scripts/helpers/pricing');

/**
 * @constructor
 * @classdesc Range price class
 * @param {dw.value.Money} min - Range minimum price
 * @param {dw.value.Money} max - Range maximum price
 */
function RangePrice(min, max) {
  base.call(this, min, max);
  var savings = priceHelper.calculatePercentOff(min, max);
  if (savings.savings && savings.savePercentage) {
    this.savings = savings.savings;
    this.savePercentage = savings.savePercentage;
  }
}

module.exports = RangePrice;
