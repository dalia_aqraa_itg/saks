'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var URLUtils = require('dw/web/URLUtils');

/**
 * Get category url
 * @param {dw.catalog.Category} category - Current category
 * @returns {string} - Url of the category
 */
function getCategoryUrl(category) {
  if (Object.hasOwnProperty.call(category.custom, 'topNavalturl') && category.custom.topNavalturl) {
    return category.custom.topNavalturl.markup;
  } else if (Object.hasOwnProperty.call(category.custom, 'alternativeUrl') && category.custom.alternativeUrl) {
    return category.custom.alternativeUrl.markup;
  }
  return URLUtils.url('Search-Show', 'cgid', category.getID()).toString();
}

/**
 * Identifies the category level
 * @param {dw.catalog.Category} category - A single category
 * @param {number} level - Level of the category at the initial stage
 * @returns {number} level - Level of the category
 */
function getCategoryLevel(category, level) {
  if (!category.root) {
    return getCategoryLevel(category.parent, level + 1);
  }
  return level;
}

/**
 * Converts a given category from dw.catalog.Category to plain object
 * @param {dw.catalog.Category} category - A single category
 * @returns {Object} plain object that represents a category
 */
function categoryToObject(category) {
  if (!category.custom || !category.custom.showInMenu) {
    return null;
  }
  var result = {
    name:
      'categoryNameoverwrite' in category.custom && !!category.custom.categoryNameoverwrite ? category.custom.categoryNameoverwrite : category.getDisplayName(),
    url: getCategoryUrl(category),
    id: category.ID,
    level: getCategoryLevel(category, 0),
    isClickable: 'isClickable' in category.custom && category.custom.isClickable ? category.custom.isClickable : false,
    HideLeftNav: 'HideLeftNav' in category.custom && category.custom.HideLeftNav ? category.custom.HideLeftNav : false,
    OnlineWithoutProduct: 'OnlineWithoutProduct' in category.custom && category.custom.OnlineWithoutProduct ? category.custom.OnlineWithoutProduct : false,
    hexColor: 'hexColor' in category.custom && category.custom.hexColor ? category.custom.hexColor : '',
    imageIconnexttocategory:
      'imageIconnexttocategory' in category.custom && category.custom.imageIconnexttocategory ? category.custom.imageIconnexttocategory : null,
    iconCategorytitlereplacement:
      'iconCategorytitlereplacement' in category.custom && category.custom.iconCategorytitlereplacement ? category.custom.iconCategorytitlereplacement : null,
    columnNumber: 'columnNumber' in category.custom && category.custom.columnNumber ? category.custom.columnNumber : null,
    contentInnav1: 'contentInnav1' in category.custom && category.custom.contentInnav1 ? category.custom.contentInnav1 : null,
    contentInnav2: 'contentInnav2' in category.custom && category.custom.contentInnav2 ? category.custom.contentInnav2 : null,
    contentTemplatesize1: 'contentTemplatesize1' in category.custom && category.custom.contentTemplatesize1 ? category.custom.contentTemplatesize1 : null,
    contentTemplatesize2: 'contentTemplatesize2' in category.custom && category.custom.contentTemplatesize2 ? category.custom.contentTemplatesize2 : null,
    hideFromdesktop: 'hideFromdesktop' in category.custom && category.custom.hideFromdesktop ? category.custom.hideFromdesktop : false,
    hideFromMobile: 'hideFromMobile' in category.custom && category.custom.hideFromMobile ? category.custom.hideFromMobile : false
  };
  var subCategories = category.hasOnlineSubCategories() // eslint-disable-line
    ? category.getOnlineSubCategories()
    : result.OnlineWithoutProduct
    ? category.getOnlineSubCategories()
    : null;

  if (subCategories) {
    collections.forEach(subCategories, function (subcategory) {
      var converted = null;
      var onlineWithoutProduct =
        'OnlineWithoutProduct' in subcategory.custom && subcategory.custom.OnlineWithoutProduct ? subcategory.custom.OnlineWithoutProduct : false;
      if (subcategory.hasOnlineProducts() || subcategory.hasOnlineSubCategories() || onlineWithoutProduct) {
        converted = categoryToObject(subcategory);
      }
      if (converted) {
        if (!result.subCategories) {
          result.subCategories = [];
        }
        result.subCategories.push(converted);
      }
    });
    if (result.subCategories) {
      result.subCategories.sort(function (a, b) {
        return a.columnNumber - b.columnNumber;
      });
      result.complexSubCategories = result.subCategories.some(function (item) {
        return !!item.subCategories;
      });
    }
  }

  return result;
}

/**
 * Represents a single category with all of it's children
 * @param {dw.util.ArrayList<dw.catalog.Category>} items - Top level categories
 * @constructor
 */
function categories(items) {
  this.categories = [];
  collections.forEach(
    items,
    function (item) {
      var onlineWithoutProduct = 'OnlineWithoutProduct' in item.custom && item.custom.OnlineWithoutProduct ? item.custom.OnlineWithoutProduct : false;
      if (item.custom && item.custom.showInMenu && (item.hasOnlineProducts() || item.hasOnlineSubCategories() || onlineWithoutProduct)) {
        this.categories.push(categoryToObject(item));
      }
    },
    this
  );
}

module.exports = categories;
