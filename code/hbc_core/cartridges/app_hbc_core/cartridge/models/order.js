'use strict';

var base = module.superModule;
var URLUtils = require('dw/web/URLUtils');
var instorePickPerson = require('*/cartridge/models/order/decorators/instorePickPerson');
var collections = require('*/cartridge/scripts/util/collections');
var ipaConstants = require('*/cartridge/scripts/util/ipaConstants');
var preferences = require('*/cartridge/config/preferences');
var preorderHelper = require('*/cartridge/scripts/helpers/preorderHelper');
/**
 * Generates an object of URLs
 * @returns {Object} an object of URLs in string format
 */
function getActionUrls() {
  return {
    submitCouponCodeUrl: URLUtils.url('Cart-AddCoupon').toString(),
    removeCouponLineItem: URLUtils.url('Cart-RemoveCouponLineItem').toString(),
    checkBalance: URLUtils.url('GiftCard-CheckBalance').toString(),
    addGiftCard: URLUtils.url('GiftCard-AddGiftCard').toString(),
    removeGiftCard: URLUtils.url('GiftCard-RemoveGiftCard').toString()
  };
}

/**
 * Returns the matching address ID or UUID for a billing address
 * @param {dw.order.Basket} basket - line items model
 * @param {Object} customer - customer model
 * @return {string|boolean} returns matching ID or false
 */
function getAssociatedCustAddress(basket, customer) {
  var address = basket.billingAddress;
  var matchingCustaddrId;
  var anAddress;

  if (!address) return false;

  // We need to match the billing addr Id and the basket billing addr to select the same on page load
  if (customer && customer.addressBook && customer.addressBook.addresses) {
    for (var j = 0, jj = customer.addressBook.addresses.length; j < jj; j++) {
      anAddress = customer.addressBook.addresses[j];
      if (anAddress && anAddress.isEquivalentAddress(address)) {
        matchingCustaddrId = anAddress.ID;
        break;
      }
    }
  }

  return matchingCustaddrId;
}

/**
 * Get the count of coupons applied to a basket
 * @param {dw.order.Basket} basket - current basket
 * @returns {number} couponCount
 */
function getTotalAppliedCoupons(basket) {
  var couponCount = 0;
  if (!empty(basket)) {
    collections.forEach(basket.couponLineItems, function (couponLineItem) {
      if (couponLineItem.applied) {
        ++couponCount;
      }
    });
  }
  return couponCount;
}

/**
 * Get the coupon codes applied to a basket
 * @param {dw.order.Basket} basket - current basket
 * @returns {string} couponCodes
 */
function getTotalAppliedCouponsCode(basket) {
  var couponCodes = [];
  if (!empty(basket)) {
    collections.forEach(basket.couponLineItems, function (couponLineItem) {
      if (couponLineItem.applied) {
        couponCodes.push(couponLineItem.couponCode);
      }
    });
  }
  return couponCodes;
}

/**
 * Gets the applied GC data html for checkout
 *
 * @param {Object} giftCardData - applied gift card data
 * @returns {HTML} html data of GC to render
 */
function getGiftCardHtmlData(giftCardData) {
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var orderGiftCard = {
    order: {
      giftCard: {
        giftCardData: giftCardData
      },
      actionUrls: {
        removeGiftCard: URLUtils.url('GiftCard-RemoveGiftCard').toString()
      }
    }
  };
  var renderedTemplate = renderTemplateHelper.getRenderedHtml(orderGiftCard, 'checkout/billing/giftCardDisplay');
  return renderedTemplate;
}

/**
 * returns the applied GC object
 *
 * @param {dw.util.Collection} giftCardPaymentInstruments - applied GC payment instruments in current basket
 * @returns {Object} applied gc object
 */
function getGiftCardData(giftCardPaymentInstruments) {
  var arrayHelper = require('*/cartridge/scripts/util/array');
  var Resource = require('dw/web/Resource');
  var formatMoney = require('dw/util/StringUtils').formatMoney;

  var giftCardData = [];
  arrayHelper.find(giftCardPaymentInstruments, function (gcPaymentIntr) {
    giftCardData.push({
      type: Resource.msg('label.order.summary.gift.card', 'checkout', null),
      giftCardNumber: gcPaymentIntr.custom.giftCardNumber,
      giftCardNumberMasked: '*************' + gcPaymentIntr.custom.giftCardNumber.toString().substr(gcPaymentIntr.custom.giftCardNumber.toString.length - 4),
      giftCardAmount: formatMoney(gcPaymentIntr.getPaymentTransaction().getAmount())
    });
  });
  return giftCardData;
}

/**
 * return thr html for order summary payment applied
 *
 * @param {Object} giftCardData - giftcard data
 * @returns {html} return order summary payment html
 */
function getOrderSummaryPaymentHtml(giftCardData) {
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var context = {
    order: giftCardData,
    currency: session.currency.currencyCode
  };
  var renderedTemplate = renderTemplateHelper.getRenderedHtml(context, 'checkout/orderTotalPaymentMethodsApplied');
  return renderedTemplate;
}

/**
 * render gift card summary html for order summary section
 *
 * @param {Object} giftCardData - gift card data
 * @returns {html} return order summary gift card html
 */
function getGiftCardSummaryHtml(giftCardData) {
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var context = {
    giftCardData: giftCardData
  };
  var renderedTemplate = renderTemplateHelper.getRenderedHtml(context, 'checkout/billing/paymentOptions/giftCardOrderSummary');
  return renderedTemplate;
}

/**
 * Check if the basket contains GC product
 *
 * @param {Object} items - Basket items
 * @returns {boolean} - check if gift card is present in basket
 */
function checkIfGiftCardInBasket(items) {
  let giftCard = false;
  if (items && items.items) {
    items.items.forEach(function (item) {
      if (item.hbcProductType === 'giftcard') {
        giftCard = true;
      }
    });
  }
  return giftCard;
}

/**
 * Get Email optin
 * @returns
 */
function getEmailOptIn() {
  return 'email_opt_in' in session.privacy && session.privacy.email_opt_in ? session.privacy.email_opt_in : false;
}

/**
 * Get giftwrap details
 * @param {Object} lineItemContainer - Order
 * @returns {Object} return object with giftwrap details
 */
function getGiftWrapDetails(lineItemContainer) {
  var Money = require('dw/value/Money');
  var Resource = require('dw/web/Resource');
  var formatMoney = require('dw/util/StringUtils').formatMoney;
  var defaultShipment = lineItemContainer.defaultShipment;
  var giftWrapObj = {};
  var giftOptionAdjustment = null;
  giftWrapObj.applied = false;
  if (defaultShipment.custom.hasOwnProperty('giftWrapType') && defaultShipment.custom.giftWrapType === 'giftpack') {
    giftWrapObj.applied = true;
    giftOptionAdjustment = lineItemContainer.getPriceAdjustmentByPromotionID('GiftOptions');
    if (giftOptionAdjustment) {
      giftWrapObj.charge =
        preferences.giftWrapAmount && !isNaN(preferences.giftWrapAmount)
          ? formatMoney(new Money(preferences.giftWrapAmount, lineItemContainer.currencyCode))
          : null;
    } else {
      giftWrapObj.charge = Resource.msg('summary.giftoption.free', 'checkout', null);
    }
  } else if (defaultShipment.custom.hasOwnProperty('giftWrapType') && defaultShipment.custom.giftWrapType === 'giftnote') {
    giftWrapObj.applied = true;
    giftWrapObj.charge = Resource.msg('summary.giftoption.free', 'checkout', null);
  }
  return giftWrapObj;
}

/**
 * Order class that represents the current order
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket/order
 * @param {Object} options - The current order's line items
 * @param {Object} options.config - Object to help configure the orderModel
 * @param {string} options.config.numberOfLineItems - helps determine the number of lineitems needed
 * @param {string} options.countryCode - the current request country code
 * @constructor
 */
function OrderModel(lineItemContainer, options) {
  var Money = require('dw/value/Money');
  var Resource = require('dw/web/Resource');
  base.call(this, lineItemContainer, options);
  this.actionUrls = getActionUrls();
  this.usingMultiShipping = false;
  instorePickPerson(this, lineItemContainer); // add property that defines the details of pick up person for store pickup
  this.associatedCustAddress = getAssociatedCustAddress(lineItemContainer, customer);
  var totalAppliedCoupons = getTotalAppliedCoupons(lineItemContainer);
  this.totalAppliedCouponCodes = getTotalAppliedCouponsCode(lineItemContainer);
  this.totalAppliedCoupons = totalAppliedCoupons;
  this.noCouponMsg = Resource.msg('label.promo.code', 'cart', null);
  this.singleCouponMsg = Resource.msg('label.promo.code.applied', 'cart', null);
  this.multipleCouponMsg = Resource.msg('label.promo.codes.applied', 'cart', null);

  // gift card items
  var giftCardPaymentInstruments = lineItemContainer.getPaymentInstruments(ipaConstants.GIFT_CARD);
  var hasGiftCard = giftCardPaymentInstruments.length > 0;
  var giftCardData = hasGiftCard ? getGiftCardData(giftCardPaymentInstruments) : null;
  var gcTotal = new Money(0, lineItemContainer.currencyCode);
  collections.forEach(giftCardPaymentInstruments, function (gcPaymentInstr) {
    gcTotal = gcTotal.add(gcPaymentInstr.paymentTransaction.amount);
  });
  var allLineItems = lineItemContainer.getProductLineItems();
  var isPreorderOnly = true;
  collections.forEach(allLineItems, function (item) {
    if (!preorderHelper.isPreorder(item.product)) {
      isPreorderOnly = false;
    }
  });
  this.isPreorderOnly = isPreorderOnly;
  this.giftCard = {
    hasGiftCard: hasGiftCard,
    giftCardData: giftCardData,
    giftCardHtml: hasGiftCard ? getGiftCardHtmlData(giftCardData) : '',
    giftCardSummary: hasGiftCard ? getGiftCardSummaryHtml(giftCardData) : '',
    maxLimitReached: !!giftCardData && giftCardData.length === preferences.maxGCLimit,
    amountReached: !(gcTotal < lineItemContainer.totalGrossPrice)
  };
  this.paymentMethod = this.giftCard.amountReached && lineItemContainer.totalGrossPrice.value !== 0 ? ipaConstants.GIFT_CARD : '';
  this.orderSummaryPaymentHtml = getOrderSummaryPaymentHtml(this);
  this.giftCardLineItem = checkIfGiftCardInBasket(this.items);
  this.resources.gcPaymentMethodID = ipaConstants.GIFT_CARD;
  this.emailOptIn = getEmailOptIn();
  this.giftWrap = getGiftWrapDetails(lineItemContainer);
  this.smsOptIn = lineItemContainer && lineItemContainer.custom.hasOwnProperty('smsOptIn') ? lineItemContainer.custom.smsOptIn : null;
  this.isAgentUser = session.isUserAuthenticated();
}

module.exports = OrderModel;
