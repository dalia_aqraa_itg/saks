'use strict';

var productDecorators = require('*/cartridge/models/product/decorators/index');
var base = module.superModule;

/**
 * Decorate product with product line item information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {Object} options - Options passed in from the factory
 * @returns {Object} - Decorated product model
 */
function orderLineItem(product, apiProduct, options) {
  base.call(this, product, apiProduct, options);
  productDecorators.price(product, apiProduct, options.promotions, false, options.currentOptionModel);
  return product;
}

module.exports = orderLineItem;
