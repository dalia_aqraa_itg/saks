'use strict';

var base = module.superModule;
var productDecorators = require('*/cartridge/models/product/decorators/index');
var productLineItemDecorators = require('*/cartridge/models/productLineItem/decorators/index');
var isInstoreEligible = require('*/cartridge/models/productLineItem/decorators/isAvailableInStore');
var fromStoreName = require('*/cartridge/models/productLineItem/decorators/fromStoreName');
var quantityDetailed = require('*/cartridge/models/productLineItem/decorators/quantityDetailed');
var isAvailableForInstore = require('*/cartridge/models/product/decorators/availableForInStorePickup');
var isDropShipItem = require('*/cartridge/models/product/decorators/isDropShipItem');
var shipTypeText = require('*/cartridge/models/product/decorators/shipTypeText');
var uspsShip = require('*/cartridge/models/productLineItem/decorators/uspsShip');
var pdRestrictedStateText = require('*/cartridge/models/productLineItem/decorators/pdRestrictedStateText');
var isSignatureRequired = require('*/cartridge/models/productLineItem/decorators/isSignatureRequired');
var giftWrapEligible = require('*/cartridge/models/productLineItem/decorators/giftWrapEligible');
var overridedPriceMessage = require('*/cartridge/models/productLineItem/decorators/overridedPriceMessage');
var preferences = require('*/cartridge/config/preferences');

/**
 * Decorate product with product line item information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {Object} options - Options passed in from the factory
 * @returns {Object} - Decorated product model
 */
function productLineItem(product, apiProduct, options) {
  // creating property with writable true.
  Object.defineProperty(product, 'availability', {
    enumerable: true,
    value: null,
    writable: true
  });
  Object.defineProperty(product, 'available', {
    enumerable: true,
    value: null,
    writable: true
  });
  Object.defineProperty(product, 'orderableNotInPurchaselimit', {
    enumerable: true,
    value: null,
    writable: true
  });
  Object.defineProperty(product, 'promoHexColorCode', {
    enumerable: true,
    value: JSON.parse(preferences.product.BADGE_HEX_COLOR) ? JSON.parse(preferences.product.BADGE_HEX_COLOR).badgePip : null
  });
  base.call(this, product, apiProduct, options);

  productDecorators.availability(product, options.quantity, apiProduct.minOrderQuantity.value, apiProduct.availabilityModel, apiProduct);
  // if lineitem is instore, get the availability from store inventory list
  if (product.fromStoreId && options.lineItem.productInventoryList) {
    productDecorators.availability(
      product,
      options.quantity,
      apiProduct.minOrderQuantity.value,
      apiProduct.getAvailabilityModel(options.lineItem.productInventoryList),
      apiProduct
    );
  }
  isAvailableForInstore(product, apiProduct, options.variationModel);
  isInstoreEligible(product);
  fromStoreName(product);
  quantityDetailed(product, options.lineItem);
  productDecorators.custom(product, apiProduct, false);
  productDecorators.getHBCProductType(product, apiProduct);
  productDecorators.promotions(product, options.promotions);
  productDecorators.promotionPricing(product, apiProduct);
  productLineItemDecorators.discountHtml(product, options.lineItem);
  isDropShipItem(product, apiProduct);
  shipTypeText(product, apiProduct);
  uspsShip(product, apiProduct);
  pdRestrictedStateText(product, apiProduct);
  isSignatureRequired(product, apiProduct);
  giftWrapEligible(product, apiProduct);
  overridedPriceMessage(product, options.lineItem);
  productLineItemDecorators.qualifyingProductLineItemForBonusProduct(product, options.lineItem);
  return product;
}

module.exports = productLineItem;
