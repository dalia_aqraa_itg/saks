// Add any productLineItem custom attribute in this decorator
'use strict';

/**
 * Provides a saved discount HTML
 * @param {Object} productLineItem - The productLineitem object
 * @returns {Object} rendering ISML
 */
function provideDiscountTotal(productLineItem) {
  // Discount added
  var discounttotal = {};
  var isPWPPromoType = false;
  var pwpPromoCallOutMsg = '';
  discounttotal.basePrice = productLineItem.price;
  discounttotal.isDisount = productLineItem.getPriceAdjustments() && productLineItem.getPriceAdjustments().length > 0;
  if (discounttotal.isDisount) {
    var collections = require('*/cartridge/scripts/util/collections');
    var discounts = [];
    collections.forEach(productLineItem.getPriceAdjustments(), function (priceAdjustment) {
      var discount = {};
      var prodPromotion = priceAdjustment.promotion;
      if (prodPromotion) {
        // We will handle the PWP promotional items in a different way in the cart
        if (!empty(prodPromotion.custom.promotionType) && prodPromotion.custom.promotionType.value === 'PWP') {
          isPWPPromoType = true;
          pwpPromoCallOutMsg = prodPromotion.calloutMsg;
        } else {
          discount.promotionCartText =
            'discountDescription' in prodPromotion.custom && !empty(prodPromotion.custom.discountDescription) ? prodPromotion.custom.discountDescription : '';
        }
        discount.promotionDiscount = priceAdjustment.price;
        discounts.push(discount);
      }
    });
    discounttotal.isPWPPromoType = isPWPPromoType;
    discounttotal.pwpPromoCallOutMsg = pwpPromoCallOutMsg;
    discounttotal.discounts = discounts;
    discounttotal.adjustedPrice = productLineItem.adjustedPrice;
  }
  return discounttotal;
}

/**
 * Provides a saved discount HTML for the Cart Page
 * @param {Object} object - The product object
 * @returns {Object} rendering ISML
 */
function provideDiscountTotalHtml(object) {
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var attributeTemplate = 'cart/productCard/cartProductDisount';
  var attributeContext = {
    lineItem: object
  };
  return renderTemplateHelper.getRenderedHtml(attributeContext, attributeTemplate);
}

/**
 * Provides a saved discount HTML for the Checkout Flow
 * @param {Object} object - The product object
 * @returns {Object} rendering ISML
 */
function provideCheckoutDiscountTotalHtml(object) {
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var attributeTemplate = 'checkout/productCard/checkoutProductDiscount';
  var attributeContext = {
    lineItem: object
  };
  return renderTemplateHelper.getRenderedHtml(attributeContext, attributeTemplate);
}

module.exports = function (object, productLineItem) {
  Object.defineProperty(object, 'discountTotal', {
    enumerable: true,
    value: provideDiscountTotal(productLineItem)
  });
  Object.defineProperty(object, 'discountTotalHtml', {
    enumerable: true,
    value: provideDiscountTotalHtml(object)
  });
  Object.defineProperty(object, 'checkoutDiscountTotalHtml', {
    enumerable: true,
    value: provideCheckoutDiscountTotalHtml(object)
  });
};
