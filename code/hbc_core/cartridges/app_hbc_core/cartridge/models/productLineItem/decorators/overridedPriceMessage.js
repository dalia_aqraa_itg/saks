'use strict';
var Resource = require('dw/web/Resource');
var preferences = require('*/cartridge/config/preferences');

function checkIfBrandExlcuded(product) {
  var brandExcluded = false;
  if (product.brand) {
    var excludedBrands = preferences.csrBrandExcluded;
    // Split Brand with '|'
    var excludedBrandArray = excludedBrands.split('|');
    for (var i = 0; i < excludedBrandArray.length; i++) {
      var excludedBrand = excludedBrandArray[i].replace(/\s/g, '');
      var productBrand = product.brand.replace(/\s/g, '');
      if (excludedBrand.toLowerCase() === productBrand.toLowerCase()) {
        brandExcluded = true;
        return brandExcluded;
      }
    }
  }
  return brandExcluded;
}
/**
 * Get the Overrided Price for the CSC
 * @param {dw.order.ProductLineItem} lineItem - Product LineItem api
 * @returns {string} - Overriden Message
 */
function getPriceOverrideMessage(lineItem) {
  if (session.isUserAuthenticated()) {
    if (!checkIfBrandExlcuded(lineItem.product)) {
      if ('priceOverrided' in lineItem.custom && lineItem.custom.priceOverrided) {
        // eslint-disable-line
        return Resource.msg('label.item.price.override.edit', 'cart', null);
      } else {
        return Resource.msg('label.item.price.override', 'cart', null);
      }
    }
  }
  return null;
}

module.exports = function (object, lineItem) {
  Object.defineProperty(object, 'overridedPriceMessage', {
    enumerable: true,
    value: getPriceOverrideMessage(lineItem)
  });
};
