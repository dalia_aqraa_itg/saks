'use strict';

module.exports = function (object, lineItem) {
  Object.defineProperty(object, 'qualifyingProductLineItemForBonusProduct', {
    enumerable: true,
    value:
      lineItem.bonusProductLineItem && lineItem.getQualifyingProductLineItemForBonusProduct()
        ? lineItem.getQualifyingProductLineItemForBonusProduct().productID
        : null
  });
};
