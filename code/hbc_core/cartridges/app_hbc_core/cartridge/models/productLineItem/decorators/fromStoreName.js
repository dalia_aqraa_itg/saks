'use strict';

/** api modules */
var StoreMgr = require('dw/catalog/StoreMgr');

module.exports = function (object) {
  Object.defineProperty(object, 'fromStoreName', {
    get: function () {
      var storeName = null;
      if (object.fromStoreId) {
        var store = StoreMgr.getStore(object.fromStoreId);
        if (store) {
          storeName = store.name;
        }
      }
      return storeName;
    }
  });
};
