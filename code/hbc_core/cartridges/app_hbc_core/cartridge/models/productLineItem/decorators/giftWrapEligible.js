'use strict';

/**
 * Get if gift wrap is eligible for product
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @returns {string} - product's gift wrap
 */
function isgiftWrapEligible(apiProduct) {
  var giftWrapEligible = false;
  if (apiProduct.custom.hasOwnProperty('giftWrapEligible') && apiProduct.custom.giftWrapEligible === 'true') {
    // eslint-disable-line
    giftWrapEligible = true;
  }
  return giftWrapEligible;
}

module.exports = function (object, apiProduct) {
  Object.defineProperty(object, 'giftWrapEligible', {
    enumerable: true,
    value: isgiftWrapEligible(apiProduct)
  });
};
