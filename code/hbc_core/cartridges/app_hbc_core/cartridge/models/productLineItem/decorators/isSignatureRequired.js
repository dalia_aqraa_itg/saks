'use strict';

/**
 * Get if signature is required for a product
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @returns {string} - product's SignatureRequired
 */
function isSignatureRequired(apiProduct) {
  var signatureRequired = false;
  if (apiProduct.custom.hasOwnProperty('signatureRequiredType') && apiProduct.custom.signatureRequiredType === 'true') {
    // eslint-disable-line
    signatureRequired = true;
  }
  return signatureRequired;
}

module.exports = function (object, apiProduct) {
  Object.defineProperty(object, 'signatureRequired', {
    enumerable: true,
    value: isSignatureRequired(apiProduct)
  });
};
