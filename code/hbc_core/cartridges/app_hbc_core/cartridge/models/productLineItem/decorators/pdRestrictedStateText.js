'use strict';

/**
 * Get getpdRestrictedStateText text
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @returns {string} - product's pdRestrictedStateText
 */
function getpdRestrictedStateText(apiProduct) {
  var pdRestrictedStateText = null;
  if (apiProduct.custom.hasOwnProperty('pdRestrictedStateText') && apiProduct.custom.pdRestrictedStateText) {
    // eslint-disable-line
    pdRestrictedStateText = apiProduct.custom.pdRestrictedStateText;
  }
  return pdRestrictedStateText;
}

module.exports = function (object, apiProduct) {
  Object.defineProperty(object, 'pdRestrictedStateText', {
    enumerable: true,
    value: getpdRestrictedStateText(apiProduct)
  });
};
