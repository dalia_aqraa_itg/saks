'use strict';

/** script modules */
var preferences = require('*/cartridge/config/preferences');

module.exports = function (object) {
  Object.defineProperty(object, 'isInstoreEligible', {
    enumerable: true,
    value: preferences.isBopisEnabled && object.isAvailableForInstore
  });
};
