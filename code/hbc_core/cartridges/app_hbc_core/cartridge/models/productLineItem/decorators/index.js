'use strict';

var base = module.superModule;
base.discountHtml = require('*/cartridge/models/productLineItem/decorators/discountHtml');
base.qualifyingProductLineItemForBonusProduct = require('*/cartridge/models/productLineItem/decorators/qualifyingProductLineItemForBonusProduct');
base.promotionPricing = require('*/cartridge/models/product/decorators/promotionPricing');
base.promotions = require('*/cartridge/models/product/decorators/promotions');
module.exports = base;
