'use strict';

module.exports = function (object, lineItem) {
  Object.defineProperty(object, 'unitOfMeasure', {
    enumerable: true,
    value: lineItem.quantity.unit
  });
  Object.defineProperty(object, 'position', {
    enumerable: true,
    value: lineItem.position
  });
};
