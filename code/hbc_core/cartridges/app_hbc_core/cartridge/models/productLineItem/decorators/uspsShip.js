'use strict';

/**
 * Get usps ship text
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @returns {string} - product's usps ship text
 */
function isUSPSShipOk(apiProduct) {
  var uspsShipOK = true;
  if (apiProduct.custom.hasOwnProperty('uspsShipOk') && apiProduct.custom.uspsShipOk === 'false') {
    // eslint-disable-line
    uspsShipOK = false;
  }
  return uspsShipOK;
}

module.exports = function (object, apiProduct) {
  Object.defineProperty(object, 'uspsShippingOK', {
    enumerable: true,
    value: isUSPSShipOk(apiProduct)
  });
};
