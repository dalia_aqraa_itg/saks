'use strict';

var ProductListItemModel = require('*/cartridge/models/productListItem');
var preferences = require('*/cartridge/config/preferences');
var PAGE_SIZE_ITEMS = preferences.defaultPageSize ? preferences.defaultPageSize : 24;
var TOTAL_PAGE_SIZE = preferences.totalPageSize ? preferences.totalPageSize : 96;
var ArrayList = require('dw/util/ArrayList');

/**
 * Generates sorted list of products based on the product availability
 *
 * @param {dw/util/ArrayList} list - Wislist products
 * @return {dw/util/ArrayList} - Sorted list
 */
function getSortedWishlist(list) {
  var instockProds = new ArrayList();
  var oosProds = new ArrayList();
  var allProdList = new ArrayList();
  var PropertyComparator = require('dw/util/PropertyComparator');

  if (!empty(list)) {
    list.toArray().forEach(function (item) {
      var itemSearchable = true;
      if (item.product) {
        if (item.product.master) {
          itemSearchable = item.product.searchable;
        } else {
          if (!empty(item.product.variationModel) && !empty(item.product.variationModel.master)) {
            var mastProduct = item.product.masterProduct;
            itemSearchable = mastProduct.searchable;
          } else {
            itemSearchable = item.product.searchable;
          }
        }
      } else {
        itemSearchable = false;
      }

      if (itemSearchable) {
        if (
          item.product &&
          (item.product.custom.hbcProductType === 'bridal' ||
            item.product.custom.hbcProductType === 'home' ||
            item.product.availabilityModel.availabilityStatus !== 'NOT_AVAILABLE')
        ) {
          instockProds.add(item);
        } else {
          oosProds.add(item);
        }
      }
    });
  }

  instockProds.sort(new PropertyComparator('creationDate', false));
  oosProds.sort(new PropertyComparator('creationDate', false));

  if (!empty(instockProds)) {
    allProdList.addAll(instockProds);
  }

  if (!empty(oosProds)) {
    allProdList.addAll(oosProds);
  }

  return allProdList;
}

/**
 * creates a plain object that represents a productList
 * @param {dw.customer.ProductList} productListObject - User's productList object
 * @param {Object} config - configuration object
 * @returns {Object} an object that contains information about the users productList
 */
function createProductListObject(productListObject, config) {
  var PAGE_SIZE = 2;
  var pageSize = config.pageSize || PAGE_SIZE;
  var pageNumber = Number(config.pageNumber) || 1;
  var totalNumber = 0;
  var result;
  var publicView = config.publicView;
  var isShared = false;
  var urlParam = config.urlParam;
  if (productListObject) {
    if (!empty(config.isShared) && config.isShared) {
      isShared = true;
    }
    result = {
      owner: {
        exists: !!productListObject.owner,
        firstName: productListObject.owner ? productListObject.owner.profile.firstName : false,
        lastName: productListObject.owner ? productListObject.owner.profile.lastName : false
      },
      publicList: productListObject.public,
      UUID: productListObject.UUID,
      publicView: publicView,
      pageNumber: pageNumber,
      items: [],
      type: productListObject.type,
      isShared: isShared
    };

    var productListItem;
    var count = productListObject.items.getLength();
    var sortedList = getSortedWishlist(productListObject.items);
    var counterAfterFilter = 0;
    sortedList.toArray().forEach(function (item) {
      productListItem = new ProductListItemModel(item).productListItem;
      if (productListItem && item.product) {
        if (config.publicView && item.product.master) {
          count--;
        } else if (totalNumber >= pageSize * (pageNumber - 1) && totalNumber < pageSize * (pageNumber - 1) + PAGE_SIZE_ITEMS) {
          result.items.push(productListItem);
          totalNumber++;
        } else {
          totalNumber++;
        }
        counterAfterFilter++;
      }
    });

    result.length = counterAfterFilter;
    var totalBlocks = Math.ceil(TOTAL_PAGE_SIZE / PAGE_SIZE_ITEMS);
    var firstValue = Math.floor(pageNumber / totalBlocks) * totalBlocks;
    var lastValue = Math.ceil(pageNumber / totalBlocks) * totalBlocks;
    var totalPageNumber = Math.ceil(count / pageSize);
    if (config.scrollPersist) {
      firstValue = (Math.ceil(pageNumber / totalBlocks) - 1) * totalBlocks;
    }
    var showMoreLink;
    var showPrevLink;
    var showPagination = Number(pageNumber) === lastValue || Number(pageNumber) === totalPageNumber;
    var showLink = pageNumber > firstValue + 1 && pageNumber < lastValue && totalNumber >= pageNumber * PAGE_SIZE_ITEMS;
    if (urlParam != null && urlParam === 'next') {
      showPrevLink = false;
    } else if (urlParam === 'previous' && showLink) {
      showPrevLink = true;
    } else if (urlParam == null && pageNumber > firstValue + 1) {
      showPrevLink = true;
    } else {
      showPrevLink = false;
    }

    if (urlParam != null && urlParam === 'previous') {
      showMoreLink = false;
    } else if (urlParam === 'next' && showLink) {
      showMoreLink = true;
    } else if (urlParam == null && pageNumber < lastValue && pageNumber < totalPageNumber) {
      showMoreLink = true;
    } else {
      showMoreLink = false;
    }

    result.showMore = showMoreLink;
    result.showPrev = showPrevLink;
    result.showPagination = showPagination;
    result.pageNumber = pageNumber;
  } else {
    result = null;
  }
  return result;
}

/**
 * @typedef config
 * @type Object
 */
/**
 * List class that represents a productList
 * @param {dw.customer.ProductList} productListObject - User's productlist
 * @param {Object} config - configuration object
 * @constructor
 */
function productList(productListObject, config) {
  this.productList = createProductListObject(productListObject, config);
}

module.exports = productList;
