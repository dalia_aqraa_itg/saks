'use strict';

var decorators = require('*/cartridge/models/product/decorators/index');
var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
var promotionCache = require('*/cartridge/scripts/util/promotionCache');
var ProductSearchModel = require('dw/catalog/ProductSearchModel');
var PromotionMgr = require('dw/campaign/PromotionMgr');
var collections = require('*/cartridge/scripts/util/collections');

/**
 * Get product search hit for a given product
 * @param {dw.catalog.Product} apiProduct - Product instance returned from the API
 * @returns {dw.catalog.ProductSearchHit} - product search hit for a given product
 */
function getSearchHitProduct(apiProduct) {
  var searchModel = new ProductSearchModel();
  searchModel.setSearchPhrase(apiProduct.ID);
  searchModel.search();

  if (searchModel.count === 0) {
    searchModel.setSearchPhrase(apiProduct.ID.replace(/-/g, ' '));
    searchModel.search();
  }

  var hit = searchModel.getProductSearchHit(apiProduct);
  if (!hit && searchModel.getProductSearchHits().hasNext()) {
    var searchHits = searchModel.getProductSearchHits();
    var tempHit = null;
    while (searchHits.hasNext()) {
      var searchHit = searchHits.next();
      var masterID = apiProduct.variant ? apiProduct.masterProduct.ID : apiProduct.ID;
      if (searchHit.productID === masterID) {
        tempHit = searchHit;
      }
    }
    if (tempHit) {
      hit = tempHit;
    }
  }
  return hit;
}

/**
 * Decorate product with product tile information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {string} productType - Product type information
 * @param {string} frpid - First represented product ID
 *
 * @returns {Object} - Decorated product model
 */
function productTile(product, vApiProduct, productType, frpid, apiOptions) {
  var frProduct = vApiProduct;
  if (frpid) {
    var ProductMgr = require('dw/catalog/ProductMgr');
    // eslint-disable-next-line no-param-reassign
    frProduct = ProductMgr.getProduct(frpid) || vApiProduct;
  }
  var searchHitProduct = getSearchHitProduct(frProduct);

  var options = productHelper.getConfig(vApiProduct, {
    options: null,
    quantity: '1'
  });
  var apiProduct = apiOptions.apiProduct ? apiOptions.apiProduct : vApiProduct;
  var searchApiProduct = options.apiProduct ? options.apiProduct : vApiProduct;
  var priceProductHit = getSearchHitProduct(searchApiProduct);
  decorators.base(product, apiProduct, productType);
  var firstPromotionID = null;

  if (promotionCache.promotions && promotionCache.promotions.length) {
    firstPromotionID = promotionCache.promotions[0];
  }
  var firstPromotion = null;

  firstPromotion = PromotionMgr.activeCustomerPromotions.getProductPromotions(apiProduct);
  if (firstPromotion && firstPromotion.length > 0) {
    product.plpPromos = firstPromotion[0];
  }
  product.variantID = product.id; // eslint-disable-line
  var representedProduct = searchHitProduct.firstRepresentedProduct;
  if (product.productType === 'set') {
    representedProduct = apiProduct;
  }
  decorators.images(product, representedProduct, {
    types: ['medium'],
    quantity: 'all'
  });

  decorators.readyToOrder(product, apiOptions.variationModel);
  decorators.searchPrice(product, priceProductHit, promotionCache.promotions, getSearchHitProduct);
  if (productType !== 'set') {
    decorators.searchVariationAttributes(product, searchHitProduct, frpid, apiOptions);
  }

  decorators.getHBCProductType(product, apiProduct);
  decorators.getSearchableIfUnavailableFlag(product, apiProduct);
  decorators.alwaysEnableSwatches(product, apiProduct);
  decorators.custom(product, apiOptions.apiProduct, false);
  decorators.turntoRatings(product, apiProduct);
  /* get the variationModel to check the number of variants available
   * If only one variant is available for a master, then swap the master ID with variant ID so the
   * product can be added to cart directly
   */
  var variationModel = apiProduct.variationModel;
  var variants = variationModel.getVariants();
  var hasOnlyOneVariant = variants.length === 1;
  // var apiProductForPromo = getDefaultVariant(apiProduct);
  product.hasOnlyOneVariant = hasOnlyOneVariant; // eslint-disable-line
  if (hasOnlyOneVariant) {
    var apiProductForPromo = variants.iterator().next();
    product.variantID = apiProductForPromo.ID; // eslint-disable-line
  }
  if (product.variationAttributes[0] && product.variationAttributes[0].onlyColorVariantID) {
    product.variantID = product.variationAttributes[0].onlyColorVariantID; // eslint-disable-line
    product.hasOnlyColor = true; // eslint-disable-line
  }
  if (
    product.variationAttributes[0] &&
    product.variationAttributes[0].onlyColorVariantID &&
    product.variationAttributes.length > 1 &&
    product.variationAttributes[1] &&
    product.variationAttributes[1].values.length > 1
  ) {
    product.variantID = product.id; // eslint-disable-line
  }
  if (product.variationAttributes.length > 1 && product.variationAttributes[1] && product.variationAttributes[1].values.length === 1) {
    // eslint-disable-next -line no-param-reassign
    product.hasOneSize = true;
  }
  // decorators.promotionPricing(product, apiProductForPromo);
  decorators.promotionPricing(product, searchHitProduct.firstRepresentedProduct);
  decorators.availability(product, 1, apiProduct.minOrderQuantity.value, apiProduct.availabilityModel, apiProduct);

  /*
   * Check if the product is entirely OOS
   */
  product.soldout = false; // eslint-disable-line
  if (searchHitProduct.getFirstRepresentedProduct().getAvailabilityModel() && !searchHitProduct.getFirstRepresentedProduct().getAvailabilityModel().getAvailability()) {
    product.soldout = true; // eslint-disable-line
  }
  return product;
}

module.exports = productTile;
