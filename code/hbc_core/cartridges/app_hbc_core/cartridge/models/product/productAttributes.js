'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var urlHelper = require('*/cartridge/scripts/helpers/urlHelpers');
var ImageModel = require('*/cartridge/models/product/productImages');

/**
 * Validates varaition attribute in of no_size or no_color
 * @param {Object} selectedProductVariationAttributeValue Vairaion Value Object
 * @returns {boolean} hideAttribute returns the status
 */
function isNoVariant(selectedProductVariationAttributeValue) {
  var hideAttribute = false;
  switch (selectedProductVariationAttributeValue.displayValue.toUpperCase()) {
    case 'NO SIZE':
    case 'NO COLOR':
    case 'NO COLOUR':
      hideAttribute = true;
      break;
  }
  return hideAttribute;
}

/**
 * Determines whether a product attribute has image swatches.  Currently, the only attribute that
 *     does is Color.
 * @param {string} dwAttributeId - Id of the attribute to check
 * @returns {boolean} flag that specifies if the current attribute should be displayed as a swatch
 */
function isSwatchable(dwAttributeId) {
  var imageableAttrs = ['color'];
  return imageableAttrs.indexOf(dwAttributeId) > -1;
}

/**
 * Retrieve all attribute values
 *
 * @param {dw.catalog.ProductVariationModel} variationModel - A product's variation model
 * @param {dw.catalog.ProductVariationAttributeValue} selectedValue - Selected attribute value
 * @param {dw.catalog.ProductVariationAttribute} attr - Attribute value'
 * @param {string} endPoint - The end point to use in the Product Controller
 * @param {string} selectedOptionsQueryParams - Selected options query params
 * @param {string} quantity - Quantity selected
 * @returns {Object} - List of attribute value objects for template context
 */
function getAllAttrValues(variationModel, selectedValue, attr, endPoint, selectedOptionsQueryParams, quantity) {
  var attrValues = variationModel.getAllValues(attr);
  var actionEndpoint = 'Product-' + endPoint;
  var maxLength = 0;
  var masterProd = variationModel.master;
  // Filterout NoVariation Attribute
  var ArrayList = require('dw/util/ArrayList');
  var nonVaraintList = attrValues;
  if (attrValues) {
    var nonVaraint = attrValues.toArray().filter(function (tAttrValue) {
      return !isNoVariant(tAttrValue);
    });
    nonVaraintList = new ArrayList(nonVaraint);
  }
  var processedAttrs = collections.map(nonVaraintList, function (value) {
    var isSelectable;
    if (
      !empty(masterProd) &&
      !empty(masterProd.custom.hbcProductType) &&
      (masterProd.custom.hbcProductType === 'home' || masterProd.custom.hbcProductType === 'bridal') &&
      masterProd.searchableIfUnavailableFlag
    ) {
      isSelectable = true;
    } else {
      isSelectable = variationModel.hasOrderableVariants(attr, value);
    }

    var isSelected = (selectedValue && selectedValue.equals(value)) || false;
    var valueUrl = '';

    var processedAttr = {
      id: value.ID,
      description: value.description,
      displayValue: value.displayValue,
      value: value.value,
      selected: isSelected,
      selectable: isSelectable
    };

    valueUrl =
      isSelected && endPoint !== 'Show'
        ? variationModel.urlUnselectVariationValue(actionEndpoint, attr)
        : variationModel.urlSelectVariationValue(actionEndpoint, attr, value);
    processedAttr.url = urlHelper.appendQueryParams(valueUrl, [selectedOptionsQueryParams, 'quantity=' + quantity]);

    if (isSwatchable(attr.attributeID)) {
      processedAttr.images = new ImageModel(value, {
        types: ['swatch'],
        quantity: 'all'
      });
    }

    if (attr.attributeID === 'size' && value.displayValue.length >= maxLength) {
      maxLength = value.displayValue.length;
    }

    return processedAttr;
  });
  return {
    processedAttr: processedAttrs,
    maxLength: maxLength
  };
}

/**
 * Gets the Url needed to relax the given attribute selection, this will not return
 * anything for attributes represented as swatches.
 *
 * ALTERED : definition changed to add a selected attribute value, this is to avoid another
 * iteration over the value
 * @param {Array} values - Attribute values
 * @param {string} attrID - id of the attribute
 * @returns {Object} -the Url that will remove the selected attribute and the selected attribute display value.
 */
function getAttrResetUrlAndValue(values, attrID) {
  var urlReturned;
  var selectedValue = null;
  var value;
  var returnValue = {};

  for (var i = 0; i < values.length; i++) {
    value = values[i];
    if (!value.images) {
      if (value.selected) {
        urlReturned = value.url;
        break;
      }

      if (value.selectable) {
        urlReturned = value.url.replace(attrID + '=' + value.value, attrID + '=');
        break;
      }
    }
    if (value.selected) {
      selectedValue = value.displayValue;
    }
  }
  returnValue.urlReturned = urlReturned;
  returnValue.selectedValue = selectedValue;

  return returnValue;
}

/**
 *
 * @param {Object} attr variation Attribute model
 * @param {Object} selectedProductVariationAttributeValue Selected variants
 * @returns {string} Returns text to display
 */
function provideAttrText(attr, selectedProductVariationAttributeValue) {
  if (selectedProductVariationAttributeValue && attr) {
    if (isNoVariant(selectedProductVariationAttributeValue)) {
      return;
    }
  }
  var attrText = '<span class="text1">' + attr.displayName + '</span>';
  if (selectedProductVariationAttributeValue && attr) {
    attrText = attrText.concat(': ').concat('<span class="text2">' + selectedProductVariationAttributeValue.displayValue + '</span>');
  }
  return attrText;
}

/**
 *
 * @param {Object} attr variation Attribute model
 * @param {Object} selectedProductVariationAttributeValue Selected variants
 * @returns {string} Returns text to display
 */
function provideEditAttrText(attr, selectedProductVariationAttributeValue) {
  if (selectedProductVariationAttributeValue && attr) {
    return attr.displayName + ' - ' + selectedProductVariationAttributeValue.displayValue;
  }
  return attr.displayName;
}

/**
 * @param {integer} maxCount maximum character size of variation attribute value
 * @returns {string} returns respective class mapped in site preference
 */
function provideSelectedSizeClass(maxCount) {
  var matchClass = '';
  if (maxCount && maxCount > 0) {
    var preferences = require('*/cartridge/config/preferences');
    var isMatchFound = false;
    var sizeTreatment = preferences.product.SIZE_TREATMENT;
    if (sizeTreatment) {
      sizeTreatment.forEach(function (enumVal) {
        if (!isMatchFound && maxCount <= enumVal.value) {
          isMatchFound = true;
          matchClass = enumVal.displayValue;
        }
      });
    }
  }
  return matchClass;
}

/**
 * @constructor
 * @classdesc Get a list of available attributes that matches provided config
 *  We are not overriding this model since, same logic has to implement with adiditional loops
 * @param {dw.catalog.ProductVariationModel} variationModel - current product variation
 * @param {Object} attrConfig - attributes to select
 * @param {Array} attrConfig.attributes - an array of strings,representing the
 *                                        id's of product attributes.
 * @param {string} attrConfig.attributes - If this is a string and equal to '*' it signifies
 *                                         that all attributes should be returned.
 *                                         If the string is 'selected', then this is comming
 *                                         from something like a product line item, in that
 *                                         all the attributes have been selected.
 *
 * @param {string} attrConfig.endPoint - the endpoint to use when generating urls for
 *                                       product attributes
 * @param {string} selectedOptionsQueryParams - Selected options query params
 * @param {string} quantity - Quantity selected
 */
function VariationAttributesModel(variationModel, attrConfig, selectedOptionsQueryParams, quantity) {
  var allAttributes = variationModel.productVariationAttributes;
  var result = [];
  collections.forEach(allAttributes, function (attr) {
    var selectedValue = variationModel.getSelectedValue(attr);
    var valuesAttr = getAllAttrValues(variationModel, selectedValue, attr, attrConfig.endPoint, selectedOptionsQueryParams, quantity);
    var values = valuesAttr.processedAttr;
    var maxCount = valuesAttr.maxLength;
    var selectedSizeClass = provideSelectedSizeClass(maxCount);
    var attributeResetValue = getAttrResetUrlAndValue(values, attr.ID);
    var resetUrl = attributeResetValue.urlReturned;

    // If no variation value skip the loop
    if (
      values &&
      values.length &&
      ((Array.isArray(attrConfig.attributes) && attrConfig.attributes.indexOf(attr.attributeID) > -1) || attrConfig.attributes === '*')
    ) {
      result.push({
        attributeId: attr.attributeID,
        displayName: attr.displayName,
        displayValue: selectedValue && selectedValue.displayValue ? selectedValue.displayValue : '',
        id: attr.ID,
        swatchable: isSwatchable(attr.attributeID),
        values: values,
        resetUrl: resetUrl,
        selectedAttribute: selectedValue,
        attrDisplay: provideAttrText(attr, selectedValue),
        attrEditDisplay: provideEditAttrText(attr, selectedValue),
        selectedSizeClass: selectedSizeClass,
        attributeSelectedValue: attributeResetValue.selectedValue
      });
    } else if (attrConfig.attributes === 'selected') {
      result.push({
        displayName: attr.displayName,
        displayValue: selectedValue && selectedValue.displayValue ? selectedValue.displayValue : '',
        attributeId: attr.attributeID,
        id: attr.ID,
        selectedSizeClass: selectedSizeClass,
        attributeSelectedValue: attributeResetValue.selectedValue,
        attrDisplay: provideAttrText(attr, null),
        attrEditDisplay: provideEditAttrText(attr, selectedValue)
      });
    }
  });
  result.forEach(function (item) {
    this.push(item);
  }, this);
}

VariationAttributesModel.prototype = [];

module.exports = VariationAttributesModel;
