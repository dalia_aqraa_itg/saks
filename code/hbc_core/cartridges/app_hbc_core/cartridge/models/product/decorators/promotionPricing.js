'use strict';

/**
 * Promotional message  of the product from promotion priceBook
 * This method returns the promo message if the pricebook attached to the product is a promotion pricebook.
 * @param {Object} apiProduct - The apiProduct
 * @returns {Object} obj - returns object with promoMessage
 */
function getPromoPriceBookPromoMsg(apiProduct) {
  var priceModel = apiProduct.priceModel;
  if (priceModel && priceModel.priceInfo && priceModel.priceInfo.priceBook) {
    var priceBook = priceModel.priceInfo.priceBook;
    var isPromotionPriceBook = 'isPromotionPriceBook' in priceBook.custom && priceBook.custom.isPromotionPriceBook;
    var priceInfo = priceModel.priceInfo.priceInfo || '';
    // fetch the promotion message from the pricebook level as a fallback
    if (priceInfo === '') {
      priceInfo = 'priceBookMessage' in priceBook.custom ? priceBook.custom.priceBookMessage : '';
    }
    return {
      isPromotionalPrice: isPromotionPriceBook,
      promoMessage: priceInfo
    };
  }
  return null;
}

module.exports = function (object, apiProduct) {
  Object.defineProperty(object, 'promotionalPricing', {
    enumerable: true,
    value: getPromoPriceBookPromoMsg(apiProduct)
  });
};
