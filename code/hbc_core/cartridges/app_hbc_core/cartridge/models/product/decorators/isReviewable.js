'use strict';

/**
 * Check if the product this reviewable or not
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @returns {boolean} - if product is drop ship item or not
 */
function isReveiwable(apiProduct) {
  var reviewable = false;
  if (apiProduct.custom.hasOwnProperty('reviewable') && apiProduct.custom.reviewable === 'true') {
    // eslint-disable-line
    reviewable = true;
  }
  return reviewable;
}

module.exports = function (object, apiProduct) {
  Object.defineProperty(object, 'isReveiwable', {
    enumerable: true,
    value: isReveiwable(apiProduct)
  });
};
