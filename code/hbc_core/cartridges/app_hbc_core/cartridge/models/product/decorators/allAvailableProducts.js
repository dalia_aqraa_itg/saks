'use strict';

/**
 * Get Default stores
 * @param {dw.catalog.Product} product Demandware product
 * @returns {Object} - array of available stores object
 */
function getStores(product) {
  var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');
  var instorePUstoreHelpers = require('*/cartridge/scripts/helpers/instorePickupStoreHelpers');
  var defaultSearchRadius = 15;
  var stores = storeHelpers.getStores(defaultSearchRadius, null, null, null, request.geolocation);
  var tStores = [];
  if (stores && stores.stores) {
    stores.stores.forEach(function (store) {
      var tStore = {};
      tStore.available = instorePUstoreHelpers.getStoreInventory(store.ID, product.ID).toString();
      tStore.id = store.ID.toString();
      tStores.push(tStore);
    });
  }
  return tStores;
}
/**
 * All all variational product
 * @param {dw.catalog.Product} apiProduct Demandware product
 * @returns {Object} - array of available products object
 */
function provideAllAvailableProducts(apiProduct) {
  var masterProduct = apiProduct.master ? apiProduct : apiProduct.variant ? apiProduct.masterProduct : apiProduct;
  var collections = require('*/cartridge/scripts/util/collections');
  var avalableProds = [];
  var numberOfAvailableItems = 0;
  var numberOfInStockItems = 0;
  collections.forEach(masterProduct.variationModel.variants, function (product) {
    var avalableProd = {};
    avalableProd.available_dc =
      product.availabilityModel && product.availabilityModel.inventoryRecord ? product.availabilityModel.inventoryRecord.ATS.value.toString() : '0';
    avalableProd.sku = product.ID;
    //avalableProd.stores = getStores(apiProduct);
    avalableProds.push(avalableProd);
    if (product.availabilityModel && product.availabilityModel.inStock){
      numberOfInStockItems++;
    }
    if (product.availabilityModel && product.availabilityModel.availability){
      numberOfAvailableItems++;
    }
  });
  return [avalableProds, numberOfAvailableItems, numberOfInStockItems];
}

module.exports = function (object, apiProduct) {
  var response = provideAllAvailableProducts(apiProduct);
  Object.defineProperty(object, 'allAvailableProducts', {
    enumerable: true,
    value: response[0]
  });

  Object.defineProperty(object, 'numberOfAvailableItems', {
    enumerable: true,
    value: response[1]
  });

  Object.defineProperty(object, 'numberOfInStockItems', {
    enumerable: true,
    value: response[2]
  });

};
