'use strict';

/**
 * Get availability for in store pickup
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {dw.catalog.ProductVariationModel} variationModel - The product's variation model
 * @returns {boolean} - if selected variant product is available return selected variant
 *    availability otherwise return master product availability
 */
function getAvailableForInStorePickup(apiProduct, variationModel) {
  var isAvailableForInstore = false;
  var actualProduct = null;
  if (apiProduct.master && variationModel) {
    // default or an arbitrary variant
    actualProduct = variationModel.defaultVariant;
  } else if (variationModel) {
    // selected variant if not master
    actualProduct = variationModel.selectedVariant;
  }
  if (actualProduct && actualProduct.custom.hasOwnProperty('pickUpAllowedInd') && actualProduct.custom.pickUpAllowedInd === 'true') {
    // eslint-disable-line
    isAvailableForInstore = true;
  }
  if (apiProduct && apiProduct.productSet) {
    isAvailableForInstore = true;
  }
  return isAvailableForInstore;
}

module.exports = function (object, apiProduct, variationModel) {
  Object.defineProperty(object, 'isAvailableForInstore', {
    enumerable: true,
    value: getAvailableForInStorePickup(apiProduct, variationModel)
  });
};
