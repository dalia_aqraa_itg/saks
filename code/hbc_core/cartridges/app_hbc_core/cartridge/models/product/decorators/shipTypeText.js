'use strict';

/**
 * Get ship type text
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @returns {string} - product's pdRestrictedShipTypeText
 *    availability otherwise return master product availability
 */
function getShipTypeText(apiProduct) {
  var isDropShipItem = null;
  if (apiProduct.custom.hasOwnProperty('pdRestrictedShipTypeText') && apiProduct.custom.pdRestrictedShipTypeText) {
    // eslint-disable-line
    isDropShipItem = apiProduct.custom.pdRestrictedShipTypeText;
  }
  return isDropShipItem;
}

module.exports = function (object, apiProduct) {
  Object.defineProperty(object, 'shipTypeText', {
    enumerable: true,
    value: getShipTypeText(apiProduct)
  });
};
