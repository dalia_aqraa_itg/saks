'use strict';

/**
 * Get drop ship items in basket
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @returns {boolean} - if product is drop ship item or not
 */
function isDropShipItem(apiProduct) {
  var isDropShip = false;
  if (apiProduct.custom.hasOwnProperty('dropShipInd') && apiProduct.custom.dropShipInd === 'true') {
    // eslint-disable-line
    isDropShip = true;
  }
  return isDropShip;
}

module.exports = function (object, apiProduct) {
  Object.defineProperty(object, 'isDropShipItem', {
    enumerable: true,
    value: isDropShipItem(apiProduct)
  });
};
