'use strict';

var Resource = require('dw/web/Resource');
var Calendar = require('dw/util/Calendar');
var preferences = require('*/cartridge/config/preferences');
var available = true;

/**
 *
 * @param {Object} object product model
 * @param {Object} quantity total quantity
 * @returns {boolean} returns availability
 */
function isInPurchaselimit(object, quantity) {
  // Validates for not null and 0, if null default will be true
  if (quantity) {
    try {
      return object.purchaselimit ? parseInt(object.purchaselimit) >= quantity : true;
    } catch (e) {}
  }
  return true;
}

module.exports = function (object, quantity, minOrderQuantity, availabilityModel, apiProduct) {
  Object.defineProperty(object, 'availability', {
    enumerable: true,
    value: (function () {
      var availability = {};
      availability.messages = [];
      availability.buttonName = {};
      availability.isInPurchaselimit = {};
      availability.isInPurchaselimitMessage = {};
      availability.isAboveThresholdLevel = {};
      availability.hexColorCode = {};

      var productQuantity = quantity ? parseInt(quantity, 10) : minOrderQuantity;
      var availabilityModelLevels = availabilityModel.getAvailabilityLevels(productQuantity);
      var inventoryRecord = availabilityModel.inventoryRecord;
      availability.isInPurchaselimit = true;
      availability.isAboveThresholdLevel = false;
      availability.isInPurchaselimitMessage = '';
      availability.outofstockmessage = Resource.msg('label.not.available.items', 'common', null);
      if (inventoryRecord && inventoryRecord.ATS) {
        availability.ats = inventoryRecord.ATS.value;
      } else {
        availability.ats = 0;
      }
      var apiProd = apiProduct;

      if (!isInPurchaselimit(object, productQuantity)) {
        if (availabilityModelLevels.notAvailable.value === productQuantity) {
          //availability.messages.push(Resource.msg('label.not.available', 'common', null));
        } else {
          availability.messages.push(Resource.msgf('label.notinpurchaselimit', 'common', null, object.purchaselimit));
        }
        availability.isInPurchaselimitMessage = Resource.msgf('label.notinpurchaselimit', 'common', null, object.purchaselimit);
        if (inventoryRecord && inventoryRecord.ATS && object.purchaselimit > inventoryRecord.ATS.value) {
          // eslint-disable-next-line no-param-reassign
          object.purchaselimit = inventoryRecord.ATS.value;
        }
        availability.isInPurchaselimit = false;
        availability.qtyValue = object.purchaselimit;
        availability.hexColorCode = JSON.parse(preferences.product.BADGE_HEX_COLOR)
          ? JSON.parse(preferences.product.BADGE_HEX_COLOR).badgeLimitedInventory
          : null;
        availability.buttonName = Resource.msg('button.addtocart', 'common', null);
        return availability;
      }

      if (inventoryRecord && inventoryRecord.inStockDate) {
        availability.inStockDate = inventoryRecord.inStockDate.toDateString();
      } else {
        availability.inStockDate = null;
      }

      if (availabilityModelLevels.inStock.value > 0) {
        var thresholdValue = preferences.product.INVENTORY_THRESHOLD_VALUE;
        if (thresholdValue && inventoryRecord && inventoryRecord.ATS.value <= thresholdValue) {
          availability.isAboveThresholdLevel = true;
          availability.messages.push(Resource.msg('label.limitedstock', 'product', null));
          availability.hexColorCode = JSON.parse(preferences.product.BADGE_HEX_COLOR)
            ? JSON.parse(preferences.product.BADGE_HEX_COLOR).badgeLimitedInventory
            : null;
        } else if (availabilityModelLevels.inStock.value === productQuantity) {
          availability.messages.push(Resource.msg('label.instock', 'common', null));
        } else {
          availability.messages.push(Resource.msgf('label.quantity.in.stock', 'common', null, availabilityModelLevels.inStock.value));
        }
        available = true;
        availability.buttonName = Resource.msg('button.addtocart', 'common', null);
      }

      if (availabilityModelLevels.preorder.value > 0) {
        if (apiProd && 'sfccPreorder' in apiProd.custom && (apiProd.custom.sfccPreorder === 'T' || apiProd.custom.sfccPreorder === 'Y')) {
          if (availabilityModelLevels.preorder.value === productQuantity) {
            availability.messages.push(Resource.msg('label.preorder', 'common', null));
          } else {
            availability.messages.push(Resource.msgf('label.preorder.items', 'common', null, availabilityModelLevels.preorder.value));
          }
          availability.buttonName = Resource.msg('button.addtocart', 'common', null);
          available = true;
          availability.preorderButtonName = Resource.msg('product.tile.preordertobag', 'product', null);
        } else {
          availability.messages.push(Resource.msg('label.not.available.items', 'common', null));
          availability.buttonName = Resource.msg('button.soldout', 'common', null);
          availability.ats = 0;
          available = false;
        }
      }

      if (availabilityModelLevels.backorder.value > 0) {
        if (apiProd && 'sfccPreorder' in apiProd.custom && (apiProd.custom.sfccPreorder === 'T' || apiProd.custom.sfccPreorder === 'Y')) {
          // This will be treated as Pre Order.
          if (availabilityModelLevels.backorder.value === productQuantity) {
            availability.messages.push(Resource.msg('label.preorder', 'common', null));
          } else {
            availability.messages.push(Resource.msgf('label.preorder.items', 'common', null, availabilityModelLevels.backorder.value));
          }
          availability.buttonName = Resource.msg('button.addtocart', 'common', null);
          availability.preorderButtonName = Resource.msg('product.tile.preordertobag', 'product', null);
          available = true;
        } else {
          availability.messages.push(Resource.msg('label.not.available.items', 'common', null));
          availability.buttonName = Resource.msg('button.soldout', 'common', null);
          availability.ats = 0;
          available = false;
        }
      }

      if (availabilityModelLevels.notAvailable.value > 0) {
        // clearing partial availability message flags
        availability.messages = [];
        if (availabilityModelLevels.notAvailable.value === productQuantity) {
          // availability.messages.push(Resource.msg('label.not.available', 'common', null));
        } else {
          availability.messages.push(Resource.msg('label.not.available.items', 'common', null));
        }
        availability.buttonName = Resource.msg('button.soldout', 'common', null);
        available = false;
      }

      if (object.productType === 'master') {
        availability.messages = [];
      }

      return availability;
    })()
  });
  Object.defineProperty(object, 'available', {
    enumerable: true,
    value: available ? (isInPurchaselimit(object, quantity) ? availabilityModel.isOrderable(parseFloat(quantity) || minOrderQuantity) : false) : false
  });
  Object.defineProperty(object, 'orderableNotInPurchaselimit', {
    enumerable: true,
    value: available
      ? isInPurchaselimit(object, quantity)
        ? availabilityModel.isOrderable(parseFloat(quantity) || minOrderQuantity)
        : availabilityModel.isOrderable(parseFloat(object.purchaselimit) || minOrderQuantity)
      : false
  });
};
