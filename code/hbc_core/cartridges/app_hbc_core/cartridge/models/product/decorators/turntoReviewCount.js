'use strict';

/**
 * Get review count from product fed from turnto feed
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @returns {boolean} - if product is drop ship item or not
 */
function getTurntoReviewCount(apiProduct) {
  var turntoReviewCount = 0;
  if (apiProduct.custom.hasOwnProperty('turntoReviewCount') && apiProduct.custom.turntoReviewCount) {
    // eslint-disable-line
    turntoReviewCount = apiProduct.custom.turntoReviewCount;
  }
  return turntoReviewCount;
}

module.exports = function (object, apiProduct) {
  Object.defineProperty(object, 'turntoReviewCount', {
    enumerable: true,
    value: getTurntoReviewCount(apiProduct)
  });
};
