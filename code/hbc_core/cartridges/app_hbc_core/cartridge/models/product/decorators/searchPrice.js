'use strict';

var PromotionMgr = require('dw/campaign/PromotionMgr');
var ArrayList = require('dw/util/ArrayList');
var pricingHelper = require('*/cartridge/scripts/helpers/pricing');
var PriceBookMgr = require('dw/catalog/PriceBookMgr');
var DefaultPrice = require('*/cartridge/models/price/default');
var RangePrice = require('*/cartridge/models/price/range');
var formatMoney = require('dw/util/StringUtils').formatMoney;

/**
 * Retrieve promotions that apply to current product
 * @param {dw.catalog.ProductSearchHit} searchHit - current product returned by Search API.
 * @param {Array<string>} activePromotions - array of ids of currently active promotions
 * @return {Array<Promotion>} - Array of promotions for current product
 */
function getPromotions(searchHit, activePromotions) {
  var productPromotionIds = searchHit.discountedPromotionIDs;

  var promotions = new ArrayList();
  activePromotions.forEach(function (promoId) {
    var index = productPromotionIds.indexOf(promoId);
    if (index > -1) {
      promotions.add(PromotionMgr.getPromotion(productPromotionIds[index]));
    }
  });

  return promotions;
}

/**
 * Get list price for a product
 *
 * @param {dw.catalog.ProductPriceModel} priceModel - Product price model
 * @return {dw.value.Money} - List price
 */
function getLP(priceModel) {
  var money = require('dw/value/Money');
  var price = money.NOT_AVAILABLE;
  var priceBook;
  var priceBookPrice;

  if (priceModel.price.valueOrNull === null && priceModel.minPrice) {
    return priceModel.minPrice;
  }

  priceBook = pricingHelper.getRootPriceBook(priceModel.priceInfo.priceBook);
  priceBookPrice = priceModel.getPriceBookPrice(priceBook.ID);

  if (priceBookPrice.available) {
    return priceBookPrice;
  }

  price = priceModel.price.available ? priceModel.price : priceModel.minPrice;

  return price;
}

/**
 * Get list price for a given product
 * @param {dw.catalog.ProductSearchHit} hit - current product returned by Search API.
 * @param {function} getSearchHit - function to find a product using Search API.
 *
 * @returns {Object} - price for a product
 */
function getListPrices(hit, getProdSearchHit) {
  var priceModel = hit.firstRepresentedProduct.getPriceModel();
  if (!priceModel.priceInfo) {
    return {};
  }
  var rootPriceBook = pricingHelper.getRootPriceBook(priceModel.priceInfo.priceBook);
  PriceBookMgr.setApplicablePriceBooks(rootPriceBook);
  var searchHit = getProdSearchHit(hit.product) || hit;

  if (searchHit) {
    if (searchHit.minPrice.available && searchHit.maxPrice.available) {
      return {
        minPrice: searchHit.minPrice,
        maxPrice: searchHit.maxPrice
      };
    }

    return {
      minPrice: hit.minPrice,
      maxPrice: hit.maxPrice
    };
  }

  return {};
}

function convertPriceModel(price) {
  var value = price.available ? price.getDecimalValue().get() : null;
  //  var currency = price.available ? price.getCurrencyCode() : null;
  var formattedPrice = price.available ? formatMoney(price) : null;
  var decimalPrice;

  if (formattedPrice) {
    decimalPrice = price.getDecimalValue().toString();
  }

  return {
    value: value,
    decimalPrice: decimalPrice,
    formatAmount: pricingHelper.formatMoney(formattedPrice)
  };
}

function getFormattedAmount(formattedPrice) {
  var formatAmount = formattedPrice;
  if (request.session.currency.defaultFractionDigits && formattedPrice) {
    // eslint-disable-line
    var decimalValue = formattedPrice.substr(formattedPrice.length - (request.session.currency.defaultFractionDigits - 1) - 1, formattedPrice.length); // eslint-disable-line
    if (parseInt(decimalValue) === 0) {
      // eslint-disable-line
      formatAmount = formattedPrice.substr(0, formattedPrice.length - request.session.currency.defaultFractionDigits - 1); // eslint-disable-line
    }
  }
  return formatAmount;
}

function getPriceModel(product, searchHit) {
  var priceModel = product.priceModel;
  if ((product.master || product.variationGroup) && product.variationModel.variants.length > 0) {
    product = searchHit.firstRepresentedProduct;
    if (product.master && ((!product.priceModel.maxPrice.available && !product.priceModel.minPrice.available) || !product.priceModel.isPriceRange())) {
      priceModel = product.variants[0].priceModel;
    } else {
      priceModel = product.priceModel;
    }
  }
  return priceModel;
}
module.exports = function (object, searchHit, activePromotions, getSearchHit) {
  Object.defineProperty(object, 'price', {
    enumerable: true,
    value: (function () {
      var product = searchHit.product;

      var listPrice = getListPrices(searchHit, getSearchHit);
      pricingHelper.setPriceBooksPrice(getPriceModel(product, searchHit));
      var salePrice = { minPrice: product.priceModel.minPrice, maxPrice: product.priceModel.maxPrice };

      var promotions = getPromotions(searchHit, activePromotions);
      if (promotions.getLength() > 0) {
        var promoProduct = searchHit.firstRepresentedProduct;
        // if the first represented product is not discounted from private sale pricebook,
        // price range display will not be displayed on tile

        var promotionalPrice = pricingHelper.getPromotionPrice(promoProduct, promotions);
        if (promotionalPrice && promotionalPrice.available) {
          salePrice = { minPrice: promotionalPrice, maxPrice: promotionalPrice };
        }
      }

      var listMinPrice = '';
      var listMaxPrice = '';

      // Code changes to show the strike through price range for the list price in different scenarios listed under ticket SFDEV-5974
      // Get the ProductSearchHit instance of the input product first and then get the list price min and max values
      // The similar changes for product details page is handled in price.js file

      if (listPrice.minPrice && listPrice.minPrice.valueOrNull !== null) {
        if (listPrice.minPrice.value !== listPrice.maxPrice.value) {
          listMinPrice = convertPriceModel(listPrice.minPrice);
          listMaxPrice = convertPriceModel(listPrice.maxPrice);
        } else {
          listMinPrice = convertPriceModel(listPrice.minPrice);
        }
      }

      if (salePrice.minPrice.value !== salePrice.maxPrice.value) {
        var rangePrice = new RangePrice(salePrice.minPrice, salePrice.maxPrice);

        if (!empty(listMinPrice)) {
          if (listMinPrice.value == salePrice.minPrice.value && listMaxPrice.value == salePrice.maxPrice.value) {
            //rangePrice.listMinPrice = listMaxPrice;
            return rangePrice;
          }
          rangePrice.listMinPrice = listMinPrice;
          rangePrice.listMaxPrice = listMaxPrice;

          //Changes to show the percentage OFF for the Price range - SFDEV-10177
          var lowerPriceSavings = pricingHelper.calculatePercentOff(salePrice.minPrice, listMinPrice);

          if (!empty(listMaxPrice)) {
            var higherPriceSavings = pricingHelper.calculatePercentOff(salePrice.maxPrice, listMaxPrice);
          } else {
            var higherPriceSavings = pricingHelper.calculatePercentOff(salePrice.maxPrice, listMinPrice);
          }

          if (isNaN(higherPriceSavings.savePercentage)) {
            rangePrice.lowerPriceSavings = lowerPriceSavings;
            rangePrice.higherPriceSavings = '';
          } else if (lowerPriceSavings.savePercentage === higherPriceSavings.savePercentage) {
            rangePrice.lowerPriceSavings = lowerPriceSavings;
            rangePrice.higherPriceSavings = '';
          } else if (lowerPriceSavings.savePercentage < higherPriceSavings.savePercentage) {
            rangePrice.lowerPriceSavings = lowerPriceSavings;
            rangePrice.higherPriceSavings = higherPriceSavings;
          } else {
            rangePrice.lowerPriceSavings = higherPriceSavings;
            rangePrice.higherPriceSavings = lowerPriceSavings;
          }
        }
        return rangePrice;
      } else if (salePrice.minPrice.value === salePrice.maxPrice.value) {
        if (listPrice.minPrice && listPrice.minPrice.valueOrNull !== null) {
          if (listPrice.minPrice.value !== salePrice.minPrice.value && listPrice.minPrice.value !== listPrice.maxPrice.value) {
            var rangeDefaultPrice = new DefaultPrice(salePrice.minPrice, listPrice.minPrice);
            rangeDefaultPrice.listMinPrice = listMinPrice;
            rangeDefaultPrice.listMaxPrice = listMaxPrice;
            rangeDefaultPrice.type = 'rangeDefault';

            //Changes to show the percentage OFF for the Price range - SFDEV-10177
            var lowerPriceSavings = pricingHelper.calculatePercentOff(salePrice.minPrice, listMinPrice);
            var higherPriceSavings = pricingHelper.calculatePercentOff(salePrice.maxPrice, listMaxPrice);
            if (lowerPriceSavings.savePercentage === higherPriceSavings.savePercentage) {
              rangeDefaultPrice.lowerPriceSavings = lowerPriceSavings;
              rangeDefaultPrice.higherPriceSavings = '';
            } else if (lowerPriceSavings.savePercentage < higherPriceSavings.savePercentage) {
              rangeDefaultPrice.lowerPriceSavings = lowerPriceSavings;
              rangeDefaultPrice.higherPriceSavings = higherPriceSavings;
            } else {
              rangeDefaultPrice.lowerPriceSavings = higherPriceSavings;
              rangeDefaultPrice.higherPriceSavings = lowerPriceSavings;
            }

            return rangeDefaultPrice;
          }
        }
      }

      if (listPrice.minPrice && listPrice.minPrice.valueOrNull !== null) {
        if (listPrice.minPrice.value !== salePrice.minPrice.value) {
          return new DefaultPrice(salePrice.minPrice, listPrice.minPrice);
        }
      }
      return new DefaultPrice(salePrice.minPrice);
    })()
  });
};
