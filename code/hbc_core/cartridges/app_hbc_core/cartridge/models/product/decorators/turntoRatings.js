'use strict';

module.exports = function (object, apiProduct) {
  // get master product for rating values
  var masterProduct = apiProduct.master ? apiProduct : apiProduct.variant ? apiProduct.masterProduct : apiProduct;
  Object.defineProperty(object, 'starRating', {
    enumerable: true,
    value:
      Object.prototype.hasOwnProperty.call(masterProduct.custom, 'turntoAverageRating') && masterProduct.custom.turntoAverageRating
        ? masterProduct.custom.turntoAverageRating
        : 0
  });
};
