'use strict';

var base = module.superModule;
var productHelper = require('*/cartridge/scripts/helpers/productHelpers');

base.custom = require('*/cartridge/models/product/decorators/custom');

base.promotionPricing = require('*/cartridge/models/product/decorators/promotionPricing');

// include gwp product custom attributes
base.gwp = require('*/cartridge/models/product/decorators/gwp');

// include product eligibility for instore pick up
base.isAvailableForInstore = require('*/cartridge/models/product/decorators/availableForInStorePickup');

// include product eligibility for reviewable
base.isReviewable = require('*/cartridge/models/product/decorators/isReviewable');

// include product's review count from turnto
base.turntoReviewCount = require('*/cartridge/models/product/decorators/turntoReviewCount');

base.turntoRatings = require('*/cartridge/models/product/decorators/turntoRatings');

base.getSearchableIfUnavailableFlag = function (product, apiProduct) {
  Object.defineProperty(product, 'searchableIfUnavailable', {
    enumerable: true,
    value: productHelper.getSearchableIfUnavailableFlag(apiProduct)
  });
};

base.getHBCProductType = function (product, apiProduct) {
  Object.defineProperty(product, 'hbcProductType', {
    enumerable: true,
    value: productHelper.getHBCProductType(apiProduct)
  });
};

base.alwaysEnableSwatches = function (product, apiProduct) {
  Object.defineProperty(product, 'alwaysEnableSwatches', {
    enumerable: true,
    value: productHelper.alwaysEnableSwatches(apiProduct)
  });
};

// to fetch all products for variations.
base.allAvailableProducts = require('*/cartridge/models/product/decorators/allAvailableProducts');
module.exports = base;
