'use strict';
var Resource = require('dw/web/Resource');
var base = module.superModule;
function readyToOrderMsg(object) {
  var msg = Resource.msg('labe.notavailable', 'product', null);
  var colorSelected;
  var sizeSelected;
  if (object.variationAttributes && object.variationAttributes.length > 0) {
    object.variationAttributes.forEach(function (attr) {
      if (attr.id == 'color' && !attr.selectedAttribute) colorSelected = false;
      if (attr.id == 'size' && !attr.selectedAttribute) sizeSelected = false;
    });
  }
  if (colorSelected == false && sizeSelected == false) {
    msg = Resource.msg('labe.notavailable', 'product', null);
  } else if (sizeSelected == false) {
    msg = Resource.msg('labe.notavailable.size', 'product', null);
  } else if (colorSelected == false) {
    msg = Resource.msg('labe.notavailable.color', 'product', null);
  }
  return msg;
}
module.exports = function (object, variationModel, productType) {
  base.call(this, object, variationModel);
  Object.defineProperty(object, 'readyToOrderMsg', {
    enumerable: true,
    writable: true,
    // eslint-disable-next-line no-nested-ternary
    value: object.readyToOrder
      ? ''
      : productType && productType === 'giftcard'
      ? Resource.msg('labe.giftcard.notavailable', 'product', null)
      : readyToOrderMsg(object)
  });
};
