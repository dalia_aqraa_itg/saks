'use strict';
var base = module.superModule;
var URLUtils = require('dw/web/URLUtils');
var Resource = require('dw/web/Resource');
var PromotionMgr = require('dw/campaign/PromotionMgr');
var collections = require('*/cartridge/scripts/util/collections');
var productHelpers = require('*/cartridge/scripts/helpers/productHelpers');
var preferences = require('*/cartridge/config/preferences');
var isFinalSaleToogle = preferences.product.IS_FINAL_SALE_TOOGLE;

/**
 * Fetch the color for feature type of product
 *
 * @param {string} featureType - feature type of the product
 * @returns {string} color
 */
function getFeaturedTypeColor(featureType) {
  return JSON.parse(preferences.product.edvp)[featureType];
}

function fullLookRecommendation(apiProduct) {
  var recommendations = [];
  var masterProduct = apiProduct.variant ? apiProduct.variationModel.master : apiProduct;

  if (masterProduct.orderableRecommendations) {
    collections.forEach(masterProduct.orderableRecommendations, function (recommendation) {
      if (recommendation) {
        var params = {
          id: recommendation.recommendedItemID
        };
        recommendations.push(params);
      }
    });
  }
  return recommendations;
}

module.exports = function (object, apiProduct, type) {
  Object.defineProperty(object, 'masterProductID', {
    value: apiProduct.variant ? apiProduct.variationModel.master.ID : apiProduct.ID,
    enumerable: true
  });

  // We are modifing property type of brand so, we are creating an brand property with writable true so that we can do our changes in value after base code executed.
  Object.defineProperty(object, 'brand', {
    enumerable: true,
    value: null,
    writable: true
  });
  base.call(this, object, apiProduct, type);
  var brandUrl = '#';
  if (apiProduct && apiProduct.brand) {
    brandUrl = URLUtils.url('Search-Show', 'cgid', 'brand', 'prefn1', 'brand', 'prefv1', apiProduct.brand);
    if (apiProduct.brand.toLowerCase() === 'chanel') {
      brandUrl = URLUtils.url('Search-Show', 'q', apiProduct.brand);
    }
  }
  object.brand = {
    // eslint-disable-line
    name: apiProduct.brand,
    url: brandUrl
  };
  Object.defineProperty(object, 'purchaselimit', {
    enumerable: true,
    value: 'purchaseLimit' in apiProduct.custom ? apiProduct.custom.purchaseLimit : null
  });

  Object.defineProperty(object, 'longDescriptionStyle', {
    enumerable: true,
    value: Resource.msgf('label.details.style', 'product', null, apiProduct.variant ? apiProduct.variationModel.master.ID : apiProduct.ID)
  });

  Object.defineProperty(object, 'DropShipShipping', {
    enumerable: true,
    value: {
      name: 'dropShipShipping' in apiProduct.custom ? apiProduct.custom.dropShipShipping : null,
      displayName: Resource.msg('label.product.dropship', 'product', null),
      dropshipTimeline: 'dropShipShipping' in apiProduct.custom ?
        Resource.msg('label.product.dropshiptype.id'.concat(apiProduct.custom.dropShipShipping), 'product', 'drop_ship_timetable_standard') : null,
      url: URLUtils.url('Page-Show', 'cid', 'dropShipShipping' in apiProduct.custom ? apiProduct.custom.dropShipShipping : null)
    }
  });

  Object.defineProperty(object, 'uspsShipOK', {
    enumerable: true,
    value: productHelpers.getUSPSShipOK(apiProduct)
  });

  Object.defineProperty(object, 'pdRestrictedShipTypeText', {
    enumerable: true,
    value: productHelpers.getPDRestrictedShipTypeText(apiProduct)
  });

  Object.defineProperty(object, 'mRecommendations', {
    enumerable: true,
    writable: true,
    value: fullLookRecommendation(apiProduct)
  });

  Object.defineProperty(object, 'featuredType', {
    enumerable: true,
    value: {
      value: 'featuredType' in apiProduct.custom ? apiProduct.custom.featuredType : null,
      color: 'featuredType' in apiProduct.custom && !!preferences.product.edvp ? getFeaturedTypeColor(apiProduct.custom.featuredType) : ''
    }
  });

  Object.defineProperty(object, 'isNotReturnable', {
    enumerable: true,
    value: {
      value: 'returnable' in apiProduct.custom && apiProduct.custom.returnable ? apiProduct.custom.returnable === 'false' : false,
      color: JSON.parse(preferences.product.BADGE_HEX_COLOR) ? JSON.parse(preferences.product.BADGE_HEX_COLOR).badgeReturnable : null
    }
  });

  Object.defineProperty(object, 'badge', {
    enumerable: true,
    value: {
      isNew: {
        value: 'isNew' in apiProduct.custom ? apiProduct.custom.isNew === 'true' : false,
        color: JSON.parse(preferences.product.BADGE_HEX_COLOR) ? JSON.parse(preferences.product.BADGE_HEX_COLOR).badgeIsNew : null
      },
      isSale: {
        value: 'isSale' in apiProduct.custom ? apiProduct.custom.isSale === 'true' || apiProduct.custom.isSale === true : false,
        color: JSON.parse(preferences.product.BADGE_HEX_COLOR) ? JSON.parse(preferences.product.BADGE_HEX_COLOR).badgeIsSale : null
      },
      isClearance: 'isClearance' in apiProduct.custom ? apiProduct.custom.isClearance === 'true' || apiProduct.custom.isClearance === true : false,
      isFinalSale: 'finalSale' in apiProduct.custom && apiProduct.custom.finalSale && isFinalSaleToogle && isFinalSaleToogle === 'true',
      limitedInvBadgeColor: JSON.parse(preferences.product.BADGE_HEX_COLOR) ? JSON.parse(preferences.product.BADGE_HEX_COLOR).badgeLimitedInventory : null
    }
  });

  Object.defineProperty(object, 'displayQuicklook', {
    enumerable: true,
    value: 'displayQuickLook' in apiProduct.custom ? apiProduct.custom.displayQuickLook : 'false'
  });

  Object.defineProperty(object, 'wishlist', {
    enumerable: true,
    value: 'wishlist' in apiProduct.custom ? apiProduct.custom.wishlist : null
  });

  // the attribute ID has been updated to attribute - sizechartsubtype vs the one in the FSD (sizeChartTemplate)
  // HBC data is being set at sizechartsubtype.
  // Object property remains the same('sizeChartTemplate') so we dont have to update all references
  Object.defineProperty(object, 'sizeChartTemplate', {
    enumerable: true,
    value: 'sizeChartSubType' in apiProduct.custom ? apiProduct.custom.sizeChartSubType : null
  });

  Object.defineProperty(object, 'pdRestrictedWarningText', {
    enumerable: true,
    value: 'pdRestrictedWarningText' in apiProduct.custom && apiProduct.custom.pdRestrictedWarningText === 'true'
  });
  Object.defineProperty(object, 'pdpURL', {
    enumerable: true,
    value: URLUtils.https('Product-Show', 'pid', apiProduct.ID).toString()
  });
};
