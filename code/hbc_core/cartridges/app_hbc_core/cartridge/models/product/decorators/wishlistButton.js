'use strict';

var PRODUCT_SHOW_ACTION = 'Product-Show';

/**
 * Fetch the wishlist button related data
 *
 * @param {dw/catalog/Product} product - Wishlist item product
 * @param {boolean} available - product availability
 * @returns {Object} - button related data object
 */
function buttonCondition(product, available) {
  var URLUtils = require('dw/web/URLUtils');
  let buttonObj = {
    text: 'wishlist.prod.movetocart',
    class: 'add-to-cart',
    type: '',
    href: URLUtils.url(PRODUCT_SHOW_ACTION, 'pid', product.ID),
    producttype: ''
  };
  if (product != null && !!product.custom.hbcProductType && (product.custom.hbcProductType === 'home' || product.custom.hbcProductType === 'bridal')) {
    buttonObj.text = 'wishlist.prod.details';
    buttonObj.class = 'pdp-details';
    buttonObj.producttype = product.custom.hbcProductType;
  } else if (available) {
    buttonObj.text = 'wishlist.prod.movetocart';
  } else {
    buttonObj.text = 'wishlist.prod.soldout';
    buttonObj.class = 'sold-out';
    buttonObj.type = 'disabled';
  }
  return buttonObj;
}

module.exports = function (object, product, availability) {
  let buttonObj = buttonCondition(product, availability);
  Object.defineProperty(object, 'buttonText', {
    enumerable: true,
    value: buttonObj.text
  });
  Object.defineProperty(object, 'buttonClass', {
    enumerable: true,
    value: buttonObj.class
  });
  Object.defineProperty(object, 'buttonType', {
    enumerable: true,
    value: buttonObj.type
  });
  Object.defineProperty(object, 'buttonAction', {
    enumerable: true,
    value: buttonObj.href
  });
  Object.defineProperty(object, 'hbcProductType', {
    enumerable: true,
    value: buttonObj.producttype
  });
};
