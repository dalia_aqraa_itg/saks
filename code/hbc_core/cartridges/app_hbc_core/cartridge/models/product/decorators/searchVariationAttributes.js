'use strict';

var COLOR_ATTRIBUTE_NAME = 'color';
var SIZE_ATTRIBUTE_NAME = 'size';
var SIZE_ATTRIBUTE_NAMES = ['size', 'Size'];
var ACTION_ENDPOINT = 'Product-Variation';
var collections = require('*/cartridge/scripts/util/collections');
var URLUtils = require('dw/web/URLUtils');
var ImageModel = require('*/cartridge/models/product/productImages');
var ArrayList = require('dw/util/ArrayList');
var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
/**
 * Validates varaition attribute in of no_size or no_color
 * @param {Object} selectedProductVariationAttributeValue Vairaion Value Object
 * @returns {boolean} hideAttribute returns the status
 */
function isNoVariant(selectedProductVariationAttributeValue) {
  var hideAttribute = false;
  switch (selectedProductVariationAttributeValue.displayValue.toUpperCase()) {
    case 'NO SIZE':
    case 'NO COLOR':
    case 'NO COLOUR':
      hideAttribute = true;
      break;
  }
  return hideAttribute;
}

/**
 * validates whether the variant is waitlistable
 * @param {String} attrId variation Attribute ID
 * @param {String} attrValue Variation attribute Value
 * @param {Object} variationModel variation attribute Model
 * @returns {boolean} status returns whether the variant is waitlistable
 */
function isWaitListable(attrId, attrValue, variationModel, selectedColor) {
  var HashMap = require('dw/util/HashMap');
  var waitlistable = false;
  var hashMap = new HashMap();
  hashMap.put(attrId, attrValue);
  if (selectedColor) {
    hashMap.put(COLOR_ATTRIBUTE_NAME, selectedColor);
  }
  var productVariants = variationModel.getVariants(hashMap);
  Object.keys(productVariants).forEach(function (key) {
    if (productVariants[key].custom.waitlist && productVariants[key].custom.waitlist == 'true') {
      waitlistable = true;
    }
  });
  return waitlistable;
}

module.exports = function (object, hit, frpid, options) {
  Object.defineProperty(object, 'variationAttributes', {
    enumerable: true,
    writable: true,
    value: (function () {
      var HashMap = require('dw/util/HashMap');
      var product = hit.product;
      var variationModel = options && options.variationModel ? options.variationModel : product.variationModel;
      var selectedColor = null;
      var colorToSelect = null;
      var selectedSize = null;
      var onlysizeSelected = null;
      var onLoadImgUrl = null;
      var onLoadQVUrl = null;
      var variables = null;
      var variantAssociatedID = null;
      var selectedColorAttrID = null;
      var selectedColorValue = null;
      var frpidColor;
      if (frpid && hit.getRepresentedVariationValues(COLOR_ATTRIBUTE_NAME).length) {
        var frpidColorObj = hit.getRepresentedVariationValues(COLOR_ATTRIBUTE_NAME)[0];
        frpidColor = frpidColorObj.value;
        selectedColorAttrID = frpidColorObj.ID;
        selectedColor = 'dwvar_' + hit.productID + '_color=' + frpidColor;
        colorToSelect = frpidColor;
        onLoadImgUrl = URLUtils.url('Product-Show', 'pid', hit.productID, 'dwvar_' + hit.productID + '_color', frpidColor).toString();
        onLoadQVUrl = URLUtils.url('Product-ShowQuickView', 'pid', hit.productID, 'dwvar_' + hit.productID + '_color', frpidColor).toString();
      }
      var colorProductVariationAttribute = variationModel.getProductVariationAttribute(COLOR_ATTRIBUTE_NAME);

      selectedColorValue = variationModel.getSelectedValue(colorProductVariationAttribute);

      if (selectedColorValue) {
        selectedColorAttrID = selectedColorValue.ID;
      }

      // SFSX-4012 Fix size id issue if the id capitalized
      var sizeProductVariationAttribute;
      for (var i = 0; i < SIZE_ATTRIBUTE_NAMES.length; i++) {
        sizeProductVariationAttribute = variationModel.getProductVariationAttribute(SIZE_ATTRIBUTE_NAMES[i]);
        if (sizeProductVariationAttribute) {
          SIZE_ATTRIBUTE_NAME = SIZE_ATTRIBUTE_NAMES[i];
          break;
        }
      }

      var selectedSizeValue = variationModel.getSelectedValue(sizeProductVariationAttribute);
      // If there is no Color variation and only size variation, then update the selected size variation.
      let colors =
        variationModel.productVariationAttributes.length > 0 && colorProductVariationAttribute
          ? variationModel.getAllValues(colorProductVariationAttribute)
          : hit.getRepresentedVariationValues(COLOR_ATTRIBUTE_NAME);
      let sizes =
        variationModel.productVariationAttributes.length > 0 && sizeProductVariationAttribute
          ? variationModel.getAllValues(sizeProductVariationAttribute)
          : hit.getRepresentedVariationValues(SIZE_ATTRIBUTE_NAME);
      if (colors.length <= 1 && !sizes.empty && sizeProductVariationAttribute) {
        var onlysizeSelectable = variationModel.hasOrderableVariants(sizeProductVariationAttribute, sizes[0]);
        if (onlysizeSelectable) {
          onlysizeSelected = sizes[0].value;
        }
      } else if (sizes.length === 1) {
        var sizeSelectable = variationModel.hasOrderableVariants(sizeProductVariationAttribute, sizes[0]);
        if (sizeSelectable) {
          selectedSize = 'dwvar_' + hit.productID + '_size=' + sizes[0].value;
        }
      }
      var tcolors = colors.toArray().filter(function (tcolor) {
        return !isNoVariant(tcolor);
      });
      colors = new ArrayList(tcolors);
      let nosizes = sizes;
      var tsizes = sizes.toArray().filter(function (tsize) {
        return !isNoVariant(tsize);
      });
      sizes = new ArrayList(tsizes);

      if (!empty(frpidColor)) {
        var colorAttrMap = new HashMap();
        colorAttrMap.put(COLOR_ATTRIBUTE_NAME, frpidColor);
        var attVariants = variationModel.getVariants(colorAttrMap);
        if (attVariants.length == 1) {
          var variant = attVariants[0];
          var masterProd = variationModel.master;
          var sfccPreorderVariant = 'sfccPreorder' in variant.custom ? variant.custom.sfccPreorder : variationModel.master && variationModel.master.custom ? variationModel.master.custom.sfccPreorder : '';
          var variantAvailabilityStatusColor = variant.availabilityModel.availabilityStatus;
          var isSelectedSwatchWaitlistable = isWaitListable(COLOR_ATTRIBUTE_NAME, frpidColor, variationModel) || (masterProd && masterProd.custom.waitlist);
          var canPreOrderOrBuy = variantAvailabilityStatusColor == 'IN_STOCK' || (variantAvailabilityStatusColor != 'NOT_AVAILABLE' && sfccPreorderVariant == 'T');
          object.showWaitlistButton = isSelectedSwatchWaitlistable && !canPreOrderOrBuy;
          object.variantAvailabilityStatus = variantAvailabilityStatusColor;
          object.variantToSelect = variant.ID;
        }
      }

      return [
        {
          attributeId: COLOR_ATTRIBUTE_NAME,
          id: COLOR_ATTRIBUTE_NAME,
          displayName: colorProductVariationAttribute.displayName,
          swatchable: true,
          values: collections.map(colors, function (color) {
            // var apiImage = color.getImage('swatch', 0);
            var s7apiImage = new ImageModel(color, {
              types: ['swatch'],
              quantity: '0'
            });
            var apiImage = s7apiImage ? s7apiImage.swatch[0] : '';

            var s7apiMedImage = new ImageModel(color, {
              types: ['medium'],
              quantity: '0'
            });
            var apiMedImage = s7apiMedImage ? s7apiMedImage.medium[0] : null;

            var s7PlpImages = new ImageModel(color, {
              types: ['medium'],
              quantity: 'all'
            });

            var MediumImages = {};

            var plpimglist = new ArrayList(s7PlpImages.medium);

            MediumImages = collections.map(plpimglist, function (image) {
              return {
                alt: image.alt,
                url: image.url.toString(),
                title: image.title
              };
            });
            if (!apiImage) {
              return {};
            }
            var colorAttrMap = new HashMap();
            colorAttrMap.put(COLOR_ATTRIBUTE_NAME, color.value);
            var attVariants = variationModel.getVariants(colorAttrMap);
            var variantToSelect = attVariants && attVariants.length == 1 ? attVariants[0] : null;
            var variantAvailabilityStatus = !empty(variantToSelect) ? variantToSelect.availabilityModel.availabilityStatus : '';
            var sfccPreorder = !empty(variantToSelect) && 'sfccPreorder' in variantToSelect.custom ? variantToSelect.custom.sfccPreorder : variationModel.master && variationModel.master.custom ? variationModel.master.custom.sfccPreorder : '';
            var variant;
            var masterProd = variationModel.master;
            var hasAllInPreOrder = true;
            
            for (var attIndex = 0; attIndex < attVariants.length; attIndex++) {
              variant = attVariants[attIndex];
              var inventoryRecord = variant.availabilityModel.inventoryRecord;
              var tempVariantAvailabilityStatus = variant.availabilityModel.availabilityStatus;
              var sfccPreorderVariant = 'sfccPreorder' in variant.custom ? variant.custom.sfccPreorder : masterProd && masterProd.custom ? masterProd.custom.sfccPreorder : '';
              if (sfccPreorderVariant == 'T') {
                sfccPreorder = sfccPreorderVariant;
              }
              if (((tempVariantAvailabilityStatus == 'BACKORDER' || tempVariantAvailabilityStatus == 'PRE_ORDER') && sfccPreorderVariant != 'T') || tempVariantAvailabilityStatus == 'NOT_AVAILABLE' || tempVariantAvailabilityStatus == 'IN_STOCK') {
                hasAllInPreOrder = false;
              }
              if(inventoryRecord && inventoryRecord.ATS.available){
                variantAvailabilityStatus = tempVariantAvailabilityStatus;
              }
            }
            

            if (attVariants.length > 1) {
              var isSelectedSwatchWaitlistable = isWaitListable(COLOR_ATTRIBUTE_NAME, color.value, variationModel) || (masterProd && masterProd.custom.waitlist);
              var canPreOrderOrBuy = variantAvailabilityStatus == 'IN_STOCK' || (variantAvailabilityStatus != 'NOT_AVAILABLE' && sfccPreorder == 'T');
              object.showWaitlistButton = isSelectedSwatchWaitlistable && !canPreOrderOrBuy;
              object.variantAvailabilityStatus = variantAvailabilityStatus;
              if (empty(object.hasAllInPreOrder) || object.hasAllInPreOrder) {
                object.hasAllInPreOrder = hasAllInPreOrder;
              }
            }

            var selectable = variationModel.hasOrderableVariants(colorProductVariationAttribute, color) && !(variantAvailabilityStatus != 'IN_STOCK' && sfccPreorder != 'T');
            var colorValueUrl;
            colorValueUrl = variationModel.urlSelectVariationValue(ACTION_ENDPOINT, colorProductVariationAttribute, color);
            var colorParams = [];
            colorParams.push('quantity=1');

            if (selectedSize != null) {
              colorParams.push(selectedSize);
            }
            for (var j = 0; j < colorParams.length; j++) {
              var param = colorParams[j];
              if (param) {
                colorValueUrl += (colorValueUrl.indexOf('?') !== -1 ? '&' : '?') + param;
              }
            }

            if (selectable && selectedColor == null) {
              if (selectedColorValue && selectedColorValue.value) {
                selectedColor = 'dwvar_' + hit.productID + '_color=' + selectedColorValue.value;
                onLoadImgUrl = URLUtils.url('Product-Show', 'pid', hit.productID, 'dwvar_' + hit.productID + '_color', selectedColorValue.value).toString();
                onLoadQVUrl = URLUtils.url(
                  'Product-ShowQuickView',
                  'pid',
                  hit.productID,
                  'dwvar_' + hit.productID + '_color',
                  selectedColorValue.value
                ).toString();
              } else {
                selectedColor = 'dwvar_' + hit.productID + '_color=' + color.value;
                onLoadImgUrl = URLUtils.url('Product-Show', 'pid', hit.productID, 'dwvar_' + hit.productID + '_color', color.value).toString();
                onLoadQVUrl = URLUtils.url('Product-ShowQuickView', 'pid', hit.productID, 'dwvar_' + hit.productID + '_color', color.value).toString();
              }
            }
            if (colorToSelect == null) {
              if (selectedColorValue && selectedColorValue.value) {
                colorToSelect = selectedColorValue.value;
              } else {
                colorToSelect = color.value;
              }
            }
            if (selectable && !variables) {
              variables = {};
              variables.color = {
                id: product.ID,
                value: color.ID
              };
              if (selectedSize) {
                variables.size = {
                  id: product.ID,
                  value: sizes.length ? sizes[0].value : nosizes[0].value
                };
              }
              var vm = productHelper.getVariationModel(product, variables);
              if (vm) {
                var variantAssociated = vm.getSelectedVariant(); // eslint-disable-line
                if (variantAssociated) {
                  variantAssociatedID = variantAssociated.ID;
                }
              }
            }
            
            return {
              variantID: variantToSelect ? variantToSelect.ID : '',
              id: color.ID,
              description: color.description,
              displayValue: color.displayValue,
              value: color.value,
              selectable: selectable,
              selected: true,
              variantAvailabilityStatus: variantAvailabilityStatus,
              sfccPreorder: sfccPreorder,
              colorToSelect: colorToSelect,
              images: {
                swatch: [
                  {
                    alt: apiImage.alt,
                    url: apiImage.url.toString(),
                    title: apiImage.title
                  }
                ],
                medium: [
                  {
                    alt: apiMedImage ? apiMedImage.alt : '',
                    url: apiMedImage ? apiMedImage.url.toString() : '',
                    title: apiMedImage ? apiMedImage.title : ''
                  }
                ]
              },
              mediumImages: MediumImages,
              url: URLUtils.url('Product-Show', 'pid', hit.productID, 'dwvar_' + hit.productID + '_color', color.value).toString(),
              valueUrl: colorValueUrl,
              onLoadImgUrl: onLoadImgUrl,
              onLoadQVUrl: onLoadQVUrl,
              waitlist: isWaitListable(COLOR_ATTRIBUTE_NAME, color.value, variationModel),
              qvurl: URLUtils.url('Product-ShowQuickView', 'pid', hit.productID, 'dwvar_' + hit.productID + '_color', color.value).toString()
            };
          }),
          onLoadImgUrl: onLoadImgUrl, // This is required at root level as these variables are used on page load
          onLoadQVUrl: onLoadQVUrl,
          onlyColorVariantID: variantAssociatedID
        },
        {
          attributeId: SIZE_ATTRIBUTE_NAME,
          id: SIZE_ATTRIBUTE_NAME,
          displayName: sizeProductVariationAttribute.displayName,
          swatchable: false,
          values: collections.map(sizes, function (size) {
            var sizeValueUrl;
            sizeValueUrl = variationModel.urlSelectVariationValue(ACTION_ENDPOINT, sizeProductVariationAttribute, size);
            var params = [];
            params.push('quantity=1');
            if (selectedColor != null) {
              params.push(selectedColor);
              variationModel.setSelectedAttributeValue(COLOR_ATTRIBUTE_NAME, selectedColorAttrID);
            }
            for (var i = 0; i < params.length; i++) {
              var param = params[i];
              if (param) {
                sizeValueUrl += (sizeValueUrl.indexOf('?') !== -1 ? '&' : '?') + param;
              }
            }

            if (onlysizeSelected) {
              onLoadImgUrl = URLUtils.url('Product-Show', 'pid', hit.productID, 'dwvar_' + hit.productID + '_size', onlysizeSelected).toString();
              onLoadQVUrl = URLUtils.url('Product-ShowQuickView', 'pid', hit.productID, 'dwvar_' + hit.productID + '_size', onlysizeSelected).toString();
            }
            variationModel.setSelectedAttributeValue(SIZE_ATTRIBUTE_NAME, size.ID);
            var sizeAttrMap = new HashMap();
            sizeAttrMap.put(COLOR_ATTRIBUTE_NAME, selectedColorAttrID);
            sizeAttrMap.put(SIZE_ATTRIBUTE_NAME, size.value);
            var sizeAttVariants = variationModel.getVariants(sizeAttrMap);
            var sizeVariantAvailabilityStatus = sizeAttVariants && sizeAttVariants.length ? sizeAttVariants[0].availabilityModel.availabilityStatus : '';
            var sizeSfccPreorder = variationModel.master && variationModel.master.custom ? variationModel.master.custom.sfccPreorder : '';
            return {
              id: size.ID,
              description: size.description,
              displayValue: size.displayValue,
              value: size.value,
              selectable: variationModel.hasOrderableVariants(sizeProductVariationAttribute, size),
              selected: true,
              variantAvailabilityStatus: sizeVariantAvailabilityStatus,
              sfccPreorder: sizeSfccPreorder,
              url: URLUtils.url('Product-Show', 'pid', hit.productID, 'dwvar_' + hit.productID + '_size', size.value).toString(),
              waitlist: isWaitListable(SIZE_ATTRIBUTE_NAME, size.value, variationModel, colorToSelect),
              valueUrl: sizeValueUrl
            };
          }),
          onLoadImgUrl: onLoadImgUrl,
          onLoadQVUrl: onLoadQVUrl
        }
      ];
    })()
  });
};
