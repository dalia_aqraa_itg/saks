// Add GWP product custom attribute in this decorator
'use strict';
module.exports = function (object, apiProduct) {
  Object.defineProperty(object, 'gwpButtonCopy', {
    enumerable: true,
    value: 'gwpButtonCopy' in apiProduct.custom ? apiProduct.custom.gwpButtonCopy : null
  });
  Object.defineProperty(object, 'gwpLink', {
    enumerable: true,
    value: 'gwpLink' in apiProduct.custom ? apiProduct.custom.gwpLink : null
  });
};
