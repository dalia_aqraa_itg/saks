'use strict';

var shoprunner = require('*/cartridge/models/product/decorators/shoprunner');
var preferences = require('*/cartridge/config/preferences');

var base = require('app_storefront_base/cartridge/models/product/bonusProduct');
var decorators = require('*/cartridge/models/product/decorators/index');
/**
 * Decorate product with bonus product information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {Object} options - Options passed in from the factory
 * @property {dw.catalog.ProductVarationModel} options.variationModel - Variation model returned by the API
 * @property {Object} options.options - Options provided on the query string
 * @property {dw.catalog.ProductOptionModel} options.optionModel - Options model returned by the API
 * @property {dw.util.Collection} options.promotions - Active promotions for a given product
 * @property {number} options.quantity - Current selected quantity
 * @property {Object} options.variables - Variables passed in on the query string
 *
 * @returns {Object} - Decorated product model
 */
function bonusProduct(product, apiProduct, options) {
  base.call(this, product, apiProduct, options);
  decorators.description(product, apiProduct);
  if (preferences.shopRunnerEnabled) {
    shoprunner(product, apiProduct);
  }
  return product;
}

module.exports = bonusProduct;
