'use strict';

var decorators = require('*/cartridge/models/product/decorators/index');
var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
var promotionCache = require('*/cartridge/scripts/util/promotionCache');
var ProductSearchModel = require('dw/catalog/ProductSearchModel');
var PromotionMgr = require('dw/campaign/PromotionMgr');

/**
 * Get product search hit for a given product
 * @param {dw.catalog.Product} apiProduct - Product instance returned from the API
 * @returns {dw.catalog.ProductSearchHit} - product search hit for a given product
 */
function getProductSearchHit(apiProduct) {
  var searchModel = new ProductSearchModel();
  searchModel.setSearchPhrase(apiProduct.ID);
  searchModel.setOrderableProductsOnly(false);
  searchModel.search();

  if (searchModel.count === 0) {
    searchModel.setSearchPhrase(apiProduct.ID.replace(/-/g, ' '));
    searchModel.search();
  }

  var hit = searchModel.getProductSearchHit(apiProduct);
  var tempHit;
  if (!hit && searchModel.getProductSearchHits().hasNext()) {
    tempHit = searchModel.getProductSearchHits().next();
    hit = tempHit;
  }
  return hit;
}

/**
 * Identify the default variant of the Master product. If no default get the first variant.
 * @param {dw.catalog.Product} product - Product information returned by the script API
 * @returns {string} defaultVariantId - the default variant id for the product
 */
function getDefaultVariant(product) {
  var defaultVariant;
  if (product.master) {
    if (product.variationModel.defaultVariant) {
      defaultVariant = product.variationModel.defaultVariant;
    } else {
      defaultVariant = product.variationModel.variants[0];
    }
  } else {
    defaultVariant = product;
  }
  return defaultVariant;
}

/**
 * Decorate product with product tile information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {string} productType - Product type information
 *
 * @returns {Object} - Decorated product model
 */
module.exports = function (product, apiProduct, productType) {
  var options = productHelper.getConfig(apiProduct, {
    options: null,
    quantity: '1'
  });
  apiProduct = options.apiProduct ? options.apiProduct : apiProduct;
  var productSearchHit = getProductSearchHit(apiProduct);
  decorators.base(product, apiProduct, productType);
  var firstPromotionID = null;

  if (promotionCache.promotions && promotionCache.promotions.length) {
    firstPromotionID = promotionCache.promotions[0];
  }
  var firstPromotion = null;

  firstPromotion = PromotionMgr.activeCustomerPromotions.getProductPromotions(apiProduct);
  if (firstPromotion && firstPromotion.length > 0) {
    product.plpPromos = firstPromotion[0];
  }
  decorators.searchPrice(product, productSearchHit, promotionCache.promotions, getProductSearchHit);
  decorators.ratings(product);
  if (productType === 'set') {
    decorators.setProductsCollection(product, apiProduct);
  }

  if (productType !== 'set') {
    decorators.searchVariationAttributes(product, productSearchHit);
  }
  decorators.images(product, apiProduct, {
    types: ['medium'],
    quantity: 'all'
  });
  decorators.getHBCProductType(product, apiProduct);
  decorators.custom(product, apiProduct, true);
  /* get the variationModel to check the number of variants available
   * If only one variant is available for a master, then swap the master ID with variant ID so the
   * product can be added to cart directly
   */
  var variationModel = apiProduct.variationModel;
  var variants = variationModel.getVariants();
  var hasOnlyOneVariant = variants.length === 1;
  var apiProductForPromo = getDefaultVariant(apiProduct);
  product.variantID = product.id; // eslint-disable-line
  product.hasOnlyOneVariant = hasOnlyOneVariant; // eslint-disable-line
  if (hasOnlyOneVariant) {
    apiProductForPromo = variants.iterator().next();
    product.variantID = apiProductForPromo.ID; // eslint-disable-line
  }
  if (product.variationAttributes[0] && product.variationAttributes[0].onlyColorVariantID) {
    product.variantID = product.variationAttributes[0].onlyColorVariantID; // eslint-disable-line
    product.hasOnlyColor = true; // eslint-disable-line
  }
  if (product.variationAttributes.length > 1 && product.variationAttributes[1] && product.variationAttributes[1].values.length === 1) {
    product.hasOneSize = true;
  }
  decorators.promotionPricing(product, apiProductForPromo);
  decorators.readyToOrder(product, variationModel);
  decorators.availability(product, 1, apiProduct.minOrderQuantity.value, apiProduct.availabilityModel, apiProduct);

  /*
   * Check if the product is entirely OOS
   */
  product.soldout = false; // eslint-disable-line
  if (apiProduct.availabilityModel && !apiProduct.availabilityModel.availability) {
    product.soldout = true; // eslint-disable-line
  }
  return product;
};
