'use strict';
var decorators = require('*/cartridge/models/product/decorators/index');
var shoprunner = require('*/cartridge/models/product/decorators/shoprunner');
var preferences = require('*/cartridge/config/preferences');
var ratingsDecorator = require('*/cartridge/models/product/decorators/ratingsturnto');
/**
 * Identify the default variant of the Master product. If no default get the first variant.
 * @param {dw.catalog.Product} product - Product information returned by the script API
 * @returns {string} defaultVariantId - the default variant id for the product
 */
function getDefaultVariant(product) {
  var defaultVariant;
  if (product.master) {
    if (product.variationModel.defaultVariant) {
      defaultVariant = product.variationModel.defaultVariant;
    } else if (product.variationModel && product.variationModel.variants.length > 0) {
      defaultVariant = product.variationModel.variants[0];
    } else {
      defaultVariant = product;
    }
  } else {
    defaultVariant = product;
  }
  return defaultVariant;
}

module.exports = function fullProduct(product, apiProduct, options) {
  decorators.base(product, apiProduct, options.productType);
  decorators.price(product, apiProduct, options.promotions, false, options.optionModel);

  if (options.variationModel) {
    decorators.images(product, options.variationModel, { types: ['large', 'medium', 'small'], quantity: 'all' });
  } else {
    decorators.images(product, apiProduct, { types: ['large', 'medium', 'small'], quantity: 'all' });
  }

  decorators.quantity(product, apiProduct, options.quantity);
  decorators.variationAttributes(product, options.variationModel, {
    attributes: '*',
    endPoint: 'Variation'
  });
  decorators.description(product, apiProduct);
  decorators.ratings(product);
  decorators.promotions(product, options.promotions);
  decorators.attributes(product, apiProduct.attributeModel);
  decorators.availability(product, options.quantity, apiProduct.minOrderQuantity.value, apiProduct.availabilityModel, apiProduct);
  decorators.options(product, options.optionModel, options.variables, options.quantity);
  decorators.quantitySelector(product, apiProduct.stepQuantity.value, options.variables, options.options);

  var category = apiProduct.getPrimaryCategory();
  if (!category && options.productType !== 'master') {
    category = !apiProduct.isProduct() ? apiProduct.getMasterProduct().getPrimaryCategory() : null;
  }

  if (category) {
    decorators.sizeChart(product, category.custom.sizeChartID);
  }

  decorators.currentUrl(product, options.variationModel, options.optionModel, 'Product-Show', apiProduct.ID, options.quantity);
  decorators.readyToOrder(product, options.variationModel);
  decorators.online(product, apiProduct);
  decorators.raw(product, apiProduct);
  decorators.pageMetaData(product, apiProduct);
  decorators.template(product, apiProduct);
  if (options.variationModel) {
    decorators.images(product, options.variationModel, {
      types: ['large', 'medium', 'small', 'hi-res', 'swatch', 'video'],
      quantity: 'all'
    });
  } else {
    decorators.images(product, apiProduct, {
      types: ['large', 'medium', 'small', 'hi-res', 'swatch', 'video'],
      quantity: 'all'
    });
  }
  decorators.getSearchableIfUnavailableFlag(product, apiProduct);
  decorators.getHBCProductType(product, apiProduct);
  decorators.custom(product, apiProduct, false, true); // ADD FOURTH PARAM TO CALL ESTIMATEDEARNS IN MODEL
  decorators.isAvailableForInstore(product, apiProduct, options.variationModel);
  decorators.isReviewable(product, apiProduct);
  decorators.turntoReviewCount(product, apiProduct);
  var apiProductForPromo = getDefaultVariant(apiProduct);
  decorators.promotionPricing(product, apiProductForPromo);
  decorators.gwp(product, apiProduct);
  if (preferences.shopRunnerEnabled) {
    shoprunner(product, apiProduct);
  }
  // Update ready to order messgae
  decorators.readyToOrder(product, options.variationModel, product.hbcProductType);
  decorators.allAvailableProducts(product, apiProduct);
  ratingsDecorator(product, apiProduct);
  return product;
};
