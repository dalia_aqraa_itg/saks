'use strict';

var decorators = require('*/cartridge/models/product/decorators/index');
var promotionCache = require('*/cartridge/scripts/util/promotionCache');
var ProductSearchModel = require('dw/catalog/ProductSearchModel');

/**
 * Get product search hit for a given product
 * @param {dw.catalog.Product} apiProduct - Product instance returned from the API
 * @returns {dw.catalog.ProductSearchHit} - product search hit for a given product
 */
function getProductSearchHit(apiProduct) {
  var searchModel = new ProductSearchModel();
  searchModel.setSearchPhrase(apiProduct.ID);
  searchModel.setOrderableProductsOnly(false);
  searchModel.search();

  if (searchModel.count === 0) {
    searchModel.setSearchPhrase(apiProduct.ID.replace(/-/g, ' '));
    searchModel.search();
  }

  var hit = searchModel.getProductSearchHit(apiProduct);
  var tempHit;
  if (!hit && searchModel.getProductSearchHits().hasNext()) {
    tempHit = searchModel.getProductSearchHits().next();
    hit = tempHit;
  }
  return hit;
}

/**
 * Decorate product with Add/Remove From Wishlist product tile information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {string} productType - Product type information
 *
 * @returns {Object} - Decorated product model
 */
module.exports = function (product, apiProduct, productType) {
  var productSearchHit = getProductSearchHit(apiProduct);

  decorators.base(product, apiProduct, productType);

  decorators.searchPrice(product, productSearchHit, promotionCache.promotions, getProductSearchHit);

  return product;
};
