'use strict';

/**
 * Get availability for in store pickup
 * @param {dw.order.LineItemCtnr} order - LineItemCntr to get the value from
 * @returns {Object} instorePickPersonData - object containing personInfoMarkFor
 */
function instorePickPerson(order) {
  var instorePickPersonData;
  if (order && order.custom.hasOwnProperty('personInfoMarkFor') && order.custom.personInfoMarkFor) {
    // eslint-disable-line
    try {
      JSON.parse(order.custom.personInfoMarkFor);
      instorePickPersonData = order.custom.personInfoMarkFor;
    } catch (e) {
      instorePickPersonData = null;
    }
  }
  return instorePickPersonData;
}

module.exports = function (object, order) {
  Object.defineProperty(object, 'personInfoMarkFor', {
    enumerable: true,
    value: instorePickPerson(order)
  });
};
