'use strict';
var PriceBookMgr = require('dw/catalog/PriceBookMgr');
var StoreMgr = require('dw/catalog/StoreMgr');
var URLUtils = require('dw/web/URLUtils');
var preferences = require('*/cartridge/config/preferences');

/**
 * Executes a custom search with the store id
 * @param {dw.catalog.ProductSearchModel} apiProductsearch - product search model
 * @param {Object} params - request params
 * @returns {Object} - containing a boolena attribute and search model
 */
function search(apiProductsearch, params) {
  var bopisEnabled = preferences.isBopisEnabled;
  var isRefinedByStore = false;
  var store = null;
  var storePriceBook = null;
  var priceBoodId = '';
  PriceBookMgr.setApplicablePriceBooks(null);
  // get defalt store from session or with geo location
  let storeID = params.storeid;
  if (bopisEnabled && storeID) {
    store = StoreMgr.getStore(storeID);
    priceBoodId = storeID;
    if (store) {
      // set store price book to filter the products available for a store
      storePriceBook = PriceBookMgr.getPriceBook(priceBoodId);
      if (storePriceBook) {
        PriceBookMgr.setApplicablePriceBooks(storePriceBook);
        // TO-FIX : displays the price set in the filter bar
        apiProductsearch.setPriceMin(0);
        isRefinedByStore = true;
      }
    }
  }
  // PSM search
  apiProductsearch.search();
  // set back the price to default to avoid price overriding
  PriceBookMgr.setApplicablePriceBooks(null);

  return {
    isRefinedByStore: isRefinedByStore,
    apiProductsearch: apiProductsearch
  };
}

/**
 * Adds store refinement to url
 * @param {Object} req - request object
 * @param {dw.web.URLUtils} actionUrl - request url
 * @returns {dw.web.URLUtils} - store added url
 */
function getStoreRefinementUrl(req, actionUrl) {
  var storeRefineUrl = URLUtils.url(actionUrl);
  var whitelistedParams = ['q', 'cgid', 'pmin', 'pmax', 'srule', 'storeid'];

  Object.keys(req.querystring).forEach(function (element) {
    if (whitelistedParams.indexOf(element) > -1) {
      storeRefineUrl.append(element, req.querystring[element]);
    }

    if (element === 'preferences') {
      var i = 1;
      Object.keys(req.querystring[element]).forEach(function (preference) {
        storeRefineUrl.append('prefn' + i, preference);
        storeRefineUrl.append('prefv' + i, req.querystring[element][preference]);
        i++;
      });
    }
  });

  return storeRefineUrl;
}

module.exports = {
  search: search,
  getStoreRefinementUrl: getStoreRefinementUrl
};
