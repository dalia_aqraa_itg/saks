'use strict';
var base = module.superModule;
var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
var collections = require('*/cartridge/scripts/util/collections');
var Resource = require('dw/web/Resource');
var preferences = require('*/cartridge/config/preferences');

/**
 * Get the count of coupons applied to a basket
 * @param {dw.order.Basket} basket - current basket
 * @returns {number} couponCount
 */
function getTotalAppliedCoupons(basket) {
  var couponCount = 0;
  if (!empty(basket)) {
    collections.forEach(basket.couponLineItems, function (couponLineItem) {
      if (couponLineItem.applied) {
        ++couponCount;
      }
    });
  }
  return couponCount;
}

/**
 * @constructor
 * @classdesc CartModel class that represents the current basket
 * @param {dw.order.Basket} basket - Current users's basket
 */
function CartModel(basket, displayHtml) {
  var preferences = require('*/cartridge/config/preferences');
  var BasketMgr = require('dw/order/BasketMgr');
  basket = BasketMgr.getCurrentBasket();
  base.call(this, basket);

  // Since basket might be null for registered customer also we cannot fetch for customer from basket object
  var URLUtils = require('dw/web/URLUtils');
  var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
  var displayHtml = displayHtml === false ? false : true;

  this.isL4D8B6 =
    basket &&
    basket.customer &&
    basket.customer.authenticated &&
    basket.customer.addressBook &&
    basket.customer.addressBook.preferredAddress &&
    basket.customer.addressBook.preferredAddress.postalCode === 'L4D 8N6';

  var totalAppliedCoupons = getTotalAppliedCoupons(basket);
  this.totalAppliedCoupons = totalAppliedCoupons;
  this.noCouponMsg = Resource.msg('label.promo.code', 'cart', null);
  this.singleCouponMsg = Resource.msg('label.promo.code.applied', 'cart', null);
  this.multipleCouponMsg = Resource.msg('label.promo.codes.applied', 'cart', null);
  this.shopRunnerEnabled = require('*/cartridge/scripts/helpers/shopRunnerHelpers').checkSRExpressCheckoutEligibility(basket);
  this.isAllProductsSREligible = require('*/cartridge/scripts/helpers/shopRunnerHelpers').checkSRCheckoutEligibilityForCart(basket);
  this.showExpressCheckoutButtonOnCart = preferences.PP_ShowExpressCheckoutButtonOnCart;
  var gwp = {
    isMultipleChoiceExists: cartHelper.isMultiChoiceBonusProduct(basket),
    buttonName: cartHelper.provideButtonName(basket)
  };
  this.gwp = gwp;
  this.isGuestUser = !customer.authenticated;
  
  if (displayHtml || !preferences.CartBEPerformanceChangesEnable) {
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
    this.wlItemsHTML = renderTemplateHelper.getRenderedHtml(this, 'wishlist/cartWishlistLanding');
    this.shoprunnerExpressHTML = renderTemplateHelper.getRenderedHtml(this, 'cart/shoprunnerExpress');
  }

  if (!this.numItems || this.numItems === 0) {
    var emptyCart = {
      Redirecturl: URLUtils.url('Home-Show')
    };
    if (!preferences.CartBEPerformanceChangesEnable) {
      this.emptyCartHtml = renderTemplateHelper.getRenderedHtml(this, 'cart/emptyCart');
      this.itemsHTML = renderTemplateHelper.getRenderedHtml(this, 'cart/emptyCart');
    }
    this.emptyCart = emptyCart;
    // this.saveForLaterItems = [];
  } else {
    // Non empty basket
    this.items = cartHelper.sortLineItems(this.items);
    this.storeModalUrl = URLUtils.url('Stores-InitSearch', 'source', 'cart').toString();
    this.toggleShitoUrl = URLUtils.url('Cart-ToggleShippingOption').toString();
    if (!preferences.CartBEPerformanceChangesEnable) {
      this.itemsHTML = renderTemplateHelper.getRenderedHtml(this, 'cart/cartItems');
      this.minicart = renderTemplateHelper.getRenderedHtml(this, 'checkout/cart/miniCart');
    }
  }
}

module.exports = CartModel;
