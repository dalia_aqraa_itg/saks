'use strict';

/**
 * @constructor
 * @classdesc Build JSON request to send prefill data
 * @param {json} args - arguments
 */
function getRequestJSON(args) {
  return { partnerAppRequestId: args };
}

function getRequestParamString(args) {
  return 'partnerAppRequestId=' + args.toString();
}

module.exports = {
  getRequestJSON: getRequestJSON,
  getRequestParamString: getRequestParamString
};
