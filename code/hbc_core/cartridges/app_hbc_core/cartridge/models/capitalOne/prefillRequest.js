'use strict';

/**
 * @constructor
 * @classdesc Build JSON request to send prefill data
 * @param {json} args - arguments
 */
function getRequestJSON(args) {
  var profile = args;

  return {
    partnerAppRequestId: args.pid || '',
    firstName: profile ? profile.firstName : '',
    lastName: profile ? profile.lastName : '',
    email: profile ? profile.email : '',
    phoneNumber: profile ? profile.phoneMobile : '',
    phoneNumberType: 'mobile',
    addressDetails: {
      postalCode: args.zipCode ? args.zipCode : ''
    },
    customerId: profile ? profile.customerNo : ''
  };
}

module.exports = {
  getRequestJSON: getRequestJSON
};
