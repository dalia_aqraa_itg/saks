'use strict';

var base = module.superModule;

/**
 * Represents content model
 * @param  {dw.content.Content} contentValue - result of ContentMgr.getContent call
 * @param  {string} renderingTemplate - rendering template for the given content
 * @return {void}
 * @constructor
 */
function content(contentValue, renderingTemplate) {
  base.call(this, contentValue, renderingTemplate);
  this.mobileBody = 'mobileBody' in contentValue.custom && contentValue.custom.mobileBody ? contentValue.custom.mobileBody : null;
  this.contentInnavscene7urllink =
    'contentInnavscene7urllink' in contentValue.custom && contentValue.custom.contentInnavscene7urllink ? contentValue.custom.contentInnavscene7urllink : null;
  this.contentInnavurllink =
    'contentInnavurllink' in contentValue.custom && contentValue.custom.contentInnavurllink ? contentValue.custom.contentInnavurllink.markup : null;
  this.navtext = 'navtext' in contentValue.custom && contentValue.custom.navtext ? contentValue.custom.navtext : null;
  this.navlink = 'navlink' in contentValue.custom && contentValue.custom.navlink ? contentValue.custom.navlink : null;
  this.loadRecaptcha =
    'includeGoogleRecaptcha' in contentValue.custom && contentValue.custom.includeGoogleRecaptcha ? contentValue.custom.includeGoogleRecaptcha : false;
  return this;
}

module.exports = content;
