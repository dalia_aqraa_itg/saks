'use strict';

/**
 * @constructor
 * @classdesc The stores model
 * @param {dw.catalog.Store} storeObject - a Store objects
 */

//* PROTOTYPE OVERRIDE*//

/**
 * @param {Object} storeObject - store model
 * To include extra custom store attributes
 */
function store(storeObject) {
  if (storeObject) {
    this.ID = storeObject.custom.hasOwnProperty('store-id') && storeObject.custom['store-id'] ? storeObject.custom['store-id'] : storeObject.ID; // eslint-disable-line
    this.name = storeObject.name;
    this.address1 = storeObject.address1;
    this.address2 = storeObject.address2;
    this.city = storeObject.city;
    this.postalCode =
      storeObject.custom.hasOwnProperty('postal-code') && storeObject.custom['postal-code'] ? storeObject.custom['postal-code'] : storeObject.postalCode; // eslint-disable-line
    this.latitude = storeObject.latitude;
    this.longitude = storeObject.longitude;

    if (storeObject.phone) {
      this.phone = storeObject.phone;
    }

    if (storeObject.countryCode) {
      this.countryCode = storeObject.countryCode.value;
    }

    if (storeObject.stateCode) {
      this.stateCode = storeObject.stateCode;
    }

    if (storeObject.custom.hasOwnProperty('store-close-hours') && storeObject.custom['store-close-hours']) {
      // eslint-disable-line
      this.storeCloseHours = storeObject.custom['store-close-hours'].substring(0, 5);
    }

    if (storeObject.custom.hasOwnProperty('store-open-hours') && storeObject.custom['store-open-hours']) {
      // eslint-disable-line
      this.storeOpenHours = storeObject.custom['store-open-hours'].substring(0, 5);
    }

    if (storeObject.storeHours) {
      this.storeHours = storeObject.storeHours.markup;
    }

    if (storeObject.custom.hasOwnProperty('store-closed') && storeObject.custom['store-closed']) {
      // eslint-disable-line
      this.storeClosed = storeObject.custom['store-closed'];
    }
  }
}

module.exports = store;
