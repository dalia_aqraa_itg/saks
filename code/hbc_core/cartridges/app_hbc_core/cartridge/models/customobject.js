'use strict';
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');

/**
 * Since this method is called from queryAll cannot use this keyword
 * @param {string} primaryKey unqiue key
 * @param {string} objectType custom object type
 * @returns {Object} returns record or null
 */
function queryOne(primaryKey, objectType) {
  var tObjectType = objectType !== null ? objectType : this.objectType;
  return primaryKey ? CustomObjectMgr.getCustomObject(tObjectType, primaryKey) : null;
}

/**
 * update values in custom object
 * @param {Object} map values to be saved
 * @param {Object} customObject custom object
 * @returns {boolean} returns its status
 */
function updateValue(map, customObject) {
  var keySet = map && map.keySet() ? map.keySet().iterator() : null;
  while (keySet && keySet.hasNext()) {
    var key = keySet.next();
    var value = map.get(key);
    customObject.custom[key] = value; // eslint-disable-line
  }
  return true;
}

/**
 * Save data into custom object
 * @param {string} primaryKey unique key for a record
 * @param {Object} map custom attributes
 * @returns {boolean} status of transaction
 */
function save(primaryKey, map) {
  // Since we need to call inside Transaction also declaring and assing on first line
  var objectType = this.objectType;
  var isAlreadyExists = queryOne(primaryKey, objectType);
  if (!isAlreadyExists && objectType && map && map.length > 0) {
    return Transaction.wrap(function () {
      var customObject = CustomObjectMgr.createCustomObject(objectType, primaryKey);
      return updateValue(map, customObject);
    });
  }
  return false;
}

/**
 * Query records from custom object
 * @param {Object} map conditional case for query
 * @param {string} sorting sorting fields with direction can be null also
 * @returns {Object} provides all records matching condition
 */
function queryAll(map, sorting) {
  if (this.objectType) {
    return map && map.length > 0 ? CustomObjectMgr.queryCustomObjects(this.objectType, map, sorting) : CustomObjectMgr.getAllCustomObjects(this.objectType);
  }
  return null;
}

/**
 * Query using custom query
 * @param {string} queryString Query String
 * @param {string} sortString sorting field with direction can be null also
 * @param {string} args sets value for prepared statements.
 * @returns {Object} returns records
 */
function queryCustom(queryString, sortString, args) {
  return CustomObjectMgr.queryCustomObjects(this.objectType, queryString, sortString, args);
}

/**
 * Delete custom objecy
 * @param {Object} customObject Custom object to be deleted
 * @returns {boolean} status
 */
function deleteCustom(customObject) {
  return Transaction.wrap(function () {
    CustomObjectMgr.remove(customObject);
  });
}

/**
 * Update custom object
 * @param {Object} map datas to be saved
 * @param {Object} co custom object
 * @returns {boolean} return the status
 */
function updateCustom(map, co) {
  return Transaction.wrap(function () {
    return updateValue(map, co);
  });
}

/**
 * Generic DAO layer for custom object
 * @param {string} objectType custom object data type
 * @constructor
 */
function CustomObject(objectType) {
  this.objectType = objectType;
  this.save = save;
  this.queryOne = queryOne;
  this.queryAll = queryAll;
  this.queryCustom = queryCustom;
  this.deleteCustom = deleteCustom;
  this.updateCustom = updateCustom;
}

module.exports = CustomObject;
