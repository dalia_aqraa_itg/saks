'use strict';

var base = module.superModule;

var PaymentMgr = require('dw/order/PaymentMgr');
var PaymentInstrument = require('dw/order/PaymentInstrument');
var collections = require('*/cartridge/scripts/util/collections');
var Resource = require('dw/web/Resource');
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');
var tsysHelper = require('*/cartridge/scripts/helpers/tsysHelpers');
var TCCHelpers = require('*/cartridge/scripts/checkout/TCCHelpers');

/**
 * Creates an array of objects containing applicable payment methods
 * @param {dw.util.ArrayList<dw.order.dw.order.PaymentMethod>} paymentMethods - An ArrayList of
 *      applicable payment methods that the user could use for the current basket.
 * @returns {Array} of object that contain information about the applicable payment methods for the
 *      current cart
 */
function applicablePaymentMethods(paymentMethods) {
  return collections.map(paymentMethods, function (method) {
    return {
      ID: method.ID,
      name: method.name
    };
  });
}

/**
 * Creates an array of objects containing applicable credit cards
 * @param {dw.util.Collection<dw.order.PaymentCard>} paymentCards - An ArrayList of applicable
 *      payment cards that the user could use for the current basket.
 * @returns {Array} Array of objects that contain information about applicable payment cards for
 *      current basket.
 */
function applicablePaymentCards(paymentCards) {
  return collections.map(paymentCards, function (card) {
    return {
      cardType: card.cardType,
      name: card.name
    };
  });
}

/**
 * Creates an array of objects containing selected payment information
 * @param {dw.util.ArrayList<dw.order.PaymentInstrument>} selectedPaymentInstruments - ArrayList
 *      of payment instruments that the user is using to pay for the current basket
 * @returns {Array} Array of objects that contain information about the selected payment instruments
 */
function getSelectedPaymentInstruments(selectedPaymentInstruments, currentBasket) {
  var Resource = require('dw/web/Resource');
  return collections.map(selectedPaymentInstruments, function (paymentInstrument) {
    var results = {
      paymentMethod: paymentInstrument.paymentMethod,
      amount: paymentInstrument.paymentTransaction.amount.value
    };
    if (paymentInstrument.paymentMethod === 'CREDIT_CARD') {
      var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
      var nonGCTotal = COHelpers.getNonGiftCardAmount(currentBasket, null);

      results.lastFour = paymentInstrument.creditCardNumberLastDigits;
      results.owner = paymentInstrument.creditCardHolder;
      results.expirationYear = paymentInstrument.creditCardExpirationYear;
      results.type = paymentInstrument.creditCardType;
      results.name = PaymentMgr.getPaymentCard(paymentInstrument.creditCardType) && PaymentMgr.getPaymentCard(paymentInstrument.creditCardType).getName()
        ? PaymentMgr.getPaymentCard(paymentInstrument.creditCardType).getName()
        : paymentInstrument.creditCardType;
      results.maskedCreditCardNumber = paymentInstrument.maskedCreditCardNumber;
      var ccLength = paymentInstrument.maskedCreditCardNumber ? paymentInstrument.maskedCreditCardNumber.toString().length : 0;
      results.shortMaskedNumber = paymentInstrument.maskedCreditCardNumber
        ? paymentInstrument.maskedCreditCardNumber.toString().substr(ccLength - 8, ccLength)
        : '';
      results.expirationMonth = paymentInstrument.creditCardExpirationMonth;

      if ('tccExpirationDate' in paymentInstrument.custom && paymentInstrument.custom.tccExpirationDate) {
        var tccExpirationDate = new Date(paymentInstrument.custom.tccExpirationDate);

        results.isTCC = true;
        results.tccExpirationDate = StringUtils.formatCalendar(new Calendar(tccExpirationDate), 'MM/dd/yyyy');

        if (tsysHelper.isTsysMode()) {
          results.name = TCCHelpers.getTCCTokenLabel(paymentInstrument.creditCardToken);
        }
      }

      results.paywithPoints = null;
      if (nonGCTotal.remainingAmount.value > 0 && paymentInstrument && paymentInstrument.custom && 'paywithPointsResponse' in paymentInstrument.custom) {
        try {
          var Money = require('dw/value/Money');
          var formatMoney = require('dw/util/StringUtils').formatMoney;
          var paypointsObj = JSON.parse(paymentInstrument.custom.paywithPointsResponse);
          results.paywithPoints = {};
          results.paywithPoints.convertionRate = paypointsObj.rewards.conversion_rate;
          results.paywithPoints.pointsBalance = paypointsObj.rewards.balance;
          var amountAvailable = new Money(
            Math.floor(Number(paypointsObj.rewards.balance) * Number(paypointsObj.rewards.conversion_rate) * 100) / 100,
            currentBasket.getCurrencyCode()
          );
          var maxapplicableOrderAmount = nonGCTotal.remainingAmount <= amountAvailable.value ? nonGCTotal.remainingAmount : amountAvailable;
          var maxPointsRedeem = Math.round(maxapplicableOrderAmount.value / paypointsObj.rewards.conversion_rate);
          results.paywithPoints.amountAvailable = formatMoney(amountAvailable);
          results.paywithPoints.maxAmountToRedeem = formatMoney(maxapplicableOrderAmount);
          results.paywithPoints.maxPointsBalance =
            maxPointsRedeem > Number(paypointsObj.rewards.balance) ? Number(paypointsObj.rewards.balance) : maxPointsRedeem;
          results.paywithPoints.resource = {};
          results.paywithPoints.resource.pointsBalance = Resource.msgf(
            'amex.membership.points.details',
            'amexpay',
            null,
            results.paywithPoints.maxAmountToRedeem,
            maxPointsRedeem,
            currentBasket.totalGrossPrice.value
          );
          results.paywithPoints.resource.pointsLabel = Resource.msgf('amex.membership.points', 'amexpay', null, maxPointsRedeem);
          results.paywithPoints.resource.pointsSummary = '';
          if (paymentInstrument && paymentInstrument.custom && 'amexAmountApplied' in paymentInstrument.custom) {
            results.paywithPoints.appliedAmount = formatMoney(new Money(paymentInstrument.custom.amexAmountApplied, currentBasket.getCurrencyCode()));
            results.paywithPoints.appliedPoints =
              Math.round(Number(paymentInstrument.custom.amexAmountApplied) / Number(results.paywithPoints.convertionRate)) >
              Number(paypointsObj.rewards.balance)
                ? Number(paypointsObj.rewards.balance)
                : Math.round(Number(paymentInstrument.custom.amexAmountApplied) / Number(results.paywithPoints.convertionRate));
          }
        } catch (e) {}
      }
    } else if (paymentInstrument.paymentMethod === 'GiftCard') {
      results.gcNumber =
        '*************' + paymentInstrument.custom.giftCardNumber.toString().substr(paymentInstrument.custom.giftCardNumber.toString.length - 4);
      results.type = Resource.msg('label.order.summary.gift.card', 'checkout', null);
      results.name = Resource.msg('label.order.summary.gc.name', 'checkout', null);
      results.shortMaskedNumber =
        '****' + paymentInstrument.custom.giftCardNumber.toString().substr(paymentInstrument.custom.giftCardNumber.toString.length - 4);
    } else if (paymentInstrument.paymentMethod === 'SAKSGiftCard') {
      results.type = Resource.msg('label.order.summary.saks.gift.card.type', 'checkout', null);
      results.name = Resource.msg('label.order.summary.saks.gift.card', 'checkout', null);
    }
    return results;
  });
}

/**
 * Payment class that represents payment information for the current basket
 * @param {dw.order.Basket} currentBasket - the target Basket object
 * @param {dw.customer.Customer} currentCustomer - the associated Customer object
 * @param {string} countryCode - the associated Site countryCode
 * @constructor
 */

base.Payment = function (currentBasket, currentCustomer, countryCode) {
  var paymentAmount = currentBasket.totalGrossPrice;
  var paymentMethods = PaymentMgr.getApplicablePaymentMethods(currentCustomer, countryCode, paymentAmount.value);
  var paymentCards = PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_CREDIT_CARD).getApplicablePaymentCards(
    currentCustomer,
    countryCode,
    paymentAmount.value
  );
  var paymentInstruments = currentBasket.paymentInstruments;

  // TODO: Should compare currentBasket and currentCustomer and countryCode to see
  //     if we need them or not
  this.applicablePaymentMethods = paymentMethods ? applicablePaymentMethods(paymentMethods) : null;

  this.applicablePaymentCards = paymentCards ? applicablePaymentCards(paymentCards) : null;

  this.selectedPaymentInstruments = paymentInstruments ? getSelectedPaymentInstruments(paymentInstruments, currentBasket) : null;
};

module.exports = base.Payment;
