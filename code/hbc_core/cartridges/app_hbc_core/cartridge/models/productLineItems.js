'use strict';

var base = module.superModule;
var collections = require('*/cartridge/scripts/util/collections');
var preferences = require('*/cartridge/config/preferences');

if (!preferences.CartBEPerformanceChangesEnable) {
  /**
   * Loops through all of the product line items and adds new properties.
   * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - All product
   * line items of the basket
   * Updates the prototype with new property, if basket has items with Instore pickup
   */
  function hasInStoreItems(productLineItems) {
    var inStoreItems = [];
    if (productLineItems.items && productLineItems.items.length) {
      productLineItems.items.forEach(function (productLineItem) {
        if (productLineItem.fromStoreId) {
          inStoreItems.push(productLineItem);
        }
      });
    }
    Object.defineProperty(productLineItems, 'hasInStoreItems', {
      enumerable: true,
      value: inStoreItems // eslint-disable-line
    });
  }

  /**
   * Loops through all of the product line items and adds new properties.
   * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - All product
   * line items of the basket
   * Updates the prototype with new property, if basket has items with dropShipItems pickup
   */
  function dropShiptItems(productLineItems) {
    var dropShipItems = [];
    if (productLineItems.items && productLineItems.items.length) {
      productLineItems.items.forEach(function (productLineItem) {
        if (productLineItem.isDropShipItem) {
          dropShipItems.push(productLineItem);
        }
      });
    }
    Object.defineProperty(productLineItems, 'dropShipItems', {
      enumerable: true,
      value: dropShipItems
    });
  }

  /**
   * Loops through all of the product line items and adds new properties.
   * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - All product
   * line items of the basket
   * Updates the prototype with new property, if basket has items with giftWrapItems pickup
   */
  function giftWrapItems(productLineItems) {
    var giftWraps = [];
    if (productLineItems.items && productLineItems.items.length) {
      productLineItems.items.forEach(function (productLineItem) {
        if (!productLineItem.giftWrapEligible) {
          giftWraps.push(productLineItem);
        }
      });
    }
    Object.defineProperty(productLineItems, 'giftWrapItems', {
      enumerable: true,
      value: giftWraps
    });
  }

  /**
   * Loops through all of the product line items and adds new properties.
   * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - All product
   * line items of the basket
   * Updates the prototype with new property, if basket has items with restrictedState
   */
  function restrictedState(productLineItems) {
    var restrictedSt = '';
    if (productLineItems.items && productLineItems.items.length) {
      productLineItems.items.forEach(function (productLineItem) {
        if (productLineItem.pdRestrictedStateText) {
          restrictedSt += productLineItem.pdRestrictedStateText + ',';
        }
      });
    }
    Object.defineProperty(productLineItems, 'restrictedStates', {
      enumerable: true,
      value: restrictedSt
    });
  }

  /**
   * Loops through all of the product line items and adds new properties.
   * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - All product
   * line items of the basket
   * Updates the prototype with new property, if basket has items with USPSRestricted items
   */
  function hasUSPSRestrictedItems(productLineItems) {
    var hasUSPSRestricted = false;
    if (productLineItems.items && productLineItems.items.length) {
      productLineItems.items.forEach(function (productLineItem) {
        if (!productLineItem.uspsShippingOK) {
          hasUSPSRestricted = true;
        }
      });
    }
    Object.defineProperty(productLineItems, 'hasUSPSRestrictedItems', {
      enumerable: true,
      value: hasUSPSRestricted
    });
  }

  /**
   * Loops through all of the product line items and adds new properties.
   * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - All product
   * line items of the basket
   * Updates the prototype with new property, if basket has items with isSignatureRequired
   */
  function isSignatureRequired(productLineItems) {
    var signatureRequired = false;
    if (productLineItems.items && productLineItems.items.length) {
      productLineItems.items.forEach(function (productLineItem) {
        if (productLineItem.signatureRequired) {
          signatureRequired = true;
        }
      });
    }
    Object.defineProperty(productLineItems, 'signatureRequired', {
      enumerable: true,
      value: signatureRequired
    });
  }

  /**
   * Loops through all of the product line items and adds new properties.
   * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - All product
   * line items of the basket
   * Updates the prototype with new property, if basket has items with hasPreOrderItems
   */
  function preOrderCartItem(productLineItems) {
    var hasPreOrderItems = false;
    if (productLineItems.items && productLineItems.items.length) {
      productLineItems.items.forEach(function (productLineItem) {
        if (!empty(productLineItem.preOrder) && !empty(productLineItem.preOrder.applicable) && productLineItem.preOrder.applicable == true) {
          hasPreOrderItems = true;
        }
      });
    }
    Object.defineProperty(productLineItems, 'hasPreOrderItems', {
      enumerable: true,
      value: hasPreOrderItems
    });
  }
}

/**
 * CONSTRUCTOR OVERRIDEN : to include extra properties
 * @constructor
 * @classdesc class that represents a collection of line items and total quantity of
 * items in current basket or per shipment
 *
 * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - the product line items
 *                                                       of the current line item container
 * @param {string} view - the view of the line item (basket or order)
 */
function ProductLineItems(productLineItems, view) {
  base.call(this, productLineItems, view);
  if (!preferences.CartBEPerformanceChangesEnable) {
    hasInStoreItems(this);
    dropShiptItems(this);
    giftWrapItems(this);
    hasUSPSRestrictedItems(this);
    restrictedState(this);
    isSignatureRequired(this);
    preOrderCartItem(this);
  }
}

/**
 * Loops through all of the product line items and adds the quantities together.
 * @param {dw.util.Collection<dw.order.ProductLineItem>} items - All product
 * line items of the basket
 * @returns {number} a number representing all product line items in the lineItem container.
 */
function getTotalQuantity(items) {
  // TODO add giftCertificateLineItems quantity
  var totalQuantity = 0;
  collections.forEach(items, function (lineItem) {
    totalQuantity += lineItem.quantity.value;
  });

  return totalQuantity;
}

ProductLineItems.getTotalQuantity = getTotalQuantity;
module.exports = ProductLineItems;
