'use strict';

var BaseAttributeValue = require('*/cartridge/models/search/attributeRefinementValue/base');

/**
 * @constructor
 * @classdesc Color attribute refinement value model
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - ProductSearchModel instance
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition - Refinement
 *     definition
 * @param {dw.catalog.ProductSearchRefinementValue} refinementValue - Raw DW refinement value
 */
function PromotionAttributeValue(productSearch, refinementDefinition, refinementValue) {
  this.productSearch = productSearch;
  this.refinementDefinition = refinementDefinition;
  this.refinementValue = refinementValue;

  this.initialize();
}

PromotionAttributeValue.prototype = Object.create(BaseAttributeValue.prototype);

PromotionAttributeValue.prototype.initialize = function () {
  BaseAttributeValue.prototype.initialize.call(this);

  this.type = 'promotion';
  this.value = this.refinementValue.value;
  this.displayValue = this.refinementValue.displayValue;
  this.selected = this.isSelected(this.productSearch, this.value);
  this.url = this.getUrl(this.productSearch, this.actionEndpoint, this.selected, this.value);
  let hello = this.selectable;
  this.title = this.getTitle(this.selected, this.selectable, this.refinementDefinition.displayName, this.displayValue);
};

/**
 * Forms URL for this price refinement value
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - ProductSearchModel instance
 * @param {string} actionEndpoint - Resource URL for Search
 * @param {boolean} selected - Indicates whether this value has been selected
 * @param {string} value - Refine by promotion ID
 * @return {string} - URL to select/deselect a price bucket refinement value
 */
PromotionAttributeValue.prototype.getUrl = function (productSearch, actionEndpoint, selected, value) {
  return selected
    ? productSearch.urlRelaxPromotion(actionEndpoint).relative().toString()
    : productSearch.urlRefinePromotion(actionEndpoint, value).relative().toString();
};

/**
 * Determines whether this price refinement value has been selected
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - ProductSearchModel instance
 * @param {string} value - Refine by promotion ID
 * @return {boolean} - Indicates whether this price refinement value is selected
 */
PromotionAttributeValue.prototype.isSelected = function (productSearch, value) {
  return productSearch.isRefinedByPromotion(value);
};

/**
 * @constructor
 * @classdesc Price refinement value class
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - ProductSearchModel instance
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition - Refinement
 *     definition
 * @param {dw.catalog.ProductSearchRefinementValue} refinementValue - Raw DW refinement value
 */
function PromotionRefinementValueWrapper(productSearch, refinementDefinition, refinementValue) {
  var value = new PromotionAttributeValue(productSearch, refinementDefinition, refinementValue);
  var items = ['id', 'type', 'selectable', 'displayValue', 'selected', 'title', 'url'];
  items.forEach(function (item) {
    this[item] = value[item];
  }, this);
}

module.exports = PromotionRefinementValueWrapper;
