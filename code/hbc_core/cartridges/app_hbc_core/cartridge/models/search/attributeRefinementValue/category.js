'use strict';

var BaseAttributeValue = require('*/cartridge/models/search/attributeRefinementValue/base');
var CatalogMgr = require('dw/catalog/CatalogMgr');
var ACTION_ENDPOINT = 'Search-Show';
var URLUtils = require('dw/web/URLUtils');
/**
 * Get category url
 * @param {dw.catalog.Category} category - Current category
 * @returns {string} - Url of the category
 */
function getCategoryUrl(category) {
  return category.custom && 'alternativeUrl' in category.custom && category.custom.alternativeUrl ? category.custom.alternativeUrl.markup : null;
}

/**
 * @constructor
 * @classdesc Category attribute refinement value model
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - ProductSearchModel instance
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition - Refinement
 *     definition
 * @param {dw.catalog.ProductSearchRefinementValue} refinementValue - Raw DW refinement value
 * @param {boolean} selected - Selected flag
 * @param {boolean} sibling - Sibling flag
 */
function CategoryAttributeValue(productSearch, refinementDefinition, refinementValue, selected, sibling) {
  this.productSearch = productSearch;
  this.refinementDefinition = refinementDefinition;
  this.refinementValue = refinementValue;
  this.subCategories = [];
  this.selected = selected;
  this.sibling = sibling;
  this.initialize();
}

CategoryAttributeValue.prototype = Object.create(BaseAttributeValue.prototype);

CategoryAttributeValue.prototype.initialize = function () {
  this.type = 'category';
  this.selectable = true;
  this.id = this.refinementValue.ID;
  this.actionEndpoint = ACTION_ENDPOINT;

  var categoryNameoverwrite = CatalogMgr.getCategory(this.id).custom.categoryNameoverwrite
    ? CatalogMgr.getCategory(this.id).custom.categoryNameoverwrite
    : null;
  this.categoryNameoverwrite = categoryNameoverwrite;

  this.displayValue = this.refinementValue.displayName;
  this.url = this.getUrl(this.productSearch, this.actionEndpoint, this.id, this.value, this.selected, this.selectable, this.sibling);
  this.title = this.getTitle(this.selected, this.selectable, this.refinementDefinition.displayName, this.displayValue);
  let categoryObject = CatalogMgr.getCategory(this.id);
  if (categoryObject) {
    this.altUrl = getCategoryUrl(categoryObject);
    this.hideLeftNav = 'HideLeftNav' in categoryObject.custom && categoryObject.custom.HideLeftNav ? categoryObject.custom.HideLeftNav : false;
  }
};

CategoryAttributeValue.prototype.getUrl = function (productSearch, actionEndpoint, id, value, selected, selectable, sibling) {
  var url = '';

  if (productSearch) {
    let refinementAttributeID = 'hbcProductType';
    let refinementValue = productSearch.getRefinementValues(refinementAttributeID);
    refinementValue = refinementValue.length ? refinementValue.toArray().join('|') : '';
    productSearch.removeRefinementValues(refinementAttributeID, refinementValue);
  }
  if (selected) {
    if (productSearch.category && productSearch.category.parent) {
      url = productSearch.urlRefineCategory(actionEndpoint, productSearch.category.parent.ID).relative().toString();
    } else {
      url = productSearch.urlRefineCategory(actionEndpoint, id).relative().toString();
    }
  } else if (sibling) {
    url = URLUtils.url(actionEndpoint, 'cgid', id).relative().toString();
  } else {
    url = productSearch.urlRefineCategory(actionEndpoint, id).relative().toString();
  }

  return url;
};

/**
 * @constructor
 * @classdesc Category attribute refinement value model
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - ProductSearchModel instance
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition - Refinement
 *     definition
 * @param {dw.catalog.ProductSearchRefinementValue} refinementValue - Raw DW refinement value
 * @param {boolean} selected - Selected flag
 * @param {boolean} sibling - Sibling category flag
 */
function CategoryRefinementValueWrapper(productSearch, refinementDefinition, refinementValue, selected, sibling) {
  var value = new CategoryAttributeValue(productSearch, refinementDefinition, refinementValue, selected, sibling);
  var items = ['id', 'type', 'displayValue', 'categoryNameoverwrite', 'selected', 'selectable', 'title', 'url', 'subCategories', 'altUrl', 'hideLeftNav'];
  items.forEach(function (item) {
    this[item] = value[item];
  }, this);
}

module.exports = CategoryRefinementValueWrapper;
