'use strict';

var base = module.superModule;

var URLUtils = require('dw/web/URLUtils');
var ACTION_ENDPOINT = 'Product-Show';
var IMAGE_SIZE = 'medium';

/**
 * Get Image URL
 *
 * @param {dw.catalog.Product} product - Suggested product
 * @return {string} - Image URL
 */
function getImageUrl(product) {
  var ImageModel = require('*/cartridge/models/product/productImages');
  var imageProduct = product;
  if (product.master) {
    imageProduct = product.variationModel.defaultVariant;
  }
  var image = new ImageModel(product, {
    types: ['medium'],
    quantity: 'single'
  });
  // tile image is fetched from first set product if ProductSet has no image configured
  try {
    if (
      product.productSet &&
      image &&
      image.medium &&
      image.medium[0].url &&
      image.medium[0].url.indexOf('ImageNotAvailable') > -1 &&
      product.productSetProducts.length > 0
    ) {
      image = new ImageModel(product.productSetProducts[0], {
        types: ['medium'],
        quantity: 'single'
      });
    }
  } catch (e) {
    var Logger = require('dw/system/Logger');
    Logger.error('Error fetching tile image for productSet' + e);
  }

  var imageURL = '';
  if (image && image.medium) {
    imageURL = image.medium[0].url;
  }
  return imageURL;
}

/**
 * Compile a list of relevant suggested products
 *
 * @param {dw.util.Iterator.<dw.suggest.SuggestedProduct>} suggestedProducts - Iterator to retrieve
 *                                                                             SuggestedProducts
 *  @param {number} maxItems - Maximum number of products to retrieve
 * @return {Object[]} - Array of suggested products
 */
function getProducts(suggestedProducts, maxItems, searchTerms) {
  var product = null;
  var products = [];

  for (var i = 0; i < maxItems; i++) {
    if (suggestedProducts.hasNext()) {
      product = suggestedProducts.next().productSearchHit.product;
      if (!product.productSet) {
        products.push({
          name: product.name,
          hbcProductType: 'hbcProductType' in product.custom && product.custom.hbcProductType ? product.custom.hbcProductType : '',
          brand: product.brand,
          imageUrl: getImageUrl(product),
          url: URLUtils.url(ACTION_ENDPOINT, 'pid', product.ID, 'type', 'Featured Items', 'term', searchTerms ? searchTerms : '')
        });
      }
    }
  }

  return products;
}

/**
 * @typedef SuggestedPhrase
 * @type Object
 * @property {boolean} exactMatch - Whether suggested phrase is an exact match
 * @property {string} value - Suggested search phrase
 */

/**
 * Compile a list of relevant suggested phrases
 *
 * @param {dw.util.Iterator.<dw.suggest.SuggestedPhrase>} suggestedPhrases - Iterator to retrieve
 *                                                                           SuggestedPhrases
 * @param {number} maxItems - Maximum number of phrases to retrieve
 * @return {SuggestedPhrase[]} - Array of suggested phrases
 */
function getPhrases(suggestedPhrases, maxItems) {
  var phrase = null;
  var phrases = [];

  for (var i = 0; i < maxItems; i++) {
    if (suggestedPhrases.hasNext()) {
      phrase = suggestedPhrases.next();
      phrases.push({
        exactMatch: phrase.exactMatch,
        value: phrase.phrase
      });
    }
  }

  return phrases;
}

/**
 * @constructor
 * @classdesc ProductSuggestions class
 *
 * @param {dw.suggest.SuggestModel} suggestions - Suggest Model
 * @param {number} maxItems - Maximum number of items to retrieve
 */
base.ProductSuggestions = function (suggestions, maxItems, searchTerms) {
  var productSuggestions = suggestions.productSuggestions;

  if (!productSuggestions) {
    this.available = false;
    this.phrases = [];
    this.products = [];
    return;
  }

  var searchPhrasesSuggestions = productSuggestions.searchPhraseSuggestions;

  this.available = productSuggestions.hasSuggestions();
  this.phrases = getPhrases(searchPhrasesSuggestions.suggestedPhrases, maxItems);
  this.products = getProducts(productSuggestions.suggestedProducts, maxItems, searchTerms);
};

module.exports = base.ProductSuggestions;
