'use strict';

var base = module.superModule;

var URLUtils = require('dw/web/URLUtils');
var endpoint = 'Search-Show';
/**
 * Get category alternatice url
 * @param {dw.catalog.Category} category - Current category
 * @returns {string} - Url of the category
 */
function getCategoryUrl(category, searchTerms) {
  return category.custom && 'alternativeUrl' in category.custom && category.custom.alternativeUrl
    ? category.custom.alternativeUrl.markup
    : URLUtils.url(endpoint, 'cgid', category.ID, 'type', 'Suggestions', 'term', searchTerms || '');
}
/**
 * @Description Iterate the category to find l1 Category
 * @param {dw.catalog.Category} category - Get Top Level Category
 * @return {Object} category - returns category who's parent categoryID is root
 */
function getTopLevelCategory(category) {
  if (category.parent.ID === 'root') {
    return category;
  }
  return getTopLevelCategory(category.parent);
}

/**
 * @constructor
 * @classdesc CategorySuggestions class
 *
 * @param {dw.suggest.SuggestModel} suggestions - Suggest Model
 * @param {number} maxItems - Maximum number of categories to retrieve
 */
base.CategorySuggestions = function (suggestions, maxItems, searchTerms) {
  this.categories = [];

  if (!suggestions.categorySuggestions) {
    this.available = false;
    return;
  }

  var categorySuggestions = suggestions.categorySuggestions;
  var iter = categorySuggestions.suggestedCategories;

  this.available = categorySuggestions.hasSuggestions();

  for (var i = 0; i < maxItems; i++) {
    var category = null;

    if (iter.hasNext()) {
      category = iter.next().category;
      this.categories.push({
        name: category.displayName,
        imageUrl: category.image ? category.image.url : '',
        url: getCategoryUrl(category, searchTerms),
        parentID: category.parent.ID,
        parentName: getTopLevelCategory(category).displayName
      });
    }
  }
};

module.exports = base.CategorySuggestions;
