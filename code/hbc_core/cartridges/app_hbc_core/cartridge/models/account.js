'use strict';

var AddressModel = require('*/cartridge/models/address');
var URLUtils = require('dw/web/URLUtils');
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');
var TCCHelpers = require('*/cartridge/scripts/checkout/TCCHelpers');

/**
 * Creates a plain object that contains profile information
 * @param {dw.customer.Profile} profile - current customer's profile
 * @param {dw.customer.Profile} rawProfile - customer's rawProfile
 * @returns {Object} an object that contains information about the current customer's profile
 */
function getProfile(profile, rawProfile) {
  var result;
  var preferences = require('*/cartridge/config/preferences');
  if (profile) {
    result = {
      firstName: profile.firstName,
      lastName: profile.lastName,
      email: profile.email,
      phone: profile.phone,
      password: '********',
      hbcRewardNumber: 'hudsonReward' in rawProfile.custom && !empty(rawProfile.custom.hudsonReward) ? rawProfile.custom.hudsonReward : '',
      profileLastModified: 'accountModifiedDate' in rawProfile.custom ? new Date(rawProfile.custom.accountModifiedDate) : rawProfile.getLastModified(),
      hudsonRewardPrefix: preferences.hudsonReward
    };
  } else {
    result = null;
  }
  return result;
}

/**
 * Creates an array of plain object that contains address book addresses, if any exist
 * @param {dw.customer.Customer} addressBook - target customer
 * @returns {Array<Object>} an array of customer addresses
 */
function getAddresses(addressBook) {
  var result = [];
  if (addressBook) {
    for (var i = 0, ii = addressBook.addresses.length; i < ii; i++) {
      result.push(new AddressModel(addressBook.addresses[i]).address);
    }
  }

  return result;
}

/**
 * Creates a plain object that contains the customer's preferred address
 * @param {dw.customer.Customer} addressBook - target customer
 * @returns {Object} an object that contains information about current customer's preferred address
 */
function getPreferredAddress(addressBook) {
  var result = null;
  if (addressBook && addressBook.preferredAddress) {
    result = new AddressModel(addressBook.preferredAddress).address;
  }

  return result;
}

/**
 * Creates a plain object that contains payment instrument information
 * @param {Object} wallet - current customer's wallet
 * @returns {Object} object that contains info about the current customer's payment instrument
 */
function getPayment(wallet) {
  if (wallet) {
    var paymentInstruments = wallet.paymentInstruments;
    var paymentInstrument = paymentInstruments[0];

    if (paymentInstrument) {
      return {
        maskedCreditCardNumber: paymentInstrument.maskedCreditCardNumber,
        creditCardType: paymentInstrument.creditCardType,
        creditCardExpirationMonth: paymentInstrument.creditCardExpirationMonth,
        creditCardExpirationYear: paymentInstrument.creditCardExpirationYear
      };
    }
  }
  return null;
}

/**
 * Creates a plain object that contains payment instrument information
 * @param {Object} userPaymentInstruments - current customer's paymentInstruments
 * @returns {Object} object that contains info about the current customer's payment instruments
 */
function getCustomerPaymentInstruments(userPaymentInstruments) {
  var paymentInstruments;

  paymentInstruments = userPaymentInstruments.map(function (paymentInstrument) {
    var ccLength = paymentInstrument.maskedCreditCardNumber.toString().length;
    var result = {
      creditCardHolder: paymentInstrument.creditCardHolder,
      maskedCreditCardNumber: paymentInstrument.maskedCreditCardNumber,
      creditCardType: paymentInstrument.creditCardType,
      creditCardExpirationMonth: paymentInstrument.creditCardExpirationMonth,
      creditCardExpirationYear: paymentInstrument.creditCardExpirationYear,
      creditCardLastFourDigit: paymentInstrument.raw.creditCardNumberLastDigits,
      UUID: paymentInstrument.UUID,
      defaultCreditCard: !!('defaultCreditCard' in paymentInstrument.raw.custom && paymentInstrument.raw.custom.defaultCreditCard),
      creationDate: paymentInstrument.raw.getCreationDate(),
      authorizedCard: !!paymentInstrument.raw.custom.authorizedCard,
      creditCardToken: paymentInstrument.raw.creditCardToken,
      creditCardLength: paymentInstrument.maskedCreditCardNumber.length,
      shortMaskedNumber: paymentInstrument.maskedCreditCardNumber.toString().substr(ccLength - 8, ccLength),
      isCreditCardExpired: paymentInstrument.raw.isCreditCardExpired()
    };

    result.cardTypeImage = {
      src: URLUtils.staticURL('/images/' + paymentInstrument.creditCardType.toLowerCase().replace(/\s/g, '') + '-dark.svg'),
      alt: paymentInstrument.creditCardType
    };

    if (paymentInstrument.creditCardType === 'TCC') {
      // Format TCC expiration date.
      var tccExpirationDate = new Date(paymentInstrument.raw.custom.tccExpirationDate);
      var today = new Date();
      result.tccExpDate = StringUtils.formatCalendar(new Calendar(tccExpirationDate), 'MM/dd/yyyy');
      result.isExpired = tccExpirationDate <= today ? 'tcc-expired' : '';

      // Determine TCC label name and token type (SAKS or SAKSMC).
      var tccNumber = paymentInstrument.raw.getCreditCardToken();
      result.tokenNumberTypeName = TCCHelpers.getTCCTokenType(tccNumber) || 'unknown';
      result.tokenLabel = TCCHelpers.getTCCTokenLabel(tccNumber);
    }

    return result;
  });

  return paymentInstruments;
}

/**
 * Get customer payment DOM HTML
 *
 * @param {dw/customer/Customer} currentCustomer - current customer
 * @returns {string} - rendered isml
 */
function getCustomerPaymentHtml(currentCustomer) {
  var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
  var ArrayList = require('dw/util/ArrayList');
  var PropertyComparator = require('dw/util/PropertyComparator');
  var paymentInstruments = getCustomerPaymentInstruments(currentCustomer.wallet.paymentInstruments);
  paymentInstruments = new ArrayList(paymentInstruments);
  paymentInstruments.sort(new PropertyComparator('defaultCreditCard', false));
  var paymentAjaxTemplate = 'account/payment/paymentAjax';

  var paymentContext = {
    paymentInstruments: paymentInstruments,
    actionUrl: URLUtils.url('PaymentInstruments-DeletePayment').toString()
  };

  var paymentHtml = renderTemplateHelper.getRenderedHtml(paymentContext, paymentAjaxTemplate);
  return paymentHtml;
}

/**
 * Get current customer's preferences
 *
 * @param {dw/customer/Profile} profile - current customer's profile
 * @returns {Object} customers's preferences object based on its setting
 */
function getPreferences(profile) {
  var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
  return accountHelpers.getCustomerPreferences(profile);
}

/**
 * Account class that represents the current customer's profile dashboard
 * @param {dw.customer.Customer} currentCustomer - Current customer
 * @param {Object} addressModel - The current customer's preferred address
 * @param {Object} orderModel - The current customer's order history
 * @constructor
 */
function account(currentCustomer, addressModel, orderModel) {
  this.profile = getProfile(currentCustomer.profile, currentCustomer.raw.profile);
  this.addresses = getAddresses(currentCustomer.addressBook);
  this.preferredAddress = addressModel || getPreferredAddress(currentCustomer.addressBook);
  this.orderHistory = orderModel;
  this.payment = getPayment(currentCustomer.wallet);
  this.registeredUser = currentCustomer.raw.authenticated && currentCustomer.raw.registered;
  this.isExternallyAuthenticated = currentCustomer.raw.externallyAuthenticated;
  this.customerPaymentInstruments =
    currentCustomer.wallet && currentCustomer.wallet.paymentInstruments ? getCustomerPaymentInstruments(currentCustomer.wallet.paymentInstruments) : null;
  this.preferences = getPreferences(currentCustomer.raw.profile);
}

account.getCustomerPaymentInstruments = getCustomerPaymentInstruments;
account.getCustomerPaymentHtml = getCustomerPaymentHtml;

module.exports = account;
