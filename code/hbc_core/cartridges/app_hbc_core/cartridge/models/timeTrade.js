'use strict';
var system = require('dw/system/System');
var preferences = require('*/cartridge/config/preferences');
var Resource = require('dw/web/Resource');

/**
 * @constructor
 * @classdesc The timetrade model
 * @param {string} locale - String of request locale
 */
function timeTrade(locale) {
  var instanceBasedLocale = system.getInstanceType() !== system.PRODUCTION_SYSTEM ? locale + '_Stage' : locale + '_Production';
  this.url = JSON.parse(preferences.timeTradeURL)[instanceBasedLocale];
  this.locationID = preferences.locationID;
  this.appointmentTypeID = preferences.appointmentTypeID;
  this.resources = {
    header: Resource.msg('header', 'timetrade', null),
    headerInfo: Resource.msg('headerInfo', 'timetrade', null),
    firstName: Resource.msg('firstName', 'timetrade', null),
    lastName: Resource.msg('lastName', 'timetrade', null),
    email: Resource.msg('email', 'timetrade', null),
    phone: Resource.msg('phone', 'timetrade', null),
    cancelLabel: Resource.msg('cancelLabel', 'timetrade', null),
    continueLabel: Resource.msg('continueLabel', 'timetrade', null),
    requiredMsg: Resource.msg('error.message.required', 'error', null),
    emailInvalidMsg: Resource.msg('msg.email.invalid', 'error', null)
  };
  return this;
}
module.exports = timeTrade;
