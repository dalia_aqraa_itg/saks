'use strict';

/**
 * ConfigOrderReco.js
 *
 * @module ConfigOrderReco
 */

/**
 *   @return {Object} Constants
 */
function getConstants() {
  return {
    IMPEX_ORDER_RECO_PATH: 'order_reco',
    SRC_FOLDER: '/src/',
    DELIMITER: ',',
    EXTENSION: '.csv',
    ORDER_RECO_FILE_NAME: 'oms_reconciliation_feed',
    ORDER_RECO_ARCHIVE_FOLDER: 'archive',
    ORDER_RECO_ARCHIVE_DAYS: 30
  };
}

/**
 *   Export function
 */
module.exports = {
  getConstants: getConstants
};
