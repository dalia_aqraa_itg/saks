'use strict';

/* Script Modules */
var InventoryServiceUtils = require('*/cartridge/scripts/services/init/InventoryServiceUtils');
var ServiceHelper = require('*/cartridge/scripts/services/helpers/ServiceHelper');
var Logger = require('dw/system/Logger');

/**  Calls Inventory availability
 * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - All product
 * @param {string} action - action required to invoke reservation vs cancel
 * @returns {Object} result - response indicating the availability
 */
function getAvailability(productLineItems, action) {
  var result = {
    hasInventory: true,
    errorInService: false
  };
  try {
    // prepare service request
    var invLookUpRequest = ServiceHelper.prepareRequest(productLineItems, action);
    // make call to service and get the resposne
    var objResponse = InventoryServiceUtils.callGetInventoryAvailabilityService(invLookUpRequest);
    if (objResponse && objResponse.status === 'OK' && objResponse.result) {
      // parse response
      var parsedResponse = ServiceHelper.parseInvLookUpResponse(objResponse.result);
      if (parsedResponse.unAvailabilityMap && parsedResponse.unAvailabilityMap.size() > 0) {
        result.hasInventory = false;
        var itemsQuantity = [];
        parsedResponse.unAvailabilityMap
          .values()
          .toArray()
          .forEach(function (item) {
            var inventoryData = {};
            inventoryData.itemID = item.ItemID;
            inventoryData.quantity = item.numberOfRecordAvailable;
            itemsQuantity.push(inventoryData);
          });
        result.omsInventory = itemsQuantity;
      }
    } else {
      result.errorInService = true;
    }
    return result;
  } catch (e) {
    // log the exception
    Logger.error('Error InventoryService.getAvailability() : ' + e);
    result.errorInService = true;
    return result;
  }
}

/**  Calls Inventory Reservation
 * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - All product
 * @param {string} action - action required to invoke reservation vs cancel
 * @param {dw.order.Order} order - SFCC order object.
 * @returns {Object} result - response object.
 */
function reserveInventory(productLineItems, action, order) {
  // preparing the response object
  var result = {};
  result.hasInventory = true;
  var unAvailabilityMap = null;
  try {
    // prepare the inventory reservation request.
    var reserveInventoryRequest = ServiceHelper.prepareRequest(productLineItems, action, order.orderNo);
    // call the service.
    var objResponse = InventoryServiceUtils.callGetInventoryReservation(reserveInventoryRequest);
    if (objResponse && objResponse.status === 'OK' && objResponse.result) {
      // parse response
      var parsedResponse = ServiceHelper.parseReserveInventoryResponse(objResponse.result);
      if (parsedResponse.ResponseCode == '99' || parsedResponse.ResponseCode == '94' || parsedResponse.ResponseCode == '1') {
        return result;
      } else if (parsedResponse.unAvailabilityMap && parsedResponse.unAvailabilityMap.size() > 0) {
        result.hasInventory = false;
        unAvailabilityMap = parsedResponse.unAvailabilityMap;
      } else if (parsedResponse.availabilityMap && parsedResponse.availabilityMap.size() > 0) {
        ServiceHelper.updateDetailsToPlis(parsedResponse.availabilityMap, order);
        unAvailabilityMap = parsedResponse.unAvailabilityMap;
      }
    } else if (objResponse && objResponse.status === 'ERROR') {
      result.hasInventory = false;
    }
  } catch (e) {
    // log the exception
    Logger.error('Error InventoryService.reserveInventory() : ' + e);
  }
  if (unAvailabilityMap && unAvailabilityMap.size() > 0) {
    var itemsQuantity = [];
    var productIds = unAvailabilityMap.keySet();
    var collections = require('*/cartridge/scripts/util/collections');
    collections.forEach(productIds, function (productId) {
      var inventoryData = {};
      inventoryData.itemID = productId;
      inventoryData.quantity = unAvailabilityMap.get(productId).availableQty;
      itemsQuantity.push(inventoryData);
    });
    result.omsInventory = itemsQuantity;
  }

  return result;
}

/** Calls Inventory Cancellation
 *
 * @param {string} reservationID - reservation ID of the order
 * @returns {Object} parsedResponse - parsed service response.
 *
 */
function cancelReservation(reservationID) {
  try {
    // prepare the cancel reservation request.
    var cancelReservationRequest = ServiceHelper.prepareCancelcancelReservationRequest(reservationID);
    // call the service.
    var objResponse = InventoryServiceUtils.callCancelInventoryReservationService(cancelReservationRequest);
    if (objResponse && objResponse.status === 'OK') {
      // parse response
      var parsedResponse = ServiceHelper.parsecancelReservationResponse(objResponse.result);
      if (!parsedResponse.success) {
        // Create Custom Object
        ServiceHelper.createCancelReservationDataInCO(reservationID);
      }
      return parsedResponse;
    }
  } catch (e) {
    // fatal error for unavailable services.
    Logger.error('Error InventoryService.cancelReservation() : ' + e);
  }
  return {};
}

module.exports = {
  getAvailability: getAvailability,
  reserveInventory: reserveInventory,
  cancelReservation: cancelReservation
};
