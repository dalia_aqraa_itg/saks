var Site = require('dw/system/Site');

/**
 * Inventory service constants
 * site preference, default values
 */
const InventoryServices = {
  INVENTORY_AVAILABILITY: 'GetInventoryAvailabilityService',
  INVENTORY_RESERVATION: 'GetInventoryReservationService',
  ENTRY_TYPE: 'Online',
  FULLFILLMENT_TYPE_SHIP: 'SHIP',
  FULLFILLMENT_TYPE_SHIP_INTL: 'INTL_SHIP',
  FULLFILLMENT_TYPE_BOPIS: 'PICKUP',
  FULLFILLMENT_TYPE_PREORDER: 'PREORDER',
  FULLFILLMENT_TYPE_PREORDER_INTL: 'INTL_PREORDER',
  FULLFILLMENT_TYPE_SDD: 'SDD',
  DISTANCE_TO_CONSIDER: '100', // make this dynamic,
  DISTANCE_UOM_TOCONSIDER: require('*/cartridge/config/preferences').countryCode.value === 'US' ? 'MILE' : 'KILOMETER',
  ENTERPRISE_CODE: Site.current.getCustomPreferenceValue('enterpriseCode'),
  ORGANIZATION_CODE: Site.current.getCustomPreferenceValue('organizationCode'),
  INVENTORY_ACTION_RESERVE: 'reserve',
  INVENTORY_ACTION_CANCEL: 'cancel',
  INVENTORY_ACTION_CREATE: 'Create',
  INVENTORY_ACTION_CANCEL_RESERVE: 'Cancel',
  INVENTORY_CHECK_CAPACITY: 'Y',
  INVENTORY_CHECK_INVENTORY: 'Y',
  INVENTORY_EXPIRATION_ADDON_TIME: Site.current.getCustomPreferenceValue('invReservationExpiryTime')
    ? Site.current.getCustomPreferenceValue('invReservationExpiryTime')
    : 0,
  INVENTORY_EERROR_CODES: 'OMS_INV_002,OMS_INV_999,OMS_INV_999,OMS_INV_005,OMS_INV_003'
};

module.exports = InventoryServices;
