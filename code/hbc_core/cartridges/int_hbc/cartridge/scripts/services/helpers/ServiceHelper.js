/* Script Modules */
var ServiceConstants = require('*/cartridge/scripts/services/helpers/ServiceConstants');
var preferences = require('*/cartridge/config/preferences');
var cookiesHelper = require('*/cartridge/scripts/helpers/cookieHelpers');
var Calendar = require('dw/util/Calendar');

var Logger = require('dw/system/Logger');

/** *****************************************
 **contains utilities for Inventory Service**
 *********************************************/

/** Append necessary http headers,
 *  method and content
 *  @param {dw.svc.LocalServiceRegistry} service - service object
 *  @returns {dw.svc.LocalServiceRegistry} service - update header and return the same service object
 */
var addServiceHeaders = function (service) {
  service.addHeader('x-api-key', preferences.API_KEY);
  service.addHeader('x-apigw-api-id', preferences.API_KEY_GW);
  service.addHeader('Content-Type', 'application/json');
  // additional headers can be added here
  return service;
};

/** Fetch expiry date for reservation service
 *  add the current time with the minutes in site
 *  preference and returns the calender object.
 *  @returns {dw.util.Calendar} - cal calender with exipration time.
 */
var getExpiryDate = function () {
  var Calendar = require('dw/util/Calendar');
  var cal = new Calendar();
  cal.setTimeZone('Etc/UTC');

  var date = cal.getTime();
  var dateInHours = cal.getTime();
  date.setTime(dateInHours.getTime() + ServiceConstants.INVENTORY_EXPIRATION_ADDON_TIME * 60 * 1000);
  cal.setTime(date);
  return cal;
};

/** Convert time into UTC String format
 *  add the current time with the minutes in site
 *  preference and returns in UTC
 *  @param {dw.util.Calendar} cal - Calender object
 *  @returns {String} - formatted date in string
 */

var convertTimeToUTCString = function (cal) {
  var StringUtils = require('dw/util/StringUtils');
  return StringUtils.formatCalendar(cal, "yyyy-MM-dd'T'HH:mm:ss'Z'");
};

/** Used to log Service request for service failures,timeouts etc.
 * @param {Object} inventoryRequest - service object
 */
var logRequest = function (inventoryRequest) {
  // log non-sensitive data ONLY
  var log = Logger.getLogger('InventoryService', 'InventoryService');
  var filteredParams = [];
  filteredParams.push(' \n==========');
  filteredParams.push(inventoryRequest);
  log.info(filteredParams.join('\n'));
};

/** prepares request
 * @param {Object} lineItems - object with productLineItems
 * @param {string} action - action required to invoke reservation vs cancel
 * @param {string} orderNo - SFCC order number
 * @returns {string} - json string of request
 */
var prepareRequest = function (lineItems, action, orderNo) {
  var StoreMgr = require('dw/catalog/StoreMgr');
  var StoreModel = require('*/cartridge/models/store');
  var Locale = require('dw/util/Locale');
  var currentLocale = Locale.getLocale(request.locale);
  var today = new Calendar();
  var promise = {};
  var promiseLines = [];
  var reqData = {};
  var promiseObj = {};
  var reservationParameters = {};

  var store;
  var hasInstoreItems = lineItems.hasInStoreItems;
  if (action === ServiceConstants.INVENTORY_ACTION_RESERVE) {
    // RESERVATION params
    promise.Action = ServiceConstants.INVENTORY_ACTION_CREATE;
    promise.CheckCapacity = ServiceConstants.INVENTORY_CHECK_CAPACITY;
    promise.CheckInventory = ServiceConstants.INVENTORY_CHECK_INVENTORY;
    reservationParameters.ExpirationDate = convertTimeToUTCString(getExpiryDate());
    reservationParameters.ReservationID = orderNo;
    promise.ReservationParameters = reservationParameters;
  }
  promise.EntryType = ServiceConstants.ENTRY_TYPE;
  var bfxCountryCode = cookiesHelper.read('bfx.country');
  var FULLFILLMENT_TYPE_SHIP = ServiceConstants.FULLFILLMENT_TYPE_SHIP;
  var FULLFILLMENT_TYPE_PREORDER = ServiceConstants.FULLFILLMENT_TYPE_PREORDER;
  if (bfxCountryCode && bfxCountryCode !== 'US') {
    FULLFILLMENT_TYPE_SHIP = ServiceConstants.FULLFILLMENT_TYPE_SHIP_INTL;
    FULLFILLMENT_TYPE_PREORDER = ServiceConstants.FULLFILLMENT_TYPE_PREORDER_INTL;
  }
  promise.FulfillmentType = FULLFILLMENT_TYPE_SHIP;
  if (hasInstoreItems && hasInstoreItems.length > 0 && action !== ServiceConstants.INVENTORY_ACTION_RESERVE) {
    promise.DistanceToConsider = ServiceConstants.DISTANCE_TO_CONSIDER;
    promise.DistanceUOMToConsider = ServiceConstants.DISTANCE_UOM_TOCONSIDER;
  } else {
    promise.DistanceToConsider = '';
    promise.DistanceUOMToConsider = '';
  }
  promise.EnterpriseCode = ServiceConstants.ENTERPRISE_CODE;
  promise.OrganizationCode = ServiceConstants.ORGANIZATION_CODE;

  var order = null;
  if (orderNo != null && orderNo != undefined) {
    var OrderMgr = require('dw/order/OrderMgr');
    order = OrderMgr.getOrder(orderNo);
  }

  if (lineItems.items.length > 0) {
    lineItems.items.forEach(function (item) {
      var promiseLine = {};
      var shipToAddress = {};
      var shiptoCountry = currentLocale.country; // default
      var shiptoState = '';
      var shiptoPostal = '';
      var pli = null;
      if (order != null) {
        var productID = item.id;
        var pliCollection = order.getProductLineItems(productID);
        var pliItr = pliCollection.iterator();
        pli = pliItr.hasNext() ? pliItr.next() : null;
      }
      if (item.preOrder && item.preOrder.applicable && item.preOrder.applicable == true) {
        // eslint-disable-line
        promiseLine.FulfillmentType = FULLFILLMENT_TYPE_PREORDER;
        promiseLine.ReqEndDate = ''; //SFSX-2211
      } else if (pli && pli.custom.sddLineItem && pli.getShipment().getShippingMethod().custom.sddEnabled) {
        promiseLine.FulfillmentType = ServiceConstants.FULLFILLMENT_TYPE_SDD;
        if (session.custom.sddstoreid) {
          promiseLine.ShipNode = session.custom.sddstoreid;
        }
        // current time stamp -  we are supposed to send the timestamp of the lookup service
        promiseLine.ReqEndDate = convertTimeToUTCString(today); //SFSX-2211
      } else {
        promiseLine.FulfillmentType = item.fromStoreId ? ServiceConstants.FULLFILLMENT_TYPE_BOPIS : FULLFILLMENT_TYPE_SHIP;
        // current time stamp -  we are supposed to send the timestamp of the lookup service
        promiseLine.ReqEndDate = item.fromStoreId ? '' : convertTimeToUTCString(today); //SFSX-2211
      }

      promiseLine.ItemID = item.id;
      promiseLine.LineId = item.position ? item.position.toString() : ''; // eslint-disable-line
      promiseLine.UnitOfMeasure = 'EACH'; // item.unitOfMeasure;
      promiseLine.RequiredQty = item.quantity.toString();
      if (item.fromStoreId) {
        if (action === ServiceConstants.INVENTORY_ACTION_RESERVE) {
          promiseLine.ShipNode = item.fromStoreId;
        }
        store = StoreMgr.getStore(item.fromStoreId);
        store = new StoreModel(store);
        if (store) {
          shiptoCountry = store.countryCode;
          shiptoState = store.stateCode;
          shiptoPostal = store.postalCode;
        }
      } else if (action === ServiceConstants.INVENTORY_ACTION_RESERVE) {
        shiptoCountry = item.shippingCountryCode ? item.shippingCountryCode : '';
        shiptoState = item.shippingStateCode ? item.shippingStateCode : '';
        shiptoPostal = item.shippingPostalCode ? item.shippingPostalCode : '';
      }
      shipToAddress.Country = shiptoCountry;
      shipToAddress.State = shiptoState;
      shipToAddress.ZipCode = shiptoPostal;
      //
      promiseLine.ShipToAddress = shipToAddress;
      promiseLines.push(promiseLine);
    });
    promiseObj.PromiseLine = promiseLines;
    promise.PromiseLines = promiseObj;
  }
  reqData.Promise = promise;
  // prepare JSON
  // var reqString = JSON.stringify(reqData).replace(/[\[\]]+/g,'');
  return JSON.stringify(reqData);
};

/** prepares request
 * @param {string} serviceName - service name to fetch the mock response
 * @returns {Object} response - response object
 */
var getMockedResponse = function (serviceName) {
  var response = '';
  switch (serviceName) {
    case ServiceConstants.INVENTORY_AVAILABILITY:
      response = '{"ResponseCode":"0","ResposneMessage":"Sucess"}';
      break;

    case ServiceConstants.INVENTORY_RESERVATION:
      response = '{"ResponseCode":"0","ResposneMessage":"Sucess"}';
      break;
    default:
      response = '';
      break;
  }
  return JSON.parse(response);
};

/** Parse the inventory look up response.
 * Creates HashMaps to contain available and unavailable items
 * @param {Object} responseObj - inventory response object
 * @return {Object} parsedResponse - inventory response object parsed
 */
var parseInvLookUpResponse = function (responseObj) {
  var HashMap = require('dw/util/HashMap');
  var response = JSON.parse(responseObj);
  var parsedResponse = {};
  var promiseLines = null;
  var suggestionOption = null;
  var unAvailableLines = null;
  var availabiltyMap = new HashMap();
  var unavailabilityMap = new HashMap();
  var errorPromiseLines = null;
  if (response && response.ResponseCode) {
    // denotes success from service
    if (response.ResponseCode === '0') {
      suggestionOption = response.SuggestedOption ? response.SuggestedOption : null;
      if (suggestionOption && suggestionOption.Option && suggestionOption.Option.PromiseLines) {
        promiseLines = suggestionOption.Option.PromiseLines;
        if (promiseLines.PromiseLine && promiseLines.PromiseLine.length > 0) {
          promiseLines.PromiseLine.forEach(function (promiseLine) {
            if (promiseLine.Assignments && promiseLine.Assignments.Assignment && promiseLine.Assignments.Assignment[0]) {
              if (!promiseLine.Assignments.Assignment[0].EmptyAssignmentReason) {
                let identifier = promiseLine.ItemID + '-' + promiseLine.LineId;
                let availability = {};
                availability.numberOfRecordAvailable =
                  promiseLine.Assignments && promiseLine.Assignments.TotalNumberOfRecords ? promiseLine.Assignments.TotalNumberOfRecords : null;
                availability.LineId = promiseLine.LineId;
                availability.ItemID = promiseLine.ItemID;
                availabiltyMap.put(identifier, availability);
              }
            }
          });
        }
      }
      if (suggestionOption && suggestionOption.UnavailableLines) {
        unAvailableLines = suggestionOption.UnavailableLines;
        if (unAvailableLines && unAvailableLines.UnavailableLine && unAvailableLines.UnavailableLine.length > 0) {
          unAvailableLines.UnavailableLine.forEach(function (unavailableLine) {
            let identifier = unavailableLine.ItemID + '-' + unavailableLine.LineId;
            let unAvailability = {};
            unAvailability.numberOfRecordAvailable = unavailableLine.AssignedQty ? new Number(unavailableLine.AssignedQty) : 0;
            unAvailability.LineId = unavailableLine.LineId;
            unAvailability.ItemID = unavailableLine.ItemID;
            unavailabilityMap.put(identifier, unAvailability);
          });
        }
      }
      //Code to check if there is any other error in the response
      if (response.PromiseLines) {
        errorPromiseLines = response.PromiseLines;
        if (errorPromiseLines && errorPromiseLines.PromiseLine && errorPromiseLines.PromiseLine.length > 0) {
          errorPromiseLines.PromiseLine.forEach(function (unavailableLine) {
            let identifier = unavailableLine.ItemID + '-' + unavailableLine.LineId;
            var err = unavailableLine.Error;
            var errCodes = ServiceConstants.INVENTORY_EERROR_CODES;
            var errorCode = unavailableLine.Error.ErrorCode;
            if (unavailableLine.Error && ServiceConstants.INVENTORY_EERROR_CODES.indexOf(unavailableLine.Error.ErrorCode) != -1) {
              let unAvailability = {};
              unAvailability.numberOfRecordAvailable = unavailableLine.AssignedQty ? new Number(unavailableLine.AssignedQty) : 0;
              unAvailability.LineId = unavailableLine.LineId;
              unAvailability.ItemID = unavailableLine.ItemID;
              unavailabilityMap.put(identifier, unAvailability);
            }
          });
        }
      }
    }
  }
  parsedResponse.availabilityMap = availabiltyMap;
  parsedResponse.unAvailabilityMap = unavailabilityMap;
  return parsedResponse;
};

/** prepares reserve inventory response
 * @param {Object} responseObj - object with productLineItems
 * @returns {Object} parsedResponse - available and unavailable response details map object.
 */

var parseReserveInventoryResponse = function (responseObj) {
  var HashMap = require('dw/util/HashMap');
  var availabiltyMap = new HashMap();
  var unavailabilityMap = new HashMap();
  var reservationIDs = [];
  var parsedResponse = {};
  var invalidRequest = false;
  var response = JSON.parse(responseObj);
  if (response) {
    parsedResponse.ResponseCode = response.ResponseCode;
    if (response && response.ResponseCode && response.ResponseCode === '0') {
      var promiseLines = response.PromiseLines;
      if (response && promiseLines) {
        if (promiseLines.PromiseLine && promiseLines.PromiseLine.length > 0) {
          promiseLines.PromiseLine.forEach(function (promiseLine) {
            let identifier = promiseLine.ItemID + '-' + promiseLine.LineId;
            let requiredData = {};
            requiredData.FulfillmentType = promiseLine.FulfillmentType;
            let reservations = promiseLine.Reservations;
            if (reservations) {
              if (reservations.Reservation && reservations.Reservation.length > 0) {
                reservations.Reservation.forEach(function (reservation) {
                  requiredData.productAvailabilityDate = reservation.ProductAvailabilityDate;
                  requiredData.shipDate = reservation.ShipDate;
                  if (reservation.ShipNode) {
                    requiredData.shipNode = reservation.ShipNode;
                    var shipNode = reservation.ShipNode;
                    if (shipNode.ShipNode) {
                      requiredData.shipNode = shipNode.ShipNode;
                    } else {
                      requiredData.shipNode = shipNode;
                    }
                  }
                  requiredData.reservationID = reservation.ReservationID;
                  requiredData.availableQty = reservation.ReservedQty;
                  requiredData.inventoryReservationExpirationTime = reservation.ExpirationDate;
                  reservationIDs.push(reservation.ReservationID);
                });
              }
              if (reservations.AvailableQty && reservations.QtyToBeReserved && Number(reservations.AvailableQty) >= Number(reservations.QtyToBeReserved)) {
                availabiltyMap.put(identifier, requiredData);
              } else {
                requiredData.availableQty = reservations.AvailableQty ? Number(reservations.AvailableQty) : 0;
                unavailabilityMap.put(promiseLine.ItemID, requiredData);
              }
            } else {
              let errors = promiseLine.Errors;
              if (errors && errors.Error && errors.Error.length > 0) {
                requiredData.availableQty = 0;
                unavailabilityMap.put(promiseLine.ItemID, requiredData);
              }
            }
          });
        }
      }
    } else if (response && response.ResponseCode && response.ResponseCode !== '0') {
      invalidRequest = true;
    }
  }

  parsedResponse.availabilityMap = availabiltyMap;
  parsedResponse.unAvailabilityMap = unavailabilityMap;
  parsedResponse.reservationIDs = reservationIDs;
  parsedResponse.invalidRequest = invalidRequest;
  return parsedResponse;
};

/** Updates required response to product line item custom attributes.
 * @param {dw.util.HashMap} availbleItemsMap - available products map.
 * @param {dw.order.Order} order - Order object.
 *
 */
var updateDetailsToPlis = function (availbleItemsMap, order) {
  var Transaction = require('dw/system/Transaction');
  var collections = require('*/cartridge/scripts/util/collections');
  var pliKey;
  var responseObj;
  collections.forEach(order.getAllProductLineItems(), function (pli, index) {
    pliKey = pli.productID + '-' + pli.position;
    responseObj = availbleItemsMap.get(pliKey);
    if (responseObj) {
      Transaction.wrap(function () {
        if (index === 0) {
          order.custom.reservationID = responseObj.reservationID ? responseObj.reservationID : ''; // eslint-disable-line
        }
        /* eslint-disable */
        pli.custom.productAvailabilityDate = responseObj.productAvailabilityDate ? responseObj.productAvailabilityDate : '';
        pli.custom.shipDate = responseObj.shipDate ? responseObj.shipDate : '';
        pli.custom.availableQty = responseObj.availableQty ? responseObj.availableQty : '';
        pli.custom.shipNode = responseObj.shipNode ? responseObj.shipNode : '';
        pli.custom.reservationID = responseObj.reservationID ? responseObj.reservationID : '';
        pli.custom.fulfillmentType = responseObj.FulfillmentType ? responseObj.FulfillmentType : '';
        /* eslint-enable */
      });
    }
  });
};

/** prepares cancel reservation request
 * @param {String} reservationID - Reservation id of the order
 * @returns {Object} reqData - returns request object for the service.
 */

var prepareCancelcancelReservationRequest = function (reservationID) {
  var promise = {};
  var reqData = {};
  var reservationParameters = {};

  promise.Action = ServiceConstants.INVENTORY_ACTION_CANCEL_RESERVE;
  promise.EnterpriseCode = ServiceConstants.ENTERPRISE_CODE;
  reservationParameters.ReservationID = reservationID;
  promise.ReservationParameters = reservationParameters;
  reqData.Promise = promise;
  return JSON.stringify(reqData);
};

/** Parse cancel reservation request
 * @param {Object} responseObj - Service response.
 * @returns {Object} reqData - returns request object for the service.
 */

var parsecancelReservationResponse = function (responseObj) {
  var response = JSON.parse(responseObj);
  var parsedResponse = {};
  parsedResponse.success = true;
  parsedResponse.responseCode = response.ResponseCode;
  if (response.ResponseCode !== '0') {
    parsedResponse.success = false;
  }
  return parsedResponse;
};

/** Create fallback data in custom object.
 *
 * @param {String} reservationID - Reservation ID.
 *
 */

var createCancelReservationDataInCO = function (reservationID) {
  var Transaction = require('dw/system/Transaction');
  Transaction.wrap(function () {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var cObj = CustomObjectMgr.getCustomObject('CancellReservation', reservationID);
    if (!cObj) {
      cObj = CustomObjectMgr.createCustomObject('CancellReservation', reservationID);
    }
    cObj.custom.reservationExpirationTime = getExpiryDate().getTime();
  });
  return;
};

module.exports = {
  addServiceHeaders: addServiceHeaders,
  logRequest: logRequest,
  prepareRequest: prepareRequest,
  getMockedResponse: getMockedResponse,
  parseInvLookUpResponse: parseInvLookUpResponse,
  parseReserveInventoryResponse: parseReserveInventoryResponse,
  updateDetailsToPlis: updateDetailsToPlis,
  prepareCancelcancelReservationRequest: prepareCancelcancelReservationRequest,
  parsecancelReservationResponse: parsecancelReservationResponse,
  createCancelReservationDataInCO: createCancelReservationDataInCO
};
