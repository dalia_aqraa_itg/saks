/* API Modules */
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
/* Script Modules */
var ServiceHelper = require('*/cartridge/scripts/services/helpers/ServiceHelper');

/**
 * Create service object
 */
module.exports = {
  initService: function (serviceName) {
    return LocalServiceRegistry.createService(serviceName, {
      createRequest: function (svc, args) {
        svc.setRequestMethod('POST'); // eslint-disable-next-line no-param-reassign
        svc = ServiceHelper.addServiceHeaders(svc); // eslint-disable-line
        return args;
      },

      parseResponse: function (svc, response) {
        return response;
      },

      // eslint-disable-next-line no-unused-vars
      mockCall: function (svc, client) {
        var mockedReponse = ServiceHelper.getMockedResponse(serviceName);
        return {
          statusCode: 200,
          statusMessage: 'OK',
          text: mockedReponse
        };
      },

      filterLogMessage: function (logMsg) {
        return logMsg.replace('headers', 'OFFWITHTHEHEADERS');
      }
    });
  }
};
