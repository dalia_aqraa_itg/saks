/* API Modules */
var Result = require('dw/svc/Result');
var Logger = require('dw/system/Logger');

/* Script Modules */
var ServiceHelper = require('*/cartridge/scripts/services/helpers/ServiceHelper');
var ServiceConstants = require('*/cartridge/scripts/services/helpers/ServiceConstants');
var InventoryServiceInit = require('*/cartridge/scripts/services/init/InventoryServiceInit');

// function, call inventory lookup service and returns the result
exports.callGetInventoryAvailabilityService = function (getAvailabilityRequest) {
  var serviceResponse = {};

  try {
    var svc = InventoryServiceInit.initService(ServiceConstants.INVENTORY_AVAILABILITY);
    var result = svc.call(getAvailabilityRequest);
    var response;
    var httpClient = null;
    var statusCode;

    // Check for service availability.
    if (result.status === Result.SERVICE_UNAVAILABLE) {
      Logger.getRootLogger().fatal(
        '[InventoryServiceUtils.js callGetInventoryAvailabilityService] Error : SERVICE_UNAVAILABLE response {0}',
        result.errorMessage
      );
    }

    // Process Response
    if (result.object) {
      httpClient = result.object;
      response = httpClient.getText();
      statusCode = httpClient.getStatusCode();
    }

    if (empty(response)) {
      ServiceHelper.logRequest(svc.requestData);
      throw new Error('No response from webservice.');
    }

    // Check for 200 status code
    if (statusCode === 200) {
      serviceResponse.result = response;
      serviceResponse.status = 'OK';
    } else {
      Logger.error(
        'Error while executing the script InventoryServiceUtils.js callGetInventoryAvailabilityService Error statusCode!=200...' + httpClient
          ? httpClient.getText()
          : ''
      );
    }

    ServiceHelper.logRequest(svc.requestData);
    // return service response;
    return serviceResponse;
  } catch (e) {
    // log script exceptions
    serviceResponse.status = 'ERROR';
    serviceResponse.result = null;
    Logger.error('Error while executing the script InventoryServiceUtils.js. callGetInventoryAvailabilityService Error...' + e.message);
    return serviceResponse;
  }
};

// function, call inventory reservation and cancellation service and returns the result
exports.callGetInventoryReservation = function (getReservationRequest) {
  var serviceResponse = {};
  // Get the service object
  try {
    var svc = InventoryServiceInit.initService(ServiceConstants.INVENTORY_RESERVATION);
    var result = svc.call(getReservationRequest);
    var response;
    var httpClient = null;
    var statusCode;

    // Check for service availability.
    if (result.status === Result.ERROR) {
      serviceResponse.status = 'ERROR';
    }
    if (result.status === Result.SERVICE_UNAVAILABLE) {
      serviceResponse.serviceUnavailable = true;
      Logger.getRootLogger().fatal('[InventoryServiceUtils.js callGetInventoryReservation] Error : SERVICE_UNAVAILABLE response {0}', result.errorMessage);
    }

    // Process Response
    if (result.object) {
      httpClient = result.object;
      response = httpClient.getText();
      statusCode = httpClient.getStatusCode();
    }

    if (empty(response)) {
      ServiceHelper.logRequest(svc.requestData);
      throw new Error('No response from webservice.');
    }

    // Check for 200 status code
    if (statusCode === 200) {
      serviceResponse.result = response;
      serviceResponse.status = 'OK';
    } else {
      Logger.error(
        'Error while executing the script InventoryServiceUtils.js callGetInventoryReservation Error statusCode!=200...' + httpClient
          ? httpClient.getText()
          : ''
      );
    }

    ServiceHelper.logRequest(svc.requestData);
    // return service response;
    return serviceResponse;
  } catch (e) {
    // log script exceptions
    Logger.error('Error while executing the script InventoryServiceUtils.js. callGetInventoryReservation Error...' + e.message);
    return serviceResponse;
  }
};

exports.callCancelInventoryReservationService = function (cancelReservationRequest) {
  var serviceResponse = {};
  // Get the service object
  try {
    var svc = InventoryServiceInit.initService(ServiceConstants.INVENTORY_RESERVATION);
    var result = svc.call(cancelReservationRequest);
    var response;
    var httpClient = null;
    var statusCode;

    // Check for service availability.
    if (result.status === Result.SERVICE_UNAVAILABLE) {
      Logger.getRootLogger().fatal(
        '[InventoryServiceUtils.js callCancelInventoryReservationService] Error : SERVICE_UNAVAILABLE response {0}',
        result.errorMessage
      );
    }

    // Process Response
    if (result.object) {
      httpClient = result.object;
      response = httpClient.getText();
      statusCode = httpClient.getStatusCode();
    }

    if (empty(response)) {
      ServiceHelper.logRequest(svc.requestData);
      throw new Error('No response from webservice.');
    }

    // Check for 200 status code
    if (statusCode === 200) {
      serviceResponse.result = response;
      serviceResponse.status = 'OK';
    } else {
      Logger.error(
        'Error while executing the script InventoryServiceUtils.js callCancelInventoryReservationService Error statusCode!=200...' + httpClient
          ? httpClient.getText()
          : ''
      );
    }

    ServiceHelper.logRequest(svc.requestData);
    // return service response;
    return serviceResponse;
  } catch (e) {
    // log script exceptions
    Logger.error('Error while executing the script InventoryServiceUtils.js. callCancelInventoryReservationService Error...' + e.message);
    return serviceResponse;
  }
};
