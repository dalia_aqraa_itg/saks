'use strict';

/**
 * OrderReconciliation Feed
 *
 * ExportOrders Job Implementation
 *
 * Processes and exports a CSV in chunk format. Progress can be seen in job run
 * status
 */

var Status = require('dw/system/Status');
var io = require('dw/io');
var File = require('dw/io/File');
var OrderMgr = require('dw/order/OrderMgr');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var Transaction = require('dw/system/Transaction');

var OrderRecoConfig = require('*/cartridge/scripts/config/ConfigOrderReco');
var OrderRecoSFTPService = require('*/cartridge/scripts/services/orderRecoSFTPService');
var OrderRecoHelper = require('*/cartridge/scripts/helpers/OrderRecoHelper');

var orders;
var orderList;
var fileWriter;
var csvWriter;
var destFile;

/**
 * Initialize readers and writers for job processing
 *
 * @param {Object}
 *            parameters job parameters
 * @param {JobStepExecution}
 *            stepExecution job step execution
 * @returns {Status} if error returns status
 */
exports.beforeStep = function (parameters) {
  var recoConstants = OrderRecoConfig.getConstants();
  var ArrayList = require('dw/util/ArrayList');
  orderList = new ArrayList();
  var orderRecoFileName = parameters.orderRecoFeedFileName ? parameters.orderRecoFeedFileName : recoConstants.ORDER_RECO_FILE_NAME;
  var orderRecoFolder = parameters.orderRecoFolder ? parameters.orderRecoFolder : recoConstants.IMPEX_ORDER_RECO_PATH;

  var destDir = new File(File.IMPEX + recoConstants.SRC_FOLDER + orderRecoFolder);
  if (!destDir.exists()) {
    destDir.mkdirs();
  }

  var calendar = new Calendar();
  calendar.timeZone = 'GMT';
  var gmtDateString = StringUtils.formatCalendar(calendar, 'yyyy-MM-dd_HH-mm-ss');
  var exportFileName = orderRecoFileName + '_' + gmtDateString + recoConstants.EXTENSION;
  destFile = new io.File(destDir, exportFileName);
  fileWriter = new io.FileWriter(destFile);
  csvWriter = new io.CSVStreamWriter(fileWriter);

  csvWriter.writeNext([
    'Order Line ID',
    'Order Number',
    'Order Date',
    'Item ID',
    'Exported Date',
    'Order Line Dollar Amount',
    'Delivery Method',
    'Shipping charge',
    'Discount',
    'PromotionCode',
    'Tender Type',
    'Status',
    'HoldType',
    'HoldStatus'
  ]);
  orders = OrderMgr.queryOrders('custom.reconciled = {0} OR custom.reconciled = NULL', null, false);

  return undefined;
};

exports.getTotalCount = function () {
  return orders.count;
};

exports.read = function () {
  if (orders.hasNext()) {
    return orders.next();
  }
  return undefined;
};
/**
 * fetch  processed order
 * Process function in chunk oriented script module
 * @param {dw.order.Order} order demandware order
 * @returns {Array} returns all processed order
 */
exports.process = function (order) {
  if (!empty(order)) {
    var tenderType = OrderRecoHelper.getTenderType(order);
    var holdType = 'holdType' in order.custom && order.custom.holdType ? order.custom.holdType : '';
    var holdStatus = 'holdStatus' in order.custom && order.custom.holdStatus ? order.custom.holdStatus : '';
    var Order = require('dw/order/Order');
    var exportedDate = order.status.value !== Order.ORDER_STATUS_FAILED ? order.creationDate : '';
    orderList.add(order);
    var promoCodes = OrderRecoHelper.getPromotionIds(order);
    var lines = [];
    var orderItems = order.getProductLineItems().iterator();
    while (orderItems.hasNext()) {
      var orderItem = orderItems.next();
      var product = orderItem.getProduct();
      var line = [];
      if (!empty(product)) {
        line[0] = order.orderNo + '-' + orderItem.position;
        line[1] = order.orderNo;
        line[2] = order.creationDate;
        line[3] = product.ID;
        line[4] = exportedDate; // new order custom attribute
        line[5] = orderItem.grossPrice;
        line[6] = 'deliveryMethod' in orderItem.custom && orderItem.custom.deliveryMethod ? orderItem.custom.deliveryMethod : ''; // new product line item custom attribute
        line[7] = order.shippingTotalGrossPrice;
        line[8] = orderItem.grossPrice.value - orderItem.proratedPrice.value;
        line[9] = promoCodes;
        line[10] = tenderType; // new order custom attribute
        line[11] = order.status.displayValue;
        line[12] = holdType; // new order custom attribute
        line[13] = holdStatus; // new order custom attribute
        lines.push(line);
      }
    }

    return lines;
  }

  return undefined;
};

exports.write = function (chunk) {
  for (var i = 0; i < chunk.size(); i++) {
    var orderLines = chunk.get(i);
    for (var j = 0; j < orderLines.size(); j++) {
      var orderLine = orderLines.get(j);
      if (!empty(orderLine)) {
        csvWriter.writeNext(orderLine.toArray());
      }
    }
  }
};

exports.afterChunk = function () {
  fileWriter.flush();
};

exports.afterStep = function () {
  csvWriter.close();
  fileWriter.close();
  orders.close();
  var zipFile = new File(destFile.fullPath + '.gz');
  destFile.gzip(zipFile);
  destFile.remove();
  var collections = require('*/cartridge/scripts/util/collections');
  collections.forEach(orderList, function (order) {
    Transaction.wrap(function () {
      // set reconcile flag to true
      order.custom.reconciled = true; // eslint-disable-line
    });
  });
  orderList.removeAll(orderList);
};

/**
 *
 * Step 2: send generated file to sftp server and archive the file
 * @param {Object} args : Job Object
 * @param {dw.job.JobStepExecution} jobStepExecution : dw.job.JobStepExecution
 * @returns {dw.system.Status} Status : dw.system.Status
 */
exports.sftpUpload = function (args, jobStepExecution) {
  var recoConstants = OrderRecoConfig.getConstants();
  var orderRecoFolder = jobStepExecution.getParameterValue('orderRecoFolder') || recoConstants.IMPEX_ORDER_RECO_PATH;
  var sourceDir = new File(File.IMPEX + recoConstants.SRC_FOLDER + orderRecoFolder);
  var recoFeedName = jobStepExecution.getParameterValue('orderRecoFeedFileName') || recoConstants.ORDER_RECO_FILE_NAME;
  var sftpFolder = jobStepExecution.getParameterValue('orderRecoSFTPLocation');
  var archiveFolder = jobStepExecution.getParameterValue('archiveFolder') || recoConstants.ORDER_RECO_ARCHIVE_FOLDER;
  var collections = require('*/cartridge/scripts/util/collections');
  collections.forEach(sourceDir.listFiles(), function (recoFile) {
    if (recoFile.name.indexOf(recoFeedName) !== -1) {
      // Upload to SFTP location
      var svc = OrderRecoSFTPService.get();
      svc.setThrowOnError();
      svc.setOperation('putBinary', [sftpFolder + recoFile.name, recoFile]);
      svc.call();

      // archive the file after upload
      var archiveFileDir = new File(sourceDir.fullPath + File.SEPARATOR + archiveFolder);
      if (!archiveFileDir.exists()) {
        archiveFileDir.mkdirs();
      }
      var archiveFile = new File(archiveFileDir.fullPath + File.SEPARATOR + recoFile.name);
      recoFile.renameTo(archiveFile);
    }
  });
  return new Status(Status.OK);
};

/**
 *
 * Step 3: cleanup the files in archive folder which are certain number of days old
 * @param {Object} args : Job Object
 * @param {dw.job.JobStepExecution} jobStepExecution : dw.job.JobStepExecution
 * @returns {dw.system.Status} Status : dw.system.Status
 */
exports.cleanupArchive = function (args, jobStepExecution) {
  var recoConstants = OrderRecoConfig.getConstants();
  var orderRecoFolder = jobStepExecution.getParameterValue('orderRecoFolder') || recoConstants.IMPEX_ORDER_RECO_PATH;
  var archiveFolder = jobStepExecution.getParameterValue('archiveFolder') || recoConstants.ORDER_RECO_ARCHIVE_FOLDER;
  var daysToBeArchived = jobStepExecution.getParameterValue('daysToBeArchived') || recoConstants.ORDER_RECO_ARCHIVE_DAYS;
  var archiveDir = new File(File.IMPEX + recoConstants.SRC_FOLDER + orderRecoFolder + File.SEPARATOR + archiveFolder);
  if (!archiveDir.exists()) {
    throw new Error('Folder Archive ' + archiveDir.fullPath + ' does not exist.');
  }
  var oldCalendar = new Calendar();
  oldCalendar.add(Calendar.DAY_OF_YEAR, -1 * daysToBeArchived);
  var listFiles = archiveDir.listFiles();
  var collections = require('*/cartridge/scripts/util/collections');
  collections.forEach(listFiles, function (archiveFile) {
    var fileCalendar = new Calendar(new Date(archiveFile.lastModified()));
    if (fileCalendar.before(oldCalendar)) {
      archiveFile.remove();
    }
  });
  return new Status(Status.OK);
};
