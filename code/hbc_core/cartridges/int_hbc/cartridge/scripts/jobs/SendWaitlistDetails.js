var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var CSVStreamWriter = require('dw/io/CSVStreamWriter');
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');

/**
 * Create File Name for WaitList Feed
 * @param {string} fileName - creating fileName
 * @returns {dw.io.File} file - returning file
 */
function createFile(fileName) {
  var targetDirectory = new File(File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'WaitList');
  targetDirectory.mkdirs();
  var file = new File(File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'WaitList' + File.SEPARATOR + fileName);
  file.createNewFile();
  return file;
}

/**
 * Generate WaitList feed
 */
function generateWaitListFeed() {
  var params = arguments[0];
  // Query All WaitList Object which are not exported.
  var customObjectsItr = CustomObjectMgr.queryCustomObjects('WaitlistDetails', 'custom.exported = {0}', null, false);
  if (customObjectsItr.count > 0) {
    // Get CSV File Writer
    var timeStamp = StringUtils.formatCalendar(new Calendar(), 'yyyy-MM-dd_HH-mm-ss');
    var writer = new CSVStreamWriter(new FileWriter(createFile('WaitListFeed_' + timeStamp + '.csv')), ',');
    if (writer) {
      var headerArr = [];
      headerArr[0] = 'emailId';
      headerArr[1] = 'productId';
      headerArr[2] = 'phoneNumber';
      headerArr[3] = 'date';
      if ('includeSourceId' in params && params.includeSourceId == 'TRUE') {
        headerArr[4] = 'sourceId';
      }
      if ('includeChannelId' in params && params.includeChannelId == 'TRUE') {
        headerArr[5] = 'channelId';
      }

      // Write Header
      writer.writeNext(headerArr);

      // Write Row
      Transaction.wrap(function () {
        while (customObjectsItr.hasNext()) {
          var co = customObjectsItr.next();
          var data = [];
          data[0] = co.custom.emailId;
          data[1] = co.custom.productId;
          data[2] = !empty(co.custom.phoneNumber) ? co.custom.phoneNumber : '';
          data[3] = co.custom.date;
          if ('includeSourceId' in params && params.includeSourceId == 'TRUE') {
            data[4] = co.custom.sourceId.displayValue;
          }
          if ('includeChannelId' in params && params.includeChannelId == 'TRUE') {
            data[5] = co.custom.channelId;
          }
          writer.writeNext(data);
          // Mark it exported
          co.custom.exported = true;
        }
      });
      // Close Write
      writer.close();
    }
  }
}

exports.generateWaitListFeed = generateWaitListFeed;
