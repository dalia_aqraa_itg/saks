'use strict';

var Status = require('dw/system/Status');
var Transaction = require('dw/system/Transaction');
var ProductMgr = require('dw/catalog/ProductMgr');
var ProductSearchModel = require('dw/catalog/ProductSearchModel');
var Calendar = require('dw/util/Calendar');
var Logger = require('dw/system/Logger');

exports.execute = function (args, jobStepExecution) {
  try {
    var currentTime = new Calendar();
    var psm = new ProductSearchModel();
    //psm.setOrderableProductsOnly(true);
    psm.setCategoryID('root');
    psm.setRecursiveCategorySearch(true);
    psm.setSearchPhrase('*');
    //psm.addRefinementValues('hbcProductType', preferences.nonChanelProductTypes);
    psm.search();
    var it = psm.getProductSearchHits();
    while (it.hasNext()) {
      var hit = it.next();
      // Assuming the Product will have preOrder PIM attribute is set.
      var product = hit.product;
      if (product && 'preOrder' in product.custom && (product.custom.preOrder === 'T' || product.custom.preOrder === 'Y')) {
        if (product.master) {
          var variants = product.variationModel.variants;
          if (variants.length > 0) {
            var allVariantPreOrderable = true;
            for (var i = 0; i < variants.length; i++) {
              var variant = variants[i];
              var availabilityModel = variant.availabilityModel;
              if (availabilityModel && !empty(availabilityModel)) {
                var inventoryRecord = availabilityModel.inventoryRecord;
                /** Rules
                 * 1. On Hand Inventory could be 0 or more than 0.
                 * 2. Future hand Inventory is greater than 0.
                 * 3. Future on Hand - InStock date is in Future date of current time.
                 *  */
                if (
                  !empty(inventoryRecord) &&
                  (inventoryRecord.preorderable || inventoryRecord.backorderable) &&
                  inventoryRecord.preorderBackorderAllocation.value > 0 &&
                  !empty(inventoryRecord.inStockDate)
                ) {
                  var inStockTime = new Calendar(inventoryRecord.inStockDate);
                  if (inStockTime.compareTo(currentTime) >= 0) {
                    variant.custom.sfccPreorder = 'T';
                  } else {
                    variant.custom.sfccPreorder = 'F';
                    allVariantPreOrderable = false;
                  }
                } else {
                  variant.custom.sfccPreorder = 'F';
                  allVariantPreOrderable = false;
                }
              } else {
                variant.custom.sfccPreorder = 'F';
                allVariantPreOrderable = false;
              }
            }
            // If all variation are not Pre Orderable, master will SFCC Preorder msut be false;
            if (!allVariantPreOrderable) {
              product.custom.sfccPreorder = 'F';
            } else {
              product.custom.sfccPreorder = 'T';
            }
          } else {
            product.custom.sfccPreorder = 'F';
          }
        } else {
          // for Other types of Product, If PIM Pre Order is Y, update the SFCC Pre Order
          var availabilityModel = product.availabilityModel;
          if (availabilityModel && !empty(availabilityModel)) {
            var inventoryRecord = availabilityModel.inventoryRecord;
            /** Rules
             * 1. On Hand Inventory could be 0 or more than 0.
             * 2. Future hand Inventory is greater than 0.
             * 3. Future on Hand - InStock date is in Future date of current time.
             *  */

            if (
              !empty(inventoryRecord) &&
              (inventoryRecord.preorderable || inventoryRecord.backorderable) &&
              inventoryRecord.preorderBackorderAllocation.value > 0 &&
              !empty(inventoryRecord.inStockDate)
            ) {
              var inStockTime = new Calendar(inventoryRecord.inStockDate);
              if (inStockTime.compareTo(currentTime) >= 0) {
                product.custom.sfccPreorder = 'T';
              } else {
                product.custom.sfccPreorder = 'F';
              }
            } else {
              product.custom.sfccPreorder = 'F';
            }
          } else {
            product.custom.sfccPreorder = 'F';
          }
        }
      } else {
        // If Product is not applcable for Pre Orde, make sure SFCC Pre Order is set to false.
        product.custom.sfccPreorder = 'F';
      }
    }
  } catch (e) {
    Logger.error(
      'There was an error while SAKS Pre Order Update: ' + e.fileName + '| line#: ' + e.lineNumber + '| Message: ' + e.message + '| Stack: ' + e.stack
    );
  }
};
