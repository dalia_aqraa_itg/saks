var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');

/**
 * Delete WaitList exported custom Object
 */
function deleteWaitListCO() {
  var customObjectsItr = CustomObjectMgr.queryCustomObjects('WaitlistDetails', 'custom.exported = {0}', null, true);
  if (customObjectsItr.count > 0) {
    Transaction.wrap(function () {
      while (customObjectsItr.hasNext()) {
        var co = customObjectsItr.next();
        CustomObjectMgr.remove(co);
      }
    });
  }
}

exports.deleteWaitListCO = deleteWaitListCO;
