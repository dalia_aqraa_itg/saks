'use strict';
/**
 *
 * Skip Step Job Implementation
 * Processes or cancel Job execution for certain time or hour.
 *
 */

var Status = require('dw/system/Status');
var Logger = require('dw/system/Logger');

exports.Check = function () {
  var params = arguments[0];

  if (!params) {
    // no params found, hence the job step can proceed and need not stop the job execution
    return new Status(Status.ERROR, 'ERROR', "No params. Don't skip");
  }

  var hoursToSkip = params.Hours_To_Skip;

  if (!hoursToSkip) {
    // no date parameter found, hence the job step can proceed and need not stop the job execution
    return new Status(Status.ERROR, 'ERROR', "No hoursToSkip. Don't skip");
  }
  var currentDate = dw.system.Site.getCalendar();
  var currentHour = currentDate.get(dw.util.Calendar.HOUR_OF_DAY);
  var str = '|' + currentHour + '|';

  if (hoursToSkip.indexOf(str) > -1) {
    Logger.info(
      'Skipping the Job Execution, as job step is configured to  skip jobs at a certain hour or time. See Skip Step parameters for the duration details'
    );
    return new Status(Status.OK, 'OK', 'Skip Job');
  }

  return new Status(Status.ERROR, 'ERROR', "Don't skip");
};
