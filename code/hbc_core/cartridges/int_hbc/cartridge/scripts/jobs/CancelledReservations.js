'use strict';
/**
 *
 * CancellReservation Job Implementation
 * Processes and cancel reserve the reserved inventory.
 *
 */

var Status = require('dw/system/Status');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Calendar = require('dw/util/Calendar');
var Transaction = require('dw/system/Transaction');
var Logger = require('dw/system/Logger');

var hooksHelper = require('*/cartridge/scripts/helpers/hooks');

exports.Process = function () {
  var cancelledTransactionObj;
  var reservationExpirationTime;
  var objResponse;
  var cal;
  var currentTime;
  var reservationID;
  // Query all custom objects.
  var cancelledTransactions = CustomObjectMgr.getAllCustomObjects('CancellReservation');

  while (cancelledTransactions.hasNext()) {
    // Get the custom object.
    cancelledTransactionObj = cancelledTransactions.next();
    reservationExpirationTime = cancelledTransactionObj.custom.reservationExpirationTime;
    reservationID = cancelledTransactionObj.custom.reservationID;

    // Get current time.
    cal = new Calendar();
    cal.setTimeZone('Etc/UTC');
    currentTime = cal.getTime();

    // Check if reservation has expired or not.
    if (reservationExpirationTime > currentTime) {
      try {
        // Call the service.
        objResponse = hooksHelper('app.inventory.reservation', 'cancelInventoryReservation', [reservationID]);
        if (objResponse && objResponse.success) {
          /* eslint-disable */
          Transaction.wrap(function () {
            CustomObjectMgr.remove(cancelledTransactionObj);
          });
          /* eslint-enable */
        }
      } catch (e) {
        Logger.error('Error InventoryService.cancelReservation() : ' + e);
      }
    } else {
      // remove the custom object if reservation has already expired.
      /* eslint-disable */
      Transaction.wrap(function () {
        CustomObjectMgr.remove(cancelledTransactionObj);
      });
      /* eslint-enable */
    }
  }
  return new Status(Status.OK);
};
