'use strict';

var InventoryService = require('*/cartridge/scripts/services/InventoryService');
var collections = require('*/cartridge/scripts/util/collections');

/**
 * get the shipment map with shippingUUID as key and shipment as value.
 * @param {dw.order.Order} order - The current order
 * @returns {dw.util.HashMap} shipmentMap - newly created map.
 */
function getShipmentMap(order) {
  var HashMap = require('dw/util/HashMap');
  var shipmentMap = new HashMap();
  collections.forEach(order.shipments, function (shipment) {
    shipmentMap.put(shipment.UUID, shipment);
  });
  return shipmentMap;
}

/**
 * updates the shipping address details to item object
 * @param {Object} productLineItems - productLinteItems modal object
 * @param {dw.util.HashMap} shipmentMap - Shipping Map
 */
function updateShippingAddressToItem(productLineItems, shipmentMap) {
  productLineItems.items.forEach(function (item) {
    let shipment = shipmentMap.get(item.shipmentUUID);
    let shippingAddress = shipment.shippingAddress;
    if (shippingAddress != null) {
      if (shippingAddress.countryCode != null) {
        item.shippingCountryCode = shippingAddress.countryCode.value; // eslint-disable-line
      }
      if (shippingAddress.stateCode != null) {
        item.shippingStateCode = shippingAddress.stateCode; // eslint-disable-line
      }
      if (shippingAddress.postalCode != null) {
        if (shippingAddress.countryCode != null && shippingAddress.countryCode.value === 'US') {
          item.shippingPostalCode = shippingAddress.postalCode.substr(0, 5); // eslint-disable-line
        } else {
          item.shippingPostalCode = shippingAddress.postalCode; // eslint-disable-line
        }
      }
    }
  });
}

/**
 * Reserves the inventory of the products in SFCC OMS Order Management.
 * @param {dw.order.Order} order - The current user's order
 * @returns {Object} an availability
 */
function reserveInventory(order) {
  var ProductLineItemsModel = require('*/cartridge/models/productLineItems');
  var productLineItems = new ProductLineItemsModel(order.productLineItems, 'basket');
  var shipmentMap = getShipmentMap(order);
  updateShippingAddressToItem(productLineItems, shipmentMap);
  return InventoryService.reserveInventory(productLineItems, 'reserve', order);
}

/**
 * Cancels the reserved inventory of the products in SFCC OMS Order Management.
 * @param {string} reservationID - Reservation ID of the order.
 * @returns {Object} an availability
 */
function cancelInventoryReservation(reservationID) {
  if (reservationID) {
    return InventoryService.cancelReservation(reservationID);
  }
  return {};
}

module.exports = {
  reserveInventory: reserveInventory,
  cancelInventoryReservation: cancelInventoryReservation
};
