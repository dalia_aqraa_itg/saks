/**
 *
 * OrderReconciliation Util functions
 */

var OrderRecoConfig = require('*/cartridge/scripts/config/ConfigOrderReco');

/**
 * Get promotion Ids
 * @param {Object} order demandware order
 * @return {Object} returns promotions
 */
function getPromotionIds(order) {
  var collections = require('*/cartridge/scripts/util/collections');
  var promotionIds = [];
  // get all order promotions
  var orderPromos = order.priceAdjustments;
  collections.forEach(orderPromos, function (orderPromo) {
    promotionIds.push(orderPromo.promotionID);
  });
  // get all coupons codes applied to order
  var couponItems = order.couponLineItems;
  collections.forEach(couponItems, function (couponItem) {
    promotionIds.push(couponItem.couponCode);
  });

  // get all shipping promotions
  var shippingPromos = order.shippingPriceAdjustments;
  collections.forEach(shippingPromos, function (shippingPromo) {
    promotionIds.push(shippingPromo.promotionID);
  });

  // get all coupons product promotions
  var productItems = order.allProductLineItems;
  collections.forEach(productItems, function (productItem) {
    var productPromos = productItem.priceAdjustments;
    collections.forEach(productPromos, function (productPromo) {
      promotionIds.push(productPromo.promotionID);
    });
  });

  return promotionIds.join(OrderRecoConfig.getConstants().DELIMITER);
}

/**
 * Get Order status
 * @param {Object} order Demandware order
 * @return {string} returns order status
 */
function getOrderStatus(order) {
  switch (order.status.value) {
    case 0:
      return 'CREATED';
    case 3:
      return 'NEW';
    case 4:
      return 'OPEN';
    case 5:
      return 'COMPLETED';
    case 6:
      return 'CANCELLED';
    case 7:
      return 'REPLACED';
    case 8:
      return 'FAILED';
    default:
      return null;
  }
}

/**
 * Get tender tyoe
 * @param {Object} order Demansware order
 * @return {Object} returns tender list
 */
function getTenderType(order) {
  var collections = require('*/cartridge/scripts/util/collections');
  var tenderTypes = [];
  // get all order payment instruments
  collections.forEach(order.paymentInstruments, function (instrument) {
    var paymentMethod = instrument.paymentMethod;
    if (paymentMethod === 'CREDIT_CARD') {
      tenderTypes.push(instrument.creditCardType);
    } else {
      tenderTypes.push(paymentMethod);
    }
  });
  return tenderTypes.join(OrderRecoConfig.getConstants().DELIMITER);
}

module.exports = {
  getPromotionIds: getPromotionIds,
  getOrderStatus: getOrderStatus,
  getTenderType: getTenderType
};
