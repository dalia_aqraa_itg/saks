'use strict';

var assert = require('chai').assert;
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();

var serviceObj = {
  authentication: '',
  serviceClient: '',
  getConfiguration: function () {
    return {
      getCredential: function () {
        return {
          getURL: function () {
            return 'configuration.url';
          }
        };
      }
    };
  }
};
describe('Order Reconciliation SFTP Service', function () {
  var orderRecoSFTPService = proxyquire('../../../../../cartridges/int_hbc/cartridge/scripts/services/orderRecoSFTPService', {
    'dw/svc/LocalServiceRegistry': {
      createService: function (serviceId, configObj) {
        return {
          call: function (params) {
            var createRequestResponse = configObj.createRequest(serviceObj, params);
            var executeRequestResponse = configObj.execute(serviceObj, createRequestResponse);

            return {
              status: 'OK',
              createRequest: createRequestResponse,
              execute: executeRequestResponse,
              object: {
                responseType: 'SUCCESS'
              }
            };
          }
        };
      }
    }
  });

  it('test Get service', function () {
    assert.isTrue(typeof orderRecoSFTPService === 'object');
  });
});
