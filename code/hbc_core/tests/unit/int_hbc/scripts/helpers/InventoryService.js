'use strict';

var assert = require('chai').assert;
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();

var serviceObj = {
  authentication: '',
  serviceClient: '',
  getConfiguration: function () {
    return {
      getCredential: function () {
        return {
          getURL: function () {
            return 'configuration.url';
          }
        };
      }
    };
  }
};

describe('Inventory Service', function () {
  var inventoryService = proxyquire('../../../../../cartridges/int_hbc/cartridge/scripts/services/InventoryService', {
    '*/cartridge/scripts/services/init/InventoryServiceUtils': {
      callGetInventoryAvailabilityService: function (a, b) {
        if (a == 'hasInventory') {
          var response = {
            status: 'OK',
            result: {
              hasInventory: true,
              omsInventory: null
            }
          };
          return response;
        } else {
          if (a == 'noInventory') {
            var response = {
              status: 'OK',
              result: {
                hasInventory: false,
                omsInventory: null
              }
            };
            return response;
          }
        }
      }
    },
    'dw/svc/LocalServiceRegistry': {
      createService: function (serviceId, configObj) {
        return {
          call: function (params) {
            var createRequestResponse = configObj.createRequest(serviceObj, params);
            var executeRequestResponse = configObj.execute(serviceObj, createRequestResponse);

            return {
              status: 'OK',
              createRequest: createRequestResponse,
              execute: executeRequestResponse,
              object: {
                responseType: 'SUCCESS'
              }
            };
          }
        };
      }
    },
    'dw/system/Logger': {
      error: function () {}
    },
    '*/cartridge/scripts/services/helpers/ServiceHelper': {
      prepareRequest: function (lineItems, action) {
        if (action == 1) {
          return 'hasInventory';
        } else {
          return 'noInventory';
        }
      },
      parseInvLookUpResponse: function (result) {
        var map = require('../../../../mocks/dw.util.HashMap');
        var obj = {
          unAvailabilityMap: map
        };
        // mock logic to override the existing logic.
        if (result.hasInventory) {
          map.put('key1', 'value1');
          map.put('key2', 'value2');
        }
        return obj;
      }
    }
  });

  it('return default availability results', function () {
    var result = inventoryService.getAvailability(0, 0);
    assert.equal(false, result.errorInService);
    assert.equal(true, result.hasInventory);
  });
});
