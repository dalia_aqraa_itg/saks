'use strict';

var assert = require('chai').assert;
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();

describe('Existing Configurations for OrderReco', function () {
  var constantConfig = proxyquire('../../../../../cartridges/int_hbc/cartridge/scripts/config/ConfigOrderReco', {});

  it('Configuration should be constant', function () {
    var constants = constantConfig.getConstants();
    var numberOfProperties = Object.keys(constants).length;

    assert.equal('object', typeof constants);
    assert.equal(7, numberOfProperties);
    assert.equal('order_reco', constants.IMPEX_ORDER_RECO_PATH);
    assert.equal('/src/', constants.SRC_FOLDER);
    assert.equal(',', constants.DELIMITER);
    assert.equal('.csv', constants.EXTENSION);
    assert.equal('oms_reconciliation_feed', constants.ORDER_RECO_FILE_NAME);
    assert.equal('archive', constants.ORDER_RECO_ARCHIVE_FOLDER);
    assert.equal(30, constants.ORDER_RECO_ARCHIVE_DAYS);
  });
});
