'use strict';

var assert = require('chai').assert;
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();

describe('TSYS helper', function () {
  before(function () {
    global.session = {
      custom: {
        tsysPreview: true
      }
    };
    global.dw = {
      system: {
        System: {
          instanceType: 1,
          PRODUCTION_SYSTEM: 1
        }
      }
    };
  });

  var tsysHelpers = proxyquire('../../../../../cartridges/app_hbc_core/cartridge/scripts/helpers/tsysHelpers', {
    'dw/system/Site': {
      current: {
        preferences: {
          custom: {
            tsysPhase2: true,
            dualAcceptanceEndSaks: true,
            dualAcceptanceEndBay: true
          }
        }
      },
      getCurrent: function () {
        return {
          ID: {
            toLowerCase: function () {
              return 'thebay';
            }
          }
        };
      }
    }
  });

  it('test isTsysMode', function () {
    var result = tsysHelpers.isTsysMode();
    assert.isTrue(result);
  });

  it('test isTsysBay', function () {
    var result = tsysHelpers.isTsysBay();
    assert.isTrue(result);
  });

  it('test isDualAcceptanceSaks', function () {
    var result = tsysHelpers.isDualAcceptanceSaks();
    assert.isFalse(result);
  });
});
