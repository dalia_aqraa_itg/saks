'use strict';

var assert = require('chai').assert;
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();

describe('validateTCCSecurityCode', function () {
  var cvvHelper = proxyquire('../../../../../cartridges/app_hbc_core/cartridge/scripts/checkout/validateTCCSecurityCode', {});

  var cardNumber = '60030444871226452209200917443';

  it('test validateSecurityCode', function () {
    var result = cvvHelper.validateSecurityCode(cardNumber);
    assert.isFalse(result);
  });

  it('test isTCCDateExpired', function () {
    var result = cvvHelper.isTCCDateExpired(cardNumber);
    assert.isTrue(result);
  });

  it('test Calc', function () {
    var result = cvvHelper.Calc(cardNumber.substr(0, 26));
    assert.equal(result, '443');
  });
});
