'use strict';

var assert = require('chai').assert;
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();

describe('cvvHelper', function () {
  var cvvHelper = proxyquire('../../../../../cartridges/app_hbc_core/cartridge/scripts/checkout/cvvHelper', {
    '*/cartridge/scripts/helpers/tsysHelpers': {
      isTsysMode: function () {
        return true;
      }
    }
  });

  var profile = {};

  var paymentInstrumentTCC = {
    creditCardToken: '60030471721946662209201020235',
    creditCardType: 'TCC'
  };

  var paymentInstrumentSaksOld = {
    creditCardToken: '8076785008',
    creditCardType: 'SAKS'
  };

  var paymentInstrumentSaksNew = {
    creditCardToken: '6003040036028892',
    creditCardType: 'SAKS',
    authorizedCard: false
  };

  it('test isCVVRequired TCC', function () {
    var result = cvvHelper.isCVVRequired(paymentInstrumentTCC, profile);
    assert.isFalse(result);
  });

  it('test isCVVRequired SAKS OLD', function () {
    var result = cvvHelper.isCVVRequired(paymentInstrumentSaksOld, profile);
    assert.isFalse(result);
  });

  it('test isCVVRequired SAKS New', function () {
    var result = cvvHelper.isCVVRequired(paymentInstrumentSaksNew, profile);
    assert.isTrue(result);
  });
});
