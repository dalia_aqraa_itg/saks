'use strict';

/* global dw empty */

var prefs = require('~/cartridge/config/paypalPreferences');
var allSites = dw.system.Site.getAllSites();
var paypalHelper = require('~/cartridge/scripts/paypal/paypalHelper');
// NVP service registration for all sites
Object.keys(allSites).forEach(function (siteId) {
  var site = allSites.get(siteId);
  var serviceName = prefs.initialNvpServiceName + '.' + site.getID();
  var allCurrencies = site.getAllowedCurrencies();
  if (dw.svc.ServiceRegistry.getDefinition(serviceName) instanceof dw.svc.HTTPServiceDefinition) {
    dw.svc.ServiceRegistry.configure(serviceName, paypalHelper.getServiceConfig(site));
  }
  Object.keys(allCurrencies).forEach(function (currencyIndex) {
    var currencyCode = allCurrencies.get(currencyIndex);
    var serviceName = prefs.initialNvpServiceName + '.' + site.getID() + '.' + currencyCode; // eslint-disable-line no-shadow
    if (dw.svc.ServiceRegistry.getDefinition(serviceName) instanceof dw.svc.HTTPServiceDefinition) {
      dw.svc.ServiceRegistry.configure(serviceName, paypalHelper.getServiceConfig(site));
    }
  });
});
