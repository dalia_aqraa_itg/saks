'use strict';

/* global dw */

/**
 * Handle entry point for SG integration
 * @param {Object} basket Basket
 * @returns {Object} processor result
 */
function Handle(basket) {
  var result = require('~/cartridge/scripts/paypal/processor').handle(basket);
  return result;
}

/**
 * Authorize entry point for SG integration
 * @param {Object} orderNumber order numebr
 * @param {Object} paymentInstrument payment intrument
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current
 *      payment method
 * @param {Object} req -  request object
 * @returns {Object} processor result
 */
function Authorize(orderNumber, paymentInstrument, paymentProcessor, req) {
  var order = dw.order.OrderMgr.getOrder(orderNumber);
  var result = require('~/cartridge/scripts/paypal/processor').authorize(order, orderNumber, paymentInstrument, req);
  return result;
}

exports.Handle = Handle;
exports.Authorize = Authorize;
