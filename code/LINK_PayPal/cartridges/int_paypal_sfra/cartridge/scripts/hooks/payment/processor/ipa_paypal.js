'use strict';

var collections = require('*/cartridge/scripts/util/collections');

var PaymentInstrument = require('dw/order/PaymentInstrument');
var PaymentMgr = require('dw/order/PaymentMgr');
var PaymentStatusCodes = require('dw/order/PaymentStatusCodes');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var Site = require('dw/system/Site');
var sitePrefs = require('dw/system/Site').getCurrent().getPreferences().getCustom();

/* global dw */

/**
 * Handle entry point for SG integration
 * @param {Object} basket Basket
 * @returns {Object} processor result
 */
function Handle(basket) {
  var result = require('~/cartridge/scripts/paypal/processor').handle(basket);
  return result;
}

/**
 * Authorize entry point for SG integration
 * @param {Object} orderNumber order numebr
 * @param {Object} paymentInstrument payment intrument
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current
 *      payment method
 * @param {Object} req -  request object
 * @returns {Object} processor result
 */
function Authorize(orderNumber, paymentInstrument, paymentProcessor, params) {
  var serverErrors = [];
  var fieldErrors = {};
  var error = false;

  try {
    Transaction.wrap(function () {
      paymentInstrument.paymentTransaction.setTransactionID(orderNumber);
      paymentInstrument.paymentTransaction.setPaymentProcessor(paymentProcessor);
    });
    var OrderMgr = require('dw/order/OrderMgr');
    var order = OrderMgr.getOrder(orderNumber);
    var ipaUtil = require('*/cartridge/scripts/util/ipaUtil');

    var args = {};
    args.Order = order;
    args.remoteAddress = params.remoteAddress;
    args.type = 'REGULAR';
    args.session = params.session;
    args.authType = 'Auth';

    args.failAttempts = params.failAttempts;
    args.customer = params.customer;
    args.deviceFingerPrintID = params.deviceFingerPrintID;

    var result = ipaUtil.authorizeCreditCard(args);

    error = result.error;
    if (result.errorcode) {
      serverErrors.push(Resource.msg(result.errorcode, 'ipa', null));
    }

    // C2NFTS: Validation API
    if (sitePrefs.forterEnabled && order) {
      // these lines must be uncommented in case if you want to activate the pre-authorization flow or being included in a top-level cartridge
      var orderNumber = order.getCurrentOrderNo();
      var argOrderValidate = {
        orderNumber               : orderNumber,
        orderValidateAttemptInput : 1,
        authorizationStep         : 'POST_AUTHORIZATION'
      };
      var forterCall       = require('*/cartridge/scripts/pipelets/forter/forterValidate');
      var forterDecision   = forterCall.validateOrder(argOrderValidate);

      // in case if no response from Forter, try to call one more time
      if (forterDecision.result === false && forterDecision.orderValidateAttemptInput == 2) {
        argOrderValidate = {
          orderNumber               : orderNumber,
          orderValidateAttemptInput : 2,
          authorizationStep         : 'POST_AUTHORIZATION'
        };
        forterCall       = require('*/cartridge/scripts/pipelets/forter/forterValidate');
        forterDecision   = forterCall.validateOrder(argOrderValidate);
      }

      // no need to handle the decline flow as the forter will always return not-review status for the paypal and klarna payment methods.
    }
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    error = true;
    serverErrors.push(Resource.msg('error.technical', 'checkout', null));
  }

  return { fieldErrors: fieldErrors, serverErrors: serverErrors, error: error };
}

exports.Handle = Handle;
exports.Authorize = Authorize;
