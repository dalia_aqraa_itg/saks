'use strict';

var server = require('server');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');

server.get('Start', server.middleware.https, csrfProtection.generateToken, function (req, res, next) {
  var Site = require('dw/system/Site');
  var tokenEnabled =
    'enableTokenEx' in Site.current.preferences.custom && Site.current.preferences.custom.enableTokenEx ? Site.current.preferences.custom.enableTokenEx : false;
  var tokenPublicKey =
    'tokenExPublicKey' in Site.current.preferences.custom && Site.current.preferences.custom.tokenExPublicKey
      ? Site.current.preferences.custom.tokenExPublicKey
      : '';
  res.render('tokenex/generateTokenForm', {
    tokenEnabled: tokenEnabled,
    tokenPublicKey: tokenPublicKey
  });
  next();
});

server.post('SubmitToken', csrfProtection.validateAjaxRequest, function (req, res, next) {
  var TokenExFacade = require('*/cartridge/scripts/services/TokenExFacade');
  var encryptedCard = req.form.token;
  var token = TokenExFacade.tokenizeEncryptedCard(decodeURIComponent(encryptedCard));
  if (token.success && token.token) {
    res.json({
      success: true,
      token: token.token
    });
  } else {
    res.json({
      success: false
    });
  }
  next();
});

module.exports = server.exports();
