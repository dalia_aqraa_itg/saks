'use strict';

/**
 * Unique TokenEx service identifiers
 */
const TokenExServices = {
  TOKENIZE_FROM_ENCRYPTED_VALUE: 'tokenex.http.tokenizeFromEncryptedValue',
  TOKENIZE: 'tokenex.http.tokenize',
  VALIDATE_TOKEN: 'tokenex.http.validateToken'
};

module.exports = TokenExServices;
