var Site = require('dw/system/Site');
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var TokenExServices = require('*/cartridge/scripts/services/TokenExServices');

module.exports = {
  getInstance: function () {
    return LocalServiceRegistry.createService(TokenExServices.TOKENIZE_FROM_ENCRYPTED_VALUE, {
      createRequest: function (svc, args) {
        if (!empty(Site.current.preferences.custom.tokenExApiURL)) {
          // eslint-disable-next-line no-param-reassign
          svc = svc.setURL(Site.current.preferences.custom.tokenExApiURL + '/ValidateToken');
        }
        // eslint-disable-next-line no-param-reassign
        svc = svc.addHeader('Content-Type', 'application/json');
        // eslint-disable-next-line no-param-reassign
        svc = svc.setRequestMethod('POST');
        return JSON.stringify(args);
      },

      parseResponse: function (svc, client) {
        return JSON.parse(client.text);
      },

      // eslint-disable-next-line no-unused-vars
      mockCall: function (svc, client) {
        var mockedReponse = '{"Error":"","ReferenceNumber":"15102913382030662954","Success":true,"Valid":true,}';

        return {
          statusCode: 200,
          statusMessage: 'Success',
          text: mockedReponse
        };
      },

      filterLogMessage: function (res) {
        return res.replace('headers', 'OFFWITHTHEHEADERS');
      }
    });
  }
};
