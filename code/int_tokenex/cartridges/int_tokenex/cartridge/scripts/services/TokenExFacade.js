'use strict';

var Site = require('dw/system/Site');
var Logger = require('dw/system/Logger').getLogger('TokenEx', 'TokenExService');
var RootLogger = require('dw/system/Logger').getRootLogger();
var TokenExServices = require('*/cartridge/scripts/services/TokenExServices');
//var preferences = require('*/cartridge/config/preferences');
var TokenExServiceFactory = require('*/cartridge/scripts/services/TokenExServiceFactory');

/**
 * TokenEx Service Call to tokenize the encrypted card on browser
 *
 * @param {string} encryptedCard - Encrypted card data
 * @returns {Object} - result returns
 */
function tokenizeEncryptedCard(encryptedCard) {
  var request = Object.create(null);
  request.APIKey = !empty(Site.current.preferences.custom.tokenExAPIKey) ? Site.current.preferences.custom.tokenExAPIKey : '';
  request.TokenExID = !empty(Site.current.preferences.custom.tokenExID) ? Site.current.preferences.custom.tokenExID : '';
  request.EcryptedData = encryptedCard;
  request.TokenScheme = !empty(Site.current.preferences.custom.tokenExSchema) ? Site.current.preferences.custom.tokenExSchema : '';

  var service = TokenExServiceFactory.getInstance(TokenExServices.TOKENIZE_FROM_ENCRYPTED_VALUE);

  Logger.debug('request ->>>' + service);
  var response = service.call(request);
  Logger.debug('response ->>>' + response);

  if (response && response.error === 0 && response.status === 'OK') {
    return {
      error: false,
      success: true,
      token: response.object.Token,
      tokenRetry: false,
      tokenEx: true
    };
  } else {
    RootLogger.fatal('FATAL: There was an error with the TokenEx tokenizeEncryptedCard service call: {0}: ', response.errorMessage);
  }
  if (!empty(Site.current.preferences.custom.enableTokenExRetry) && Site.current.preferences.custom.enableTokenExRetry === true) {
    return {
      error: false,
      success: true,
      token: '',
      tokenRetry: true,
      tokenEx: true
    };
  }
  return {
    error: true,
    success: false,
    tokenRetry: false
  };
}

/**
 *
 * TokenEx Service Call to tokenize the encrypted card on browser
 *
 * @param {string} rawCCNumber - Card number
 * @returns {Object} - result status
 */
function tokenizeUnEncryptedCard(rawCCNumber) {
  var request = Object.create(null);
  request.APIKey = !empty(Site.current.preferences.custom.tokenExAPIKey) ? Site.current.preferences.custom.tokenExAPIKey : '';
  request.TokenExID = !empty(Site.current.preferences.custom.tokenExID) ? Site.current.preferences.custom.tokenExID : '';
  request.Data = rawCCNumber;
  request.TokenScheme = !empty(Site.current.preferences.custom.tokenExSchema) ? Site.current.preferences.custom.tokenExSchema : '';

  var service = TokenExServiceFactory.getInstance(TokenExServices.TOKENIZE);

  Logger.debug('request ->>>' + service);
  var response = service.call(request);
  Logger.debug('response ->>>' + response);

  if (response && response.error === 0 && response.status === 'OK') {
    return {
      error: false,
      success: true,
      token: response.object.Token,
      tokenRetry: false,
      tokenEx: true
    };
  } else {
    RootLogger.fatal('FATAL: There was an error with the TokenEx tokenizeUnEncryptedCard service call: {0}: ', response.errorMessage);
  }
  if (!empty(Site.current.preferences.custom.enableTokenExRetry) && Site.current.preferences.custom.enableTokenExRetry === true) {
    return {
      error: false,
      success: true,
      token: '',
      tokenRetry: true,
      tokenEx: true
    };
  }
  return {
    error: true,
    success: false,
    tokenRetry: false
  };
}

/**
 * Validates the token
 *
 * @param {string} token - toekn to be validated
 * @returns {Object} - result status
 */
function validateToken(token) {
  var request = Object.create(null);
  request.APIKey = !empty(Site.current.preferences.custom.tokenExAPIKey) ? Site.current.preferences.custom.tokenExAPIKey : '';
  request.TokenExID = !empty(Site.current.preferences.custom.tokenExID) ? Site.current.preferences.custom.tokenExID : '';
  request.Token = token;

  var service = TokenExServiceFactory.getInstance(TokenExServices.VALIDATE_TOKEN);
  var response = service.call(request);

  if (response && response.error === 0 && response.status === 'OK') {
    return {
      error: false,
      success: true,
      valid: true
    };
  } else {
    RootLogger.fatal('FATAL: There was an error with the TokenEx validateToken service call: {0}: ', response.errorMessage);
  }
  return {
    error: true,
    success: false,
    valid: false
  };
}

module.exports = {
  tokenizeEncryptedCard: tokenizeEncryptedCard,
  tokenizeUnEncryptedCard: tokenizeUnEncryptedCard,
  validateToken: validateToken
};
