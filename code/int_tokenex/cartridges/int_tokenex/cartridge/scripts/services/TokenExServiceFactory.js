'use strict';

const TokenExServices = require('*/cartridge/scripts/services/TokenExServices');

/**
 * TokenEx Services
 */
const TokenExInit = require('*/cartridge/scripts/services/init/TokenExInit');
const TokenExForUnEncryptedValue = require('*/cartridge/scripts/services/init/TokenExForUnEncryptedValue');
const TokenExValidateTokenInit = require('*/cartridge/scripts/services/init/TokenExValidateTokenInit');

/**
 * Using the serviceName return an instance of the Service.
 *
 * @param {string} serviceName - Service name
 * @returns {InstanceType} Instance of Service
 */
function _getServiceBuilder(serviceName) {
  //eslint-disable-line
  if (serviceName === TokenExServices.TOKENIZE_FROM_ENCRYPTED_VALUE) {
    return TokenExInit;
  } else if (serviceName === TokenExServices.VALIDATE_TOKEN) {
    return TokenExValidateTokenInit;
  } else if (serviceName === TokenExServices.TOKENIZE) {
    return TokenExForUnEncryptedValue;
  }

  throw new Error('service ID not recognized: ' + serviceName);
}

exports.getInstance = function (serviceName) {
  const builder = _getServiceBuilder(serviceName);
  return builder.getInstance();
};
