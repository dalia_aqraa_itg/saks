var Site = require('dw/system/Site');
var OrderMgr = require('dw/order/OrderMgr');
var Order = require('dw/order/Order');
var Transaction = require('dw/system/Transaction');
var TokenExFacade = require('*/cartridge/scripts/services/TokenExFacade');
var Logger = require('dw/system/Logger');

/**
 * Create Card token
 *
 * @param {string} encryptedCard - Encrypted card
 * @returns {Object} - Tokenex object
 */
function createToken(encryptedCard) {
  var token;
  if ('enableTokenEx' in Site.current.preferences.custom && Site.current.preferences.custom.enableTokenEx === true) {
    token = TokenExFacade.tokenizeEncryptedCard(encryptedCard);
  } else {
    token = {
      error: true,
      success: false,
      token: Math.random().toString(36).substr(2),
      tokenRetry: false,
      tokenEx: false
    };
  }
  return token;
}

exports.RetryOrders = function retryOrders() {
  // fetch Orders which are not tokenized
  var ordersItr = OrderMgr.queryOrders('custom.tokenExRetry={0}', null, true);
  while (ordersItr.hasNext()) {
    var order = ordersItr.next();
    Logger.info('Order ID : ' + order.orderNo);
    // Get the encrypted data, call tokenex, get token and save it to Payment Instrument and Set order status as Ready for Export
    if (!empty(order.custom.tokenExRetry) && order.custom.tokenExRetry === true && !empty(order.custom.tokenExData)) {
      var encryptedData = order.custom.tokenExData;
      var creditCardPaymentInstruments = order.getPaymentInstruments('CREDIT_CARD');
      if (creditCardPaymentInstruments && creditCardPaymentInstruments.length > 0) {
        // Generate Token
        var tokenObj = createToken(encryptedData);
        if (tokenObj && tokenObj.success === true && tokenObj.tokenRetry === false && tokenObj.token) {
          for (var i = 0; i < creditCardPaymentInstruments.length; i++) {
            var creditCardPaymentInstrument = creditCardPaymentInstruments[i];
            // Verify Card
            if (tokenObj.token) {
              // eslint-disable-next-line no-loop-func
              Transaction.wrap(function () {
                if (empty(creditCardPaymentInstrument.creditCardToken)) {
                  creditCardPaymentInstrument.setCreditCardToken(tokenObj.token);
                  // Will Add Auth Logic if we need to @TBD
                  order.setExportStatus(Order.EXPORT_STATUS_READY); // marking it READY so the offline process picks up the order
                  // reset flag for TokenEx Retry
                  order.custom.tokenExRetry = false;
                  order.custom.orderActionModify = true;
                  // resetting the order status to mark the order as token resolved. AUTH hold will be added from OMS side
                  delete order.custom.orderStatus;
                  order.custom.orderStatus = null;
                  delete order.custom.tokenExData;

                  var OrderAPIUtils = require('*/cartridge/scripts/util/OrderAPIUtil');
                  order.custom.omsCreateOrderXML = OrderAPIUtils.buildRequestXML(order);
                } else {
                  order.setExportStatus(Order.EXPORT_STATUS_FAILED); // marking it failed so the offline process picks up the order
                  // reset flag for TokenEx Retry
                  order.custom.tokenExRetry = false;
                  order.custom.orderActionModify = true;
                  // resetting the order status to mark the order as token resolved. AUTH hold will be added from OMS side
                  delete order.custom.orderStatus;
                  order.custom.orderStatus = null;
                  delete order.custom.tokenExData;
                }
              });
            }
          }
        }
      }
    }
  }
};
