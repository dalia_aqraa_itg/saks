'use strict';
var tokenExUtil = require('../tokenEx');

var tokenEx = {};

tokenEx.submitToken = function () {
  $('body').on('click', 'button.tokenex-form-submit', function (e) {
    e.preventDefault();
    var $form = $(this).closest('form');
    var action = $form.attr('action');
    $('.generated-token').text('');
    var cardNumber = $('.tokenEx-cardNumber').val();
    if (cardNumber !== undefined && cardNumber !== '') {
      var publicKey = $('input.tokenExPubKey').val();
      $form.spinner().start();
      if (publicKey) {
        var token = tokenExUtil.encryptCC(publicKey, cardNumber.trim());
        // console.log(encodeURIComponent(token));
        $.ajax({
          url: action,
          type: 'post',
          dataType: 'json',
          data: $form.serialize() + '&token=' + encodeURIComponent(token),
          success: function (data) {
            $form.spinner().stop();
            if (data.success && data.token) {
              $('.generated-token').text(data.token);
            } else {
              $('.generated-token').text('Token Not Found');
            }
          }
        });
      }
    }
  });
};

module.exports = tokenEx;
