'use strict';

/**
 * Encrypt Credit card using tokenex
 *
 * @param {string} publicKey - public key to be encrypted with
 * @param {string} creditCard - Credit card to be encrypted
 * @returns {token} - returns token
 */
function encryptCC(publicKey, creditCard) {
  if (window.TxEncrypt) {
    // eslint-disable-next-line new-cap
    return window.TxEncrypt(publicKey, creditCard);
  }
  return null;
}

/**
 * Validate Credit card
 *
 * @param {string} creditCard - Credit card to be validated
 * @returns {boolean} - check status
 */
function validateCreditCard(creditCard) {
  var regex = /^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/;
  return regex.test(creditCard);
}

/**
 * Check credit card
 *
 * @param {string} creditCard - Credit cards
 * @returns {boolean} - check status
 */
function luhnCheck(creditCard) {
  if (/[^0-9-\s]+/.test(creditCard)) return false;

  let nCheck = 0;
  let bEven = false;
  // eslint-disable-next-line no-param-reassign
  creditCard = creditCard.replace(/\D/g, '');

  for (var n = creditCard.length - 1; n >= 0; n--) {
    var cDigit = creditCard.charAt(n);
    var nDigit = parseInt(cDigit, 10);

    // eslint-disable-next-line no-cond-assign
    if (bEven && (nDigit *= 2) > 9) nDigit -= 9;

    nCheck += nDigit;
    bEven = !bEven;
  }
  return nCheck % 10 === 0;
}

module.exports = {
  encryptCC: encryptCC,
  validateCreditCard: validateCreditCard,
  luhnCheck: luhnCheck
};
