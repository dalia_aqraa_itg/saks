'use strict';

var server = require('server');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');

server.post('ApplyPoints', server.middleware.https, function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var Transaction = require('dw/system/Transaction');
  var Resource = require('dw/web/Resource');
  var Money = require('dw/value/Money');
  var formatMoney = require('dw/util/StringUtils').formatMoney;
  var basket = BasketMgr.getCurrentBasket();
  var amexAmount = req.form.amexAmount;
  var amexAppliedPoints = req.form.amexAppliedPoints;
  if (amexAmount) {
    amexAmount = amexAmount.replace('$', '').replace(',', '');
    var amexpayHelper = require('*/cartridge/scripts/helpers/amexpayHelper');
    var pi = amexpayHelper.hasAmexCreditCard(basket);
    var convertionRate = null;
    var rewardBalancePoint = null;
    var rewardAmexAmount = null;
    var appliedPoints;
    var obj = JSON.parse(pi.custom.paywithPointsResponse);
    if (obj && obj.rewards) {
      convertionRate = obj.rewards.conversion_rate;
      rewardBalancePoint = Number(obj.rewards.balance);
      rewardAmexAmount = rewardBalancePoint * Number(convertionRate);
    }
    if (convertionRate) {
      appliedPoints = Math.round(Number(amexAmount) / Number(convertionRate));
    }

    if (!empty(rewardAmexAmount) && Number(amexAmount) > Number(rewardAmexAmount)) {
      amexAmount = rewardAmexAmount.toString();
    }

    // Also Check if the Applied Amex Amount is more than order total
    var totalOrderTotal = basket.totalGrossPrice.value;
    if (Number(amexAmount) > totalOrderTotal) {
      if (Number(rewardAmexAmount) >= totalOrderTotal) {
        amexAmount = totalOrderTotal.toString();
      } else {
        amexAmount = rewardAmexAmount.toString();
      }
    }

    if (pi) {
      Transaction.wrap(function () {
        pi.custom.amexAmountApplied = amexAmount;
      });
    }
    if (!empty(amexAppliedPoints)) {
      // Ristrict if user tries to submit more points
      if (appliedPoints > rewardBalancePoint) {
        appliedPoints = rewardBalancePoint;
      } else {
        appliedPoints = amexAppliedPoints;
      }
    }
    amexAmount = new Money(amexAmount, basket.getCurrencyCode());
    delete session.custom.orderAmountFull;
    if (amexAmount.value == basket.totalGrossPrice.value) {
      session.custom.orderAmountFull = true;
    }
  }
  var pointSummary = Resource.msgf('amex.points.summary', 'amexpay', null, formatMoney(amexAmount), appliedPoints);
  res.json({
    pointSummary: pointSummary,
    amountApplied: formatMoney(amexAmount)
  });

  next();
});

server.get('RemoveAmexPoints', function (req, res, next) {
  var BasketMgr = require('dw/order/BasketMgr');
  var Transaction = require('dw/system/Transaction');
  var basket = BasketMgr.getCurrentBasket();
  var amexpayHelper = require('*/cartridge/scripts/helpers/amexpayHelper');
  var pi = amexpayHelper.hasAmexCreditCard(basket);
  if (pi) {
    Transaction.wrap(function () {
      delete pi.custom.amexAmountApplied;
    });
  }
  res.json({});

  next();
});
module.exports = server.exports();
