'use strict';

/* API Includes */
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var Logger = require('dw/system/Logger');
var amexpayHelper = require('*/cartridge/scripts/helpers/amexpayHelper');
var preferences = require('*/cartridge/scripts/config/amexpayPreferences');

function addServiceHeaders(service) {
  service.addHeader('Content-Type', 'application/json');
  service.addHeader('TX_TokenExID', preferences.tokenExID);
  service.addHeader('TX_APIKey', preferences.tokenExAPIKey);
  service.addHeader('TX_HMACKey', preferences.txHMACKey);
  return service;
}

var AmexPayWithPoints = function () {
  /**
   * Check Whether or not the checkout is eligible for same-day delivery
   *
   * @param {String} resource URL
   * @param {Object} data The JSON data to send in the body of the request
   *
   */
  this.generateHMACPayload = function (data) {
    var serviceDefinition = LocalServiceRegistry.createService('amexpay.http.hmac.payload.post', {
      createRequest: function (service, requestBody) {
        service.setRequestMethod('POST');
        service.addHeader('TX_URL', preferences.txURL);
        addServiceHeaders(service);
        return requestBody;
      },
      parseResponse: function (service, httpClient) {
        return JSON.parse(httpClient.text);
      },
      // eslint-disable-next-line no-unused-vars
      mockCall: function (svc, client) {
        var mockedReponse = '{"HashValue": "13Kr0CK+wwAOnhV8Zka0QLEbvN4Y5KKv1tXBq8K5ZTA=","RefNumber": "20081003095491634105"}';
        return {
          statusCode: 200,
          statusMessage: 'Success',
          text: mockedReponse
        };
      },
      filterLogMessage: function (logMsg) {
        try {
          if (logMsg.indexOf('https') === -1) {
            var obj = JSON.parse(logMsg);
            obj.HashValue = '***********';
            obj.RefNumber = '***********';
            return JSON.stringify(obj);
          }
        } catch (e) {
          Logger.error('Line #43, File amexpayServiceInit.js | Error stack trace' + e.message);
        }
        return logMsg;
      }
    });

    var result = serviceDefinition.call(JSON.stringify(data.requestBody));

    return result;
  };

  this.getPayPoints = function (data) {
    var serviceDefinition = LocalServiceRegistry.createService('amexpay.http.detokenize.post', {
      createRequest: function (service, requestBody) {
        service.setRequestMethod('POST');
        addServiceHeaders(service);
        data.serviceURL = service.getURL();
        service.addHeader('TX_URL', preferences.txURL);
        service.addHeader('Authorization', amexpayHelper.getSignature(data, false));
        service.addHeader('x-amex-request-id', amexpayHelper.getUniqueIdentifier());
        service.addHeader('X-AMEX-API-KEY', preferences.key);
        return requestBody;
      },
      parseResponse: function (service, httpClient) {
        return JSON.parse(httpClient.text);
      },
      // eslint-disable-next-line no-unused-vars
      mockCall: function (svc, client) {
        var mockedReponse =
          '{"timestamp": "Sun Aug 09 2020 10:36:52 GMT-0000 (GMT)","rewards": {"conversion_rate": "0.007","balance": "1000000","unit": "Points"}}';
        return {
          statusCode: 200,
          statusMessage: 'Success',
          text: mockedReponse
        };
      },
      filterLogMessage: function (logMsg) {
        try {
          if (logMsg.indexOf('https') === -1) {
            var obj = JSON.parse(logMsg);
            obj.balance = '***********';
            obj.conversion_rate = '***********';
            return JSON.stringify(obj);
          }
        } catch (e) {
          Logger.error('Line #93, File amexpayServiceInit.js | Error stack trace' + e.message);
        }
        return logMsg;
      }
    });

    var result = serviceDefinition.call(JSON.stringify(data.requestBody));

    return result;
  };

  this.redeemPoints = function (data) {
    var serviceDefinition = LocalServiceRegistry.createService('amexpay.http.redeem.post', {
      createRequest: function (service, requestBody) {
        service.setRequestMethod('POST');
        addServiceHeaders(service);
        data.serviceURL = service.getURL();
        service.addHeader('TX_URL', preferences.amexRedeemURL);
        service.addHeader('Authorization', amexpayHelper.getSignature(data, true));
        service.addHeader('x-amex-request-id', amexpayHelper.getUniqueIdentifier());
        service.addHeader('X-AMEX-API-KEY', preferences.key);
        return requestBody;
      },
      parseResponse: function (service, httpClient) {
        return JSON.parse(httpClient.text);
      },
      // eslint-disable-next-line no-unused-vars
      mockCall: function (svc, client) {
        var mockedReponse =
          '{"timestamp": "Sun Aug 09 2020 10:36:52 GMT-0000 (GMT)","rewards": {"conversion_rate": "0.007","balance": "1000000","unit": "Points"}}';
        return {
          statusCode: 200,
          statusMessage: 'Success',
          text: mockedReponse
        };
      },
      filterLogMessage: function (logMsg) {
        try {
          if (logMsg.indexOf('https') === -1) {
            var obj = JSON.parse(logMsg);
            obj.rewards.balance = '***********';
            obj.rewards.conversion_rate = '***********';
            return JSON.stringify(obj);
          }
        } catch (e) {
          Logger.error('Line #142, File amexpayServiceInit.js | Error stack trace' + e.message);
        }
        return logMsg;
      }
    });

    var result = serviceDefinition.call(JSON.stringify(data.requestBody));

    return result;
  };
};

module.exports = AmexPayWithPoints;
