'use strict';
var Logger = require('dw/system/Logger');
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');

function getNonce() {
  var Crypto = require('dw/crypto');
  var nonce = new Crypto.SecureRandom().nextInt(1000000).toString();
  return nonce;
}

function hasPointsOnAmexCard(lineItemCtnr) {
  var hasPoints = false;
  if (!lineItemCtnr) {
    return hasPoints;
  }
  if ('tokenExRetry' in lineItemCtnr.custom && lineItemCtnr.custom.tokenExRetry) {
    return hasPoints;
  }

  try {
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var paymentInstruments = lineItemCtnr.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);
    var paymentItr = paymentInstruments.iterator();
    if (paymentItr.hasNext()) {
      var pi = paymentItr.next();
      if (pi && pi.getCreditCardToken() != null && 'paywithPointsResponse' in pi.custom) {
        var obj = JSON.parse(pi.custom.paywithPointsResponse);
        if (obj && obj.rewards && Number(obj.rewards.balance) > 0) {
          hasPoints = true;
        }
      }
    }
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    Logger.error('There was an error in determining the cc has pay points' + errorMsg);
  }

  return hasPoints;
}

function writeCOForOfflineRedemption(lineItemCtnr) {
  if (!lineItemCtnr) {
    return;
  }
  if ('tokenExRetry' in lineItemCtnr.custom && lineItemCtnr.custom.tokenExRetry) {
    return;
  }
  try {
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var paymentInstruments = lineItemCtnr.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);
    var paymentItr = paymentInstruments.iterator();
    if (paymentItr.hasNext()) {
      var pi = paymentItr.next();
      if (
        pi &&
        pi.getCreditCardToken() != null &&
        'paywithPointsResponse' in pi.custom &&
        'amexAmountApplied' in pi.custom &&
        'authorization_code' in pi.custom &&
        pi.custom.authorization_code !== ''
      ) {
        var obj = JSON.parse(pi.custom.paywithPointsResponse);
        var CustomObjectMgr = require('dw/object/CustomObjectMgr');
        var Transaction = require('dw/system/Transaction');
        Transaction.wrap(function () {
          // Query Custom Object and update it if Email and Mobile is same.
          var query = 'custom.orderNo = {0}';
          var amexRedeemObj = CustomObjectMgr.queryCustomObject('AmexPayWithPointsDown', query, lineItemCtnr.orderNo);
          if (!amexRedeemObj) {
            amexRedeemObj = CustomObjectMgr.createCustomObject('AmexPayWithPointsDown', lineItemCtnr.orderNo);
          }
          if (amexRedeemObj) {
            amexRedeemObj.custom.conversionRate = obj.rewards.conversion_rate;
            amexRedeemObj.custom.authCode = pi.custom.authorization_code;
            amexRedeemObj.custom.rewardsDetails = JSON.stringify(obj.rewards);
            amexRedeemObj.custom.pointsAmount = pi.custom.amexAmountApplied;
            amexRedeemObj.custom.numberOfAttempts = 0; // marked zero initially during order placement
            amexRedeemObj.custom.attemptRedemption = true; // marking would pick the custom object in the job
          }
        });
      }
    }
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    Logger.error('There was an error in determining the cc has pay points' + errorMsg);
  }
}

function hasAmexCreditCard(lineItemCtnr) {
  var isAmex = false;
  if (!lineItemCtnr) {
    return isAmex;
  }
  if ('tokenExRetry' in lineItemCtnr.custom && lineItemCtnr.custom.tokenExRetry) {
    return isAmex;
  }

  try {
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var paymentInstruments = lineItemCtnr.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);
    var paymentItr = paymentInstruments.iterator();
    if (paymentItr.hasNext()) {
      var pi = paymentItr.next();
      if (pi && pi.getCreditCardToken() != null && pi.getCreditCardType().toLowerCase() === 'amex') {
        isAmex = pi;
      }
    }
  } catch (e) {
    var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
    Logger.error('There was an error in determining the cc has pay points' + errorMsg);
  }

  return isAmex;
}

function getURLProperties(url) {
  var urlObj = {};
  var temp = url.split('://');
  var temp1 = temp[1].indexOf('/') ? temp[1].split('/') : '';
  var temp2 = temp[1].split(temp1[0]);
  urlObj.abs = url;
  urlObj.protocol = temp[0];
  urlObj.host = temp1[0];
  urlObj.path = temp2[1];
  urlObj.querystring = url.indexOf('?') > -1 ? url.split('?')[1] : '';
  return urlObj;
}

function getSignature(params, isRedeemCall) {
  var Crypto = require('dw/crypto');
  var Mac = require('dw/crypto/Mac');
  var Encoding = require('dw/crypto/Encoding');
  var Bytes = require('dw/util/Bytes');
  var preferences = require('*/cartridge/scripts/config/amexpayPreferences');
  var hashbody = params.hmacHashKey;
  var url = getURLProperties(params.serviceURL);
  var timestamp = StringUtils.formatCalendar(new Calendar(), "YYYY-MM-dd'T'MM:hh:mm.SSS-07:00");
  var method = 'POST';
  var port = '443';
  var nonce = getNonce();
  var path = preferences.amexGetPayPointsPath;
  if (isRedeemCall) {
    path = preferences.amexRedeemPointsPath;
  }
  var macData = timestamp + '\n' + nonce + '\n' + method + '\n' + path + '\n' + preferences.amexPayHost + '\n' + port + '\n' + hashbody + '\n';

  var macSha256 = new Mac(Mac.HMAC_SHA_256);
  var bytesKey = new Bytes(preferences.txSecret);

  var signatureBytes = macSha256.digest(macData, bytesKey);
  var hashOfMacData = Encoding.toBase64(signatureBytes);
  var signature = 'MAC id="' + preferences.key + '",ts="' + timestamp + '",nonce="' + nonce + '", bodyhash="' + hashbody + '", mac="' + hashOfMacData + '"';
  return signature;
}

function getUniqueIdentifier() {
  var UUIDUtils = require('dw/util/UUIDUtils');
  return UUIDUtils.createUUID();
}

function buildRequest(token, pi, cvv) {
  var preferences = require('*/cartridge/scripts/config/amexpayPreferences');
  var today = new Calendar();
  today = StringUtils.formatCalendar(today, "YYYY-MM-dd'T'MM:hh:mm.SSS-07:00");
  var requestObj = {};
  requestObj.merchant_client_id = preferences.merchantClientID;
  requestObj.timestamp = today.toString();
  requestObj.card = {};
  requestObj.card.number = '{{{' + token + '}}}';
  if (pi.getCreditCardExpirationMonth()) {
    requestObj.card.expiration_month = StringUtils.formatNumber(pi.getCreditCardExpirationMonth().toString(), '00');
  }
  if (pi.getCreditCardExpirationYear()) {
    requestObj.card.expiration_year = pi.getCreditCardExpirationYear().toString();
  }
  if (cvv && cvv != null && cvv != undefined && cvv != 'undefined' && cvv != '') {
    requestObj.card.cid = cvv;
  }
  return requestObj;
}

function getAmexPayAmount(lineItemCtnr) {
  var pi = hasAmexCreditCard(lineItemCtnr);
  if (pi && hasPointsOnAmexCard(lineItemCtnr)) {
    var result = {};
    var Transaction = require('dw/system/Transaction');
    var Resource = require('dw/web/Resource');
    var Money = require('dw/value/Money');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var nonGCTotal = COHelpers.getNonGiftCardAmount(lineItemCtnr, null);
    var formatMoney = require('dw/util/StringUtils').formatMoney;
    var amountApplied = Number(pi.custom.amexAmountApplied);
    var maxAmount;
    var orderTotal = nonGCTotal.remainingAmount.value;
    var convertionRate, balance;
    var obj = JSON.parse(pi.custom.paywithPointsResponse);
    var appliedPoints = 0;
    if (obj && obj.rewards) {
      convertionRate = obj.rewards.conversion_rate;
      balance = obj.rewards.balance;
    }
    if (convertionRate) {
      maxAmount = Math.floor(Number(balance) * Number(convertionRate) * 100) / 100;
    }

    if (orderTotal <= maxAmount) {
      maxAmount = orderTotal;
    }
    if (amountApplied >= maxAmount) {
      amountApplied = maxAmount;
    }
    if (amountApplied == 0) {
      amountApplied = maxAmount;
    }

    if (session.custom.orderAmountFull) {
      amountApplied = maxAmount;
    }

    appliedPoints = Math.round(Number(amountApplied) / Number(convertionRate));

    if ('amexAmountApplied' in pi.custom && pi.custom.amexAmountApplied && !empty(pi.custom.amexAmountApplied)) {
      Transaction.wrap(function () {
        pi.custom.amexAmountApplied = amountApplied;
      });

      amountApplied = new Money(amountApplied, lineItemCtnr.getCurrencyCode());

      result.pointSummary = Resource.msgf('amex.points.summary', 'amexpay', null, formatMoney(amountApplied), appliedPoints);
      result.amountApplied = formatMoney(amountApplied);
      return result;
    } else {
      return null;
    }
  }
  return null;
}

function redeemPoints(pi, redeemReqObj) {
  var response = {};
  response.error = true;
  if (pi) {
    var token = pi.getCreditCardToken();
    var requestObj = {};
    requestObj = buildRequest(token, pi);
    var redeemObj = {};
    redeemObj.requestBody = redeemReqObj;
    redeemObj.requestBody.merchant_client_id = requestObj.merchant_client_id;
    redeemObj.requestBody.timestamp = requestObj.timestamp;
    redeemObj.requestBody.card = requestObj.card;
    var amexpayService = new (require('*/cartridge/scripts/services/init/amexpayServiceInit'))();
    var Result = require('dw/svc/Result');
    var hmacResponse = amexpayService.generateHMACPayload(redeemObj);
    var getPointsResponse = null;
    var response = {};
    response.error = true;
    if (hmacResponse.status === hmacResponse.SERVICE_UNAVAILABLE) {
      Logger.getRootLogger().fatal(' Token Ex HMAC service unavailable ' + hmacResponse.errorMessage);
      response.errorMessage = hmacResponse.errorMessage;
    }

    if (hmacResponse.status !== 'OK' || !hmacResponse.object) {
      Logger.getRootLogger().fatal(' Error during generating the hmac' + hmacResponse.errorMessage);
      response.errorMessage = hmacResponse.errorMessage;
    } else if (hmacResponse.object) {
      response.hmacResponse = hmacResponse.object;
      if (hmacResponse.object.HashValue) {
        response.error = false;
      }
    } else {
      response.errorMessage = 'Unexpected Error';
    }
    if (!response.error) {
      // if hmac body is returned then make a call to detokenize the card to fetch the points
      redeemObj.hmacHashKey = response.hmacResponse.HashValue;
      getPointsResponse = amexpayService.redeemPoints(redeemObj);
    }
    if (getPointsResponse) {
      response.error = true;
      if (getPointsResponse.object) {
        if (getPointsResponse.object.rewards && getPointsResponse.object.rewards.unit == 'Points' && Number(getPointsResponse.object.rewards.reservation) > 0) {
          response.error = false;
        } else {
          response.errorMessage = 'Reservation Quantity is zero';
        }
      }
    }
  }
  return response;
}

function getRewardPoints(pi, cvv) {
  if (pi) {
    var token = pi.getCreditCardToken();
    var requestObj = {};
    requestObj.requestBody = buildRequest(token, pi, cvv);
    var amexpayService = new (require('*/cartridge/scripts/services/init/amexpayServiceInit'))();
    var Result = require('dw/svc/Result');
    var hmacResponse = amexpayService.generateHMACPayload(requestObj);
    var getPointsResponse = null;
    var response = {};
    response.error = true;
    response.hmacResponse = {};
    if (hmacResponse.status === hmacResponse.SERVICE_UNAVAILABLE) {
      Logger.getRootLogger().fatal(' Token Ex HMAC service unavailable ' + hmacResponse.errorMessage);
      response.hmacResponse.errorMessage = hmacResponse.errorMessage;
      response.hmacResponse.status = hmacResponse.status;
    }

    if (hmacResponse.status !== 'OK' || !hmacResponse.object) {
      Logger.getRootLogger().fatal(' Error during generating the hmac' + hmacResponse.errorMessage);
      response.hmacResponse.errorMessage = hmacResponse.errorMessage;
      response.hmacResponse.status = hmacResponse.status;
    } else if (hmacResponse.object) {
      response.hmacResponse = hmacResponse.object;
      if (hmacResponse.object.HashValue) {
        response.error = false;
      }
    } else {
      response.hmacResponse.errorMessage = 'Unexpected Error';
    }
    if (!response.error) {
      // if hmac body is returned then make a call to detokenize the card to fetch the points
      requestObj.hmacHashKey = response.hmacResponse.HashValue;
      getPointsResponse = amexpayService.getPayPoints(requestObj);
    }
    if (getPointsResponse) {
      if (getPointsResponse.object) {
        response.getPointsResponse = getPointsResponse.object;
        if (getPointsResponse.object.rewards && getPointsResponse.object.rewards.unit == 'Points' && Number(getPointsResponse.object.rewards.balance) >= 2) {
          var Transaction = require('dw/system/Transaction');
          Transaction.wrap(function () {
            pi.custom.paywithPointsResponse = JSON.stringify(getPointsResponse.object);
          });
          response.error = false;
        }
      }
    }
  }
}

module.exports = {
  getUniqueIdentifier: getUniqueIdentifier,
  getSignature: getSignature,
  getRewardPoints: getRewardPoints,
  getAmexPayAmount: getAmexPayAmount,
  hasPointsOnAmexCard: hasPointsOnAmexCard,
  hasAmexCreditCard: hasAmexCreditCard,
  writeCOForOfflineRedemption: writeCOForOfflineRedemption,
  redeemPoints: redeemPoints
};
