var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var preferences = require('*/cartridge/scripts/config/amexpayPreferences');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');

/**
 * Redeem Amex Pay Points
 * "basket": {
		"purchase_date": "2015-10-16T00:40:00.715-07:00",
		"amount": "50"
	},
	"rewards": {
		"count": "10",
		"unit": "points",
		"amount": "1"
	},
	"transaction_approval_code": "12345654"
 */
function redeemPoints() {
  var customObjectsItr = CustomObjectMgr.queryCustomObjects('AmexPayWithPointsDown', 'custom.attemptRedemption = {0}', null, true);
  if (customObjectsItr.count > 0) {
    Transaction.wrap(function () {
      while (customObjectsItr.hasNext()) {
        var co = customObjectsItr.next();
        var noOfAttempts = Number(co.custom.numberOfAttempts);
        if (noOfAttempts >= Number(preferences.noOfRedemptionAttempts)) {
          co.custom.attemptRedemption = false;
        } else {
          var redeemReqObj = {};
          redeemReqObj.transaction_approval_code = co.custom.authCode;
          redeemReqObj.rewards = {};
          redeemReqObj.rewards.amount = co.custom.pointsAmount;
          redeemReqObj.rewards.unit = 'Points';
          redeemReqObj.rewards.count = Math.round(Number(co.custom.pointsAmount) / Number(co.custom.conversionRate));
          var OrderMgr = require('dw/order/OrderMgr');
          var order = OrderMgr.getOrder(co.custom.orderNo);
          var amexpayHelper = require('*/cartridge/scripts/helpers/amexpayHelper');
          var pi = amexpayHelper.hasAmexCreditCard(order);
          redeemReqObj.basket = {};
          var orderCalender = new Calendar(order.getCreationDate());
          orderCalender = StringUtils.formatCalendar(orderCalender, "YYYY-MM-dd'T'MM:hh:mm.SSS-07:00");
          redeemReqObj.basket.purchase_date = orderCalender;
          redeemReqObj.basket.amount = order.totalGrossPrice.value;
          var response = amexpayHelper.redeemPoints(pi, redeemReqObj);
          if (!response.error) {
            co.custom.attemptRedemption = false;
            //remove
            CustomObjectMgr.remove(co);
          } else {
            co.custom.numberOfAttempts = noOfAttempts + 1;
            var message = co.custom.failureLogMessage;
            var log = [];
            if (message) {
              for (var i = 0; i < message.length; i++) {
                log.push(message[i]);
              }
            }
            log.push(response.errorMessage);
            co.custom.failureLogMessage = log;
          }
        }
      }
    });
  }
}

exports.redeemPoints = redeemPoints;
