var Site = require('dw/system/Site');
var CurrentSite = Site.current.preferences;
var preferences = CurrentSite ? CurrentSite.custom : {};
/**
 * Constants
 */

var config = {};

config.tokenExSchema = preferences.tokenExSchema;

config.enableTokenEx = preferences.enableTokenEx ? preferences.enableTokenEx : false;

config.tokenExPublicKey = preferences.enableTokenEx ? preferences.tokenExPublicKey : '';

config.tokenExApiURL = preferences.tokenExApiURL ? preferences.tokenExApiURL : '';

config.tokenExAPIKey = preferences.tokenExAPIKey ? preferences.tokenExAPIKey : '';

config.tokenExID = preferences.tokenExID ? preferences.tokenExID : '';

config.txURL = preferences.txURL;

config.txSecret = preferences.txSecret;

config.txHMACKey = preferences.txHMACKey;

config.merchantClientID = preferences.merchantClientID;

config.amexGetPayPointsPath = preferences.amexGetPayPointsPath;

config.amexPayHost = preferences.amexPayHost;

config.key = preferences.key;

config.port = preferences.port;

config.amexRedeemPointsPath = preferences.amexRedeemPointsPath;

config.noOfRedemptionAttempts = preferences.noOfRedemptionAttempts;

config.amexRedeemURL = preferences.amexRedeemURL;

module.exports = config;
