'use strict';

function editAmex() {
  $('.modify-points').on('click', function (e) {
    e.preventDefault();
    $('.paywithpoints-contianer-body').removeClass('d-none');
    $('.paywithpoints-summary').addClass('d-none');
    $('.amex-info').addClass('d-none');
    $('body').trigger('checkout:updateAmexForm');
    $('body').trigger('checkout:removeAmexPoints');
  });
}

function updateOrderTotals() {
  var orderTotal = $('.grand-total-sum').html().toString().replace('$', '').replace(',', ''); //89764.00
  var strOrderTotal = $('.grand-total-sum').html().toString();
  var convertionRate = $('#amex-amount').data('convertionrate');
  var maxpoints = $('#amex-amount').data('maxpoints');
  var amexpointstoDollars = parseFloat(maxpoints) * parseFloat(convertionRate);
  if (amexpointstoDollars >= parseFloat(orderTotal)) {
    $('#amex-amount').attr('data-amount', orderTotal);
    $('#amex-amount').data('amount', orderTotal);
  }

  if (parseFloat($('#amex-amount').val().toString().replace('$', '').replace(',', '')) > parseFloat(orderTotal)) {
    $('#amex-amount').val(orderTotal);
  }

  var maxAmount = $('#amex-amount').data('amount').toString();
  $('#amex-amount').val(maxAmount.replace(',', ''));

  var currentPoints = Math.round(parseFloat($('#amex-amount').val()) / parseFloat(convertionRate));
  if (currentPoints > parseFloat(maxpoints)) {
    currentPoints = parseFloat(maxpoints);
  }
  $('.actual-points').empty().html(currentPoints);
  $('span.point-balance span:last-child').html(strOrderTotal);
  $('#amex-amount').val('$' + $('#amex-amount').val());
}

function countDecimalDigits(number) {
  var char_array = number.toString().split(''); // split every single char
  var not_decimal = char_array.lastIndexOf('.');
  return not_decimal < 0 ? 0 : char_array.length - not_decimal - 1;
}

function truncate(num, places) {
  return Math.trunc(num * Math.pow(10, places)) / Math.pow(10, places);
}

function initializeEvents() {
  editAmex();

  $('body').on('checkout:updateAmexForm', function (e) {
    var orderTotal = parseFloat($('.grand-total-sum').text().replace('$', '').replace(',', ''));
    var amexAmount = parseFloat($('#amex-amount').data('amount'));
    var maxTotal = orderTotal;

    $('#amex-amount').data('amount', maxTotal);
    $('.amex-order-total').text('$' + maxTotal);
  });

  var ammountApplied = $('.amex-info').data('applied');
  if (ammountApplied != 'false') {
    $('.amex-info').find('.order-total-amex-amount').html(ammountApplied);
  }

  $('.js-amex-point-balance').on('click', function () {
    $('body').trigger('checkout:updateAmexForm');
    $('.amex-main').addClass('d-none');
    $('.amex-points-container').removeClass('d-none');
  });

  $('.amexpay-apply').on('click', function () {
    if (parseFloat($('#amex-amount').val().replace('$', '')) > 0) {
      $('.paywithpoints-contianer-body').addClass('d-none');
      $.spinner().start();
      $.ajax({
        url: $('button.amexpay-apply').data('action'),
        data: {
          amexAmount: $('#amex-amount').val(),
          amexAppliedPoints: $('.actual-points').html()
        },
        type: 'post',
        success: function (data) {
          $(".paywithpoints-summary").empty().html(data.pointSummary);
          $(".paywithpoints-summary").removeClass("d-none");
          $(".amex-info").removeClass("d-none");
          $(".amex-info").find(".order-total-amex-amount").html(data.amountApplied);
          $('body').trigger('checkout:applyAmex');

          editAmex();

          $.spinner().stop();
        },
        error: function () {
          $.spinner().stop();
        }
      });
    } else {
      var maxAmount = $('#amex-amount').attr('data-amount').toString();
      $('#amex-amount').val(maxAmount.replace(',', ''));
      var convertionRate = $('#amex-amount').data('convertionrate');
      var currentPoints = Math.round(parseFloat($('#amex-amount').val()) / parseFloat(convertionRate));
      if (currentPoints > parseFloat($('#amex-amount').data('maxpoints'))) {
        currentPoints = parseFloat($('#amex-amount').data('maxpoints'));
      }
      $('.actual-points').empty().html(currentPoints);
      $('#amex-amount').val('$' + $('#amex-amount').val());
    }
  });

  $('#amex-amount').on('input', function () {
    var maxAmount = $('#amex-amount').attr('data-amount').toString();
    $(this).val($(this).val().replace('$', '').replace(',', ''));
    if (parseFloat($(this).val()) > parseFloat(maxAmount.replace(',', ''))) {
      $('#amex-amount').val(maxAmount.replace(',', ''));
    }
    var convertionRate = $('#amex-amount').data('convertionrate');
    if (isNaN($(this).val())) {
      $(this).val('$');
      return;
    }
    var currentPoints = Math.round(parseFloat($(this).val()) / parseFloat(convertionRate));
    if (currentPoints > parseFloat($('#amex-amount').data('maxpoints'))) {
      currentPoints = parseFloat($('#amex-amount').data('maxpoints'));
    }
    $('.actual-points').empty().html(currentPoints);
    var roundOff =
      countDecimalDigits($(this).val().replace('$', '')) > 2 ? truncate(parseFloat($(this).val().replace('$', '')), 2) : $(this).val().replace('$', '');
    $(this).val('$' + roundOff);
  });
}

module.exports = {
  eventEmitters: function () {
    $('body').on('checkout:updateAmexPointsView', function (e, data) {
      var amexHTML = data.amexHTML;
      $('.amex-append-content').find('.amex-points-main-container').remove();
      $('.amex-append-content').append(amexHTML);
      $('.amex-info').addClass('d-none');
      initializeEvents();
    });
    $('body').on('checkout:removeAmexPoints', function (e, data) {
      var url = $('#checkout-main').data('amexremovepoints');
      $.ajax({
        url: url,
        success: function () {
          $('.amex-info').addClass('d-none');
          $('.paywithpoints-contianer-body').removeClass('d-none');
          $('.paywithpoints-summary').addClass('d-none');
        }
      });
    });
  },
  initializeEvents: initializeEvents,
  amexpaySummaryUpdate: function () {
    $('body').on('checkout:amexSummaryUpdate', function (e, data) {
      if (data.totals.amexpay) {
        $('.paywithpoints-summary').empty().html(data.totals.amexpay.pointSummary);
        $('body').find('.order-total-amex-amount').empty().html(data.totals.amexpay.amountApplied);
        updateOrderTotals();
        if (!data.totals.amexpay.amountApplied) {
          $('.amex-info').addClass('d-none');
        }
      }
    });
  },
  amexpayResetAlert: function () {
    $('body').on('checkout:amexpayResetAlert', function (e) {
      $('body').find('.order-total-amex-amount').empty();
      $('.amex-info').data('applied', false);
      if (!$('.amex-info').hasClass('d-none')) {
        $('.amex-info').addClass('d-none');
      }
      $.ajax({
        url: $('.amex-info').attr('data-amexreseturl'),
        type: 'get',
        success: function (data) {
          //  Just reset.
        }
      });
    });
  }
};
