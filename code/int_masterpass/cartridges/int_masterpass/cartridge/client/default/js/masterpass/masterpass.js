'use strict';

/**
 * Master pass payment click initialization function
 *
 */
function mpCheckoutInit() {
  $('#masterpass-payment').on('click', function () {
    var checkoutId = $('#masterpass-config').data('checkoutid');
    var suppress3Ds = $('#masterpass-config').data('masterpass_suppress3ds');
    var allowedCardTypes = $('#masterpass-config').data('allowedcardtypes');
    var shippingLocationProfile = $('#masterpass-config').data('shippinglocationprofile');
    var amount = $('.grand-total-value').text();
    var currency = $('#masterpass-config').data('currency');
    var cartId = $('#masterpass-config').data('cartid');
    var callbackUrl = $('#masterpass-config').data('callbackurl');

    // eslint-disable-next-line no-undef
    masterpass.checkout({
      checkoutId: checkoutId,
      allowedCardTypes: allowedCardTypes.split(','),
      amount: amount,
      currency: currency,
      shippingLocationProfile: shippingLocationProfile,
      suppress3Ds: suppress3Ds === 'true',
      cartId: cartId,
      callbackUrl: encodeURI(callbackUrl)
    });
  });
}
module.exports = mpCheckoutInit;
