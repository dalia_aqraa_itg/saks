'use strict';

var server = require('server');

var preferences = require('*/cartridge/config/preferences');
var Transaction = require('dw/system/Transaction');
var masterpassHelper = require('*/cartridge/scripts/helper/masterpassHelper');
var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');

/**
 * Renders the promodrawer.isml template.
 */
server.get('Checkout', function (req, res, next) {
  var params = req.querystring;
  var URLUtils = require('dw/web/URLUtils');
  var BasketMgr = require('dw/order/BasketMgr');
  var currentBasket = BasketMgr.getCurrentBasket();
  var mpStatus = params.mpstatus;

  COHelpers.ensureNoEmptyShipments(req);

  if (mpStatus !== 'success') {
    res.redirect(URLUtils.https('Cart-Show', 'mpstatus', mpStatus));
    return next();
  }

  delete req.session.raw.custom.associateTier; 
  delete req.session.raw.custom.specialVendorDiscountTier; 
  req.session.raw.custom.isHBCTenderType = false;

  var requestParams = {};
  requestParams.transactionId = params.oauth_verifier;
  var checkoutID = preferences.masterpass.checkoutId;
  requestParams.checkoutId = checkoutID[req.locale.id.toString()];
  requestParams.cartId = currentBasket.getUUID();
  var masterpassService = new (require('*/cartridge/scripts/init/masterpassService'))();
  var result = masterpassService.getPaymentData(requestParams);
  var allowedShippingLocations = preferences.masterpass.allowedShippingLocations || 'CA';
  // handle success service response
  if (result.getStatus() === 'OK' && result.object) {
    var responseObj = result.object;
    var mpShippingAddress = responseObj.shippingAddress;
    // If the selected shipping address in the wallet
    if (!mpShippingAddress.country || (mpShippingAddress.country && mpShippingAddress.country.indexOf(allowedShippingLocations) === -1)) {
      res.redirect(URLUtils.https('Cart-Show', 'shippingError', true, 'mpstatus', 'failure'));
      return next();
    }
    Transaction.wrap(function () {
      currentBasket.custom.masterpassTxnId = requestParams.transactionId;
      currentBasket.custom.masterpassPostBackAttempted = false;
      currentBasket.custom.masterpassWalletId = responseObj.walletId;
    });
    masterpassHelper.saveResponseDetails(responseObj, currentBasket);
    Transaction.wrap(function () {
      basketCalculationHelpers.calculateTotals(currentBasket);
    });
    res.redirect(URLUtils.https('Checkout-Begin', 'stage', 'placeOrder', 'mpstatus', mpStatus));
    return next();
  }
  res.redirect(URLUtils.https('Cart-Show', 'mpError', true));
  return next();
});

module.exports = server.exports();
