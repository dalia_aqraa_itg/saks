'use strict';

/* API Includes */
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var SortedMap = require('dw/util/SortedMap');
var preferences = require('*/cartridge/config/preferences');
var Crypto = require('dw/crypto');
var StringUtils = require('dw/util/StringUtils');

/**
 * Get OAuth signature
 *
 * @param {string} requestMethod - req method
 * @param {string} requestUrl - req URL
 * @param {dw/util/SortedMap} OAuthParams - OAUth params
 * @param {string} privateKeyAlias - private Key alias
 * @returns {string} - signed string
 */
function getOauthSignature(requestMethod, requestUrl, OAuthParams, privateKeyAlias) {
  var signatureBaseString = '';

  // step 1.1
  signatureBaseString += requestMethod;

  // step 1.2
  signatureBaseString += '&' + Crypto.Encoding.toURI(requestUrl.toLowerCase());

  // get oauth parameters in string format
  var oAuthQueryStringParams = '';
  for (var key in OAuthParams) {
    //eslint-disable-line
    oAuthQueryStringParams += key + '=' + OAuthParams.get(key) + '&';
  }
  oAuthQueryStringParams = oAuthQueryStringParams.substring(0, oAuthQueryStringParams.length - 1);

  // step 1.3
  signatureBaseString += '&' + Crypto.Encoding.toURI(oAuthQueryStringParams);

  // step 2
  var privateKey = new Crypto.KeyRef(privateKeyAlias);
  var sig = new Crypto.Signature();
  var signedString = sig.sign(StringUtils.encodeBase64(signatureBaseString), privateKey, 'SHA256withRSA');

  return signedString;
}

/**
 * Prepare OAuth Data
 *
 * @param {dw/util/SortedMap} OAuthParams - Params
 * @param {dw/util/SortedMap} requestParams - Params
 * @returns {string} - OAuth Data string
 */
function prepareOAuthData(OAuthParams, requestParams) {
  var doubleQuote = '"';
  var OAuthData = 'OAuth ';

  // eslint-disable-next-line no-restricted-syntax
  for (var key in OAuthParams) {
    // don't add request parameters
    if (!requestParams.isEmpty() && requestParams.containsKey(key)) {
      // eslint-disable-next-line no-continue
      continue;
    }
    var paramValue = OAuthParams.get(key);
    paramValue = !empty(paramValue) ? Crypto.Encoding.toURI(paramValue) : '';
    OAuthData += key + '=' + doubleQuote + paramValue + doubleQuote + ',';
  }

  return OAuthData.substr(0, OAuthData.length - 1);
}

/**
 *
 *
 * @param {string} body - Body
 * @param {string} requestMethod - Request method
 * @param {string} requestUrl - Request URL String
 * @param {dw/util/SortedMap} requestParams - Request Params
 * @returns {string} - OAuth Data string
 */
function getAuthorizationheader(body, requestMethod, requestUrl, requestParams) {
  var OAuthParams = new SortedMap();
  var Bytes = require('dw/util/Bytes');
  // add request parametrs
  if (!requestParams.isEmpty()) {
    OAuthParams.putAll(requestParams);
  }

  // prepare oauth_body_hash
  if (body != null) {
    var sha256 = new Crypto.MessageDigest(Crypto.MessageDigest.DIGEST_SHA_256);
    var hash = sha256.digestBytes(new Bytes(body, 'UTF-8'));
    OAuthParams.put('oauth_body_hash', Crypto.Encoding.toBase64(hash));
  }

  const nonce = new Crypto.SecureRandom().nextInt(1000000).toString();
  const consumerKey = preferences.masterpass.consumerKey;
  const privateKeyAlias = preferences.masterpass.certAlias;
  const timeStamp = Math.round(Date.now() / 1000).toString();

  OAuthParams.put('oauth_version', '1.0');
  OAuthParams.put('oauth_nonce', nonce);
  OAuthParams.put('oauth_timestamp', timeStamp);
  OAuthParams.put('oauth_consumer_key', consumerKey);
  OAuthParams.put('oauth_signature_method', 'RSA-SHA256');
  OAuthParams.put('oauth_signature', getOauthSignature(requestMethod, requestUrl, OAuthParams, privateKeyAlias));

  return prepareOAuthData(OAuthParams, requestParams);
}

var masterpassServiceInit = function () {
  /**
   * To get the payment data for decryption
   *
   * @param {Object} data - Input params object
   * @returns {Object} - result
   */
  this.getPaymentData = function (data) {
    var authService = LocalServiceRegistry.createService('masterpass.http.paymentdata.get', {
      createRequest: function (service, params) {
        var requestParams = new SortedMap();
        var urlWithOutParams = service.URL.toString();
        var url = urlWithOutParams;

        // set methods to "GET" as defalut would be "POST"
        service.setRequestMethod('GET');

        // check if last character of the url contains '/' if no append '/' else not
        url += url.charAt(url.length - 1).toString() === '/' ? '' : '/';

        // append trasactionId
        url += params.transactionId;
        urlWithOutParams = url;

        // set service url
        service.setURL(url);

        // append params
        if (params != null && typeof params === 'object') {
          // eslint-disable-next-line no-restricted-syntax
          for (var key in params) {
            if (key.toString() !== 'transactionId') {
              service.addParam(key, params[key]);
              requestParams.put(key, params[key]);
            }
          }
        }

        // add headers
        service
          .addHeader('User-Agent', 'www.thebay.com')
          .addHeader('Content-type', 'application/x-www-form-urlencoded; charset=UTF-8')
          .addHeader('Content-length', 0)
          .addHeader('Authorization', getAuthorizationheader(null, 'GET', urlWithOutParams, requestParams))
          .addHeader('Connection', 'Close');

        return;
      },
      parseResponse: function (service, httpClient) {
        return JSON.parse(httpClient.text);
      }
    });

    var result = authService.call(data);

    return result;
  };
  /**
   * Returns the Masterpass Postback  service
   * For POST request with body 'content-type' should be 'application/json; charset=UTF-8'
   * @param {Object} data - Input params object
   * @returns {Object} - result
   *
   */
  this.postBackService = function (data) {
    var postBackService = LocalServiceRegistry.createService('masterpass.http.postback.post', {
      createRequest: function (svc, params) {
        var url = svc.URL.toString();
        var body = JSON.stringify(params);

        // set methods to "GET" as defalut would be "POST"
        svc.setRequestMethod('POST');

        // add headers
        svc
          .addHeader('User-Agent', 'www.thebay.com')
          .addHeader('Content-type', 'application/json; charset=UTF-8')
          .addHeader('Content-length', body.length)
          .addHeader('Authorization', getAuthorizationheader(body, 'POST', url, new SortedMap()))
          .addHeader('Connection', 'Close');

        return body;
      },

      parseResponse: function (svc, response) {
        return response;
      },

      getRequestLogMessage: function (request) {
        return request;
      },

      getResponseLogMessage: function (response) {
        return response.text;
      }
    });

    var result = postBackService.call(data);

    return result;
  };
};

module.exports = masterpassServiceInit;
