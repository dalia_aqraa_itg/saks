/* eslint-disable max-len */
'use strict';
var Status = require('dw/system/Status');
var MasterPassLogger = require('dw/system/Logger').getLogger('MasterPass', 'MasterPass');
var OrderMgr = require('dw/order/OrderMgr');
var Transaction = require('dw/system/Transaction');

/** ************************************************************************************************
 * Use this service after every Masterpass transaction to communicate the result of the transaction
 * The postback service returns HTTP response code 204 for successful response.
 ***************************************************************************************************/
/**
 * execute service call function
 *
 * @returns {dw/system/Status} - returns the status
 */
function executePostbackService() {
  /**
   * Search all masterpass orders
   * custom.masterpassTxnId != NULL - This will Return all the Masterpass orders &&
   * masterpassPostBackAttempted !=true - Returns if Postback calll is not being sent yet
   */
  var masterpassOrders = OrderMgr.searchOrders('custom.masterpassPostBackAttempted !=true AND custom.masterpassTxnId != NULL', null);
  MasterPassLogger.debug('Orders count', masterpassOrders.count);

  // return if no orders found
  if (masterpassOrders.count === 0) {
    MasterPassLogger.error('[PostbackCall.js:executePostbackService] || No Masterpass orders found');
    return new Status(Status.OK, 'OK', 'successful');
  }

  while (masterpassOrders.hasNext()) {
    var order = masterpassOrders.next();
    MasterPassLogger.debug('orderNumber', order.orderNo);

    // get all masterpass payment instruments
    var paymentInstruments = order.getPaymentInstruments('CREDIT_CARD');
    var paymentInstrument = paymentInstruments[0];
    if (paymentInstrument) {
      var paymentTransaction = paymentInstrument.getPaymentTransaction();

      var paymentSuccessful = 'authorization_code' in paymentInstrument.custom;
      var paymentCode = paymentSuccessful ? paymentInstrument.custom.authorization_code : '';
      // service call parameters
      var requestParams = {};
      requestParams.transactionId = !empty(order.custom.masterpassTxnId) ? order.custom.masterpassTxnId : '';
      requestParams.currency = order.getCurrencyCode();
      requestParams.amount = !empty(paymentTransaction.amount) ? paymentTransaction.amount.value : '0.00';
      requestParams.paymentSuccessful = paymentSuccessful;
      requestParams.paymentCode = paymentCode;
      requestParams.paymentDate = new Date().toISOString();

      // call the service
      var masterpassService = new (require('*/cartridge/scripts/init/masterpassService'))();
      var result = masterpassService.postBackService(requestParams);

      // handle service error & service unavailable expection
      if (result.getStatus() === 'SERVICE_UNAVAILABLE' || result.getStatus() === 'ERROR') {
        // eslint-disable-next-line no-continue
        continue;
      }

      // handle success service response
      if (result.getStatus() === 'OK') {
        var httpClient = result.getObject();
        // commit order custom attribute 'postBackAttempted' if the response status is 204
        if (httpClient.getStatusCode() === 204) {
          // eslint-disable-next-line no-loop-func
          Transaction.wrap(function () {
            order.custom.masterpassPostBackAttempted = true;
          });
        }
      }
    }
  } // end of while statement

  return new Status(Status.OK, 'OK', 'successful');
}

/**
 * Run command for the job
 *
 * @returns {dw/system/Status} Status of the job
 */
function run() {
  var params = arguments[0];

  try {
    /* Returns true if the given {params} object contains a isDisabled property as true.
     * This will allows us to disable a step without removing it from the configuration
     */
    if (params != null && ['true', true].indexOf(params.IsDisabled) > -1) {
      return new Status(Status.OK, 'OK', 'Step disabled, skip it...');
    }

    return executePostbackService();
  } catch (e) {
    MasterPassLogger.error('[PostbackCall.js}|| Error:  {0}', e);
    return new Status(Status.ERROR, 'ERROR', 'Error occured while executing job.');
  }
}

exports.Run = run;
