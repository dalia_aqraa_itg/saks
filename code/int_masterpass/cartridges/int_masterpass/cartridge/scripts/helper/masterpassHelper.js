'use strict';

var Transaction = require('dw/system/Transaction');
var collections = require('*/cartridge/scripts/util/collections');
var PaymentInstrument = require('dw/order/PaymentInstrument');
var Site = require('dw/system/Site');
var TokenExFacade = require('*/cartridge/scripts/services/TokenExFacade');
var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');

/**
 * Creates a token. This should be replaced by utilizing a tokenization provider
 * @param {string} unEncryptedCard - Unencrypted card number
 * @returns {string} a token
 */
function createToken(unEncryptedCard) {
  var token;
  if ('enableTokenEx' in Site.current.preferences.custom && Site.current.preferences.custom.enableTokenEx === true) {
    token = TokenExFacade.tokenizeUnEncryptedCard(unEncryptedCard);
  } else {
    token = {
      error: false,
      success: true,
      token: Math.random().toString(36).substr(2),
      tokenRetry: false,
      tokenEx: false
    };
  }
  return token;
}

/**
 * Save response details in customer object
 *
 * @param {Object} response - Service response
 * @param {dw/order/Basket} currentBasket - current customer basket
 * @returns {boolean} - status
 */
function saveResponseDetails(response, currentBasket) {
  var basket = currentBasket;
  var mpCreditCard = response.card;
  var mpPersonalInfo = response.personalInfo;
  var mpShippingAddress = response.shippingAddress;
  var mpBillingAddress = mpCreditCard.billingAddress;
  var name = mpPersonalInfo.recipientName.split(' ');
  var firstName = name[0];
  var lastName = name[1];
  var tokenObj = createToken(mpCreditCard.accountNumber);

  if (mpPersonalInfo && mpPersonalInfo.recipientEmailAddress) {
    Transaction.wrap(function () {
      basket.setCustomerEmail(mpPersonalInfo.recipientEmailAddress);
    });
  }

  var billingAddress = basket.billingAddress;
  Transaction.wrap(function () {
    if (!billingAddress) {
      billingAddress = basket.createBillingAddress();
    }
    billingAddress.setFirstName(firstName || '');
    billingAddress.setLastName(lastName || '');
    billingAddress.setAddress1(mpBillingAddress.line1 || '');
    billingAddress.setAddress2(mpBillingAddress.line2 || '');
    billingAddress.setCity(mpBillingAddress.city || '');
    billingAddress.setPostalCode(mpBillingAddress.postalCode || '');
    billingAddress.setPhone(mpPersonalInfo.recipientPhone || '');
    billingAddress.setCountryCode(mpBillingAddress.country || '');
    billingAddress.setStateCode(mpBillingAddress.subdivision ? mpBillingAddress.subdivision.split('-')[1]  : '');

    billingAddress.custom.customerEmail = mpPersonalInfo.recipientEmailAddress;
  });

  var resultObj = cartHelper.hasStoreProductsInBasket(currentBasket);
  var shipToHomeShipment = resultObj.shipment;

  if (shipToHomeShipment) {
    var shippingAddress = shipToHomeShipment.getShippingAddress();
    Transaction.wrap(function () {
      if (!shippingAddress) {
        shippingAddress = shipToHomeShipment.createShippingAddress();
      }
      shippingAddress.setFirstName(firstName || '');
      shippingAddress.setLastName(lastName || '');
      shippingAddress.setAddress1(mpShippingAddress.line1 || '');
      shippingAddress.setAddress2(mpShippingAddress.line2 || '');
      shippingAddress.setCity(mpShippingAddress.city || '');
      shippingAddress.setPostalCode(mpShippingAddress.postalCode || '');
      shippingAddress.setPhone(mpPersonalInfo.recipientPhone || '');
      shippingAddress.setCountryCode(mpShippingAddress.country || '');
      shippingAddress.setStateCode(mpShippingAddress.subdivision.split('-')[1] || '');
      shippingAddress.custom.customerEmail = mpPersonalInfo.recipientEmailAddress;
    });
  }

  if (resultObj.hasBOPISItems) {
    Transaction.wrap(function () {
      var pickUpPersonData = {
        fullName: mpPersonalInfo.recipientName,
        email: mpPersonalInfo.recipientEmailAddress,
        phone: mpPersonalInfo.recipientPhone
      };
      var server = require('server');
      var instorePickUpForm = server.forms.getForm('instorepickup');
      // pre-populate data from profile
      instorePickUpForm.copyFrom(pickUpPersonData);
      basket.custom.personInfoMarkFor = JSON.stringify(pickUpPersonData);
    });
  }

  Transaction.wrap(function () {
    var paymentInstruments = basket.getPaymentInstruments();

    collections.forEach(paymentInstruments, function (item) {
      basket.removePaymentInstrument(item);
    });

    var paymentInstrument = basket.createPaymentInstrument(PaymentInstrument.METHOD_CREDIT_CARD, basket.totalGrossPrice);

    paymentInstrument.setCreditCardHolder(mpCreditCard.cardHolderName);
    paymentInstrument.setCreditCardType(mpCreditCard.brandId.charAt(0).toUpperCase() + mpCreditCard.brandId.substr(1));
    if (tokenObj.token) {
      paymentInstrument.setCreditCardNumber(tokenObj.token);
    } else if (mpCreditCard.accountNumber) {
      paymentInstrument.setCreditCardNumber(mpCreditCard.accountNumber);
    }
    paymentInstrument.setCreditCardExpirationMonth(mpCreditCard.expiryMonth);
    paymentInstrument.setCreditCardExpirationYear(mpCreditCard.expiryYear);

    if (tokenObj && tokenObj.token) {
      paymentInstrument.setCreditCardToken(tokenObj.token);
    }
  });

  return true;
}

module.exports = {
  saveResponseDetails: saveResponseDetails
};
