/**
 * Set search configuration values
 *
 * @param {dw.customer.customerMgr} customerMgr - customer
 * @param {String} preferenceType - preference type being updated
 * @param {Array} valueArray
 * @param {String} suggestedDesigner
 * @param {String} action
 */

function updateCustomerPreferences(customerMgr, preferenceType, valueArray, suggestedDesigner, action, updatingFromPA) {
  var CustomerMgr = require('dw/customer/CustomerMgr');
  var Transaction = require('dw/system/Transaction');
  var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
  var customerMgr = customerMgr; //CustomerMgr.getCustomerByCustomerNumber(req.form.customerNo);
  var preferenceType = preferenceType; //req.form.preferenceType;
  var valueArray = valueArray; //JSON.parse(req.form.value);
  var suggestedDesigner = suggestedDesigner; //req.form.suggestedDesigner;
  var action = action; //req.form.action;
  switch (preferenceType) {
    case 'CategoryPreferences':
      var CategoryPreferencesJson = JSON.parse(customerMgr.profile.custom.CategoryPreferences);
      var splitValue = valueArray[0].split(',');
      if (action == 'ADD') {
        if (CategoryPreferencesJson == null) {
          var tempObj = {};
          tempObj[splitValue[0]] = splitValue[1];
          CategoryPreferencesJson = tempObj;
        } else {
          CategoryPreferencesJson[splitValue[0]] = splitValue[1];
        }
      } else if (action == 'REMOVE') {
        delete CategoryPreferencesJson[splitValue[0]];
      }
      Transaction.wrap(function () {
        customerMgr.profile.custom.CategoryPreferences = JSON.stringify(CategoryPreferencesJson);
        if (customerMgr.profile.custom.CategoryPreferences == '{}') {
          //If All Added Data Deleted - Make Attribute EMPTY
          customerMgr.profile.custom.CategoryPreferences = '';
        }
      });
      break;
    case 'DesignerPreferences':
      var DesignerPreferencesJson = JSON.parse(customerMgr.profile.custom.DesignerPreferences);
      for (var i = 0; i < valueArray.length; i++) {
        var value = valueArray[i];
        if (action == 'ADD') {
          if (DesignerPreferencesJson == null) {
            var tempObj = {};
            tempObj[value] = value;
            DesignerPreferencesJson = tempObj;
          } else {
            DesignerPreferencesJson[value] = value;
          }
        } else if (action == 'REMOVE') {
          delete DesignerPreferencesJson[value];
        }
      }
      Transaction.wrap(function () {
        customerMgr.profile.custom.DesignerPreferences = JSON.stringify(DesignerPreferencesJson);
        if (customerMgr.profile.custom.DesignerPreferences == '{}') {
          //If All Added Data Deleted - Make Attribute EMPTY
          customerMgr.profile.custom.DesignerPreferences = '';
        }
        if (suggestedDesigner == 'true') {
          var SuggestedDesignerPreferencesJson = JSON.parse(customerMgr.profile.custom.SuggestedDesignerPreferences);
          if (SuggestedDesignerPreferencesJson != null && SuggestedDesignerPreferencesJson.hasOwnProperty(value)) {
            // If Designer from Default list from SFCC added - We dont need to DELETE that records - As Customer.SuggestedDesignerPreferences would be empty
            delete SuggestedDesignerPreferencesJson[value];
            customerMgr.profile.custom.SuggestedDesignerPreferences = JSON.stringify(SuggestedDesignerPreferencesJson);
          }
        }
      });
      break;
    case 'SizePreferences':
      var SizePreferencesJson = JSON.parse(customerMgr.profile.custom.SizePreferences);
      for (var i = 0; i < valueArray.length; i++) {
        var value = valueArray[i];
        var splitValue = value.split('#'); // ID#key|value;
        var categoryID = splitValue[0]; // ID
        if (updatingFromPA) {
          var showMySizesObj = searchHelper.showMySizesOnPLP(categoryID);
          categoryID = showMySizesObj['categoryToAddInto'];
        }
        var sizedata = splitValue[1].split('|');
        if (action == 'ADD') {
          if (SizePreferencesJson == null) {
            //Adding Data to Empty object
            var tempObj = {};
            tempObj[sizedata[0]] = sizedata[1];
            var catTempObj = {};
            catTempObj[categoryID] = tempObj;
            SizePreferencesJson = catTempObj;
          } else {
            if (SizePreferencesJson.hasOwnProperty(categoryID)) {
              //Checking if current Categegory is already Added in the object
              var CurrentCatTempObj = SizePreferencesJson[categoryID];
              CurrentCatTempObj[sizedata[0]] = sizedata[1];
              SizePreferencesJson[categoryID] = CurrentCatTempObj;
            } else {
              //Checking if current Categegory is Not in the object
              var tempObj = {};
              tempObj[sizedata[0]] = sizedata[1];
              SizePreferencesJson[categoryID] = tempObj;
            }
          }
        } else if (action == 'REMOVE') {
          //Deleting Data
          if (SizePreferencesJson.hasOwnProperty(categoryID)) {
            //Checking if current Categegory is already Added in the object
            var CurrentCatTempObj = SizePreferencesJson[categoryID];
            delete CurrentCatTempObj[sizedata[0]];
            if (JSON.stringify(CurrentCatTempObj) === '{}') {
              delete SizePreferencesJson[categoryID];
            } else {
              SizePreferencesJson[categoryID] = CurrentCatTempObj;
            }
          }
        }
      }
      Transaction.wrap(function () {
        customerMgr.profile.custom.SizePreferences = JSON.stringify(SizePreferencesJson);
        if (customerMgr.profile.custom.SizePreferences == '{}') {
          //If All Added Data Deleted - Make Attribute EMPTY
          customerMgr.profile.custom.SizePreferences = '';
        }
      });
      break;
    default:
      break;
  }
  // Updating preferencesRecentlyUpdated for PC - MW JOB to capture and send the data
  Transaction.wrap(function () {
    customerMgr.profile.custom.preferencesRecentlyUpdated = true;
  });
}

exports.updateCustomerPreferences = updateCustomerPreferences;
