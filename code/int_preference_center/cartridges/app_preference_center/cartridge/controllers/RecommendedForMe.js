var server = require('server');
var CustomerMgr = require('dw/customer/CustomerMgr');
var Transaction = require('dw/system/Transaction');
server.get('Show', function (req, res, next) {
  var template = 'product-recommendations-PA';
  var Site = require('dw/system/Site');
  var ProductMgr = require('dw/catalog/ProductMgr');
  var viewData = res.getViewData();
  var forYouLoggedIn = false;
  if (req.querystring.fy) forYouLoggedIn = req.querystring.fy;

  var apiProduct = '';
  if (viewData.product) {
    apiProduct = ProductMgr.getProduct(viewData.product.id);
  }
  res.render(template, {
    apiProduct: apiProduct,
    forYouLoggedIn: forYouLoggedIn
  });
  next();
});

module.exports = server.exports();
