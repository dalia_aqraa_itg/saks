var server = require('server');
var CustomerMgr = require('dw/customer/CustomerMgr');
var Transaction = require('dw/system/Transaction');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');

server.get('Show', userLoggedIn.validateLoggedIn, function (req, res, next) {
  var template = 'preferenceCenter';
  var Site = require('dw/system/Site');
  var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
  var customPreferences = Site.current.preferences.custom;
  // Preference Center Data
  if (req.currentCustomer.raw.profile && req.currentCustomer.raw.authenticated) {
    var customerNo = req.currentCustomer.raw.profile.customerNo;
    var DesignerPreferences = req.currentCustomer.raw.profile.custom.DesignerPreferences;
    var CategoryPreferences = req.currentCustomer.raw.profile.custom.CategoryPreferences;
    var SizePreferences = req.currentCustomer.raw.profile.custom.SizePreferences;
    var SuggestedDesignerPreferences = req.currentCustomer.raw.profile.custom.SuggestedDesignerPreferences;
    var DefaultSuggestionsPreferenceCenter = '';
    if ('DefaultSuggestionsPreferenceCenter' in customPreferences) {
      DefaultSuggestionsPreferenceCenter = customPreferences.DefaultSuggestionsPreferenceCenter;
    }
    if (DesignerPreferences) {
      DesignerPreferences = JSON.parse(DesignerPreferences);
    }
    if (CategoryPreferences) {
      CategoryPreferences = JSON.parse(CategoryPreferences);
    }
  }
  var SelectedSizes = '';
  if (SizePreferences) {
    SizePreferences = JSON.parse(SizePreferences);
    Object.keys(SizePreferences).forEach(function (catid) {
      Object.keys(SizePreferences[catid]).forEach(function (sizeid) {
        SelectedSizes = SelectedSizes + catid + '-' + sizeid + '#';
      });
    });
  }

  if (SuggestedDesignerPreferences) {
    SuggestedDesignerPreferences = JSON.parse(SuggestedDesignerPreferences);
  } else if (DefaultSuggestionsPreferenceCenter) {
    SuggestedDesignerPreferences = JSON.parse(DefaultSuggestionsPreferenceCenter); //If No Suggestion on customer record - Pull the data from Default list - TO show New Account suggestions
    if (DesignerPreferences) {
      for (var key in DesignerPreferences) {
        if (SuggestedDesignerPreferences[key]) {
          //If Designers from Default list already saved on customer preference - remove them from the list
          delete SuggestedDesignerPreferences[key];
        }
      }
    }
  } else {
    SuggestedDesignerPreferences = {};
  }

  //Category Prefernces Data
  var CatgegoryIDsPreferences;
  if ('enablePreferenceCenterCategories' in customPreferences) {
    CatgegoryIDsPreferences = [];
    let arrayLength = customPreferences.enablePreferenceCenterCategories.length;
    for (let i = 0; i < arrayLength; i++) {
      let categoryID = customPreferences.enablePreferenceCenterCategories[i];
      let startChecked = !!CategoryPreferences ? !!CategoryPreferences[categoryID] : false;

      CatgegoryIDsPreferences.push({
        id: categoryID,
        startChecked: startChecked // TODO: properly set this
      });
    }
  }

  var refinements = searchRefinements('brand');

  var BrandRefinements;
  for (i = 0; i < refinements.length; i++) {
    if (refinements[i].attributeID == 'brand') {
      BrandRefinements = refinements[i].values;
      break;
    }
  }
  var BrandRefinementsAlpha = {};
  var tempNumberArray = [];
  for (i = 0; i < BrandRefinements.length; i++) {
    var firstLetter = BrandRefinements[i].value.charAt(0).toUpperCase();

    //Check if FirstLetter is Number
    if (!(firstLetter <= '9' && firstLetter >= '0') && BrandRefinements[i].value.indexOf('À') <= -1) {
      if (!BrandRefinementsAlpha.hasOwnProperty(firstLetter)) {
        BrandRefinementsAlpha[firstLetter] = [];
      }
      BrandRefinementsAlpha[firstLetter].push(BrandRefinements[i]);
    } else {
      tempNumberArray.push(BrandRefinements[i]);
    }
    BrandRefinements[i].startChecked = !!DesignerPreferences ? !!DesignerPreferences[BrandRefinements[i].displayValue] : false;
  }
  BrandRefinementsAlpha['#'] = [];
  for (j = 0; j < tempNumberArray.length; j++) {
    BrandRefinementsAlpha['#'].push(tempNumberArray[j]);
  }

  // Sizes Refinements
  var SizeIDsPreferencesData;

  if ('enablePreferenceCenterCategoriesSizes' in customPreferences) {
    SizeIDsPreferencesData = {};
    var SizeIDsPreferences = customPreferences.enablePreferenceCenterCategoriesSizes;
    SizeIDsPreferences.forEach(function (SizeIDsPreference) {
      refinements = searchRefinements(SizeIDsPreference);
      var tempSizeData = [];
      for (i = 0; i < refinements.length; i++) {
        if (refinements[i].attributeID == 'sizeRefinement') {
          for (v = 0; v < refinements[i].values.length; v++) {
            var key = refinements[i].values[v].value;
            var value = refinements[i].values[v].displayValue;
            var tempVal = '' + value.toUpperCase();
            if (tempVal.indexOf('INTIMATES') !== -1) {
              continue;
            }
            var obj = {};
            obj[key] = value;
            tempSizeData.push(obj);
          }
          SizeIDsPreferencesData[SizeIDsPreference] = tempSizeData;
          break;
        }
      }
    });
  }

  DesignerPreferences = searchHelper.sortObjectAlphabetically(DesignerPreferences);

  var addCategoriesFlag = req.querystring.addcat ? true : false;
  res.render(template, {
    DesignerPreferences: DesignerPreferences,
    CategoryPreferences: CategoryPreferences,
    SizePreferences: SizePreferences,
    SelectedSizes: SelectedSizes,
    SuggestedDesignerPreferences: SuggestedDesignerPreferences,
    BrandRefinementsAlpha: BrandRefinementsAlpha,
    pageType: 'preference-center',
    customerNo: customerNo,
    CatgegoryIDsPreferences: CatgegoryIDsPreferences,
    SizeIDsPreferencesData: SizeIDsPreferencesData,
    addCategoriesFlag: addCategoriesFlag
  });
  next();
});

function searchRefinements(catgoryId) {
  var param = { cgid: catgoryId };
  var CatalogMgr = require('dw/catalog/CatalogMgr');
  var refineSearch = require('*/cartridge/models/bopis/refineSearch');
  var ProductSearchModel = require('dw/catalog/ProductSearchModel');
  var ProductSearch = require('*/cartridge/models/search/productSearch');
  var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
  var apiProductSearch = new ProductSearchModel();
  apiProductSearch = searchHelper.setupSearch(apiProductSearch, param);
  storeRefineResult = refineSearch.search(apiProductSearch, '');
  apiProductsearch = storeRefineResult.apiProductsearch; // eslint-disable-line
  var productSearch = new ProductSearch(apiProductSearch, param, param, CatalogMgr.getSortingOptions(), CatalogMgr.getSiteCatalog().getRoot(), true);
  var refinements = productSearch.refinements;
  return refinements;
}

server.post('updateCustomerPreferences', function (req, res, next) {
  var preferenceCenterHelper = require('*/cartridge/scripts/preferenceCenterHelper');
  var customerMgr = CustomerMgr.getCustomerByCustomerNumber(req.form.customerNo);
  var preferenceType = req.form.preferenceType;
  var valueArray = JSON.parse(req.form.value);
  var suggestedDesigner = req.form.suggestedDesigner;
  var action = req.form.action;
  var updatingFromPA = req.form.updatingFromPA;
  preferenceCenterHelper.updateCustomerPreferences(customerMgr, preferenceType, valueArray, suggestedDesigner, action, updatingFromPA);
});

module.exports = server.exports();
