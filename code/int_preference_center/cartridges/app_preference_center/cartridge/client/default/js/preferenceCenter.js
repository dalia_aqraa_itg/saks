'use strict';
document.addEventListener('DOMContentLoaded', () => {
  const myDesignersTab = document.querySelector('#my-designers-tab');
  const myCategoriesTab = document.querySelector('#my-categories-tab');
  const mySizesTab = document.querySelector('#my-sizes-tab');
  const removeRefinementBtns = document.querySelectorAll('.preference-center-refinement-close-btn');
  const addDesignerRecBtns = document.querySelectorAll('.preference-center-designer-recs__item-plus');
  const addSugesstionDesignerRecBtns = document.querySelectorAll('.suggested-designer-li');
  const designerSearchItems = document.querySelectorAll('.preference-center-refinement-search-item');
  const categoryListItems = document.querySelectorAll('.preference-center-category-list-item');
  const sizeListItems = document.querySelectorAll('.preference-center-refinement-size-item');
  myDesignersTab.addEventListener('click', preferenceCenterTabsListener);
  myCategoriesTab.addEventListener('click', preferenceCenterTabsListener);
  mySizesTab.addEventListener('click', preferenceCenterTabsListener);
  removeRefinementBtns.forEach(element => {
    element.addEventListener('click', removeRefinementBtnsListener);
  });
  addDesignerRecBtns.forEach(element => {
    element.addEventListener('click', addDesignerRecsListener);
  });
  addSugesstionDesignerRecBtns.forEach(element => {
    element.addEventListener('click', addDesignerRecsListener);
  });
  designerSearchItems.forEach(element => {
    element.addEventListener('click', designerSearchItemsListener);
  });
  categoryListItems.forEach(element => {
    element.addEventListener('click', categoryListItemsListener);
  });
  sizeListItems.forEach(element => {
    element.addEventListener('click', sizeListItemsListener);
  });
  initializeSearch();
  searchToTop();
  sizeAccordian();
  setupSelectedSizes();
});

function searchToTop() {
  $('.search_refinement_value').click(function () {
    var screensize = $(window).width();
    if (screensize < 550) {
      window.scroll(0, findPos(document.getElementById('search_refinement_value')));
    }
  });
}

function findPos(obj) {
  var curtop = -30;
  if (obj.offsetParent) {
    do {
      curtop += obj.offsetTop;
    } while ((obj = obj.offsetParent));
    return [curtop];
  }
}

function setupSelectedSizes() {
  var SelectedSizes = document.getElementById('SelectedSizes').innerHTML;
  var splitSelectedSizes = SelectedSizes.split('#');

  for (var j = 0; j < splitSelectedSizes.length; j++) {
    if (document.getElementById('list-' + splitSelectedSizes[j])) {
      document.getElementById('list-' + splitSelectedSizes[j]).classList.add('selected');
    }
  }
}

function preferenceCenterTabsListener(event) {
  const selectedTab = document.querySelector('.preference-center-tab--selected');
  const activeContent = document.querySelector('.preference-center-tabs-content--active');
  const newActiveContent = document.getElementById('my-' + event.target.id.split('-')[1] + '-content');
  selectedTab.classList.remove('preference-center-tab--selected');
  activeContent.classList.remove('preference-center-tabs-content--active');
  event.target.classList.add('preference-center-tab--selected');
  newActiveContent.classList.add('preference-center-tabs-content--active');
}

function removeRefinementBtnsListener(event) {
  let currentTabContent = event.target.closest('.preference-center-tabs-content');

  if (currentTabContent.id === 'my-designers-content') {
    removeDesigner(event.currentTarget.parentElement.dataset.key);
  } else if (currentTabContent.id === 'my-categories-content') {
    removeCategory(event.currentTarget.parentElement.dataset.key);
  } else {
    event.preventDefault();
    var parentLI = event.target.closest('li');
    removeSize(parentLI.dataset.key, parentLI.dataset.catid);
  }
}

function addDesignerRecsListener(event) {
  addDesigner(event.currentTarget.parentElement.dataset.key, event.currentTarget.parentElement.dataset.label, event);
  event.currentTarget.parentElement.remove();
  // Sugestion block
  if (event.target.classList.contains('suggested-designer') || event.target.classList.contains('suggested-designer-li')) {
    // Designer Adding from Sugested list
    if (!$('.suggested-designer').length) {
      $('#preference-center-designer-recs-SuggestedDesigner').remove();
      $('.designer-search-bar-with-sugesstions')[0].classList.remove('designer-search-bar-with-sugesstions');
      $('.designer-search-results-with-sugesstions')[0].classList.remove('designer-search-results-with-sugesstions');
    }
  }
}

function designerSearchItemsListener(event) {
  event.preventDefault();
  if (event.currentTarget.classList.contains('selected')) {
    removeDesigner(event.currentTarget.dataset.key);
    event.currentTarget.classList.remove('selected');
  } else {
    addDesigner(event.currentTarget.dataset.key, event.currentTarget.dataset.label, event);
  }
}

function addDesigner(id, label, event) {
  // add to selected designer list
  var designerList = document.querySelector('#my-designers-content .preference-center-refinement-list');

  //check that designer doesn't already exist in my designers list
  var currentDesignerLi = designerList.getElementsByClassName('preference-center-refinement');
  for (var i = 0; i < currentDesignerLi.length; i++) {
    if (currentDesignerLi[i].dataset.key === id) {
      return;
    }
  }
  var newDesignerNode = document.createElement('li');
  newDesignerNode.dataset.key = id;
  newDesignerNode.dataset.label = label;
  newDesignerNode.dataset.adobelaunchpreferencetype = event.currentTarget.dataset.adobelaunchpreferencetype;
  newDesignerNode.classList.add('preference-center-refinement');
  newDesignerNode.innerHTML = `
        <span class="preference-center-refinement-label">${label}</span>
        <div class="preference-center-refinement-close-btn">
            <div class="preference-center-refinement-x svg-svg-24-cross-thick"></div>
        </div>
    `;

  designerList.appendChild(newDesignerNode);
  designerList
    .querySelector(`li[data-key="${id}"]`)
    .querySelector('.preference-center-refinement-close-btn')
    .addEventListener('click', removeRefinementBtnsListener);

  var designerSearchList;
  var suggestedDesigner = false;
  if (event.target.classList.contains('suggested-designer') || event.target.classList.contains('suggested-designer-li')) {
    // Designer Adding from Sugested list
    suggestedDesigner = true;
    designerSearchList = document.querySelector('#preference-center-refinement-search-list-suggested');
    var nextSugestion = designerSearchList.getElementsByClassName('hide-important')[0]; //Unhiding Next sugestion from DOM
    document.querySelector('#preference-center-refinement-search-list-outer').querySelector(`a[data-key="${id}"]`).classList.add('selected');
    if (nextSugestion) {
      nextSugestion.classList.remove('hide-important');
    }
  } else {
    designerSearchList = document.querySelector('#preference-center-refinement-search-list-outer');
    designerSearchList.querySelector(`a[data-key="${id}"]`).classList.add('selected');

    //check if the selected designer is in recommended designers list and then remove if it is.
    if (document.querySelector('#preference-center-refinement-search-list-suggested')) {
      var sugested_designer_items = document
        .querySelector('#preference-center-refinement-search-list-suggested')
        .getElementsByClassName('preference-center-designer-recs__list-item');
      for (var i = 0; i < sugested_designer_items.length; i++) {
        if (sugested_designer_items[i].dataset.key === id) {
          suggestedDesigner = true;
          sugested_designer_items[i].remove();
          break;
        }
      }
    }
  }

  updateCustomerPreferences('DesignerPreferences', label, 'ADD', suggestedDesigner);
}

function removeDesigner(id) {
  // remove from the selected designer list
  var designerList = document.querySelector('#my-designers-content .preference-center-refinement-list');
  designerList.querySelector(`li[data-key="${id}"]`).remove();

  // remove the check mark
  var designerSearchList = document.querySelector('#preference-center-refinement-search-list-outer');
  var unSelectDesinger = designerSearchList.querySelector(`a[data-key="${id}"]`);
  if (unSelectDesinger) {
    unSelectDesinger.classList.remove('selected');
  }
  // ajax
  updateCustomerPreferences('DesignerPreferences', id, 'REMOVE', false);
}

function categoryListItemsListener(event) {
  event.preventDefault();
  if (event.currentTarget.classList.contains('selected')) {
    removeCategory(event.currentTarget.dataset.key, event.currentTarget.dataset.label);
    event.currentTarget.classList.remove('selected');
  } else {
    addCategory(event.currentTarget.dataset.key, event.currentTarget.dataset.label);
  }
}

function addCategory(id, label) {
  // add to selected category list
  var categoryList = document.querySelector('#my-categories-content .preference-center-refinement-list');
  var newCategoryNode = document.createElement('li');
  newCategoryNode.dataset.key = id;
  newCategoryNode.dataset.label = label;
  newCategoryNode.dataset.adobelaunchpreferencetype = event.currentTarget.dataset.adobelaunchpreferencetype;
  newCategoryNode.classList.add('preference-center-refinement');
  newCategoryNode.innerHTML = `
        <span class="preference-center-refinement-label">${label}</span>
        <div class="preference-center-refinement-close-btn">
            <div class="preference-center-refinement-x svg-svg-24-cross-thick"></div>
        </div>
    `;

  categoryList.appendChild(newCategoryNode);
  categoryList
    .querySelector(`li[data-key="${id}"]`)
    .querySelector('.preference-center-refinement-close-btn')
    .addEventListener('click', removeRefinementBtnsListener);

  // check the box in the search list
  var categorySearchList = document.querySelector('#preference-center-category-list');
  categorySearchList.querySelector(`a[data-key="${id}"]`).classList.add('selected');

  // ajax
  updateCustomerPreferences('CategoryPreferences', '' + id + ',' + label, 'ADD', false);
}

function removeCategory(id, label) {
  // remove from the selected category list
  var categoryList = document.querySelector('#my-categories-content .preference-center-refinement-list');
  categoryList.querySelector(`li[data-key="${id}"]`).remove();

  // remove the check mark
  var categorySearchList = document.querySelector('#preference-center-category-list');
  categorySearchList.querySelector(`a[data-key="${id}"]`).classList.remove('selected');

  // ajax
  updateCustomerPreferences('CategoryPreferences', '' + id + ',' + label, 'REMOVE', false);
}

function sizeListItemsListener(event) {
  event.preventDefault();
  if (event.currentTarget.classList.contains('selected')) {
    removeSize(event.currentTarget.dataset.key, event.currentTarget.dataset.catid);
    // removeSize(event.currentTarget.parentElement.dataset.key);
    event.currentTarget.classList.remove('selected');
  } else {
    addSize(
      event.currentTarget.dataset.key,
      event.currentTarget.dataset.label,
      event.currentTarget.dataset.category,
      event.currentTarget.dataset.size,
      event.currentTarget.dataset.catid,
      event
    );
  }
}

function addSize(id, label, category, size, catId, event) {
  // add to selected size list
  var sizeList = document.querySelector('#my-sizes-content .preference-center-refinement-size-list');
  var newSizeCategoryNode = document.createElement('li');
  var containsCategory = false;
  $('.preference-center-refinement-size-category').each((i, elem) => {
    if (elem.dataset.catid == catId) {
      containsCategory = true;
    }
  });

  if (!containsCategory) {
    newSizeCategoryNode.dataset.catid = catId;
    newSizeCategoryNode.classList.add('preference-center-refinement');
    newSizeCategoryNode.classList.add('preference-center-refinement-size-category');
    newSizeCategoryNode.id = 'preference-center-refinement-size-category-' + catId;

    newSizeCategoryNode.innerHTML = `
        <span class="preference-center-refinement-label preference-center-refinement-title">${category}<br/></span>
            <ul class="preference-center-refinement-sub-list" id="preference-center-refinement-sub-list-${catId}">

            </ul> 
        `;

    sizeList.appendChild(newSizeCategoryNode);

    var sizeSubList = document.querySelector('#my-sizes-content .preference-center-refinement-size-list li #preference-center-refinement-sub-list-' + catId);
    var newSizeNode = document.createElement('li');
    newSizeNode.dataset.key = id;
    newSizeNode.dataset.catid = catId;
    newSizeNode.dataset.label = label;
    newSizeNode.dataset.category = category;
    newSizeNode.dataset.size = size;
    newSizeNode.dataset.adobelaunchpreferencetype = event.currentTarget.dataset.adobelaunchpreferencetype;
    newSizeNode.classList.add('preference-center-refinement');
    newSizeNode.innerHTML = `

            <span class="preference-center-refinement-label"><br/>${size}</span>
            <div class="preference-center-refinement-close-btn">
                <div class="preference-center-refinement-x remove-sizes svg-svg-24-cross-thick"></div>
            </div>

        `;
    sizeSubList.appendChild(newSizeNode);
  } else {
    var sizeSubList = document.querySelector('#preference-center-refinement-sub-list-' + catId);
    var newSizeNode = document.createElement('li');
    newSizeNode.dataset.key = id;
    newSizeNode.dataset.label = label;
    newSizeNode.dataset.catid = catId;
    newSizeNode.dataset.category = category;
    newSizeNode.dataset.size = size;
    newSizeNode.dataset.adobelaunchpreferencetype = event.currentTarget.dataset.adobelaunchpreferencetype;
    newSizeNode.classList.add('preference-center-refinement');

    newSizeNode.innerHTML = `

            <span class="preference-center-refinement-label"><br/>${size}</span>
            <div class="preference-center-refinement-close-btn">
                <div class="preference-center-refinement-x remove-sizes svg-svg-24-cross-thick"></div>
            </div>

        `;
    sizeSubList.appendChild(newSizeNode);
  }

  var sizeSearchList = document.querySelector('#preference-center-refinement-size-list-outer');
  sizeSearchList.querySelector(`a[data-key="${id}"]`).classList.add('selected');

  //Adding remove Size listner on CROSS
  const sizeRemoveCross = document.querySelectorAll('.remove-sizes');
  sizeRemoveCross.forEach(function (cross) {
    cross.addEventListener('click', removeRefinementBtnsListener);
  });

  // ajax
  updateCustomerPreferences('SizePreferences', id, 'ADD', false);
}

function removeSize(id, catId) {
  // remove from the selected designer list
  var sizeList = document.querySelector('#my-sizes-content .preference-center-refinement-size-list');
  sizeList.querySelector(`li[data-key="${id}"]`).remove();

  // remove the check mark
  var idSplit = id.split('|');
  var dataSplit = idSplit[0].split('#');
  document.getElementById('list-' + dataSplit[0] + '-' + dataSplit[1]).classList.remove('selected');

  //check if Category is empty

  var length = $('#preference-center-refinement-sub-list-' + catId + ' li').length;
  if (length == 0) {
    $('#preference-center-refinement-size-category-' + catId).remove();
  }

  // ajax
  updateCustomerPreferences('SizePreferences', id, 'REMOVE', false);
}

function updateCustomerPreferences(preferenceType, value, action, suggestedDesigner) {
  var customerNo = document.getElementById('CustomerPreferencesCustomerNo').innerHTML;
  const updateCustomerPreferencesUrl = document.getElementById('updateCustomerPreferencesUrl').href;
  $.ajax({
    url: updateCustomerPreferencesUrl,
    type: 'post',
    dataType: 'json',
    data: {
      customerNo: customerNo,
      preferenceType: preferenceType,
      value: JSON.stringify([value]),
      suggestedDesigner: suggestedDesigner,
      action: action
    },
    success: function (data) {},
    error: function (err) {}
  });
}

function initializeSearch() {
  $('#search_refinement_value').keyup(function () {

    var input, filter, ul, li, a, i, j, txtValue;
    input = document.getElementById('search_refinement_value');
    let resetButton = document.querySelector('.designer-search-bar .close-button');
    let searchIcon = document.querySelector('.designer-search-bar .search-icon');
    filter = input.value;
    if (filter === '') {
      searchIcon.classList.remove('d-none');
      resetButton.classList.add('d-none');
    } else {
      searchIcon.classList.add('d-none');
      resetButton.classList.remove('d-none');
    }
    ul = document.getElementById('preference-center-refinement-search-list-inner');
    //Hide all Aplha tags
    let els = document.querySelectorAll('.alphaTitle');
    let alphaTitleEls = document.querySelectorAll('.alphatitleli');
    els.forEach(function (el) {
      el.classList.add('visible-hidden');
    });

    alphaTitleEls.forEach(function (alphaTitleEl) {
      alphaTitleEl.classList.add('alpha-title-padding');
    });

    var aplhaVisibleArray = [];
    li = document.querySelectorAll('.designerName');
    for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName('a')[0];
      txtValue = a.dataset.key;
      if (txtValue.toUpperCase().includes(filter.toUpperCase())) {
        li[i].style.display = '';
        //Adding ID to array
        if (!aplhaVisibleArray.includes(a.dataset.alphakey)) {
          aplhaVisibleArray.push(a.dataset.alphakey);
        }
      } else {
        li[i].style.display = 'none';
      }
    }
    for (j = 0; j < aplhaVisibleArray.length; j++) {
      let aplhaEle = document.getElementById(aplhaVisibleArray[j]);
      aplhaEle.classList.remove('visible-hidden');
      aplhaEle.closest('li').classList.remove('alpha-title-padding');
    }
  });

  //add event listener for search clear button

  $('.designer-search-bar .close-button').click(function(e){
    var input, filter, ul, li, a, i, j, txtValue;
    input = document.getElementById('search_refinement_value');
    input.value = '';
    filter = input.value;
    ul = document.getElementById('preference-center-refinement-search-list-inner');
    let resetButton = e.target;
    let searchIcon = document.querySelector('.designer-search-bar .search-icon');
    resetButton.classList.add('d-none');
    searchIcon.classList.remove('d-none');
    
    //Hide all Aplha tags
    let els = document.querySelectorAll('.alphaTitle');
    let alphaTitleEls = document.querySelectorAll('.alphatitleli');
    els.forEach(function (el) {
      el.classList.add('visible-hidden');
    });

    alphaTitleEls.forEach(function (alphaTitleEl) {
      alphaTitleEl.classList.add('alpha-title-padding');
    });

    var aplhaVisibleArray = [];
    li = document.querySelectorAll('.designerName');
    for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName('a')[0];
      txtValue = a.dataset.key;
      if (txtValue.toUpperCase().includes(filter.toUpperCase())) {
        li[i].style.display = '';
        //Adding ID to array
        if (!aplhaVisibleArray.includes(a.dataset.alphakey)) {
          aplhaVisibleArray.push(a.dataset.alphakey);
        }
      } else {
        li[i].style.display = 'none';
      }
    }
    for (j = 0; j < aplhaVisibleArray.length; j++) {
      let aplhaEle = document.getElementById(aplhaVisibleArray[j]);
      aplhaEle.classList.remove('visible-hidden');
      aplhaEle.closest('li').classList.remove('alpha-title-padding');
    }
  });
}

function sizeAccordian() {
  var acc = document.getElementsByClassName('accordion');
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener('click', function () {
      this.classList.toggle('active');

      var panel = this.nextElementSibling;
      if (panel.style.display === 'block') {
        panel.style.display = 'none';
      } else {
        panel.style.display = 'block';
      }
    });
  }
}
