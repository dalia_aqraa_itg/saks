'use-strict';
var path = require('path');
var webpack = require('sgmf-scripts').webpack;
var ExtractTextPlugin = require('sgmf-scripts')['extract-text-webpack-plugin'];
var sgmfScripts = require('sgmf-scripts');

var bootstrapPackages = {
  Alert: 'exports-loader?Alert!bootstrap/js/src/alert',
  // Button: 'exports-loader?Button!bootstrap/js/src/button',
  Carousel: 'exports-loader?Carousel!bootstrap/js/src/carousel',
  Collapse: 'exports-loader?Collapse!bootstrap/js/src/collapse',
  // Dropdown: 'exports-loader?Dropdown!bootstrap/js/src/dropdown',
  Modal: 'exports-loader?Modal!bootstrap/js/src/modal',
  // Popover: 'exports-loader?Popover!bootstrap/js/src/popover',
  Scrollspy: 'exports-loader?Scrollspy!bootstrap/js/src/scrollspy',
  Tab: 'exports-loader?Tab!bootstrap/js/src/tab',
  // Tooltip: 'exports-loader?Tooltip!bootstrap/js/src/tooltip',
  Util: 'exports-loader?Util!bootstrap/js/src/util'
};

module.exports = [
  {
    mode: 'production',
    name: 'js',
    entry: sgmfScripts.createJsPath(),
    output: {
      path: path.resolve('./cartridges/app_preference_center/cartridge/static'),
      filename: '[name].js'
    },
    resolve: {
      alias: {
        jquery: path.resolve(__dirname, '../sfra/node_modules/jquery'),
        bootstrap: path.resolve(__dirname, '../sfra/node_modules/bootstrap')
      }
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          },
          exclude: /node_modules/
        },
        {
          test: /bootstrap(.)*\.js$/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/env'],
              plugins: ['@babel/plugin-proposal-object-rest-spread'],
              cacheDirectory: true
            }
          }
        }
      ]
    },
    plugins: [new webpack.ProvidePlugin(bootstrapPackages)]
  },
  {
    mode: 'none',
    name: 'scss',
    entry: sgmfScripts.createScssPath(),
    output: {
      path: path.resolve('./cartridges/app_preference_center/cartridge/static'),
      filename: '[name].css'
    },
    resolve: {
      alias: {
        jquery: path.resolve(__dirname, '../sfra/node_modules/jquery'),
        bootstrap: path.resolve(__dirname, '../sfra/node_modules/bootstrap'),
        saks: path.resolve(__dirname, '../hbc_saksfifthavenue/cartridges/app_hbc_saksfifthavenue/cartridge/client/default/scss/')
      }
    },
    module: {
      rules: [
        {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
            use: [
              {
                loader: 'css-loader',
                options: {
                  url: false
                }
              },
              {
                loader: 'postcss-loader',
                options: {
                  plugins: [require('autoprefixer')()]
                }
              },
              {
                loader: 'sass-loader',
                options: {
                  includePaths: [path.resolve(process.cwd(), '../sfra/node_modules/')]
                }
              }
            ]
          })
        }
      ]
    },
    plugins: [new ExtractTextPlugin({ filename: '[name].css' })]
  }
];
