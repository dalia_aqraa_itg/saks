import Collection = require('../util/Collection');
import CustomerPaymentInstrument = require('./CustomerPaymentInstrument');

declare class Wallet {
  public paymentInstruments: Collection<CustomerPaymentInstrument>;

  public createPaymentInstrument(paymentMethodId: string): CustomerPaymentInstrument;
  public getPaymentInstruments(): Collection<CustomerPaymentInstrument>;
  public getPaymentInstruments(paymentMethodID: string): Collection<CustomerPaymentInstrument>;
  public removePaymentInstrument(instrument: CustomerPaymentInstrument): void;
}

export = Wallet;
