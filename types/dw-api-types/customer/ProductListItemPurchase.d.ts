import Quantity = require('../value/Quantity');
import ProductListItem = require('./ProductListItem');

declare class ProductListItemPurchase {
  public item: ProductListItem;
  public orderNo: string;
  public purchaseDate: Date;
  public purchaserName: string;
  public quantity: Quantity;

  public getItem(): ProductListItem;
  public getOrderNo(): string;
  public getPurchaseDate(): Date;
  public getPurchaserName(): string;
  public getQuantity(): Quantity;
}

export = ProductListItemPurchase;
