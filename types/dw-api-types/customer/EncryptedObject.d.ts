import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');

declare class EncryptedObject<T extends CustomAttributes> extends ExtensibleObject<T> {}

export = EncryptedObject;
