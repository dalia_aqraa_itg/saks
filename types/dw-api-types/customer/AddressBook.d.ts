import List = require('../util/List');
import CustomerAddress = require('./CustomerAddress');

declare class AddressBook {
  public addresses: List<CustomerAddress>;
  public preferredAddress: CustomerAddress | null;

  public createAddress(name: string): CustomerAddress;
  public getAddress(id: string): CustomerAddress | null;
  public getAddresses(): List<CustomerAddress>;
  public getPreferredAddress(): CustomerAddress | null;
  public removeAddress(address: CustomerAddress): void;
  public setPreferredAddress(anAddress: CustomerAddress): void;
}

export = AddressBook;
