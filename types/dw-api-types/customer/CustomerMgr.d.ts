import ObjectTypeDefinition = require('../object/ObjectTypeDefinition');
import Collection = require('../util/Collection');
import Map = require('../util/Map');
import SeekableIterator = require('../util/SeekableIterator');
import AuthenticationStatus = require('./AuthenticationStatus');
import Customer = require('./Customer');
import CustomerGroup = require('./CustomerGroup');
import CustomerList = require('./CustomerList');
import CustomerPasswordConstraints = require('./CustomerPasswordConstraints');
import Profile = require('./Profile');

/**
 * Provides helper methods for managing customers and customer profiles. Note: this class allows access to sensitive information through operations that retrieve the Profile object. Pay attention to appropriate legal and regulatory requirements related to this data.
 */
declare class CustomerMgr {
  public static authenticateCustomer(login: string, password: string): AuthenticationStatus;

  /**
   * Creates a new Customer using the supplied login, password.
   * @param login
   * @param password
   */
  public static createCustomer(login: string, password: string): Customer;

  /**
   * Creates a new Customer using the supplied login, password, and a customerNo.
   * @param login
   * @param password
   * @param customerNo
   */
  public static createCustomer(login: string, password: string, customerNo: string): Customer;

  /**
   * Given an authentication provider Id and an external Id: creates a Customer record in the system if one does not exist already for the same 'authenticationProviderId' and 'externalId' pair.
   * @param authenticationProviderId
   * @param externalId
   */
  public static createExternallyAuthenticatedCustomer(authenticationProviderId: string, externalId: string): Customer;

  /**
   * Returns the meta data for profiles.
   */
  public static describeProfileType(): ObjectTypeDefinition;

  /**
   * Returns the customer with the specified customer number.
   * @param customerNumber
   */
  public static getCustomerByCustomerNumber(customerNumber: string): Customer | null;

  /**
   * Returns the customer for the specified login name.
   * @param login
   */
  public static getCustomerByLogin(login: string): Customer | null;

  /**
   * Returns the customer associated with the specified password reset token.
   * @param token
   */
  public static getCustomerByToken(token: string): Customer | null;

  /**
   * Returns the customer group with the specified ID or null if group does not exists.
   * @param id
   */
  public static getCustomerGroup(id: string): CustomerGroup | null;

  /**
   * Returns the customer groups of the current site.
   */
  public static getCustomerGroups(): Collection<CustomerGroup>;

  /**
   * Returns the customer list identified by the specified ID.
   * @param id
   */
  public static getCustomerList(id: string): CustomerList | null;

  /**
   * Given an authentication provider Id and external Id returns the Customer Profile in our system.
   * @param authenticationProviderId
   * @param externalId
   */
  public static getExternallyAuthenticatedCustomerProfile(authenticationProviderId: string, externalId: string): Profile | null;

  /**
   * Returns an instance of CustomerPasswordConstraints for the customer list assigned to the current site.
   */
  public static getPasswordConstraints(): CustomerPasswordConstraints;

  /**
   * Returns the profile with the specified customer number.
   * @param customerNumber
   */
  public static getProfile(customerNumber: string): Profile | null;

  /**
   * Returns the number of registered customers in the system.
   */
  public static getRegisteredCustomerCount(): number;

  /**
   * Returns the customer list of the current site.
   */
  public static getSiteCustomerList(): CustomerList;

  /**
   * Checks if the given password matches the password constraints (for example password length) of the current site's assigned customerlist.
   * @param password
   */
  public static isAcceptablePassword(password: string): boolean;

  /**
     * This method authenticates the current session using the supplied login and password. If a different customer is currently authenticated in the session, then this customer is "logged out" and her/his privacy and form data are deleted. If the authentication with the given credentials fails, then null is returned and no changes to the session are made.
     *
If the input value "RememberMe" is set to True, this method stores a cookie on the customer's machine which will be used to identify the customer when the next session is initiated. The cookie is set to expire in 180 days (i.e. 6 months). Note that a customer who is remembered is not automatically authenticated and will have to explicitly log in to access any personal information.
     * @param login
     * @param password
     * @param rememberMe
     */
  public static loginCustomer(authStatus: AuthenticationStatus, rememberMe: boolean): Customer | null;

  /**
   * Logs in externally authenticated customer if it has already been created in the system and the profile is not disabled or locked
   * @param authenticationProviderId
   * @param externalId
   * @param rememberMe
   */
  public static loginExternallyAuthenticatedCustomer(authenticationProviderId: string, externalId: string, rememberMe: boolean): Customer | null;

  /**
   * Logs out the customer currently logged into the storefront.
   * @param rememberMe
   */
  public static logoutCustomer(rememberMe: boolean): Customer;

  /**
   * Executes a user-definable function on a set of customer profiles.
   * @param processFunction
   * @param querystring
   * @param args
   * @param */
  public static processProfiles(processFunction: Function, querystring: string, ...args: string[]): void;

  /**
   * Searches for a single profile instance.
   * @param querystring
   * @param args
   * @param */
  public static queryProfile(querystring: string, ...args: string[]): Profile | null;

  /**
   * Searches for profile instances.
   * @param querystring
   * @param sortstring
   * @param args
   * @param */
  public static queryProfiles(querystring: string, sortstring: string, ...args: string[]): SeekableIterator<Profile>;

  /**
   * Searches for profile instances.
   * @param queryAttributes
   * @param sortstring
   */
  public static queryProfiles(queryAttributes: Map<string, string>, sortstring: string): SeekableIterator<Profile>;

  /**
   * Logs out the supplied customer and deletes the customer record.
   * @param customer
   */
  public static removeCustomer(customer: Customer): void;

  /**
   * Searches for a single profile instance.
   * @param querystring
   * @param args
   * @param */
  public static searchProfile(querystring: string, ...args: string[]): Profile | null;

  /**
   * Searches for profile instances.
   * @param querystring
   * @param sortstring
   * @param args
   * @param */
  public static searchProfiles(querystring: string, sortstring: string, ...args: string[]): SeekableIterator<Profile>;

  /**
   * Searches for profile instances.
   * @param queryAttributes
   * @param sortstring
   */
  public static searchProfiles(queryAttributes: Map<string, string>, sortstring: string): SeekableIterator<Profile>;

  /**
   * The customer groups of the current site.
   */
  public readonly customerGroups: Collection<CustomerGroup>;

  /**
   * An instance of CustomerPasswordConstraints for the customer list assigned to the current site.
   */
  public readonly passwordConstraints: CustomerPasswordConstraints;

  /**
   * The number of registered customers in the system. This number can be used for reporting purposes.
   */
  public readonly registeredCustomerCount: number;

  /**
   * The customer list of the current site.
   */
  public readonly siteCustomerList: CustomerList;
  private constructor();
}

export = CustomerMgr;
