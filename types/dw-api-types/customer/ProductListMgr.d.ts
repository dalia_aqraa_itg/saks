import Collection = require('../util/Collection');
import Map = require('../util/Map');
import SeekableIterator = require('../util/SeekableIterator');
import Customer = require('./Customer');
import CustomerAddress = require('./CustomerAddress');
import ProductList = require('./ProductList');
import Profile = require('./Profile');

/**
 * ProductListMgr provides methods for retrieving, creating, searching for, and removing product lists.
 */
declare class ProductListMgr {
  /**
   * Creates a new instance of a product list, of the specified type.
   * @param customer
   * @param type
   */
  public static createProductList(customer: Customer, type: number): ProductList;

  /**
   * Gets the product list by its ID.
   * @param ID
   */
  public static getProductList(ID: string): ProductList | null;

  /**
   * Returns the first product list belonging to the customer with the specified profile.
   * @param profile
   * @param type
   */
  public static getProductList(profile: Profile, type: number): ProductList;

  /**
   * Retrieve all product lists of the specified type owned by the specified customer.
   * @param customer
   * @param type
   */
  public static getProductLists(customer: Customer, type: number): Collection<ProductList>;

  /**
   * Retrieve all the product lists of the specified type and event type belonging to the specified customer.
   * @param customer
   * @param type
   * @param eventType
   */
  public static getProductLists(customer: Customer, type: number, eventType: string): Collection<ProductList>;

  /**
   * Returns the collection of product lists that have the specified address as the shipping address.
   * @param customerAddress
   */
  public static getProductLists(customerAddress: CustomerAddress): Collection<ProductList>;

  /**
   * Searches for product list instances.
   * @param queryAttributes
   * @param sortString
   */
  public static queryProductLists(queryAttributes: Map<string, string>, sortString: string): SeekableIterator<ProductList>;

  /**
   * Searches for product list instances.
   * @param queryString
   * @param sortString
   * @param args
   * @param */

  public static queryProductLists(queryString: string, sortString: string, ...args: string[]): SeekableIterator<ProductList>;

  /**
   * Removes the specified product list from the system.
   * @param productList
   */
  public static removeProductList(productList: ProductList): void;
  private constructor();
}

export = ProductListMgr;
