import Collection = require('../util/Collection');
import AddressBook = require('./AddressBook');
import CustomerActiveData = require('./CustomerActiveData');
import CustomerGroup = require('./CustomerGroup');
import ExternalProfile = require('./ExternalProfile');
import OrderHistory = require('./OrderHistory');
import ProductList = require('./ProductList');
import Profile = require('./Profile');

declare class Customer {
  public readonly activeData: CustomerActiveData;
  public readonly addressBook: AddressBook;
  public readonly anonymous: boolean;
  public readonly authenticated: boolean;
  public readonly customerGroups: Collection<CustomerGroup>;
  public readonly externallyAuthenticated: boolean;
  public readonly ID: string;
  public readonly note: string;
  public readonly orderHistory: OrderHistory;
  public readonly profile: Profile | null;
  public readonly registered: boolean;
  private constructor();

  /**
   * Creates an externalProfile and attaches it to the list of external profiles for the customer
   * @param authenticationProviderId
   * @param externalId
   */
  public createExternalProfile(authenticationProviderId: string, externalId: string): ExternalProfile;

  /**
   * Returns the active data for this customer.
   */
  public getActiveData(): CustomerActiveData;

  /**
   * Returns the address book for the profile of this customer, or null if this customer has no profile, such as for an anonymous customer.
   */
  public getAddressBook(): AddressBook | null;

  /**
   * Returns the customer groups this customer is member of.
   */
  public getCustomerGroups(): Collection<CustomerGroup>;

  /**
   * A convenience method for finding an external profile among the customer's external profiles collection
   * @param authenticationProviderId
   * @param externalId
   */
  public getExternalProfile(authenticationProviderId: string, externalId: string): ExternalProfile | null;

  /**
   * Returns a collection of any external profiles the customer may have
   */
  public getExternalProfiles(): Collection<ExternalProfile>;

  /**
   * Returns the unique, system generated ID of the customer.
   */
  public getID(): string;

  /**
   * Returns the note for this customer, or null if this customer has no note, such as for an anonymous customer or when note has 0 length.
   */
  public getNote(): string | null;

  /**
   * Returns the customer order history.
   */
  public getOrderHistory(): OrderHistory;

  /**
   * Returns the product lists of the specified type.
   * @param type
   */
  public getProductLists(type: number): Collection<ProductList>;

  /**
   * Returns the customer profile.
   */
  public getProfile(): Profile | null;

  /**
   * Identifies if the customer is anonymous. An anonymous customer is the opposite of a registered customer.
   */
  public isAnonymous(): boolean;

  /**
   * Identifies if the customer is authenticated. This method checks whether this customer is the customer associated with the session and than checks whether the session in an authenticated state. Note: The pipeline debugger will always show 'false' for this value regardless of whether the customer is authenticated or not.
   */
  public isAuthenticated(): boolean;

  /**
   * Identifies if the customer is externally authenticated. An externally authenticated customer does not have the password stored in our system but logs in through an external OAuth provider (Google, Facebook, LinkedIn, etc.)
   */
  public isExternallyAuthenticated(): boolean;

  /**
   * Returns true if there exist CustomerGroup for all of the given IDs and the customer is member of at least one of that groups.
   * @param groupIDs A list of unique semantic customer group IDs.
   */
  public isMemberOfAnyCustomerGroup(...groupIDs: string[]): boolean;

  /**
   * Returns true if the customer is member of the specified CustomerGroup.
   * @param group
   */
  public isMemberOfCustomerGroup(group: CustomerGroup): boolean;

  /**
   * Returns true if there is a CustomerGroup with such an ID and the customer is member of that group.
   * @param groupID
   */
  public isMemberOfCustomerGroup(groupID: string): boolean;

  /**
   * Returns true if there exist CustomerGroup for all of the given IDs and the customer is member of all that groups.
   * @param groupIDs
   */
  public isMemberOfCustomerGroups(...groupIDs: string[]): boolean;

  /**
   * Identifies if the customer is registered. A registered customer may or may not be authenticated. This method checks whether the user has a profile.
   */
  public isRegistered(): boolean;

  /**
   * Sets the note for this customer. This is a no-op for an anonymous customer.
   * @param aValue
   */
  public setNote(aValue: string): void;
}

export = Customer;
