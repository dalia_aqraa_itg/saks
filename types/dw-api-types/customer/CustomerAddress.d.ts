import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import OrderAddress = require('../order/OrderAddress');
import EnumValue = require('../value/EnumValue');

declare global {
  namespace ICustomAttributes {
    interface CustomerAddress extends CustomAttributes {}
  }
}

declare class CustomerAddress extends ExtensibleObject<ICustomAttributes.CustomerAddress> {
  public address1: string;
  public address2: string;
  public city: string;
  public companyName: string;
  public countryCode: EnumValue<string>;
  public firstName: string;
  public fullName: string;
  public ID: string;
  public jobTitle: string;
  public lastName: string;
  public phone: string;
  public postalCode: string;
  public postBox: string;
  public salutation: string;
  public secondName: string;
  public stateCode: string;
  public suffix: string;
  public suite: string;
  public title: string;

  public getAddress1(): string;
  public getAddress2(): string;
  public getCity(): string;
  public getCompanyName(): string;
  public getCountryCode(): EnumValue<string>;
  public getFirstName(): string;
  public getFullName(): string;
  public getID(): string;
  public getJobTitle(): string;
  public getLastName(): string;
  public getPhone(): string;
  public getPostalCode(): string;
  public getPostBox(): string;
  public getSalutation(): string;
  public getSecondName(): string;
  public getStateCode(): string;
  public getSuffix(): string;
  public getSuite(): string;
  public getTitle(): string;
  public isEquivalentAddress(address: CustomerAddress | OrderAddress): boolean;

  public setAddress1(value: string): void;
  public setAddress2(value: string): void;
  public setCity(city: string): void;
  public setCompanyName(companyName: string): void;
  public setCountryCode(countryCode: string): void;
  public setFirstName(firstName: string): void;
  public setID(value: string): void;
  public setJobTitle(jobTitle: string): void;
  public setLastName(lastName: string): void;
  public setPhone(phonenumber: string): void;
  public setPostalCode(postalCode: string): void;
  public setPostBox(postBox: string): void;
  public setSalutation(value: string): void;
  public setSecondName(secondName: string): void;
  public setStateCode(state: string): void;
  public setSuffix(suffix: string): void;
  public setSuite(value: string): void;
  public setTitle(title: string): void;
}
export = CustomerAddress;
