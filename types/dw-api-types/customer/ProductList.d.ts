import Product = require('../catalog/Product');
import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Collection = require('../util/Collection');
import EnumValue = require('../value/EnumValue');
import Customer = require('./Customer');
import CustomerAddress = require('./CustomerAddress');
import ProductListItem = require('./ProductListItem');
import ProductListItemPurchase = require('./ProductListItemPurchase');
import ProductListRegistrant = require('./ProductListRegistrant');

declare global {
  namespace ICustomAttributes {
    interface ProductList extends CustomAttributes {}
  }
}

declare class ProductList extends ExtensibleObject<ICustomAttributes.ProductList> {
  public static EXPORT_STATUS_EXPORTED: number;
  public static EXPORT_STATUS_NOTEXPORTED: number;
  public static TYPE_CUSTOM_1: number;
  public static TYPE_CUSTOM_2: number;
  public static TYPE_CUSTOM_3: number;
  public static TYPE_GIFT_REGISTRY: number;
  public static TYPE_SHOPPING_LIST: number;
  public static TYPE_WISH_LIST: number;

  public anonymous: boolean;
  public coRegistrant: ProductListRegistrant;
  public currentShippingAddress: CustomerAddress;
  public description: string;
  public eventCity: string;
  public eventCountry: string;
  public eventDate: Date;
  public eventState: string;
  public eventType: string;
  public exportStatus: EnumValue<number>;
  public giftCertificateItem: ProductListItem;
  public ID: string;
  public items: Collection<ProductListItem>;
  public lastExportTime: Date;
  public name: string;
  public owner: Customer;
  public postEventShippingAddress: CustomerAddress;
  public productItems: Collection<ProductListItem>;
  public public: boolean;
  public publicItems: Collection<ProductListItem>;
  public purchases: Collection<ProductListItemPurchase>;
  public registrant: ProductListRegistrant;
  public shippingAddress: CustomerAddress;
  public type: number;

  public createCoRegistrant(): ProductListRegistrant;
  public createGiftCertificateItem(): ProductListItem;
  public createProductItem(product: Product): ProductListItem;
  public createRegistrant(): ProductListRegistrant;
  public getCoRegistrant(): ProductListRegistrant;
  public getCurrentShippingAddress(): CustomerAddress;
  public getDescription(): string;
  public getEventCity(): string;
  public getEventCountry(): string;
  public getEventDate(): Date;
  public getEventState(): string;
  public getEventType(): string;
  public getExportStatus(): EnumValue<number>;
  public getGiftCertificateItem(): ProductListItem;
  public getID(): string;
  public getItem(ID: string): ProductListItem;
  public getItems(): Collection<ProductListItem>;
  public getLastExportTime(): Date;
  public getName(): string;
  public getOwner(): Customer;
  public getPostEventShippingAddress(): CustomerAddress;
  public getProductItems(): Collection<ProductListItem>;
  public getPublicItems(): Collection<ProductListItem>;
  public getPurchases(): Collection<ProductListItemPurchase>;
  public getRegistrant(): ProductListRegistrant;
  public getShippingAddress(): CustomerAddress;
  public getType(): number;
  public isAnonymous(): boolean;
  public isPublic(): boolean;
  public removeCoRegistrant(): void;
  public removeItem(item: ProductListItem): void;
  public removeRegistrant(): void;
  public setDescription(description: string): void;
  public setEventCity(eventCity: string): void;
  public setEventCountry(eventCountry: string): void;
  public setEventDate(eventDate: Date): void;
  public setEventState(eventState: string): void;
  public setEventType(eventType: string): void;
  public setName(name: string): void;
  public setPostEventShippingAddress(address: CustomerAddress): void;
  public setPublic(flag: boolean): void;
  public setShippingAddress(address: CustomerAddress): void;
}

export = ProductList;
