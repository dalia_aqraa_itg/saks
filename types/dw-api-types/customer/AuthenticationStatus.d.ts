import Customer = require('./Customer');
/**
 * Holds the status of an authentication process.
 */
declare class AuthenticationStatus {
  /**
   * Authentication was successful
   */
  public static readonly AUTH_OK: 'AUTH_OK';

  /**
   * customer could be found, but is disabled. Password was not verified.
   */
  public static readonly ERROR_CUSTOMER_DISABLED: 'ERROR_CUSTOMER_DISABLED';

  /**
   * customer could be found, but is locked (too many failed login attempts). Password was verified before.
   */
  public static readonly ERROR_CUSTOMER_LOCKED: 'ERROR_CUSTOMER_LOCKED';

  /**
   * customer could not be found
   */
  public static readonly ERROR_CUSTOMER_NOT_FOUND: 'ERROR_CUSTOMER_NOT_FOUND';

  /**
   * Password does match, but is expired.
   */
  public static readonly ERROR_PASSWORD_EXPIRED: 'ERROR_PASSWORD_EXPIRED';

  /**
   * the used password is not correct
   */
  public static readonly ERROR_PASSWORD_MISMATCH: 'ERROR_PASSWORD_MISMATCH';

  /**
   * Any other error
   */
  public static readonly ERROR_UNKNOWN: 'ERROR_UNKNOWN';

  /**
   * checks whether the authentication was successful or not
   */
  public readonly authenticated: boolean;
  /**
   * The customer, corresponding to the login used during authentication. This customer is not logged in after authentication
   */
  public readonly customer: Customer;

  public readonly status: IAuthenticationStatus;

  private constructor();

  /**
   * The customer, corresponding to the login used during authentication.
   */
  public getCustomer(): Customer;
  /**
   * the status code (see the constants above)
   */
  public getStatus(): IAuthenticationStatus;

  /**
   * checks whether the authentication was successful or not
   */
  public isAuthenticated(): boolean;
}

type IAuthenticationStatus =
  | typeof AuthenticationStatus.AUTH_OK
  | typeof AuthenticationStatus.ERROR_CUSTOMER_DISABLED
  | typeof AuthenticationStatus.ERROR_CUSTOMER_LOCKED
  | typeof AuthenticationStatus.ERROR_PASSWORD_EXPIRED
  | typeof AuthenticationStatus.ERROR_PASSWORD_MISMATCH
  | typeof AuthenticationStatus.ERROR_UNKNOWN;

export = AuthenticationStatus;
