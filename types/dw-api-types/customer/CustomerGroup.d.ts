import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Customer = require('./Customer');

declare global {
  namespace ICustomAttributes {
    interface CustomerGroup extends CustomAttributes {}
  }
}
declare class CustomerGroup extends ExtensibleObject<ICustomAttributes.CustomerGroup> {
  public description: string;
  public ID: string;
  public ruleBased: Boolean;

  public assignCustomer(customer: Customer): void;
  public getDescription(): string;
  public getID(): string;
  public isRuleBased(): Boolean;
  public unassignCustomer(customer: Customer): void;
}

export = CustomerGroup;
