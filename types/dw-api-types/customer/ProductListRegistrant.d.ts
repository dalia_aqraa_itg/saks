import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');

declare global {
  namespace ICustomAttributes {
    interface ProductListRegistrant extends CustomAttributes {}
  }
}

declare class ProductListRegistrant extends ExtensibleObject<ICustomAttributes.ProductListRegistrant> {
  public email: string;
  public firstName: string;
  public lastName: string;
  public role: string;

  public getEmail(): string;
  public getFirstName(): string;
  public getLastName(): string;
  public getRole(): string;
  public setEmail(email: string): void;
  public setFirstName(firstName: string): void;
  public setLastName(lastName: string): void;
  public setRole(role: string): void;
}

export = ProductListRegistrant;
