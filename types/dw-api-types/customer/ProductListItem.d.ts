import Product = require('../catalog/Product');
import ProductOptionModel = require('../catalog/ProductOptionModel');
import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Collection = require('../util/Collection');
import Quantity = require('../value/Quantity');
import ProductList = require('./ProductList');
import ProductListItemPurchase = require('./ProductListItemPurchase');

declare global {
  namespace ICustomAttributes {
    interface ProductListItem extends CustomAttributes {}
  }
}

declare class ProductListItem extends ExtensibleObject<ICustomAttributes.ProductListItem> {
  public static TYPE_GIFT_CERTIFICATE: number;
  public static TYPE_PRODUCT: number;

  public ID: string;
  public list: ProductList;
  public priority: number;
  public product: Product;
  public productID: string;
  public productOptionModel: ProductOptionModel;
  public public: boolean;
  public purchasedQuantity: Quantity;
  public purchasedQuantityValue: number;
  public purchases: Collection<ProductListItemPurchase>;
  public quantity: Quantity;
  public quantityValue: number;
  public type: number;

  public createPurchase(quantity: number, purchaserName: string): ProductListItemPurchase;
  public getID(): string;
  public getList(): ProductList;
  public getPriority(): number;
  public getProduct(): Product;
  public getProductID(): string;
  public getProductOptionModel(): ProductOptionModel;
  public getPurchasedQuantity(): Quantity;
  public getPurchasedQuantityValue(): number;
  public getPurchases(): Collection<ProductListItemPurchase>;
  public getQuantity(): Quantity;
  public getQuantityValue(): number;
  public getType(): number;
  public isPublic(): boolean;
  public setPriority(priority: number): void;
  public setProductOptionModel(productOptionModel: ProductOptionModel): void;
  public setPublic(flag: boolean): void;
  public setQuantityValue(value: number): void;
}

export = ProductListItem;
