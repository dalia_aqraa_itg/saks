import CustomAttributes = require('../object/CustomAttributes');
import EnumValue = require('../value/EnumValue');
import AddressBook = require('./AddressBook');
import Credentials = require('./Credentials');
import Customer = require('./Customer');
import EncryptedObject = require('./EncryptedObject');
import Wallet = require('./Wallet');

interface IProfileCustomAttributes extends CustomAttributes {}

declare class Profile extends EncryptedObject<IProfileCustomAttributes> {
  public readonly addressBook: AddressBook;
  public birthday: Date;
  public companyName: string;
  public readonly credentials: Credentials;
  public readonly customer: Customer;
  public readonly customerNo: string;
  public email: string;
  public fax: string;
  public readonly female: boolean;
  public firstName: string;
  public gender: EnumValue<number>;
  public jobTitle: string;
  public readonly lastLoginTime: Date;
  public lastName: string;
  public readonly lastVisitTime: Date;
  public readonly male: boolean;
  public readonly nextBirthday: Date;
  public phoneBusiness: string;
  public phoneHome: string;
  public phoneMobile: string;
  public preferredLocale: string;
  public readonly previousLoginTime: Date;
  public readonly previousVisitTime: Date;
  public salutation: string;
  public secondName: string;
  public suffix: string;
  public taxID: string;
  public readonly taxIDMasked: string;
  public taxIDType: EnumValue<string>;
  public title: string;
  public readonly wallet: Wallet;

  /**
   * Returns the customer's address book.
   */
  public getAddressBook(): AddressBook;

  /**
   * Returns the customer's birthday as a date.
   */
  public getBirthday(): Date;

  /**
   * Returns the customer's company name.
   */
  public getCompanyName(): string;

  /**
   * Returns the customer's credentials.
   */
  public getCredentials(): Credentials;

  /**
   * Returns the customer object related to this profile.
   */
  public getCustomer(): Customer;

  /**
   * Returns the customer's number, which is a number used to identify the Customer.
   */
  public getCustomerNo(): string;

  /**
   * Returns the customer's email address.
   */
  public getEmail(): string;

  /**
   * Returns the fax number to use for the customer. The length is restricted to 32 characters.
   */
  public getFax(): string;

  /**
   * Returns the customer's first name.
   */
  public getFirstName(): string;

  /**
   * Returns the customer's gender.
   */
  public getGender(): EnumValue<number>;

  /**
   * Returns the customer's job title.
   */
  public getJobTitle(): string;

  /**
   * Returns the last login time of the customer.
   */
  public getLastLoginTime(): Date;

  /**
   * eturns the customer's last name.
   */
  public getLastName(): string;

  /**
   * Returns the last visit time of the customer.
   */
  public getLastVisitTime(): Date;

  /**
   * Returns the upcoming customer's birthday as a date. If the customer already had birthday this year the method returns the birthday of the next year. Otherwise its birthday in this year. If the customer has not set a birthday this method returns null.
   */
  public getNextBirthday(): Date | null;

  /**
   * Returns the business phone number to use for the customer.
   */
  public getPhoneBusiness(): string;

  /**
   * Returns the phone number to use for the customer.
   */
  public getPhoneHome(): string;

  /**
   * Returns the mobile phone number to use for the customer.
   */
  public getPhoneMobile(): string;

  /**
   * Returns the customer's preferred locale.
   */
  public getPreferredLocale(): string;

  /**
   * Returns the time the customer logged in prior to the current login.
   */
  public getPreviousLoginTime(): Date;

  /**
   * Returns the time the customer visited the store prior to the current visit.
   */
  public getPreviousVisitTime(): Date;

  /**
   * Returns the salutation to use for the customer.
   */
  public getSalutation(): string;

  /**
   * Returns the customer's second name.
   */
  public getSecondName(): string;

  /**
   * Returns the customer's suffix, such as "Jr." or "Sr.".
   */
  public getSuffix(): string;

  /**
     * Returns the tax ID value. The value is returned either plain text if the current context allows plain text access, or if it's not allowed, the ID value will be returned masked. The following criteria must be met in order to have plain text access:
     *
        - the method call must happen in the context of a storefront request;
        - the current customer must be registered and authenticated;
        - it is the profile of the current customer;
        - and the current protocol is HTTPS.
     */
  public getTaxID(): string;

  /**
   * Returns the masked value of the tax ID.
   */
  public getTaxIDMasked(): string;

  /**
   * Returns the tax ID type.
   */
  public getTaxIDType(): EnumValue<string>;

  /**
   * Returns the customer's title, such as "Mrs" or "Mr".
   */
  public getTitle(): string;

  /**
   * Returns the wallet of this customer.
   */
  public getWallet(): Wallet;

  /**
   * Indicates that the customer is female when set to true.
   */
  public isFemale(): boolean;

  /**
     * Indicates that the customer is male when set to true.

     */
  public isMale(): boolean;

  /**
   * Sets the customer's birthday as a date.
   * @param aValue
   */
  public setBirthday(aValue: Date): void;

  /**
   * Sets the customer's company name.
   * @param aValue
   */
  public setCompanyName(aValue: string): void;

  /**
   * Sets the customer's email address.
   * @param aValue
   */
  public setEmail(aValue: string): void;

  /**
   * Sets the fax number to use for the customer. The length is restricted to 32 characters.
   * @param number
   */
  public setFax(number: string): void;

  /**
   * Sets the customer's first name.
   * @param aValue
   */
  public setFirstName(aValue: string): void;

  /**
   * Sets the customer's gender
   * @param aValue
   */
  public setGender(aValue: number): void;

  /**
   * Sets the customer's job title.
   * @param aValue
   */
  public setJobTitle(aValue: string): void;

  /**
   * Sets the customer's last name.
   * @param aValue
   */
  public setLastName(aValue: string): void;

  /**
   * Sets the business phone number to use for the customer. The length is restricted to 32 characters.
   * @param number
   */
  public setPhoneBusiness(number: string): void;

  /**
   * Sets the phone number to use for the customer. The length is restricted to 32 characters.
   * @param number
   */
  public setPhoneHome(number: string): void;

  /**
   * Sets the mobile phone number to use for the customer. The length is restricted to 32 characters.
   * @param number
   */
  public setPhoneMobile(number: string): void;

  /**
   * Sets the customer's preferred locale.
   * @param aValue
   */
  public setPreferredLocale(aValue: string): void;

  /**
   * Sets the salutation to use for the customer.
   * @param salutation
   */
  public setSalutation(salutation: string): void;

  /**
   * Sets the customer's second name.
   * @param aValue
   */
  public setSecondName(aValue: string): void;

  /**
   * Sets the the customer's suffix.
   * @param aValue
   */
  public setSuffix(aValue: string): void;

  /**
   * Sets the tax ID value. The value can be set if the current context allows write access. The current context allows write access if the currently logged in user owns this profile and the connection is secured.
   * @param taxID
   */
  public setTaxID(taxID: string): void;

  /**
   * Sets the tax ID type.
   * @param taxIdType
   */
  public setTaxIDType(taxIdType: string): void;

  /**
   * Sets the customer's title.
   * @param aValue
   */
  public setTitle(aValue: string): void;
}

export = Profile;
