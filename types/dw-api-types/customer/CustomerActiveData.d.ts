import ActiveData = require('../object/ActiveData');

declare class CustomerActiveData extends ActiveData {
  public avgOrderValue: number;
  public discountValueWithCoupon: number;
  public discountValueWithoutCoupon: number;
  public giftOrders: number;
  public giftUnits: number;
  public lastOrderDate: Date;
  public orders: number;
  public orderValue: number;
  public orderValueMonth: number;

  public productMastersOrdered: string[];
  public productsAbandonedMonth: string[];
  public productsOrdered: string[];
  public productsViewedMonth: string[];
  public returns: number;
  public returnValue: number;
  public sourceCodeOrders: number;
  public topCategoriesOrdered: string[];
  public visitsMonth: number;
  public visitsWeek: number;
  public visitsYear: number;

  public getAvgOrderValue(): number;
  public getDiscountValueWithCoupon(): number;
  public getDiscountValueWithoutCoupon(): number;
  public getGiftOrders(): number;
  public getGiftUnits(): number;
  public getLastOrderDate(): Date;
  public getOrders(): number;
  public getOrderValue(): number;
  public getOrderValueMonth(): number;
  public getProductMastersOrdered(): string[];
  public getProductsAbandonedMonth(): string[];
  public getProductsOrdered(): string[];
  public getProductsViewedMonth(): string[];
  public getReturns(): number;
  public getReturnValue(): number;
  public getSourceCodeOrders(): number;
  public getTopCategoriesOrdered(): string[];
  public getVisitsMonth(): number;
  public getVisitsWeek(): number;
  public getVisitsYear(): number;
}
export = CustomerActiveData;
