/**
 * Provides access to the constraints of customer passwords. An instance of this class can be obtained via CustomerMgr.getPasswordConstraints().
 */
declare class CustomerPasswordConstraints {
  /**
   * Returns the minimum length.
   */
  public static getMinLength(): number;

  /**
   * Returns the minimum number of special characters.
   */
  public static getMinSpecialChars(): number;

  /**
   * Returns true if letters are enforced.
   */
  public static isForceLetters(): boolean;

  /**
   * Returns true if mixed case is enforced.
   */
  public static isForceMixedCase(): boolean;

  /**
   * Returns true if numbers are enforced.
   */
  public static isForceNumbers(): boolean;
  /**
   * Returns true if letters are enforced.
   */
  public readonly forceLetters: boolean;

  /**
   * Returns true if mixed case is enforced.
   */
  public readonly forceMixedCase: boolean;

  /**
   * Returns true if numbers are enforced.
   */
  public readonly forceNumbers: boolean;

  /**
   * The minimum length.
   */
  public readonly minLength: number;

  /**
   * The minimum number of special characters.
   */
  public readonly minSpecialChars: number;

  private constructor();
}

export = CustomerPasswordConstraints;
