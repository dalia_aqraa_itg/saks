import Status = require('../system/Status');

declare class Credentials {
  public authenticationProviderID: string;
  public enabled: Boolean;
  public enabledFlag: Boolean;
  public externalID: string;
  public locked: Boolean;
  public login: string;
  public passwordAnswer: string;
  public passwordQuestion: string;
  public remainingLoginAttempts: number;

  public createResetPasswordToken(): string;
  public getAuthenticationProviderID(): string;
  public getEnabledFlag(): Boolean;
  public getExternalID(): string;
  public getLogin(): string;
  public getPasswordAnswer(): string;
  public getPasswordQuestion(): string;
  public getRemainingLoginAttempts(): number;
  public isEnabled(): Boolean;
  public isLocked(): Boolean;
  public setAuthenticationProviderID(authenticationProviderID: string): void;
  public setEnabledFlag(enabledFlag: boolean): void;
  public setExternalID(externalID: string): void;
  public setLogin(newLogin: string, currentPassword: string): Boolean;
  public setPassword(newPassword: string, oldPassword: string, verifyOldPassword: boolean): Status;
  public setPasswordAnswer(answer: string): void;
  public setPasswordQuestion(question: string): void;
  public setPasswordWithToken(token: string, newPassword: string): Status;
}

export = Credentials;
