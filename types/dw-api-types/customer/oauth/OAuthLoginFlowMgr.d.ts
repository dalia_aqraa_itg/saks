import URLRedirect = require('../../web/URLRedirect');
import OAuthAccessTokenResponse = require('./OAuthAccessTokenResponse');
import OAuthFinalizedResponse = require('./OAuthFinalizedResponse');
import OAuthUserInfoResponse = require('./OAuthUserInfoResponse');
/**
 *
The OAuthLoginFlowMgr encapsulates interactions with third party OAuth providers.
 */
declare class OAuthLoginFlowMgr {
  /**
   * This method works in tandem with the initiateOAuthLogin(String) method.
   */
  public static finalizeOAuthLogin(): OAuthFinalizedResponse;

  /**
   * This method works in tandem with another method - finalizeOAuthLogin().
   * @param oauthProviderId
   */
  public static initiateOAuthLogin(oauthProviderId: string): URLRedirect;

  /**
   * This method is called internally by finalizeOAuthLogin().
   */
  public static obtainAccessToken(): OAuthAccessTokenResponse;

  /**
   * This method is called internally by finalizeOAuthLogin().
   * @param oauthProviderId
   * @param accessToken
   */
  public static obtainUserInfo(oauthProviderId: string, accessToken: string): OAuthUserInfoResponse;
  constructor();
}

export = OAuthLoginFlowMgr;
