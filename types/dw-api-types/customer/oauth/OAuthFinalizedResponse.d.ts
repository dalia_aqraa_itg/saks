import OAuthAccessTokenResponse = require('./OAuthAccessTokenResponse');
import OAuthUserInfoResponse = require('./OAuthUserInfoResponse');
/**
 * Contains the combined responses from the third-party OAuth server when finalizing the authentication.
 */
declare class OAuthFinalizedResponse {
  /**
   * The access token response
   */
  public readonly accessTokenResponse: OAuthAccessTokenResponse;

  /**
   * The user info response
   */
  public readonly userInfoResponse: OAuthUserInfoResponse;
  private constructor();

  /**
   * Returns the access token response
   */
  public getAccessTokenResponse(): OAuthAccessTokenResponse;

  /**
   * Returns the user info response
   */
  public getUserInfoResponse(): OAuthUserInfoResponse;
}

export = OAuthFinalizedResponse;
