/**
 * Contains OAuth-related artifacts from the HTTP response from the third-party OAuth server when requesting an access token
 */
declare class OAuthAccessTokenResponse {
  /**
   * The access token
   */
  public readonly accessToken: string;

  /**
   * The access token expiration
   */
  public readonly accessTokenExpiry: number;

  /**
   * The error status. In cases of errors - more detailed error information can be seen in the error log files (specifity of error details vary by OAuth provider).
   */
  public readonly errorStatus: string;

  /**
   * The OAuth provider id
   */
  public readonly oauthProviderId: string;

  /**
   * The refresh token
   */
  public readonly refreshToken: string;
  private constructor();

  /**
   * Returns the access token
   */
  public getAccessToken(): string;

  /**
   * Returns the access token expiration
   */
  public getAccessTokenExpiry(): number;

  /**
   * Returns the error status.
   */
  public getErrorStatus(): string;

  /**
   * Returns the OAuth provider id
   */
  public getOauthProviderId(): string;

  /**
   * Returns the refresh token
   */
  public getRefreshToken(): string;
}

export = OAuthAccessTokenResponse;
