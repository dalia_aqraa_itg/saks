import PaymentInstrument = require('../order/PaymentInstrument');

declare class CustomerPaymentInstrument extends PaymentInstrument {
  public bankAccountDriversLicense: string;
  public bankAccountNumber: string;
  public creditCardNumber: string;
  public getBankAccountDriversLicense(): string;
  public getBankAccountNumber(): string;
  public getCreditCardNumber(): string;
}

export = CustomerPaymentInstrument;
