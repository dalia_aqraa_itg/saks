import MarkupText = require('../content/MarkupText');
import ExtensibleObject = require('../object/ExtensibleObject');
import Collection = require('../util/Collection');
import PageMetaTag = require('../web/PageMetaTag');

/**
 * This class represents a page designer managed page. A page comprises of multiple
 * regions that hold components, which themselves again can have regions holding components,
 * i.e. spanning a hierarchical tree of components. Using the `PageMgr.renderPage(String, String)`
 * a page can be rendered. As such page implements a render function for creating render output
 * the render function of the page itself will also want to access its various properties like
 * the SEO title etc.
 */
declare class Page {
  public readonly description: string | null;
  public readonly ID: string;
  public readonly name: string;
  public readonly visible: boolean;
  public readonly pageDescription: string | null;
  public readonly pageKeywords: string | null;
  public readonly pageMetaTags: PageMetaTag[];
  public readonly pageTitle: string | null;
  public readonly typeID: string;
  private constructor();

  public getDescription(): string;
  public getID(): string;
  public getName(): string;
  public hasVisibilityRules(): boolean;
  public isVisible(): boolean;
  public getPageDescription(): string | null;
  public getPageKeywords(): string | null;
  public getPageMetaTag(id: string): PageMetaTag | null;
  public getPageMetaTags(): PageMetaTag[];
  public getPageTitle(): string | null;
}

export = Page;
