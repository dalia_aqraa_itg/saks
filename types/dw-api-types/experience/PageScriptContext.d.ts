import Map = require('dw/util/Map');
import Page = require('./Page');

/**
 * This is the context that is handed over to the `render` function of the respective page type script.
 */
declare class ComponentScriptContext {
  /** The page for which the corresponding page type script is currently executed. */
  public readonly page: Page;
  /** The `parameters` argument as passed when kicking off page rendering via `PageMgr.renderPage(String, String)`. */
  public readonly renderParameters: string;
  /** The content attributes of the page. Currently those are not merchant manageable but are solely set in your respective page type `render` function. */
  public readonly content: Map<string, object>;
  private constructor();

  public getPage(): Page;
  public getRenderParameters(): string;
  public getContent(): Map<string, object>;
}

export = ComponentScriptContext;
