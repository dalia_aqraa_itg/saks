import Collection = require('dw/util/Collection');
import PageMetaTag = require('dw/web/PageMetaTag');
import Component = require('./Component');

/**
 * This class represents a region which serves as container of components. Using the `PageMgr.renderRegion(Region)` or `PageMgr.renderRegion(Region, RegionRenderSettings)` a region can be rendered.
 */
declare class Region {
  public readonly ID: string;
  public readonly size: string;
  public readonly visibleComponents: Collection<Component>;
  private constructor();

  public getID(): string;
  public getSize(): string;
  public getVisibleComponents(): boolean;
}

export = Region;
