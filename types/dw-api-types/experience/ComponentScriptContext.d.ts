import Map = require('dw/util/Map');
import Component = require('./Component');
import ComponentRenderSettings = require('./ComponentRenderSettings');

declare class ComponentScriptContext {
  public readonly component: Component;
  public readonly componentRenderSettings: ComponentRenderSettings;
  public readonly content: Map<string, object>;
  private constructor();

  public getComponent(): Component;
  public getComponentRenderSettings(): ComponentRenderSettings;
  public getContent(): Map<string, object>;
}

export = ComponentScriptContext;
