import Map = require('../util/Map');

declare class FormElementValidationResult {
  public data: Map<string, string>;
  public message: string;
  public valid: Boolean;

  constructor(valid: Boolean);
  constructor(valid: boolean, message: string);
  constructor(valid: boolean, message: string, data: Map<string, string>);

  public addData(key: Object, value: Object): void;
  public getData(): Map<string, string>;
  public getMessage(): string;
  public isValid(): Boolean;
  public setMessage(message: string): void;
  public setValid(valid: Boolean): void;
}

export = FormElementValidationResult;
