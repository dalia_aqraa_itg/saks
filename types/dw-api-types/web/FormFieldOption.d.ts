import FormField = require('./FormField');
/**
 * Represents an option for a form field.
 */
declare class FormFieldOption {
  /**
   * Identifies if this option is checked.
   */
  public readonly checked: boolean;

  /**
   * The value for the HTML value attribute of a HTML option element.
   */
  public readonly htmlValue: string;

  /**
   * The value for the HTML label attribute of the HTML option element. If not specified in the form option definition the label is identical with the string representation of option value (see getValue()).
   */
  public label: string;

  /**
   * The object that was bound to this option value.
   */
  public readonly object: Object;

  /**
   * The ID of the option. This is an internal ID used to uniquely reference this option. If not specified in the form option definition the ID is identical with the string representation of the option value (see getValue()).
   */
  public readonly optionId: string;

  /**
   * The parent, which is a field element.
   */
  public readonly parent: FormField;

  /**
   * Identifies if this option is selected.
   */
  public readonly selected: boolean;

  /**
   * The actual value associated with this option. This value is formatted and than returned as HTML value with the method getHtmlValue().
   */
  public readonly value: Object;
  private constructor();

  /**
   * Returns the value for the HTML value attribute of a HTML option element.
   */
  public getHtmlValue(): string;

  /**
   * Returns the value for the HTML label attribute of the HTML option element.
   */
  public getLabel(): string;

  /**
   * Returns the object that was bound to this option value.
   */
  public getObject(): Object;

  /**
   * Returns the ID of the option.
   */
  public getOptionId(): string;

  /**
   * The parent, which is a field element.
   */
  public getParent(): FormField;

  /**
   * The actual value associated with this option.
   */
  public getValue(): Object;

  /**
   * Identifies if this option is checked.
   */
  public isChecked(): boolean;

  /**
   * Identifies if this option is selected.
   */
  public isSelected(): boolean;

  /**
   * Sets the label attribute for this option.
   * @param label
   */
  public setLabel(label: string): void;
}

export = FormFieldOption;
