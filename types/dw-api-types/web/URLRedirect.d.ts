/**
 * Represents a URLRedirect in Commerce Cloud Digital.
 */
declare class URLRedirect {
  /**
   * The URL which was calculated to be the redirect URL. The Location parameter can be directly used as value for an redirect location.
   */
  public readonly location: string;

  /**
   * The corresponding status code for the redirect location.
   */
  public readonly status: number;

  protected constructor();

  /**
   * Returns the URL which was calculated to be the redirect URL.
   */
  public getLocation(): string;

  /**
   * Returns the corresponding status code for the redirect location.
   */
  public getStatus(): number;
}

export = URLRedirect;
