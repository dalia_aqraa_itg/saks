declare class CSRFProtection {
  /**
   * Constructs a new unique CSRF token for this session.
   */
  public static generateToken(): string;

  /**
   * Returns the system generated CSRF token name.
   */
  public static getTokenName(): string;

  /**
   * Verifies that a client request contains a valid CSRF token, and that the token has not expired.
   */
  public static validateRequest(): boolean;

  /**
   * The system generated CSRF token name. Currently, this name is not user configurable. Must be used for validateRequest() to work
   */
  public readonly tokenName: string;
  private constructor();
}

export = CSRFProtection;
