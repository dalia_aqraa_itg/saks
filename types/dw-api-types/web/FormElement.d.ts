import FormElementValidationResult = require('./FormElementValidationResult');

declare class FormElement {
  public dynamicHtmlName: string;
  public formId: string;
  public htmlName: string;
  public parent: FormElement;
  public valid: boolean;
  public validationResult: FormElementValidationResult;

  public clearFormElement(): void;
  public getDynamicHtmlName(): string;
  public getFormId(): string;
  public getHtmlName(): string;
  public getParent(): FormElement;
  public getValidationResult(): FormElementValidationResult;
  public invalidateFormElement(): void;
  public invalidateFormElement(error: string): void;
  public isValid(): boolean;
}

export = FormElement;
