import FormElement = require('./FormElement');
import FormGroup = require('./FormGroup');

declare class Form extends FormGroup {
  public secureKeyHtmlName: string;
  public secureKeyValue: string;

  public getSecureKeyHtmlName(): string;
  public getSecureKeyValue(): string;
}

export = Form;
