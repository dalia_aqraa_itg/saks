import FormElement = require('./FormElement');

declare class FormAction extends FormElement {
  public description: string;
  public label: string;
  public object: object;
  public submitted: boolean;
  public triggered: boolean;
  public x: number;
  public y: number;

  public getDescription(): string;
  public getLabel(): string;
  public getObject(): object;
  public getX(): number;
  public getY(): number;
  public isSubmitted(): boolean;
  public isTriggered(): boolean;
}

export = FormAction;
