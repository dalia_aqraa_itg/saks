declare class PageMetaTag {
  /**
   * The page meta tag content.
   */
  public readonly content: string;
  /**
   * The page meta tag ID.
   */
  public readonly ID: string;
  /**
   * Returns true if the page meta tag type is name, false otherwise.
   */
  public readonly name: boolean;
  /**
   * Returns true if the page meta tag type is property, false otherwise.
   */
  public readonly property: boolean;
  /**
   * Returns true if the page meta tag type is title, false otherwise.
   */
  public readonly title: boolean;
  private constructor();
}

export = PageMetaTag;
