import Collection = require('../util/Collection');
import Iterator = require('../util/Iterator');
import URL = require('./URL');

/**
 * A page model is a helper class to apply a pages to a collection of elements or an iterator of elements and supports creating URLs for continued paging through the elements. The page model is intended to be initialized with the collection or iterator, than the paging position is applyed and than the elements are extracted with getPageElements(). In case the page model is initialized with a collection the page model can be reused multiple times.
 */
declare class PagingModel<T> {
  /**
   * The default page size.
   */
  public static readonly DEFAULT_PAGE_SIZE: number;

  /**
   * The maximum supported page size.
   */
  public static readonly MAX_PAGE_SIZE: number;

  /**
   * The URL Parameter used for the page size.
   */
  public static readonly PAGING_SIZE_PARAMETER: string;

  /**
   * The URL parameter used for the start position.
   */
  public static readonly PAGING_START_PARAMETER: string;
  // new (elements : Collection<T>) : PagingModel<T>

  /**
   * Returns an URL containing the page size parameter appended to the specified url.
   * @param url
   * @param pageSize
   */
  public static appendPageSize(url: URL, pageSize: number): URL;

  /**
   * The count of the number of items in the model.
   */
  public readonly count: number;

  /**
   * The index number of the current page. The page counting starts with 0. The method also works with a miss-aligned start. In that case the start is always treated as the start of a page.
   */
  public readonly currentPage: number;

  /**
   * Identifies if the model is empty.
   */
  public readonly empty: boolean;

  /**
   * The index of the last element on the current page.
   */
  public readonly end: number;

  /**
   * The maximum possible page number. Counting for pages starts with 0. The method also works with a miss-aligned start. In that case the returned number might be higher than ((count-1) / pageSize).
   */
  public readonly maxPage: number;

  /**
   * The total page count. The method also works with a miss-aligned start. In that case the returned number might be higher than (count / pageSize).
   */
  public readonly pageCount: number;

  /**
   * An iterator that can be used to iterate through the elements of the current page. In case of a collection as the page models source, the method can be called multiple times. Each time a fresh iterator is returned. In case of an iterator as the page models source, the method must be called only once. The method will always return the same iterator, which means the method amy return an exhausted iterator.
   */
  public readonly pageElements: Iterator<T>;

  /**
   * The size of the page.
   */
  public pageSize: number;

  /**
   * The current start position from which iteration will start.
   */
  public start: number;

  /**
   * Constructs the PagingModel using the specified iterator and count value.
   * @param elements
   * @param count
   */
  public constructor(elements: Iterator<T>, count: number);
  // new (elements : Iterator<T>, count : number) : PagingModel<T>

  /**
   * Constructs the PagingModel using the specified collection.
   * @param elements
   */
  public constructor(elements: Collection<T>);

  /**
   * Returns an URL by appending the current page start position and the current page size to the URL.
   * @param url
   */
  public appendPaging(url: URL): URL;

  /**
   * Returns an URL by appending the paging parameters for a desired page start position and the current page size to the specified url.
   * @param url
   * @param position
   */
  public appendPaging(url: URL, position: number): URL;

  /**
   * Returns the count of the number of items in the model.
   */
  public getCount(): number;

  /**
   * Returns the index number of the current page.
   */
  public getCurrentPage(): number;

  /**
   * Returns the index of the last element on the current page.
   */
  public getEnd(): number;

  /**
   * Returns the maximum possible page number.
   */
  public getMaxPage(): number;

  /**
   * Returns the total page count.
   */
  public getPageCount(): number;

  /**
   * Returns an iterator that can be used to iterate through the elements of the current page.
   */
  public getPageElements(): Iterator<T>;

  /**
   * Returns the size of the page.
   */
  public getPageSize(): number;

  /**
   * Returns the current start position from which iteration will start.
   */
  public getStart(): number;

  /**
   * Identifies if the model is empty.
   */
  public isEmpty(): boolean;

  /**
   * Sets the size of the page.
   * @param pageSize
   */
  public setPageSize(pageSize: number): void;

  /**
   * Sets the current start position from which iteration will start.
   * @param start
   */
  public setStart(start: number): void;
}

export = PagingModel;
