import List = require('../util/List');
import ClickStreamEntry = require('./ClickStreamEntry');

declare class ClickStream {
  public clicks: List<ClickStreamEntry>;
  public first: ClickStreamEntry;
  public last: ClickStreamEntry;
  public partial: boolean;

  public getClicks(): List<ClickStreamEntry>;
  public getFirst(): ClickStreamEntry;
  public getLast(): ClickStreamEntry;
  public isPartial(): boolean;
}

export = ClickStream;
