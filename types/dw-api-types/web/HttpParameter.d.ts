import Collection = require('../util/Collection');

/**
 * Represents an HTTP parameter.
 */
declare class HttpParameter {
  /**
   * The value of the current HttpParameter attribute as a boolean. If there is more than one value defined, only the first one is returned. For an undefined attribute it returns null.
   */
  public readonly booleanValue: boolean | null;
  /**
   * The value of the current HttpParameter attribute as a date. If there is more than one value defined, only the first one is returned. For an undefined attribute and if attribute is not a date it return null.
   */
  public readonly dateValue: Date | null;

  /**
   * The value of the current HttpParameter attribute as a number. If there is more than one value defined, only the first one is returned. For an undefined attribute it returns 0.0.
   */
  public readonly doubleValue: number;

  /**
   * Identifies if there is a value for the http parameter attribute and whether the value is empty. A value is treated as empty if it's not blank.
   */
  public readonly empty: boolean;

  /**
   * The value of the current HttpParameter attribute as int. If there is more than one value defined, only the first one is returned. For an undefined attribute it returns null.
   */
  public readonly intValue: number | null;

  /**
   * The raw value for this HttpParameter instance. The raw value is the not trimmed String value of this HTTP parameter. If there is more than one value defined, only the first one is returned. For an undefined attribute the method returns a null.
   */
  public readonly rawValue: string | null;

  /**
   * A Collection of all raw values for this HTTP parameter. The raw value is the not trimmed String value of this HTTP parameter.
   */
  public readonly rawValues: Collection<string>;

  /**
   * The value of the current HttpParameter attribute. If there is more than one value defined, only the first one is returned. For an undefined attribute the method returns a null.
   */
  public readonly stringValue: string | null;

  /**
   * A Collection of all defined values for this HTTP parameter.
   */
  public readonly stringValues: Collection<string>;

  /**
   * Identifies if the parameter was submitted. This is equivalent to the check, whether the parameter has a value.
   */
  public readonly submitted: boolean;

  /**
   * The value of the current HttpParameter attribute. If there is more than one value defined, only the first one is returned. For an undefined attribute the method returns null.
   */
  public readonly value: string | null;

  /**
   * A Collection of all defined values for this current HTTP parameter.
   */
  public readonly values: Collection<string>;

  protected constructor();

  /**
   * Identifies if the given value is part of the actual values.
   * @param value
   */
  public containsStringValue(value: string): boolean;

  /**
   * Returns the value of the current HttpParameter attribute as a boolean. If there is more than one value defined, only the first one is returned. For an undefined attribute it returns null.
   */
  public getBooleanValue(): boolean | null;

  /**
   * Returns the value of the current HttpParameter attribute as a boolean. If there is more than one value defined, only the first one is returned. For an undefined attribute it returns the given default value.
   * @param defaultValue
   */
  public getBooleanValue(defaultValue: boolean): boolean;

  /**
   * Returns the value of the current HttpParameter attribute as a date. If there is more than one value defined, only the first one is returned. For an undefined attribute and if attribute is not a date it return null.
   */
  public getDateValue(): Date | null;

  /**
   * Returns the value of the current HttpParameter attribute as a date. If there is more than one value defined, only the first one is returned. For an undefined attribute it returns the given default value and if the attributes is not a date it returns null.
   * @param defaultValue
   */
  public getDateValue(defaultValue: Date): Date;

  /**
   * Returns the value of the current HttpParameter attribute as a number. If there is more than one value defined, only the first one is returned. For an undefined attribute it returns 0.0.
   */
  public getDoubleValue(): number;

  /**
   * Returns the value of the current HttpParameter attribute as a number. If there is more than one value defined, only the first one is returned. For an undefined attribute it returns the given default value.
   * @param defaultValue
   */
  public getDoubleValue(defaultValue: number): number;

  /**
   * Returns the value of the current HttpParameter attribute as int. If there is more than one value defined, only the first one is returned. For an undefined attribute it returns null.
   */
  public getIntValue(): number | null;

  /**
   * Returns the value of the current HttpParameter attribute as an integer. If there is more than one value defined, only the first one is returned. For an undefined attribute it returns the given default value.
   * @param defaultValue
   */
  public getIntValue(defaultValue: number): number;

  /**
   * Returns the raw value for this HttpParameter instance. The raw value is the not trimmed String value of this HTTP parameter. If there is more than one value defined, only the first one is returned. For an undefined attribute the method returns a null.
   */
  public getRawValue(): string | null;

  /**
   * Returns a Collection of all raw values for this HTTP parameter. The raw value is the not trimmed String value of this HTTP parameter.
   */
  public getRawValues(): Collection<string>;

  /**
   * Returns the value of the current HttpParameter attribute. If there is more than one value defined, only the first one is returned. For an undefined attribute the method returns a null.
   */
  public getStringValue(): string | null;

  /**
   * Returns the value of the current HttpParameter attribute. If there is more than one value defined, only the first one is returned. For an undefined attribute the method returns the given default value.
   * @param defaultValue
   */
  public getStringValue(defaultValue: string): string;

  /**
   * Returns a Collection of all defined values for this HTTP parameter.
   */
  public getStringValues(): Collection<string>;

  /**
   * Returns the value of the current HttpParameter attribute. If there is more than one value defined, only the first one is returned. For an undefined attribute the method returns null.
   */
  public getValue(): string | null;

  /**
   * Returns a Collection of all defined values for this current HTTP parameter.
   */
  public getValues(): Collection<string>;

  /**
   * Identifies if the given String is an actual value of this http parameter.
   * @param value
   */
  public isChecked(value: string): boolean;
  /**
   * Identifies if there is a value for the http parameter attribute and whether the value is empty. A value is treated as empty if it's not blank.
   */
  public isEmpty(): boolean;

  /**
   * Identifies if the parameter was submitted. This is equivalent to the check, whether the parameter has a value.
   */
  public isSubmitted(): boolean;
  public toString(): string;
}

export = HttpParameter;
