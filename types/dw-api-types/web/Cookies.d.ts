declare class Cookies {
  /**
   * The number of known cookies.
   */
  public cookieCount: number;
  /** Returns the number of known cookies. */
  public getCookieCount(): number;
}

export = Cookies;
