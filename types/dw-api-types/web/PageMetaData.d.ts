declare class PageMetaData {
  public description: string;
  public keywords: string;
  public title: string;

  public getDescription(): string;
  public getKeywords(): string;
  public getTitle(): string;

  public setDescription(description: string): void;
  public setKeywords(keywords: string): void;
  public setTitle(title: string): void;
}

export = PageMetaData;
