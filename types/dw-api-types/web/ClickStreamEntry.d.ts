declare class ClickStreamEntry {
  /**
   * The host.
   */
  public readonly host: string;

  /**
   * The locale sent from the user agent.
   */
  public readonly locale: string;

  /**
   * The path.
   */
  public readonly path: string;

  /**
   * The name of the called pipeline. In most cases the name can be derived from the path, but not in all cases. If with URL rewritting a special landing page is defined for a DNS name, than the system internally might use a specific pipeline associated with this landing page.
   */
  public readonly pipelineName: string;

  /**
   * The query string.
   */
  public readonly queryString: string;

  /**
   * The referer.
   */
  public readonly referer: string;

  /**
   * The remote address.
   */
  public readonly remoteAddress: string;

  /**
   * The entry's timestamp.
   */
  public readonly timestamp: number;

  /**
   * The full URL for this click. The URL is returned as relative URL.
   */
  public readonly url: string;

  /**
   * The user agent.
   */
  public readonly userAgent: string;
  private constructor();

  /**
   * Returns the host.
   */
  public getHost(): string;

  /**
   * Returns the locale sent from the user agent.
   */
  public getLocale(): string;

  /**
   * Returns a specific parameter value from the stored query string.
   * @param name
   */
  public getParameter(name: string): string;

  /**
   * Returns the path.
   */
  public getPath(): string;

  /**
   * Returns the name of the called pipeline.
   */
  public getPipelineName(): string;

  /**
   * Returns the query string.
   */
  public getQueryString(): string;

  /**
   * Returns the referer.
   */
  public getReferer(): string;

  /**
   * Returns the remote address.
   */
  public getRemoteAddress(): string;

  /**
   * Returns the entry's timestamp.
   */
  public getTimestamp(): Number;

  /**
   * Returns the full URL for this click.
   */
  public getUrl(): string;

  /**
   * Returns the user agent.
   */
  public getUserAgent(): string;
}

export = ClickStreamEntry;
