import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');

declare global {
  namespace ICustomAttributes {
    interface ServiceProfile extends CustomAttributes {}
  }
}

declare class ServiceProfile extends ExtensibleObject<ICustomAttributes.ServiceProfile> {
  /**
   * Returns the unique Service ID.
   */
  public getID(): string;
  /**
   * Returns the interval of the circuit breaker in milliseconds.
   */
  public getCbMillis(): number;
  /**
   * Returns the maximum number of errors in an interval allowed by the circuit breaker.
   */
  public getCbCalls(): number;
  /**
   * Returns the interval of the rate limiter in milliseconds.
   */
  public getRateLimitMillis(): number;
  /**
   * Returns the maximum number of calls in an interval allowed by the rate limiter.
   */
  public getRateLimitCalls(): number;
  /**
   * Returns the service call timeout in milliseconds.
   */
  public getTimeoutMillis(): number;
}

export = ServiceProfile;
