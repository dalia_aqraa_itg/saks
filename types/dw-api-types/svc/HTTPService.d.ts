import File = require('../io/File');
import HTTPClient = require('../net/HTTPClient');
import Service = require('./Service');

/**
 * Represents an HTTP Service.
 */
declare class HTTPService<T, K> extends Service<T, K> {
  /**
   * The authentication type.
   */
  public authentication: string;

  /**
   * The caching time to live value.
   */
  public cachingTTL: number;

  /**
   * The underlying HTTP client object.
   */
  public readonly client: HTTPClient;

  /**
   * The request body encoding to declare.
   */
  public encoding: string;

  /**
   * The output file, or null if there is none.
   */
  public outFile: File | null;

  /**
   * The request method.
   */
  public requestMethod: string;

  protected constructor();

  /**
   * Adds an HTTP Header.
   * @param name
   * @param val
   */
  public addHeader(name: string, val: string): this;

  /**
   * Adds a query parameter that will be appended to the URL.
   * @param name
   * @param val
   */
  public addParam(name: string, val: string): this;

  /**
   * Returns the authentication type.
   */
  public getAuthentication(): string;

  /**
   * Returns the caching time to live value.
   */
  public getCachingTTL(): number;

  /**
   * Returns the underlying HTTP client object.
   */
  public getClient(): HTTPClient;

  /**
   * Returns the request body encoding to declare.
   */
  public getEncoding(): string;

  /**
   *  Returns the output file, or null if there is none.
   */
  public getOutFile(): File | null;

  /**
   * Returns the request method.
   */
  public getRequestMethod(): string;

  /**
   * Sets the type of authentication.
   * @param authentication
   */
  public setAuthentication(authentication: string): this;

  /**
   * Enables caching for GET requests.
   * @param ttl
   */
  public setCachingTTL(ttl: number): this;

  /**
   * Sets the encoding of the request body (if any).
   * @param encoding
   */
  public setEncoding(encoding: string): this;

  /**
   * Sets the output file in which to write the HTTP response body.
   * @param outFile
   */
  public setOutFile(outFile: File): this;

  /**
   * Sets the HTTP request method.
   * @param requestMethod
   */
  public setRequestMethod(requestMethod: string): this;
}

export = HTTPService;
