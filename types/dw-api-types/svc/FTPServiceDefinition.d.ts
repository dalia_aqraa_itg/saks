import ServiceDefinition = require('./ServiceDefinition');

declare class FTPServiceDefinition extends ServiceDefinition {
  /**
     * The status of whether the     isAutoDisconnectttt: any;
underlying FTP connection will be disconnected after the service call.
     */
  public autoDisconnect: boolean;
  private constructor();

  /**
   * Returns the status of whether the underlying FTP connection will be disconnected after the service call.
   */
  public isAutoDisconnect(): boolean;

  /**
   *
   * @param b Sets the auto-disconnect flag.
   */
  public setAutoDisconnect(b: boolean): this;
}

export = FTPServiceDefinition;
