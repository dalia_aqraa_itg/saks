import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import ServiceCredential = require('./ServiceCredential');
import ServiceProfile = require('./ServiceProfile');

declare global {
  namespace ICustomAttributes {
    interface ServiceConfig extends CustomAttributes {}
  }
}
/**
 * Configuration object for Services.
 */
declare class ServiceConfig extends ExtensibleObject<ICustomAttributes.ServiceConfig> {
  /**
   * Returns the unique Service ID.
   */
  public getID(): string;
  /**
   * Returns the type of the service, such as HTTP or SOAP.
   */
  public getServiceType(): string;
  /**
   * Returns the related service credentials.
   */
  public getCredential(): ServiceCredential;
  /**
   * Returns the related service profile.
   */
  public getProfile(): ServiceProfile;
}

export = ServiceConfig;
