import Service = require('./Service');

/**
 * Represents an HTTP Service.
 */
declare class SOAPService<T, K> extends Service<T, K> {
  /**
   * The authentication type.
   */
  public authentication: string;

  /**
   * The serviceClient object.
   */
  public serviceClient: Object;

  protected constructor();

  /**
   * Returns the authentication type.
   */
  public getAuthentication(): String;

  /**
   * Returns the serviceClient object.
   */
  public getServiceClient(): Object;

  /**
   * Sets the type of authentication.
   * @param authentication
   */
  public setAuthentication(authentication: string): this;

  /**
   * Sets the serviceClient object.
   * @param o
   */
  public setServiceClient(o: Object): this;
}

export = SOAPService;
