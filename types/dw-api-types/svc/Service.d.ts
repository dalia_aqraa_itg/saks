import Result = require('./Result');
import ServiceConfig = require('./ServiceConfig');
/**
 * Base class of Services.
A service represents a call-specific configuration. Configurations are inherited from a shared ServiceDefinition, and can be overridden for specific calls on the service.
 */
declare class Service<REQ, RESP> {
  /**
   * Invokes the service.
   */
  public call(...args: any[]): Result<RESP>;
  /**
   * Returns the property that stores the object returned by createRequest.
   */
  public getRequestData(): REQ;
  /**
   * Returns the property that stores the object returned by the service.
   *
   * This property is only useful after the service [Service.call(Object[])](https://info.demandware.com/DOC2/index.jsp?topic=%2Fcom.demandware.dochelp%2FDWAPI%2Fscriptapi%2Fhtml%2Fapi%2Fclass_dw_svc_Service.html&anchor=dw_svc_Service_call_Object_DetailAnchor) completes, and is the same as the object
   * inside the [Result](https://info.demandware.com/DOC2/index.jsp?topic=%2Fcom.demandware.dochelp%2FDWAPI%2Fscriptapi%2Fhtml%2Fapi%2Fclass_dw_svc_Result.html).
   */
  public getResponse(): RESP;
  /**
   * Forces the mock mode to be enabled.
   */
  public setMock(): this;
  /**
   * Returns the status of whether this service is executing in mock mode.
   */
  public isMock(): boolean;
  /**
   * Forces a Service to throw an error when there is a problem instead of returning a Result with non-OK status.
   */
  public setThrowOnError(): this;
  /**
   * Returns the status of whether this service will throw an error when encountering a problem.
   */
  public isThrowOnError(): boolean;
  /**
   * Override the URL to the given value. Any query parameters (if applicable) will be appended to this URL.
   */
  public setURL(url: string): this;
  /**
   * Override the Credential by the credential object with the given ID.
   *
   * If the URL is also overridden, that URL will continue to override the URL in this credential.
   */
  public setCredentialID(id: string): this;
  /**
   * Returns the ID of the currently associated Credential.
   */
  public getCredentialID(): string;
  /**
   * Returns the current URL, excluding any custom query parameters.
   */
  public getURL(): string;
  /**
   * Returns the Service Configuration.
   */
  public getConfiguration(): ServiceConfig;
}

export = Service;
