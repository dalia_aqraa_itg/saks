import Service = require('./Service');
import ServiceCallback = require('./ServiceCallback');
import ServiceDefinition = require('./ServiceDefinition');

/**
 * @deprecated
 */
declare class ServiceRegistry {
  /**
   * Configure the given serviceId with a callback.
   * @param serviceID
   * @param configObj
   */
  public static configure<T, L, Q extends Service<T, L>>(serviceID: string, configObj: ServiceCallback<Q>): ServiceDefinition;

  /**
   * Constructs a new instance of the given service.
   * @param serviceID
   */
  public static get<REQ, RESP, SERVICE extends Service<REQ, RESP>>(serviceID: string): SERVICE;

  /**
   * Gets a Service Definition.
   * @param serviceID
   */
  public static getDefinition(serviceID: string): ServiceDefinition;

  /**
   * Returns the status of whether the given service has been configured with a callback
   * @param serviceID
   */
  public static isConfigured(serviceID: string): boolean;
  protected constructor();
}

export = ServiceRegistry;
