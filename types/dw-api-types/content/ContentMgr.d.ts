import Content = require('./Content');
import Folder = require('./Folder');
import Library = require('./Library');

declare class ContentMgr {
  /**
   * Returns the content with the corresponding identifier within the current site's site library.
   * @param id - the ID of the content asset to find.
   */
  public static getContent(id: String): Content | null;
  /**
   * Returns the folder identified by the specified id within the current site's site library.
   * @param id - the ID of the folder to find.
   */
  public static getFolder(id: String): Folder | null;
  /**
   * The content library of the current site.
   */
  public static getSiteLibrary(): Library | null;

  /**
   * The content library of the current site.
   */
  public readonly siteLibrary: Library | null;
  private constructor();
}

export = ContentMgr;
