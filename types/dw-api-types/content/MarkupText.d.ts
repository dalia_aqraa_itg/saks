declare class MarkupText {
  // The content with all links rewritten for storefront use.
  public markup: string;
  // The original content source, without any links re-written.
  public source: string;

  public getMarkup(): string;
  public getSource(): string;
  public toString(): string;
}

export = MarkupText;
