import SearchModel = require('../catalog/SearchModel');
import Iterator = require('../util/Iterator');
import PageMetaTag = require('../web/PageMetaTag');
import URL = require('../web/URL');
import Content = require('./Content');
import ContentSearchRefinements = require('./ContentSearchRefinements');
import Folder = require('./Folder');

/**
 * The class is the central interface to a content search result and a content search refinement. It also provides utility methods to generate a search URL.
 */
declare class ContentSearchModel extends SearchModel {
  /**
   * URL Parameter for the content ID
   */
  public static readonly CONTENTID_PARAMETER: string;

  /**
   * URL Parameter for the folder ID
   */
  public static readonly FOLDERID_PARAMETER: string;

  /**
   * Returns an URL that you can use to execute a query for a specific Content.
   * @param action
   * @param cid
   */
  public static urlForContent(action: string, cid: string): URL;

  /**
   * Returns an URL that you can use to execute a query for a specific Content.
   * @param url
   * @param cid
   */
  public static urlForContent(url: URL, cid: string): URL;

  /**
   * Returns an URL that you can use to execute a query for a specific Folder.
   * @param action
   * @param fid
   */
  public static urlForFolder(action: string, fid: string): URL;

  /**
   * Returns an URL that you can use to execute a query for a specific Folder.
   * @param url
   * @param fid
   */
  public static urlForFolder(url: URL, fid: string): URL;

  /**
   * Returns an URL that you can use to execute a query for a specific attribute name-value pair.
   * @param action
   * @param name
   * @param value
   */
  public static urlForRefine(action: string, name: string, value: string): URL;

  /**
   * Returns an URL that you can use to execute a query for a specific attribute name-value pair.
   * @param url
   * @param name
   * @param value
   */
  public static urlForRefine(url: URL, name: string, value: string): URL;

  /**
   * An Iterator containing all Content Assets that are the result of the search.
   */
  public readonly content: Iterator<Content>;

  /**
   * The content ID against which the search results apply.
   */
  public contentID: string;

  /**
   * The deepest common folder of all content assets in the search result.
   */
  public readonly deepestCommonFolder: Folder;

  /**
   * The folder against which the search results apply.
   */
  public readonly folder: Folder;

  /**
   * The folder ID against which the search results apply.
   */
  public folderID: string;

  /**
   * The method returns true, if this is a pure search for a folder. The method checks, that a folder ID is specified and no search phrase is specified.
   */
  public readonly folderSearch: boolean;

  /**
     * Reserved for beta users.
    Returns all page meta tags, defined for this instance for which content can be generated.
    The meta tag content is generated based on the content listing page meta tag context and rules. The rules are obtained from the current folder context or inherited from the parent folder, up to the root folder.
    */
  public readonly pageMetaTags: PageMetaTag[];

  /**
   * Get the flag that determines if the folder search will be recursive.
   */
  public recursiveFolderSearch: boolean;

  /**
   * The method returns true, if the search is refined by a folder. The method checks, that a folder ID is specified.
   */
  public readonly refinedByFolder: boolean;

  /**
   * Identifies if this is a folder search and is refined with further criteria, like a name refinement or an attribute refinement.
   */
  public readonly refinedFolderSearch: boolean;

  /**
   * The set of search refinements used in this search.
   */
  public readonly refinements: ContentSearchRefinements;

  constructor();

  constructor();

  /**
   * Returns an Iterator containing all Content Assets that are the result of the search.
   */
  public getContent(): Iterator<Content>;

  /**
   * Returns the content ID against which the search results apply.
   */
  public getContentID(): string;

  /**
   * Returns the deepest common folder of all content assets in the search result.
   */
  public getDeepestCommonFolder(): Folder;

  /**
   * Returns the folder against which the search results apply.
   */
  public getFolder(): Folder;

  /**
   * Returns the folder ID against which the search results apply.
   */
  public getFolderID(): string;

  /**
   * Reserved for beta users.
   * @param id
   */
  public getPageMetaTag(id: string): PageMetaTag | null;

  /**
   * Reserved for beta users.
   */
  public getPageMetaTags(): PageMetaTag[];

  /**
   * Returns the set of search refinements used in this search.
   */
  public getRefinements(): ContentSearchRefinements;

  /**
   * The method returns true, if this is a pure search for a folder.
   */
  public isFolderSearch(): boolean;

  /**
   * Get the flag that determines if the folder search will be recursive.
   */
  public isRecursiveFolderSearch(): boolean;

  /**
   * The method returns true, if the search is refined by a folder.
   */
  public isRefinedByFolder(): boolean;

  /**
   * Identifies if this is a folder search and is refined with further criteria, like a name refinement or an attribute refinement.
   */
  public isRefinedFolderSearch(): boolean;

  /**
   * Execute the search.
   */
  public search(): void;

  /**
   * Sets the contentID used in this search.
   * @param contentID
   */
  public setContentID(contentID: string | null): void;

  /**
   * Sets the folderID used in this search.
   * @param folderID
   */
  public setFolderID(folderID: string): void;

  /**
   * Set a flag to indicate if the search in folder should be recursive.
   * @param recurse
   */
  public setRecursiveFolderSearch(recurse: boolean): void;

  /**
   * Returns an URL that you can use to re-execute the query using the specified pipeline action and folder refinement.
   * @param action
   * @param refineFolderID
   */
  public urlRefineFolder(action: string, refineFolderID: string): URL;

  /**
   * Returns an URL that you can use to re-execute the query using the specified URL and folder refinement.
   * @param url
   * @param refineFolderID
   */
  public urlRefineFolder(url: URL, refineFolderID: string): URL;

  /**
   * Returns an URL that you can use to re-execute the query with no folder refinement.
   * @param action
   */
  public urlRelaxFolder(action: string): URL;

  /**
   * Returns an URL that you can use to re-execute the query with no folder refinement.
   * @param url
   */
  public urlRelaxFolder(url: URL): URL;
}

export = ContentSearchModel;
