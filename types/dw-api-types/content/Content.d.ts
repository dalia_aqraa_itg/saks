import MarkupText = require('../content/MarkupText');
import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Collection = require('../util/Collection');
import PageMetaTag = require('../web/PageMetaTag');
import Folder = require('./Folder');

declare global {
  namespace ICustomAttributes {
    interface Content extends CustomAttributes {}
  }
}

declare class Content extends ExtensibleObject<ICustomAttributes.Content> {
  public readonly classificationFolder: Folder | null;
  public readonly description: string | null;
  public readonly folders: Collection<Folder>;
  public readonly ID: string;
  public readonly name: string;
  public readonly online: boolean;
  public readonly onlineFlag: boolean;
  public readonly pageDescription: string | null;
  public readonly pageKeywords: string | null;
  public readonly pageMetaTags: PageMetaTag[];
  public readonly pageTitle: string | null;
  public readonly pageURL: string;
  public readonly searchable: boolean;
  public readonly searchableFlag: boolean;
  public readonly siteMapChangeFrequency: string;
  public readonly siteMapPriority: number;
  public readonly template: string;
  private constructor();

  public getClassificationFolder(): Folder | null;
  public getDescription(): string;
  public getFolders(): Collection<Folder>;
  public getID(): string;
  public getName(): string;
  public getOnlineFlag(): boolean;
  public getPageDescription(): string | null;
  public getPageKeywords(): string | null;
  public getPageMetaTag(id: string): PageMetaTag | null;
  public getPageMetaTags(): PageMetaTag[];
  public getPageTitle(): string | null;
  public getPageURL(): string;
  public getSearchableFlag(): boolean;
  public getSiteMapChangeFrequency(): string;
  public getSiteMapPriority(): number;
  public getTemplate(): string | null;
  public isOnline(): boolean;
  public isSearchable(): boolean;
}

export = Content;
