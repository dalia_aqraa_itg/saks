import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Collection = require('../util/Collection');
import Content = require('./Content');

declare global {
  namespace ICustomAttributes {
    interface Folder extends CustomAttributes {}
  }
}

declare class Folder extends ExtensibleObject<ICustomAttributes.Folder> {
  public readonly content: Collection<Content>;
  public readonly description: string | null;
  public readonly displayName: string | null;
  public readonly ID: string;
  public readonly online: boolean;
  public readonly onlineContent: Collection<Content>;
  public readonly onlineSubFolders: Collection<Folder>;
  public readonly pageDescription: string | null;
  public readonly pageKeywords: string | null;
  public readonly pageTitle: string | null;
  public readonly pageURL: string | null;
  public readonly parent: Folder | null;
  public readonly root: boolean;
  public readonly subFolders: Collection<Folder>;
  public readonly template: string | null;

  private constructor();

  /**
   * Returns the content objects for this folder, sorted by position.
   */
  public getContent(): Collection<Content>;

  /**
   * Returns the description for the folder as known in the current locale or null if it cannot be found.
   */
  public getDescription(): string | null;

  /**
   * Returns the display name for the folder as known in the current locale or null if it cannot be found.
   */
  public getDisplayName(): string | null;

  /**
   * Returns the ID of the folder.
   */
  public getID(): string;

  /**
   * Returns the online content objects for this folder, sorted by position.
   */
  public getOnlineContent(): Collection<Content>;

  /**
   * Returns the online subfolders of this folder, sorted by position.
   */
  public getOnlineSubFolders(): Collection<Folder>;

  /**
   * Returns the page description for this folder using the value in the current locale, or returns null if no value was found.
   */
  public getPageDescription(): string | null;

  /**
   * Returns the page keywords for this folder using the value in the current locale, or returns null if no value was found.
   */
  public getPageKeywords(): string | null;

  /**
   * Returns the page title for this folder using the value in the current locale, or returns null if no value was found.
   */
  public getPageTitle(): string | null;

  /**
   * Returns the page URL for this folder using the value in the current locale, or returns null if no value was found.
   */
  public getPageURL(): string | null;

  /**
   * Returns the parent folder of this folder.
   */
  public getParent(): Folder | null;

  /**
   * Returns the subfolders of this folder, sorted by position.
   */
  public getSubFolders(): Collection<Folder>;

  /**
   * Returns the name of the template used to render the folder in the store front.
   */
  public getTemplate(): string | null;

  /**
   * Indicates if the folder is set online or offline.
   */
  public isOnline(): boolean;

  /**
   * Indicates if this is the root folder.
   */
  public isRoot(): boolean;
}

export = Folder;
