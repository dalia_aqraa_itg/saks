import URL = require('../web/URL');

declare type scaleModes = 'cut' | 'fit';

declare type formats = 'tif' | 'tiff' | 'jpg' | 'jpeg' | 'png' | 'gif';

interface ITransform {
  scaleWidth?: number;
  scaleHeight?: number;
  scaleMode?: scaleModes;

  imageX?: number;
  imageY?: number;
  imageURI?: string;

  cropX?: number;
  cropY?: number;
  cropWidth?: number;
  cropHeight?: number;

  format?: formats;
}

declare class MediaFile {
  public absURL: URL;
  public alt: string;
  public httpsURL: URL;
  public httpURL: URL;
  public title: string;
  public URL: URL;
  public viewType: string;

  public getAbsImageURL(transform: ITransform): URL;
  public getAbsURL(): URL;
  public getAlt(): string;
  public getHttpImageURL(transform: ITransform): URL;
  public getHttpsImageURL(transform: ITransform): URL;
  public getHttpsURL(): URL;
  public getHttpURL(): URL;
  public getImageURL(transform: ITransform): URL;
  public getTitle(): string;
  public getURL(): URL;
  public getViewType(): string;
}

export = MediaFile;
