import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Folder = require('./Folder');

declare global {
  namespace ICustomAttributes {
    interface Library extends CustomAttributes {}
  }
}

declare class Library extends ExtensibleObject<ICustomAttributes.Library> {
  public readonly displayName: string;
  public readonly ID: string;
  public readonly root: Folder | null;

  public getDisplayName(): string;
  public getID(): string;
  public getRoot(): Folder | null;
}

export = Library;
