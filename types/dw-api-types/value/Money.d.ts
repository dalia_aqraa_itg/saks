import Decimal = require('../util/Decimal');
import Quantity = require('./Quantity');

/**
 * Represents money in Commerce Cloud Digital.
 */
declare class Money {
  /**
   * Represents that there is no money available.
   */
  public static NOT_AVAILABLE: Money;

  /**
   * Prorates the specified values using the specified discount.
   * @param dist
   * @param values
   */
  public static prorate(dist: Money, ...values: Money[]): Money;

  /**
   * Identifies if the instance contains settings for value and currency.
   */
  public readonly available: boolean;

  /**
   * The money as Decimal, null is returned when the money is not available.
   */
  public readonly decimalValue: Decimal | null;

  /**
   * The value of the money instance.
   */
  public readonly value: number;

  /**
   * Return the value of the money instance or null if the Money instance is NOT_AVAILABLE.
   */
  public readonly valueOrNull: number | null;

  /**
   * The ISO 4217 currency mnemonic (such as 'USD', 'EUR') of the currency the money value relates to. Note a money instance may also describe a price that is 'not available'. In this case the value of this attribute is N/A.
   */
  public readonly currencyCode: string;

  /**
   * Constructs a new money instance with the specified amount for the specified currency.
   * @param value
   * @param currencyCode
   */
  constructor(value: number, currencyCode: string);

  /**
   * Returns a Money instance by adding the specified Money object to the current object.
   * @param value
   */
  public add(value: Money): Money;

  /**
   * Adds a certain percentage to the money object.
   * @param percent
   */
  public addPercent(percent: number): Money;

  /**
   * Adds a rate (e.g. 0.05) to the money object. This is typically for example to add a tax rate.
   * @param value
   */
  public addRate(value: number): Money;

  /**
   * Compares two Money values. An exception is thrown if the two Money values are of different currency. If one of the Money values represents the N/A value it is treated as 0.0.
   * @param other
   */
  public compareTo(other: Money): number;

  /**
   * Divide Money object by specified divisor. If this Money is N/A the result is also N/A.
   * @param divisor
   */
  public divide(divisor: number): Money;

  /**
   * Compares two money values whether they are equivalent.
   * @param other
   */
  public equals(other: Money): boolean;

  /**
   * Returns the ISO 4217 currency mnemonic (such as 'USD', 'EUR') of the currency the money value relates to. Note a money instance may also describe a price that is 'not available'. In this case the value of this attribute is N/A.
   */
  public getCurrencyCode(): string;

  /**
   * Returns the money as Decimal, null is returned when the money is not available.
   */
  public getDecimalValue(): Decimal | null;

  /**
   * Returns the value of the money instance.
   */
  public getValue(): number;

  /**
   * Return the value of the money instance or null if the Money instance is NOT_AVAILABLE.
   */
  public getValueOrNull(): number | null;

  /**
   * Calculates the hash code for a money;
   */
  public hashCode(): number;

  /**
   * Identifies if the instance contains settings for value and currency.
   */
  public isAvailable(): boolean;

  /**
   * Identifies if two Money value have the same currency.
   * @param value
   */
  public isOfSameCurrency(value: Money): boolean;

  /**
   * Multiply Money object by specified factor. If this Money is N/A the result is also N/A.
   * @param factor
   */
  public multiply(quantity: Quantity | number): Money;

  /**
   * Method returns a new instance of Money with the same currency but different value. An N/A instance is returned if value is null.
   * @param value
   */
  public newMoney(value: Decimal | null): Money;

  /**
   * Convenience method. Calculates and returns the percentage off this price represents in relation to the passed base price. The result is generally equal to 100.0 - this.percentOf(value). For example, if this value is $30 and the passed value is $50, then the return value will be 40.0, representing a 40% discount.
   * @param value
   */
  public percentLessThan(value: Money): number | null;

  /**
   * Convenience method. Calculates and returns the percentage of the passed value this price represents. For example, if this value is $30 and the passed value is $50, then the return value will be 60.0 (i.e. 60%).
   * @param value
   */
  public percentOf(value: Money): number | null;

  /**
   * Returns a new Money instance by substracting the specified Money object from the current object. Only objects representing the same currency can be subtracted. If one of the Money values is N/A, the result is N/A.
   * @param value
   */
  public subtract(value: Money): Money;

  /**
   * Subtracts a certain percentage from the money object. The percent value is given as true percent value, so for example 10 represent 10%. If this Money is N/A the result is also N/A.
   * @param percent
   */
  public subtractPercent(percent: number): Money;

  /**
   * Subtracts a rate (e.g. 0.05) from the money object. This is typically for example to subtract a tax rates.
   * @param value
   */
  public subtractRate(value: number): Money;

  /**
   * Returns a string representation of Money according to the regional settings configured for current request locale, for example '$59.00' or 'USD 59.00'.
   */
  public toFormattedString(): string;

  /**
   * Returns a string representation for the numeric value of this money. The number is formatted with the decimal symbols of the platforms default locale.
   */
  public toNumberString(): string;

  /**
   * Returns a string representation of this Money object.
   */
  public toString(): string;
}

export = Money;
