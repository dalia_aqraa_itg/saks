declare class EnumValue<T> {
  /**
   * The display value of the enumeration value. If no display value is configured the method return the string representation of the value.
   */
  public readonly displayValue: string;

  /**
   * The value of the enumeration value. This is either an integer value or a string.
   */
  public readonly value: T;

  /**
   * Returns the display value of the enumeration value
   */
  public getDisplayValue(): string;

  /**
   * Returns the value of the enumeration value.
   */
  public getValue(): T;
}

export = EnumValue;
