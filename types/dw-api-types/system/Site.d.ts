import CustomAttributes = require('../object/CustomAttributes');
import Calendar = require('../util/Calendar');
import List = require('../util/List');
import SitePreferences = require('./SitePreferences');
/**
 * This class represents a site in Commerce Cloud Digital and provides access to several site-level configuration values which are managed from within the Business Manager. It is only possible to get a reference to the current site as determined by the current request. The static method getCurrent() returns a reference to the current site.
 */
declare class Site {
  /**
   * Constant that represents the Site under maintenance/offline
   */
  public static readonly SITE_STATUS_MAINTENANCE: number;

  /**
   * Constant that represents the Site is Online
   */
  public static readonly SITE_STATUS_ONLINE: number;

  /**
   * Constant that represents the Site is in preview mode or online/password (protected)
   */
  public static readonly SITE_STATUS_PROTECTED: number;

  /**
   * Returns all sites.
   */
  public static getAllSites(): List<Site>;

  /**
   * Returns a new Calendar object in the time zone of the current site.
   */
  public static getCalendar(): Calendar;

  /**
   * Returns the current site.
   */
  public static getCurrent(): Site;

  /**
   * The current site.
   */
  public readonly current: Site; // (Read Only)

  /**
   * The default currency code for the current site.
   */
  public readonly defaultCurrency: string; // (Read Only)

  /**
   * Return default locale for the site.
   */
  public readonly defaultLocale: string; // (Read Only)

  /**
   * The configured HTTP host name. If no host name is configured the method returns the instance hostname.
   */
  public readonly httpHostName: string;

  /**
   * The configured HTTPS host name. If no host name is configured the method returns the HTTP host name or the instance hostname, if that is not configured as well.
   */
  public readonly httpsHostName: string;

  /**
   * The ID of the site.
   */
  public readonly ID: string;

  /**
   * A descriptive name for the site.
   */
  public readonly name: string;

  /**
   * Whether oms is active in the current site. This depends on a general property which states whether oms is active for the server, and a site-dependent preference whether oms is available for the current site.
   */
  public readonly OMSEnabled: boolean;

  /**
   * This method returns a container of all site preferences of this site.
   */
  public readonly preferences: SitePreferences;

  /**
   * The status of this site.
   *
   * Possible values are SITE_STATUS_ONLINE, SITE_STATUS_MAINTENANCE, SITE_STATUS_PROTECTED
   */
  public readonly status: number;

  /**
   * The code for the time zone in which the storefront is running.
   */
  public readonly timezone: string;

  /**
   * Returns time zone offset in which the storefront is running.
   */
  public readonly timezoneOffset: number;

  /**
   * The allowed currencies of the current site as a collection of currency codes.
   */
  public readonly allowedCurrencies: List<string>;

  /**
   * The allowed locales of the current site as a collection of locale ID's.
   */
  public readonly allowedLocales: List<string>;

  /**
   * All sites.
   */
  public readonly allSites: List<Site>;

  /**
   * A new Calendar object in the time zone of the current site.
   */
  public readonly calendar: Calendar;

  private constructor();

  /**
   * Returns the allowed currencies of the current site as a collection of currency codes.
   */
  public getAllowedCurrencies(): List<string>;

  /**
   * Returns the allowed locales of the current site as a collection of locale ID's.
   */
  public getAllowedLocales(): List<string>;

  /**
   * Returns a custom preference value.
   * @param name
   */
  public getCustomPreferenceValue<K extends keyof ICustomAttributes.SitePreferences>(name: K): ICustomAttributes.SitePreferences[K] | null;

  /**
   * Returns the default currency code for the current site.
   */
  public getDefaultCurrency(): string;

  /**
   * Return default locale for the site.
   */
  public getDefaultLocale(): string;

  /**
   * Returns the configured HTTP host name.
   */
  public getHttpHostName(): string;

  /**
   * Returns the configured HTTPS host name.
   */
  public getHttpsHostName(): string;

  /**
   * Returns the ID of the site.
   */
  public getID(): string;

  /**
   * Returns a descriptive name for the site.
   */
  public getName(): string;

  /**
   * This method returns a container of all site preferences of this site.
   */
  public getPreferences(): SitePreferences;

  /**
   * Returns the status of this site.
   */
  public getStatus(): number;

  /**
   * Returns the code for the time zone in which the storefront is running.
   */
  public getTimezone(): string;

  /**
   * Returns time zone offset in which the storefront is running.
   */
  public getTimezoneOffset(): Number;

  /**
   * Whether oms is active in the current site.
   */
  public isOMSEnabled(): boolean;

  /**
   * The method sets a value for a custom preference.
   * @param name
   * @param value
   */
  public setCustomPreferenceValue(name: string, value: Object): void;
}

export = Site;
