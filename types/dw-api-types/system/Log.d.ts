import LogNDC = require('./LogNDC');

/**
 * A log4j like logger instance. To obtain such an instance, use the Logger.getRootLogger() or Logger.getLogger(string) or Logger.getLogger(string, string) methods.
 */
declare class Log {
  /**
   * Returns the Nested Diagnostic Context for this script call.
   */
  public static getNDC(): LogNDC;
  /**
   * This method returns true if debug logging is enabled for this logging instance.
   */
  public readonly debugEnabled: boolean;

  /**
   * This method returns true if error logging is enabled for this logging instance.
   */
  public readonly errorEnabled: boolean;

  /**
   * This method returns true if information logging is enabled for this logging instance.
   */
  public readonly infoEnabled: boolean;

  /**
   * The Nested Diagnostic Context for this script call.
   */
  public readonly NDC: LogNDC;

  /**
   * This method returns true if warning logging is enabled for this logging instance.
   */
  public readonly warnEnabled: boolean;

  private constructor();

  /**
   * The method reports an debug level message.
   * @param msg
   * @param args
   * */
  public debug(msg: string, ...args: any[]): void;

  /**
   * The method reports an error level message.
   * @param msg
   * @param args
   * */
  public error(msg: string, ...args: any[]): void;

  /**
   * The method reports an warning level message.
   * @param msg
   * @param args
   */
  public fatal(msg: string, ...args: any[]): void;

  /**
   * The method reports an information level message.
   * @param msg
   * @param args
   * */
  public info(msg: string, ...args: any[]): void;

  /**
   * This method returns true if debug logging is enabled for this logging instance.
   */
  public isDebugEnabled(): boolean;

  /**
   * This method returns true if error logging is enabled for this logging instance.
   */
  public isErrorEnabled(): boolean;

  /**
   * This method returns true if information logging is enabled for this logging instance.
   */
  public isInfoEnabled(): boolean;

  /**
   * This method returns true if warning logging is enabled for this logging instance.
   */
  public isWarnEnabled(): boolean;

  /**
   * The method reports an warning level message.
   * @param msg
   * @param args
   * */
  public warn(msg: string, ...args: any[]): void;
}

export = Log;
