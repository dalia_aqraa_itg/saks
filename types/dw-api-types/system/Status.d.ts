import List = require('../util/List');
import Map = require('../util/Map');
import StatusItem = require('./StatusItem');

/**
 * A Status is used for communicating an API status code back to a client. A status consists of multiple StatusItem. Most often a Status contains only one StatusItem. For convenience, a message with parameters is formatted using standard formatting patterns. If you want to display locale-specific messages in your application, you should use the Status.getCode() as key for a resource bundle.
 */
declare class Status {
  /**
   * status value to indicate an ERROR status
   */
  public static ERROR: number;
  /**
   * status value to indicate an OK status
   */
  public static OK: number;
  /**
   * The status code either of the first ERROR StatusItem or when there is no ERROR StatusITEM, the first StatusItem in the overall list. The status code is the unique identifier for the message and can be used by client programs to check for a specific status and to generate a localized message.
   */
  public readonly code: string;

  /**
   * The details either of the first ERROR StatusItem or when there is no ERROR StatusItem, the first StatusItem in the overall list.
   */
  public readonly details: Map<string, string>;

  /**
   * Checks if the status is an ERROR. The Status is an ERROR if one of the contained StatusItems is an ERROR.
   */
  public readonly error: boolean;

  /**
   * All status items.
   */
  public readonly items: List<StatusItem>;

  /**
   * The message either of the first ERROR StatusItem or when there is no ERROR StatusItem, the first StatusItem in the overall list. Note: Custom code and client programs must not use this message to identify a specific status. The getCode() must be used for that purpose. The actual message can change from release to release.
   */
  public readonly message: string;

  /**
   * The parameters either of the first ERROR StatusItem or when there is no ERROR StatusItem, the first StatusItem in the overall list.
   */
  public readonly parameters: List<string>;

  /**
   * The overall status. If all StatusItems are OK, the method returns OK. If one StatusItem is an ERROR it returns ERROR.
   */
  public readonly status: number;

  /**
   * Creates a Status object with no StatusItems.
   */
  constructor();

  /**
   * Creates a Status with a single StatusItem.
   * @param status
   */
  constructor(status: number);

  /**
   * Creates a Status with a single StatusItem.
   * @param status
   * @param code
   */
  constructor(status: number, code: string);

  /**
   * Creates a Status with a single StatusItem.
   * @param status
   * @param code
   * @param message
   * @param parameters
   */
  constructor(status: number, code: string, message: string, ...parameters: string[]);

  /**
   * Add detail information for the given key of the first ERROR StatusItem or when there is no ERROR StatusItem, the first StatusItem in the overall list.
   * @param key
   * @param value
   */
  public addDetail(key: string, value: string): void;

  /**
   * Adds an additional status item to this status instance.
   * @param item
   */
  public addItem(item: StatusItem): void;

  /**
   * Returns the status code either of the first ERROR StatusItem or when there is no ERROR StatusITEM, the first StatusItem in the overall list.
   */
  public getCode(): string;

  /**
   * Returns the detail value for the given key of the first ERROR StatusItem or when there is no ERROR StatusItem, the first StatusItem in the overall list.
   * @param key
   */
  public getDetail(key: string): string;

  /**
   * Returns the details either of the first ERROR StatusItem or when there is no ERROR StatusItem, the first StatusItem in the overall list.
   */
  public getDetails(): Map<string, string>;

  /**
   * Returns all status items.
   */
  public getItems(): List<StatusItem>;

  /**
   * Returns the message either of the first ERROR StatusItem or when there is no ERROR StatusItem, the first StatusItem in the overall list.
   */
  public getMessage(): string;

  /**
   * Returns the parameters either of the first ERROR StatusItem or when there is no ERROR StatusItem, the first StatusItem in the overall list.
   */
  public getParameters(): List<string>;

  /**
   * Returns the overall status.
   */
  public getStatus(): number;

  /**
   * Checks if the status is an ERROR.
   */
  public isError(): boolean;
}

export = Status;
