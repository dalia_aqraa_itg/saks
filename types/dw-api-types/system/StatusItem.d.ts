import List = require('../util/List');
import Map = require('../util/Map');

/**
 * A StatusItem holds all the status information. Multi StatusItems are bundled together into a Status.
 */
declare class StatusItem {
  /**
   * The status code is the unique identifier for the message and can be used by client programs to check for a specific status and to generate a localized message.
   */
  public code: string;

  /**
   * The optional details for this StatusItem.
   */

  public readonly details: Map<string, string>;

  /**
   * Returns whether this Status Item represents and error.
   */
  public readonly error: boolean;

  /**
   * The default human readable message for this Status. Note: Custom code and client programs must not use this message to identify a specific status. The getCode() must be used for that purpose. The actual message can change from release to release.
   */
  public message: string;

  /**
   * The parameters to construct a custom message.
   */
  public parameters: List<string>;

  /**
   * The status.
   */
  public status: number;

  /**
   * Constructs a new OK StatusItem.
   */
  constructor();

  /**
   * Constructs a new StatusItem with the given status.
   * @param status
   */
  constructor(status: number);

  /**
   * Constructs a new StatusItem with the given status and code.
   * @param status
   * @param code
   */
  constructor(status: number, code: string);

  /**
   * Constructs a new StatusItem with the given values.
   * @param status
   * @param code
   * @param message
   * @param parameters
   */
  constructor(status: number, code: string, message: string, ...parameters: string[]);

  /**
   * Add an additional detail to this StatusItem.
   * @param key
   * @param value
   */
  public addDetail(key: string, value: Object): void;

  /**
   * The status code is the unique identifier for the message and can be used by client programs to check for a specific status and to generate a localized message.
   */
  public getCode(): string;

  /**
   * Returns the optional details for this StatusItem.
   */
  public getDetails(): Map<string, string>;

  /**
   * Returns the default human readable message for this Status.
   */
  public getMessage(): string;

  /**
   * Returns the parameters to construct a custom message.
   */
  public getParameters(): List<string>;

  /**
   * Returns the status.
   */
  public getStatus(): number;

  /**
   * Returns whether this Status Item represents and error.
   */
  public isError(): boolean;

  /**
   * Method to set the status code.
   * @param code
   */
  public setCode(code: string): void;

  /**
   * Sets the default human readable message for this Status.
   * @param message
   */
  public setMessage(message: string): void;

  /**
   * Sets the parameters for a custom message.
   * @param parameters
   */
  public setParameters(...parameters: string[]): void;

  /**
   * Set the status
   * @param status
   */
  public setStatus(status: number): void;
}

export = StatusItem;
