import Log = require('./Log');

/**
 * The Logger class provides logging utility methods.
 */
declare class Logger {
  /**
   * The method reports an debug level message.
   * @param msg
   * @param args
   * @param */

  public static debug(msg: string, ...args: any[]): void;

  /**
   * The method reports an error level message.
   * @param msg
   * @param args
   */
  public static error(msg: string, ...args: any[]): void;

  /**
   * Returns the logger object for the given category.
   * @param category
   */
  public static getLogger(category: string): Log;

  /**
   * Returns the logger object for the given file name prefix and category.
   * @param fileNamePrefix
   * @param category
   */
  public static getLogger(fileNamePrefix: string, category: string): Log;

  /**
   * Returns the root logger object.
   */
  public static getRootLogger(): Log;

  /**
   * The method reports an information level message.
   * @param msg
   * @param args
   * */

  public static info(msg: string, ...args: any[]): void;

  /**
   * This method returns true if debug logging is enabled.
   */
  public static isDebugEnabled(): boolean;

  /**
   * This method returns true if error logging is enabled.
   */
  public static isErrorEnabled(): boolean;

  /**
   * This method returns true if info logging is enabled.
   */
  public static isInfoEnabled(): boolean;

  /**
   * This method returns true if warning logging is enabled.
   */
  public static isWarnEnabled(): boolean;

  /**
   * The method reports an warning level message.
   * @param msg
   * @param args
   * @param */

  public static warn(msg: string, ...args: any[]): void;
  /**
   * This method returns true if debug logging is enabled.
   */
  public readonly debugEnabled: boolean;

  /**
   * This method returns true if error logging is enabled.
   */
  public readonly errorEnabled: boolean;

  /**
   * This method returns true if info logging is enabled.
   */
  public readonly infoEnabled: boolean;

  /**
   * The root logger object.
   */
  public readonly rootLogger: Log;

  /**
   * This method returns true if warning logging is enabled.
   */
  public readonly warnEnabled: boolean;

  private constructor();
}

export = Logger;
