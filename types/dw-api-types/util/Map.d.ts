import Collection = require('./Collection');
import Set = require('./Set');

declare class Map<K, V> {
  /**
   * Convenience variable, for an empty and immutable list.
   */
  public static EMPTY_MAP: Map<any, any>;
  /**
   * Identifies if this map is empty.
   */
  public readonly empty: boolean;

  /**
   * The size of the map. This is a bean attribute method and supports the access to the collections length similar to a ECMA array, such as 'products.length'.
   */
  public readonly length: number;

  protected constructor();

  /**
   * Clears the map of all objects.
   */
  public clear(): void;

  /**
   * dentifies if this map contains an element identfied by the specified key.
   * @param {K} key
   */
  public containsKey(key: K): boolean;

  /**
   * Identifies if this map contains an element identfied by the specified value.
   * @param {V} value
   */
  public containsValue(value: V): boolean;

  /**
   * Returns a set of the map's entries.
   */
  public entrySet(): Set<V>;

  /**
   * Returns the object associated with the key or null.
   * @param {K} key
   */
  public get(key: K): V | null;

  /**
   * Returns the size of the map.
   */
  public getLength(): number;

  /**
   * Identifies if this map is empty.
   */
  public isEmpty(): boolean;

  /**
   * Returns a set of the map's keys.
   */
  public keySet(): Set<K>;

  /**
   * Puts the specified value into the map using the specified key to identify it.
   * @param key
   * @param value
   */
  public put(key: K, value: V): V;

  /**
   * Copies all of the objects inside the specified map into this map.
   * @param other
   */
  public putAll(other: Map<K, V>): void;

  /**
   * Removes the object from the map that is identified by the key.
   * @param key
   */
  public remove(key: K): V;

  /**
   * Returns the size of the map.
   */
  public size(): number;

  /**
   * Returns a collection of the values contained in this map.
   */
  public values(): Collection<V>;
}

export = Map;
