/**
 * Represents a Calendar and is based on the java.util.Calendar class. Refer to the java.util.Calendar documentation for more information.

 **IMPORTANT NOTE** : Please use the stringUtils.formatCalendar(Calendar) functions to convert a Calendar object into a string.
 */
declare class Calendar {
  /**
   * Indicates whether the HOUR is before or after noon.
   */
  public static readonly AM_PM: number;

  /**
   * Value for the month of year field representing April.
   */
  public static readonly APRIL: number;

  /**
   * Value for the month of year field representing August.
   */
  public static readonly AUGUST: number;

  /**
   * Represents a date.
   */
  public static readonly DATE: number;

  /**
   * Represents a day of the month.
   */
  public static readonly DAY_OF_MONTH: number;

  /**
   * Represents a day of the week.
   */
  public static readonly DAY_OF_WEEK: number;

  /**
   * Represents a day of the week in a month.
   */
  public static readonly DAY_OF_WEEK_IN_MONTH: number;

  /**
   * Represents a day of the year.
   */
  public static readonly DAY_OF_YEAR: number;

  /**
   * Value for the month of year field representing December.
   */
  public static readonly DECEMBER: number;

  /**
   * Indicates the daylight savings offset in milliseconds.
   */
  public static readonly DST_OFFSET: number;

  /**
   * Indicates the era such as 'AD' or 'BC' in the Julian calendar.
   */
  public static readonly ERA: number;

  /**
   * Value for the month of year field representing February.
   */
  public static readonly FEBRUARY: number;

  /**
   * Value for the day of the week field representing Friday.
   */
  public static readonly FRIDAY: number;

  /**
   * Represents an hour.
   */
  public static readonly HOUR: number;

  /**
   * Represents an hour of the day.
   */
  public static readonly HOUR_OF_DAY: number;

  /**
   * The input date pattern, for instance MM/dd/yyyy
   */
  public static readonly INPUT_DATE_PATTERN: number;

  /**
   * The input date time pattern, for instance MM/dd/yyyy h:mm a
   */
  public static readonly INPUT_DATE_TIME_PATTERN: number;

  /**
   * The input time pattern, for instance h:mm a
   */
  public static readonly INPUT_TIME_PATTERN: number;

  /**
   * Value for the month of year field representing January.
   */
  public static readonly JANUARY: number;

  /**
   * Value for the month of year field representing July.
   */
  public static readonly JULY: number;

  /**
   * Value for the month of year field representing June.
   */
  public static readonly JUNE: number;

  /**
   * The long date pattern, for instance MMM/d/yyyy
   */
  public static readonly LONG_DATE_PATTERN: number;

  /**
   * Value for the month of year field representing March.
   */
  public static readonly MARCH: number;

  /**
   * Value for the month of year field representing May.
   */
  public static readonly MAY: number;

  /**
   * Represents a millisecond.
   */
  public static readonly MILLISECOND: number;

  /**
   * Represents a minute.
   */
  public static readonly MINUTE: number;

  /**
   * Value for the day of the week field representing Monday.
   */
  public static readonly MONDAY: number;

  /**
   * Represents a month where the first month of the year is 0.
   */
  public static readonly MONTH: number;

  /**
   * Value for the month of year field representing November.
   */
  public static readonly NOVEMBER: number;

  /**
   * Value for the month of year field representing October.
   */
  public static readonly OCTOBER: number;

  /**
   * Value for the day of the week field representing Saturday.
   */
  public static readonly SATURDAY: number;

  /**
   * Represents a second.
   */
  public static readonly SECOND: number;

  /**
   * Value for the month of year field representing September.
   */
  public static readonly SEPTEMBER: number;

  /**
   * The short date pattern, for instance M/d/yy
   */
  public static readonly SHORT_DATE_PATTERN: number;

  /**
   * Value for the day of the week field representing Sunday.
   */
  public static readonly SUNDAY: number;

  /**
   * Value for the day of the week field representing Thursday.
   */
  public static readonly THURSDAY: number;

  /**
   * The time pattern, for instance h:mm:ss a
   */
  public static readonly TIME_PATTERN: number;

  /**
   * Value for the day of the week field representing Tuesday.
   */
  public static readonly TUESDAY: number;

  /**
   * Value for the day of the week field representing Wednesday.
   */
  public static readonly WEDNESDAY: number;

  /**
   * Represents a week of the month.
   */
  public static readonly WEEK_OF_MONTH: number;

  /**
   * Represents a week in the year.
   */
  public static readonly WEEK_OF_YEAR: number;

  /**
   * Represents a year.
   */
  public static readonly YEAR: number;

  /**
   * Indicates the raw offset from GMT in milliseconds.
   */
  public static readonly ZONE_OFFSET: number;

  /**
   * The first day of the week base on locale context. For example, in the US the first day of the week is SUNDAY. However, in France the first day of the week is MONDAY.
   */
  public firstDayOfWeek: number;

  /**
     * The current time stamp of this calendar. This method is also used to convert a Calendar into a Date.

    ***WARNING**: Keep in mind that the returned Date object is always in the time zone GMT. That means that time zone information set at the calendar object will not be honored and gets lost.
    */
  public time: Date;

  /**
   * The current time zone of this calendar.
   */
  public timeZone: string;

  /**
   * Creates a new Calendar object that is set to the current time.
   */
  constructor();

  /**
   * Creates a new Calendar object for the given Date object.
   */
  constructor(date: Date);

  /**
   * Adds or subtracts the specified amount of time to the given calendar field, based on the calendar's rules.
   * @param field  the calendar field.
   * @param value  the amount of date or time to be added to the field
   */
  public add(field: number, value: number): void;

  /**
   * Indicates if this Calendar represents a time after the time represented by the specified Object.
   * @param obj
   */
  public after(obj: Calendar): boolean;

  /**
   * Indicates if this Calendar represents a time before the time represented by the specified Object.
   * @param obj
   */
  public before(obj: Calendar): boolean;

  /**
   * Sets all the calendar field values and the time value (millisecond offset from the Epoch) of this Calendar undefined.
   */
  public clear(): void;

  /**
   * Sets the given calendar field value and the time value (millisecond offset from the Epoch) of this Calendar undefined.
   * @param field
   */
  public clear(field: number): void;

  /**
   * Compares the time values (millisecond offsets from the Epoch) represented by two Calendar objects.
   * @param anotherCalendar
   */
  public compareTo(anotherCalendar: Calendar): number;

  /**
   * Compares two calendar values whether they are equivalent.
   * @param other
   */
  public equals(other: Calendar): boolean;

  /**
   * Returns the value of the given calendar field.
   * @param field
   */
  public get(field: number): number;

  /**
   * Returns the maximum value that the specified calendar field could have.
   * @param field
   */
  public getActualMaximum(field: number): number;

  /**
   * Returns the minimum value that the specified calendar field could have.
   * @param field
   */
  public getActualMinimum(field: number): number;

  /**
   * Returns the first day of the week base on locale context.
   */
  public getFirstDayOfWeek(): number;

  /**
   * Returns the maximum value for the given calendar field.
   * @param field
   */
  public getMaximum(field: number): number;

  /**
   * Returns the minimum value for the given calendar field.
   * @param field
   */
  public getMinimum(field: number): number;

  /**
   * Returns the current time stamp of this calendar.
   */
  public getTime(): Date;

  /**
   * Returns the current time zone of this calendar.
   */
  public getTimeZone(): string;

  /**
   * Calculates the hash code for a calendar;
   */
  public hashCode(): number;

  /**
   * Indicates if the specified year is a leap year.
   * @param year
   */
  public isLeapYear(year: number): boolean;

  /**
   * Checks, whether two calendar dates fall on the same day.
   * @param other
   */
  public isSameDay(other: Calendar): boolean;

  /**
   * Indicates if the field is set.
   * @param field
   */
  public isSet(field: number): boolean;

  /**
   * Parses the string according to the date and time format pattern and set the time at this calendar object.
   * @param timestring
   * @param format
   */
  public parseByFormat(timestring: string, format: string): void;

  /**
   * Parses the string according the date format pattern of the given locale.
   * @param timestring
   * @param locale
   * @param pattern
   */
  public parseByLocale(timestring: string, locale: string, pattern: number): void;

  /**
   * Rolls the specified field up or down one value.
   * @param field
   * @param up
   */
  public roll(field: number, up: boolean): void;

  /**
   * Rolls the specified field using the specified value.
   * @param field
   * @param amount
   */
  public roll(field: number, amount: number): void;

  /**
   * Sets the given calendar field to the given value.
   * @param field
   * @param value
   */
  public set(field: number, value: number): void;

  /**
   * Sets the values for the calendar fields YEAR, MONTH, and DAY_OF_MONTH.
   * @param year
   * @param month
   * @param date
   */
  public set(year: number, month: number, date: number): void;

  /**
   * Sets the values for the calendar fields YEAR, MONTH, DAY_OF_MONTH, HOUR_OF_DAY, and MINUTE.
   * @param year
   * @param month
   * @param date
   * @param hourOfDay
   * @param minute
   */
  public set(year: number, month: number, date: number, hourOfDay: number, minute: number): void;

  /**
   * Sets the values for the calendar fields YEAR, MONTH, DAY_OF_MONTH, HOUR_OF_DAY, MINUTE and SECOND.
   * @param year
   * @param month
   * @param date
   * @param hourOfDay
   * @param minute
   * @param second
   */
  public set(year: number, month: number, date: number, hourOfDay: number, minute: number, second: number): void;

  /**
   * Sets what the first day of the week is.
   * @param value
   */
  public setFirstDayOfWeek(value: number): void;

  /**
   * Sets the current time stamp of this calendar.
   *
   * WARNING: Keep in mind that the set Date object is always interpreted in the time zone GMT.
   * @param date
   */
  public setTime(date: Date): void;

  /**
   * Sets the current time zone of this calendar.
   * @param timeZone
   */
  public setTimeZone(timeZone: string): void;
}

export = Calendar;
