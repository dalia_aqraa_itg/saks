import Collection = require('./Collection');

declare class List<T> extends Collection<T> {
  /**
   * Convenience variable, for an empty and immutable list.
   */
  public static EMPTY_LIST: List<any>;

  protected constructor();

  /**
   * Adds the specified object into the list at the specified index.
   * @param index
   * @param value
   */
  public addAt(index: number, value: T): void;

  /**
   * Creates and returns a new List that is the result of concatenating this list with each of the specified values.
   * @param values
   */
  public concat(...values: T[]): List<T>;

  /**
   * Replaces all of the elements in the list with the given object.
   * @param obj
   */
  public fill(obj: T): void;

  /**
   * Returns the object at the specified index.
   * @param index
   */
  public get(index: number): T;

  /**
   * Returns the index of the first occurrence of the specified element in this list, or -1 if this list does not contain the element.
   * @param value
   */
  public indexOf(value: T): number;

  /**
   * Converts all elements of the list to a string by calling the toString() method and then concatenates them together, with a comma between elements.
   */
  public join(separator?: string): string;

  /**
   * Returns the index of the last occurrence of the specified element in this list, or -1 if this list does not contain the element.
   * @param value
   */
  public lastIndexOf(value: T): number;

  /**
   * Removes and returns the last element from the list.
   */
  public pop(): T;

  /**
   * Appends the specified values to the end of the list in order.
   * @param values
   */
  public push(...values: T[]): number;

  /**
   * Removes the object at the specified index.
   * @param index
   */
  public removeAt(index: number): T;

  /**
   * Replaces all occurrences of oldValue with newValue.
   * @param oldValue
   * @param newValue
   */
  public replaceAll(oldValue: T, newValue: T): boolean;

  /**
   * Reverses the order of the elements in the list.
   */
  public reverse(): void;

  /**
   * Rotates the elements in the list by the specified distance.
   * @param distance
   */
  public rotate(distance: number): void;

  /**
   * Replaces the object at the specified index in this list with the specified object.
   * @param index
   * @param value
   */
  public set(index: number, value: T): T;

  /**
   * Removes and returns the first element of the list.
   */
  public shift(): T;

  /**
   * Randomly permutes the elements in the list.
   */
  public shuffle(): void;

  /**
   * Returns the size of this list.
   */
  public size(): number;

  /**
   * Returns a slice, or sublist, of this list.
   * @param from
   */
  public slice(from: number, to?: number): List<T>;

  /**
   * Sorts the elements of the list based on their natural order.
   */
  public sort(comparator?: Object): void;

  /**
   * Returns a list containing the elements in this list identified by the specified arguments.
   * @param from
   * @param to
   */
  public subList(from: number, to: number): List<T>;

  /**
   * Swaps the elements at the specified positions in the list.
   * @param i
   * @param j
   */
  public swap(i: number, j: number): void;

  /**
   * Inserts values at the beginning of the list.
   * @param values
   */
  public unshift(...values: T[]): number;
}

export = List;
