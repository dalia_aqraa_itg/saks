declare class Geolocation {
  public available: Boolean;
  public city: string;
  public countryCode: string;
  public countryName: string;
  public latitude: number;
  public longitude: number;
  public metroCode: string;
  public postalCode: string;
  public regionCode: string;
  public regionName: string;

  public getCity(): string;
  public getCountryCode(): string;
  public getCountryName(): string;
  public getLatitude(): number;
  public getLongitude(): number;
  public getMetroCode(): string;
  public getPostalCode(): string;
  public getRegionCode(): string;
  public getRegionName(): string;
  public isAvailable(): Boolean;
}

export = Geolocation;
