/**
 * Represents a Locale supported by the system.
 */
declare class Locale {
  /**
   * Returns a Locale instance for the given localeId, or null if no suchLocale could be found.
   * @param localeId
   */
  public static getLocale(localeId: string): Locale;
  /**
   * The country/region code for this Locale, which will either be the empty string or an upercase ISO 3166 2-letter code.
   */
  public readonly country: string;

  /**
   * A name for the Locale's country that is appropriate for display to the user, or an empty string if no country has been specified for the Locale. The display country is returned in the language defined for this locale, and not in the language of the session locale.
   */
  public readonly displayCountry: string;

  /**
   * A name for the Locale's language that is appropriate for display to the user, or an empty string if no language has been specified for the Locale. The display language is returned in the language defined for this locale, and not in the language of the session locale.
   */
  public readonly displayLanguage: string;

  /**
   * A name for the Locale that is appropriate for display to the user, or an empty string if no display name has been specified for the Locale. The display name is returned in the language defined for this locale, and not in the language of the session locale.
   */
  public readonly displayName: string;

  /**
     * The String representation of the 'localeID'.
    The identifier of the Locale. Contains a combination of the language and the country key, concatenated by "_", e.g. "en_US". This attribute is the primary key of the class.

    */
  public readonly ID: string;

  /**
   * A three-letter abbreviation for this Locale's country, or an empty string if no country has been specified for the Locale .
   */
  public readonly ISO3Country: string;

  /**
   * A three-letter abbreviation for this Locale's language, or an empty string if no language has been specified for the Locale.
   */
  public readonly ISO3Language: string;

  /**
   * The language code for this Locale, which will either be the empty string or a lowercase ISO 639 code.
   */
  public readonly language: string;

  private constructor();

  /**
   * Returns the country/region code for this Locale, which will either be the empty string or an upercase ISO 3166 2-letter code.
   */
  public getCountry(): string;

  /**
   * Returns a name for the Locale's country that is appropriate for display to the user, or an empty string if no country has been specified for the Locale.
   */
  public getDisplayCountry(): string;

  /**
   * Returns a name for the Locale's language that is appropriate for display to the user, or an empty string if no language has been specified for the Locale.
   */
  public getDisplayLanguage(): string;

  /**
   * Returns a name for the Locale that is appropriate for display to the user, or an empty string if no display name has been specified for the Locale.
   */
  public getDisplayName(): string;

  /**
   * Returns the String representation of the 'localeID'.
   */
  public getID(): string;

  /**
   * Returns a three-letter abbreviation for this Locale's country, or an empty string if no country has been specified for the Locale .
   */
  public getISO3Country(): string;

  /**
   * Returns a three-letter abbreviation for this Locale's language, or an empty string if no language has been specified for the Locale.
   */
  public getISO3Language(): string;

  /**
   * Returns the language code for this Locale, which will either be the empty string or a lowercase ISO 639 code.
   */
  public getLanguage(): string;

  /**
   * Returns the String representation of the 'localeID'.
   */
  public toString(): string;
}

export = Locale;
