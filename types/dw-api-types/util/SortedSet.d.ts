import Collection = require('./Collection');
import Set = require('./Set');
declare class SortedSet<T> extends Set<T> {
  /**
   * Constructor to create a new SortedSet.
   */
  public constrtuctor();

  /**
   * Constructor to create a new SortedSet.
   * @param comparator
   */
  public constrtuctor(comparator: Object);

  /**
   * Constructor for a new SortedSet.
   * @param collection
   */
  public constrtuctor(collection: Collection<T>);

  /**
   * Returns a shallow copy of this set.
   */
  public clone(): SortedSet<T>;

  /**
   * Returns the first (lowest) element currently in this sorted set.
   */
  public first(): T;

  /**
   * Returns a view of the portion of this sorted set whose elements are strictly less than toElement.
   * @param key
   */
  public headSet(key: T): SortedSet<T>;

  /**
   * Returns the last (highest) element currently in this sorted set.
   */
  public last(): T;

  /**
   * Returns a view of the portion of this sorted set whose elements range from fromElement, inclusive, to toElement, exclusive.
   * @param from
   * @param to
   */
  public subSet(from: T, to: T): SortedSet<T>;

  /**
   * Returns a view of the portion of this sorted set whose elements are greater than or equal to fromElement.
   * @param key
   */
  public tailSet(key: T): SortedSet<T>;
}

export = SortedSet;
