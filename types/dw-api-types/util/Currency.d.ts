declare class Currency {
  public static getCurrency(currencyCode: string): Currency;
  public currencyCode: string;
  public defaultFractionDigits: number;
  public name: string;
  public symbol: string;
  public getCurrencyCode(): string;
  public getDefaultFractionDigits(): number;
  public getName(): string;
  public getSymbol(): string;
  public tostring(): string;
}

export = Currency;
