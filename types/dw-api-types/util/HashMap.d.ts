import Map = require('./Map');

declare class HashMap<K, V> extends Map<K, V> {
  constructor();
  public clone(): HashMap<K, V>;
}

export = HashMap;
