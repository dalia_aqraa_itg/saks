/**
 * This class represents a suggested phrase. Use getPhrase() method to get access to the phrase.
 */
declare class SuggestedPhrase {
  /**
   * This method returns a flag signaling whether this phrase is a exact match.
   */
  public readonly exactMatch: boolean;

  /**
   * This method returns the actual phrase as a string value.
   */
  public readonly phrase: string;

  private constructor();

  /**
   * This method returns the actual phrase as a string value.
   */
  public getPhrase(): string;

  /**
   * This method returns a flag signaling whether this phrase is a exact match.
   */
  public isExactMatch(): boolean;
}

export = SuggestedPhrase;
