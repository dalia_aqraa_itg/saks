import Collection = require('../util/Collection');
import ObjectAttributeGroup = require('./ObjectAttributeGroup');
import ObjectAttributeValueDefinition = require('./ObjectAttributeValueDefinition');
import ObjectTypeDefinition = require('./ObjectTypeDefinition');

declare class ObjectAttributeDefinition {
  public static readonly VALUE_TYPE_BOOLEAN: number;
  public static readonly VALUE_TYPE_DATE: number;
  public static readonly VALUE_TYPE_DATETIME: number;
  public static readonly VALUE_TYPE_EMAIL: number;
  public static readonly VALUE_TYPE_ENUM_OF_INT: number;
  public static readonly VALUE_TYPE_ENUM_OF_STRING: number;
  public static readonly VALUE_TYPE_HTML: number;
  public static readonly VALUE_TYPE_IMAGE: number;
  public static readonly VALUE_TYPE_INT: number;
  public static readonly VALUE_TYPE_MONEY: number;
  public static readonly VALUE_TYPE_NUMBER: number;
  public static readonly VALUE_TYPE_PASSWORD: number;
  public static readonly VALUE_TYPE_QUANTITY: number;
  public static readonly VALUE_TYPE_SET_OF_INT: number;
  public static readonly VALUE_TYPE_SET_OF_NUMBER: number;
  public static readonly VALUE_TYPE_SET_OF_STRING: number;
  public static readonly VALUE_TYPE_STRING: number;
  public static readonly VALUE_TYPE_TEXT: number;

  public valueTypeCode: number;
  public values: Collection<ObjectAttributeValueDefinition>;
  public unit: string;
  public system: boolean;
  public objectTypeDefinition: ObjectTypeDefinition;
  public multiValueType: boolean;
  public mandatory: boolean;
  public key: boolean;
  public ID: string;
  public displayName: string;
  public defaultValue: ObjectAttributeValueDefinition;
  public attributeGroups: Collection<ObjectAttributeGroup>;

  private constructor();

  /**
   * Returns all attribute groups the attribute is assigned to.
   */
  public getAttributeGroups(): Collection<ObjectAttributeGroup>;

  /**
   * Return the default value for the attribute or null if none is defined.
   */
  public getDefaultValue(): ObjectAttributeValueDefinition;

  /**
   * Returns the display name for the attribute, which can be used in the user interface.
   */
  public getDisplayName(): string;

  /**
   * Returns the ID of the attribute definition.
   */
  public getID(): string;

  /**
   * Returns the object type definition in which this attribute is defined.
   */
  public getObjectTypeDefinition(): ObjectTypeDefinition;

  /**
   * Returns the attribute's unit representation such as inches for length or pounds for weight.
   */
  public getUnit(): string;

  /**
   * Returns the list of attribute values.
   */
  public getValues(): Collection<ObjectAttributeValueDefinition>;

  /**
   * Returns a code for the data type stored in the attribute.
   */
  public getValueTypeCode(): number;

  /**
   * Identifies if the attribute represents the primary key of the object.
   */
  public isKey(): boolean;

  /**
   * Checks if this attribute is mandatory.
   */
  public isMandatory(): boolean;

  /**
   * Returns true if the attribute can have multiple values.
   */
  public isMultiValueType(): boolean;

  /**
   * Returns true if the attribute is of type 'Set of'.
   */
  public isSetValueType(): boolean;

  /**
   * Indicates if the attribute is a pre-defined system attribute or a custom attribute.
   */
  public isSystem(): boolean;

  /**
   * Returns a boolean flag indicating whether or not values of this attribute definition should be encoded using the encoding="off" flag in ISML templates.
   */
  public requiresEncoding(): boolean;
}

export = ObjectAttributeDefinition;
