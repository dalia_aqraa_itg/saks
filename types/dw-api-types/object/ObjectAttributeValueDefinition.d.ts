declare class ObjectAttributeValueDefinition {
  public displayValue: string;
  public value: Object;

  public getDisplayValue(): string;
  public getValue(): Object;
}

export = ObjectAttributeValueDefinition;
