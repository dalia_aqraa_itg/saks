import CustomAttributes = require('./CustomAttributes');
import ExtensibleObject = require('./ExtensibleObject');

/**
 * Represents a custom object and its corresponding attributes.
 */
declare class CustomObject<T extends CustomAttributes> extends ExtensibleObject<T> {
  /**
   * The type of the CustomObject.
   */
  public readonly type: string;
  private constructor();

  /**
   * Returns the type of the CustomObject.
   */
  public getType(): string;
}

export = CustomObject;
