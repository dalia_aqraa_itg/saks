import MarkupText = require('../content/MarkupText');
import EnumValue = require('../value/EnumValue');

declare class CustomAttributes {
  private constructor();
  [name: string]: boolean | number | string | Date | Array<EnumValue<string | number>> | string[] | number[] | MarkupText | null;
}

export = CustomAttributes;
