import Collection = require('../util/Collection');
import ObjectAttributeDefinition = require('./ObjectAttributeDefinition');
import ObjectTypeDefinition = require('./ObjectTypeDefinition');

declare class ObjectAttributeGroup {
  public attributeDefinitions: Collection<ObjectAttributeDefinition>;
  public description: string;
  public displayName: string;
  public ID: string;
  public objectTypeDefinition: ObjectTypeDefinition;
  public system: Boolean;

  public getAttributeDefinitions(): Collection<ObjectAttributeDefinition>;
  public getDescription(): string;
  public getDisplayName(): string;
  public getID(): string;
  public getObjectTypeDefinition(): ObjectTypeDefinition;
  public isSystem(): boolean;
}

export = ObjectAttributeGroup;
