import CustomAttributes = require('./CustomAttributes');
import ExtensibleObject = require('./ExtensibleObject');

declare global {
  namespace ICustomAttributes {
    interface ActiveData extends CustomAttributes {}
  }
}

declare class ActiveData extends ExtensibleObject<ICustomAttributes.ActiveData> {}

export = ActiveData;
