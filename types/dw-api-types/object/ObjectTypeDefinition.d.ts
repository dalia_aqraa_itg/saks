import Collection = require('../util/Collection');
import ObjectAttributeDefinition = require('./ObjectAttributeDefinition');
import ObjectAttributeGroup = require('./ObjectAttributeGroup');

declare class ObjectTypeDefinition {
  public attributeDefinitions: Collection<ObjectAttributeDefinition>;
  public displayName: string;
  public ID: string;
  public Syetem: boolean;

  public getAttributeDefinitions(): Collection<ObjectAttributeDefinition>;
  public getAttributeGroup(name: string): ObjectAttributeGroup | null;
  public getAttributeGroups(): Collection<ObjectAttributeGroup>;
  public getCustomAttributeDefinition(name: string): ObjectAttributeDefinition | null;
  public getDisplayName(): string;
  public getID(): string;
  public getSystemAttributeDefinition(name: string): ObjectAttributeDefinition | null;
  public isSystem(): boolean;
}

export = ObjectTypeDefinition;
