import CustomAttributes = require('./CustomAttributes');
import ObjectTypeDefinition = require('./ObjectTypeDefinition');
import PersistentObject = require('./PersistentObject');

declare class ExtensibleObject<T extends CustomAttributes> extends PersistentObject {
  public custom: T;
  public describe(): ObjectTypeDefinition;
  public getCustom(): T;
}

export = ExtensibleObject;
