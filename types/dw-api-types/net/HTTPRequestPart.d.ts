import File = require('../io/File');
/**
 * Simple class representing a single part in a multi-part HTTP POST request. A part always has a name, and either a string value or a value which comes from the contents of a File. For each of the two types, a character encoding may be specified, and for file parts, a content type may additionally be specified.
 */
declare class HTTPRequestPart {
  /**
   * The content type of this part.
   */
  public readonly contentType: string;

  /**
   * Get the charset to be used to encode the string.
   */
  public readonly encoding: string;

  /**
   * Get the file value of the part.
   */
  public readonly fileValue: File;

  /**
   * Get the name of the part.
   */
  public readonly name: string;

  /**
   * Get the string value of the part.
   */
  public readonly stringValue: string;

  /**
   * Construct a part representing a simple string name/value pair.
   * @param name
   * @param value
   */
  constructor(name: string, value: string);

  /**
   * Construct a part representing a simple string name/value pair.
   * @param name
   * @param value
   * @param encoding
   */
  constructor(name: string, value: string, encoding: string);

  /**
   * Construct a part representing a name/File pair.
   * @param name
   * @param file
   */
  constructor(name: string, file: File);

  /**
   * Construct a part representing a name/File pair.
   * @param name
   * @param file
   * @param contentType
   * @param encoding
   */
  constructor(name: string, file: File, contentType: string, encoding: string);

  /**
   * Returns the content type of this part.
   */
  public getContentType(): string;

  /**
   * Get the charset to be used to encode the string.
   */
  public getEncoding(): string;

  /**
   * Get the file value of the part.
   */
  public getFileValue(): File;

  /**
   * Get the name of the part.
   */
  public getName(): string;

  /**
   * Get the string value of the part.
   */
  public getStringValue(): string;
}

export = HTTPRequestPart;
