import Status = require('../system/Status');
import List = require('../util/List');
import MimeEncodedText = require('../value/MimeEncodedText');

/**
 * This class is used to send an email with either plain text or MimeEncodedText content. Recipient data (from, to, cc, bcc) and subject are specified using setter methods. When the send() method is invoked, the email is put into an internal queue and sent asynchronously.
 */
declare class Mail {
  /**
   * Gets the bcc address List<string>.
   */
  public bcc: List<string>;

  /**
   * Gets the cc address List<string>.
   */
  public cc: List<string>;

  /**
   * Gets the email address to use as the from address for the email.
   */
  public from: string;

  /**
   * Gets the subject of the email.
   */
  public subject: string;

  /**
   * Gets the to address List<string> where the email is sent.
   */
  public to: List<string>;
  constructor();

  /**
   * Adds an address to the bcc List<string>.
   * @param bcc
   */
  public addBcc(bcc: string): Mail;

  /**
   * Adds an address to the cc List<string>.
   * @param cc
   */
  public addCc(cc: string): Mail;

  /**
   * Adds an address to the to address List<string>.
   * @param to
   */
  public addTo(to: string): Mail;

  /**
   * Gets the bcc address List<string>.
   */
  public getBcc(): List<string>;

  /**
   * Gets the cc address List<string>.
   */
  public getCc(): List<string>;

  /**
   * Gets the email address to use as the from address for the email.
   */
  public getFrom(): string;

  /**
   * Gets the subject of the email.
   */
  public getSubject(): string;

  /**
   * Gets the to address List<string> where the email is sent.
   */
  public getTo(): List<string>;

  /**
   * prepares an email that is queued to the internal mail system for delivery.
   */
  public send(): Status;

  /**
   * Sets the bcc address List<string>.
   * @param bcc
   */
  public setBcc(bcc: List<string>): Mail;

  /**
   * Sets the cc address List<string> where the email is sent.
   * @param cc
   */
  public setCc(cc: List<string>): Mail;

  /**
   * Mandatory Sets the email content.
   * @param content
   */
  public setContent(content: string): Mail;

  /**
   * Mandatory Sets the email content, MIME type, and encoding.
   * @param content
   * @param mimeType
   * @param encoding
   */
  public setContent(content: string, mimeType: string, encoding: string): Mail;

  /**
   * Mandatory Uses MimeEncodedText to set the content, MIME type and encoding.
   * @param mimeEncodedText
   */
  public setContent(mimeEncodedText: MimeEncodedText): Mail;

  /**
   * Mandatory Sets the sender address for this email.
   * @param from
   */
  public setFrom(from: string): Mail;

  /**
   * Mandatory sets the subject for the email.
   * @param subject
   */
  public setSubject(subject: string): Mail;

  /**
   * Sets the to address List<string> where the email is sent.
   * @param to
   */
  public setTo(to: List<string>): Mail;
}

export = Mail;
