declare class FTPFileInfo {
  /**
   * Identifies if the file is a directory.
   */
  public directory: boolean;

  /**
   * The name of the file.
   */
  public name: string;

  /**
   * The size of the file.
   */
  public size: number;

  /**
   * The timestamp of the file.
   */
  public timestamp: Date;

  /**
   * Constructs the FTPFileInfo instance.
   * @param name
   * @param size
   * @param directory
   * @param timestamp
   */
  constructor(name: string, size: number, directory: boolean, timestamp: Date);

  /**
   * Identifies if the file is a directory.
   */
  public getDirectory(): boolean;

  /**
   * Returns the name of the file.
   */
  public getName(): string;

  /**
   * Returns the size of the file.
   */
  public getSize(): number;

  /**
   * Returns the timestamp of the file.
   */
  public getTimestamp(): Date;
}

export = FTPFileInfo;
