import File = require('../io/File');
import FTPFileInfo = require('./FTPFileInfo');

declare class FTPClient {
  /**
   * The maximum size for get() returning a File is forty times the default size for getting a file. The largest file allowed is 200MB.
   */
  public static readonly MAX_GET_FILE_SIZE: number;

  /**
   * The maximum size for get() returning a String is five times the default size for getting a String. The largest String allowed is 10MB.
   */
  public static readonly MAX_GET_STRING_SIZE: number;

  /**
   * Identifies if the FTP client is currently connected to the FTP server.
   */
  public readonly connected: boolean;

  /**
   * The reply code from the last FTP action.
   */
  public readonly replyCode: number;

  /**
   * The string message from the last FTP action.
   */
  public readonly replyMessage: string;

  /**
   * The timeout for this client, in milliseconds.
   */
  public timeout: number;

  constructor();

  /**
   * Changes the current directory on the remote server to the given path.
   * @param path
   */
  public cd(path: string): boolean;

  /**
   *     Connects and logs on to an FTP Server as "anonymous" and returns a boolean indicating success or failure.
   * @param host
   */
  public connect(host: string): boolean;

  /**
   * Connects and logs on to an FTP server and returns a boolean indicating success or failure.
   * @param host
   * @param user
   * @param password
   */
  public connect(host: string, user: string, password: string): boolean;

  /**
   * Connects and logs on to an FTP Server as "anonymous" and returns a boolean indicating success or failure.
   * @param host
   * @param port
   */
  public connect(host: string, port: number): boolean;

  /**
   * Connects and logs on to an FTP server and returns a boolean indicating success or failure.
   * @param host
   * @param port
   * @param user
   * @param password
   */
  public connect(host: string, port: number, user: string, password: string): boolean;

  /**
   * Deletes the remote file on the server identified by the path parameter.
   * @param path
   */
  public del(path: string): boolean;

  /**
   * The method first logs the current user out from the server and then disconnects from the server.
   */
  public disconnect(): void;

  /**
   * Reads the content of a remote file and returns it as a string using "ISO-8859-1" encoding to read it.
   * @param path
   */
  public get(path: string): string;

  /**
   * Reads the content of a remote file and returns it as string using the passed encoding.
   * @param path
   * @param encoding
   */
  public get(path: string, encoding: string): string;

  /**
   * Reads the content of a remote file and returns it as a string using "ISO-8859-1" encoding to read it.
   * @param path
   * @param maxGetSize
   */
  public get(path: string, maxGetSize: number): String;

  /**
   * Reads the content of a remote file and returns it as a string using the specified encoding.
   * @param path
   * @param encoding
   * @param maxGetSize
   */
  public get(path: string, encoding: string, maxGetSize: number): String;

  /**
   * Reads the content of a remote file and creates a local copy in the given file using the passed string encoding to read the file content and using the system standard encoding "UTF-8" to write the file.
   * @param path
   * @param encoding
   * @param file
   */
  public get(path: string, encoding: string, file: File): boolean;

  /**
   * Reads the content of a remote file and creates a local copy in the given file using the passed string encoding to read the file content and using the system standard encoding "UTF-8" to write the file.
   * @param path
   * @param encoding
   * @param file
   * @param maxGetSize
   */
  public get(path: string, encoding: string, file: File, maxGetSize: number): boolean;

  /**
   * Reads the content of a remote file and creates a local copy in the given file.
   * @param path
   * @param file
   */
  public getBinary(path: string, file: File): boolean;

  /**
   * Reads the content of a remote file and creates a local copy in the given file.
   * @param path
   * @param file
   * @param maxGetSize
   */
  public getBinary(path: string, file: File, maxGetSize: number): boolean;

  /**
   * Identifies if the FTP client is currently connected to the FTP server.
   */
  public getConnected(): boolean;

  /**
   * Returns the reply code from the last FTP action.
   */
  public getReplyCode(): number;

  /**
   * Returns the string message from the last FTP action.
   */
  public getReplyMessage(): string;

  /**
   * Returns the timeout for this client, in milliseconds.
   */
  public getTimeout(): number;

  /**
   * Returns a list of FTPFileInfo objects containing information about the files in the current directory.
   */
  public list(): FTPFileInfo[] | null;

  /**
   * Returns a list of FTPFileInfo objects containing information about the files in the remote directory defined by the given path.
   * @param path
   */
  public list(path: string): FTPFileInfo[] | null;

  /**
   * Creates a directory
   * @param path
   */
  public mkdir(path: string): boolean;

  /**
   * Puts the specified content to the specified full path using "ISO-8859-1" encoding.
   * @param path
   * @param content
   */
  public put(path: string, content: string): boolean;

  /**
   * Put the given content to a file on the given full path on the FTP server.
   * @param path
   * @param content
   * @param encoding
   */
  public put(path: string, content: string, encoding: string): boolean;

  /**
   * Put the content of the given file into a file on the remote FTP server with the given full path.
   * @param path
   * @param file
   */
  public putBinary(path: string, file: File): boolean;

  /**
   * Deletes the remote directory on the server identified by the path parameter.
   * @param path
   */
  public removeDirectory(path: string): boolean;

  /**
   * Renames an existing file.
   * @param from
   * @param to
   */
  public rename(from: string, to: string): boolean;

  /**
   * Sets the timeout for connections made with the FTP client to the given number of milliseconds.
   * @param timeoutMillis
   */
  public setTimeout(timeoutMillis: number): void;
}

export = FTPClient;
