declare class OutputStream {
  /**
   * Closes the output stream.
   */
  public close(): void;
}

export = OutputStream;
