import Writer = require('./Writer');

declare class PrintWriter extends Writer {
  /**
   * Prints the given string into the output stream.
   * @param str - the String object
   */
  public print(str: string): void;

  /**
   * Print the given string followed by a line break into the output stream.
   * @param str - the String object
   */
  public println(str: string): void;

  /**
   * Prints a line break into the output stream.
   */
  public println(): void;
}

export = PrintWriter;
