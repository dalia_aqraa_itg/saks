import List = require('../util/List');

declare class File {
  /**
   * Catalogs root directory.
   */
  public static CATALOGS: string;
  /**
   * Reserved for future use.
   */
  public static DYNAMIC: string;
  /**
   * Import/export root directory.
   */
  public static IMPEX: string;
  /**
   * Libraries root directory.
   */
  public static LIBRARIES: string;
  /**
   * The UNIX style '/' path separator, which must be used for files paths.
   */
  public static SEPARATOR: string;
  /**
   * Static content root directory.
   */
  public static STATIC: string;
  /**
   * Temp root directory.
   */
  public static TEMP: string;

  /**
   * Returns a File representing a directory for the specified root directory type.
   * @param rootDir - root directory type (see the constants defined in this class)
   * @param args - root directory specific arguments
   */
  public static getRootDirectory(rootDir: string, ...args: string[]): File;
  /**
   * Indicates that this file is a directory.
   */
  public readonly directory: Boolean;
  /**
   * Indicates if this file is a file.
   */
  public readonly file: Boolean;
  /**
   * Return the full file path denoted by this File.
   * This value will be the same regardless of which constructor was used to create this File.
   */
  public readonly fullPath: string;
  /**
   * The name of the file or directory denoted by this object.
   * This is just the last name in the pathname's name sequence.
   * If the pathname's name sequence is empty, then the empty string is returned.
   */
  public readonly name: string;
  /**
   * The root directory type, e.g. "IMPEX" represented by this File.
   */
  public readonly rootDirectoryType: string;

  /**
   * Creates a File from the given absolute file path in the file namespace.
   * @param absPath - the absolute file path throws IOException
   */
  constructor(absPath: string);

  /**
   * Creates a File given a root directory and a relative path.
   * @param rootDir - File object representing root directory
   * @param relPath - relative file path
   */
  constructor(rootDir: File, relPath: string);

  /**
   * Copy a file.
   * @param file - the File object to copy to
   */
  public copyTo(file: File): File;

  /**
   * Create file.
   */
  public createNewFile(): boolean;

  /**
   * Indicates if the file exists.
   */
  public exists(): boolean;

  /**
   * Return the file path of this File.
   */
  public getPath(): string;

  /**
   * Return the full file path denoted by this File.
   */
  public getFullPath(): string;

  /**
   * Returns the name of the file or directory denoted by this object.
   */
  public getName(): string;

  /**
   * Returns the root directory type, e.g.
   */
  public getRootDirectoryType(): string;

  /**
   * Assumes this instance is a gzip file.
   * @param root - a File indicating root. root must be a directory.
   */
  public gunzip(root: File): void;

  /**
   * GZip this instance into a new gzip file.
   * @param outputZipFile - the zip file created.
   */
  public gzip(outputZipFile: File): void;

  /**
   * Indicates that this file is a directory.
   */
  public isDirectory(): boolean;

  /**
   * Indicates if this file is a file.
   */
  public isFile(): boolean;

  /**
   * Return the time, in milliseconds, that this file was last modified.
   */
  public lastModified(): number;

  /**
   * Return the length of the file in bytes.
   */
  public length(): number;

  /**
   * Returns an array of strings naming the files and directories in the directory denoted by this object.
   */
  public list(): string[];

  /**
   * Returns an array of File objects in the directory denoted by this File.
   */
  public listFiles(): List<File>;

  /**
   * Returns an array of File objects denoting the files and directories in the directory denoted by this object that satisfy the specified filter.
   * @param filter - a Javascript function which accepts a File argument and returns true or false.
   */
  public listFiles(filter: (file: File) => boolean): List<File>;

  /**
   * Returns an MD5 hash of the content of the file of this instance.
   */
  public md5(): string;

  /**
   * Creates a directory.
   */
  public mkdir(): boolean;

  /**
   * Creates a directory, including, its parent directories, as needed.
   */
  public mkdirs(): boolean;

  /**
   * Deletes the file or directory denoted by this object.
   */
  public remove(): boolean;

  /**
   * Rename file.
   * @param file - the File object to rename to
   */
  public renameTo(file: File): boolean;

  /**
   * Assumes this instance is a zip file.
   * @param root - a File indicating root. root must be a directory.
   */
  public unzip(root: File): void;

  /**
   * Zip this instance into a new zip file.
   * @param outputZipFile - the zip file created.
   */
  public zip(outputZipFile: File): void;
}

export = File;
