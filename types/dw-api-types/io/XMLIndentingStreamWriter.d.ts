import Writer = require('./Writer');
import XMLStreamWriter = require('./XMLStreamWriter');

declare class XMLIndentingStreamWriter extends XMLStreamWriter {
  /**
   * The indent.
   */
  public indent: String;
  /**
   * The string that is used for a new line character.
   * The default is the normal new line character.
   */
  public newLine: String;
  /**
   * Constructs the writer for the specified writer.
   * @param writer - the writer to use.
   */
  constructor(writer: Writer);
  /**
   * Returns the indent.
   */
  public getIndent(): String;
  /**
   * Returns the string that is used for a new line character.
   * The default is the normal new line character.
   */
  public getNewLine(): String;
  /**
   * Specifies a string that will be used as identing characters.
   * The default are two space characters.
   * @param indent - The indent to set.
   */
  public setIndent(indent: String): void;
  /**
   * Sets the string that is used for a new line character.
   * @param newLine - The newLine to set.
   */
  public setNewLine(newLine: String): void;
}

export = XMLIndentingStreamWriter;
