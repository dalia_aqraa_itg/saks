import List = require('../util/List');
import Writer = require('./Writer');

declare class CSVStreamWriter {
  /**
   * Creates a new CSVStreamWriter.
   * The separator character and the quote character can be specified in the call.
   * @param writer - the writer to use.
   * @param separator - a string, which represents the separator character.
   * @param quote - a string, which represents the quote character.
   */
  constructor(writer: Writer, separator?: string, quote?: string);
  /**
   * Closes the stream.
   */
  public close(): void;
  /**
   * Write a single line to the CSV file.
   */
  public writeNext(line: string[]): void;
}

export = CSVStreamWriter;
