/**
 * Useful constants for working with XML streams.
 */
declare class XMLStreamConstants {
  /**
   * Represents an attribute in an element.
   */
  public static readonly ATTRIBUTE: number;

  /**
   * Represents a CDATA section in an element.
   */
  public static readonly CDATA: number;

  /**
   * Represents the character data in an XML document.
   */
  public static readonly CHARACTERS: number;

  /**
   * Represents a comment in an XML document.
   */
  public static readonly COMMENT: number;

  /**
   * Represents the document type definition.
   */
  public static readonly DTD: number;

  /**
   * Represents the end of an XML document.
   */
  public static readonly END_DOCUMENT: number;

  /**
   * Represents the end of an element in an XML document.
   */
  public static readonly END_ELEMENT: number;

  /**
   * Represents the entity declaration in an XML document.
   */
  public static readonly ENTITY_DECLARATION: number;

  /**
   * Represents an entity reference in an XML document.
   */
  public static readonly ENTITY_REFERENCE: number;

  /**
   * Represents a namespace declaration in an XML document.
   */
  public static readonly NAMESPACE: number;

  /**
   * Represents the notation declaration in an XML document.
   */
  public static readonly NOTATION_DECLARATION: number;

  /**
   * Represents processing instruction in an XML document.
   */
  public static readonly PROCESSING_INSTRUCTION: number;

  /**
   * Represents a space in an XML document.
   */
  public static readonly SPACE: number;

  /**
   * Represents the start of an XML document.
   */
  public static readonly START_DOCUMENT: number;

  /**
   * Represents the start of an element in an XML document.
   */
  public static readonly START_ELEMENT: number;
}

export = XMLStreamConstants;
