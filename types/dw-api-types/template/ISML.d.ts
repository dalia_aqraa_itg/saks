declare class ISML {
  /**
   * Renders an ISML template and writes the output to the current response.
   * @param template
   */
  public static renderTemplate(template: string): void;

  /**
   * Renders an ISML template and writes the output to the current response.
   * @param template
   * @param templateArgs
   */
  public static renderTemplate(template: string, templateArgs: object): void;
  private constructor();
}

export = ISML;
