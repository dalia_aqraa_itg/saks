import Product = require('./Product');

declare class Variant extends Product {
  public masterProduct: Product;
  public getMasterProduct(): Product;
}

export = Variant;
