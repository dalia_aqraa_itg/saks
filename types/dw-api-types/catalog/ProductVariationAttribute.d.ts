declare class ProductVariationAttribute {
  public attributeID: string;
  public displayName: string;
  public ID: string;
  public getAttributeID(): string;
  public getDisplayName(): string;
  public getID(): string;
}

export = ProductVariationAttribute;
