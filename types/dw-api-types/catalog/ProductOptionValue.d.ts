import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');

declare global {
  namespace ICustomAttributes {
    interface ProductOptionValue extends CustomAttributes {}
  }
}

declare class ProductOptionValue extends ExtensibleObject<ICustomAttributes.ProductOptionValue> {
  public description: string;
  public displayValue: string;
  public ID: string;
  public productIDModifier: string;

  public getDescription(): string;
  public getDisplayValue(): string;
  public getID(): string;
  public getProductIDModifier(): string;
}

export = ProductOptionValue;
