import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');

declare global {
  namespace ICustomAttributes {
    interface PriceBook extends CustomAttributes {}
  }
}

declare class PriceBook extends ExtensibleObject<ICustomAttributes.PriceBook> {
  public currencyCode: string;
  public description: string;
  public displayName: string;
  public ID: string;
  public online: boolean;
  public onlineFlag: boolean;
  public onlineFrom: Date;
  public onlineTo: Date;
  public parentPriceBook: PriceBook;

  public getCurrencyCode(): string;
  public getDescription(): string;
  public getDisplayName(): string;
  public getID(): string;
  public getOnlineFlag(): boolean;
  public getOnlineFrom(): Date;
  public getOnlineTo(): Date;
  public getParentPriceBook(): PriceBook;
  public isOnline(): boolean;
}

export = PriceBook;
