import Money = require('../value/Money');
import PriceBook = require('./PriceBook');

declare class ProductPriceInfo {
  public onlineFrom: Date;
  public onlineTo: Date;
  public percentage: number;
  public price: Money;
  public priceBook: PriceBook;
  public priceInfo: string;
  public getOnlineFrom(): Date;
  public getOnlineTo(): Date;
  public getPercentage(): number;
  public getPrice(): Money;
  public getPriceBook(): PriceBook;
  public getPriceInfo(): string;
}

export = ProductPriceInfo;
