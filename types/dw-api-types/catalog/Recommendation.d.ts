import MarkupText = require('../content/MarkupText');
import MediaFile = require('../content/MediaFile');
import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Catalog = require('./Catalog');
import Category = require('./Category');
import Product = require('./Product');

declare global {
  namespace ICustomAttributes {
    interface Recommendation extends CustomAttributes {}
  }
}

declare class Recommendation extends ExtensibleObject<ICustomAttributes.Recommendation> {
  public calloutMsg: MarkupText;
  public catalog: Catalog;
  public image: MediaFile;
  public longDescription: MarkupText;
  public name: string;
  public recommendationType: number;
  public recommendedItem: Product;
  public recommendedItemID: string;
  public shortDescription: MarkupText;
  public sourceItem: Product | Category;
  public sourceItemID: string;

  public getCalloutMsg(): MarkupText;
  public getCatalog(): Catalog;
  public getImage(): MediaFile;
  public getLongDescription(): MarkupText;
  public getName(): string;
  public getRecommendationType(): number;
  public getRecommendedItem(): Product;
  public getRecommendedItemID(): string;
  public getShortDescription(): MarkupText;
  public getSourceItem(): Product | Category;
  public getSourceItemID(): string;
}

export = Recommendation;
