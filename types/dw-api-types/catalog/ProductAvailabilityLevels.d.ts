import Quantity = require('../value/Quantity');

declare class ProductAvailabilityLevels {
  public backorder: Quantity;
  public count: number;
  public inStock: Quantity;
  public notAvailable: Quantity;
  public preorder: Quantity;

  public getBackorder(): Quantity;
  public getCount(): number;
  public getInStock(): Quantity;
  public getNotAvailable(): Quantity;
  public getPreorder(): Quantity;
}

export = ProductAvailabilityLevels;
