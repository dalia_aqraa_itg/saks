import ObjectAttributeDefinition = require('../object/ObjectAttributeDefinition');
import ObjectAttributeGroup = require('../object/ObjectAttributeGroup');
import Collection = require('../util/Collection');

declare class ProductAttributeModel {
  public attributeGroups: Collection<ObjectAttributeGroup>;
  public orderRequiredAttributeDefinitions: Collection<ObjectAttributeGroup>;
  public visibleAttributeGroups: Collection<ObjectAttributeGroup>;

  public getAttributeDefinition(id: string): ObjectAttributeDefinition;
  public getAttributeDefinitions(group: ObjectAttributeGroup): Collection<ObjectAttributeDefinition>;
  public getAttributeGroup(id: string): ObjectAttributeGroup;
  public getAttributeGroups(): Collection<ObjectAttributeGroup>;
  public getDisplayValue(definition: ObjectAttributeDefinition): Object;
  public getOrderRequiredAttributeDefinitions(): Collection<ObjectAttributeDefinition>;
  public getValue(definition: ObjectAttributeDefinition): Object;
  public getVisibleAttributeDefinitions(group: ObjectAttributeGroup): Collection<ObjectAttributeDefinition>;
  public getVisibleAttributeGroups(): Collection<ObjectAttributeGroup>;
}

export = ProductAttributeModel;
