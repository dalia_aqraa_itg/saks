import SeekableIterator = require('../util/SeekableIterator');
import Catalog = require('./Catalog');
import Product = require('./Product');

/**
 * Provides helper methods for getting products based on Product ID or Catalog.
 */
declare class ProductMgr {
  /**
   * Returns the product with the specified id.
   * @param productID
   */
  public static getProduct(productID: string): Product | null;

  /**
   * Returns all products assigned to the current site.
   */
  public static queryAllSiteProducts(): SeekableIterator<Product>;

  /**
   * Returns all products assigned to the current site.
   */
  public static queryAllSiteProductsSorted(): SeekableIterator<Product>;

  /**
   * Returns all products assigned to the the specified catalog, where assignment has the same meaning as it does for queryAllSiteProducts().
   * @param catalog
   */
  public static queryProductsInCatalog(catalog: Catalog): SeekableIterator<Product>;

  /**
   * Returns all products assigned to the the specified catalog.
   * @param catalog
   */
  public static queryProductsInCatalogSorted(catalog: Catalog): SeekableIterator<Product>;
  private constructor();
}

export = ProductMgr;
