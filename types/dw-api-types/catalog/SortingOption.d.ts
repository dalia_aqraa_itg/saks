import SortingRule = require('./SortingRule');

declare class SortingOption {
  /**
   * The description of the sorting option for the current locale.
   */
  public readonly description: string;

  /**
   * The display name of the of the sorting option for the current locale.
   */
  public readonly displayName: string;

  /**
   * The ID of the sorting option.
   */
  public readonly ID: string;

  /**
   * The sorting rule for this sorting option, or null if there is no associated rule.
   */
  public readonly sortingRule: SortingRule | null;

  private constructor();

  /**
   * Returns the description of the sorting option for the current locale.
   */
  public getDescription(): string;

  /**
   * Returns the display name of the of the sorting option for the current locale.
   */
  public getDisplayName(): string;

  /**
   * Returns the ID of the sorting option.
   */
  public getID(): string;

  /**
   * Returns the sorting rule for this sorting option, or null if there is no associated rule.
   */
  public getSortingRule(): SortingRule | null;
}

export = SortingOption;
