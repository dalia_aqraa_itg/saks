import MediaFile = require('../content/MediaFile');
import Collection = require('../util/Collection');
import HashMap = require('../util/HashMap');
import List = require('../util/List');
import URL = require('../web/URL');
import Product = require('./Product');
import ProductVariationAttribute = require('./ProductVariationAttribute');
import ProductVariationAttributeValue = require('./ProductVariationAttributeValue');
import Variant = require('./Variant');
import VariationGroup = require('./VariationGroup');

declare class ProductVariationModel {
  public defaultVariant: Variant;
  public master: Product;
  public productVariationAttributes: Collection<ProductVariationAttribute>;
  public selectedVariant: Variant;
  public selectedVariants: Collection<Variant>;
  public variants: Collection<Variant>;
  public variationGroups: Collection<VariationGroup>;

  public getAllValues(attribute: ProductVariationAttribute): Collection<ProductVariationAttributeValue>;
  public getDefaultVariant(): Variant;
  public getFilteredValues(attribute: ProductVariationAttribute): Collection<ProductVariationAttributeValue>;
  public getHtmlName(attribute: ProductVariationAttribute): string;
  public getHtmlName(prefix: string, attribute: ProductVariationAttribute): string;
  public getImage(viewtype: string, attribute: ProductVariationAttribute, value: ProductVariationAttributeValue): MediaFile;
  public getImage(viewtype: string, index: number): MediaFile;
  public getImage(viewtype: string): MediaFile;
  public getImages(viewtype: string): List<MediaFile>;
  public getMaster(): Product;
  public getProductVariationAttribute(id: string): ProductVariationAttribute;
  public getProductVariationAttributes(): Collection<ProductVariationAttribute>;
  public getSelectedValue(attribute: ProductVariationAttribute): ProductVariationAttributeValue;
  public getSelectedVariant(): Variant;
  public getSelectedVariants(): Collection<Variant>;
  public getVariants(): Collection<Variant>;
  public getVariants(filter: HashMap<string, string>): Collection<Variant>;
  public getVariationGroups(): Collection<VariationGroup>;
  public getVariationValue(variantOrVariationGroup: Product, attribute: ProductVariationAttribute): ProductVariationAttributeValue | null;
  public hasOrderableVariants(attribute: ProductVariationAttribute, value: ProductVariationAttributeValue): boolean;
  public isSelectedAttributeValue(attribute: ProductVariationAttribute, value: ProductVariationAttributeValue): boolean;
  /**
     * Applies a selected attribute value to this model instance. Usually this method is used to set the model state corresponding to the variation attribute values specified by a URL. The URLs can be obtained by using one of the models URL methods, like urlSelectVariationValue(String, ProductVariationAttribute, ProductVariationAttributeValue) and urlUnselectVariationValue(String, ProductVariationAttribute). Anyway, there are some limitations to keep in mind when selecting variation attribute values. A Variation Model created for a Variation Group or Variant Product is bound to an initial state. Example:

    *    * A Variation Model created for Variation Group A can't be switched to Variation Group B.
    *    * A Variation Model created for Variant A can't be switched to Variant B.
    *    * The state of a Variation Model for a Variation Group that defines color = red can't be changed to color = black.
    *    * The state of a Variation Model for a Variant that defines color = red / size = L can't be changed to color = black / size = S. However, the state of a Variation Model created for a Variation Group that defines color = red can be changed to a more specific state by adding another selected value, e.g. size = L.


    * The state of a Variation Model created for a Variation Master can be changed in any possible way because the initial state involves all variation values and Variants.
     * @param variationAttributeID the ID of an product variation attribute, must not be null, otherwise a exception is thrown
     * @param variationAttributeValueID the ID of the product variation attribute value to apply, this value must not be part of the initial model state (e.g. the variant or group that the model has been created for), otherwise a exception is thrown
     */
  public setSelectedAttributeValue(variationAttributeID: string, variationAttributeValueID: string): void;
  public url(action: string, ...varAttrAndValues: string): URL;
  public urlSelectVariationValue(action: string, attribute: ProductVariationAttribute, value: ProductVariationAttributeValue): string;
  public urlUnselectVariationValue(action: string, attribute: ProductVariationAttribute): string;
}

export = ProductVariationModel;
