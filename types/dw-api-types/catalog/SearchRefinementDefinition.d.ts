import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');

declare global {
  namespace ICustomAttributes {
    interface SearchRefinementDefinition extends CustomAttributes {}
  }
}
/**
 * Common search refinement definition base class.
 */
declare class SearchRefinementDefinition extends ExtensibleObject<ICustomAttributes.SearchRefinementDefinition> {
  /**
   * The attribute ID. If the refinement definition is not an attribute refinement, the method returns an empty string.
   */
  public readonly attributeID: string;

  /**
   * Identifies if this is an attribute refinement.
   */
  public readonly attributeRefinement: boolean;

  /**
   * The cut-off threshold.
   */
  public readonly cutoffThreshold: number;

  /**
   * The display name.
   */
  public readonly displayName: string;

  /**
   * A code for the data type used for this search refinement definition. See constants defined in ObjectAttributeDefinition.
   */
  public readonly valueTypeCode: number;

  protected constructor();

  /**
   * Returns the attribute ID.
   */
  public getAttributeID(): string;

  /**
   * Returns the cut-off threshold.
   */
  public getCutoffThreshold(): number;

  /**
   * Returns the display name.
   */
  public getDisplayName(): string;

  /**
   * Returns a code for the data type used for this search refinement definition.
   */
  public getValueTypeCode(): number;

  /**
   * Identifies if this is an attribute refinement.
   */
  public isAttributeRefinement(): boolean;
}

export = SearchRefinementDefinition;
