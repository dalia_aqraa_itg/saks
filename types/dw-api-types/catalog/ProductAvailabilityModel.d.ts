import ProductAvailabilityLevels = require('./ProductAvailabilityLevels');
import ProductInventoryRecord = require('./ProductInventoryRecord');

declare class ProductAvailabilityModel {
  public static AVAILABILITY_STATUS_BACKORDER: string;
  public static AVAILABILITY_STATUS_IN_STOCK: string;
  public static AVAILABILITY_STATUS_NOT_AVAILABLE: string;
  public static AVAILABILITY_STATUS_PREORDER: string;
  public availability: number;
  public availabilityStatus: string;
  public inStock: boolean;
  public inventoryRecord: ProductInventoryRecord;
  public orderable: boolean;
  public SKUCoverage: number;
  public timeToOutOfStock: number;

  public getAvailability(): number;
  public getAvailabilityLevels(quantity: number): ProductAvailabilityLevels;
  public getAvailabilityStatus():
    | typeof ProductAvailabilityModel.AVAILABILITY_STATUS_BACKORDER
    | typeof ProductAvailabilityModel.AVAILABILITY_STATUS_IN_STOCK
    | typeof ProductAvailabilityModel.AVAILABILITY_STATUS_NOT_AVAILABLE
    | typeof ProductAvailabilityModel.AVAILABILITY_STATUS_PREORDER;

  /**
   * Returns the ProductInventoryRecord for the Product associated with this model.
   * @return the ProductInventoryRecord or null if there is none.
   */
  public getInventoryRecord(): ProductInventoryRecord | null;
  public getSKUCoverage(): number;
  public getTimeToOutOfStock(): number;

  /**
     *
     * Returns true if the Product is in-stock in the given quantity. This is determined as follows:
     *   * If the product is not currently online (based on its online flag and online dates), then return false.
     *   * If there is no inventory-list for the current site, then return false.
     *   * If there is no inventory-record for the product, then return the default setting on the inventory-list.
     *   * If there is no allocation-amount on the inventory-record, then return the value of the perpetual-flag.
     *   * If there is an allocation-amount, but the perpetual-flag is true, then return true.
     *   * If the quantity is less than or equal to the stock-level, then return true.
     * Otherwise return false.

     * @param quantity - the quantity that is requested
     * @return true if the Product is in-stock.
     * @throws if the specified quantity is less or equal than zero
    */
  public isInStock(quantity: number): boolean;
  public isInStock(): boolean;
  public isOrderable(quantity: number): boolean;
  public isOrderable(): boolean;
}

export = ProductAvailabilityModel;
