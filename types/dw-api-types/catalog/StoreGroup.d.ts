import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Collection = require('../util/Collection');
import Store = require('./Store');

declare global {
  namespace ICustomAttributes {
    interface StoreGroup extends CustomAttributes {}
  }
}

declare class StoreGroup extends ExtensibleObject<ICustomAttributes.StoreGroup> {
  /**
   * The ID of the store group.
   */
  public readonly ID: string;

  /**
   * The name of the store group.
   */
  public readonly name: string;

  /**
   * All the stores that are assigned to the store group.
   */

  public readonly stores: Collection<Store>;
  private constructor();

  /**
   * Returns the ID of the store group.
   */
  public getID(): string;

  /**
   * Returns the name of the store group.
   */
  public getName(): string;

  /**
   * Returns all the stores that are assigned to the store group.
   */
  public getStores(): Collection<Store>;
}

export = StoreGroup;
