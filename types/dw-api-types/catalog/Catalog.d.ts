import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Category = require('./Category');

declare global {
  namespace ICustomAttributes {
    interface Catalog extends CustomAttributes {}
  }
}

declare class Catalog extends ExtensibleObject<ICustomAttributes.Catalog> {
  public description: string;
  public displayName: string;
  public ID: string;
  public root: Category;

  public getDescription(): string;
  public getDisplayName(): string;
  public getID(): string;
  public getRoot(): Category;
}

export = Catalog;
