import Collection = require('../util/Collection');
import URL = require('../web/URL');
import URLRedirect = require('../web/URLRedirect');
import SearchRefinementValue = require('./SearchRefinementValue');

/**
 * Common search model base class.
 */
declare class SearchModel {
  /**
   * URL Parameter for the Search Phrase
   */
  public static readonly SEARCH_PHRASE_PARAMETER: string;

  /**
   * Sorting parameter ASCENDING
   */
  public static readonly SORT_DIRECTION_ASCENDING: number;

  /**
   * Sorting parameter DESCENDING
   */
  public static readonly SORT_DIRECTION_DESCENDING: number;

  /**
   * Sorting parameter NO_SORT - will remove a sorting condition
   */
  public static readonly SORT_DIRECTION_NONE: number;

  /**
   * Returns an URLRedirect object for a search phrase.
   * @param searchPhrase
   */
  public static getSearchRedirect(searchPhrase: string): URLRedirect;

  /**
   * The number of search results found by this search.
   */
  public readonly count: number;

  /**
   * Identifies if the query is emtpy when no search term, search parameter or refinement was specified for the search. In case also no result is returned. This "empty" is different to a query with a specified query and with an empty result.
   */
  public readonly emptyQuery: boolean;

  /**
   * The method returns true, if this search is refined by at least one attribute.
   */
  public readonly refinedByAttribute: boolean;

  /**
   * Identifies if this was a refined search. A search is a refined search if at least one refinement is part of the query.
   */
  public readonly refinedSearch: boolean;

  /**
   * The search phrase used in this search.
   */
  public searchPhrase: string;
  protected constructor();

  /**
   * Adds a refinement.
   * @param attributeID
   * @param values
   */
  public addRefinementValues(attributeID: string, values: string): void;

  /**
   * Identifies if the search can be relaxed without creating a search for all searchable items.
   */
  public canRelax(): boolean;

  /**
   * Returns the number of search results found by this search.
   */
  public getCount(): number;

  /**
   * Returns the maximum refinement value selected in the query for the specific attribute, or null if there is no maximum refinement value or no refinement for that attribute.
   * @param attributeID
   */
  public getRefinementMaxValue(attributeID: string): string;

  /**
   * Returns the minimum refinement value selected in the query for the specific attribute, or null if there is no minimum refinement value or no refinement for that attribute.
   * @param attributeID
   */
  public getRefinementMinValue(attributeID: string): string;

  /**
   * Returns the list of selected refinement values for the given attribute as used in the search.
   * @param attributeID
   */
  public getRefinementValues(attributeID: string): Collection<SearchRefinementValue>;

  /**
   * Returns the search phrase used in this search.
   */
  public getSearchPhrase(): string;

  /**
   * Returns the sorting condition for a given attribute name.
   * @param attributeID
   */
  public getSortingCondition(attributeID: string): number;

  /**
   * Identifies if the query is emtpy when no search term, search parameter or refinement was specified for the search.
   */
  public isEmptyQuery(): boolean;

  /**
   * Identifies if this search has been refined on the given attribute.
   * @param attributeID
   */
  public isRefinedByAttribute(attributeID: string): boolean;

  /**
   * The method returns true, if this search is refined by at least one attribute.
   */
  public isRefinedByAttribute(): boolean;

  /**
   * Identifies if this search has been refined on the given attribute and value.
   * @param attributeID
   * @param value
   */
  public isRefinedByAttributeValue(attributeID: string, value: string): boolean;

  /**
   * Identifies if this was a refined search.
   */
  public isRefinedSearch(): boolean;

  /**
   * Identifies if this search has been refined on the given attribute.
   * @param attributeID
   */
  public isRefinementByValueRange(attributeID: string): boolean;

  /**
   * Identifies if this search has been refined on the given attribute and range values.
   * @param attributeID
   * @param minValue
   * @param maxValue
   */
  public isRefinementByValueRange(attributeID: string, minValue: string, maxValue: string): boolean;

  /**
   * Removes a refinement.
   * @param attributeID
   * @param values
   */
  public removeRefinementValues(attributeID: string, values: string): void;

  /**
   * Execute the search.
   */
  public search(): void;

  /**
   * Sets a refinement value range for an attribute.
   * @param attributeID
   * @param minValue
   * @param maxValue
   */
  public setRefinementValueRange(attributeID: string, minValue: string, maxValue: string): void;

  /**
   * Sets refinement values for an attribute.
   * @param attributeID
   * @param values
   */
  public setRefinementValues(attributeID: string, values: string): void;

  /**
   * Sets the search phrase used in this search.
   * @param phrase
   */
  public setSearchPhrase(phrase: string): void;

  /**
   * Sets or removes a sorting condition for the specified attribute.
   * @param attributeID
   * @param direction
   */
  public setSortingCondition(attributeID: string, direction: number): void;

  /**
   * Constructs an URL that you can use to re-execute the exact same query.
   * @param action
   */
  public url(action: string): URL;

  /**
   * Constructs an URL that you can use to re-execute the exact same query.
   * @param url
   */
  public url(url: URL): URL;

  /**
   * Constructs an URL that you can use to re-execute the query with a default sorting.
   * @param url
   */
  public urlDefaultSort(url: string): URL;

  /**
   * Constructs an URL that you can use to re-execute the query with a default sorting.
   * @param url
   */
  public urlDefaultSort(url: URL): URL;

  /**
   * Constructs an URL that you can use to re-execute the query with an additional refinement.
   * @param action
   * @param attributeID
   * @param value
   */
  public urlRefineAttribute(action: string, attributeID: string, value: string): URL;

  /**
   * Constructs an URL that you can use to re-execute the query with an additional refinement.
   * @param url
   * @param attributeID
   * @param value
   */
  public urlRefineAttribute(url: URL, attributeID: string, value: string): URL;

  /**
   * Constructs an URL that you can use to re-execute the query with an additional refinement value for a given refinement attribute.
   * @param action
   * @param attributeID
   * @param value
   */
  public urlRefineAttributeValue(action: string, attributeID: string, value: string): URL;

  /**
   * Constructs an URL that you can use to re-execute the query with an additional refinement value for a given refinement attribute.
   * @param url
   * @param attributeID
   * @param value
   */
  public urlRefineAttributeValue(url: URL, attributeID: string, value: string): URL;

  /**
   * Constructs an URL that you can use to re-execute the query with an additional refinement value range for a given refinement attribute.
   * @param action
   * @param attributeID
   * @param minValue
   * @param maxValue
   */
  public urlRefineAttributeValueRange(action: string, attributeID: string, minValue: string, maxValue: string): URL;

  /**
   * Constructs an URL that you can use to re-execute the query without the specified refinement.
   * @param action
   * @param attributeID
   */
  public urlRelaxAttribute(action: string, attributeID: string): URL;

  /**
   * Constructs an URL that you can use to re-execute the query without the specified refinement.
   * @param url
   * @param attributeID
   */
  public urlRelaxAttribute(url: URL, attributeID: string): URL;

  /**
   * Constructs an URL that you can use to re-execute the query without the specified refinement.
   * @param action
   * @param attributeID
   * @param value
   */
  public urlRelaxAttributeValue(action: string, attributeID: string, value: string): URL;

  /**
   * Constructs an URL that you can use to re-execute the query without the specified refinement value.
   * @param url
   * @param attributeID
   * @param value
   */
  public urlRelaxAttributeValue(url: URL, attributeID: string, value: string): URL;

  /**
   * Constructs an URL that you can use to re-execute the query with a specific sorting criteria.
   * @param action
   * @param sortBy
   * @param sortDir
   */
  public urlSort(action: string, sortBy: string, sortDir: number): URL;

  /**
   * Constructs an URL that you can use to re-execute the query with a specific sorting criteria.
   * @param url
   * @param sortBy
   * @param sortDir
   */
  public urlSort(url: URL, sortBy: string, sortDir: number): URL;
}

export = SearchModel;
