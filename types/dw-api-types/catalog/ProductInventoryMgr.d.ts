import ProductInventoryList = require('./ProductInventoryList');

/**
 * This manager provides access to inventory-related objects.
 */
declare class ProductInventoryMgr {
  /**
   * Returns the inventory list assigned to the current site or null if no inventory list is assigned to the current site.
   */
  public static getInventoryList(): ProductInventoryList | null;

  /**
   * Returns the inventory list with the passed ID or null if no inventory list exists with that ID.
   * @param listID
   */
  public static getInventoryList(listID: string): ProductInventoryList | null;

  /**
   * The inventory list assigned to the current site or null if no inventory list is assigned to the current site.
   */
  public readonly inventoryList: ProductInventoryList | null;
  private constructor();
}

export = ProductInventoryMgr;
