import PersistentObject = require('../object/PersistentObject');

declare class SortingRule extends PersistentObject {
  public readonly ID: string;
  public getID(): string;
}

export = SortingRule;
