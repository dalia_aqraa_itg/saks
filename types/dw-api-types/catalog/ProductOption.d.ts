import MediaFile = require('../content/MediaFile');
import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Collection = require('../util/Collection');
import ProductOptionValue = require('./ProductOptionValue');

declare global {
  namespace ICustomAttributes {
    interface ProductOption extends CustomAttributes {}
  }
}

declare class ProductOption extends ExtensibleObject<ICustomAttributes.ProductOption> {
  public defaultValue: ProductOptionValue;
  public description: string;
  public displayName: string;
  public htmlName: string;
  public ID: string;
  public image: MediaFile;
  public optionValues: Collection<ProductOptionValue>;

  public getDefaultValue(): ProductOptionValue;
  public getDescription(): string;
  public getDisplayName(): string;
  public getHtmlName(): string;
  public getHtmlName(prefix: string): string;
  public getID(): string;
  public getImage(): MediaFile;
  public getOptionValues(): Collection<ProductOptionValue>;
}

export = ProductOption;
