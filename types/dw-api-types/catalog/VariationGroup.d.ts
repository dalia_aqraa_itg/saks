import Product = require('./Product');

declare class VariationGroup extends Product {
  public masterProduct: Product;
  public getMasterProduct(): Product;
}

export = VariationGroup;
