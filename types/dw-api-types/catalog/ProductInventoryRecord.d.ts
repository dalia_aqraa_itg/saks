import ExtensibleObject = require('../object/ExtensibleObject');
import Quantity = require('../value/Quantity');

import CustomAttributes = require('../object/CustomAttributes');

declare global {
  namespace ICustomAttributes {
    interface ProductInventoryRecord extends CustomAttributes {}
  }
}

declare class ProductInventoryRecord extends ExtensibleObject<ICustomAttributes.ProductInventoryRecord> {
  public allocation: Quantity;
  public allocationResetDate: Date;
  public ATS: Quantity;
  public backorderable: boolean;
  public inStockDate: Date;
  public perpetual: boolean;
  public preorderable: boolean;
  public preorderBackorderAllocation: Quantity;
  public reserved: Quantity;
  public stockLevel: Quantity;
  public turnover: Quantity;

  public getAllocation(): Quantity;
  public getAllocationResetDate(): Date;
  public getATS(): Quantity;
  public getInStockDate(): Date;
  public getPreorderBackorderAllocation(): Quantity;
  public getReserved(): Quantity;
  public getStockLevel(): Quantity;
  public getTurnover(): Quantity;
  public isBackorderable(): boolean;
  public isPerpetual(): boolean;
  public isPreorderable(): boolean;
  public setAllocation(quantity: number): void;
  public setBackorderable(backorderableFlag: boolean): void;
  public setInStockDate(inStockDate: Date): void;
  public setPerpetual(perpetualFlag: boolean): void;
  public setPreorderable(preorderableFlag: boolean): void;
  public setPreorderBackorderAllocation(quantity: number): void;
}

export = ProductInventoryRecord;
