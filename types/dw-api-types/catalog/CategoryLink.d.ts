import Category = require('./Category');

declare class CategoryLink {
  public static LINKTYPE_ACCESSORY: number;
  public static LINKTYPE_CROSS_SELL: number;
  public static LINKTYPE_OTHER: number;
  public static LINKTYPE_SPARE_PART: number;
  public static LINKTYPE_UP_SELL: number;

  public sourceCategory: Category;
  public targetCategory: Category;
  public typeCode: number;

  public getSourceCategory(): Category;
  public getTargetCategory(): Category;
  public getTypeCode(): number;
}

export = CategoryLink;
