import MarkupText = require('../content/MarkupText');
import MediaFile = require('../content/MediaFile');
import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Collection = require('../util/Collection');
import EnumValue = require('../value/EnumValue');
import ProductInventoryList = require('./ProductInventoryList');
import StoreGroup = require('./StoreGroup');

declare global {
  namespace ICustomAttributes {
    interface Store extends CustomAttributes {}
  }
}

declare class Store extends ExtensibleObject<ICustomAttributes.Store> {
  public address1: string;
  public address2: string;
  public city: string;
  public countryCode: EnumValue<string>;
  public email: string;
  public fax: string;
  public ID: string;
  public image: MediaFile;
  public inventoryList: ProductInventoryList | null;
  public inventoryListID: string | null;
  public latitude: number;
  public longitude: number;
  public name: string;
  public phone: string;
  public posEnabled: boolean;
  public postalCode: string;
  public stateCode: string;
  public storeEvents: MarkupText;
  public storeGroups: Collection<StoreGroup>;
  public storeHours: MarkupText;
  public storeLocatorEnabled: boolean;

  private constructor();

  public getAddress1(): string;
  public getAddress2(): string;
  public getCity(): string;
  public getCountryCode(): EnumValue<string>;

  public getEmail(): string;
  public getFax(): string;
  public getID(): string;
  public getImage(): MediaFile;
  public getInventoryList(): ProductInventoryList;
  public getInventoryListID(): string;
  public getLatitude(): number;
  public getLongitude(): number;
  public getName(): string;
  public getPhone(): string;
  public getPostalCode(): string;
  public getStateCode(): string;
  public getStoreEvents(): MarkupText;
  public getStoreGroups(): Collection<StoreGroup>;
  public getStoreHours(): MarkupText;
  public isPosEnabled(): boolean;
  public isStoreLocatorEnabled(): boolean;
}

export = Store;
