import SearchRefinementDefinition = require('./SearchRefinementDefinition');

declare class ProductSearchRefinementDefinition extends SearchRefinementDefinition {
  /**
   * Identifies if this is a category refinement.
   */
  public readonly categoryRefinement: boolean;

  /**
   * Identifies if this is a price refinement.
   */
  public readonly priceRefinement: boolean;

  protected constructor();

  /**
   * Identifies if this is a category refinement.
   */
  public isCategoryRefinement(): boolean;

  /**
   * Identifies if this is a price refinement.
   */
  public isPriceRefinement(): boolean;
}

export = ProductSearchRefinementDefinition;
