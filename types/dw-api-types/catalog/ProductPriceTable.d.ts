import Collection = require('../util/Collection');
import Money = require('../value/Money');
import Quantity = require('../value/Quantity');
import PriceBook = require('./PriceBook');

declare class ProductPriceTable {
  public quantities: Collection<Quantity>;

  public getNextQuantity(quantity: Quantity): Quantity;
  public getPercentage(quantity: Quantity): number;
  public getPrice(quantity: Quantity): Money;
  public getPriceBook(quantity: Quantity): PriceBook;
  public getQuantities(): Collection<Quantity>;
}

export = ProductPriceTable;
