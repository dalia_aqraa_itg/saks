import MediaFile = require('../content/MediaFile');
import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Collection = require('../util/Collection');
import CategoryAssignment = require('./CategoryAssignment');
import CategoryLink = require('./CategoryLink');
import Product = require('./Product');
import ProductAttributeModel = require('./ProductAttributeModel');
import Recommendation = require('./Recommendation');
import SortingRule = require('./SortingRule');

declare global {
  namespace ICustomAttributes {
    interface Category extends CustomAttributes {}
  }
}

declare class Category extends ExtensibleObject<ICustomAttributes.Category> {
  public allRecommendations: Collection<Recommendation>;
  public categoryAssignments: Collection<CategoryAssignment>;
  public defaultSortingRule: SortingRule;
  public description: string;
  public displayName: string;
  public ID: string;
  public image: MediaFile;
  public incomingCategoryLinks: Collection<CategoryLink>;
  public online: Boolean;
  public onlineCategoryAssignments: Collection<CategoryAssignment>;
  public onlineFlag: Boolean;
  public onlineFrom: Date;
  public onlineIncomingCategoryLinks: Collection<CategoryLink>;
  public onlineOutgoingCategoryLinks: Collection<CategoryLink>;
  public onlineProducts: Collection<Product>;
  public onlineSubCategories: Collection<Category>;
  public onlineTo: Date;
  public orderableRecommendations: Collection<Recommendation>;
  public outgoingCategoryLinks: Collection<CategoryLink>;
  public pageDescription: string;
  public pageKeywords: string;
  public pageTitle: string;
  public pageURL: string;
  public parent: Category;
  public productAttributeModel: ProductAttributeModel;
  public products: Collection<Product>;
  public recommendations: Collection<Recommendation>;
  public root: Boolean;
  public searchPlacement: number;
  public searchRank: number;
  public siteMapChangeFrequency: string;
  public siteMapPriority: number;
  public subCategories: Collection<Category>;
  public template: string;
  public thumbnail: MediaFile;
  public topLevel: Boolean;

  public getAllRecommendations(): Collection<Recommendation>;
  public getAllRecommendations(type: number): Collection<Recommendation>;
  public getCategoryAssignments(): Collection<CategoryAssignment>;
  public getDefaultSortingRule(): SortingRule;
  public getDescription(): string;
  public getDisplayName(): string;
  public getID(): string;
  public getImage(): MediaFile;
  public getIncomingCategoryLinks(): Collection<CategoryLink>;
  public getIncomingCategoryLinks(type: number): Collection<CategoryLink>;
  public getOnlineCategoryAssignments(): Collection<CategoryAssignment>;
  public getOnlineFlag(): Boolean;
  public getOnlineFrom(): Date;
  public getOnlineIncomingCategoryLinks(): Collection<CategoryLink>;
  public getOnlineOutgoingCategoryLinks(): Collection<CategoryLink>;
  public getOnlineProducts(): Collection<Product>;
  public getOnlineSubCategories(): Collection<Category>;
  public getOnlineTo(): Date;
  public getOrderableRecommendations(): Collection<Recommendation>;
  public getOrderableRecommendations(type: number): Collection<Recommendation>;
  public getOutgoingCategoryLinks(): Collection<CategoryLink>;
  public getOutgoingCategoryLinks(type: number): Collection<CategoryLink>;
  public getPageDescription(): string;
  public getPageKeywords(): string;
  public getPageTitle(): string;
  public getPageURL(): string;
  public getParent(): Category;
  public getProductAttributeModel(): ProductAttributeModel;
  public getProducts(): Collection<Product>;
  public getRecommendations(): Collection<Recommendation>;
  public getRecommendations(type: number): Collection<Recommendation>;
  public getSearchPlacement(): number;
  public getSearchRank(): number;
  public getSiteMapChangeFrequency(): string;
  public getSiteMapPriority(): number;
  public getSubCategories(): Collection<Category>;
  public getTemplate(): string;
  /**
   * @deprecated
   */
  public getThumbnail(): MediaFile;
  public isDirectSubCategoryOf(parent: Category): Boolean;
  public isOnline(): Boolean;
  public isRoot(): Boolean;
  public isSubCategoryOf(ancestor: Category): Boolean;
  public isTopLevel(): Boolean;
  public setSearchPlacement(placement: number): void;
  public setSearchRank(rank: number): void;
}

export = Category;
