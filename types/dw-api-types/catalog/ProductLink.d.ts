import Product = require('./Product');

declare class ProductLink {
  public static LINKTYPE_ACCESSORY: number;
  public static LINKTYPE_ALT_ORDERUNIT: number;
  public static LINKTYPE_CROSS_SELL: number;
  public static LINKTYPE_NEWER_VERSION: number;
  public static LINKTYPE_OTHER: number;
  public static LINKTYPE_REPLACEMENT: number;
  public static LINKTYPE_SPARE_PART: number;
  public static LINKTYPE_UP_SELL: number;

  public sourceProduct: Product;
  public targetProduct: Product;
  public typeCode: number;

  public getSourceProduct(): Product;
  public getTargetProduct(): Product;
  public getTypeCode(): number;
}

export = ProductLink;
