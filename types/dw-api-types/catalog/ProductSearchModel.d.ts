import SearchPhraseSuggestions = require('../suggest/SearchPhraseSuggestions');
import Iterator = require('../util/Iterator');
import List = require('../util/List');
import PageMetaTag = require('../web/PageMetaTag');
import URL = require('../web/URL');
import Category = require('./Category');
import Product = require('./Product');
import ProductSearchHit = require('./ProductSearchHit');
import ProductSearchRefinements = require('./ProductSearchRefinements');
import SearchModel = require('./SearchModel');
import SortingRule = require('./SortingRule');

/**
 * The class is the central interface to a product search result and a product search refinement. It also provides utility methods to generate a search URL
 */
declare class ProductSearchModel extends SearchModel {
  /**
   * URL Parameter for the category ID
   */
  public static readonly CATEGORYID_PARAMETER: string;

  /**
   * URL Parameter for the maximum price
   */
  public static readonly PRICE_MAX_PARAMETER: string;

  /**
   * URL Parameter for the minimum price
   */
  public static readonly PRICE_MIN_PARAMETER: string;

  /**
   * URL Parameter for the product ID
   */
  public static readonly PRODUCTID_PARAMETER: string;

  /**
   * constant indicating that all related products should be returned for the next product search by promotion ID
   */
  public static readonly PROMOTION_PRODUCT_TYPE_ALL: string;

  /**
   * constant indicating that only bonus products should be returned for the next product search by promotion ID. This constant should be set using setPromotionProductType(string) when using the search model to find the available list of bonus products for a Choice of Bonus Product (Rule) promotion, along with setPromotionID(string).
   */
  public static readonly PROMOTION_PRODUCT_TYPE_BONUS: string;

  /**
   * constant indicating that only discounted products should be returned for the next product search by promotion ID
   */
  public static readonly PROMOTION_PRODUCT_TYPE_DISCOUNTED: string;

  /**
   * URL Parameter for the promotion product type
   */
  public static readonly PROMOTION_PRODUCT_TYPE_PARAMETER: string;

  /**
   * constant indicating that only qualifying products should be returned for the next product search by promotion ID
   */
  public static readonly PROMOTION_PRODUCT_TYPE_QUALIFYING: string;

  /**
   * URL Parameter for the promotion ID
   */
  public static readonly PROMOTIONID_PARAMETER: string;

  /**
   * URL Parameter prefix for a refinement name
   */
  public static readonly REFINE_NAME_PARAMETER_PREFIX: string;

  /**
   * URL Parameter prefix for a refinement value
   */
  public static readonly REFINE_VALUE_PARAMETER_PREFIX: string;

  /**
   * URL Parameter prefix for a refinement value
   */
  public static readonly SORT_BY_PARAMETER_PREFIX: string;

  /**
   * URL Parameter prefix for a refinement value
   */
  public static readonly SORT_DIRECTION_PARAMETER_PREFIX: string;

  /**
   * URL Parameter prefix for a sorting rule
   */
  public static readonly SORTING_RULE_PARAMETER: string;

  /**
   * Constructs a URL that you can use to execute a query for a specific Category.
   * @param action
   * @param cgid
   */
  public static urlForCategory(action: string, cgid: string): URL;

  /**
   * Constructs a URL that you can use to execute a query for a specific Category.
   * @param url
   * @param cgid
   */
  public static urlForCategory(url: URL, cgid: string): URL;

  /**
   * Constructs a URL that you can use to execute a query for a specific Product.
   * @param action
   * @param cgid
   * @param pid
   */
  public static urlForProduct(action: string, cgid: string, pid: string): URL;

  /**
   * Constructs a URL that you can use to execute a query for a specific Product.
   * @param url
   * @param cgid
   * @param pid
   */
  public static urlForProduct(url: URL, cgid: string, pid: string): URL;

  /**
   * Constructs a URL that you can use to execute a query for a specific attribute name-value pair.
   * @param action
   * @param attributeID
   * @param value
   */
  public static urlForRefine(action: string, attributeID: string, value: string): URL;

  /**
   * Constructs a URL that you can use to execute a query for a specific attribute name-value pair.
   * @param url
   * @param attributeID
   * @param value
   */
  public static urlForRefine(url: URL, attributeID: string, value: string): URL;

  /**
   * The category object for the category id specified in the query. If a category with that id doesn't exist or if the category is offline this method returns null.
   */
  public readonly category: Category | null;

  /**
   * The category id that was specified in the search query.
   */
  public categoryID: string;

  /**
   * The method returns true, if this is a pure search for a category. The method checks, that a category ID is specified and no search phrase is specified.
   */
  public readonly categorySearch: boolean;

  /**
   * The deepest common category of all products in the search result. In case of an empty search result the method returns the root category.
   */
  public readonly deepestCommonCategory: Category;

  /**
   * Get the flag indicating whether unorderable products should be excluded when the next call to getProducts() is made. If this value has not been previously set, then the value returned will be based on the value of the search preference.
   */
  public orderableProductsOnly: boolean;

  /**
     * Reserved for beta users.
    Returns all page meta tags, defined for this instance for which content can be generated.
    The meta tag content is generated based on the product listing page meta tag context and rules. The rules are obtained from the current category context or inherited from the parent category, up to the root category.
    */
  public readonly pageMetaTags: PageMetaTag[];

  /**
   * The maximum price for the product associated with this search.
   */
  public priceMax: number;

  /**
   * The minimum price for the product associated with this search.
   */
  public priceMin: number;

  /**
   * The product id that was specified in the search query.
   */
  public productID: string;

  /**
     * All products in the search result.
    Note that products that were removed or went offline since the last index update are not included in the returned set.
    */
  public readonly products: Iterator<Product>;

  /**
     * The product search hits in the search result.
    Note that method does also return search hits representing products that were removed or went offline since the last index update, i.e. you must implement appropriate checks before accessing the product related to the search hit instance (see ProductSearchHit.getProduct)
    */
  public readonly productSearchHits: Iterator<ProductSearchHit>;

  /**
   * The promotion id that was specified in the search query or null if no promotion id set. If multiple promotion id's specified the method returns only the first id. See setPromotionIDs(List) and getPromotionIDs().
   */
  public promotionID: string | null;

  /**
   * A list of promotion id's that were specified in the search query or an empty list if no promotion id set.
   */
  public promotionIDs: List<string>;

  /**
   * The promotion product type specified in the search query.
   */
  public promotionProductType: string;

  /**
   * Get the flag that determines if the category search will be recursive.
   */
  public recursiveCategorySearch: boolean;

  /**
   * The method returns true, if the search is refined by a category. The method checks, that a category ID is specified.
   */
  public readonly refinedByCategory: boolean;

  /**
   * Identifies if this search has been refined by price.
   */
  public readonly refinedByPrice: boolean;

  /**
   * Identifies if this is a category search and is refined with further criteria, like a brand refinement or an attribute refinement.
   */
  public readonly refinedCategorySearch: boolean;

  /**
   * The ProductSearchRefinements associated with this search and filtered by session currency.
   */
  public readonly refinements: ProductSearchRefinements;

  /**
   * Returns search phrase suggestions for the current search phrase. Search phrase suggestions may contain alternative search phrases as well as lists of corrected and completed search terms.
   */
  public readonly searchPhraseSuggestions: SearchPhraseSuggestions;

  /**
   * The sorting rule explicitly set on this model to be used to order the products in the results of this query, or null if no rule has been explicitly set. This method does not return the sorting rule that will be used implicitly based on the context of the search, such as the refinement category.
   */
  public sortingRule: SortingRule;

  constructor();

  /**
   * Returns the category object for the category id specified in the query.
   */
  public getCategory(): Category | null;

  /**
   * Returns the category id that was specified in the search query.
   */
  public getCategoryID(): string;

  /**
   * Returns the deepest common category of all products in the search result.
   */
  public getDeepestCommonCategory(): Category;

  /**
   * Get the flag indicating whether unorderable products should be excluded when the next call to getProducts() is made.
   */
  public getOrderableProductsOnly(): boolean;

  /**
   * Reserved for beta users.
   * @param id
   */
  public getPageMetaTag(id: string): PageMetaTag;

  /**
   * Reserved for beta users.
   */
  public getPageMetaTags(): PageMetaTag[];

  /**
   * Returns the maximum price for the product associated with this search.
   */
  public getPriceMax(): number;

  /**
   * Returns the minimum price for the product associated with this search.
   */
  public getPriceMin(): number;

  /**
   * Returns the product id that was specified in the search query.
   */
  public getProductID(): string;

  /**
   * Returns all products in the search result.
   */
  public getProducts(): Iterator<Product>;

  /**
   * Returns the underlying ProductSearchHit for a product, or null if no ProductSearchHit found for this product.
   * @param product
   */
  public getProductSearchHit(product: Product): ProductSearchHit;

  /**
   * the method indicates if the search result is ordered by a personalized sorting rule.
   */
  public isPersonalizedSort(): boolean;

  /**
   * Returns the product search hits in the search result.
   */
  public getProductSearchHits(): Iterator<ProductSearchHit>;

  /**
   * Returns the promotion id that was specified in the search query or null if no promotion id set.
   */
  public getPromotionID(): string;

  /**
   * Returns a list of promotion id's that were specified in the search query or an empty list if no promotion id set.
   */
  public getPromotionIDs(): List<string>;

  /**
   * Returns the promotion product type specified in the search query.
   */
  public getPromotionProductType(): string;

  /**
   * Returns the ProductSearchRefinements associated with this search and filtered by session currency.
   */
  public getRefinements(): ProductSearchRefinements;

  /**
   * Returns search phrase suggestions for the current search phrase.
   */
  public getSearchPhraseSuggestions(): SearchPhraseSuggestions;

  /**
   * Returns the sorting rule explicitly set on this model to be used to order the products in the results of this query, or null if no rule has been explicitly set.
   */
  public getSortingRule(): SortingRule;

  /**
   * The method returns true, if this is a pure search for a category.
   */
  public isCategorySearch(): boolean;

  /**
   * Get the flag that determines if the category search will be recursive.
   */
  public isRecursiveCategorySearch(): boolean;

  /**
   * The method returns true, if the search is refined by a category.
   */
  public isRefinedByCategory(): boolean;

  /**
   * Identifies if this search has been refined by price.
   */
  public isRefinedByPrice(): boolean;

  /**
   * Identifies if this search has been refined by the given price range.
   * @param priceMin
   * @param priceMax
   */
  public isRefinedByPriceRange(priceMin: number, priceMax: number): boolean;

  /**
   * Identifies if this is a category search and is refined with further criteria, like a brand refinement or an attribute refinement.
   */
  public isRefinedCategorySearch(): boolean;

  /**
   * Execute the search.
   */
  public search(): void;

  /**
   * Specifies the category id used for the search query.
   * @param categoryID
   */
  public setCategoryID(categoryID: string): void;

  /**
   * Set a flag indicating whether unorderable products should be excluded when the next call to getProducts() is made.
   * @param orderableOnly
   */
  public setOrderableProductsOnly(orderableOnly: boolean): void;

  /**
   * Sets the maximum price for the product associated with this search.
   * @param priceMax
   */
  public setPriceMax(priceMax: number): void;

  /**
   * Sets the minimum price for the product associated with this search.
   * @param priceMin
   */
  public setPriceMin(priceMin: number): void;

  /**
   * Specifies the product id used for the search query.
   * @param productID
   */
  public setProductID(productID: string | null): void;

  /**
   * Specifies the promotion id used for the search query.
   * @param promotionID
   */
  public setPromotionID(promotionID: string | null): void;

  /**
   * Specifies multiple promotion id's used for the search query.
   * @param promotionIDs
   */
  public setPromotionIDs(promotionIDs: List<string>): void;

  /**
   * Specifies the promotion product type used for the search query.
   * @param promotionProductType
   */
  public setPromotionProductType(promotionProductType: string): void;

  /**
   * Set a flag to indicate if the search in category should be recursive.
   * @param recurse
   */
  public setRecursiveCategorySearch(recurse: boolean): void;

  /**
   * Sets the sorting rule to be used to order the products in the results of this query.
   * @param rule
   */
  public setSortingRule(rule: SortingRule): void;

  /**
   * Constructs a URL that you can use to re-execute the query with a category refinement.
   * @param action
   * @param refineCategoryID
   */
  public urlRefineCategory(action: string, refineCategoryID: string): URL;

  /**
   * Constructs a URL that you can use to re-execute the query with a category refinement.
   * @param url
   * @param refineCategoryID
   */
  public urlRefineCategory(url: URL, refineCategoryID: string): URL;

  /**
   * Constructs a URL that you can use to re-execute the query with an additional price filter.
   * @param action
   * @param min
   * @param max
   */
  public urlRefinePrice(action: string, min: number, max: number): URL;

  /**
   * Constructs a URL that you can use to re-execute the query with an additional price filter.
   * @param url
   * @param min
   * @param max
   */
  public urlRefinePrice(url: URL, min: number, max: number): URL;

  /**
   * Constructs a URL that you can use to re-execute the query without any category refinement.
   * @param action
   */
  public urlRelaxCategory(action: string): URL;

  /**
   * Constructs a URL that you can use to re-execute the query without any category refinement.
   * @param url
   */
  public urlRelaxCategory(url: URL): URL;

  /**
   * Constructs a URL that you can use to re-execute the query with no price filter.
   * @param action
   */
  public urlRelaxPrice(action: string): URL;

  /**
   * Constructs a URL that you can use to would re-execute the query with no price filter.
   * @param url
   */
  public urlRelaxPrice(url: URL): URL;

  /**
   * Constructs a URL that you can use to re-execute the query but sort the results by the given rule.
   * @param action
   * @param rule
   */
  public urlSortingRule(action: string, rule: SortingRule): URL;

  /**
   * Constructs a URL that you can use to re-execute the query but sort the results by the given rule.
   * @param url
   * @param rule
   */
  public urlSortingRule(url: URL, rule: SortingRule): URL;
}

export = ProductSearchModel;
