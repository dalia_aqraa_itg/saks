import MarkupText = require('../content/MarkupText');
import MediaFile = require('../content/MediaFile');
import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Category = require('./Category');
import Product = require('./Product');

declare global {
  namespace ICustomAttributes {
    interface CategoryAssignment extends CustomAttributes {}
  }
}

declare class CategoryAssignment extends ExtensibleObject<ICustomAttributes.CategoryAssignment> {
  public calloutMsg: MarkupText;
  public category: Category;
  public image: MediaFile;
  public longDescription: MarkupText;
  public name: string;
  public product: Product;
  public shortDescription: MarkupText;

  public getCalloutMsg(): MarkupText;
  public getCategory(): Category;
  public getImage(): MediaFile;
  public getLongDescription(): MarkupText;
  public getName(): string;
  public getProduct(): Product;
  public getShortDescription(): MarkupText;
}

export = CategoryAssignment;
