import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Product = require('./Product');
import ProductInventoryRecord = require('./ProductInventoryRecord');

declare global {
  namespace ICustomAttributes {
    interface ProductInventoryList extends CustomAttributes {}
  }
}

declare class ProductInventoryList extends ExtensibleObject<ICustomAttributes.ProductInventoryList> {
  public defaultInStockFlag: boolean;
  public description: string;
  public ID: string;
  public getDefaultInStockFlag(): boolean;
  public getDescription(): string;
  public getID(): string;
  public getRecord(product: Product): ProductInventoryRecord;
  public getRecord(productID: string): ProductInventoryRecord;
}

export = ProductInventoryList;
