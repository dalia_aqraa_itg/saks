import Collection = require('../util/Collection');
import Money = require('../value/Money');
import URL = require('../web/URL');
import ProductOption = require('./ProductOption');
import ProductOptionValue = require('./ProductOptionValue');

declare class ProductOptionModel {
  public options: Collection<ProductOption>;

  public getOption(optionID: string): ProductOption;
  public getOptions(): Collection<ProductOption>;
  public getOptionValue(option: ProductOption, valueID: string): ProductOptionValue;
  public getOptionValues(option: ProductOption): Collection<ProductOptionValue>;
  public getPrice(optionValue: ProductOptionValue): Money;
  public getSelectedOptionValue(option: ProductOption): ProductOptionValue;
  public isSelectedOptionValue(option: ProductOption, value: ProductOptionValue): boolean;
  public setSelectedOptionValue(option: ProductOption, value: ProductOptionValue): void;
  public url(action: string, varOptionAndValues: Object): URL;
  public urlSelectOptionValue(action: string, option: ProductOption, value: ProductOptionValue): string;
}

export = ProductOptionModel;
