import Collection = require('../util/Collection');
import Money = require('../value/Money');
import Quantity = require('../value/Quantity');
import LineItem = require('./LineItem');
import PriceAdjustment = require('./PriceAdjustment');
import ProductLineItem = require('./ProductLineItem');
import Shipment = require('./Shipment');

declare class ProductShippingLineItem extends LineItem {
  /**
   * The gross price of the product shipping line item after applying all product-shipping-level adjustments.
   */
  public readonly adjustedGrossPrice: Money;

  /**
   * The net price of the product shipping line item after applying all product-shipping-level adjustments.
   */
  public readonly adjustedNetPrice: Money;

  /**
   * The price of the product shipping line item after applying all pproduct-shipping-level adjustments. For net pricing the adjusted net price is returned (see getAdjustedNetPrice()). For gross pricing, the adjusted gross price is returned (see getAdjustedGrossPrice()).
   */
  public readonly adjustedPrice: Money;

  /**
   * The tax of the unit after applying adjustments, in the purchase currency.
   */
  public readonly adjustedTax: Money;

  /**
   * An iterator of price adjustments that have been applied to this product shipping line item.
   */
  public readonly priceAdjustments: Collection<PriceAdjustment>;

  /**
   * The parent product line item this shipping line item belongs to.
   */
  public readonly productLineItem: ProductLineItem;

  /**
   * The quantity of the shipping cost.
   */
  public quantity: Quantity;

  /**
   * The shipment this shipping line item belongs to.
   */
  public readonly shipment: Shipment;

  /**
   * The 'surcharge' flag.
   */
  public surcharge: boolean;

  private constructor();

  /**
   * Returns the gross price of the product shipping line item after applying all product-shipping-level adjustments.
   */
  public getAdjustedGrossPrice(): Money;

  /**
   * Returns the net price of the product shipping line item after applying all product-shipping-level adjustments.
   */
  public getAdjustedNetPrice(): Money;

  /**
   * Returns the price of the product shipping line item after applying all pproduct-shipping-level adjustments.
   */
  public getAdjustedPrice(): Money;

  /**
   * Returns the tax of the unit after applying adjustments, in the purchase currency.
   */
  public getAdjustedTax(): Money;

  /**
   * Returns an iterator of price adjustments that have been applied to this product shipping line item.
   */
  public getPriceAdjustments(): Collection<PriceAdjustment>;

  /**
   * Returns the parent product line item this shipping line item belongs to.
   */
  public getProductLineItem(): ProductLineItem;

  /**
   * Returns the quantity of the shipping cost.
   */
  public getQuantity(): Quantity;

  /**
   * Returns the shipment this shipping line item belongs to.
   */
  public getShipment(): Shipment;

  /**
   * Returns the 'surcharge' flag.
   */
  public isSurcharge(): boolean;

  /**
     * Sets price attributes of the line item based on the purchase currency, taxation policy and line item quantity.
    The method sets the 'basePrice' attribute of the line item.
    * @param value 
    */
  public setPriceValue(value: number): void;

  /**
   * Sets the quantity of the shipping cost.
   * @param quantity
   */
  public setQuantity(quantity: Quantity): void;

  /**
   * Sets the 'surcharge' flag.
   * @param flag
   */
  public setSurcharge(flag: boolean): void;
}

export = ProductShippingLineItem;
