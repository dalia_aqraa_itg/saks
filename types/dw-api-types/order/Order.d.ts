import SourceCodeGroup = require('../campaign/SourceCodeGroup');
import Customer = require('../customer/Customer');
import CustomAttributes = require('../object/CustomAttributes');
import Note = require('../object/Note');
import LineItemCtnr = require('../order/LineItemCtnr');
import Status = require('../system/Status');
import Collection = require('../util/Collection');
import FilteringCollection = require('../util/FilteringCollection');
import EnumValue = require('../value/EnumValue');
import Money = require('../value/Money');
import Appeasement = require('./Appeasement');
import AppeasementItem = require('./AppeasementItem');
import Invoice = require('./Invoice');
import InvoiceItem = require('./InvoiceItem');
import OrderItem = require('./OrderItem');
import OrderPaymentInstrument = require('./OrderPaymentInstrument');
import Return = require('./Return');
import ReturnCase = require('./ReturnCase');
import ReturnCaseItem = require('./ReturnCaseItem');
import ReturnItem = require('./ReturnItem');
import ShippingOrder = require('./ShippingOrder');
import ShippingOrderItem = require('./ShippingOrderItem');

declare global {
  namespace ICustomAttributes {
    interface Order extends CustomAttributes {}
  }
}

/**
 * The Order class represents an order.
 */
declare class Order extends LineItemCtnr<ICustomAttributes.Order> {
  public static readonly CONFIRMATION_STATUS_CONFIRMED: 2;
  public static readonly CONFIRMATION_STATUS_NOTCONFIRMED: 1;
  public static readonly EXPORT_STATUS_EXPORTED: 1;
  public static readonly EXPORT_STATUS_FAILED: 3;
  public static readonly EXPORT_STATUS_NOTEXPORTED: 0;
  public static readonly EXPORT_STATUS_READY: 2;
  public static readonly ORDER_STATUS_CANCELLED: 6;
  public static readonly ORDER_STATUS_COMPLETED: 5;
  public static readonly ORDER_STATUS_CREATED: 0;
  public static readonly ORDER_STATUS_FAILED: 8;
  public static readonly ORDER_STATUS_NEW: 3;
  public static readonly ORDER_STATUS_OPEN: 4;
  public static readonly ORDER_STATUS_REPLACED: 7;
  public static readonly PAYMENT_STATUS_NOTPAID: 0;
  public static readonly PAYMENT_STATUS_PAID: 2;
  public static readonly PAYMENT_STATUS_PARTPAID: 1;
  public static readonly SHIPPING_STATUS_NOTSHIPPED: 0;
  public static readonly SHIPPING_STATUS_PARTSHIPPED: 1;
  public static readonly SHIPPING_STATUS_SHIPPED: 2;

  /**
   * The affiliate partner ID value, or null.
   */
  public affiliatePartnerID: string | null;

  /**
   * The affiliate partner name value, or null.
   */
  public affiliatePartnerName: string | null;

  /**
   * The collection of AppeasementItems associated with this order.
   */
  public readonly appeasementItems: FilteringCollection<AppeasementItem>;

  /**
   * The collection of Appeasements associated with this order.
   */
  public readonly appeasements: FilteringCollection<Appeasement>;

  /**
   * If this order was cancelled, returns the value of the cancel code or null.
   */
  public cancelCode: EnumValue<number> | null;

  /**
   * If this order was cancelled, returns the text describing why the order was cancelled or null.
   */
  public cancelDescription: string | null;

  /**
   * The sum of the captured amounts. The captured amounts are calculated on the fly. Associate a payment capture for an PaymentInstrument with an Invoice using Invoice.addCaptureTransaction(OrderPaymentInstrument, Money).
   */
  public readonly capturedAmount: Money;

  /**
     * The confirmation status of the order.
    Possible values are CONFIRMATION_STATUS_NOTCONFIRMED and CONFIRMATION_STATUS_CONFIRMED.
    */
  public confirmationStatus: EnumValue<number>;

  /**
   * The name of the user who has created the order. If an agent user has created the order, the agent user's name is returned. Otherwise "Customer" is returned.
   */
  public readonly createdBy: string;

  /**
   * The current order. The current order represents the most recent order in a chain of orders. For example, if Order1 was replaced by Order2, Order2 is the current representation of the order and Order1 is the original representation of the order. If you replace Order2 with Order3, Order 3 is now the current order and Order1 is still the original representation of the order. If this order has not been replaced, this method returns this order because this order is the current order.
   */
  public readonly currentOrder: Order;

  /**
   * The order number of the current order. The current order represents the most recent order in a chain of orders. For example, if Order1 was replaced by Order2, Order2 is the current representation of the order and Order1 is the original representation of the order. If you replace Order2 with Order3, Order 3 is now the current order and Order1 is still the original representation of the order. If this order has not been replaced, calling this method returns the same value as the getOrderNo() method because this order is the current order.
   */
  public readonly currentOrderNo: string;

  /**
   * The ID of the locale that was in effect when the order was placed. This is the customer's locale.
   */
  public readonly customerLocaleID: string;

  /**
   * The customer-specific reference information for the order, or null.
   */
  public customerOrderReference: string | null;

  /**
   * A date after which an order can be exported.
   */
  public exportAfter: Date;

  /**
     * The export status of the order.
    Possible values are: EXPORT_STATUS_NOTEXPORTED, EXPORT_STATUS_EXPORTED, EXPORT_STATUS_READY, and EXPORT_STATUS_FAILED.
    */
  public exportStatus: EnumValue<number>;

  /**
   * The value of an external order number associated with this order, or null.
   */
  public externalOrderNo: string | null;

  /**
   * The status of an external order associated with this order, or null.
   */
  public externalOrderStatus: string | null;

  /**
   * The text describing the external order, or null.
   */
  public externalOrderText: string | null;

  /**
   * Returns true, if the order is imported and false otherwise.
   */
  public readonly imported: boolean;

  /**
   * The collection of InvoiceItems associated with this order.
   */
  public readonly invoiceItems: FilteringCollection<InvoiceItem>;

  /**
   * The invoice number for this Order.
   */
  public invoiceNo: string;

  /**
   * The collection of Invoices associated with this order.
   */
  public readonly invoices: FilteringCollection<Invoice>;

  /**
   * The order number for this order.
   */
  public readonly orderNo: string;

  /**
   * The URL safe token for this order.
   */
  public readonly orderToken: string;

  /**
   * The original order associated with this order. The original order represents an order that was the first ancestor in a chain of orders. For example, if Order1 was replaced by Order2, Order2 is the current representation of the order and Order1 is the original representation of the order. If you replace Order2 with Order3, Order1 is still the original representation of the order. If this order is the first ancestor, this method returns this order.
   */
  public readonly originalOrder: Order;

  /**
   * The order number of the original order associated with this order. The original order represents an order that was the first ancestor in a chain of orders. For example, if Order1 was replaced by Order2, Order2 is the current representation of the order and Order1 is the original representation of the order. If you replace Order2 with Order3, Order1 is still the original representation of the order. If this order is the first ancestor, this method returns the value of getOrderNo().
   */
  public readonly originalOrderNo: string;

  /**
     * The order payment status value.
    Possible values are PAYMENT_STATUS_NOTPAID, PAYMENT_STATUS_PARTPAID or PAYMENT_STATUS_PAID.
    */
  public paymentStatus: EnumValue<number>;

  /**
   * The sum of the refunded amounts. The refunded amounts are calculated on the fly. Associate a payment refund for an PaymentInstrument with an Invoice using Invoice.addRefundTransaction(OrderPaymentInstrument, Money).
   */
  public readonly refundedAmount: Money;

  /**
     * The IP address of the remote host from which the order was created.

    If the IP address was not captured for the order because order IP logging was disabled at the time the order was created, null will be returned.
    */
  public readonly remoteHost: string | null;

  /**
   * If this order was replaced by another order, returns the value of the replace code. Otherwise. returns null.
   */
  public replaceCode: EnumValue<number> | null;

  /**
   * If this order was replaced by another order, returns the value of the replace description. Otherwise returns null.
   */
  public replaceDescription: string | null;

  /**
   * The order that this order replaced or null. For example, if you have three orders where Order1 was replaced by Order2 and Order2 was replaced by Order3, calling this method on Order3 will return Order2. Similarly, calling this method on Order1 will return null as Order1 was the original order.
   */
  public readonly replacedOrder: Order | null;

  /**
   * The order number that this order replaced or null if this order did not replace an order. For example, if you have three orders where Order1 was replaced by Order2 and Order2 was replaced by Order3, calling this method on Order3 will return the order number for Order2. Similarly, calling this method on Order1 will return null as Order1 was the original order.
   */
  public readonly replacedOrderNo: string | null;

  /**
   * The order that replaced this order, or null.
   */
  public readonly replacementOrder: Order | null;

  /**
   * If this order was replaced by another order, returns the order number that replaced this order. Otherwise returns null.
   */
  public readonly replacementOrderNo: string | null;

  /**
   * The collection of ReturnCaseItems associated with this order.
   */
  public readonly returnCaseItems: FilteringCollection<ReturnCaseItem>;

  /**
   * The collection of ReturnCases associated with this order.
   */
  public readonly returnCases: FilteringCollection<ReturnCase>;

  /**
   * The collection of ReturnItems associated with this order.
   */
  public readonly returnItems: FilteringCollection<ReturnItem>;

  /**
   * The collection of Returns associated with this order.
   */
  public readonly returns: FilteringCollection<Return>;

  /**
   * The collection of ShippingOrderItems associated with this order.
   */
  public readonly shippingOrderItems: FilteringCollection<ShippingOrderItem>;

  /**
   * The collection of ShippingOrders associated with this order.
   */
  public readonly shippingOrders: FilteringCollection<ShippingOrder>;

  /**
     * The order shipping status.
    Possible values are SHIPPING_STATUS_NOTSHIPPED, SHIPPING_STATUS_PARTSHIPPED or SHIPPING_STATUS_SHIPPED.
    */
  public shippingStatus: EnumValue<number>;

  /**
   * The source code stored with the order or null if no source code is attached to the order.
   */
  public readonly sourceCode: string | null;

  /**
   * The source code group attached to the order or null if no source code group is attached to the order.
   */
  public readonly sourceCodeGroup: SourceCodeGroup | null;

  /**
   * The source code group id stored with the order or null if no source code group is attached to the order.
   */
  public readonly sourceCodeGroupID: string;

  /**
     * The status of the order.
    Possible values are ORDER_STATUS_CREATED, ORDER_STATUS_NEW, ORDER_STATUS_OPEN, ORDER_STATUS_COMPLETED, ORDER_STATUS_CANCELLED, ORDER_STATUS_FAILED or ORDER_STATUS_REPLACED.
    */
  public status: EnumValue<number>;
  private constructor();

  /**
   * Returns an unsorted collection of the payment instruments in this container.
   */
  public getPaymentInstruments(): Collection<OrderPaymentInstrument>;

  /**
   * Returns an unsorted collection of PaymentInstrument instances based on the specified payment method ID.
   * @param paymentMethodID
   */
  public getPaymentInstruments(paymentMethodID: string): Collection<OrderPaymentInstrument>;

  /**
   * Creates a new Appeasement associated with this order.
   * @param appeasementnumber
   */
  public createAppeasement(appeasementnumber: string): Appeasement;

  /**
   * Creates a new Appeasement associated with this order.
   */
  public createAppeasement(): Appeasement;

  /**
   * Creates a new ReturnCase associated with this order specifying whether the ReturnCase is an RMA (return merchandise authorization).
   * @param returnCasenumber
   * @param isRMA
   */
  public createReturnCase(returnCasenumber: string, isRMA: boolean): ReturnCase;

  /**
   * Creates a new ReturnCase associated with this order specifying whether the ReturnCase is an RMA (return merchandise authorization).
   * @param isRMA
   */
  public createReturnCase(isRMA: boolean): ReturnCase;

  /**
   * Returns the order item with the given status which wraps a new service item which is created and added to the order.
   * @param ID
   * @param status
   */
  public createServiceItem(ID: string, status: string): OrderItem;

  /**
   * Creates a new ShippingOrder for this order.
   */
  public createShippingOrder(): ShippingOrder;

  /**
   * Creates a new ShippingOrder for this order.
   * @param shippingOrdernumber
   */
  public createShippingOrder(shippingOrdernumber: string): ShippingOrder;

  /**
   * Returns the affiliate partner ID value, or null.
   */
  public getAffiliatePartnerID(): string;

  /**
   * Returns the affiliate partner name value, or null.
   */
  public getAffiliatePartnerName(): string;

  /**
   * Returns the Appeasement associated with this order with the given appeasementnumber.
   * @param appeasementnumber
   */
  public getAppeasement(appeasementnumber: string): Appeasement;

  /**
   * Returns the AppeasementItem associated with this Order with the given appeasementItemID.
   * @param appeasementItemID
   */
  public getAppeasementItem(appeasementItemID: string): AppeasementItem;

  /**
   * Returns the collection of AppeasementItems associated with this order.
   */
  public getAppeasementItems(): FilteringCollection<AppeasementItem>;

  /**
   * Returns the collection of Appeasements associated with this order.
   */
  public getAppeasements(): FilteringCollection<Appeasement>;

  /**
   * If this order was cancelled, returns the value of the cancel code or null.
   */
  public getCancelCode(): EnumValue<number> | null;

  /**
   * If this order was cancelled, returns the text describing why the order was cancelled or null.
   */
  public getCancelDescription(): string | null;

  /**
   * Returns the sum of the captured amounts.
   */
  public getCapturedAmount(): Money;

  /**
   * Returns the confirmation status of the order.
   * Possible values are CONFIRMATION_STATUS_NOTCONFIRMED and CONFIRMATION_STATUS_CONFIRMED.
   */
  public getConfirmationStatus(): EnumValue<number>;

  /**
   * Returns the name of the user who has created the order.
   */
  public getCreatedBy(): string;

  /**
   * Returns the current order.
   */
  public getCurrentOrder(): Order;

  /**
   * Returns the order number of the current order.
   */
  public getCurrentOrderNo(): string;

  /**
   * Returns the ID of the locale that was in effect when the order was placed.
   */
  public getCustomerLocaleID(): string;

  /**
   * Returns the customer-specific reference information for the order, or null.
   */
  public getCustomerOrderReference(): string | null;

  /**
   * Returns a date after which an order can be exported.
   */
  public getExportAfter(): Date;

  /**
   * Returns the export status of the order.
   * Possible values are: EXPORT_STATUS_NOTEXPORTED, EXPORT_STATUS_EXPORTED, EXPORT_STATUS_READY, and EXPORT_STATUS_FAILED.
   */
  public getExportStatus(): EnumValue<number>;

  /**
   * Returns the value of an external order number associated with this order, or null.
   */
  public getExternalOrderNo(): string | null;

  /**
   * Returns the status of an external order associated with this order, or null.
   */
  public getExternalOrderStatus(): string | null;

  /**
   * Returns the text describing the external order, or null.
   */
  public getExternalOrderText(): string | null;

  /**
   * Returns the Invoice associated with this order with the given invoicenumber.
   * @param invoicenumber
   */
  public getInvoice(invoicenumber: string): Invoice;

  /**
   * Returns the InvoiceItem associated with this order with the given ID.
   * @param invoiceItemID
   */
  public getInvoiceItem(invoiceItemID: string): InvoiceItem | null;

  /**
   * Returns the collection of InvoiceItems associated with this order.
   */
  public getInvoiceItems(): FilteringCollection<InvoiceItem>;

  /**
   * Returns the invoice number for this Order.
   */
  public getInvoiceNo(): string;

  /**
   * Returns the collection of Invoices associated with this order.
   */
  public getInvoices(): FilteringCollection<Invoice>;

  /**
   * Returns the order export XML as string object.
   * @param encryptionMethod
   * @param encryptionKey
   * @param encryptUsingEKID
   *
   * @deprecated
   */
  public getOrderExportXML(encryptionMethod: string, encryptionKey: string, encryptUsingEKID: boolean): string;

  /**
   * Returns the order export XML as String object.
   *
   * Example:
   * ```
   * var orderXmlAsString = order.getOrderExportXML(null, null);
   * var orderXml = new XML(orderXmlAsString);
   * ```
   * This method can be called for placed orders only, otherwise an exception will be thrown.
   * Also, an exception will be thrown if the method is called in a transaction with changes.
   * @param encryptionMethod
   * @param encryptionKey
   */
  public getOrderExportXML(encryptionMethod: string, encryptionKey: string): string;

  /**
   * Returns the OrderItem for the itemID.
   * @param itemID
   */
  public getOrderItem(itemID: string): OrderItem | null;

  /**
   * Returns the order number for this order.
   */
  public getOrderNo(): string;

  /**
   * Returns the URL safe token for this order.
   */
  public getOrderToken(): string;

  /**
   * Returns the original order associated with this order.
   */
  public getOriginalOrder(): Order;

  /**
   * Returns the order number of the original order associated with this order.
   */
  public getOriginalOrderNo(): string;

  /**
     * Returns the order payment status value.
    Possible values are PAYMENT_STATUS_NOTPAID, PAYMENT_STATUS_PARTPAID or PAYMENT_STATUS_PAID.
    */
  public getPaymentStatus(): EnumValue<number>;

  /**
   * Returns the sum of the refunded amounts.
   */
  public getRefundedAmount(): Money;

  /**
   * Returns the IP address of the remote host from which the order was created.
   */
  public getRemoteHost(): string;

  /**
   * If this order was replaced by another order, returns the value of the replace code.
   */
  public getReplaceCode(): EnumValue<number>;

  /**
   * If this order was replaced by another order, returns the value of the replace description.
   */
  public getReplaceDescription(): string;

  /**
   * Returns the order that this order replaced or null.
   */
  public getReplacedOrder(): Order | null;

  /**
   * Returns the order number that this order replaced or null if this order did not replace an order.
   */
  public getReplacedOrderNo(): string;

  /**
   * Returns the order that replaced this order, or null.
   */
  public getReplacementOrder(): Order;

  /**
   * If this order was replaced by another order, returns the order number that replaced this order.
   */
  public getReplacementOrderNo(): string;

  /**
   * Returns the Return associated with this order with the given returnnumber.
   * @param returnnumber
   */
  public getReturn(returnnumber: string): Return;

  /**
   * Returns the ReturnCase associated with this order with the given returnCasenumber.
   * @param returnCasenumber
   */
  public getReturnCase(returnCasenumber: string): ReturnCase;

  /**
   * Returns the ReturnCaseItem associated with this order with the given returnCaseItemID.
   * @param returnCaseItemID
   */
  public getReturnCaseItem(returnCaseItemID: string): ReturnCaseItem;

  /**
   * Returns the collection of ReturnCaseItems associated with this order.
   */
  public getReturnCaseItems(): FilteringCollection<ReturnCaseItem>;

  /**
   * Returns the collection of ReturnCases associated with this order.
   */
  public getReturnCases(): FilteringCollection<ReturnCase>;

  /**
   * Returns the ReturnItem associated with this order with the given ID.
   * @param returnItemID
   */
  public getReturnItem(returnItemID: string): ReturnItem;

  /**
   * Returns the collection of ReturnItems associated with this order.
   */
  public getReturnItems(): FilteringCollection<ReturnItem>;

  /**
   * Returns the collection of Returns associated with this order.
   */
  public getReturns(): FilteringCollection<Return>;

  /**
   * Returns the ShippingOrder associated with this order with the given shippingOrdernumber.
   * @param shippingOrdernumber
   */
  public getShippingOrder(shippingOrdernumber: string): ShippingOrder;

  /**
   * Returns the ShippingOrderItem associated with this order with the given shippingOrderItemID.
   * @param shippingOrderItemID
   */
  public getShippingOrderItem(shippingOrderItemID: string): ShippingOrderItem;

  /**
   * Returns the collection of ShippingOrderItems associated with this order.
   */
  public getShippingOrderItems(): FilteringCollection<ShippingOrderItem>;

  /**
   * Returns the collection of ShippingOrders associated with this order.
   */
  public getShippingOrders(): FilteringCollection<ShippingOrder>;

  /**
     * Returns the order shipping status.
    Possible values are SHIPPING_STATUS_NOTSHIPPED, SHIPPING_STATUS_PARTSHIPPED or SHIPPING_STATUS_SHIPPED.
    */
  public getShippingStatus(): EnumValue<number>;

  /**
   * Returns the source code stored with the order or null if no source code is attached to the order.
   */
  public getSourceCode(): string;

  /**
   * Returns the source code group attached to the order or null if no source code group is attached to the order.
   */
  public getSourceCodeGroup(): SourceCodeGroup | null;

  /**
   * Returns the source code group id stored with the order or null if no source code group is attached to the order.
   */
  public getSourceCodeGroupID(): string | null;

  /**
     * Returns the status of the order.
    Possible values are ORDER_STATUS_CREATED, ORDER_STATUS_NEW, ORDER_STATUS_OPEN, ORDER_STATUS_COMPLETED, ORDER_STATUS_CANCELLED, ORDER_STATUS_FAILED or ORDER_STATUS_REPLACED.
    */
  public getStatus(): EnumValue<number>;

  /**
   * Returns true, if the order is imported and false otherwise.
   */
  public isImported(): boolean;

  /**
   * Ensures that the order is authorized.
   */
  public reauthorize(): Status;

  /**
   * Sets the affiliate partner ID value.
   * @param affiliatePartnerID
   */
  public setAffiliatePartnerID(affiliatePartnerID: string): void;

  /**
   * Sets the affiliate partner name value.
   * @param affiliatePartnerName
   */
  public setAffiliatePartnerName(affiliatePartnerName: string): void;

  /**
   * Sets the cancel code value.
   * @param cancelCode
   */
  public setCancelCode(cancelCode: string): void;

  /**
   * Sets the description as to why the order was cancelled.
   * @param cancelDescription
   */
  public setCancelDescription(cancelDescription: string): void;

  /**
     * Sets the confirmation status value.
    Possible values are CONFIRMATION_STATUS_NOTCONFIRMED or CONFIRMATION_STATUS_CONFIRMED.
    * @param status
    */
  public setConfirmationStatus(status: number): void;

  /**
   * This method is used to associate the order object with the specified customer object.
   * @param customer
   */
  public setCustomer(customer: Customer): void;

  /**
   * Sets the customer-specific reference information for the order.
   * @param reference
   */
  public setCustomerOrderReference(reference: string): void;

  /**
   * Sets the date after which an order can be exported.
   * @param date
   */
  public setExportAfter(date: Date): void;

  /**
   * Sets the export status of the order.
   *
   * Possible values are: EXPORT_STATUS_NOTEXPORTED, EXPORT_STATUS_EXPORTED, EXPORT_STATUS_READY, and EXPORT_STATUS_FAILED.
   * @param status
   */
  public setExportStatus(status: number): void;

  /**
   * Sets the value of an external order number associated with this order
   * @param externalOrderNo
   */
  public setExternalOrderNo(externalOrderNo: string): void;

  /**
   * Sets the status of an external order associated with this order
   * @param status
   */
  public setExternalOrderStatus(status: string): void;

  /**
   * Sets the text describing the external order.
   * @param text
   */
  public setExternalOrderText(text: string): void;

  /**
   * Sets the invoice number for this Order.
   * @param aValue
   */
  public setInvoiceNo(aValue: string): void;

  /**
     * Sets the order payment status.
    Possible values are PAYMENT_STATUS_NOTPAID, PAYMENT_STATUS_PARTPAID or PAYMENT_STATUS_PAID.
    * @param status
    */
  public setPaymentStatus(status: number): void;

  /**
   * Sets the value of the replace code.
   * @param replaceCode
   */
  public setReplaceCode(replaceCode: string): void;

  /**
   * Sets the value of the replace description.
   * @param replaceDescription
   */
  public setReplaceDescription(replaceDescription: string): void;

  /**
     * Sets the order shipping status value.
    Possible values are SHIPPING_STATUS_NOTSHIPPED, SHIPPING_STATUS_PARTSHIPPED or SHIPPING_STATUS_SHIPPED.
    * @param status
    */
  public setShippingStatus(status: number): void;

  /**
   * Sets the status of the order.
   * @param status
   */
  public setStatus(status: number): void;

  /**
   * Tracks an order change.
   * @param text
   */
  public trackOrderChange(text: string): Note;
}

export = Order;
