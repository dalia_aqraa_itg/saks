import Collection = require('../util/Collection');
import EnumValue = require('../value/EnumValue');
import Money = require('../value/Money');
import Quantity = require('../value/Quantity');
import AbstractItem = require('./AbstractItem');
import ReturnItem = require('./ReturnItem');

declare class ReturnCaseItem extends AbstractItem {
  public static readonly STATUS_CANCELLED: string;
  public static readonly STATUS_CONFIRMED: string;
  public static readonly STATUS_NEW: string;
  public static readonly STATUS_PARTIAL_RETURNED: string;
  public static readonly STATUS_RETURNED: string;

  /**
   * Return the Quantity authorized for this ReturnCaseItem, may be N/A.
   */
  public readonly authorizedQuantity: Quantity;

  /**
   * Price of a single unit before discount application.
   */
  public readonly basePrice: Money;

  /**
   * Return the note for this return case item.
   */
  public readonly note: string;

  /**
   * Returns null or the parent item.
   */
  public readonly parentItem: ReturnCaseItem | null;

  /**
   * The reason code for return case item.
   */
  public readonly reasonCode: EnumValue<string>;

  /**
   * Mandatory number of ReturnCase to which this item belongs
   */
  public readonly returnCaseNumber: string;

  /**
   * Unsorted collection of ReturnItems associated with this ReturnCaseItem.
   */
  public readonly returnItems: Collection<ReturnCaseItem>;

  /**
     * Gets the return case item status.

        The possible values are STATUS_NEW,STATUS_CONFIRMED, STATUS_PARTIAL_RETURNED, STATUS_RETURNED, STATUS_CANCELLED. 
    */
  public readonly status: EnumValue<string>;
  private constructor();

  /**
   * Create a new ReturnItem for this ReturnCaseItem and assign it to the given Return.
   * @param returnNumber 	- number of Return to which new item is assigned.
   */
  public createReturnItem(returnNumber: string): ReturnItem;

  /**
   * Return the Quantity authorized for this ReturnCaseItem, may be N/A.
   */
  public getAuthorizedQuantity(): Quantity;

  /**
   * Price of a single unit before discount application.
   */
  public getBasePrice(): Money;

  /**
   * Return the note for this return case item.
   */
  public getNote(): string;

  /**
   * Returns null or the parent item.
   */
  public getParentItem(): ReturnCaseItem | null;

  /**
   * Returns the reason code for return case item.
   */
  public getReasonCode(): EnumValue<string>;

  /**
   * Mandatory number of ReturnCase to which this item belongs
   */
  public getReturnCaseNumber(): string;

  /**
   * Unsorted collection of ReturnItems associated with this ReturnCaseItem.
   */
  public getReturnItems(): Collection<ReturnItem>;

  /**
   * Gets the return case item status.
   */
  public getStatus(): EnumValue<string>;

  /**
   * Set the optional authorized Quantity for this item.
   * @param authorizedQuantity - null or the quantity
   */
  public setAuthorizedQuantity(authorizedQuantity: Quantity | null): void;

  /**
   * Sets a note for this return case item.
   * @param note 	- the note for this return case item to set
   */
  public setNote(note: string): void;

  /**
   * Set a parent item.
   * @param parentItem - The parent item, null is allowed
   */
  public setParentItem(parentItem: ReturnCaseItem | null): void;

  /**
   * Changes the reason code.
   * @param reasonCode  - the reason code to set
   */
  public setReasonCode(reasonCode: string): void;

  /**
   * Sets the status.
   * @param statusstring  statusString - the status
   * @throws NullPointerException - if status is null
   * @throws IllegalArgumentException - if the status transition to the status is not allowed
   */
  public setStatus(statusstring: string): void;
}

export = ReturnCaseItem;
