import Product = require('../catalog/Product');
import Collection = require('../util/Collection');
import Money = require('../value/Money');
import LineItemCtnr = require('./LineItemCtnr');
import ProductShippingModel = require('./ProductShippingModel');
import Shipment = require('./Shipment');
import ShipmentShippingModel = require('./ShipmentShippingModel');
import ShippingMethod = require('./ShippingMethod');

/**
 * Provides methods to access the shipping information.
 */
declare class ShippingMgr {
  /**
   * Applies product and shipment-level shipping cost to the specified line item container.
   * @param lineItemCtnr
   */
  public static applyShippingCost(lineItemCtnr: LineItemCtnr<any>): void;

  /**
   * Returns the active shipping methods of the current site applicable to the session currency and current customer group.
   */
  public static getAllShippingMethods(): Collection<ShippingMethod>;

  /**
   * Returns the default shipping method of the current site applicable to the session currency.
   */
  public static getDefaultShippingMethod(): ShippingMethod;

  /**
   * Returns the shipping model for the specified product.
   * @param product
   */
  public static getProductShippingModel(product: Product): ProductShippingModel;

  /**
   * Returns the shipping model for the specified shipment.
   * @param shipment
   */
  public static getShipmentShippingModel(shipment: Shipment): ShipmentShippingModel;

  /**
   * Returns the shipping cost amount for the specified shipping method and the specified order value.
   * @param shippingMethod
   * @param orderValue
   */
  public static getShippingCost(shippingMethod: ShippingMethod, orderValue: Money): Money;

  /**
   * The active shipping methods of the current site applicable to the session currency and current customer group.
   */
  public readonly allShippingMethods: Collection<ShippingMethod>;

  /**
   * The default shipping method of the current site applicable to the session currency. Does an additional check if there is a base method and if their currencies are the same. Returns NULL if the two currencies are different.
   */
  public readonly defaultShippingMethod: ShippingMethod;
  private constructor();
}

export = ShippingMgr;
