import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import EnumValue = require('../value/EnumValue');
import Money = require('../value/Money');
import OrderPaymentInstrument = require('./OrderPaymentInstrument');
import PaymentProcessor = require('./PaymentProcessor');

declare global {
  namespace ICustomAttributes {
    interface PaymentTransaction extends CustomAttributes {}
  }
}

declare class PaymentTransaction extends ExtensibleObject<ICustomAttributes.PaymentTransaction> {
  public static readonly TYPE_AUTH: string;
  public static readonly TYPE_AUTH_REVERSAL: string;
  public static readonly TYPE_CAPTURE: string;
  public static readonly TYPE_CREDIT: string;

  /**
   * The amount of the transaction
   */
  public amount: Money;

  /**
   * The payment instrument related to this payment transaction.
   */
  public readonly paymentInstrument: OrderPaymentInstrument;

  /**
   * The payment processor related to this payment transaction.
   */
  public paymentProcessor: PaymentProcessor;

  /**
   * The payment service-specific transaction id.
   */
  public transactionID: string;

  /**
   * The value of the transaction type where the value is one of TYPE_AUTH, TYPE_AUTH_REVERSAL, TYPE_CAPTURE or TYPE_CREDIT.
   */
  public type: EnumValue<string>;

  private constructor();

  /**
   * Returns the amount of the transaction.
   */
  public getAmount(): Money;

  /**
   * Returns the payment instrument related to this payment transaction.
   */
  public getPaymentInstrument(): OrderPaymentInstrument;

  /**
   * Returns the payment processor related to this payment transaction.
   */
  public getPaymentProcessor(): PaymentProcessor;

  /**
   * Returns the payment service-specific transaction id.
   */
  public getTransactionID(): string;

  /**
   * Returns the value of the transaction type where the value is one of TYPE_AUTH, TYPE_AUTH_REVERSAL, TYPE_CAPTURE or TYPE_CREDIT.
   */
  public getType(): EnumValue<string>;

  /**
   * Sets the amount of the transaction.
   * @param amount
   */
  public setAmount(amount: Money): void;

  /**
   * Sets the payment processor related to this payment transaction.
   * @param paymentProcessor
   */
  public setPaymentProcessor(paymentProcessor: PaymentProcessor): void;

  /**
   * Sets the payment service-specific transaction id.
   * @param transactionID
   */
  public setTransactionID(transactionID: string): void;

  /**
   *
   * @param type Sets the value of the transaction type where permissible values are TYPE_AUTH, TYPE_AUTH_REVERSAL, TYPE_CAPTURE or TYPE_CREDIT.
   */
  public setType(type: string): void;
}

export = PaymentTransaction;
