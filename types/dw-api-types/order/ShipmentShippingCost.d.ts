import Money = require('../value/Money');

/**
 * Represents shipping cost applied to shipments. 
Returned by ShipmentShippingModel.getShippingCost(ShippingMethod).
 */
declare class ShipmentShippingCost {
  /**
   * The shipping amount.
   */
  public readonly amount: Money;

  private constructor();
  /**
   * Returns the shipping amount.
   */
  public getAmount(): Money;
}

export = ShipmentShippingCost;
