import Discount = require('../campaign/Discount');
import Product = require('../catalog/Product');
import ProductOptionModel = require('../catalog/ProductOptionModel');
import Customer = require('../customer/Customer');
import ProductListItem = require('../customer/ProductListItem');
import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Note = require('../object/Note');
import Status = require('../system/Status');
import Collection = require('../util/Collection');
import HashMap = require('../util/HashMap');
import List = require('../util/List');
import EnumValue = require('../value/EnumValue');
import Money = require('../value/Money');
import Quantity = require('../value/Quantity');
import BonusDiscountLineItem = require('./BonusDiscountLineItem');
import CouponLineItem = require('./CouponLineItem');
import GiftCertificateLineItem = require('./GiftCertificateLineItem');
import LineItem = require('./LineItem');
import OrderAddress = require('./OrderAddress');
import OrderPaymentInstrument = require('./OrderPaymentInstrument');
import PaymentInstrument = require('./PaymentInstrument');
import PriceAdjustment = require('./PriceAdjustment');
import ProductLineItem = require('./ProductLineItem');
import Shipment = require('./Shipment');

declare class LineItemCtnr<T extends CustomAttributes> extends ExtensibleObject<T> {
  /**
   * constant for Business Type B2B
   */
  public static readonly BUSINESS_TYPE_B2B: number;
  /**
   * constant for Business Type B2C
   */
  public static readonly BUSINESS_TYPE_B2C: number;
  /**
   * constant for Channel Type CallCenter
   */
  public static readonly CHANNEL_TYPE_CALLCENTER: number;
  /**
   * constant for Channel Type DSS
   */
  public static readonly CHANNEL_TYPE_DSS: number;
  /**
   * constant for Channel Type Facebook Ads
   */
  public static readonly CHANNEL_TYPE_FACEBOOKADS: number;
  /**
   * constant for Channel Type Marketplace
   */
  public static readonly CHANNEL_TYPE_MARKETPLACE: number;
  /**
   * constant for Channel Type Online Reservation
   */
  public static readonly CHANNEL_TYPE_ONLINERESERVATION: number;
  /**
   * constant for Channel Type Pinterest
   */
  public static readonly CHANNEL_TYPE_PINTEREST: number;
  /**
   * constant for Channel Type Store
   */
  public static readonly CHANNEL_TYPE_STORE: number;
  /**
   * constant for Channel Type Storefront
   */
  public static readonly CHANNEL_TYPE_STOREFRONT: number;
  /**
   * constant for Channel Type Subscriptions
   */
  public static readonly CHANNEL_TYPE_SUBSCRIPTIONS: number;
  /**
   * constant for Channel Type Twitter
   */
  public static readonly CHANNEL_TYPE_TWITTER: number;

  /**
   * The adjusted total gross price (including tax) in purchase currency. Adjusted merchandize prices represent the sum of product prices before services such as shipping, but after product-level and order-level adjustments.
   */
  public readonly adjustedMerchandizeTotalGrossPrice: Money;

  /**
   * The total net price (excluding tax) in purchase currency. Adjusted merchandize prices represent the sum of product prices before services such as shipping, but after product-level and order-level adjustments.
   */
  public readonly adjustedMerchandizeTotalNetPrice: Money;

  /**
   * The adjusted merchandize total price including product-level and order-level adjustments. If the line item container is based on net pricing the adjusted merchandize total net price is returned. If the line item container is based on gross pricing the adjusted merchandize total gross price is returned.
   */
  public readonly adjustedMerchandizeTotalPrice: Money;

  /**
   * The subtotal tax in purchase currency. Adjusted merchandize prices represent the sum of product prices before services such as shipping have been added, but after adjustment from promotions have been added.
   */
  public readonly adjustedMerchandizeTotalTax: Money;

  /**
   * The adjusted sum of all shipping line items of the line item container, including tax after shipping adjustments have been applied.
   */

  public readonly adjustedShippingTotalGrossPrice: Money;

  /**
   * The sum of all shipping line items of the line item container, excluding tax after shipping adjustments have been applied.
   */
  public readonly adjustedShippingTotalNetPrice: Money;

  /**
   * The adjusted shipping total price. If the line item container is based on net pricing the adjusted shipping total net price is returned. If the line item container is based on gross pricing the adjusted shipping total gross price is returned.
   */
  public readonly adjustedShippingTotalPrice: Money;

  /**
   * The tax of all shipping line items of the line item container after shipping adjustments have been applied.
   */
  public readonly adjustedShippingTotalTax: Money;

  /**
   * All product, shipping, price adjustment, and gift certificate line items of the line item container.
   */
  public readonly allLineItems: Collection<LineItem>;

  /**
   * All product line items of the container, no matter if they are dependent or independent. This includes option, bundled and bonus line items.
   */
  public readonly allProductLineItems: Collection<ProductLineItem>;

  /**
   * A hash mapping all products in the line item container to their total quantities. The total product quantity is used chiefly to validate the availability of the items in the cart. This method is not appropriate to look up prices because it returns products such as bundled line items which are included in the price of their parent and therefore have no corresponding price.
   *
   * The method counts all direct product line items, plus dependent product line items that are not option line items. It also excludes product line items that are not associated to any catalog product.
   */
  public readonly allProductQuantities: HashMap<Product, Quantity>;

  /**
   * The collection of all shipping price adjustments applied somewhere in the container. This can be adjustments applied to individual shipments or to the container itself. Note that the promotions engine only applies shipping price adjustments to the the default shipping line item of shipments, and never to the container.
   */
  public readonly allShippingPriceAdjustments: Collection<PriceAdjustment>;

  /**
   * The billing address defined for the container. Returns null if no billing address has been created yet.
   */
  public readonly billingAddress: OrderAddress | null;

  /**
   * An unsorted collection of the the bonus discount line items associated with this container.
   */
  public readonly bonusDiscountLineItems: Collection<BonusDiscountLineItem>;

  /**
   * The collection of product line items that are bonus items (where ProductLineItem.isBonusProductLineItem() is true).
   */
  public readonly bonusLineItems: Collection<ProductLineItem>;

  /**
     * The type of the business this order has been placed in.
    Possible values are BUSINESS_TYPE_B2C or BUSINESS_TYPE_B2B.
    */
  public readonly businessType: EnumValue<number>;

  /**
     * The channel type defines in which sales channel this order has been created. This can be used to distinguish order placed through Storefront, Call Center or Marketplace.
    Possible values are CHANNEL_TYPE_STOREFRONT, CHANNEL_TYPE_CALLCENTER, CHANNEL_TYPE_MARKETPLACE, CHANNEL_TYPE_DSS, CHANNEL_TYPE_STORE, CHANNEL_TYPE_PINTEREST, CHANNEL_TYPE_TWITTER, CHANNEL_TYPE_FACEBOOKADS, CHANNEL_TYPE_SUBSCRIPTIONS or CHANNEL_TYPE_ONLINERESERVATION.
    */
  public readonly channelType: EnumValue<number>;

  /**
   * A sorted collection of the coupon line items in the container. The coupon line items are returned in the order they were added to container.
   */
  public readonly couponLineItems: Collection<CouponLineItem>;

  /**
   * The currency code for this line item container. The currency code is a 3-character currency mnemonic such as 'USD' or 'EUR'. The currency code represents the currency in which the calculation is made, and in which the buyer sees all prices in the store front.
   */
  public readonly currencyCode: string;

  /**
   * The customer associated with this container.
   */
  public readonly customer: Customer;

  /**
   * The email of the customer associated with this container.
   */
  public readonly customerEmail: string;

  /**
   * The name of the customer associated with this container.
   */
  public readonly customerName: string;

  /**
   * The customer number of the customer associated with this container.
   */
  public readonly customerNo: string;

  /**
   * The default shipment of the line item container.
   */
  public readonly defaultShipment: Shipment;

  /**
   * The Etag of the line item container. The Etag is a hash that represents the overall container state including any associated objects like line items.
   */
  public readonly etag: string;

  /**
   * All gift certificate line items of the container.
   */
  public readonly giftCertificateLineItems: Collection<GiftCertificateLineItem>;

  /**
   * An unsorted collection of the PaymentInstrument instances that represent GiftCertificates in this container.
   */
  public readonly giftCertificatePaymentInstruments: Collection<PaymentInstrument>;

  /**
   * The total gross price of all gift certificates in the cart. Should usually be equal to total net price.
   */
  public readonly giftCertificateTotalGrossPrice: Money;

  /**
   * The total net price (excluding tax) of all gift certificates in the cart. Should usually be equal to total gross price.
   */
  public readonly giftCertificateTotalNetPrice: Money;

  /**
   * The gift certificate total price. If the line item container is based on net pricing the gift certificate total net price is returned. If the line item container is based on gross pricing the gift certificate total gross price is returned.
   */
  public readonly giftCertificateTotalPrice: Money;

  /**
   * The total tax of all gift certificates in the cart. Should usually be 0.0.
   */
  public readonly giftCertificateTotalTax: Money;

  /**
   * The total gross price (including tax) in purchase currency. Merchandize total prices represent the sum of product prices before services such as shipping or adjustment from promotions have been added.
   */
  public readonly merchandizeTotalGrossPrice: Money;

  /**
   * The total net price (excluding tax) in purchase currency. Merchandize total prices represent the sum of product prices before services such as shipping or adjustment from promotion have been added.
   */
  public readonly merchandizeTotalNetPrice: Money;

  /**
   * The merchandize total price. If the line item container is based on net pricing the merchandize total net price is returned. If the line item container is based on gross pricing the merchandize total gross price is returned.
   */
  public readonly merchandizeTotalPrice: Money;

  /**
   * The total tax in purchase currency. Merchandize total prices represent the sum of product prices before services such as shipping or adjustment from promotions have been added.
   */
  public readonly merchandizeTotalTax: Money;

  /**
   * The list of notes for this object, ordered by creation time from oldest to newest.
   */
  public readonly notes: List<Note>;

  /**
   * An unsorted collection of the payment instruments in this container.
   */
  public readonly paymentInstruments: Collection<OrderPaymentInstrument>;

  /**
   * The collection of price adjustments that have been applied to the totals such as promotion on the purchase value (i.e. $10 Off or 10% Off). The price adjustments are sorted by the order in which they were applied to the order by the promotions engine.
   */
  public readonly priceAdjustments: Collection<PriceAdjustment>;

  /**
   * The product line items of the container that are not dependent on other product line items. This includes line items representing bonus products in the container but excludes option, bundled, and bonus line items. The returned collection is sorted by the position attribute of the product line items.
   */
  public readonly productLineItems: Collection<ProductLineItem>;

  /**
     * A hash map of all products in the line item container and their total quantities. The total product quantity is for example used to lookup the product price.

    The method counts all direct product line items, plus dependent product line items that are not bundled line items and no option line items. It also excludes product line items that are not associated to any catalog product, and bonus product line items.
    */
  public readonly productQuantities: HashMap<Product, Quantity>;

  /**
   * The total quantity of all product line items. Not included are bundled line items and option line items.
   */
  public readonly productQuantityTotal: number;

  /**
     * All shipments of the line item container.
    The first shipment in the returned collection is the default shipment. All other shipments are sorted ascending by shipment ID.
    */
  public readonly shipments: Collection<Shipment>;

  /**
   * The of shipping price adjustments applied to the shipping total of the container. Note that the promotions engine only applies shipping price adjustments to the the default shipping line item of shipments, and never to the container.
   */
  public readonly shippingPriceAdjustments: Collection<PriceAdjustment>;

  /**
   * The sum of all shipping line items of the line item container, including tax before shipping adjustments have been applied.
   */
  public readonly shippingTotalGrossPrice: Money;

  /**
   * The sum of all shipping line items of the line item container, excluding tax before shipping adjustments have been applied.
   */
  public readonly shippingTotalNetPrice: Money;

  /**
   * The shipping total price. If the line item container is based on net pricing the shipping total net price is returned. If the line item container is based on gross pricing the shipping total gross price is returned.
   */
  public readonly shippingTotalPrice: Money;

  /**
   * The tax of all shipping line items of the line item container before shipping adjustments have been applied.
   */
  public readonly shippingTotalTax: Money;

  /**
   * The grand total price gross of tax for LineItemCtnr, in purchase currency. Total prices represent the sum of product prices, services prices and adjustments.
   */
  public readonly totalGrossPrice: Money;

  /**
   * The grand total price for LineItemCtnr net of tax, in purchase currency. Total prices represent the sum of product prices, services prices and adjustments.
   */
  public readonly totalNetPrice: Money;
  /**
   * The grand total tax for LineItemCtnr, in purchase currency. Total prices represent the sum of product prices, services prices and adjustments.
   */
  public readonly totalTax: Money;

  /**
   * Adds a note to the object.
   * @param subject
   * @param text
   */
  public addNote(subject: string, text: string): Note;

  /**
   * Create a billing address for the LineItemCtnr.
   */
  public createBillingAddress(): OrderAddress;

  /**
   * Creates a product line item in the container based on the passed Product and BonusDiscountLineItem.
   * @param bonusDiscountLineItem
   * @param product
   * @param optionModel
   * @param shipment
   */
  public createBonusProductLineItem(
    bonusDiscountLineItem: BonusDiscountLineItem,
    product: Product,
    optionModel: ProductOptionModel | null,
    shipment: Shipment | null
  ): ProductLineItem;

  /**
   * Creates a new CouponLineItem for this container based on the supplied coupon code.
   * @param couponCode
   * @param campaignBased
   */
  public createCouponLineItem(couponCode: string, campaignBased: boolean): CouponLineItem;

  /**
   * Creates a coupon line item that is not based on the Commerce Cloud Digital campaign system and associates it with the specified coupon code.
   * @param couponCode
   */
  public createCouponLineItem(couponCode: string): CouponLineItem;

  /**
   * Creates a gift certificate line item.
   * @param amount
   * @param recipientEmail
   */
  public createGiftCertificateLineItem(amount: number, recipientEmail: string): GiftCertificateLineItem;

  /**
   * Creates an OrderPaymentInstrument representing a Gift Certificate.
   * @param giftCertificateCode
   * @param amount
   */
  public createGiftCertificatePaymentInstrument(giftCertificateCode: string, amount: Money): OrderPaymentInstrument;

  /**
   * Creates a payment instrument using the specified payment method id and amount.
   * @param paymentMethodId
   * @param amount
   */
  public createPaymentInstrument(paymentMethodId: string, amount: Money): OrderPaymentInstrument;

  /**
   * Creates an order price adjustment.
   * The promotion id is mandatory and must not be the ID of any actual promotion defined in Commerce Cloud Digital; otherwise an exception is thrown.
   * @param promotionID
   */
  public createPriceAdjustment(promotionID: string): PriceAdjustment;

  /**
     * Creates an order level price adjustment for a specific discount.
    The promotion id is mandatory and must not be the ID of any actual promotion defined in Commerce Cloud Digital; otherwise an exception is thrown.
    * @param promotionID
    * @param discount
    */
  public createPriceAdjustment(promotionID: string, discount: Discount): PriceAdjustment;

  /**
   * Creates a new product line item in the container and assigns it to the specified shipment.
   * @param productID
   * @param shipment
   */
  public createProductLineItem(productID: string, shipment: Shipment): ProductLineItem;

  /**
   * Creates a new product line item in the basket and assigns it to the specified shipment.
   * @param productListItem
   * @param shipment
   */
  public createProductLineItem(productListItem: ProductListItem, shipment: Shipment): ProductLineItem;

  /**
   * Creates a new product line item in the container and assigns it to the specified shipment.
   * @param product
   * @param optionModel
   * @param shipment
   */
  public createProductLineItem(product: Product, optionModel: ProductOptionModel, shipment: Shipment): ProductLineItem;

  /**
   * Creates a standard shipment for the line item container.
   * @param id
   */
  public createShipment(id: string): Shipment;

  /**
   * Creates a shipping price adjustment to be applied to the container.
   * @param promotionID
   */
  public createShippingPriceAdjustment(promotionID: string): PriceAdjustment;

  /**
   * Returns the adjusted total gross price (including tax) in purchase currency.
   */
  public getAdjustedMerchandizeTotalGrossPrice(): Money;

  /**
   * Returns the total net price (excluding tax) in purchase currency.
   */
  public getAdjustedMerchandizeTotalNetPrice(): Money;

  /**
   * Returns the adjusted merchandize total price including product-level and order-level adjustments.
   */
  public getAdjustedMerchandizeTotalPrice(): Money;

  /**
   * Returns the adjusted merchandize total price including order-level adjustments if requested.
   * @param applyOrderLevelAdjustments
   */
  public getAdjustedMerchandizeTotalPrice(applyOrderLevelAdjustments: boolean): Money;

  /**
   * Returns the subtotal tax in purchase currency.
   */
  public getAdjustedMerchandizeTotalTax(): Money;

  /**
   * Returns the adjusted sum of all shipping line items of the line item container, including tax after shipping adjustments have been applied.
   */
  public getAdjustedShippingTotalGrossPrice(): Money;

  /**
   * Returns the sum of all shipping line items of the line item container, excluding tax after shipping adjustments have been applied.
   */
  public getAdjustedShippingTotalNetPrice(): Money;

  /**
   * Returns the adjusted shipping total price.
   */
  public getAdjustedShippingTotalPrice(): Money;

  /**
   * Returns the tax of all shipping line items of the line item container after shipping adjustments have been applied.
   */
  public getAdjustedShippingTotalTax(): Money;

  /**
   * Returns all product, shipping, price adjustment, and gift certificate line items of the line item container.
   */
  public getAllLineItems(): Collection<LineItem>;

  /**
   * Returns all product line items of the container, no matter if they are dependent or independent.
   */
  public getAllProductLineItems(): Collection<ProductLineItem>;

  /**
   * Returns all product line items of the container that have a product ID equal to the specified product ID, no matter if they are dependent or independent.
   * @param productID
   */
  public getAllProductLineItems(productID: string): Collection<ProductLineItem>;

  /**
   * Returns a hash mapping all products in the line item container to their total quantities.
   */
  public getAllProductQuantities(): HashMap<Product, Quantity>;

  /**
   * Returns the collection of all shipping price adjustments applied somewhere in the container.
   */
  public getAllShippingPriceAdjustments(): Collection<PriceAdjustment>;

  /**
   * Returns the billing address defined for the container.
   */
  public getBillingAddress(): OrderAddress | null;

  /**
   * Returns an unsorted collection of the the bonus discount line items associated with this container.
   */
  public getBonusDiscountLineItems(): Collection<BonusDiscountLineItem>;

  /**
   * Returns the collection of product line items that are bonus items (where ProductLineItem.isBonusProductLineItem() is true).
   */
  public getBonusLineItems(): Collection<ProductLineItem>;

  /**
     * Returns the type of the business this order has been placed in.
    Possible values are BUSINESS_TYPE_B2C or BUSINESS_TYPE_B2B.
    */
  public getBusinessType(): EnumValue<number>;

  /**
   * The channel type defines in which sales channel this order has been created.
   */
  public getChannelType(): EnumValue<number>;

  /**
   * Returns the coupon line item representing the specified coupon code.
   * @param couponCode
   */
  public getCouponLineItem(couponCode: string): CouponLineItem | null;

  /**
   * Returns a sorted collection of the coupon line items in the container.
   */
  public getCouponLineItems(): Collection<CouponLineItem>;

  /**
   * Returns the currency code for this line item container.
   */
  public getCurrencyCode(): string;

  /**
   * Returns the customer associated with this container.
   */
  public getCustomer(): Customer;

  /**
   * Returns the email of the customer associated with this container.
   */
  public getCustomerEmail(): string;

  /**
   * Returns the name of the customer associated with this container.
   */
  public getCustomerName(): string;

  /**
   * Returns the customer number of the customer associated with this container.
   */
  public getCustomerNo(): string;

  /**
   * Returns the default shipment of the line item container.
   */
  public getDefaultShipment(): Shipment | null;

  /**
   * Returns the Etag of the line item container.
   */
  public getEtag(): string;

  /**
   * Returns all gift certificate line items of the container.
   */
  public getGiftCertificateLineItems(): Collection<GiftCertificateLineItem>;

  /**
   * Returns all gift certificate line items of the container, no matter if they are dependent or independent.
   * @param giftCertificateId
   */
  public getGiftCertificateLineItems(giftCertificateId: string): Collection<GiftCertificateLineItem>;

  /**
   * Returns an unsorted collection of the PaymentInstrument instances that represent GiftCertificates in this container.
   */
  public getGiftCertificatePaymentInstruments(): Collection<PaymentInstrument>;

  /**
   * Returns an unsorted collection containing all PaymentInstruments of type PaymentInstrument.METHOD_GIFT_CERTIFICATE where the specified code is the same code on the payment instrument.
   * @param giftCertificateCode
   */
  public getGiftCertificatePaymentInstruments(giftCertificateCode: string): Collection<PaymentInstrument>;

  /**
   * Returns the total gross price of all gift certificates in the cart.
   */
  public getGiftCertificateTotalGrossPrice(): Money;

  /**
   * Returns the total net price (excluding tax) of all gift certificates in the cart.
   */
  public getGiftCertificateTotalNetPrice(): Money;

  /**
   * Returns the gift certificate total price.
   */
  public getGiftCertificateTotalPrice(): Money;

  /**
   * Returns the total tax of all gift certificates in the cart.
   */
  public getGiftCertificateTotalTax(): Money;

  /**
   * Returns the total gross price (including tax) in purchase currency.
   */
  public getMerchandizeTotalGrossPrice(): Money;

  /**
   * Returns the total net price (excluding tax) in purchase currency.
   */
  public getMerchandizeTotalNetPrice(): Money;

  /**
   * Returns the merchandize total price.
   */
  public getMerchandizeTotalPrice(): Money;

  /**
   * Returns the total tax in purchase currency.
   */
  public getMerchandizeTotalTax(): Money;

  /**
   * Returns the list of notes for this object, ordered by creation time from oldest to newest.
   */
  public getNotes(): List<Note>;

  /**
   * Returns an unsorted collection of the payment instruments in this container.
   */
  public getPaymentInstruments(): Collection<OrderPaymentInstrument>;

  /**
   * Returns an unsorted collection of PaymentInstrument instances based on the specified payment method ID.
   * @param paymentMethodID
   */
  public getPaymentInstruments(paymentMethodID: string): Collection<OrderPaymentInstrument>;

  /**
   * Returns the price adjustment associated to the specified promotion ID.
   * @param promotionID
   */
  public getPriceAdjustmentByPromotionID(promotionID: string): PriceAdjustment;

  /**
   * Returns the collection of price adjustments that have been applied to the totals such as promotion on the purchase value (i.e.
   */
  public getPriceAdjustments(): Collection<PriceAdjustment>;

  /**
   * Returns the product line items of the container that are not dependent on other product line items.
   */
  public getProductLineItems(): Collection<ProductLineItem>;

  /**
   * Returns the product line items of the container that have a product ID equal to the specified product ID and that are not dependent on other product line items.
   * @param productID
   */
  public getProductLineItems(productID: string): Collection<ProductLineItem>;

  /**
   * Returns a hash map of all products in the line item container and their total quantities.
   */
  public getProductQuantities(): HashMap<Product, Quantity>;

  /**
   * Returns a hash map of all products in the line item container and their total quantities.
   * @param includeBonusProducts
   */
  public getProductQuantities(includeBonusProducts: boolean): HashMap<Product, Quantity>;

  /**
   * Returns the total quantity of all product line items.
   */
  public getProductQuantityTotal(): number;

  /**
   * Returns the shipment for the specified ID or null if no shipment with this ID exists in the line item container.
   * @param id
   */
  public getShipment(id: string): Shipment | null;

  /**
   * Returns all shipments of the line item container.
   */
  public getShipments(): Collection<Shipment>;

  /**
   * Returns the shipping price adjustment associated with the specified promotion ID.
   * @param promotionID
   */
  public getShippingPriceAdjustmentByPromotionID(promotionID: string): PriceAdjustment;

  /**
   * Returns the of shipping price adjustments applied to the shipping total of the container.
   */
  public getShippingPriceAdjustments(): Collection<PriceAdjustment>;

  /**
   * Returns the sum of all shipping line items of the line item container, including tax before shipping adjustments have been applied.
   */
  public getShippingTotalGrossPrice(): Money;

  /**
   * Returns the sum of all shipping line items of the line item container, excluding tax before shipping adjustments have been applied.
   */
  public getShippingTotalNetPrice(): Money;

  /**
   * Returns the shipping total price.
   */
  public getShippingTotalPrice(): Money;

  /**
   * Returns the tax of all shipping line items of the line item container before shipping adjustments have been applied.
   */
  public getShippingTotalTax(): Money;

  /**
   * Returns the grand total price gross of tax for LineItemCtnr, in purchase currency.
   */
  public getTotalGrossPrice(): Money;

  /**
   * Returns the grand total price for LineItemCtnr net of tax, in purchase currency.
   */
  public getTotalNetPrice(): Money;

  /**
   * Returns the grand total tax for LineItemCtnr, in purchase currency.
   */
  public getTotalTax(): Money;

  /**
   * Removes the all Payment Instruments from this container and deletes the Payment Instruments.
   */
  public removeAllPaymentInstruments(): void;

  /**
   * Removes the specified bonus discount line item from the line item container.
   * @param bonusDiscountLineItem
   */
  public removeBonusDiscountLineItem(bonusDiscountLineItem: BonusDiscountLineItem): void;

  /**
   * Removes the specified coupon line item from the line item container.
   * @param couponLineItem
   */
  public removeCouponLineItem(couponLineItem: CouponLineItem): void;

  /**
   * Removes the specified gift certificate line item from the line item container.
   * @param giftCertificateLineItem
   */
  public removeGiftCertificateLineItem(giftCertificateLineItem: GiftCertificateLineItem): void;

  /**
   * Removes the specified Payment Instrument from this container and deletes the Payment Instrument.
   * @param pi
   */
  public removePaymentInstrument(pi: PaymentInstrument): void;

  /**
   * Removes the specified price adjustment line item from the line item container.
   * @param priceAdjustment
   */
  public removePriceAdjustment(priceAdjustment: PriceAdjustment): void;

  /**
   * Removes the specified product line item from the line item container.
   * @param productLineItem
   */
  public removeProductLineItem(productLineItem: ProductLineItem): void;

  /**
   * Removes the specified shipment and all associated product, gift certificate, shipping and price adjustment line items from the line item container.
   * @param shipment
   */
  public removeShipment(shipment: Shipment): void;

  /**
   * Removes the specified shipping price adjustment line item from the line item container.
   * @param priceAdjustment
   */
  public removeShippingPriceAdjustment(priceAdjustment: PriceAdjustment): void;

  /**
   * Sets the email address of the customer associated with this container.
   * @param aValue
   */
  public setCustomerEmail(aValue: string): void;

  /**
   * Sets the name of the customer associated with this container.
   * @param aValue
   */
  public setCustomerName(aValue: string): void;

  /**
   * Sets the customer number of the customer associated with this container.
   * @param customerNo
   */
  public setCustomerNo(customerNo: string): void;

  /**
     * Calculates the tax for all shipping and order-level merchandise price adjustments in this LineItemCtnr.

    The tax on each adjustment is calculated from the taxes of the line items the adjustment applies across.

    This method must be invoked at the end of tax calculation of a basket or an order.
    */
  public updateOrderLevelPriceAdjustmentTax(): void;

  /**
   * Recalculates the totals of the line item container.
   */
  public updateTotals(): void;
  /**
   * Verifies whether the manual price adjustments made for the line item container exceed the corresponding limits for the current user and the current site.
   */
  public verifyPriceAdjustmentLimits(): Status;
}

export = LineItemCtnr;
