import AbstractItem = require('./AbstractItem');

/**
 * Represents an item of an Appeasement which is associated with one OrderItem usually representing an Order ProductLineItem. Items are created using method Appeasement.addItems(Money, List)
When the related Appeasement were set to status COMPLETED, only the the custom attributes of the appeasement item can be changed.
 */
declare class AppeasementItem extends AbstractItem {
  /**
   * The number of the Appeasement to which this item belongs.
   */
  public readonly appeasementNumber: string;

  /**
   * Returns null or the parent item.
   */
  public parentItem: AppeasementItem | null;

  private constructor();

  /**
   * Returns the number of the Appeasement to which this item belongs.
   */
  public getAppeasementNumber(): string;

  /**
   * Returns null or the parent item.
   */
  public getParentItem(): AppeasementItem | null;

  /**
   * Set a parent item.
   * @param parentItem
   */
  public setParentItem(parentItem: AppeasementItem): void;
}

export = AppeasementItem;
