import Extensible = require('../object/Extensible');
import Collection = require('../util/Collection');
import Money = require('../value/Money');
import LineItem = require('./LineItem');
import OrderItem = require('./OrderItem');
import TaxItem = require('./TaxItem');

/**
 * An item which references, or in other words is based upon, an OrderItem. Provides methods to access the OrderItem, the order LineItem which has been extended, and the Order. In addition it defines methods to access item level prices and the item id. Supports custom-properties.
 */
declare class AbstractItem extends Extensible {
  /**
   * Gross price of item.
   */
  public readonly grossPrice: Money;

  /**
   * The item-id used for referencing between items
   */
  public readonly itemID: string;

  /**
   * The Order Product- or Shipping- LineItem associated with this item. Should never return null.
   */
  public readonly lineItem: LineItem;

  /**
   * Net price of item.
   */
  public readonly netPrice: Money;

  /**
   * The order item extensions related to this item. Should never return null.
   */
  public readonly orderItem: OrderItem;

  /**
   * The order-item-id used for referencing the OrderItem
   */
  public readonly orderItemID: string;

  /**
   * Total tax for item.
   */
  public readonly tax: Money;

  /**
   * Price of entire item on which tax calculation is based. Same as getNetPrice() or getGrossPrice() depending on whether the order is based on net or gross prices.
   */
  public readonly taxBasis: Money;

  /**
   * Tax items representing a tax breakdown
   */
  public readonly taxItems: Collection<TaxItem>;

  /**
   * Gross price of item.
   */
  public getGrossPrice(): Money;

  /**
   * The item-id used for referencing between items
   */
  public getItemID(): string;

  /**
   * Returns the Order Product- or Shipping- LineItem associated with this item.
   */
  public getLineItem(): LineItem;

  /**
   * Net price of item.
   */
  public getNetPrice(): Money;

  /**
   * Returns the order item extensions related to this item.
   */
  public getOrderItem(): OrderItem;

  /**
   * The order-item-id used for referencing the OrderItem
   */
  public getOrderItemID(): string;

  /**
   * Total tax for item.
   */
  public getTax(): Money;

  /**
   * Price of entire item on which tax calculation is based.
   */
  public getTaxBasis(): Money;

  /**
   * Tax items representing a tax breakdown
   */
  public getTaxItems(): Collection<TaxItem>;
}

export = AbstractItem;
