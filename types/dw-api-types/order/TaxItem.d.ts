import Money = require('../value/Money');
import TaxGroup = require('./TaxGroup');

declare class TaxItem {
  /**
   * Gets the amount.
   */
  public readonly amount: Money;

  /**
   * The tax group.
   */
  public readonly taxGroup: TaxGroup;
  private constructor();

  /**
   * Gets the amount.
   */
  public getAmount(): Money;

  /**
   * Returns the tax group.
   */
  public getTaxGroup(): TaxGroup;
}

export = TaxItem;
