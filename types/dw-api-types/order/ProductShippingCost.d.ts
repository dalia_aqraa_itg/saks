import Money = require('../value/Money');

/**
 * Instances of ProductShippingCost represent product specific shipping costs. 
Use ProductShippingModel.getShippingCost(ShippingMethod) to get the shipping cost for a specific product.
 */
declare class ProductShippingCost {
  /**
   * The shipping amount.
   */
  public readonly amount: Money;

  /**
   * Returns true if shipping cost is a fixed-price shipping cost, and false if surcharge shipping cost.
   */
  public readonly fixedPrice: boolean;

  /**
   * Returns true if shipping cost is a surcharge to the shipment shipping cost, and false if fixed-price shipping cost.
   */
  public readonly surcharge: boolean;

  private constructor();

  /**
   * Returns the shipping amount.
   */
  public getAmount(): Money;

  /**
   * Returns true if shipping cost is a fixed-price shipping cost, and false if surcharge shipping cost.
   */
  public isFixedPrice(): boolean;

  /**
     * Returns true if shipping cost is a surcharge to the shipment shipping cost, and false if fixed-price shipping cost.

    */
  public isSurcharge(): boolean;
}

export = ProductShippingCost;
