import CertificateRef = require('../crypto/CertificateRef');
import EncryptedObject = require('../customer/EncryptedObject');

import CustomAttributes = require('../object/CustomAttributes');

interface IPaymentInstrumentCustomAttributes extends CustomAttributes {}

declare class PaymentInstrument extends EncryptedObject<IPaymentInstrumentCustomAttributes> {
  public static ENCRYPTION_ALGORITHM_RSA: string;
  public static METHOD_BANK_TRANSFER: string;
  public static METHOD_BML: string;
  public static METHOD_CREDIT_CARD: string;
  public static METHOD_GIFT_CERTIFICATE: string;

  public bankAccountDriversLicense: string;
  public bankAccountDriversLicenseStateCode: string;
  public bankAccountHolder: string;
  public bankAccountNumber: string;
  public bankAccountNumberLastDigits: string;
  public bankRoutingnumber: string;
  public creditCardExpirationMonth: number;
  public creditCardExpirationYear: number;
  public creditCardExpired: boolean;
  public creditCardHolder: string;
  public creditCardIssueNumber: string;
  public creditCardnumber: string;
  public creditCardnumberLastDigits: string;
  public creditCardToken: string;
  public creditCardType: string;
  public creditCardValidFromMonth: number;
  public creditCardValidFromYear: number;
  public giftCertificateCode: string;
  public maskedBankAccountDriversLicense: string;
  public maskedBankAccountNumber: string;
  public maskedCreditCardNumber: string;
  public maskedGiftCertificateCode: string;
  public paymentMethod: string;
  public permanentlyMasked: boolean;

  public getBankAccountDriversLicense(): string;
  public getBankAccountDriversLicenseLastDigits(): string;
  public getBankAccountDriversLicenseLastDigits(count: number): string;
  public getBankAccountDriversLicenseStateCode(): string;
  public getBankAccountHolder(): string;
  public getBankAccountnumberLastDigits(): string;
  public getBankAccountnumberLastDigits(count: number): string;
  public getBankRoutingnumber(): string;
  public getCreditCardExpirationMonth(): number;
  public getCreditCardExpirationYear(): number;
  public getCreditCardHolder(): string;
  public getCreditCardIssueNumber(): string;
  public getCreditCardnumber(): string;
  public getCreditCardnumberLastDigits(): string;
  public getCreditCardnumberLastDigits(count: number): string;
  public getCreditCardToken(): string;
  public getCreditCardType(): string;
  public getCreditCardValidFromMonth(): number;
  public getCreditCardValidFromYear(): number;
  public getEncryptedBankAccountDriversLicense(algorithm: string, publicKey: string): string;
  public getEncryptedBankAccountnumber(algorithm: string, publicKey: string): string;
  public getEncryptedCreditCardnumber(algorithm: string, publicKey: CertificateRef): string;
  public getGiftCertificateCode(): string;
  public getMaskedBankAccountDriversLicense(): string;
  public getMaskedBankAccountDriversLicense(ignore: number): string;
  public getMaskedBankAccountnumber(): string;
  public getMaskedBankAccountnumber(ignore: number): string;
  public getMaskedCreditCardNumber(): string;
  public getMaskedCreditCardNumber(ignore: number): string;
  public getMaskedGiftCertificateCode(): string;
  public getMaskedGiftCertificateCode(ignore: number): string;
  public getPaymentMethod(): string;
  public isCreditCardExpired(): Boolean;
  public isPermanentlyMasked(): Boolean;

  public setBankAccountDriversLicense(license: string): void;
  public setBankAccountDriversLicenseStateCode(stateCode: string): void;
  public setBankAccountHolder(holder: string): void;
  public setBankAccountNumber(accountnumber: string): void;
  public setBankRoutingNumber(routingnumber: string): void;
  public setCreditCardExpirationMonth(aValue: number): void;
  public setCreditCardExpirationYear(aValue: number): void;
  public setCreditCardHolder(aValue: string): void;
  public setCreditCardIssueNumber(aValue: string): void;
  public setCreditCardNumber(aValue: string): void;
  public setCreditCardToken(token: string): void;
  public setCreditCardType(aValue: string): void;
  public setCreditCardValidFromMonth(aValue: number): void;
  public setCreditCardValidFromYear(aValue: number): void;
  public setGiftCertificateCode(giftCertificateCode: string): void;
}

export = PaymentInstrument;
