import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Collection = require('../util/Collection');
import BonusDiscountLineItem = require('./BonusDiscountLineItem');
import PriceAdjustment = require('./PriceAdjustment');

declare global {
  namespace ICustomAttributes {
    interface CouponLineItem extends CustomAttributes {}
  }
}

/**
 * The CouponLineItem class is used to store redeemed coupons in the Basket.
 */
declare class CouponLineItem extends ExtensibleObject<ICustomAttributes.CouponLineItem> {
  /**
   * Identifies if the coupon is currently applied in the basket. A coupon line is applied if there exists at least one price adjustment related to the coupon line item.
   */
  public applied: boolean;

  /**
   * Returns true the line item represents a coupon of a Commerce Cloud Digital campaign. If the coupon line item represents a custom coupon code, the method returns false.
   */
  public basedOnCampaign: boolean;

  /**
   * The bonus discount line items of the line item container triggered by this coupon.
   */
  public bonusDiscountLineItems: Collection<BonusDiscountLineItem>;

  /**
   * The coupon code.
   */
  public couponCode: string;

  /**
   * The price adjustments of the line item container triggered by this coupon.
   */
  public priceAdjustments: Collection<PriceAdjustment>;

  /**
   * This method provides a detailed error status in case the coupon code of this coupon line item instance became invalid.
   */
  public statusCode: string;

  /**
     * Allows to check whether the coupon code of this coupon line item instance is valid. Coupon line item is valid, if status code is one of the following:

        * CouponStatusCodes.APPLIED
        * CouponStatusCodes.NO_APPLICABLE_PROMOTION

    */
  public valid: boolean;
  private constructor();

  /**
   * Associates the specified price adjustment with the coupon line item.
   *
   * @param priceAdjustment
   */
  public associatePriceAdjustment(priceAdjustment: PriceAdjustment): void;

  /**
   * Returns the bonus discount line items of the line item container triggered by this coupon.
   */
  public getBonusDiscountLineItems(): Collection<BonusDiscountLineItem>;

  /**
   * Returns the coupon code.
   */
  public getCouponCode(): string;

  /**
   * Returns the price adjustments of the line item container triggered by this coupon.
   */
  public getPriceAdjustments(): Collection<PriceAdjustment>;

  /**
   * This method provides a detailed error status in case the coupon code of this coupon line item instance became invalid.
   */
  public getStatusCode(): string;

  /**
   * Identifies if the coupon is currently applied in the basket.
   */
  public isApplied(): boolean;

  /**
   * Returns true the line item represents a coupon of a Commerce Cloud Digital campaign.
   */
  public isBasedOnCampaign(): boolean;

  /**
   * Allows to check whether the coupon code of this coupon line item instance is valid.
   */
  public isValid(): boolean;
}

export = CouponLineItem;
