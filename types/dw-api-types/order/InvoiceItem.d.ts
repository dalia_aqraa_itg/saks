import Collection = require('../util/Collection');
import EnumValue = require('../value/EnumValue');
import Money = require('../value/Money');
import Quantity = require('../value/Quantity');
import AbstractItem = require('./AbstractItem');

declare class InvoiceItem extends AbstractItem {
  /**
   * Price of a single unit before discount application.
   */
  public readonly basePrice: Money;

  /**
   * The captured amount for this item.
   */
  public readonly capturedAmount: Money;

  /**
   * The number of the invoice to which this item belongs.
   */
  public readonly invoiceNumber: String;

  /**
   * Returns null or the parent item.
   */
  public readonly parentItem: InvoiceItem | null;

  /**
   * The quantity of this item.
   */
  public readonly quantity: Quantity;

  /**
   * The refunded amount for this item.
   */
  public readonly refundedAmount: Money;
  private constructor();

  /**
   * Price of a single unit before discount application.
   */
  public getBasePrice(): Money;

  /**
   * Returns the captured amount for this item.
   */
  public getCapturedAmount(): Money;

  /**
   * Returns the number of the invoice to which this item belongs.
   */
  public getInvoiceNumber(): String;

  /**
   * Returns null or the parent item.
   */
  public getParentItem(): InvoiceItem | null;

  /**
   * Returns the quantity of this item.
   */
  public getQuantity(): Quantity;

  /**
   * Returns the refunded amount for this item.
   */
  public getRefundedAmount(): Money;

  /**
   * Updates the captured amount for this item.
   * @param capturedAmount
   */
  public setCapturedAmount(capturedAmount: Money): void;

  /**
   * Set a parent item.
   * @param parentItem
   */
  public setParentItem(parentItem: InvoiceItem | null): void;

  /**
   * Updates the refunded amount for this item.
   * @param refundedAmount
   */
  public setRefundedAmount(refundedAmount: Money): void;
}

export = InvoiceItem;
