import CustomAttributes = require('../object/CustomAttributes');
import Status = require('../system/Status');
import LineItemCtnr = require('./LineItemCtnr');
import Order = require('./Order');

declare global {
  namespace ICustomAttributes {
    interface Basket extends CustomAttributes {}
  }
}

/**
 * The Basket class represents a shopping cart.
 */
declare class Basket extends LineItemCtnr<ICustomAttributes.Basket> {
  /**
     * Returns if the basket was created by an agent.

    An agent basket is created by an agent on behalf of the customer in comparison to a storefront basket which is created by the customer e.g. in the storefront. An agent basket can be created with BasketMgr.createAgentBasket().
    */
  public readonly agentBasket: boolean;

  /**
     * The timestamp when the inventory for this basket expires.

    It will return null for the following reasons:

        - No reservation for the basket was done
        - Reservation is outdated meaning the timestamp is in the past

    Please note that the expiry timestamp will not always be valid for the whole basket. It will not be valid for new items added or items whose quantity has changed after the reservation was done.
    */
  public readonly inventoryReservationExpiry: Date | null;

  /**
   * The order that this basket represents if the basket is being used to edit an order, otherwise this method returns null.
   */
  public readonly orderBeingEdited: Order | null;

  /**
   * The number of the order that this basket represents if the basket is being used to edit an order, otherwise this method returns null.
   */
  public readonly orderNoBeingEdited: string | null;

  private constructor();

  /**
   * Returns the timestamp when the inventory for this basket expires.
   */
  public getInventoryReservationExpiry(): Date | null;

  /**
   * Returns the order that this basket represents if the basket is being used to edit an order, otherwise this method returns null.
   */
  public getOrderBeingEdited(): Order | null;

  /**
   * Returns the number of the order that this basket represents if the basket is being used to edit an order, otherwise this method returns null.
   */
  public getOrderNoBeingEdited(): string | null;

  /**
   * Returns if the basket was created by an agent.
   */
  public isAgentBasket(): boolean;

  /**
     *
    Reserves inventory for all items in this basket for 10 minutes.
    */
  public reserveInventory(): Status;

  /**
   * Reserves inventory for all items in this basket for a specified amount of minutes.
   * @param reservationDurationInMinutes
   */
  public reserveInventory(reservationDurationInMinutes: number): Status;

  /**
   * Reserves inventory for all items in this basket for a specified amount of minutes.
   * @param reservationDurationInMinutes
   * @param removeIfNotAvailable
   */
  public reserveInventory(reservationDurationInMinutes: number, removeIfNotAvailable: boolean): Status;

  /**
     * Set the type of the business this order has been placed in.
    Possible values are LineItemCtnr.BUSINESS_TYPE_B2C or LineItemCtnr.BUSINESS_TYPE_B2B.
    * @param aType
    */
  public setBusinessType(aType: number): void;

  /**
   * Set the channel type in which sales channel this order has been created.
   * @param aType
   */
  public setChannelType(aType: number): void;

  /**
   * Register a "start checkout" event for the current basket.
   */
  public startCheckout(): void;

  /**
   * Updates the basket currency if different to session currency, otherwise does nothing.
   */
  public updateCurrency(): void;
}

export = Basket;
