import MarkupText = require('../content/MarkupText');
import MediaFile = require('../content/MediaFile');
import Customer = require('../customer/Customer');
import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Status = require('../system/Status');

declare global {
  namespace ICustomAttributes {
    interface PaymentCard extends CustomAttributes {}
  }
}
/**
 * Represents payment cards and provides methods to access the payment card attributes and status.
 */
declare class PaymentCard extends ExtensibleObject<ICustomAttributes.PaymentCard> {
  /**
   * Returns 'true' if payment card is active (enabled), otherwise 'false' is returned.
   */
  public readonly active: boolean;

  /**
   * The unique card type of the payment card.
   */
  public readonly cardType: string;

  /**
   * The description of the payment card.
   */
  public readonly description: MarkupText;

  /**
   * The reference to the payment card image.
   */
  public readonly image: MediaFile;

  /**
   * The name of the payment card.
   */
  public readonly name: string;

  private constructor();

  /**
   * Returns the unique card type of the payment card.
   */
  public getCardType(): string;

  /**
   * Returns the description of the payment card.
   */
  public getDescription(): MarkupText;

  /**
   * Returns the reference to the payment card image.
   */
  public getImage(): MediaFile;

  /**
   * Returns the name of the payment card.
   */
  public getName(): string;

  /**
   * Returns 'true' if payment card is active (enabled), otherwise 'false' is returned.
   */
  public isActive(): boolean;

  /**
   * Returns 'true' if this payment card is applicable for the specified customer, country and payment amount and the session currency.
   * @param customer
   * @param countryCode
   * @param paymentAmount
   */
  public isApplicable(customer: Customer | null, countryCode: string | null, paymentAmount: number | null): boolean;

  /**
   * Verify the card against the provided values.
   * @param expiresMonth
   * @param expiresYear
   * @param cardNumber
   */
  public verify(expiresMonth: number, expiresYear: number, cardNumber: string): Status;

  /**
   * Verify the card against the provided values.
   * @param expiresMonth
   * @param expiresYear
   * @param cardNumber
   * @param csc
   */
  public verify(expiresMonth: number, expiresYear: number, cardNumber: string, csc: string): Status;
}

export = PaymentCard;
