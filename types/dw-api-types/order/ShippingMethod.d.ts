import CustomerGroup = require('../customer/CustomerGroup');
import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Collection = require('../util/Collection');

declare global {
  namespace ICustomAttributes {
    interface ShippingMethod extends CustomAttributes {}
  }
}
/**
 * ShippingMethod represents how the shipment will be shipped.
 */
declare class ShippingMethod extends ExtensibleObject<ICustomAttributes.ShippingMethod> {
  /**
   * The base shipping method or null if undefined.
   */
  public readonly baseMethod: ShippingMethod | null;

  /**
   * The currency code associated with the shipping method
   */
  public readonly currencyCode: string;

  /**
   * The customer groups assigned to the shipping method. Assigned ids that do not belong to an existing customer group are ignored.
   */
  public readonly customerGroups: Collection<CustomerGroup>;

  /**
   * Returns 'true' if the shipping method is marked as 'default' for the current session's currency. Otherwise 'false' is returned.
   */
  public readonly defaultMethod: boolean;

  /**
     * The dependent shipping methods of this shipping method, regardless of the online status of the methods.
    Dependent shipping methods have this method as their base method.
    */
  public readonly dependentMethods: Collection<ShippingMethod>;

  /**
   * The description of the shipping method as specified in the current locale or null if it could not be found.
   */
  public readonly description: string | null;

  /**
   * The display name of the shipping method in the current locale or null if it could not be found.
   */
  public readonly displayName: string | null;

  /**
   * The ID of the shipping method.
   */
  public readonly ID: string;

  /**
   * Returns true if shipping method is online, false otherwise
   */
  public readonly online: boolean;

  /**
   * The tax class id of the shipping method.
   */
  public readonly taxClassID: string;
  private constructor();

  /**
   * Returns the base shipping method or null if undefined.
   */
  public getBaseMethod(): ShippingMethod | null;

  /**
   * Returns the currency code associated with the shipping method
   */
  public getCurrencyCode(): string;

  /**
   * Returns the customer groups assigned to the shipping method.
   */
  public getCustomerGroups(): Collection<CustomerGroup>;

  /**
   * Returns the dependent shipping methods of this shipping method, regardless of the online status of the methods.
   */
  public getDependentMethods(): Collection<ShippingMethod>;

  /**
   * Returns the description of the shipping method as specified in the current locale or null if it could not be found.
   */
  public getDescription(): string | null;

  /**
   * Returns the display name of the shipping method in the current locale or null if it could not be found.
   */
  public getDisplayName(): string | null;

  /**
   * Returns the ID of the shipping method.
   */
  public getID(): string;

  /**
   * Returns the tax class id of the shipping method.
   */
  public getTaxClassID(): string;

  /**
   * Returns 'true' if the shipping method is marked as 'default' for the current session's currency.
   */
  public isDefaultMethod(): boolean;

  /**
   * Returns true if shipping method is online, false otherwise
   */
  public isOnline(): boolean;
}

export = ShippingMethod;
