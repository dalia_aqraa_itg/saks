import Decimal = require('../util/Decimal');

declare class TaxGroup {
  /**
   * Creates a TaxGroup.
   * This TaxGroup can be used for example in ReturnItem.addTaxItem(Decimal, TaxGroup).
   * @param taxType - the tax type
   * @param caption - the caption
   * @param description - the description
   * @param taxRate - the tax rate as floating point. 1.0 means 100 %.
   */
  public static create(taxType: string, caption: string, description: string, taxRate: Decimal): TaxGroup;

  /**
   * Gets the caption.
   */
  public readonly caption: string;

  /**
   * Gets the description.
   */
  public readonly description: string;

  /**
   * Gets the percentage amount of the rate.
   */
  public readonly rate: number;

  /**
   * Gets the tax type.
   */
  public readonly taxType: string;
  private constructor();

  /**
   * Gets the caption.
   */
  public getCaption(): string;

  /**
   * Gets the description.
   */
  public getDescription(): string;

  /**
   * Gets the percentage amount of the rate.
   */
  public getRate(): Number;

  /**
   * Gets the tax type.
   */
  public getTaxType(): string;
}

export = TaxGroup;
