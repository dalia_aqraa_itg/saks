import MarkupText = require('../content/MarkupText');
import MediaFile = require('../content/MediaFile');
import Customer = require('../customer/Customer');
import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import List = require('../util/List');
import PaymentCard = require('./PaymentCard');
import PaymentProcessor = require('./PaymentProcessor');

declare global {
  namespace ICustomAttributes {
    interface PaymentMethod extends CustomAttributes {}
  }
}

/**
 * The PaymentMethod class represents a logical type of payment a customer can make in the storefront. This class provides methods to access the payment method attributes, status, and (for card-based payment methods) the related payment cards.
A typical storefront presents the customer a list of payment methods that a customer can choose from after he has entered his billing address during the checkout. PaymentMgr.getApplicablePaymentMethods(Customer, String, Number) is used to determine the PaymentMethods that are relevant for the customer based on the amount of his order, his customer groups, and his shipping address.
 */
declare class PaymentMethod extends ExtensibleObject<ICustomAttributes.PaymentMethod> {
  /**
   * Returns 'true' if payment method is active (enabled), otherwise 'false' is returned.
   */
  public readonly active: boolean;

  /**
   * Returns enabled payment cards that are assigned to this payment method, regardless of current customer, country or payment amount restrictions. The payment cards are sorted as defined in the Business Manager.
   */
  public readonly activePaymentCards: List<PaymentCard>;

  /**
   * The description of the payment method.
   */
  public readonly description: MarkupText;

  /**
   * The unique ID of the payment method.
   */
  public readonly ID: string;

  /**
   * The reference to the payment method image.
   */
  public readonly image: MediaFile;

  /**
   * The name of the payment method.
   */
  public readonly name: string;

  /**
   * The payment processor associated to this payment method.
   */
  public readonly paymentProcessor: PaymentProcessor;

  private constructor();

  /**
   * Returns enabled payment cards that are assigned to this payment method, regardless of current customer, country or payment amount restrictions.
   */
  public getActivePaymentCards(): List<PaymentCard>;

  /**
   * Returns the sorted list of all enabled payment cards of this payment method applicable for the specified customer, country, payment amount and the session currency The payment cards are sorted as defined in the Business Manager.
   * @param customer
   * @param countryCode
   * @param paymentAmount
   */
  public getApplicablePaymentCards(customer: Customer, countryCode: string, paymentAmount: number): List<PaymentCard>;

  /**
   * Returns the description of the payment method.
   */
  public getDescription(): MarkupText;

  /**
   * Returns the unique ID of the payment method.
   */
  public getID(): string;

  /**
   * Returns the reference to the payment method image.
   */
  public getImage(): MediaFile;

  /**
   * Returns the name of the payment method.
   */
  public getName(): string;

  /**
   * Returns the payment processor associated to this payment method.
   */
  public getPaymentProcessor(): PaymentProcessor;

  /**
   * Returns 'true' if payment method is active (enabled), otherwise 'false' is returned.
   */
  public isActive(): boolean;

  /**
   * Returns 'true' if this payment method is applicable for the specified customer, country and payment amount and the session currency.
   * @param customer
   * @param countryCode
   * @param paymentAmount
   */
  public isApplicable(customer: Customer, countryCode: string, paymentAmount: number): boolean;
}

export = PaymentMethod;
