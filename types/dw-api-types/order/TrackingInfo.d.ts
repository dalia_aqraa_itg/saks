import Extensible = require('../object/Extensible');
import Collection = require('../util/Collection');
import ShippingOrder = require('./ShippingOrder');
import TrackingRef = require('./TrackingRef');

/**
 * 
Provides basic information about a tracking info. An instance is identified by an ID and can be referenced from n ShippingOrderItems using TrackingRefs. This also allows one ShippingOrderItem to be associated with n TrackingInfo.

See Also:

ShippingOrder.addTrackingInfo(string)
ShippingOrderItem.addTrackingRef(string, Quantity)

 */
declare class TrackingInfo extends Extensible {
  /**
   * Get the Carrier.
   */
  public carrier: string;

  /**
   * Get the service(ship method) of the used carrier.
   */
  public carrierService: string;

  /**
   * Get the mandatory identifier for this tracking information. The id allows the tracking information to be referenced from TrackingRefs. To support short shipping a shipping-order-item can manage a list of TrackingRefs, each with an optional quantity value allowing individual items to ship in multiple parcels with known item quantity in each.
   */
  public readonly ID: string;

  /**
   * Get the ship date.
   */
  public shipDate: Date;

  /**
   * Gets the shipping order.
   */
  public readonly shippingOrder: ShippingOrder;

  /**
   * Get the tracking number.
   */
  public trackingNumber: string;

  /**
   * Gets the tracking refs (shipping order items) which are assigned to this tracking info.
   */
  public readonly trackingRefs: Collection<TrackingRef>;

  /**
   * Get the id of the shipping warehouse.
   */
  public warehouseID: string;
  private constructor();

  /**
   * Get the Carrier.
   */
  public getCarrier(): string;

  /**
   * Get the service(ship method) of the used carrier.
   */
  public getCarrierService(): string;

  /**
   * Get the mandatory identifier for this tracking information.
   */
  public getID(): string;

  /**
   * Get the ship date.
   */
  public getShipDate(): Date;

  /**
   * Gets the shipping order.
   */
  public getShippingOrder(): ShippingOrder;

  /**
   * Get the tracking number.
   */
  public getTrackingNumber(): string;

  /**
   * Gets the tracking refs (shipping order items) which are assigned to this tracking info.
   */
  public getTrackingRefs(): Collection<TrackingRef>;

  /**
   * Get the id of the shipping warehouse.
   */
  public getWarehouseID(): string;

  /**
   * Set the Carrier.
   * @param carrier  the Carrier
   */
  public setCarrier(carrier: string): void;

  /**
   * Set the service(ship method) of the used carrier.
   * @param carrierService  the carrier service, eg. the ship method
   */
  public setCarrierService(carrierService: string): void;

  /**
   * Set the ship date.
   * @param shipDate the ship date
   */
  public setShipDate(shipDate: Date): void;
  /**
   * Set the TrackingNumber.
   * @param trackingNumber  the TrackingNumber
   */
  public setTrackingNumber(trackingNumber: string): void;

  /**
   * Set the id of the shipping warehouse.
   * @param warehouseID  the id of the shipping warehouse
   */
  public setWarehouseID(warehouseID: string): void;
}

export = TrackingInfo;
