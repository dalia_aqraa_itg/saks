import CustomerAddress = require('../customer/CustomerAddress');
import OrderAddress = require('./OrderAddress');

/**
 *
 * Represents a specific location for a shipment.
 *
 * _Note: this class allows access to sensitive personal and private information. Pay attention to appropriate legal and regulatory requirements related to this data._
 */
declare class ShippingLocation {
  /**
   * The shipping location's first address.
   */
  public address1: string;

  /**
   * The shipping location's second address.
   */
  public address2: string;

  /**
   *  The shipping location's city.
   */
  public city: string;

  /**
   * The shipping location's country code.
   */
  public countryCode: string;

  /**
   * The shipping location's postal code.
   */
  public postalCode: string;

  /**
   * The shipping location's post box.
   */
  public postBox: string;

  /**
   * The shipping location's state code.
   */
  public stateCode: string;

  /**
   * The shipping location's suite.
   */
  public suite: string;

  /**
   * Constructs a new shipping location.
   */
  constructor();

  /**
   * Constructs a new shipping location and initializes it with the values of the specified address object.
   */
  constructor(address: CustomerAddress);

  /**
   * Constructs a new shipping location and initializes it with the values of the specified address object.
   */
  constructor(address: OrderAddress);

  /**
   * Returns the shipping location's first address.
   */
  public getAddress1(): string;

  /**
   * Returns the shipping location's second address.
   */
  public getAddress2(): string;

  /**
   * Returns the shipping location's city.
   */
  public getCity(): string;

  /**
   * Returns the shipping location's country code.
   */
  public getCountryCode(): string;

  /**
   * Returns the shipping location's postal code.
   */
  public getPostalCode(): string;

  /**
   * Returns the shipping location's post box.
   */
  public getPostBox(): string;

  /**
   * Returns the shipping location's state code.
   */
  public getStateCode(): string;

  /**
   * Returns the shipping location's suite.
   */
  public getSuite(): string;

  /**
   * Sets the shipping location's first address.
   * @param aValue
   */
  public setAddress1(aValue: string): void;

  /**
   * Sets the shipping location's second address.
   * @param aValue
   */
  public setAddress2(aValue: string): void;

  /**
   * Sets the shipping location's city.
   * @param aValue
   */
  public setCity(aValue: string): void;

  /**
   * Sets the shipping location's country code.
   * @param aValue
   */
  public setCountryCode(aValue: string): void;

  /**
   * Sets the shipping location's postal code.
   * @param aValue
   */
  public setPostalCode(aValue: string): void;

  /**
   * Sets the shipping location's post box.
   * @param aValue
   */
  public setPostBox(aValue: string): void;

  /**
   * Sets the shipping location's state code.
   * @param aValue
   */
  public setStateCode(aValue: string): void;

  /**
   * Sets the shipping location's suite.
   * @param aValue
   */
  public setSuite(aValue: string): void;
}

export = ShippingLocation;
