import Quantity = require('../value/Quantity');
import ShippingOrderItem = require('./ShippingOrderItem');
import TrackingInfo = require('./TrackingInfo');

declare class TrackingRef {
  /**
   * Gets the quantity, the shipping order item is assigned to the tracking info.
   */
  public quantity: Quantity;

  /**
   * Gets the shipping order item which is assigned to the tracking info.
   */
  public readonly shippingOrderItem: ShippingOrderItem;

  /**
   * Gets the tracking info, the shipping order item is assigned to.
   */
  public readonly trackingInfo: TrackingInfo;
  private constructor();

  /**
   * Gets the quantity, the shipping order item is assigned to the tracking info.
   */

  public getQuantity(): Quantity;

  /**
   * Gets the shipping order item which is assigned to the tracking info.
   */
  public getShippingOrderItem(): ShippingOrderItem;

  /**
   * Gets the tracking info, the shipping order item is assigned to.
   */
  public getTrackingInfo(): TrackingInfo;

  /**
   * Sets the quantity, the shipping order item is assigned to the tracking info.
   */
  public setQuantity(quantity: Quantity): void;
}

export = TrackingRef;
