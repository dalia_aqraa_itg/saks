import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Collection = require('../util/Collection');
import EnumValue = require('../value/EnumValue');
import Money = require('../value/Money');
import GiftCertificateLineItem = require('./GiftCertificateLineItem');
import LineItem = require('./LineItem');
import OrderAddress = require('./OrderAddress');
import PriceAdjustment = require('./PriceAdjustment');
import ProductLineItem = require('./ProductLineItem');
import ShippingLineItem = require('./ShippingLineItem');
import ShippingMethod = require('./ShippingMethod');

declare global {
  namespace ICustomAttributes {
    interface Shipment extends CustomAttributes {}
  }
}

declare class Shipment extends ExtensibleObject<ICustomAttributes.Shipment> {
  /**
   * Shipment shipping status representing 'Not shipped'.
   */
  public static readonly SHIPPING_STATUS_NOTSHIPPED: number;

  /**
   * Shipment shipping status representing 'Shipped'.
   */
  public static readonly SHIPPING_STATUS_SHIPPED: number;

  /**
   * The adjusted total gross price, including tax, in the purchase currency. Adjusted merchandize prices represent the sum of product prices before services such as shipping have been added, but after adjustments from i.e. promotions have been added.
   */
  public readonly adjustedMerchandizeTotalGrossPrice: Money;

  /**
   * The adjusted net price, excluding tax, in the purchase currency. Adjusted merchandize prices represent the sum of product prices before services such as shipping have been added, but after adjustments from i.e. promotions have been added.
   */
  public readonly adjustedMerchandizeTotalNetPrice: Money;

  /**
   * The merchandize total price after all product discounts. If the line item container is based on net pricing the adjusted merchandize total net price is returned. If the line item container is based on gross pricing the adjusted merchandize total gross price is returned.
   */
  public readonly adjustedMerchandizeTotalPrice: Money;

  /**
   * The total tax in purchase currency. Adjusted merchandize prices represent the sum of product prices before services such as shipping have been added, but after adjustments from i.e. promotions have been added.
   */
  public readonly adjustedMerchandizeTotalTax: Money;

  /**
   * The adjusted sum of all shipping line items of the shipment, including tax after shipping adjustments have been applied.
   */
  public readonly adjustedShippingTotalGrossPrice: Money;

  /**
   * The sum of all shipping line items of the shipment, excluding tax after shipping adjustments have been applied.
   */
  public readonly adjustedShippingTotalNetPrice: Money;

  /**
   * The adjusted shipping total price. If the line item container is based on net pricing the adjusted shipping total net price is returned. If the line item container is based on gross pricing the adjusted shipping total gross price is returned.
   */
  public readonly adjustedShippingTotalPrice: Money;

  /**
   * The tax of all shipping line items of the shipment after shipping adjustments have been applied.
   */
  public readonly adjustedShippingTotalTax: Money;

  /**
     * All line items related to the shipment.

    The returned collection may include line items of the following types:

        - ProductLineItem
        - ShippingLineItem
        - GiftCertificateLineItem
        - PriceAdjustment

    Their common type is LineItem.

    Each ProductLineItem in the collection may itself contain bundled or option product line items, as well as a product-level shipping line item.
    */
  public readonly allLineItems: Collection<LineItem>;

  /**
   * Return true if this shipment is the default shipment.
   */
  public readonly default: boolean;

  /**
   * Returns true if this line item represents a gift, false otherwise.
   */
  public gift: boolean;

  /**
   * All gift certificate line items of the shipment.
   */
  public readonly giftCertificateLineItems: Collection<GiftCertificateLineItem>;

  /**
   * The value set for gift message or null if no value set.
   */
  public giftMessage: string;

  /**
   * The ID of this shipment.
   */
  public readonly ID: string;

  /**
   * The total gross price, including tax, in the purchase currency. Merchandize total prices represent the sum of product prices before services such as shipping or adjustments from i.e. promotions have been added.
   */
  public readonly merchandizeTotalGrossPrice: Money;

  /**
   * The net price, excluding tax, in the purchase currency. Merchandize total prices represent the sum of product prices before services such as shipping or adjustments from i.e. promotions have been added.
   */
  public readonly merchandizeTotalNetPrice: Money;

  /**
   * The merchandize total price. If the line item container is based on net pricing the merchandize total net price is returned. If the line item container is based on gross pricing the merchandize total gross price is returned.
   */
  public readonly merchandizeTotalPrice: Money;

  /**
   * The total tax in purchase currency. Merchandize total prices represent the sum of product prices before services such as shipping or adjustments from i.e. promotions have been added.
   */
  public readonly merchandizeTotalTax: Money;

  /**
   * A collection of all product line items related to this shipment.
   */
  public readonly productLineItems: Collection<ProductLineItem>;

  /**
   * The merchandise total price of the shipment after considering all product price adjustments and prorating all Buy-X-Get-Y and order-level discounts, according to the scheme described in PriceAdjustment.getProratedPrices(). For net pricing the net price is returned. For gross pricing, the gross price is returned.
   */
  public readonly proratedMerchandizeTotalPrice: Money;

  /**
   * The shipment number for this shipment. This number is automatically generated.
   */
  public readonly shipmentNo: string;

  /**
   * The shipping address or null if none is set.
   */
  public readonly shippingAddress: OrderAddress | null;

  /**
   * A collection of all shipping line items of the shipment, excluding any product-level shipping costs that are associated with ProductLineItems of the shipment.
   */
  public readonly shippingLineItems: Collection<ShippingLineItem>;

  /**
   * The shipping method or null if none is set.
   */
  public shippingMethod: ShippingMethod | null;

  /**
   * The shipping method ID or null if none is set.
   */
  public readonly shippingMethodID: string;

  /**
     * A collection of price adjustments that have been applied to the shipping costs of the shipment, for example by the promotions engine.
    Note that this method returns all shipping price adjustments in this shipment regardless of which shipping line item they belong to. Use ShippingLineItem.getShippingPriceAdjustments() to retrieve the shipping price adjustments associated with a specific shipping line item.
    */
  public readonly shippingPriceAdjustments: Collection<PriceAdjustment>;

  /**
   * The shipping status. Possible values are SHIPMENT_NOTSHIPPED or SHIPMENT_SHIPPED.
   */
  public shippingStatus: EnumValue<number>;

  /**
   * The sum of all shipping line items of the shipment, including tax before shipping adjustments have been applied.
   */
  public readonly shippingTotalGrossPrice: Money;

  /**
   * The sum of all shipping line items of the shipment, excluding tax before shipping adjustments have been applied.
   */
  public readonly shippingTotalNetPrice: Money;

  /**
   * The shipping total price. If the line item container is based on net pricing the shipping total net price is returned. If the line item container is based on gross pricing the shipping total gross price is returned.
   */
  public readonly shippingTotalPrice: Money;

  /**
   * The tax of all shipping line items of the shipment before shipping adjustments have been applied.
   */
  public readonly shippingTotalTax: Money;

  /**
   * Convenenience method. Same as getShippingLineItem(ShippingLineItem.STANDARD_SHIPPING_ID)
   */
  public readonly standardShippingLineItem: ShippingLineItem;

  /**
   * The grand total price gross of tax for the shipment, in purchase currency. Total prices represent the sum of product prices, services prices and adjustments.
   */
  public readonly totalGrossPrice: Money;

  /**
   * The grand total price for the shipment net of tax, in purchase currency. Total prices represent the sum of product prices, services prices and adjustments.
   */
  public readonly totalNetPrice: Money;

  /**
   * The total tax for the shipment, in purchase currency. Total prices represent the sum of product prices, services prices and adjustments.
   */
  public readonly totalTax: Money;

  /**
   * The tracking number of this shipment.
   */
  public trackingNumber: string;

  private constructor();

  /**
   * A shipment has initially no shipping address.
   */
  public createShippingAddress(): OrderAddress;

  /**
   * Creates a new shipping line item for this shipment.
   * @param id
   */
  public createShippingLineItem(id: string): ShippingLineItem;

  /**
   * Returns the adjusted total gross price, including tax, in the purchase currency.
   */
  public getAdjustedMerchandizeTotalGrossPrice(): Money;

  /**
   * Returns the adjusted net price, excluding tax, in the purchase currency.
   */
  public getAdjustedMerchandizeTotalNetPrice(): Money;

  /**
   * Returns the merchandize total price after all product discounts.
   */
  public getAdjustedMerchandizeTotalPrice(): Money;

  /**
   * Returns the merchandise total price of the shipment after considering all product price adjustments and, optionally, prorating all order-level discounts.
   * @param applyOrderLevelAdjustments
   */
  public getAdjustedMerchandizeTotalPrice(applyOrderLevelAdjustments: boolean): Money;

  /**
   * Returns the total tax in purchase currency.
   */
  public getAdjustedMerchandizeTotalTax(): Money;

  /**
   * Returns the adjusted sum of all shipping line items of the shipment, including tax after shipping adjustments have been applied.
   */
  public getAdjustedShippingTotalGrossPrice(): Money;

  /**
   * Returns the sum of all shipping line items of the shipment, excluding tax after shipping adjustments have been applied.
   */
  public getAdjustedShippingTotalNetPrice(): Money;

  /**
   * Returns the adjusted shipping total price.
   */
  public getAdjustedShippingTotalPrice(): Money;

  /**
   * Returns the tax of all shipping line items of the shipment after shipping adjustments have been applied.
   */
  public getAdjustedShippingTotalTax(): Money;

  /**
   * Returns all line items related to the shipment.
   */
  public getAllLineItems(): Collection<LineItem>;

  /**
   * Returns all gift certificate line items of the shipment.
   */
  public getGiftCertificateLineItems(): Collection<GiftCertificateLineItem>;

  /**
   * Returns the value set for gift message or null if no value set.
   */
  public getGiftMessage(): string | null;

  /**
   * Returns the ID of this shipment.
   */
  public getID(): string;

  /**
   * Returns the total gross price, including tax, in the purchase currency.
   */
  public getMerchandizeTotalGrossPrice(): Money;

  /**
   * Returns the net price, excluding tax, in the purchase currency.
   */
  public getMerchandizeTotalNetPrice(): Money;

  /**
   * Returns the merchandize total price.
   */
  public getMerchandizeTotalPrice(): Money;

  /**
   * Returns the total tax in purchase currency.
   */
  public getMerchandizeTotalTax(): Money;

  /**
   * Returns a collection of all product line items related to this shipment.
   */
  public getProductLineItems(): Collection<ProductLineItem>;

  /**
   * Returns the merchandise total price of the shipment after considering all product price adjustments and prorating all Buy-X-Get-Y and order-level discounts, according to the scheme described in PriceAdjustment.getProratedPrices().
   */
  public getProratedMerchandizeTotalPrice(): Money;

  /**
   * Returns the shipment number for this shipment.
   */
  public getShipmentNo(): string;

  /**
   * Returns the shipping address or null if none is set.
   */
  public getShippingAddress(): OrderAddress | null;

  /**
   * Returns the shipping line item identified by the specified ID, or null if not found.
   * @param id
   */
  public getShippingLineItem(id: string): ShippingLineItem | null;

  /**
   * Returns a collection of all shipping line items of the shipment, excluding any product-level shipping costs that are associated with ProductLineItems of the shipment.
   */
  public getShippingLineItems(): Collection<ShippingLineItem>;

  /**
   * Returns the shipping method or null if none is set.
   */
  public getShippingMethod(): ShippingMethod | null;

  /**
   * Returns the shipping method ID or null if none is set.
   */
  public getShippingMethodID(): string | null;

  /**
     * Returns a collection of price adjustments that have been applied to the shipping costs of the shipment, for example by the promotions engine.
    Note that this method returns all shipping price adjustments in this shipment regardless of which shipping line item they belong to.
    */
  public getShippingPriceAdjustments(): Collection<PriceAdjustment>;

  /**
   * Returns the shipping status.
   */
  public getShippingStatus(): EnumValue<number>;

  /**
   * Returns the sum of all shipping line items of the shipment, including tax before shipping adjustments have been applied.
   */
  public getShippingTotalGrossPrice(): Money;

  /**
   * Returns the sum of all shipping line items of the shipment, excluding tax before shipping adjustments have been applied.
   */
  public getShippingTotalNetPrice(): Money;

  /**
   * Returns the shipping total price.
   */
  public getShippingTotalPrice(): Money;

  /**
   * Returns the tax of all shipping line items of the shipment before shipping adjustments have been applied.
   */
  public getShippingTotalTax(): Money;

  /**
   * Convenenience method.
   */
  public getStandardShippingLineItem(): ShippingLineItem;

  /**
   * Returns the grand total price gross of tax for the shipment, in purchase currency.
   */
  public getTotalGrossPrice(): Money;

  /**
   * Returns the grand total price for the shipment net of tax, in purchase currency.
   */
  public getTotalNetPrice(): Money;

  /**
   * Returns the total tax for the shipment, in purchase currency.
   */
  public getTotalTax(): Money;

  /**
   * Returns the tracking number of this shipment.
   */
  public getTrackingNumber(): string;

  /**
   * Return true if this shipment is the default shipment.
   */
  public isDefault(): boolean;

  /**
   * Returns true if this line item represents a gift, false otherwise.
   */
  public isGift(): boolean;

  /**
   * Removes the specified shipping line item and any of its dependent shipping price adjustments.
   * @param shippingLineItem
   */
  public removeShippingLineItem(shippingLineItem: ShippingLineItem): void;

  /**
   * Controls if this line item is a gift or not.
   * @param isGift
   */
  public setGift(isGift: boolean): void;

  /**
   * Sets the value to set for the gift message.
   * @param message
   */
  public setGiftMessage(message: string): void;

  /**
   * Set the specified shipping method for the specified shipment.
   * @param method
   */
  public setShippingMethod(method: ShippingMethod | null): void;

  /**
   * Sets the shipping status of the shipment.
   * @param status
   */
  public setShippingStatus(status: Number): void;

  /**
   * Sets the tracking number of this shipment.
   * @param aValue
   */
  public setTrackingNumber(aValue: string): void;
}

export = Shipment;
