import ProductListItem = require('../customer/ProductListItem');
import LineItem = require('./LineItem');
import Shipment = require('./Shipment');

declare class GiftCertificateLineItem extends LineItem {
  /**
   * The ID of the gift certificate that this line item was used to create. If this line item has not been used to create a Gift Certificate, this method returns null.
   */
  public giftCertificateID: string | null;

  /**
   * The message to include in the email of the person receiving the gift certificate line item.
   */
  public message: string;

  /**
   * The associated ProductListItem.
   */
  public productListItem: ProductListItem;

  /**
   * The email address of the person receiving the gift certificate line item.
   */
  public recipientEmail: string;

  /**
   * The name of the person receiving the gift certificate line item.
   */
  public recipientName: string;

  /**
   * The name of the person or organization that sent the gift certificate line item or null if undefined.
   */
  public senderName: string | null;

  /**
   * The associated Shipment.
   */
  public shipment: Shipment;

  private constructor();

  /**
   * Returns the ID of the gift certificate that this line item was used to create.
   */
  public getGiftCertificateID(): string;

  /**
   * Returns the message to include in the email of the person receiving the gift certificate line item.
   */
  public getMessage(): string;

  /**
   * Returns the associated ProductListItem.
   */
  public getProductListItem(): ProductListItem;

  /**
   * Returns the email address of the person receiving the gift certificate line item.
   */
  public getRecipientEmail(): string;

  /**
   * Returns the name of the person receiving the gift certificate line item.
   */
  public getRecipientName(): string;

  /**
   * Returns the name of the person or organization that sent the gift certificate line item or null if undefined.
   */
  public getSenderName(): string | null;

  /**
   * Returns the associated Shipment.
   */
  public getShipment(): Shipment;

  /**
   * Sets the ID of the gift certificate associated with this line item.
   * @param id  the ID of the gift certificate associated with this line item.
   */
  public setGiftCertificateID(id: string): void;

  /**
   * Sets the message to include in the email of the person receiving the gift certificate line item.
   * @param message  the message to include in the email of the person receiving the gift certificate line item.
   */
  public setMessage(message: string): void;

  /**
   * Sets the associated ProductListItem.
   * @param productListItem  the product list item to be associated
   */
  public setProductListItem(productListItem: ProductListItem): void;

  /**
   * Sets the email address of the person receiving the gift certificate line item.
   * @param recipientEmail  the email address of the person receiving the gift certificate line item.
   */
  public setRecipientEmail(recipientEmail: string): void;

  /**
   * Sets the name of the person receiving the gift certificate line item.
   * @param recipient the name of the person receiving the gift certificate line item.
   */
  public setRecipientName(recipient: string): void;

  /**
   * Sets the name of the person or organization that sent the gift certificate line item.
   * @param sender  the name of the person or organization that sent the gift certificate line item
   */
  public setSenderName(sender: string): void;

  /**
   * Associates the gift certificate line item with the specified shipment.
   * @param shipment The new shipment of the gift certificate line item
   */
  public setShipment(shipment: Shipment): void;
}

export = GiftCertificateLineItem;
