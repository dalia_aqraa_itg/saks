import Status = require('../system/Status');
import GiftCertificate = require('./GiftCertificate');
import OrderPaymentInstrument = require('./OrderPaymentInstrument');

declare class GiftCertificateMgr {
  /**
   * Creates a Gift Certificate.
   * @param amount
   * @param code
   */
  public static createGiftCertificate(amount: number, code: string): GiftCertificate;

  /**
   * Creates a Gift Certificate.
   * @param amount
   */
  public static createGiftCertificate(amount: number): GiftCertificate;

  /**
   * Returns the Gift Certificate identified by the specified gift certificate code.
   * @param giftCertificateCode
   */
  public static getGiftCertificate(giftCertificateCode: string): GiftCertificate;

  /**
   * Returns the Gift Certificate identified by the specified gift certificate code.
   * @param giftCertificateCode
   */
  public static getGiftCertificateByCode(giftCertificateCode: string): GiftCertificate;

  /**
   * Returns the Gift Certificate identified by the specified merchant ID.
   * @param merchantID
   */
  public static getGiftCertificateByMerchantID(merchantID: string): GiftCertificate;

  /**
   * Redeems an amount from a Gift Certificate.
   * @param paymentInstrument
   */
  public static redeemGiftCertificate(paymentInstrument: OrderPaymentInstrument): Status;
  private constructor();
}

export = GiftCertificateMgr;
