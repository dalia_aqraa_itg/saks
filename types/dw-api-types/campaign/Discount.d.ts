import Promotion = require('../campaign/Promotion');
/**
 * Superclass of all specific discount classes.
 */
declare class Discount {
  /**
   * Constant representing discounts of type amount.
   */
  public static readonly TYPE_AMOUNT: string;
  /**
   * Constant representing discounts of type bonus.
   */
  public static readonly TYPE_BONUS: string;

  /**
   * Constant representing discounts of type bonus choice.
   */
  public static readonly TYPE_BONUS_CHOICE: string;

  /**
   * Constant representing discounts of type fixed-price.
   */
  public static readonly TYPE_FIXED_PRICE: string;

  /**
   * Constant representing discounts of type fixed price shipping.
   */
  public static readonly TYPE_FIXED_PRICE_SHIPPING: string;

  /**
   * Constant representing discounts of type free.
   */
  public static readonly TYPE_FREE: string;

  /**
   * Constant representing discounts of type free shipping.
   */
  public static readonly TYPE_FREE_SHIPPING: string;

  /**
   * Constant representing discounts of type percentage.
   */
  public static readonly TYPE_PERCENTAGE: string;

  /**
   * Constant representing discounts of type percent off options.
   */
  public static readonly TYPE_PERCENTAGE_OFF_OPTIONS: string;

  /**
   * Constant representing discounts of type price book price.
   */
  public static readonly TYPE_PRICEBOOK_PRICE: string;

  /**
   * Constant representing discounts of type total fixed price.
   */
  public static readonly TYPE_TOTAL_FIXED_PRICE: string;

  protected constructor();

  /**
   * Returns the promotion this discount is based on.
   */
  public getPromotion(): Promotion;

  /**
   * Returns the quantity of the discount.
   */
  public getQuantity(): number;

  /**
   * Returns the type of the discount.
   */
  public getType(): string;
}

export = Discount;
