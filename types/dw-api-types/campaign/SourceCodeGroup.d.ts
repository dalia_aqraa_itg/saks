import PriceBook = require('../catalog/PriceBook');
import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Collection = require('../util/Collection');

declare global {
  namespace ICustomAttributes {
    interface SourceCodeGroup extends CustomAttributes {}
  }
}

declare class SourceCodeGroup extends ExtensibleObject<ICustomAttributes.SourceCodeGroup> {
  public ID: string;
  public priceBooks: Collection<PriceBook>;

  public getID(): string;
  public getPriceBooks(): Collection<PriceBook>;
}

export = SourceCodeGroup;
