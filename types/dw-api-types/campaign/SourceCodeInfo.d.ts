import URLRedirect = require('../web/URLRedirect');
import SourceCodeGroup = require('./SourceCodeGroup');

declare class SourceCodeInfo {
  public static STATUS_ACTIVE: number;
  public static STATUS_INACTIVE: number;
  public static STATUS_INVALID: number;

  public readonly code: string;
  public readonly group: SourceCodeGroup;
  public readonly redirect: URLRedirect | null;
  public readonly status: number;

  private constructor();

  public getCode(): string;
  public getGroup(): SourceCodeGroup;
  public getRedirect(): URLRedirect | null;
  public getStatus(): number;
}

export = SourceCodeInfo;
