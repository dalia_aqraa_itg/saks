import SourceCodeGroup = require('../campaign/SourceCodeGroup');
import Store = require('../catalog/Store');
import StoreGroup = require('../catalog/StoreGroup');
import CustomerGroup = require('../customer/CustomerGroup');
import CustomAttributes = require('../object/CustomAttributes');
import ExtensibleObject = require('../object/ExtensibleObject');
import Collection = require('../util/Collection');
import Coupon = require('./Coupon');
import Promotion = require('./Promotion');

declare global {
  namespace ICustomAttributes {
    interface Campaign extends CustomAttributes {}
  }
}

/**
 * A Campaign is a set of experiences (or site configurations) which may be deployed as a single unit for a given time frame. The system currently supports 3 types of experience that may be assigned to a campaign:

    1. Promotions
    2. Slot Configurations
    3. Sorting Rules

This list may be extended in the future.

A campaign can have a start and end date or be open-ended. It may also have "qualifiers" which determine which customers the campaign applies to. The currently supported qualifiers are:

    1. Customer groups (where "Everyone" is a possible customer group)
    2. Source codes
    3. Coupons

A campaign can have list of stores or store groups where it can be applicable to.
 */
declare class Campaign extends ExtensibleObject<ICustomAttributes.Campaign> {
  /**
     * Returns 'true' if the campaign is currently active, otherwise 'false'.
A campaign is active if it is enabled and scheduled for now.
     */
  public readonly active: boolean;

  /**
   * Returns true if campaign is applicable to store, otherwise false.
   */
  public readonly applicableInStore: boolean;

  /**
   * Returns true if campaign is applicable to online site, otherwise false.
   */
  public readonly applicableOnline: boolean;

  /**
   * The coupons assigned to the campaign.
   */
  public readonly coupons: Collection<Coupon>;

  /**
   * The customer groups assigned to the campaign
   */
  public readonly customerGroups: Collection<CustomerGroup>;

  /**
   * The internal description of the campaign.
   */
  public readonly description: string;

  /**
   * Returns true if campaign is enabled, otherwise false.
   */
  public readonly enabled: boolean;

  /**
   * The end date of the campaign. If no end date is defined for the campaign, null is returned. A campaign w/o end date will run forever.
   */
  public readonly endDate: Date | null;

  /**
   * The unique campaign ID.
   */
  public readonly ID: string;

  /**
   * Returns promotions defined in this campaign in no particular order.
   */
  public readonly promotions: Collection<Promotion>;
  /**
   * Get the promotion associated with this discount.
   */
  public readonly promotion: Promotion;

  /**
   * Get the promotion ID associated with this discount.
   */
  public readonly promotionID: string;

  /**
   * The source codes assigned to the campaign.
   */
  public readonly sourceCodeGroups: Collection<SourceCodeGroup>;

  /**
   * The start date of the campaign. If no start date is defined for the campaign, null is returned. A campaign w/o start date is immediately effective.
   */
  public readonly startDate: Date | null;

  /**
   * Returns store groups assigned to the campaign.
   */
  public readonly storeGroups: Collection<StoreGroup>;

  /**
   * Returns stores assigned to the campaign.
   */
  public readonly stores: Collection<Store>;
  private constructor();

  /**
   * Returns the coupons assigned to the campaign.
   */
  public getCoupons(): Collection<Coupon>;

  /**
   * Returns the customer groups assigned to the campaign.
   */
  public getCustomerGroups(): Collection<CustomerGroup>;

  /**
   * Returns the internal description of the campaign.
   */
  public getDescription(): string;

  /**
   * Returns the end date of the campaign.
   */
  public getEndDate(): Date | null;

  /**
   * Returns the unique campaign ID.
   */
  public getID(): string;

  /**
   * Returns promotions defined in this campaign in no particular order.
   */
  public getPromotions(): Collection<Promotion>;

  /**
   * Returns the source codes assigned to the campaign.
   */
  public getSourceCodeGroups(): Collection<SourceCodeGroup>;

  /**
   * Returns the start date of the campaign.
   */
  public getStartDate(): Date | null;

  /**
   * Returns store groups assigned to the campaign.
   */
  public getStoreGroups(): Collection<StoreGroup>;

  /**
   * Returns stores assigned to the campaign.
   */
  public getStores(): Collection<Store>;
  /**
   * Returns 'true' if the campaign is currently active, otherwise 'false'.
   */
  public isActive(): boolean;

  /**
   * Returns true if campaign is applicable to store, otherwise false.
   */
  public isApplicableInStore(): boolean;

  /**
   * Returns true if campaign is applicable to online site, otherwise false.
   */
  public isApplicableOnline(): boolean;

  /**
   * Returns true if campaign is enabled, otherwise false.
   */
  public isEnabled(): boolean;
}

export = Campaign;
