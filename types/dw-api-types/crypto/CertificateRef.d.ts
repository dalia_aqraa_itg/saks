declare class CertificateRef {
  constructor(alias: string);
  public tostring(): string;
}

export = CertificateRef;
