var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'hbcbuild123@gmail.com',
    pass: 'Pfsweb@123'
  }
});

var mailOptions = {
    from: "hbcbuild123@gmail.com",
    to: "pjahagirda@pfsweb.com",
    subject: "Node.js Email with Secure OAuth",
    generateTextFromHTML: true,
    html: "Build Complete for <strong>"+process.env['SFCC_SANDBOX_API_HOST'] +" </strong> instance. Code version: <strong>" +process.env['BuildVersion']+ "</strong>"
};

transporter.sendMail(mailOptions, function(error, info){
  if (error) {
    console.log(error);
  } else {
    console.log('Email sent: ' + info.response);
  }
});